/**
 * Created by surge on 2016-01-14.
 */

var initDocument = function (destId, pageUrl) { //Aka Destination Guide
  var docView = Object.create(DocumentView);
  docView.init(destId, pageUrl);
};

var DocumentView = {
  init: function(destId, pageUrl) {
    this.selDocType       = $('#inDestinationTypeId');
    this.inDestCity       = $('#inDestCity');
    this.inDestCountry    = $('#inDestCountry');
    this.inDestLocLat     = $('#inDestLocLat');
    this.inDestLocLong    = $('#inDestLocLong');
    this.inDestCountryLbl = $('#inDestCountryLbl');
    this.inDestCityLbl    = $('#inDestCityLbl');
    this.inDestPhotoFile  = $('#inDestPhotoFile');
    this.destinationForm  = $('#destinationForm');
    this.newPageBtn       = $('#newPageBtn');
    this.uploadBtn        = $('#uploadBtn');
    this.modalPhUp        = $('#uploadPhotoModal');
    this.btnPhUploadSubmit= $('#uploadPhotoModal #uploadPhotoSubmit');
    this.areaPageSort     = $('#sortDiv');

    var self = this;

    self.inDestCountry.autocomplete({
      source: countries
    });

    this.destinationForm.find(":input").change(function () {
      self.destinationForm.data('changed', true);
    });

    this.newPageBtn.on('click', function (event) {
      event.preventDefault();
      if (self.destinationForm.data('changed')) {
        return confirm('Your changes will be lost! Continue?');
      }
    });

    this.uploadBtn.on('click', function (event) {
      event.preventDefault();
      if (self.destinationForm.data('changed')) {
        return confirm('Your changes will be lost! Continue?');
      }
    });

    this.areaPageSort.sortable({
      helper: "clone",
      update: function (event, ui) {
        var order = $(this).sortable('toArray').toString();
        doPost('/guide/reorderPages?inDestId='+destId+'&inDestGuideIds=' + order);
      }
    });

    this.btnPhUploadSubmit.off('click');
    this.btnPhUploadSubmit.on('click', function(event){ //uploadPhoto function in the past
      event.preventDefault();

      var uploadurl= '/guide/signedS3CoverUrl?inDestId='+destId+'&inDestPhotoName=' +
                     escapeFilename($("#inDestPhotoFile").val().replace("C:\\fakepath\\", ""));

      $("#uploadSpinner").show();
      if($.browser.msie) {
        signedS3(uploadurl, function(signedURL, publicURL, s3Bucket) {
          return iFrameUpload( signedURL, publicURL, s3Bucket);
        });
      } else {
        var s3upload = s3upload != null ? s3upload : new S3Upload({
          file_dom_selector: 'inDestPhotoFile',
          s3_sign_put_url: uploadurl,
          s3_object_caption: $("#inDestPhotoCaption").val(),
          onProgress: function(percent, message) {
            $("#progressUpload").text(percent + "%");
          },
          onFinishS3Put: function(public_url) {
            $('#uploadPhotoModal').modal('hide');
            getPage(public_url);
            console.log('Upload finished: ', public_url);
          },
          onError: function(status) {
            alert('Upload error: ', status);
            console.log('Upload error: ', status);
          }
        });
      }
    });

    this.inDestPhotoFile.off('change');
    this.inDestPhotoFile.on('change', function() {
      var extension = $("#inDestPhotoFile").val().substring($("#inDestPhotoFile").val().lastIndexOf('.'));
      var ValidFileType = ".jpeg, .jpg , .png , .gif";
      if (ValidFileType.toLowerCase().indexOf(extension.toLowerCase()) < 0) {
        alert("Please only select an image file of type (JPG, GIF, PNG)");
        $("#uploadPhotoSubmit").attr('disabled', true);
        return;
      }

      if ( !$.browser.msie ) {
        var size = this.files[0].size;
        if (size > 1024000) {
          alert("The image file must be less than 1MB");
          $("#uploadPhotoSubmit").attr('disabled', true);
          return;
        }
      }

      $('#pretty-input').val($(this).val().replace("C:\\fakepath\\", ""));
      $("#uploadPhotoSubmit").attr('disabled', false);
    });


    this.inDestCity.autocomplete({
      source: function (request, response) {
        $.ajax({
          url: "http://api.geonames.org/searchJSON",
          dataType: "jsonp",
          data: {
            username: "umapped",
            featureClass: "P",
            style: "full",
            maxRows: 12,
            name_startsWith: request.term
          },
          success: function (data) {
            response($.map(data.geonames, function (item) {
              return {
                label: item.name + (item.adminName1 ? ", " + item.adminName1 : "") +
                       ", " + item.countryName,
                value: item.name,
                name: item.lng + "," + item.lat
              }
            }));
          }
        });
      },
      minLength: 2,
      select: function (event, ui) {
        var array = ui.item.label.split(',');
        var gps = ui.item.name.split(',');
        if (array[2] == null && array[1] != null) {
          $("#inDestCountry").val(array[1]);
        }
        else {
          $("#inDestCountry").val(array[2]);
        }
        if (gps[0] != null && gps[1] != null) {
          $("#inDestLocLong").val(gps[0]);
          $("#inDestLocLat").val(gps[1]);
        }
        else {
          $("#inDestLocLong").val("");
          $("#inDestLocLat").val("");
        }
      },
      open: function () {
        $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
      },
      close: function () {
        $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
      }
    });

    this.selDocType.off('change');
    this.selDocType.on('change', function(event){
      var typeId = $(this).val();
      var typeName = $(this).find('option:selected').text();
      self.switchDocType(typeId, typeName);
    });

    self.selDocType.trigger('change');

    if(destId) {
      initMobilizer();
      initT42(destId, null, null, pageUrl);
    }
  },


  switchDocType: function(typeId, typeName) {
    var self = this;
    console.log('Type changed to ' + typeId + ' ' + typeName);
    var isSimpleType = false;
    switch (typeId) {
      case '1':
          self.inDestCityLbl.text('City');
          self.inDestCountryLbl.text('Country *');
          self.inDestCountry.addClass('required');
          self.inDestCity.removeClass('required');
        break;
      case '2':
        self.inDestCityLbl.text('City *');
        self.inDestCountryLbl.text('Country *');
        self.inDestCountry.addClass('required');
        self.inDestCity.addClass('required');
        break;
      case '8':
      case '9':
        isSimpleType = true;
        self.inDestCountry.removeClass('required');
        self.inDestCity.removeClass('required');
        break;
      default:
        self.inDestCityLbl.text('City');
        self.inDestCountryLbl.text('Country');
        self.inDestCountry.removeClass('required');
        self.inDestCity.removeClass('required');
        break;
    }

    //Fallback if ID is different on this server instance
    if(!isSimpleType) {
      switch (typeName) {
        case 'General':
        case 'Company Information':
            isSimpleType = true;
          break;
        default:
          break;
      }
    }

    if(isSimpleType) {
      self.inDestCity.parent().hide();
      self.inDestCountry.parent().hide();
      self.inDestLocLat.parent().hide();
      self.inDestLocLong.parent().hide();
    } else {
      self.inDestCity.parent().show();
      self.inDestCountry.parent().show();
      self.inDestLocLat.parent().show();
      self.inDestLocLong.parent().show();
    }
  }
};

var editDocument = function(url) {
  getPage(url);
}

var allDocumentsUI = {
  init: function(jsonUrl) {
    this.tblDocuments = $('#allDocumentsTable');
    this.tblJsonUrl   = jsonUrl;

    var allDocumentListUI = this;
    this.tblDocuments
        .DataTable({
      "processing": true,
      "serverSide": true,
      "sDom": 'ltipr',
      "oLanguage": {
         "sInfoFiltered": "",
         "sLengthMenu": "Show _MENU_ Documents",
         "sInfo": "",
         "sInfoEmpty": ""
      },
      "order": [[ 1, "asc" ]],
      "columnDefs": [
        { "data": "imgUrl",   "targets" : 0, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = allDocumentListUI.renderDocumentCover(data);
              return res;
            } else {
              return data;
            }
          }
        },
        { "data": "docName", "name": "name",  "targets" : 1, "searchable" : false, "orderable": true },
        { "data": "type", "name": "destinationtypeid",  "targets" : 2, "searchable" : false, "orderable": false },
        { "data": "about",   "targets" : 3, "searchable" : false, "orderable": false },
        { "data": "createdBy", "name": "createdby",  "targets" : 4, "searchable" : false, "orderable": true },
        { "data": "createdTime", "name": "createdtimestamp", "targets" : 5, "searchable" : false, "orderable": true },
        { "data": "modifiedBy", "name": "modifiedby", "targets" : 6, "searchable" : false, "orderable": true },
        { "data": "modifiedTime", "name": "lastupdatedtimestamp", "targets" : 7, "searchable" : false, "orderable": true },
        { "data": "id",         "targets" : 8, "searchable" : false, "orderable": false,
          "render" : function(data, type, row, meta) {
            if (type === 'display' ) {
              var res = allDocumentListUI.renderDocEditButton(data);
              var str = row["canEdit"];
              if (!str) {
                  res =  allDocumentListUI.renderDocViewButton(data);
              }
              return res;
            } else {
              return data;
            }
          }
        }
      ],
      "language": {
        "emptyTable":     "No Documents Found",
        "searchPlaceholder": "Enter search keyword",
        "lengthMenu": "Show _MENU_ Documents",
        "info": ""
      },
      "ajax": {
        url: allDocumentListUI.tblJsonUrl,
        type: 'POST',
        contentType: "application/json",
        data: function (d) {
          d.cmpyId = $('#inCmpyId').val();
          d.destType = $('#inDestType').val();
          if($('#inSearchTitleOnly').is(":checked")){
            d.titleOnly = true;
          } else if($('#inSearchTitleOnly').is(":not(:checked)")){
            d.titleOnly = false;
          }
          d.keyword = $('#searchTerm').val();
          console.log(JSON.stringify(d));
          return  JSON.stringify(d);
        },
        error: function (xhr, textStatus, errorThrown) {
          window.location.replace("/login");
        }
      }
    });

  },
  renderDocEditButton: function(id) {
    btn = '<a role="button" style="width: 100px;" class="btn btn-info input-block-level pull-right" onclick="javascript:editDocument(\'' + id + '\');return false;"> Edit </a>' ;
    return btn;
  },
  renderDocViewButton: function(id) {
      btn = '<a role="button" style="width: 100px;" class="btn btn-info input-block-level pull-right" onclick="javascript:getModalPage(\'' + id + '\');return false;"> View </a>' ;
      return btn;
    },
  renderDocumentCover: function(imgUrl) {
    if (imgUrl != null) {
      btn = '<div style="background-image:url(' + imgUrl + ');center;width:62px;height:62px;margin:auto;background-size:cover;" >&nbsp;</div>';
      return btn;
    } else {
      btn = '<div style="background-image:url(https://s3.amazonaws.com/static-umapped-com/public/img/photo-placeholder-gray.png);center;width:62px;height:62px;margin:auto;background-size:cover;" >&nbsp;</div>';
      return btn;
    }
  }

};

var initAllDocuments = function(jsonUrl) {
  var allDocumentsTable = Object.create(allDocumentsUI);
  allDocumentsTable.init(jsonUrl);
};

var searchDocumentsUI = {
  init: function(jsonUrl) {
    this.tblDocuments = $('#searchDocumentsTable');
    this.tblJsonUrl   = jsonUrl;

    var searchDocumentListUI = this;
    this.tblDocuments
        .DataTable({
      "processing": true,
      "serverSide": true,
      "sDom": 'ltipr',
      "bLengthChange": false,
      "oLanguage": {
         "sInfoFiltered": "",
         "sLengthMenu": "Show _MENU_ Documents",
         "sInfo": "",
         "sInfoEmpty": ""
      },
      "columnDefs": [
        { "data": "docName", "name": "name",  "targets" : 0, "searchable" : false, "orderable": true },
        { "data": "type",   "targets" : 1, "searchable" : false, "orderable": false },
        { "data": "about",   "targets" : 2, "searchable" : false, "orderable": false },
        { "data": "id",         "targets" : 3, "searchable" : false, "orderable": false,
          "render" : function(data, type, row, meta) {
            if (type === 'display' ) {
              var res = searchDocumentListUI.renderDocAddButton(data);
              return res;
            } else {
              return data;
            }
          }
        }
      ],
      "language": {
        "emptyTable":     "No Documents Found",
        "searchPlaceholder": "Enter search keyword",
        "lengthMenu": "Show _MENU_ Documents",
        "info": ""
      },
      "ajax": {
        url: searchDocumentListUI.tblJsonUrl,
        type: 'POST',
        contentType: "application/json",
        data: function (d) {
          d.tripId = $('#inTripId').val();
          d.destType = $('#inDestType').val();
          if($('#inSearchTitleOnly').is(":checked")){
            d.titleOnly = true;
          } else if($('#inSearchTitleOnly').is(":not(:checked)")){
            d.titleOnly = false;
          }
          d.keyword = $('#searchTerm').val();
          console.log(JSON.stringify(d));
          return  JSON.stringify(d);
        },
        error: function (xhr, textStatus, errorThrown) {
            window.location.replace("/login");
        }
      }
    });

  },
  renderDocAddButton: function(id) {
    btn = '<a role="button" style="width: 60px;" class="btn btn-info input-block-level pull-right" onclick="javascript:$(\'#searchGuideModal\').modal(\'hide\');doPost(\'' + id + '\');return false;"> Add </a>' ;
    return btn;
  },
  renderDocumentCover: function(imgUrl) {
    if (imgUrl != null) {
      btn = '<div style="background-image:url(' + imgUrl + ');center;width:62px;height:62px;margin:auto;background-size:cover;" >&nbsp;</div>';
      return btn;
    } else {
      btn = '<div style="background-image:url(https://s3.amazonaws.com/static-umapped-com/public/img/photo-placeholder-gray.png);center;width:62px;height:62px;margin:auto;background-size:cover;" >&nbsp;</div>';
      return btn;
    }
  }

};

var initSearchDocuments = function(jsonUrl) {
  var searchDocumentsTable = Object.create(searchDocumentsUI);
  searchDocumentsTable.init(jsonUrl);
};



var allPagesUI = {
  init: function(jsonUrl) {
    this.tblPages = $('#allPagesTable');
    this.tblJsonUrl   = jsonUrl;

    var allPageListUI = this;
    this.tblPages
        .DataTable({
      "processing": true,
      "serverSide": true,
      "sDom": 'ltipr',
      "oLanguage": {
         "sInfoFiltered": "",
         "sLengthMenu": "Show _MENU_ Pages",
         "sInfo": "",
         "sInfoEmpty": ""
      },
      "columnDefs": [
        { "data": "imgUrl",   "targets" : 0, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = allPageListUI.renderPageCover(data);
              return res;
            } else {
              return data;
            }
          }
        },
        { "data": "docName",   "targets" : 1, "searchable" : false, "orderable": false },
        { "data": "pageName", "name": "name",  "targets" : 2, "searchable" : false, "orderable": true },
        { "data": "about",   "targets" : 3, "searchable" : false, "orderable": false },
        { "data": "createdBy", "name": "createdby",  "targets" : 4, "searchable" : false, "orderable": true },
        { "data": "createdTime", "name": "createdtimestamp", "targets" : 5, "searchable" : false, "orderable": true },
        { "data": "modifiedBy", "name": "modifiedby", "targets" : 6, "searchable" : false, "orderable": true },
        { "data": "modifiedTime", "name": "lastupdatedtimestamp", "targets" : 7, "searchable" : false, "orderable": true },
        { "data": "id",         "targets" : 8, "searchable" : false, "orderable": false,
          "render" : function(data, type, row, meta) {
            if (type === 'display' ) {
              var res = allPageListUI.renderPageEditButton(data);
              var str = row["canEdit"];
              if (!str) {
                res =  allPageListUI.renderPageViewButton(data);
              }
              return res;
            } else {
              return data;
            }
          }
        }
      ],
      "language": {
        "emptyTable":     "No Pages Found",
        "searchPlaceholder": "Enter search keyword",
        "lengthMenu": "Show _MENU_ Pages",
        "info": ""
      },
      "ajax": {
        url: allPageListUI.tblJsonUrl,
        type: 'POST',
        contentType: "application/json",
        data: function (d) {
          d.cmpyId = $('#inCmpyId').val();
          if($('#inSearchTitleOnly').is(":checked")){
            d.titleOnly = true;
          } else if($('#inSearchTitleOnly').is(":not(:checked)")){
            d.titleOnly = false;
          }
          d.keyword = $('#searchTerm').val();
          console.log(JSON.stringify(d));
          return  JSON.stringify(d);
        },
        error: function (xhr, textStatus, errorThrown) {
          window.location.replace("/login");
        }
      }
    });

  },
  renderPageEditButton: function(id) {
    btn = '<a role="button" style="width: 100px;" class="btn btn-info input-block-level pull-right" onclick="javascript:editDocument(\'' + id + '\');return false;"> Edit </a>' ;
    return btn;
  },
  renderPageViewButton: function(id) {
      btn = '<a role="button" style="width: 100px;" class="btn btn-info input-block-level pull-right" onclick="javascript:getModalPage(\'' + id + '\');return false;"> View </a>' ;
      return btn;
    },
  renderPageCover: function(imgUrl) {
    if (imgUrl != null) {
      btn = '<div style="background-image:url(' + imgUrl + ');center;width:62px;height:62px;margin:auto;background-size:cover;" >&nbsp;</div>';
      return btn;
    } else {
      btn = '<div style="background-image:url(https://s3.amazonaws.com/static-umapped-com/public/img/photo-placeholder-gray.png);center;width:62px;height:62px;margin:auto;background-size:cover;" >&nbsp;</div>';
      return btn;
    }
  }

};

var initAllPages = function(jsonUrl) {
  var allPagesTable = Object.create(allPagesUI);
  allPagesTable.init(jsonUrl);
};