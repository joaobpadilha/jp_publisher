(function () {

  console.log("Travel 42 File Loaded");

  // Establish a reference to the `window` object, and save the previous value
  // of the `Travel42` variable.
  var root = this;

  function Travel42(el, options) {
    var self = this, options = options || {};

    if (!el) {
      throw new Error('Travel42: Missing required argument `el`');
    }

    self._options = options;
  }

  Travel42.prototype = {};

})();


var initT42 = function(destId, tripId, date, onAddUrl) {
  console.warn("T42 is Initializing.");

  $(document).off('keypress', '#t42Destination');
  $(document).on('keypress', '#t42Destination', function(event){
    if(event.keyCode == 13) {
      $('#t42DestSearchBtn').trigger('click');
    }
  });

  $(document).undelegate('#t42DestSearchBtn', 'click');
  $(document).delegate('#t42DestSearchBtn', 'click', function(){
    console.warn("Delegate t42DestSearchBtn called...");
    var term = $('#t42Destination').val();
    var url = '/guide/t42/search?term=' + term;
    getHtmlAndReplace(url, 't42SearchResults', function(){
      //Initializing grid table here
      var table = $('#t42DestSearchResultsTable');
      if(isDataTable(table[0])){
        table.DataTable().destroy();
      }
      table.DataTable({
        paging: true,
        lengthChange: false,
        pageLength: 10,
        searching: false
      });
    });
  });

  $(document).off('click', 'a[data-target=t42DestChoice]');
  $(document).on('click', 'a[data-target=t42DestChoice]', 'click', function(){
    var placeKey = $(this).data('key');
    var url = '/guide/t42/destination?placeKey=' + placeKey;
    $('#t42DestinationGudes').children().remove();
    getHtmlAndReplace(url, 't42DestinationGudes', function(){
      $.jStorage.deleteKey(getJStorageKey('t42_guides'));

      $.fn.modal.defaults.maxHeight = function(){
        // subtract the height of the modal header and footer
        return $(window).height() - 165;
      };

      $('#t42DestGuidesModal').modal('show');
    });
  });

  $(document).off('click', 'a[data-target=t42ContentAdd]');
  $(document).on('click', 'a[data-target=t42ContentAdd]',  function(event) {
    var destId = $(this).data('dest');
    var seqNum = $(this).data('number');
    var tab = $(this).data('tab');

    var guides = $.jStorage.get(getJStorageKey('t42_guides'));
    if (guides == null) {
      guides = {};
    }

    if(guides[destId] == null) {
      guides[destId] = {};
    }

    var currGuides = guides[destId][tab];
    if(currGuides == null) {
      currGuides = [];
    }
    var isNew = true;
    for (var idx = 0; idx < currGuides.length; idx++) { //Checking for duplicates
      if (currGuides[idx] == seqNum) {
        isNew = false;
      }
    }

    if (isNew) {
      currGuides.push(seqNum);
      guides[destId][tab] = currGuides;
      $.jStorage.set(getJStorageKey('t42_guides'), guides);
    }
    $('#' + tab + '_count').text(currGuides.length);
    $(this).attr('data-target', 't42ContentRemove');
    $(this).children().remove();
    $(this).append($('<i class="fa fa-minus-circle fa-lg"></i>'));
  });

  $(document).off('click', 'a[data-target=t42ContentRemove]');
  $(document).on('click', 'a[data-target=t42ContentRemove]', function(event) {
    var destId = $(this).data('dest');
    var seqNum = $(this).data('number');
    var tab = $(this).data('tab');

    var counter = $('#' + tab + '_count');

    var guides = $.jStorage.get(getJStorageKey('t42_guides'));
    if (guides == null) {
      guides = {};
    }
    if(guides[destId] == null) {
      guides[destId] = {};
    }
    var currGuides = guides[destId][tab];
    for (var idx = 0; idx < currGuides.length; idx++) { //Checking for duplicates
      if (currGuides[idx] == seqNum) {
        currGuides.splice(idx, 1);
        break;
      }
    }
    guides[destId][tab] = currGuides;
    $.jStorage.set(getJStorageKey('t42_guides'), guides);
    counter.text(currGuides.length);
    $(this).attr('data-target', 't42ContentAdd');
    $(this).children().remove();
    $(this).append($('<i class="fa fa-plus-circle fa-lg"></i>'));
  });


  $(document).off('click', 'a[data-target=t42HotelAdd]');
  $(document).on('click', 'a[data-target=t42HotelAdd]',  function(event) {
    var hotelId = $(this).data('hotel');
    var tab = $(this).data('tab');

    var hotels = $.jStorage.get(getJStorageKey('t42_hotels'));
    if (hotels == null) {
      hotels = [];
    }

    if(hotels[hotelId] == null) {
      hotels.push(hotelId);
    }

    $.jStorage.set(getJStorageKey('t42_hotels'), hotels);

    $('#' + tab + '_count').text(hotels.length);
    $(this).attr('data-target', 't42HotelRemove');
    $(this).children().remove();
    $(this).append($('<i class="fa fa-minus-circle fa-lg"></i>'));
  });


  $(document).off('click', 'a[data-target=t42HotelRemove]');
  $(document).on('click', 'a[data-target=t42HotelRemove]', function(event) {
    var hotelId = $(this).data('hotel');
    var tab = $(this).data('tab');

    var counter = $('#' + tab + '_count');

    var hotels = $.jStorage.get(getJStorageKey('t42_hotels'));
    if (hotels == null) {
      hotels = [];
    }

    var idx = hotels.indexOf(hotelId);
    if(idx >= 0) {
      hotels.splice(idx, 1); //Removing
    }

    $.jStorage.set(getJStorageKey('t42_hotels'), hotels);
    counter.text(hotels.length);
    $(this).attr('data-target', 't42ContentAdd');
    $(this).children().remove();
    $(this).append($('<i class="fa fa-plus-circle fa-lg"></i>'));
  });

  $(document).off('click', 'button[data-target=t42AddSelected]');
  $(document).on('click', 'button[data-target=t42AddSelected]', function(event){
    var url = '';

    if(destId) {
      url = '/guide/t42/todest/' + destId;
    }

    if(tripId) {
      url = '/trips/' + tripId + '/t42notes?date=' + date;
    }

    var guides =  $.jStorage.get(getJStorageKey('t42_guides'));
    var hotels =  $.jStorage.get(getJStorageKey('t42_hotels'));

    if(guides == null && hotels == null) {
      return;
    }

    var packet = {
      destinations: guides,
      hotels: hotels
    };

    $.ajax(
        {
          type: "POST",
          url: url,
          data: JSON.stringify(packet),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        }).always(
        function (data, textStatus, jqXHR) {
          if (textStatus == 'success') {
            $.jStorage.deleteKey(getJStorageKey('t42_guides'));
            $.jStorage.deleteKey(getJStorageKey('t42_hotels'));

            $('#t42DestGuidesModal').modal('hide');
            $('#t42SearchModal').modal('hide');

            if(onAddUrl) {
              getPage(onAddUrl);
            } else {
              reloadPage();
            }
          }
          else if (textStatus == 'parsererror') {
            //Need to go to login page
            window.location.href = "/";
          }
          else if (textStatus != 'abort') {
            switch (jqXHR.status) {
              case 503:
              case 504:
                headLineAlertDisplay("Request timeout. Please try again in a couple of minutes.");
                break;
              default:
                headLineAlertDisplay("Unknown System Error has occurred - " + textStatus);
            }
          }
        });
  });
};






