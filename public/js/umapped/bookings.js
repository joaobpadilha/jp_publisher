/**
 * Utilities used in booking management screens.
 *
 * Prefix all functions in this module with bk*
 */
var origFlightNum;

var itineraryViewInit = function(itineraryUrl) {
  var itView = Object.create(ItineraryView);
  itView.init(itineraryUrl);
};

var ItineraryView = {
  init: function (itineraryUrl) {
      initMobilizer();
    this.addContent = $('.btnT42ContentAdd');
    this.addExternalContent = $('.btnExternalContentAdd');
    this.addApproachGuide = $('.btnAddApproachGuide');
    this.afarContent = $('.btnAfarContentAdd');
    this.t42SearchModal = $('#t42SearchModal');
    this.mobilizerModal = $('#mobilizerModal');
    this.approachModal = $('#approachModal');
    this.afarModal = $('#afarSearchModal');
    this.btnDelBookingSubmit = $('#deleteBookingSubmit');
    this.modalDelBk  = $('#deleteModal');
    this.btnDelNoteSubmit = $('#deleteNoteSubmit');
    this.modalDelNote = $('#deleteNoteModal');
    this.btnCopyPageFromDoc = $('.btnCopyPageFromDocs');
    this.btnDeleteNote = $('.btnDeleteNote');
    this.btnImportFromTmplt = $('#btnImportFromTmplt');

    var self = this;
    this.addContent.click(function (e) {
      console.log('Showing T42 Modal');
      self.t42SearchModal.modal('show');

      var date   = $(this).data('date');
      var tripId = $(this).data('tripid');
      initT42(null, tripId, date, itineraryUrl);
    });

    this.addExternalContent.click(function (e) {
          console.log('Showing External Modal');
        var date   = $(this).data('date');
        $("#webSearchDate").val(date);
        self.mobilizerModal.modal('show');
    });


    this.addApproachGuide.click(function (e) {
        console.log('Showing Approach Modal');
        var date   = $(this).data('date');
        $("#approachDate").val(date);
        self.approachModal.modal('show');
    });

    this.afarContent.click(function (e) {
        console.log('Showing Afar Modal');
        self.afarModal.modal('show');
        var date = $(this).data('date');
        var tripId = $(this).data('tripid');
        initAfar(null, tripId, date, itineraryUrl);
    });

    this.btnDelBookingSubmit.click(function() {
      self.modalDelBk.modal('hide');
    });

    this.btnDelNoteSubmit.click(function() {
      self.modalDelNote.modal('hide');
    });

    this.btnCopyPageFromDoc.click(function(){
      var dt = $(this).data('date');
      self.showDocumentSearch(dt);
    });
    this.btnDeleteNote.click(function(){
      var noteId = $(this).data('noteid');
      var name = $(this).data('name');
      self.deleteNote(noteId, name);
    });

    this.btnImportFromTmplt.click(function(){
      self.showCalendarTab();
    });

    $('#showbookings').ddslick({
      width: 200,
      onSelected: function (data) {
        //callback function: do something with selectedData;
        var selected = data.selectedData.value;

        $('#allBookingList').hide();
        $('#dateNavigation').hide();
        $('#activitiesList').hide();
        $('#flightsList').hide();
        $('#cruisesList').hide();
        $('#transportsList').hide();
        $('#hotelsList').hide();
        $('#notesList').hide();

        if (selected == 'FLIGHTS') {
          $('#flightsList').show();
        }
        else if (selected == 'TRANSPORTS') {
          $('#transportsList').show();
        }
        else if (selected == 'HOTELS') {
          $('#hotelsList').show();
        }
        else if (selected == 'CRUISES') {
          $('#cruisesList').show();
        }
        else if (selected == 'ACTIVITIES') {
          $('#activitiesList').show();
        }
        else if (selected == 'NOTES') {
          $('#notesList').show();
        }
        else {
          $('#allBookingList').show();
          $('#dateNavigation').show();

        }
      }
    });

    $('#dateNavigation').ddslick({
      width: 150,
      onSelected: function (data) {
        //callback function: do something with selectedData;
        var selected = data.selectedData.value;
        scrollTo(selected);
      }
    });
  },

  deleteNote: function (noteId, noteName) {
    if (noteId != null) {
      $("#deleteNoteId").val(noteId);
      $("#deleteNoteModalMsg").text("Delete " + noteName + "?");
      $("#deleteNoteModal").modal('show');
    }
  },

  showDocumentSearch: function (date) {
    $("#searchPageModalDate").val(date);
    $("#searchPageModal").modal('show');
  },

  showCalendarTab: function () {
    $('#bookingTabs a[href="#templateTab"]').tab('show');
  }
};

var bkInit = function (tripStartDate, cmpyId, cmpyIdInt, tabName) {
  //console.log("bkInit( tabName: " + tabName + ")")
  $('#deleteBookingSubmit').click(function() {
    $('#deleteModal').modal('hide');
  });

  var reloadPhotos = false;

  if (tabName == "FLIGHTS") {
    $("#flightArrivalDate").datepicker({
      minDate: new Date(tripStartDate),
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 2
    });

    $("#flightDate").datepicker({
      minDate: new Date(tripStartDate),
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 2,
      onClose: function (selectedDate) {
        var date1 = new Date(selectedDate);
        $("#flightArrivalDate").datepicker("option",
            "minDate",
            new Date(date1.getFullYear(), date1.getMonth(), date1.getDate() - 1));
      }
    });

    $('#flightTime').timepicker();
    $('#flightArrivalTime').timepicker();
    $('#flightTime').on('click', function (e) {
      $('#flightTime').timepicker('showWidget');
    });

    $('#flightArrivalTime').on('click', function (e) {
      $('#flightArrivalTime').timepicker('showWidget');
    });
    poiAutocompleteRegister('flightDepartAirport', 'flightDepartAirportId', cmpyId, 2, true);
    poiAutocompleteRegister('flightArrivalAirport', 'flightArrivalAirportId', cmpyId, 2, true);

    /* Photos Re-ordering functionality */
    $('#FLIGHT_BookingForm #photosBtn').unbind('click').click(function(){
      bkPhotosBtnAction('FLIGHT_BookingForm', 'flightAirlineId', reloadPhotos);
    });

    $("#FLIGHT_BookingForm :input").change(function() {
      $("#FLIGHT_BookingForm").data('changed', true);
    });
  }

  //Flight Code Testing
  $("#flightNumber").focusin(function() {
    origFlightNum = $(this).val().trim().toUpperCase();

  });

    //Flight Code Testing
  $("#flightNumber").focusout(function() {
    var text = $(this).val().trim().toUpperCase();

    if (text != origFlightNum) {
      var company = cmpyIdInt;
      var packet = {
        term: text, cmpy: company
      };

      //alert($(this).val());
      $.ajax({
        type: "POST",
        url: '/flight/search',
        data: JSON.stringify(packet),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
          if (data) {
            //var jsonData = $.parseJSON(data);
            poiInputFieldSetValues('flightDepartAirport',
                'flightDepartAirportId',
                data[0].departAirportLabel,
                data[0].departAirportId);
            poiInputFieldSetValues('flightArrivalAirport',
                'flightArrivalAirportId',
                data[0].arrivalAirportLabel,
                data[0].arrivalAirportId);
            $('#flightTime').val(data[0].departTime);
            $('#flightTime').timepicker('setTime', data[0].departTime);
            $('#flightArrivalTime').val(data[0].arrivalTime);
            $('#flightArrivalTime').timepicker('setTime', data[0].arrivalTime);
          }

        }
      });
    }

  });

  if (tabName == "HOTELS") {
    /*******************
     * Hotels Specific *
     *******************/
    var hotelPoiId = $('#hotelProviderId').val();
    if (hotelPoiId.length > 0) {
      bkGetPoiAndCheckBenefits(hotelPoiId, cmpyIdInt);
    }
    $('#hotelProviderId' ).unbind('change').change(function(){
      if ($(this).val() > 0) {
        bkGetPoiAndCheckBenefits($(this).val(), cmpyIdInt);
      }
    });

    $('#amenitiesPasteBtn').unbind('click').click(function(){
      var amenitiesLbl = $('#amenitiesBtnArea > label').text();

      if (amenitiesLbl != null && amenitiesLbl.indexOf('Ensemble') > -1) {
        var r = confirm("IMPORTANT NOTICE\n\nBefore adding the Ensemble amenities to your booking, please be absolutely sure that you have booked the correct rate code (GHL or GHP) that entitles your client to receive these amenities.\n\nPlease also note that hotels do alter the amenities they provide during the course of the year.\n\nAlways verify that the amenity information is current by consulting the amenity chart on the Ensemble Extranet.");
        if (r == true) {
          var hotelNote = $('#hotelNote');
          if (hotelNote.val().indexOf(bkAmenitiesBenefits) == -1) {
            hotelNote.val(hotelNote.val() +"\n" +   bkAmenitiesBenefits);
          }
        }
      } else {
        var hotelNote = $('#hotelNote');
        if (hotelNote.val().indexOf(bkAmenitiesBenefits) == -1) {
          hotelNote.val(hotelNote.val() +"\n" +   bkAmenitiesBenefits);
        }
      }

    });

    $("#hotelCheckOut").datepicker({
      minDate: new Date(tripStartDate),
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 2
    });

    $("#hotelCheckIn").datepicker({
      minDate: new Date(tripStartDate),
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 2,
      onClose: function (selectedDate) {
        $("#hotelCheckOut").datepicker("option", "minDate", selectedDate);
      }
    });

    $('#hotelCheckinTime').timepicker();
    $('#hotelCheckoutTime').timepicker();
    $('#hotelCheckinTime').on('click', function (e) {
      $('#hotelCheckinTime').timepicker('showWidget');
    });

    $('#hotelCheckoutTime').on('click', function (e) {
      $('#hotelCheckoutTime').timepicker('showWidget');
    });
    poiAutocompleteRegister('hotelName', 'hotelProviderId', cmpyId, 3, false, bkOnPoiClear);

    $('#HOTEL_BookingForm #photosBtn').unbind('click').click(function(){
      bkPhotosBtnAction('HOTEL_BookingForm', 'hotelProviderId', reloadPhotos);
    });

    $("#HOTEL_BookingForm :input").change(function() {
      $("#HOTEL_BookingForm").data('changed', true);
    });
  }

  if (tabName == "CRUISES") {
    $("#cruiseDepartDate").datepicker({
      minDate: new Date(tripStartDate),
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 2,
      onClose: function (selectedDate) {
        $("#cruiseArriveDate").datepicker("option", "minDate", selectedDate);
      }
    });

    $("#cruiseArriveDate").datepicker({
      minDate: new Date(tripStartDate),
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 2
    });

    $('#cruiseBoardTime').timepicker();
    $('#cruiseDisembarkTime').timepicker();
    $('#cruiseDisembarkTime').on('click', function (e) {
      $('#cruiseDisembarkTime').timepicker('showWidget');
    });

    $('#inCruiseBoardTime').on('click', function (e) {
      $('#inCruiseBoardTime').timepicker('showWidget');
    });
    poiAutocompleteRegister('searchShipName', 'searchShipProviderId', cmpyId, 6, true);
    poiAutocompleteRegister('cruiseName', 'cruiseProviderId', cmpyId, 6, false, bkOnPoiClear);
    poiAutocompleteRegister('cruisePortDepart', 'cruisePortDepartId', cmpyId, 7);
    poiAutocompleteRegister('cruisePortArrive', 'cruisePortArriveId', cmpyId, 7);

    $('#CRUISE_BookingForm #photosBtn').unbind('click').click(function(){
      bkPhotosBtnAction('CRUISE_BookingForm', 'cruiseProviderId', reloadPhotos);
    });

    $("#CRUISE_BookingForm :input").change(function() {
      $("#CRUISE_BookingForm").data('changed', true);
    });

    $("#searchShipName").autocomplete( "option", "appendTo", "#searchShipForm" );

  }

  if (tabName == "TRANSPORTS") {
    $("#transferDate").datepicker({
      minDate: new Date(tripStartDate),
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 2,
      onClose: function (selectedDate) {
        $("#transferEndDate").datepicker("option", "minDate", selectedDate);
      }
    });

    $("#transferEndDate").datepicker({
      minDate: new Date(tripStartDate),
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 2
    });

    $('#transferTime').timepicker();
    $('#transferEndTime').timepicker();
    $('#transferTime').on('click', function (e) {
      $('#transferTime').timepicker('showWidget');
    });

    $('#transferEndTime').on('click', function (e) {
      $('#transferEndTime').timepicker('showWidget');
    });
    poiAutocompleteRegister('transferProvider', 'transferProviderId', cmpyId, 4, false, bkOnPoiClear);
    $('#TRANSPORT_BookingForm #photosBtn').unbind('click').click(function(){
      bkPhotosBtnAction('TRANSPORT_BookingForm', 'transferProviderId', reloadPhotos);
    });

    $("#TRANSPORT_BookingForm :input").change(function() {
      $("#TRANSPORT_BookingForm").data('changed', true);
    });
  }

  if (tabName == "ACTIVITIES") {
    $("#activityEndDate").datepicker({
      minDate: new Date(tripStartDate),
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 2
    });

    $("#activityStartDate").datepicker({
      minDate: new Date(tripStartDate),
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 2,
      onClose: function (selectedDate) {
        $("#activityEndDate").datepicker("option", "minDate", selectedDate);
      }
    });

    $('#activityStartTime').timepicker();
    $('#activityEndTime').timepicker();
    $('#activityStartTime').on('click', function (e) {
      $('#activityStartTime').timepicker('showWidget');
    });

    $('#activityEndTime').on('click', function (e) {
      $('#activityEndTime').timepicker('showWidget');
    });
    poiAutocompleteRegister('activityProviderName', 'activityProviderId', cmpyId, 5, false, bkOnPoiClear);

    $('#activityDestination').autocomplete({
      source: "/guide/destinationListAutoComplete?type=5&cmpyId=" + cmpyId,
      minLength: 5,
      delay: 500,
      select: function (event, ui) {
        $("#activityDestination").val(ui.item.label);
        $("#activityDestinationId").val(ui.item.id);
        return false;
      },
      response: function (event, ui) {
        $("#activityDestinationId").val("");
        if (ui.content.length == 1) {
          $("#activityDestination").val(ui.content[ 0 ].label);
          $("#activityDestinationId").val(ui.content[ 0 ].id);
          $(this).autocomplete("close");
        }
      }
    });
    $('#ACTIVITY_BookingForm #photosBtn').unbind('click').click(function(){
      bkPhotosBtnAction('ACTIVITY_BookingForm', 'activityProviderId', reloadPhotos);
    });
    $("#ACTIVITY_BookingForm :input").change(function() {
      $("#ACTIVITY_BookingForm").data('changed', true);
    });
  }


  $('#flightAirlineId, #hotelProviderId, #cruiseProviderId, #transferProviderId, #activityProviderId').change(function(){
    var curr = $(this);
    //console.log("ID Changed to" + $(this).val());
    if (curr.val().length == 0) {
      return;
    }

    var form = curr.parents('form');
    bkCheckIfTherePhotos(form.attr('id'), curr.attr('id'), function() {
      form.find('#photosBtn').removeClass('hide');
    });
  });


  //Initialization for Editable POI linked elements
  //Format: inputFiledId: { poiInputId, cmpyIntId, parentFormId, poiEditAreaId}
  //Format: 'inputFiledId': { poiId: 'poiInputId', cmpyId:cmpyIdInt, bkFormId:'parentFormId', editorId:'poiEditAreaId', poiTypeId:poiTypeId},
  var poiInputs = [
    {poiName: 'hotelName',            poiId: 'hotelProviderId',     cmpyId:cmpyIdInt, bkFormId:'HOTEL_BookingForm',     editorId:'HOTELS_poiEditRegion',    poiTypeId: 3, bkId: 'hotel-booking',    bkTitleId: 'bkAreaTitleHotel',      tabId:'bookingTabs'},
    {poiName: 'cruiseName',           poiId: 'cruiseProviderId',    cmpyId:cmpyIdInt, bkFormId:'CRUISE_BookingForm',    editorId:'CRUISES_poiEditRegion',   poiTypeId: 6, bkId: 'cruise-booking',   bkTitleId: 'bkAreaTitleCruise',     tabId:'bookingTabs'},
    {poiName: 'cruisePortDepart',     poiId: 'cruisePortDepartId',  cmpyId:cmpyIdInt, bkFormId:'CRUISE_BookingForm',    editorId:'CRUISES_poiEditRegion',   poiTypeId: 7, bkId: 'cruise-booking',   bkTitleId: 'bkAreaTitleCruise',     tabId:'bookingTabs'},
    {poiName: 'cruisePortArrive',     poiId: 'cruisePortArriveId',  cmpyId:cmpyIdInt, bkFormId:'CRUISE_BookingForm',    editorId:'CRUISES_poiEditRegion',   poiTypeId: 7, bkId: 'cruise-booking',   bkTitleId: 'bkAreaTitle',           tabId:'bookingTabs'},
    {poiName: 'activityProviderName', poiId: 'activityProviderId',  cmpyId:cmpyIdInt, bkFormId:'ACTIVITY_BookingForm',  editorId:'ACTIVITIES_poiEditRegion',poiTypeId: 5, bkId: 'tour-booking',     bkTitleId: 'bkAreaTitleActivities', tabId:'bookingTabs'},
    {poiName: 'transferProvider',     poiId: 'transferProviderId',  cmpyId:cmpyIdInt, bkFormId:'TRANSPORT_BookingForm', editorId:'TRANSPORTS_poiEditRegion',poiTypeId: 4, bkId: 'transfer-booking', bkTitleId: 'bkAreaTitleTransport',  tabId:'bookingTabs'}
  ];

  $.each(poiInputs, function(k,v){
    (function(){
      $('#' + v.poiName).addEdit({
        showOnLoad: true,
        onEdit : function() {
          var editorUI = Object.create(PoiEditorUI);
          currEditorUI = editorUI; //TODO: Temporary hack to access via Global variable
          editorUI.init(v.editorId, true);
          var prevTitleText = $('#' + v.bkTitleId).text();
          var poiId = $('#' + v.poiId).val();

          var preventSwitch = function() {
            alert('Please close the Vendor editor by clicking "To Booking" before you can select another booking tab.');
            return false;
          };

          if(poiId.length > 0) {
            editorUI.setPoi(poiId, v.cmpyId);
            $('#' + v.bkTitleId).text('Edit Vendor');
          }
          else {
            editorUI.newPoi();
            editorUI.editor.name.val($('#' + v.poiName).val());
            editorUI.editor.setType(v.poiTypeId);
            $('#' + v.bkTitleId).text('Create New Vendor');
          }
          editorUI.onClose = function() {
            reloadPhotos = true;
            poiInputFieldSetValues(v.poiName, v.poiId, editorUI.editor.name.val(), editorUI.editor.poiId.val());
            $('#' + v.editorId).slideUp();
            $('#' + v.bkFormId).slideDown();
            $('#' + v.bkId).slideDown();
            $('#' + v.bkTitleId).text(prevTitleText);
            $('#' + v.tabId).unbind('show', preventSwitch);
          };

          $('#' + v.tabId).unbind('show', preventSwitch).bind('show', preventSwitch);
          $('#' + v.editorId).slideDown();
          $('#' + v.bkFormId).slideUp();
          $('#' + v.bkId).slideUp();

        }
      });
    })();
  });

};

var bkOnPoiClear = function(nameSelector, poiSelector) {
  //console.log("bkOnPoiClear(nameSelector:" + nameSelector + ")");
  //Hiding photos button
  $('#' + nameSelector).parents('form').find('#photosBtn, #virtuosoBenefitsBtnArea').addClass('hide');

};

var bkHotelPoi = null;
var bkAmenitiesBenefits = "";

var bkGetPoiAndCheckBenefits = function(poiId, cmpyId) {
  poiStorageDeleteById(poiId);
  poiStorageGet(poiId, cmpyId, function(poi){
    bkHotelPoi = poi;
    $('#virtuosoBenefitsBtnArea').addClass('hide');
    if (poi.features.length > 0) {
      bkAmenitiesBenefits = "";
      for (var idx = 0; idx < poi.features.length; idx++) {
        var f = poi.features[idx];
        if ($.inArray('Virtuoso Hotels',f.tags) == 0) {
          if(bkAmenitiesBenefits.length == 0) {
            bkAmenitiesBenefits = "Virtuoso Amenities\n";
          }
          $('#amenitiesBtnArea > label').text("Virtuoso Preferred Hotel");
          $('#amenitiesPasteBtn > span').text("Add Virtuoso Amenities");
          if (f.name && f.name.length > 0) {
            bkAmenitiesBenefits += " - " + f.name + "\n";
          }
          if (f.desc && f.desc.length > 0) {
            var decoded = f.desc.replace(/&amp;/g, '&');
            bkAmenitiesBenefits += "    " + decoded + "\n";
          }
          $('#amenitiesBtnArea').removeClass('hide');
        }
        if ($.inArray('Ensemble Hotel',f.tags) == 0) {
          if(bkAmenitiesBenefits.length == 0) {
            bkAmenitiesBenefits = "Ensemble Amenities\n";
          }
          $('#amenitiesBtnArea > label').text("Ensemble Preferred Hotel");
          $('#amenitiesPasteBtn > span').text("Add Ensemble Amenities");
          if (f.desc && f.desc.length > 0) {
            bkAmenitiesBenefits += "   " + f.desc + "\n";
          }
          $('#amenitiesBtnArea').removeClass('hide');
        }
        if ($.inArray('TravelLeaders Hotel',f.tags) == 0) {
          if(bkAmenitiesBenefits.length == 0) {
            bkAmenitiesBenefits = "Travel Leaders Amenities\n";
          }
          $('#amenitiesBtnArea > label').text("Travel Leaders Preferred Hotel");
          $('#amenitiesPasteBtn > span').text("Add Travel Leaders Amenities");
          if (f.desc && f.desc.length > 0) {
            bkAmenitiesBenefits += f.desc + "\n";
          }
          $('#amenitiesBtnArea').removeClass('hide');
        }
          if ($.inArray('Indagare Hotel',f.tags) == 0) {
              if(bkAmenitiesBenefits.length == 0) {
                  bkAmenitiesBenefits = "Indagare Amenities:\n";
              }
              $('#amenitiesBtnArea > label').text("Indagare Hotel Collection");
              $('#amenitiesPasteBtn > span').text("Add Indagare Amenities");
              if (f.desc && f.desc.length > 0) {
                  bkAmenitiesBenefits += f.desc + "\n";
              }
              $('#amenitiesBtnArea').removeClass('hide');
          }
      }
    }
  });
};

var bkConfirmCopyBooking = function (detailId, tabName, detailName, bookingNum, startDate, startTime, endDate, endTime, comments) {

  if (detailId != null) {
    $("#detailId2").val(detailId);
    $("#bookingStartDate").val(startDate);
    $("#bookingNum").val(bookingNum);
    if(startTime != null && startTime.trim().length > 0){
        $("#bookingStartTime").val(startTime);
    } else {
        $("#bookingStartTime").val('12:00 AM');
    }
    $("#bookingEndDate").val(endDate);
    if(endTime != null && endTime.trim().length > 0){
        $("#bookingEndTime").val(endTime);
    } else {
        $("#bookingEndTime").val('12:00 AM');
    }
    $("#bookingNote").text(comments);
    $("#copyBookingModalMsg").text("Copy " + detailName + "?");
    if (tabName == "FLIGHTS") {
        $("#copyBookingModal #startDateLabel").text("Departure Date");
        $("#copyBookingModal #startTimeLabel").text("Departure Time");
        $("#copyBookingModal #endDateLabel").text("Arrival Date");
        $("#copyBookingModal #endTimeLabel").text("Arrival Time");
    } else if (tabName == "HOTELS"){
        $("#copyBookingModal #startDateLabel").text("Check-in Date");
        $("#copyBookingModal #startTimeLabel").text("Check-in Time");
        $("#copyBookingModal #endDateLabel").text("Check-out Date");
        $("#copyBookingModal #endTimeLabel").text("Check-out Time");
    }
    $("#copyBookingModal").modal('show');
  }

  $("#bookingStartDate").datepicker({
      defaultDate: new Date(startDate),
      changeMonth: true,
      numberOfMonths: 2,

  });

  $("#bookingEndDate").datepicker({
      defaultDate: new Date(endDate),
      changeMonth: true,
      numberOfMonths: 2,

  });

  $('#bookingStartTime').timepicker({defaultTime: 'value'});

  $('#bookingEndTime').timepicker({defaultTime: 'value'});

  $('#bookingStartTime').on('click', function (e) {
    $('#bookingStartTime').timepicker('showWidget');
  });
  $('#bookingEndTime').on('click', function (e) {
    $('#bookingEndTime').timepicker('showWidget');
  });

};

var bkConfirmDelete = function (detailId, detailName) {
  if (detailId != null) {
    $("#detailId").val(detailId);
    $("#deleteModalMsg").text("Delete " + detailName + "?");
    $("#deleteModal").modal('show');
  }
};

var bkConfirmExit = function (url) {

  if ($('.BookingForm').data ('changed') ) {
    if ( confirm ( "Please click Save or your changes will be lost! Discard Changes?" ) ) {
      getPage ( url ) ;
    }
  } else {
    getPage ( url ) ;
  }

}

var bkBuildPhotoItem = function(bookingFormName, poiId, fileId, fileUrl, desc){
  var table = $('<table>', {
    id : "poiPhotoTable_" + fileId
  }).addClass('table');
  var rowPhoto = $('<tr>');

  $('<img>',{
    class:"img-rounded span12",
    src:fileUrl,
    height:"120px"
  }).appendTo($('<td>').appendTo(rowPhoto));

  var rowDesc  = $('<tr><td>').text(desc);
  //Nothing to add here for now

  var rowRemove= $('<tr>');
  $('<a>', {
    href: "#poiPhotoDeleteModal",
    'data-toggle':'modal',
    class:"btn btn-danger btn-small"
  })
      .click(function(){
        var delSubmit = $('#poiPhotoDeleteSubmit');
        delSubmit.click(function(){
          table.parent().remove();
          bkPhotosGenInputs(bookingFormName);
          $('#poiPhotoDeleteModal').modal('hide');
        });
      })
      .append($('<div>',{
        class:""
      }).append('&nbsp;Hide'))
      .appendTo($('<td>',{
        style:"text-align:center;",
        class:"pull-right"
      }).appendTo(rowRemove));


  table.append(rowPhoto)
      .append(rowDesc)
      .append(rowRemove);

  return table;
};

var bkCheckIfTherePhotos = function(bookingFormName, bookingPoiId, cb) {
  var detailsId = $('#' + bookingFormName + " #inTripDetailId").val();
  var poiId = $('#' + bookingPoiId).val();

  var request = new ApiMessage();
  request['@type']  = 'BKFILE';
  request.header = new ApiRequestHeader();
  request.body = new BookingFileBody('LIST');
  request.body.poi_id = poiId;
  request.body.detailsId = detailsId;

  ExecuteCommand(request, "/trips/bookings/photos/execute.json", function(data) {
    if (data.body.files && data.body.files.length > 0) {
      if (cb) {
        cb();
      }
    }
  });
};

var bkPhotosGenInputs = function(formId) {
  var form = $('#' + formId);
  $('#' + formId + ' input[name*=inPhotosReset]').remove();
  $('#' + formId + ' input[name*=inDetailPhotos]').remove();
  $.each($('#photoMgrModalItems').sortable('toArray'), function(k, v){
    $('<input>', {
      type : "hidden",
      name : "inDetailPhotos[" + k + "]",
      value : v
    }).appendTo(form);
  });
};

var bkPhotosBtnAction = function(bookingFormName, bookingPoiId, ignoreBooking) {
  //console.log("bkPhotosBtnAction: form =" + bookingFormName + " for=" + bookingPoiId);
  var detailsId = $('#' + bookingFormName + " #inTripDetailId").val();
  var poiId = $('#' + bookingPoiId).val();

  if (ignoreBooking == undefined || ignoreBooking == false) {
    $('#photoMgrModalReset').unbind('click').click(function () {
      bkPhotosBtnAction(bookingFormName, bookingPoiId, true);
      $('#' + bookingFormName + ' input[name*=inPhotosReset]').remove();
      var form = $('#' + bookingFormName);
      $('<input>', {
        type : "hidden",
        name : "inPhotosReset",
        value : true,
      }).appendTo(form);
    });
  } else {
    detailsId = null;
  }

  if (poiId.trim().length == 0) {
    $('#photoMgrModal').modal('show');
    return;
  }

  var existingId = $('#photoMgrModal #photoMgrPoi').val();
  if (poiId == existingId && ignoreBooking != true) {
    $('#photoMgrModal').modal('show');
    //Do nothing if still the same id
    return;
  }

  var request = new ApiMessage();
  request['@type']  = 'BKFILE';
  request.header = new ApiRequestHeader();
  request.body = new BookingFileBody('LIST');
  request.body.poi_id = poiId;
  request.body.detailsId = detailsId;

  var photoItems = $('#photoMgrModalItems');
  photoItems.children().remove();
  if (photoItems.hasClass('ui-sortable')) {
    photoItems.sortable("enable");
  }

  $('#photoMgrModal').modal('show');
  $('#photoMgrModalSpinner').show();

  ExecuteCommand(request, "/trips/bookings/photos/execute.json", function(data){

    $.each(data.body.files, function(k, v){
      var li = $('<li>', {
        class : "umSortItem",
        id : v.file_id
      });
      li.append(bkBuildPhotoItem(bookingFormName, poiId, v.file_id, v.url, v.desc));
      photoItems.append(li);
    });

    //Adding a hidden input field to the modal to indicate which poiid has its photos
    $('#photoMgrModal #photoMgrPoi').val(data.body.poi_id);

    photoItems.sortable({
      update : function(event, ui) {
        bkPhotosGenInputs(bookingFormName);
      }
    });

    $('#photoMgrToggle').bootstrapSwitch('destroy');
    $('#photoMgrToggle').bootstrapSwitch({
      onColor: "success",
      offColor: "danger",
      state: data.body.showPhotos,
      onSwitchChange : function(event, state) {
        //console.log("Photo state changed to :" + state);
        var form = $('#' + bookingFormName);
        $('#' + bookingFormName + ' input[name*=inPhotosToggle]').remove();

        $('<input>', {
          type : "hidden",
          name : "inPhotosToggle",
          value : state,
        }).appendTo(form);

      }
    });

    $('#photoMgrModalSpinner').hide();
  });

};

/**
 * jQuery plugin based on AddClear plugin to allow edit button
 */
(function($, window, document, undefined) {

  // Create the defaults once
  var pluginName = "addEdit",
      defaults = {
        closeSymbol: '<i class="fa fa-pencil-square-o"></i>',
        color: "#00b4ff",
        top: 2,
        right: -32,
        returnFocus: true,
        showOnLoad: false,
        onEdit: null,
        hideOnBlur: false,
        fontSize: "28px"
      };

  // The actual plugin constructor
  function Plugin(element, options) {
    this.element = element;
    this.options = $.extend({}, defaults, options);
    this._defaults = defaults;
    this._name = pluginName;
    this.init();
  }

  Plugin.prototype = {

    init: function() {
      var $this = $(this.element),
          me = this,
          options = this.options;
      var editHash = '#edit_' + $this.attr('id');
      $this.wrap("<div style='position:relative;'></div>");
      $this.after($("<a href='" + editHash + "' style='display: none;'>" + options.closeSymbol + "</a>"));
      $this.next().css({
        color: options.color,
        'text-decoration': 'none',
        display: 'none',
        'line-height': 1,
        overflow: 'hidden',
        position: 'absolute',
        right: options.right,
        top: options.top,
        'font-size': options.fontSize

      }, this);

      if ($this.val().length >= 1 && options.showOnLoad === true) {
        $this.siblings("a[href='" + editHash + "']").show();
      }

      $this.focus(function() {
        if ($(this).val().length >= 1) {
          $(this).siblings("a[href='" + editHash + "']").show();
        }
      });

      $this.blur(function() {
        var self = this;

        if (options.hideOnBlur) {
          setTimeout(function() {
            $(self).siblings("a[href='" + editHash + "']").hide();
          }, 50);
        }
      });

      $this.keyup(function() {
        if ($(this).val().length >= 1) {
          $(this).siblings("a[href='" + editHash + "']").show();
        } else {
          $(this).siblings("a[href='" + editHash + "']").hide();
        }
      });

      $("a[href='" + editHash + "']").click(function(e) {
        //$(this).siblings(me.element).val("");
        //$(this).hide();
        if (options.returnFocus === true) {
          $(this).siblings(me.element).focus();
        }
        if (options.onEdit) {
          options.onEdit($(this).siblings("input"));
        }
        e.preventDefault();
      });
    }

  };

  $.fn[pluginName] = function(options) {
    return this.each(function() {
      if (!$.data(this, "plugin_" + pluginName)) {
        $.data(this, "plugin_" + pluginName,
            new Plugin(this, options));
      }
    });
  };

})(jQuery, window, document);


