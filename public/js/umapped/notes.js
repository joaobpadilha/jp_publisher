/**************************************************************
 *************** Functions for Table Notes ********************
 **************************************************************/

//Removes the third column containing the Edit and Delete buttons from the table.
function removeCell() {
    $("#hiddenNoteTable table tr:eq(0) th").each(function(i){
    if($(this).attr("id") !== "col3") return;
    $("#hiddenNoteTable table tr > th:nth-child("+(i+1)+")").remove();
    $("#hiddenNoteTable table tr > td:nth-child("+(i+1)+")").remove();
    return false;
    });
}

//Adds the third column containing the Edit and Delete buttons to the table.
function addCell() {
    $("#tripNoteTable table tr:eq(0) th").each(function(i){
    if($(this).attr("id") !== "col2") return;
    if($(this).is(':last-child')) {
    var head = "<th id='col3' style='width: 15%'></th>";
    var cell = "<td id='col3' style='vertical-align: top; padding: 10px 20px;'>"
    + "<div style='vertical-align: top; padding-bottom: 10px;margin-right:0px; display: inline-block;'>"
    + "<a role='button' onclick='javascript:deleteTableRow($(this).closest(&quot;.tableRow&quot;));'>"
    + "<i class='fa fa-trash'  style='font-size: 20px; color: #666666'></i></a>"
    + "<a role='button' onclick='javascript:moveRowUp($(this).closest(&quot;.tableRow&quot;));'>"
    + "<i class='fa fa-chevron-up'  style='font-size: 20px; color: #666666; padding-left: 20px'></i></a>"
    + "<a role='button' onclick='javascript:moveRowDown($(this).closest(&quot;.tableRow&quot;));'>"
    + "<i class='fa fa-chevron-down'  style='font-size: 20px; color: #666666; padding-left: 10px'></i></a></div></td></tr>";
    $("#tripNoteTable table tr > th:nth-child("+(i+1)+")").after(head);
    $("#tripNoteTable table tr > td:nth-child("+(i+1)+")").after(cell);
    return false;
    }
    });
}


function addTableRow() {
       var newTableRow = "<tr class='tableRow'>"
       + "<td id='col1' style='vertical-align: top; padding: 10px 20px; border: 1px solid #AFAFAF;'>"
       + "</td><td id='col2' style='vertical-align: top; padding: 10px 20px; border: 1px solid #AFAFAF;'>"
       + "</td><td id='col3' style='vertical-align: top; padding: 10px 20px;'>"
       + "<div style='vertical-align: top; padding-bottom: 10px;margin-right:0px; display: inline-block;'>"
       + "<a role='button' onclick='javascript:deleteTableRow($(this).closest(&quot;.tableRow&quot;));'>"
       + "<i class='fa fa-trash'  style='font-size: 20px; color: #666666'></i></a>"
       + "<a role='button' onclick='javascript:moveRowUp($(this).closest(&quot;.tableRow&quot;));'>"
       + "<i class='fa fa-chevron-up'  style='font-size: 20px; color: #666666; padding-left: 20px'></i></a>"
       + "<a role='button' onclick='javascript:moveRowDown($(this).closest(&quot;.tableRow&quot;));'>"
       + "<i class='fa fa-chevron-down'  style='font-size: 20px; color: #666666; padding-left: 10px'></i></a></div></td></tr>";
       $("table #tableNoteBody").append(newTableRow);

       $("tbody#tableNoteBody td#col1, tbody#tableNoteBody td#col2").click(function(){
           editCell($(this));
       });

    updateIntro();
    return false;
}

function deleteTableRow(tableRow) {
    tableRow.remove();
    updateIntro();
    return false;
}

function moveRowUp(tableRow) {
    var row = tableRow;
    row.insertBefore(row.prev());
    updateIntro();
    return false;
}

function moveRowDown(tableRow) {
    var row = tableRow;
    row.insertAfter(row.next());
    updateIntro();
    return false;
}


//Helper function used to clone and alter the table in order to create Note Intro
function updateIntro() {
    var origNoteTable = $("#tripNoteTable table").clone();
    $("#hiddenNoteTable").html(origNoteTable);
    removeCell();
    var tableContents = $("#hiddenNoteTable").html();
    $("#inTripNoteIntro").val(tableContents);
    return false;
}

function resetTable() {
    var tableTemplate = "<table id='tableNoteTable' class='insurance' style='width: 750px; margin-top:10px'>"
                                    + "<thead style='margin:auto; width=100%; margin-left:2px;padding-right:4px;'>"
                                    +   "<tr><th id='col1' style='width: 40%'></th>"
                                    +       "<th id='col2' style='width: 45%'></th>"
                                    +       "<th id='col3' style='width: 15%'></th></tr>"
                                    + "</thead><tbody id='tableNoteBody'></tbody></table>";

    $("#tripNoteTable").html(tableTemplate);
    updateIntro();
    return false;
}

function editCell(cell) {
    var cellIndex = cell.index();
    var rowIndex = cell.closest(".tableRow").index();
    var cellContent = cell.html();

    $('#editCellModal').modal('show');
    if(typeof(CKEDITOR.instances.cellEdit) !== "object"){
      CKEDITOR.replace('cellEdit', {
              removeButtons: 'Table,Maximize',
              width: '100%',
              height: 100,
              startupFocus : true
          });
    }
    var editor = CKEDITOR.instances['cellEdit'];
    editor.setData(cellContent);
    editor.focus();

    var btn = '<button type="submit" class="btn btn-success " id="editCellSubmit" onclick="updateCell('+ rowIndex +', ' + cellIndex +')">Update</button>';
    $("button#editCellSubmit").replaceWith(btn);

    return false;
}

function updateCell(row, col) {
    var oldCell = $('#tripNoteTable tbody#tableNoteBody tr:eq(' + row + ') td:eq(' + col + ')');
    var cellContent = CKEDITOR.instances['cellEdit'].getData();
    oldCell.html(cellContent);
    $('#editCellModal').modal('hide');

    updateIntro();
    return false;

}
