/**
 * Created by surge on 2015-03-27.
 */
var BillingPlanView = {
  init: function(planUrl, addonUrl) {
    this.id             = $('#planId');
    this.name           = $('#planDescription');
    this.schedule       = $('#planCurrency');
    this.cost           = $('#planCost');
    this.expiration     = $('#planExpire');
    this.enabled        = $('#planEnabled');
    this.newUserState   = $('#planNewUserState');
    this.btnsCRU        = $('#planUpdate, #planCreate');
    this.table          = $('#plansTable');
    this.addonsTable    = $('#addonsTable');
    this.btnSync        = $('#btnFreshbookSync');
    this.stateAreaName  = 'billingStateArea';
    this.visible        = $('#planVisible');
    this.consortium_id  = $('#consortium');
    this.features       = $('#planFeatures');
    this.pushToCons     = $('#pushToConsortium');
    this.addAddon       = $('#planAddAddon');
    this.billingType    = $('#planBillingType');

    var view = this;
    //Enable Bootstrap Switch buttons
    this.enabled.bootstrapSwitch('destroy');
    this.enabled.bootstrapSwitch({
      onColor: "success",
      offColor: "danger"
    });

    this.visible.bootstrapSwitch('destroy');
    this.visible.bootstrapSwitch({
      onColor: "success",
      offColor: "danger"
    });

    this.newUserState.bootstrapSwitch('destroy');
    this.newUserState.bootstrapSwitch({
                                   onColor: "success",
                                   offColor: "danger"
                                 });


    //Enable date picker
    this.expiration.datepicker({
      changeMonth: true,
      numberOfMonths: 1,
      dateFormat: "yy-mm-dd",
      changeYear: true
    });

    this.btnsCRU.unbind('click').click(function() {
      postForm(planUrl, 'formPlanCreate');
      return false;
    });

    this.addAddon.unbind('click').click(function(){
      getPage(addonUrl);
      return false;
    });

    this.table.DataTable({
      "paging": true,
      "lengthMenu": [25, 50, 100],
      "ordering": true,
      "order" : [[0, 'asc']]
    });

    this.addonsTable.DataTable({
                           "paging": false,
                           "ordering": false
                         });

    this.btnSync.click(function(){
      var requestUrl = '/billing/plans/sync';
      postJsonRequestUrl(requestUrl, function(data){
        billingCheckStatus(data.statusUrl, view.stateAreaName);
      });
    });


    this.pushToCons.unbind('click').click(function() {
      var requestUrl = '/billing/plans/pushAddon';
      postForm(requestUrl, 'formPlanCreate');
      return false;
    });

    // Docs at: http://www.virtuosoft.eu/code/bootstrap-duallistbox/
    this.features.bootstrapDualListbox(
        {
          bootstrap2Compatible: true,
          nonSelectedListLabel: 'Available Features',
          selectedListLabel: 'Active Features',
          moveOnSelect: false,
          preserveSelectionOnMove: 'moved'
        });
  }

};

var initBillingPlan = function(planUrl, addonUrl) {
  var plan = Object.create(BillingPlanView);
  plan.init(planUrl, addonUrl);
};

var BillingUserView = {
  init: function(){
    this.userId       = $('#inUserId');
    this.cmpyIdNU     = $('#userForm > #inCmpyId');
    this.billedTo     = $('#billingBilledTo');
    this.billAgent    = $('#billingBilledToAgent');
    this.billCompany  = $('#billingBilledToCompany');
    this.billUser     = $('#billingBilledToUser');
    this.schedule     = $('#billingSchedule');
    this.plan         = $('#billingPlan');
    this.pmntMethod   = $('#billingPmntMethod');
    this.startDate    = $('#billingStartDate');
    this.cutoffDate   = $('#serviceCutoffDate');
    this.cutoffArea   = $('#cutoffDateArea');
    this.dates        = $('#billingStartDate, #serviceCutoffDate');
    this.recurrent    = $('#billingRecurrent');
    this.cmpyName     = $('#billingCmpyName');
    this.cmpyId       = $('#billingCmpyId');
    this.agentName    = $('#billingAgentName');
    this.agentId      = $('#billingAgentId');

    this.urlPlans     = '/billing/plans/sched/'; //TODO: Needs to be maintained
    this.urlACCmpies  = '/user/companies/autocomplete';
    this.urlACWorkers = '/user/coworkers/autocomplete';

    var userView = this;

    if(this.cmpyIdNU.val().length > 0) {
      this.urlACCmpies  += '?cmpyid=' + this.cmpyIdNU.val();
      this.urlACWorkers += '?cmpyid=' + this.cmpyIdNU.val();
    } else {
      this.urlACCmpies  += '?userId=' + this.userId.val();
      this.urlACWorkers += '?userId=' + this.userId.val();
    }

    //Enable date picker for billing items
    this.dates.datepicker({
      changeMonth: true,
      numberOfMonths: 1,
      dateFormat: "yy-mm-dd",
      changeYear: true
    });


    this.cmpyName.autocomplete({
      source: userView.urlACCmpies,
      minLength: 0,
      delay: 50,
      select: function (event, ui) {
        userView.cmpyName.val(ui.item.label);
        userView.cmpyId.val(ui.item.id);
        return false;
      }
    });

    this.agentName.autocomplete({
      source: userView.urlACWorkers,
      minLength: 0,
      delay: 50,
      select: function (event, ui) {
        userView.agentName.val(ui.item.label);
        userView.agentId.val(ui.item.id);
        return false;
      }
    });

    this.cutoffArea.addClear({
      right:80,
      top:8,
      showOnLoad: true,
      color: "#666666",
      forceShow: true,
      onClear: function(){
        userView.cutoffDate.val('');
        userView.cutoffArea.siblings('a').show();
      }
    });

    this.onBilledToChange(true);
    this.onScheduleChange();

    this.billedTo.on('change', function(event) {
      userView.onBilledToChange();
    });
    this.schedule.on('change', function(event){
      userView.onScheduleChange(event);
    });
  },

  onScheduleChange: function(event) {
    var schedType = this.schedule.val();
    switch(schedType) {
      case "COMPANY":
      case "AGENT":
        this.plan.parent().hide();
        this.pmntMethod.parent().hide();
        switch(this.billedTo.val()) {
          case 'AGENT':
          case 'COMPANY':
              break;
          default:
            alert("When user's schedule is set to COMPANY or AGENT. Billed to value should be the same as schedule.");
            break;
        }
        break;
      default:
        this.plan.parent().show();
        var billedTo = this.billedTo.val();
        switch(billedTo) {
          case "COMPANY":
          case "AGENT":
            this.pmntMethod.parent().hide();
            break;
          default:
            this.pmntMethod.parent().show();
            break;
        }
        break;
    }

    var url = this.urlPlans + schedType;
    //console.log("Requesting from: " + url);
    var userView = this;
    postJsonRequestUrl(url, function(data){
      var currPlanId = userView.plan.val();
      userView.plan.children().remove();
      $.each(data, function(k,v){
        var sel = $('<option>',{
          value: k,
          text: v
        });
        if (k == currPlanId) {
          sel.attr("selected", true);
        }
        userView.plan.append(sel);
      });
    });
  },

  onBilledToChange: function(keepSchedule) {
    var area = this.billedTo.val();
    //console.log("User will be billed as: " + area);
    switch(area) {
      case "COMPANY":
        this.billCompany.show();
        this.billUser.hide();
        this.billAgent.hide();
        this.pmntMethod.parent().hide(); //Not Applicable
        this.agentId.val("");
        this.agentName.val("");
        this.cmpyName.addClass("required");
        this.agentName.removeClass("required");
        if (keepSchedule != true) {
          //console.log("Schedule goes to COMPANY");
          this.schedule.val('COMPANY');
        }
        this.schedule.trigger('change');
        break;
      case "USER":
        this.billCompany.hide();
        this.billUser.show();
        this.billAgent.hide();
        this.pmntMethod.parent().show(); //Applicable
        this.cmpyName.removeClass("required");
        this.agentName.removeClass("required");

        if (keepSchedule != true) {
          this.schedule.val('MONTHLY');
          this.schedule.trigger('change');
        }

        switch(this.schedule.val()) {
          case 'AGENT':
          case 'COMPANY':
            alert("When user is billed directly, account can't be set to AGENT or COMPANY schedule. User must have  correct plan.");
            break;
        }
        break;
      case "AGENT":
        this.billCompany.hide();
        this.billUser.hide();
        this.billAgent.show();
        this.pmntMethod.parent().hide(); //Not Applicable
        this.cmpyId.val("");
        this.cmpyName.val("");
        this.cmpyName.removeClass("required");
        this.agentName.addClass("required");
        if(keepSchedule != true) {
          //console.log("Schedule goes to AGENT");
          this.schedule.val('AGENT');
          this.schedule.trigger('change');
        }
        break;
    }
  }
};

var initBillingUser = function(){
  var buv = Object.create(BillingUserView);
  buv.init();

  //Enabling addons area

  var submitAddonChange = function() {
    var addon = $(this).parents('tr[data-addonid]'),
        startDate = addon.find('[name="addonStartDate"]'),
        cutoffDate = addon.find('[name="addonCutoffDate"]');

    var config = {
      addonId: addon.data('addonid'),
      userId: addon.data('user'),
      startDate: startDate.val(),
      cutoffDate: cutoffDate.val(),
      enabled:  addon.find('input[name="addonEnabled"]').bootstrapSwitch('state')
    };

    console.log(JSON.stringify(config));
    $.ajax(
        {
          type: "POST",
          url: '/user/addons/edit',
          data: JSON.stringify(config),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        })
     .always(
         function (data, textStatus, jqXHR) {
           if (textStatus == 'success') {
             reloadPage();
             headLineAlertDisplay("Successfully updated user: " + config.userId);
           }
           else if (textStatus == 'parsererror') {
             //Need to go to login page
             window.location.href = "/";
           }
           else if (textStatus != 'abort') {
             switch (jqXHR.status) {
               case 501:
                 headLineAlertDisplay("Feature not yet implemented.");
               case 503:
               case 504:
                 headLineAlertDisplay("Request timeout. Please try again in a couple of minutes.");
                 break;
               default:
                 headLineAlertDisplay("Unknown System Error has occurred - " + textStatus);
             }
           }
         });
    console.log("Updated requested...");
  };

  var addonDates = $('input[name="addonStartDate"], input[name="addonCutoffDate"]');
  addonDates.datepicker({
                          changeMonth: true,
                          numberOfMonths: 1,
                          dateFormat: "yy-mm-dd",
                          changeYear: true
                        });
  addonDates.on('change', submitAddonChange);


  var addonToggles = $('tr[data-addonid] input[type="checkbox"]');
  addonToggles.bootstrapSwitch('destroy');
  addonToggles.bootstrapSwitch({
                                 onColor: "success",
                                 offColor: "danger"
                               });

  addonToggles.on('switchChange.bootstrapSwitch', submitAddonChange);
};

var BillingCmpyView = {
  init: function(){
    this.schedule     = $('#billingSchedule');
    this.plan         = $('#billingPlan');
    this.pmntMethod   = $('#billingPmntMethod');
    this.startDate    = $('#billingStartDate');
    this.cutoffDate   = $('#serviceCutoffDate');
    this.cutoffArea   = $('#cutoffDateArea');
    this.recurrent    = $('#billingRecurrent');
    this.dates        = $('#billingStartDate, #serviceCutoffDate');
    this.urlPlans     = '/billing/plans/sched/'; //TODO: Needs to be maintained


    //Enable date picker for billing items
    this.dates.datepicker({
      changeMonth: true,
      numberOfMonths: 1,
      dateFormat: "yy-mm-dd",
      changeYear: true
    });

    this.onScheduleChange();

    var cmpyView = this;
    this.schedule.change(function(){
      cmpyView.onScheduleChange();
    });

    this.cutoffArea.addClear({
      right:70,
      top:8,
      showOnLoad: true,
      color: "#666666",
      forceShow: true,
      onClear: function(){
        cmpyView.cutoffDate.val('');
        cmpyView.cutoffArea.siblings('a').show();
      }
    });
  },

  onScheduleChange: function() {
    var schedType = this.schedule.val();
    var url = this.urlPlans + schedType;
    //console.log("Requesting from: " + url);
    var userView = this;
    postJsonRequestUrl(url, function(data){
      var currPlanId = userView.plan.val();
      userView.plan.children().remove();
      $.each(data, function(k,v){
        var sel = $('<option>',{
          value: k,
          text: v
        });
        if (k == currPlanId) {
          sel.attr("selected", true);
        }
        userView.plan.append(sel);
      });

    });
  }
};

var initBillingCmpy = function(){
  var bcv = Object.create(BillingCmpyView);
  bcv.init();
};

var BillingReportsUsers = {
  init: function(tblJsonUrl) {
    this.userType             = $('#billingUserType');
    this.userTable            = $('#userTable');
    this.tblJsonUrl           = tblJsonUrl;
    this.btnFreshbookSyncMod  = $('#btnFreshbookSyncMod');
    this.btnFreshbookSyncAll  = $('#btnFreshbookSyncAll');
    this.stateAreaName        = 'billingStateArea';

    var view = this;
    this.userType.change(function(){
      var name = $(this).val();
      view.tableInit(name);
    });

    this.btnFreshbookSyncMod.click(function(){
      var requestUrl = '/billing/users/sync/USERS_SYNC_MODIFIED';
      postJsonRequestUrl(requestUrl, function(data){
        billingCheckStatus(data.statusUrl, view.stateAreaName);
      });
    });

    this.btnFreshbookSyncAll.click(function(){
      var requestUrl = '/billing/users/sync/USERS_SYNC_ALL';
      postJsonRequestUrl(requestUrl, function(data){
        billingCheckStatus(data.statusUrl, view.stateAreaName);
      });
    });

  },

  tableInit: function(userType) {
    var view = this;
    if(isDataTable(this.userTable[0])){
      this.userTable.DataTable().destroy();
    }

    this.userTable.DataTable({
      "processing": true,
      "serverSide": true,
      "columnDefs": [
        { "data": "userId", "name": "userid", "targets" : 0, "searchable" : false, "orderable": true },
        { "data": "name",  "name": "firstname", "targets" : 1, "searchable" : false, "orderable": true },
        { "data": "schedule", "targets" : 2, "searchable" : false, "orderable": false },
        { "data": "plan",     "targets" : 3, "searchable" : false, "orderable": false },
        { "data": "billTo",   "targets" : 4, "searchable" : false, "orderable": false },
        { "data": "pmntType", "targets" : 5, "searchable" : false, "orderable": false },
        { "data": "isFresh",  "targets" : 6, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              if (data == true) {
                return '<span class="label label-success">Yes</span>';
              } else {
                return '<span class="label label-important">No</span>';
              }
            } else {
              return data;
            }
          }
        },
        { "data": "reports",  "targets" : 7, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = view.renderTableButton(data);
              return res;
            } else {
              return data;
            }
          }
        },
        { "data": "actions",  "targets" : 8, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = view.renderTableButton(data);
              return res;
            } else {
              return data;
            }
          }
        }
      ],
      "ajax": {
        url: this.tblJsonUrl + '?userType=' + userType,
        type: 'POST',
        contentType: "application/json",
        data: function (d) {
          return  JSON.stringify(d);
        }
      }
    });
  },

  renderTableButton: function(actions) {
    if (Object.keys(actions).length == 0) {
      return '';
    }

    if (Object.keys(actions).length == 1) {
      var title = Object.keys(actions)[0];
      btn = '<a role="button" class="btn btn-info input-block-level" onclick="getPage(\''+ actions[title] + '\');return false;">' + title + '</a>' ;
      return btn;
    }

    var btn  =  '<div class="btn-group">' +
                '<button class="btn btn-info" data-toggle="dropdown">Manage</button>' +
                '<button class="btn btn-info dropdown-toggle" data-toggle="dropdown">' +
                '   <span class="caret"></span>' +
                '</button>' +
                '<ul class="dropdown-menu">';

    $.each(actions, function(k,v){
      btn += '<li>';
      btn += '<a onclick="getPage(\''+ v + '\');return false;">';
      btn += k;
      btn += '</a></li>';
    });

    btn += '</ul>';
    btn += '</div>';
    return btn;
  }

};

var BillingReportsCmpy = {
  init: function(tblJsonUrl) {
    this.schedule     = $('#billingSchedule');
    this.cmpyTable    = $('#cmpyTable');
    this.tblJsonUrl   = tblJsonUrl;
    this.btnFreshbookSyncMod  = $('#btnFreshbookSyncMod');
    this.btnFreshbookSyncAll  = $('#btnFreshbookSyncAll');
    this.stateAreaName        = 'billingStateArea';

    var view = this;
    this.schedule.change(function(){
      var name = $(this).val();
      view.tableInit(name);
    });

    this.btnFreshbookSyncMod.click(function(){
      var requestUrl = '/billing/users/sync/CMPYS_SYNC_MODIFIED';
      postJsonRequestUrl(requestUrl, function(data){
        billingCheckStatus(data.statusUrl, view.stateAreaName);
      });
    });

    this.btnFreshbookSyncAll.click(function(){
      var requestUrl = '/billing/users/sync/CMPYS_SYNC_ALL';
      postJsonRequestUrl(requestUrl, function(data){
        billingCheckStatus(data.statusUrl, view.stateAreaName);
      });
    });
  },

  tableInit: function(schedule) {
    var view = this;
    if(isDataTable(this.cmpyTable[0])){
      this.cmpyTable.DataTable().destroy();
    }

    this.cmpyTable.DataTable({
      "processing": true,
      "serverSide": true,
      "columnDefs": [
        { "data": "name",     "targets" : 0, "searchable" : false, "orderable": false },
        { "data": "schedule", "targets" : 1, "searchable" : false, "orderable": false },
        { "data": "plan",     "targets" : 2, "searchable" : false, "orderable": false },
        { "data": "pmntType", "targets" : 3, "searchable" : false, "orderable": false },
        { "data": "startDate","targets" : 4, "searchable" : false, "orderable": false },
        { "data": "cutoffDate","targets": 5, "searchable" : false, "orderable": false },
        { "data": "isFresh",  "targets" : 6, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              if (data == true) {
                return '<span class="label label-success">Yes</span>';
              } else {
                return '<span class="label label-important">No</span>';
              }
            } else {
              return data;
            }
          }
        },
        { "data": "reports",  "targets" : 7, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = view.renderTableButton(data);
              return res;
            } else {
              return data;
            }
          }
        },
        { "data": "actions",  "targets" : 8, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = view.renderTableButton(data);
              return res;
            } else {
              return data;
            }
          }
        }
      ],
      "ajax": {
        url: this.tblJsonUrl + '?schedule=' + schedule,
        type: 'POST',
        contentType: "application/json",
        data: function (d) {
          return  JSON.stringify(d);
        }
      }
    });
  },

  renderTableButton: function(actions) {
    if (Object.keys(actions).length == 0) {
      return '';
    }

    if (Object.keys(actions).length == 1) {
      var title = Object.keys(actions)[0];
      btn = '<a role="button" class="btn btn-info input-block-level" onclick="getPage(\''+ actions[title] + '\');return false;">' + title + '</a>' ;
      return btn;
    }

    var btn  =  '<div class="btn-group">' +
                '<button class="btn btn-info" data-toggle="dropdown">Manage</button>' +
                '<button class="btn btn-info dropdown-toggle" data-toggle="dropdown">' +
                '   <span class="caret"></span>' +
                '</button>' +
                '<ul class="dropdown-menu">';

    $.each(actions, function(k,v){
      btn += '<li>';
      btn += '<a onclick="getPage(\''+ v + '\');return false;">';
      btn += k;
      btn += '</a></li>';
    });

    btn += '</ul>';
    btn += '</div>';
    return btn;
  }

};

/**
 * Individual Report For User
 */
var BillingReportForUser = {
  init: function(urlPage) {
    this.btnDateSet   = $('#btnDateSet');
    this.billingMonth = $('#billingMonth');
    this.billingYear  = $('#billingYear');
    this.tblPublished = $('#publishedTable');
    this.tblUsers     = $('#billedUsersTable');
    this.genInvoice   = $('#generateInvoice');
    this.stateAreaName= 'billingStateArea';

    this.urlPage    = urlPage;

    var view = this;
    this.btnDateSet.click(function(){
      var url = view.urlPage + '?month=' + view.billingMonth.val() + '&year=' + view.billingYear.val();
      getPage(url);
      return false;
    });

    this.tblPublished.DataTable({
      "paging": true,
      "lengthMenu": [10, 25, 50, "All"]
    });

    this.tblUsers.DataTable({
      "paging": true,
      "lengthMenu": [10, 25, 50, "All"]
    });

    this.genInvoice.click(function(){
      var requestUrl = $(this).attr('href');
      postJsonRequestUrl(requestUrl, function(data){
        billingCheckStatus(data.statusUrl, view.stateAreaName);
      });
      return false;
    });
  }
};

/**
 * Individual Report For User
 */
var BillingReportForCmpy = {
  init: function(urlPage) {
    this.btnDateSet   = $('#btnDateSet');
    this.billingMonth = $('#billingMonth');
    this.billingYear  = $('#billingYear');
    this.tblPublished = $('#publishedTable');
    this.tblUsers     = $('#billedUsersTable');
    this.genInvoice   = $('#generateInvoice');
    this.stateAreaName= 'billingStateArea';

    this.urlPage    = urlPage;

    var view = this;
    this.btnDateSet.click(function(){
      var url = view.urlPage + '?month=' + view.billingMonth.val() + '&year=' + view.billingYear.val();
      getPage(url);
      return false;
    });

    this.tblPublished.DataTable({
      "paging": true,
      "lengthMenu": [10, 25, 50, "All"]
    });

    this.tblUsers.DataTable({
      "paging": true,
      "lengthMenu": [10, 25, 50, "All"]
    });

    this.genInvoice.click(function(){
      var requestUrl = $(this).attr('href');
      postJsonRequestUrl(requestUrl, function(data){
        billingCheckStatus(data.statusUrl, view.stateAreaName);
      });
      return false;
    });
  }
};

var BillingInvoices = {
  init: function(urlPage, urlStatus) {
    this.billingMonth       = $('#billingMonth');
    this.billingYear        = $('#billingYear');
    this.btnSwitchPeriod    = $('#btnSwitchPeriod');
    this.tblInvoices        = $('#invoicesTable');
    this.btnGenerate        = $('#btnGenerate');
    this.btnPushFreshbooks  = $('#btnPushFreshbooks');
    this.btnSendFreshbooks  = $('#btnSendFreshbooks');
    this.btnSyncFreshbooks  = $('#btnSyncFreshbooks');
    this.stateAreaName      = 'billingStateArea';
    this.urlPage            = urlPage;
    this.urlStatus          = urlStatus;

    var view = this;

    this.btnGenerate.click(function(){
      var requestUrl = '/billing/invoices/generate/year/'+view.billingYear.val()+'/month/' + view.billingMonth.val();
      postJsonRequestUrl(requestUrl, function(data){
        billingCheckStatus(data.statusUrl, view.stateAreaName, function() {
          view.reloadPage();
        });
      });
    });

    this.btnSwitchPeriod.click(function(){
      return view.reloadPage();
    });

    this.btnPushFreshbooks.click(function(){
      var requestUrl = '/billing/invoices/push/year/'+view.billingYear.val()+'/month/' + view.billingMonth.val();
      postJsonRequestUrl(requestUrl, function(data){
        billingCheckStatus(data.statusUrl, view.stateAreaName, function() {
          view.reloadPage();
        });
      });
    });

    this.btnSendFreshbooks.click(function(){

      var res = confirm("Are you sure you want to send all invoices for " + view.billingMonth.val() +
                         ", " + view.billingYear.val() + " to users? This can't be undone!");

      if (res) {
        var requestUrl = '/billing/invoices/send/year/' + view.billingYear.val() + '/month/' + view.billingMonth.val();
        postJsonRequestUrl(requestUrl, function (data) {
          billingCheckStatus(data.statusUrl, view.stateAreaName, function () {
            view.reloadPage();
          });
        });
      }
    });

    this.btnSyncFreshbooks.click(function(){
      var requestUrl = '/billing/invoices/sync/year/'+view.billingYear.val()+'/month/' + view.billingMonth.val();
      postJsonRequestUrl(requestUrl, function(data){
        billingCheckStatus(data.statusUrl, view.stateAreaName, function() {
          view.reloadPage();
        });
      });
    });

    this.tblInvoices.DataTable({
      "paging": true,
      "lengthMenu": [25, 50, 100, "All"]
    });

    billingCheckStatus(this.urlStatus, this.stateAreaName); //Checking status on load
  },

  reloadPage: function() {
    var url = this.urlPage + '?month=' + this.billingMonth.val() + '&year=' + this.billingYear.val();
    getPage(url);
    return false;
  }
};

var billingCheckStatus = function(requestUrl, idProgressArea, onStop) {
  var interval = 5000; //Every 5 seconds
  var statusCheckResponseNumber = 0;

  postJsonRequestUrl(requestUrl, function(data){
    statusCheckResponseNumber++;
    var stateText = data.currState;
    switch (data.currState) {
      case 'INVOICES_GENERATING':
        stateText = 'Billing State: Generating Invoices';
        break;
      case 'INVOICES_PUSHING':
        stateText = 'Billing State: Pushing Invoices to Freshbooks';
        break;
      case 'INVOICES_SENDING':
        stateText = 'Billing State: Sending Invoices to Customers';
        break;
      case 'INVOICES_SYNCING':
        stateText = 'Billing State: Synchronizing Invoices with Freshbooks';
        break;
      case 'PLANS_SYNCING':
        stateText = 'Billing State: Synchronizing Billing Plans and Freshbooks Items';
        break;
      case 'USERS_SYNCING':
        stateText = 'Billing State: ';
        break;
    }

    $('#' + idProgressArea + ' #billingState').text(stateText);

    if (data.totalItems != 0) {
      var percentComplete = (data.currItem / data.totalItems) * 100;
      $('#' + idProgressArea + ' .bar').css('width', percentComplete + '%');
      var progressElement = $('<span>').text(data.currItem + '/' + data.totalItems);
      $('#' + idProgressArea + ' .progress span').replaceWith(progressElement);
    } else { //Total number of items is unknown
      $('#' + idProgressArea + ' .bar').css('width', 100 + '%');
      var progressElement = $('<span>').text(data.currItem);
      $('#' + idProgressArea + ' .progress span').replaceWith(progressElement);
    }

    if (onStop && data.currState == 'STOPPED') {//On stop callback, i.e. call this when STOPPED state just appeared
      if($('#' + idProgressArea).is(":visible") ||
         (!$('#' + idProgressArea).is(":visible") && statusCheckResponseNumber == 1)) {
        onStop();
      }
    }

    if (data.currState != 'STOPPED') {
      $('#' + idProgressArea).show();
      setTimeout(function() {
        billingCheckStatus(requestUrl, idProgressArea, onStop);
      }, interval);
    } else {
      $('#' + idProgressArea).hide();
    }
  });
};

var FeatureView = {
 init: function(editUrl) {
   this.checkboxes = $('#formFeature input[type="checkbox"]');
   this.btnsCRU =  $('#featureUpdate, #featureCreate');
   this.multiselect = $("#featureCapabilities");
   this.table = $('#featuresTable');


   this.checkboxes.bootstrapSwitch('destroy');
   this.checkboxes.bootstrapSwitch(
       {
         onColor: "success", offColor: "danger"
       });

   // Docs at: http://www.virtuosoft.eu/code/bootstrap-duallistbox/
   this.multiselect.bootstrapDualListbox(
       {
         bootstrap2Compatible: true,
         nonSelectedListLabel: 'Available Capabilities',
         selectedListLabel: 'Feature Capabilities',
         moveOnSelect: false,
         preserveSelectionOnMove: 'moved'
       });

   this.btnsCRU.unbind('click').click(function() {
     postForm(editUrl, 'formFeature');
     return false;
   });

   this.table.DataTable({
                          "paging": true,
                          "lengthMenu": [25, 50, 100, "All"],
                          "ordering": false
                        });


 }
};

var initFeatures = function (editUrl) {
  var f = Object.create(FeatureView);
  f.init(editUrl);
};


var CapabilityView = {
  init: function (editUrl) {
    this.checkboxes = $('#formCapability input[type="checkbox"]');
    this.btnsCRU = $('#capabilityUpdate');
    this.table = $('#capabilitiesTable');


    this.checkboxes.bootstrapSwitch('destroy');
    this.checkboxes.bootstrapSwitch(
        {
          onColor: "success", offColor: "danger"
        });

    this.btnsCRU.unbind('click').click(
        function () {
          postForm(editUrl, 'formCapability');
          return false;
        });

    this.table.DataTable(
        {
          "paging": true, "lengthMenu": [25, 50, 150], "ordering": false
        });
  }
};

var initCapability = function (editUrl) {
  var c = Object.create(CapabilityView);
  c.init(editUrl);
};


var bkConfirmCCUpdate = function (url) {

  if(confirm("Are you sure you want to update the credit card billing for this user?")) {
    doPost( url ) ;
  }

}



var BillingCCAuditUI = {
  init: function(jsonUrl) {
    this.tblCompanies = $('#ccAuditTable');
    this.tblJsonUrl   = jsonUrl;

    var emailsListUI = this;
    this.tblCompanies.DataTable({
      "processing": true,
      "serverSide": true,
      "columnDefs": [
        { "data": "txnId",   "targets" : 0, "searchable" : true, "orderable": false },
        { "data": "txnType",     "targets" : 1, "searchable" : true, "orderable": false },
        { "data": "timestamp",  "targets" : 2, "searchable" : false, "orderable": false},
        { "data": "invoiceUser",  "targets" : 3, "searchable" : false, "orderable": false},
        { "data": "billedUser",  "targets" : 4, "searchable" : false, "orderable": false},
        { "data": "createdBy",  "targets" : 5, "searchable" : false, "orderable": false},
        { "data": "status",  "targets" : 6, "searchable" : false, "orderable": false},
        { "data": "msg",  "targets" : 7, "searchable" : false, "orderable": false},
        { "data": "plan",    "targets" : 8, "searchable" : true, "orderable": false },
        { "data": "processor",  "targets" : 9, "searchable" : true, "orderable": false }
      ],
      "language": {
        "emptyTable":     "No Audit Activity"
      },
      "pageLength": 50,
      "ajax": {
        url: emailsListUI.tblJsonUrl,
        type: 'POST',
        contentType: "application/json",
        data: function (d) {
          return  JSON.stringify(d);
        }
      }
    });

  }

};


var initBilingAuditCC = function(jsonUrl) {
  var ccAuditDtTable = Object.create(BillingCCAuditUI);
  ccAuditDtTable.init(jsonUrl);
};

