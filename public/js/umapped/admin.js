/**
 * Created by surge on 2015-03-30.
 */
var CompaniesListAdminUI = {
  init: function(jsonUrl) {
    this.tblCompanies = $('#companiesTable');
    this.tblJsonUrl   = jsonUrl;

    var cmpiesListUI = this;
    this.tblCompanies.DataTable({
      "processing": true,
      "serverSide": true,
      "columnDefs": [
        { "data": "name",       "targets" : 0, "searchable" : false, "orderable": false },
        { "data": "address",    "targets" : 1, "searchable" : false, "orderable": false },
        { "data": "contact",    "targets" : 2, "searchable" : false, "orderable": false },
        { "data": "type",       "targets" : 3, "searchable" : false, "orderable": false },
        { "data": "actions",    "targets" : 4, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = cmpiesListUI.renderEditButton(data);
              return res;
            } else {
              return data;
            }
          }
        }
      ],
      "ajax": {
        url: cmpiesListUI.tblJsonUrl,
        type: 'POST',
        contentType: "application/json",
        data: function (d) {
          return  JSON.stringify(d);
        }
      }
    });

  },
  renderEditButton: function(actions) {
    if (Object.keys(actions).length == 1) {
      var title = Object.keys(actions)[0];
      btn = '<a role="button" class="btn btn-info input-block-level" onclick="getPage(\''+ actions[title] + '\');return false;">' + title + '</a>' ;
      return btn;
    }

    var btn  =  '<div class="btn-group">' +
                '<button class="btn btn-info" data-toggle="dropdown">Manage</button>' +
                '<button class="btn btn-info dropdown-toggle" data-toggle="dropdown">' +
                '   <span class="caret"></span>' +
                '</button>' +
                '<ul class="dropdown-menu">';

    $.each(actions, function(k,v){
      btn += '<li>';
      btn += '<a onclick="getPage(\''+ v + '\');return false;">';
      btn += k;
      btn += '</a></li>';
    });

    btn += '</ul>';
    btn += '</div>';
    return btn;
  }
};

var initCompanies = function(jsonUrl) {
  var cmpyDtTable = Object.create(CompaniesListAdminUI);
  cmpyDtTable.init(jsonUrl);
};


var UsersListAdminUI = {
  init: function(jsonUrl) {
    this.tblCompanies = $('#usersTable');
    this.tblJsonUrl   = jsonUrl;

    var cmpiesListUI = this;
    this.tblCompanies.DataTable({
      "processing": true,
      "serverSide": true,
      "columnDefs": [
        { "data": "userId", "name": "userid", "targets" : 0, "searchable" : false, "orderable": true },
        { "data": "name", "name": "firstname", "targets" : 1, "searchable" : false, "orderable": true },
        { "data": "email",      "targets" : 2, "searchable" : false, "orderable": false },
        { "data": "contact",    "targets" : 3, "searchable" : false, "orderable": false },
        { "data": "bilPlanName","targets" : 4, "searchable" : false, "orderable": false },
        { "data": "bilAddons",  "targets" : 5, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = cmpiesListUI.renderAddons(data);
              return res;
            } else {
              return data;
            }
          }
        },

        { "data": "actions",  "targets" : 6, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = cmpiesListUI.renderEditButton(data);
              return res;
            } else {
              return data;
            }
          }
        }
      ],
      "ajax": {
        url: cmpiesListUI.tblJsonUrl,
        type: 'POST',
        contentType: "application/json",
        data: function (d) {
          return  JSON.stringify(d);
        }
      }
    });

  },
  renderAddons: function(addons){
    var res = '<ul>';
    for(var aIdx = 0; aIdx < addons.length; aIdx++) {
      res += '<li>';
      res += addons[aIdx];
      res += '</li>';
    }
    res += '</ul>';
    return res;
  },
  renderEditButton: function(actions) {
    if (Object.keys(actions).length == 1) {
      var title = Object.keys(actions)[0];
      btn = '<a role="button" class="btn btn-info input-block-level" onclick="getPage(\''+ actions[title] + '\');return false;">' + title + '</a>' ;
      return btn;
    }

    var btn  =  '<div class="btn-group">' +
                '<button class="btn btn-info" data-toggle="dropdown">Manage</button>' +
                '<button class="btn btn-info dropdown-toggle" data-toggle="dropdown">' +
                '   <span class="caret"></span>' +
                '</button>' +
                '<ul class="dropdown-menu">';

    $.each(actions, function(k,v){
      btn += '<li>';
      btn += '<a onclick="getPage(\''+ v + '\');return false;">';
      btn += k;
      btn += '</a></li>';
    });

    btn += '</ul>';
    btn += '</div>';
    return btn;
  }
};

var initUsersAdmin = function(jsonUrl) {
  var userDtTable = Object.create(UsersListAdminUI);
  userDtTable.init(jsonUrl);
};


var UsersListCmpyUI = {
  init: function(jsonUrl) {
    this.tblCompanies = $('#usersCmpyTable');
    this.tblJsonUrl   = jsonUrl;

    var cmpiesListUI = this;
    this.tblCompanies.DataTable({
      "processing": true,
      "serverSide": true,
      "columnDefs": [
        { "data": "userId",     "targets" : 0, "searchable" : false, "orderable": false },
        { "data": "name",       "targets" : 1, "searchable" : false, "orderable": false },
        { "data": "email",      "targets" : 2, "searchable" : false, "orderable": false },
        { "data": "contact",    "targets" : 3, "searchable" : false, "orderable": false },
        { "data": "admin",      "targets" : 4, "searchable" : false, "orderable": false },
        { "data": "access",     "targets" : 5, "searchable" : false, "orderable": false },
        { "data": "actions",  "targets" : 6, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = cmpiesListUI.renderEditButton(data);
              return res;
            } else {
              return data;
            }
          }
        }
      ],
      "ajax": {
        url: cmpiesListUI.tblJsonUrl,
        type: 'POST',
        contentType: "application/json",
        data: function (d) {
          return  JSON.stringify(d);
        }
      }
    });

  },
  renderEditButton: function(actions) {
    if (Object.keys(actions).length == 1) {
      var title = Object.keys(actions)[0];
      btn = '<a role="button" class="btn btn-info input-block-level" onclick="getPage(\''+ actions[title] + '\');return false;">' + title + '</a>' ;
      return btn;
    }

    var btn  =  '<div class="btn-group">' +
                '<button class="btn btn-info" data-toggle="dropdown">Manage</button>' +
                '<button class="btn btn-info dropdown-toggle" data-toggle="dropdown">' +
                '   <span class="caret"></span>' +
                '</button>' +
                '<ul class="dropdown-menu">';

    $.each(actions, function(k,v){
      btn += '<li>';
      btn += '<a onclick="getPage(\''+ v + '\');return false;">';
      btn += k;
      btn += '</a></li>';
    });

    btn += '</ul>';
    btn += '</div>';
    return btn;
  }
};

var initUsersCmpy = function(jsonUrl) {
  var userCmpyDtTable = Object.create(UsersListCmpyUI);
  userCmpyDtTable.init(jsonUrl);
};

var MessengerAdminUI = {
  init: function(jsonUrl, cleanupUrl, stateUrl) {
    var view = this;

    this.tblConferences = $('#conferencesTable');
    this.btnCleanMsg    = $('#btnCleanMessenger');

    this.tblJsonUrl     = jsonUrl;
    this.stateAreaName  = 'messengerStateArea';

    //When just getting inside - let's check the state of the background jobs
    setTimeout(function(){
      var data = {
        stateUrl: stateUrl,
        condition: 'STOPPED'
      };
      view.stateUpdater(data,  view.stateAreaName);
    }, 100);

    this.onStateRunning = function() {
      view.btnCleanMsg.hide();
      $('#' + view.stateAreaName).show();
    };

    this.onStateStop = function() {
      console.log("Stopping to monitor state...");
      view.btnCleanMsg.show();
      $('#' + view.stateAreaName).hide();

      view.tblConferences.DataTable().ajax.reload(null, false);
    };

    this.stateUpdater = function(prevData, idProgressArea){
      var interval = 1000; //Every 5 seconds
      var statusCheckResponseNumber = 0;

      postJsonRequestUrl(prevData.stateUrl, function(data){
        statusCheckResponseNumber++;
        var stateText = data.state;
        $('#' + idProgressArea + ' #messengerState').text(stateText);
        if (data.maxVal != 0) {
          var percentComplete = (data.value / data.maxVal) * 100;
          $('#' + idProgressArea + ' .bar').css('width', percentComplete + '%');
          var progressElement = $('<span>').text(data.value + '/' + data.maxVal);
          $('#' + idProgressArea + ' .progress span').replaceWith(progressElement);
        }
        else { //Total number of items is unknown
          $('#' + idProgressArea + ' .bar').css('width', 100 + '%');
          var progressElement = $('<span>').text(data.value);
          $('#' + idProgressArea + ' .progress span').replaceWith(progressElement);
        }

        if (data.condition == 'STOPPED' && prevData.condition != 'STOPPED') {
          view.onStateStop();
        }

        if (data.condition != 'STOPPED') {
          setTimeout(function() {
            view.onStateRunning();
            view.stateUpdater(data, idProgressArea);
          }, interval);
        }
      });
    };

    this.btnCleanMsg.click(function(){
      postJsonRequestUrl(cleanupUrl, function(data){
        setTimeout(function(){
          view.stateUpdater(data,  view.stateAreaName);
        }, 1000);
      });
    });

    this.tblConferences.DataTable({
      "processing": true,
      "serverSide": true,
      "columnDefs": [
        { "data": "trip",         "name":"tripid",        "targets" : 0, "searchable" : false, "orderable": false },
        { "data": "tripStartDate","name":"tripStartDate", "targets" : 1, "searchable" : false, "orderable": false },
        { "data": "tripEndDate",  "name":"tripEndDate",   "targets" : 2, "searchable" : false, "orderable": false },
        { "data": "removed",      "name":"removed",       "targets" : 3, "searchable" : false, "orderable": true  },
        { "data": "removedDate",  "name":"removed_ts",    "targets" : 4, "searchable" : false, "orderable": true  },
        { "data": "roomCount",    "name":"room_count",    "targets" : 5, "searchable" : false, "orderable": true  },
        { "data": "msgCount",     "name":"msg_count",     "targets" : 6, "searchable" : false, "orderable": true  },
        { "data": "userCount",    "name":"user_count",    "targets" : 7, "searchable" : false, "orderable": false },
        { "data": "actions",      "name":"actions",       "targets" : 8, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = view.renderManageButton(data);
              return res;
            } else {
              return data;
            }
          }
        }
      ],
      "ajax": {
        url: view.tblJsonUrl,
        type: 'POST',
        contentType: "application/json",
        data: function (d) {
          return  JSON.stringify(d);
        }
      }
    });

  },
  renderManageButton: function(actions) {
    if (Object.keys(actions).length == 1) {
      var title = Object.keys(actions)[0];
      btn = '<a role="button" class="btn btn-info input-block-level" onclick="getPage(\''+ actions[title] + '\');return false;">' + title + '</a>' ;
      return btn;
    }

    var btn  =  '<div class="btn-group">' +
                '<button class="btn btn-info" data-toggle="dropdown">Manage</button>' +
                '<button class="btn btn-info dropdown-toggle" data-toggle="dropdown">' +
                '   <span class="caret"></span>' +
                '</button>' +
                '<ul class="dropdown-menu">';

    $.each(actions, function(k,v){
      btn += '<li>';
      btn += '<a onclick="getPage(\''+ v + '\');return false;">';
      btn += k;
      btn += '</a></li>';
    });

    btn += '</ul>';
    btn += '</div>';
    return btn;
  }
};


var initMessengerAdmin = function(jsonUrl, cleanupUrl, stateUrl) {
  var msgadminui = Object.create(MessengerAdminUI);
  msgadminui.init(jsonUrl, cleanupUrl, stateUrl);
};
