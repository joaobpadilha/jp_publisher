var editTrip = function(url) {
  activateTopMenu('mainMenuDashboard');
  getPage(url);
}

var EmailParseLogUI = {
  init: function(jsonUrl) {
    this.tblCompanies = $('#emailsTable');
    this.tblJsonUrl   = jsonUrl;

    var emailsListUI = this;
    this.tblCompanies
        .DataTable({
      "processing": true,
      "serverSide": true,
      "columnDefs": [
        { "data": "createdDate",   "targets" : 0, "searchable" : true, "orderable": false },
        { "data": "subject",     "targets" : 1, "searchable" : true, "orderable": false },
        { "data": "state",    "targets" : 2, "searchable" : true, "orderable": false },
        { "data": "parser",  "targets" : 3, "searchable" : true, "orderable": false },
        { "data": "bookingCount",  "targets" : 4, "searchable" : false, "orderable": false},
        { "data": "attachCount",  "targets" : 5, "searchable" : false, "orderable": false}
      ],
      "language": {
              "emptyTable":     "No Email Activity"
      },
      "ajax": {
              url: emailsListUI.tblJsonUrl,
              type: 'POST',
              contentType: "application/json",
              data: function (d) {
                return  JSON.stringify(d);
              },
              error: function (xhr, textStatus, errorThrown) {
                window.location.replace("/login");
              }
            }
    });

  }

};


var initEmailParseLog = function(jsonUrl) {
  var emailDtTable = Object.create(EmailParseLogUI);
  emailDtTable.init(jsonUrl);
};



var PublishedTripsUI = {
  init: function(jsonUrl) {
    this.tblCompanies = $('#publishedTripsTable');
    this.tblJsonUrl   = jsonUrl;

    var publishedTripListUI = this;
    this.tblCompanies
        .DataTable({
      "processing": true,
      "serverSide": true,
      "columnDefs": [
        { "data": "imgUrl",   "targets" : 0, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = publishedTripListUI.renderTripCover(data);
              return res;
            } else {
              return data;
            }
          }
        },
        { "data": "tripName",   "targets" : 1, "searchable" : false, "orderable": false },
        { "data": "id",         "targets" : 2, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = publishedTripListUI.renderTripButton(data);
              return res;
            } else {
              return data;
            }
          }
        }
      ],
      "language": {
        "emptyTable":     "No Upcoming Trips"
      },
      "ajax": {
        url: publishedTripListUI.tblJsonUrl,
        type: 'POST',
        contentType: "application/json",
        data: function (d) {
          return  JSON.stringify(d);
        },
        error: function (xhr, textStatus, errorThrown) {
          window.location.replace("/login");
        }
      }
    });

  },
  renderTripButton: function(id) {
    btn = '<a role="button" class="btn btn-info input-block-level" onclick="editTrip(\'' + id + '\');return false;"> Edit </a>' ;
    return btn;
  },
  renderTripCover: function(imgUrl) {
    if (imgUrl != null) {
      btn = '<div style="background-image:url(' + imgUrl + ');center;width:40px;height:40px;margin:auto;background-size:cover;" >&nbsp;</div>';
      return btn;
    } else {
      btn = '<div style="background-image:url(https://s3.amazonaws.com/static-umapped-com/public/img/photo-placeholder-gray.png);center;width:40px;height:40px;margin:auto;background-size:cover;" >&nbsp;</div>';
      return btn;
    }
  }

};

var initPublishedTrips = function(jsonUrl) {
  var publishedTripsTable = Object.create(PublishedTripsUI);
  publishedTripsTable.init(jsonUrl);
};



var PendingTripsUI = {
  init: function(jsonUrl) {
    this.tblCompanies = $('#pendingTripsTable');
    this.tblJsonUrl   = jsonUrl;

    var pendingTripListUI = this;
    this.tblCompanies
        .DataTable({
      "processing": true,
      "serverSide": true,
      "columnDefs": [
        { "data": "imgUrl",   "targets" : 0, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = pendingTripListUI.renderTripCover(data);
              return res;
            } else {
              return data;
            }
          }
        },
        { "data": "tripName",   "targets" : 1, "searchable" : false, "orderable": false },
        { "data": "id",         "targets" : 2, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = pendingTripListUI.renderTripButton(data);
              return res;
            } else {
              return data;
            }
          }
        }
      ],
      "language": {
        "emptyTable":     "No Pending Trips"
      },
      "ajax": {
        url: pendingTripListUI.tblJsonUrl,
        type: 'POST',
        contentType: "application/json",
        data: function (d) {
          return  JSON.stringify(d);
        },
        error: function (xhr, textStatus, errorThrown) {
          window.location.replace("/login");
        }
      }
    });

  },
  renderTripButton: function(id) {
    btn = '<a role="button" class="btn btn-info input-block-level" onclick="editTrip(\'' + id + '\');return false;"> Edit </a>' ;
    return btn;
  },
  renderTripCover: function(imgUrl) {
    if (imgUrl != null) {
      btn = '<div style="background-image:url(' + imgUrl + ');center;width:40px;height:40px;margin:auto;background-size:cover;" >&nbsp;</div>';
      return btn;
    } else {
      btn = '<div style="background-image:url(https://s3.amazonaws.com/static-umapped-com/public/img/photo-placeholder-gray.png);center;width:40px;height:40px;margin:auto;background-size:cover;" >&nbsp;</div>';
      return btn;
    }
  }

};

var initPendingTrips = function(jsonUrl) {
  var pendingTripsTable = Object.create(PendingTripsUI);
  pendingTripsTable.init(jsonUrl);
};




var SharedTripsUI = {
  init: function(jsonUrl) {
    this.tblCompanies = $('#sharedTripsTable');
    this.tblJsonUrl   = jsonUrl;
    var sharedTripListUI = this;

    this.tblCompanies
        .DataTable({
      "processing": true,
      "serverSide": true,
      "columnDefs": [
        { "data": "imgUrl",   "targets" : 0, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = sharedTripListUI.renderTripCover(data);
              return res;
            } else {
              return data;
            }
          }
        },
        { "data": "tripName",   "targets" : 1, "searchable" : false, "orderable": true },
        { "data": "tripDate",   "targets" : 2, "searchable" : false, "orderable": true },
        { "data": "status",   "targets" : 3, "searchable" : false, "orderable": false },
        { "data": "createdBy",   "targets" : 4, "searchable" : false, "orderable": false },
        { "data": "company",   "targets" : 5, "searchable" : false, "orderable": false },
        { "data": "accessLevel",   "targets" : 6, "searchable" : false, "orderable": false },
        { "data": "id",         "targets" : 7, "searchable" : false, "orderable": false,
          "render" : function(data, type, row, meta) {
            if (type === 'display' ) {
              var res = sharedTripListUI.renderTripEditButton(data);
              var str = row["accessLevel"];
              if (str === 'Read Access') {
                res =  sharedTripListUI.renderTripViewButton(data);
              }
              return res;
            } else {
              return data;
            }
          }
        }
      ],
      "language": {
        "emptyTable":     "No Shared Trips",
        "info": "",
        "infoEmpty": "",
        "lengthMenu": "Show _MENU_ trips"
      },
      "ajax": {
        url: sharedTripListUI.tblJsonUrl,
        type: 'POST',
        contentType: "application/json",
        data: function (d) {
          return  JSON.stringify(d);
        },
        error: function (xhr, textStatus, errorThrown) {
          window.location.replace("/login");
        }
      }
    });


  },
  renderTripEditButton: function(id) {
    btn = '<a role="button" class="btn btn-info input-block-level" onclick="editTrip(\'' + id + '\');return false;"> Edit </a>' ;
    return btn;
  },
  renderTripViewButton: function(id) {
    btn = '<a role="button" class="btn btn-info input-block-level" onclick="editTrip(\'' + id + '\');return false;"> View </a>' ;
    return btn;
  },
  renderTripCover: function(imgUrl) {
    if (imgUrl != null) {
      btn = '<div style="background-image:url(' + imgUrl + ');center;width:42px;height:42px;margin:auto;background-size:cover;" >&nbsp;</div>';
      return btn;
    } else {
      btn = '<div style="background-image:url(https://s3.amazonaws.com/static-umapped-com/public/img/photo-placeholder-gray.png);center;width:42px;height:42px;margin:auto;background-size:cover;" >&nbsp;</div>';
      return btn;
    }
  }

};

var initSharedTrips = function(jsonUrl) {
  var sharedTripsTable = Object.create(SharedTripsUI);
  sharedTripsTable.init(jsonUrl);
};

var allTripsUI = {
  init: function(jsonUrl) {
    this.tblCompanies = $('#allTripsTable');
    this.tblJsonUrl   = jsonUrl;

    var allTripListUI = this;
    this.tblCompanies
        .DataTable({
      "processing": true,
      "pageLength": 25,
      "serverSide": true,
      "sDom": 'ltipr',
      "oLanguage": {
               "sInfoFiltered": "",
               "sLengthMenu": "Show _MENU_ trips",
               "sInfo": "",
               "sInfoEmpty": ""
             },
      "columnDefs": [
        { "data": "imgUrl",   "targets" : 0, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = allTripListUI.renderTripCover(data);
              return res;
            } else {
              return data;
            }
          }
        },
        { "data": "tripName",   "targets" : 1, "searchable" : false, "orderable": true },
        { "data": "tripDate",   "targets" : 2, "searchable" : false, "orderable": true },
        { "data": "status",   "targets" : 3, "searchable" : false, "orderable": false },
        { "data": "createdBy",   "targets" : 4, "searchable" : false, "orderable": false },
        { "data": "id",         "targets" : 5, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = allTripListUI.renderTripButton(data);
              return res;
            } else {
              return data;
            }
          }
        }
      ],
      "language": {
        "emptyTable":     "No Trips Found",
        "lengthMenu": "Show _MENU_ trips",
        "info": ""
      },
      "ajax": {
        url: allTripListUI.tblJsonUrl,
        type: 'POST',
        contentType: "application/json",
        data: function (d) {
          d.status = $('#searchStatus').val();
          if($('#allTrips').is(":checked")){
            d.allTrips = true;

          } else if($('#allTrips').is(":not(:checked)")){
            d.allTrips = false;
          }
          d.keyword = $('#searchTerm').val();
          d.filterDate = $('#filterDate').val();
          console.log(JSON.stringify(d));
          return  JSON.stringify(d);
        },
        error: function (xhr, textStatus, errorThrown) {
          window.location.replace("/login");
        }
      }
    });

  },
  renderTripButton: function(id) {
    btn = '<a role="button" class="btn btn-info input-block-level" onclick="editTrip(\'' + id + '\');return false;"> Edit </a>' ;
    return btn;
  },
  renderTripCover: function(imgUrl) {
    if (imgUrl != null) {
      btn = '<div style="background-image:url(' + imgUrl + ');center;width:42px;height:42px;margin:auto;background-size:cover;" >&nbsp;</div>';
      return btn;
    } else {
      btn = '<div style="background-image:url(https://s3.amazonaws.com/static-umapped-com/public/img/photo-placeholder-gray.png);center;width:42px;height:42px;margin:auto;background-size:cover;" >&nbsp;</div>';
      return btn;
    }
  }

};

var initAllTrips = function(jsonUrl) {
  var allTripsTable = Object.create(allTripsUI);
  allTripsTable.init(jsonUrl);
};





