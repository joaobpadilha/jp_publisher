
function imageCropper(imgurl) {
  var ms = (new Date).getTime();
  imgurl+="?"
  if (imgurl.indexOf('?') > 0) {
    imgurl += "&v1=" + ms;
  } else {
    imgurl += "?v1=" + ms;
  }
  $('#imgCropperId').attr('src', imgurl);
  $('#imgCropperModal' ).modal('show');

  $('#imgCropperModal').on('shown', function() {
    $ ('#imgCropperId').cropper ( {
      built: function () {
      },
      aspectRatio : 16 / 9.
    });
  })
}

/**
 * Request for image crop
 * @param url - url to go to after successful crop
 * @param fileId - file id of the original image
 * @param useCase - use case which requires image crop
 * @param useId - use case id to modify to use new cropped image
 */
function cropImg (url, fileId, useCase, useId) {
  $('#imgCropSpinner').show();

  var cropArea = $('#imgCropperId').cropper('getCropBoxData');
  var width = cropArea.width;
  if (width > 1024) {
    width = 1024;
  }
  var height = cropArea.height;
  if (height > 576) {
    height = 576;
  }

  var result = $('#imgCropperId').cropper('getCroppedCanvas', {
    width: width,
    height: height
  });

  result.toBlob(function (blob) {
    var formData = new FormData();
    formData.append('croppedImage', blob);
    formData.append('fileId', fileId);
    formData.append('useCase', useCase);
    formData.append('useId', useId);
    formData.append('width', result.width);
    formData.append('height', result.height);

    //upload to server
    $.ajax('/img/cropper/save', {
      method: "POST",
      data: formData,
      processData: false,
      contentType: false,
      success: function (response) {
        $('#imgCropSpinner').hide();
        $('#imgCropperModal' ).modal('hide');
        if(response.returnCode != 0) {
          displayAlert(response.msg, response.returnCode, response.severityLevel);
          return;
        } else {
          getPage(url);
        }
      },
      error: function (response) {
        $('#imgCropSpinner').hide();
        alert('Error saving the cropped image.')
      }
    });
  }, 'image/jpeg');
}

function resetImgCropper() {
  $('#imgCropperId').cropper('reset');
}

function restoreOrigImg (url, fileId, useCase, useId) {
  $('#imgCropSpinner').show();
  var formData = new FormData();
  formData.append('fileId', fileId);
  formData.append('useCase', useCase);
  formData.append('useId', useId);
  $.ajax('/img/cropper/restore', {
    method: "POST",
    data: formData,
    processData: false,
    contentType: false,
    success: function (response) {
      $('#imgCropSpinner').hide();
      $('#imgCropperModal' ).modal('hide');
      if(response.returnCode != 0) {
        displayAlert(response.msg, response.returnCode, response.severityLevel);
        return;
      } else {
        getPage(url);
      }
    },
    error: function () {
      alert('Error restoring the original image.')
    }
  });
}
