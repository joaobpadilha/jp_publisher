/**
 * POI Data feeds
 */

var initFeeds = function() {

  $('#feedSourceSelector').change(function(){
    var newUrl = $(this).find(":selected").val();
    getPage(newUrl);
    return false;
  });

  $('#feedStateSelector').change(function(){
    var newUrl = $(this).find(":selected").val();
    getPage(newUrl);
    return false;
  });

  $('#feedFilesTable').DataTable({
    "paging": true,
    "lengthMenu": [5, 15, 50, "All"],
    "order": [[ 2, "desc" ]]
  });

  //Reload page when new data is loaded
  $('#feedAssociateModalClose, #feedAssociateModalCloseTop').unbind("click").click(function(){
    $('#feedAssociateModal').modal("hide");
    reloadPage();
    return false;
  });

  $('#feedLoadModalClose, #feedLoadModalCloseTop').unbind("click").click(function(){
    $('#feedLoadModal').modal("hide");
    reloadPage();
    return false;
  });

  $('#feedPurgeModalClose, #feedPurgeModalCloseTop').unbind("click").click(function(){
    $('#feedPurgeModal').modal("hide");
    reloadPage();
    return false;
  });

  $('#feedCopyModifiedModalClose, #feedCopyModifiedModalCloseTop').unbind("click").click(function(){
    $('#feedCopyModifiedModal').modal("hide");
    reloadPage();
    return false;
  });

  $('#feedSyncPOIModalClose, #feedSyncPOIModalCloseTop').unbind("click").click(function(){
    $('#feedSyncPOIModal').modal("hide");
    reloadPage();
    return false;
  });

  $('#feedRecordPoiModalClose, #feedRecordPoiModalCloseTop').unbind("click").click(function(){
    $('#feedRecordPoiModal').modal("hide");
    reloadPage();
    return false;
  });

  $('#feedDeleteOrphanPOIModalClose, #feedDeleteOrphanPOIModalCloseTop').unbind("click").click(function(){
    $('#feedDeleteOrphanPOIModal').modal("hide");
    reloadPage();
    return false;
  });

  $('#feedClearAmenitiesModalClose, #feedClearAmenitiesModalCloseTop').unbind("click").click(function(){
    $('#feedClearAmenitiesModal').modal("hide");
    reloadPage();
    return false;
  });


};

var initTmpltFeeds = function() {
  $('#feedSourceSelector').change(function(){
    var newUrl = $(this).find(":selected").val();
    getPage(newUrl);
    return false;
  });


  $('#feedLoadModalClose, #feedLoadModalCloseTop').unbind("click").click(function(){
    $('#feedLoadModal').modal("hide");
    reloadPage();
    return false;
  });

  $('#feedWipeAllModalCloseTop, #feedWipeAllModalClose').unbind("click").click(function(){
    $('#feedWipeAllModal').modal("hide");
    reloadPage();
    return false;
  });

  $('#feedProcessModalCloseTop, #feedProcessModalClose').unbind("click").click(function(){
    $('#feedProcessModal').modal("hide");
    reloadPage();
    return false;
  });
};

var feedLoadFile = function(requestUrl) {
  $('#feedLoadModalSpinner').show();
  postJsonRequestUrl(requestUrl, function(data){
    var alertArea = $('#feedLoadModalAlert');
    if (data.SUCCESS != 0 && data.ERROR == 0) {
      alertArea.removeClass().addClass("alert alert-success");
    } else if (data.SUCCESS == 0 && data.ERROR == 0) {
      alertArea.removeClass().addClass("alert alert-error");
    } else {
      alertArea.removeClass().addClass("alert alert-info");
    }

    $('#feedLoadModalAlert span').text("Imported:" + data.SUCCESS+ " Failed:" + data.ERROR );
    alertArea.show();
    $('#feedLoadModalSpinner').hide();
  });

  return false;
};

var feedPurge = function(requestUrl) {
  postJsonRequestUrl(requestUrl, function(data){
    var alertArea = $('#purgeResults');
    if (data.SUCCESS != 0 && data.ERROR == 0) {
      $('#purgeSuccess').show().text(data.SUCCESS + " records were purged");
    } else if (data.SUCCESS == 0 && data.ERROR == 0) {
      $('#purgeFail').show().text(data.SUCCESS + " records were purged. Check data");
    }
    alertArea.show();
  });
  return false;
};

var feedDelDup = function(requestUrl) {
  postJsonRequestUrl(requestUrl, function(data){
    var alertArea = $('#delDupResults');
    if (data.SUCCESS != 0 && data.ERROR == 0) {
      $('#delDupSuccess').show().text(data.SUCCESS + " duplicates were deleted");
    } else if (data.SUCCESS == 0 && data.ERROR == 0) {
      $('#delDupFail').show().text(data.SUCCESS + " records were deleted. Check data");
    }
    alertArea.show();
  });
  return false;
};

var feedMove = function(requestUrl) {
  $('#feedCopyModifiedModalSpinner').show();
  postJsonRequestUrl(requestUrl, function(data){
    var alertArea = $('#moveResults');
    if (data.INSERTS == 0 && data.UPDATED == 0 && data.PURGED == 0) {
      $('#moveFail').show().text("No records were modified - check data");
    } else {
      var res = data.INSERTS + " inserted records<br/>";
      res += data.UPDATED + " updated records<br/>";
      res += data.PURGED + " purged records";
      $('#moveSuccess').show().append(res);
    }
    alertArea.show();
    $('#feedCopyModifiedModalSpinner').hide();
  });
  return false;
};

var feedSyncPoi = function(requestUrl) {
  $('#feedSyncPOIModalSpinner').show();
  postJsonRequestUrl(requestUrl, function(data){
    var alertArea = $('#syncResults');
    if (data.UPDATED == 0 && data.NEW == 0) {
      $('#syncFail').show().text("No records were modified - check data");
    } else {
      var res = data.UPDATED + " updated records<br/>";
      res += data.NEW + " new records<br/>";
      $('#syncSuccess').show().append(res);
    }
    alertArea.show();
    $('#feedSyncPOIModalSpinner').hide();
  });
  return false;
};

var feedRecordShowMatches = function(requestUrl) {
  $('#feedRecordPoiModal').modal('show');
  $('#feedRecordPoiModalSpinner').show();

  postJsonRequestUrl(requestUrl, function(data){
    //Got an array of pois
    var tableBody = $('#feedRecordPoiTableData');

    $('#feedRecordName').val(data.feed.name);
    $('#feedRecordCountry').val(data.feed.countryCode);
    $('#feedRecordCity').val(data.feed.city);
    $('#feedRecordNameAddress').val(data.feed.address);
    $('#feedRecordPostal').val(data.feed.postalCode);

    tableBody.children().remove();
    for (var idx = 0; idx < data.pois.length; idx++) {
      var poi = data.pois[idx];
      var row = $('<tr>');

      var id = $('<td>').text(poi['poi-id-js']);
      var name = $('<td>').text(poi.name);
      var country = $('<td>').text(poi['country-code']);
      var city = $('<td>').text(poi.addresses[0]?poi.addresses[0].locality:"");
      var address = $('<td>').text(poi.addresses[0]?poi.addresses[0].street_address:"");
      var postalCode = $('<td>').text(poi.addresses[0]?poi.addresses[0].postal_code:"");
      var score = $('<td>').text(poi.score);
      var button = $('<td><a onclick="$(\'#feedRecordPoiModal\').modal(\'hide\');'+
                     'doPost(\'/poi/feed/link/'+ data.feed.importId + '/poi/' + poi['poi-id-js'] +
                     '\');return false;" role="button" class="btn btn-info input-block-level">Assign</a></td>');

      row.append(id);
      row.append(name);
      row.append(country);
      row.append(city);
      row.append(address);
      row.append(postalCode);
      row.append(score);
      row.append(button);

      tableBody.append(row);
    }

    $('#feedRecordPoiTable').DataTable({
      "paging": true,
      "lengthMenu": [5, 10, 15, "All"],
      "order": [[ 6, "desc" ]]
    });

    $('#feedRecordPoiModal #recAsNewBtn').unbind('click').click(function(){
      $('#feedRecordPoiModal').modal('hide');
      doPost(data.newUrl);
      return false;
    });

    $('#feedRecordPoiModalSpinner').hide();
  });
  return false;

};

var feedLoad = function(requestUrl, stateUrl) {
  $('#feedLoadModalSpinner').show();
  postJsonRequestUrl(requestUrl, function(data){
    var progressArea = $("#feedLoadProgressArea");
    progressArea.show();
    $('#feedProgressOperation').text('Initializing');
    $('#feedLoadModalSpinner').hide();
    feedCheckProgress(stateUrl, 'feedLoadProgressArea');
  });
};

var feedLoadSimple = function(requestUrl) {
  postJsonRequestUrl(requestUrl, function(data){
    $('#loadResults').show();
    if(data.SUCCESS >= 0) {
      $('#loadCount').show().text(data.SUCCESS + " records loaded.");
    }
  });
};

var feedAssociate = function(stateUrl) {
  var requestUrl = $('#feedAssociatePoiTypes').find(":selected").val();
  $('#feedAssociateModalSpinner').show();

  postJsonRequestUrl(requestUrl, function(data){

    if (data.MISMATCH != 0) {
      $('#associationMismatch').show().text(data.MISMATCH + " records are not found");
    }

    if (data.MATCH != 0) {
      $('#associationMatch').show().text(data.MATCH + " records have reliably identifiable versions in the database");
    }

    if (data.UNSURE != 0) {
      $('#associationUnsure').show().text(data.UNSURE + " records have similarly sounding POIs");
    }

    if (data.UNPROCESSED != 0) {
      $('#associationUnprocessed').show().text(data.UNPROCESSED + " records were not processed for some reason");
    }

    if (data.MISMATCH == 0 && data.MATCH == 0 && data.UNSURE == 0 && data.UNPROCESSED == 0) {
      $('#associationMismatch').show().text("Wrong POI type - no records of this type found for association");
    }

    $('#associationResults').show();
    $('#feedAssociateModalSpinner').hide();
  });

  //Can execute only after request went out
  setTimeout(function() {
    feedCheckProgress(stateUrl, 'feedAssociateState');
    $('#feedAssociateModalSpinner').hide();
  }, 1000);

  return false;
};

var feedCheckProgress = function(requestUrl, idProgressArea) {
  var interval = 5000; //Every 5 seconds

  postJsonRequestUrl(requestUrl, function(data){
    var stateText = data.currState;
    switch (data.currState) {
      case 'INIT':
        stateText = 'Current State: Initializing operation';
        break;
      case 'IMPORT':
        stateText = 'Current State: Performing Feed Import';
        break;
      case 'ASSOCIATE':
        stateText = 'Current State: Associating Feed Records with POI';
        break;
      case 'POI_SYNC':
        stateText = 'Current State: Synchronizing Feed with POI records';
        break;
      case 'STOPPED':
        stateText = 'Currently there are no live actions';
        break;
    }

    $('#' + idProgressArea + ' #feedState').text(stateText);

    if (data.totalItems != 0) {
      var percentComplete = (data.currItem / data.totalItems) * 100;
      $('#' + idProgressArea + ' .bar').css('width', percentComplete + '%');
      var progressElement = $('<span>').text(data.currItem + '/' + data.totalItems);
      $('#' + idProgressArea + ' .progress span').replaceWith(progressElement);
    } else { //Total number of items is unknown
      $('#' + idProgressArea + ' .bar').css('width', 100 + '%');
      var progressElement = $('<span>').text(data.currItem);
      $('#' + idProgressArea + ' .progress span').replaceWith(progressElement);
    }

    if (data.currState != 'STOPPED') {
      $('#' + idProgressArea).show();
      setTimeout(function() {
        feedCheckProgress(requestUrl, idProgressArea);
      }, interval);
    } else {
      $('#' + idProgressArea).hide();
    }
  });
};

var feedClearAmenities = function(requestUrl) {
  postJsonRequestUrl(requestUrl, function(data){
    $('#clearedAmenitiesResults').show();
    if(data.CLEARED >= 0) {
      $('#clearedAmenitiesCount').show().text(data.CLEARED + " records with amenities cleared");
    }
  });
};

var feedDeleteOrphanPoi = function(requestUrl) {
  postJsonRequestUrl(requestUrl, function(data){
    $('#deleteOrphansResults').show();
    if(data.DELETED >= 0) {
      $('#deletedOrphansCounts').show().text(data.DELETED + " records deleted");
    }
    if(data.SUCCESS >= 0) {
      $('#deletedOrphansCounts').show().text(data.SUCCESS + " records deleted");
    }
  });
};

var feedWipeAll = function(requestUrl) {
  postJsonRequestUrl(requestUrl, function(data){
    $('#wipeAllResults').show();
    if(data.DELETED >= 0) {
      $('#wipeAllCount').show().text(data.DELETED + " records deleted.");
    }
  });
};

var feedProcessSimple = function(requestUrl) {
  postJsonRequestUrl(requestUrl, function(data){
    $('#processResults').show();
    var text =  "";
    if(data.SUCCESS >= 0) {
      text += data.SUCCESS + " records converted.\n";
    }

    if(data.ERROR > 0) {
      text += data.ERROR + " records failed.";
    }

    $('#processCount').show().text(text);
  });
};

var feedRenderRecordButtons = function(actions) {
  var btn  =  '<div class="btn-group">' +
              '<button class="btn btn-info" data-toggle="dropdown">Manage</button>' +
              '<button class="btn btn-info dropdown-toggle" data-toggle="dropdown">' +
              '   <span class="caret"></span>' +
              '</button>' +
              '<ul class="dropdown-menu">';

  $.each(actions, function(k,v){
    btn += '<li>';
    if (k == 'Manual Match') {
      btn += '<a onclick="feedRecordShowMatches(\''+ v + '\');return false;">';
    } else {
      btn += '<a onclick="doPost(\''+ v + '\');return false;">';
    }
    btn += k;
    btn += '</a></li>';
  });

  btn += '</ul>';
  btn += '</div>';
  return btn;
};




