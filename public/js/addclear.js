// Author: Stephen Korecky
// Website: http://stephenkorecky.com
// Plugin Website: http://github.com/skorecky/Add-Clear

;
(function ($, window, document, undefined) {

  // Create the defaults once
  var pluginName = "addClear", defaults = {
        closeSymbol: "&#10006;",
        color: "#CCC",
        top: 1,
        right: 4,
        fontSize: "14px",
        returnFocus: true,
        showOnLoad: false,
        onClear: null,
        hideOnBlur: false,
        forceShow: false
      };

  // The actual plugin constructor
  function Plugin(element, options) {
    this.element = element;
    this.options = $.extend({}, defaults, options);
    this._defaults = defaults;
    this._name = pluginName;
    this.init();
  }

  Plugin.prototype = {
    init: function () {
      var $this = $(this.element);
      var me = this;
      var options = this.options;

      var clearBtn = $("<a href='#clear' style='display: none;'>")
          .css({
            color: options.color,
            'text-decoration': 'none',
            display: 'none',
            'line-height': 1,
            overflow: 'hidden',
            position: 'absolute',
            right: options.right,
            top: options.top,
            'font-size': options.fontSize
          })
          .click(function (e) {
            var element = $(me.element);
            element.val("");
            $(this).hide();
            if (options.returnFocus === true) {
              element.focus();
            }
            if (options.onClear) {
              options.onClear(element);
            }
            e.preventDefault();
          })
          .append(options.closeSymbol);

      $this.wrap("<div style='position:relative;' class='add-clear-div'></div>");
      $this.after(clearBtn);

      if (($this.val().length >= 1 || options.forceShow) && options.showOnLoad === true) {
        clearBtn.show();
      }

      $this.focus(function () {
        if ($(this).val().length >= 1) {
          clearBtn.show();
        }
      });

      $this.blur(function () {
        if (options.hideOnBlur) {
          setTimeout(function () {
            clearBtn.hide();
          }, 50);
        }
      });

      $this.keyup(function () {
        if ($(this).val().length >= 1) {
          clearBtn.show();
        }
        else {
          clearBtn.hide();
        }
      });
    }
  };

  $.fn[pluginName] = function (options) {
    return this.each(function () {
      if (!$.data(this, "plugin_" + pluginName)) {
        $.data(this, "plugin_" + pluginName, new Plugin(this, options));
      }
    });
  };

})(jQuery, window, document);
