/* 
UMAPPED MOBILE - CLICK BEHAVIORS
SCRIPTED BY OLIVIER COUILLARD
FOR KOKOMO INC.
WWW.KOKOMOWEB.COM
*/

$(document).ready(function(){
	//
	$('#list_places, #list_tags').on('click',function(){
		$('#list_places, #list_tags').removeClass('active');
		$(this).addClass('active');

		// determine which list is being opened, and which isn't (simple since we only have two options)
		var whichList = $(this).attr('id').split('_')[1],
		otherList = whichList == 'places' ? 'tags' : 'places';

		// fetch the height of the list, since it's never fixed when generated dynamically
		$('#'+whichList+'_container').css({height:$('#'+whichList+'_container ul').outerHeight()+'px'});
		$('#'+otherList+'_container').css({height:0});

		// initiate 'outside of menu' closing click behavior
		$('body').on('mouseup',function(){
			$('#list_places, #list_tags').removeClass('active');
			$('#places_container, #tags_container').css({height:0});

			// once it's executed, unbind the behavior so it doesn't repeatedly executes itself for no reason
			$('body').off('mouseup');
		});
	})
});