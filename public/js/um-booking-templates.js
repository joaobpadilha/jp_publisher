

function getBookingPage(url) {

  var calElement = $('#templateEventsCalendar');
  var calEvents = calElement.fullCalendar('clientEvents');
  var tripId = calElement.data('tripId');
  //If we have added new bookings, intercepting and reloading page
  if (calEvents.length > 0) {
    var tabName = url.attributes['href'].value.split('#')[1];
    switch(tabName) {
      case 'flightTab':
          getPage('/trips/bookings/'+ tripId +'/FLIGHTS');
        break;
      case 'hotelTab':
        getPage('/trips/bookings/'+ tripId +'/HOTELS');
        break;
      case 'cruiseTab':
        getPage('/trips/bookings/'+ tripId +'/CRUISES');
        break;
      case 'transferTab':
        getPage('/trips/bookings/'+ tripId +'/TRANSPORTS');
        break;
      case 'activityTab':
        getPage('/trips/bookings/'+ tripId +'/ACTIVITIES');
        break;
      default:
        getPage('/trips/bookings/'+ tripId +'/ITINERARY');
        break;
    }
    return false;
  }
  return true;
}

function buildBookingEvent(booking, newTripStartDate) {
  var event = {
    id: (booking.tmpltDetailId == null)?booking.tripDetailId:booking.tmpltDetailId ,
    title: booking.name,
    className: booking.cssClassName + "Cal",
    editable: true,
    startEditable: true,
    durationEditable: true,
    url: booking.detailsEditUrl,
    booking: booking
  };

  if (newTripStartDate != null) {
    event.start = newTripStartDate.clone();
    event.end = newTripStartDate.clone();

    //Setting start time if available or setting as full day event if not
    if (booking.timeStart) {
      var timeStart = moment.utc(booking.timeStart, "hh:mm A");
      event.start.hours(timeStart.hours());
      event.start.minutes(timeStart.minutes());
    }
    else {
      event.allDay = true;
    }

    //Setting end date and time (if available)
    event.end.add(booking.duration, "days");
    if (booking.timeEnd) {
      var timeEnd = null;
      if(moment.utc(booking.timeEnd).hour() < 9) {
        timeEnd = moment.utc(booking.timeEnd.add(1, 'days'), "hh:mm A");
      } else {
        timeEnd = moment.utc(booking.timeEnd, "hh:mm A");
      }
      event.end.hours(timeEnd.hours());
      event.end.minutes(timeEnd.minutes());
    }
  } else {
    event.start = moment.utc(booking.timeStart);
    if(moment.utc(booking.timeEnd).hour() < 9) {
        event.end = moment.utc(booking.timeEnd).add(1, 'days');
    } else {
        event.end = moment.utc(booking.timeEnd);
    }
  }

  /* Adjusting start and end day by this many days is not required for drag and drop
   if (booking.dayOffset != -1) {
   event.start.add(booking.dayOffset, "days");
   event.end.add(booking.dayOffset, "days");
   }
   */
  return event;
}

function loadTripTemplateBookings(templateId) {
  clearTemplateBookingsList();

  var bookingsUrl = '/template/bookings/' + templateId + '.json';
  $.getJSON(bookingsUrl, function(result) {
    $.each(result.bookings, function (key, value) {
      for (var bookingIdx = 0; bookingIdx < value.length; bookingIdx++) {
        var tripStartDate = $('#templateEventsCalendar').data('tripStartDate').clone();
        var event = buildBookingEvent(value[bookingIdx], tripStartDate);
        addTemplateBookingToList(event);
      }
    });
  });
}

function clearTemplateBookingsList() {
  $('#external-events .fc-event').remove();
}

function addTemplateBookingToList(eventObject) {
  var newEventDiv = $(document.createElement("div"));
  newEventDiv.addClass('fc-event');
  newEventDiv.addClass(eventObject.className);

  var newIconDiv = $(document.createElement("div"));
  newIconDiv.addClass(eventObject.booking.cssClassName);
  newIconDiv.addClass('calendarLabelIcon');
  newEventDiv.append(newIconDiv);
  var newBookingLabelSpan =  $(document.createElement("span"));
  newBookingLabelSpan.addClass('calendarLabel');
  newBookingLabelSpan.append(eventObject.booking.typeLabel);
  newEventDiv.append(newBookingLabelSpan);
  var newBookingNameDiv =  $(document.createElement("div"));
  newBookingNameDiv.append(eventObject.title);
  newEventDiv.append(newBookingNameDiv);
  // store the Event Object in the DOM element so we can get to it later
  newEventDiv.data('eventObject', eventObject);

  // make the event draggable using jQuery UI
  newEventDiv.draggable({
    zIndex: 999,
    revert: true,      // will cause the event to go back to its
    revertDuration: 0  //  original position after the drag
  });

  $('#external-events').append(newEventDiv);
}

function getDaysDifference(momentBefore, momentAfter) {
  var yyBefore = momentBefore.get('year');
  var yyAfter  = momentAfter.get('year');
  var ddBefore = momentBefore.dayOfYear();
  var ddAfter  = momentAfter.dayOfYear();
  return (yyAfter*365 + ddAfter) - (yyBefore*365 + ddBefore);
}

function persistNewBooking(eventData, action) {
  var tripId = $('#templateEventsCalendar').data('tripId');
  var persistUrl = '/trips/bookings/persistJson/' + tripId;

  eventData.booking.timeStart = eventData.start; //Goes in as ISO8601 string
  eventData.booking.timeEnd   = eventData.end;   //Goes in as ISO8601 string

  var bookingRequest = {
    operation: action,
    bookings: [eventData.booking]
  };
  if (!eventData.booking.tripDetailId) {
    bookingRequest.operation = 'COPY';
  }

  $.ajax({
    type: "POST",
    url: persistUrl ,
    data: JSON.stringify(bookingRequest),
    contentType: "application/json; charset=utf-8",
    dataType: "json"})
      .always(function(data, textStatus, jqXHR) {
        if (textStatus == 'success') {
          displayAlert(data.msg, data.returnCode, data.severityLevel);
          if(data.returnCode == 0){
            updateEvents(data.bookings);
            updateBookingCategoriesBadges(data.bookings, data.operation);
          }
        }
      });
  return true;
}

/** Matching all received results against all entries in the calendar and updating them */
function updateEvents(bookings) {
  var calEvents = $('#templateEventsCalendar').fullCalendar('clientEvents');
  $.each(bookings, function(key, value) {
    for (var bookingIdx = 0; bookingIdx < value.length; bookingIdx++) {
      booking = value[bookingIdx];
      for (var eIdx = 0; eIdx < calEvents.length; eIdx++) {
        var event = calEvents[eIdx];
        if ((event.booking.tmpltDetailId && event.booking.tmpltDetailId == booking.tmpltDetailId) ||
            (event.booking.tripDetailId && event.booking.tripDetailId == booking.tripDetailId)) {
          event.booking = booking;
          event.className = booking.cssClassName + "Cal"; //Redecorating the event
          break;
        }
      }
    }
  });
}

function updateBookingCategoriesBadges(bookings, operation) {
  //Iterating over a map of types to arrays of bookings
  $.each(bookings, function(key, value) {
    for (var bookingIdx = 0; bookingIdx < value.length; bookingIdx++) {
      booking = value[bookingIdx];
      var currBadge = $("#" + booking.rootType.toLowerCase() + "CountBadge");
      var currBadgeVal = parseInt(currBadge.text(), 10);
      switch (operation) {
        case 'COPY':
        case 'ADD':
            currBadgeVal++;
          break;
        case 'DELETE':
            currBadgeVal--;
          break;
      }
      currBadge.text(currBadgeVal);
    }
  });
}

function displayAlert(msg, returnCode, severityLevel) {
  $('#alertMsg').remove();
  var newAlertDiv = $(document.createElement("div"));
  newAlertDiv.attr('id', 'alertMsg');
  newAlertDiv.text(msg);
  $('#alertBox').append(newAlertDiv);
  $('#alertBox').show();
}

function deleteTripCalendarBooking(id) {
  var event = $('#templateEventsCalendar').fullCalendar('clientEvents', id)[0];
  console.log("Calendar Booking Delete: ID:" + event.id + " Booking ID: " + event.booking.tripDetailId);
  persistNewBooking(event, 'DELETE');
  var newEvent =  buildBookingEvent(event.booking);
  $('#templateEventsCalendar').fullCalendar('removeEvents', id);
  if(event.booking.tmpltDetailId) {
    addTemplateBookingToList(newEvent);
  }
}

function initializeTemplateCalendar(tripStartDate, tripId) {
  /* initialize the calendar
   -----------------------------------------------------------------*/
  $('#templateEventsCalendar').data('tripStartDate', tripStartDate);
  $('#templateEventsCalendar').data('tripId', tripId);

  $('#templateEventsCalendar').fullCalendar({
    defaultDate: tripStartDate.clone(),
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    editable: true,
    eventLimit: false,
    weekNumbers: true,
    firstDay: 1, //Monday is the first day of the week
    droppable: true, // this allows things to be dropped onto the calendar !!!
    eventSources : [
      {
        events: function (start, end, timezone, callback) {
          var bookingsUrl = '/trips/bookings/' + tripId + '.json?timeStart=' + start.valueOf() + "&timeEnd=" + end.valueOf();
          $.getJSON(bookingsUrl, function (result) {
            var bookingEvents = [];
            $.each(result.bookings, function (key, value) {
              for (var bookingIdx = 0; bookingIdx < value.length; bookingIdx++) {
                var event = buildBookingEvent(value[bookingIdx], null);
                bookingEvents.push(event);
              }
            });
            callback(bookingEvents);
          });
        },
        color:'#999999'
      }
    ],
    drop: function (date) { // this function is called when something is dropped
      // retrieve the dropped element's stored Event Object
      var originalEventObject = $(this).data('eventObject');

      // we need to copy it, so that multiple events don't have a reference to the same object
      var copiedEventObject = $.extend({}, originalEventObject);

      var dayDifference = getDaysDifference(date, copiedEventObject.start);

      var newStart = date.clone();
      newStart.hours(copiedEventObject.start.hours());
      newStart.minutes(copiedEventObject.start.minutes());

      var newEnd = copiedEventObject.end.clone();
      newEnd.hours(copiedEventObject.end.hours());
      newEnd.minutes(copiedEventObject.end.minutes());
      if (dayDifference > 0) {
        newEnd.subtract(dayDifference, "days");
      } else if (dayDifference < 0) {
        newEnd.add(0 - dayDifference, "days");
      }

      // assign it the date that was reported
      copiedEventObject.start = newStart;
      copiedEventObject.end = newEnd;

      // render the event on the calendar
      // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
      $('#templateEventsCalendar').fullCalendar('renderEvent', copiedEventObject, true);

      persistNewBooking(copiedEventObject, 'COPY');

      $(this).remove();
    },

    eventRender: function(event, element) {
      $(element).prepend('<span class="calendarLabel"><div class="'+ event.booking.cssClassName + ' calendarLabelIcon"></div><span class="calendarLabel">' + event.booking.typeLabel + '</span>' +
                         '<i class="fa fa-times pull-right calendarEventX" onclick="deleteTripCalendarBooking(\''+ event.id +'\');return false;"></i></span>');
    },
    eventDrop: function(event, delta, revertFunc) {
      persistNewBooking(event, 'UPDATE');
    },
    eventClick: function(calEvent, jsEvent, view) {
      getPage(calEvent.booking.detailsEditUrl);return false;
    }
  });
}
