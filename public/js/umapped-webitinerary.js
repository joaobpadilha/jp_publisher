<!-- ajax get -->
var request;

$(document).ready(function () {
    $("#alertBox").hide();

    $('.alert .close').live("click", function (e) {
        $(this).parent().hide();
    });

});

window.onhashchange = function(e) {
    // Do something whenever location hash value changes
    hashChange();
};

function hashChange() {
    //make sure we get the previous page - limitation back button is only 1 level
    var hashUrl = window.location.hash.substring(1);
    if (window.location.hash != "" && hashUrl != "" && hashUrl != currentPage && hashUrl.charAt(0) == "/") {
      getPage(hashUrl);
    }
}


function getPage(urlStr) {
    if (request) {
        request.abort();
    }
    $('#spinner').show();

  <!--google analytics-->
  ga('send', 'pageview', urlStr);

    request = $.ajax({
        type: "GET",
        cache: false,
        url: urlStr,
        dataType: "html"
    });

    // callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR) {
        currentPage = urlStr;
        window.location.hash = urlStr;
        displayPage(response);
    });

    // callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown) {
        // log the error to the console
        if (textStatus != 'abort') {
            $('#alertMsg').text("A System Error has occurred - " + textStatus);
            $('#alertBox').show();
        }
    });

    // callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        $('#spinner').hide();
    });
}

<!-- ajax post with serialized post data -->
function postForm(urlStr, formId) {
    var $form = $("#" + formId);

    if (!$form.valid()) {
        return;
    }

    if (request) {
        request.abort();
    }
    $('#spinner').show();




    // let's select and cache all the fields
    var $inputs = $form.find("input, select, button, textarea");
    // serialize the data in the form
    var serializedData = $form.serialize();
    // let's disable the inputs for the duration of the ajax request
    $inputs.prop("disabled", true);

  //Google Analytics
  var s = serializedData;
  if (serializedData.length > 300) {
    s = serializedData.substring(0,300)
  }
  var gaUrl = urlStr + "/post/?" + s;
  ga('send', 'pageview', gaUrl);

    request = $.ajax({
        type: "POST",
        url: urlStr,
        data: serializedData
    });

    // callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR) {
        console.log(textStatus);
        displayPage(response);

    });

    // callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown) {

        // log the error to the console

        $('#alertMsg').text("A System Error has occurred - " + textStatus);
        $('#alertBox').show();
    });

    // callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        $('#spinner').hide();
        $inputs.prop("disabled", false);

    });
}


<!-- handle ajax HTML response pageHeader pageBody pageFooter-->
function displayPage(html) {
    $('#spinner').hide();
    <!-- hide alert -->
    $("#alertBox").hide();

    <!-- remove existing divs before writing html to DOM    -->
    var alertMsg = html.match(/<--alertMsg-->([\s\S]*?)<--endAlertMsg-->/g);
    if (alertMsg != null) {
        var a = alertMsg[0].replace("<--alertMsg-->", '');
        a = a.replace("<--endAlertMsg-->", '');
        $('#alertMsg').remove();

        $('#alertBox').append(a);
        if (a.length > 2) {
            $("#alertBox").show();
        }
    }

    <!-- remove pageTitle divs before writing html to DOM script not escaped -->
    var pageTitle = html.match(/<--pageTitle-->([\s\S]*?)<--endPageTitle-->/g);
    if (pageTitle != null) {
        var a = pageTitle[0].replace("<--pageTitle-->", "");
        a = a.replace("<--endPageTitle-->", "");
        $("#pageTitle").remove();
        $("#contentTitle").append(a);
    }


    <!-- remove pageHeader existing divs before writing html to DOM    -->
    var pageHeader = html.match(/<--pageHeader-->([\s\S]*?)<--endPageHeader-->/g);
    if (pageHeader != null) {
        var a = pageHeader[0].replace("<--pageHeader-->", "");
        a = a.replace("<--endPageHeader-->", "");
        $("#pageHeader").remove();
        $("#contentHeader").append(a);
    }

    <!-- remove pageBody existing divs before writing html to DOM    -->
    var pageBody = html.match(/<--pageBody-->([\s\S]*?)<--endPageBody-->/g);
    if (pageBody != null) {
        var a = pageBody[0].replace("<--pageBody-->", "");
        a = a.replace("<--endPageBody-->", "");
        $("#pageBody").remove();

        $("#contentBody").append(a);
    }

    <!-- remove existing divs before writing html to DOM    -->
    var pageFooter = html.match(/<--pageFooter-->([\s\S]*?)<--endPageFooter-->/g);
    if (pageFooter != null) {
        var a = pageFooter[0].replace("<--pageFooter-->", "");
        a = a.replace("<--endPageFooter-->", "");
        $("#pageFooter").remove();
        $("#contentFooter").append(a);
    }

    if (alertMsg == null && pageTitle == null && pageHeader == null && pageBody == null && pageFooter == null) {
        var tripUrl = html.match(/<--tripUrl-->([\s\S]*?)<--endTripUrl-->/g);
        if (tripUrl != null) {
            var a = tripUrl[0].replace("<--tripUrl-->", "");
            a = a.replace("<--endTripUrl-->", "");
            if (a != null && a.length > 0) {
                window.location.href = a;

            } else {
                window.location.href = "/home";

            }

        } else  {
            //redirect to login
            window.location.href = "/home";
        }


    }

    $('html, body').animate({scrollTop: 0}, 'slow');
}
