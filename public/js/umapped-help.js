/*
    Help subsystem - manages the display and content of the help messages
    Dependent on intro.js and jstorage.js

    Must be initialized by the index page with a call to initHelp (<userid>);

    DEBUG_MODE - forces the page to be displayed all the time
    DISPLAY_WHATS_NEW = display a popup on the home screen when a user logs in
    DISPLAY_WELCOME - displays a welcome tour on the home screen when a user logs in

    initHelp (<userid>)
    This creates the namespace for all the help state - the namespace is dependent of the userid and the help version
    This allows us to control the help version and also allow us to make sure that the help is displayed on a per user id basis.
    The initialization also takes care to delete any old help keys from local storage (key starts with HELP_)

    You can also control a What's New pop up by setting the DISPLAY_WHATS_NEW variable to true
    The WHATS_NEW is controlled by the APPLICATION_VERSION

     displayHelp ()
     This function is called automatically upon a successful AJAX call from the getPage() function
     It will use the current hash url to determine if a help function need to be displayed.
     A help function will only be displayed if:
     1. there is a correct function to display the help content (i.e an entry in helpFunctionMap)
     2. the help has not been previously displayed for this version and for this user.

     You can use HTML formatting within the pop up - the max width of the pop up is 800px, the min width is 200px
     To link to a dynamic page from within the pop-up, use an onclick... escape all inner quotes with @quot and don't forget to close the popup with introJs().exit()
     javascript:getPage(&quot;/providers/newProvider&quot;);introJs().exit();'

    showHelpCurrentPage()
    This function will display the help associated with the current hash page - if no help is explicitly defined, the default help will be shown.
 */

 var HELP_VERSION = "1.1"
 var HELP_KEY_PREFIX;

 var DISPLAY_WHATS_NEW = 1 ;
 var DISPLAY_WELCOME = 1 ;
 var WELCOME_VERSION = '1.1' ;

 var APPLICATION_VERSION = '1.29.5' ;


 var DEBUG_MODE = 0;

// define the mapping for eadh url for pop up help
var popupHelpFunctionMap = new Array();
popupHelpFunctionMap['#/dashboard/cmpies'] = 'help_adminHome';

//optional - allow the help function to be selectively redisplayed per url
var popupHelpFunctionVersionMap = new Array();

 //define the mapping for each url to each help function
 var helpFunctionMap = new Array();
 helpFunctionMap['#/dashboard/cmpies'] = 'help_adminHome';

helpFunctionMap['#/tours/myTours'] = 'help_about_trip';
helpFunctionMap['#/tours/otherTours'] = 'help_about_trip';
helpFunctionMap['#/tours/tours'] = 'help_about_trip';

helpFunctionMap['#/template/templates'] = 'help_about_template';


helpFunctionMap['#/guide/destination'] = 'help_about_document';
helpFunctionMap['#/providers/providers'] = 'help_about_vendor';
helpFunctionMap['#/sharedTrips/search'] = 'help_shared_trips';

helpFunctionMap['#/cmpy/cmpy'] = 'help_manage_cmpy';
helpFunctionMap['#/user/users'] = 'help_manage_users';
helpFunctionMap['#/cmpy/groups'] = 'help_manage_groups';
helpFunctionMap['#/user/user'] = 'help_user_profile';
helpFunctionMap['#/cmpy/billing'] = 'help_cmpy_usage';

helpFunctionMap['#/template/new'] = 'help_edit_template';
helpFunctionMap['#/template/template'] = 'help_edit_template';
helpFunctionMap['#/template/bookings'] = 'help_template_bookings';
helpFunctionMap['#/template/guides'] = 'help_template_guides';

helpFunctionMap['#/providers/newProvider'] = 'help_edit_vendor';
helpFunctionMap['#/guide/newDestination'] = 'help_edit_document';
helpFunctionMap['#/guide/newDestinationGuide'] = 'help_edit_document';

helpFunctionMap['help_start_publishing'] = 'help_start_publishing';
helpFunctionMap['help_start_collaboration'] = 'help_shared_trips';
helpFunctionMap['help_invite_collaborators'] = 'help_start_collaboration';


helpFunctionMap['#/trips/tripInfo'] = 'help_trip_info';
helpFunctionMap['#/trips/new'] = 'help_trip_info';

helpFunctionMap['#/trips/bookings'] = 'help_trip_booking';
helpFunctionMap['#/trips/newTrip/guides'] = 'help_trip_document';
helpFunctionMap['#/trips/newTrip/preview'] = 'help_trip_preview';
helpFunctionMap['#/tours/newTour/publish'] = 'help_trip_publish';


//optional - allow the help function to be selectively redisplayed per url
 var helpFunctionVersionMap = new Array();
helpFunctionVersionMap['#/dashboard/cmpies'] = '1';

 function initHelp (userId) {
     HELP_KEY_VERSION = "HELP_" + HELP_VERSION;

     HELP_KEY_PREFIX = HELP_KEY_VERSION + "_" + userId + "_";
    //clean up help state
    var allKeys = $.jStorage.index();
    for (var i in allKeys) {
        var key = allKeys[i];

        if (key.indexOf("HELP_") == 0) {
            //found a help key - if it is not the current prefix, delete it
            if (key.indexOf(HELP_KEY_VERSION) == -1) {
                $.jStorage.deleteKey(key);
            }
        }
    }
    var welcomeDisplayed = false;
    //check to see if we need to display a What's new notice
     if (DISPLAY_WELCOME && WELCOME_VERSION != null) {
         var key = HELP_KEY_PREFIX + "_WELCOME_" +  WELCOME_VERSION;
         var displayHelp =  $.jStorage.get(key);

         if (displayHelp == null) {
             $.jStorage.set(key,'true');
             welcomeDisplayed = true;
             help_welcome();

         }
     }

   //display what's new only if we are not displaying a inital welcome
     if (!welcomeDisplayed && DISPLAY_WHATS_NEW && APPLICATION_VERSION != null) {
        var key = HELP_KEY_PREFIX + "_WHATS_NEW_" + APPLICATION_VERSION;
        var displayHelp =  $.jStorage.get(key);
        if (displayHelp == null) {
            $.jStorage.set(key,'true');
            help_whats_new();
        }
    }

 }

 function displayHelp () {
    if (HELP_KEY_PREFIX != null) {
        var url = getStrippedURl();
        var key = HELP_KEY_PREFIX + url;
        var functionName = popupHelpFunctionMap[url];
        var functionNameVer = popupHelpFunctionVersionMap[url];
        if (functionNameVer != null) {
            key = key + "_" + functionNameVer;
        }
        var displayHelp =  $.jStorage.get(key);
        if (DEBUG_MODE) {
            displayHelp = null;
        }

        if (displayHelp == null && functionName != null && window[functionName] != undefined) {
            window[functionName]();
            $.jStorage.set(key,'true');
        }
    }
 }

 function showHelpCurrentPage () {
     var url = getStrippedURl();

     showHelp(url);
  }

 function showHelp (helpKey) {
    var functionName = helpFunctionMap[helpKey];
    if (functionName != null) {
        window[functionName]();
    } else {
        help_default();
    }
 }

function getStrippedURl() {
    var url = window.location.hash;
    if(url.indexOf("?") > 0) {
        var u = url.substring(0, url.indexOf("?"));
        return u;
    } else if (url.indexOf("trips/bookings/") > 0) {
        return "#/trips/bookings";
    } else if (url.indexOf("trips/newTrip/guides") > 0) {
        return"#/trips/newTrip/guides";
    }

    return url;
}

// each function is responsible for defining the steps and content of the pop up menu
function help_whats_new () {
  var intro = introJs();
  intro.setOptions({
    steps: [
      {
        intro:  "<div class='help_window'>  " +
                "     <div class='help_title'>Privacy Policy Update</div>" +
                "     <div class='help_body'> " +
                "    We use cookies to improve your experience. For more information, please <a href='http://umapped.com/privacy-policy/' target='_blank'>click here</a>.<br/><br/>" +
                "   By continuing to browse the site, you agree to our use of cookies. If you do not agree, please contact <a href='emailto:support@umapped.com' target='_blank'>support@umapped.com</a>. We have recently updated our <a href='http://umapped.com/privacy-policy/' target='_blank'>Privacy Policy</a> and <a href='http://umapped.com/terms-of-service/' target='_blank'>Terms of Use</a>." +
                "     </div> " +
                "   </div>"
      }
    ],
    showStepNumbers: false
  });
  intro.start();

}


function help_welcome() {
  var intro = introJs();
  intro.setOptions({
            steps: [
              {
                element: '#mainMenuTrip',
                intro:  "<div class='help_window'>  " +
                  "     <div class='help_title'>Trips</div>" +
                  "     <div class='help_body'> " +
                  "         This is where  you  will find all of your <span class='help_strong'> PENDING </span> (in-progress) and <span class='help_strong'> PUBLISHED </span> (sent to travellers) trips. . [Depending on your access level, you may also see trips created by other users in your company.] <br><br> " +
                  "         You can create a new trip or edit an existing trip at anytime.  <br> <br>" +
                  "         You may also see <span class='help_strong'>SHARED TRIPS</span> if other companies have invited you to collaborate. You can add additional bookings and documents to shared trips.<br/>" +
                  "     </div> " +
                  "   </div>"
              },
              {
                element: '#mainMenuTripTemplate',
                  intro:  "<div class='help_sm_window'>  " +
                      "     <div class='help_sm_title'>Templates</div>" +
                      "     <div class='help_body_c'> " +
                      "          Create templates for itineraries that you may want to reuse in the future.  These templates can be easily imported into new trips, saving you valuable time!" +
                      "     </div> " +
                      "   </div>"              },
              {
                element: '#mainMenuPoi',
                  intro:  "<div class='help_sm_window'>  " +
                      "     <div class='help_sm_title'>Vendors</div>" +
                      "     <div class='help_body_c'> " +
                      "          Manage your company’s database of hotels, activities and transportation vendors. You can add photos, contact information and descriptions for each vendor.<br><br> " +
                      "       When you add Bookings to a Trip for one of your vendors, their details will automatically appear.<br/> <br/>Contact us at <a href='mailto:support@umapped.com'> support@umapped.com </a> if you would like help importing your vendor database." +
                      "     </div> " +
                      "   </div>"              },
              {
                element: '#mainMenuDocument',
                  intro:  "<div class='help_sm_window'>  " +
                      "     <div class='help_sm_title'>Documents</div>" +
                      "     <div class='help_body_c'> " +
                      "          Manage your company’s content database, which may include day-by-day tour descriptions, destination guides and recommended places within a city.  <br><br> " +
                      "          Contact us at <a href='mailto:support@umapped.com'> support@umapped.com </a> if you would like help importing your existing content." +

                      "     </div> " +
                      "   </div>",
                  position:"left"              },
                {
                    element: '#mainMenuAdmin',
                    intro:  "<div class='help_sm_window'>  " +
                        "     <div class='help_sm_title'>Company</div>" +
                        "     <div class='help_body_c'> " +
                        "          Manage your company’s contact information, and authorized users. <br><br> " +
                        "       This is also where you can add your logo and find your company’s email API address. " +
                        "     </div> " +
                        "   </div>" ,
                    position:"left"},
                {
                    element: '#mainMenuSupport',
                    intro:  "<div class='help_sm_window'>  " +
                    "     <div class='help_sm_title'>Need Help?</div>" +
                    "     <div class='help_body_c'> " +
                    "          Be sure to check our support website at <a href='http://support.umapped.com' target='_blank'> http://support.umapped.com</a><br/> Or contact us at 1-800-975-6713 or <a href='mailto:support@umapped.com'> support@umapped.com" +
                    "     </div> " +
                    "   </div>",
                    position:"left"
                },
                {
                    element: '#mainMenuProfile',
                    intro:  "<div class='help_sm_window'>  " +
                        "     <div class='help_sm_title'>Profile</div>" +
                        "     <div class='help_body_c'> " +
                        "          Manage your user profile, including your contact information and profile photo. You can also change your password. Your contact information will appear when you publish a Trip. " +
                        "     </div> " +
                        "   </div>" ,
                    position:"left"}

            ]
          });
          intro.start();
}


//help_start_publishing
function help_start_publishing() {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                element: '#mainMenuTrip',
                intro:  "<div class='help_window'>  " +
                    "     <div class='help_title'>Trips</div>" +
                    "     <div class='help_body'> " +
                    "         This is where  you  will find all of your <span class='help_strong'> PENDING </span> (in-progress) and <span class='help_strong'> PUBLISHED </span> (sent to travellers) trips. . [Depending on your access level, you may also see trips created by other users in your company.] <br><br> " +
                    "         You can create a new trip or edit an existing trip at anytime.  <br> <br>" +
                    "         You may also see <span class='help_strong'>SHARED TRIPS</span> if other companies have invited you to collaborate. You can add additional bookings and documents to shared trips.<br/>" +
                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}

//default help screen
function help_default () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>Need Help?</div>" +
                    "     <div class='help_body_center' >" +
                    "          Be sure to check our support website at <a href='http://support.umapped.com' target='_blank'> http://support.umapped.com</a><br/> Or contact us at 1-800-975-6713 or <a href='mailto:support@umapped.com'> support@umapped.com </a>   " +
                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}

function help_about_trip () {
        var intro = introJs();
        intro.setOptions({
                  steps: [
                    {
                      intro: "<div class='help_window'>  " +
                          "     <div class='help_title'>My Trips</div>" +
                          "     <div class='help_body'> " +
                          "         Search for trips using the trip name or by the user who created the trip. The trip status may be <span class='help_strong'>PUBLISHED </span>(interactive copy sent) or <span class='help_strong'>PENDING </span>(being worked on or a read-only version for sent for review). <br><br> " +
                          "         Depending on your access level, you may also see trips created by other users in your company. " +
                          "     </div> " +
                          "   </div>"

                    }
                  ],
                    showStepNumbers: false
                });
                intro.start();
}

function help_about_template () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>My Templates</div>" +
                    "     <div class='help_body'> " +
                    "         Create templates for itineraries that you may want to reuse in the future.  The Bookings you enter will not include specific dates, so that you can easily import them for new trips, saving you valuable time!<br><br> " +
                    "         Click <span class='help_strong'>+ NEW TEMPLATE</span> (top right) to create a new template; click <span class='help_strong'>EDIT</span> to view details or make changes to an existing template. <br><br>  " +
                    "         <span class='help_strong'>PRIVATE</span> templates are only visible to you. <span class='help_strong'>PUBLIC</span> templates that are available anyone in your company" +
                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}

function help_about_document () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>My Documents</div>" +
                    "     <div class='help_body'> " +
                    "          Your documents contain your company’s content database (e.g. day-by-day tour descriptions, destination guides and recommended places within a city).  <br><br> " +
                    "          You can attach documents to any Trip or use your content to quickly package a custom document. You can also upload an existing word and PDF doc under 3 MB or email it to your company Email API (found under your company profile).  <br><br>  " +

                    "          Click <span class='help_strong'>+NEW DOCUMENT</span> (top right) to create a new Vendor; click <span class='help_strong'>EDIT</span> to view details or make changes to an existing Vendor.  <br><br>                   "    +
                    "          Contact us at <a href='mailto:support@umapped.com'> support@umapped.com </a> if you would like help importing your existing content. <br/><br>" +
                    "           NOTE: Documents can only be managed by company administrators and allow all your users to work from the same content database." +
                    "   </div>"

            }
        ],
        showStepNumbers: false
    });

    intro.start();

}

function help_about_vendor () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>My Vendors</div>" +
                    "     <div class='help_body'> " +
                    "           Vendors relate to the Bookings for your trips. When you add Bookings to a Trip for one of your vendors, their details (photos, contact information and descriptions) will automatically appear. <br/><br/>"  +
                    "           Click <span class='help_strong'>+ New Vendor</span> (top right) to create a new Vendor; click EDIT to view details or make changes to an existing Vendor. <br/><br/>  " +
                    "           UMapped provides you with a default database of vendors (search the Company named 'Default' to search this database). <br/> <br/>  " +
                    "           Contact us at <a href='mailto:support@umapped.com'> support@umapped.com </a> if you would like to import your vendor database." +
                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}


function help_shared_trips () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>Shared Trips</div>" +
                    "     <div class='help_body'> " +
                    "         UMapped enables collaboration between travel professionals. <br><br> You will only see <span class='help_strong'>SHARED TRIPS</span> if other companies have invited you to collaborate. <br>You can add additional bookings and documents to shared trips. <br><br> " +
                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}

function help_manage_cmpy () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>Company</div>" +
                    "     <div class='help_body'> " +
                    "         Manage your company’s contact information, and authorized users.  <br/> " +
                    "         This is also where you can add your logo and find your secure email API address. " +
                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}

function help_manage_users () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>Authorized Users</div>" +
                    "     <div class='help_body'> " +
                    "         Manage the users that have access to your company.<br> " +
                    "         Acees Levels: <br>" +
                    "         - <span class='help_strong'> FULL ACCESS</span> allows the user to administer the company<br>" +
                    "         - <span class='help_strong'> PERSONAL ACCESS</span> allows this user to create trips and access her own trips<br><br>" +
                    "         NOTE: When you can create a new user, an email will automatically be sent to the new user with instructions to complete the account setup. " +
                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}

function help_manage_groups () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>Groups</div>" +
                    "     <div class='help_body'> " +
                    "         Groups allow you to allow selected users within your company to collaborate and have access to each others trips.<br> " +
                    "         All members of a group will have full access to each other's trips. <br><br>" +
                    "         For example, groups can be used to allow all users in one office to have access to each others's trips. <br><br>" +
                    "         You can create multiple groups and a user can belong to mulitple groups.  " +
                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}


function help_user_profile () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>My Profile</div>" +
                    "     <div class='help_body'> " +
                    "        Manage your user profile, including your contact information and profile photo. You can also change your password." +
                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}


function help_cmpy_usage () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>Usage Report</div>" +
                    "     <div class='help_body'> " +
                    "         View your company's usage reports for any selected period. You can view detailed activity for  <span class='help_strong'>NEWLY PUBLISHED TRIPS</span>  (published during this time period) or <span class='help_strong'>EXISTING TRIPS</span> (published during a previous period)." +
                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}

function help_edit_template () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>New Templates</div>" +
                    "     <div class='help_body'> " +
                    "         Creating a new template is a 3-step process.<br> <br>" +
                    "         <span class='help_strong'>STEP 1:</span><br>  Give your template a name you will remember, so you can easily search for it.  Choose whether to make your template private (accessible for you & your company administrators) or public (accessible to all users in your company)." +
                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}

function help_template_bookings () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>Template Bookings</div>" +
                    "     <div class='help_body'> " +
                    "         <span class='help_strong'>STEP 2:</span><br> Add any combination of flights, hotels, transportation and activities to your template." +
                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}

function help_template_guides () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>Template Documents</div>" +
                    "     <div class='help_body'> " +
                    "         <span class='help_strong'>STEP 3:</span><br>  Add document(s) from your database to your template." +

                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}

function help_edit_vendor () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>New Vendors</div>" +
                    "     <div class='help_body'> " +
                    "         Your company’s vendors are only visible and accessible to users in your company. When you add Bookings to a Trip for one of your vendors, their details (photos, contact information and descriptions) will automatically appear. Once you save a vendor, you will be able to add photos (use our image library to search, import or upload photos).  <br><br>" +
                    "         <span class='help_strong'> VENDOR INFORMATION: </span><br>" +
                    "         Enter a vendor name, type (hotel, activity, transportation, airline, airport) & any tags (which can be useful for vendor searches).<br>Note: Most commercial airports and airlines are in the default database. Airports require the 3-letter airport code; Airlines require the 2-letter airline code. <br><br>" +
                    "         <span class='help_strong'> ADDRESS  INFORMATION:</span> <br>" +
                    "         Enter the address information (GPS coordinates may automatically display or you can enter them in decimal format e.g. 14.123)  <br><br>" +
                    "         <span class='help_strong'> CONTACT  INFORMATION: </span><br>" +
                    "         Enter the contact information, including a description about the vendor." +
                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}

function help_edit_document () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>New Document</div>" +
                    "     <div class='help_body'> " +
                    "         Within a trip, you will be able to attach an entire document or select pages from your documents to create a custom trip document.  <br><br>" +
                    "         Start by including general document information about your document  (title, type, tags).  <br><br>" +
                    "         Click Add New Page to get started. Each Page can  can include a name, location, description, photos, links and video links (youtube). You can drag and drop to re-order pages. <br><br>" +
                    "         We recommend adding a cover photo to your document as well as photos for each page (search from our image library to import or upload photos)!" +
                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}

function help_edit_page () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>About Editing Pages</div>" +
                    "     <div class='help_body'> " +
                    "         A page include detail information about a place or activity, including photos and videos. You can also recommend a page - the page will show up as a recommendation to your travelers using the mobile apps.<br> <br>" +
                    "         <span class='help_strong'> Page Information: </span><br>" +
                    "         - <span class='help_strong'> Title </span>: the title of the page that will appear to your users<br>" +
                    "         - <span class='help_strong'> Tag </span>: comma separated keywords you can use to classify this page  <br>" +
                    "         - <span class='help_strong'> Recommendation Type </span>: you can classify this pages as a recommendation (Hotel, Food & Drink, Sights, Goings On, Shopping, Other) -  the page will show up as a recommendation to your travelers using the mobile apps  <br>" +
                    "         - <span class='help_strong'> Location </span>: you can search for an address using the Landmark field or you can enter the GPS coordinates (in decimal format e.g. 14.123) <br>" +
                    "         - <span class='help_strong'> Description </span>: you can enter a detailed description <br><br>" +
                    "         <span class='help_strong'> Copy This Page : </span><br>" +
                    "         - You can copy this page and add it to an exsiting document.<br><br>" +
                    "         <span class='help_strong'> Add a Photo:</span> <br>" +
                    "         - You can use our image library to search and import or upload photos to add photos for this page. You can add captions to any photos you add.<br/><br/>" +
                    "         <span class='help_strong'> Add a Video: </span><br>" +
                    "         - You an add the link to a YouTube video.<br><br>" +
                    "         <span class='help_strong'> Add a Link: </span><br>" +
                    "         - You can add links to related web pages.<br><br>" +
                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}

function help_trip_info () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                element: '#uploadBtn',
                intro:  "<div class='help_sm_window'>  " +
                "     <div class='help_sm_title'>Upload</div>" +
                "     <div class='help_body_sm_c'> " +
                "     <a href='https://umapped.freshdesk.com/solution/articles/4000063115' target='_blank'> How do I add a trip cover photo?</a> " +
                "     </div> " +
                "   </div>",
                position:"left"
            },
            {
                element: '#manageTripBtn',
                intro:  "<div class='help_sm_window'>  " +
                "     <div class='help_sm_title'>Manage Trip</div>" +
                "     <div class='help_body_sm_c'> " +
                "     <a href='https://umapped.freshdesk.com/solution/articles/4000098006-manage-trips' target='_blank'> How do I copy, merge or save this trip into a template?</a> " +
                "     </div> " +
                "   </div>",
                position:"left"
            },
            {
                element: '#inviteButton',
                intro:  "<div class='help_sm_window'>  " +
                "     <div class='help_sm_title'>Add Collaborator</div>" +
                "     <div class='help_body_sm_c'> " +
                "     <a href='https://umapped.freshdesk.com/solution/articles/4000062489-how-do-i-add-a-trip-collaborator-' target='_blank'> How do I invite travel professionals to collaborate on this trip?</a> " +
                "     </div> " +
                "   </div>",
                position:"top"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}

function help_trip_booking () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                element: '#importBookingsBtn',
                intro:  "<div class='help_sm_window'>  " +
                "     <div class='help_sm_title'>Import Bookings</div>" +
                "     <div class='help_body_sm_c'> " +
                "     <a href='https://umapped.freshdesk.com/solution/articles/4000076276-import-bookings' target='_blank'> How do I import from Sabre, ClientBase, emails & other systems?</a> " +
                "     </div> " +
                "   </div>"
            },
            {
                element: '#addBookingsBtn',
                intro:  "<div class='help_sm_window'>  " +
                "     <div class='help_sm_title'>Add Bookings</div>" +
                "     <div class='help_body_sm_c'> " +
                "     <a href='https://umapped.freshdesk.com/solution/articles/4000074633-how-can-i-add-bookings-myself-' target='_blank'> How do I add proposed and confirmed bookings?</a> " +
                "     </div> " +
                "   </div>"
            },
            {
                element: '#tourOperatorsBtn',
                intro:  "<div class='help_sm_window'>  " +
                "     <div class='help_sm_title'>Tour Operators</div>" +
                "     <div class='help_body_sm_c'> " +
                "     <a href='https://umapped.freshdesk.com/solution/articles/4000076278-tour-operators' target='_blank'> How do I import bookings and content from Tour Operators </a> " +
                "     </div> " +
                "   </div>"
            },
            {
                element: '#addContentBtn',
                intro:  "<div class='help_sm_window'>  " +
                "     <div class='help_sm_title'>Add Content</div>" +
                "     <div class='help_body_sm_c'> " +
                "     <a href='https://umapped.freshdesk.com/solution/articles/4000054451-what-is-add-content-for-in-step-2-itinerary-' target='_blank'> How can I add new or saved content for this trip?<br/>(e.g. notes, quotes, recommendations, photos & travel42)  </a> " +
                "     </div> " +
                "   </div>",
                position:"left"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}

function help_trip_document () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                element: '#createCustomDocBtn',
                intro:  "<div class='help_sm_window'>  " +
                "     <div class='help_sm_title'>Create a Custom Document</div>" +
                "     <div class='help_body_sm_c'> " +
                "     <a href='https://umapped.freshdesk.com/solution/articles/4000031598' target='_blank'>  How do I create a custom Document for this itinerary?</a> " +
                "     </div> " +
                "   </div>"
            },
            {
                element: '#addDocumentBtn',
                intro:  "<div class='help_sm_window'>  " +
                "     <div class='help_sm_title'>Add a Document</div>" +
                "     <div class='help_body_sm_c'> " +
                "     <a href='https://umapped.freshdesk.com/solution/articles/4000060448' target='_blank'> How do I create Documents to save content in my library?<br/> (e.g. notes, quotes, recommendations, photos & travel42)</a> " +
                "     </div> " +
                "   </div>",
                position:"top"
            },
            {
                element: '#attachFileBtn',
                intro:  "<div class='help_sm_window'>  " +
                "     <div class='help_sm_title'>Attach Word or PDF</div>" +
                "     <div class='help_body_sm_c'> " +
                "     <a href='https://umapped.freshdesk.com/solution/articles/4000062008-how-do-i-attach-documents-e-tickets-vouchers-to-my-client-s-itinerary-' target='_blank'> How do I upload supporting PDF and Word files? </a> " +
                "     </div> " +
                "   </div>",
                position:"top"
            }

        ],
        showStepNumbers: false
    });
    intro.start();
}


function help_trip_preview () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>Preview</div>" +
                    "     <div class='help_body'> " +
                    "         Review all the bookings as well as any documents and files attached to this trip." +
                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}


function help_trip_publish () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                element: '#publishTripBtn',
                intro:  "<div class='help_sm_window'>  " +
                "     <div class='help_sm_title'>Publish</div>" +
                "     <div class='help_body_sm_c'> " +
                "     <a href='https://umapped.freshdesk.com/solution/articles/4000098197-what-happens-when-i-publish-my-trip-' target='_blank'> What happens when I publish my trip (Pending & Published trip status)?</a> " +
                "     </div> " +
                "   </div>",
                position:"left"
            },
            {
                element: '#addTravelerBtn',
                intro:  "<div class='help_sm_window'>  " +
                "     <div class='help_sm_title'>Add Traveler</div>" +
                "     <div class='help_body_sm_c'> " +
                "     <a href='https://umapped.freshdesk.com/solution/articles/4000040897-10-sending-itineraries-to-travelers' target='_blank'> How do I send the trip to my clients?</a> " +
                "     </div> " +
                "   </div>",
                position:"left"
            },
            {
                element: '#shareBtn',
                intro:  "<div class='help_sm_window'>  " +
                "     <div class='help_sm_title'>Share With Professionals</div>" +
                "     <div class='help_body_sm_c'> " +
                "     <a href='https://umapped.freshdesk.com/solution/articles/4000092094-sending-to-travel-agents' target='_blank'> How do I invite other travel professionals to collaborate on this trip?</a> " +
                "     </div> " +
                "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}


function help_start_collaboration () {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                intro: "<div class='help_window'>  " +
                    "     <div class='help_title'>Collaborate</div>" +
                    "     <div class='help_body'> " +
                    "         Click on <span class='help_strong'>+ Collaborator </span> to invite more travel professionals to collaborate with you on this trip. You can select different access levels for each collaborator.  <br><br>" +
                    "         If the travel professional is not already a member of UMapped, an invitation to join UMapped will be sent to them.<br>" +
                    "         There are no extra charges to you or the collaborators. " +
                    "     </div> " +
                    "   </div>"
            }
        ],
        showStepNumbers: false
    });
    intro.start();
}
