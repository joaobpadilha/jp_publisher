/**
 *MASONRY
 */

$(document).ready(function(){
    var gutter = 40;
    var min_width = 300;
    var max_width = 800;
    var $container = $('.container').masonry({
                                                itemSelector : '.box',
                                                gutterWidth: gutter,
                                                isAnimated: true,
                                                  columnWidth: function( containerWidth ) {
                                                    var box_width = (((containerWidth - 2*gutter)/3) | 0) ;

                                                    if (box_width < min_width) {
                                                        box_width = (((containerWidth - gutter)/2) | 0);
                                                    }

                                                    if (box_width < min_width) {
                                                        box_width = containerWidth;
                                                    }

                                                    if (box_width > max_width) {
                                                        box_width = max_width;
                                                    }

                                                    $('.box').width(box_width);

                                                    return box_width;
                                                  } ,
                                                  layoutPriorities: {
                                                      // Masonry default: Try to occupy highest available position.
                                                      // Weight: Pixels of vertical distance from most upper spot.
                                                      // Shelf order: Try to display bricks in original order.
                                                      //   (increases ordered-ness, decreases space-efficiency)
                                                      // Weight: Pixels of distance of current brick's top left corner
                                                      //         from previous brick's top right corner.
                                                      upperPosition: 100,
                                                      shelfOrder: 100
                                                    }
                                            });;



    $container.imagesLoaded( function(){
        $container.masonry({
            itemSelector : '.box',
            gutterWidth: gutter,
            isAnimated: true,
              columnWidth: function( containerWidth ) {
                var box_width = (((containerWidth - 2*gutter)/3) | 0) ;

                if (box_width < min_width) {
                    box_width = (((containerWidth - gutter)/2) | 0);
                }

                if (box_width < min_width) {
                    box_width = containerWidth;
                }

                if (box_width > max_width) {
                    box_width = max_width;
                }

                $('.box').width(box_width);

                return box_width;
              },
              layoutPriorities: {
                  // Masonry default: Try to occupy highest available position.
                  // Weight: Pixels of vertical distance from most upper spot.
                  // Shelf order: Try to display bricks in original order.
                  //   (increases ordered-ness, decreases space-efficiency)
                  // Weight: Pixels of distance of current brick's top left corner
                  //         from previous brick's top right corner.
                  upperPosition: 100,
                  shelfOrder: 100
                }
        });
    });


});

// Slide down menu
$(document).ready(function(){
  $("#menu-btn").click(function(){
    $("nav").slideDown("fast");
  });
});
// Close menu
$(document).ready(function(){
  $("#close-btn").click(function(){
    $("nav").slideUp("fast");
  });
});



