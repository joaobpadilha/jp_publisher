#!/bin/sh

HOST_IP=localhost
PGSQL_USER=vagrant
PGSQL_PSWD=pwd
PGSQL_PUB_DB_NAME="publisher"
PGSQL_WEB_DB_NAME="dfu1sjn695emk7"

DATABASE_USER=$PGSQL_USER
DATABASE_PASS=$PGSQL_PSWD

DATABASE_WEB_USER=$PGSQL_USER
DATABASE_WEB_PASS=$PGSQL_PSWD

###############################################################################
# Env variables below should not be modified                                  #
###############################################################################

#Publisher Database
DATABASE_PUB_URL="jdbc:postgresql://$HOST_IP/$PGSQL_PUB_DB_NAME"

#Mobile Database
DATABASE_WEB_URL="jdbc:postgresql://$HOST_IP/$PGSQL_WEB_DB_NAME"

FIVE_FILTERS="http://ec2-54-165-231-255.compute-1.amazonaws.com/full-text-rss/"

#MEMCACHE Configruation
MEMCACHIER_SERVERS="$HOST_IP:12000"
MEMCACHIER_USERNAME=""
MEMCACHIER_PASSWORD=""

#AWS S3 Test Bucket
S3_BUCKET="umapped-test"
S3_ACCESS_KEY="AKIAIQAZDML3PT72QMSQ"
S3_SECRET="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"

#URL used for links
HOST_URL="http://localhost:9000"

#STMP INFO - please set the SMTP host to sendgrid for live email
#SMTP_HOST="localhost"
SMTP_HOST="smtp.sendgrid.net"
SMTP_USER_ID="george_sg"
SMTP_PASSWORD="W1nter11"
API_URL=http://localhost:9000/
EMAIL_ENABLED="y"

#AMAZON SQS Test Queue for email API processing
SQS_Q_URL=https://sqs.us-east-1.amazonaws.com/733704146939/u_workq_1_test

export FIREBASE_URI="umapped-george.firebaseio.com"
export FIREBASE_SECRET="oRUQx8NLAawYMCfevGet9NyMlqDpxEYfqToe6N95"
export ENV="DEV"

## Instead of sending email to worldmate - emails will be forwarded to:
export WORLDMATE_ADDR="support@umapped.com"

#Freshbooks Parameters (disabled for security)
export FRESHBOOKS_URL="https://umappedinc.freshbooks.com/api/2.1/xml-in"
export FRESHBOOKS_TOKEN=""

export REDIS_URL="localhost"
export EMAIL_COMMUNICATOR_HOST="talkthierry.umapped.com"
export SMS_CLICKATELL_APIID="3586000"
export SMS_CLICKATELL_TOKEN="aKcKtlry3mDGViQPfOcMA3jQX.uclgEISLUwn7OGC.eSIHnuSdEhQtLlJrg6otV"
export SMS_CALLBACK_USER="serguei"
export SMS_CALLBACK_PASS="]Any44Y492TT@zF4wCn+9kDVn.UxiYCM6N@./HLs4;C];9qTCx"

export JAVA_HOME
export SMTP_HOST
export SMTP_USER_ID
export SMTP_PASSWORD
export DATABASE_WEB_URL
export DATABASE_URL
export MEMCACHIER_PASSWORD
export MEMCACHIER_SERVERS
export S3_BUCKET
export S3_ACCESS_KEY
export S3_SECRET
export HOST_URL
export SQS_Q_URL
export FIVE_FILTERS
export DATABASE_PUB_URL
export DATABASE_USER
export DATABASE_WEB_USER
export DATABASE_PASS
export DATABASE_WEB_PASS


alias clean="/Users/george/Dev/activator-dist-1.3.7/activator clean"
alias cleanfiles="/Users/george/Dev/activator-dist-1.3.7/activator clean-files"
alias compile="/Users/george/Dev/activator-dist-1.3.7/activator compile"
alias debug="/Users/george/Dev/activator-dist-1.3.7/activator -Duser.timezone=GMT -jvm-debug 9998 -Dhttps.port=9443 -Dhttps.keyStore=`pwd`/../cert.umdev/server.keystore -Dhttps.keyStorePassword=123456 run"
