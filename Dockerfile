FROM ubuntu:14.04.2

MAINTAINER Serguei Moutovkin <serguei@umapped.com>

ENV HOME /root
WORKDIR /root

#Making apt-get not to ask interactive questions
ENV DEBIAN_FRONTEND noninteractive
ENV JAVA_VERSION 8

# Installing needed packages, Java and then followed by cleanup
RUN apt-get update && \
    apt-get install -y software-properties-common unzip wget && \
    add-apt-repository ppa:webupd8team/java && \
    echo oracle-java$JAVA_VERSION-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections && \
    echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections && \
    apt-get update && \
    apt-get install -y oracle-java$JAVA_VERSION-installer && \
    update-java-alternatives -s java-$JAVA_VERSION-oracle && \
    apt-get clean && \
    apt-get purge && \
    echo "JAVA_HOME=/usr/lib/jvm/java-$JAVA_VERSION-oracle" >> /etc/environment && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Java location
ENV JAVA_HOME /usr/lib/jvm/java-$JAVA_VERSION-oracle

# Play Activator Download
ENV ACTIVATOR_VERSION 1.3.2
RUN wget http://downloads.typesafe.com/typesafe-activator/$ACTIVATOR_VERSION/typesafe-activator-$ACTIVATOR_VERSION.zip && \
    unzip typesafe-activator-$ACTIVATOR_VERSION.zip && \
    rm -f typesafe-activator-$ACTIVATOR_VERSION.zip && \
    mv activator-$ACTIVATOR_VERSION /opt/activator

COPY . /tmp/publisher
WORKDIR /tmp/publisher
RUN /opt/activator/activator dist && \
    unzip target/universal/publisher-1.0-SNAPSHOT.zip && \
    mv publisher-1.0-SNAPSHOT /root && \
    rm -rf /tmp/* /var/tmp/*

ENTRYPOINT /root/publisher-1.0-SNAPSHOT/bin/publisher
EXPOSE 9000
