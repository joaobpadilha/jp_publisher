package controllers;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.Status;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.ExpressionList;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.AdminAccess;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.audit.AuditActionType;
import com.mapped.publisher.audit.AuditActorType;
import com.mapped.publisher.audit.AuditModuleType;
import com.mapped.publisher.audit.event.AuditTripDocAttachment;
import com.mapped.publisher.audit.event.AuditTripDocCustom;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.FileForm;
import com.mapped.publisher.form.ImageForm;
import com.mapped.publisher.js.BaseJsonResponse;
import com.mapped.publisher.persistence.*;
import com.mapped.publisher.persistence.redis.RedisKeys;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.utils.*;
import com.mapped.publisher.view.*;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.external.bing.image.BingImage;
import com.umapped.external.bing.image.Value;
import models.publisher.*;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import play.data.Form;
import play.data.FormFactory;
import play.data.validation.Constraints;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static com.mapped.publisher.persistence.RecordState.PENDING;
import static com.mapped.publisher.persistence.redis.RedisKeys.*;
import static com.mapped.publisher.utils.S3Util.BUCKET_NAME;
import static models.publisher.FileImageMigrate.ImageUser.*;

/**
 * Created by george on 2017-02-14.
 */
public class FileController extends Controller {

  private static final boolean DEBUG = false;

  @Inject
  private RedisMgr redis;

  private HttpExecutionContext ec;

  @Inject
  FormFactory formFactory;

  @Inject
  public FileController(HttpExecutionContext ec) {
    this.ec = ec;
  }


  @With({Authenticated.class, Authorized.class})
  public CompletionStage<Result> getFiles() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Credentials cred = sessionMgr.getCredentials();

    //bind html form to form bean
    Form<FileForm> fileForm = formFactory.form(FileForm.class);
    final FileForm fileInfo = fileForm.bindFromRequest().get();
    final FileResultsView view = buildView(fileInfo);


    if (fileInfo == null || fileInfo.getInCmpyId() == null ||
        fileInfo.getInCmpyId().trim().length() == 0 || fileInfo.getInKeyword() == null ||
        fileInfo.getInKeyword().trim().length() == 0) {
      view.message = "A System Error has occurred - please try again";
      return CompletableFuture.completedFuture(ok(views.html.guide.fileSearch.render(view)));
    }

    final ArrayList<String> cmpyList = new ArrayList<>();
    cmpyList.add(fileInfo.getInCmpyId());

    if (sessionMgr.getCredentials().isUmappedAdmin()) {
      cmpyList.add(APPConstants.DEFAULT_CMPY_ID);
    }

    if (cmpyList != null && cmpyList.size() > 0) {

      CompletionStage<FileResultsView> promiseSearchResults =  CompletableFuture.supplyAsync(() -> {
        FileResultsView asyncView = buildView(fileInfo);
        try {
          boolean isCmpyAdmin = false;
          if (cred.isCmpyAdmin()) {
            isCmpyAdmin = true;
          }
          List<FileView> umappedResults = searchUMapped(fileInfo.getInKeyword(), cmpyList, sessionMgr.getCredentials().getUserId(), isCmpyAdmin);
          if (umappedResults != null) {
            asyncView.umappedResults = umappedResults;
          }

          //return PDF.toBytes(views.html.external
          // .icbellagio.document.render(destView));
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        return asyncView;
      }, ec.current());


      return promiseSearchResults.thenApplyAsync(fileResultsView -> ok(views.html.guide.fileSearch.render(fileResultsView)));
    }
    else {
      view.message = "UnAuthorized access.";
      return CompletableFuture.completedFuture(ok(views.html.guide.fileSearch.render(view)));
    }
  }

  private static List<FileView> searchUMapped(String keyword, List<String> cmpyIds, String userid, boolean isCmpyAdmin) {
    Connection conn = null;
    List<FileView> results = new ArrayList<>();

    if (keyword != null && keyword.trim().length() > 0) {
      String[] keywords1;

      if (keyword.contains("+")) {
        keywords1 = keyword.trim().toUpperCase().split("\\+");
      }
      else {
        keywords1 = keyword.trim().toUpperCase().split(" ");
      }
      try {
        conn = DBConnectionMgr.getConnection4Publisher();
        HashMap<String, FileView> imgResults = FileMgr.searchFiles(keyword, cmpyIds, userid, conn, isCmpyAdmin);
        if (imgResults != null) {
          for (String fileName : imgResults.keySet()) {
            FileView view = imgResults.get(fileName);
            String origFileName = fileName;

            if (view.title != null && keywords1 != null) {
              boolean found = false;
              for (String k : keywords1) {
                if (view.title.toUpperCase().contains(k)) {
                  results.add(0, view);
                  found = true;
                  break;
                }
              }
              if (!found) {
                results.add(view);

              }
            }
            else {
              results.add(view);
            }
          }
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
      finally {
        if (conn != null) {
          try {
            conn.close();
          }
          catch (SQLException e) {
            e.printStackTrace();
          }
        }
      }
    }
    return results;
  }


  @With({Authenticated.class, Authorized.class})
  public Result copyFileToPage() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<FileForm> fileSearchForm = formFactory.form(FileForm.class);
    FileForm fileSearchInfo = fileSearchForm.bindFromRequest().get();

    FileResultsView view = buildView(fileSearchInfo);


    if (fileSearchInfo == null ||
        fileSearchInfo.getInFileId() == null ||
        fileSearchInfo.getInFileId().length() == 0 ||
        fileSearchInfo.getInPageId() == null ||
        fileSearchInfo.getInPageId().length() == 0 ||
        fileSearchInfo.getInCmpyId() == null ||
        fileSearchInfo.getInCmpyId().trim().length() == 0) {
      //return error msg - search term is mandatory
      view.message = "Error - Please try again.";
      return ok(views.html.guide.fileSearch.render(view));

    }
    else {
      try {
        Ebean.beginTransaction();

        DestinationGuideAttach origFile = DestinationGuideAttach.find.byId(fileSearchInfo.getInFileId());
        DestinationGuideAttach attach = DestinationGuideAttach.buildGuideAttachment(fileSearchInfo.getInPageId(),
            sessionMgr.getUserId());
        if (origFile != null) {
          attach.setFileInfo(origFile.getFileInfo());
          attach.setAttachtype(origFile.getAttachtype());
          //set rank
          int maxRow = DestinationGuideAttach.maxPageRankByDestGuideId(fileSearchInfo.getInPageId(),
              attach.getAttachtypeStr());
          attach.setRank(++maxRow);
          attach.setAttachurl(origFile.getAttachurl());
          attach.setName(origFile.getName());
          attach.setAttachname(origFile.getFile().getFilename());
          attach.setComments(fileSearchInfo.getInFileCaption());
          if (attach.getComments() == null || attach.getComments().length() < 1)
          {
            attach.setComments(origFile.getComments());
          }
          attach.save();
          Ebean.commitTransaction();
          DestinationPostController.invalidateCache(fileSearchInfo.getInDocId());

          String successMsg = "File added successfully";
          flash(SessionConstants.SESSION_PARAM_MSG, successMsg);
          return redirect(routes.DestinationController.newDestinationGuide(fileSearchInfo.getInDocId(),
              fileSearchInfo.getInPageId()));
        }
      }
      catch (Exception e) {
        e.printStackTrace();

        Ebean.rollbackTransaction();
        Log.err("Error saving file. Cmpy: " + fileSearchInfo.getInCmpyId() + " FileId: " + fileSearchInfo.getInFileId(), e);

      }

    }

    BaseView baseView = new BaseView();
    baseView.message = "System Error - please resubmit";
    return ok(views.html.common.message.render(baseView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result copyFileToTrip() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<FileForm> fileSearchForm = formFactory.form(FileForm.class);
    FileForm fileSearchInfo = fileSearchForm.bindFromRequest().get();

    if (fileSearchInfo.getInTripId() == null) {
      return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_MISSING_DATA);
    }

    FileResultsView view = buildView(fileSearchInfo);

    Trip trip = Trip.find.byId(fileSearchInfo.getInTripId());
    if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).lt(SecurityMgr.AccessLevel.APPEND)) {
      BaseView baseView = new BaseView();
      baseView.message = "Error - you have no permission to add files.";
      return ok(views.html.common.message.render(baseView));
    }

    int count = TripAttachment.activeCountByTripId(trip.tripid);
    if (count > 60) {
      BaseView baseView = new BaseView();
      baseView.message = "Error - you have exceeded the maximum number of files.";
      return ok(views.html.common.message.render(baseView));
    }

    UserProfile userProfile = UserProfile.findByPK(sessionMgr.getUserId());


    if (fileSearchInfo == null ||
        fileSearchInfo.getInFileId() == null ||
        fileSearchInfo.getInFileId().length() == 0 ||
        fileSearchInfo.getInTripId() == null ||
        fileSearchInfo.getInTripId().length() == 0 ||
        fileSearchInfo.getInCmpyId() == null ||
        fileSearchInfo.getInCmpyId().trim().length() == 0) {
      //return error msg - search term is mandatory
      view.message = "Error - Please try again.";
      return ok(views.html.guide.fileSearch.render(view));

    }
    else {
      try {
        Ebean.beginTransaction();

        DestinationGuideAttach origFile = DestinationGuideAttach.find.byId(fileSearchInfo.getInFileId());

        if (origFile != null) {
          String id = Utils.getUniqueId();

          String filename = id + "_" + origFile.getFile().getFilename();

          TripAttachment tripAttachment = TripAttachment.buildTripAttachment(userProfile, trip,
              origFile.getName(),
              filename,
              origFile.getAttachurl(),
              fileSearchInfo.getInFileCaption());
          //String nextUrl = "/tours/newTour/uploadSuccess?inTripId=" + trip.tripid + "&inFileId=" + tripAttachment.getPk();
          tripAttachment.setStatus(APPConstants.STATUS_ACTIVE);
          tripAttachment.setLastupdatedtimestamp(System.currentTimeMillis());
          tripAttachment.setModifiedby(sessionMgr.getUserId());
          if (tripAttachment.getDescription() == null || tripAttachment.getDescription().length() < 1)
          {
            tripAttachment.setDescription(origFile.getComments());
          }
          tripAttachment.save();
          Ebean.commitTransaction();

          //Create audit log
          audit:
          {
            TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_DOC_ATTACHMENT,
                AuditActionType.ADD,
                AuditActorType.WEB_USER)
                .withTrip(trip)
                .withCmpyid(trip.cmpyid)
                .withUserid(sessionMgr.getUserId());

            ((AuditTripDocAttachment) ta.getDetails()).withAttachementId(tripAttachment.getPk())
                .withFileName(tripAttachment.getOrigfilename())
                .withSysFileName(tripAttachment.getFilename());
            ta.save();
          }

          String successMsg = "File added successfully";
          flash(SessionConstants.SESSION_PARAM_MSG, successMsg);
          return redirect(routes.TripController.guides(trip.tripid,
              new DestinationSearchView.Tab.Bound(DestinationSearchView.Tab.ATTACHMENTS)));
        }
      }
      catch (Exception e) {
        e.printStackTrace();

        Ebean.rollbackTransaction();
        Log.err("Error saving file. Cmpy: " + fileSearchInfo.getInCmpyId() + " FileId: " + fileSearchInfo.getInFileId(), e);

      }

    }

    BaseView baseView = new BaseView();
    baseView.message = "System Error - please resubmit";
    return ok(views.html.common.message.render(baseView));
  }


  @With({Authenticated.class, Authorized.class})
  public Result copyFileToNote() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<FileForm> fileSearchForm = formFactory.form(FileForm.class);
    FileForm fileSearchInfo = fileSearchForm.bindFromRequest().get();

    if (fileSearchInfo.getInNoteId() == null) {
      return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_MISSING_DATA);
    }

    FileResultsView view = buildView(fileSearchInfo);

    TripNote       tripNote = TripNote.find.byId(fileSearchInfo.getInNoteId());

    if (tripNote == null) {
      //return error msg - search term is mandatory
      view.message = "Error - Trip Note doesn't exist.";
      return ok(views.html.guide.fileSearch.render(view));

    }

    Trip trip = Trip.find.byId(tripNote.getTrip().getTripid());
    if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).lt(SecurityMgr.AccessLevel.APPEND)) {
      BaseView baseView = new BaseView();
      baseView.message = "Error - you have no permission to add files.";
      return ok(views.html.common.message.render(baseView));
    }

    UserProfile userProfile = UserProfile.findByPK(sessionMgr.getUserId());


    if (fileSearchInfo == null ||
        fileSearchInfo.getInFileId() == null ||
        fileSearchInfo.getInFileId().length() == 0 ||
        fileSearchInfo.getInNoteId() == null ||
        fileSearchInfo.getInNoteId() < 0L ||
        fileSearchInfo.getInCmpyId() == null ||
        fileSearchInfo.getInCmpyId().trim().length() == 0) {
      //return error msg - search term is mandatory
      view.message = "Error - Please try again.";
      return ok(views.html.guide.fileSearch.render(view));

    }
    else {
      try {
        Ebean.beginTransaction();

        DestinationGuideAttach origFile = DestinationGuideAttach.find.byId(fileSearchInfo.getInFileId());

        if (origFile != null) {

          TripNoteAttach attach   = TripNoteAttach.build(tripNote, userProfile.getUserid());
          attach.setFileInfo(origFile.getFileInfo());
          attach.setAttachType(PageAttachType.FILE_LINK);
          //set rank
          int maxRow = TripNoteAttach.maxPageRankByNoteId(fileSearchInfo.getInNoteId(), attach.getAttachType().getStrVal());
          attach.setRank(++maxRow);
          attach.setAttachUrl(origFile.getFileInfo().getUrl());
          attach.setName(origFile.name);
          attach.setAttachName(origFile.getFileInfo().getFilename());
          attach.setComments(fileSearchInfo.getInFileCaption());
          attach.save();
          BookingNoteController.invalidateCache(tripNote.getTrip().tripid);

          Ebean.commitTransaction();

          //Create audit log
          audit:
          {
            TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_DOC_ATTACHMENT,
                AuditActionType.ADD,
                AuditActorType.WEB_USER)
                .withTrip(trip)
                .withCmpyid(trip.cmpyid)
                .withUserid(sessionMgr.getUserId());

            ((AuditTripDocAttachment) ta.getDetails()).withAttachementId(String.valueOf(attach.getPk()))
                .withFileName(attach.getName())
                .withSysFileName(attach.getAttachName())
                .withText("Added File to Trip Note");
            ta.save();
          }

          String successMsg = "File added successfully";
          flash(SessionConstants.SESSION_PARAM_MSG, successMsg);
          return redirect(routes.BookingNoteController.getNote(tripNote.getTrip().getTripid(), tripNote.getNoteId(), null, null));
        }
      }
      catch (Exception e) {
        e.printStackTrace();

        Ebean.rollbackTransaction();
        Log.err("Error saving file. Cmpy: " + fileSearchInfo.getInCmpyId() + " FileId: " + fileSearchInfo.getInFileId(), e);

      }

    }

    BaseView baseView = new BaseView();
    baseView.message = "System Error - please resubmit";
    return ok(views.html.common.message.render(baseView));
  }



  private static FileResultsView buildView(FileForm fileInfo) {
    FileResultsView view = new FileResultsView();
    view.searchResults = new HashMap<>();
    view.searchType = ImageLibrary.ImgSrc.UMAPPED;
    view.cmpyId = fileInfo.getInCmpyId();
    view.keyword = fileInfo.getInKeyword();
    view.tripId = fileInfo.getInTripId();
    view.docId = fileInfo.getInDocId();
    view.pageId = fileInfo.getInPageId();
    view.vendorId = fileInfo.getInVendorId();
    view.templateId = fileInfo.getInTemplateId();
    view.tripNoteId = fileInfo.getInNoteId();
    view.multipleFiles = fileInfo.getInMultipleFiles();
    view.inSearchOrigin = fileInfo.getInSearchOrigin();
    return view;
  }

  public static FileInfo uploadHelper(DestinationGuideAttach attach, final String filename, Account account,
                                       ObjectNode on,  String nextUrl) {
    //1. Setting up temporary file record
    FileSrc   src = FileSrc.find.byId(FileSrc.FileSrcType.FILE_USER_UPLOAD);

    FileInfo file = FileInfo.buildPending(account.getUid(), src);

    String      normalized = S3Util.normalizeFilename(filename);
    LstFileType fileType   = LstFileType.findFromFileName(filename);

    file.setBucket(src.getRealBucketName());
    file.setFilepath(DocumentsHelper.getFileKey(file, normalized, account));
    file.setFilename(filename);
    file.setFiletype(fileType);
    file.setAsPublicUrl();

    //2. Now preparing upload authorization response
    try {
      String signedPutUrl = S3Util.authorizeS3Put(file.getBucket(), file.getFilepath());
      S3Data s3Data = S3Util.authorizeS3(file.getBucket(), file.getFilepath());
      on.put("signed_request", signedPutUrl);
      on.put("url", routes.FileController.uploadConfirmation(file.getPk(), "GUIDE").url());
      on.put("policy", s3Data.policy);
      on.put("accessKey", s3Data.accessKey);
      on.put("key", s3Data.key);
      on.put("s3Bucket", s3Data.s3Bucket);
      on.put("signature", s3Data.signature);

    }catch (Exception e) {
      Log.err("FileController::uploadHelper: Failure to authorize upload", e);
      return null;
    }

    file.save();

    attach.setFileInfo(file);
    attach.save();

    //3. Storing URL to which we redirect after image upload is confirmed
    if(nextUrl != null) {
      CacheMgr.getRedis().set(RedisKeys.K_FILE_UPLOAD_REDIRECT, file.getPk().toString(), nextUrl, RedisMgr.WriteMode.BLOCKING);
    }
    CacheMgr.getRedis().set(K_FILE_UPLOAD_MODEL, file.getPk().toString(), attach, RedisMgr.WriteMode.FAST);


    return file;
  }

  public static FileInfo uploadHelper(TripAttachment attach, final String filename, Account account,
                                      ObjectNode on,  String nextUrl) {
    //1. Setting up temporary file record
    FileSrc   src = FileSrc.find.byId(FileSrc.FileSrcType.FILE_USER_UPLOAD);

    FileInfo file = FileInfo.buildPending(account.getUid(), src);

    String      normalized = S3Util.normalizeFilename(filename);
    LstFileType fileType   = LstFileType.findFromFileName(filename);

    file.setBucket(src.getRealBucketName());
    file.setFilepath(DocumentsHelper.getFileKey(file, normalized, account));
    file.setFilename(filename);
    file.setFiletype(fileType);
    file.setAsPublicUrl();

    //2. Now preparing upload authorization response
    try {
      String signedPutUrl = S3Util.authorizeS3Put(file.getBucket(), file.getFilepath());
      S3Data s3Data = S3Util.authorizeS3(file.getBucket(), file.getFilepath());
      on.put("signed_request", signedPutUrl);
      on.put("url", routes.FileController.uploadConfirmation(file.getPk(), "TRIP").url());
      on.put("policy", s3Data.policy);
      on.put("accessKey", s3Data.accessKey);
      on.put("key", s3Data.key);
      on.put("s3Bucket", s3Data.s3Bucket);
      on.put("signature", s3Data.signature);

    }catch (Exception e) {
      Log.err("FileController::uploadHelper: Failure to authorize upload", e);
      return null;
    }

    file.save();

    attach.setFileInfo(file);
    attach.save();

    //3. Storing URL to which we redirect after image upload is confirmed
    if(nextUrl != null) {
      CacheMgr.getRedis().set(RedisKeys.K_FILE_UPLOAD_REDIRECT, file.getPk().toString(), nextUrl, RedisMgr.WriteMode.BLOCKING);
    }
    CacheMgr.getRedis().set(K_FILE_UPLOAD_MODEL, file.getPk().toString(), attach, RedisMgr.WriteMode.FAST);


    return file;
  }


  public static FileInfo uploadHelper(TripNoteAttach attach, final String filename, Account account,
                                      ObjectNode on,  String nextUrl) {
    //1. Setting up temporary file record
    FileSrc   src = FileSrc.find.byId(FileSrc.FileSrcType.FILE_USER_UPLOAD);

    FileInfo file = FileInfo.buildPending(account.getUid(), src);

    String      normalized = S3Util.normalizeFilename(filename);
    LstFileType fileType   = LstFileType.findFromFileName(filename);

    file.setBucket(src.getRealBucketName());
    file.setFilepath(DocumentsHelper.getFileKey(file, normalized, account));
    file.setFilename(filename);
    file.setFiletype(fileType);
    file.setAsPublicUrl();

    //2. Now preparing upload authorization response
    try {
      String signedPutUrl = S3Util.authorizeS3Put(file.getBucket(), file.getFilepath());
      S3Data s3Data = S3Util.authorizeS3(file.getBucket(), file.getFilepath());
      on.put("signed_request", signedPutUrl);
      on.put("url", routes.FileController.uploadConfirmation(file.getPk(), "NOTE").url());
      on.put("policy", s3Data.policy);
      on.put("accessKey", s3Data.accessKey);
      on.put("key", s3Data.key);
      on.put("s3Bucket", s3Data.s3Bucket);
      on.put("signature", s3Data.signature);

    }catch (Exception e) {
      Log.err("FileController::uploadHelper: Failure to authorize upload", e);
      return null;
    }

    file.save();

    attach.setFileInfo(file);
    attach.save();

    //3. Storing URL to which we redirect after image upload is confirmed
    if(nextUrl != null) {
      CacheMgr.getRedis().set(RedisKeys.K_FILE_UPLOAD_REDIRECT, file.getPk().toString(), nextUrl, RedisMgr.WriteMode.BLOCKING);
    }
    CacheMgr.getRedis().set(K_FILE_UPLOAD_MODEL, file.getPk().toString(), attach, RedisMgr.WriteMode.FAST);


    return file;
  }


  @With({Authenticated.class, Authorized.class})
  public Result uploadConfirmation(Long fileId, String source) {
    Optional<String> url = redis.get(RedisKeys.K_FILE_UPLOAD_REDIRECT, fileId.toString());

    if(!url.isPresent()) {
      return UserMessage.message(ReturnCode.LOGICAL_ERROR, "File upload confirmation failure. (No Redirect).");
    }

    if (source.equals("TRIP")) {
      uploadConfirmationTripHelper(fileId);
    } else if (source.equals("NOTE")) {
      uploadConfirmationNoteHelper(fileId);
    } else {
      uploadConfirmationHelper(fileId);
    }
    return redirect(url.get());
  }

  public static boolean uploadConfirmationHelper(Long fileId) {

    boolean result = true;
    RedisMgr redis = CacheMgr.getRedis();
    FileInfo file = FileInfo.find.byId(fileId);
    boolean res = updateFileMetadata(file);
    if(res) {

      //Checking for duplicates right here before saving and if needed replace objects in the model
      FileInfo dupFile = FileInfo.byHash(file.getHash());
      if(dupFile != null && !dupFile.getPk().equals(file.getPk())) {
        try {
          Ebean.beginTransaction();
          //Setting model to use existing image
          Optional<DestinationGuideAttach> attachOp = redis.getBin(K_FILE_UPLOAD_MODEL,
                                                                  fileId.toString(),
                                                                  DestinationGuideAttach.class);
          if (attachOp.isPresent()) {
            DestinationGuideAttach model = attachOp.get();
            model.refresh();
            FileInfo prevFile = FileInfo.find.byId(dupFile.getPk());
            model.setFileInfo(prevFile);
            model.save();
          }

          //Now we can delete newly uploaded image
          S3Util.deleteS3File(file.getBucket(), file.getFilepath());

          //Finally deleting image record

          file.delete();

          Ebean.commitTransaction();
        } catch (Exception e) {
          Ebean.rollbackTransaction();
          Log.err("FileController::uploadConfirmation(): Failure, while deduplicating file", e);
          result = false;
        }
      } else {
        file.save();
      }
      flash(SessionConstants.SESSION_PARAM_MSG, "File uploaded and confirmed successfully");
    } else {
      //We should clear model and file here
      flash(SessionConstants.SESSION_PARAM_MSG, "File upload failed. File was not found the server.");
      //Marking as locked to clearly mark it as failed upload as locked is not used for anything else.
      file.setState(RecordState.LOCKED);
      Optional<DestinationGuideAttach> attachOp = redis.getBin(K_FILE_UPLOAD_MODEL,
                                                                fileId.toString(),
                                                                DestinationGuideAttach.class);
      if(attachOp.isPresent()) {
        DestinationGuideAttach attach = attachOp.get();
        attach.refresh();
        attach.setFileInfo(null);
        attach.save();
      }
      result = false;
      file.save();
    }

    redis.delete(K_FILE_UPLOAD_MODEL, fileId.toString(), RedisMgr.WriteMode.UNSAFE);

    redis.delete(K_FILE_UPLOAD_REDIRECT, fileId.toString(), RedisMgr.WriteMode.UNSAFE);

    return result;
  }

  public static boolean uploadConfirmationTripHelper(Long fileId) {

    boolean result = true;
    RedisMgr redis = CacheMgr.getRedis();
    FileInfo file = FileInfo.find.byId(fileId);
    boolean res = updateFileMetadata(file);
    if(res) {

      //Checking for duplicates right here before saving and if needed replace objects in the model
      FileInfo dupFile = FileInfo.byHash(file.getHash());
      if(dupFile != null && !dupFile.getPk().equals(file.getPk())) {
        try {
          Ebean.beginTransaction();
          //Setting model to use existing image
          Optional<TripAttachment> attachOp = redis.getBin(K_FILE_UPLOAD_MODEL,
              fileId.toString(),
              TripAttachment.class);
          if (attachOp.isPresent()) {
            TripAttachment model = attachOp.get();
            model.refresh();
            FileInfo prevFile = FileInfo.find.byId(dupFile.getPk());
            model.setFileInfo(prevFile);
            model.save();
          }

          //Now we can delete newly uploaded image
          S3Util.deleteS3File(file.getBucket(), file.getFilepath());

          //Finally deleting image record

          file.delete();

          Ebean.commitTransaction();
        } catch (Exception e) {
          Ebean.rollbackTransaction();
          Log.err("FileController::uploadConfirmation(): Failure, while deduplicating file", e);
          result = false;
        }
      } else {
        file.save();
      }
      flash(SessionConstants.SESSION_PARAM_MSG, "File uploaded and confirmed successfully");
    } else {
      //We should clear model and file here
      flash(SessionConstants.SESSION_PARAM_MSG, "File upload failed. File was not found the server.");
      //Marking as locked to clearly mark it as failed upload as locked is not used for anything else.
      file.setState(RecordState.LOCKED);
      Optional<TripAttachment> attachOp = redis.getBin(K_FILE_UPLOAD_MODEL,
          fileId.toString(),
          TripAttachment.class);
      if(attachOp.isPresent()) {
        TripAttachment attach = attachOp.get();
        attach.refresh();
        attach.setFileInfo(null);
        attach.save();
      }
      result = false;
      file.save();
    }

    redis.delete(K_FILE_UPLOAD_MODEL, fileId.toString(), RedisMgr.WriteMode.UNSAFE);

    redis.delete(K_FILE_UPLOAD_REDIRECT, fileId.toString(), RedisMgr.WriteMode.UNSAFE);

    return result;
  }


  public static boolean uploadConfirmationNoteHelper(Long fileId) {

    boolean result = true;
    RedisMgr redis = CacheMgr.getRedis();
    FileInfo file = FileInfo.find.byId(fileId);
    boolean res = updateFileMetadata(file);
    if(res) {

      //Checking for duplicates right here before saving and if needed replace objects in the model
      FileInfo dupFile = FileInfo.byHash(file.getHash());
      if(dupFile != null && !dupFile.getPk().equals(file.getPk())) {
        try {
          Ebean.beginTransaction();
          //Setting model to use existing image
          Optional<TripNoteAttach> attachOp = redis.getBin(K_FILE_UPLOAD_MODEL,
              fileId.toString(),
              TripNoteAttach.class);
          if (attachOp.isPresent()) {
            TripNoteAttach model = attachOp.get();
            model.refresh();
            FileInfo prevFile = FileInfo.find.byId(dupFile.getPk());
            model.setFileInfo(prevFile);
            model.save();
          }

          //Now we can delete newly uploaded image
          S3Util.deleteS3File(file.getBucket(), file.getFilepath());

          //Finally deleting image record

          file.delete();

          Ebean.commitTransaction();
        } catch (Exception e) {
          Ebean.rollbackTransaction();
          Log.err("FileController::uploadConfirmation(): Failure, while deduplicating file", e);
          result = false;
        }
      } else {
        file.save();
      }
      flash(SessionConstants.SESSION_PARAM_MSG, "File uploaded and confirmed successfully");
    } else {
      //We should clear model and file here
      flash(SessionConstants.SESSION_PARAM_MSG, "File upload failed. File was not found the server.");
      //Marking as locked to clearly mark it as failed upload as locked is not used for anything else.
      file.setState(RecordState.LOCKED);
      Optional<TripNoteAttach> attachOp = redis.getBin(K_FILE_UPLOAD_MODEL,
          fileId.toString(),
          TripNoteAttach.class);
      if(attachOp.isPresent()) {
        TripNoteAttach attach = attachOp.get();
        attach.refresh();
        attach.setFileInfo(null);
        attach.save();
      }
      result = false;
      file.save();
    }

    redis.delete(K_FILE_UPLOAD_MODEL, fileId.toString(), RedisMgr.WriteMode.UNSAFE);

    redis.delete(K_FILE_UPLOAD_REDIRECT, fileId.toString(), RedisMgr.WriteMode.UNSAFE);

    return result;
  }
  /**
   *
   * @return false if no file object was found at S3
   */
  public static boolean updateFileMetadata(FileInfo fi) {
    if(fi.getState() == RecordState.ACTIVE) {
      return true; //Already confirmed
    }

    ObjectMetadata meta = S3Util.getMetadata(fi.getBucket(), fi.getFilepath());
    if (!fi.updateFromMetadata(meta)) {
      Log.err("FileController::confirmUpload: File Upload Failure! File not found on S3: " + fi.getFilepath());
      return  false;
    }

    if (!S3Util.setObjectPublic(fi.getBucket(), fi.getFilepath())) {
      Log.err("FileController::updateImageMetadata: Failed to set uploaded object public. Bucket: " + fi.getBucket() +
          " Key: " +  fi.getFilepath());
    }

    //TODO: ? Should we stream file into the buffer here and check width and height?
    fi.setState(RecordState.ACTIVE);
    return true;
  }

}
