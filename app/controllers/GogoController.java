package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.actions.TravelboundAuthorized;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.GogoForm;
import com.mapped.publisher.parse.extractor.booking.email.ReservationsHolder;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.parse.schemaorg.*;
import com.mapped.publisher.persistence.VOModeller;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.TabType;
import com.mapped.publisher.view.UserView;
import models.publisher.Trip;
import models.publisher.TripNote;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by twong on 25/06/17.
 */
public class GogoController extends Controller {
    @Inject
    FormFactory formFactory;


    @With({Authenticated.class, Authorized.class})
    public Result searchBooking(String tripId) {
        SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
        Trip trip = Trip.find.byId(tripId);
        if (trip != null && SecurityMgr.getTripAccessLevel(trip, sessionMgr.getCredentials()).gt(SecurityMgr.AccessLevel.READ)) {
            String userId = (String) CacheMgr.get(APPConstants.CACHE_GOGO_USERID+sessionMgr.getCredentials().getUserId());

            return ok(views.html.gogo.searchModal.render(trip.tripid,userId));

        }
        BaseView view = new BaseView();
        view.message = "Unauthorized access - Please contact Umapped Support";
        return ok(views.html.common.message.render(view));

    }
    @With({Authenticated.class, Authorized.class})
    public Result importBooking(String tripId) {
        SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

        Form<GogoForm> gogoForm = formFactory.form(GogoForm.class);
        GogoForm gogoForm1 = gogoForm.bindFromRequest().get();
        BaseView view = new BaseView();

        Trip trip = Trip.find.byId(tripId);
        if (trip != null && SecurityMgr.getTripAccessLevel(trip, sessionMgr.getCredentials()).gt(SecurityMgr.AccessLevel.READ)) {
            String uid = gogoForm1.getInGogoUserId();
            String pwd = gogoForm1.getInGogoPwd();
            String booking = gogoForm1.getInGogoBookingId();
            if (uid == null || uid.isEmpty()) {
                view.message = "Please enter your GoGo Vacations user id";
                return ok(views.html.common.message.render(view));
            }
            if (pwd == null || pwd.isEmpty()) {
                view.message = "Please enter your GoGo Vacations password";
                return ok(views.html.common.message.render(view));
            }
            if (booking == null || booking.isEmpty()) {
                view.message = "Please enter your GoGo Vacations booking confirmation number";
                return ok(views.html.common.message.render(view));
            } else {
                booking = booking.toUpperCase();
            }


            try {
                HttpClient c = HttpClientBuilder.create().build();
                HttpPost p = new HttpPost(ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.GOGO_URL));
                ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
                postParameters.add(new BasicNameValuePair("LoginID", uid));
                postParameters.add(new BasicNameValuePair("Password", pwd));
                postParameters.add(new BasicNameValuePair("ConfirmationNo", booking));

                p.setEntity(new UrlEncodedFormEntity(postParameters));
                HttpResponse loginResponse = c.execute(p);
                if (loginResponse != null && loginResponse.getStatusLine().getStatusCode() == 200) {

                    BufferedReader rd = new BufferedReader(new InputStreamReader(loginResponse.getEntity().getContent()));

                    StringBuffer result = new StringBuffer();
                    String line = "";
                    while ((line = rd.readLine()) != null) {
                        result.append(line);
                    }
                    String response = result.toString();
                    if (response != null && response.contains("ReservationPackage")) {
                        ReservationsHolder rh = new ReservationsHolder();
                        rh.includeReservationStatus = true;
                        boolean parseResult = rh.parseJson(response.toString());

                        if (parseResult && rh.hasValidReservations() &&
                                rh.reservationPackage != null) {


                            VOModeller voModeller = new VOModeller(trip, sessionMgr.getCredentials().getAccount(), false);
                            TripVO tripVO = new TripVO();
                            tripVO = rh.toTripVO(tripVO);
                            tripVO.setImportSrc(BookingSrc.ImportSrc.GOGO);
                            tripVO.setImportSrcId(booking);
                            tripVO.setImportTs(System.currentTimeMillis());

                            voModeller.buildTripVOModels(tripVO);
                            //before we save, we try to link notes to bookings where the notes src pk is equal to the booking src pk

                            voModeller.saveAllModels();
                            CacheMgr.set(APPConstants.CACHE_GOGO_USERID+sessionMgr.getCredentials().getUserId(), uid);
                            flash(SessionConstants.SESSION_PARAM_MSG, "Successfully imported " + (voModeller.getDetails().size() + voModeller.getNotes().size()) + " bookings from GoGo Vacations");
                            return redirect(routes.BookingController.bookings(trip.getTripid(), new TabType.Bound(TabType.ITINERARY), null));

                        }
                    }

                } else {
                    switch (loginResponse.getStatusLine().getStatusCode()) {
                        case 403:
                            Log.debug("GoGoController: Invalid userid/pwd  UserId:" + sessionMgr.getUserId() + " Booking: " + booking);
                            view.message = "Authentication Error - please verify your GOGO User Id and Password";
                            break;
                        case 404:
                            Log.debug("GoGoController: Booking Not found UserId:" + sessionMgr.getUserId() + " Booking: " + booking);
                            view.message = "Booking cannot be retrieved - please verify your GOGO Booking Confirmation Number";
                            break;
                        case 500:
                            Log.debug("GoGoController: Error code 500:  UserId:" + sessionMgr.getUserId() + " Booking: " + booking);
                            view.message = "Booking cannot be retrieved - Please contact Umapped Support";
                            break;

                    }
                    return ok(views.html.common.message.render(view));
                }
            } catch (Exception e) {
                Log.err("GoGoController: Error retrieving booking: UserId:" + sessionMgr.getUserId() + " Booking: " + booking, e);
                view.message = "System Error - Please contact Umapped Support";
                return ok(views.html.common.message.render(view));
            }

        }
        view.message = "System Error - Please contact Umapped Support";
        return ok(views.html.common.message.render(view));

    }

}
