package controllers;

import actors.*;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Transaction;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.AdminAccess;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.SessionConstants;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.form.DataTablesForm;
import com.mapped.publisher.form.admin.FormBillingPLan;
import com.mapped.publisher.form.admin.FormCapability;
import com.mapped.publisher.form.admin.FormFeature;
import com.mapped.publisher.js.BackgroundJobStateView;
import com.mapped.publisher.parse.virtuoso.cruise.*;
import com.mapped.publisher.parse.virtuoso.cruise.v5_0.Benefit;
import com.mapped.publisher.persistence.*;
import com.mapped.publisher.persistence.cache.BillingStateCO;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.S3Util;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;
import com.mapped.publisher.view.admin.CapabilityView;
import com.mapped.publisher.view.admin.FeaturesView;
import com.mapped.publisher.view.admin.MessengerView;
import com.mapped.publisher.view.billing.InvoiceView;
import com.mapped.publisher.view.billing.PlansView;
import com.mapped.publisher.view.billing.ReportForCmpyView;
import com.mapped.publisher.view.billing.ReportForUserView;
import com.umapped.api.schema.local.PoiJson;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.common.BkApiSrcIdConstants;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.activity.UmActivityReservation;
import com.umapped.persistence.reservation.cruise.UmCruiseReservation;

import static com.mapped.publisher.utils.S3Util.getObjectList;
import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;
import static models.publisher.S3ImgIndex.findByFilenameOrig;

import com.mapped.publisher.parse.virtuoso.cruise.v5_0.Root.Cruise.CruiseInfo.Promotions.Promotion;

import models.publisher.*;
import org.joda.time.Instant;
import org.joda.time.YearMonth;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.ws.WSClient;
import play.libs.ws.WSRequest;
import play.libs.ws.WSResponse;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import javax.inject.Inject;
import java.text.Normalizer;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.CompletionStage;

/**
 * Billing and other admin-only logic will be migrated here
 *
 * Created by surge on 2015-03-26.
 */
public class AdminController
    extends Controller {
  @Inject
  WSClient    ws;
  @Inject
  FormFactory formFactory;

  @With({Authenticated.class, AdminAccess.class})
  public Result capabilities(Integer capPk) {
    CapabilityView cv = new CapabilityView();

    if(capPk != null) {
      cv.editable = SysCapability.find.byId(capPk);
    }
    cv.capabilities = SysCapability.find.all();

    return ok(views.html.admin.capabilities.render(cv));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result capabilityEdit() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Form<FormCapability> fForm = formFactory.form(FormCapability.class);
    FormCapability       form  = fForm.bindFromRequest().get();

    SysCapability cap = SysCapability.find.byId(form.capPk);

    if(cap != null) {
      cap.setSummary(form.capSummary.trim());
      cap.setDescription(form.capDescription.trim());
      cap.setEnabled(form.capEnabled == null ?false:form.capEnabled);
      cap.setUserControlled(form.capUserControlled == null ? false: form.capUserControlled);
      cap.markModified(sessionMgr.getUserId());
      cap.update();
    }

    return redirect(routes.AdminController.capabilities(null));
  }


  @With({Authenticated.class, AdminAccess.class})
  public Result features(Integer featurePk) {
    FeaturesView fv = new FeaturesView();

    if(featurePk != null) {
      fv.editable = new FeaturesView.FeatureInfo();
      fv.editable.capabilities = SysCapability.getFeatureCapabilities(featurePk);
      if(fv.editable.capabilities == null) {
        fv.editable.capabilities = new ArrayList<>(0);
      }
      fv.editable.feature = SysFeature.find.byId(featurePk);
    }

    //Get all the different capabilities
    fv.availableCaps = SysCapability.find.all();

    List<SysFeature> features = SysFeature.find.all();
    for(SysFeature f: features){
      FeaturesView.FeatureInfo fi = new FeaturesView.FeatureInfo();
      fi.feature = f;
      fi.capabilities = SysCapability.getFeatureCapabilities(f.getPk());
      if(fi.capabilities == null) {
        fi.capabilities = new ArrayList<>(0);
      }
      fv.features.add(fi);
    }

    return ok(views.html.admin.features.render(fv));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result featureEdit() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Form<FormFeature> fForm = formFactory.form(FormFeature.class);
    FormFeature       form  = fForm.bindFromRequest().get();

    SysFeature f = (form.featId != null) ? SysFeature.find.byId(form.featId) : null;
    if(f == null) {
      f = SysFeature.build(sessionMgr.getUserId());
    } else {
      f.markUpdated(sessionMgr.getUserId());

    }

    f.setName(form.featureName.trim());
    f.setDescription(form.featureDescription.trim());
    f.setEnabled(form.featureEnabled == null?false:form.featureEnabled);
    f.setUserControlled(form.featureUserControlled==null?false:form.featureUserControlled);
    f.save();

    try {
      Ebean.beginTransaction();
      int deleted = SysFeatureCapability.deleteAllForFeature(form.featId);
      Log.debug("Deleted " + deleted + " feature capabilities for feature: " + form.featureName);
      if(form.featureCapabilities != null) {
        for (Integer c : form.featureCapabilities) {
          SysFeatureCapability fc = SysFeatureCapability.buildFeatureCapablity(f.getPk(), c, sessionMgr.getUserId());
          fc.save();
        }
      }
      Ebean.commitTransaction();
    } catch (Exception e) {
      e.printStackTrace();
      Ebean.rollbackTransaction();
    }

    return redirect(routes.AdminController.features(null));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result billingPlans(Long planId, Boolean newAddon) {
    PlansView pv = new PlansView();

    PlansView.PlanDetails details = null;
    if (planId != 0L) {
      details = new PlansView.PlanDetails();
      details.plan = BillingPlan.find.byId(planId);
      details.expireDate = Utils.getISO8601Date(details.plan.getExpireTs());
      details.features = SysFeature.getBillingPlanFeatures(planId);
      if(newAddon) {
        pv.planType = BillingPlan.PlanType.ADDON;
        pv.parent = details;
      } else {
        pv.editable = details;
        pv.planType = details.plan.getType();
      }
    } else {
      pv.planType = BillingPlan.PlanType.PLAN;
    }

    if(pv.planType == BillingPlan.PlanType.ADDON && pv.editable != null) {
      pv.parent = new PlansView.PlanDetails();
      pv.parent.plan = pv.editable.plan.getParentPlan();
      pv.parent.features = SysFeature.getBillingPlanFeatures(pv.parent.plan.getPlanId());
    }

    //If showing regular plan add all addons
    if(pv.planType == BillingPlan.PlanType.PLAN && pv.editable != null) {
      List<BillingPlan> addonPlans = BillingPlan.findPlanAddons(pv.editable.plan.getPlanId());
      for (BillingPlan plan: addonPlans) {
        PlansView.PlanDetails pd = new PlansView.PlanDetails();
        pd.plan = plan;
        if (plan.getExpireTs() == null) {
          pd.expireDate = "NEVER";
        } else {
          pd.expireDate = Utils.getISO8601Date(plan.getExpireTs());
        }
        pd.features = SysFeature.getBillingPlanFeatures(plan.getPlanId());
        pd.usersEnabled = BillingAddonsForUser.getEnabledCount(plan.getPlanId());
        pd.usersDisabled = BillingAddonsForUser.getDisabledCount(plan.getPlanId());
        pv.editable.addons.add(pd);
      }
    }

    pv.availableFeatures = SysFeature.allEnabled();

    List<BillingPlan> plans = BillingPlan.findPlans();

    for (BillingPlan plan: plans) {
      PlansView.PlanDetails pd = new PlansView.PlanDetails();
      pd.plan = plan;
      if (plan.getExpireTs() == null) {
        pd.expireDate = "NEVER";
      } else {
        pd.expireDate = Utils.getISO8601Date(plan.getExpireTs());
      }
      pd.features = SysFeature.getBillingPlanFeatures(plan.getPlanId());
      List<BillingPlan> subPlans = BillingPlan.findPlanAddons(plan.getPlanId());
      for(BillingPlan ap: subPlans) {
        PlansView.PlanDetails apd = new PlansView.PlanDetails();
        apd.usersEnabled = BillingAddonsForUser.getEnabledCount(ap.getPlanId());
        apd.usersDisabled = BillingAddonsForUser.getDisabledCount(ap.getPlanId());
        apd.plan = ap;
        pd.addons.add(apd);
      }
      pd.userCount = BillingSettingsForUser.getPlanUseCount(plan.getPlanId());
      pd.cmpyCount = BillingSettingsForCmpy.getPlanUseCount(plan.getPlanId());
      pv.plans.add(pd);
    }
    pv.schedules = BillingSchedule.getDirect();
    pv.consortiums = Consortium.find.all();
    pv.message = flash(SessionConstants.SESSION_PARAM_MSG);
    return ok(views.html.admin.billingPlans.render(pv));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result billingPlanCreate() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Form<FormBillingPLan> pForm      = formFactory.form(FormBillingPLan.class);
    FormBillingPLan       form       = pForm.bindFromRequest().get();

    BillingPlan bp = null;
    if (form.planId != null) {
      bp = BillingPlan.find.byId(form.planId);
    }

    if (bp == null) {
      bp = BillingPlan.buildPlan(sessionMgr.getUserId());
    }

    if(form.planType != null) {
      bp.setType(form.planType);
    }

    bp.setName(form.planName.trim());
    bp.setDescription(form.planDescription.trim());
    bp.setCost(form.planCost != null ? form.planCost : 0.0f);
    bp.setSchedule(form.planSchedule);
    bp.setCurrency(form.planCurrency);
    bp.setTrialLength(form.planTrialDays);
    bp.setBillingType(form.planBillingType);
    bp.setEnabled((form.planEnabled == null || !form.planEnabled)?false:true);
    bp.setVisible((form.planVisible == null || !form.planVisible)?false:true);

    if(form.planType == BillingPlan.PlanType.ADDON) {
      bp.setNewUserState((form.planNewUserState == null || !form.planNewUserState)?false:true);
    }

    if(form.parentId != null) {
      BillingPlan pp = BillingPlan.find.byId(form.parentId);
      bp.setParentPlan(pp);
    }

    if(form.planConsortium == null || form.planConsortium == 0) {
      bp.setConsortium(null);
    } else {
      Consortium cons = Consortium.find.byId(form.planConsortium);
      bp.setConsortium(cons);
    }

    long expireTs = (form.planExpire.length() > 0) ?Utils.getMilliSecs(form.planExpire):0l;
    if (expireTs > 0) {
      bp.setExpireTs(expireTs);
    } else
    {
      bp.setExpireTs(null);
    }

    if(form.planId == null) {
      bp.save();
    } else {
      bp.markModified(sessionMgr.getUserId());
      bp.update();
    }

    try {
      Ebean.beginTransaction();
      int deleted = BillingPlanFeature.deleteAllForPlan(bp.getPlanId());
      Log.debug("Deleted " + deleted + " features for billing plan: " + bp.getName());
      if (form.planFeatures != null) {
        for (Integer f : form.planFeatures) {
          BillingPlanFeature pf = BillingPlanFeature.build(bp.getPlanId(), f);
          pf.save();
        }
      }
      Ebean.commitTransaction();
    } catch (Exception e) {
      e.printStackTrace();
      Ebean.rollbackTransaction();
    }

    //TODO: need to figure out what it means if plan cost changes and how it affects existing users
    return redirect(routes.AdminController.billingPlans(0L, false));
  }

  /**
   * Helper function to return list of plans based on the selected schedule
   * @param schedName
   * @return
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result billingPlanList(String schedName) {
    BillingSchedule.Type bsType = BillingSchedule.Type.valueOf(schedName);
    List<BillingPlan> plans = BillingPlan.plansPerSchedule(bsType);
    Map<String, String> result = new LinkedHashMap<>();
    for(BillingPlan bp: plans) {
      result.put(bp.getPlanId().toString(), bp.getDescriptive());
    }
    return ok(Json.toJson(result));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result billingAddonPush(){
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Form<FormBillingPLan> pForm = formFactory.form(FormBillingPLan.class);
    FormBillingPLan form = pForm.bindFromRequest().get();

    BillingPlan bp = null;
    if (form.planId != null) {
      bp = BillingPlan.find.byId(form.planId);
    }

    if(bp == null) {
      return badRequest();
    }

    List<UserProfile> planUsers = UserProfile.getActiveBilledPlanUsers(form.parentId);
    Transaction    t = Ebean.beginTransaction();
    t.setPersistCascade(false);
    t.setBatchMode(true);
    t.setBatchSize(200);

    long billingStartTime = Utils.addDaysToTs(System.currentTimeMillis(), bp.getTrialLength());
    int userCount = 0;
    try {
      for (UserProfile up : planUsers) {
        BillingAddonsForUser ba = BillingAddonsForUser.getByPk(form.planId, up.getUserid());
        if(ba == null) {
          ba = BillingAddonsForUser.buildAddonForUser(up.getUserid(), form.planId, sessionMgr.getUserId());
          ba.setTrialCount(bp.getTrialLength());
          ba.setStartTs(billingStartTime);
          ba.setEnabled(true);
          ba.save();
          ++userCount;
        }
      }
      t.commit();
    }catch (Exception e){
      t.rollback();
    }

    flash(SessionConstants.SESSION_PARAM_MSG, "Addon was added to " + userCount + " users");
    return redirect(routes.AdminController.billingPlans(0L, false));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result billingPlanSyncFreshbooks() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    final BillingActor.BillingMessage msg = new BillingActor.BillingMessage(BillingActor.BillingCommand.PLANS_SYNC);
    msg.setAdminUserId(sessionMgr.getUserId());

    BillingStateCO state = BillingStateCO.getFromCache();
    if (state.currState == BillingStateCO.State.STOPPED) {
      /** Do the work if nothing else is happening */
      ActorsHelper.tell(SupervisorActor.UmappedActor.BILLING, msg);
    } else {
      Log.err("Failed attempt to sync plans while billing was not stopped: " + state.currState.name());
    }

    Map<String, String> result = new HashMap<>();
    result.put("statusUrl", controllers.routes.AdminController.billingStatus().url());
    return ok(Json.toJson(result));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result billingInvoices(String month, Integer year) {
    InvoiceView iv = new InvoiceView();

    YearMonth period;
    if (month == null || year == null) {
      period = new YearMonth(System.currentTimeMillis());
      year = period.getYear();
      month = ReportForUserView.MONTHS.get(period.getMonthOfYear() - 1);
    } else {
      int monthIdx = ReportForUserView.MONTHS.indexOf(month) + 1;
      period = new YearMonth(year, monthIdx);
    }
    iv.period = period;

    long startTS = Utils.getFirstMsOfMonth(period.getYear(), period.getMonthOfYear());
    long finishTS = Utils.getLastMsOfMonth(period.getYear(), period.getMonthOfYear());

    List<BillingInvoice> bis = BillingInvoice.getAllBetweenTs(startTS, finishTS);

    iv.isPushed = true;
    for(BillingInvoice bi : bis) {
      if (bi.getState() == BillingInvoice.InvoiceState.LOCAL) {
        iv.isPushed = false;
      }
      InvoiceView.InvoiceDetails invd = new InvoiceView.InvoiceDetails();
      invd.invoice = bi;
      if (bi.getUserId() != null) {
        invd.user = UserProfile.findByPK(bi.getUserId());
        invd.reportUrl = routes.AdminController.billingDetailsForUser(bi.getUserId(), month, year).url();
      } else {
        invd.cmpy = Company.find.byId(bi.getCmpyId());
        invd.reportUrl = routes.AdminController.billingDetailsForCmpy(bi.getCmpyId(), month, year).url();
      }
      if (invd.invoice.getState() == BillingInvoice.InvoiceState.ERROR ||
          invd.invoice.getState() == BillingInvoice.InvoiceState.LOCAL) {
        invd.deleteUrl = routes.AdminController.deleteInvoice(bi.getInvId(), month, year).url();
      }
      iv.invoices.add(invd);
    }

    return ok(views.html.admin.billingInvoices.render(iv));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result deleteInvoice(Long invoiceId, String month, Integer year) {

    BillingInvoice bi = BillingInvoice.find.byId(invoiceId);
    bi.delete();

    return redirect(routes.AdminController.billingInvoices(month, year));
  }


  @With({Authenticated.class, AdminAccess.class})
  public Result billingFreshbookClientSync(String command) {

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    final BillingActor.BillingMessage cmd = new BillingActor.BillingMessage(BillingActor.BillingCommand.valueOf(command));
    cmd.setAdminUserId(sessionMgr.getUserId());

    BillingStateCO state = BillingStateCO.getFromCache();
    if (state.currState == BillingStateCO.State.STOPPED) {
      /** Do the work if nothing else is happening */
      ActorsHelper.tell(SupervisorActor.UmappedActor.BILLING, cmd);
    } else {
      Log.err("Failed attempt to sync users while billing was not stopped: " + state.currState.name());
    }

    Map<String, String> result = new HashMap<>();
    result.put("statusUrl", controllers.routes.AdminController.billingStatus().url());

    return ok(Json.toJson(result));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result billingInvoicesGenerate(String month, Integer year) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    int monthIdx = ReportForUserView.MONTHS.indexOf(month) + 1;
    final BillingActor.BillingMessage cmd = new BillingActor.BillingMessage(BillingActor.BillingCommand.INVOICES_GENERATE);
    cmd.setPeriod(year, monthIdx);
    cmd.setAdminUserId(sessionMgr.getUserId());

    BillingStateCO state = BillingStateCO.getFromCache();
    if (state.currState == BillingStateCO.State.STOPPED) {
      /** Do the work if nothing else is happening */
      ActorsHelper.tell(SupervisorActor.UmappedActor.BILLING, cmd);
    } else {
      Log.err("Failed attempt to generate invoices while billing was not stopped: " + state.currState.name());
    }

    Map<String, String> result = new HashMap<>();
    result.put("statusUrl", controllers.routes.AdminController.billingStatus().url());

    return ok(Json.toJson(result));
  }


  @With({Authenticated.class, AdminAccess.class})
  public Result billingInvoiceGenerateUser(String userId, String month, Integer year){
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    int monthIdx = ReportForUserView.MONTHS.indexOf(month) + 1;
    final BillingActor.BillingMessage cmd = new BillingActor.BillingMessage(BillingActor.BillingCommand.INVOICE_GENERATE_USER);
    cmd.setPeriod(year, monthIdx);
    cmd.setAdminUserId(sessionMgr.getUserId());
    cmd.setId(userId);


    BillingStateCO state = BillingStateCO.getFromCache();
    if (state.currState == BillingStateCO.State.STOPPED) {
      /** Do the work if nothing else is happening */
      ActorsHelper.tell(SupervisorActor.UmappedActor.BILLING, cmd);
    } else {
      Log.err("Ignoring attempt to generate user invoice while billing was not stopped: " + state.currState.name());
    }

    Map<String, String> result = new HashMap<>();
    result.put("statusUrl", controllers.routes.AdminController.billingStatus().url());
    return ok(Json.toJson(result));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result billingInvoiceGenerateCmpy(String cmpyId, String month, Integer year){
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    int monthIdx = ReportForUserView.MONTHS.indexOf(month) + 1;
    final BillingActor.BillingMessage cmd = new BillingActor.BillingMessage(BillingActor.BillingCommand.INVOICE_GENERATE_CMPY);
    cmd.setPeriod(year, monthIdx);
    cmd.setAdminUserId(sessionMgr.getUserId());
    cmd.setId(cmpyId);

    BillingStateCO state = BillingStateCO.getFromCache();
    if (state.currState == BillingStateCO.State.STOPPED) {
      /** Do the work if nothing else is happening */
      ActorsHelper.tell(SupervisorActor.UmappedActor.BILLING, cmd);
    } else {
      Log.err("Ignoring attempt to generate cmpy invoice while billing was not stopped: " + state.currState.name());
    }

    Map<String, String> result = new HashMap<>();
    result.put("statusUrl", controllers.routes.AdminController.billingStatus().url());
    return ok(Json.toJson(result));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result billingInvoicesPushFreshbooks(String month, Integer year) {
    return billingInvoicesInitiateAction(BillingActor.BillingCommand.INVOICES_PUSH, month, year);
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result billingInvoicesSendFreshbooks(String month, Integer year) {
    return billingInvoicesInitiateAction(BillingActor.BillingCommand.INVOICES_SEND, month, year);
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result billingInvoicesSyncFreshbooks(String month, Integer year) {
    return billingInvoicesInitiateAction(BillingActor.BillingCommand.INVOICES_SYNC, month, year);
  }

  private Result billingInvoicesInitiateAction(final BillingActor.BillingCommand cmd, String month, Integer year) {
    SessionMgr sessionMgr = new SessionMgr(session());
    int monthIdx = ReportForUserView.MONTHS.indexOf(month) + 1;
    BillingActor.BillingMessage msg = new BillingActor.BillingMessage(cmd);
    msg.setPeriod(year, monthIdx);
    msg.setAdminUserId(sessionMgr.getUserId());

    BillingStateCO state = BillingStateCO.getFromCache();
    if (state.currState == BillingStateCO.State.STOPPED) {
      /** Do the work if nothing else is happening */
      ActorsHelper.tell(SupervisorActor.UmappedActor.BILLING, msg);
    } else {
      Log.err("Failed attempt to execute command "+ cmd.name() +
              ", while billing was not stopped: " + state.currState.name());
    }

    Map<String, String> result = new HashMap<>();
    result.put("statusUrl", controllers.routes.AdminController.billingStatus().url());

    return ok(Json.toJson(result));
  }

  public Result billingStatus() {
    BillingStateCO state = BillingStateCO.getFromCache();
    if (state == null) {
      state = new BillingStateCO();
    }
    return ok(Json.toJson(state));
  }

  /**
   * Billing reports for users who are being billed directly
   * @return
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result billingReportUser() {
    BaseView bv = new BaseView();
    return ok(views.html.admin.billingReportsUsers.render(bv));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result billingReportCmpy() {
    BaseView bv = new BaseView();
    return ok(views.html.admin.billingReportsCmpy.render(bv));
  }

  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, AdminAccess.class})
  public Result billingReportUserDT(String userType) {
    JsonNode jsonRequest = request().body().asJson();
    DataTablesForm dtRequest = Json.fromJson(jsonRequest, DataTablesForm.class);

    BillingSettingsForUser.BillingUserType ut = null;
    if(userType != null) {
      ut = BillingSettingsForUser.BillingUserType.valueOf(userType);
    }

    if (ut == null) {
      ut = BillingSettingsForUser.BillingUserType.UB_ALL;
    }

    class UBRow {
      public String userId;
      public String name;
      public String schedule;
      public String plan;
      public String billTo;
      public String pmntType;
      public Boolean isFresh;
      public Map<String, String> reports;
      public Map<String, String> actions;

      public UBRow() {
        actions = new TreeMap<>();
        reports = new TreeMap<>();
      }
    }

    String sortOrder = "userid asc";
    if (dtRequest.columns != null && dtRequest.columns.size() > 0 && dtRequest.order != null && dtRequest.order.size() > 0 && dtRequest.order.get(0).column != null) {
      if (dtRequest.columns.get(dtRequest.order.get(0).column).name != null && !dtRequest.columns.get(dtRequest.order.get(0).column).name.isEmpty()) {
        sortOrder = dtRequest.columns.get(dtRequest.order.get(0).column).name;
        if (dtRequest.order.get(0).dir != null) {
          sortOrder += " " + dtRequest.order.get(0).dir;
        }
      }
    }

    DataTablesView<UBRow> dtv = new DataTablesView<>(dtRequest.draw);
    dtv.recordsTotal    = BillingSettingsForUser.getRowCount(ut);
    String term = "%";
    if (!dtRequest.search.value.trim().isEmpty()) {
      term = dtRequest.search.value;
    }
    dtv.recordsFiltered = BillingSettingsForUser.getRowCountFiltered(ut, term);
    List<UserProfile> recs = BillingSettingsForUser.getProfilesForType(ut,
                                                                       dtRequest.start,
                                                                       dtRequest.length,
                                                                       term, sortOrder);
    for (UserProfile up : recs) {

      BillingSettingsForUser bs = BillingSettingsForUser.find.byId(up.userid);

      UBRow row = new UBRow();
      row.userId  = up.userid;
      row.name    = up.firstname + " " + up.lastname;
      row.isFresh = false;

      if (bs != null && bs.getFreshbookClientData() != null) {
        if (bs.getFreshbookClientData().getId() != null) {
          row.isFresh = true;
        }
      }

      if (bs != null && bs.getSchedule() != null) {
        row.schedule = bs.getSchedule().getLongName();
      } else {
        row.schedule = "NOT SET";
      }

      if (bs != null && bs.getPlan() != null) {
        row.plan = bs.getPlan().getName();
      } else {
        row.plan = "NOT SET";
      }

      if (bs != null && bs.getBillTo() != null) {
        row.billTo = bs.getBillTo().getName().name();
        if (bs.getBillTo().getName() == BillingEntity.Type.AGENT) {
          row.billTo += " (" + bs.getAgent() + ")";
        } else if (bs.getBillTo().getName() == BillingEntity.Type.COMPANY) {
          Company cmpy = Company.find.byId(bs.getCmpyid());
          row.billTo += " (" + cmpy.getName() + ")";
        }
      } else {
        row.billTo = "NOT SET";
      }

      if (bs != null && bs.getPmntType() != null) {
        row.pmntType = bs.getPmntType().getName().name();
      } else {
        row.pmntType = "NOT SET";
      }

      if (bs != null) {
        row.reports.put("Reports", controllers.routes.AdminController.billingDetailsForUser(up.userid, null, null).url());
      }

      row.actions.put("Profile", controllers.routes.UserController.user(up.userid, (String)null, false).url());
      dtv.data.add(row);
    }

    return  ok(Json.toJson(dtv));
  }

  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, AdminAccess.class})
  public Result billingReportCmpyDT(String schedule) {
    JsonNode jsonRequest = request().body().asJson();
    DataTablesForm dtRequest = Json.fromJson(jsonRequest, DataTablesForm.class);

    BillingSchedule.Type bst = null;
    if (schedule != null) {
      try {
        bst = BillingSchedule.Type.valueOf(schedule);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    class BCRow {
      public String name;
      public String schedule;
      public String plan;
      public String pmntType;
      public String startDate;
      public String cutoffDate;
      public Boolean isFresh;

      public Map<String, String> reports;
      public Map<String, String> actions;

      public BCRow() {
        reports = new TreeMap<>();
        actions = new TreeMap<>();
        isFresh = false;
      }
    }

    DataTablesView<BCRow> dtv = new DataTablesView<>(dtRequest.draw);

    dtv.recordsTotal = Company.getRowCount_Billing(bst);

    dtv.recordsFiltered = Company.getRowCountFiltered_Billing(bst, dtRequest.search.value);

    List<Company> recs = Company.getFilteredPage_Billing(bst, dtRequest.start,
                                                 dtRequest.length,
                                                 dtRequest.search.value);

    for (Company c : recs) {
      BillingSettingsForCmpy bsc = BillingSettingsForCmpy.find.byId(c.cmpyid);

      BCRow row = new BCRow();
      row.name = c.name;

      if (bsc != null && c.getAuthorizeduserid() != null && bsc.getMeta() != null) {
        if(bsc.getFreshbookClientData().getId() != null) {
          row.isFresh = true;
        }
      }

      if (bsc != null && bsc.getSchedule() != null) {
        row.schedule = bsc.getSchedule().getLongName();
      } else {
        row.schedule = "NOT SET";
      }

      if (bsc != null && bsc.getPlan() != null) {
        row.plan = bsc.getPlan().getName();
      } else {
        row.plan = "NOT SET";
      }

      if (bsc != null && bsc.getPmntType() != null) {
        row.pmntType = bsc.getPmntType().getName().name();
      } else {
        row.pmntType = "NOT SET";
      }

      if (bsc != null && bsc.getStartTs() != null) {
        row.startDate = bsc.getStartDate();
      } else {
        row.startDate = "NOT SET";
      }

      if (bsc != null && bsc.getCutoffTs() != null) {
        row.cutoffDate = bsc.getCutoffDate();
      } else {
        row.cutoffDate = "NOT SET";
      }

      if (bsc != null) {
        row.reports.put("Reports", controllers.routes.AdminController.billingDetailsForCmpy(c.cmpyid,null, null).url());
      }

      row.actions.put("Profile", controllers.routes.CompanyController.viewCompany(c.cmpyid).url());
      dtv.data.add(row);
    }

    return  ok(Json.toJson(dtv));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result billingDetailsForCmpy(String cmpyId, String month, Integer year) {

    /* 1. Determine dates and exact timestamps for start and end of billing */
    YearMonth period;
    if (month == null || year == null) {
      period = new YearMonth(System.currentTimeMillis());
      year = period.getYear();
      month = ReportForUserView.MONTHS.get(period.getMonthOfYear() - 1);
    } else {
      int monthIdx = ReportForUserView.MONTHS.indexOf(month) + 1;
      period = new YearMonth(year, monthIdx);
    }

    /* 2. Build report */
    ReportForCmpyView cv = BillingActor.buildReportForCmpy(cmpyId, period);
    cv.invGenUrl = routes.AdminController.billingInvoiceGenerateCmpy(cmpyId, month, year).url();

    return ok(views.html.admin.billingDetailsForCmpy.render(cv));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result billingDetailsForUser(String userId, String month, Integer year) {

    /* 1. Determine dates and exact timestamps for start and end of billing */
    YearMonth period;
    if (month == null || year == null) {
      period = new YearMonth(System.currentTimeMillis());
      year = period.getYear();
      month = ReportForUserView.MONTHS.get(period.getMonthOfYear() - 1);
    } else {
      int monthIdx = ReportForUserView.MONTHS.indexOf(month) + 1;
      period = new YearMonth(year, monthIdx);
    }

    /* 2. Build report */
    ReportForUserView uv = BillingActor.buildReportForUser(userId, period);
    uv.invGenUrl = routes.AdminController.billingInvoiceGenerateUser(userId, month, year).url();

    return ok(views.html.admin.billingDetailsForUser.render(uv));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result cruiseMgmt() {

    BaseView view = new BaseView();


    return ok(views.html.admin.cruise.cruiseMgmt.render(view));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result downloadCruise() {

    final DynamicForm form = formFactory.form().bindFromRequest();

    String startDate = form.get("inStartDate");
    String endDate = form.get("inEndDate");

    String term = form.get("inTerm");
    if (term == null || term.trim().length() == 0) {
      term = "";
    }

    BaseView view = new BaseView();
    if (startDate != null && startDate.trim().length() > 0 && endDate != null && endDate.trim().length() > 0) {
      startDate = startDate + "T00:00:00.000Z";
      endDate = endDate + "T23:59:00.000Z";
      view.message = "Downloading " + startDate + " to " + endDate + " Term: " + term;

      final VirtuosoCruisesActor.Command msg = new VirtuosoCruisesActor.Command(startDate, endDate, term);
      ActorsHelper.tell(SupervisorActor.UmappedActor.VIRTUOSO_CRUISES, msg);
    }
    return ok(views.html.admin.cruise.cruiseMgmt.render(view));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result downloadCruiseURL() {

    final DynamicForm form = formFactory.form().bindFromRequest();

    String url = form.get("inUrl");

    BaseView view = new BaseView();

    view.message = "Downloaded cruise from url: " + url;

    CruiseVO cruiseVO = CruiseParser.getCruise(url);
    long ret = cruiseVO.save();

    return ok(views.html.admin.cruise.cruiseMgmt.render(view));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result downloadCruiseXML() {

    final DynamicForm form = formFactory.form().bindFromRequest();

    String name = form.get("inFileName");
    String fromDate = form.get("fromStartDate");
    String toDate = form.get("toStartDate");

    BaseView view = new BaseView();

    String filename = name;
    if (!filename.endsWith(".xml")) {
       filename = filename + ".xml";
    }

    view.message = "Downloaded cruise from XML: " + filename;

    List<CruiseVO> cruiseVOList = CruiseParser.getCruiseFromXML(filename, fromDate, toDate);
    int i = 0;
    if (cruiseVOList != null && cruiseVOList.size() > 0) {
      for (CruiseVO cruiseVO : cruiseVOList) {
        long ret = cruiseVO.save();
        i++;
      }
    }
    view.message += " - " + i + " cruises";
    return ok(views.html.admin.cruise.cruiseMgmt.render(view));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result matchCruisePOI() {

    BaseView view = new BaseView();

    int portInserted = 0;
    int portFound = 0;
    int portFailed = 0;

    int shipInserted = 0;
    int shipFound = 0;
    int shipFailed = 0;

    //TmpltImport.findBySrcReferenceId("14034307",TmpltType.Type.CRUISE_VIRTUOSO);//
    List<TmpltImport> recs = TmpltImport.findUnsynced(TmpltType.Type.CRUISE_VIRTUOSO);
    for (TmpltImport rec1: recs) {
      try {
        TmpltImport rec = TmpltImport.find.byId(rec1.getImportId());

        ObjectMapper mapper = new ObjectMapper();
        com.fasterxml.jackson.databind.ObjectReader r = mapper.reader(CruiseVO.class);
        CruiseVO cruiseVO = r.readValue(rec.getRaw());
        StringBuilder sb = new StringBuilder();

        sb.append(cruiseVO.getCruiseName());
        sb.append(" - ");
        sb.append(cruiseVO.getCruiseShip().getCompany());
        if (cruiseVO.getCruiseStops() != null) {
          for (CruiseStopVO stopVO : cruiseVO.getCruiseStops()) {
           // sb.append("Stop: "); sb.append(stopVO.getDay()); sb.append(" ");sb.append(stopVO.getName()); sb.append(" ");sb.append(stopVO.getLatitude()); sb.append(" ");sb.append(stopVO.getLongitude());sb.append("\n");
            String nameLower = stopVO.getName().toLowerCase();
            String[] tokens = stopVO.getName().split(",");
            String country = null;
            String stopName = stopVO.getName();
            if (tokens.length > 1) {
              country = tokens[tokens.length - 1];
            }
            if (tokens.length > 0) {
              stopName = tokens[0];
            }

            if (stopVO.getLongitude() != 0 && stopVO.getLatitude() != 0 && !(nameLower.contains("day") && nameLower.contains("sea"))) {
              //check if a poi exist
              List<PoiRS> pois = PoiMgr.findPortByNameorGPS(stopName, stopVO.getLatitude(), stopVO.getLongitude(), (float) 0.1);
              if (pois != null && pois.size() > 0) {
                portFound++;
                for (PoiRS rs : pois) {
                  System.out.println( sb.toString() + ": " + stopVO.getName() + " POI: " + rs.getName() + " - " + rs.locLat + " - " + rs.locLong);
                }
              } else {
                try {
                  //insert the poi for port stop
                  PoiRS poiRS = new PoiRS();
                  poiRS.setCmpyId(0);
                  poiRS.consortiumId = 0;
                  poiRS.setSrcId(4);//ports virtuoso
                  poiRS.setSrcReference(cruiseVO.getCruiseId());
                  poiRS.setId(DBConnectionMgr.getUniqueLongId());
                  poiRS.setName(stopName);
                  poiRS.locLat = stopVO.getLatitude();
                  poiRS.locLong = stopVO.getLongitude();
                  poiRS.createdby = "system";
                  poiRS.createdtimestamp = System.currentTimeMillis();
                  poiRS.modifiedby = "system";
                  poiRS.lastupdatedtimestamp = System.currentTimeMillis();
                  poiRS.state = RecordState.ACTIVE;
                  poiRS.setTypeId(PoiTypeInfo.Instance().byName("Port").getId());
                  CountriesInfo.Country c = CountriesInfo.Instance().searchByName(country);
                  if (c != null) {
                    poiRS.countryCode = c.getAlpha3();
                  }
                  else {
                    poiRS.countryCode = "UNK";
                    Log.log(LogLevel.ERROR,
                            "Country: " + country + " can not be found for Cruise. POI id:" + poiRS.getId() +
                            " name:" + poiRS.getName());
                  }
                  PoiMgr.save(poiRS);
                  portInserted++;
                } catch (Exception e) {
                  portFailed++;
                }
              }
            }
          }
        }

        //create the cruise ship record and add all the urls
        if (cruiseVO.getCruiseShip() != null && cruiseVO.getCruiseShip().getName() != null && cruiseVO.getCruiseShip().getName().trim().length() > 0) {
          List<Integer> types = new ArrayList<>();
          types.add(6);
          Set<Integer> cmpyIds = new TreeSet();
          cmpyIds.add(0);

          List<PoiRS> cruiseShips = PoiController.findMergedByTerm(cruiseVO.getCruiseShip().getName(), types,
                                                                   cmpyIds, 1);
          boolean found = false;
          if (cruiseShips != null) {
            for (PoiRS cruise: cruiseShips) {
              if (cruise.getName().toLowerCase().equals(cruiseVO.getCruiseShip().getName().toLowerCase())) {
                found = true;
              }
            }
         }

          if (!found) {
            try {
              PoiRS poiRS = new PoiRS();
              poiRS.setCmpyId(0);
              poiRS.consortiumId = 0;
              poiRS.setSrcId(4);//ports virtuoso
              poiRS.setSrcReference(cruiseVO.getCruiseId());
              poiRS.setId(DBConnectionMgr.getUniqueLongId());
              poiRS.createdby = "system";
              poiRS.createdtimestamp = System.currentTimeMillis();
              poiRS.modifiedby = "system";
              poiRS.lastupdatedtimestamp = System.currentTimeMillis();
              poiRS.state = RecordState.ACTIVE;
              poiRS.setTypeId(6);
              poiRS.countryCode = "UNK";
              PoiJson poiJson = new PoiJson();
              poiJson.setName(cruiseVO.getCruiseShip().getName());
              poiJson.setCmpyId(0);
              poiJson.setPoiTypeId(PoiTypeInfo.Instance().byName("Ship").getId());
              poiJson.setDesc(cruiseVO.getCruiseShip().getNote());
              poiJson.setPoiId(poiRS.getId());
              poiJson.setCode(cruiseVO.getCruiseShip().getCompany().trim());
              poiJson.addTags(cruiseVO.getCruiseShip().getCompany().trim());
              poiJson.addUrl(cruiseVO.getCruiseShip().getDeckplanUrl());
              if (cruiseVO.getCruiseShip().getCruisePhotos() != null) {
                for (String url : cruiseVO.getCruiseShip().getCruisePhotos().keySet()) {
                  poiJson.addUrl(url);
                }
              }
              poiRS.setData(poiJson);
              PoiMgr.save(poiRS);
              shipInserted++;
            } catch (Exception e) {
              shipFailed++;
            }
          } else {
            shipFound++;
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    view.message = "Port Found: " + portFound + " Port Inserted: " + portInserted + " Port Failed: " + portFailed + " Ship Found: " + shipFound + " Ship Inserted: " + shipInserted + " Ship Failed: " + shipFailed;
    return ok(views.html.admin.cruise.cruiseMgmt.render(view));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result importCruiseTemplate() {
    BaseView view = new BaseView();
    int inserted = 0;
    int updated = 0;
    int failed = 0;
    int read = 0;

    List<TmpltImport> recs = TmpltImport.findUnsynced(TmpltType.Type.CRUISE_VIRTUOSO); //TmpltImport.findBySrcReferenceId("14034307", TmpltType.Type.CRUISE_VIRTUOSO);
    for (TmpltImport rec1: recs) {
      try {
        TmpltImport rec = TmpltImport.find.byId(rec1.getImportId());
        read++;
        ObjectMapper mapper = new ObjectMapper();
        com.fasterxml.jackson.databind.ObjectReader r = mapper.reader(CruiseVO.class);
        CruiseVO cruiseVO = r.readValue(rec.getRaw());
        try {
          if (rec.getTmpltId() == null || rec.getTmpltId().trim().length() == 0) {
            processInsert(rec, cruiseVO);
            inserted++;
          }
          else {
            processUpdate(rec, cruiseVO);
            updated++;
          }
        } catch (Exception e) {
          failed++;
          e.printStackTrace();
        }
      }
      catch (Exception e) {
      }
    }

    view.message = "Cruise read: " + read +" Cruise Updated: " + updated + " Cruise Inserted: " + inserted + " Cruise Failed: " + failed;
    return ok(views.html.admin.cruise.cruiseMgmt.render(view));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result importCruiseShipPhotos () {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Account account;
    if(sessionMgr.getAccount().isPresent()) {
      account = sessionMgr.getAccount().get();
    } else {
      account = Account.find.byId(sessionMgr.getAccountId());
    }
    BaseView view = new BaseView();
    List <PoiRS> ships = PoiMgr.findCruiseShipsNoFiles();
    for (PoiRS ship: ships) {
      int i = 0;
      Set<String> urls = ship.data.getUrls();
      System.out.println("Ship: " + ship.getName() + " - " + ship.getCode());
      if (urls != null && urls.size() > 0) {
        for (String url: urls) {
          if (url != null && !url.contains("deckcharts.com") && i < 5) {
            try {
              String fileName = url.substring(url.lastIndexOf("/") + 1).trim();
              FileImage image = ImageController.downloadAndSaveImage(url, fileName,
                                                                     FileSrc.FileSrcType.IMG_VENDOR_VIRTUOSO, null,
                                                                     account, false);
              if (image != null) {
                PoiFile pf = PoiFile.buildPoiFile(ship.getId(), ship.getCmpyId(), fileName, "",
                                                  Account.LEGACY_PUBLISHER_USERID);
                pf.setFileImage(image);
                pf.setName(fileName);
                pf.setNameSys(S3Util.normalizeFilename(fileName));
                pf.setSrcId(4);//virtuoso
                pf.setState(RecordState.ACTIVE);
                LstFileType fileType = LstFileType.findFromFileName(fileName);
                pf.setExtension(fileType);
                pf.setUrl(image.getUrl());
                pf.save();
                System.out.println("Photo Saved: " + url + " S3: " + image.getUrl());
                i++;
              }
            } catch (Exception e) {
              e.printStackTrace();
            }
          }
        }
        if (i > 0) {
          PoiController.incrementFileCount(ship, "system");
        }
      } else {
        System.out.println("NO Photos... \n\n");
      }
    }


    view.message = "Photos importing";
    return ok(views.html.admin.cruise.cruiseMgmt.render(view));
  }

 /* process new cruises and import tmplt_Details cruises*/
  public static boolean processInsert (TmpltImport tmpltImport, CruiseVO cruiseVO) {
    try {
      Ebean.beginTransaction();
      String cruiseName = cruiseVO.getCruiseName() + " - " + cruiseVO.getCruiseShip().getName() + " - " + Utils.formatTimestamp(
              cruiseVO.getStartDate().getTime(),
              "MMM dd, YYYY");
      if (cruiseVO.getDuration() != null && cruiseVO.getDuration().trim().length() > 0) {
        cruiseName += " (" + cruiseVO.getDuration() + " days)";
      }
      Log.debug("Processing Insert for " + cruiseName);

      cruiseVO.setCruiseName(cruiseName.trim());
      Template template = new Template();
      template.setCreatedby("system");
      template.setCreatedtimestamp(System.currentTimeMillis());
      template.setVisibility(Template.Visibility.PUBLIC);
      template.setTemplateid(DBConnectionMgr.getUniqueId());
      template.setCmpyid("0");
      template.setStatus(0);
      template.setTmpltType(TmpltType.Type.CRUISE_VIRTUOSO);
      template.setName(cruiseName);
      template.setDescription(cruiseVO.getDescription());
      template.setModifiedby("system");
      template.setLastupdatedtimestamp(System.currentTimeMillis());
      template.save();

      //process the locations
      PoiRS startPort = null;
      PoiRS endPort = null;
      CruiseStopVO startPortVO = null;
      CruiseStopVO endPortVO = null;

      if (cruiseVO.getCruiseStops() != null && cruiseVO.getCruiseStops().size() > 0) {
        int rank = 0;
        for (int i = 0; i < cruiseVO.getCruiseStops().size(); i++) {
          PoiRS poi = null;
          CruiseStopVO stopVO = cruiseVO.getCruiseStops().get(i);
          String nameLower = stopVO.getName().toLowerCase();
          String[] tokens = stopVO.getName().split(",");
          String stopName = stopVO.getName();
          if (tokens.length > 0) {
            stopName = tokens[0];
          }
          //let's lookup the port
          if (stopVO.getLongitude() != 0 && stopVO.getLatitude() != 0 && !(nameLower.contains("day") && nameLower.contains("sea"))) {
            //check if a poi exist
            List<PoiRS> pois = PoiMgr.findPortByNameorGPS(stopName, stopVO.getLatitude(), stopVO.getLongitude(), (float) 0.1);
            if (pois != null && pois.size() > 0) {
              poi = pois.get(0);
            }
          } else if (stopVO.getLongitude() == 0 && stopVO.getLatitude() == 0 && !(nameLower.contains("day") && nameLower.contains("sea"))) {
            //let's add the port location if we can find one
            List<PoiRS> ports = PoiController.findMergedByTerm(stopVO.getName(), 7, 0, 3);
            if (ports != null && ports.size() > 0) {
              for (PoiRS port : ports) {
                if (port.getName().equalsIgnoreCase(stopVO.getName())) {
                  poi = port;
                  break;
                }
              }

              if (poi == null) {
                poi = ports.get(0);
              }
            }
          }

          TmpltDetails cruiseStop = new TmpltDetails();
          cruiseStop.setDetailsid(DBConnectionMgr.getUniqueId());
          cruiseStop.setCreatedby("system");
          cruiseStop.setTemplateid(template.getTemplateid());
          cruiseStop.setCreatedtimestamp(System.currentTimeMillis());
          cruiseStop = processCruiseStop(cruiseStop, poi, cruiseVO.getCruiseStops().get(i));
          cruiseStop.setRank(rank++);
          UmActivityReservation cruiseStopReservation = new UmActivityReservation();
          cruiseStop.setReservation(cruiseStopReservation);
          cruiseStopReservation.getActivity().name = cruiseStop.getName();
          cruiseStopReservation.setNotesPlainText(cruiseStop.getComments());
          cruiseStop.save();


          if (i == 0) {
            if (poi != null) {
              startPort = poi;
            }
            startPortVO = cruiseVO.getCruiseStops().get(i);
          } else if (i == cruiseVO.getCruiseStops().size() - 1) {
            if (poi != null) {
              endPort = poi;
            }
            endPortVO = cruiseVO.getCruiseStops().get(i);
          }
        }
      }

      //process the cruise itself
      TmpltDetails cruise = new TmpltDetails();
      UmCruiseReservation cruiseReservation = new UmCruiseReservation();
      cruise.setReservation(cruiseReservation);
      cruise.setDetailsid(DBConnectionMgr.getUniqueId());
      cruise.setTemplateid(template.getTemplateid());
      cruise.setCreatedby("system");
      cruise.setCreatedtimestamp(System.currentTimeMillis());
      cruise.setModifiedby("system");
      cruise.setLastupdatedtimestamp(System.currentTimeMillis());
      cruise = processCruise(cruise, cruiseVO, startPort, startPortVO, endPort, endPortVO);
      cruiseReservation.getCruise().name = cruiseVO.getCruiseShip().getName();
      cruiseReservation.getCruise().getProvider().name = cruiseVO.getCruiseShip().getCompany();
      cruiseReservation.setNotesPlainText(cruise.comments);
      cruiseReservation.setNotesPlainText(cruise.getComments());
      cruise.save();

      tmpltImport.setSyncTs(System.currentTimeMillis());
      tmpltImport.setTmpltId(template.getTemplateid());
      tmpltImport.save();
      Ebean.commitTransaction();
    } catch (Exception e) {
      Log.err("importCruiseTemplate: insert cruise - Cruise update error: " + cruiseVO.getCruiseId() + " " + e.getMessage(), e);
      try {
        Ebean.rollbackTransaction();
      } catch (Exception re) {
        Log.err("importCruiseTemplate: insert cruise - Cruise update error: " + cruiseVO.getCruiseId() + " " + re.getMessage(), re);
      }
    }
    return false;
  }

  /*
  This cruise already exists in the template table - so we need to figure out what to update/delete
   */
  public static boolean processUpdate (TmpltImport tmpltImport, CruiseVO cruiseVO) {
    try {
      Ebean.beginTransaction();
      Template template = null;
      if (tmpltImport.getTmpltId() != null && tmpltImport.getTmpltId().length() > 0) {
        template = Template.find.byId(tmpltImport.getTmpltId());
      }
      if (template != null) {
        String cruiseName = cruiseVO.getCruiseName() + " - " + cruiseVO.getCruiseShip().getName() + " - " + Utils.formatTimestamp(cruiseVO.getStartDate().getTime(), "MMM dd, YYYY");
        if (cruiseVO.getDuration() != null && cruiseVO.getDuration().trim().length() > 0) {
          cruiseName += " (" + cruiseVO.getDuration() + " days)";
        }
        cruiseVO.setCruiseName(cruiseName.trim());
        Log.debug("Processing Update for " + cruiseName);



        List<TmpltDetails> allDetails = TmpltDetails.findByTemplateId(template.templateid);
        //process the cruise itself
        TmpltDetails cruise = null;
        for (TmpltDetails d : allDetails) {
          if (d.detailtypeid == ReservationType.CRUISE) {
            cruise = d;
            break;
          }
        }
        if (cruise == null) {
          Log.err("importCruiseTemplate: update cruise - Cruise not found: " + template.getTemplateid());
          return false;

        }

        template.setCmpyid("0");
        template.setTmpltType(TmpltType.Type.CRUISE_VIRTUOSO);
        template.setName(cruiseName);
        template.setDescription(cruiseVO.getDescription());
        template.save();

        //process the locations
        PoiRS startPort = null;
        PoiRS endPort = null;
        CruiseStopVO startPortVO = null;
        CruiseStopVO endPortVO = null;

        if (cruiseVO.getCruiseStops() != null && cruiseVO.getCruiseStops().size() > 0) {
          int rank = 0;

          for (int i = 0; i < cruiseVO.getCruiseStops().size(); i++) {
            PoiRS poi = null;
            CruiseStopVO stopVO = cruiseVO.getCruiseStops().get(i);
            String nameLower = stopVO.getName().toLowerCase();
            String[] tokens = stopVO.getName().split(",");
            String stopName = stopVO.getName();
            if (tokens.length > 0) {
              stopName = tokens[0];
            }
            //let's lookup the port
            if (stopVO.getLongitude() != 0 && stopVO.getLatitude() != 0 && !(nameLower.contains("day") && nameLower.contains("sea"))) {
              //check if a poi exist
              List<PoiRS> pois = PoiMgr.findPortByNameorGPS(stopName, stopVO.getLatitude(), stopVO.getLongitude(), (float) 0.1);
              if (pois != null && pois.size() > 0) {
                poi = pois.get(0);
              }
            }
            //let's lookup the port
            TmpltDetails cruiseStop = null;
            for (TmpltDetails d : allDetails) {
              if (d.getStatus() == 0 && d.getName() != null && stopVO.getName() != null && d.getName().equals(stopVO.getName()) && (d.detailtypeid == ReservationType.CRUISE_STOP || d.detailtypeid == ReservationType.TOUR) && stopVO.getStartTimestamp().getTime() == d.getStarttimestamp().longValue() && d.getEndtimestamp()
                      .longValue() == stopVO.getEndTimestamp()
                      .getTime()) {
                cruiseStop = d;
                break;
              }
            }
            if (cruiseStop == null) {
              cruiseStop = new TmpltDetails();
              cruiseStop.setDetailsid(DBConnectionMgr.getUniqueId());
              cruiseStop.setTemplateid(template.getTemplateid());
              cruiseStop.setCreatedby("system");
              cruiseStop.setCreatedtimestamp(System.currentTimeMillis());
              cruiseStop.setStatus(0);
            } else {
              //remove this from the list
              allDetails.remove(cruiseStop);
            }
            cruiseStop = processCruiseStop(cruiseStop, poi, stopVO);
            cruiseStop.setRank(rank++);

            UmActivityReservation cruiseStopReservation = cast(cruiseStop.getReservation(), UmActivityReservation.class);
            if (cruiseStopReservation == null) {
              cruiseStopReservation = new UmActivityReservation();
              cruiseStop.setReservation(cruiseStopReservation);
            }
            cruiseStopReservation.getActivity().name = cruiseStop.getName();
            cruiseStopReservation.setNotesPlainText(cruiseStop.getComments());

            cruiseStop.save();

            if (i == 0) {
              if (poi != null) {
                startPort = poi;
              }
              startPortVO = stopVO;
            } else if (i == cruiseVO.getCruiseStops().size() - 1) {
              if (poi != null) {
                endPort = poi;
              }
              endPortVO = stopVO;
            }
          }

          //remove any existing stops that are no longer in the cruise download
          for (TmpltDetails d : allDetails) {
            if (d.detailtypeid == ReservationType.CRUISE_STOP || d.detailtypeid == ReservationType.TOUR) {
              d.setStatus(APPConstants.STATUS_DELETED);
              d.setLastupdatedtimestamp(System.currentTimeMillis());
              d.setModifiedby("system");
              d.save();
            }
          }
        }


        cruise = processCruise(cruise, cruiseVO, startPort, startPortVO, endPort, endPortVO);
        UmCruiseReservation cruiseReservation = cast(cruise.getReservation(), UmCruiseReservation.class);
        if (cruiseReservation == null) {
          cruiseReservation = new UmCruiseReservation();
          cruise.setReservation(cruiseReservation);
        }
        cruiseReservation.getCruise().name = cruiseVO.getCruiseShip().getName();
        cruiseReservation.getCruise().getProvider().name = cruiseVO.getCruiseShip().getCompany();
        cruiseReservation.setNotesPlainText(cruise.getComments());

        cruise.save();

        tmpltImport.setSyncTs(System.currentTimeMillis());
        tmpltImport.setTmpltId(template.getTemplateid());
        tmpltImport.save();
      }
      Ebean.commitTransaction();
    } catch (Exception e) {
      Log.err("importCruiseTemplate: update cruise - Cruise update error: " + cruiseVO.getCruiseId() + " " + e.getMessage(), e);
      try {
        Ebean.rollbackTransaction();
      } catch (Exception re) {
        Log.err("importCruiseTemplate: update cruise - Cruise update error: " + cruiseVO.getCruiseId() + " " + re.getMessage(), re);
      }
    }

    return false;
  }

  /*
  create the tmpltdetails as a CRUISE record
   */

  public static TmpltDetails processCruise (TmpltDetails cruise, CruiseVO cruiseVO, PoiRS startPort, CruiseStopVO startPortVO, PoiRS endPort, CruiseStopVO endPortVO) {
    cruise.setName(cruiseVO.getCruiseName());
    cruise.setDetailtypeid(ReservationType.CRUISE);
    cruise.setEndtimestamp(cruiseVO.getEndDate().getTime());
    cruise.setStarttimestamp(cruiseVO.getStartDate().getTime());

    StringBuilder sb = new StringBuilder();
    if (cruiseVO.getCruiseDescription() != null) {
      sb.append("Description: ");
      sb.append("\n");
      sb.append(cruiseVO.getCruiseDescription());
      sb.append("\n\n");
    }
    if (cruiseVO.getNote() != null) {
      sb.append("All Guests Receive: ");
      sb.append("\n");
      sb.append(cruiseVO.getNote().replaceAll("<br />","\n").replaceAll("<br>","\n"));
      sb.append("\n\n");
    }
    /*if (cruiseVO.getIncludedAmenities() != null) {
      sb.append(cruiseVO.getIncludedAmenities());
      sb.append("\n\n");
    }
    if (cruiseVO.getExcludedAmenities() != null) {
      sb.append(cruiseVO.getExcludedAmenities());
      sb.append("\n\n");
    }*/
    /*
    if (cruiseVO.getBenefits().size() > 0) {
      sb.append("Virtuoso Benefits:");

      for (Benefit bnft : cruiseVO.getBenefits()) {
        sb.append("\n");
        sb.append("- ");
        sb.append(bnft.getBenefitName().replaceAll("<br />","\n").replaceAll("<br>","\n"));
      }
      sb.append("\n\n");
    }
    */
    /*
    if (cruiseVO.getPromotions().size() > 0) {
      sb.append("Promotions:");

      for (Promotion promo : cruiseVO.getPromotions()) {
        sb.append("\n");
        sb.append("- ");
        sb.append(promo.getName().replaceAll("<br />","\n").replaceAll("<br>","\n"));
        sb.append("\n\t");
        sb.append(promo.getPromotionUrl());
      }
      sb.append("\n\n");
    }
    */

    cruise.setComments(sb.toString());

    if (startPort != null || startPortVO != null) {
      if (startPort != null) {
        cruise.setLocStartPoiId(startPort.getId());
        cruise.setLocStartPoiCmpyId(0);
      } else if (startPortVO != null) {
        cruise.setLocStartLat(startPortVO.getLatitude());
        cruise.setLocStartLong(startPortVO.getLongitude());
        cruise.setLocStartName(startPortVO.getName());
      }
    }

    if (endPort != null || endPortVO != null) {
      if (endPort != null) {
        cruise.setLocFinishPoiId(endPort.getId());
        cruise.setLocFinishPoiCmpyId(0);
      } else if (endPortVO != null) {
        cruise.setLocFinishLat(endPortVO.getLatitude());
        cruise.setLocFinishLong(endPortVO.getLongitude());
        cruise.setLocFinishName(endPortVO.getName());
      }
    }

    //let's try to find the ship
    if (cruiseVO.getCruiseShip() != null && cruiseVO.getCruiseShip().getName() != null && cruiseVO.getCruiseShip().getName().trim().length() > 0) {
      List<Integer> types = new ArrayList<>();
      types.add(6);
      Set<Integer> cmpyIds = new TreeSet();
      cmpyIds.add(0);

        List<PoiRS> cruiseShips = PoiController.findMergedByTerm(cruiseVO.getCruiseShip().getName(), types, cmpyIds, 1);
      PoiRS foundCruiseShip = null;
      if (cruiseShips != null) {
        for (PoiRS cruise1: cruiseShips) {
          if (cruise1.getName().toLowerCase().equals(cruiseVO.getCruiseShip().getName().toLowerCase())) {
            foundCruiseShip = cruise1;
          }
        }
      }


      if (foundCruiseShip != null) {
        cruise.setPoiId(foundCruiseShip.getId());
        cruise.setPoiCmpyId(0);
      }
    }

    cruise.setLastupdatedtimestamp(System.currentTimeMillis());
    cruise.setModifiedby("system");

    return cruise;
  }

  /*
    Match ports with the Shoretrips ports
   */
  @With({Authenticated.class, AdminAccess.class})
  public CompletionStage<Result> matchShoretrips() {
    BaseView view = new BaseView();
    WSRequest requestHolder =
        ws.url("https://www.shoretrips.com/integration/gateway/LOCATIONS?partner=UMAPPED&accesskey=IP-UM-20150612")
          .setRequestTimeout(50000l);


    return requestHolder.get().thenApply((WSResponse response) -> {
      org.w3c.dom.Document responseDoc = null;

      if (response.getStatus() == 200) {
        responseDoc = response.asXml();
        int found      = 0;
        int duplicates = 0;

        int      notFound = 0;
        NodeList list     = responseDoc.getElementsByTagName("Location");
        for (int i = 0; i < list.getLength(); i++) {
          Element m       = (Element) list.item(i);
          String  code    = m.getAttribute("Code");
          String  name    = m.getAttribute("Name");
          String  lat     = m.getAttribute("Lat");
          String  lng     = m.getAttribute("Lng");
          String  country = m.getAttribute("Country");

          List<PoiRS> ports = PoiMgr.findPortByNameorGPS(name,
                                                         Float.parseFloat(lat),
                                                         Float.parseFloat(lng),
                                                         (float) 0.2);
          PoiRS       port  = null;
          if (ports == null || ports.size() == 0) {
            notFound++;
            System.out.println("Port not found: " + code + " " + name + " " + lat + " " + lng + " " + country);
          }
          else {
            if (ports.size() > 1) {
              for (PoiRS rs : ports) {
                //get rid of accents for compare
                String rsName = Normalizer.normalize(rs.getName().trim().toLowerCase(), Normalizer.Form.NFD);
                rsName = rsName.replaceAll("[^\\p{ASCII}]", "");

                if (name.toLowerCase().contains(rsName)) {
                  port = rs;
                  break;
                }
              }
              if (port == null) {
                //System.out.println("Found more than one: " + code + " " +  name + " " + lat + " " + lng + " " +
                // country);
                boolean dupResolved = false;
                for (PoiRS rs : ports) {
                  //let's see if this port is in use
                  if (TmpltDetails.countByStartPoiId(rs.getId()) > 0) {
                    dupResolved = true;
                    if (rs.getCode() == null || rs.getCode().trim().length() == 0) {
                      rs.setCode(code);
                      rs.updateLastUpdatedTS("system");
                      PoiMgr.update(rs);
                    }
                  }
                }
                if (dupResolved) {
                  found++;
                }
                else {
                  Log.info("Found more than one: " + code + " " + name + " " + lat + " " + lng + " " + country);
                  duplicates++;
                }
              }
            }
            else {
              port = ports.get(0);
            }

            if (port != null) {
              found++;
              if (port.getCode() == null || port.getCode().trim().length() == 0) {
                port.setCode(code);
                port.updateLastUpdatedTS("system");
                PoiMgr.update(port);
              }
            }

            //          System.out.println("Port  found: " + code + " " +  name + " " + lat + " " + lng + " " + country);

          }

        }
        System.out.println("Found: " + found + " Not Found: " + notFound + " Duplicates: " + duplicates);
      }
      else {
        Log.err("service call failed. Status: " + response.getStatus());
      }
      view.message = "Cruise read: ";
      return ok(views.html.admin.cruise.cruiseMgmt.render(view));
    });

  }

  /*
   * Process all cruise stops VO and populate the corresponding TmpltDetail rec
   */
  public static TmpltDetails processCruiseStop (TmpltDetails cruiseStop, PoiRS port, CruiseStopVO portVO) {
    cruiseStop.setName(portVO.getName());
    if (portVO.getCruiseStop()) {
      cruiseStop.setDetailtypeid(ReservationType.CRUISE_STOP);
    } else {
      cruiseStop.setDetailtypeid(ReservationType.TOUR);
    }
    cruiseStop.setEndtimestamp(portVO.getEndTimestamp().getTime());
    cruiseStop.setStarttimestamp(portVO.getStartTimestamp().getTime());
    cruiseStop.setDayOffset(portVO.getDay());

    if (port != null || portVO != null) {
      if (port != null) {
        cruiseStop.setPoiId(null);
        cruiseStop.setPoiCmpyId(0);
        cruiseStop.setLocStartPoiId(port.getId());
        cruiseStop.setLocStartPoiCmpyId(0);
      } else if (portVO != null) {
        cruiseStop.setLocStartLat(portVO.getLatitude());
        cruiseStop.setLocStartLong(portVO.getLongitude());
        cruiseStop.setLocStartName(portVO.getName());
        cruiseStop.setPoiId(null);
        cruiseStop.setLocStartPoiId(null);
      }
    }

    StringBuilder sb = new StringBuilder();
    if (portVO.getNote() != null) {
      sb.append(portVO.getNote().replaceAll("<br />","\n").replaceAll("<br>","\n"));
      sb.append("\n\n");
    }
    if (portVO.getBenefits().size() > 0) {
      sb.append("Virtuoso Benefits:");

      for (Benefit bnft : portVO.getBenefits()) {
        sb.append("\n");
        sb.append("- ");
        sb.append(bnft.getBenefitName().replaceAll("<br />","\n").replaceAll("<br>","\n"));
      }
      sb.append("\n\n");
    }
    cruiseStop.setComments(sb.toString());
    cruiseStop.setLastupdatedtimestamp(System.currentTimeMillis());
    cruiseStop.setModifiedby("system");


    return cruiseStop;
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result feedTmpltMgmt(Integer srcId) {
    FeedTmpltView ftv = new FeedTmpltView();
    ftv.srcList = FeedSrc.findByType(FeedSrc.FeedType.TMPLT);

    if(srcId != null) {
      for(FeedSrc fs: ftv.srcList) {
        if (fs.getSrcId() == srcId.intValue()) {
          ftv.currSrc = fs;
        }
      }
    }

    if(ftv.currSrc == null && ftv.srcList.size() > 0) {
      ftv.currSrc = ftv.srcList.get(0);
    }
    return ok(views.html.admin.feed.tmpltMgmt.render(ftv));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result feedTmpltLoad(Integer srcId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Map<PoiFeedUtil.FeedResult, Integer> result = null;
    if(srcId != null) {
      try {

        FeedSrc src = FeedSrc.find.byId(srcId);
        if (src.getFeedType() == FeedSrc.FeedType.TMPLT) {
          FeedHelperTmplt helper = FeedHelperTmplt.build(sessionMgr.getAccount().get(), src);
          result = helper.load();
        }
      } catch (Exception e) {
        Log.err("Failed to instantiate loader class for the feed id:" + srcId, e);
        e.printStackTrace();
      }
    }

    if(result == null) {
      result = new HashMap<>();
      result.put(PoiFeedUtil.FeedResult.SUCCESS, 0);
      result.put(PoiFeedUtil.FeedResult.ERROR, 0);
    }

    return ok(Json.toJson(result));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result feedTmpltProcess(Integer srcId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    int loaded = 0;
    Map<String, Integer> results = new HashMap<>();
    if(srcId != null) {
      try {
        FeedSrc src = FeedSrc.find.byId(srcId);
        if (src.getFeedType() == FeedSrc.FeedType.TMPLT) {
          FeedHelperTmplt helper = FeedHelperTmplt.build(sessionMgr.getAccount().get(), src);
          helper.process();
        }
      } catch (Exception e) {
        Log.err("Failed to instantiate loader class for the feed id:" + srcId, e);
        e.printStackTrace();
      }
    }
    results.put("SUCCESS", loaded);
    return ok(Json.toJson(results));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result feedTmpltWipe(Integer srcId) {
    int deleted = 0;
    Map<String, Integer> results = new HashMap<>();
    if(srcId != null) {
      deleted = TmpltImport.deleteFromSrc(srcId);
    }
    results.put("DELETED", deleted);
    return ok(Json.toJson(results));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result feedTmpltCleanup(Integer srcId, String mode) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Map<PoiFeedUtil.FeedResult, Integer> result = new HashMap<>();
    result.put(PoiFeedUtil.FeedResult.SUCCESS, 0);
    result.put(PoiFeedUtil.FeedResult.ERROR, 0);

    try {
      PoiMgr.DeleteMode deleteMode = PoiMgr.DeleteMode.valueOf(mode);
      FeedHelperTmplt helper = FeedHelperTmplt.build(sessionMgr.getAccount().get(), srcId);
      if(helper != null) {
        result = helper.cleanup(deleteMode);
      }
    }
    catch (IllegalArgumentException e) {
      Log.err("feedTmpltCleanup(): Wrong mode:" + mode);
    }

    return ok(Json.toJson(result));
  }



  /**
   * POI De-Duplication Administration
   * @param poiType
   * @return
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result poiDedup(Integer poiType) {
    return TODO;
  }


  /**
   * For extreme cases - request to perform gc on this particular Play instance
   * @return
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result gc() {
    Utils.gc();
    Utils.gc();
    return UserMessage.message(ReturnCode.SUCCESS, "Garbage collection requested");
  }


  /**
   * Messenger maintenance page
   * @return
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result messenger() {

    MessengerView mv = new MessengerView();
    return ok(views.html.admin.messenger.render(mv));
  }

  /**
   * Messenger maintenance page
   * @return
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result messengerState() {
    BackgroundJobStateView bjsv = BackgroundJobStateView.getFromCache(MaintenanceActor.Protocol
                                                                          .Command.CLEAN_MESSENGER.name());
    bjsv.stateUrl = routes.AdminController.messengerState().url();
    bjsv.enablePrettyPrint(); //For now let's look pretty
    if(!bjsv.isUpdatedInTheLastMinutes(1)) {
      bjsv.stop();
    }
    return bjsv.toJsonResult();
  }

  /**
   * Messenger Conferences
   * @return
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result messengerConferencesDT() {
    JsonNode jsonRequest = request().body().asJson();
    DataTablesForm dtRequest = Json.fromJson(jsonRequest, DataTablesForm.class);

    class ConferenceRow {
      public String              tripId;
      public String              trip;
      public String              tripStartDate;
      public String              tripEndDate;
      public Boolean             removed;
      public String              removedDate;
      public Integer             roomCount;
      public Integer             msgCount;
      public Integer             userCount;
      public Map<String, String> actions;
      public ConferenceRow() {
        actions = new TreeMap<>();
      }
    }

    DataTablesView<ConferenceRow> dtv = new DataTablesView<>(dtRequest.draw);
    dtv.recordsTotal    = MessengerConference.getRowCount();
    dtv.recordsFiltered = MessengerConference.getRowCountFiltered(dtRequest);

    List<MessengerConference> recs = MessengerConference.getPage(dtRequest);

    for (MessengerConference up : recs) {
      ConferenceRow row = new ConferenceRow();
      row.tripId = up.getTripid();
      Trip t = Trip.findByPK(up.getTripid());
      row.trip = t.getName();
      row.tripStartDate = Utils.getISO8601Date(t.getStarttimestamp());
      row.tripEndDate = Utils.getISO8601Date(t.getEndtimestamp());
      row.removed = up.isRemoved();
      row.removedDate = Utils.getISO8601Date(up.getRemovedTs());
      row.roomCount = up.getRoomCount();
      row.msgCount = up.getMsgCount();
      row.userCount = up.getUsersCount();
      row.actions.put("Manage", ""); //TODO: Add preview/manage content to get full record
      dtv.data.add(row);
    }
    return  ok(Json.toJson(dtv));
  }

  /**
   * Starts messenger cleanup maintenance procedure
   * @return
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result messengerCleanup(){
    SessionMgr sessionMgr = (SessionMgr) ctx().args.get(SessionMgr.CONTEXT_KEY);
    MaintenanceActor.Protocol cmd = MaintenanceActor.Protocol.build(MaintenanceActor.Protocol.Command.CLEAN_MESSENGER);
    cmd.msg.accountId = sessionMgr.getAccountId();

    ActorsHelper.tell(SupervisorActor.UmappedActor.MAINTENANCE, cmd);

    return redirect(routes.AdminController.messengerState());
  }


  /**
   * Billing reports for users who are being billed directly
   * @return
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result billingCCAudit() {
    BaseView bv = new BaseView();
    return ok(views.html.admin.billingReportsCCAudit.render(bv));
  }


  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, AdminAccess.class})
  public Result billingCCAuditDT() {
    JsonNode jsonRequest = request().body().asJson();
    DataTablesForm dtRequest = Json.fromJson(jsonRequest, DataTablesForm.class);



    class UBRow {
      public String txnId;
      public String txnType;
      public String plan;
      public String processor;
      public String invoiceUser;
      public String billedUser;
      public String status;
      public String msg;
      public String createdBy;
      public String timestamp;

      public UBRow() {
      }
    }

    DataTablesView<UBRow> dtv = new DataTablesView<>(dtRequest.draw);
    dtv.recordsTotal    = BillingCCAudit.getRowCount();
    dtv.recordsFiltered = dtv.recordsTotal;

    Account account = null;
    Long txnId = null;
    if (dtRequest.search.value  != null && !dtRequest.search.value.isEmpty()) {
      account = Account.findActiveByLegacyId(dtRequest.search.value);
      if (account != null) {
        dtv.recordsFiltered = BillingCCAudit.getRowCountFiltered(account);
      } else {
        try {
          if (account == null && Long.parseLong(dtRequest.search.value) > 0) {
            txnId = Long.decode(dtRequest.search.value);
            dtv.recordsFiltered = BillingCCAudit.getRowCountFiltered(txnId);
          }
        } catch (Exception e) {

        }
      }
    }



    List<BillingCCAudit> recs = BillingCCAudit.getAudit(dtRequest.start, dtRequest.length, account, txnId);

    HashMap<Long, Account> accountCache = new HashMap<>();
    HashMap<Long, BillingPlan> planCache = new HashMap<>();

    for (BillingCCAudit audit : recs) {
      if (!accountCache.containsKey(audit.getCreatedBy())) {
        accountCache.put(audit.getCreatedBy(), Account.find.byId(audit.getCreatedBy()));
      }
      if (!accountCache.containsKey(audit.getBilledUid())) {
        accountCache.put(audit.getBilledUid(), Account.find.byId(audit.getBilledUid()));
      }
      if (!accountCache.containsKey(audit.getInvoiceUid())) {
        accountCache.put(audit.getInvoiceUid(), Account.find.byId(audit.getInvoiceUid()));
      }
      if (audit.getPlanId() != null && !planCache.containsKey(audit.getPlanId())) {
        planCache.put(audit.getPlanId(), BillingPlan.find.byId(audit.getPlanId()));
      }


      UBRow row = new UBRow();
      row.txnId = audit.getTxnId().toString();
      row.txnType = audit.getTxnType().name();
      if (audit.getPlanId() != null && planCache.containsKey(audit.getPlanId())) {
        row.plan = planCache.get(audit.getPlanId()).getName() + " " + audit.getPlanId() ;
      }
      row.processor = audit.getCcProcessor().name();
      row.invoiceUser = accountCache.get(audit.getInvoiceUid()).getLegacyId();
      row.billedUser = accountCache.get(audit.getBilledUid()).getLegacyId();
      row.createdBy = accountCache.get(audit.getCreatedBy()).getLegacyId();
      row.timestamp = Utils.formatTimestampPrint(audit.getCreatedTs().getTime());
      row.msg = audit.getGatewayResp();
      row.status = audit.getStatus().name();

      dtv.data.add(row);
    }

    return  ok(Json.toJson(dtv));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result fixCruiseStartLoc() {
    long ts = Instant.now().getMillis();
    List<TmpltImport> imports = TmpltImport.findByTmpltTypeAndGtStartTS(TmpltType.Type.CRUISE_VIRTUOSO, ts);
    for (TmpltImport ti: imports) {
      try {
        ObjectMapper mapper = new ObjectMapper();
        com.fasterxml.jackson.databind.ObjectReader r = mapper.reader(CruiseVO.class);
        CruiseVO cruiseVO = r.readValue(ti.getRaw());
        List<Integer> types = new ArrayList<>();
        types.add(7);
        Set<Integer> cmpies = new TreeSet<>();
        cmpies.add(0);
        if (cruiseVO != null && cruiseVO.getStartLocation() != null && !cruiseVO.getStartLocation().isEmpty()) {
          List<TmpltDetails> getCruises = TmpltDetails.findByTemplateIdType(ti.getTmpltId(), ReservationType.CRUISE);
          if (getCruises != null && getCruises.size() == 1) {
            TmpltDetails t = getCruises.get(0);


            if (t.getLocStartName() != null && !t.getLocStartName().equals(cruiseVO.getStartLocation()) && t.getLocStartPoiId() == null) {
              Log.info("fixCruiseStartLoc - processing tmpltId: " + ti.getImportId(), " template: " + t.getTemplateid() + " Cruise start: " + cruiseVO.getStartLocation() + " Template: " + t.getLocStartName());

              t.setLocStartName(cruiseVO.getStartLocation());
              t.setLocStartLat(0.0f);
              t.setLocStartLong(0.0f);
              t.setLocStartPoiCmpyId(0);
              t.setLocStartPoiId(null);
              t.setLocFinishName(cruiseVO.getEndLocation());
              t.setLocFinishLat(0.0f);
              t.setLocFinishLong(0.0f);
              t.setLocFinishPoiCmpyId(0);
              t.setLocFinishPoiId(null);

              List<PoiRS> pois = PoiController.findMergedByTerm(cruiseVO.getStartLocation(), types,cmpies,2);
              if (pois != null) {
                for (PoiRS poi: pois) {
                  if (cruiseVO.getStartLocation().contains(poi.getName())) {
                    Log.info("fixCruiseStartLoc - Start Loc: " + cruiseVO.getStartLocation() + " Poi: " + poi.getName());
                    t.setLocStartPoiId(poi.getId());
                    if (poi.getMainAddress() != null && poi.getMainAddress().getCoordinates() != null) {
                      t.setLocStartLong(poi.getMainAddress().getCoordinates().getLongitude());
                      t.setLocStartLat(poi.getMainAddress().getCoordinates().getLatitude());
                    }
                  }
                }
              }

              if (cruiseVO.getEndLocation() != null && !cruiseVO.getEndLocation().isEmpty()) {
                pois = PoiController.findMergedByTerm(cruiseVO.getEndLocation(), types, cmpies, 2);
                if (pois != null) {
                  for (PoiRS poi : pois) {
                    if (cruiseVO.getEndLocation().contains(poi.getName())) {
                      Log.info("fixCruiseStartLoc - End Loc: " + cruiseVO.getEndLocation() + " Poi: " + poi.getName());
                      t.setLocFinishPoiId(poi.getId());
                      if (poi.getMainAddress() != null && poi.getMainAddress().getCoordinates() != null) {
                        t.setLocFinishLong(poi.getMainAddress().getCoordinates().getLongitude());
                        t.setLocFinishLat(poi.getMainAddress().getCoordinates().getLatitude());
                      }
                    }
                  }
                }
              }
            }
            t.setModifiedby("system");
            t.setLastupdatedtimestamp(ts);
            t.save();
          }

        }
      } catch (Exception e) {

      }

    }

    return ok("ok");
  }
}