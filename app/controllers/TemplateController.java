package controllers;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.AutocompleteSearchForm;
import com.mapped.publisher.form.TemplateSearchForm;
import com.mapped.publisher.form.TripGuideForm;
import com.mapped.publisher.form.template.*;
import com.mapped.publisher.js.BaseJsonResponse;
import com.mapped.publisher.js.BookingDetailsJson;
import com.mapped.publisher.js.TemplateJson;
import com.mapped.publisher.parse.schemaorg.ActivityReservation;
import com.mapped.publisher.persistence.*;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;
import com.mapped.publisher.view.migration.FlightMigrationViewUtil;
import com.umapped.api.schema.types.Address;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.UmReservation;
import com.umapped.persistence.reservation.accommodation.UmAccommodationReservation;
import com.umapped.persistence.reservation.activity.UmActivityReservation;
import com.umapped.persistence.reservation.cruise.UmCruiseReservation;
import com.umapped.persistence.reservation.flight.UmFlightReservation;
import com.umapped.persistence.reservation.transfer.UmTransferReservation;

import models.publisher.*;
import models.publisher.utils.reservation.migration.UmFlightReservationBuilder;

import org.joda.time.LocalDate;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

import static controllers.ImageController.uploadHelper;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-07-24
 * Time: 2:43 PM
 */
public class TemplateController
    extends Controller {

  @Inject
  FormFactory formFactory;

  private FlightMigrationViewUtil fltMigrationViewUtil = new FlightMigrationViewUtil();
  
  @With({Authenticated.class, Authorized.class})
  public Result autocompleteSearchList() {SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Form<AutocompleteSearchForm> form = formFactory.form(AutocompleteSearchForm.class).bindFromRequest();
    //use bean validator framework, if errors returning nothing on this request
    if(form.hasErrors()) {
      return badRequest();
    }

    AutocompleteSearchForm templateSearchForm = form.get();

    List<String> cmpies = new ArrayList<>();

    //use companies from the user's session
    cmpies.add(sessionMgr.getCredentials().getCmpyId());

    List<Template> privateTemplates = Template.findMyTemplates(cmpies,
                                                               sessionMgr.getUserId(),
                                                               templateSearchForm.term.trim());

    List<Template> publicTemplates = Template.findPublicTemplates(cmpies,
                                                                  templateSearchForm.term.trim(),
                                                                  sessionMgr.getUserId());


    ObjectNode result = Json.newObject();
    ArrayNode array = result.arrayNode();

    for (Template tmplt : privateTemplates) {
      ObjectNode classNode = Json.newObject();
      classNode.put("label", tmplt.getName());
      classNode.put("id", tmplt.getTemplateid());
      array.add(classNode);
    }

    for (Template tmplt : publicTemplates) {
      ObjectNode classNode = Json.newObject();
      classNode.put("label", tmplt.getName());
      classNode.put("id", tmplt.getTemplateid());
      array.add(classNode);
    }

    return ok(array);
  }


  @With({Authenticated.class, Authorized.class})
  public Result searchTemplate() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    TemplateSearchView view = new TemplateSearchView();
    view.message = msg;
    view.myTemplates = new ArrayList<TemplateView>();
    view.publicTemplates = new ArrayList<TemplateView>();

    //bind html form to form bean
    Form<TemplateSearchForm> templateForm = formFactory.form(TemplateSearchForm.class);
    TemplateSearchForm templateSearchForm = templateForm.bindFromRequest().get();
    view.tripId = templateSearchForm.getInTripId();

    List<String> cmpies = new ArrayList<String>();
    HashMap<String, String> agencyList = new HashMap<String, String>();
    Company c = null;
    if (templateSearchForm != null &&
        templateSearchForm.getInCmpyId() != null &&
        !templateSearchForm.getInCmpyId().equalsIgnoreCase("All")) {
      c = Company.find.byId(templateSearchForm.getInCmpyId());
      if (c != null && SecurityMgr.canAccessCmpy(c, sessionMgr)) {
        view.cmpyId = templateSearchForm.getInCmpyId();
        cmpies.add(c.getCmpyid());
        agencyList.put(c.cmpyid, c.name);

      }
      else {
        //use companies from the user's session
        Credentials cred = sessionMgr.getCredentials();
        if(cred.hasCompany()) {
          cmpies.add(cred.getCmpyId());
          agencyList.put(cred.getCmpyId(),cred.getCmpyName());
        }
      }
    }


    if (templateSearchForm.getInSearchTerm() != null &&
        templateSearchForm.getInSearchTerm().trim().length() > 0 && c != null) {
      if (cmpies.size() > 0) {
        List<Template> l1 = Template.findMyTemplates(cmpies,
                                                     sessionMgr.getUserId(),
                                                     templateSearchForm.getInSearchTerm().trim());
        if (l1 != null) {
          for (Template t : l1) {
            TemplateView v = TemplateController.buildTemplateView(t, sessionMgr, agencyList);
            if (v != null) {
              view.myTemplates.add(v);
            }
          }
        }

        List<Template> l2 = Template.findPublicTemplates(cmpies,
                                                         templateSearchForm.getInSearchTerm().trim(),
                                                         sessionMgr.getUserId());
        if (l2 != null) {
          for (Template t : l2) {
            TemplateView v = TemplateController.buildTemplateView(t, sessionMgr, agencyList);
            if (v != null) {
              view.publicTemplates.add(v);
            }
          }
        }
      }
    }
    return ok(views.html.template.templatesSearch.render(view));
  }

  public static TemplateView buildTemplateView(Template t, SessionMgr sessionMgr, HashMap<String, String> cmpyList) {
    TemplateView view = new TemplateView();
    view.templateId = t.getTemplateid();
    view.templateName = t.getName();
    view.templateNote = t.getDescription();
    view.templateSharing = t.getVisibility();
    view.templateStatus = t.getStatus();
    view.canEdit = SecurityMgr.canEditTemplate(t, sessionMgr);
    view.tripAgency = cmpyList.get(t.cmpyid);
    view.userId = t.createdby;
    return view;
  }

  @With({Authenticated.class, Authorized.class})
  public Result newTemplate() {
    return template(null);
  }

  @With({Authenticated.class, Authorized.class})
  public Result template(String templateId) {
    TemplateView view = new TemplateView();
    view.templateSharing = Template.Visibility.PRIVATE;

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    HashMap<String, String> agencyList = new HashMap<>();
    Credentials cred = sessionMgr.getCredentials();
    if(cred.hasCompany()) {
      agencyList.put(cred.getCmpyId(), cred.getCmpyName());
    }

    view.tripAgencyList = agencyList;

    if (templateId != null) {
      Template template = Template.find.byId(templateId);
      if (template != null && (template.status == APPConstants.STATUS_DELETED || !SecurityMgr.canEditTemplate(template,
                                                                                                              sessionMgr))) {
        BaseView baseView = new BaseView();
        baseView.message = "Cannot access this template.";
        return ok(views.html.common.message.render(baseView));
      }
      else if (template != null && SecurityMgr.canAccessTemplate(template,
                                                                 sessionMgr) && template.status != APPConstants.STATUS_DELETED) {
        view.templateId = template.templateid;
        view.templateName = template.name;
        view.templateNote = template.description;
        view.templateSharing = template.visibility;
        view.templateStatus = template.status;
        view.duration = template.getDuration();
        view.coverUrl = template.getImageUrl();
        view.canEdit = SecurityMgr.canEditTemplate(template, sessionMgr);
        sessionMgr.setParam(SessionConstants.SESSION_PARAM_TRIPID, template.templateid);

        Company c = Company.find.byId(template.cmpyid);
        view.tripAgency = c.name;
        view.tripAgencyId = c.cmpyid;
      }
    }
    return ok(views.html.template.template.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result newFromTrip(String tripId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Credentials cred = sessionMgr.getCredentials();

    final DynamicForm form = formFactory.form().bindFromRequest();
    String templateName = form.get("inTemplateName");

    if (tripId == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Missing parameters";
      return ok(views.html.common.message.render(baseView));
    }

    Trip tripModel = Trip.find.byId(tripId);
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);

    if (accessLevel.lt(SecurityMgr.AccessLevel.OWNER)) {
      BaseView baseView = new BaseView();
      baseView.message = "Cannot access this trip.";
      return ok(views.html.common.message.render(baseView));
    }

    List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
    if (tripDetails == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Trip has no bookings to create template from.";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      Ebean.beginTransaction();
      Template template = Template.buildTemplate(sessionMgr.getUserId());

      template.setCmpyid(cred.getCmpyId());

      template.setName(templateName);
      template.setDescription(tripModel.getComments());
      template.setVisibility(Template.Visibility.PRIVATE);
      if (tripModel.getFileImage() != null) {
        template.setFileImage(tripModel.getFileImage());
      } else {
        template.setCoverFilename(tripModel.getCoverfilename());
        template.setCoverUrl(tripModel.getImageUrl());
      }
      template.setDuration(Utils.getDaysBetween(tripModel.getStarttimestamp(), tripModel.getEndtimestamp()));
      template.save();

      for (TripDetail tripDetail : tripDetails) {
        TmpltDetails tmpltDetails = TmpltDetails.buildTmpltDetails(sessionMgr.getUserId());
        tmpltDetails.setTemplateid(template.getTemplateid());
        tmpltDetails.setDetailtypeid(tripDetail.getDetailtypeid());
        tmpltDetails.setStarttimestamp(Utils.extractHoursMinutes(tripDetail.getStarttimestamp()));
        tmpltDetails.setEndtimestamp(Utils.extractHoursMinutes(tripDetail.getEndtimestamp()));
        tmpltDetails.setDuration(Utils.getDaysBetween(tripDetail.getStarttimestamp(), tripDetail.getEndtimestamp()));
        tmpltDetails.setDayOffset(Utils.getDaysBetween(tripModel.getStarttimestamp(), tripDetail.getStarttimestamp()));

        tmpltDetails.setName(tripDetail.getName());
        tmpltDetails.setPoiCmpyId(tripDetail.getPoiCmpyId());
        tmpltDetails.setPoiId(tripDetail.getPoiId());

        tmpltDetails.setLocStartLat(tripDetail.getLocStartLat());
        tmpltDetails.setLocStartLong(tripDetail.getLocStartLong());
        tmpltDetails.setLocStartName(tripDetail.getLocStartName());
        tmpltDetails.setLocStartPoiId(tripDetail.getLocStartPoiId());
        tmpltDetails.setLocStartPoiCmpyId(tripDetail.getLocStartPoiCmpyId());

        tmpltDetails.setLocFinishLat(tripDetail.getLocFinishLat());
        tmpltDetails.setLocFinishLong(tripDetail.getLocFinishLong());
        tmpltDetails.setLocFinishName(tripDetail.getLocFinishName());
        tmpltDetails.setLocFinishPoiId(tripDetail.getLocFinishPoiId());
        tmpltDetails.setLocFinishPoiCmpyId(tripDetail.getLocFinishPoiCmpyId());

        ReservationType reservationType = tripDetail.getDetailtypeid().getRootLevelType();
        
        tmpltDetails.setReservation(tripDetail.getReservation());
        
        switch (reservationType) {
          case FLIGHT:
            // remove the passenger info
            UmFlightReservation flt = cast(tmpltDetails.getReservation(), UmFlightReservation.class);
            if (flt !=null) {
              flt.setTravelers(null);
            }
            break;
          default:
            // Nothing for now
            break;
        }
        tmpltDetails.save();
      }

      List<TripDestination> tripDestinations = TripDestination.getActiveByTripId(tripId);
      for (TripDestination dest : tripDestinations) {
        TmpltGuide tmpltGuide = TmpltGuide.buildGuide(template.getTemplateid(), sessionMgr.getUserId());
        tmpltGuide.setDestinationid(dest.getDestinationid());
        tmpltGuide.save();
      }

      Ebean.commitTransaction();
      return template(template.getTemplateid());
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      e.printStackTrace();
      BaseView baseView = new BaseView();
      baseView.message = "System Failure. Conversion of this trip has failed.";
      return ok(views.html.common.message.render(baseView));
    }
  }


  /**
   * Helper function to build poi objects for each of the booking types
   *
   * @param tmpltModel
   * @param detail
   * @param poiIdMain
   * @param poiNameMain
   * @param poiIdStart
   * @param poiNameStart
   * @param poiIdFinish
   * @param poiNameFinish
   * @param cred
   * @return
   */
  public static List<PoiRS> buildTripDetailPois(Template tmpltModel,
                                                TmpltDetails detail,
                                                Long poiIdMain,
                                                String poiNameMain,
                                                Long poiIdStart,
                                                String poiNameStart,
                                                Long poiIdFinish,
                                                String poiNameFinish,
                                                Credentials cred) {
    List<PoiRS> result = new ArrayList<>(3);
    Set<Integer> userCmpies = new HashSet<>();
    userCmpies.add(cred.getCmpyIdInt());

    Integer cmpIdToUse = cred.getCmpyIdInt();
    PoiRS poiMain = PoiController.getMergedPoi(poiIdMain, userCmpies);
    PoiRS poiStart = PoiController.getMergedPoi(poiIdStart, userCmpies);
    PoiRS poiFinish = PoiController.getMergedPoi(poiIdFinish, userCmpies);

    if (poiMain != null) {
      detail.setPoiCmpyId(cmpIdToUse);
      detail.setPoiId(poiMain.getId());
      detail.setLocStartLat(poiMain.locLat);
      detail.setLocStartLong(poiMain.locLong);
      detail.setName(poiMain.getName());
    }
    else {
      detail.setName(poiNameMain);
      detail.setPoiId(null);
      detail.setPoiCmpyId(null);
      detail.setLocStartLat(0);
      detail.setLocStartLong(0);
    }

    if (poiStart != null) {
      detail.setLocStartPoiCmpyId(cmpIdToUse);
      detail.setLocStartPoiId(poiStart.getId());
      detail.setLocStartName(poiStart.getName());
      detail.setLocStartLat(poiStart.locLat);
      detail.setLocStartLong(poiStart.locLong);
    }
    else {
      detail.setLocStartName(poiNameStart);
      detail.setLocStartPoiId(null);
      detail.setLocStartPoiCmpyId(null);
    }

    if (poiFinish != null) {
      detail.setLocFinishPoiCmpyId(cmpIdToUse);
      detail.setLocFinishPoiId(poiFinish.getId());
      detail.setLocFinishName(poiFinish.getName());
      detail.setLocFinishLat(poiFinish.locLat);
      detail.setLocFinishLong(poiFinish.locLong);
    }
    else {
      detail.setLocFinishName(poiNameFinish);
      detail.setLocFinishPoiId(null);
      detail.setLocFinishPoiCmpyId(null);
      detail.setLocFinishLat(0);
      detail.setLocFinishLong(0);
    }

    result.add(0, poiMain);
    result.add(1, poiStart);
    result.add(2, poiFinish);

    return result;
  }


  //POST METHODS
  @With({Authenticated.class, Authorized.class})
  public Result updateTemplate() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<FormUpdateTemplate> form = formFactory.form(FormUpdateTemplate.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }
    FormUpdateTemplate       templateInfo     = form.get();
    Company c = Company.find.byId(templateInfo.inTemplateCmpyId);
    if (c == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Invalid company - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }
    if (!SecurityMgr.canAccessCmpy(c, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "Unauthorized access";
      return ok(views.html.common.message.render(baseView));
    }

    Template template = null;
    if (templateInfo.inTemplateId == null || templateInfo.inTemplateId.trim().length() == 0) {
      template = Template.buildTemplate(sessionMgr.getUserId());
    }
    else {
      template = Template.find.byId(templateInfo.inTemplateId);
    }

    template.setName(templateInfo.inTemplateName);
    template.setDescription(templateInfo.inTemplateNote);
    template.setCmpyid(c.cmpyid);
    template.setLastupdatedtimestamp(System.currentTimeMillis());
    template.setModifiedby(sessionMgr.getUserId());

    template.setVisibility(templateInfo.inTemplateShare);
    template.setDuration(templateInfo.inDuration);

    template.save();
    sessionMgr.setParam(SessionConstants.SESSION_PARAM_TRIPID, template.templateid);

    return redirect(routes.TemplateController.bookings(template.getTemplateid(), new TabType.Bound(TabType.FLIGHTS),
                                                     null, false));
  }

  @With({Authenticated.class, Authorized.class})
  public Result bookingsJson(String tmpltId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Template template = Template.find.byId(tmpltId);
    if (template == null || !SecurityMgr.canAccessTemplate(template, sessionMgr)) {
      BaseJsonResponse bj = new BaseJsonResponse(ReturnCode.AUTH_TEMPLATE_FAIL);
      return ok(bj.toJson());
    }

    Company company = Company.find.byId(template.cmpyid);

    TemplateJson tbj = new TemplateJson(template.getTemplateid(), template.getName());
    tbj.cmpyId = company.cmpyid;
    tbj.cmpyName = company.name;
    tbj.canEdit = SecurityMgr.canEditTemplate(template, sessionMgr);

    List<TmpltDetails> details = TmpltDetails.findByTemplateId(template.templateid);
    for (TmpltDetails detail : details) {
      BookingDetailsJson jsonDetails = new BookingDetailsJson();
      jsonDetails.type = detail.getDetailtypeid();
      jsonDetails.rootType = detail.getDetailtypeid().getRootLevelType();
      jsonDetails.tmpltDetailId = detail.getDetailsid();
      jsonDetails.dayOffset = detail.getDayOffset();
      jsonDetails.duration = detail.getDuration();
      jsonDetails.cssClassName = ViewUtils.getClassTypeForBookingType(detail.getDetailtypeid());
      jsonDetails.timeStart = Utils.getTimeString(detail.starttimestamp);
      jsonDetails.timeEnd = Utils.getTimeString(detail.endtimestamp);
      jsonDetails.typeLabel = detail.getDetailtypeid().getLabel();
      jsonDetails.detailsEditUrl = controllers.routes.TemplateController.bookings(tmpltId,
                                                                                  new TabType.Bound(TabType.fromBookingType(
                                                                                      detail.getDetailtypeid())),
                                                                                  detail.getDetailsid(),
                                                                                  false).url();

      switch (detail.detailtypeid.getRootLevelType()) {
        case FLIGHT:
          UmFlightReservation flightReservation = cast(detail.getReservation(), UmFlightReservation.class);
          
          jsonDetails.name = flightReservation != null ? flightReservation.getFlight().flightNumber : "";
          break;
        case CRUISE:
          UmCruiseReservation cruiseReservation = cast(detail.getReservation(), UmCruiseReservation.class);
          jsonDetails.name = cruiseReservation != null ? cruiseReservation.getCruise().name : "";
          break;
        case HOTEL:
          UmAccommodationReservation hotelReservation = cast(detail.getReservation(), UmAccommodationReservation.class);          
          jsonDetails.name = hotelReservation != null ? hotelReservation.getAccommodation().name : "";
          break;
        case ACTIVITY:
          UmActivityReservation activitiyReservation = cast(detail.getReservation(), UmActivityReservation.class);
          jsonDetails.name = activitiyReservation != null ? activitiyReservation.getActivity().name : "";
          break;
        case TRANSPORT:
          UmTransferReservation transportReservation = cast(detail.getReservation(), UmTransferReservation.class);

          jsonDetails.name = transportReservation != null ? transportReservation.getTransfer().getProvider().name : "";
          break;
      }
      tbj.addBookingDetails(jsonDetails);
    }

    return ok(tbj.toJson());
  }


  @With({Authenticated.class, Authorized.class}) @BodyParser.Of(BodyParser.Json.class)
  public Result updateBookingsJson(String tmpltId) {
    SessionMgr sessionMgr = new SessionMgr(session());
    Template template = Template.find.byId(tmpltId);
    if (template == null || !SecurityMgr.canAccessTemplate(template, sessionMgr)) {
      BaseJsonResponse bj = new BaseJsonResponse(ReturnCode.AUTH_TEMPLATE_FAIL);
      return ok(bj.toJson());
    }

    try {
      JsonNode json = request().body().asJson();

      Iterator<JsonNode> itrN = json.elements();
      while (itrN.hasNext()) {
        JsonNode n = itrN.next();
        String detailsId = n.findValue("tmpltDetailId").textValue();
        String timeStart = n.findValue("timeStart").textValue();
        String timeEnd = n.findValue("timeEnd").textValue();
        int duration = n.findValue("duration").intValue();
        int dayOffset = n.findValue("dayOffset").intValue();
        TmpltDetails tmpltDetails = TmpltDetails.find.byId(detailsId);
        if (tmpltDetails == null) {
          BaseJsonResponse bj = new BaseJsonResponse(ReturnCode.DB_RECORD_NOT_FOUND).withMsg("Booking does not exist");
          return ok(bj.toJson());
        }

        if (true && (tmpltDetails.getDuration() != duration ||
                     tmpltDetails.getDayOffset() != dayOffset ||
                     tmpltDetails.getStarttimestamp() != Utils.getMilliSecs(timeStart) ||
                     tmpltDetails.getEndtimestamp() != Utils.getMilliSecs(timeEnd))) {
          Log.log(LogLevel.DEBUG,
                  "====================================================================================");
          Log.log(LogLevel.DEBUG, "Duration  before: " + tmpltDetails.getDuration() + " after: " + duration);
          Log.log(LogLevel.DEBUG, "DayOffset before: " + tmpltDetails.getDayOffset() + " after: " + dayOffset);
          Log.log(LogLevel.DEBUG,
                  "Start     before: " + tmpltDetails.getStarttimestamp() + " after: " + Utils.getMilliSecs(timeStart));
          Log.log(LogLevel.DEBUG,
                  "End       before: " + tmpltDetails.getEndtimestamp() + " after: " + Utils.getMilliSecs(timeEnd));
        }
        tmpltDetails.setDuration(duration);
        tmpltDetails.setDayOffset(dayOffset);
        tmpltDetails.setStarttimestamp(Utils.getMilliSecs(timeStart));
        tmpltDetails.setEndtimestamp(Utils.getMilliSecs(timeEnd));
        tmpltDetails.update();
      }

      BaseJsonResponse bj = new BaseJsonResponse().withMsg("Template successfully updated");
      return ok(bj.toJson());
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "Error while handling JSON request");
      e.printStackTrace();
      BaseJsonResponse bj = new BaseJsonResponse(ReturnCode.HTTP_REQ_FAIL).withMsg(
          "System Error. Unable to handle browser request.");
      return ok(bj.toJson());
    }
  }


  /**
   * POST
   */
  @With({Authenticated.class, Authorized.class})
  public Result preview(String templateId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Template template = Template.find.byId(templateId);
    if (template == null || !SecurityMgr.canAccessTemplate(template, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "Unauthorized Access";
      return ok(views.html.common.message.render(baseView));
    }

    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    BaseView view = new BaseView();
    view.message = msg;
    return ok(views.html.template.preview.render(view, template));
  }

  @With({Authenticated.class, Authorized.class})
  public Result bookings(String tmpltId, TabType.Bound activeTab, String tmpltDetailsIdId, boolean isModal) {
    TemplateBookingView view = new TemplateBookingView();
    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    view.message = msg;

    SessionMgr sessionMgr = new SessionMgr(session());
    final DynamicForm form = formFactory.form().bindFromRequest();

    String tripId = form.get("inTripId"); //TODO: Serguei: Figure out where this comes from

    //check for active tab
    view.activeTab = TabType.fromString(flash(SessionConstants.SESSION_PARAM_ACTIVE_TAB));
    if (view.activeTab == TabType.ITINERARY) {
      view.activeTab = activeTab.value();
    }

    Template template = Template.find.byId(tmpltId);
    if (template == null || !SecurityMgr.canAccessTemplate(template, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "Unauthorized Access";
      return ok(views.html.common.message.render(baseView));
    }
    Company c = Company.find.byId(template.cmpyid);
    view.templateId = template.templateid;
    view.templateName = template.name;
    view.canEdit = SecurityMgr.canEditTemplate(template, sessionMgr);
    view.cmpyId = c.cmpyid;
    view.cmpyName = c.name;

    //check for edit
    if (tmpltDetailsIdId != null && tmpltDetailsIdId.length() > 0) {
      TmpltDetails templateDetail = TmpltDetails.find.byId(tmpltDetailsIdId);
      if (templateDetail != null &&
          templateDetail.status == APPConstants.STATUS_ACTIVE &&
          templateDetail.templateid.equals(tmpltId)) {
        TripBookingDetailView editDetailView = TemplateController.buildBookingView(templateDetail);
        if (editDetailView != null) {
          view.bookingDetail = editDetailView;
        }
 
        if (templateDetail.getDetailtypeid().getRootLevelType() == ReservationType.FLIGHT) {
          UmFlightReservation flight = cast(templateDetail.getReservation(), UmFlightReservation.class);
          if (flight != null) {
            editDetailView.origComments = flight.getNotesPlainText();
          }
        }
        //change the active tab
        view.activeTab = TabType.fromBookingType(editDetailView.detailsTypeId);
      }
    }

    List<TmpltDetails> details = TmpltDetails.findByTemplateId(template.templateid);
    for (TmpltDetails detail : details) {
      TripBookingDetailView detailView = TemplateController.buildBookingView(detail);
      switch (detail.detailtypeid.getRootLevelType()) {
        case FLIGHT:
          view.flights.add(detailView);
          break;
        case CRUISE:
          view.cruises.add(detailView);
          break;
        case HOTEL:
          view.hotels.add(detailView);
          break;
        case ACTIVITY:
          view.activities.add(detailView);
          break;
        case TRANSPORT:
          view.transfers.add(detailView);
          break;
      }
    }

    if (isModal) {
      //get the guides for modal view
      List<TmpltGuide> guides = TmpltGuide.findByTemplate(template.templateid);
      if (guides != null && guides.size() > 0) {
        HashMap<Integer, String> destinationTypeDesc = new HashMap<>();
        List<DestinationType> destinationTypes = DestinationType.findActive();
        if (destinationTypes != null) {
          for (DestinationType u : destinationTypes) {
            destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());
          }
        }

        for (TmpltGuide guide : guides) {
          Destination dest = Destination.find.byId(guide.destinationid);
          DestinationView destView = new DestinationView();
          destView.id = dest.destinationid;
          destView.name = dest.name;
          destView.destinationType = String.valueOf(dest.destinationtypeid);
          destView.destinationTypeDesc = destinationTypeDesc.get(dest.destinationtypeid);

          destView.cmpyId = dest.cmpyid;
          destView.city = dest.city;
          destView.country = dest.country;
          destView.intro = dest.intro;
          destView.locLat = String.valueOf(dest.loclat);
          destView.locLong = String.valueOf(dest.loclong);
          destView.isTourGuide = false;
          //get covers
          if (dest.hasCover()) {
            destView.coverUrl = dest.getCoverurl();
          }
          view.guides.add(destView);
        }
      }
    }

    if (isModal) {
      view.tripId = tripId;
      return ok(views.html.template.templateBookingModal.render(view));
    }
    else {
      return ok(views.html.template.templateBooking.render(view));
    }
  }


  public static TripBookingDetailView buildBookingView(TmpltDetails tmpltDetail) {

    TripBookingDetailView bookingDetails = new TripBookingDetailView();

    //Get all 3 POI objects here - to get fresh data
    PoiRS poiMain = PoiController.getMergedPoi(tmpltDetail.getPoiId(), tmpltDetail.getPoiCmpyId());
    PoiRS poiStart = PoiController.getMergedPoi(tmpltDetail.getLocStartPoiId(), tmpltDetail.getLocStartPoiCmpyId());
    PoiRS poiFinish = PoiController.getMergedPoi(tmpltDetail.getLocFinishPoiId(), tmpltDetail.getLocFinishPoiCmpyId());
   
    // TODO: (Wei) Get the new reservation and set it to view
    // consider to refactor it hide it from view
    UmReservation reservation = tmpltDetail.getReservation();
    
    bookingDetails.poiMain = poiMain;
    bookingDetails.poiStart = poiStart;
    bookingDetails.poiFinish = poiFinish;
    bookingDetails.tripId = tmpltDetail.templateid;
    bookingDetails.createdById = tmpltDetail.createdby;
    bookingDetails.status = tmpltDetail.status;
    bookingDetails.detailsId = tmpltDetail.detailsid;
    bookingDetails.detailsTypeId = tmpltDetail.detailtypeid;
    bookingDetails.reservation = reservation;
    
    if (reservation != null) {
      bookingDetails.comments       = Utils.escapeHtml(tmpltDetail.getReservation().getNotesPlainText());
      bookingDetails.origComments   = Utils.escapeHtmlBr(tmpltDetail.getReservation().getNotesPlainText());
    }

    bookingDetails.duration = tmpltDetail.getDuration();

    if (tmpltDetail.starttimestamp != null && tmpltDetail.starttimestamp > 0) {
      bookingDetails.startDatePrint = Utils.formatDatePrint(tmpltDetail.starttimestamp);
      bookingDetails.startDate = Utils.getDateString(tmpltDetail.starttimestamp);
      bookingDetails.startTime = Utils.getTimeString(tmpltDetail.starttimestamp);
      bookingDetails.startDateMs = tmpltDetail.starttimestamp;
    }
    if (tmpltDetail.endtimestamp != null && tmpltDetail.starttimestamp != null &&
        !tmpltDetail.endtimestamp.equals(tmpltDetail.starttimestamp) &&
        tmpltDetail.starttimestamp > 0 && tmpltDetail.endtimestamp > 0) {
      bookingDetails.endDate = Utils.getDateString(tmpltDetail.endtimestamp);
      bookingDetails.endDatePrint = Utils.formatDatePrint(tmpltDetail.endtimestamp);
      bookingDetails.endTime = Utils.getTimeString(tmpltDetail.endtimestamp);
      bookingDetails.endDateMs = tmpltDetail.endtimestamp;
    }

    //All trip types are explicitly mentioned below, no need for root type
    switch (tmpltDetail.detailtypeid) {
      case FLIGHT:
        UmFlightReservation flightReservation = cast(reservation, UmFlightReservation.class);
        
        if (flightReservation != null) {
          bookingDetails.flightId = flightReservation.getFlight().flightNumber;

          if (bookingDetails.poiStart != null) {
            bookingDetails.departureAirport = bookingDetails.poiStart.getName();
            bookingDetails.departureAirportId = bookingDetails.poiStart.getId();
            bookingDetails.departureAirportCode = bookingDetails.poiStart.getCode();
            Address address = bookingDetails.poiStart.getMainAddress();
            if (address != null) {
              bookingDetails.departCity = address.getLocality();
              String cc = (address.getCountryCode() != null) ? address.getCountryCode() : poiStart.countryCode;
              bookingDetails.departCountry = CountriesInfo.Instance().byAlpha3(cc).getName(CountriesInfo.LangCode.EN);
            }
          }
          else {
            bookingDetails.departureAirport = tmpltDetail.getLocStartName();
          }

          if (bookingDetails.poiFinish != null) {
            bookingDetails.arrivalAirport = bookingDetails.poiFinish.getName();
            bookingDetails.arrivalAirportId = bookingDetails.poiFinish.getId();
            bookingDetails.arrivalAirportCode = bookingDetails.poiFinish.getCode();
            Address address = bookingDetails.poiFinish.getMainAddress();
            if (address != null) {
              bookingDetails.arriveCity = address.getLocality();
              String cc = (address.getCountryCode() != null) ? address.getCountryCode() : poiFinish.countryCode;
              bookingDetails.arriveCountry = CountriesInfo.Instance().byAlpha3(cc).getName(CountriesInfo.LangCode.EN);
            }
          }
          else {
            bookingDetails.arrivalAirport = tmpltDetail.getLocFinishName();
          }
          if (poiMain != null) {
            bookingDetails.providerName = poiMain.getName();
          }
        }
        else {
          Log.err("Database records missing no flight details for template " + tmpltDetail.templateid);
          return null;
        }
        break;
      case CRUISE:
        UmCruiseReservation cruise = cast(tmpltDetail.getReservation(), UmCruiseReservation.class);
        if (cruise != null) {
          bookingDetails.cruise = new TripBookingDetailView.Cruise();
          bookingDetails.cruise.cmpyName = cruise.getCruise().getProvider().name;
          bookingDetails.cruise.name = cruise.getCruise().name;

          if (poiMain != null) {
            bookingDetails.cruise.name = poiMain.getName();
          }
          else {
            bookingDetails.cruise.name = (tmpltDetail.getName() == null) ? tmpltDetail.getName() : cruise.getCruise().name;
          }
          bookingDetails.name = bookingDetails.cruise.name;

          if (bookingDetails.poiStart != null) {
            bookingDetails.cruise.portDepart = bookingDetails.poiStart.getName();
          }
          else {
            bookingDetails.cruise.portDepart = tmpltDetail.getLocStartName();
          }

          if (bookingDetails.poiFinish != null) {
            bookingDetails.cruise.portArrive = bookingDetails.poiFinish.getName();
          }
          else {
            bookingDetails.cruise.portArrive = tmpltDetail.getLocFinishName();
          }
        }
        else {
          Log.err("Database records missing no cruise details for template " + tmpltDetail.templateid);
          return null;
        }
        break;
      case HOTEL:
        UmAccommodationReservation hotel = cast(tmpltDetail.getReservation(), UmAccommodationReservation.class);
       
        if (hotel != null) {
          if (bookingDetails.poiMain == null && bookingDetails.providerName == null) {
            bookingDetails.providerName = hotel.getAccommodation().name;
          }
        }
        else {
          Log.err("Database records missing no hotel details for template " + tmpltDetail.templateid);
          return null;
        }
        break;

      /** ACTIVITIES **/
      case ACTIVITY:
      case TOUR:
      case EVENT:
      case RESTAURANT:
      case CRUISE_STOP:
      case SHORE_EXCURSION:
        UmActivityReservation activity = cast(tmpltDetail.getReservation(), UmActivityReservation.class);
        
        if (activity != null) {
          if (bookingDetails.poiMain == null) {
            bookingDetails.providerName = activity.getActivity().getOrganizedBy().name;
          }
          bookingDetails.contact = activity.getActivity().getOrganizedBy().contactPoint;
          if (activity.getActivity().umDocumentId != null) {
            Destination destination = Destination.find.byId(activity.getActivity().umDocumentId);
            bookingDetails.destination = destination.getName();
            bookingDetails.destinationId = destination.destinationid;
          }
        }
        else {
          Log.err("Database records missing no activity details for template " + tmpltDetail.templateid);
          return null;
        }
        break;
      case TRANSPORT:
      case CAR_RENTAL:
      case RAIL:
      case PRIVATE_TRANSFER:
      case TAXI:
      case BUS:
      case PUBLIC_TRANSPORT:
        UmTransferReservation transport = cast(tmpltDetail.getReservation(), UmTransferReservation.class);
        if (transport != null) {
          if (bookingDetails.poiMain == null) {
            bookingDetails.providerName = transport.getTransfer().getProvider().name;
          }

          bookingDetails.contact = transport.getTransfer().getProvider().contactPoint;
        }
        else {
          Log.err("Database records missing no transport details for template " + tmpltDetail.templateid);
          return null;
        }
        break;
    }

    //Updating poi information that might have changed
    if (bookingDetails.poiMain != null && bookingDetails.poiMain.getIdLong() != null) {
      bookingDetails.poiId = bookingDetails.poiMain.getId();
      bookingDetails.providerName = bookingDetails.poiMain.getName();
      bookingDetails.providerComments = bookingDetails.poiMain.data.getDesc();
    }
    else if (bookingDetails.providerName == null && tmpltDetail.getName() != null) {
      bookingDetails.providerName = tmpltDetail.getName();
    }

    if (bookingDetails.poiStart != null) { //Updating data from probably up to date poi object
      bookingDetails.locStartName = bookingDetails.poiStart.getName();
      bookingDetails.locStartPoi = bookingDetails.poiStart.getId();
      bookingDetails.locStartLat = bookingDetails.poiStart.locLat;
      bookingDetails.locStartLong = bookingDetails.poiStart.locLong;
    }
    else {
      bookingDetails.locStartName = tmpltDetail.getLocStartName();
      bookingDetails.locStartLat = tmpltDetail.getLocStartLat();
      bookingDetails.locStartLong = tmpltDetail.getLocStartLong();
    }

    if (bookingDetails.poiFinish != null) {
      bookingDetails.locFinishName = bookingDetails.poiFinish.getName();
      bookingDetails.locFinishPoi = bookingDetails.poiFinish.getId();
      bookingDetails.locFinishLat = bookingDetails.poiFinish.locLat;
      bookingDetails.locFinishLong = bookingDetails.poiFinish.locLong;
    }
    else {
      bookingDetails.locFinishName = tmpltDetail.getLocFinishName();
      bookingDetails.locFinishLat = tmpltDetail.getLocFinishLat();
      bookingDetails.locFinishLong = tmpltDetail.getLocFinishLong();
    }

    //Extracting other misc data from whatever poi information is currently available (mainly for older records)
    PoiRS prs = null;
    if (bookingDetails.poiMain != null && bookingDetails.poiMain.getIdLong() != null) {
      prs = bookingDetails.poiMain;
    }
    else if (bookingDetails.poiStart != null) {
      prs = bookingDetails.poiStart;
    }
    else if (bookingDetails.poiFinish != null) {
      prs = bookingDetails.poiFinish;
    }

    if (prs != null && prs.getIdLong() != null) {

      bookingDetails.photos = PoiController.getPhotosForPoi(prs.getId(), prs.getCmpyId());

      Address a = prs.getMainAddress();
      if (a != null) {
        bookingDetails.formattedAddr = prs.getMainAddressString();
        bookingDetails.providerAddr1 = a.getStreetAddress();
        if (bookingDetails.providerAddr1 != null && bookingDetails.formattedAddr.contains(bookingDetails.providerAddr1) &&
            bookingDetails.formattedAddr.length() > bookingDetails.providerAddr1.length()) {
          bookingDetails.providerAddr2 = bookingDetails.formattedAddr.substring(bookingDetails.providerAddr1.length() + 1);
        }
        bookingDetails.city = a.getLocality();
        bookingDetails.state = a.getRegion();
        bookingDetails.zip = a.getPostalCode();

        String cc = (a.getCountryCode() != null) ? a.getCountryCode() : prs.countryCode;
        if (cc != null && !cc.equals("UNK")) {
          bookingDetails.country = CountriesInfo.Instance().byAlpha3(cc).getName(CountriesInfo.LangCode.EN);
        }
      }

      if (prs.data.getPhoneNumbers().size() > 0) {
        bookingDetails.providerPhone = prs.getMainPhone().getNumber();
      }

      if (prs.data.getUrls().size() > 0) {
        bookingDetails.providerWeb = prs.getMainUrl();
      }

      if (prs.data.getEmails().size() > 0) {
        bookingDetails.providerEmail = prs.getMainEmail().getAddress();
      }
    }
    return bookingDetails;
  }

  @With({Authenticated.class, Authorized.class})
  public Result deleteBooking() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    String templateId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);

    //bind html form to form bean
    //check for edit
    final DynamicForm form = formFactory.form().bindFromRequest();
    String tripDetailId = form.get("inTripDetailId");

    //use bean validator framework
    if (tripDetailId == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    TmpltDetails tmpltDetail = TmpltDetails.find.byId(tripDetailId);
    Template template = Template.find.byId(templateId);

    if (template == null ||
        !SecurityMgr.canEditTemplate(template, sessionMgr) ||
        tmpltDetail == null ||
        !tmpltDetail.templateid.equals(templateId)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to delete this booking";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      Ebean.beginTransaction();
      String msg = "";

      tmpltDetail.delete();
      Ebean.commitTransaction();

      TabType nextTab = TabType.fromBookingType(tmpltDetail.detailtypeid);
      flash(SessionConstants.SESSION_PARAM_ACTIVE_TAB, nextTab.name());
      flash(SessionConstants.SESSION_PARAM_MSG, msg + " deleted");

      return bookings(tmpltDetail.getTemplateid(), new TabType.Bound(nextTab), null, false);
    }
    catch (Exception e) {
      e.printStackTrace();
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result newCruise() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<FormCruiseTemplate> form = formFactory.form(FormCruiseTemplate.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    FormCruiseTemplate cruiseInfo     = form.get();

    if (cruiseInfo.inCruiseName == null || cruiseInfo.inCruiseName.trim().length() == 0) {
      return UserMessage.message(ReturnCode.HTTP_REQ_MISSING_DATA,
                                 "Cruise Name is a required field - please correct and resubmit");
    }
    else if (cruiseInfo.inCruisePortDepart == null || cruiseInfo.inCruisePortDepart.trim().length() == 0) {
      return UserMessage.message(ReturnCode.HTTP_REQ_MISSING_DATA,
                                 "Cruise Departure Port is a required field - please correct and resubmit");
    }
    else if (cruiseInfo.inCruisePortArrive == null || cruiseInfo.inCruisePortArrive.trim().length() == 0) {
      return UserMessage.message(ReturnCode.HTTP_REQ_MISSING_DATA,
                                 "Cruise Arrival Port is a required field - please correct and resubmit");
    }

    Template template = Template.find.byId(cruiseInfo.inTemplateId);
    if (template == null || !SecurityMgr.canEditTemplate(template, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to edit this template";
      return ok(views.html.common.message.render(baseView));
    }

    TmpltDetails detail = null;
    if (cruiseInfo.inTmpltDetailId != null) {
      detail = TmpltDetails.find.byId(cruiseInfo.inTmpltDetailId);

      if (detail == null || !detail.templateid.equals(template.templateid)) {
        BaseView baseView = new BaseView();
        baseView.message = "You are not authorized to edit this template";
        return ok(views.html.common.message.render(baseView));
      }
    }

    try {
      Ebean.beginTransaction();

      if (detail == null) {
        detail = new TmpltDetails();
        detail.setDetailsid(Utils.getUniqueId());
        detail.setCreatedtimestamp(System.currentTimeMillis());
        detail.setCreatedby(sessionMgr.getUserId());
      }

      detail.setComments(cruiseInfo.inCruiseNote);
      detail.setDetailtypeid(ReservationType.CRUISE);
      detail.setTemplateid(cruiseInfo.inTemplateId);
      detail.setStatus(APPConstants.STATUS_ACTIVE);
      detail.setDuration(cruiseInfo.inDuration);
      detail.setDayOffset(cruiseInfo.inDayOffset);

      if (cruiseInfo.inStartTime != null && cruiseInfo.inStartTime.length() > 0) {
        detail.setStarttimestamp(Utils.getMilliSecs(cruiseInfo.inStartTime));
      }

      if (cruiseInfo.inEndTime != null && cruiseInfo.inEndTime.length() > 0) {
        detail.setEndtimestamp(Utils.getMilliSecs(cruiseInfo.inEndTime));
      }

      List<PoiRS> pois = buildTripDetailPois(template,
                                             detail,
                                             cruiseInfo.inCruiseProviderId,
                                             cruiseInfo.inCruiseName,
                                             cruiseInfo.inCruisePortDepartProviderId,
                                             cruiseInfo.inCruisePortDepart,
                                             cruiseInfo.inCruisePortArriveProviderId,
                                             cruiseInfo.inCruisePortArrive,
                                             sessionMgr.getCredentials());

      PoiRS poiMain = pois.get(0); //Main vendor
      PoiRS poiStart = pois.get(1); //Departure
      PoiRS poiFinish = pois.get(2); //Arrival

      detail.setLastupdatedtimestamp(System.currentTimeMillis());
      detail.setModifiedby(sessionMgr.getUserId());

      UmCruiseReservation cruiseReservation = cast(detail.getReservation(), UmCruiseReservation.class);
      if (cruiseReservation == null) {
        cruiseReservation = new UmCruiseReservation();
        detail.setReservation(cruiseReservation);
      }
      
      if (poiMain != null) {
        cruiseReservation.getCruise().name = poiMain.getName();
      }
      else {
        cruiseReservation.getCruise().name = cruiseInfo.inCruiseName;
      }
      cruiseReservation.setNotesPlainText(cruiseInfo.inCruiseNote);


      cruiseReservation.getCruise().getProvider().name = cruiseInfo.inCruiseCmpyName;
      detail.save();
      Ebean.commitTransaction();

      if (cruiseInfo.inTmpltDetailId == null) {
        flash(SessionConstants.SESSION_PARAM_MSG, "Cruise " + cruiseInfo.inCruiseName + " Added");
      }
      else {
        BookingController.invalidateCache(detail.templateid, detail.getDetailsid());
        flash(SessionConstants.SESSION_PARAM_MSG, "Cruise " + cruiseInfo.inCruiseName + " Updated");
      }
      return bookings(detail.getTemplateid(), new TabType.Bound(TabType.CRUISES), null, false);
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "A system error has occurred";
      e.printStackTrace();
      return ok(views.html.common.message.render(baseView));
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result newFlight() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<FormTemplateFlight> form = formFactory.form(FormTemplateFlight.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    FormTemplateFlight flightInfo = form.get();

    if (flightInfo.inFlightNumber == null || flightInfo.inFlightNumber.trim().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Flight Number is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));

    }
    else if (flightInfo.inFlightDepartAirport == null || flightInfo.inFlightDepartAirport.trim().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Departure Airport is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));

    }
    else if (flightInfo.inFlightArrivalAirport == null || flightInfo.inFlightArrivalAirport.trim().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Arrival Airport is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));

    }

    Template template = Template.find.byId(flightInfo.inTemplateId);
    if (template == null || !SecurityMgr.canEditTemplate(template, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to edit this template";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      Ebean.beginTransaction();

      TmpltDetails detail = null;
      if (flightInfo.inTemplateDetailId != null) {
        detail = TmpltDetails.find.byId(flightInfo.inTemplateDetailId);
        if (detail == null || !detail.templateid.equals(flightInfo.inTemplateId)) {
          BaseView baseView = new BaseView();
          baseView.message = "You are not authorized to edit this template";
          return ok(views.html.common.message.render(baseView));
        }
      }

      if (detail == null) {
        detail = new TmpltDetails();
        detail.setDetailsid(Utils.getUniqueId());
        detail.setCreatedtimestamp(System.currentTimeMillis());
        detail.setCreatedby(sessionMgr.getUserId());
      }

      UmFlightReservation reservation = cast(detail.getReservation(), UmFlightReservation.class);
      if (reservation == null) {
        reservation = new UmFlightReservation();
        detail.setReservation(reservation);
      }
      
      reservation.getFlight().flightNumber = flightInfo.inFlightNumber;
      reservation.getFlight().departureTerminal = flightInfo.inDepartureTerminal;
      reservation.getFlight().arrivalTerminal = flightInfo.inArrivalTerminal;
      reservation.setNotesPlainText(flightInfo.inFlightNote);
      
      detail.setDetailtypeid(ReservationType.FLIGHT);
      detail.setTemplateid(flightInfo.inTemplateId);
      detail.setStatus(APPConstants.STATUS_ACTIVE);
      detail.setDayOffset(flightInfo.inDayOffset);

      if (flightInfo.inStartTime != null && flightInfo.inStartTime.length() > 0) {
        detail.setStarttimestamp(Utils.getMilliSecs(flightInfo.inStartTime));
      }

      if (flightInfo.inStartTime != null && flightInfo.inStartTime.length() > 0) {
        detail.setEndtimestamp(Utils.getMilliSecs(flightInfo.inEndTime));
      }

      List<PoiRS> pois = buildTripDetailPois(template,
                                             detail,
                                             null,
                                             flightInfo.inFlightNumber,
                                             flightInfo.inFlightDepartAirportId,
                                             flightInfo.inFlightDepartAirport,
                                             flightInfo.inFlightArrivalAirportId,
                                             flightInfo.inFlightArrivalAirport,
                                             sessionMgr.getCredentials());

      PoiRS poiMain = null;

      if (flightInfo.inFlightNumber != null) {
        int length = flightInfo.inFlightNumber.length();

        StringBuffer buffer = new StringBuffer(length);

        for (int i = 0; i < length; i++) {
          char ch = flightInfo.inFlightNumber.charAt(i);
          if (Character.isDigit(ch) && i > 1) {
            break;
          }
          else {
            buffer.append(ch);
          }
        }

        String airlineCode = buffer.toString().trim();
        int pt = PoiTypeInfo.Instance().byName("Airline").getId();

        if (detail.getPoiId() != null) {
          poiMain = PoiController.findAndMergeByCode(airlineCode, pt, detail.getPoiCmpyId());
        }
        else {
          Set<Integer> userCmpies = new HashSet<>();
          userCmpies.add(sessionMgr.getCredentials().getCmpyIdInt());
          poiMain = PoiController.findAndMergeByCode(airlineCode, pt, userCmpies);

          if (poiMain != null) {
            poiMain.setCmpyId((Integer) userCmpies.toArray()[0]);
          }
        }
      }

      if (poiMain != null) {
        detail.setPoiCmpyId(poiMain.getCmpyId());
        detail.setPoiId(poiMain.getId());
        if (poiMain.getName() != null && poiMain.getName().trim().length() > 0) {
          detail.setName(poiMain.getName());
        }
      }

      detail.setLastupdatedtimestamp(System.currentTimeMillis());
      detail.setModifiedby(sessionMgr.getUserId());
      detail.save();

      Ebean.commitTransaction();

      if (flightInfo.inTemplateDetailId == null) {
        flash(SessionConstants.SESSION_PARAM_MSG, flightInfo.inFlightNumber + " Added");
      }
      else {
        BookingController.invalidateCache(detail.templateid, detail.getDetailsid());
        flash(SessionConstants.SESSION_PARAM_MSG, flightInfo.inFlightNumber + " Updated");
      }
      return bookings(detail.getTemplateid(), new TabType.Bound(TabType.FLIGHTS), null, false);
    }
    catch (Exception e) {
      e.printStackTrace();
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "A system error has occurred";
      return ok(views.html.common.message.render(baseView));
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result newHotel() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<HotelBookingForm> form = formFactory.form(HotelBookingForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    HotelBookingForm hotelInfo = form.get();

    Template template = Template.find.byId(hotelInfo.inTemplateId);
    if (template == null || !SecurityMgr.canEditTemplate(template, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to edit this template";
      return ok(views.html.common.message.render(baseView));
    }

    if (hotelInfo.inHotelName == null || hotelInfo.inHotelName.trim().length() == 0) {
      //check to make sure dates are good
      BaseView baseView = new BaseView();
      baseView.message = "Hotel Name is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      Ebean.beginTransaction();
      TmpltDetails detail = null;
      if (hotelInfo.inTemplateDetailId != null) {
        detail = TmpltDetails.find.byId(hotelInfo.inTemplateDetailId);
        if (detail == null || !detail.templateid.equals(hotelInfo.inTemplateId)) {
          BaseView baseView = new BaseView();
          baseView.message = "You are not authorized to edit this trip";
          return ok(views.html.common.message.render(baseView));
        }
      }
      if (detail == null) {
        detail = new TmpltDetails();
        detail.setDetailsid(Utils.getUniqueId());
        detail.setCreatedtimestamp(System.currentTimeMillis());
        detail.setCreatedby(sessionMgr.getUserId());
      }
     

      detail.setDetailtypeid(ReservationType.HOTEL);
      detail.setTemplateid(hotelInfo.inTemplateId);
      detail.setStatus(APPConstants.STATUS_ACTIVE);
      detail.setDayOffset(hotelInfo.inDayOffset);
      detail.setDuration(hotelInfo.inDuration);

      if (hotelInfo.inStartTime != null && hotelInfo.inStartTime.length() > 0) {
        detail.setStarttimestamp(Utils.getMilliSecs(hotelInfo.inStartTime));
      }

      if (hotelInfo.inStartTime != null && hotelInfo.inStartTime.length() > 0) {
        detail.setEndtimestamp(Utils.getMilliSecs(hotelInfo.inEndTime));
      }


      List<PoiRS> pois = buildTripDetailPois(template,
                                             detail,
                                             hotelInfo.inHotelProviderId,
                                             hotelInfo.inHotelName,
                                             null,
                                             null,
                                             null,
                                             null,
                                             sessionMgr.getCredentials());

      PoiRS poiMain = pois.get(0); //Main vendor


      detail.setLastupdatedtimestamp(System.currentTimeMillis());
      detail.setModifiedby(sessionMgr.getUserId());
      
      UmAccommodationReservation hotelReservation = cast(detail.getReservation(), UmAccommodationReservation.class);
      if (hotelReservation == null) {
        hotelReservation = new UmAccommodationReservation();
        detail.setReservation(hotelReservation);
      }
      
      hotelReservation.setNotesPlainText(hotelInfo.inHotelNote);
      
      if (poiMain != null) {
        hotelReservation.getAccommodation().name = poiMain.getName();
      }
      else {
        hotelReservation.getAccommodation().name = hotelInfo.inHotelName;
      }
      detail.save();

      Ebean.commitTransaction();

      if (hotelInfo.inTemplateDetailId == null) {
        flash(SessionConstants.SESSION_PARAM_MSG, hotelInfo.inHotelName + " Added");
      }
      else {
        BookingController.invalidateCache(detail.templateid, detail.getDetailsid());
        flash(SessionConstants.SESSION_PARAM_MSG, hotelInfo.inHotelName + " Updated");
      }
      return bookings(detail.getTemplateid(), new TabType.Bound(TabType.HOTELS), null, false);
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "A system error has occurred";
      e.printStackTrace();
      return ok(views.html.common.message.render(baseView));
    }

  }

  @With({Authenticated.class, Authorized.class})
  public Result newTransport() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<FormTemplateTransport> form = formFactory.form(FormTemplateTransport.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    FormTemplateTransport       transferInfo     = form.get();

    Template template = Template.find.byId(transferInfo.inTripId);
    if (template == null || !SecurityMgr.canEditTemplate(template, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to edit this trip";
      return ok(views.html.common.message.render(baseView));
    }

    if (transferInfo.inTransferCmpy == null || transferInfo.inTransferCmpy.trim().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Company Name is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      Ebean.beginTransaction();
      TmpltDetails detail = null;
      if (transferInfo.inTemplateDetailId != null) {
        detail = TmpltDetails.find.byId(transferInfo.inTemplateDetailId);
        if (detail == null || !detail.templateid.equals(transferInfo.inTripId)) {
          BaseView baseView = new BaseView();
          baseView.message = "You are not authorized to edit this trip";
          return ok(views.html.common.message.render(baseView));
        }
      }

      if (detail == null) {
        detail = new TmpltDetails();
        detail.setDetailsid(Utils.getUniqueId());
        detail.setCreatedtimestamp(System.currentTimeMillis());
        detail.setCreatedby(sessionMgr.getUserId());
        detail.setReservation(new UmTransferReservation());
      }

      detail.setComments(transferInfo.inTransferNote);
      detail.setDayOffset(transferInfo.inDayOffset);
      detail.setDuration(transferInfo.inDuration);

      ReservationType activityType = ReservationType.valueOf(transferInfo.inBookingType);
      detail.setDetailtypeid(activityType);

      detail.setTemplateid(transferInfo.inTripId);
      detail.setStatus(APPConstants.STATUS_ACTIVE);

      List<PoiRS> pois = buildTripDetailPois(template,
                                             detail,
                                             transferInfo.inTransferProviderId,
                                             transferInfo.inTransferCmpy,
                                             null,
                                             transferInfo.inTransferPickUp,
                                             null,
                                             transferInfo.inTransferDropOff,
                                             sessionMgr.getCredentials());

      PoiRS poiMain = pois.get(0); //Main vendor
      PoiRS poiStart = pois.get(1); //Departure
      PoiRS poiFinish = pois.get(2); //Arrival

      if (transferInfo.inStartTime != null && transferInfo.inStartTime.length() > 0) {
        detail.setStarttimestamp(Utils.getMilliSecs(transferInfo.inStartTime));
      }

      if (transferInfo.inEndTime != null && transferInfo.inEndTime.length() > 0) {
        detail.setEndtimestamp(Utils.getMilliSecs(transferInfo.inEndTime));
      }

      detail.setLastupdatedtimestamp(System.currentTimeMillis());
      detail.setModifiedby(sessionMgr.getUserId());
      
      UmTransferReservation reservation = cast(detail.getReservation(), UmTransferReservation.class);
      if (reservation == null) {
        reservation = new UmTransferReservation();
        detail.setReservation(reservation);
      }

      reservation.setNotesPlainText(transferInfo.inTransferNote);

      if (poiMain != null) {
        reservation.getTransfer().getProvider().name = poiMain.getName();
      }
      else {
        reservation.getTransfer().getProvider().name = transferInfo.inTransferCmpy;
      }
       
      reservation.getTransfer().getPickupLocation().name = transferInfo.inTransferPickUp;
      reservation.getTransfer().getDropoffLocation().name = transferInfo.inTransferDropOff;
      reservation.getTransfer().getProvider().contactPoint = transferInfo.inTransferContact;
      
      detail.save();

      Ebean.commitTransaction();

      if (transferInfo.inTemplateDetailId == null) {
        flash(SessionConstants.SESSION_PARAM_MSG, transferInfo.inTransferCmpy + " Added");
      }
      else {
        BookingController.invalidateCache(detail.templateid, detail.getDetailsid());
        flash(SessionConstants.SESSION_PARAM_MSG, transferInfo.inTransferCmpy + " Updated");
      }

      return bookings(detail.getTemplateid(), new TabType.Bound(TabType.TRANSPORTS), null, false);
    }
    catch (Exception e) {
      e.printStackTrace();
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "A system error has occurred";
      e.printStackTrace();
      return ok(views.html.common.message.render(baseView));
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result newActivity() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<FormActivityTemplate> form = formFactory.form(FormActivityTemplate.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    FormActivityTemplate activityInfo = form.get();
    Template template = Template.find.byId(activityInfo.inTemplateId);
    if (template == null || !SecurityMgr.canEditTemplate(template, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to edit this trip";
      return ok(views.html.common.message.render(baseView));
    }

    if (activityInfo.inActivityProviderName == null || activityInfo.inActivityProviderName.trim().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Activity Name is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      Ebean.beginTransaction();
      TmpltDetails detail = null;
      if (activityInfo.inTemplateDetailId != null) {
        detail = TmpltDetails.find.byId(activityInfo.inTemplateDetailId);
        if (detail == null || !detail.templateid.equals(activityInfo.inTemplateId)) {
          BaseView baseView = new BaseView();
          baseView.message = "You are not authorized to edit this trip";
          return ok(views.html.common.message.render(baseView));
        }
      }

      if (detail == null) {
        detail = new TmpltDetails();
        detail.setDetailsid(Utils.getUniqueId());
        detail.setCreatedtimestamp(System.currentTimeMillis());
        detail.setCreatedby(sessionMgr.getUserId());
      }

      UmActivityReservation reservation = cast(detail.getReservation(), UmActivityReservation.class);
      if (reservation == null) {
        reservation = new UmActivityReservation();
        detail.setReservation(reservation);
      }
      reservation.setNotesPlainText(activityInfo.inActivityNote);
       
      ReservationType activityType = ReservationType.valueOf(activityInfo.inBookingType);
      detail.setDetailtypeid(activityType);
      detail.setTemplateid(activityInfo.inTemplateId);
      detail.setStatus(APPConstants.STATUS_ACTIVE);

      detail.setDayOffset(activityInfo.inDayOffset);
      detail.setDuration(activityInfo.inDuration);

      List<PoiRS> pois = buildTripDetailPois(template,
                                             detail,
                                             activityInfo.inActivityProviderId,
                                             activityInfo.inActivityProviderName,
                                             activityInfo.inLocStartProvider,
                                             activityInfo.inLocStartName,
                                             activityInfo.inLocFinishProvider,
                                             activityInfo.inLocFinishName,
                                             sessionMgr.getCredentials());

      PoiRS poiMain = pois.get(0); //Main vendor
      PoiRS poiStart = pois.get(1); //Departure
      PoiRS poiFinish = pois.get(2); //Arrival

      if (activityInfo.inStartTime != null && activityInfo.inStartTime.length() > 0) {
        detail.setStarttimestamp(Utils.getMilliSecs(activityInfo.inStartTime));
      }

      if (activityInfo.inStartTime != null && activityInfo.inStartTime.length() > 0) {
        detail.setEndtimestamp(Utils.getMilliSecs(activityInfo.inEndTime));
      }

      if (poiMain != null) {
        reservation.getActivity().name = poiMain.getName();
      }
      else {
        reservation.getActivity().name = activityInfo.inActivityProviderName;
      }
      
      if (activityInfo.inActivityDestinationId != null &&
          activityInfo.inActivityDestinationId.length() > 0 &&
          activityInfo.inActivityDestination != null &&
          activityInfo.inActivityDestination.length() > 0) {
        Destination destination = Destination.find.byId(activityInfo.inActivityDestinationId);
        if (destination != null && destination.cmpyid.equals(template.cmpyid)) {
          reservation.getActivity().umDocumentId = destination.destinationid;
        }
      }
      else {
        reservation.getActivity().umDocumentId = null;
      }
      
      reservation.getActivity().getOrganizedBy().contactPoint = activityInfo.inActivityContact;
      
      detail.setLastupdatedtimestamp(System.currentTimeMillis());
      detail.setModifiedby(sessionMgr.getUserId());
      detail.save();

      Ebean.commitTransaction();

      if (activityInfo.inTemplateDetailId == null) {
        flash(SessionConstants.SESSION_PARAM_MSG, activityInfo.inActivityProviderName + " Added");
      }
      else {
        BookingController.invalidateCache(detail.templateid, detail.getDetailsid());
        flash(SessionConstants.SESSION_PARAM_MSG, activityInfo.inActivityProviderName + " Updated");
      }

      return bookings(detail.getTemplateid(), new TabType.Bound(TabType.ACTIVITIES), null, false);
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "A system error has occurred";
      e.printStackTrace();
      return ok(views.html.common.message.render(baseView));
    }

  }


  public static int deleteTemplateHard(String templateId){
    int result = 0;

    String delFlights = "DELETE from tmplt_flight where detailsid in (select detailsid from tmplt_details " +
                        "where" + " templateid = ?);";
    String delHotels = "DELETE from tmplt_hotel where detailsid in (select detailsid from tmplt_details where " + "templateid = ?);";
    String delCruises = "DELETE from tmplt_cruise where detailsid in (select detailsid from tmplt_details where" +
                        " " + "templateid = ?);";
    String delTransport = "DELETE from tmplt_transport where detailsid in (select detailsid from tmplt_details " +
                          "" + "where templateid = ?);";
    String delActivities = "DELETE from tmplt_activity where detailsid in (select detailsid from tmplt_details " +
                           "" + "where templateid = ?);";
    String delDetails = "DELETE from tmplt_details where templateid = ?;";

    String delGuides = "DELETE FROM tmplt_guide WHERE templateid = ?;";

    String delMeta   = "DELETE FROM tmplt_meta WHERE tmplt_id = ?;";

    String delTemplate = "DELETE from template where templateid = ?;";
    PreparedStatement prep = null;
    Connection conn = null;
    try {
      try {
        conn = DBConnectionMgr.getConnection4Publisher();
        conn.setAutoCommit(false);

        prep = conn.prepareStatement(delFlights);
        prep.setString(1, templateId);
        result += prep.executeUpdate();
        if (prep != null) {
          prep.close();
        }

        prep = conn.prepareStatement(delHotels);
        prep.setString(1, templateId);
        result += prep.executeUpdate();
        if (prep != null) {
          prep.close();
        }

        prep = conn.prepareStatement(delCruises);
        prep.setString(1, templateId);
        result += prep.executeUpdate();
        if (prep != null) {
          prep.close();
        }

        prep = conn.prepareStatement(delTransport);
        prep.setString(1, templateId);
        result += prep.executeUpdate();
        if (prep != null) {
          prep.close();
        }

        prep = conn.prepareStatement(delActivities);
        prep.setString(1, templateId);
        result += prep.executeUpdate();
        if (prep != null) {
          prep.close();
        }

        prep = conn.prepareStatement(delDetails);
        prep.setString(1, templateId);
        result += prep.executeUpdate();
        if (prep != null) {
          prep.close();
        }

        prep = conn.prepareStatement(delGuides);
        prep.setString(1, templateId);
        result += prep.executeUpdate();
        if (prep != null) {
          prep.close();
        }

        prep = conn.prepareStatement(delMeta);
        prep.setString(1, templateId);
        result += prep.executeUpdate();
        if (prep != null) {
          prep.close();
        }

        prep = conn.prepareStatement(delTemplate);
        prep.setString(1, templateId);
        result += prep.executeUpdate();

        conn.commit();
      }
      catch (Exception e) {
        if (conn != null) {
          conn.rollback();
        }
        Log.err("deleteTemplateHard: Failed:", e);
        result = -1;
      }
      finally {
        if (prep != null) {
          prep.close();
        }
        if (conn != null) {
          conn.close();
        }
      }
    }
    catch (SQLException e) {
      e.printStackTrace();
    }

    return result;
  }

  @With({Authenticated.class, Authorized.class})
  public Result deleteTemplate(String inTemplateId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    String templateId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);

    //use bean validator framework
    if (inTemplateId == null) {
      return UserMessage.message(ReturnCode.HTTP_REQ_MISSING_DATA, "Errors on the form - please resubmit");
    }

    Template template = Template.find.byId(inTemplateId);

    if (template == null || !SecurityMgr.canEditTemplate(template, sessionMgr)) {
      return UserMessage.message(ReturnCode.AUTH_BOOKING_DEL_FAIL, "No authorized to delete this template");
    }

    try {
      deleteTemplateHard(templateId);
      flash(SessionConstants.SESSION_PARAM_MSG, template.name + "  deleted");
      return redirect(routes.TemplateController.templates());
    }
    catch (Exception e) {
      e.printStackTrace();
      return UserMessage.message(ReturnCode.LOGICAL_ERROR);
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result templates() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    TemplateSearchView view = new TemplateSearchView();
    view.message = msg;
    view.myTemplates = new ArrayList<TemplateView>();
    view.publicTemplates = new ArrayList<TemplateView>();

    //bind html form to form bean
    Form<TemplateSearchForm> templateForm = formFactory.form(TemplateSearchForm.class);
    TemplateSearchForm templateSearchForm = templateForm.bindFromRequest().get();

    HashMap<String, String> agencyList = new HashMap<String, String>();
    List<String> cmpies = new ArrayList<String>();
    List<String> adminCmpies = new ArrayList<String>();

    Credentials cred = sessionMgr.getCredentials();
    if (cred.hasCompany()) {
      agencyList.put(cred.getCmpyId(), cred.getCmpyName());

      if (cred.isCmpyAdmin()) {
        adminCmpies.add(cred.getCmpyId());
      }
      else {
        cmpies.add(cred.getCmpyId());
      }
    }

    view.tripAgencyList = agencyList;
    view.cmpyId = "All";

    if (templateSearchForm != null &&
        templateSearchForm.getInCmpyId() != null &&
        !templateSearchForm.getInCmpyId().equalsIgnoreCase("All")) {
      Company c = Company.find.byId(templateSearchForm.getInCmpyId());
      if (c != null && SecurityMgr.canAccessCmpy(c, sessionMgr)) {
        view.cmpyId = templateSearchForm.getInCmpyId();
        cmpies = new ArrayList<String>();
        adminCmpies = new ArrayList<>();
        if (SecurityMgr.isCmpyAdmin(c, sessionMgr)) {
          adminCmpies.add(c.getCmpyid());
        }
        else {
          cmpies.add(c.getCmpyid());
        }
      }
    }


    if (templateSearchForm.getInSearchTerm() == null || templateSearchForm.getInSearchTerm().trim().length() == 0) {
      if (cmpies.size() > 0) {
        List<Template> l1 = Template.findMostMyRecentPTemplates(cmpies, sessionMgr.getUserId());
        if (l1 != null) {
          for (Template t : l1) {
            TemplateView v = TemplateController.buildTemplateView(t, sessionMgr, agencyList);
            if (v != null) {
              view.myTemplates.add(v);
            }
          }
        }

        List<Template> l2 = Template.findMostRecentPublicTemplates(cmpies, sessionMgr.getUserId());
        if (l2 != null) {
          for (Template t : l2) {
            TemplateView v = TemplateController.buildTemplateView(t, sessionMgr, agencyList);
            if (v != null) {
              view.publicTemplates.add(v);
            }
          }
        }
      }

      if (adminCmpies.size() > 0) {
        List<Template> l1 = Template.findMostAllRecentTemplates(adminCmpies);
        if (l1 != null) {
          for (Template t : l1) {
            TemplateView v = TemplateController.buildTemplateView(t, sessionMgr, agencyList);
            if (v != null && t.createdby.equals(sessionMgr.getUserId())) {
              view.myTemplates.add(v);
            }
            else {
              view.publicTemplates.add(v);
            }
          }
        }
      }
    }
    else {
      view.searchTerm = templateSearchForm.getInSearchTerm().trim();
      String term = "%" + templateSearchForm.getInSearchTerm().trim().replace(" ", "%") + "%";


      if (cmpies.size() > 0) {
        List<Template> l1 = Template.findMyTemplates(cmpies, sessionMgr.getUserId(), term);
        if (l1 != null) {
          for (Template t : l1) {
            TemplateView v = TemplateController.buildTemplateView(t, sessionMgr, agencyList);
            if (v != null) {
              view.myTemplates.add(v);
            }
          }
        }

        List<Template> l2 = Template.findPublicTemplates(cmpies, sessionMgr.getUserId(), term);
        if (l2 != null) {
          for (Template t : l2) {
            TemplateView v = TemplateController.buildTemplateView(t, sessionMgr, agencyList);
            if (v != null) {
              view.publicTemplates.add(v);
            }
          }
        }
      }

      if (adminCmpies.size() > 0) {
        List<Template> l1 = Template.findAllTemplates(adminCmpies, term);
        if (l1 != null) {
          for (Template t : l1) {
            TemplateView v = TemplateController.buildTemplateView(t, sessionMgr, agencyList);
            if (v != null && t.createdby.equals(sessionMgr.getUserId())) {
              view.myTemplates.add(v);
            }
            else {
              view.publicTemplates.add(v);
            }
          }
        }
      }
    }

    return ok(views.html.template.templates.render(view));
  }

  /**
   * This action is not called from any page and has to be deleted
   *
   * @deprecated
   */
  @Deprecated @With({Authenticated.class, Authorized.class})
  public Result addGuide() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<TripGuideForm> form = formFactory.form(TripGuideForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    TripGuideForm tripInfo = form.get();
    Template template = Template.find.byId(tripInfo.getInTripId());
    if (template == null || !SecurityMgr.canEditTemplate(template, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to add documents";
      return ok(views.html.common.message.render(baseView));
    }
    else {
      List<TmpltGuide> existingGuides = TmpltGuide.findByTemplateDestination(template.templateid,
                                                                             tripInfo.getInDestId());
      if (existingGuides == null || existingGuides.size() == 0) {
        Destination dest = Destination.find.byId(tripInfo.getInDestId());
        if (dest == null || dest.status != APPConstants.STATUS_ACTIVE || !dest.cmpyid.equals(template.cmpyid)) {
          BaseView baseView = new BaseView();
          baseView.message = "Invalid Guide";
          return ok(views.html.common.message.render(baseView));
        }

        TmpltGuide guide = new TmpltGuide();
        guide.setPk(DBConnectionMgr.getUniqueId());
        guide.setTemplateid(template.templateid);
        guide.setDestinationid(dest.destinationid);
        guide.setStatus(APPConstants.STATUS_ACTIVE);
        guide.setCreatedby(sessionMgr.getUserId());
        guide.setCreatedtimestamp(System.currentTimeMillis());
        guide.save();

        flash(SessionConstants.SESSION_PARAM_MSG, dest.name + "  added");
      }
    }

    return redirect(routes.TemplateController.guides(tripInfo.getInTripId()));
  }

  @With({Authenticated.class, Authorized.class})
  public Result guides(String inTemplateId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    String templateId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);
    DestinationSearchView view = new DestinationSearchView();
    view.destinationList = new ArrayList<DestinationView>();

    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      view.message = msg;
    }
    //bind html form to form bean
    //check for edit
    if (inTemplateId == null) {
      inTemplateId = templateId;
    }

    //use bean validator framework
    if (inTemplateId == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }
    else {
      Template template = Template.find.byId(inTemplateId);

      if (template == null || !SecurityMgr.canEditTemplate(template, sessionMgr)) {
        BaseView baseView = new BaseView();
        baseView.message = "You are not authorized to delete this booking";
        return ok(views.html.common.message.render(baseView));
      }
      else {
        view.title = template.name;
        view.tripId = template.templateid;
        List<TmpltGuide> guides = TmpltGuide.findByTemplate(template.templateid);
        if (guides != null) {
          HashMap<Integer, String> destinationTypeDesc = new HashMap<Integer, String>();
          List<DestinationType> destinationTypes = DestinationType.findActive();
          if (destinationTypes != null) {
            ArrayList<GenericTypeView> destinationTypeList = new ArrayList<GenericTypeView>();
            for (DestinationType u : destinationTypes) {
              GenericTypeView typeView = new GenericTypeView();
              typeView.id = String.valueOf(u.getDestinationtypeid());
              typeView.name = u.getName();
              destinationTypeList.add(typeView);
              destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());

            }
            view.destinationTypeList = destinationTypeList;
          }

          for (TmpltGuide t : guides) {
            Destination dest = Destination.find.byId(t.destinationid);
            DestinationView destView = new DestinationView();
            destView.id = dest.destinationid;
            destView.name = dest.name;
            destView.destinationType = String.valueOf(dest.destinationtypeid);
            destView.destinationTypeDesc = destinationTypeDesc.get(dest.destinationtypeid);

            destView.cmpyId = dest.cmpyid;
            destView.city = dest.city;
            destView.country = dest.country;
            destView.intro = dest.intro;
            destView.locLat = String.valueOf(dest.loclat);
            destView.locLong = String.valueOf(dest.loclong);
            destView.isTourGuide = false;
            //get covers
            if (dest.hasCover()) {
              destView.coverUrl = dest.getCoverurl();
            }
            view.destinationList.add(destView);
          }
        }
      }
    }

    return ok(views.html.template.guide.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result deleteCover(String templateId) {

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    if (templateId == null || templateId.trim().length() == 0) {
      ObjectNode result = Json.newObject();
      result.put("msg", "Error - please resubmit");
      return ok(result);
    }

    Template template = Template.find.byId(templateId);
    if (template == null || !SecurityMgr.canEditTemplate(template, sessionMgr)) {
      ObjectNode result = Json.newObject();
      result.put("msg", "Error - Unauthorized access");
      return ok(result);
    }

    try {
      Ebean.beginTransaction();
      template.setFileImage(null);
      template.markModified(sessionMgr.getUserId());
      template.save();
      Ebean.commitTransaction();
      flash(SessionConstants.SESSION_PARAM_MSG, "Cover Photo deleted successfully. ");
      return template(template.getTemplateid());
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "Failed to delete cover image - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }
  }

  //attach trip cover photo
  @With({Authenticated.class, Authorized.class})
  public Result uploadCover(String templateId, String photoName) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Account a;
    if(sessionMgr.getAccount().isPresent()) {
      a = sessionMgr.getAccount().get();
    } else {
      a = Account.find.byId(sessionMgr.getAccountId());
    }

    if (templateId == null || templateId.length() == 0 || photoName == null || photoName.length() == 0) {
      ObjectNode result = Json.newObject();
      result.put("msg", "Your browser failed to prepare upload request.");
      return ok(result);
    }

    Template template = Template.find.byId(templateId);
    if (template == null || !SecurityMgr.canEditTemplate(template, sessionMgr)) {
      return BaseJsonResponse.codeResponse(ReturnCode.AUTH_TEMPLATE_FAIL, "You are not authorized to edit this " +
                                                                          "template");
    }

    ObjectNode result = Json.newObject();
    String nextUrl = controllers.routes.TemplateController.template(templateId).url();
    FileImage fileImage = uploadHelper(template, photoName, a, result, nextUrl);

    if(fileImage != null) {
      template.setFileImage(fileImage);
      template.setCoverFilename(photoName);
      template.setCoverUrl(fileImage.getUrl());
      template.markModified(sessionMgr.getUserId());
      template.update();
      return ok(result);
    }

    return BaseJsonResponse.codeResponse(ReturnCode.STORAGE_S3_UPLOAD, "3rd party service provider error.");
  }

  @With({Authenticated.class, Authorized.class})
  public Result deleteGuide() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<TripGuideForm> form = formFactory.form(TripGuideForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    TripGuideForm tripInfo = form.get();
    Template template = Template.find.byId(tripInfo.getInTripId());

    if (template == null || !SecurityMgr.canEditTemplate(template, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to edit this template";
      return ok(views.html.common.message.render(baseView));
    }

    List<TmpltGuide> existingGuides = TmpltGuide.findByTemplateDestination(template.templateid, tripInfo.getInDestId());
    Destination dest = Destination.find.byId(tripInfo.getInDestId());

    if (existingGuides != null && existingGuides.size() > 0) {
      for (TmpltGuide guide : existingGuides) {
        guide.delete();
      }
      if (dest != null) {
        flash(SessionConstants.SESSION_PARAM_MSG, dest.name + "  deleted");
      }
      else {
        flash(SessionConstants.SESSION_PARAM_MSG, "Guide deleted");
      }
    }
    return redirect(routes.TemplateController.guides(tripInfo.getInTripId()));
  }


  @With({Authenticated.class, Authorized.class})
  public Result cruiseSearch() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    final DynamicForm form = formFactory.form().bindFromRequest();
    CruiseSearchView searchView =  new CruiseSearchView();
    searchView.cruises =  new ArrayList<>();
    try {
      long inShipId = Long.parseLong(form.get("inSearchShipProviderId"));
      int inMonth = Integer.parseInt(form.get("inCruiseMonth"));
      int inYear = Integer.parseInt(form.get("inCruiseYear"));
      String inTripId = form.get("inTripId");
      HashMap <Long, PoiRS> poiCache = new HashMap();
      if (inTripId != null && inTripId.trim().length() > 0) {
        searchView.tripId = inTripId;
        LocalDate firstDayOfMonth = new LocalDate(inYear, inMonth, 1);
        LocalDate lastDayOfMonth = new LocalDate(inYear, inMonth, 1).dayOfMonth().withMaximumValue();

        List<TmpltDetails> cruises = TmpltDetails.findCuisesByPoiIdDates(inShipId,
                                                                         firstDayOfMonth.toDate().getTime(),
                                                                         lastDayOfMonth.toDate().getTime());
        if (cruises != null && cruises.size() > 0) {
          for (TmpltDetails td : cruises) {
            TripBookingView cruise = new TripBookingView();
            cruise.lang =ctx().lang();
            Template template = Template.find.byId(td.templateid);
            cruise.tripStartDate = String.valueOf(td.getStarttimestamp());
            cruise.tripEndDate = String.valueOf(td.getEndtimestamp());
            cruise.tripEndDatePrint = Utils.formatDatePrint(td.getEndtimestamp());
            cruise.tripStartDatePrint = Utils.formatDatePrint(td.getStarttimestamp());
            cruise.tripName = template.getName();
            cruise.comments = td.getComments();
            cruise = getCruiseDetails(cruise, td, poiCache);
            searchView.cruises.add(cruise);

          }
        } else {
          searchView.message = "No cruises found - please check the cruise ship and date combination";

        }
      } else {
        searchView.message = "No cruises found - please check the cruise ship and date combination";
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      searchView.message = " No cruises found - please check the cruise ship and date combination ";

    }

    return ok(views.html.template.cruiseSearchResults.render(searchView));
  }

  private static TripBookingView getCruiseDetails(TripBookingView cruiseView, TmpltDetails cruiseDetails, HashMap <Long, PoiRS> poiCache) {
    PoiRS startPort = null;
    PoiRS endPort = null;


    List<TmpltDetails> cruises = TmpltDetails.findByTemplateId(cruiseDetails.templateid);
    if (cruises != null && cruises.size() > 0) {
      for (TmpltDetails td : cruises) {
        if (td.detailtypeid == ReservationType.CRUISE_STOP || td.detailtypeid == ReservationType.TOUR) {

          TripBookingDetailView detailView = new TripBookingDetailView();
          detailView.providerName = td.getName();
          detailView.comments = td.getComments();
          detailView.startDate = String.valueOf(td.getStarttimestamp());
          detailView.startDateMs = td.getStarttimestamp();
          detailView.startDatePrint = Utils.formatDatePrint(td.getStarttimestamp());
          detailView.startTime = Utils.formatTime12(td.getStarttimestamp());
          detailView.endDate = String.valueOf(td.getEndtimestamp());
          detailView.endDateMs = td.getEndtimestamp();
          detailView.endDatePrint = Utils.formatDatePrint(td.getEndtimestamp());
          detailView.endTime = Utils.formatTime12(td.getEndtimestamp());

          detailView.detailsTypeId = td.detailtypeid;
          detailView.rank = td.getDayOffset();

          if (td.getLocStartPoiId() != null) {
            PoiRS port = poiCache.get(td.getLocStartPoiId());
            if (port == null) {
              port = PoiMgr.findForCmpy(td.getLocStartPoiId(), 0);
              poiCache.put(port.getId(), port);
            }
            detailView.arriveCity = port.getName();
            detailView.locFinishLat = port.locLat;
            detailView.locFinishLong = port.locLong;
            detailView.arrivalAirportCode = port.getCode();
            if (startPort == null) {
              startPort = port;
            }
            endPort = port;

          }
          cruiseView.addBookingDetailView(ReservationType.CRUISE_STOP, detailView);
        }
      }

      TripBookingDetailView detailView = new TripBookingDetailView();
      detailView.providerName = cruiseDetails.getName();
      detailView.comments = cruiseDetails.getComments();
      detailView.startDate = String.valueOf(cruiseDetails.getStarttimestamp());
      detailView.startDateMs = cruiseDetails.getStarttimestamp();
      detailView.startDatePrint = Utils.formatDateTimePrint(cruiseDetails.getStarttimestamp());
      detailView.endDate = String.valueOf(cruiseDetails.getEndtimestamp());
      detailView.endDateMs = cruiseDetails.getEndtimestamp();
      detailView.endDatePrint = Utils.formatDateTimePrint(cruiseDetails.getEndtimestamp());
      detailView.detailsTypeId = cruiseDetails.detailtypeid;
      detailView.detailsId = cruiseDetails.detailsid;
      long dur = (cruiseDetails.endtimestamp - cruiseDetails.starttimestamp) / (24 * 60 * 60 * 1000);
      detailView.duration = (int)dur + 1;

      TripBookingDetailView.Cruise cruise = new TripBookingDetailView.Cruise();
      detailView.cruise = cruise;

      if (cruiseDetails.getPoiId() != null) {
        PoiRS ship = poiCache.get(cruiseDetails.getPoiId());
        if (ship == null) {
          ship = PoiMgr.findForCmpy(cruiseDetails.getPoiId(), 0);
          poiCache.put(ship.getId(), ship);
        }

        cruise.cmpyName = ship.getCode();
      }
      cruise.name = cruiseView.tripName;
      if (startPort != null) {
        cruise.portDepart = startPort.getName();
        detailView.locStartLat = startPort.locLat;
        detailView.locStartLong = startPort.locLong;
      }
      if (endPort != null) {
        cruise.portArrive = endPort.getName();
        detailView.locFinishLat = endPort.locLat;
        detailView.locFinishLong = endPort.locLong;
      }
      cruiseView.addBookingDetailView(ReservationType.CRUISE, detailView);
    }
    return cruiseView;
  }

  @With({Authenticated.class, Authorized.class})
  public Result tourSearchModal (String tripId, int srcID, String imgName) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    BaseView view = new BaseView();

    Trip trip = Trip.find.byId(tripId);
    if (trip != null && SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND) && srcID > 0) {
      String season = null;
      if(srcID == FeedSourcesInfo.byName(FeedHelperGlobusTours.GLOBUS_TOURS_FEED_SRC_NAME)) {
        season = Utils.getYear(trip.getStarttimestamp());
      }
      return ok(views.html.template.tourSearchModal.render(view, tripId, srcID, imgName, Utils.formatDateControl(trip.starttimestamp), season));
    }
    view.message = "You are not authorized to import a booking for this trip";
    return ok(views.html.common.message.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result tourSearch() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    final DynamicForm form = formFactory.form().bindFromRequest();
    TourSearchView searchView =  new TourSearchView();
    searchView.tours =  new ArrayList<>();
    try {
      String inKeyword = form.get("inKeyword");
      int inSrcId = Integer.parseInt(form.get("inSrcId"));
      searchView.brand = FeedSourcesInfo.byId(inSrcId);
      if(searchView.brand.equals(FeedHelperBigFive.BIG_FIVE_FEED_SRC_NAME)) {
        searchView.brandUrl = "http://www.bigfive.com/contact-big-five/";
      }
      searchView.srcId = inSrcId;
      String inTripId = form.get("inTripId");
      if (inTripId != null && inTripId.trim().length() > 0 &&
          inKeyword != null && inKeyword.trim().length() > 0 &&
          inSrcId > 0) {
        searchView.tripId = inTripId;
        List<Template> tours;
        /* OLD Search Logic, may be we should have dropdown for seasons, otherwise trips are missing
        String season = form.get("inSeason");
        if(season != null && season.length() > 0) {
          tours = Template.findFromFeedByNameAndSeason(inSrcId, inKeyword, season);
        } else {
          tours = Template.findToursByKeywords(inKeyword, inSrcId);
        }
        */
        tours = Template.findToursByKeywords(inKeyword, inSrcId);

        if (tours != null && tours.size() > 0) {
          for (Template td : tours) {
            TemplateBookingView tour = new TemplateBookingView();
            tour.templateId = td.templateid;
            tour.templateName = td.getName();
            tour.comments = td.getDescription();
            tour.coverUrl = td.getImageUrl();
            searchView.tours.add(tour);
            List<TmpltDetails> details = TmpltDetails.findByTemplateId(td.templateid);
            if (details != null && !details.isEmpty()) {
              for (TmpltDetails tmpltDetails : details) {
                TripBookingDetailView detailView = new TripBookingDetailView();
                detailView.detailsId = tmpltDetails.detailsid;
                detailView.name = tmpltDetails.getName();
                detailView.comments = tmpltDetails.getComments();
                tour.activities.add(detailView);
              }
            }
          }
        } else {
          searchView.message = "No tours found";
        }
      } else {
        searchView.message = "No cruises found";
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      searchView.message = " No cruises found ";
    }

    return ok(views.html.template.tourSearchResults.render(searchView));
  }



  //Added by George: To retrieve and view photos from Template Bookings
  @With({Authenticated.class, Authorized.class})
  public  Result bookingPhotos(String detailsId) {
    if (detailsId != null) {
      TmpltDetails td = TmpltDetails.findByPK(detailsId);
      if (td == null) {
        BaseView baseView = new BaseView();
        baseView.message = "No record found.";
        return badRequest(views.html.common.message.render(baseView));
      }

      PoiView view = new PoiView();
      view.photos = getPhotosForBooking(td, td.getPoiId(), td.getPoiCmpyId(), false);
      if (view.photos.size() > 0) {
        return ok(views.html.poi.photoModal.render(view));
      }
    }
    BaseView baseView = new BaseView();
    baseView.message = "No photos available.";
    return ok(views.html.common.message.render(baseView));
  }

  public static List<AttachmentView> getPhotosForBooking(TmpltDetails detail, Long poiId, Integer cmpyId, boolean showAll) {
    List<AttachmentView> photos = new ArrayList<>();
    try {
        List<PoiFile> files = getBookingFiles(detail.getDetailsid(), poiId, cmpyId);
        if (files != null) {
          for (PoiFile f : files) {
            AttachmentView v = new AttachmentView();
            v.id = Long.toString(f.getFileId());
            v.name = f.getName();
            v.attachName = f.getNameSys();
            v.attachUrl = f.getUrl();
            v.attachType = f.getExtension().getMimeType();
            v.comments = f.getDescription();
            photos.add(v);
          }
        }

    }
    catch (Exception e) {
      Log.err("getPhotosForBooking: " + poiId, e.getMessage());
      e.printStackTrace();
    }
    return photos;
  }

  public static List<PoiFile> getBookingFiles(String detailsId, Long poiId, Integer cmpyId) {
    List<PoiFile> result = null;

    List<TripDetailFile> tdfs = TripDetailFile.getBookingFiles(detailsId);
    if (tdfs != null && tdfs.size() > 0) {
      result = new ArrayList<>(tdfs.size());
      for (TripDetailFile tdf : tdfs) {
        PoiFile pf = PoiFile.find.byId(tdf.getFileId().poiFileId);
        result.add(pf);
      }
    } else if (poiId != null && cmpyId != null) {
      result = PoiController.gatherFilesForPoi(poiId, cmpyId, "images");
    }
    return result;
  }

}
