package controllers;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.msg.flightstat.event.AlertEvent;
import com.mapped.publisher.msg.flightstat.status.FlightStatusMsg;
import com.mapped.publisher.utils.Utils;
import models.publisher.Company;
import models.publisher.FlightAlert;
import org.apache.commons.lang3.time.StopWatch;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static controllers.PoiController.autocompleteHelper;

/**
 * Created by ryan on 28/09/16.
 */
public class FlightController
    extends Controller {

  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("h:m a");

  public static class FlightDetails {
    @JsonProperty String term;
    @JsonProperty String cmpy;
  }

  public Result flightAutofill() {
    StopWatch sw = new StopWatch();
    sw.start();
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      FlightDetails fd = Json.fromJson(request().body().asJson(), FlightDetails.class);
    if (fd != null && fd.cmpy != null && !fd.cmpy.isEmpty() && fd.term != null && fd.term.trim().length() >= 4) {
      String airlineCode = null;
      int flightId = 0;
      try {
        String trimTerm = fd.term.trim().replace(" ", "");
        airlineCode = trimTerm.substring(0, 2);
        flightId = Integer.parseInt(trimTerm.substring(2));
      }
      catch (Exception e) {

      }
      if (airlineCode != null && flightId != 0) {
        try {
          ObjectNode result = Json.newObject();
          ArrayNode array = result.arrayNode();

          JsonNode queryItems = queryHelper(airlineCode, flightId, fd.cmpy);
          if (queryItems != null) {
            JsonNode departAirport = null;
            departAirport = queryItems.get("departAirport");
            JsonNode arrivalAirport = null;
            arrivalAirport = queryItems.get("arrivalAirport");
            ObjectNode classNode = Json.newObject();

            if (departAirport != null && departAirport.isArray() && departAirport.size() > 0
                && !departAirport.get(0).get("label").asText().equals("")
                && !departAirport.get(0).get("id").asText().equals("")) {
              classNode.put("departAirportLabel", departAirport.get(0).get("label").asText());
              classNode.put("departAirportId", departAirport.get(0).get("id").asText());
            }

            if (arrivalAirport != null && arrivalAirport.isArray() && arrivalAirport.size() > 0
                && !arrivalAirport.get(0).get("label").asText().equals("")
                && !arrivalAirport.get(0).get("id").asText().equals("")) {
              classNode.put("arrivalAirportLabel", arrivalAirport.get(0).get("label").asText());
              classNode.put("arrivalAirportId", arrivalAirport.get(0).get("id").asText());
            }

            if (queryItems.get("departTimeMS") != null && queryItems.get("departTimeMS").asLong() > 0) {
              LocalTime departTime = Instant.ofEpochMilli(queryItems.get("departTimeMS").asLong())
                                            .atZone(ZoneId.systemDefault())
                                            .toLocalTime();
              classNode.put("departTime", departTime.format(formatter));
            }

            if (queryItems.get("arriveTimeMS") != null && queryItems.get("arriveTimeMS").asLong() > 0) {
              LocalTime arrivalTime = Instant.ofEpochMilli(queryItems.get("arriveTimeMS").asLong())
                                             .atZone(ZoneId.systemDefault())
                                             .toLocalTime();
              classNode.put("arrivalTime", arrivalTime.format(formatter));
            }

            if (!queryItems.get("airlineCode").asText().equals("")) {
              classNode.put("airlineCode", queryItems.get("airlineCode").asText());
            }



            classNode.put("flightId", queryItems.get("flightId").asText());

            array.add(classNode);

            sw.stop();
            return ok(array);
          }
        }
        catch (NumberFormatException e) {
        }
      }

    }
    return ok();


  }

  private JsonNode queryHelper(String airlineCode, int flightId, String cmpyId) {
    return queryObjects(airlineCode, flightId, cmpyId);
  }

  private JsonNode queryObjects(String airlineCode, int flightId, String cmpyId) {
    FlightAlert fa = FlightAlert.findLatestByFlight(airlineCode, flightId);

    if (fa != null) {

      List<Integer> types = new ArrayList<>();
      types.add(0, 2);
      Set<Integer> searchCmpyIds = new HashSet<>();
      searchCmpyIds.add(Integer.parseInt(cmpyId));
      searchCmpyIds.add(Company.PUBLIC_COMPANY_ID);

      ArrayNode departAirports = autocompleteHelper(fa.getDepartAirport(), types, searchCmpyIds);
      ArrayNode arrivalAirports = autocompleteHelper(fa.getArrivalAirport(), types, searchCmpyIds);

      Long departTimeMS = null;
      Long arriveTimeMS = null;

      try {
        ObjectMapper mapper = new ObjectMapper();
        com.fasterxml.jackson.databind.ObjectReader r = mapper.readerFor(FlightStatusMsg.class);
        FlightStatusMsg jsonResp = r.readValue(fa.getResponse());
        if(jsonResp != null && jsonResp.getFlightStatuses() != null && jsonResp.getFlightStatuses().size() > 0
            && jsonResp.getFlightStatuses().get(0).getOperationalTimes() != null) {
          com.mapped.publisher.msg.flightstat.status.FlightStatus fs = jsonResp.getFlightStatuses().get(0);

          if (jsonResp.getFlightStatuses().get(0).getOperationalTimes().getPublishedDeparture() != null) {
            departTimeMS = Utils.getDateTimeFromISO(fs.getOperationalTimes().getPublishedDeparture().getDateLocal()).getMillis();

          } else if (jsonResp.getFlightStatuses().get(0).getOperationalTimes().getScheduledGateDeparture() != null) {
            departTimeMS = Utils.getDateTimeFromISO(fs.getOperationalTimes().getScheduledGateDeparture().getDateLocal()).getMillis();

          }
          if (jsonResp.getFlightStatuses().get(0).getOperationalTimes().getPublishedArrival() != null) {
            arriveTimeMS = Utils.getDateTimeFromISO(fs.getOperationalTimes().getPublishedArrival().getDateLocal()).getMillis();
          } else if (jsonResp.getFlightStatuses().get(0).getOperationalTimes().getScheduledGateArrival() != null) {
            arriveTimeMS = Utils.getDateTimeFromISO(fs.getOperationalTimes().getScheduledGateArrival().getDateLocal()).getMillis();
          }

        }

      } catch (Exception e) {

      }

      if (departTimeMS == null) {
        departTimeMS = fa.getNewDepartTime();
      }
      if (arriveTimeMS == null) {
        arriveTimeMS = fa.getNewArriveTime();
      }

      ObjectNode queries = Json.newObject();
      queries.put("flightId", fa.getFlightId());
      queries.put("airlineCode", fa.getAirlineCode());


      if (departTimeMS != null && departTimeMS > 0) {
        queries.put("departTimeMS", departTimeMS);
      }
      if (arriveTimeMS  != null && arriveTimeMS > 0) {
        queries.put("arriveTimeMS", arriveTimeMS);
      }
      queries.set("departAirport", departAirports);
      queries.set("arrivalAirport", arrivalAirports);

      return queries;
    }
    return null;
  }
}
