package controllers;

import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.TripSearchForm;
import com.mapped.publisher.persistence.PoiMgr;
import com.mapped.publisher.persistence.PoiRS;
import com.mapped.publisher.persistence.PoiTypeInfo;
import com.mapped.publisher.persistence.TripMgr;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;
import com.umapped.persistence.enums.ReservationType;
import models.publisher.*;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;
import org.h2.util.StringUtils;

import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created by twong on 16/05/16.
 */
public class ClientbaseController extends Controller {

  @Inject
  FormFactory formFactory;

  public static DecimalFormat decimalFormat = new DecimalFormat("#.00");

  public Result index () {
    BaseView view = new BaseView();
    view.message = flash(SessionConstants.SESSION_PARAM_MSG);
    return ok(views.html.clientbase.login.render(view));

  }
  public Result login() {
    //bind html form to form bean
    final DynamicForm form = formFactory.form().bindFromRequest();
    String userId = form.get("LoginID");
    String password = form.get("PassWord");
    //use bean validator framework
    if (userId == null || userId.isEmpty() || password == null || password.isEmpty()) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.clientbase.login.render(baseView));
    }

    try {
      UserProfile u = UserProfile.find.byId(userId);
      //try to see if the user provided an email address instead
      if (u == null && Utils.isValidEmailAddress(userId)) {
        List<UserProfile> users = UserProfile.findActiveByEmail(userId);
        if (users != null && users.size() == 1) {
          u = users.get(0);
        }
      }

      if(u == null || u.status == APPConstants.STATUS_DELETED) {
        BaseView view1 = new BaseView("Invalid user id/password combination - please retry");
        return ok(views.html.clientbase.login.render(view1));
      } else if (u.getUsertype() != 1) {
        BaseView view1 = new BaseView("Umapped Admin accounts canoot be used - please retry");
        return ok(views.html.clientbase.login.render(view1));
      }

      //TODO: Convert to accounts here
      if(u.status != APPConstants.STATUS_ACTIVE) {
        SecurityController.auditActivity(u.userid, APPConstants.USER_AUDIT_LOGON_LOCKED);
        BaseView view1 = new BaseView();
        view1.message = "Your account is currently not active - please contact your administrator or our support desk";
        return ok(views.html.clientbase.login.render(view1));
      }


      //let's see if the user has a service cutoff
      BillingSettingsForUser billing     = BillingSettingsForUser.findSettingsByUserId(u.getUserid());
      long                   currentTime = System.currentTimeMillis();
      if (billing != null) {
        if (billing.getCutoffTs() != null && billing.getCutoffTs() < currentTime) {
          SecurityController.auditActivity(u.userid, APPConstants.USER_AUDIT_LOGON_LOCKED);
          BaseView view1 = new BaseView();
          view1.message = "Your account is currently not active - please contact your administrator or our support"
                          + " desk";
          return ok(views.html.clientbase.login.render(view1));
        }
      }

      AccountController.AccountHelper ah = AccountController.AccountHelper.build(userId);
      Account a = Account.findByLegacyId(userId);
      ah.setAccount(a).setUserProfile(u);

      List<UserPwd> userPwds = UserPwd.authenticate(u.getUserid(),
                                                    Utils.hashPwd(password,
                                                                  String.valueOf(u.getCreatedtimestamp())));
      if (userPwds != null && userPwds.size() > 0) {
        Credentials cred = SecurityMgr.getCredentials(a);
        if (cred == null) {
          SecurityController.auditActivity(u.userid, APPConstants.USER_AUDIT_LOGON_LOCKED);
          BaseView view1 = new BaseView();
          view1.message = "System Error - please retry";
          return ok(views.html.clientbase.login.render(view1));
        }

        if (!cred.isUmappedAdmin() && !cred.hasCompany()) {
          BaseView view1 = new BaseView();
          view1.message = "Your account is currently not active - please contact your administrator or our support"
                          + " desk";
          SecurityController.auditActivity(u.userid, APPConstants.USER_AUDIT_LOGON_LOCKED);
          return ok(views.html.clientbase.login.render(view1));
        }


        SessionMgr sessionMgr = new SessionMgr(session());
        sessionMgr.setUserId(a.getLegacyId());
        sessionMgr.setAccountId(a.getUid());
        sessionMgr.setSessionExpiry();
        sessionMgr.setTimezoneOffsetMins(0);
        sessionMgr.setSessionId(cred.getSessionId());
        cred.updateCache();

        ah.authSuccess();
        flash(SessionConstants.SESSION_PARAM_MSG, " Welcome " + u.getFirstname());
        return redirect(routes.ClientbaseController.home());
      }
      else {
        boolean locked = ah.authFail();
        if (locked) {
          BaseView view1 = new BaseView();
          view1.message = "Your account has been locked. Please reset your password or contact support@umapped.com";
          return ok(views.html.clientbase.login.render(view1));
        }
      }
    }
    catch (Exception e) {
      Log.err("Invalid user id/password combination ", e);
      session().clear();
      BaseView view1 = new BaseView();
      view1.message = "Invalid user id/password combination - please retry";
      return ok(views.html.clientbase.login.render(view1));
    }

    BaseView view1 = new BaseView();
    view1.message = "Invalid user id/password combination - please retry";
    return ok(views.html.clientbase.login.render(view1));
  }

  @With({Authenticated.class, Authorized.class})
  public Result logout() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    sessionMgr.logout();
    flash(SessionConstants.SESSION_PARAM_MSG, "You have been logged out successfully.");
    return redirect(routes.ClientbaseController.index());
  }

  @With({Authenticated.class, Authorized.class})
  public Result home () {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Credentials cred = sessionMgr.getCredentials();
    HomeView view = getHomeView(sessionMgr, cred);

    TripsView trips = new TripsView();



    HashMap<String, String> agencyList = new HashMap<String, String>();
    if(cred.hasCompany()) {
      agencyList.put(cred.getCmpyId(), cred.getCmpyName());
    }
    if (agencyList.size() > 1) {
      agencyList.put("All", "All");
    }
    trips.tripAgencyList = agencyList;

    trips.cmpyId = "All";

    if (cred.isCmpyAdmin() || cred.hasGroupies()) {
      trips.displayCmpyTabs = true;
    }
    else {
      trips.displayCmpyTabs = false;
    }
    return ok(views.html.clientbase.trips.render (view, trips));
  }

  public static HomeView getHomeView(SessionMgr sessionMgr, Credentials cred) {
    HomeView view = new HomeView().withFirebaseURI(ConfigMgr.getAppParameter(com.mapped.common
                                                                                 .CoreConstants.FIREBASE_URI))
                                  .withFirebaseToken(sessionMgr.getCredentials().getFirebaseToken());



    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      view.message = msg;
    }

    view.cmpyName = cred.getCmpyName();

    view.firstName = cred.getFirstName();
    view.lastName = cred.getLastName();
    view.loggedInUserId = cred.getUserId();
    view.isUMappedAdmin = cred.isUmappedAdmin();
    view.isCmpyAdmin = cred.getCmpyLinkType() == AccountCmpyLink.LinkType.ADMIN;
    view.isPowerUser = cred.getCmpyLinkType() == AccountCmpyLink.LinkType.POWER;

    return view;
  }

  @With({Authenticated.class, Authorized.class})
  public Result doSearchTrips() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    int offsetMins = sessionMgr.getTimezoneOffsetMins();

    long currentClientTime = Utils.getCurrentClientDate(offsetMins);

    //bind html form to form bean
    Form<TripSearchForm> tripForm = formFactory.form(TripSearchForm.class);
    TripSearchForm tripSearchForm = tripForm.bindFromRequest().get();
    boolean allTrips = false;
    boolean searchArchives = false;
    FilterType filterType = FilterType.ACTIVE;

    if (tripSearchForm.isInAllTrips()) {
      allTrips = true;
    }

    if (tripSearchForm.isInArchived()) {
      searchArchives = true;
      filterType = FilterType.ALL;
    }


    String term = "";
    if (tripSearchForm.getInSearchTerm() != null && tripSearchForm.getInSearchTerm().length() > 0) {
      term = "%" + tripSearchForm.getInSearchTerm().replace(" ", "%") + "%";
    }
    else {
      term = "%";
    }


    String cmpyId = tripSearchForm.getInCmpyId();

    TripsView view = new TripsView();
    view.searchResults = new ArrayList<TripInfoView>();


    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      view.message = msg;
    }

    Credentials cred = sessionMgr.getCredentials();

    Map<String, Company> cmpiesMap = new HashMap<>();

    ArrayList<String> cmpies = new ArrayList<String>();

    HashMap<String, String> agencyList = new HashMap<>();
    if(cred.hasCompany()) {
      agencyList.put(cred.getCmpyId(), cred.getCmpyName());
    }


    view.tripAgencyList = agencyList;

    int startPos = tripSearchForm.getInStartPos();


    int startAdminPos = tripSearchForm.getInStartAdminPos();


    view.tableName = tripSearchForm.getInTableName();
    view.isTour = false;


    view.searchTerm = tripSearchForm.getInSearchTerm();
    view.cmpyId = cmpyId;
    view.searchResults = new ArrayList<TripInfoView>();

    view.searchStatus = tripSearchForm.getSearchStatus();
    view.allTrips = allTrips;
    view.searchArchives = searchArchives;
    if (cred.isCmpyAdmin() || cred.hasGroupies()) {
      view.displayCmpyTabs = true;
    }
    else {
      view.displayCmpyTabs = false;
    }

    ArrayList<Integer> statuses = new ArrayList<Integer>();
    if (view.searchStatus == 0) {
      statuses.add(0);
    }
    else if (view.searchStatus == 1) {
      statuses.add(1);
    }
    else {
      view.searchStatus = 3;
      statuses.add(0);
      statuses.add(1);
    }

    int maxRows = APPConstants.MAX_RESULT_COUNT;

    //separate all cmpies into 2 list for proper queries - member or admin
    ArrayList<String> adminCmpies = new ArrayList<String>();
    ArrayList<String> memberCmpies = new ArrayList<String>();
    if(cred.isCmpyAdmin(cmpyId)) {
      adminCmpies.add(cmpyId);
    } else {
      memberCmpies.add(cmpyId);
    }

    if (adminCmpies.size() > 0 && startAdminPos != -1) {
      List<Trip> t = new ArrayList<>();
      if (allTrips) {
        t = Trip.findActivePendingByCmpyIds(sessionMgr.getUserId(),
                                            maxRows,
                                            adminCmpies,
                                            term,
                                            statuses,
                                            startAdminPos,
                                            "tripName",
                                            "asc",
                                            filterType, currentClientTime);
      }
      else {
        t = Trip.findAllMyTripsByTime(sessionMgr.getUserId(),
                                      maxRows,
                                      adminCmpies,
                                      term,
                                      statuses,
                                      startAdminPos,
                                      allTrips,
                                      "tripName",
                                      "asc",
                                      filterType, currentClientTime);
      }
      if (t != null && t.size() > 0) {
        List<TripInfoView> trips = TripController.buildTripView(t, cmpiesMap, null);
        view.searchResults.addAll(trips);
        view.searchResultsAdminCount = t.size();
        view.nextAdminPos = -1;
        view.prevAdminPos = -1;
        if (startAdminPos >= APPConstants.MAX_RESULT_COUNT) {
          view.prevAdminPos = startAdminPos - APPConstants.MAX_RESULT_COUNT + 1;
          if (view.prevAdminPos < 0) {
            view.prevAdminPos = -1;
          }
        }
        if (t.size() == APPConstants.MAX_RESULT_COUNT) {
          //there are more records
          view.nextAdminPos = startAdminPos + APPConstants.MAX_RESULT_COUNT;
        }
      }
    }


    if (memberCmpies.size() > 0 && startAdminPos != -1) {
      Connection conn = null;
      try {
        conn = DBConnectionMgr.getConnection4Publisher();

        List<Trip> t2 = new ArrayList<>();

        if (allTrips) {
          t2 = TripMgr.findMyTrips(term,
              memberCmpies,
              sessionMgr.getUserId(),
              maxRows,
              startPos,
              statuses,
              conn,
              "tripName",
              "asc",
               filterType, currentClientTime);

        } else {
          t2 = TripMgr.findMyTrips2(term,
              memberCmpies,
              sessionMgr.getUserId(),
              maxRows,
              startPos,
              statuses,
              conn,
              allTrips,
              "tripName",
              "asc",
               filterType, currentClientTime);

        }

        if (t2 != null && t2.size() > 0) {
          List<TripInfoView> trips = TripController.buildTripView(t2, cmpiesMap, null);
          view.searchResults.addAll(trips);
          view.searchResultsCount = t2.size();
          view.nextPos = -1;
          view.prevPos = -1;
          if (startPos >= APPConstants.MAX_RESULT_COUNT) {
            view.prevPos = startPos - APPConstants.MAX_RESULT_COUNT + 1;
            if (view.prevPos < 0) {
              view.prevPos = -1;
            }
          }
          if (t2.size() == APPConstants.MAX_RESULT_COUNT) {
            //there are more records
            view.nextPos = startPos + APPConstants.MAX_RESULT_COUNT;
          }
        }
      }
      catch (Exception e) {
        Log.err("Error - ", e);
      }
      finally {
        if (conn != null) {
          try {
            conn.close();
          }
          catch (Exception e) {
            Log.err("Error - ", e);
          }
        }
      }
    }

    if (view.searchResults != null) {
      Collections.sort(view.searchResults, TripInfoView.TripInfoViewNameComparator);
    }

    if (startAdminPos > 0 || startPos > 0) {
      //continuation
      return ok(views.html.clientbase.searchTripsMore.render(view));

    }
    else {
      //new search
      return ok(views.html.clientbase.searchTrips.render(view));
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result tripDetails (String tripId) {
    try {
      SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      Credentials cred = sessionMgr.getCredentials();
      String userId = cred.getUserId();

      if (tripId == null) {
        BaseView view1 = new BaseView();
        view1.message = "System Error - please retry";
        return ok(views.html.common.message.render(view1));
      }

      if (tripId != null) {
        Trip trip = Trip.find.byId(tripId);
        //Anyone with read access can access all bookings
        SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(trip, sessionMgr);
        if (accessLevel.ge(SecurityMgr.AccessLevel.READ)) {

          //Clear ClientBase Groups from Cache
          String cacheKey = null;
          if (tripId != null && userId != null && !StringUtils.isNumber(userId)) {
            cacheKey = APPConstants.CACHE_CLIENTBASE_BOOKINGS_GROUP + userId + tripId;
          }
          CacheMgr.getRedis().delKey(cacheKey);


          TripBookingView view = new TripBookingView();
          view.lang = ctx().lang();
          view.accessLevel = accessLevel;
          view.loggedInUserId = sessionMgr.getUserId();
          view.setupPoiView(sessionMgr);
          String msg = flash(SessionConstants.SESSION_PARAM_MSG);
          if (msg != null && msg.length() > 0) {
            view.message = msg;
          }
          view.tripId = trip.tripid;
          view.tripCmpyId = trip.cmpyid;
          Company cmpy = Company.find.byId(trip.cmpyid);
          view.display12Hr = cmpy.display12hrClock();

          if (trip.starttimestamp > 0) {
            view.tripStartDatePrint = Utils.getDateStringPrint(trip.starttimestamp);
            view.tripStartDate = Utils.formatDateControlYYYY(trip.starttimestamp);
          }

          if (trip.endtimestamp > 0) {
            view.tripEndDatePrint = Utils.getDateStringPrint(trip.endtimestamp);
            view.tripEndDate = Utils.getDateString(trip.endtimestamp);
          }
          view.tripName = trip.name;
          view.tripStatus = trip.status;

          //get any notes
          //view.notes = BookingNoteController.getAllTripNotesView(trip, sessionMgr, true);

          List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
          if (tripDetails != null) {
            for (TripDetail tripDetail : tripDetails) {
              if (tripDetail.getDetailtypeid() != ReservationType.CRUISE_STOP) {
                TripBookingDetailView bookingDetails = BookingController.buildBookingView(tripId, tripDetail, true);
                view.addBookingDetailView(tripDetail.getDetailtypeid(), bookingDetails);
              }
            }

            //handle cross timezone booking
            if (view != null && view.flights != null && view.flights.size() > 0) {
              view.handleCrosTimezoneFlights();
            }


            return ok(views.html.clientbase.bookings.render(view));
          }
        }
      }
      BaseView view = new BaseView();
      view.message = "System Error";
      return ok(views.html.common.message.render(view));
    }
    catch (Exception e) {
      Log.err("System Error: ", e);
      BaseView view = new BaseView();
      view.message = "System Error";
      return ok(views.html.common.message.render(view));
    }

  }

  @With({Authenticated.class, Authorized.class})
  public Result groupBookings (String tripId, String bookingIds, String groupedIds) {
    try {
      SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      Credentials cred = sessionMgr.getCredentials();

      String userId = cred.getUserId();

      //HomeView homeView = getHomeView(sessionMgr, cred);

      if (tripId == null) {
        BaseView view1 = new BaseView();
        view1.message = "System Error - please retry";
        return ok(views.html.common.message.render(view1));
      }

      Map<String, ClientBaseBookingGroupView> clientBaseBookingGroupViewMap = null;


      if (tripId != null && bookingIds != null && !bookingIds.isEmpty()) {
        Trip trip = Trip.find.byId(tripId);
        //Anyone with read access can access all bookings
        SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(trip, sessionMgr);
        if (accessLevel.ge(SecurityMgr.AccessLevel.READ)) {

          //First Build Trip View
          TripBookingView view = new TripBookingView();
          //TripBookingView allBookings = new TripBookingView();
          view.lang = ctx().lang();
          view.accessLevel = accessLevel;
          view.loggedInUserId = sessionMgr.getUserId();
          view.setupPoiView(sessionMgr);
          String msg = flash(SessionConstants.SESSION_PARAM_MSG);
          if (msg != null && msg.length() > 0) {
            view.message = msg;
          }
          view.tripId = trip.tripid;
          view.tripCmpyId = trip.cmpyid;
          Company cmpy = Company.find.byId(trip.cmpyid);
          view.display12Hr = cmpy.display12hrClock();

          if (trip.starttimestamp > 0) {
            view.tripStartDatePrint = Utils.getDateStringPrint(trip.starttimestamp);
            view.tripStartDate = Utils.formatDateControlYYYY(trip.starttimestamp);
          }

          if (trip.endtimestamp > 0) {
            view.tripEndDatePrint = Utils.getDateStringPrint(trip.endtimestamp);
            view.tripEndDate = Utils.getDateString(trip.endtimestamp);
          }
          view.tripName = trip.name;
          view.tripStatus = trip.status;



          //Then Proceed to Group Bookings

          List<TripBookingDetailView> keyDetailViews = new ArrayList<>();
          Map<String, ClientBaseBookingGroupView> unsortedGroupedBookings = null;
          Map<String, ClientBaseBookingGroupView> sortedGroupedBookings = new LinkedHashMap<>();
          Map<String, TripDetail> importedDetails = new HashMap<>();

          List<String> newGroupIds = null; //List of newly selected groups

          //Created a list of newly selected bookings to group from string
          if (groupedIds != null && !groupedIds.isEmpty()) {
            newGroupIds = new ArrayList(Arrays.asList(groupedIds.split(",")));
          }

          String cacheKey = null;
          if (tripId != null && userId != null && !StringUtils.isNumber(userId)) {
            cacheKey = APPConstants.CACHE_CLIENTBASE_BOOKINGS_GROUP + userId + tripId;
          }

          if (cacheKey != null) {
            unsortedGroupedBookings = (Map<String, ClientBaseBookingGroupView>) CacheMgr.get(cacheKey);
          }

          //If Map exists in cache, then just adjust Map
          if (unsortedGroupedBookings != null && unsortedGroupedBookings.size() > 0) {

            if (newGroupIds != null && newGroupIds.size() > 0) {
              List<TripBookingDetailView> newTbdvGroup = new ArrayList<>();
              for (String id : newGroupIds) {
                //TripBookingDetailView tbdv = unsortedGroupedBookings.get(id);
                if (unsortedGroupedBookings.containsKey(id)) {
                  ClientBaseBookingGroupView oldCbgv = unsortedGroupedBookings.remove(id);
                  if (oldCbgv != null && oldCbgv.bookingDetailViews != null && oldCbgv.bookingDetailViews.size() > 0) {
                    TripBookingDetailView deletedTbdv = oldCbgv.bookingDetailViews.remove(0);
                    newTbdvGroup.add(deletedTbdv);
                    if (oldCbgv.bookingDetailViews != null && oldCbgv.bookingDetailViews.size() > 0) {
                      if (oldCbgv.bookingDetailViews.size() > 1) {
                        Collections.sort(oldCbgv.bookingDetailViews, TripBookingDetailView.TripBookingDetailViewComparator);
                      }
                      String newKey = oldCbgv.bookingDetailViews.get(0).detailsId;
                      oldCbgv.populateGroupInfo();
                      unsortedGroupedBookings.put(newKey, oldCbgv);
                    }
                  }

                } else {
                  for (String key : unsortedGroupedBookings.keySet()) {
                    ClientBaseBookingGroupView oldCbgv = unsortedGroupedBookings.get(key);
                    List<TripBookingDetailView> tbdvToBeDeleted = new ArrayList<>();
                    for (TripBookingDetailView tbdv : oldCbgv.bookingDetailViews) {
                      if (tbdv.detailsId.equals(id)) {
                        newTbdvGroup.add(tbdv);
                        tbdvToBeDeleted.add(tbdv);
                        //oldCbgv.bookingDetailViews.remove(tbdv);
                      }
                    }
                    if (tbdvToBeDeleted != null && tbdvToBeDeleted.size() > 0) {
                      oldCbgv.bookingDetailViews.removeAll(tbdvToBeDeleted);
                      oldCbgv.populateGroupInfo();
                    }
                    if (oldCbgv.bookingDetailViews.size() > 1) {
                      Collections.sort(oldCbgv.bookingDetailViews, TripBookingDetailView.TripBookingDetailViewComparator);
                    }
                    unsortedGroupedBookings.put(key, oldCbgv);

                  }
                }
              }

              //Create new CBGV with newly sorted tbdv-list and insert into unsortedGroup Map
              ClientBaseBookingGroupView newCbgv = new ClientBaseBookingGroupView();
              Collections.sort(newTbdvGroup, TripBookingDetailView.TripBookingDetailViewComparator);
              newCbgv.bookingDetailViews = newTbdvGroup;

              newCbgv.populateGroupInfo();
              unsortedGroupedBookings.put(newTbdvGroup.get(0).detailsId, newCbgv);

            }

            String newBookingIds = "";
            int i = 0;
            for (Map.Entry<String, ClientBaseBookingGroupView> entry : unsortedGroupedBookings.entrySet()) {
              List<TripBookingDetailView> tdbvList = entry.getValue().bookingDetailViews;

              for (int j = 0; j < tdbvList.size(); j++) {
                if (entry.getKey().equals(tdbvList.get(j).detailsId)) {
                  keyDetailViews.add(tdbvList.get(j));
                }

                String id = tdbvList.get(j).detailsId;
                newBookingIds += id;
                if (j < tdbvList.size() - 1) {
                  newBookingIds += "_";
                }
              }
              if (i < unsortedGroupedBookings.entrySet().size() - 1) {
                newBookingIds += ",";
              }
              i++;
            }

            //This ensures that the Booking Groups overall are also chronologically ordered
            if (keyDetailViews != null && keyDetailViews.size() > 1) {
              Collections.sort(keyDetailViews, TripBookingDetailView.TripBookingDetailViewComparator);
            }

            for (TripBookingDetailView t : keyDetailViews) {
              ClientBaseBookingGroupView cbgv = unsortedGroupedBookings.get(t.getDetailsId());
              if (cbgv != null) {
                sortedGroupedBookings.put(t.getDetailsId(), cbgv);
              }
            }

            //save this to the cach for this user + destination
            if (cacheKey != null && sortedGroupedBookings.size() > 0) {
              CacheMgr.set(cacheKey, sortedGroupedBookings, APPConstants.CACHE_TRANSIENT_EXPIRY_SECS);
            }

            return ok(views.html.clientbase.groupBookings.render(view, sortedGroupedBookings, newBookingIds));

          } else { //Doesn't exist in cache, so build from scratch
            unsortedGroupedBookings = new LinkedHashMap<>();

            //Map to hold Grouped Bookings with first booking as Key
            Map<String, List<String>> groupedBookingsMap = new HashMap<>();
            List<String> groupLists = new ArrayList(Arrays.asList(bookingIds.split(","))); //List of grouped bookings created from string


            //For each Group List, iterate through the elements (ids) in that group.
            //For each element in the iteration, check if the newly selected Ids to group contains the element,
            //If yes, remove element from group and continue.
            if (groupLists != null && groupLists.size() > 0) {
              for (String groupList : groupLists) {
                List<String> group = new ArrayList(Arrays.asList(groupList.split("_")));
                if (group != null && group.size() > 0) {
                  if (newGroupIds != null && newGroupIds.size() > 0) {
                    for (String id : newGroupIds) {
                      if (group.contains(id)) {
                        group.remove(id);
                      }
                    }
                  }
                  //Check to make sure the adjusted group list is not empty, then insert back into group map
                  if (group != null && group.size() > 0) {
                    groupedBookingsMap.put(group.get(0), group);
                  }
                }
              }
            }

            //Finally insert list of new GroupIds into Map with the first element as the key
            if (newGroupIds != null && newGroupIds.size() > 0) {
              groupedBookingsMap.put(newGroupIds.get(0), newGroupIds);
            }


            List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
            if (tripDetails != null) {
              Map<String, TripDetail> tripDetailsMap = new HashMap<>();
              for (TripDetail detail : tripDetails) {
                if (detail.getDetailtypeid() != ReservationType.CRUISE_STOP) {
                  tripDetailsMap.put(detail.detailsid, detail);
                }
              }


              String newBookingIds = "";
              int i = 0;
              for (Map.Entry<String, List<String>> entry : groupedBookingsMap.entrySet()) {

                String key = entry.getKey();
                List<String> group = entry.getValue();

                TripDetail keyTd = tripDetailsMap.get(key);
                if (keyTd != null) {
                  TripBookingDetailView keyTbdv = BookingController.buildBookingView(tripId, keyTd, true);
                  keyDetailViews.add(keyTbdv);
                }


                ClientBaseBookingGroupView cbgv = unsortedGroupedBookings.get(key);
                if (cbgv == null) {
                  cbgv = new ClientBaseBookingGroupView();
                  cbgv.bookingDetailViews = new ArrayList<>();
                }

                for (int j = 0; j < group.size(); j++) {
                  String id = group.get(j);
                  newBookingIds += id;
                  if (j < group.size() - 1) {
                    newBookingIds += "_";
                  }
                  TripDetail tripDetail = tripDetailsMap.get(id);
                  if (tripDetail != null) {
                    TripBookingDetailView tbdv = BookingController.buildBookingView(tripId, tripDetail, true);

                    importedDetails.put(tripDetail.detailsid, tripDetail);
                    view.addBookingDetailView(tripDetail.getDetailtypeid(), tbdv);

                    cbgv.bookingDetailViews.add(tbdv);
                  }
                }
                if (i < groupedBookingsMap.entrySet().size() - 1) {
                  newBookingIds += ",";
                }

                //This ensures Bookings within a Group are chronologically ordered
                if (cbgv != null && cbgv.bookingDetailViews != null && cbgv.bookingDetailViews.size() > 1) {
                  Collections.sort(cbgv.bookingDetailViews, TripBookingDetailView.TripBookingDetailViewComparator);
                }
                cbgv.populateGroupInfo();
                unsortedGroupedBookings.put(key, cbgv);
              }

              //This ensures that the Booking Groups overall are also chronologically ordered
              if (keyDetailViews != null && keyDetailViews.size() > 1) {
                Collections.sort(keyDetailViews, TripBookingDetailView.TripBookingDetailViewComparator);
              }

              for (TripBookingDetailView t : keyDetailViews) {
                ClientBaseBookingGroupView cbgv = unsortedGroupedBookings.get(t.getDetailsId());
                if (cbgv != null) {
                  sortedGroupedBookings.put(t.getDetailsId(), cbgv);

                }
              }

              //save this to the cach for this user + destination
              if (cacheKey != null && sortedGroupedBookings.size() > 0) {
                CacheMgr.set(cacheKey, sortedGroupedBookings, APPConstants.CACHE_TRANSIENT_EXPIRY_SECS);
              }

              return ok(views.html.clientbase.groupBookings.render(view, sortedGroupedBookings, newBookingIds));
            }
          }
        }
      }
      BaseView view = new BaseView();
      view.message = "System Error";
      return ok(views.html.common.message.render(view));
    }
    catch (Exception e) {
      Log.err("System Error - ", e);
      BaseView view = new BaseView();
      view.message = "System Error";
      return ok(views.html.common.message.render(view));
    }

  }

  @With({Authenticated.class, Authorized.class})
  public Result ungroupBookings (String tripId, String bookingIds, String ungroupedIds) {

    try {
      SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      Credentials cred = sessionMgr.getCredentials();

      String userId = cred.getUserId();

      //HomeView homeView = getHomeView(sessionMgr, cred);

      if (tripId == null) {
        BaseView view1 = new BaseView();
        view1.message = "System Error - please retry";
        return ok(views.html.common.message.render(view1));
      }


      if (tripId != null && bookingIds != null && !bookingIds.isEmpty()) {
        Trip trip = Trip.find.byId(tripId);
        //Anyone with read access can access all bookings
        SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(trip, sessionMgr);
        if (accessLevel.ge(SecurityMgr.AccessLevel.READ)) {

          //First Build Trip View
          TripBookingView view = new TripBookingView();
          //TripBookingView allBookings = new TripBookingView();
          view.lang = ctx().lang();
          view.accessLevel = accessLevel;
          view.loggedInUserId = sessionMgr.getUserId();
          view.setupPoiView(sessionMgr);
          String msg = flash(SessionConstants.SESSION_PARAM_MSG);
          if (msg != null && msg.length() > 0) {
            view.message = msg;
          }
          view.tripId = trip.tripid;
          view.tripCmpyId = trip.cmpyid;
          Company cmpy = Company.find.byId(trip.cmpyid);
          view.display12Hr = cmpy.display12hrClock();

          if (trip.starttimestamp > 0) {
            view.tripStartDatePrint = Utils.getDateStringPrint(trip.starttimestamp);
            view.tripStartDate = Utils.formatDateControlYYYY(trip.starttimestamp);
          }

          if (trip.endtimestamp > 0) {
            view.tripEndDatePrint = Utils.getDateStringPrint(trip.endtimestamp);
            view.tripEndDate = Utils.getDateString(trip.endtimestamp);
          }
          view.tripName = trip.name;
          view.tripStatus = trip.status;



          //Then Proceed to Group Bookings

          List<TripBookingDetailView> keyDetailViews = new ArrayList<>();
          Map<String, ClientBaseBookingGroupView> unsortedGroupedBookings = null;
          Map<String, ClientBaseBookingGroupView> sortedGroupedBookings = new LinkedHashMap<>();
          Map<String, TripDetail> importedDetails = new HashMap<>();

          List<String> newGroupIds = null; //List of newly selected groups

          //Created a list of newly selected bookings to group from string
          if (ungroupedIds != null && !ungroupedIds.isEmpty()) {
            newGroupIds = new ArrayList(Arrays.asList(ungroupedIds.split(",")));
          }

          String cacheKey = null;
          if (tripId != null && userId != null && !StringUtils.isNumber(userId)) {
            cacheKey = APPConstants.CACHE_CLIENTBASE_BOOKINGS_GROUP + userId + tripId;
          }

          if (cacheKey != null) {
            unsortedGroupedBookings = (Map<String, ClientBaseBookingGroupView>) CacheMgr.get(cacheKey);
          }

          //If Map exists in cache, then just adjust Map
          if (unsortedGroupedBookings != null && unsortedGroupedBookings.size() > 0 && newGroupIds != null && newGroupIds.size() > 0) {

            if (newGroupIds != null && newGroupIds.size() > 0) {
              List<TripBookingDetailView> newTbdvGroup = new ArrayList<>();
              for (String id : newGroupIds) {
                //TripBookingDetailView tbdv = unsortedGroupedBookings.get(id);
                if (unsortedGroupedBookings.containsKey(id)) {
                  ClientBaseBookingGroupView oldCbgv = unsortedGroupedBookings.remove(id);
                  if (oldCbgv != null && oldCbgv.bookingDetailViews != null && oldCbgv.bookingDetailViews.size() > 0) {
                    TripBookingDetailView deletedTbdv = oldCbgv.bookingDetailViews.remove(0);
                    newTbdvGroup.add(deletedTbdv);
                    if (oldCbgv.bookingDetailViews != null && oldCbgv.bookingDetailViews.size() > 0) {
                      if (oldCbgv.bookingDetailViews.size() > 1) {
                        Collections.sort(oldCbgv.bookingDetailViews, TripBookingDetailView.TripBookingDetailViewComparator);
                      }
                      String newKey = oldCbgv.bookingDetailViews.get(0).detailsId;
                      oldCbgv.populateGroupInfo();
                      unsortedGroupedBookings.put(newKey, oldCbgv);
                    }
                  }

                } else {
                  for (String key : unsortedGroupedBookings.keySet()) {
                    ClientBaseBookingGroupView oldCbgv = unsortedGroupedBookings.get(key);
                    List<TripBookingDetailView> tbdvToBeDeleted = new ArrayList<>();
                    for (TripBookingDetailView tbdv : oldCbgv.bookingDetailViews) {
                      if (tbdv.detailsId.equals(id)) {
                        newTbdvGroup.add(tbdv);
                        tbdvToBeDeleted.add(tbdv);
                        //oldCbgv.bookingDetailViews.remove(tbdv);
                      }
                    }
                    if (tbdvToBeDeleted != null && tbdvToBeDeleted.size() > 0) {
                      oldCbgv.bookingDetailViews.removeAll(tbdvToBeDeleted);
                      oldCbgv.populateGroupInfo();
                    }
                    if (oldCbgv.bookingDetailViews.size() > 1) {
                      Collections.sort(oldCbgv.bookingDetailViews, TripBookingDetailView.TripBookingDetailViewComparator);
                    }
                    unsortedGroupedBookings.put(key, oldCbgv);

                  }
                }
              }

              //Create new CBGV for each TBDV and insert into unsortedGroup Map
              for (TripBookingDetailView tbdv : newTbdvGroup) {
                ClientBaseBookingGroupView newCbgv = new ClientBaseBookingGroupView();
                newCbgv.bookingDetailViews = new ArrayList<>();
                newCbgv.bookingDetailViews.add(tbdv);
                newCbgv.populateGroupInfo();
                unsortedGroupedBookings.put(tbdv.detailsId, newCbgv);
              }


            }

            String newBookingIds = "";
            int i = 0;
            for (Map.Entry<String, ClientBaseBookingGroupView> entry : unsortedGroupedBookings.entrySet()) {
              List<TripBookingDetailView> tdbvList = entry.getValue().bookingDetailViews;

              for (int j = 0; j < tdbvList.size(); j++) {
                if (entry.getKey().equals(tdbvList.get(j).detailsId)) {
                  keyDetailViews.add(tdbvList.get(j));
                }

                String id = tdbvList.get(j).detailsId;
                newBookingIds += id;
                if (j < tdbvList.size() - 1) {
                  newBookingIds += "_";
                }
              }
              if (i < unsortedGroupedBookings.entrySet().size() - 1) {
                newBookingIds += ",";
              }
              i++;
            }

            //This ensures that the Booking Groups overall are also chronologically ordered
            if (keyDetailViews != null && keyDetailViews.size() > 1) {
              Collections.sort(keyDetailViews, TripBookingDetailView.TripBookingDetailViewComparator);
            }

            for (TripBookingDetailView t : keyDetailViews) {
              ClientBaseBookingGroupView cbgv = unsortedGroupedBookings.get(t.getDetailsId());
              if (cbgv != null) {
                sortedGroupedBookings.put(t.getDetailsId(), cbgv);
              }
            }

            //save this to the cach for this user + destination
            if (cacheKey != null && sortedGroupedBookings.size() > 0) {
              CacheMgr.set(cacheKey, sortedGroupedBookings, APPConstants.CACHE_TRANSIENT_EXPIRY_SECS);
            }

            return ok(views.html.clientbase.groupBookings.render(view, sortedGroupedBookings, newBookingIds));

          } else { //Doesn't exist in cache, so build from scratch
            unsortedGroupedBookings = new LinkedHashMap<>();

            //Map to hold Grouped Bookings with first booking as Key
            Map<String, List<String>> groupedBookingsMap = new HashMap<>();
            List<String> groupLists = new ArrayList(Arrays.asList(bookingIds.split(","))); //List of grouped bookings created from string


            //For each Group List, iterate through the elements (ids) in that group.
            //For each element in the iteration, check if the newly selected Ids to group contains the element,
            //If yes, remove element from group and continue.
            if (groupLists != null && groupLists.size() > 0) {
              for (String groupList : groupLists) {
                List<String> group = new ArrayList(Arrays.asList(groupList.split("_")));
                if (group != null && group.size() > 0) {
                  if (newGroupIds != null && newGroupIds.size() > 0) {
                    for (String id : newGroupIds) {
                      if (group.contains(id)) {
                        group.remove(id);
                      }
                    }
                  }
                  //Check to make sure the adjusted group list is not empty, then insert back into group map
                  if (group != null && group.size() > 0) {
                    groupedBookingsMap.put(group.get(0), group);
                  }
                }
              }
            }

            //Finally insert list of new GroupIds into Map with the first element as the key
            if (newGroupIds != null && newGroupIds.size() > 0) {
              groupedBookingsMap.put(newGroupIds.get(0), newGroupIds);
            }
            //Finally insert each individual Booking as it's own list
            if (newGroupIds != null && newGroupIds.size() > 0) {
              for (String bkId : newGroupIds) {
                List<String> newBkList = new ArrayList<>();
                newBkList.add(bkId);
                groupedBookingsMap.put(bkId, newBkList);
              }
            }


            List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
            if (tripDetails != null) {
              Map<String, TripDetail> tripDetailsMap = new HashMap<>();
              for (TripDetail detail : tripDetails) {
                if (detail.getDetailtypeid() != ReservationType.CRUISE_STOP) {
                  tripDetailsMap.put(detail.detailsid, detail);
                }
              }


              String newBookingIds = "";
              int i = 0;
              for (Map.Entry<String, List<String>> entry : groupedBookingsMap.entrySet()) {

                String key = entry.getKey();
                List<String> group = entry.getValue();

                TripDetail keyTd = tripDetailsMap.get(key);
                if (keyTd != null) {
                  TripBookingDetailView keyTbdv = BookingController.buildBookingView(tripId, keyTd, true);
                  keyDetailViews.add(keyTbdv);
                }


                ClientBaseBookingGroupView cbgv = unsortedGroupedBookings.get(key);
                if (cbgv == null) {
                  cbgv = new ClientBaseBookingGroupView();
                  cbgv.bookingDetailViews = new ArrayList<>();
                }

                for (int j = 0; j < group.size(); j++) {
                  String id = group.get(j);
                  newBookingIds += id;
                  if (j < group.size() - 1) {
                    newBookingIds += "_";
                  }
                  TripDetail tripDetail = tripDetailsMap.get(id);
                  if (tripDetail != null) {
                    TripBookingDetailView tbdv = BookingController.buildBookingView(tripId, tripDetail, true);

                    importedDetails.put(tripDetail.detailsid, tripDetail);
                    view.addBookingDetailView(tripDetail.getDetailtypeid(), tbdv);

                    cbgv.bookingDetailViews.add(tbdv);
                  }
                }
                if (i < groupedBookingsMap.entrySet().size() - 1) {
                  newBookingIds += ",";
                }

                //This ensures Bookings within a Group are chronologically ordered
                if (cbgv != null && cbgv.bookingDetailViews != null && cbgv.bookingDetailViews.size() > 1) {
                  Collections.sort(cbgv.bookingDetailViews, TripBookingDetailView.TripBookingDetailViewComparator);
                }
                cbgv.populateGroupInfo();
                unsortedGroupedBookings.put(key, cbgv);
              }

              //This ensures that the Booking Groups overall are also chronologically ordered
              if (keyDetailViews != null && keyDetailViews.size() > 1) {
                Collections.sort(keyDetailViews, TripBookingDetailView.TripBookingDetailViewComparator);
              }

              for (TripBookingDetailView t : keyDetailViews) {
                ClientBaseBookingGroupView cbgv = unsortedGroupedBookings.get(t.getDetailsId());
                if (cbgv != null) {
                  sortedGroupedBookings.put(t.getDetailsId(), cbgv);

                }
              }

              //save this to the cach for this user + destination
              if (cacheKey != null && sortedGroupedBookings.size() > 0) {
                CacheMgr.set(cacheKey, sortedGroupedBookings, APPConstants.CACHE_TRANSIENT_EXPIRY_SECS);
              }

              return ok(views.html.clientbase.groupBookings.render(view, sortedGroupedBookings, newBookingIds));
            }
          }
        }
      }
      BaseView view = new BaseView();
      view.message = "System Error";
      return ok(views.html.common.message.render(view));
    }
    catch (Exception e) {
      Log.err("System Error - ", e);
      BaseView view = new BaseView();
      view.message = "System Error";
      return ok(views.html.common.message.render(view));
    }

  }


  @With({Authenticated.class, Authorized.class})
  public Result importCB (String tripId, String bookingIds) {
    try {
      SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      Credentials cred = sessionMgr.getCredentials();
      String userId = cred.getUserId();

      HomeView homeView = getHomeView(sessionMgr, cred);

      if (tripId == null) {
        BaseView view1 = new BaseView();
        view1.message = "System Error - please retry";
        return ok(views.html.common.message.render(view1));
      }

      if (tripId != null && bookingIds != null && !bookingIds.isEmpty()) {
        //Determine whether to use default grouping (via importSrcId) or user's grouping
        boolean useDefaultGroup = true;
        if (bookingIds.contains("_")) {
          useDefaultGroup = false;
        }

        Trip trip = Trip.find.byId(tripId);
        //Anyone with read access can access all bookings
        SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(trip, sessionMgr);

        if (accessLevel.ge(SecurityMgr.AccessLevel.READ)) {
          //Map to hold Grouped Bookings with first booking as Key
          Map<String, List<String>> groupedBookingsMap = new HashMap<>();
          List<String> groupLists = new ArrayList(Arrays.asList(bookingIds.split(","))); //List of grouped bookings created from string

          List <String> selectedDetailIds = new ArrayList<>();
          if (groupLists != null && groupLists.size() > 0) {
            for (String groupList : groupLists) {
              List<String> group = new ArrayList(Arrays.asList(groupList.split("_")));
              if (group != null && group.size() > 0) {
                groupedBookingsMap.put(group.get(0), group);

                selectedDetailIds.addAll(group);
              }
            }
          }

          TripBookingView view = new TripBookingView();

          TripBookingView allBookings = new TripBookingView();
          view.lang = ctx().lang();
          view.accessLevel = accessLevel;
          view.loggedInUserId = sessionMgr.getUserId();
          view.setupPoiView(sessionMgr);
          String msg = flash(SessionConstants.SESSION_PARAM_MSG);
          if (msg != null && msg.length() > 0) {
            view.message = msg;
          }
          view.tripId = trip.tripid;
          view.tripCmpyId = trip.cmpyid;
          Company cmpy = Company.find.byId(trip.cmpyid);
          view.display12Hr = cmpy.display12hrClock();

          if (trip.starttimestamp > 0) {
            view.tripStartDatePrint = Utils.getDateStringPrint(trip.starttimestamp);
            view.tripStartDate = Utils.formatDateControlYYYY(trip.starttimestamp);
          }

          if (trip.endtimestamp > 0) {
            view.tripEndDatePrint = Utils.getDateStringPrint(trip.endtimestamp);
            view.tripEndDate = Utils.getDateString(trip.endtimestamp);
          }
          view.tripName = trip.name;
          view.tripStatus = trip.status;

          //get any notes
          //view.notes = BookingNoteController.getAllTripNotesView(trip, sessionMgr, true);

          List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
          if (tripDetails != null) {

            List<TripBookingDetailView> keyDetailViews = new ArrayList<>();
            Map<String, ClientBaseBookingGroupView> unsortedGroupedBookings = null;
            Map<String, ClientBaseBookingGroupView> sortedGroupedBookings = new LinkedHashMap<>();;
            Map<String, TripDetail> importedDetails = new HashMap<>();

            String cacheKey = null;
            if (tripId != null && userId != null && !StringUtils.isNumber(userId)) {
              cacheKey = APPConstants.CACHE_CLIENTBASE_BOOKINGS_GROUP + userId + tripId;
            }

            if (cacheKey != null) {
              unsortedGroupedBookings = (Map<String, ClientBaseBookingGroupView>) CacheMgr.get(cacheKey);
            }

            Map<String, TripBookingDetailView> tripDetailsViewMap = new HashMap<>();
            Map<String, TripDetail> tripDetailsMap = new HashMap<>();
            for (TripDetail detail : tripDetails) {
              TripBookingDetailView bookingDetails = BookingController.buildBookingView(tripId, detail, true);
              if (detail.getDetailtypeid() != ReservationType.CRUISE_STOP) {
                tripDetailsViewMap.put(detail.detailsid, bookingDetails);
                tripDetailsMap.put(detail.detailsid, detail);
              }
              allBookings.addBookingDetailView(detail.getDetailtypeid(), bookingDetails);
              view.addBookingDetailView(detail.getDetailtypeid(), bookingDetails); //TripBookingView Needs to include all tbdvs to retrieve cruisestops and other infos

              if (selectedDetailIds.contains(detail.getDetailsid())) {
                importedDetails.put(detail.getDetailsid(), detail);
              }
            }

            if (unsortedGroupedBookings != null && unsortedGroupedBookings.size() > 0) {
              sortedGroupedBookings = unsortedGroupedBookings;
            } else {
              if (!useDefaultGroup) {
                unsortedGroupedBookings = new HashMap<>();

                for (Map.Entry<String, List<String>> entry : groupedBookingsMap.entrySet()) {
                  String key = entry.getKey();
                  List<String> group = entry.getValue();
                  TripBookingDetailView keyTbdv = tripDetailsViewMap.get(key);
                  if (keyTbdv != null) {
                    keyDetailViews.add(keyTbdv);
                  }


                  ClientBaseBookingGroupView cbgv = unsortedGroupedBookings.get(key);
                  if (cbgv == null) {
                    cbgv = new ClientBaseBookingGroupView();
                    cbgv.bookingDetailViews = new ArrayList<>();
                  }

                  for (String id : group) {

                    TripBookingDetailView tbdv = tripDetailsViewMap.get(id);
                    if (tbdv != null) {
                      //view.addBookingDetailView(tbdv.getDetailsTypeId(), tbdv); //Uneccessary since we Need to include all tbdvs to retrieve cruisestops and other infos

                      cbgv.bookingDetailViews.add(tbdv);
                    }
                  }

                  //This ensures Bookings within a Group are chronologically ordered
                  if (cbgv.bookingDetailViews != null && cbgv.bookingDetailViews.size() > 1) {
                    Collections.sort(cbgv.bookingDetailViews, TripBookingDetailView.TripBookingDetailViewComparator);
                  }
                  cbgv.populateGroupInfo();
                  unsortedGroupedBookings.put(key, cbgv);
                }

                //This ensures that the Booking Groups overall are also chronologically ordered
                if (keyDetailViews != null && keyDetailViews.size() > 1) {
                  Collections.sort(keyDetailViews, TripBookingDetailView.TripBookingDetailViewComparator);
                }

                for (TripBookingDetailView t : keyDetailViews) {
                  ClientBaseBookingGroupView cbgv = unsortedGroupedBookings.get(t.getDetailsId());
                  if (cbgv != null && cbgv.bookingDetailViews != null && cbgv.bookingDetailViews.size() > 0) {
                    sortedGroupedBookings.put(t.getDetailsId(), cbgv);
                  }
                }
              } else {
                unsortedGroupedBookings = new HashMap<>();

                for (Map.Entry<String, List<String>> entry : groupedBookingsMap.entrySet()) {
                  String key = entry.getKey();
                  List<String> group = entry.getValue();
                  TripBookingDetailView keyTbdv = tripDetailsViewMap.get(key);
                  if (keyTbdv != null) {
                    keyDetailViews.add(keyTbdv);
                  }

                  ClientBaseBookingGroupView cbgv = null;

                  for (String id : group) { //only 1 per list

                    TripBookingDetailView tbdv = tripDetailsViewMap.get(id);
                    if (tbdv != null) {
                      //view.addBookingDetailView(tbdv.getDetailsTypeId(), tbdv); //Uneccessary since we Need to include all tbdvs to retrieve cruisestops and other infos

                  /*TripDetail tripDetail = tripDetailsMap.get(id);
                  String srcKey = "";
                  if (tbdv.importSrc != null) {
                    srcKey = tbdv.importSrc.toString();
                    if (tbdv.importSrcId != null) {
                      srcKey += tbdv.importSrcId;
                    }
                    //for GDS, we will further split this by provider name
                    //ie a sabre import with flights on 1 airline + hotel will be grouped as 2 set of reservations
                    String cmpyName = tripDetail.getName();

                    if (tripDetail.importSrc != null) {
                      switch (tripDetail.importSrc) {
                        case AMADEUS_CHECKMYTRIPS:
                        case AMADEUS_PDF:
                        case APOLLO:
                        case SABRE_PDF:
                        case SABRE_TRIPCASE:
                        case TRAVELPORT__VIEWTRIPS:
                        case WORLDSPAN_EMAIL:
                        case WORLDSPAN_MYTRIPS:
                        case WORLDSPAN_TRAMS:
                          if (tripDetail.getPoiId() != null) {
                            PoiRS poi = PoiMgr.findForCmpy(tripDetail.getPoiId(), tripDetail.getPoiCmpyId());
                            if (poi != null && poi.getName() != null && !poi.getName().isEmpty()) {
                              cmpyName = poi.getName();
                            } else if (tripDetail.detailtypeid.getRootLevelType() == ReservationType.FLIGHT) {
                              String code ="";
                              if (cmpyName.contains(" ") && cmpyName.indexOf(" ") > 1) {
                                code = cmpyName.substring(0, cmpyName.indexOf(" "));
                                if (code != null && code.trim().length() == 2) {
                                  List<Integer> poiTypes = new ArrayList<Integer>();
                                  poiTypes.add(PoiTypeInfo.Instance().byName("Airline").getId());
                                  Set<Integer> cmpyIds = new HashSet<>();
                                  cmpyIds.add(0);
                                  cmpyIds.add(Company.find.byId(trip.cmpyid).getCmpyId());
                                  Map<Long, List<PoiRS>> airlines = PoiMgr.findByCode(code, poiTypes, cmpyIds, 1);
                                  if (airlines != null) {
                                    for (List<PoiRS> list : airlines.values()) {
                                      for (PoiRS rs: list) {
                                        cmpyName = rs.getName();
                                        break;
                                      }
                                      break;
                                    }
                                  }
                                }
                              }
                            }

                          }
                          srcKey += cmpyName;
                          break;
                        default:
                          if (tripDetail.importSrc == BookingSrc.ImportSrc.API && tripDetail.importSrcId != null) {
                            if (tripDetail.importSrcId.equals("SABRE")) {
                              if (tripDetail.getPoiId() != null) {
                                PoiRS poi = PoiMgr.findForCmpy(tripDetail.getPoiId(), tripDetail.getPoiCmpyId());
                                if (poi != null && poi.getName() != null && !poi.getName().isEmpty()) {
                                  cmpyName = poi.getName();
                                }
                              }
                              srcKey += cmpyName;
                            }
                          }
                          break;
                      }
                    }

                  }

                  if (!srcKey.isEmpty()) {
                    //key = srcKey;
                  }*/

                      if (tbdv.origComments != null) {
                        tbdv.origComments = tbdv.origComments.replace("\n", " ").replace("\r", " ");
                        if (tbdv.origComments.length() > 256) {
                          tbdv.origComments = tbdv.origComments.substring(0, 255);
                        }
                      }

                      cbgv = sortedGroupedBookings.get(key);
                      if (cbgv == null) {
                        cbgv = new ClientBaseBookingGroupView();
                        cbgv.bookingDetailViews = new ArrayList<>();
                      }
                      cbgv.bookingDetailViews.add(tbdv);
                    }
                  }

                  //This ensures Bookings within a Group are chronologically ordered
                  if (cbgv != null && cbgv.bookingDetailViews != null && cbgv.bookingDetailViews.size() > 1) {
                    Collections.sort(cbgv.bookingDetailViews, TripBookingDetailView.TripBookingDetailViewComparator);
                  }
                  cbgv.populateGroupInfo();
                  unsortedGroupedBookings.put(key, cbgv);
                }

                //This ensures that the Booking Groups overall are also chronologically ordered
                if (keyDetailViews != null && keyDetailViews.size() > 1) {
                  Collections.sort(keyDetailViews, TripBookingDetailView.TripBookingDetailViewComparator);
                }

                for (TripBookingDetailView t : keyDetailViews) {
                  ClientBaseBookingGroupView cbgv = unsortedGroupedBookings.get(t.getDetailsId());
                  if (cbgv != null && cbgv.bookingDetailViews != null && cbgv.bookingDetailViews.size() > 0) {
                    sortedGroupedBookings.put(t.getDetailsId(), cbgv);
                  }
                }
              }
            }


            //let's group all the bookings together
            StringBuilder xmlBuilder = new StringBuilder();
            // xmlBuilder.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
            xmlBuilder.append("<RESCARD>\n" );
            xmlBuilder.append("<RESERVATIONLIST>\n");
            for (Map.Entry<String, ClientBaseBookingGroupView> entry: sortedGroupedBookings.entrySet()) {
              String key = entry.getKey();
              ClientBaseBookingGroupView details = entry.getValue();
              if (details != null) {
                if (details.bookingDetailViews.size() <= 1) {
                  //process each as a separate reservation
                  if (details.bookingDetailViews.size() == 1) {
                    TripBookingDetailView p = details.bookingDetailViews.get(0);
                    F.Tuple<String, String> resInfo = new F.Tuple(details.vendorName, details.vendorCode);
                    Html src = null;
                    if (p.detailsTypeId.getRootLevelType() != ReservationType.CRUISE_STOP) { //All cruise stops should follow their respective cruise and no processed as a reservation
                      if (p.detailsTypeId.getRootLevelType() == ReservationType.FLIGHT) {
                        /*if (p.poiMain != null && p.poiMain.getName() != null && !p.poiMain.getName().isEmpty()
                            && p.poiMain.getCode() != null && !p.poiMain.getCode().isEmpty()) {
                          resInfo = new F.Tuple(p.poiMain.getName(), p.poiMain.getCode());
                        }*/
                        src = views.html.clientbase.xml.ServiceProviderDetail.render(view, p, EnhancedTripBookingDetailView.parseFlight(p), resInfo);
                      } else if (p.detailsTypeId.getRootLevelType() == ReservationType.HOTEL) {
                        src = views.html.clientbase.xml.ServiceProviderDetail.render(view, p, EnhancedTripBookingDetailView.parseHotel(p), resInfo);
                      } else if (p.detailsTypeId.getRootLevelType() == ReservationType.CRUISE) {
                        src = views.html.clientbase.xml.ServiceProviderDetail.render(view, p, EnhancedTripBookingDetailView.parseCruiseCB(view, p, allBookings), resInfo);
                      } else {
                        src = views.html.clientbase.xml.ServiceProviderDetail.render(view, p, null, resInfo);
                      }
                      if (src != null) {
                        xmlBuilder.append(src.toString());
                      }
                    }
                  }
                } else {
                  //process group as one
                  List<TripDetail> importedDetailsList = new ArrayList<>(importedDetails.values());
                  Html reservationXML = views.html.clientbase.xml.ServiceProviderList.render(details, view, allBookings, importedDetailsList);
                  xmlBuilder.append(reservationXML.toString());
                }
              }
            }
            xmlBuilder.append("</RESERVATIONLIST>\n");
            xmlBuilder.append("</RESCARD>\n" );



            String xml = xmlBuilder.toString().replaceAll("(?m)^[ \t]*\r?\n", "");
            //System.out.println(xml);

            return ok(views.html.clientbase.importCB.render(homeView, view, xml, sortedGroupedBookings, bookingIds));
          }
        }
      }
      BaseView view = new BaseView();
      view.message = "System Error";
      return ok(views.html.common.message.render(view));
    }
    catch (Exception e) {
      Log.err("System Error - ", e);
      BaseView view = new BaseView();
      view.message = "System Error";
      return ok(views.html.common.message.render(view));
    }

  }

  @With({Authenticated.class, Authorized.class})
  public Result editGroup() {
    try {
      SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      Credentials cred = sessionMgr.getCredentials();
      String userId = cred.getUserId();

      final DynamicForm form = formFactory.form().bindFromRequest();
      String tripId = form.get("inTripId");
      String groupId = form.get("inGroupId");
      String bookingIds = form.get("inBookingIds");
      String resType = form.get("inResType");
      String vendorName = form.get("inVendorName");
      String vendorCode = form.get("inVendorCode");
      String totalPrice = form.get("inTotalPrice");
      String totalTaxes = form.get("inTotalTaxes");
      String confirmationNo = form.get("inConfirmationNo");

      if (tripId != null && !tripId.isEmpty() && userId != null && !userId.isEmpty() && groupId != null
          && !groupId.isEmpty() && bookingIds != null && !bookingIds.isEmpty()) {

        Trip trip = Trip.find.byId(tripId);
        //Anyone with read access can access all bookings
        SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(trip, sessionMgr);
        if (accessLevel.ge(SecurityMgr.AccessLevel.READ)) {

          String cacheKey = null;
          if (tripId != null && userId != null && !StringUtils.isNumber(userId)) {
            cacheKey = APPConstants.CACHE_CLIENTBASE_BOOKINGS_GROUP + userId + tripId;
          }

          Map<String, ClientBaseBookingGroupView> cachedGroupedBookings = null;

          if (cacheKey != null) {
            cachedGroupedBookings = (Map<String, ClientBaseBookingGroupView>) CacheMgr.get(cacheKey);
          }

          if (cachedGroupedBookings != null && cachedGroupedBookings.size() > 0) {
            ClientBaseBookingGroupView oldCbgv = cachedGroupedBookings.get(groupId);

            if (oldCbgv != null) {
              if (resType != null && !resType.isEmpty()) {
                oldCbgv.resType = resType;
              }
              if (vendorName != null && !vendorName.isEmpty()) {
                oldCbgv.vendorName = vendorName;
              }

              if (vendorName != null && !vendorName.isEmpty() && vendorName.equalsIgnoreCase("UMAPPED")) {
                oldCbgv.vendorCode = "UM";
              } else if (Integer.parseInt(resType) == 0) {
                TripBookingDetailView tbdv = oldCbgv.bookingDetailViews.get(0);
                if (tbdv.detailsTypeId.getRootLevelType() == ReservationType.FLIGHT) {
                  if (tbdv.poiMain != null && tbdv.poiMain.getName() != null && !tbdv.poiMain.getName().isEmpty()
                      && tbdv.poiMain.getCode() != null && !tbdv.poiMain.getCode().isEmpty()) {
                    if (vendorName.trim().equalsIgnoreCase(tbdv.poiMain.getName())) {
                      oldCbgv.vendorCode = tbdv.poiMain.getCode();
                    } else {
                      oldCbgv.vendorCode = "";
                    }
                  }
                }
              } else {
                oldCbgv.vendorCode = "";
              }

              if (totalPrice != null && !totalPrice.isEmpty()) {
                oldCbgv.totalPrice = formatUMPrices(totalPrice);
              }
              if (totalTaxes != null && !totalTaxes.isEmpty()) {
                oldCbgv.totalTaxes = formatUMPrices(totalTaxes);
              }
              if (confirmationNo != null && !confirmationNo.isEmpty()) {
                oldCbgv.confirmationNo = confirmationNo;
              }

              cachedGroupedBookings.replace(groupId, oldCbgv);
            }

            //save this to the cach for this user + destination
            if (cacheKey != null && cachedGroupedBookings != null && cachedGroupedBookings.size() > 0) {
              CacheMgr.set(cacheKey, cachedGroupedBookings, APPConstants.CACHE_TRANSIENT_EXPIRY_SECS);
            }
          }

          return redirect(routes.ClientbaseController.groupBookings(tripId, bookingIds, null));
        }
      }

      BaseView view = new BaseView();
      view.message = "System Error - please retry";
      return ok(views.html.common.message.render(view));

    } catch (Exception e) {
      Log.err("System Error - ", e);
      BaseView view = new BaseView();
      view.message = "System Error";
      return ok(views.html.common.message.render(view));
    }
  }

  public static F.Tuple<String, String> getReservationProvider (TripBookingDetailView v, TripDetail td) {
    F.Tuple<String, String> importInfo = null;
    if (td.importSrc != null) {

      switch (td.importSrc) {
        case AEROPLAN_PDF:
          importInfo = new F.Tuple<>("AEROPLAN", "AEP");
          break;
        case ALPINE:
          importInfo = new F.Tuple<>("ALPINE ADVENTURES", "ALP");
          break;
        case BIG_FIVE:
          importInfo = new F.Tuple<>("BIG FIVE", "BGF");
          break;
        case CENTRAV:
          importInfo = new F.Tuple<>("CENTRAV", "CTV");
          break;
        case CLASSIC_VACATIONS:
          importInfo = new F.Tuple<>("CLASSIC VACATIONS", "CLS");
          break;
        case GLOBUS_TOUR:
          importInfo = new F.Tuple<>("GLOBUS", "GLB");
          break;
        case QUEEN_OF_CLUB:
          importInfo = new F.Tuple<>("QUEEN OF CLUB", "QOC");
          break;
        case SHORE_TRIPS:
          importInfo = new F.Tuple<>("SHORE TRIPS", "SHR");
          break;
        case TRAMADA:
          importInfo = new F.Tuple<>("TRAMADA", "TRM");
          break;
        case TRAVEL2:
          importInfo = new F.Tuple<>("TRAVEL2", "TR2");
          break;
        case TRAVEL2_ISLANDS:
          importInfo = new F.Tuple<>("ISLANDS IN THE SUN", "ISL");
          break;
        case TRAVEL2_QANTAS:
          importInfo = new F.Tuple<>("QANTAS VACATIONS", "QTV");
          break;
        case VIRTUOSO_AIR:
          importInfo = new F.Tuple<>("VIRTUOSO AIR", "VTA");
          break;
        case VIRTUOSO_CRUISE:
          importInfo = new F.Tuple<>("VIRTUOSO CRUISE", "VTC");
          break;
        case AMADEUS_CHECKMYTRIPS:
        case AMADEUS_PDF:
        case APOLLO:
        case SABRE_PDF:
        case SABRE_TRIPCASE:
        case TRAVELPORT__VIEWTRIPS:
        case WORLDSPAN_EMAIL:
        case WORLDSPAN_MYTRIPS:
        case WORLDSPAN_TRAMS:
          String cmpyName = td.getName();
          String code = "";
          if (td.getPoiId() != null) {
            PoiRS poi = PoiMgr.findForCmpy(td.getPoiId(), td.getPoiCmpyId());
            if (poi != null && poi.getName() != null && !poi.getName().isEmpty()) {
              cmpyName = poi.getName();
            } else if (td.detailtypeid.getRootLevelType() == ReservationType.FLIGHT) {
              if (cmpyName.contains(" ") && cmpyName.indexOf(" ") > 1) {
                code = cmpyName.substring(0, cmpyName.indexOf(" "));
                if (code != null && code.trim().length() == 2) {
                  List<Integer> poiTypes = new ArrayList<Integer>();
                  poiTypes.add(PoiTypeInfo.Instance().byName("Airline").getId());
                  Set<Integer> cmpyIds = new HashSet<>();
                  cmpyIds.add(0);
                  Trip trip = Trip.findByPK(td.getTripid());
                  cmpyIds.add(Company.find.byId(trip.cmpyid).getCmpyId());
                  Map<Long, List<PoiRS>> airlines = PoiMgr.findByCode(code, poiTypes, cmpyIds, 1);
                  if (airlines != null) {
                    for (List<PoiRS> list : airlines.values()) {
                      for (PoiRS rs: list) {
                        cmpyName = rs.getName();
                        break;
                      }
                      break;
                    }
                  }
                }
              }
            }

          }
          importInfo = new F.Tuple<>(cmpyName.toUpperCase(), code.toUpperCase());
          break;
        default:
          if (td.importSrc == BookingSrc.ImportSrc.API && td.importSrcId != null) {
            if (td.importSrcId.equals("SABRE")) {
               cmpyName = td.getName();
               code = "";
              if (td.getPoiId() != null) {
                PoiRS poi = PoiMgr.findForCmpy(td.getPoiId(), td.getPoiCmpyId());
                if (poi != null && poi.getName() != null && !poi.getName().isEmpty()) {
                  cmpyName = poi.getName();
                } else if (td.detailtypeid.getRootLevelType() == ReservationType.FLIGHT) {
                  if (cmpyName.contains(" ") && cmpyName.indexOf(" ") > 1) {
                    code = cmpyName.substring(0, cmpyName.indexOf(" "));
                    if (code != null && code.trim().length() == 2) {
                      List<Integer> poiTypes = new ArrayList<Integer>();
                      poiTypes.add(PoiTypeInfo.Instance().byName("Airline").getId());
                      Set<Integer> cmpyIds = new HashSet<>();
                      cmpyIds.add(0);
                      Trip trip = Trip.findByPK(td.getTripid());
                      cmpyIds.add(Company.find.byId(trip.cmpyid).getCmpyId());
                      Map<Long, List<PoiRS>> airlines = PoiMgr.findByCode(code, poiTypes, cmpyIds, 1);
                      if (airlines != null) {
                        for (List<PoiRS> list : airlines.values()) {
                          for (PoiRS rs: list) {
                            cmpyName = rs.getName();
                            break;
                          }
                          break;
                        }
                      }
                    }
                  }
                }

              }
            } else if (td.importSrcId.equals("SKI.COM")) {
              importInfo = new F.Tuple<>("SKI.COM", "SKI");
            } else if (td.importSrcId.equals("ULTIMATE JET VACATIONS")) {
              importInfo = new F.Tuple<>("ULTIMATE JET VACATIONS", "UJV");
            } else if (td.importSrcId.equals("CLIENTBASE")) {
              importInfo = new F.Tuple<>("CLIENTBASE", "CB");
            } else if (td.importSrcId.equals("HOTELZON")) {
              importInfo = new F.Tuple<>("HOTELZON", "HTZ");
            }
          }
          break;
      }
    }

    if (td.getDetailtypeid().getRootLevelType() == ReservationType.CRUISE) {
      importInfo = getReservationProviderFromBooking(v, td);
    }

    if (importInfo == null) {
      return new F.Tuple<>("UMAPPED", "UM");
    } else
      return importInfo;

  }

  public static F.Tuple<String, String> getReservationProviderForBooking (TripBookingDetailView v, List<TripDetail> details) {
    for (TripDetail td: details) {
      if (td.getDetailsid().equals(v.detailsId)) {
        return getReservationProviderFromBooking(v, td);
      }
    }
    return new F.Tuple<>("UMAPPED", "UM");
  }

  public static F.Tuple<String, String> getReservationProviderFromBooking (TripBookingDetailView v, TripDetail td) {
    F.Tuple<String, String> importInfo = null;
    String cmpyName = v.name;

    switch (td.detailtypeid.getRootLevelType()) {
      case FLIGHT:
        if (td.getPoiId() != null) {
          PoiRS airline = PoiMgr.findForCmpy(td.getPoiId(), td.getPoiCmpyId());
          if (airline != null) {
            importInfo = new F.Tuple<>(airline.getName(), airline.getCode());
          }
        }

        if (importInfo == null) {
          String code ="";
          if (v.name.contains(" ") && v.name.indexOf(" ") > 1) {
            code = v.name.substring(0, v.name.indexOf(" "));
            if (code != null && code.trim().length() == 2) {
              List<Integer> poiTypes = new ArrayList<Integer>();
              poiTypes.add(PoiTypeInfo.Instance().byName("Airline").getId());
              Set<Integer> cmpyIds = new HashSet<>();
              Trip trip = Trip.findByPK(td.getTripid());
              cmpyIds.add(0);
              cmpyIds.add(Company.find.byId(trip.cmpyid).getCmpyId());
              Map<Long, List<PoiRS>> airlines = PoiMgr.findByCode(code, poiTypes, cmpyIds, 1);
              if (airlines != null) {
                for (List<PoiRS> list : airlines.values()) {
                  for (PoiRS rs: list) {
                    code = rs.getCode();
                    cmpyName = rs.getName();
                    break;
                  }
                  break;
                }
              }
            }
          }
          importInfo = new F.Tuple<>(cmpyName, code);
        }
        break;
      case HOTEL:
      case TRANSPORT:
      case ACTIVITY:
        if (td.getPoiId() != null) {
          PoiRS poi = PoiMgr.findForCmpy(td.getPoiId(), td.getPoiCmpyId());
          if (poi != null) {
            cmpyName = poi.getName();
            if (poi.getCode() != null) {
              importInfo = new F.Tuple(cmpyName, poi.getCode());
              break;
            }
          }
        }
        if (cmpyName != null) {
          importInfo = new F.Tuple(cmpyName, "");
        }
        break;
      case CRUISE:
        String code = null;
        if (td.getPoiId() != null) {
          PoiRS poi = PoiMgr.findForCmpy(td.getPoiId(), td.getPoiCmpyId());
          if (poi != null) {
            cmpyName = poi.getName();
            code = poi.getCode();
          }
        }
        if (code == null) {
          code = cmpyName;
        }

        if (code != null) {
          code = code.toLowerCase();
          if (code.contains("ama waterway")) {
            importInfo = new F.Tuple("AMA Waterways", "AM");
          } else if (code.contains("azamara")) {
            importInfo = new F.Tuple("Azamara", "AZ");
          } else if (code.contains("celebrity")) {
            importInfo = new F.Tuple("Celebrity Cruises", "CB");
          } else if (code.contains("costa")) {
            importInfo = new F.Tuple("Costa Cruise Lines", "CO");
          } else if (code.contains("crystal")) {
            importInfo = new F.Tuple("Crystal Cruises", "CS");
          } else if (code.contains("cunard")) {
            importInfo = new F.Tuple("Cunard Cruises", "CU");
          } else if (code.contains("disney")) {
            importInfo = new F.Tuple("Disney Cruise Line", "DS");
          } else if (code.contains("holland")) {
            importInfo = new F.Tuple("Holland America Line", "HA");
          } else if (code.contains("island")) {
            importInfo = new F.Tuple("Island Cruises", "IC");
          } else if (code.contains("msc")) {
            importInfo = new F.Tuple("MSC Cruises", "MSC");
          } else if (code.contains("norwegian")) {
            importInfo = new F.Tuple("Norwegian Cruise Line", "NC");
          } else if (code.contains("princess")) {
            importInfo = new F.Tuple("Princess Cruises", "PC");
          } else if (code.contains("oceania")) {
            importInfo = new F.Tuple("Oceania Cruise Line", "OE");
          } else if (code.contains("regent")) {
            importInfo = new F.Tuple("Regent Seven Seas Cruise Line", "RE");
          } else if (code.contains("royal")) {
            importInfo = new F.Tuple("Royal Caribbean International", "RC");
          } else if (code.contains("viking")) {
            importInfo = new F.Tuple("Viking River Cruises", "VR");
          }
        }
        break;
    }

    if (importInfo == null)
      return new F.Tuple<>("UMAPPED", "UM");

    return importInfo;
  }

  public static String getResType(List<TripBookingDetailView> details) {
    int flights = 0;
    int hotels = 0;
    int carRentals = 0;
    int cruise = 0;
    int rail = 0;
    int tour = 0;
    int transport = 0;

    for (TripBookingDetailView v: details) {
      switch (v.detailsTypeId.getRootLevelType()) {
        case FLIGHT:
          flights++;
          break;
        case CRUISE:
          cruise++;
          break;
        case HOTEL:
          hotels++;
          break;
        case ACTIVITY:
          tour++;
          break;
        case TRANSPORT:
          if (v.detailsTypeId == ReservationType.CAR_RENTAL || v.detailsTypeId == ReservationType.CAR_DROPOFF)
            carRentals++;
          else if (v.detailsTypeId == ReservationType.RAIL)
              rail++;
          else
            transport++;

          break;
      }
    }


    if(flights > 0 && hotels == 0 && carRentals == 0 && cruise == 0 && rail == 0 && tour == 0 && transport == 0) {
      return "0";
    } else if(flights == 0 && hotels > 0 && carRentals == 0 && cruise == 0 && rail == 0 && tour == 0 && transport == 0) {
      return "1";
    } else if(cruise > 0) {
      return "3";
    } else if(flights == 0 && hotels == 0 && carRentals > 0 && cruise == 0 && rail == 0 && tour == 0 && transport == 0) {
      return "2";
    } else if(flights == 0 && hotels == 0 && carRentals == 0 && cruise == 0 && rail > 0 && tour == 0 && transport == 0) {
      return "6";
    } else if(flights == 0 && hotels == 0 && carRentals == 0 && cruise == 0 && rail == 0 && tour == 0 && transport > 0) {
      return "9";
    } else {
      return "7";
    }
  }

  public static String getGroupCurrency(List<TripBookingDetailView> details) {
    String currency = "";
    for (TripBookingDetailView v: details) {
      if (v.getCurrency() != null && !v.getCurrency().isEmpty()) {
        currency = v.getCurrency();
        break;
      }
    }
    return currency;
  }

  public static String getGroupConfirmationNo(List<TripBookingDetailView> details) {
    String confirmNo = "";
    for (TripBookingDetailView v: details) {
      if (v.getBookingNumber() != null && !v.getBookingNumber().isEmpty()) {
        confirmNo = v.getBookingNumber();
        break;
      }
    }
    return confirmNo;
  }

  public static String formatCBPrices(String amount) {
    String result = "";
    if (amount != null && !amount.isEmpty()) {
      result = formatUMPrices(amount);
      result  = result.replace(".", ""); //replace all non-numeric
    }
    return result;
  }

  public static String formatUMPrices(String amount) {
    String result = "";
    if (amount != null && !amount.isEmpty()) {
      amount = amount.replaceAll("[^\\d.]", ""); //replace all non-numeric
      Double dblAmt = Double.parseDouble(amount);
      if (dblAmt != null && dblAmt > 0.0) {
        result = decimalFormat.format(dblAmt);
      }
    }
    return result;
  }
}
