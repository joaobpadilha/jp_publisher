package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.common.SessionConstants;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.persistence.PageAttachType;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import com.umapped.itinerary.analyze.recommendation.RecommendationList;
import com.umapped.itinerary.analyze.recommendation.RecommendedItem;
import com.umapped.itinerary.analyze.recommendation.RecommendedItemType;
import com.umapped.service.offer.RecommendOffer;
import models.publisher.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import com.avaje.ebean.Ebean;
import java.util.List;

/**
 * Created by wei on 2017-04-17.
 */
public class RecommendationNoteController
    extends Controller {

  private static String Template = "<table border='0' padding='0'><tr><td style='vertical-align: top'>" + "<span " +
                                   "style='font-size: 24px; color: #b5b5b5'>%s</span><br/>" + "<span " +
                                   "style='font-size: 20px; font-weight: bold'>%s</span><br>" + "<span " +
                                   "style='font-size: 12px; font-weight: bold'>%s</span><br><br>" + "%s %s</td>" +
                                   "<td width= '150px'  style='display:block; min-height: 200px; vertical-align:top;position:relative' align='center'>%s%s" +
                                   "<div style='position: absolute; width: 160px;bottom: 0;font-size:10px; color: #666666'>Bought you by %s</div>" +
                                   "</td></tr></table>";

  private static String BookingButton = "<button style='background-color: #ffffff; " + "" + "border: 2px solid; color: black; " +
                                        "padding: 10px 35px; text-align: center; " + "text-decoration: none; display: " +
                                        "inline-block; font-size: 16px; margin-bottom: 10px" + "'>BOOK</button>";

  private static String Image = "<img src='%s' style='max-height: 130px; max-width: 130px; margin-bottom: 20px'/>";

  private static String PriceTable = "<br><br><div style='font-weight:bold'><ul>%s</ul</div>";

  private static String PriceRow = "<li>%s: %s %s</li>";

  @BodyParser.Of(BodyParser.Json.class) @With({Authenticated.class, Authorized.class})
  public Result addRecommendationNote(String tripId) {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    JsonNode jsonRequest = request().body().asJson();

    RecommendOffer recommendation = Json.fromJson(jsonRequest, RecommendOffer.class);
    Trip tripModel = Trip.findByPK(tripId);
    if (tripModel == null || SecurityMgr.getTripAccessLevel(tripModel, sessionMgr.getCredentials()).lt(SecurityMgr
                                                                                                           .AccessLevel.APPEND)) {
      flash(SessionConstants.SESSION_PARAM_MSG, "Unauthorized access.");
      return ok();
    }

    RecommendedItem item = recommendation.getItem();

    String bookingSection = "";

    TripNote tn = TripNote.buildTripNote(tripModel, sessionMgr.getUserId());
    tn.setName(item.name);
    if (tn.getName() == null || tn.getName().isEmpty()) {
      if (RecommendedItemType.Content.equals(item.type)) {
        tn.setName("Information");
      } else if (RecommendedItemType.Activity.equals(item.type)) {
        tn.setName("Suggestion");
        bookingSection = BookingButton;
      } else {
        tn.setName("Note");
      }
    }
    if (RecommendedItemType.Activity.equals(item.type)) {
      tn.setName("Suggestion");
      bookingSection = BookingButton;
    }

    String imageContent = "";
    if (!StringUtils.isEmpty(item.imageUrl)) {
      imageContent = String.format(Image, item.imageUrl);
    }

    if(recommendation.getNoteDate() != null && !recommendation.getNoteDate().equals("NA")) {
      try {
        long ms = Utils.getMilliSecs(recommendation.getNoteDate());
        if (ms > 1) {
          tn.setNoteTimestamp(ms);
        }
      } catch (Exception e) {

      }
    } else if (recommendation.getNoteDate() == null || recommendation.getNoteDate().isEmpty()) {
      long timestamp = ZonedDateTime.of(recommendation.getDate(), LocalTime.of(0, 0), ZoneId.of("UTC"))
                                    .toInstant()
                                    .toEpochMilli();

      tn.setNoteTimestamp(timestamp);
    }
    String title = "";

    if(bookingSection.equals(BookingButton)) {
      //
      tn.setName("Recommended: " + item.name);
      String s = views.html.offer.offercard.render("", item.description, buildPriceSection1(item), item.imageUrl, "Brought to you by Musement").toString();
      tn.setIntro(s);
      tn.setTag("musement");

    } else {
      String s = views.html.offer.event.render(title, item.name, buildAddressSection(item), item.description, buildPriceSection(item), imageContent, bookingSection, item.supplier).toString();
      tn.setTag("Wcities");



      tn.setIntro(s.toString());
    }

    int currRows = TripNote.tripNoteCount(tripModel.tripid);

    tn.setRank(++currRows);


    tn.setType(TripNote.NoteType.CONTENT);
    tn.save();

    //add images if any
    if (item.imageUrl != null && !item.imageUrl.isEmpty() && !bookingSection.equals(BookingButton)) {
      try {
        String fileName = item.imageUrl.substring(item.imageUrl.lastIndexOf("/") + 1);
        if (fileName == null || fileName.isEmpty()) {
          fileName = "img.jpg";

        }
        FileImage fileImage = ImageController.downloadAndSaveImage(item.imageUrl, fileName, FileSrc.FileSrcType.IMG_WCITIES,"", sessionMgr.getCredentials().getAccount(), false);
        if (fileImage != null) {
          TripNoteAttach tna = TripNoteAttach.build(tn, sessionMgr.getUserId());
          tna.setAttachName(fileName);
          tna.setAttachType(PageAttachType.PHOTO_LINK);
          tna.setImage(fileImage);
          tna.setComments(item.imageAttribution);
          tna.setName(fileName);
          tna.save();
        }
      } catch (Exception e) {
        e.printStackTrace();
        Log.err("RecommendationNoteController:cannot download image for note: " + tn.getNoteId() + " url: " + item.imageUrl);
      }
    }
    BookingController.invalidateCache(tripId, null);
    flash(SessionConstants.SESSION_PARAM_MSG, "WCITIES guide added successfully.");
    return ok();
  }

  @BodyParser.Of(BodyParser.Json.class) @With({Authenticated.class, Authorized.class})
  public Result addRecommendationNotes(String tripId) {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    JsonNode jsonRequest = request().body().asJson();

    RecommendOffer recommendation = Json.fromJson(jsonRequest, RecommendOffer.class);
    Trip tripModel = Trip.findByPK(tripId);
    if (tripModel == null || SecurityMgr.getTripAccessLevel(tripModel, sessionMgr.getCredentials()).lt(SecurityMgr
                                                                                                           .AccessLevel.APPEND)) {
      flash(SessionConstants.SESSION_PARAM_MSG, "Unauthorized access.");
      return ok();
    }
    List<RecommendedItem> items = recommendation.getItems();

    if (items != null) {
      for (RecommendedItem item : items) {
        String bookingSection = "";

        TripNote tn = TripNote.buildTripNote(tripModel, sessionMgr.getUserId());
        tn.setName(item.name);
        if (tn.getName() == null || tn.getName().isEmpty()) {
          if (RecommendedItemType.Content.equals(item.type)) {
            tn.setName("Information");
          } else if (RecommendedItemType.Activity.equals(item.type)) {
            tn.setName("Suggestion");
            bookingSection = BookingButton;
          } else {
            tn.setName("Note");
          }
        }
        if (RecommendedItemType.Activity.equals(item.type)) {
          tn.setName("Suggestion");
          bookingSection = BookingButton;
        }

        String imageContent = "";
        if (!StringUtils.isEmpty(item.imageUrl)) {
          imageContent = String.format(Image, item.imageUrl);
        }

        if (recommendation.getNoteDate() != null && !recommendation.getNoteDate().equals("NA")) {
          try {
            long ms = Utils.getMilliSecs(recommendation.getNoteDate());
            if (ms > 1) {
              tn.setNoteTimestamp(ms);
            }
          } catch (Exception e) {

          }
        } else if (recommendation.getNoteDate() == null || recommendation.getNoteDate().isEmpty()) {
          long timestamp = ZonedDateTime.of(recommendation.getDate(), LocalTime.of(0, 0), ZoneId.of("UTC"))
                                        .toInstant()
                                        .toEpochMilli();

          tn.setNoteTimestamp(timestamp);
        }
        String title = "";

        if (bookingSection.equals(BookingButton)) {
          //
          tn.setName("Recommended: " + item.name);
          String s = views.html.offer.offercard.render("", item.description, buildPriceSection1(item), item.imageUrl, "Brought to you by Musement").toString();
          tn.setIntro(s);
          tn.setTag("musement");

        } else {
          String s = views.html.offer.event.render(title, item.name, buildAddressSection(item), item.description, buildPriceSection(item), imageContent, bookingSection, item.supplier).toString();
          tn.setTag("Wcities");


          tn.setIntro(s.toString());
        }

        int currRows = TripNote.tripNoteCount(tripModel.tripid);

        tn.setRank(++currRows);


        tn.setType(TripNote.NoteType.CONTENT);
        tn.save();

        //add images if any
        if (item.imageUrl != null && !item.imageUrl.isEmpty() && !bookingSection.equals(BookingButton)) {
          try {
            String fileName = item.imageUrl.substring(item.imageUrl.lastIndexOf("/") + 1);
            if (fileName == null || fileName.isEmpty()) {
              fileName = "img.jpg";

            }
            FileImage fileImage = ImageController.downloadAndSaveImage(item.imageUrl, fileName, FileSrc.FileSrcType.IMG_WCITIES, "", sessionMgr.getCredentials().getAccount(), false);
            if (fileImage != null) {
              TripNoteAttach tna = TripNoteAttach.build(tn, sessionMgr.getUserId());
              tna.setAttachName(fileName);
              tna.setAttachType(PageAttachType.PHOTO_LINK);
              tna.setImage(fileImage);
              tna.setComments(item.imageAttribution);
              tna.setName(fileName);
              tna.save();
            }
          } catch (Exception e) {
            e.printStackTrace();
            Log.err("RecommendationNoteController:cannot download image for note: " + tn.getNoteId() + " url: " + item.imageUrl);
          }
        }
      }
    }
    BookingController.invalidateCache(tripId, null);
    flash(SessionConstants.SESSION_PARAM_MSG, "Selected WCITIES guides added successfully.");
    return ok();
  }
  
  public static int insertWcitiesAsTripNote (Trip tripModel, RecommendedItem item, LocalDate itemDate, String inNoteDate, Account account) throws Exception {
    if (tripModel != null && item != null) {
      try {
        String bookingSection = "";
        Ebean.beginTransaction();
        TripNote tn = TripNote.buildTripNote(tripModel, account.getLegacyId());
        tn.setName(item.name);
        if (tn.getName() == null || tn.getName().isEmpty()) {
          if (RecommendedItemType.Content.equals(item.type)) {
            tn.setName("Information");
          }
          else if (RecommendedItemType.Activity.equals(item.type)) {
            tn.setName("Suggestion");
            bookingSection = BookingButton;
          }
          else {
            tn.setName("Note");
          }
        }
        if (RecommendedItemType.Activity.equals(item.type)) {
          tn.setName("Suggestion");
          bookingSection = BookingButton;
        }

        String imageContent = "";
        if (!StringUtils.isEmpty(item.imageUrl)) {
          imageContent = String.format(Image, item.imageUrl);
        }

        if (inNoteDate != null && !inNoteDate.equals("NA")) {
          try {
            long ms = Utils.getMilliSecs(inNoteDate);
            if (ms > 1) {
              tn.setNoteTimestamp(ms);
            }
          }
          catch (Exception e) {
            tn.setTag(APPConstants.NOTE_TAG_LAST_ITEM);
          }
        }
        else if (inNoteDate == null || inNoteDate.isEmpty()) {
          long timestamp = ZonedDateTime.of(itemDate, LocalTime.of(0, 0), ZoneId.of("UTC")).toInstant().toEpochMilli();

          tn.setNoteTimestamp(timestamp);
        }
        else {
          tn.setTag(APPConstants.NOTE_TAG_LAST_ITEM);

        }
        String title = "";

        if (bookingSection.equals(BookingButton)) {
          //
          tn.setName("Recommended: " + item.name);
          String s = views.html.offer.offercard.render("",
                                                       item.description,
                                                       buildPriceSection1(item),
                                                       item.imageUrl,
                                                       "Brought to you by Musement").toString();
          tn.setIntro(s);
          tn.setTag("musement");

        }
        else {
          String s = views.html.offer.event.render(title,
                                                   item.name,
                                                   buildAddressSection(item),
                                                   item.description,
                                                   buildPriceSection(item),
                                                   imageContent,
                                                   bookingSection,
                                                   item.supplier).toString();

          if (tn.getTag() == null || tn.getTag().isEmpty()) {
            tn.setTag(APPConstants.WCITIES_NOTE_TAG);
          }
          else {
            if (!tn.getTag().contains(APPConstants.WCITIES_NOTE_TAG)) {
              tn.setTag(tn.getTag() + ", " + APPConstants.WCITIES_NOTE_TAG);
            }
          }

          tn.setIntro(s.toString());
        }

        int currRows = TripNote.tripNoteCount(tripModel.tripid);

        tn.setRank(++currRows);


        tn.setType(TripNote.NoteType.CONTENT);
        tn.save();

        //add images if any
        if (item.imageUrl != null && !item.imageUrl.isEmpty() && !bookingSection.equals(BookingButton)) {
          try {
            String fileName = item.imageUrl.substring(item.imageUrl.lastIndexOf("/") + 1);
            if (fileName == null || fileName.isEmpty()) {
              fileName = "img.jpg";

            }
            FileImage fileImage = ImageController.downloadAndSaveImage(item.imageUrl,
                                                                       fileName,
                                                                       FileSrc.FileSrcType.IMG_WCITIES,
                                                                       "",
                                                                       account,
                                                                       false);
            if (fileImage != null) {
              TripNoteAttach tna = TripNoteAttach.build(tn, account.getLegacyId());
              tna.setAttachName(fileName);
              tna.setAttachType(PageAttachType.PHOTO_LINK);
              tna.setImage(fileImage);
              tna.setComments(item.imageAttribution);
              tna.setName(fileName);
              tna.save();
            }
          }
          catch (Exception e) {
            e.printStackTrace();
            Log.err("RecommendationNoteController:cannot download image for note: " + tn.getNoteId() + " url: " + item.imageUrl);

          }
        }
        Ebean.commitTransaction();
        BookingController.invalidateCache(tripModel.tripid, null);
        return APPConstants.PROCESS_WCITIES_NOTE_SUCCESS;
      } catch (Exception e) {
        Ebean.rollbackTransaction();;
        Log.log(LogLevel.ERROR, "autoAddAllWcities: Error adding Wcities guide", e);
        throw  e;
      }
    }
    return APPConstants.PROCESS_WCITIES_NOTE_ERROR;
  }


  private static String buildAddressSection(RecommendedItem item) {
    StringBuilder address = new StringBuilder();
    if (item.address != null) {
      address.append(item.address.streetAddress);


      if (StringUtils.isNotEmpty(item.address.locality)) {
        address.append(", ");
        address.append(item.address.locality);
      }

      if (StringUtils.isNotEmpty(item.address.region)) {
        address.append(", ");
        address.append(item.address.region);
      }

      if (StringUtils.isNotEmpty(item.address.country)) {
        address.append(", ");
        address.append(item.address.country);
      }
    }
    return address.toString();
  }

  private static String buildPriceSection(RecommendedItem item) {
    StringBuilder priceBuilder = new StringBuilder();

    if (item.prices != null && !item.prices.isEmpty()) {
      StringBuilder priceRowBuilder = new StringBuilder();
      for (RecommendedItem.Price p : item.prices) {
        priceRowBuilder.append(String.format(PriceRow, StringUtils.isEmpty(p.description) ? "Price" : p.description, p.value, StringUtils.isEmpty(p.currency) ? "USD" : p.currency));
      }
      priceBuilder.append(String.format(PriceTable, priceRowBuilder.toString()));
    }
    return priceBuilder.toString();
  }

  private static String buildPriceSection1(RecommendedItem item) {
    StringBuilder priceBuilder = new StringBuilder();

    if (item.prices != null && !item.prices.isEmpty()) {
      StringBuilder priceRowBuilder = new StringBuilder();
      for (RecommendedItem.Price p : item.prices) {
        priceBuilder.append(p.value);
        priceBuilder.append(StringUtils.isEmpty(p.currency) ? "USD" : p.currency);
        break;
      }
    }
    return priceBuilder.toString();
  }
}
