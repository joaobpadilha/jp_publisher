package controllers;

import com.google.inject.Inject;
import com.mapped.publisher.persistence.CountriesInfo;
import com.mapped.publisher.utils.Utils;
import com.umapped.api.schema.common.ResponseMessageJson;
import com.umapped.api.schema.local.CountryJson;
import com.umapped.api.schema.types.Operation;
import com.umapped.api.schema.types.ReturnCode;
import models.publisher.Account;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by surge on 2014-11-03.
 */
public class Helpers
    extends Controller {

  @Inject
  FormFactory formFactory;

  /**
   * Returns list of countries support by the system
   */
  public Result countries() {
    ResponseMessageJson jsonResponse = new ResponseMessageJson("0", ReturnCode.SUCCESS);
    CountryJson cj = new CountryJson();
    jsonResponse.body = cj;
    cj.countries = new ArrayList<>();
    cj.operation = Operation.LIST;

    Map<String, CountriesInfo.Country> countries = CountriesInfo.Instance().getAlpha3CountryMap();
    for(String a3 : countries.keySet()) {
      CountriesInfo.Country c = countries.get(a3);
      CountryJson.CountryDetails cd = new CountryJson.CountryDetails();
      cd.alpha3 = c.getAlpha3();
      cd.alpha2 = c.getAlpha2();
      cd.name = c.getName(CountriesInfo.LangCode.EN);
      cj.countries.add(cd);
    }
    return ok(jsonResponse.toJson());
  }


  /**
   * Helper to encode decode base64 values
   */
  public Result base64autoconvert(String value) {
    Long lVal = null;

    //Is account?
    if(value.startsWith("~")) {
      return ok(Account.decodeId(value).toString());
    }

    try {
      lVal = Long.parseLong(value);
    } catch (NumberFormatException nfe){}


    if(lVal != null) {
      return ok(Utils.base64encode(lVal));
    }

    return ok(Utils.base64decode(value).toString());
  }
}
