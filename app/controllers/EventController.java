package controllers;

import com.google.inject.Inject;
import com.mapped.common.CoreConstants;
import com.mapped.persistence.*;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.persistence.TripBrandingMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.utils.comparator.StringIgnoreCase;
import com.mapped.publisher.view.*;
import models.publisher.Company;
import models.publisher.Trip;
import models.publisher.TripPublishHistory;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-28
 * Time: 12:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class EventController
    extends Controller {

  @Inject
  FormFactory formFactory;

  public Result event() {
    final DynamicForm form       = formFactory.form().bindFromRequest();
    String            mapPK      = form.get("mapId");
    String            page       = form.get("pageId");
    String            allPostsIn = form.get("allPosts");

    boolean allPosts = Boolean.parseBoolean(allPostsIn);


    if (mapPK == null || mapPK.length() == 0) {
      EventInfoView view = new EventInfoView();
      view.message = "Invalid link.";
      return ok(views.html.event.eventError.render(view));

    }

    EventInfoView view = getEventView(mapPK);
    if (view == null) {
      view = new EventInfoView();
      view.message = "Invalid link.";
      return ok(views.html.event.eventError.render(view));
    }
    else {
      view.allPosts = allPosts;
      if (view.tagGroup != null && view.tagGroup.contains(page)) {
        view.selectedPage = page;
      }
      if (page != null && view.eventPages != null && view.eventPages.get(page) != null) {
        return ok(views.html.event.eventTagV2.render(view));
      }
      else {
        return ok(views.html.event.eventMainV2.render(view));
      }

    }
  }

  public static EventInfoView getEventView(String mapPK) {
    EventInfoView view = new EventInfoView();

    try {
      Maps map = MapsMgr.getByPk(mapPK);
      if (map == null || map.getStatus() != APPConstants.STATUS_ACTIVE) {
        return null;
      }

      EventInfoView cacheView = (EventInfoView) CacheMgr.get(APPConstants.CACHE_EVENT_VIEW_PREFIX + map.getPk());
      if (cacheView != null) {
        //check to see if there were any updates since last time
        List<MapComments> updates = MapCommentsExtMgr.getSytemUpdatesByMapsIdSinceLastUpdate(map.getPk(),
                                                                                             cacheView.cacheTimestamp);
        if (updates == null || updates.size() == 0) {
          if (map.getMapType() != null && map.getMapType().equals(APPConstants.MAP_TRIP_TYPE)) {
            //get agency and agent info
            //trip id might be different from mapsid due to the group functionality
            String tripId = WebItineraryController.getTripIdFromComposite(mapPK);
            Trip   trip   = Trip.find.byId(tripId);
            if (trip != null) {
              //find info from co-branding
              TripPublishHistory tp    = TripPublishHistory.getLastPublished(trip.tripid, null);
              UserView           agent = null;
              if (tp != null) {
                agent = UserController.getUserView(tp.createdby);

              }
              else {
                agent = UserController.getUserView(trip.createdby);
              }
              if (agent != null) {
                cacheView.agentName = agent.name;
                cacheView.agentEmail = agent.email;
                cacheView.agentPhone = agent.phone;
                cacheView.agentProfilePhoto = agent.profilePhotoUrl;

                if (agent.phone == null || agent.phone.trim().length() == 0) {
                  cacheView.agentPhone = agent.mobile;
                }
              }

              List<TripBrandingView> brandingViews = TripBrandingMgr.findActiveBrandedCmpies(trip);
              CmpyInfoView           cmpyView      = null;
              TripBrandingView overwritten = null;

              if (brandingViews != null && brandingViews.size() > 0) {
                Company c = TripBrandingMgr.getMainContact(brandingViews);
                cmpyView = CompanyController.getCmpyView(c.cmpyid);
                //if this is overwritten by tag for the same company e.g. ski.com
                overwritten = TripBrandingMgr.getOverwrittenByTag(brandingViews, trip);
              }
              else {
                cmpyView = CompanyController.getCmpyView(trip.cmpyid);
              }

              if (overwritten == null) {
                if (cmpyView != null) {
                  cacheView.agencyName = cmpyView.cmpyName;
                  cacheView.agencyLogo = cmpyView.logoUrl;
                  if (cmpyView.address != null) {
                    cacheView.agencyEmail = cmpyView.address.email;
                    cacheView.agencyPhone = cmpyView.address.phone;
                    cacheView.agencyWebsite = cmpyView.address.web;
                    cacheView.agencyFacebook = cmpyView.address.facebook;
                    cacheView.agencyTwitter = cmpyView.address.twitter;
                  }
                }
              } else {
                cacheView.agencyName = overwritten.cmpyName;
                cacheView.agencyLogo = Utils.escapeS3Umapped_Prd(overwritten.cmpyLogoUrl);
                cacheView.agencyEmail = overwritten.cmpyEmail;
                cacheView.agencyPhone = overwritten.cmpyPhone;
                cacheView.agencyWebsite = overwritten.cmpyWebsite;
                cacheView.agentPhone = overwritten.cmpyPhone;
              }
            }
          }
          return cacheView;
        }
      }

      List<Users> owners = new ArrayList<>();
      if (map.getMapType() != null && map.getMapType().equals(APPConstants.MAP_TRIP_TYPE)) {
        List<Users> users = UsersMgrExt.getOwnersByMapsId(mapPK);
        if (users != null) {
          owners.addAll(users);
        }


        //get agency and agent info
        //trip id might be different from mapsid due to the group functionality
        String tripId = WebItineraryController.getTripIdFromComposite(mapPK);
        Trip   trip   = Trip.find.byId(tripId);
        if (trip != null) {
          //find info from co-branding
          TripPublishHistory tp    = TripPublishHistory.getLastPublished(trip.tripid, null);
          UserView           agent = null;
          if (tp != null) {
            agent = UserController.getUserView(tp.createdby);

          }
          else {
            agent = UserController.getUserView(trip.createdby);
          }
          if (agent != null) {
            view.agentName = agent.name;
            view.agentEmail = agent.email;
            view.agentPhone = agent.phone;
            view.agentProfilePhoto = agent.profilePhotoUrl;

            if (agent.phone == null || agent.phone.trim().length() == 0) {
              view.agentPhone = agent.mobile;
            }
          }

          List<TripBrandingView> brandingViews = TripBrandingMgr.findActiveBrandedCmpies(trip);
          CmpyInfoView           cmpyView      = null;
          TripBrandingView overwritten = null;

          if (brandingViews != null && brandingViews.size() > 0) {
            Company c = TripBrandingMgr.getMainContact(brandingViews);
            cmpyView = CompanyController.getCmpyView(c.cmpyid);
            //if this is overwritten by tag for the same company e.g. ski.com
            overwritten = TripBrandingMgr.getOverwrittenByTag(brandingViews, trip);
          }
          else {
            cmpyView = CompanyController.getCmpyView(trip.cmpyid);
          }

          if (overwritten == null) {
            if (cmpyView != null) {
              view.agencyName = cmpyView.cmpyName;
              view.agencyLogo = cmpyView.logoUrl;
              if (cmpyView.address != null) {
                view.agencyEmail = cmpyView.address.email;
                view.agencyPhone = cmpyView.address.phone;
                view.agencyWebsite = cmpyView.address.web;
                view.agencyFacebook = cmpyView.address.facebook;
                view.agencyTwitter = cmpyView.address.twitter;
              }
            }
          } else {
            view.agencyName = overwritten.cmpyName;
            view.agencyLogo = Utils.escapeS3Umapped_Prd(overwritten.cmpyLogoUrl);
            view.agencyEmail = overwritten.cmpyEmail;
            view.agencyPhone = overwritten.cmpyPhone;
            view.agencyWebsite = overwritten.cmpyWebsite;
            view.agentPhone = overwritten.cmpyPhone;
          }


        }

      }
      else {
        Users owner = UsersMgr.getByPk(map.getCreatedBy());
        owners.add(owner);
      }

      //build user view;
      if (owners == null || owners.size() == 0) {
        return null;
      }

      view.owners = new ArrayList<UserView>();
      view.eventPageList = new ArrayList<EventPageView>();
      for (Users u : owners) {
        UserView uv = (UserView) CacheMgr.get(APPConstants.CACHE_USER_VIEW_PREFIX + u.getPk());
        if (uv != null) {
          view.owners.add(uv);
        }
        else {
          uv = new UserView();
          uv.firstName = u.getFName();
          uv.lastName = u.getLName();
          List<File> f = FileMgrExt.getAllActiveByParentId(u.getPk());
          if (f != null && f.size() > 0 && f.get(0).getFilePath() != null) {
            uv.profilePhotoUrl = Utils.authorizeS3Get(f.get(0).getFilePath());
          }
          view.owners.add(uv);
          CacheMgr.set(APPConstants.CACHE_USER_VIEW_PREFIX + u.getPk(), uv, APPConstants.CACHE_EXPIRY_SECS);
        }
      }
      view.name = map.getName();

      view.note = Utils.escapeHtml(map.getNote());
            /*
            if (view.note != null && view.note.length() > 200) {
                    String s = view.note.substring(0, 199);
                    int lastspace = s.lastIndexOf(' ');
                    s = s.substring(0, lastspace);
                    s = s + "...";
                    view.note = Utils.escapeHtml(s);

            } */


      view.id = map.getPk();
      view.isTrip = false;
      if (map.getMapType() != null && map.getMapType().equals(APPConstants.MAP_TRIP_TYPE)) {
        view.isTrip = true;
      }
      try {
        if (map.getDate() != null && map.getDate().length() > 0) {
          view.startDatePrint = Utils.formatDateTimePrint(Long.valueOf(map.getDate()));
        }

        if (map.getEndDate() != null && map.getEndDate().length() > 0) {
          view.endDatePrint = Utils.formatDateTimePrint(Long.valueOf(map.getEndDate()));
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }


      //get cover
      Boolean foundCover = Boolean.FALSE;

      ArrayList<File> mapFiles = FileMgrExt.getAllActiveByParentId(mapPK);
      if (mapFiles != null) {
        for (File f : mapFiles) {
          AttachmentView aView = new AttachmentView();
          aView.id = f.getPk();
          aView.comments = f.getFileNote();
          if (aView.comments != null && aView.comments.length() > 200) {
            String s = aView.comments.substring(0, 199).trim();
            try {
              int lastspace = s.lastIndexOf(' ');
              if (lastspace > 0) {
                s = s.substring(0, lastspace);
                s = s + "...";
                aView.comments = s;
              }
            }
            catch (Exception e) {
              e.printStackTrace();
            }

          }
          aView.tag = f.getFileTag();
          aView.attachName = f.getFilePath();
          aView.attachUrl = (Utils.authorizeS3Get(f.getFilePath()));

          if (f.getStatus() == 0 && f
              .getFileType()
              .equalsIgnoreCase(CoreConstants.IMAGE) && f.getFilePath() != null && f.getFilePath().length() > 0) {
            foundCover = Boolean.TRUE;
            view.cover = aView;
          }
          else if (f.getStatus() == 0 && f.getFileType().equalsIgnoreCase(CoreConstants.AUDIO)) {
            if (view.audio == null) {
              view.audio = new ArrayList<AttachmentView>();
            }
            view.audio.add(aView);
          }
        }
      }

      //get pages
      //get places content
      view.dateGroup = new ArrayList<String>();
      view.tagGroup = new ArrayList<String>();
      view.eventPageCovers = new HashMap<String, String>();
      view.eventPageMap = new HashMap<String, EventPageView>();
      view.photos = new ArrayList<AttachmentView>();
      view.videos = new ArrayList<AttachmentView>();

      view.photoCountByTag = new HashMap<String, Integer>();


      ArrayList<Page> pages = PageMgrExt.getAllActiveByMapsId(mapPK); // PageContentExtMgr.getAllPageContentByMapsId
      // (m.getPk());
      if (pages != null) {
        view.eventPages = new HashMap<String, List<EventPageView>>();
        for (Page p : pages) {
          if (p.getStatus() == APPConstants.STATUS_ACTIVE) {
            if (p.getPageType() == null || (!p
                .getPageType()
                .equals(String.valueOf(APPConstants.PAGE_BOOKING_TYPE)) && !p
                .getPageType()
                .equals(String.valueOf(APPConstants.PAGE_GUIDE_TYPE)))) {
              Milestones m = MilestonesMgr.getByPk(p.getMilestonesId());
              if (m != null && m.getStatus() == 0) {
                EventPageView pageView = new EventPageView();
                view.eventPageList.add(pageView);
                view.eventPageMap.put(p.getPk(), pageView);


                pageView.id = p.getPk();
                pageView.lastUpdatedTimestamp = m.getLastUpdateTimestamp();

                pageView.name = m.getName();
                if (m.getNote() != null && !m.getNote().equalsIgnoreCase(APPConstants.MP_PLACEHOLDER_NOTE)) {
                  pageView.note = Utils.escapeHtml(m.getNote());
                                   /*
                                  if (pageView.note != null && pageView.note.length() > 200) {
                                      String s = pageView.note.substring(0, 199);
                                      int lastspace = s.lastIndexOf(' ');
                                      s = s.substring(0, lastspace);
                                      s = s + "...";
                                      pageView.note = s;
                                      pageView.moreBtn = true;
                                  }
                                  */
                }

                if (p.getPageTag() != null && !p.getPageTag().equalsIgnoreCase(APPConstants.MP_PLACEHOLDER_TAG) && !p
                    .getPageTag()
                    .equalsIgnoreCase(APPConstants.MP_PLACEHOLDER_TAG1)) {
                  pageView.pageTag = p.getPageTag();
                }

                pageView.address = m.getAddress();
                if (pageView.name == null) {
                  pageView.name = "";
                }
                if (pageView.address == null) {
                  pageView.address = "";
                }
                pageView.publisherNote = m.getPublisherNote();
                pageView.tag = m.getTag();
                pageView.locLat = m.getLatitude();
                pageView.locLong = m.getLongitude();
                pageView.startDatePrint = Utils.formatDate(m.getStartDate());
                pageView.endDatePrint = Utils.formatDate(m.getEndDate());
                pageView.startTimePrint = Utils.getTimeString(m.getStartDate());
                pageView.endTimePrint = Utils.getTimeString(m.getEndDate());
                if (m.getStartDate() != null && m.getStartDate().trim().length() > 0) {
                  try {
                    Long l = Long.valueOf(m.getStartDate());
                    if (l != null) {
                      pageView.startTimestamp = l;
                    }
                  }
                  catch (NumberFormatException ne) {
                    ne.printStackTrace();
                  }
                }


                UserView uv = (UserView) CacheMgr.get(APPConstants.CACHE_USER_VIEW_PREFIX + m.getCreatedBy());
                if (uv == null) {
                  Users u = UsersMgr.getByPk(p.getCreatedBy());
                  if (u != null) {
                    uv = new UserView();
                    uv.firstName = u.getFName();
                    uv.lastName = u.getLName();
                    List<File> f = FileMgrExt.getAllActiveByParentId(u.getPk());
                    if (f != null && f.size() > 0 && f.get(0).getFilePath() != null) {
                      uv.profilePhotoUrl = Utils.authorizeS3Get(f.get(0).getFilePath());
                    }
                    CacheMgr.set(APPConstants.CACHE_USER_VIEW_PREFIX + u.getPk(), uv, APPConstants.CACHE_EXPIRY_SECS);
                  }
                }

                if (uv != null) {
                  String name = "";
                  if (uv.firstName != null) {
                    name = uv.firstName;
                  }

                  if (uv.lastName != null && uv.lastName.length() > 0) {
                    name = name + " " + uv.lastName.charAt(0) + ".";
                  }
                  pageView.userName = name;
                  pageView.userProfileUrl = uv.profilePhotoUrl;
                }

                if (m.getLongitude() != null && m.getLatitude() != null && m.getLongitude().length() > 3 && m
                                                                                                                .getLatitude()
                                                                                                                .length() > 3) {
                  view.displayMap = true;
                }

                int photoCount = 0;
                //get attachments
                ArrayList<File> pageFiles = FileMgrExt.getAllActiveByParentId(m.getPk());
                for (File f : pageFiles) {
                  if (f.getStatus() == APPConstants.STATUS_ACTIVE) {
                    AttachmentView aView = new AttachmentView();
                    aView.id = f.getPk();
                    aView.tag = f.getFileTag();
                    aView.attachName = f.getFilePath();
                    aView.attachUrl = (Utils.authorizeS3Get(f.getFilePath()));

                    uv = (UserView) CacheMgr.get(APPConstants.CACHE_USER_VIEW_PREFIX + m.getCreatedBy());
                    if (uv == null) {
                      Users u = UsersMgr.getByPk(p.getCreatedBy());
                      if (u != null) {
                        uv = new UserView();
                        uv.firstName = u.getFName();
                        uv.lastName = u.getLName();
                        List<File> f1 = FileMgrExt.getAllActiveByParentId(u.getPk());
                        if (f1 != null && f1.size() > 0 && f1.get(0).getFilePath() != null) {
                          uv.profilePhotoUrl = Utils.authorizeS3Get(f1.get(0).getFilePath());
                        }
                        CacheMgr.set(APPConstants.CACHE_USER_VIEW_PREFIX + u.getPk(),
                                     uv,
                                     APPConstants.CACHE_EXPIRY_SECS);
                      }
                    }

                    if (uv != null) {
                      String name = "";
                      if (uv.firstName != null) {
                        name = uv.firstName;
                      }

                      if (uv.lastName != null && uv.lastName.length() > 0) {
                        name = name + " " + uv.lastName.charAt(0) + ".";
                      }

                      aView.comments = name;
                    }

                    if (f.getFileNote() != null && f.getFileNote().trim().length() > 0) {
                      if (aView.comments != null) {
                        aView.comments += " - " + f.getFileNote();
                      }
                      else {
                        aView.comments = f.getFileNote();
                      }

                    }


                    if (f.getFileType().equalsIgnoreCase(CoreConstants.IMAGE)) {
                      if (pageView.photos == null) {
                        pageView.photos = new ArrayList<AttachmentView>();
                      }
                      pageView.photos.add(aView);
                      if (!foundCover) {
                        view.cover = aView;
                        foundCover = true;
                      }
                      view.photos.add(aView);
                      photoCount++;

                    }
                    else if (f.getFileType().equalsIgnoreCase(CoreConstants.VIDEO)) {
                      if (pageView.videos == null) {
                        pageView.videos = new ArrayList<AttachmentView>();
                      }
                      pageView.videos.add(aView);
                      view.videos.add(aView);

                    }
                    else if (f.getFileType().equalsIgnoreCase(CoreConstants.AUDIO)) {
                      if (pageView.audio == null) {
                        pageView.audio = new ArrayList<AttachmentView>();
                      }
                      pageView.audio.add(aView);
                    }
                  }

                }

                if (p.getPageTag() != null && p.getPageTag().length() > 0 && !p
                    .getPageTag()
                    .equalsIgnoreCase(APPConstants.MP_PLACEHOLDER_TAG)) {
                  List<EventPageView> list = view.eventPages.get(p.getPageTag());
                  if (list == null) {
                    list = new ArrayList<EventPageView>();
                  }
                  list.add(pageView);
                  view.eventPages.put(p.getPageTag(), list);
                  if (!view.tagGroup.contains(p.getPageTag())) {
                    view.tagGroup.add(p.getPageTag());
                  }

                  if (view.eventPageCovers.get(p.getPageTag()) == null) {
                    if (pageView.photos != null && pageView.photos.size() > 0) {
                      view.eventPageCovers.put(p.getPageTag(), pageView.photos.get(0).attachUrl);
                    }
                  }
                  //update photocount
                  if (view.photoCountByTag.containsKey(p.getPageTag())) {
                    int i = view.photoCountByTag.get(p.getPageTag());
                    photoCount = photoCount + i;

                  }
                  view.photoCountByTag.put(p.getPageTag(), photoCount);
                }
                else {
                  List<EventPageView> list = view.eventPages.get(APPConstants.MP_UNTAGGED_PAGE);
                  if (list == null) {
                    list = new ArrayList<EventPageView>();
                  }
                  list.add(pageView);
                  view.eventPages.put(APPConstants.MP_UNTAGGED_PAGE, list);

                  if (view.eventPageCovers.get(APPConstants.MP_UNTAGGED_PAGE) == null) {
                    if (pageView.photos != null && pageView.photos.size() > 0) {
                      view.eventPageCovers.put(APPConstants.MP_UNTAGGED_PAGE, pageView.photos.get(0).attachUrl);
                    }
                  }

                  //update photocount
                  if (view.photoCountByTag.containsKey(APPConstants.MP_UNTAGGED_PAGE)) {
                    int i = view.photoCountByTag.get(APPConstants.MP_UNTAGGED_PAGE);
                    photoCount = photoCount + i;

                  }
                  view.photoCountByTag.put(APPConstants.MP_UNTAGGED_PAGE, photoCount);
                  if (!view.tagGroup.contains(APPConstants.MP_UNTAGGED_PAGE)) {
                    view.tagGroup.add(APPConstants.MP_UNTAGGED_PAGE);
                  }
                }
              }
            }
          }

        }
      }
      Collections.sort(view.tagGroup, new StringIgnoreCase());
      Collections.sort(view.eventPageList, EventPageView.EventPageViewComparator);

      ArrayList<EventPageView> temp = new ArrayList<EventPageView>();
      for (EventPageView e : view.eventPageList) {
        if (e.pageTag == null || e.pageTag.trim().length() == 0) {
          temp.add(e);
        }
      }

      for (EventPageView e : temp) {
        view.eventPageList.remove(e);
        view.eventPageList.add(e);
      }

      for (String k : view.eventPages.keySet()) {
        List<EventPageView> p = view.eventPages.get(k);
        Collections.sort(p, EventPageView.EventPageViewComparatorByDate);
        view.eventPages.put(k, p);


      }


      //resort by start date
      ArrayList<EventPageView> eventCopy = new ArrayList<EventPageView>();
      eventCopy.addAll(view.eventPageList);
      Collections.sort(eventCopy, EventPageView.EventPageViewDateComparator);
      ArrayList<String> temp1 = new ArrayList<String>();
      for (EventPageView e : eventCopy) {
        if (e.startTimestamp == null) {
          temp1.add(e.id);
        }
        else {
          view.dateGroup.add(e.id);
        }
      }
      view.dateGroup.addAll(temp1);


      view.cacheTimestamp = System.currentTimeMillis();
      CacheMgr.set(APPConstants.CACHE_EVENT_VIEW_PREFIX + mapPK, view, APPConstants.CACHE_TRANSIENT_EXPIRY_SECS);

    }
    catch (Exception e) {
      e.printStackTrace();
      Log.log(LogLevel.ERROR, "event error: " + mapPK, e);
      return null;
    }

    return view;

  }

  public Result eventMap() {
    final DynamicForm form   = formFactory.form().bindFromRequest();
    String            mapPK  = form.get("mapId");
    String            page   = form.get("pageId");
    String            pagePK = form.get("pagePk");


    if (mapPK == null || mapPK.length() == 0) {
      EventInfoView view = new EventInfoView();
      view.message = "Invalid link.";
      return ok(views.html.event.eventError.render(view));

    }

    EventInfoView view = getEventView(mapPK);
    if (view == null) {
      view = new EventInfoView();
      view.message = "Invalid link.";
      return ok(views.html.event.eventError.render(view));
    }
    else {
      if (view.tagGroup != null && view.tagGroup.contains(page)) {
        view.selectedPage = page;
      }
      if (view.eventPageMap != null && view.eventPageMap.containsKey(pagePK)) {
        view.selectedPagePk = pagePK;
      }
      return ok(views.html.event.eventMapV2.render(view));


    }
  }

  public Result eventWrongUrl() {
    /* hack for an error on the mobile... the url is mangled and coming in like this
http://localhost:9000/event/mapId?=47f5f45e806cfd3c72148568884ddffdad15f0c8cc7673fbb6da9ddca5ea634a

     */
    String mapPK = null;
    String s     = request().toString();

    if (s.contains("mapId?=")) {
      mapPK = s.substring(s.indexOf("mapId?=") + 7).trim();
      if (mapPK != null && mapPK.length() > 0) {
        return redirect("/event/?mapId=" + mapPK);
      }
    }

    BaseView view = new BaseView();
    return ok(views.html.error.error404.render(view));


  }


}
