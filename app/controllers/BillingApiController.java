package controllers;

import com.avaje.ebean.annotation.EnumValue;
import com.braintreegateway.*;
import com.braintreegateway.exceptions.NotFoundException;
import com.fasterxml.jackson.databind.JsonNode;
import com.freshbooks.model.Client;
import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.*;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.common.Credentials;
import com.mapped.publisher.common.SessionConstants;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.js.BaseJsonResponse;
import com.mapped.publisher.parse.schemaorg.DateTime;
import com.mapped.publisher.persistence.CountriesInfo;
import com.mapped.publisher.persistence.RecordStatus;
import com.mapped.publisher.persistence.billing.BraintreeSunbscription;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.CmpyInfoView;
import com.mapped.publisher.view.billing.BraintreeInvoiceView;
import com.umapped.api.billing.*;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.helper.BraintreeGatewayFactory;
import com.umapped.persistence.accountprop.AccountBilling;
import com.umapped.persistence.accountprop.BraintreeBillingInfo;
import io.jsonwebtoken.Claims;
import it.innove.play.pdf.PdfGenerator;
import models.publisher.*;
import models.publisher.BillingPlan;
import org.apache.commons.lang.time.StopWatch;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;
import scala.reflect.macros.Typers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by twong on 17/10/16.
 */
public class BillingApiController
    extends Controller {

  @Inject PdfGenerator pdfGenerator;

  @Inject BraintreeGatewayFactory braintreeFactory;

  //TransactionStatus
  public enum TxnStatus {
    @EnumValue("OK")
    SUCCESS,
    @EnumValue("NB")
    NO_CARD,
    @EnumValue("CE")
    CARD_EXPIRED,
    @EnumValue("CD")
    CARD_DECLINED,
    @EnumValue("VE")
    VALIDATIONN_ERROR
  }

  //TransactionStatus
  public enum PymtStatus {
    @EnumValue("OK")
    SUCCESS,
    @EnumValue("IC")
    INVALID_CARD,
    @EnumValue("CE")
    CARD_EXPIRED,
    @EnumValue("CD")
    CARD_DECLINED,
    @EnumValue("ER")
    VALIDATIONN_ERROR
  }

  //@With({AuthenticatedAccountJwtAuth.class})
  @With(ValidSession.class)
  public Result info() {
    //make sure there is a valid session - we cannot use the authenticated annotation because this is sometimes
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Account userAccount = sessionMgr.getCredentials().getAccount();

    AccountProp billingProperties = AccountProp.findByPk(userAccount.getUid(), AccountProp.PropertyType.BILLING_INFO);
    BraintreeBillingInfo billingInfo = null;
    BraintreeGateway braintreeGateway = braintreeFactory.getInstance();

    if (billingProperties != null && billingProperties.billingProperties != null && billingProperties.billingProperties.braintree != null) {
      billingInfo = billingProperties.billingProperties.braintree;
    }

    BillingInfo response = new BillingInfo();
    response.bilAddr = new BillingAddress();
    response.bilAddr.fName = userAccount.getFirstName();
    response.bilAddr.lName = userAccount.getLastName();
    response.billingStatus = sessionMgr.getCredentials().getBillingStatus();
    response.custId = userAccount.getUid().toString();

    //set the braintree customer info if any
    if (billingInfo != null && billingInfo.custId != null && !billingInfo.custId.isEmpty()) {
      ClientTokenRequest request = new ClientTokenRequest().customerId(billingInfo.custId);
      response.btreeToken = braintreeGateway.clientToken().generate(request);
      if (billingInfo.pymtId != null && !billingInfo.pymtId.isEmpty()) {
        response.hasPymt = true;
      }
      if (billingInfo.address != null) {
        response.bilAddr.fName = billingInfo.address.fName;
        response.bilAddr.lName = billingInfo.address.lName;
        response.bilAddr.streetAddress = billingInfo.address.streetAddress;
        response.bilAddr.extendedAddress = billingInfo.address.extendedAddress;
        response.bilAddr.region = billingInfo.address.region;
        response.bilAddr.locality = billingInfo.address.locality;
        response.bilAddr.postalCode = billingInfo.address.postalCode;
        response.bilAddr.country = billingInfo.address.country;
        response.bilAddr.countryCodeAlpha2 = billingInfo.address.countryCodeAlpha2;
        response.bilAddr.company = billingInfo.address.company;
      }
      if (billingInfo.ccInfo != null) {
        billingInfo.ccInfo.cardNumber = Utils.maskCC(billingInfo.ccInfo.cardNumber);
        response.ccInfo = billingInfo.ccInfo;
      }
    } else {
      //initialize braintree with no customer
      response.btreeToken = braintreeGateway.clientToken().generate();

    }

    //get all plans
    BillingSettingsForUser bsu = BillingSettingsForUser.findSettingsByUserId(userAccount.getLegacyId());
    if (bsu == null) {
      BaseView view = new BaseView();
      view.message = "No billing plan set up - please contact support@umapped.com";
      return ok(views.html.common.message.render(view));
    }
    BillingPlan bp = bsu.getPlan();
    if (bsu.getBillTo().getName().ordinal() == BillingEntity.Type.AGENT.ordinal()) {
      BillingSettingsForUser bsu1 = BillingSettingsForUser.findSettingsByUserId(bsu.getAgent());
      bp = bsu1.getPlan();
    } else if (bsu.getBillTo().getName().ordinal() == BillingEntity.Type.COMPANY.ordinal()) {
      BillingSettingsForCmpy bsc = BillingSettingsForCmpy.find.byId(bsu.getCmpyid());
      bp = bsc.getPlan();
    }

    if (bp != null) {
      response.myBilPlan = new com.umapped.api.billing.BillingPlan();
      response.myBilPlan.cur = bp.getCurrency().name();
      response.myBilPlan.description = bp.getDescription();
      response.myBilPlan.id = bp.getPlanId().toString();
      response.myBilPlan.name = bp.getName();
      response.myBilPlan.price = bp.getCost().toString();
      response.myBilPlan.agent = new BillingAgent();
      response.myBilPlan.agent.fName = userAccount.getFirstName();
      response.myBilPlan.agent.lName = userAccount.getLastName();
      response.myBilPlan.agent.uid = userAccount.getUid().toString();
      response.myBilPlan.agent.legacyUserid = userAccount.getLegacyId();

      List<BillingAddonsForUser> addons = BillingAddonsForUser.getAddonsForUser(userAccount.getLegacyId());
      response.myBilPlan.addons = new ArrayList<>();
      if (addons != null && addons.size() > 0) {
        for (BillingAddonsForUser myAddon: addons) {
          BillingAddon bao = new BillingAddon();
          BillingPlan abp = BillingPlan.find.byId(myAddon.getPk().getPlanId());
          bao.cur = abp.getCurrency().name();
          bao.description = abp.getDescription();
          bao.enabled = myAddon.getEnabled();
          bao.id = abp.getPlanId().toString();
          bao.name = abp.getName();
          bao.price = abp.getCost().toString();
          if (myAddon.getStartTs() != null && myAddon.getStartTs() > 0) {
            bao.startDate = Utils.formatDatePrint(myAddon.getStartTs());
          }
          if (myAddon.getCutoffTs() != null && myAddon.getCutoffTs() > 0) {
            bao.cutoffDate = Utils.formatDatePrint(myAddon.getCutoffTs());
          }
          response.myBilPlan.addons.add(bao);
        }
      }

      //Availalbe addons not yet added to the user
      for(BillingPlan p: BillingPlan.newAddonsForUser(userAccount.getLegacyId(), bp.getPlanId(), true)){
        if(p.getSchedule() == bp.getSchedule()) {
          BillingAddon bao = new BillingAddon();
          bao.cur = p.getCurrency().name();
          bao.description = p.getDescription();
          bao.enabled = false;
          bao.id = p.getPlanId().toString();
          bao.name = p.getName();
          bao.price = p.getCost().toString();
          response.myBilPlan.addons.add(bao);
        }
      }
    }

    //get any users that are billing to you
    if (bsu.getBillTo().getName().ordinal() == BillingEntity.Type.USER.ordinal()) {
      List<BillingSettingsForUser> billedUsers = BillingSettingsForUser.getMinions(userAccount.getLegacyId());
      if (billedUsers != null && billedUsers.size() > 0) {
        response.billedToMe = new ArrayList<>();
        for (BillingSettingsForUser bsu1: billedUsers) {
          com.umapped.api.billing.BillingPlan agentBilPlan = new com.umapped.api.billing.BillingPlan();
          Account agentAccount = Account.findActiveByLegacyId(bsu1.getUserId());
          agentBilPlan.cur = bp.getCurrency().name();
          agentBilPlan.description = bp.getDescription();
          agentBilPlan.id = bp.getPlanId().toString();
          agentBilPlan.name = bp.getName();
          agentBilPlan.price = bp.getCost().toString();
          agentBilPlan.agent = new BillingAgent();
          agentBilPlan.agent.fName = agentAccount.getFirstName();
          agentBilPlan.agent.lName = agentAccount.getLastName();
          agentBilPlan.agent.uid = agentAccount.getUid().toString();
          agentBilPlan.agent.legacyUserid = agentAccount.getLegacyId();

          List<BillingAddonsForUser> addons = BillingAddonsForUser.getAddonsForUser(agentAccount.getLegacyId());
          agentBilPlan.addons = new ArrayList<>();
          if (addons != null && addons.size() > 0) {
            for (BillingAddonsForUser myAddon: addons) {
              BillingAddon bao = new BillingAddon();
              BillingPlan abp = BillingPlan.find.byId(myAddon.getPk().getPlanId());
              bao.cur = abp.getCurrency().name();
              bao.description = abp.getDescription();
              bao.enabled = myAddon.getEnabled();
              bao.id = abp.getPlanId().toString();
              bao.name = abp.getName();
              bao.price = abp.getCost().toString();
              if (myAddon.getStartTs() != null && myAddon.getStartTs() > 0) {
                bao.startDate = Utils.formatDatePrint(myAddon.getStartTs());
              }
              if (myAddon.getCutoffTs() != null && myAddon.getCutoffTs() > 0) {
                bao.cutoffDate = Utils.formatDatePrint(myAddon.getCutoffTs());
              }
              agentBilPlan.addons.add(bao);
            }
          }

          //Availalbe addons not yet added to the user
          for(BillingPlan p: BillingPlan.newAddonsForUser(agentAccount.getLegacyId(), bp.getPlanId(), true)){
            if(p.getSchedule() == bp.getSchedule()) {
              BillingAddon bao = new BillingAddon();
              bao.cur = p.getCurrency().name();
              bao.description = p.getDescription();
              bao.enabled = false;
              bao.id = p.getPlanId().toString();
              bao.name = p.getName();
              bao.price = p.getCost().toString();
              agentBilPlan.addons.add(bao);
            }
          }

          response.billedToMe.add(agentBilPlan);
        }
      }

    }

    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg == null || msg.isEmpty()) {
      msg = sessionMgr.getParam(SessionConstants.SESSION_PARAM_MSG);
    }
    if (msg != null) {
      response.message = msg;
      sessionMgr.setParam(SessionConstants.SESSION_PARAM_MSG, "");
    }

    return ok(views.html.user.billingInfoPlans.render(response));
  }

   /*
    API call to add/update a billing address:
    - creates a braintree customer if there is none
    - creates/updates a billing address for this customer
   */

  @BodyParser.Of(BodyParser.Json.class)
  @With(ValidSession.class)
  public Result address() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Account userAccount = sessionMgr.getCredentials().getAccount();
    try {
      BillingAddress address = null;
      JsonNode node = request().body().asJson();
      address = Json.fromJson(request().body().asJson(), BillingAddress.class);

      if (userAccount != null && userAccount.getState() == RecordStatus.ACTIVE && userAccount.getAccountType() ==
                                                                                  Account.AccountType.PUBLISHER && address !=null) {
        if (address.countryCodeAlpha2 == null && address.country != null && address.country.length() == 2) {
          address.countryCodeAlpha2 = address.country;
          address.country = CountriesInfo.Instance().byAlpha2(address.countryCodeAlpha2).getName(CountriesInfo.LangCode.EN);
        }

        processBraintreeCustomer(userAccount, address);

        return ok();

      }
    }
    catch (Exception e) {
      Log.err("BillingAPIController:address - exception U: " + userAccount.getLegacyId(), e);
      return internalServerError();
    }

    return unauthorized();
  }

  /*
    API call to add a new CC:
    - removes any existing payment methods for the customer
    - creates a new payment method for this customer
    - updates all subscriptions with this new payment method
   */
  @With(ValidSession.class)
  public Result paymentMethod(String token) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Account userAccount = sessionMgr.getCredentials().getAccount();
    try {
      Long txnId = DBConnectionMgr.getUniqueLongId();

      if (userAccount != null && userAccount.getState() == RecordStatus.ACTIVE && userAccount.getAccountType() ==
                                                                                  Account.AccountType.PUBLISHER &&
          token != null && !token
          .isEmpty()) {

        AccountProp accountProp = AccountProp.findByPk(userAccount.getUid(),
                                                       AccountProp.PropertyType.BILLING_INFO);

        PymtStatus pymtStatus = addPaymentMehtod(userAccount, accountProp, token, txnId);
        if (pymtStatus == PymtStatus.SUCCESS) {
          //now that we have a payment, we should go an charge any subscription or addons
          // go through all the subscriptions and add-ons for this user to bill to this payment
          //make sure we check to set the correct billing date for the payment
          BillingSettingsForUser settingsForUser =  BillingSettingsForUser.findSettingsByUserId(userAccount.getLegacyId());
          if (settingsForUser == null) {
            return BaseJsonResponse.codeResponse(ReturnCode.DB_BILLING_PLAN_NOT_FOUND, "Billing Plan not found");
          }

          processAllSubscriptions(userAccount, accountProp.billingProperties, settingsForUser, userAccount, txnId);

          sessionMgr.setParam(SessionConstants.SESSION_PARAM_MSG, "Credit card information updated successfully");
          if (sessionMgr.getCredentials().getBillingStatus() == Credentials.BillingStatus.CREDIT_CARD_SETUP_NEEDED
              || sessionMgr.getCredentials().getBillingStatus() == Credentials.BillingStatus.CREDIT_CARD_UPDATE_NEEDED) {
            //update the session so the user can continue now
            sessionMgr.getCredentials().setBillingStatus(Credentials.BillingStatus.CREDIT_CARD_OK).saveCache();
            //redirect to home
            return ok("/home");
          }
          return ok();
        } else {
          if (pymtStatus == PymtStatus.INVALID_CARD) {
            return internalServerError("We currently only accept Visa and Mastercard. Please try again with a different card.");
          } else {
            return internalServerError(
                "There was a problem processing your credit card; please double check your payment information and try again. Please note only Visa and Mastercard are currently accepted.");
          }
        }
      }
    }
    catch (Exception e) {
      Log.err("Braintree:paymentMethod -  Exception: " + userAccount.getLegacyId(), e);
    }

    return unauthorized("There was a problem processing your credit card; please double check your payment information and try again.");
  }



  /*
   Umapped admin to update subscription when plan is changed. It will cancel any plan before and subscribe to the new plan
   */

  @With({Authenticated.class, AdminAccess.class})
  public Result updateCCBilling(String userId, String cmpyId) {
    BaseView baseView = new BaseView();
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Account account = Account.findActiveByLegacyId(userId);
    Account agentAccount = account;
    if (account != null) {
      BillingSettingsForUser userSetting = BillingSettingsForUser.findSettingsByUserId(userId);
      if (userSetting == null) {
        baseView.message = "Billing not configured properly for user ";
        return ok(views.html.common.message.render(baseView));
      }
      if (userSetting.getBillTo().getName().ordinal() == BillingEntity.Type.COMPANY.ordinal()) {
        baseView.message = "A plan billed to company is not eligible " ;
        return ok(views.html.common.message.render(baseView));
      }
      BillingPlan plan = userSetting.getPlan();
      //this is going to be charged to the billing agent
      if (userSetting.getAgent() != null && !userSetting.getAgent().isEmpty()&& userSetting.getBillTo().getName().ordinal() == BillingEntity.Type.AGENT.ordinal()) {
        agentAccount = Account.findActiveByLegacyId(userSetting.getAgent());
        plan = BillingSettingsForUser.findSettingsByUserId(agentAccount.getLegacyId()).getPlan();
      }
      if (plan == null || !plan.getEnabled() || plan.getBillingType() == BillingPlan.BillType.INV) {
        baseView.message = "The current plan is not eligible for Credit card billing - " + plan.getName() + " - " + plan.getPlanId();
        return ok(views.html.common.message.render(baseView));
      }

      AccountProp billingSettings = AccountProp.findByPk(agentAccount.getUid(), AccountProp.PropertyType.BILLING_INFO);


      if (billingSettings == null || billingSettings.billingProperties == null || billingSettings.billingProperties.braintree == null
          || !billingSettings.billingProperties.braintree.hasPayment()) {
        baseView.message = "No Credit Card Payment on file for " + account.getLegacyId();
        return ok(views.html.common.message.render(baseView));
      }

      BraintreeSunbscription currentSub = userSetting.getBraintree();
      if (currentSub == null || !currentSub.isActive()  || !(currentSub.planId.equals(plan.getPlanId().toString()) && currentSub.custToken.equals(agentAccount.getUid().toString()))) {
        //get current subscription
        //cancel current subscription
        //create new subscription
        try {

          if (Instant.now().isAfter(Instant.ofEpochMilli(userSetting.getStartTs()))) {
            baseView.message = "Billing start date for the plan needs to be greater than today";
            return ok(views.html.common.message.render(baseView));
          }

          changeSubscription(billingSettings.billingProperties, plan, userSetting, sessionMgr.getCredentials().getAccount(), account, agentAccount);

          flash(SessionConstants.SESSION_PARAM_MSG, "Plan subscription successfully Updated");
          return redirect(routes.UserController.user(userId, cmpyId, true));

        } catch (Exception e) {
          baseView.message = "Error updating subscription: " + e.getMessage();
          return ok(views.html.common.message.render(baseView));
        }

      } else {
        baseView.message = "Cannot update credit card subscription - the is no plan change: " + userId;
        return ok(views.html.common.message.render(baseView));
      }

    }
    baseView.message = "Cannot update credit card subscription: " + userId;
    return ok(views.html.common.message.render(baseView));

  }


    /*
   Umapped admin to cancel a subscription
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result cancelCCBilling(String userId, String cmpyId) {
    BaseView baseView = new BaseView();
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Account account = Account.findActiveByLegacyId(userId);
    Account agentAccount = account;
    if (account != null) {
      BillingSettingsForUser userSetting = BillingSettingsForUser.findSettingsByUserId(userId);
      if (userSetting == null) {
        baseView.message = "Error - Billing not configured properly ";
        return ok(views.html.common.message.render(baseView));
      }
      BillingPlan plan = userSetting.getPlan();
      //this is going to be charged to the billing agent
      if (plan == null && userSetting.getAgent() != null && !userSetting.getAgent().isEmpty()&& userSetting.getBillTo().getName().ordinal() == BillingEntity.Type.AGENT.ordinal()) {
        agentAccount = Account.findActiveByLegacyId(userSetting.getAgent());
      }

      BraintreeSunbscription currentSub = userSetting.getBraintree();
      if (currentSub != null && currentSub.subToken != null) {
        //get current subscription
        //cancel current subscription
        //create new subscription
        try {
          BraintreeGateway braintreeGateway = braintreeFactory.getInstance();
          Long txnId = DBConnectionMgr.getUniqueLongId();
          cancelSingleSubscription(userSetting,braintreeGateway, sessionMgr.getCredentials().getAccount(), account, agentAccount, txnId);
          flash(SessionConstants.SESSION_PARAM_MSG, "Subscription cancelled successfully");
          return redirect(routes.UserController.user(userId, cmpyId, true));
        } catch (Exception e) {
          baseView.message = "Error cancelling subscription: " + e.getMessage();
          return ok(views.html.common.message.render(baseView));
        }

      } else {
        baseView.message = "Cannot cancel CC user - no active subscription found: " + userId;
        return ok(views.html.common.message.render(baseView));
      }

    }
    baseView.message = "Cannot cancel CC user: " + userId;
    return ok(views.html.common.message.render(baseView));

  }

  /*
  Umapped admin to sync a umapped user with braintree
  */
  @With({Authenticated.class, AdminAccess.class})
  public Result syncCCUser(String userId, String cmpyId) {

    Account agent = Account.findActiveByLegacyId(userId);
    BaseView baseView = new BaseView();
     SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    if (agent != null) {
      BraintreeGateway gateway = braintreeFactory.getInstance();

      try {
        Customer custResult = gateway.customer().find(agent.getUid().toString());
        if (custResult != null) {
          AccountProp accountProp = AccountProp.findByPk(agent.getUid(), AccountProp.PropertyType.BILLING_INFO);
          BraintreeBillingInfo bsu =  null;
          if (accountProp != null && accountProp.billingProperties != null && accountProp.billingProperties.braintree != null) {
            bsu = accountProp.billingProperties.braintree;
          } else {
            bsu = new BraintreeBillingInfo();
          }
          if (bsu.address == null) {
            bsu.address = new BillingAddress();
          }

          bsu.custId = custResult.getId();

          if (custResult.getDefaultPaymentMethod() != null) {
            PaymentMethod paymentMethod = gateway.paymentMethod().find(custResult.getDefaultPaymentMethod().getToken());
            if (paymentMethod != null && CreditCard.class.isInstance(paymentMethod)) {
              CreditCard creditCard = (CreditCard) paymentMethod;
              bsu.pymtId = paymentMethod.getToken();
              if (bsu.ccInfo == null) {
                bsu.ccInfo = new BillingCCInfo();
              }
              bsu.ccInfo.cardNumber = Utils.maskCC(creditCard.getMaskedNumber());
              bsu.ccInfo.expiryMM = creditCard.getExpirationMonth();
              bsu.ccInfo.expiryYY = creditCard.getExpirationYear();
              bsu.ccInfo.cardType = creditCard.getCardType();
              bsu.ccInfo.imgUrl = creditCard.getImageUrl();
              bsu.ccUpdateNeeded = false;

              if (creditCard.getBillingAddress() != null) {
                if (bsu.address == null) {
                  bsu.address = new BillingAddress();
                }
                bsu.address.company = creditCard.getBillingAddress().getCompany();
                bsu.address.fName = creditCard.getBillingAddress().getFirstName();
                bsu.address.lName = creditCard.getBillingAddress().getLastName();
                bsu.address.streetAddress = creditCard.getBillingAddress().getStreetAddress();
                bsu.address.extendedAddress = creditCard.getBillingAddress().getExtendedAddress() ;
                bsu.address.locality = creditCard.getBillingAddress().getLocality() ;
                bsu.address.postalCode = creditCard.getBillingAddress().getPostalCode() ;
                bsu.address.region = creditCard.getBillingAddress().getRegion() ;
                bsu.address.countryCodeAlpha2 = creditCard.getBillingAddress().getCountryCodeAlpha2() ;
                bsu.address.country = creditCard.getBillingAddress().getCountryName();
                bsu.addrId = creditCard.getBillingAddress().getId();
              }
            }
            if (accountProp == null) {
              accountProp = AccountProp.build(agent.getUid(), AccountProp.PropertyType.BILLING_INFO, sessionMgr.getCredentials().getAccount().getUid());
            }
            accountProp.billingProperties.braintree = bsu;
            accountProp.setModifiedBy(sessionMgr.getCredentials().getAccount().getUid());
            accountProp.setModifiedTs(Timestamp.from(Instant.now()));
            accountProp.prePersist();

            accountProp.save();

            BillingCCAudit audit = BillingCCAudit.buildBraintree(sessionMgr.getCredentials().getAccount().getUid(), DBConnectionMgr.getUniqueLongId())
                                                 .setStatus(BillingCCAudit.Status.OK)
                                                 .setBilledUid(agent.getUid())
                                                 .setInvoiceUid(agent.getUid())
                                                 .setMeta(bsu)
                                                 .setTxnType(BillingCCAudit.TxnType.SYNC_CUSTOMER);
            audit.save();

            flash(SessionConstants.SESSION_PARAM_MSG, "Customer sync successfully with Braintree");
            return redirect(routes.UserController.user(userId, cmpyId, true));
          }
        }
      } catch (NotFoundException nfe) {
        baseView.message = "Customer does not exist in Braintree " + userId;
        return ok(views.html.common.message.render(baseView));
      }

    }
    baseView.message = "Error syncing with Braintree" + userId;
    return ok(views.html.common.message.render(baseView));
  }

  @With(ValidSession.class)
  public Result printInvoice (Long userid) {
    BaseView baseView = new BaseView();
    baseView.message = "Unable to generate invoice - please contact Umapped support";

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    final DynamicForm form = Form.form().bindFromRequest();
    String yearStr = form.get("invyear");
    String monthStr = form.get("invmonth");
    int year = 0;
    int month = 0;
    if (yearStr != null && monthStr != null) {
      year = Integer.parseInt(yearStr);
      month = Integer.parseInt(monthStr);
    } else {
      return ok(views.html.error.error.render(baseView));
    }

    AccountProp accountProp = null;
    Account agent = null;
    if (sessionMgr.getCredentials().isUmappedAdmin()) {
      agent = Account.find.byId(userid);
      if (agent != null) {
        accountProp = AccountProp.findByPk(agent.getUid(), AccountProp.PropertyType.BILLING_INFO);
      }
    } else {
      if (sessionMgr.getAccount().isPresent()) {
        agent = sessionMgr.getAccount().get();
      }
      accountProp = AccountProp.findByPk(sessionMgr.getCredentials().getAccount().getUid(),
                                         AccountProp.PropertyType.BILLING_INFO);
    }
    if (agent != null && accountProp != null && accountProp.billingProperties != null && accountProp.billingProperties.braintree != null && accountProp.billingProperties.braintree.hasPayment()) {
      BraintreeGateway gateway  = braintreeFactory.getInstance();
      BraintreeBillingInfo billingInfo = accountProp.billingProperties.braintree;

      Calendar startCal = Calendar.getInstance();
      startCal.set(year, month - 1, 1, 0, 0, 0);



      Calendar endCal = Calendar.getInstance();
      endCal.set(year, month-1, 1, 00, 00, 0);
      endCal.add(Calendar.MONTH,1);

      HashMap<String, String> subscription =  new HashMap<>();
      HashMap<String, String> planaddons =  new HashMap<>();

      BillingSettingsForUser bsu = BillingSettingsForUser.findSettingsByUserId(agent.getLegacyId());
      BillingPlan agentPlan = bsu.getPlan();
      if (agentPlan == null) {
        baseView.message = "Unable to generate invoice - please contact Umapped support";
        return ok(views.html.error.error.render(baseView));

      }
      List<BillingPlan> addons = BillingPlan.findPlanAddons(agentPlan.getPlanId());

      if (addons != null) {
        for(BillingPlan bp: addons){
          planaddons.put(bp.getPlanId().toString(), bp.getName());
        }
      }


      List<BillingAddonsForUser> agentAddons = BillingAddonsForUser.getAddonsForUser(agent.getLegacyId());

      List<BillingSettingsForUser> minions = BillingSettingsForUser.getMinions(agent.getLegacyId());
      if (minions != null) {
        for (BillingSettingsForUser minion: minions) {
          if (minion.getBraintree() != null) {
            Account minionAcct = Account.findByLegacyId(minion.getUserId());
            subscription.put(minion.getBraintree().subToken, minionAcct.getFullName() + " (" + minion.getUserId() +  ")");
          }
        }
      }

      try {

        TransactionSearchRequest searchRequest = new TransactionSearchRequest().customerId()
                                                                               .is(billingInfo.custId)
                                                                               .createdAt()
                                                                               .between(startCal, endCal);



        ResourceCollection<Transaction> transactions = gateway.transaction().search(searchRequest);
        BraintreeInvoiceView view  = new BraintreeInvoiceView();
        view.name = agent.getFullName();
        view.address = new BillingAddress();
        view.address.company = billingInfo.address.company;
        view.address.streetAddress = billingInfo.address.streetAddress;
        view.address.extendedAddress = billingInfo.address.extendedAddress;
        view.address.locality = billingInfo.address.locality;
        view.address.region = billingInfo.address.region;
        view.address.postalCode = billingInfo.address.postalCode;
        view.address.countryCodeAlpha2 = billingInfo.address.countryCodeAlpha2;

        view.invoiceDate = Utils.formatTimestamp(startCal.getTimeInMillis(), "MMM d") + " to " + Utils.formatTimestamp(endCal.getTimeInMillis(), "d, YYYY");

        BigDecimal total = new BigDecimal(0);
        BigDecimal dueTotal = new BigDecimal(0);

        if (transactions != null) {
          for (Transaction t: transactions) {
            BraintreeInvoiceView.TxnDetail txnDetails = new BraintreeInvoiceView.TxnDetail();
            txnDetails.amount = "$" + t.getAmount();
            view.currency = t.getCurrencyIsoCode();
            txnDetails.date = Utils.formatTimestamp(t.getCreatedAt().getTimeInMillis(), "MMM d, YYYY");
            StringBuilder sb = new StringBuilder();
            if (t.getPlanId() != null) {
              if (agentPlan.getPlanId().toString().equals(t.getPlanId())) {
                sb.append("Plan: ");
                sb.append(agentPlan.getName());
              } else if (planaddons.containsKey(t.getPlanId())) {
                sb.append("Add Ons: ");
                sb.append(planaddons.get(t.getPlanId()));
              } else {
                BillingPlan bilPlan = BillingPlan.find.byId(Long.parseLong(t.getPlanId()));
                if (bilPlan != null) {
                  if (bilPlan.getParentPlan() == null) {
                    sb.append("Plan: ");
                  }
                  else {
                    sb.append("Add On: ");
                  }
                  sb.append(bilPlan.getName());

                }
              }
              if (t.getSubscriptionId() != null && subscription.containsKey(t.getSubscriptionId())) {
                if (!sb.toString().isEmpty()) {
                  sb.append("<br/>");
                }
                sb.append("User: ");
                sb.append(subscription.get(t.getSubscriptionId()));
                sb.append("<br/>");
              } else if (t.getDescriptor() != null && t.getDescriptor().getName() != null && t.getDescriptor().getName().contains("Umapped*")){
                if (!sb.toString().isEmpty()) {
                  sb.append("<br/>");
                }
                sb.append("User: ");
                sb.append(t.getDescriptor().getName().substring(t.getDescriptor().getName().indexOf("*")+1));
                sb.append("<br/>");
              }
            } else if (t.getOrderId() != null) {
              Trip trip = Trip.findByPK(t.getOrderId());
              if (trip !=  null) {
                sb.append("Trip: " + trip.getName());
              }

            }
            if (sb.toString().isEmpty() && t.getDescriptor() != null && t.getDescriptor().getName() != null) {
              sb.append(t.getDescriptor().getName());
            }
            txnDetails.plan = sb.toString();

            switch (t.getStatus()) {
              case AUTHORIZED:
              case AUTHORIZING:
              case SETTLING:
              case SUBMITTED_FOR_SETTLEMENT:
              case SETTLEMENT_PENDING:
                total = total.add(t.getAmount());
                txnDetails.status = "Pending";
                break;
              case PROCESSOR_DECLINED:
              case SETTLEMENT_DECLINED:
                txnDetails.status = "Declined";
                dueTotal = dueTotal.add(t.getAmount());
                break;
              case FAILED:
              case GATEWAY_REJECTED:
                dueTotal = dueTotal.add(t.getAmount());
                txnDetails.status = "Failed";
                break;
              case SETTLEMENT_CONFIRMED:
              case SETTLED:
                txnDetails.status = "Confirmed";
                total = total.add(t.getAmount());
                break;
              case VOIDED:
                dueTotal = dueTotal.add(t.getAmount());
                txnDetails.status = "Voided";
                break;
              case AUTHORIZATION_EXPIRED:
                dueTotal = dueTotal.add(t.getAmount());
                txnDetails.status = "Expired";
                break;
              default:
                txnDetails.status = t.getStatus().name();
                break;
            }

            if (t.getPlanId() != null) {
              if (agentPlan.getPlanId().toString().equals(t.getPlanId())) {
                txnDetails.txnType = "Subscription";
              } else{
                txnDetails.txnType = "Add-On";
              }
            } else if (t.getOrderId() != null) {
              txnDetails.txnType = "Pay Per Trip";
            }
            view.addTransactions(txnDetails);

          }

          /*
          if (true) {//calculate taxes
            BigDecimal taxes = total.subtract(total.divide(new BigDecimal(1.13), 2, BigDecimal.ROUND_HALF_UP));
            BigDecimal net = total.subtract(taxes);
            view.taxes = "$" + taxes.toString();
            view.netTotal = "$" + net.toString();

          }
          */
          view.total = "$" + total.toString();
          view.dueTotal = "$" + dueTotal.toString();

        }
        Html html = (Html) views.html.user.billingInvoice.render(view);
        response().setHeader("Content-Disposition", "attachment; filename=" + "Umapped_Invoice_" + Utils.formatTimestamp(startCal.getTimeInMillis(), "MMM_YYY") + ".pdf");

        return  pdfGenerator.ok(html, null);
      } catch (Exception e) {
        e.printStackTrace();
        Log.err("BRAINTREE - Error getting invoice details: " + agent.getLegacyId() + " Month: " + month + " Year: " + year, e);
        return ok(views.html.error.error.render(baseView));

      }



    } else {

      return ok(views.html.error.error.render(baseView));
    }


  }


  /*
   Receive asynchronous subscription events from Braintree
   All events are stored in the CC Audit log table
   A post save hook will generate an error message for alerting purposes for all notification messages
   */
  public Result braintreeSubscriptionWebhook() {
    BraintreeGateway gateway = braintreeFactory.getInstance();

    final DynamicForm form = Form.form().bindFromRequest();
    String bt_payload = form.get("bt_payload");
    String bt_signature = form.get("bt_signature");
    Long createdBy = 0L;
    Long billedTo = 0L;
    Long invoicedTo = 0L;
    Long planId = 0L;
    BillingSettingsForUser bsu = null;
    BillingAddonsForUser bau = null;
    try {
      WebhookNotification webhookNotification = gateway.webhookNotification().parse(bt_signature, bt_payload);
      if (webhookNotification == null || webhookNotification.getKind() == null || webhookNotification.getSubscription() == null) {
        Log.err("BRAINTREE - WebHook Exception - null");
      } else {
        Subscription subscription = webhookNotification.getSubscription();
        StringBuilder gatewayMsg = new StringBuilder();
        gatewayMsg.append("Subscription Id: ");
        gatewayMsg.append(subscription.getId());
        String userid = null;
        if (subscription.getDescriptor() != null && subscription.getDescriptor().getName() != null) {
          gatewayMsg.append(" ");gatewayMsg.append(subscription.getDescriptor().getName());
        }
        try {
          planId = Long.parseLong(subscription.getPlanId());
        } catch (Exception nfe) {

        }



        bsu = BillingSettingsForUser.getByBraintreeSubscriptionId(subscription.getId());
        if (bsu != null) {
          // this is a p[lan subscription
          gatewayMsg.append(" Plan subscription.");
          invoicedTo = Account.findByLegacyId(bsu.getUserId()).getUid();
          try {
            billedTo = Long.parseLong(bsu.getBraintree().custToken);
          } catch (Exception e) {

          }
        } else {
          //maybe it is an add-on subscription
          bau = BillingAddonsForUser.getByBraintreeSubscriptionId(subscription.getId());
          if (bau != null) {
            gatewayMsg.append(" Add-on subscription.");
            invoicedTo = Account.findByLegacyId(bau.getPk().getUserid()).getUid();
            try {
              billedTo = Long.parseLong(bau.getBraintree().custToken);
            }
            catch (Exception e) {

            }
          } else {
            gatewayMsg.append(" UNKNOWN SUNBSCRIPTION!!!");
          }
          gatewayMsg.append(" Event timestamp: " );
          gatewayMsg.append(Utils.formatDateTimePrint(subscription.getUpdatedAt().getTimeInMillis()));
        }


        BillingCCAudit billingCCAudit = BillingCCAudit.buildBraintree(createdBy, DBConnectionMgr.getUniqueLongId())
                                                      .setBilledUid(billedTo)
                                                      .setInvoiceUid(invoicedTo)
                                                      .setGatewayResp(gatewayMsg.toString())
                                                      .setPlanId(planId);

        boolean suspendUser = false;

        switch (webhookNotification.getKind()) {
          case SUBSCRIPTION_CANCELED:
            if (bau != null) {
              BraintreeSunbscription bauSub = bau.getBraintree();
              if (bauSub.active) {
                //cancel came from webhook
                suspendUser = true;
              }
              bauSub.active = false;
              bau.setBraintreeData(bauSub);
              bau.update();
            } else if (bsu != null) {
              BraintreeSunbscription bsuSub = bsu.getBraintree();
              if (bsuSub.active) {
                //cancel came from webhook
                suspendUser = true;
              }
              bsuSub.active = false;
              bsu.setBraintreeData(bsuSub);
              bsu.update();
            }
            billingCCAudit.setTxnType(BillingCCAudit.TxnType.NOTIFY_SUBSCRIPTION_CANCELED)
                .setStatus(BillingCCAudit.Status.OK);
            break;
          case SUBSCRIPTION_CHARGED_UNSUCCESSFULLY:
            //let's disable access
            suspendUser = true;
            billingCCAudit.setTxnType(BillingCCAudit.TxnType.NOTIFY_SUBSCRIPTION_CHARGED_FAILED)
               .setStatus(BillingCCAudit.Status.ERROR);
            break;
          case SUBSCRIPTION_EXPIRED:
            billingCCAudit.setTxnType(BillingCCAudit.TxnType.NOTIFY_SUBSCRIPTION_EXPIRED)
                .setStatus(BillingCCAudit.Status.ERROR);
            break;
          case SUBSCRIPTION_TRIAL_ENDED:
            billingCCAudit.setTxnType(BillingCCAudit.TxnType.NOTIFY_SUBSCRIPTION_TRIAL_ENDED)
                .setStatus(BillingCCAudit.Status.ERROR);
            break;
          case SUBSCRIPTION_WENT_PAST_DUE:
            //A subscription has moved from the active status to the past due status. This occurs when a subscription’s initial transaction is declined.
            //if this is for an addon, we will disable the add-on
            //if this is for plan, we can lock the user
            suspendUser = true;
            billingCCAudit.setTxnType(BillingCCAudit.TxnType.NOTIFY_SUBSCRIPTION_PAST_DUE)
                .setStatus(BillingCCAudit.Status.ERROR);
            break;
          case SUBSCRIPTION_CHARGED_SUCCESSFULLY:
            billingCCAudit.setTxnType(BillingCCAudit.TxnType.NOTIFY_SUBSCRIPTION_CHARGED)
                .setStatus(BillingCCAudit.Status.OK);
            break;
          case SUBSCRIPTION_WENT_ACTIVE:
            billingCCAudit.setTxnType(BillingCCAudit.TxnType.NOTIFY_SUBSCRIPTION_ACTIVE)
                .setStatus(BillingCCAudit.Status.OK);
            break;
        }

        if (suspendUser) {
          if (bau != null) {
            bau.setEnabled(false);
            bau.setModifiedBy("system");
            bau.setModifiedTs(Instant.now().toEpochMilli());
            bau.update();
          } else if (bsu != null) {
            Account userAccount = Account.findActiveByLegacyId(bsu.getUserId());

            if (bsu.getBillTo().getName() == BillingEntity.Type.USER) {
              //if this is the bill to agent, let's force a billing setting
              AccountProp billingProp = AccountProp.findByPk(userAccount.getUid(), AccountProp.PropertyType.BILLING_INFO);
              if (billingProp != null && billingProp.billingProperties != null && billingProp.billingProperties.braintree != null) {
                BraintreeBillingInfo braintree = billingProp.billingProperties.braintree;
                braintree.ccUpdateNeeded = true;
                billingProp.setModifiedBy(0L);
                billingProp.setModifiedTs(Timestamp.from(Instant.now()));
                billingProp.save();
              }
            } else {
              //lock the account for the minion
              if (userAccount != null) {
                userAccount.setState(RecordStatus.LOCKED);
                userAccount.setModifiedBy(0L);
                userAccount.setModifiedTs(Timestamp.from(Instant.now()));
                userAccount.update();
              }
            }
          }
        }
        if (bau != null) {
          BraintreeSunbscription bauSub = bau.getBraintree();
          bauSub.braintreeStatus = webhookNotification.getKind().name();
          bauSub.lastUpdatedTS = Utils.formatDateTimePrint(subscription.getUpdatedAt().getTimeInMillis());
          bau.setBraintreeData(bauSub);
          bau.update();
        } else if (bsu != null) {
          BraintreeSunbscription bsuSub = bsu.getBraintree();
          bsuSub.braintreeStatus = webhookNotification.getKind().name();
          bsuSub.lastUpdatedTS = Utils.formatDateTimePrint(subscription.getUpdatedAt().getTimeInMillis());
          bsu.setBraintreeData(bsuSub);
          bsu.update();
        }


        billingCCAudit.save();
      }

    } catch (Exception e) {
      Log.err("BRAINTREE - WebHook Exception ", e.getMessage());
    }

    return ok();
  }


  public Result braintreeEventWebhook() {
    BraintreeGateway gateway = braintreeFactory.getInstance();

    final DynamicForm form = Form.form().bindFromRequest();
    String bt_payload = form.get("bt_payload");
    String bt_signature = form.get("bt_signature");
    Long createdBy = 0L;
    Long planId = 0L;
    BillingSettingsForUser bsu = null;
    BillingAddonsForUser bau = null;
    try {
      WebhookNotification webhookNotification = gateway.webhookNotification().parse(bt_signature, bt_payload);
      if (webhookNotification == null || webhookNotification.getKind() == null || webhookNotification.getSubscription() == null) {
        Log.err("BRAINTREE - Event WebHook Exception - null");
      } else {
        BillingCCAudit audit = BillingCCAudit.buildBraintree(createdBy, DBConnectionMgr.getUniqueLongId()).setStatus(BillingCCAudit.Status.NOTIFY);

        switch (webhookNotification.getKind()) {
          case DISPUTE_LOST:
            audit.setTxnType(BillingCCAudit.TxnType.NOTIFY_DISPUTE_LOST);
            break;
          case DISPUTE_OPENED:
            audit.setTxnType(BillingCCAudit.TxnType.NOTIFY_DISPUTE_OPENED);
            break;
          case DISPUTE_WON:
            audit.setTxnType(BillingCCAudit.TxnType.NOTIFY_DISPUTE_WON);
            break;
          case DISBURSEMENT:
            audit.setTxnType(BillingCCAudit.TxnType.NOTIFY_DISBURSEMENT);
            break;
          case DISBURSEMENT_EXCEPTION:
            audit.setTxnType(BillingCCAudit.TxnType.NOTIFY_DISBURSEMENT_EXCEPTION);
            break;
          case TRANSACTION_DISBURSED:
            audit.setTxnType(BillingCCAudit.TxnType.NOTIFY_TRANSACTION_DISBURSED);
            break;
        }


        StringBuilder sb = new StringBuilder();

        if (webhookNotification.getKind() == WebhookNotification.Kind.DISPUTE_LOST ||
            webhookNotification.getKind() == WebhookNotification.Kind.DISPUTE_OPENED ||
            webhookNotification.getKind() == WebhookNotification.Kind.DISPUTE_WON) {
          sb.append("Id: ");
          sb.append(webhookNotification.getDispute().getId());
          sb.append(" ");
          sb.append("Amount: ");
          sb.append(webhookNotification.getDispute().getAmount());
          sb.append(" ");
          sb.append("Received Date: ");
          sb.append(Utils.formatDateTimePrint(webhookNotification.getDispute().getReceivedDate().getTimeInMillis()));
          sb.append(" ");
          sb.append("Reply-By Date: ");
          sb.append(Utils.formatDateTimePrint(webhookNotification.getDispute().getReplyByDate().getTimeInMillis()));
          sb.append(" ");
          sb.append("Opened Date: ");
          sb.append(Utils.formatDateTimePrint(webhookNotification.getDispute().getOpenedDate().getTimeInMillis()));
          sb.append(" ");
          sb.append("Status: ");
          sb.append(webhookNotification.getDispute().getStatus().name());
          sb.append(" ");
          sb.append("Reason: ");
          sb.append(webhookNotification.getDispute().getReason().name());
          sb.append(" ");
        }
        else if (webhookNotification.getKind() == WebhookNotification.Kind.DISBURSEMENT_EXCEPTION ||
                 webhookNotification.getKind() == WebhookNotification.Kind.DISBURSEMENT ||
                 webhookNotification.getKind() == WebhookNotification.Kind.TRANSACTION_DISBURSED) {
          sb.append("Date: ");
          sb.append(Utils.formatDateTimePrint(webhookNotification.getDisbursement()
                                                                 .getDisbursementDate()
                                                                 .getTimeInMillis()));
          sb.append(" ");
          sb.append("Amt: ");
          sb.append(webhookNotification.getDisbursement().getAmount());
          sb.append(" ");


          if (webhookNotification.getKind() == WebhookNotification.Kind.DISBURSEMENT_EXCEPTION) {
            sb.append("Msg: ");
            sb.append(webhookNotification.getDisbursement().getExceptionMessage());
            sb.append(" ");
            sb.append("Follow up: ");
            sb.append(webhookNotification.getDisbursement().getFollowUpAction());
            sb.append(" ");
          }
        }
        audit.setGatewayResp(sb.toString());
        audit.save();
      }

    } catch (Exception e) {
      Log.err("BRAINTREE - Event WebHook Exception ", e.getMessage());
    }

    return ok();
  }













  /*
  Utility method to create or update a Braintree customer and set the address as the default
 */
  private void processBraintreeCustomer(Account userAccount, BillingAddress address) throws Exception {
    AccountProp billingProperties = AccountProp.findByPk(userAccount.getUid(),
                                                         AccountProp.PropertyType.BILLING_INFO);
    BraintreeBillingInfo billingInfo  = null;
    if (billingProperties != null && billingProperties.billingProperties != null && billingProperties.billingProperties.braintree != null ) {
      billingInfo = billingProperties.billingProperties.braintree;
    }


    //validate the address

    //save it to the account prop - braintree
    BraintreeGateway braintreeGateway = braintreeFactory.getInstance();
    CustomerRequest request = new CustomerRequest()
        .firstName(address.fName)
        .lastName(address.lName)
        .company(address.company)
        .email(userAccount.getEmail())
        .id(userAccount.getUid().toString());

    Customer customer = null;

    Long txnId = DBConnectionMgr.getUniqueLongId();
    BillingCCAudit audit = BillingCCAudit.buildBraintree(userAccount.getUid(), txnId)
                                         .setStatus(BillingCCAudit.Status.SUBMITTED)
                                         .setBilledUid(userAccount.getUid())
                                         .setInvoiceUid(userAccount.getUid())
                                         .setMeta(billingInfo);

    com.braintreegateway.Result<Customer> custResult = null;
    //create/update the braintree customer
    if (billingInfo == null) {
      //first time, let's create the customer in braintree
      audit.setTxnType(BillingCCAudit.TxnType.NEW_CUSTOMER);
      audit.save();

      custResult = braintreeGateway.customer().create(request);
      if  (custResult != null && custResult.isSuccess()) {
        // true
        customer = custResult.getTarget();
        String customerToken = customer.getId();
        billingInfo = new BraintreeBillingInfo();
        billingInfo.address =  new BillingAddress();
        billingInfo.custId =  customerToken;
      }
    } else {
      audit.setTxnType(BillingCCAudit.TxnType.UPDATE_CUSTOMER);
      audit.save();

      custResult = braintreeGateway.customer().update(billingInfo.custId, request);
    }
    if  (custResult != null && custResult.isSuccess()) {
      // true
      customer = custResult.getTarget();
      String customerToken = customer.getId();
      if (billingInfo == null) {
        billingInfo = new BraintreeBillingInfo();
        billingInfo.address = new BillingAddress();
        billingInfo.custId = customerToken;
      }
      audit.setStatus(BillingCCAudit.Status.OK)
                    .setMeta(billingInfo)
                    .save();
    } else {
      if (custResult != null) {
        audit.setStatus(BillingCCAudit.Status.ERROR)
                      .setGatewayResp(custResult.getMessage())
                      .save();
      } else {
        audit.setStatus(BillingCCAudit.Status.ERROR)
                      .save();
      }
    }

    //set the customer address
    if (customer != null) {
      //update the addess
      audit = BillingCCAudit.buildBraintree(userAccount.getUid(), txnId)
                            .setStatus(BillingCCAudit.Status.SUBMITTED)
                            .setBilledUid(userAccount.getUid())
                            .setInvoiceUid(userAccount.getUid())
                            .setMeta(billingInfo);

      com.braintreegateway.Result<Address> addrResult = null;

      AddressRequest addressRequest = new AddressRequest().firstName(address.fName)
                                                          .lastName(address.lName)
                                                          .company(address.company)
                                                          .streetAddress(address.streetAddress)
                                                          .extendedAddress(address.extendedAddress)
                                                          .locality(address.locality)
                                                          .region(address.region)
                                                          .postalCode(address.postalCode)
                                                          .countryCodeAlpha2(address.countryCodeAlpha2);

      if (billingInfo.addrId != null && !billingInfo.addrId.isEmpty()) {
        try {
          audit.setTxnType(BillingCCAudit.TxnType.UPDATE_ADDRESS);
          audit.save();

          addrResult = braintreeGateway.address().update(billingInfo.custId,billingInfo.addrId,addressRequest);
        } catch (NotFoundException nfe) {
          Log.log(LogLevel.ERROR, "BRAINTREE: Payment method not found for " + userAccount.getLegacyId());
          billingInfo.addrId = null;
          audit.setStatus(BillingCCAudit.Status.ERROR)
                        .setGatewayResp(nfe.getMessage())
                        .save();
        }
      } else {
        audit.setTxnType(BillingCCAudit.TxnType.NEW_ADDRESS);
        audit.save();
        addrResult = braintreeGateway.address().create(billingInfo.custId, addressRequest);
        if (addrResult.isSuccess()) {
          billingInfo.addrId = addrResult.getTarget().getId();
        } else {
          //error
        }
      }
      if (addrResult.isSuccess()) {
        Address bAddress = addrResult.getTarget();
        //save local
        if (billingProperties == null) {
          billingProperties = AccountProp.build(userAccount.getUid(), AccountProp.PropertyType.BILLING_INFO, 0L);
        }
        if (billingInfo.address == null) {
          billingInfo.address = new BillingAddress();
        }
        billingInfo.address.company = address.company;
        billingInfo.address.fName = address.fName;
        billingInfo.address.lName = address.lName;
        billingInfo.address.streetAddress = address.streetAddress;
        billingInfo.address.extendedAddress = address.extendedAddress;
        billingInfo.address.locality = address.locality;
        billingInfo.address.region = address.region;
        billingInfo.address.countryCodeAlpha2 = address.countryCodeAlpha2;
        billingInfo.address.country = address.country;
        billingInfo.address.postalCode = address.postalCode;

        billingProperties.billingProperties.braintree = billingInfo;
        billingProperties.prePersist();
        billingProperties.save();

        audit.setStatus(BillingCCAudit.Status.OK)
                      .setMeta(billingInfo)
                      .save();
      }
      else {
        //error
        if (addrResult != null) {
          audit.setStatus(BillingCCAudit.Status.ERROR)
                        .setGatewayResp(addrResult.getMessage())
                        .save();
        } else {
          audit.setStatus(BillingCCAudit.Status.ERROR)
                        .save();
        }
      }
    } else {
      //should not happen
    }



  }


  /*
   * Utility method to add a payment nonce to the customer and address in braintree
   */
  private PymtStatus addPaymentMehtod (Account userAccount, AccountProp accountProp,  String paymentNonce, Long txnId) {

    BraintreeBillingInfo billingInfo  = null;
    if (accountProp != null && accountProp.billingProperties != null && accountProp.billingProperties.braintree != null) {
      billingInfo = accountProp.billingProperties.braintree;

      if (billingInfo != null && billingInfo.custId != null && billingInfo.addrId != null) {
        BillingCCAudit audit = null;
        com.braintreegateway.Result<? extends PaymentMethod> result = null;
        BraintreeGateway braintreeGateway = braintreeFactory.getInstance();
        if (billingInfo.pymtId != null) {
          //revoke the current payment
          audit = BillingCCAudit.buildBraintree(userAccount.getUid(), txnId)
                                               .setStatus(BillingCCAudit.Status.SUBMITTED)
                                               .setBilledUid(userAccount.getUid())
                                               .setInvoiceUid(userAccount.getUid())
                                               .setMeta(billingInfo)
                                               .setTxnType(BillingCCAudit.TxnType.UPDATE_PAYMENT);
          try {
            audit.save();
            PaymentMethodRequest request = new PaymentMethodRequest().customerId(billingInfo.custId)
                                                                     .billingAddressId(billingInfo.addrId)
                                                                     .paymentMethodNonce(paymentNonce)
                                                                     .options()
                                                                     .makeDefault(Boolean.TRUE)
                                                                     .done();
            result = braintreeGateway.paymentMethod().update(billingInfo.pymtId,request );

          } catch (NotFoundException nfe) {
            Log.log(LogLevel.ERROR, "BRAINTREE: Payment method not found for " + userAccount.getLegacyId());
            billingInfo.pymtId = null;
            accountProp.setModifiedBy(userAccount.getUid());
            accountProp.prePersist();
            accountProp.save();
            audit.setStatus(BillingCCAudit.Status.ERROR)
                          .setGatewayResp(nfe.getMessage())
                          .save();
          }
        } else {


          PaymentMethodRequest request = new PaymentMethodRequest().customerId(billingInfo.custId)
                                                                   .billingAddressId(billingInfo.addrId)
                                                                   .paymentMethodNonce(paymentNonce)
                                                                   .options()
                                                                   .makeDefault(Boolean.TRUE)
                                                                   .done();

          audit = BillingCCAudit.buildBraintree(userAccount.getUid(), txnId)
                                               .setStatus(BillingCCAudit.Status.SUBMITTED)
                                               .setBilledUid(userAccount.getUid())
                                               .setInvoiceUid(userAccount.getUid())
                                               .setMeta(billingInfo)
                                               .setTxnType(BillingCCAudit.TxnType.NEW_PAYMENT);
          audit.save();

          result = braintreeGateway.paymentMethod().create(request);
        }

        if (result != null && result.isSuccess()) {
          billingInfo.pymtId = result.getTarget().getToken();
          billingInfo.ccUpdateNeeded = false;

          if (CreditCard.class.isInstance(result.getTarget())) {
            CreditCard cc = (CreditCard) result.getTarget();
            if (billingInfo.ccInfo == null) {
              billingInfo.ccInfo = new BillingCCInfo();
            }
            billingInfo.ccInfo.cardNumber = Utils.maskCC(cc.getMaskedNumber());
            billingInfo.ccInfo.expiryMM = cc.getExpirationMonth();
            billingInfo.ccInfo.expiryYY = cc.getExpirationYear();
            billingInfo.ccInfo.cardType = cc.getCardType();
            billingInfo.ccInfo.imgUrl = cc.getImageUrl();
          }
          accountProp.setModifiedBy(userAccount.getUid());
          accountProp.prePersist();
          accountProp.save();
          audit.setStatus(BillingCCAudit.Status.OK)
                        .setMeta(billingInfo)
                        .save();
          return PymtStatus.SUCCESS;
        } else {
          //error
          if (result != null) {
            Log.log(LogLevel.ERROR, "BRAINTREE: Payment method issue: " + userAccount.getLegacyId() + " " + result.getMessage());
            StringBuilder sb = new StringBuilder();
            sb.append(result.getMessage());
            if (result.getCreditCardVerification() != null && result.getCreditCardVerification().getProcessorResponseCode() != null) {
              sb.append(" Processor Response Code: ");
              sb.append(result.getCreditCardVerification().getProcessorResponseCode());
            }

            audit.setStatus(BillingCCAudit.Status.ERROR)
                          .setGatewayResp(sb.toString())
                          .save();

            if (result.getErrors() != null && result.getErrors().getAllValidationErrors() != null) {
              for (ValidationError error : result.getErrors().getAllValidationErrors()) {
                Log.log(LogLevel.ERROR, "BRAINTREE: Payment method issue: " + userAccount.getLegacyId() + " " + error.getMessage());
              }
            }
            if (audit.getGatewayResp() != null && audit.getGatewayResp().trim().contains("Credit card type is not accepted by this merchant account")) {
              return PymtStatus.INVALID_CARD;
            }
          } else {
            audit.setStatus(BillingCCAudit.Status.ERROR)
                          .save();
          }
        }
      }
    }


    return PymtStatus.VALIDATIONN_ERROR;
  }


  /*
   * Utility method to create/update subscription - if one already exist, it will be updated with the payment method on file as well as set the billing date
   * This supports both the original agent and all agents billed to the original agent.
   * This will also set up subscriptions for any enabled add-ons
   * Note: this method does not support plan changes
   */
  private  int processAllSubscriptions (Account account, AccountBilling accountBilling,  BillingSettingsForUser billingSettingsForUser, Account modifiedBy, Long txnId) throws Exception{
    //find the user's own billing settings
    int processedCount = 0;

    models.publisher.BillingPlan plan = billingSettingsForUser.getPlan();
    if (plan != null && plan.getBillingType() != models.publisher.BillingPlan.BillType.INV && accountBilling != null
        && accountBilling.braintree != null && accountBilling.braintree.hasPayment()
        && (plan.getSchedule() == BillingSchedule.Type.MONTHLY || plan.getSchedule() == BillingSchedule.Type.YEARLY)
        &&  billingSettingsForUser.getBillTo().getName().ordinal() == BillingEntity.Type.USER.ordinal()) {

      //if this is an existing user, we set the billing start date to the start of the next invoice
      boolean billImmediate = true;
      if (BillingInvoice.invoiceExistsForUser(account.getLegacyId())) {
        billImmediate = false;
      }

      StringBuilder errorDetails = new StringBuilder();
      errorDetails.append("Billing Agent: ")
          .append(account.getUid())
          .append(" Plan: ")
          .append(billingSettingsForUser.getPlan().getPlanId());

      BraintreeGateway braintreeGateway  = braintreeFactory.getInstance();


      //update any previous subscription or create new ones
      com.braintreegateway.Result<Subscription> result = updateSubscription(billingSettingsForUser, accountBilling, billingSettingsForUser.getPlan(), braintreeGateway, billImmediate, null, modifiedBy, account, account, txnId);

      if (result != null) {
        if (result.isSuccess()) {
          Log.info("BRAINTREE: Success charging for subscription: for user " + errorDetails.toString());
          processedCount++;
        }
        else {
          Log.err("BRAINTREE: Error charging for subscription: " + result.getMessage() + " for user " + errorDetails.toString());
          throw new Exception("Braintree error - cannot create subscription for user:  " + errorDetails.toString());
        }
      }
      processAllAddonsForUser(account, accountBilling, braintreeGateway, modifiedBy, account, account, txnId);

      //let's figure out if there are any other users billing to this agent
      List<BillingSettingsForUser> minions =  BillingSettingsForUser.getMinions(account.getLegacyId());
      if (minions != null && minions.size() > 0) {
        for (BillingSettingsForUser minionBillingSetting : minions) {
          Account minionAcct = Account.findByLegacyId(minionBillingSetting.getUserId());
          //we can now charge the user
          updateSubscription(minionBillingSetting, accountBilling, billingSettingsForUser.getPlan(),  braintreeGateway, billImmediate, null, modifiedBy, account, minionAcct, txnId);
          if (result != null && result.isSuccess()) {
            Log.info("BRAINTREE: Success charging for subscription: " + errorDetails.toString());
            processedCount++;
            processAllAddonsForUser(minionAcct, accountBilling, braintreeGateway, account, account,minionAcct, txnId);
          } else {
            throw new Exception("Braintree error - cannot create subscription for user:  "  + errorDetails.toString());
          }
        }
      }
    }

    return processedCount;
  }


  /*
   * Utility method to handle a change in subscription - if a user  already exist, it will be cancelled and a new one created for the specified plan
   */
  private  void changeSubscription (AccountBilling accountBilling,  BillingPlan plan, BillingSettingsForUser billingSettingsForUser, Account modifiedBy, Account invoicedAcct, Account billedAcct) throws Exception {

    if (plan != null && plan.getBillingType() != models.publisher.BillingPlan.BillType.INV && accountBilling != null && accountBilling.braintree != null && accountBilling.braintree
        .hasPayment() && (plan.getSchedule() == BillingSchedule.Type.MONTHLY || plan.getSchedule() == BillingSchedule.Type.YEARLY)) {
      // the new plan is eligible
      Long txnId = DBConnectionMgr.getUniqueLongId();
      BraintreeGateway braintreeGateway = braintreeFactory.getInstance();
      Calendar nextBillDate = new Calendar.Builder().setInstant(billingSettingsForUser.getStartTs()).build();

      if (billingSettingsForUser.getBraintree() != null && billingSettingsForUser.getBraintree().isActive()) {
        //let's cancel the subscription for this user

        com.braintreegateway.Result<Subscription> result = cancelSingleSubscription(billingSettingsForUser, braintreeGateway, modifiedBy, billedAcct, invoicedAcct, txnId);
        if (result == null || !result.isSuccess()) {
          throw new Exception("Braintree error - cannot cancel subscription for user:  "  + billingSettingsForUser.getUserId() + " Sub: " + billingSettingsForUser.getBraintree().subToken);
        }
      }

      com.braintreegateway.Result<Subscription> result = updateSubscription(billingSettingsForUser, accountBilling, plan, braintreeGateway, false, nextBillDate, modifiedBy, billedAcct, invoicedAcct, txnId);
      if (result == null || !result.isSuccess()) {
        throw new Exception("Braintree error - cannot update subscription for user:  "  +  billingSettingsForUser.getUserId() + " Plan: " + billingSettingsForUser.getPlan().getPlanId());
      }
    }
  }


  /*
   * Utility method to update or create a single Braintree subscription
   */
  private com.braintreegateway.Result<Subscription>  updateSubscription(BillingSettingsForUser billingSettingsForUser, AccountBilling accountBilling, BillingPlan plan, BraintreeGateway braintreeGateway, boolean billImmediate, Calendar billDate, Account modifiedBy, Account billedAcct, Account invoicedAcct, Long txnId) {
    com.braintreegateway.Result<Subscription> result = null;
    BillingCCAudit audit = BillingCCAudit.buildBraintree(modifiedBy.getUid(), txnId)
                                         .setStatus(BillingCCAudit.Status.SUBMITTED)
                                         .setBilledUid(billedAcct.getUid())
                                         .setInvoiceUid(invoicedAcct.getUid())
                                         .setMeta(billingSettingsForUser.getBraintree())
                                         .setPlanId(plan.getPlanId());

    if (billingSettingsForUser.getBraintree() != null && billingSettingsForUser.getBraintree().isActive()) {
      audit.setTxnType(BillingCCAudit.TxnType.UPDATE_SUBSCRIPTION);
      audit.save();
      result = braintreeGateway.subscription().update(billingSettingsForUser.getBraintree().subToken,buildBraintreeSusbcription(accountBilling.braintree, plan, billingSettingsForUser.getUserId(), billingSettingsForUser, billImmediate, true, billDate, braintreeFactory));
    } else {
      audit.setTxnType(BillingCCAudit.TxnType.NEW_SUBSCRIPTION);
      result = braintreeGateway.subscription().create(buildBraintreeSusbcription(accountBilling.braintree, plan, billingSettingsForUser.getUserId(), billingSettingsForUser, billImmediate, false, billDate, braintreeFactory));
    }

    if (result != null) {
      if (result.isSuccess()) {
        BraintreeSunbscription braintreeData = new BraintreeSunbscription();
        braintreeData.subToken = result.getTarget().getId();
        braintreeData.custToken = accountBilling.braintree.custId;
        braintreeData.planId = plan.getPlanId().toString();
        braintreeData.createdTS = new DateTime().setTime(LocalDateTime.now());
        braintreeData.active = true;
        billingSettingsForUser.setBraintreeData(braintreeData);
        billingSettingsForUser.update();
        audit.setStatus(BillingCCAudit.Status.OK)
                      .setMeta(braintreeData)
                      .save();

      }
      else {
        Log.err("BRAINTREE: Error charging for subscription: " + result.getMessage() + " for user " + billingSettingsForUser.getUserId() + " Plan: " + plan.getPlanId());
        audit.setStatus(BillingCCAudit.Status.ERROR)
                      .setGatewayResp(result.getMessage())
                      .save();
      }
    } else {
      audit.setStatus(BillingCCAudit.Status.ERROR)
                    .save();
    }
    return result;
  }

  /*
   * Utility method to cancel all  Braintree subscription for an agent and all the users billing to the agent
   */
  private int cancelSubscription (Account account, AccountBilling accountBilling,  BillingSettingsForUser billingSettingsForUser, Account modifiedBy) throws Exception{
    //find the user's own billing settings
    int processedCount = 0;
    models.publisher.BillingPlan plan = billingSettingsForUser.getPlan();
    if (plan != null && plan.getBillingType() != models.publisher.BillingPlan.BillType.INV && accountBilling != null && accountBilling.braintree != null && accountBilling.braintree.hasPayment()) {
      //let's figure out the billing start date
      //if this is an existing user, we set the billing start date to the start of the next invoice
      Long txnId = DBConnectionMgr.getUniqueLongId();

      if (BillingInvoice.invoiceExistsForUser(account.getLegacyId())) {
        billingSettingsForUser.getStartDate();
      } else {
        //we set the braintree subscription to charge as of now

      }
      StringBuilder errorDetails = new StringBuilder();
      errorDetails.append("Billing Agent: ")
                  .append(account.getUid())
                  .append(" Plan: ")
                  .append(billingSettingsForUser.getPlan().getPlanId());

      BraintreeGateway braintreeGateway  = braintreeFactory.getInstance();


      //update any previous subscription
      com.braintreegateway.Result result = null;
      if (billingSettingsForUser.getBraintree() != null && billingSettingsForUser.getBraintree().isActive()) {
        result = cancelSingleSubscription(billingSettingsForUser, braintreeGateway, modifiedBy, account, account, txnId);
        if (result != null && result.isSuccess()) {
          processedCount++;
        } else {
          Log.err("BRAINTREE: Error cancelling subscription: "  + result.getMessage() +" for user " + errorDetails.toString());
          throw new Exception("Braintree error - cannot create subscription for user:  "  + errorDetails.toString());
        }

      }

      //cancel all active add-on subscriptions for this user
      cancelAllAddonsForUser(account, braintreeGateway, modifiedBy, account, account, txnId);

      //let's figure out if there are any other users billing to this agent
      List<BillingSettingsForUser> minions =  BillingSettingsForUser.getMinions(account.getLegacyId());
      if (minions != null && minions.size() > 0) {
        for (BillingSettingsForUser minionBillSetting : minions) {
          if (minionBillSetting.getBraintree() != null && minionBillSetting.getBraintree().isActive()
              && minionBillSetting.getBraintree().custToken != null
              && minionBillSetting.getBraintree().custToken.equals(billingSettingsForUser.getBraintree().custToken)) {
            Account minionAcct = Account.findActiveByLegacyId(minionBillSetting.getUserId());

            result = cancelSingleSubscription(minionBillSetting, braintreeGateway, modifiedBy,  account, minionAcct, txnId);
            //we can now charge the user
            if (result != null && result.isSuccess()) {
              processedCount++;
              //cancel all active add-on subscriptions for this user
              cancelAllAddonsForUser(minionAcct, braintreeGateway, modifiedBy,  account,minionAcct, txnId);

            }
            else {
              throw new Exception("Braintree error - cannot create subscription for user:  " + errorDetails.toString());
            }
          }
        }
      }
    }

    return processedCount;
  }

  /*
   Utility method to cancel a single subsription
   */
  private com.braintreegateway.Result<Subscription> cancelSingleSubscription (BillingSettingsForUser billingSettingsForUser, BraintreeGateway braintreeGateway, Account modifiedBy, Account billedAcct, Account invoicedAcct, Long txnId){
    com.braintreegateway.Result<Subscription> result = null;

    if (billingSettingsForUser.getBraintree() != null && billingSettingsForUser.getBraintree().isActive()) {
      BillingCCAudit audit = BillingCCAudit.buildBraintree(modifiedBy.getUid(), txnId)
                                           .setTxnType(BillingCCAudit.TxnType.CANCEL_SUBSCRIPTION)
                                           .setStatus(BillingCCAudit.Status.SUBMITTED)
                                           .setBilledUid(billedAcct.getUid())
                                           .setInvoiceUid(invoicedAcct.getUid())
                                           .setMeta(billingSettingsForUser.getBraintree())
                                           .setPlanId(Long.parseLong(billingSettingsForUser.getBraintree().planId));
      audit.save();

      result = braintreeGateway.subscription()
                               .cancel(billingSettingsForUser.getBraintree().subToken);


      //we can now charge the user
      if (result != null && result.isSuccess()) {
        BraintreeSunbscription braintreeSunbscription = billingSettingsForUser.getBraintree();
        braintreeSunbscription.active = false;
        billingSettingsForUser.setBraintreeData(braintreeSunbscription);
        billingSettingsForUser.update();
        audit.setStatus(BillingCCAudit.Status.OK)
                      .setGatewayResp(result.getMessage())
                      .save();
      } else {
        BraintreeSunbscription braintreeSunbscription = billingSettingsForUser.getBraintree();
        braintreeSunbscription.active = false;
        billingSettingsForUser.setBraintreeData(braintreeSunbscription);
        billingSettingsForUser.update();

        if (result != null) {
          Log.err("BRAINTREE: Error cancelling  subscription: " + result.getMessage() + " for user " + billingSettingsForUser.getUserId() + " Plan: " + billingSettingsForUser.getPlan().getPlanId());
          audit.setStatus(BillingCCAudit.Status.ERROR).setGatewayResp(result.getMessage()).save();
        } else {
          Log.err("BRAINTREE: Error cancelling  subscription: for user " + billingSettingsForUser.getUserId() + " Plan: " + billingSettingsForUser.getPlan().getPlanId());

          audit.setStatus(BillingCCAudit.Status.ERROR).save();
        }
      }
    }

    return result;
  }

  /*
  Utility method to cancel a single add-on subsription
  */
  public static com.braintreegateway.Result<Subscription> cancelSingleAddonSubscription (BillingAddonsForUser billingSettingsForUser, BraintreeGateway braintreeGateway, Account modifiedBy, Account billedAcct, Account invoicedAcct, Long txnId){
    com.braintreegateway.Result<Subscription> result = null;

    if (billingSettingsForUser.getBraintree() != null && billingSettingsForUser.getBraintree().isActive()) {
      BillingCCAudit audit = BillingCCAudit.buildBraintree(modifiedBy.getUid(), txnId)
                                           .setTxnType(BillingCCAudit.TxnType.CANCEL_ADDON)
                                           .setStatus(BillingCCAudit.Status.SUBMITTED)
                                           .setBilledUid(billedAcct.getUid())
                                           .setInvoiceUid(invoicedAcct.getUid())
                                           .setMeta(billingSettingsForUser.getBraintree())
                                           .setPlanId(Long.parseLong(billingSettingsForUser.getBraintree().planId));
      audit.save();

      result = braintreeGateway.subscription()
                               .cancel(billingSettingsForUser.getBraintree().subToken);


      //we can now charge the user
      if (result != null) {
        if (result.isSuccess()) {
          BraintreeSunbscription braintreeSunbscription = billingSettingsForUser.getBraintree();
          braintreeSunbscription.active = false;
          billingSettingsForUser.setBraintreeData(braintreeSunbscription);
          billingSettingsForUser.update();
          audit.setStatus(BillingCCAudit.Status.OK).setGatewayResp(result.getMessage()).save();
        }
        else {
          audit.setStatus(BillingCCAudit.Status.ERROR).setGatewayResp(result.getMessage()).save();
          BraintreeSunbscription braintreeSunbscription = billingSettingsForUser.getBraintree();
          braintreeSunbscription.active = false;
          billingSettingsForUser.setBraintreeData(braintreeSunbscription);
          billingSettingsForUser.update();
          Log.err("BRAINTREE: Error cancelling  subscription: " + result.getMessage() + " for user " + billingSettingsForUser.getPk().getUserid() + " Addon: " + billingSettingsForUser.getPk().getPlanId() + " Sub: " + billingSettingsForUser
                      .getBraintree().subToken);
        }
      } else {
        audit.setStatus(BillingCCAudit.Status.ERROR)
             .save();
        Log.err("BRAINTREE: Error cancelling  subscription:  for user " + billingSettingsForUser.getPk().getUserid() + " Addon: " + billingSettingsForUser.getPk().getPlanId() + " Sub: " + billingSettingsForUser
            .getBraintree().subToken);
      }
    }

    return result;
  }


  /*
   * process all active addons that can be billed
   * we will create a new subscription as needed
   */
  private int processAllAddonsForUser (Account account, AccountBilling accountBilling, BraintreeGateway braintreeGateway, Account modifiedBy, Account billedAcct, Account invoicedAcct, Long txnId) throws  Exception{
    List<BillingAddonsForUser> addOns = BillingAddonsForUser.getPaidActiveAddonsForUser(account.getLegacyId());
    int processed = 0;
    if (addOns != null) {
      for (BillingAddonsForUser addOn: addOns) {
        com.braintreegateway.Result<Subscription> result = processSingleAddonsForUser(accountBilling, addOn, braintreeGateway, modifiedBy, billedAcct, invoicedAcct, txnId, braintreeFactory);
        if (result.isSuccess()) {
          processed++;
        } else {
          throw new Exception("Braintree error - cannot process add-on subscription for user:  " + account.getLegacyId() + " Add-on Id: " + addOn.getPk().getPlanId() + " -- " + result.getMessage());
        }
      }
    }
    return processed;
  }

  /*
   * process all active addons that can be billed
   * we will create a new subscription as needed
   */
  public static com.braintreegateway.Result<Subscription> processSingleAddonsForUser (AccountBilling accountBilling, BillingAddonsForUser addOn, BraintreeGateway braintreeGateway, Account modifiedBy, Account billedAcct, Account invoicedAcct, Long txnId, BraintreeGatewayFactory factory) throws  Exception{
    if (addOn != null && addOn.getEnabled()) {
      BillingPlan bp = BillingPlan.find.byId(addOn.getPk().getPlanId());
      if  (bp.getEnabled() && bp.getParentPlan() != null && bp.getCost() > 0) {
        Calendar billDate = new Calendar.Builder().setInstant(addOn.getStartTs()).build();
        if (billDate.before(Calendar.getInstance())) {
          billDate = null; //reset since it is not future dated
        } else {
          //set to first of next month
          billDate = Calendar.getInstance();
          billDate.add(Calendar.MONTH, 1);
          billDate.set(Calendar.DAY_OF_MONTH, 1);
        }

        return updateAddonSubscription(addOn, accountBilling, bp, braintreeGateway, false, billDate, modifiedBy, billedAcct, invoicedAcct, txnId, factory);
      }
    }

    return null;
  }

  /*
  * process all active addons that are billed and we will cancel the subscription
  */
  private int cancelAllAddonsForUser (Account account, BraintreeGateway braintreeGateway, Account modifiedBy, Account billedAcct, Account invoicedAcct, Long txnId) throws  Exception{
    List<BillingAddonsForUser> addOns = BillingAddonsForUser.getPaidActiveAddonsForUser(account.getLegacyId());
    int processed = 0;
    if (addOns != null) {
      for (BillingAddonsForUser addOn: addOns) {
        cancelSingleAddonSubscription (addOn, braintreeGateway,modifiedBy, billedAcct, invoicedAcct, txnId );
        processed++;
      }
    }
    return processed;
  }

  public static com.braintreegateway.Result<Subscription>  updateAddonSubscription(BillingAddonsForUser billingAddonSettingsForUser, AccountBilling accountBilling, BillingPlan plan, BraintreeGateway braintreeGateway, boolean billImmediate, Calendar billDate, Account modifiedBy, Account billedAcct, Account invoicedAcct, Long txnId, BraintreeGatewayFactory factory) {
    com.braintreegateway.Result<Subscription> result = null;


    BillingCCAudit audit = BillingCCAudit.buildBraintree(modifiedBy.getUid(), txnId)
                                         .setStatus(BillingCCAudit.Status.SUBMITTED)
                                         .setBilledUid(billedAcct.getUid())
                                         .setInvoiceUid(invoicedAcct.getUid())
                                         .setMeta(billingAddonSettingsForUser.getBraintree())
                                         .setPlanId(plan.getPlanId());

    if (billingAddonSettingsForUser.getBraintree() != null && billingAddonSettingsForUser.getBraintree().isActive()) {
      audit.setTxnType(BillingCCAudit.TxnType.UPDATE_ADDON);
      audit.save();

      result = braintreeGateway.subscription().update(billingAddonSettingsForUser.getBraintree().subToken,buildBraintreeSusbcription(accountBilling.braintree, plan, billingAddonSettingsForUser.getPk().getUserid(), null, billImmediate, true, billDate, factory));
    } else {
      audit.setTxnType(BillingCCAudit.TxnType.NEW_ADDON);
      audit.save();

      result = braintreeGateway.subscription().create(buildBraintreeSusbcription(accountBilling.braintree, plan, billingAddonSettingsForUser.getPk().getUserid(), null, billImmediate, false, billDate, factory));
    }

    if (result != null) {
      if (result.isSuccess()) {
        BraintreeSunbscription braintreeData = new BraintreeSunbscription();
        braintreeData.subToken = result.getTarget().getId();
        braintreeData.custToken = accountBilling.braintree.custId;
        braintreeData.planId = plan.getPlanId().toString();
        braintreeData.createdTS = new DateTime().setTime(LocalDateTime.now());
        braintreeData.active = true;
        billingAddonSettingsForUser.setBraintreeData(braintreeData);
        billingAddonSettingsForUser.update();
        Log.info("BRAINTREE: Success charging for add-on: user " + billingAddonSettingsForUser.getPk().getUserid() + " Add-on Plan: " + plan.getPlanId());
        audit.setStatus(BillingCCAudit.Status.OK)
                      .setMeta(braintreeData)
                      .save();
      }
      else {
        BraintreeSunbscription braintreeSunbscription = billingAddonSettingsForUser.getBraintree();
        if (braintreeSunbscription != null) {
          braintreeSunbscription.active = false;
          billingAddonSettingsForUser.setBraintreeData(braintreeSunbscription);
        } else {
          billingAddonSettingsForUser.setBraintreeData(null);
        }
      billingAddonSettingsForUser.save();
       audit.setStatus(BillingCCAudit.Status.ERROR)
                      .setGatewayResp(result.getMessage())
                      .save();
        Log.err("BRAINTREE: Error charging for add-on subscription: " + result.getMessage() + " for user " + billingAddonSettingsForUser.getPk().getUserid() + " Add-on Plan: " + plan.getPlanId());
      }
    } else {
      audit.setStatus(BillingCCAudit.Status.ERROR)
                    .save();

    }
    return result;
  }

  public static SubscriptionRequest buildBraintreeSusbcription (BraintreeBillingInfo info, models.publisher.BillingPlan plan, String billedAgent, BillingSettingsForUser billingSettingsForUser, boolean billImmediate, boolean isUpdate, Calendar billDate, BraintreeGatewayFactory factory) {
    int billingDayOfMonth = 1;

    String userid = billedAgent;
    userid = userid.replaceAll("[^A-Za-z0-9 ]","");

    String descriptor = "";

    if (ConfigMgr.isProduction()) {
      if (userid.length() > 22) {
        userid = userid.substring(0, 21);
        descriptor = userid;
      }
    } else {
      if (userid.length() > 14) {
        userid = userid.substring(0, 13);
        descriptor = "Umapped*" + userid;
      }
    }

    SubscriptionRequest subscriptionRequest = new SubscriptionRequest()
        .planId(plan.getPlanId().toString())
        .merchantAccountId(factory.getMerchantIdForCurrency(plan.getCurrency()))
        .descriptor()
          .name(descriptor)
//          .phone("800-975-6713")
//          .url("umapped.com")
          .done()
        .paymentMethodToken(info.pymtId);
    if (!isUpdate) {
      //set the subsctiption id to a unique number
      subscriptionRequest.id(Utils.shortenLCNumber(DBConnectionMgr.getUniqueLongId()));
    }

    if (!isUpdate && !billImmediate) {

      //let's figure out the billing start date - only set for first time subscription
      Calendar calendar = new Calendar.Builder().setInstant(Instant.now().toEpochMilli()).build();

      //if a valid date is passed in, let's use it
      if (billDate != null && billDate.after(calendar)) {
       subscriptionRequest.firstBillingDate(billDate);
      } else if (billingSettingsForUser != null) {
        if (plan.getSchedule() == BillingSchedule.Type.MONTHLY) {
          //set to the first of next month
          calendar.add(Calendar.MONTH, 1);
          calendar.set(Calendar.DAY_OF_MONTH, 1);
        } else if (plan.getSchedule() == BillingSchedule.Type.YEARLY) {
          //based on the start bill date - it will be set to the next renewal date - this year or next
          Calendar startCal = new Calendar.Builder().setInstant(billingSettingsForUser.getStartTs()).build();
          if (startCal.get(Calendar.MONTH) <= calendar.get(Calendar.MONTH)) {
            //renewal is next year
            calendar.add(Calendar.YEAR, 1);
          }
          calendar.set(Calendar.MONTH, startCal.get(Calendar.MONTH));
          calendar.set(Calendar.DAY_OF_MONTH, 1);
        }
        Log.debug("BRAINTREE - Subscription: bill-for: " + billingSettingsForUser.getUserId() + " plan: " + plan.getPlanId().toString() + " Billing day: " + billingDayOfMonth + " Next Bill date: " + calendar.toString());
        subscriptionRequest.firstBillingDate(calendar);
      }

    }

    return subscriptionRequest;
  }


  public Result testCust(String accountId) {

    //let's test customer creating
    //get the token from the message body
    Account account = Account.findActiveByLegacyId(accountId);
    BillingAddress address = new BillingAddress();
    address.company = "Thierry INC";
    address.fName = account.getFirstName();
    address.lName = account.getLastName();
    address.streetAddress = "1 Yonge Street, Suite 102";
    address.locality= "Toronto";
    address.region = "ON1w";
    address.country = "Canada";
    address.countryCodeAlpha2 = "CA";
    address.postalCode = "M5S1A1";

    StopWatch sw = new StopWatch();
    sw.start();
    try {
      processBraintreeCustomer(account, address);
    } catch (Exception e) {
      e.printStackTrace();
    }
    sw.stop();


    return ok("ok " + sw.getTime());
  }

  public Result testNonce(String accountId, String nonce) {

    //let's test customer creating
    //get the token from the message body
    Account account = Account.findActiveByLegacyId(accountId);
    AccountProp accountProp = AccountProp.findByPk(account.getUid(), AccountProp.PropertyType.BILLING_INFO);

    StopWatch sw = new StopWatch();
    sw.start();
    try {
      addPaymentMehtod(account, accountProp, nonce, DBConnectionMgr.getUniqueLongId());
    } catch (Exception e) {
      e.printStackTrace();
    }
    sw.stop();


    return ok("ok " + sw.getTime());
  }


  public Result testSubscription (String accountId) {

    //let's test customer creating
    //get the token from the message body
    Account account = Account.findActiveByLegacyId(accountId);
    AccountProp accountProp = AccountProp.findByPk(account.getUid(), AccountProp.PropertyType.BILLING_INFO);

    StopWatch sw = new StopWatch();
    sw.start();
    try {
      processAllSubscriptions(account,
                              accountProp.billingProperties,
                              BillingSettingsForUser.findSettingsByUserId(account.getLegacyId()),
                              Account.findActiveByLegacyId("admin"),
                              DBConnectionMgr.getUniqueLongId());
    } catch (Exception e) {
      e.printStackTrace();
    }
    sw.stop();


    return ok("ok " + sw.getTime());
  }

  public Result testCancelSubscription (String accountId) {

    //let's test customer creating
    //get the token from the message body
    Account account = Account.findActiveByLegacyId(accountId);
    AccountProp accountProp = AccountProp.findByPk(account.getUid(), AccountProp.PropertyType.BILLING_INFO);

    BillingSettingsForUser bs = BillingSettingsForUser.findSettingsByUserId(account.getLegacyId());
    Client c = new Client();
    c.setCity("city");
    c.setCountry("country");
    c.setFirstName("thierry");
    bs.setFreshbookClientData(c);
    bs.save();

    StopWatch sw = new StopWatch();
    sw.start();
    try {
      cancelSubscription(account,
                         accountProp.billingProperties,
                         BillingSettingsForUser.findSettingsByUserId(account.getLegacyId()),
                         Account.findActiveByLegacyId("admin"));
    } catch (Exception e) {
      e.printStackTrace();
    }
    sw.stop();


    return ok("ok " + sw.getTime());
  }


  /*
    Check if we need to prevent the user from loggin in by redirecting to the CC setup page
    This will be for plan that are CC eligible and if they do not have a freskbook billing
   */
  public static Credentials.BillingStatus getBillingState(Credentials cred) {
    AccountProp billingProperties = AccountProp.findByPk(cred.getAccount().getUid(),
                                                         AccountProp.PropertyType.BILLING_INFO);

    if (billingProperties != null && billingProperties.billingProperties != null && billingProperties.billingProperties.braintree != null && billingProperties.billingProperties.braintree.hasPayment()) {
      if (billingProperties.billingProperties.braintree.ccUpdateNeeded) {
        return Credentials.BillingStatus.CREDIT_CARD_UPDATE_NEEDED;
      } else {
        BraintreeBillingInfo bi = billingProperties.billingProperties.braintree;
        if (bi != null && bi.ccInfo != null && bi.ccInfo.expiryYY != null && bi.ccInfo.expiryMM != null) {
          Calendar calendar = new Calendar.Builder().setDate(Integer.parseInt(bi.ccInfo.expiryYY),
                                                             Integer.parseInt(bi.ccInfo.expiryMM),
                                                             1).build();
          calendar.add(Calendar.DAY_OF_MONTH, -1); //calendar month index is zero based... now we are at the last day of the month

          if (calendar.toInstant().isBefore(Instant.now().plus(Duration.ofDays(30)))) {//expires in the next 30 days
            return Credentials.BillingStatus.CREDIT_CARD_EXPIRING;
          }
        }
        return Credentials.BillingStatus.CREDIT_CARD_OK;
      }
    }

    //no valid payment on file
    //check if one is needed
    BillingSettingsForUser bsu = BillingSettingsForUser.findSettingsByUserId(cred.getUserId());

    if ((billingProperties == null
        || billingProperties.billingProperties == null
        || billingProperties.billingProperties.braintree == null
        || !billingProperties.billingProperties.braintree.hasPayment())
        && bsu != null &&  bsu.getBillTo().getName() == BillingEntity.Type.USER) {
      //if the user needs to pay and there is no payment and the plan requires a credit card,
      // then require a setup
      BillingPlan bp = bsu.getPlan();
      if (bp != null) {
        if (bp.getBillingType() == BillingPlan.BillType.CC) {
          return Credentials.BillingStatus.CREDIT_CARD_SETUP_NEEDED;
        }
        else if (bp.getBillingType() == BillingPlan.BillType.BOTH) {
          //check if the user has been billed before
          if (!BillingInvoice.invoiceExistsForUser(cred.getUserId())) {
            return Credentials.BillingStatus.CREDIT_CARD_SETUP_NEEDED;
          }
          else {
            return Credentials.BillingStatus.CREDIT_CARD_OK;
          }
        }
        return Credentials.BillingStatus.INVOICE_BILLING;
      } else {
        return Credentials.BillingStatus.UNKNOWN;
      }
    } else if (bsu != null && bsu.getBillTo().getName() == BillingEntity.Type.AGENT) {
      BillingSettingsForUser bsa = BillingSettingsForUser.findSettingsByUserId(bsu.getAgent());
      if (bsa != null) {
        BillingPlan bp = bsa.getPlan();
        boolean isCCPlan = false;
        if (bp != null) {
          if (bp.getBillingType() == BillingPlan.BillType.CC) {
            isCCPlan = true;
          }
          else if (bp.getBillingType() == BillingPlan.BillType.BOTH) {
            //check if the user has been billed before
            if (!BillingInvoice.invoiceExistsForUser(cred.getUserId())) {
              isCCPlan = true;
            }
          }
        }
        //if this is a credit card plan, then there should be an active subscription
        if (isCCPlan && (bsu.getBraintree() == null
                         || !bsu.getBraintree().isActive())) {
          return Credentials.BillingStatus.AGENT_SUBSCRIPTION_NOT_SETUP;
        }
      }
    }

    return Credentials.BillingStatus.INVOICE_BILLING;
  }

  public static TxnStatus chargeTrip(String tripId, Account user, BraintreeGatewayFactory factory) {
    Log.debug("chargeTrip 1 - tripId: " + tripId + " user: " + user);
    AccountProp accountProp = AccountProp.findByPk(user.getUid(), AccountProp.PropertyType.BILLING_INFO);
    if (accountProp != null && accountProp.billingProperties != null && accountProp.billingProperties.braintree != null && accountProp.billingProperties.braintree.hasPayment()) {
      BillingSettingsForUser bsu = BillingSettingsForUser.findSettingsByUserId(user.getLegacyId());
      if (bsu != null) {
        BillingPlan bp = bsu.getPlan();
        if (bp == null && bsu.getAgent() != null && !bsu.getAgent().isEmpty()) {
          BillingSettingsForUser asu = BillingSettingsForUser.findSettingsByUserId(bsu.getAgent());
          bp = asu.getPlan();
        }
        if (bp != null) {
          BigDecimal amount = new BigDecimal(bp.getCost());
          amount = amount.setScale(2, RoundingMode.HALF_UP);
          BraintreeGateway braintreeGateway = factory.getInstance();

          BillingCCAudit audit = BillingCCAudit.buildBraintree(user.getUid(), DBConnectionMgr.getUniqueLongId());
          audit.setBilledUid(user.getUid())
              .setInvoiceUid(user.getUid())
              .setPlanId(bp.getPlanId())
              .setStatus(BillingCCAudit.Status.SUBMITTED)
              .setTxnType(BillingCCAudit.TxnType.TRANSACTION)
              .setGatewayResp("Trip: " + tripId)
              .save();

          try {

            BraintreeBillingInfo bi = accountProp.billingProperties.braintree;
            TransactionRequest request = new TransactionRequest().customerId(bi.custId)
                                                                 .paymentMethodToken(bi.pymtId)
                                                                 .orderId(tripId)
                                                                 .amount(amount)
                                                                 .merchantAccountId(factory.getMerchantIdForCurrency(bp.getCurrency()))
                                                                 .options()
                                                                 .submitForSettlement(true)
                                                                 .done();



            com.braintreegateway.Result<Transaction> result = braintreeGateway.transaction().sale(request);
            Log.debug("chargeTrip 4 - tripId: " + tripId + " request: " + bi.pymtId + " " + bi.custId + " " + factory.getMerchantIdForCurrency(bp.getCurrency())
            + " " + amount + " " + request.toQueryString());

            StringBuilder sb = new StringBuilder(audit.getGatewayResp());
            sb.append("\n ");

            if (result != null) {

              if (result.isSuccess()) {
                Log.debug("chargeTrip  - tripId: " + tripId + " success");

                Transaction transaction = result.getTarget();
                sb.append("Id: ");
                sb.append(transaction.getId());
                sb.append(" Auth Code: ");
                sb.append(transaction.getProcessorAuthorizationCode());
                sb.append(" Processor Text: ");
                sb.append(transaction.getProcessorResponseText());
                sb.append(" Processor Code: ");
                sb.append(transaction.getProcessorResponseCode());

                audit.setGatewayResp(sb.toString()).setStatus(BillingCCAudit.Status.OK).save();

                return TxnStatus.SUCCESS;
              }
              else {
                Log.err("chargeTrip 7 - tripId: " + tripId + result.toString() + " " + result.getMessage());

                if (result.getErrors() != null) {
                  if (result.getErrors().getAllDeepValidationErrors() != null) {
                    for (ValidationError ve : result.getErrors().getAllDeepValidationErrors()) {
                      sb.append(ve.getMessage());
                      sb.append("\n ");
                    }
                  }

                  for (ValidationError ve : result.getErrors().getAllValidationErrors()) {
                    sb.append(ve.getMessage());
                    sb.append("\n ");
                  }

                  audit.setStatus(BillingCCAudit.Status.ERROR).setGatewayResp(sb.toString()).save();
                  return TxnStatus.VALIDATIONN_ERROR;
                }
                Transaction transaction = result.getTarget();

                if (transaction != null) {

                  sb.append(" ERROR: \n ");
                  sb.append(transaction.getStatus().name());
                  sb.append(" ");
                  sb.append(transaction.getProcessorResponseCode());
                  sb.append(" ");
                  sb.append(transaction.getProcessorResponseText());
                  sb.append(" ");
                  sb.append(transaction.getAdditionalProcessorResponse());
                  sb.append(" Id: ");
                  sb.append(transaction.getId());
                  sb.append(" Auth Code: ");
                  sb.append(transaction.getProcessorAuthorizationCode());
                  sb.append(" Processor Text: ");
                  sb.append(transaction.getProcessorResponseText());
                  sb.append(" Processor Code: ");
                  sb.append(transaction.getProcessorResponseCode());
                  audit.setStatus(BillingCCAudit.Status.ERROR);
                  audit.save();

                  switch (transaction.getStatus()) {
                    case AUTHORIZATION_EXPIRED:
                    case FAILED:
                    case SETTLEMENT_DECLINED:
                    case PROCESSOR_DECLINED:
                    case GATEWAY_REJECTED:
                    case UNRECOGNIZED:
                    case VOIDED:
                      return TxnStatus.CARD_DECLINED;

                  }
                }
              }
            }
          } catch (Exception e) {
            e.printStackTrace();
            Log.err("chargeTrip 12- tripId: " + tripId + " user: " + user, e);
            audit.setStatus(BillingCCAudit.Status.ERROR)
                .setGatewayResp(audit.getGatewayResp() + " " + e.getMessage())
                .save();
          }
        }


        return TxnStatus.SUCCESS;
      }
    }

    return TxnStatus.NO_CARD;

  }

}