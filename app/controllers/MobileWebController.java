package controllers;

import com.google.inject.Inject;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.form.ResetPwdForm;
import com.mapped.publisher.persistence.RecordStatus;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.UmappedEmail;
import com.mapped.publisher.view.BaseView;
import com.umapped.mobile.api.MobileAuthenticateService;
import models.publisher.Account;
import models.publisher.AccountAuth;
import models.publisher.EmailLog;
import models.publisher.UserProfile;
import org.apache.commons.mail.EmailException;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by wei on 2017-07-26.
 */
public class MobileWebController
    extends Controller {
  @Inject FormFactory formFactory;

  @Inject MobileAuthenticateService authenticateService;

  public Result resetPasswordForm() {
    BaseView view = new BaseView();

    return ok(views.html.mobile2.resetPwd.render(view));
  }

  public Result triggerResetPassword() {
    Form<ResetPwdForm> resetPwdForm = formFactory.form(ResetPwdForm.class).bindFromRequest();

    //use bean validator framework
    if (resetPwdForm.hasErrors()) {
      BaseView baseView = new BaseView("Errors on the form - please resubmit");
      return ok(views.html.mobile2.resetPwd.render(baseView));
    }

    try {
      ResetPwdForm resetInfo = resetPwdForm.get();
      //TODO: Discuss how this should change when userId is not a requirement
      Account a = Account.findByEmail(resetInfo.getInEmail());
      if (a == null) {
        BaseView baseView = new BaseView("Requested account does not exist.");
        return ok(views.html.mobile2.resetPwd.render(baseView));
      }

      if (a.getState() == RecordStatus.DELETED) {
        BaseView baseView = new BaseView("User has been disabled. Please contact your administrator.");
        return ok(views.html.mobile2.resetPwd.render(baseView));
      }

      if (a.getAccountType() == Account.AccountType.UMAPPED_ADMIN) {
        BaseView baseView = new BaseView(
            "Password reset feature not available for umapped administrators. Please contact Umapped support.");
        return ok(views.html.mobile2.resetPwd.render(baseView));
      }

      return resetPasswordHelper(a, a, false);
    }
    catch (Exception e) {
      BaseView view1 = new BaseView("System error. Failed to reset password");
      return ok(views.html.mobile2.resetPwd.render(view1));
    }
  }

  private Result resetPasswordHelper(Account a, Account requestedBy, boolean isAdmin) {
    AccountAuth auth = AccountAuth.getPassword(a.getUid());
    if (auth == null) {
      auth = AccountAuth.build(requestedBy.getUid());
      auth.setAuthType(AccountAuth.AAuthType.PASSWORD);
      auth.setUid(a.getUid());
      auth.setSalt(UUID.randomUUID().toString());
    }

    a.setState(RecordStatus.LOCKED);
    a.setModifiedTs(Timestamp.from(Instant.now()));
    a.save();

    String resetToken = UUID.randomUUID().toString();

    if (a.getLegacyId() != null) { //TODO: Remove when legacy user profiles are no longer needed
      UserProfile u = UserProfile.find.byId(a.getLegacyId());
      if (u != null) {
        u.setStatus(APPConstants.STATUS_ACCOUNT_LOCKED);
        u.setPwdresetid(resetToken);
        u.setPwdresetexpiry(System.currentTimeMillis() + APPConstants.PWD_RESET_EXPIRY);
        u.setModifiedby(requestedBy.getLegacyId());
        u.update();
        SecurityController.auditActivity(u.userid, APPConstants.USER_AUDIT_PASSWORD_RESET_REQ);
      }
    }

    auth.setResetToken(resetToken);
    auth.setResetExpire(System.currentTimeMillis() + APPConstants.PWD_RESET_EXPIRY);
    auth.save();

    String subject = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_PWD_RESET_SUBJECT);
    String hostUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);
    String fromEmail = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_PWD_RESET_FROM);

    StringBuilder htmlPart = new StringBuilder();
    if (isAdmin) {
      htmlPart.append("<p>Your password has been reset by an administrator.</p> <p> Click <a href='");
    }
    else {
      htmlPart.append("<p>You have requested a password reset.</p> <p> Click <a href='");
    }
    htmlPart.append(hostUrl)
            .append(routes.MobileWebController.resetPwdLink(resetToken).url())
            .append("'> here </a> to reset your password </p> <p>If you " + "did not reset your password, please " +
                    "contact your administrator or our support desk at " + "help@umapped.com.</p><p>Please note that " +
                    "this link will expire.</p>");

    try {
      List<String> emails = new ArrayList<>();
      emails.add(a.getEmail());

      UmappedEmail email = UmappedEmail.buildDefault();
      email.withHtml(htmlPart.toString())
           .withSubject(subject)
           .withToList(emails)
           .withEmailType(EmailLog.EmailTypes.PASSWORD_RESET)
           .withAccountUid(a.getUid())
           .setFrom(fromEmail, "Umapped")
           .addTo(a.getEmail());


      String emailId = email.buildAndSend();
      Log.debug("resetPwd(): Sent password reset email ID: " + emailId + " to: " + a.getEmail());
      if (isAdmin) {
        BaseView baseView = new BaseView("A password reset email has been sent.");
        return ok(views.html.common.message.render(baseView));
      }

      BaseView successView = new BaseView("You will receive an email to complete your password reset shortly.");
      return ok(views.html.mobile2.resetPwdStatus.render(successView));
    }
    catch (EmailException ee) {
      Log.err("resetPasswordHelper(): Failed send password reset email: " + a.getEmail(), ee);
      if (isAdmin) {
        BaseView baseView = new BaseView("System error. Please contact Umapped support at support@umapped.com");
        return ok(views.html.common.message.render(baseView));
      }

      BaseView emailSendErrorView = new BaseView("System Error. Please retry or contact your administrator.");
      return ok(views.html.mobile2.resetPwd.render(emailSendErrorView));
    }
  }

  public Result resetPwdLink(String token) {
    //use bean validator framework
    if (token == null || token.length() == 0) {
      BaseView baseView = new BaseView("Errors on the form - please resubmit");
      return ok(views.html.mobile2.resetPwd.render(baseView));
    }

    try {
      AccountAuth auth = AccountAuth.findByResetToken(token);
      if (auth.getResetExpire() != null && auth.getResetExpire() < System.currentTimeMillis()) {
        auth.clearPasswordReset();
        BaseView bv = new BaseView("Password reset link expired.");
        return ok(views.html.mobile2.resetPwd.render(bv));
      }

      BaseView baseView = new BaseView("Please change your password.");
      baseView.linkId = token;
      return ok(views.html.mobile2.resetPwdLink.render(baseView));
    }
    catch (Exception e) {
      BaseView view1 = new BaseView("Invalid link - please retry or contact your administrator.");
      return ok(views.html.mobile2.resetPwd.render(view1));
    }
  }


  public Result setNewPassword() {
    //bind html form to form bean
    Form<ResetPwdForm> resetPwdForm = formFactory.form(ResetPwdForm.class).bindFromRequest();
    //use bean validator framework
    if (resetPwdForm.hasErrors()) {
      BaseView bv = new BaseView("Unknown error with submitted request.");
      return ok(views.html.mobile2.resetPwd.render(bv));
    }

    try {
      ResetPwdForm resetInfo = resetPwdForm.get();
      if (resetInfo.getInLinkId() == null) {
        BaseView bv = new BaseView("Password reset link is corrupted.");
        return ok(views.html.mobile2.resetPwd.render(bv));
      }

      AccountAuth auth = AccountAuth.findByResetToken(resetInfo.getInLinkId());
      if (auth == null) {
        BaseView bv = new BaseView("Password reset not requested.");
        return ok(views.html.mobile2.resetPwd.render(bv));
      }

      if (auth.getResetExpire() != null && auth.getResetExpire() < System.currentTimeMillis()) {
        auth.clearPasswordReset();
        BaseView bv = new BaseView("Password reset link expired.");
        return ok(views.html.mobile2.resetPwd.render(bv));
      }

      Account a = Account.find.byId(auth.getUid());
      if (a.getState() != RecordStatus.LOCKED) {
        BaseView bv = new BaseView("Invalid link - please retry or contact your administrator.");
        return ok(views.html.mobile2.resetPwd.render(bv));
      }

      if (resetInfo.getInNewPwd() != null && resetInfo.getInNewPwd().length() < 8) {
        BaseView bv = new BaseView("Password must be longer than 8 characters.");
        bv.linkId = resetInfo.getInLinkId();
        return ok(views.html.mobile2.resetPwdLink.render(bv));
      }

      if (resetInfo.getInNewPwd() != null && resetInfo.getInNewPwd().length() > 1024) {
        BaseView bv = new BaseView("Your password is too ambitious we support only passwords shorter than 1024 " +
                                   "characters.");
        bv.linkId = resetInfo.getInLinkId();
        return ok(views.html.mobile2.resetPwdLink.render(bv));
      }


      if (resetInfo.getInConfirmPwd() == null || resetInfo.getInNewPwd() == null || !resetInfo.getInConfirmPwd()
                                                                                              .equals(resetInfo
                                                                                                          .getInNewPwd())) {
        BaseView bv = new BaseView("Your passwords do not match. Please retry.");
        bv.linkId = resetInfo.getInLinkId();
        return ok(views.html.mobile2.resetPwdLink.render(bv));
      }

      boolean res = false;
      if (a.getAccountType() == Account.AccountType.TRAVELER) {
        res = authenticateService.setPassword(a, resetInfo.getInNewPwd());
      }
      else {
        UserProfile u = UserProfile.findByPK(a.getLegacyId());

        AccountController.AccountHelper ah = AccountController.AccountHelper.build(a.getUid());
        ah.setAccount(a).setUserProfile(u);

        res = ah.resetPassword(resetInfo.getInNewPwd());
      }
      if (!res) {
        BaseView bv = new BaseView("System error. Failed to reset password.");
        return ok(views.html.mobile2.resetPwd.render(bv));
      }

      BaseView baseView = new BaseView("Your password has been set successfully.");
      return ok(views.html.mobile2.resetPwdStatus.render(baseView));
    }
    catch (Exception e) {
      BaseView view1 = new BaseView("System error. Failed to reset password");
      return ok(views.html.mobile2.resetPwd.render(view1));
    }
  }
}
