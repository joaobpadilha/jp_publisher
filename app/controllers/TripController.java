package controllers;

import actors.*;
import akka.NotUsed;
import akka.actor.Status;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.mapped.common.CoreConstants;
import com.mapped.persistence.Users;
import com.mapped.persistence.UsersMgrExt;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.audit.AuditActionType;
import com.mapped.publisher.audit.AuditActorType;
import com.mapped.publisher.audit.AuditModuleType;
import com.mapped.publisher.audit.event.AuditTrip;
import com.mapped.publisher.audit.event.AuditTripDocGuide;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.*;
import com.mapped.publisher.msg.TripPdfRec;
import com.mapped.publisher.parse.BigFive.TourList.Tour;
import com.mapped.publisher.persistence.CompanyMgr;
import com.mapped.publisher.persistence.SharedTripRS;
import com.mapped.publisher.persistence.TripBrandingMgr;
import com.mapped.publisher.persistence.TripMgr;
import com.mapped.publisher.persistence.communicator.MessengerUpdateHelper;
import com.mapped.publisher.utils.*;
import com.mapped.publisher.view.*;
import com.umapped.api.schema.types.Email;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.helper.BraintreeGatewayFactory;
import com.umapped.helper.TripPublisherHelper;
import com.umapped.helper.UmappedEmailFactory;
import com.umapped.itinerary.analyze.ItineraryAnalyzeResult;
import com.umapped.itinerary.analyze.model.json.ZoneDateTimeJsonMarshaller;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.service.offer.OfferService;
import com.umapped.service.offer.UMappedCityLookup;
import models.publisher.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.mail.EmailAttachment;
import org.apache.xmlbeans.impl.common.SystemCache;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-11
 * Time: 10:32 AM
 * To change this template use File | Settings | File Templates.
 */
public class TripController
    extends Controller {

  @Inject
  BraintreeGatewayFactory braintreeFactory;
  @Inject
  private FormFactory formFactory;

  @Inject
  TripPublisherHelper publisherHelper;


  private static HashMap<String, java.lang.reflect.Method> cachedCustomEmailTemplates = new HashMap<>();
  private static HashMap<String, java.lang.reflect.Method> cachedCustomFullEmailTemplates = new HashMap<>();

  private static ArrayList<String> cmpIdsForDefaultEmailTemplate = new ArrayList<>();

  private static HashMap<String, java.lang.reflect.Method> cachedCustomAgentEmailTemplates = new HashMap<>();
  private static ArrayList<String> cmpIdsForDefaultAgentEmailTemplate = new ArrayList<>();

  @With({Authenticated.class, Authorized.class})
  public Result calendarTrips() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    final DynamicForm form = formFactory.form().bindFromRequest();
    String inDate = form.get("inDate");
    DateMidnight inDateTime = null;
    if (inDate != null && inDate.length() > 0) {
      try {
        long ms = Long.parseLong(inDate);
        if (ms > 0) {
          inDateTime = new DateMidnight(ms);
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    if (inDateTime == null) {
      inDateTime = new DateMidnight();
    }

    long startMS = inDateTime.minusDays(inDateTime.getDayOfMonth() - 1).getMillis();
    long endMS = inDateTime.plusDays(inDateTime.dayOfMonth().getMaximumValue() - inDateTime.getDayOfMonth())
                           .getMillis();

    TripsView view = new TripsView();
    view.startDay = String.valueOf(inDateTime.getDayOfMonth());
    view.startMonth = String.valueOf(inDateTime.getMonthOfYear());

    view.startYear = String.valueOf(inDateTime.getYear());

    view.nextDate = String.valueOf(inDateTime.minusDays(inDateTime.getDayOfMonth() - 1).plusMonths(1).getMillis());
    view.prevDate = String.valueOf(inDateTime.minusDays(inDateTime.getDayOfMonth() - 1).minusMonths(1).getMillis());

    Credentials cred = sessionMgr.getCredentials();

    Map<String, Company> cmpiesMap = new HashMap<>();

    ArrayList<String> cmpies = new ArrayList<>();

    HashMap<String, String> agencyList = new HashMap<>();

    if(cred.hasCompany()) {
      Company c = cred.getCompany();
      agencyList.put(c.getCmpyid(), c.getName());
      cmpies.add(cred.getCmpyId());
      cmpiesMap.put(c.getName(), c);
    }

    view.tripAgencyList = agencyList;

    List<Trip> pendingTrips = Trip.findByUserIdDate(sessionMgr.getUserId(),
                                                    startMS,
                                                    endMS,
                                                    APPConstants.STATUS_PUBLISH_PENDING);
    if (pendingTrips != null) {
      view.pendingTripCount = pendingTrips.size();
      view.pendingTrips = TripController.buildTripView(pendingTrips, cmpiesMap, null);
    }

    List<Trip> publishedTrips = Trip.findByUserIdDate(sessionMgr.getUserId(),
                                                      startMS,
                                                      endMS,
                                                      APPConstants.STATUS_PUBLISHED);
    if (publishedTrips != null) {
      view.publishedTripCount = publishedTrips.size();
      view.publishedTrips = TripController.buildTripView(publishedTrips, cmpiesMap, null);

    }

    List<Trip> pendingGroupTrips = new ArrayList<>();
    List<Trip> publishedGroupTrips = new ArrayList<>();


    if (cred.getCmpyLinkType() == AccountCmpyLink.LinkType.ADMIN) {
      //get all companies where the user is the admin
      List<String> adminCmpies = new ArrayList<>();
      adminCmpies.add(cred.getCmpyId());

      List<Trip> pTrips = Trip.findByCmpyIdsDate(sessionMgr.getUserId(),
                                                 adminCmpies,
                                                 startMS,
                                                 endMS,
                                                 APPConstants.STATUS_PUBLISH_PENDING);
      if (pTrips != null) {
        pendingGroupTrips.addAll(pTrips);

      }

      pTrips = Trip.findByCmpyIdsDate(sessionMgr.getUserId(),
                                      adminCmpies,
                                      startMS,
                                      endMS,
                                      APPConstants.STATUS_PUBLISHED);
      if (pTrips != null) {
        publishedGroupTrips.addAll(pTrips);
      }
    }


    if (cred.getCmpyLinkType() != AccountCmpyLink.LinkType.ADMIN && cred.hasGroupies()) {
      List<String> users = new ArrayList<>();
      List<String> groups = new ArrayList<>();

      groups.addAll(cred.getUserGroups().keySet());

      for (String groupId : groups) {
        users.addAll(cred.getUserGroups().get(groupId));
      }

      List<Trip> pTrips = Trip.findByUserIdsDate(users, startMS, endMS, APPConstants.STATUS_PUBLISH_PENDING);
      if (pTrips != null) {
        pendingGroupTrips.addAll(pTrips);

      }

      pTrips = Trip.findByUserIdsDate(users, startMS, endMS, APPConstants.STATUS_PUBLISHED);
      if (pTrips != null) {
        publishedGroupTrips.addAll(pTrips);
      }
    }

    if (pendingGroupTrips != null) {
      view.groupPendingTripsCount = pendingGroupTrips.size();
      view.groupPendingTrips = TripController.buildTripView(pendingGroupTrips, cmpiesMap, null);
    }
    if (publishedGroupTrips != null) {
      view.groupPublishedTripsCount = publishedGroupTrips.size();
      view.groupPublishedTrips = TripController.buildTripView(publishedGroupTrips, cmpiesMap, null);
    }


    return ok(views.html.trip.upcomingTrips.render(view));
  }

  public static List<TripInfoView> buildTripView(List<Trip> trips,
                                                 Map<String, Company> cmpies,
                                                 Map<String, String> userNames) {
    ArrayList<TripInfoView> tripViews = new ArrayList<>();
    for (Trip t : trips) {

      TripInfoView tripView = new TripInfoView();
      tripView.tripId = t.tripid;
      tripView.tripName = t.name;
      tripView.tripNote = t.comments;
      tripView.tripStatus = t.status;
      tripView.cover = t.getCoverView();
      if (userNames != null && userNames.containsKey(t.createdby)) {
        tripView.userName = userNames.get(t.createdby);
      }
      tripView.userId = t.createdby;

      String dateFormat = "MMM dd, yyyy";
      SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);

      if (t.starttimestamp > 0) {
        Date d = new Date(t.starttimestamp);
        tripView.tripStartDate = dateFormatter.format(d);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(t.starttimestamp);
        tripView.startYear = String.valueOf(cal.get(Calendar.YEAR));
        tripView.startMonth = String.valueOf(cal.get(Calendar.MONTH));
        tripView.startDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
        tripView.starttimestamp = t.starttimestamp;
        tripView.tripStartDatePrint = Utils.formatTimestamp(t.starttimestamp, "MMM dd");
      }

      if (t.endtimestamp > 0) {
        Date d = new Date(t.endtimestamp);
        tripView.tripEndDate = dateFormatter.format(d);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(t.endtimestamp);
        tripView.endYear = String.valueOf(cal.get(Calendar.YEAR));
        tripView.endMonth = String.valueOf(cal.get(Calendar.MONTH));
        tripView.endDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
        tripView.tripEndDatePrint = Utils.formatTimestamp(t.endtimestamp, "MMM dd");

      }

      Company c = cmpies.get(t.cmpyid);
      if (c != null) {
        tripView.tripAgency = c.getName();
        tripView.tripAgencyId = c.getCmpyid();
      }
      tripViews.add(tripView);
    }
    return tripViews;
  }

  /**
   * Called via Trips->New Trip
   */
  @With({Authenticated.class, Authorized.class})
  public Result newTrip() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    TripInfoView view = new TripInfoView();
    view.accessLevel = SecurityMgr.AccessLevel.OWNER;
    Credentials cred = sessionMgr.getCredentials();
    view.tripAgency = cred.getCmpyName();
    view.tripAgencyId = cred.getCmpyId();
    view.messengerSubscriptionControl = false;
    return ok(views.html.trip.tripInfo.render(view));
  }


  /**
   * Called after clicking "Save" on the Trip Info page aka Step 1
   * POST
   */
  @With({Authenticated.class, Authorized.class})
  public Result createTrip() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<TripInfoForm> form = formFactory.form(TripInfoForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }
    TripInfoForm tripInfo =  form.get();

    Company c = Company.find.byId(tripInfo.getInTripAgency());
    if (c == null) {
      return UserMessage.message(ReturnCode.HTTP_REQ_MISSING_DATA, "Invalid agency - please resubmit");
    }

    if (c.maxtrips != APPConstants.CMPY_MAX_NUM_TRIPS) {
      //check to see if we exceed the limit
      int numOfTrips = Trip.numOfTrips(c.cmpyid);
      if (numOfTrips > c.getMaxtrips()) {
        BaseView baseView = new BaseView();
        baseView.message = "You have exceeded your trial limits - please contact us to extend your demo";
        return ok(views.html.common.message.render(baseView));
      }
    }

    Trip tripModel = null;
    if (tripInfo.getInTripId() == null) {
      if (!SecurityMgr.canCreateTrip(c, sessionMgr)) {
        BaseView baseView = new BaseView();
        baseView.message = "Unauthorized access";
        return ok(views.html.common.message.render(baseView));
      }

      List<Trip> trips = Trip.findPendingByCmpyNameType(tripInfo.getInTripName(),
                                                        tripInfo.getInTripAgency(),
                                                        APPConstants.TOUR_TYPE);
      if (trips != null && trips.size() > 0) {
        BaseView baseView = new BaseView();
        baseView.message = "Duplicate Trip Name - A pending trip with this name already exists.";
        return ok(views.html.common.message.render(baseView));
      }

      tripModel = new Trip();
      tripModel.tripid = Utils.getUniqueId();
      tripModel.name = tripInfo.getInTripName();
      tripModel.comments = tripInfo.getInTripNote();
      tripModel.tag = tripInfo.getInTripTag();
      tripModel.cmpyid = tripInfo.getInTripAgency();
      if (tripInfo.getInStartDate() != null && tripInfo.getInStartDate().length() > 0) {
        tripModel.setStarttimestamp(Utils.getMilliSecs(tripInfo.getInStartDate()));
      } else {
        tripModel.setStarttimestamp(System.currentTimeMillis());
      }
      if (tripInfo.getInEndDate() != null && tripInfo.getInEndDate().length() > 0) {
        tripModel.setEndtimestamp(Utils.getMilliSecs(tripInfo.getInEndDate()));
      } else {
        DateTime dateTime = new DateTime();
        tripModel.setEndtimestamp(dateTime.plusDays(1).getMillis());
      }
      tripModel.status = APPConstants.STATUS_PUBLISH_PENDING;
      tripModel.triptype = APPConstants.TOUR_TYPE;
      tripModel.visibility = APPConstants.PRIVATE_VISIBILITY;

      tripModel.createdtimestamp = System.currentTimeMillis();
      tripModel.lastupdatedtimestamp = System.currentTimeMillis();
      tripModel.createdby = sessionMgr.getUserId();
      tripModel.modifiedby = sessionMgr.getUserId();
      tripModel.save();
      TripPassengerInfoView view = new TripPassengerInfoView();
      view.tripId = tripModel.tripid;
      String dateFormat = "MMM dd, yyyy";
      SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);

      if (tripModel.starttimestamp > 0) {
        Date d = new Date(tripModel.starttimestamp);
        view.tripStartDate = dateFormatter.format(d);
      }

      if (tripModel.endtimestamp > 0) {
        Date d = new Date(tripModel.endtimestamp);
        view.tripEndDate = dateFormatter.format(d);
      }
      view.tripName = tripModel.name;
      flash(SessionConstants.SESSION_PARAM_MSG, "Trip created successfully");
      sessionMgr.setParam(SessionConstants.SESSION_PARAM_TRIPID, tripModel.tripid);

      //Firebase - create new conference
      MessengerUpdateHelper.build(tripModel, sessionMgr.getAccountId())
                           .setAddDefaultRooms(true)
                           .setForcedOverwrite(true) //new trip - forcing
                           .update();

      //Create audit log
      audit:
      {
        TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP, AuditActionType.ADD, AuditActorType.WEB_USER)
                                .withTrip(tripModel)
                                .withCmpyid(tripModel.cmpyid)
                                .withUserid(sessionMgr.getUserId());

        ((AuditTrip) ta.getDetails()).withName(tripModel.getName())
                                     .withId(tripModel.getTripid())
                                     .withStatus(AuditTrip.TripStatus.PENDING);
        ta.save();
      }

      return redirect(routes.TripController.tripInfo(tripModel.tripid));
    }
    else { //Modifying existing trip
      tripModel = Trip.find.byId(tripInfo.getInTripId());

      if (SecurityMgr.getTripAccessLevel(tripModel, sessionMgr).lt(SecurityMgr.AccessLevel.APPEND)) {
        return UserMessage.message(ReturnCode.AUTH_TRIP_FAIL, "Account not authorized to edit trip.");
      }

      tripModel.setName(tripInfo.getInTripName());
      tripModel.setComments(tripInfo.getInTripNote());
      tripModel.setTag(tripInfo.getInTripTag());

      tripModel.setCmpyid(tripInfo.getInTripAgency());
      tripModel.setStarttimestamp(Utils.getMilliSecs(tripInfo.getInStartDate()));
      tripModel.setEndtimestamp(Utils.getMilliSecs(tripInfo.getInEndDate()));
      tripModel.setLastupdatedtimestamp(System.currentTimeMillis());
      tripModel.setModifiedby(sessionMgr.getUserId());
      tripModel.save();

      sessionMgr.setParam(SessionConstants.SESSION_PARAM_TRIPID, tripModel.tripid);

      try {
        boolean updateMessengerUserOnly = false;
        boolean messengerSubscriptionNewState = tripInfo.getInMessengerSubscription() != null &&
                                                tripInfo.getInMessengerSubscription();

        Optional<MessengerConference> omc = MessengerConference.findByPk(tripModel.tripid);
        if (omc.isPresent()) {
          MessengerConference mc = omc.get();

          boolean currState = mc.getUserState(sessionMgr.getUserId());

          if (currState != messengerSubscriptionNewState) {
            updateMessengerUserOnly = true;
          }
        }
        else {
          updateMessengerUserOnly = true;
        }

        if (updateMessengerUserOnly) {
          MessengerUpdateHelper.build(tripModel, sessionMgr.getAccountId())
                               .setAddDefaultRooms(true)
                               .modifyUserTripAccessState(sessionMgr.getUserId(), messengerSubscriptionNewState);
        }
        else {
          //Firebase - update existing conference
          MessengerUpdateHelper.build(tripModel, sessionMgr.getAccountId())
                               .setAddDefaultRooms(true)
                               .update();
        }
      }
      catch (Exception e) {
        Log.err("Cannot create conference for trip: " + tripModel.tripid, e);
      }

      //Create audit log
      audit:
      {
        TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP, AuditActionType.MODIFY, AuditActorType.WEB_USER)
                                .withTrip(tripModel)
                                .withCmpyid(tripModel.cmpyid)
                                .withUserid(sessionMgr.getUserId());

        ((AuditTrip) ta.getDetails()).withName(tripModel.getName())
                                     .withId(tripModel.getTripid())
                                     .withStatus(tripModel.status);
        ta.save();
      }


      return redirect(routes.BookingController.bookings(tripModel.tripid, new TabType.Bound(TabType.ITINERARY), null));
    }
  }

  public TripInfoView tripInfoHelper(Trip tripModel, SessionMgr sessionMgr, SecurityMgr.AccessLevel accessLevel) {

    TripInfoView tripView = new TripInfoView();
    tripView.accessLevel = accessLevel;
    tripView.tripId = tripModel.tripid;
    tripView.cover = tripModel.getCoverView();
    if (tripView.cover != null && tripView.cover.url != null) {
      if (tripView.cover.url.contains("?")) {
        tripView.cover.url += "&v="+ Instant.now().toEpochMilli();
      } else {
        tripView.cover.url += "?v="+ Instant.now().toEpochMilli();
      }
    }
    tripView.tripName = tripModel.name;
    tripView.tripNote = tripModel.comments;
    tripView.tripTag = tripModel.tag;
    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      tripView.message = msg;
    }

    //Block to configure Messenger subscriptions
    if(accessLevel.ge(SecurityMgr.AccessLevel.APPEND)) {
      //Checking if the user has subscribed to the messenger conference related to this trip
      Optional<MessengerConference> omc = MessengerConference.findByPk(tripModel.tripid);
      if (omc.isPresent()) {
        MessengerConference mc      = omc.get();
        Map<String, String> mcu     = mc.getUsers();
        String              boolVal = mcu.get(sessionMgr.getUserId());
        if (boolVal != null) {
          tripView.messengerSubscription = Boolean.valueOf(boolVal);
        }
      }

      //Is trip creator?
      if(sessionMgr.getUserId().equals(tripModel.createdby)) {
        tripView.messengerSubscriptionControl = false; //Trip creator - can't unsubscribe
      } else {
        TripShare ts = TripShare.getSharedTripForUser(tripModel.tripid, sessionMgr.getUserId());
        if(ts != null) {
          tripView.messengerSubscriptionControl = false; //Trip creator - can't unsubscribe
        } else {
          tripView.messengerSubscriptionControl = true; //Anyone else - go ahead - edit
        }
      }
    }

    String           dateFormat    = "MM/dd/yy";
    SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);

    if (tripModel.starttimestamp > 0) {
      Date d = new Date(tripModel.starttimestamp);
      tripView.tripStartDate = dateFormatter.format(d);
      tripView.tripStartDatePrint = Utils.getDateStringPrint(tripModel.starttimestamp);

    }

    if (tripModel.endtimestamp > 0) {
      Date d = new Date(tripModel.endtimestamp);
      tripView.tripEndDate = dateFormatter.format(d);
      tripView.tripEndDatePrint = Utils.getDateStringPrint(tripModel.endtimestamp);

    }

    Company c = Company.find.byId(tripModel.cmpyid);
    if (c != null) {
      tripView.tripAgency = c.getName();
      tripView.tripAgencyId = c.getCmpyid();
    }

    String userCmpyId = sessionMgr.getCredentials().getCmpyId();
    if (userCmpyId != null) {
      tripView.userCmpyId = userCmpyId;

      Company uc = Company.find.byId(userCmpyId);
      tripView.userCmpyName = uc.getName();
      tripView.userCmpyLogo = uc.getLogourl();
    }

    tripView.tripStatus = tripModel.status;
    tripView.hasPublishedGroups = TripPublishHistory.hasPublishedGroups(tripModel.tripid);

    tripView.numberOfBookings = TripDetail.numberOfBookings(tripModel.tripid);
    sessionMgr.setParam(SessionConstants.SESSION_PARAM_TRIPID, tripModel.tripid);

    /* Build new Recent Activity View */
    final long localTimeOffsetMillis = sessionMgr.getTimezoneOffsetMins() * 60 * 1000;
    tripView.rav = AuditController.buildActivityView(tripModel,
        0,
        APPConstants.MAX_AUDIT_RESULT_COUNT,
        localTimeOffsetMillis);

    /* Build new CollaboratorsView if collaboration is allowed for this company */
    if (Company.isCollaborationEnabled(tripModel.cmpyid)) {
      tripView.collaborationEnabled = true;
      tripView.collabView = SharedTripsController.buildCollaboratorsView(tripModel,
          accessLevel,
          sessionMgr.getUserId());
      tripView.collabView.collabEmailLogs = EmailController.buildEmailLogs(tripModel.tripid, EmailLog.EmailTypes.COLLABORATOR_ITINERARY);
    }
    else {
      tripView.collaborationEnabled = false;
    }

    //check to see if there are any entries made by collaborators
    try {
      tripView.hasCollaboratorEntries = TripMgr.hasCollaboratorEntries(tripModel.tripid, tripModel.cmpyid);

      //get passengers and check if they are registered?
      List<AccountTripLink> tripLinks  = AccountTripLink.findByTripNoGroup(tripModel.tripid);
      if (tripLinks != null && tripLinks.size() > 0) {
        List<TripPassengerInfoView> passengerInfoViews = new ArrayList<TripPassengerInfoView>();
        for (AccountTripLink tripLink : tripLinks) {
          Account traveler = Account.find.byId(tripLink.getPk().getUid());
          TripPassengerInfoView passengerInfoView = new TripPassengerInfoView();
          passengerInfoView.tripId = tripModel.tripid;
          passengerInfoView.id = String.valueOf(traveler.getUid());
          passengerInfoView.email = traveler.getEmail();
          passengerInfoView.name = traveler.getFullName();
          passengerInfoView.msisdn = (traveler.getMsisdn() == null) ? "": traveler.getMsisdn();
          passengerInfoView.isUmappedUser = false;
          if (AccountSession.hasSession(traveler.getUid())) {
            passengerInfoView.isUmappedUser = true;
          }
          passengerInfoViews.add(passengerInfoView);
        }
        tripView.passengers = passengerInfoViews;
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    return tripView;
  }


  /**
   * Main trip info view (i.e. "Step 1")
   */
  @With({Authenticated.class, Authorized.class})
  public Result tripInfo(String tripId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());


    if (tripId == null || tripId.trim().length() == 0) {
      tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);
    }

    if (tripId == null || tripId.trim().length() == 0) {
      return UserMessage.message(ReturnCode.HTTP_REQ_MISSING_DATA, "Trip ID is not specified");
    }

    Trip                    tripModel   = Trip.find.byId(tripId);
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);

    if (accessLevel.lt(SecurityMgr.AccessLevel.READ)) {
      if (tripModel != null && tripModel.status == APPConstants.STATUS_DELETED) {
        return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND,
                                   "This trip has been deleted and is no longer available.");
      }
      return UserMessage.message(ReturnCode.AUTH_TRIP_FAIL);
    }

    TripInfoView tripView = tripInfoHelper(tripModel, sessionMgr, accessLevel);
    //check to see if we can autopick photo for this trip


    if (tripModel != null && tripView.numberOfBookings >= 2) {
      String cover = publisherHelper.getCoverPhoto(tripModel);
      if (cover != null) {
        tripView.autoPickPhoto = true;
      }
    }


    return ok(views.html.trip.tripInfo.render(tripView));
  }


  @With({Authenticated.class, Authorized.class})
  public Result autoAddCover(String tripId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Trip tripModel   = Trip.find.byId(tripId);
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);

    if (accessLevel.lt(SecurityMgr.AccessLevel.READ)) {
      if (tripModel != null && tripModel.status == APPConstants.STATUS_DELETED) {
        return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND,
                "This trip has been deleted and is no longer available.");
      }
      return UserMessage.message(ReturnCode.AUTH_TRIP_FAIL);
    }

    String cover = publisherHelper.getCoverPhoto(tripModel);
    if (cover != null) {
      FileImage fileImage = ImageController.downloadAndSaveImage(cover, "cover.jpg", FileSrc.FileSrcType.IMG_VENDOR_WIKIPEDIA,"", sessionMgr.getCredentials().getAccount(), false);
      if (fileImage != null) {
        tripModel.setCoverImage(fileImage);
        tripModel.setLastupdatedtimestamp(Instant.now().toEpochMilli());
        tripModel.setModifiedby(sessionMgr.getUserId());
        tripModel.save();
        return redirect(routes.TripController.tripInfo(tripModel.getTripid()));
      }
    }
    return UserMessage.message(ReturnCode.LOGICAL_ERROR);

  }

  /**
   * Simple trip info view (i.e. "Step 1")
   */
  @With({Authenticated.class, Authorized.class})
  public Result simpleTripInfo(String tripId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    String groupId = flash(SessionConstants.SESSION_PARAM_GROUPID);
    final DynamicForm form = formFactory.form().bindFromRequest();

    if (groupId == null) {
      groupId = form.get("inGroupId");
    }


    if (tripId == null || tripId.trim().length() == 0) {
      tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);
    }

    if (tripId == null || tripId.trim().length() == 0) {
      return UserMessage.message(ReturnCode.HTTP_REQ_MISSING_DATA, "Trip ID is not specified");
    }

    Trip                    tripModel   = Trip.find.byId(tripId);
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);

    if (accessLevel.lt(SecurityMgr.AccessLevel.READ)) {
      if (tripModel != null && tripModel.status == APPConstants.STATUS_DELETED) {
        return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND,
            "This trip has been deleted and is no longer available.");
      }
      return UserMessage.message(ReturnCode.AUTH_TRIP_FAIL);
    }

    TripInfoView tripInfoView = tripInfoHelper(tripModel, sessionMgr, accessLevel);

    TripGroup group = null;
    if (groupId != null) {
      group = TripGroup.findByPK(groupId);
      if (group == null || !group.getTripid().equals(tripModel.tripid)) {
        group = null;
        groupId = null;
      } else {
        tripInfoView.groupId = groupId;
      }
    }

    //look for trip publish status at the group level if not default group
    if (group != null) {
      tripInfoView.tripStatus = group.publishstatus;
      tripInfoView.compositeId = TourController.getCompositePK(tripId, group.groupid);
    } else {
      TripPublishHistory pubHistory = TripPublishHistory.getPublishedDefaultGroup(tripModel.tripid);
      if (pubHistory != null) {
        tripInfoView.tripStatus = pubHistory.getStatus();
      } else {
        tripInfoView.tripStatus = APPConstants.STATUS_PUBLISHED_REVIEW;
      }

      tripInfoView.compositeId = tripId;
    }

    List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
    TripBookingView tbv = WebItineraryController.buildTripBookingView(tripModel, tripDetails, false);
    tripInfoView.isCmpyAdmin = sessionMgr.getCredentials().isCmpyAdmin();

    return ok(views.html.trip.simpleTripInfo.render(tripInfoView, tbv));
  }

  /**
   * POST
   */
  @With({Authenticated.class, Authorized.class})
  public Result previewTrip() {
    try {
      SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      final DynamicForm form = formFactory.form().bindFromRequest();
      String tripId = form.get("tripId");
      if (tripId == null) {
        tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);

      }

      if (tripId != null) {
        Trip trip = Trip.find.byId(tripId);

        SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(trip, sessionMgr);
        if (accessLevel.ge(SecurityMgr.AccessLevel.READ)) {
          sessionMgr.setParam(SessionConstants.SESSION_PARAM_TRIPID, tripId);

          ArrayList<TripBookingDetailView> bookingLocations = new ArrayList<>();

          TripPreviewView previewView = new TripPreviewView();
          previewView.accessLevel = accessLevel;
          TripBookingView view = new TripBookingView();
          view.lang = ctx().lang();
          previewView.tripId = trip.tripid;
          previewView.tripStatus = String.valueOf(trip.status);
          previewView.wasPublished = (TripPublishHistory.countPublished(trip.tripid) > 0); //George: toggle for draft B/G in PDF


          view.tripId = trip.tripid;
          view.tripCmpyId = trip.cmpyid;

          long minStartTimestamp = 0;


          view.tripName = trip.name;
          view.tripStatus = trip.status;

          List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
          view.notes = BookingNoteController.getAllTripNotesView(trip, sessionMgr, true);


          List<TripDestination> tripDestinations = TripDestination.getActiveByTripId(tripId);
          int numAttachments = TripAttachment.activeCountByTripId(tripId);

          if ((tripDetails == null || tripDetails.size() == 0) &&
              (tripDestinations == null || tripDestinations.size() == 0) &&
              numAttachments == 0 && (view.notes == null || view.notes.size() == 0)) {
            BaseView view1 = new BaseView();
            view1.message = "No Bookings/Documents/Attachments added.";
            return ok(views.html.common.message.render(view1));
          }

          if (view.notes != null) {
            for (TripBookingDetailView note: view.notes) {
              if (note.locStartLong != 0.0 && note.locStartLat != 0.0) {
                bookingLocations.add(note);
              }
            }
          }

          //process all the bookings
          if (tripDetails != null && tripDetails.size() > 0) {
            for (TripDetail tripDetail : tripDetails) {
              TripBookingDetailView bookingDetails = BookingController.buildBookingView(tripId, tripDetail, true);
              if (bookingDetails != null) {
                if (bookingDetails.locStartLat != null && bookingDetails.locStartLat != 0.0 &&
                    bookingDetails.locStartLong != null && bookingDetails.locStartLong != 0.0) {
                  bookingLocations.add(bookingDetails);
                }

                if (tripDetail.starttimestamp > 0) {
                  Calendar cal = Calendar.getInstance();
                  cal.setTimeInMillis(tripDetail.starttimestamp);
                  bookingDetails.startYear = String.valueOf(cal.get(Calendar.YEAR));
                  bookingDetails.startMonth = String.valueOf(cal.get(Calendar.MONTH));
                  bookingDetails.startDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));

                  bookingDetails.startHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
                  bookingDetails.startMin = String.valueOf(cal.get(Calendar.MINUTE));

                  if (minStartTimestamp == 0 || minStartTimestamp > tripDetail.starttimestamp) {
                    minStartTimestamp = tripDetail.starttimestamp;
                  }
                }


                if (tripDetail.endtimestamp > 0) {
                  Calendar cal = Calendar.getInstance();
                  cal.setTimeInMillis(tripDetail.endtimestamp);
                  bookingDetails.endYear = String.valueOf(cal.get(Calendar.YEAR));
                  bookingDetails.endMonth = String.valueOf(cal.get(Calendar.MONTH));
                  bookingDetails.endDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
                  bookingDetails.endHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
                  bookingDetails.endMin = String.valueOf(cal.get(Calendar.MINUTE));
                }

                switch (tripDetail.detailtypeid.getRootLevelType()) {
                  case FLIGHT:
                    if (tripDetail.starttimestamp.longValue() <= 0) {
                      BaseView view1 = new BaseView();
                      view1.message = "Flight - " + bookingDetails.flightId + " missing departure time ";
                      return ok(views.html.common.message.render(view1));
                    }
                    else if (bookingDetails.flightId == null || bookingDetails.flightId.trim().length() == 0) {
                      BaseView view1 = new BaseView();
                      view1.message = "Flight - missing flight number";
                      return ok(views.html.common.message.render(view1));
                    }
                    else {
                      view.addBookingDetailView(tripDetail.getDetailtypeid(), bookingDetails);
                    }
                    break;
                  case HOTEL:
                    if (tripDetail.starttimestamp.longValue() <= 0) {
                      BaseView view1 = new BaseView();
                      view1.message = "Hotel - " + bookingDetails.providerName + " missing check-in time ";
                      return ok(views.html.common.message.render(view1));
                    }
                    else if (tripDetail.endtimestamp.longValue() <= 0) {
                      BaseView view1 = new BaseView();
                      view1.message = "Hotel - " + bookingDetails.providerName + " missing check-out time ";
                      return ok(views.html.common.message.render(view1));
                    }
                    else {
                      view.addBookingDetailView(tripDetail.getDetailtypeid(), bookingDetails);
                    }
                    break;
                  case CRUISE:
                    if (tripDetail.starttimestamp.longValue() <= 0) {
                      BaseView view1 = new BaseView();
                      view1.message = "Cruise - " + bookingDetails.cruise.name + " missing departure date and time ";
                      return ok(views.html.common.message.render(view1));
                    }
                    else if (tripDetail.endtimestamp.longValue() <= 0) {
                      BaseView view1 = new BaseView();
                      view1.message = "Cruise - " + bookingDetails.cruise.name + " missing arrival date and time ";
                      return ok(views.html.common.message.render(view1));
                    }
                    else {
                      view.addBookingDetailView(tripDetail.getDetailtypeid(), bookingDetails);
                    }
                    break;
                  case ACTIVITY:
                    if (tripDetail.starttimestamp.longValue() <= 0) {
                      BaseView view1 = new BaseView();
                      view1.message = "Activity - " + bookingDetails.providerName + " missing start date/time ";
                      return ok(views.html.common.message.render(view1));
                    }
                    else {
                      view.addBookingDetailView(tripDetail.getDetailtypeid(), bookingDetails);
                    }
                    break;
                  case TRANSPORT:
                    if (tripDetail.starttimestamp.longValue() <= 0) {
                      BaseView view1 = new BaseView();
                      view1.message = "Transfer - " + bookingDetails.providerName + " missing pick-up date/time ";
                      return ok(views.html.common.message.render(view1));
                    }
                    else {
                      view.addBookingDetailView(tripDetail.getDetailtypeid(), bookingDetails);
                    }
                    break;
                }
              }
            }
            if (!bookingLocations.isEmpty()) {
              previewView.bookingsLocations = bookingLocations;
            }
          }


          DestinationSearchView dests = TripController.getGuides(view);
          previewView.destinations = dests;


          HashMap<Integer, String> destinationTypeDesc = new HashMap<>();
          List<DestinationType> destinationTypes = DestinationType.findActive();
          if (destinationTypes != null) {
            ArrayList<GenericTypeView> destinationTypeList = new ArrayList<>();
            for (DestinationType u : destinationTypes) {
              GenericTypeView typeView = new GenericTypeView();
              typeView.id = String.valueOf(u.getDestinationtypeid());
              typeView.name = u.getName();
              destinationTypeList.add(typeView);
              destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());

            }
            previewView.destinationTypeList = destinationTypeList;
          }

          if (dests != null) {
            for (DestinationView dest : dests.destinationList) {
              if (dest.guides != null) {
                for (DestinationGuideView guide : dest.guides) {
                  if (guide.locLat != null && Float.parseFloat(guide.locLat) != 0 &&
                      guide.locLong != null && Float.parseFloat(guide.locLong) != 0) {
                    guide.destinationName = dest.name;
                    if (previewView.documentLocations == null) {
                      previewView.documentLocations = new ArrayList<>();
                    }
                    previewView.documentLocations.add(guide);
                  }
                }
              }
            }
          }


          previewView.tripAttachments = WebItineraryController.getTripAttachments(trip.tripid,
                                                                                  sessionMgr.getTimezoneOffsetMins());

          //set staring date for calendar
          view.tripStartDatePrint = Utils.getDateStringPrint(trip.starttimestamp);
          view.tripStartDate = String.valueOf(trip.starttimestamp);

          if (minStartTimestamp > 0) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(minStartTimestamp);
            previewView.startYear = String.valueOf(cal.get(Calendar.YEAR));
            previewView.startMonth = String.valueOf(cal.get(Calendar.MONTH) + 1);
            previewView.startDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
            previewView.startHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
            previewView.startMin = String.valueOf(cal.get(Calendar.MINUTE));
          }
          else if (trip.starttimestamp > 0) {


            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(trip.starttimestamp);
            previewView.startYear = String.valueOf(cal.get(Calendar.YEAR));
            previewView.startMonth = String.valueOf(cal.get(Calendar.MONTH) + 1);
            previewView.startDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
            previewView.startHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
            previewView.startMin = String.valueOf(cal.get(Calendar.MINUTE));
          }


          if (trip.endtimestamp > 0) {
            view.tripEndDatePrint = Utils.getDateStringPrint(trip.endtimestamp);
            view.tripEndDate = Utils.getDateString(trip.endtimestamp);

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(trip.endtimestamp);
            previewView.endYear = String.valueOf(cal.get(Calendar.YEAR));
            previewView.endMonth = String.valueOf(cal.get(Calendar.MONTH) + 1);
            previewView.endDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
            previewView.endHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
            previewView.endMin = String.valueOf(cal.get(Calendar.MINUTE));
          }
          String msg = flash(SessionConstants.SESSION_PARAM_MSG);
          previewView.message = msg;
          String activeTab = flash(SessionConstants.SESSION_PARAM_ACTIVE_TAB);
          if (activeTab != null) {
            previewView.activeTab = activeTab;
          }
          else {
            if (tripDetails != null && tripDetails.size() > 0) {
              previewView.activeTab = "Bookings";
            }
            else if (dests != null || (previewView.tripAttachments != null && previewView.tripAttachments.size() > 0)) {
              previewView.activeTab = "Guides";
            }
          }
          previewView.bookings = view;
          //handle cross timezone booking
          if (previewView.bookings != null && previewView.bookings.flights != null && previewView.bookings.flights.size() > 0) {
            previewView.bookings.handleCrosTimezoneFlights();
          }
          return ok(views.html.trip.preview.render(previewView));
        }


      }
    }
    catch (Exception e) {
      e.printStackTrace();
      BaseView view = new BaseView();
      view.message = "System Error: tc726";
      return ok(views.html.common.message.render(view));
    }
    BaseView view = new BaseView();
    view.message = "System Error: tc730";
    return ok(views.html.common.message.render(view));
  }

  /*helper
        Simple logic to automatically include documents based on a simple algorithm
    */
  public static DestinationSearchView getGuides(TripBookingView bookingView) {
    if (bookingView != null) {
      List<TripDestination> tripDests = TripDestination.getAllByTripId(bookingView.tripId);
      List<String> activeDestId = new ArrayList<>();
      List<String> deleteDestId = new ArrayList<>();

      if (tripDests != null) {
        for (TripDestination tripDest : tripDests) {
          if (tripDest.status == APPConstants.STATUS_ACTIVE) {
            activeDestId.add(tripDest.destinationid);
          }
          else {
            deleteDestId.add(tripDest.destinationid);
          }
        }
      }
      //bug - if the same doc is added, deleted and added again, we don't keep it
      for (String id : activeDestId) {
        if (deleteDestId.contains(id)) {
          deleteDestId.remove(id);
        }
      }

      /* removed the automatic logic to try to include docs
      ArrayList<String> countries = new ArrayList<String>();
      ArrayList<String> cities = new ArrayList<String>();
      ArrayList<String> activities = new ArrayList<String>();
      ArrayList<String> tours = new ArrayList<String>();

      TripBookingDetailView origDeparture = null;

      if (bookingView.flights != null) {
          TripBookingDetailView previous = null;

          for (TripBookingDetailView d : bookingView.flights) {
              if (origDeparture == null)
                  origDeparture = d;


              //make sure there is a gap of a least 1 day between flights and don't include cities
              if  (previous != null && (d.startDateMs - previous.endDateMs) > (24 * 60 * 60 * 1000)) {
                  if (previous.city != null &&  previous.city.trim().length() > 0 && !previous.city.trim()
                  .equalsIgnoreCase(origDeparture.departCity.trim()) && !cities.contains(previous.city.trim()
                  .toUpperCase()))
                      cities.add(previous.city.trim().toUpperCase());

                  if (previous.country != null && previous.country.trim().length() > 0 && !previous.country
                  .trim().equalsIgnoreCase(origDeparture.departCountry.trim()) && !countries.contains(previous
                  .country.trim().toUpperCase()))
                      countries.add(previous.country.trim().toUpperCase());
              }

              previous = d;
          }
          //make sure there is a gap of a least 1 day between flights and don't include cities
          if  (origDeparture != null && previous != null && previous.city != null && origDeparture.city != null
           && cities != null && previous.city.trim().length() > 0 && !previous.city.trim().equalsIgnoreCase
           (origDeparture.departCity.trim()) && !cities.contains(previous.city.trim().toUpperCase())) {
             cities.add(previous.city.trim().toUpperCase());
          }

          if (origDeparture != null && previous.country != null && origDeparture.country != null && countries
          != null && previous.country.trim().length() > 0 && !previous.country.trim().equalsIgnoreCase
          (origDeparture.departCountry.trim()) && !countries.contains(previous.country.trim().toUpperCase())) {
             countries.add(previous.country.trim().toUpperCase());
          }
      }

      if (bookingView.hotels != null) {
          for (TripBookingDetailView d : bookingView.hotels) {
              if (origDeparture != null && cities != null && origDeparture.departCity != null && d.city != null
               && d.city.trim().length() > 0 && origDeparture.departCity != null  && !d.city.trim()
               .equalsIgnoreCase(origDeparture.departCity.trim()) && !cities.contains(d.city.trim().toUpperCase
               ()))
                  cities.add(d.city.trim().toUpperCase());

              if (origDeparture != null && d.country != null && origDeparture.departCountry != null &&
              countries != null &&  d.country.trim().length() > 0 && origDeparture.departCountry != null  && !d
              .country.trim().equalsIgnoreCase(origDeparture.departCountry.trim()) && !countries.contains(d
              .country.trim().toUpperCase()))
                  countries.add(d.country.trim().toUpperCase());
          }
      }
      */


      //get destinations
      try {
        List<Destination> destList = new ArrayList<>();

        //TOTO optimize SQL
        /*
        if (countries != null && !countries.isEmpty()) {
            for (String country : countries) {
                List<Destination> countryDestList = Destination.findActiveDestByTypeCityCmpy(bookingView
                .tripCmpyId, APPConstants.COUNTRY_DESTINATION_TYPE, null, country);
                if (countryDestList != null) {
                    destList.addAll(countryDestList);
                }
            }
        }

        if (cities != null && !cities.isEmpty()) {
            for (String city : cities) {
                if (city.trim().length() > 0) {
                    List<Destination> cityDestList = Destination.findActiveDestByTypeCityCmpy(bookingView
                    .tripCmpyId, APPConstants.CITY_DESTINATION_TYPE, city, null);
                    if (cityDestList != null) {
                        destList.addAll(cityDestList);
                    }
                }
            }
        }
        */

        for (TripBookingDetailView d : bookingView.getSubtreeBookings(ReservationType.ACTIVITY)) {
          if (d.destinationId != null) {
            Destination dest = Destination.find.byId(d.destinationId);
            if (dest != null) {
              destList.add(dest);
            }
          }
        }

        if (destList != null) {
          //check to see if we need to add any manually added guides
          for (Destination dest : destList) {
            int i = activeDestId.indexOf(dest.destinationid);
            if (i >= 0) {
              activeDestId.remove(i);
            }
          }

          if (!activeDestId.isEmpty()) {
            List<Destination> manualDest = Destination.findActiveDestByIds(activeDestId);
            if (manualDest != null) {
              destList.addAll(manualDest);
            }
          }

          HashMap<Integer, String> destinationTypeDesc = new HashMap<>();
          List<DestinationType> destinationTypes = DestinationType.findActive();
          if (destinationTypes != null) {
            for (DestinationType u : destinationTypes) {
              destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());
            }
          }

          DestinationSearchView view = new DestinationSearchView();
          ArrayList<String> addedGuides = new ArrayList<>();
          List<DestinationView> destViews = new ArrayList<>();
          for (Destination dest : destList) {
            if (!deleteDestId.contains(dest.destinationid) && !addedGuides.contains(dest.destinationid)) {
              DestinationView destView = DestinationController.getDestView(dest.destinationid, destinationTypeDesc);

              destView.isTourGuide = false;
              //get covers
              if (dest.hasCover()) {
                destView.coverUrl = dest.getCoverurl();
                destView.cover = dest.getCoverView();
                if (destView.cover.url.contains("?")) {
                  destView.cover.url += "&v="+ Instant.now().toEpochMilli();
                } else {
                  destView.cover.url += "?v="+ Instant.now().toEpochMilli();
                }
              }

              if (dest.loclat != 0 && dest.loclong != 0) {
                view.displayMap = true;
              }

              if (dest.destinationtypeid == APPConstants.TRIP_DOC_DESTINATION_TYPE || dest.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE) {
                destViews.add(0, destView);
              }
              else {
                destViews.add(destView);
              }
              addedGuides.add(destView.id);
            }
          }
          view.destinationList = destViews;
          return view;
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    return null;
  }

  /**
   * Simple logic to automatically include documents based on a simple algorithm
   */
  public static DestinationSearchView getGuidesImproved(String tripId) {

    DestinationSearchView view = new DestinationSearchView();
    view.destinationList = new ArrayList<>();

    if(tripId == null) {
      return view;
    }

    List<Destination> allDestinations = Destination.getAllActivelyLinkedForTrip(tripId);

    //Nothing is linked - bye
    if(allDestinations == null && allDestinations.size() == 0) {
      return view;
    }

    HashMap<Integer, String> destinationTypeDesc = new HashMap<>();
    List<DestinationType> destinationTypes = DestinationType.findActive();
    if (destinationTypes != null) {
      for (DestinationType u : destinationTypes) {
        destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());
      }
    }

    for (Destination dest : allDestinations) {
      DestinationView destView = DestinationController.getDestView(dest, destinationTypeDesc);

      destView.isTourGuide = false;

      if (dest.hasCover()) {
        destView.coverUrl = dest.getCoverurl();
        destView.cover = dest.getCoverView();
        if (destView.cover.url.contains("?")) {
          destView.cover.url += "&v="+ Instant.now().toEpochMilli();
        } else {
          destView.cover.url += "?v="+ Instant.now().toEpochMilli();
        }
      }

      if (dest.loclat != 0 && dest.loclong != 0) {
        view.displayMap = true;
      }

      if (dest.destinationtypeid == APPConstants.TRIP_DOC_DESTINATION_TYPE ||
          dest.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE) {
        view.destinationList.add(0, destView);
      }
      else {
        view.destinationList.add(destView);
      }
    }
    return view;
  }

  @With({Authenticated.class, Authorized.class})
  public Result deleteTripGuide() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<TripGuideForm> form = formFactory.form(TripGuideForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    TripGuideForm tripInfo = form.get();
    Trip tripModel = Trip.findByPK(tripInfo.getInTripId());
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);

    try {
      if (accessLevel.ge(SecurityMgr.AccessLevel.APPEND) && tripInfo.getInDestId() != null) {
        Destination dest = Destination.find.byId(tripInfo.getInDestId());
        if (dest != null && dest.status == APPConstants.STATUS_ACTIVE) {

          //If allowed only to append, then check that guide was created by the user
          if (accessLevel == SecurityMgr.AccessLevel.APPEND && !dest.getCreatedby().equals(sessionMgr.getUserId())) {
            BaseView baseView = new BaseView();
            baseView.message = "System Error - Can not delete guides created by other users";
            return ok(views.html.common.message.render(baseView));
          }

          TripDestination tripDest = TripDestination.find.byId(Utils.hash(tripInfo.getInDestId() + tripInfo.getInTripId()));
          if (tripDest == null) {
            tripDest = new TripDestination();
            tripDest.setTripdestid(Utils.hash(tripInfo.getInDestId() + tripInfo.getInTripId()));
            tripDest.setDestinationid(tripInfo.getInDestId());
            tripDest.setTripid(tripInfo.getInTripId());
            tripDest.setCreatedtimestamp(System.currentTimeMillis());
            tripDest.setCreatedby(sessionMgr.getUserId());
            if (dest.name != null) {
              tripDest.setName(dest.name);
            }
            else {
              tripDest.setName("");
            }
            tripDest.rank = 0;
          }

          tripDest.setStatus(APPConstants.STATUS_DELETED);
          tripDest.setModifiedby(sessionMgr.getUserId());
          tripDest.setLastupdatedtimestamp(System.currentTimeMillis());
          tripDest.save();

          //Create audit log
          audit:
          {
            TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_DOC_GUIDE,
                                                 AuditActionType.DELETE,
                                                 AuditActorType.WEB_USER)
                                    .withTrip(tripModel)
                                    .withCmpyid(tripModel.cmpyid)
                                    .withUserid(sessionMgr.getUserId());

            ((AuditTripDocGuide) ta.getDetails()).withGuideId(dest.getDestinationid()).withName(dest.getName());
            ta.save();
          }

          flash(SessionConstants.SESSION_PARAM_MSG, "Document - " + dest.name + " - has been deleted.");
        }
      }
    }
    catch (Exception e) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));

    }

    return redirect(routes.TripController.guides(tripModel.tripid, new DestinationSearchView.Tab.Bound(DestinationSearchView.Tab.ATTACHMENTS)));

  }

  //post
  @With({Authenticated.class, Authorized.class})
  public Result guides(String tripId, DestinationSearchView.Tab.Bound selectedTab) {
    try {
      SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      if (tripId != null) {
        Trip trip = Trip.find.byId(tripId);
        Credentials creds = sessionMgr.getCredentials();
        SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(trip, sessionMgr);
        if (accessLevel.ge(SecurityMgr.AccessLevel.READ)) {
          DestinationSearchView view = TripController.getGuides(tripId);
          view.addCapabilities(creds.getCapabilities());
          view.accessLevel = accessLevel;
          view.tripId = trip.tripid;
          view.tripInfo = new TripInfoView();
          view.tripInfo.tripId = trip.tripid;
          view.tripInfo.tripAgencyId = trip.cmpyid;
          view.tripInfo.userId = trip.createdby;
          view.cmpyId = trip.cmpyid;
          view.loggedInUserId = sessionMgr.getUserId();
          view.isAdminOfACompany = sessionMgr.getCredentials().isCmpyAdmin(trip.cmpyid);

          if (trip.starttimestamp > 0) {
            view.tripInfo.tripStartDatePrint = Utils.getDateStringPrint(trip.starttimestamp);
            view.tripInfo.tripStartDate = String.valueOf(trip.starttimestamp);
          }

          if (trip.endtimestamp > 0) {
            view.tripInfo.tripEndDatePrint = Utils.getDateStringPrint(trip.endtimestamp);
            view.tripInfo.tripEndDate = Utils.getDateString(trip.endtimestamp);
          }
          view.tripInfo.tripName = trip.name;
          view.tripInfo.tripStatus = trip.status;

          HashMap<Integer, String> destinationTypeDesc = new HashMap<>();
          List<DestinationType> destinationTypes = DestinationType.findActive();
          if (destinationTypes != null) {
            ArrayList<GenericTypeView> destinationTypeList = new ArrayList<>();
            for (DestinationType u : destinationTypes) {
              GenericTypeView typeView = new GenericTypeView();
              typeView.id = String.valueOf(u.getDestinationtypeid());
              typeView.name = u.getName();
              destinationTypeList.add(typeView);
              destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());

            }
            view.destinationTypeList = destinationTypeList;
          }

          String msg = flash(SessionConstants.SESSION_PARAM_MSG);
          view.message = msg;

          view.selectedTab = DestinationSearchView.Tab.CUSTOM_DOC;
          if (selectedTab != null) {
            view.selectedTab = selectedTab.value();
          }

          //get trip attachments
          List<TripAttachment> attachments = TripAttachment.findByTripId(trip.tripid);
          if (attachments != null && attachments.size() > 0) {
            view.tripAttachments = new ArrayList<>();
            for (TripAttachment attach : attachments) {
              AttachmentView attachView = new AttachmentView();
              attachView.createdById = attach.createdby;
              if(attach.getName() != null && attach.getName().length() > 0) {
                attachView.attachName = attach.getName();
              } else {
                attachView.attachName = attach.getOrigfilename();
              }
              attachView.attachUrl = attach.fileurl;
              attachView.id = attach.pk;
              if(attach.getDescription() != null && attach.getDescription().length() > 0) {
                attachView.comments = attach.getDescription();
              }

              //adjust for local timezone
              long localTimestamp = attach.createdtimestamp + (sessionMgr.getTimezoneOffsetMins() * 60 * 1000);
              attachView.lastUpdateTimestampPrint = Utils.formatDateTimePrint(localTimestamp);
              if (attach.filename != null && (attach.filename.toLowerCase().endsWith("doc") || attach.filename
                  .toLowerCase()
                  .endsWith("docx"))) {
                attachView.attachType = "doc";
              }
              else if (attach.filename != null && attach.filename.toLowerCase().endsWith("pdf")) {
                attachView.attachType = "pdf";
              }

              view.tripAttachments.add(attachView);
            }
          }

          //get list of Travel 42 reports if available
          Map<String, String> travel42Reports = (Map<String, String>) CacheMgr.get(APPConstants.CACHE_T42_REPORT_USER
                                                                                   + sessionMgr.getUserId());
          if (travel42Reports != null && travel42Reports.size() > 0 && view.tripDestinationView != null) {
            //find the custom trip view and set the report list
            view.tripDestinationView.travel42Reports = travel42Reports;
          }

          return ok(views.html.trip.guides.render(view));
        }
        else {
          BaseView view1 = new BaseView();
          view1.message = "Unauthorized access -";
          return ok(views.html.common.message.render(view1));
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      BaseView view = new BaseView();
      view.message = "System Error: tc1159";
      return ok(views.html.common.message.render(view));
    }
    BaseView view = new BaseView();
    view.message = "System Error: tc1163";
    return ok(views.html.common.message.render(view));
  }

  public static DestinationSearchView getGuides(String tripId) {
    if (tripId != null) {
      List<TripDestination> tripDests = TripDestination.getActiveByTripId(tripId);
      List<String> activeDestId = new ArrayList<>();
      /* Need to have a map of creators to allow set proper trip deletion rules */
      Map<String, String> destinationLinkCreatorors = new HashMap<>();
      String tripDestinationId = null;
      boolean tripDestinationMergeBookings = false;
      if (tripDests != null) {
        for (TripDestination tripDest : tripDests) {
          activeDestId.add(tripDest.destinationid);
          destinationLinkCreatorors.put(tripDest.destinationid, tripDest.createdby);
          // trip document - trip destination id is equals to the trip id.
          if (tripDest.tripdestid.equals(tripId)) {
            tripDestinationId = tripDest.destinationid;
            tripDestinationMergeBookings = tripDest.isMergebookings();
          }
        }
        List<Destination> destList = Destination.findActiveDestByIds(activeDestId);
        HashMap<Integer, String> destinationTypeDesc = new HashMap<>();
        List<DestinationType> destinationTypes = DestinationType.findActive();
        if (destinationTypes != null) {
          for (DestinationType u : destinationTypes) {
            destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());
          }
        }
        DestinationSearchView view = new DestinationSearchView();
        ArrayList<String> addedGuides = new ArrayList<>();
        List<DestinationView> destViews = new ArrayList<>();
        for (Destination dest : destList) {
          if (!addedGuides.contains(dest.destinationid)) {

            DestinationView destView = new DestinationView();
            destView.id = dest.destinationid;
            destView.createdById = destinationLinkCreatorors.get(destView.id);
            destView.name = dest.name;
            destView.destinationType = String.valueOf(dest.destinationtypeid);
            destView.destinationTypeDesc = destinationTypeDesc.get(dest.destinationtypeid);
            destView.tag = dest.tag;
            destView.cmpyId = dest.cmpyid;
            destView.city = dest.city;
            destView.country = dest.country;
            destView.intro = dest.intro;
            destView.locLat = String.valueOf(dest.loclat);
            destView.locLong = String.valueOf(dest.loclong);
            destView.isTourGuide = false;

            for (SysContentSite s : MobilizerController.getContentSites()) {
              destView.addMobilizedDomain(s.getDomain(), s.getName());
            }

            //get covers
            if (dest.hasCover()) {
              destView.coverUrl = dest.getCoverurl();
              destView.cover = dest.getCoverView();
              if (destView.cover.url.contains("?")) {
                destView.cover.url += "&v="+ Instant.now().toEpochMilli();
              } else {
                destView.cover.url += "?v="+ Instant.now().toEpochMilli();
              }
            }

            if (dest.loclat != 0 && dest.loclong != 0) {
              view.displayMap = true;
            }

            if (tripDestinationId != null && dest.getDestinationid().equals(tripDestinationId)) {
              //check for pages
              List<DestinationGuide> guides = DestinationGuide.findActiveDestGuides(dest.destinationid);
              if (guides != null) {
                List<DestinationGuideView> guideViews = new ArrayList<>();
                for (DestinationGuide destGuide : guides) {
                  DestinationGuideView guideView = new DestinationGuideView();
                  guideView.createdById = destGuide.createdby;
                  guideView.destinationId = destGuide.destinationid;
                  guideView.id = destGuide.destinationguideid;
                  guideView.city = destGuide.city;
                  guideView.country = destGuide.country;
                  guideView.description = destGuide.description;
                  guideView.intro = destGuide.intro;
                  guideView.landmark = destGuide.landmark;
                  guideView.name = destGuide.name;
                  guideView.locLat = String.valueOf(destGuide.loclat);
                  guideView.locLong = String.valueOf(destGuide.loclong);
                  guideView.tag = destGuide.tag;
                  guideView.zipCode = destGuide.zipcode;
                  if (destGuide.loclat != 0 && destGuide.loclong != 0) {
                    view.displayMap = true;
                  }
                  guideView.rank = destGuide.rank;
                  guideView.streetAddr1 = destGuide.streetaddr1;
                  //get cover if any
                  //get any attachments
                  List<DestinationGuideAttach> covers = DestinationGuideAttach.getCover(destGuide.destinationguideid);
                  if (covers != null && covers.size() == 1) {

                    guideView.coverName = covers.get(0).getName();
                    guideView.coverUrl = covers.get(0).getAttachurl();
                  }
                  if (destGuide.getDestguidetimestamp() != null && destGuide.getDestguidetimestamp() > 0) {
                    guideView.setupTimestamp(destGuide.getDestguidetimestamp());

                  }
                  guideViews.add(guideView);
                }

                destView.guides = DestinationController.groupByDate(guideViews);
              }

              //set merge the trip bookings flag
              destView.mergeBookings = tripDestinationMergeBookings;

              view.tripDestinationView = destView;
              //get the rest of the pages
            }
            else {
              if (dest.destinationtypeid == APPConstants.TRIP_DOC_DESTINATION_TYPE || dest.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE) {
                destViews.add(0, destView);
              }
              else {
                destViews.add(destView);
              }

              addedGuides.add(destView.id);
            }
          }
        }
        view.destinationList = destViews;
        return view;

      }
    }
    return null;
  }

  @With({Authenticated.class, Authorized.class})
  public Result addTripGuide() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<TripGuideForm> form = formFactory.form(TripGuideForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    TripGuideForm tripInfo = form.get();
    Trip tripModel = Trip.findByPK(tripInfo.getInTripId());

    try {
      if (SecurityMgr.getTripAccessLevel(tripModel, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND) && tripInfo.getInDestId() != null) {
        Destination dest = Destination.find.byId(tripInfo.getInDestId());
        if (dest != null && dest.status == APPConstants.STATUS_ACTIVE) {
          TripDestination tripDest = TripDestination.find.byId(Utils.hash(tripInfo.getInDestId() + tripInfo.getInTripId()));
          if (tripDest == null) {
            tripDest = new TripDestination();
            tripDest.setTripdestid(Utils.hash(tripInfo.getInDestId() + tripInfo.getInTripId()));
            tripDest.setDestinationid(tripInfo.getInDestId());
            tripDest.setTripid(tripInfo.getInTripId());
            tripDest.setCreatedtimestamp(System.currentTimeMillis());
            tripDest.setCreatedby(sessionMgr.getUserId());

            if (dest.name != null) {
              tripDest.setName(dest.name);
            }
            else {
              tripDest.setName("");
            }
            tripDest.rank = 0;
          }

          tripDest.setStatus(APPConstants.STATUS_ACTIVE);
          tripDest.setModifiedby(sessionMgr.getUserId());
          tripDest.setLastupdatedtimestamp(System.currentTimeMillis());
          tripDest.save();

          //Create audit log
          audit:
          {
            TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_DOC_GUIDE,
                                                 AuditActionType.ADD,
                                                 AuditActorType.WEB_USER)
                                    .withTrip(tripModel)
                                    .withCmpyid(tripModel.cmpyid)
                                    .withUserid(sessionMgr.getUserId());

            ((AuditTripDocGuide) ta.getDetails()).withGuideId(dest.getDestinationid()).withName(dest.getName());
            ta.save();
          }

          flash(SessionConstants.SESSION_PARAM_MSG, "Document - " + dest.name + " - has been added.");
        }
      }
    }
    catch (Exception e) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));

    }

    return redirect(routes.TripController.guides(tripModel.tripid, new DestinationSearchView.Tab.Bound(DestinationSearchView.Tab.ATTACHMENTS)));
  }

  public Result previewPassengerEmail(String tripId, String travelerUid) {
    SessionMgr sessionMgr = new SessionMgr(session());
    Form<PreviewEmailForm> publishForm = formFactory.form(PreviewEmailForm.class);
    PreviewEmailForm publishInfo = publishForm.bindFromRequest().get();
    TripGroup group = null;

    Trip trip = Trip.find.byId(tripId);
    if (trip != null && travelerUid != null && StringUtils.isNumeric(travelerUid)) {
      AccountTripLink tripLink = AccountTripLink.findByUid(Long.parseLong(travelerUid), trip.getTripid());
      if (tripLink != null) {
        Account traveler = Account.find.byId(tripLink.getPk().getUid());
        if (publishInfo.getInGroupId() != null && publishInfo.getInGroupId().trim().length() > 0) {
          group = TripGroup.findByPK(publishInfo.getInGroupId());
        }

        UserProfile up = null;
        TripPublishHistory tp = TripPublishHistory.getLastPublished(trip.tripid, null);
        if (tp != null) {
          up = UserProfile.findByPK(tp.createdby);
        }
        else {
          up = UserProfile.findByPK(sessionMgr.getUserId());
        }

        EmailView view = buildEmailView(trip, up, null, group, traveler.getUid());
        //check to see if the trip has been published
        if (TripPublishHistory.countPublished(trip.tripid) == 0) {
          view.published = false;
        }
        //TODO remove hardcoded hack for portuguese language
        //should be a trip driven property
        if (view.businessCard.companyName != null &&
            (view.businessCard.companyName.toLowerCase().contains("atlantico") ||
             view.businessCard.companyName.toLowerCase().contains("atlântico") ||
             view.businessCard.companyName.toLowerCase().contains("agaxtur"))) {
          ctx().changeLang("pt"); //TODO: MIG24 Verify correct Switch of the language
          view.tripInfo.tripStartDatePrint = Utils.formatTimestamp(trip.starttimestamp, "EEE MMM dd, yyy", "pt");
          view.tripInfo.tripEndDatePrint = Utils.formatTimestamp(trip.endtimestamp, "EEE MMM dd, yyy", "pt");
        } else {
          if(publishInfo.getInLang() != null && publishInfo.getInLang().trim().length() > 0 &&
                  (publishInfo.getInLang().equals("es") || publishInfo.getInLang().equals("pt") || publishInfo.getInLang().equals("fr"))){
            ctx().changeLang(publishInfo.getInLang());
          } else {
            ctx().changeLang("en");
          }
        }
        try {
          if (!cmpIdsForDefaultEmailTemplate.contains(trip.getCmpyid())) {
            java.lang.reflect.Method render = cachedCustomEmailTemplates.get(trip.getCmpyid());
            if (render == null) {
              render = getCustomEmailTemplate(trip, CmpyCustomTemplate.TEMPLATE_TYPE.EMAIL_PASSENGER, cachedCustomEmailTemplates);
            }
            if (render != null) {
              Html html = (Html) render.invoke(null, view);
              if (html != null) {
                return ok(html);

              }
            } else {
              //let's check the other template
              render = cachedCustomFullEmailTemplates.get(trip.getCmpyid());
              if (render == null) {
                render = getCustomEmailTemplate(trip, CmpyCustomTemplate.TEMPLATE_TYPE.FULL_EMAIL_PASSENGER, cachedCustomFullEmailTemplates);
              }
              if (render != null) {
                //let's render the full email
                TripPreviewView previewView = WebItineraryController.getFullEmailView(trip);
                play.api.i18n.Lang lang = new play.api.i18n.Lang("en", "US");
                Html html = (Html) render.invoke(null, previewView, lang.code(), false);
                if (html != null) {
                  return ok(html);
                }
              }
            }
          }
        }
        catch (Exception e) {
          e.printStackTrace();
        }

        if (!cmpIdsForDefaultEmailTemplate.contains(trip.getCmpyid())) {
          cmpIdsForDefaultEmailTemplate.add(trip.getCmpyid());
        }

        return ok(views.html.email.trip.render(view));
      }
    }

    return ok("Cannot preview email - please contact support@umapped.com");
  }

  @With({Authenticated.class, Authorized.class})
  public Result publishReviewTrip() {
    try {

      SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      String            comments    = "";
      Form<PublishForm> publishForm = formFactory.form(PublishForm.class);
      PublishForm       publishInfo = publishForm.bindFromRequest().get();
      comments = publishInfo.getInComments();
      String    tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);
      TripGroup group  = null;
      if (tripId == null) {
        tripId = publishInfo.getInTripId();
      }
      boolean published   = false;
      boolean republished = false;

      if (publishInfo.getInPublish() != null && publishInfo.getInPublish().equalsIgnoreCase("Y")) {
        published = true;
      }

      boolean selfRegistration = false;
      if (publishInfo.getInAllowSelfRegistration() != null && publishInfo.getInAllowSelfRegistration().equals("Y")) {
        selfRegistration = true;
      }

      if (tripId != null) {
        Trip trip = Trip.find.byId(tripId);
        if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND_N_PUBLISH)) {
          long endtimestamp = trip.endtimestamp + (23 * 60 * 60 * 1000) + (59 * 60 * 1000) + (59 * 1000);

          if (endtimestamp < (System.currentTimeMillis() - (8 * 60 * 60 * 1000))) {   //fudge the validation by 8
            // hours to account for west coast time
            BaseView view = new BaseView();
            view.message = "The trip end date must be greater than today. Please correct the date and republish.";
            return ok(views.html.common.message.render(view));
          }
          String mapPk = trip.tripid;
          //check for group
          if (publishInfo.getInGroupId() != null && publishInfo.getInGroupId().trim().length() > 0) {
            group = TripGroup.findByPK(publishInfo.getInGroupId());
            if (group == null || !group.getTripid().equals(tripId)) {
              group = null;
            }
            else {
              mapPk = TourController.getCompositePK(tripId, group.groupid);
              if (group.publishstatus == APPConstants.STATUS_PUBLISHED) {
                //once a trip is published - all access should remain
                published = true;
                republished = true;
              }
            }

          }
          else {
            if (trip.status == APPConstants.STATUS_PUBLISHED) {
              //once a trip is published - all access should remain
              published = true;
              republished = true;
            }
          }


          if (trip.status == APPConstants.STATUS_PENDING && TripPublishHistory.countPublished(trip.getTripid()) == 0) {
            //if the agent needs to pay in order to publish, check to see if this trip has already been charged
            //the agent that creates the trip is billed if the agent is per trip billing
            boolean payg = false;
            Account billedAccount = null;
            if (trip.getCreatedby().equals(sessionMgr.getUserId())) {
              payg = sessionMgr.getCredentials().isPerTripBilling();
              billedAccount = sessionMgr.getCredentials().getAccount();
            } else {
              //this trip was created by someone else... le't see if that person was PAYG
              //check if this is pay a fyou go that requires a credit card
              BillingSettingsForUser billing     = BillingSettingsForUser.findSettingsByUserId(trip.getCreatedByLegacy());
              if (billing != null) {
                BillingPlan bp = billing.getPlan();
                if (bp == null && billing.getAgent() != null && !billing.getAgent().isEmpty()) {
                  BillingSettingsForUser asu = BillingSettingsForUser.findSettingsByUserId(billing.getAgent());
                  bp = asu.getPlan();
                }
                if (bp != null && bp.getSchedule() == BillingSchedule.Type.TRIP && bp.getBillingType() == BillingPlan.BillType.CC) {
                  payg = true;
                  billedAccount = Account.findActiveByLegacyId(trip.getCreatedByLegacy());
                }
              }
            }

            if (payg && billedAccount != null) {
              BillingApiController.TxnStatus txnStatus = BillingApiController.chargeTrip(trip.getTripid(),
                                                                                         billedAccount,
                                                                                         braintreeFactory);
              BaseView view = new BaseView();

              switch (txnStatus) {
                case SUCCESS:
                  break;
                case NO_CARD:
                  view.message = "You do not have a valid credit card set up - please review your Credit Card information";
                  return ok(views.html.common.message.render(view));
                case CARD_DECLINED:
                  view.message = "Your credit payment failed - please review your Credit Card information";
                  return ok(views.html.common.message.render(view));
                case CARD_EXPIRED:
                  view.message = "You do not have a valid credit card set up - please review your Credit Card information";
                  return ok(views.html.common.message.render(view));
                default:
                  view.message = "Your credit payment failed - please review your Credit Card information";
                  return ok(views.html.common.message.render(view));
              }
            }
          }

          AgentView   agentView = new AgentView();
          UserProfile up        = UserProfile.find.byId(sessionMgr.getUserId());
          up.setEmail(TripBrandingMgr.getWhieLabelEmail(trip,up.getEmail())); //allow overwrite of email domain for ski.com);
          Company     cmpy      = Company.find.byId(trip.cmpyid);
          agentView.agentName = up.firstname + " " + up.lastname;

          //call common publish routine
          try {
            TripPublisherHelper.publishTrip(trip,
                                            up,
                                            mapPk,
                                            agentView,
                                            cmpy,
                                            published,
                                            republished,
                                            publishInfo.getInSkipTravellerEmail(),
                                            selfRegistration,
                                            comments,
                                            group,
                                            ctx().lang(),
                                            false);
            if (published) {
              flash(SessionConstants.SESSION_PARAM_MSG, "Trip published");
            }
            else {
              flash(SessionConstants.SESSION_PARAM_MSG, "Trip sent for review");
            }

            if (group != null) {
              flash(SessionConstants.SESSION_PARAM_GROUPID, group.groupid);
            }
            if (publishInfo.getInViewType() != null && publishInfo.getInViewType().equals("SimpleTripInfo")) {
              return redirect(routes.TourController.simpleReviewTour());
            } else {
              return redirect("/tours/newTour/publish");// TourController.reviewTour();
            }
          } catch (Exception e) {
            BaseView view = new BaseView();
            view.message = "System Error: tc3135";
            e.printStackTrace();
            return ok(views.html.common.message.render(view));
          }
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    BaseView view = new BaseView();
    view.message = "System Error: tc3146";
    return ok(views.html.common.message.render(view));
  }



  public static int sendUMappedEmailNotitication(List<Account> travelers,
                                                 Trip trip,
                                                 UserProfile up,
                                                 String comments,
                                                 TripGroup group) {
    if (travelers == null || travelers.size() == 0 || trip == null || up == null) {
      return -1;
    }
    int rc = 0;

    List<TripPassengerView> tripPassengers = new ArrayList<>();
    for (Account traveler : travelers) {
      TripPassengerView pView = new TripPassengerView();
      if (traveler.getFullName() != null && traveler.getFullName().length() > 0) {
        pView.name = traveler.getFullName();
      }

      if (traveler.getEmail() != null && Utils.isValidEmailAddress(traveler.getEmail())) {
        pView.email = traveler.getEmail();
      }
      tripPassengers.add(pView);
    }

    StringBuilder notificationMsg = new StringBuilder();
    String notificationTitle = "Your itinerary is ready.";
    notificationMsg.append(trip.getName());
    if (comments != null) {
      notificationMsg.append("\n");
      notificationMsg.append(comments);
    }
    notificationMsg.append("\n From ");
    notificationMsg.append(up.getFullName());


    for(Account traveler: travelers) {
      if (traveler.getEmail() != null && Utils.isValidEmailAddress(traveler.getEmail())) {
        AccountTripLink tripLink = AccountTripLink.findByUid(traveler.getUid(), trip.getTripid());
        rc = sendUmappedEmailNotification(traveler, tripLink, trip, up, comments, group, tripPassengers);
        //try to send mobile notification
        final MobilePushNotificationActor.Command cmd2 = new MobilePushNotificationActor.Command(traveler.getEmail(), notificationTitle, notificationMsg.toString(), DBConnectionMgr.getUniqueLongId());
        ActorsHelper.tell(SupervisorActor.UmappedActor.MOBILE_PUSH_NOTIFY, cmd2);
      }
    }
    return rc;
  }

  public static int sendCCUMappedEmailNotitication(String name, String email,
                                                 Trip trip,
                                                 UserProfile up,
                                                 String comments) {
    if (name == null || name.isEmpty() || trip == null || up == null) {
      return -1;
    }
    int rc = 0;

    List<TripPassengerView> tripPassengers = new ArrayList<>();
    TripPassengerView pView = new TripPassengerView();
    pView.name = name;
    pView.email = email;
    tripPassengers.add(pView);

    if (email != null && Utils.isValidEmailAddress(email)) {
      rc = sendUmappedEmailNotification(null, null, trip, up, comments, null, tripPassengers);

    }
    return rc;
  }

  public static int sendUMappedAgentEmailNotitication(List<TripAgent> agents,
                                                      Trip trip,
                                                      UserProfile up,
                                                      String comments,
                                                      TripGroup group) {
    if (agents == null || agents.size() == 0 || trip == null || up == null) {
      return -1;
    }
    //send email notifications
    //send email
    try {
      EmailView view = buildEmailView(trip, up, comments, group, null);

      String sender = "Umapped";
      if (view.businessCard.companyName != null && view.businessCard.companyName.trim().length() > 0) {
        sender = view.businessCard.companyName;
      }



      /*
      if (up.getEmail() != null && up.getEmail().trim().length() > 0) {
        fromEmail = up.getEmail();
      }
      else {
        fromEmail = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_TRIP_NOTIFICATION_FROM);
      }
      */

      String emailEnabled = ConfigMgr.getAppParameter(CoreConstants.EMAIL_ENABLED);

      if (emailEnabled != null && emailEnabled.equalsIgnoreCase("Y")) {

        for (TripAgent p : agents) {
          if (p.getStatus() == APPConstants.STATUS_ACTIVE) {
            TripPassengerView pView = new TripPassengerView();
            if (p.getName() != null && p.getName().length() > 0) {
              pView.name = p.getName();
            }

            if (p.getEmail() != null && Utils.isValidEmailAddress(p.getEmail())) {
              pView.email = p.getEmail();
            }
            view.passengers.add(pView);
          }
        }


        String subject = trip.getName() + " - " + Utils.getDateStringPrint(trip.getStarttimestamp()) + " to " +
                         Utils.getDateStringPrint(trip.getEndtimestamp());




        String emailBody = null;
        //check to see if there is a custom email template
        try {
          if (!cmpIdsForDefaultAgentEmailTemplate.contains(trip.getCmpyid())) {
            java.lang.reflect.Method render = cachedCustomAgentEmailTemplates.get(trip.getCmpyid());
            if (render == null) {
              CmpyCustomTemplate customTemplate = CmpyCustomTemplate.findActiveByCmpyType(trip.getCmpyid(),
                                                                                          CmpyCustomTemplate.TEMPLATE_TYPE.EMAIL_AGENCY
                                                                                              .ordinal());
              if (customTemplate != null && customTemplate.isfiletemplate && customTemplate.getPath() != null &&
                  customTemplate.getPath().startsWith("views")) {
                final Class<?> clazz = Class.forName(customTemplate.getPath());
                render = clazz.getDeclaredMethod("render", EmailView.class);
                cachedCustomAgentEmailTemplates.put(trip.getCmpyid(), render);
              }
            }
            if (render != null) {
              Html html = (Html) render.invoke(null, view);
              if (html != null) {
                emailBody = html.toString();
              }
            }
          }
        }
        catch (Exception e) {
          e.printStackTrace();
        }

        if (emailBody == null) {
          if (!cmpIdsForDefaultAgentEmailTemplate.contains(trip.getCmpyid())) {
            cmpIdsForDefaultAgentEmailTemplate.add(trip.getCmpyid());
          }
          try {
            if (view.businessCard.companyName != null && (view.businessCard.companyName
                    .toLowerCase()
                    .contains("atlantico") ||
                    view.businessCard.companyName
                            .toLowerCase()
                            .contains("atlântico") ||
                    view.businessCard.companyName
                            .toLowerCase()
                            .contains("agaxtur"))) {
              ctx().changeLang("pt"); //TODO: MIG24 Verify correct Switch of the language
              view.tripInfo.tripStartDatePrint = Utils.formatTimestamp(trip.starttimestamp, "EEE MMM dd, yyy", "pt");
              view.tripInfo.tripEndDatePrint = Utils.formatTimestamp(trip.endtimestamp, "EEE MMM dd, yyy", "pt");

            } else {
              ctx().changeLang("en"); //TODO: MIG24 Verify correct Switch of the language
            }
          } catch (Exception e) {
            //no context
          }
          emailBody = views.html.email.agent.render(view).toString();
        }



        //initialize email mgr
        UmappedEmail emailMgr = UmappedEmailFactory.getInstance(trip.getCmpyid());
        List<String> emails = new ArrayList<>();
        for (TripAgent p : agents) {
          if (p.getStatus() == APPConstants.STATUS_ACTIVE) {
            if (p.getEmail() != null && Utils.isValidEmailAddress(p.getEmail())) {
              emailMgr.addTo(p.email);
              emails.add(p.email);
            }
          }
        }
        //if this trip has the attachment tag - then include it in the email
        if (trip.getTag() != null && trip.getTag().contains(APPConstants.CMPY_TAG_API_ATTACH_ITINERARY_PDF_EMAIL)) {
          emailMgr.addAttachment("itinerary.pdf", trip.getName(), new URL(S3Util.getPDFUrl("trip/" + trip.getTripid() + "/itinerary_" + trip.getCreatedtimestamp() + ".pdf")));
        }
        emailMgr.withViaFromName(sender)
                .withSubject(subject).withTripId(trip.tripid)
                .withToList(emails)
                .withEmailType(EmailLog.EmailTypes.COLLABORATOR_ITINERARY)
                .withHtml(emailBody)
                .buildAndSend();

      }

    }
    catch (Exception e) {
      e.printStackTrace();
      Log.log(LogLevel.ERROR, "Cannot send trip email. Tripid: " + trip.tripid, e);
      return -1;//ok(views.html.common.message.render(view1));
    }
    return 0;
  }

  /*
    Send email notification to all the collaborators that this trip has been published
   */
  public static int sendCollaboratorsEmailNotification(Trip trip, UserProfile up, String comments, TripGroup group) {

    try {
      List<String> emails = new ArrayList<>();
      List<TripShare> shares = TripShare.getTripCollaborators(trip);
      if (shares != null && shares.size() > 0) {
        for (TripShare share : shares) {
          UserProfile u = share.getUser();
          if (u.getEmail() != null && up.getEmail() != null && !up.getEmail().equals(u.getEmail())) {
            emails.add(u.getEmail());
          }
        }
      }

      List<TripInvite> invites = TripInvite.getTripInvites(trip);
      if (invites != null && invites.size() > 0) {
        for (TripInvite invite : invites) {
          UserInvite u = invite.getUserInvite();
          if (u.getEmail() != null && up.getEmail() != null && !up.getEmail().equals(u.getEmail())) {
            emails.add(u.getEmail());
          }
        }

        //if trip is not published by the creator, notify the creator also
        if (!trip.getCreatedby().equals(up.getUserid())) {
          UserProfile u2 = UserProfile.findByPK(trip.createdby);
          if (u2 != null && u2.getEmail() != null) {
            emails.add(u2.email);
          }
        }
      }

      if (emails != null && emails.size() > 0) {
        EmailView view = buildEmailView(trip, up, comments, group, null);

        String sender = "Umapped";
        if (view.businessCard.companyName != null && view.businessCard.companyName.trim().length() > 0) {
          sender = view.businessCard.companyName;
        }


        /*
        if (up.getEmail() != null && up.getEmail().trim().length() > 0 ) {
          fromEmail = up.getEmail();
        } else {
          fromEmail =ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_TRIP_NOTIFICATION_FROM);
        }
        */
        String emailEnabled = ConfigMgr.getAppParameter(CoreConstants.EMAIL_ENABLED);

        if (emailEnabled != null && emailEnabled.equalsIgnoreCase("Y")) {

          String subject = trip.getName() + " - " + Utils.getDateStringPrint(trip.getStarttimestamp()) + " to " +
                           Utils.getDateStringPrint(trip.getEndtimestamp());


          String emailBody = null;
          //check to see if there is a custom email template
          try {
            if (!cmpIdsForDefaultAgentEmailTemplate.contains(trip.getCmpyid())) {
              java.lang.reflect.Method render = cachedCustomAgentEmailTemplates.get(trip.getCmpyid());
              if (render == null) {
                CmpyCustomTemplate customTemplate = CmpyCustomTemplate.findActiveByCmpyType(trip.getCmpyid(),
                                                                                            CmpyCustomTemplate.TEMPLATE_TYPE.EMAIL_AGENCY
                                                                                                .ordinal());

                if (customTemplate != null && customTemplate.isfiletemplate && customTemplate.getPath() != null &&
                    customTemplate.getPath().startsWith("views")) {
                  final Class<?> clazz = Class.forName(customTemplate.getPath());
                  render = clazz.getDeclaredMethod("render", EmailView.class);
                  cachedCustomAgentEmailTemplates.put(trip.getCmpyid(), render);
                }
              }
              if (render != null) {
                Html html = (Html) render.invoke(null, view);
                if (html != null) {
                  emailBody = html.toString();
                }
              }
            }
          }
          catch (Exception e) {
            e.printStackTrace();
          }

          if (emailBody == null) {
            if (view.businessCard.companyName != null && (view.businessCard.companyName.toLowerCase()
                                                                                       .contains("atlantico") || view.businessCard.companyName
                                                              .toLowerCase()
                                                              .contains("atlântico")
                                                          ||
                                                          view.businessCard.companyName.toLowerCase().contains("agaxtur"))) {
              ctx().changeLang("pt"); //TODO: MIG24 Verify correct Switch of the language
              view.tripInfo.tripStartDatePrint = Utils.formatTimestamp(trip.starttimestamp, "EEE MMM dd, yyy", "pt");
              view.tripInfo.tripEndDatePrint = Utils.formatTimestamp(trip.endtimestamp, "EEE MMM dd, yyy", "pt");

            }
            if (!cmpIdsForDefaultAgentEmailTemplate.contains(trip.getCmpyid())) {
              cmpIdsForDefaultAgentEmailTemplate.add(trip.getCmpyid());
            }
            emailBody = views.html.email.agent.render(view).toString();
          }

          //initialize email mgr
          UmappedEmail emailMgr = UmappedEmailFactory.getInstance(trip.getCmpyid());
          for (String email: emails) {
            emailMgr.addTo(email);
          }
          //if this trip has the attachment tag - then include it in the email
          if (trip.getTag() != null && trip.getTag().contains(APPConstants.CMPY_TAG_API_ATTACH_ITINERARY_PDF_EMAIL)) {
            emailMgr.addAttachment("itinerary.pdf", trip.getName(), new URL(S3Util.getPDFUrl("trip/" + trip.getTripid() + "/itinerary_" + trip.getCreatedtimestamp() + ".pdf")));
          }
          emailMgr.withViaFromName(sender)
                  .withSubject(subject)
                  .withHtml(emailBody)
                  .withEmailType(EmailLog.EmailTypes.COLLABORATOR_ITINERARY)
                  .withToList(emails)
                  .withTripId(trip.tripid)
                  .buildAndSend();

        }

//send mobile notifications
        StringBuilder notificationMsg = new StringBuilder();
        String notificationTitle = "Your client's itinerary is ready";
        notificationMsg.append(trip.getName());
        for (String email: emails) {
          //try to send mobile notification
          final MobilePushNotificationActor.Command cmd2 = new MobilePushNotificationActor.Command(email, notificationTitle, notificationMsg.toString(), DBConnectionMgr.getUniqueLongId());
          ActorsHelper.tell(SupervisorActor.UmappedActor.MOBILE_PUSH_NOTIFY, cmd2);
        }
      }
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "Cannot send trip email to collaborators. Tripid: " + trip.tripid, e);
      return -1;//ok(views.html.common.message.render(view1));
    }
    return 0;
  }

  private static int sendUmappedEmailNotification(Account traveler,
                                                  AccountTripLink tripLink,
                                                  Trip trip,
                                                  UserProfile up,
                                                  String comments,
                                                  TripGroup group,
                                                  List<TripPassengerView> allPassengers) {

    List<String> emails = new ArrayList<>();

    //send email notifications
    //send email
    try {
      EmailView view = buildEmailView(trip, up, comments, group, (traveler != null?traveler.getUid():0L));
      String sender = "Umapped";

      if (view.businessCard.companyName != null && view.businessCard.companyName.trim().length() > 0) {
        sender = view.businessCard.companyName;
      }

      view.passengers = allPassengers;


      String emailEnabled = ConfigMgr.getAppParameter(CoreConstants.EMAIL_ENABLED);

      if (emailEnabled != null && emailEnabled.equalsIgnoreCase("Y")) {
        String subject = "Get Ready For Your Trip - " + trip.getName()  ;

        String emailBody = null;
        //check to see if there is a custom email template
        try {
          if (!cmpIdsForDefaultEmailTemplate.contains(trip.getCmpyid())) {
            java.lang.reflect.Method render = cachedCustomEmailTemplates.get(trip.getCmpyid());
            if (render == null) {
              render = getCustomEmailTemplate(trip, CmpyCustomTemplate.TEMPLATE_TYPE.EMAIL_PASSENGER, cachedCustomEmailTemplates);
            }
            if (render != null) {
              Html html = (Html) render.invoke(null, view);
              if (html != null) {
                emailBody = html.toString();
              }
            } else {
              //let's check the other template
              render = cachedCustomFullEmailTemplates.get(trip.getCmpyid());
              if (render == null) {
                render = getCustomEmailTemplate(trip, CmpyCustomTemplate.TEMPLATE_TYPE.FULL_EMAIL_PASSENGER, cachedCustomFullEmailTemplates);
              }
              if (render != null) {
                //let's render the full email
                subject = trip.getName();
                TripPreviewView previewView = WebItineraryController.getFullEmailView(trip);
                play.api.i18n.Lang lang = new play.api.i18n.Lang("en", "US");
                Html html = (Html) render.invoke(null, previewView, lang.code(), false);
                if (html != null) {
                  emailBody = html.toString();

                }
              }
            }

          }

        }
        catch (Exception e) {
          e.printStackTrace();
        }

        if (emailBody == null) {
          if (!cmpIdsForDefaultEmailTemplate.contains(trip.getCmpyid())) {
            cmpIdsForDefaultEmailTemplate.add(trip.getCmpyid());
          }
          //TODO remove hardcoded hack for portuguese language
          //should be a trip driven property
          try {
            if (view.businessCard.companyName != null &&
                    (view.businessCard.companyName.toLowerCase().contains("atlantico") ||
                            view.businessCard.companyName.toLowerCase().contains("atlântico") ||
                            view.businessCard.companyName.toLowerCase().contains("agaxtur"))) {
              ctx().changeLang("pt"); //TODO: MIG24 Verify correct Switch of the language
              view.tripInfo.tripStartDatePrint = Utils.formatTimestamp(trip.starttimestamp, "EEE MMM dd, yyy", "pt");
              view.tripInfo.tripEndDatePrint = Utils.formatTimestamp(trip.endtimestamp, "EEE MMM dd, yyy", "pt");

            } else {
              ctx().changeLang("en");
            }
          } catch (Exception e) {
            //no http context
          }
          emailBody = views.html.email.trip.render(view).toString();
        }

        //initialize email mgr
        UmappedEmail emailMgr = UmappedEmailFactory.getInstance(trip.getCmpyid());
        if (traveler != null) {
          emailMgr.addTo(traveler.getEmail());
          emails.add(traveler.getEmail());
        } else if (allPassengers != null && allPassengers.size() > 0){
          for (TripPassengerView pv: allPassengers) {
            if (pv.email != null && Utils.isValidEmailAddress(pv.email) && !emails.contains(pv.email)) {
              emailMgr.addTo(pv.email);
              emails.add(pv.email);
            }
          }
        }

        //for Ski.com add harry@ski.com as a BCC
        //this is a temporary hardcoding - will need to add a proper field for audit email
        if (trip.getCmpyid().equals("920269968500066832") && ConfigMgr.isProduction()) {
          emailMgr.addBcc("harry@ski.com");
          emails.add("harry@ski.com");
        }
        //if this trip has the attachment tag - then include it in the email
        if (trip.getTag() != null && trip.getTag().contains(APPConstants.CMPY_TAG_API_ATTACH_ITINERARY_PDF_EMAIL)) {
          emailMgr.addAttachment("itinerary.pdf", trip.getName(), new URL(S3Util.getPDFUrl("trip/" + trip.getTripid() + "/itinerary_" + trip.getCreatedtimestamp() + ".pdf")));
        }
        if (tripLink != null) {
          emailMgr.withAccountUid(tripLink.getPk().getUid())
                  .withEmailType(EmailLog.EmailTypes.TRAVELLER_ITINERARY);
        } else {
          emailMgr.withEmailType(EmailLog.EmailTypes.CC_ITINERARY);
        }
        emailMgr.withViaFromName(sender)
                .withSubject(subject)
                .withHtml(emailBody)
                .withTripId(trip.tripid)
                .withToList(emails)
                .buildAndSend();

      }

    }
    catch (Exception e) {
      e.printStackTrace();
      Log.err("Cannot send trip email. Tripid: " + trip.tripid, e);
      return -1;//ok(views.html.common.message.render(view1));
    }
    return 0;
  }

  public static java.lang.reflect.Method getCustomEmailTemplate(Trip trip, CmpyCustomTemplate.TEMPLATE_TYPE template_type, Map<String, java.lang.reflect.Method> cache){
    CmpyCustomTemplate customTemplate = CmpyCustomTemplate.findActiveByCmpyType(trip.getCmpyid(),
                                                                                template_type
                                                                                    .ordinal());
    if (customTemplate != null && customTemplate.isfiletemplate && customTemplate.getPath() != null &&
        customTemplate.getPath().startsWith("views")) {
      try {
        final Class<?> clazz = Class.forName(customTemplate.getPath());
        java.lang.reflect.Method render = null;
        if (template_type == CmpyCustomTemplate.TEMPLATE_TYPE.FULL_EMAIL_PASSENGER) {
          render = clazz.getDeclaredMethod("render", TripPreviewView.class, String.class, Boolean.class);

        } else {
          render = clazz.getDeclaredMethod("render", EmailView.class);
        }
        cache.put(trip.getCmpyid(), render);
        return render;
      } catch (Exception e) {
        Log.err("getCustomEmailTemplate: Error getting custom template: Trip: " + trip.getTripid(), e);
      }
    }
    return null;
  }

  public static EmailView buildEmailView(Trip trip,
                                         UserProfile up,
                                         String comments,
                                         TripGroup group,
                                         Long travelerId) {
    String hostUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);

    String tripPK = trip.tripid;
    if (group != null && group.tripid.equals(trip.tripid)) {
      tripPK = TourController.getCompositePK(trip.tripid, group.groupid);
    }

    EmailView view = new EmailView();
    view.tripInfo = new TripInfoView();
    view.passengers = new ArrayList<>();
    view.businessCard = new BusinessCardView();

    view.tripInfo.tripId = trip.getTripid();
    view.tripInfo.tripName = trip.getName();
    view.tripInfo.tripStartDate = String.valueOf(trip.getStarttimestamp());
    view.tripInfo.tripEndDate = String.valueOf(trip.getEndtimestamp());
    view.tripInfo.tripStartDatePrint = Utils.formatDatePrint(trip.getStarttimestamp());
    view.tripInfo.tripEndDatePrint = Utils.formatDatePrint(trip.getEndtimestamp());
    view.tripInfo.cover = trip.getCoverView();

    if (trip.tag != null && !trip.tag.isEmpty() && TripBrandingMgr.hasOverwriteBrandingTag(trip.tag)) {
      //check for Ski.com tags - do not display app icons for United and Delta
        view.displayMobileApps = false;

    }

    if (view.tripInfo.cover == null) {
      view.tripInfo.cover = new ImageView();
      view.tripInfo.url = "http://static-umapped-com.s3.amazonaws" +
                          ".com/public/lib/web-itinerary-splash/images/default_splash.jpg";
    }

    //Selecting company logo based on trip branding preferences
    Company cmpy;
    TripBranding branding = TripBranding.findPrimaryByTripId(trip.getTripid());
    if (branding != null) {
      cmpy = Company.find.byId(branding.pk.cmpyid);
    }
    else {
      cmpy = Company.find.byId(trip.getCmpyid());
    }

    //cobranding overwrite by tag
    TripBrandingView overwriteBrandingView = TripBrandingMgr.buildTripCobrandingByTag(trip, null);

    List<CmpyAddress> addresses = CmpyAddress.findMainActiveByCmpyId(cmpy.cmpyid);
    CmpyAddress cmpyAddress = null;
    if (addresses != null && addresses.size() > 0) {
      cmpyAddress = addresses.get(0);
    }

    view.tripInfo.agencyName = cmpy.getName();

    //TODO: Wrong logic: Selecting agent name based on who publishes the trip
    view.businessCard.companyLogoUrl = cmpy.logourl;
    view.businessCard.setFirstName(up.getFirstname());
    view.businessCard.setLastName(up.getLastname());
    view.businessCard.email = TripBrandingMgr.getWhieLabelEmail(trip,up.getEmail()); //allow overwrite of email domain for ski.com
    view.businessCard.phone = up.getPhone();
    view.businessCard.mobile = up.getMobile();
    view.businessCard.fax = up.getFax();
    view.businessCard.socialTwitterUrl = up.getTwitter();
    view.businessCard.socialFacebookUrl = up.getFacebook();
    view.businessCard.profilePhotoUrl = up.getProfilephotourl();
    view.businessCard.webUrl = up.getWeb();
    view.businessCard.companyName = cmpy.getName();

    if(up != null) {
      AgentView agentView = new AgentView();
      view.businessCard.agentView = agentView;
      StringBuffer sb = new StringBuffer();
      if (up.firstname != null) {
        sb.append(up.firstname);
      }
      if (sb.length() > 0 && up.lastname != null) {
        sb.append(" ");
        sb.append(up.lastname);
      }
      agentView.agentName = sb.toString();
      agentView.agentEmail = TripBrandingMgr.getWhieLabelEmail(trip, up.getEmail()); //allow overwrite of email domain
      // for ski.com
      agentView.agentPhone = up.phone;
      agentView.agentMobile = up.mobile;
      agentView.cmpyWeb = up.web;
      agentView.agentFax = up.fax;
      agentView.agentFacebook = up.facebook;
      agentView.agentTwitter = up.twitter;

      TripBrandingView       overwritten   = null;
      List<TripBrandingView> brandingViews = TripBrandingMgr.findActiveBrandedCmpies(trip);
      if (brandingViews != null && brandingViews.size() > 0) {
        cmpy = TripBrandingMgr.getMainContact(brandingViews);
        //previewView.tripBranding = brandingViews;
        //if this is overwritten by tag for the same company e.g. ski.com
        overwritten = TripBrandingMgr.getOverwrittenByTag(brandingViews, trip);
      }

      if (cmpy == null) {
        cmpy = Company.find.byId(trip.cmpyid);
      }

      if (overwritten == null) {
        if (cmpy != null) {
          agentView.cmpyName = cmpy.name;
          if (cmpy.getLogoname() != null &&
                  cmpy.getLogoname().length() > 0 &&
                  cmpy.getLogourl() != null &&
                  cmpy.getLogourl().length() > 0) {
            agentView.cmpyLogo = Utils.escapeS3Umapped_Prd(cmpy.getLogourl());
            agentView.cmpyTag = cmpy.getTag();
          }
        }
        List<CmpyAddress> cmpyAddrs = CmpyAddress.findMainActiveByCmpyId(cmpy.cmpyid);
        if (cmpyAddrs != null && cmpyAddrs.size() > 0) {
          if (agentView.agentPhone == null || agentView.agentPhone.isEmpty()) {
            agentView.agentPhone =  TripBrandingMgr.getCompanyExtension(Account.findActiveByLegacyId(up.getUserid()), cmpyAddress.phone);
          }
          if (agentView.cmpyWeb == null || agentView.cmpyWeb.isEmpty()) {
            agentView.cmpyWeb = cmpyAddress.web;
          }

        }
      }
      else {
        agentView.cmpyName = overwritten.cmpyName;
        agentView.cmpyLogo = Utils.escapeS3Umapped_Prd(overwritten.cmpyLogoUrl);
        agentView.cmpyEmail = overwritten.cmpyEmail;
        agentView.cmpyPhone = TripBrandingMgr.getCompanyExtension(Account.findActiveByLegacyId(up.getUserid()), overwritten.cmpyPhone);
        agentView.cmpyTag = overwritten.cmpyTag;
        agentView.cmpyFax = overwritten.cmpyFax;
        agentView.cmpyFacebook = overwritten.cmpyFacebook;
        agentView.cmpyTwitter = overwritten.cmpyTwitter;
        agentView.agentMobile = "";
        if (agentView.agentPhone == null || agentView.agentPhone.length() < 1) {
          agentView.agentPhone = agentView.cmpyPhone;
        }
        if (agentView.cmpyWeb == null || agentView.cmpyWeb.length() < 1) {
          agentView.cmpyWeb = overwritten.cmpyWebsite;
        }
      }
    }

    //check to see if th company is part of a consortium
    List<ConsortiumCompany> consortiums = ConsortiumCompany.findByCmpyId(cmpy.getCmpyId());
    if (consortiums != null) {
      for (ConsortiumCompany c : consortiums) {
        Consortium consortium = Consortium.find.byId(c.getConsId());
        if (consortium != null) {
          view.businessCard.addConsortiumNames(consortium.getName());
        }
      }
    }

    if (cmpyAddress != null) {
      view.tripInfo.agencyAddress = cmpyAddress.formatAddress();
      view.tripInfo.agencyEmail = cmpyAddress.getEmail();
      view.tripInfo.agencyPhone = cmpyAddress.getPhone();
      view.tripInfo.agencyWebsite = cmpyAddress.getWeb();
      view.businessCard.companyUrl = cmpyAddress.getWeb();

      if (view.businessCard.phone == null || view.businessCard.phone.length() < 1) {
        view.businessCard.phone = cmpyAddress.getPhone();
      }
      if (view.businessCard.webUrl == null || view.businessCard.webUrl.length() < 1) {
        view.businessCard.webUrl = cmpyAddress.getWeb();
      }


      if (view.businessCard.socialTwitterUrl == null || view.businessCard.socialTwitterUrl.length() == 0) {
        view.businessCard.socialTwitterUrl = cmpyAddress.getTwitter();
      }

      if (view.businessCard.socialFacebookUrl == null || view.businessCard.socialFacebookUrl.length() == 0) {
        view.businessCard.socialFacebookUrl = cmpyAddress.getFacebook();
      }
    }

    if (overwriteBrandingView != null) {
      view.businessCard.companyLogoUrl = overwriteBrandingView.cmpyLogoUrl;
      view.businessCard.companyName = overwriteBrandingView.cmpyName;
      view.tripInfo.agencyEmail = overwriteBrandingView.cmpyEmail;
      view.tripInfo.agencyPhone = overwriteBrandingView.cmpyPhone;
      view.tripInfo.agencyWebsite = overwriteBrandingView.cmpyWebsite;
      view.businessCard.companyUrl = overwriteBrandingView.cmpyWebsite;
      view.businessCard.phone = overwriteBrandingView.cmpyPhone;
      view.businessCard.fax = overwriteBrandingView.cmpyFax;
      view.businessCard.mobile ="";
      view.businessCard.webUrl = overwriteBrandingView.cmpyWebsite;
    }


    if (comments != null && comments.trim().length() > 0) {
      view.note = comments.replaceAll("\n", "<br/>\n");
    }

    if (travelerId == null) {
      view.travelerId = Long.parseLong("0");
      view.tripUrl = hostUrl + routes.WebItineraryController.index(tripPK, null, 0);
    } else {
      view.travelerId = travelerId;
      view.tripUrl = hostUrl + routes.WebItineraryController.index(tripPK, null, travelerId);
    }
    return view;
  }

  /**
   * Saves dest guides for this trip
   */
  @With({Authenticated.class, Authorized.class})
  public Result saveDestGuides() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<TripDestinationForm> form = formFactory.form(TripDestinationForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    TripDestinationForm tripInfo = form.get();
    try {
      Trip trip = Trip.findByPK(tripInfo.getInTripId());
      if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND) && tripInfo.getInDestId() != null) {
        List<TripDestination> currentList = TripDestination.getAllByTripId(tripInfo.getInTripId());
        Map<String, TripDestination> currentMap = new HashMap<>();
        for (TripDestination t : currentList) {
          currentMap.put(t.destinationid, t);
        }

        for (String s : tripInfo.getInDestId()) {
          TripDestination tripDest = currentMap.get(s);
          if (tripDest == null) {
            Destination dest = Destination.find.byId(s);
            if (dest != null && dest.status == APPConstants.STATUS_ACTIVE) {
              tripDest = new TripDestination();
              tripDest.setDestinationid(s);
              tripDest.setTripdestid(Utils.hash(s + tripInfo.getInTripId()));
              tripDest.setTripid(tripInfo.getInTripId());
              tripDest.setCreatedtimestamp(System.currentTimeMillis());
              tripDest.setCreatedby(sessionMgr.getUserId());
              if (dest.name != null) {
                tripDest.setName(dest.name);
              }
              else {
                tripDest.setName("");
              }
              tripDest.setRank(0);


              tripDest.setStatus(APPConstants.STATUS_ACTIVE);
              tripDest.setModifiedby(sessionMgr.getUserId());
              tripDest.setLastupdatedtimestamp(System.currentTimeMillis());
              tripDest.save();
            }
          }
        }
      }
      return redirect(routes.TourController.reviewTour());
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    BaseView view = new BaseView();
    view.message = "System Error: tc3790";
    return ok(views.html.common.message.render(view));
  }

  /* Search utility methods */
  public static List<TripInfoView> searchMyTrips(SessionMgr sessionMgr,
                                                 String term,
                                                 int searchType,
                                                 String cmpyId,
                                                 int startPos) {
    int maxRows = APPConstants.MAX_RESULT_COUNT;

    Credentials cred = sessionMgr.getCredentials();

    Map<String, Company> cmpiesMap = new HashMap<>();

    ArrayList<String> cmpies = new ArrayList<>();

    HashMap<String, String> agencyList = new HashMap<>();

    if(cred.hasCompany()) {
      agencyList.put(cred.getCmpyId(), cred.getCmpyName());
    }

    //filter cmpy
    if (cmpyId != null && !cmpyId.equalsIgnoreCase("All")) {
      //filter cmpies to only cmpy selected
      if (cmpies.contains(cmpyId)) {
        cmpies = new ArrayList<>();
        cmpies.add(cmpyId);
      }
    }
    List<Trip> searchResults = new ArrayList<>();
    if (searchType == APPConstants.SEARCH_MY_PENDING_TRIPS) {
      searchResults = Trip.findPendingByUserId(sessionMgr.getUserId(), maxRows, cmpies, term, startPos);
    }
    else if (searchType == APPConstants.SEARCH_MY_TRIPS) {
      searchResults = Trip.findActiveByUserId(sessionMgr.getUserId(), maxRows, cmpies, term, startPos);
    }


    return TripController.buildTripView(searchResults, cmpiesMap, null);
  }


  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result searchAllTrips() {
    JsonNode jsonRequest = request().body().asJson();
    DataTablesForm dtRequest = Json.fromJson(jsonRequest, DataTablesForm.class);
    SessionMgr    sessionMgr = SessionMgr.fromContext(ctx());

    class TripRow {
      public String imgUrl;
      public String tripName;
      public String tripDate;
      public String status;
      public String createdBy;
      //public String company;
      public String id;
    }

    int offsetMins = sessionMgr.getTimezoneOffsetMins();

    long currentClientTime = Utils.getCurrentClientDate(offsetMins);

    DataTablesView<TripRow> dtv = new DataTablesView<>(dtRequest.draw);

    if (jsonRequest.get("allTrips") != null) {
      dtv.allTrips = Boolean.valueOf(jsonRequest.get("allTrips").asText());
    } else {
      dtv.allTrips = false;
    }
    dtv.selectStatus = Integer.valueOf(jsonRequest.get("status").asText());
    dtv.filterDate = Integer.valueOf(jsonRequest.get("filterDate").asText());


    dtv.searchTerm = "";
    if (jsonRequest.get("keyword").asText() != null && jsonRequest.get("keyword").asText().length() > 0) {
      dtv.searchTerm = "%" + jsonRequest.get("keyword").asText().replace(" ", "%") + "%";
    }
    else {
      dtv.searchTerm = "%";
    }

    Credentials cred = sessionMgr.getCredentials();

    Map<String, Company> cmpiesMap = new HashMap<>();

    ArrayList<String> cmpies = new ArrayList<>();

    HashMap<String, String> agencyList = new HashMap<>();
    if(cred.hasCompany()) {
      Company c = cred.getCompany();
      agencyList.put(c.cmpyid, c.getName());
      cmpies.add(cred.getCmpyId());
      cmpiesMap.put(c.getCmpyid(), c);
    }

    ArrayList<Integer> statuses = new ArrayList<>();
    if (dtv.selectStatus == 0) {
      statuses.add(0);
    }
    else if (dtv.selectStatus == 1) {
      statuses.add(1);
    }
    else {
      dtv.selectStatus = 3;
      statuses.add(0);
      statuses.add(1);
    }

    int maxRows = dtRequest.length;

    FilterType ft;
    if(dtv.filterDate == 0) {
      ft = FilterType.INPROGRESS;
    }
    else if(dtv.filterDate == 1) {
      ft = FilterType.FUTURE;
    }
    else if(dtv.filterDate == 2) {
      ft = FilterType.PAST;
    }
    else if(dtv.filterDate == 3) {
      ft = FilterType.ACTIVE;
    }
    else {
      ft = FilterType.ALL;
    }


    //separate all cmpies into 2 list for proper queries - member or admin
    ArrayList<String> adminCmpies = new ArrayList<>();
    ArrayList<String> memberCmpies = new ArrayList<>();

    if(cred.getCmpyLinkType() == AccountCmpyLink.LinkType.ADMIN) {
      adminCmpies.add(cred.getCmpyId());
    } else {
      memberCmpies.add(cred.getCmpyId());
    }

    if (adminCmpies.size() > 0 && dtRequest.start != -1) {
      List<Trip> t = new ArrayList<>();
      String sortCol = jsonRequest.get("columns").get(Integer.parseInt(jsonRequest.get("order").get(0).get("column").asText())).get("data").asText();
      String sortOrder = jsonRequest.get("order").get(0).get("dir").asText();

      if (dtv.allTrips) {
        t = Trip.findActivePendingByCmpyIds(sessionMgr.getUserId(),
                                            maxRows,
                                            adminCmpies,
                                            dtv.searchTerm,
                                            statuses,
                                            dtRequest.start,
                                            sortCol,
                                            sortOrder,
                                            ft, currentClientTime);

        dtv.recordsFiltered = Trip.countActivePendingByCmpyIds(sessionMgr.getUserId(),
                adminCmpies,
                dtv.searchTerm,
                statuses,
                ft, currentClientTime);
      }
      else {
        t = Trip.findAllMyTripsByTime(sessionMgr.getUserId(),
                                      maxRows,
                                      adminCmpies,
                                      dtv.searchTerm,
                                      statuses,
                                      dtRequest.start,
                                      dtv.allTrips,
                                      sortCol,
                                      sortOrder,
                                      ft, currentClientTime);

        dtv.recordsFiltered = Trip.countAllMyTripsByTime(sessionMgr.getUserId(),
                adminCmpies,
                dtv.searchTerm,
                statuses,
                dtv.allTrips,
                ft, currentClientTime);
      }

      if (t != null && t.size() > 0) {
        for (Trip rec : t) {
          TripRow row = new TripRow();
          ///Trip trip = rec.getTrip()                                      ;
          row.id =  routes.TripController.tripInfo(rec.getTripid()).url();
          row.tripName = "<a onclick=\"editTrip('"+ routes.TripController.tripInfo(rec.getTripid()).url()  + "')\">" + rec.getName() + "</a>";
          row.tripDate = Utils.getDateStringPrint(rec.getStarttimestamp()) + " - " + Utils.getDateStringPrint(rec.getEndtimestamp());
          row.imgUrl = rec.getImageUrl();
          if (rec.getStatus() == 0) {
            row.status = "Published";
          } else {
            row.status = "Pending";
          }
          row.createdBy = rec.getCreatedby();

          //row.company = cmpiesMap.get(rec.getCmpyid()).getName();


          dtv.data.add(row);
        }
      }
    }


    if (memberCmpies.size() > 0 && dtRequest.start != -1) {
      Connection conn = null;
      try {
        conn = DBConnectionMgr.getConnection4Publisher();

        List<Trip> t2 = new ArrayList<>();
        String sortCol = jsonRequest.get("columns").get(Integer.parseInt(jsonRequest.get("order").get(0).get("column").asText())).get("data").asText();
        String sortOrder = jsonRequest.get("order").get(0).get("dir").asText();

        if (dtv.allTrips) {
          t2 = TripMgr.findMyTrips(dtv.searchTerm,
              memberCmpies,
              sessionMgr.getUserId(),
              maxRows,
              dtRequest.start,
              statuses,
              conn,
              sortCol,
              sortOrder,
              ft, currentClientTime);

          dtv.recordsFiltered = TripMgr.countMyTrips(dtv.searchTerm,
                  memberCmpies,
                  sessionMgr.getUserId(),
                  statuses,
                  conn,
                  ft, currentClientTime);

        } else {
          t2 = TripMgr.findMyTrips2(dtv.searchTerm,
                                    memberCmpies,
                                    sessionMgr.getUserId(),
                                    maxRows,
                                    dtRequest.start,
                                    statuses,
                                    conn,
                                    dtv.allTrips,
                                    sortCol,
                                    sortOrder,
                                    ft, currentClientTime);

          dtv.recordsFiltered = TripMgr.countMyTrips2(dtv.searchTerm,
                  memberCmpies,
                  sessionMgr.getUserId(),
                  statuses,
                  conn,
                  dtv.allTrips,
                  ft, currentClientTime);
        }

        //dtv.recordsTotal = TripMgr.findDashboardAllTripsCount(sessionMgr.getCredentials().getCmpyId(), conn);

        if (t2 != null && t2.size() > 0) {
          for (Trip rec : t2) {
            TripRow row = new TripRow();
            ///Trip trip = rec.getTrip();
            row.id =  routes.TripController.tripInfo(rec.getTripid()).url();
            row.tripName = "<a onclick=\"editTrip('"+ routes.TripController.tripInfo(rec.getTripid()).url()  + "')\">" + rec.getName() + "</a>";
            row.tripDate = Utils.getDateStringPrint(rec.getStarttimestamp()) + " - " + Utils.getDateStringPrint(rec.getEndtimestamp());
            row.imgUrl = rec.getImageUrl();
            if (rec.getStatus() == 0) {
              row.status = "Published";
            } else {
              row.status = "Pending";
            }
            row.createdBy = rec.getCreatedby();

            //row.company = cmpiesMap.get(rec.getCmpyid()).getName();


            dtv.data.add(row);
          }
        }
      }
      catch (Exception e) {
        e.printStackTrace();
        Log.log(LogLevel.ERROR, "Search All Trips - User: " + sessionMgr.getUserId(), e);
      }
      finally {
        if (conn != null) {
          try {
            conn.close();
          }
          catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    }


    return  ok(Json.toJson(dtv));
  }

  public static HashMap getTripCompanies(String userId) {
    HashMap<String, String> results = new HashMap<String, String>();
    Connection conn = null;
    try {
      conn = DBConnectionMgr.getConnection4Publisher();

      results = CompanyMgr.findCmpies(userId, conn);

    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "getTripCompanies - User id: " + userId, e);
    }
    finally {
      if (conn != null) {
        try {
          conn.close();
        }
        catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    return results;
  }


    /* Helper */

  @With({Authenticated.class, Authorized.class})
  public Result doSearchTrips() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    int offsetMins = sessionMgr.getTimezoneOffsetMins();

    long currentClientTime = Utils.getCurrentClientDate(offsetMins);
    //bind html form to form bean
    Form<TripSearchForm> tripForm = formFactory.form(TripSearchForm.class);
    TripSearchForm tripSearchForm = tripForm.bindFromRequest().get();
    boolean allTrips = false;
    boolean searchArchives = false;

    if (tripSearchForm.isInAllTrips()) {
      allTrips = true;
    }

    if (tripSearchForm.isInArchived()) {
      searchArchives = true;
    }


    String term = "";
    if (tripSearchForm.getInSearchTerm() != null && tripSearchForm.getInSearchTerm().length() > 0) {
      term = "%" + tripSearchForm.getInSearchTerm().replace(" ", "%") + "%";
    }
    else {
      term = "%";
    }


    String cmpyId = tripSearchForm.getInCmpyId();

    TripsView view = new TripsView();
    view.searchResults = new ArrayList<>();


    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      view.message = msg;
    }

    Credentials cred = sessionMgr.getCredentials();

    Map<String, Company> cmpiesMap = new HashMap<>();

    ArrayList<String> cmpies = new ArrayList<>();

    HashMap<String, String> agencyList = new HashMap<>();
    if(cred.hasCompany()) {
      Company c = cred.getCompany();
      agencyList.put(c.cmpyid, c.getName());
      cmpies.add(cred.getCmpyId());
      cmpiesMap.put(c.getCmpyid(), c);
    }

    view.tripAgencyList = agencyList;

    int startPos = tripSearchForm.getInStartPos();


    int startAdminPos = tripSearchForm.getInStartAdminPos();


    view.tableName = tripSearchForm.getInTableName();
    view.isTour = false;
    if (cred.isCmpyAdmin() || cred.hasGroupies()) {
      view.displayCmpyTabs = true;
    }
    else {
      view.displayCmpyTabs = false;
    }

    //filter cmpy
    if (cmpyId != null && !cmpyId.equalsIgnoreCase("All")) {
      //filter cmpies to only cmpy selected
      if (cmpies.contains(cmpyId)) {
        cmpies = new ArrayList<>();
        cmpies.add(cmpyId);
      }
    }
    view.searchTerm = tripSearchForm.getInSearchTerm();
    view.cmpyId = cmpyId;
    view.searchResults = new ArrayList<>();

    view.searchStatus = tripSearchForm.getSearchStatus();
    view.allTrips = allTrips;
    view.searchArchives = searchArchives;

    ArrayList<Integer> statuses = new ArrayList<>();
    if (view.searchStatus == 0) {
      statuses.add(0);
    }
    else if (view.searchStatus == 1) {
      statuses.add(1);
    }
    else {
      view.searchStatus = 3;
      statuses.add(0);
      statuses.add(1);
    }

    int maxRows = APPConstants.MAX_RESULT_COUNT;

    //separate all cmpies into 2 list for proper queries - member or admin
    ArrayList<String> adminCmpies = new ArrayList<>();
    ArrayList<String> memberCmpies = new ArrayList<>();

    if(cred.getCmpyLinkType() == AccountCmpyLink.LinkType.ADMIN) {
      adminCmpies.add(cred.getCmpyId());
    } else {
      memberCmpies.add(cred.getCmpyId());
    }

    if (adminCmpies.size() > 0 && startAdminPos != -1) {
      List<Trip> t = new ArrayList<>();
      //int sortCol = Integer.parseInt(jsonRequest.get("order").get(0).get("column").asText());
      //String sortOrder = jsonRequest.get("order").get(0).get("dir").asText();

      if (allTrips) {
        t = Trip.findActivePendingByCmpyIds(sessionMgr.getUserId(),
                                            maxRows,
                                            adminCmpies,
                                            term,
                                            statuses,
                                            startAdminPos,
                                            "tripName",
                                            "asc",
                                            FilterType.ACTIVE, currentClientTime);
      }
      else {
        t = Trip.findAllMyTripsByTime(sessionMgr.getUserId(),
                                      maxRows,
                                      adminCmpies,
                                      term,
                                      statuses,
                                      startAdminPos,
                                      allTrips,
                                      "tripName",
                                      "asc",
                                      FilterType.ACTIVE, currentClientTime);
      }
      if (t != null && t.size() > 0) {
        List<TripInfoView> trips = buildTripView(t, cmpiesMap, null);
        view.searchResults.addAll(trips);
        view.searchResultsAdminCount = t.size();
        view.nextAdminPos = -1;
        view.prevAdminPos = -1;
        if (startAdminPos >= APPConstants.MAX_RESULT_COUNT) {
          view.prevAdminPos = startAdminPos - APPConstants.MAX_RESULT_COUNT + 1;
          if (view.prevAdminPos < 0) {
            view.prevAdminPos = -1;
          }
        }
        if (t.size() == APPConstants.MAX_RESULT_COUNT) {
          //there are more records
          view.nextAdminPos = startAdminPos + APPConstants.MAX_RESULT_COUNT;
        }
      }
    }


    if (memberCmpies.size() > 0 && startPos != -1) {
      Connection conn = null;
      try {
        conn = DBConnectionMgr.getConnection4Publisher();

        List<Trip> t2 = new ArrayList<>();

        if (allTrips) {
          t2 = TripMgr.findMyTrips(term,
                                  memberCmpies,
                                  sessionMgr.getUserId(),
                                  maxRows,
                                  startPos,
                                  statuses,
                                  conn,
                                  "tripName",
                                  "asc",
                                  FilterType.ACTIVE, currentClientTime);

        } else {
          t2 = TripMgr.findMyTrips2(term,
                                    memberCmpies,
                                    sessionMgr.getUserId(),
                                    maxRows,
                                    startPos,
                                    statuses,
                                    conn,
                                    allTrips,
                                    "tripName",
                                    "asc",
                                    FilterType.ACTIVE, currentClientTime);

        }

        if (t2 != null && t2.size() > 0) {
          List<TripInfoView> trips = buildTripView(t2, cmpiesMap, null);
          view.searchResults.addAll(trips);
          view.searchResultsCount = t2.size();
          view.nextPos = -1;
          view.prevPos = -1;
          if (startPos >= APPConstants.MAX_RESULT_COUNT) {
            view.prevPos = startPos - APPConstants.MAX_RESULT_COUNT + 1;
            if (view.prevPos < 0) {
              view.prevPos = -1;
            }
          }
          if (t2.size() == APPConstants.MAX_RESULT_COUNT) {
            //there are more records
            view.nextPos = startPos + APPConstants.MAX_RESULT_COUNT;
          }
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
      finally {
        if (conn != null) {
          try {
            conn.close();
          }
          catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    }

    if (view.searchResults != null) {
      Collections.sort(view.searchResults, TripInfoView.TripInfoViewNameComparator);
    }

    if (startAdminPos > 0 || startPos > 0) {
      //continuation
      return ok(views.html.trip.searchTripsMore.render(view));

    }
    else {
      //new search
      return ok(views.html.trip.searchTrips.render(view));

    }


  }

  @With({Authenticated.class, Authorized.class})
  public Result otherTrips() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<TripSearchForm> tripForm = formFactory.form(TripSearchForm.class);
    TripSearchForm tripSearchForm = tripForm.bindFromRequest().get();

    String term = "";
    if (tripSearchForm.getInSearchTerm() != null && tripSearchForm.getInSearchTerm().length() > 0) {
      term = "%" + tripSearchForm.getInSearchTerm().replace(" ", "%") + "%";
    }
    else {
      term = "%";
    }

    int searchType = tripSearchForm.getSearchType();
    if (searchType != APPConstants.SEARCH_OTHER_PENDING_TRIPS && searchType != APPConstants.SEARCH_OTHER_TRIPS) {
      searchType = APPConstants.SEARCH_OTHER_TRIPS;
    }

    String cmpyId = tripSearchForm.getInCmpyId();

    TripsView view = new TripsView();

    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      view.message = msg;
    }

    Credentials cred = sessionMgr.getCredentials();

    Map<String, Company> cmpiesMap = new HashMap<>();

    ArrayList<String> cmpies = new ArrayList<>();

    HashMap<String, String> agencyList = new HashMap<>();
    if(cred.hasCompany()) {
      Company c = cred.getCompany();
      agencyList.put(c.cmpyid, c.getName());
      cmpies.add(cred.getCmpyId());
      cmpiesMap.put(c.getCmpyid(), c);
    }

    view.tripAgencyList = agencyList;

    //filter cmpy
    if (cmpyId != null && !cmpyId.equalsIgnoreCase("All")) {
      //filter cmpies to only cmpy selected
      if (cmpies.contains(cmpyId)) {
        cmpies = new ArrayList<>();
        cmpies.add(cmpyId);
      }
    }
    view.searchTerm = tripSearchForm.getInSearchTerm();
    view.cmpyId = cmpyId;

    int startPos = tripSearchForm.getInStartPos();
    if (startPos < 0) {
      startPos = 0;
    }

    view.searchType = searchType;
    view.tableName = tripSearchForm.getInTableName();
    view.isTour = false;

    view.searchResults = TripController.searchOtherTrips(sessionMgr, term, searchType, cmpyId, startPos);
    view.searchResultsCount = 0;

    view.nextPos = 0;
    view.prevPos = -1;
    if (startPos >= APPConstants.MAX_RESULT_COUNT) {
      view.prevPos = startPos - APPConstants.MAX_RESULT_COUNT + 1;
      if (view.prevPos < 0) {
        view.prevPos = -1;
      }
    }

    if (view.searchResults != null) {
      view.searchResultsCount = view.searchResults.size();


      if (view.searchResults.size() == APPConstants.MAX_RESULT_COUNT) {
        //there are more records
        view.nextPos = startPos + APPConstants.MAX_RESULT_COUNT;
      }
    }

    return ok(views.html.trip.searchResults.render(view));
  }

  public static List<TripInfoView> searchOtherTrips(SessionMgr sessionMgr,
                                                    String term,
                                                    int searchType,
                                                    String cmpyId,
                                                    int startPos) {
    int maxRows = APPConstants.MAX_RESULT_COUNT;

    Credentials cred = sessionMgr.getCredentials();

    Map<String, Company> cmpiesMap = new HashMap<>();

    ArrayList<String> cmpies = new ArrayList<>();

    HashMap<String, String> agencyList = new HashMap<>();
    if(cred.hasCompany()) {
      Company c = cred.getCompany();
      agencyList.put(c.cmpyid, c.getName());
      cmpies.add(cred.getCmpyId());
      cmpiesMap.put(c.getCmpyid(), c);
    }

    //filter cmpy
    if (cmpyId != null && !cmpyId.equalsIgnoreCase("All")) {
      //filter cmpies to only cmpy selected
      if (cmpies.contains(cmpyId)) {
        cmpies = new ArrayList<>();
        cmpies.add(cmpyId);
      }
    }

    List<Trip> searchResults = new ArrayList<>();

    if (cred.getCmpyLinkType() == AccountCmpyLink.LinkType.ADMIN) {
      //get all companies where the user is the admin
      List<String> adminCmpies = new ArrayList<>();
      adminCmpies.add(cred.getCmpyId());

      if (searchType == APPConstants.SEARCH_OTHER_PENDING_TRIPS) {
        List<Trip> pTrips = Trip.findPendingByCmpyIds(sessionMgr.getUserId(), maxRows, adminCmpies, term, startPos);
        if (pTrips != null) {
          searchResults.addAll(pTrips);
        }
      }
      else if (searchType == APPConstants.SEARCH_OTHER_TRIPS) {
        List<Trip> pTrips = Trip.findActiveByCmpyIds(sessionMgr.getUserId(), maxRows, adminCmpies, term, startPos);
        if (pTrips != null) {
          searchResults.addAll(pTrips);
        }
      }
    }

    if (cred.hasGroupies()) {
      List<String> users = new ArrayList<>();
      List<String> memberCmpies = new ArrayList<>();

      List<String> groups = new ArrayList<>();
      groups.addAll(cred.getUserGroups().keySet());

      if(cred.getCmpyLinkType() == AccountCmpyLink.LinkType.ADMIN) {
        memberCmpies.add(cred.getCmpyId());
      }

      for (String groupId : groups) {
        users.addAll(cred.getUserGroups().get(groupId));
      }

      if (users.size() > 0) {
        if (searchType == APPConstants.SEARCH_OTHER_PENDING_TRIPS) {
          List<Trip> pTrips = Trip.findPendingByUserIds(maxRows, users, term, startPos, memberCmpies);
          if (pTrips != null) {
            searchResults.addAll(pTrips);
          }
        }
        else {
          List<Trip> pTrips = Trip.findActiveByUserIds(maxRows, users, term, startPos, memberCmpies);
          if (pTrips != null) {
            searchResults.addAll(pTrips);
          }
        }
      }
    }

    HashMap<String, String> userProfiles = new HashMap<>();

    if (searchResults != null) {
      ArrayList<String> userIds = new ArrayList<>();
      for (Trip t : searchResults) {
        if (t.createdby != null && !userIds.contains(t.createdby)) {
          userIds.add(t.createdby);
        }
      }
      List<UserProfile> users = UserProfile.getUserProfiles(userIds);
      for (UserProfile u : users) {
        if (!userProfiles.containsKey(u.getUserid())) {
          if (u.getFirstname() != null) {
            userProfiles.put(u.getUserid(), u.getFirstname() + " " + u.getLastname());
          }
        }
      }
    }

    return TripController.buildTripView(searchResults, cmpiesMap, userProfiles);
  }


  //public access
  public Result loadPdf() {
    final DynamicForm form = formFactory.form().bindFromRequest();
    String inTripId = form.get("inTripId");
    String hostUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);
    String inPrintFriendly = form.get("inPrint");
    Boolean isPrintFriendly = Boolean.valueOf(inPrintFriendly);


    if (inTripId == null || inTripId.length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Error";
      return ok(views.html.error.error404.render(baseView));
    }

    Trip trip = Trip.find.byId(inTripId);
    if (trip == null || trip.getStatus() == APPConstants.STATUS_DELETED) {
      BaseView baseView = new BaseView();
      baseView.message = "Error";
      return ok(views.html.error.error404.render(baseView));
    }

    //add check to see if there is a doc with merge bookings - if yes, redirect to the custom PDF
    TripDestination tripDest = TripDestination.getActiveTripDocByTripId(trip.tripid);
    if (tripDest != null && tripDest.isMergebookings()) {
      //redirect to the destination PDF
      flash(SessionConstants.SESSION_DEST_ID, tripDest.destinationid);
      flash(SessionConstants.SESSION_PARAM_TRIPID, trip.tripid);

      return redirect(routes.DestinationController.loadPdf());
    }

    BaseView view = new BaseView();

    long maxTime = System.currentTimeMillis() - (5 * 60 * 1000);
    String fileName = TripPDFActor.getFileName(trip);

    //check if there already is a PDF generation in progess
    TripPdfRec rec = (TripPdfRec) CacheMgr.get(APPConstants.CACHE_TRIP_PDF + inTripId);
    if (rec != null && (System.currentTimeMillis() - rec.startTimestamp) < APPConstants.PDF_TIMEOUT) {
      fileName = rec.fileName;
    }
    else {
      //invoke background scheduler
      final TripPDFActor.Command cmd = new TripPDFActor.Command(trip, fileName, ctx().lang(), isPrintFriendly, false, null);
      ActorsHelper.tell(SupervisorActor.UmappedActor.PDF_TRIP, cmd);

      rec = new TripPdfRec();
      rec.fileName = fileName;
      rec.startTimestamp = System.currentTimeMillis();
      CacheMgr.set(APPConstants.CACHE_TRIP_PDF + trip.tripid, rec, APPConstants.CACHE_PDF_EXPIRY_SECS);
    }

    //return refresh page
    view.url = hostUrl + "/trips/getPdf?inTripId=" + inTripId + "&inFileName=" + fileName;
    return ok(views.html.common.loadDocument.render(view));
  }

  public Result retrievePDF() {
    final DynamicForm form = formFactory.form().bindFromRequest();
    String inTripId = form.get("inTripId");
    String inFileName = form.get("inFileName");
    BaseView view = new BaseView();
    if (inTripId != null && inFileName != null) {

      TripPdfRec rec = (TripPdfRec) CacheMgr.get(APPConstants.CACHE_TRIP_PDF + inTripId);
      if (rec != null && (System.currentTimeMillis() - rec.startTimestamp) < APPConstants.PDF_TIMEOUT) {
        //sleep again
        String hostUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);
        view.url = hostUrl + "/trips/getPdf?inTripId=" + inTripId + "&inFileName=" + inFileName;
        return ok(views.html.common.loadDocument.render(view));

      }
      else if (rec == null && S3Util.doesPDFexist(inFileName)) {
        boolean skipRedirect =false;
        String browserAgent = request().getHeader("User-Agent");
        if (browserAgent != null && browserAgent.toLowerCase().contains("trident")) {
          skipRedirect = true;
        }
        view.url = S3Util.getPDFUrl(inFileName) + "?" + System.currentTimeMillis();
        return ok(views.html.common.downloadDocument.render(view, skipRedirect));
      }
    }

    view.message = "Cannot generate PDF - please contact support@umapped.com";

    return ok(views.html.error.error500.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result copyTrip(String tripId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    String     msg        = "Error Copying this Trip - please try again";

    final DynamicForm form     = formFactory.form().bindFromRequest();
    String            tripName = form.get("inTripName");

    if (tripId != null && tripName != null && tripName.trim().length() > 0) {
      Trip trip = Trip.findByPK(tripId);
      if (trip != null && SecurityMgr.getTripAccessLevel(trip, sessionMgr)
                                     .ge(SecurityMgr.AccessLevel.READ) && SecurityMgr.canAccessCmpy(trip.cmpyid,
                                                                                                    sessionMgr)) {
        List<Trip> trips = Trip.findPendingByCmpyNameType(tripName, trip.getCmpyid(), APPConstants.TOUR_TYPE);
        if (trips != null && trips.size() > 0) {
          msg = "Duplicate Trip Name - A pending trip with this name already exists.";
        }
        else {
          try {
            Trip targetTrip = trip.clone();
            targetTrip.setName(tripName);
            targetTrip.setStatus(APPConstants.STATUS_PUBLISH_PENDING);
            targetTrip.setCreatedby(sessionMgr.getUserId());
            targetTrip.setModifiedby(sessionMgr.getUserId());
            targetTrip.save();

            copyTrip(trip, targetTrip, sessionMgr.getUserId());

            //redirect to newly created trip
            sessionMgr.setParam(SessionConstants.SESSION_PARAM_TRIPID, targetTrip.tripid);
            msg = tripName + " created successfully";

            //Create audit log
            audit:
            {
              TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP, AuditActionType.ADD, AuditActorType.WEB_USER)
                                      .withTrip(targetTrip)
                                      .withCmpyid(targetTrip.cmpyid)
                                      .withUserid(sessionMgr.getUserId());

              ((AuditTrip) ta.getDetails()).withName(targetTrip.getName())
                                           .withId(targetTrip.getTripid())
                                           .withStatus(AuditTrip.TripStatus.PENDING);
              ta.save();
            }
            tripId = targetTrip.getTripid();
          }
          catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    }
    flash(SessionConstants.SESSION_PARAM_MSG, msg);
    return redirect(routes.TripController.tripInfo(tripId));
  }

  public static void copyTrip (Trip srcTrip, Trip targetTrip, String userId) throws Exception {
    try {
      HashMap <String, String> tripDetailsPKMapping = new HashMap<>();

      Ebean.beginTransaction();
      List<TripDetail> detailCopies = new ArrayList<>();
      List<TripDetail> tripDetails = TripDetail.findActiveByTripId(srcTrip.tripid);
      //copy all bookings& booking details
      for (TripDetail tripDetail : tripDetails) {
        String detailId = tripDetail.getDetailsid();
        tripDetail.prepareForReuse(userId,userId);
        tripDetail.setTripid(targetTrip.getTripid());
        tripDetail.insert();
        tripDetailsPKMapping.put(detailId, tripDetail.getDetailsid());

        //copy poi booking overrides
        List <TripDetailFile> detailFiles = TripDetailFile.getBookingFiles(detailId);
        if (detailFiles != null) {
          for (TripDetailFile file: detailFiles) {
            file.prepareForReuse(tripDetail.getDetailsid(), file.getFileId().poiFileId);
            file.insert();
          }
        }
      }

      //copy all attachments
      List<TripAttachment> tripAttachments = TripAttachment.findByTripId(srcTrip.tripid);
      for(TripAttachment attach : tripAttachments) {
        if (attach.getStatus() == APPConstants.STATUS_ACTIVE) {
          attach.prepareForReuse(userId,userId);
          attach.setTripid(targetTrip.tripid);
          attach.insert();
        }
      }

      //TODO: 2016-11-17: Figure out if this ever can be executed, if so what is the use case
      // copy custom trip document and all custom pages
      //get list of doc that already exist in target
      List<TripDestination> targetTripDests = TripDestination.getActiveByTripId(targetTrip.tripid);
      List<String> existingDestId = new ArrayList<>();
      if (targetTripDests != null) {
        for (TripDestination td: targetTripDests) {
          existingDestId.add(td.destinationid);
        }
      }

      List<TripDestination> tripDestinations = TripDestination.getActiveByTripId(srcTrip.tripid);
      for(TripDestination dest : tripDestinations) {
        if (dest.tripdestid.equals(srcTrip.getTripid())) {
          //this is a custom doc
          Destination origDest = Destination.find.byId(dest.getDestinationid());
          String origDestId = origDest.getDestinationid();

          //check to see if a custom doc already exist for the new Trip...
          TripDestination targetTripDest = TripDestination.getActiveTripDocByTripId(targetTrip.tripid);

          Destination targetDest = null;
          if (targetTripDest != null) {
            targetDest = Destination.find.byId(targetTripDest.getDestinationid());
          }

          if (targetDest == null) {
            origDest.prepareForReuse(userId,userId);
            origDest.insert();

            dest.prepareForReuse(userId,userId);
            dest.setTripid(targetTrip.tripid);
            dest.setDestinationid(origDest.getDestinationid());
            dest.setTripdestid(targetTrip.tripid);
            dest.insert();
          }

          //find the pages and link to the right one
          List<DestinationGuide> guides = DestinationGuide.findActiveDestGuides(origDestId);
          if (guides != null) {
            for (DestinationGuide guide : guides) {
              String guideId = guide.getDestinationguideid();
              guide.prepareForReuse(userId,userId);
              guide.setDestinationid(origDest.getDestinationid());
              guide.insert();

              //copy the attachments
              List<DestinationGuideAttach> attachList = DestinationGuideAttach.findActiveByDestGuide(guideId);
              if (attachList != null) {
                for (DestinationGuideAttach attach: attachList) {
                  attach.prepareForReuse(userId,userId);
                  attach.setDestinationguideid(guide.getDestinationguideid());
                  attach.insert();
                }
              }
            }
          }
        } else {
          if (!existingDestId.contains(dest.destinationid)) {
            dest.prepareForReuse(userId,userId);
            dest.setTripdestid(Utils.hash(dest.getDestinationid() + targetTrip.tripid));
            dest.setTripid(targetTrip.tripid);
            dest.insert();
          }
        }
      }

      //copy trip Notes
      List <TripNote> notes = TripNote.getNotesByTripId(srcTrip.getTripid());
      if (notes != null) {
        for (TripNote note: notes) {
          Long noteId = note.getNoteId();
          note.prepareForReuse(userId,userId);
          note.setTrip(targetTrip);
          if (note.getTripDetailId() != null) {
            note.setTripDetailId(tripDetailsPKMapping.get(note.getTripDetailId()));
          }
          note.insert();

          //copy all attachments
          List<TripNoteAttach> attachments = TripNoteAttach.findByNoteId(noteId);
          if (attachments != null) {
            for (TripNoteAttach attach : attachments) {
              attach.prepareForReuse(userId,userId);
              attach.setTripNote(note);
              attach.insert();
            }
          }
        }
      }
      Account modifiedBy = Account.findByLegacyId(targetTrip.getModifiedby());
      MessengerUpdateHelper.build(targetTrip, modifiedBy)
                           .setAddDefaultRooms(true)
                           .addRoomsToUpdate(detailCopies)
                           .update();
      //update the cache
      BookingNoteController.invalidateCache(srcTrip.tripid);
      BookingNoteController.invalidateCache(targetTrip.tripid);

    } catch (Exception e) {
      e.printStackTrace();
      Ebean.rollbackTransaction();
      throw e;
    } finally {
      Ebean.commitTransaction();
    }

  }

  @With({Authenticated.class, Authorized.class})
  public Result mergeTrip(String srcTripId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    String msg = "Error Merging this Trip - please try again";

    final DynamicForm form = formFactory.form().bindFromRequest();
    String targetTripId = form.get("inTargetTripId");

    Trip srcTrip = Trip.findByPK(srcTripId);
    Trip targetTrip = Trip.findByPK(targetTripId);

    if (srcTrip != null && SecurityMgr.getTripAccessLevel(srcTrip, sessionMgr)
                                      .ge(SecurityMgr.AccessLevel.READ) && srcTrip.status == APPConstants.STATUS_PUBLISH_PENDING && targetTrip != null && SecurityMgr
            .getTripAccessLevel(targetTrip, sessionMgr)
            .ge(SecurityMgr.AccessLevel.APPEND)) {

      try {
        copyTrip(srcTrip, targetTrip, sessionMgr.getUserId());
        //delete the src trip
        srcTrip.setStatus(APPConstants.STATUS_DELETED);
        srcTrip.setModifiedby(sessionMgr.getUserId());
        srcTrip.setLastupdatedtimestamp(System.currentTimeMillis());
        srcTrip.save();

        MessengerUpdateHelper.build(srcTrip, sessionMgr.getAccountId()).wipeTrip();

        //redirect to newly created trip
        sessionMgr.setParam(SessionConstants.SESSION_PARAM_TRIPID, targetTrip.tripid);
        msg = srcTrip.getName() + " merged to " + targetTrip.getName() + " successfully";

        //Create audit log for delete
        audit:
        {
          TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP, AuditActionType.DELETE, AuditActorType.WEB_USER)
                                  .withTrip(srcTrip)
                                  .withCmpyid(srcTrip.getCmpyid())
                                  .withUserid(sessionMgr.getUserId());

          ((AuditTrip) ta.getDetails()).withName(srcTrip.getName())
                                       .withId(srcTrip.getTripid())
                                       .withStatus(AuditTrip.TripStatus.PENDING);
          ta.save();
        }
        //Create audit log for copy

        audit:
        {
          TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP, AuditActionType.MERGE, AuditActorType.WEB_USER)
                                  .withTrip(targetTrip)
                                  .withCmpyid(targetTrip.getCmpyid())
                                  .withUserid(sessionMgr.getUserId());

          ((AuditTrip) ta.getDetails()).withName(srcTrip.getName())
                                       .withId(targetTrip.getTripid())
                                       .withStatus(AuditTrip.TripStatus.PENDING);
          ta.save();
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    if (targetTrip != null) {
      flash(SessionConstants.SESSION_PARAM_MSG, msg);
      return redirect(routes.TripController.tripInfo(targetTrip.getTripid()));
    } else {
      BaseView view = new BaseView();
      view.message = msg;
      return ok(views.html.common.message.render(view));
    }
  }

  /* get all the trips a user has access */
  @With({Authenticated.class, Authorized.class})
  public Result tripAutocomplete(String tripId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    final DynamicForm form = formFactory.form().bindFromRequest();
    String tripName = form.get("term");


    ObjectNode result = Json.newObject();
    ArrayNode array = result.arrayNode();
    long currentTime = System.currentTimeMillis();
    Trip trip = Trip.findByPK(tripId);
    if (trip != null &&
        SecurityMgr.canAccessCmpy(trip.cmpyid, sessionMgr) &&
        tripName != null && tripName.trim().length() > 0) {
      tripName = "%" + tripName.replaceAll(" ", "%") + "%";

      Connection conn = null;
      try {
        conn = DBConnectionMgr.getConnection4Publisher();
        List<String> cmpies = new ArrayList<>();
        ArrayList<Integer> statuses = new ArrayList<>();
        statuses.add(0);
        statuses.add(1);
        cmpies.add(trip.cmpyid);

        List<Trip> t2 = null;
        if (SecurityMgr.isCmpyAdmin(trip.cmpyid, sessionMgr)) {
          t2 = TripMgr.findAdminTripsIncShare(tripName, cmpies, sessionMgr.getUserId(), 10, 0, statuses, conn);
        }
        else {
          t2 = TripMgr.findMyTripsIncShare(tripName, cmpies, sessionMgr.getUserId(), 10, 0, statuses, conn);
        }

        if (t2 != null) {
          for (Trip t : t2) {
            if (t.endtimestamp > currentTime && !t.tripid.equals(trip.tripid)) {
              StringBuilder sb = new StringBuilder();
              sb.append(t.name);
              sb.append("  (");
              sb.append(Utils.getDayMonthStringPrint(t.getStarttimestamp()));
              sb.append(" - ");
              sb.append(Utils.getDayMonthStringPrint(t.getEndtimestamp()));
              sb.append(")");

              ObjectNode classNode = Json.newObject();
              classNode.put("label", sb.toString());
              classNode.put("id", t.tripid);
              array.add(classNode);
            }
          }
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
      finally {
        if (conn != null) {
          try {
            conn.close();
          }
          catch (Exception e) {
            e.printStackTrace();
          }
        }
      }


    }
    return ok(array);
  }

  /* get all the trips a user has access */
  @With({Authenticated.class, Authorized.class})
  public Result tripAutocompleteSearch() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    int offsetMins = sessionMgr.getTimezoneOffsetMins();

    long currentClientTime = Utils.getCurrentClientDate(offsetMins);

    final DynamicForm form = formFactory.form().bindFromRequest();
    String tripName = form.get("term");


    ObjectNode result = Json.newObject();
    ArrayNode array = result.arrayNode();
    long currentTime = System.currentTimeMillis();
    String cmpyid = sessionMgr.getCredentials().getMainCmpy();
    if (cmpyid != null && SecurityMgr.canAccessCmpy(cmpyid, sessionMgr) && tripName != null && tripName.trim()
                                                                                                       .length() > 0) {
      tripName = "%" + tripName.replaceAll(" ", "%") + "%";

      Connection conn = null;
      try {
        conn = DBConnectionMgr.getConnection4Publisher();
        List<String> cmpies = new ArrayList<>();
        ArrayList<Integer> statuses = new ArrayList<>();
        statuses.add(0);
        statuses.add(1);
        cmpies.add(cmpyid);

        List<Trip> t2 = null;
        if (SecurityMgr.isCmpyAdmin(cmpyid, sessionMgr)) {
          t2 = Trip.findActiveNameCmpyId(10, cmpyid, tripName);
        }
        else {
          t2 = TripMgr.findMyTrips(tripName, cmpies, sessionMgr.getUserId(), 10, 0, statuses, conn, "tripName", "asc", FilterType.ACTIVE, currentClientTime);
        }

        if (t2 != null) {
          for (Trip t : t2) {
            if (t.endtimestamp > currentTime ) {
              StringBuilder sb = new StringBuilder();
              sb.append(t.name);
              sb.append("  (");
              sb.append(Utils.getDayMonthStringPrint(t.getStarttimestamp()));
              sb.append(" - ");
              sb.append(Utils.getDayMonthStringPrint(t.getEndtimestamp()));
              sb.append(")");

              ObjectNode classNode = Json.newObject();
              classNode.put("label", sb.toString());
              classNode.put("id", t.tripid);
              array.add(classNode);
            }
          }
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
      finally {
        if (conn != null) {
          try {
            conn.close();
          }
          catch (Exception e) {
            e.printStackTrace();
          }
        }
      }


    }
    return ok(array);
  }

  public static class flightDetails {
    public String term;
    public String cmpy;
  }

  @With({Authenticated.class, Authorized.class})
  public Result importRecordLocator(String tripId) {
    final SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<RecordLocatorForm> form = formFactory.form(RecordLocatorForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    RecordLocatorForm formInfo = form.get();
    if (formInfo.getInProvider() == RecordLocatorForm.Provider.AMADEUS && (formInfo.getInFirstName() == null || formInfo.getInFirstName().isEmpty())) {
      return UserMessage.message( "Please enter a first name for Amadeus record locators");
    }

    Trip trip = Trip.findByPK(tripId);
    final String recLocator = formInfo.getInRecordLocator();

    if (trip != null &&
        SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND) &&
        recLocator != null &&
        recLocator.trim().length() > 0 && formInfo.getInLastName() != null &&
        formInfo.getInLastName().trim().length() > 0) {
      try {
        CacheMgr.set(APPConstants.CACHE_REC_LOCATOR_WEB + formInfo.getInRecordLocator()+sessionMgr.getUserId(), null);
        formInfo.setInTripId(tripId);
        formInfo.setUserId(sessionMgr.getUserId());

        final RecLocatorActor.Command cmd = new RecLocatorActor.Command(formInfo);
        ActorsHelper.tell(SupervisorActor.UmappedActor.RECORD_LOCATOR, cmd);

        Source<ByteString, ?> source = Source.<ByteString>actorRef(65535, OverflowStrategy.dropNew())
                .mapMaterializedValue(sourceActor -> {

                  StopWatch sw = new StopWatch();
                  sw.start();
                  boolean timeout = true;
                  while (sw.getTime() < 150000) {
                    try {
                      Thread.sleep(1000);
                      Object o = CacheMgr.get(APPConstants.CACHE_REC_LOCATOR_WEB + recLocator + sessionMgr.getUserId());
                      if (o != null) {
                        if (o instanceof BaseView) {
                          BaseView baseView = (BaseView) o;
                          String s = views.html.common.message.render(baseView).toString();
                          sourceActor.tell(ByteString.fromString(s), null);
                        }
                        else {
                          BaseView baseView = new BaseView();
                          baseView.message = "Error Retrieving the Record Locator - please contact support@umapped.com if the problem persists";
                          String s = views.html.common.message.render(baseView).toString();
                          sourceActor.tell(ByteString.fromString(s), null);
                        }
                        timeout = false;
                        break;
                      }
                      else {
                        sourceActor.tell(ByteString.fromString(" "), null);
                      }
                    }
                    catch (Exception e) {
                    }
                  }
                  if (timeout) {
                    BaseView baseView = new BaseView();
                    baseView.message = "Error Retrieving the Record Locator - please contact support@umapped.com if the problem persists";
                    String s = views.html.common.message.render(baseView).toString();
                    sourceActor.tell(ByteString.fromString(s), null);
                  }
                  sourceActor.tell(new Status.Success(NotUsed.getInstance()), null);
                  return null;
                });
        // Serves this stream with 200 OK
        return ok().chunked(source);

      }
      catch (Exception e) {
        e.printStackTrace();
        BaseView baseView = new BaseView();
        baseView.message = "Error Retrieving the Record Locator - please contact support@umapped.com if the problem persists";
        return ok(views.html.common.message.render(baseView));
      }

    } else {
      BaseView baseView = new BaseView();
      baseView.message = "Error Retrieving the Record Locator - please contact support@umapped.com if the problem persists";
      return ok(views.html.common.message.render(baseView));
    }
  }


  @With({Authenticated.class, Authorized.class})
  public Result travelerAutocomplete() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    final DynamicForm form = formFactory.form().bindFromRequest();
    ObjectNode result = Json.newObject();
    ArrayNode array = result.arrayNode();

    String email = form.get("term");
    if (email != null) {
      email = "%" + email + "%";
      Account loggedin = Account.findByLegacyId(sessionMgr.getUserId());
      List<Account> accounts = Account.passengersByEmail(email, loggedin.getUid());
      if (accounts != null && accounts.size() > 0) {
        for (Account a: accounts) {
          ObjectNode classNode = Json.newObject();
          ObjectNode element = result.objectNode();
          classNode.put("label", a.getEmail());
          element.put("firstName", a.getFirstName());
          element.put("lastName", a.getLastName());

          if (a.getMsisdn() == null) {
            element.put("msisdn", "");
          } else {
            element.put("msisdn", a.getMsisdn());
          }

          classNode.put("id", element.toString());
          array.add(classNode);

        }
      }
    }

    return ok(array);

  }

  public static List<TripDetail> reorderBookings (List <TripDetail> bookings, List<DateTime> dateTimes) {
    if (bookings != null && bookings.size() > 0 && dateTimes != null && dateTimes.size() > 0) {
      //there are flights that crossed the date lines
      List<TripDetail> newFlights = new ArrayList<>();

      // reorder the flights for that day so they are chronological

      for (DateTime dt : dateTimes) {
        //find flights on that day to process...
        ArrayList<TripDetail> sameDay = new ArrayList<>();
        ArrayList<TripDetail> sameDayOrdered = new ArrayList<>();

        TripDetail crossZoneFlight = null;
        for (TripDetail f : bookings) {
          if (f.starttimestamp.longValue() >= dt.toDateMidnight().getMillis() &&
              f.starttimestamp.longValue() < dt.plusDays(1).toDateMidnight().getMillis()) {
            sameDay.add(f);
            if (f.endtimestamp.longValue() > 0 && f.endtimestamp.longValue() < f.starttimestamp.longValue()) {
              crossZoneFlight = f;
              sameDayOrdered.add(f);
            }
          }
        }

        if (crossZoneFlight != null) {
          for (TripDetail v : sameDay) {
            if (v.starttimestamp.longValue() > crossZoneFlight.endtimestamp.longValue() &&
                (v.getLocFinishPoiId() != null &&
                 crossZoneFlight.getLocStartPoiId() != null &&
                 !v.getLocFinishPoiId().equals(
                crossZoneFlight.getLocStartPoiId()))) {
              //flight is after the cross zone flight
              sameDayOrdered.add(v);
            }
            else {
              sameDayOrdered.add(sameDayOrdered.indexOf(crossZoneFlight), v);
            }
            if (!v.getDetailsid().equals(crossZoneFlight.getDetailsid())) {
              bookings.remove(v);
            }
          }

          //remove all flights and replace the order
          int indexOfCrosszoneFlight = sameDayOrdered.indexOf(crossZoneFlight);
          for (TripDetail td : sameDayOrdered) {
            if (!td.getDetailsid().equals(crossZoneFlight.getDetailsid())) {
              int index = sameDayOrdered.indexOf(td);
              if (index < indexOfCrosszoneFlight) {
                //insert before in the bookings array
                bookings.add(bookings.indexOf(crossZoneFlight), td);

              } else {
                //insert after in the bookings array
                if (bookings.size() == bookings.indexOf(crossZoneFlight) + 1) {
                  bookings.add(td);
                } else {
                  bookings.add(bookings.indexOf(crossZoneFlight) + 1, td);
                }
              }
            }
          }


        }
      }
    }

    return  bookings;
  }


  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result publishedTrips() {
    JsonNode jsonRequest = request().body().asJson();
    DataTablesForm dtRequest = Json.fromJson(jsonRequest, DataTablesForm.class);
    SessionMgr    sessionMgr = SessionMgr.fromContext(ctx());

    class TripRow {
      public String imgUrl;
      public String tripName;
      public String id;
    }

    int offsetMins = sessionMgr.getTimezoneOffsetMins();

    long currentClientTime = Utils.getCurrentClientDate(offsetMins);

    DataTablesView<TripRow> dtv = new DataTablesView<>(dtRequest.draw);
    dtv.recordsTotal    = Trip.getARowCountByUserUpcomingTrips(sessionMgr.getUserId(), currentClientTime);
    dtv.recordsFiltered = Trip.getARowCountByUserUpcomingTripsFiltered(sessionMgr.getUserId(), dtRequest.search.value, currentClientTime);
    List<Trip> recs = Trip.getPublishedTripPage(sessionMgr.getUserId(),
                                                dtRequest.search.value,
                                                dtRequest.start,
                                                dtRequest.length, currentClientTime);
    for (Trip trip : recs) {
      TripRow row = new TripRow();
      row.id =  routes.TripController.tripInfo(trip.getTripid()).url();
      row.tripName = trip.getName() + "<br>" + Utils.formatDatePrint(trip.getStarttimestamp()) + " - " + Utils.formatDatePrint(trip.getEndtimestamp());
      row.imgUrl = trip.getImageUrl();
      dtv.data.add(row);
    }

    return  ok(Json.toJson(dtv));
  }

  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result pendingTrips() {
    JsonNode jsonRequest = request().body().asJson();
    DataTablesForm dtRequest = Json.fromJson(jsonRequest, DataTablesForm.class);
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    class TripRow {
      public String imgUrl;
      public String tripName;
      public String id;
    }

    DataTablesView<TripRow> dtv = new DataTablesView<>(dtRequest.draw);
    dtv.recordsTotal = Trip.getARowCountByUserPendingTrips(sessionMgr.getUserId());
    dtv.recordsFiltered = Trip.getARowCountByUserPendingTripsFiltered(sessionMgr.getUserId(), dtRequest.search.value);
    List<Trip> recs = Trip.getPendingTripPage(sessionMgr.getUserId(), dtRequest.search.value, dtRequest.start, dtRequest.length);
    long currentTime = System.currentTimeMillis();
    for (Trip trip : recs) {
      TripRow row = new TripRow();
      row.id = routes.TripController.tripInfo(trip.getTripid()).url();
      row.tripName = trip.getName() + "<br>" + Utils.formatDatePrint(trip.getStarttimestamp()) + " - " + Utils.formatDatePrint(
          trip.getEndtimestamp());
      row.imgUrl = trip.getImageUrl();
      dtv.data.add(row);
    }

    return ok(Json.toJson(dtv));
  }
}

