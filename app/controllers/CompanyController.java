package controllers;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.AdminAccess;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.*;
import com.mapped.publisher.js.BaseJsonResponse;
import com.mapped.publisher.persistence.PoiMgr;
import com.mapped.publisher.persistence.UserProfileMgr;
import com.mapped.publisher.persistence.UserProfileRS;
import com.mapped.publisher.utils.*;
import com.mapped.publisher.view.*;
import com.mapped.publisher.view.billing.BillingInfoView;
import com.umapped.api.schema.common.RequestHeaderJson;
import com.umapped.api.schema.common.RequestMessageJson;
import com.umapped.api.schema.common.ResponseMessageJson;
import com.umapped.api.schema.local.CompanyJson;
import com.umapped.api.schema.local.ConsortiumJson;
import com.umapped.api.schema.types.Operation;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.helper.UmappedEmailFactory;
import it.innove.play.pdf.PdfGenerator;
import models.publisher.*;
import org.joda.time.DateTime;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.db.DB;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;

import java.sql.Connection;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-03
 * Time: 2:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class CompanyController
    extends Controller {

  @Inject
  PdfGenerator pdfGenerator;
  @Inject
  FormFactory  formFactory;

  @With({Authenticated.class, Authorized.class})
  public Result companies() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Credentials cred = sessionMgr.getCredentials();

    if (!cred.isUmappedAdmin() && !cred.isCmpyAdmin()) {
      return  UserMessage.message(ReturnCode.AUTH_ACCOUNT_FAIL, "Unauthorized Action");
    }

    //if there is only 1 company, redirect to details page
    if (cred.isCmpyAdmin() && !cred.isUmappedAdmin()) {
      redirect(controllers.routes.CompanyController.viewCompany(cred.getCmpyId()));
    }

    CmpiesView cmpiesView = new CmpiesView();
    cmpiesView.isUMappedAdmin = SecurityMgr.isUmappedAdmin(sessionMgr);

    final DynamicForm form = formFactory.form().bindFromRequest();
    String term = form.get("inTerm");

    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    cmpiesView.message = msg;
    List<Company> companies = null;

    if (term == null || term.length() == 0) {
      if (cred.isUmappedAdmin()) {
        companies = Company.findMostRecentActive();
      }
      else {
        companies = new ArrayList<>();
        companies.add(Company.find.byId(cred.getCmpyId()));
      }
    }
    else {
      companies = Company.findByKeyword(term);
      cmpiesView.term = term;
      if (companies == null || companies.size() == 0) {
        cmpiesView.message = "No Results Found.";
      }
    }

    if (companies != null) {
      List<CmpyInfoView> cmpyInfoViews = CompanyController.buildCmpyInfoView(companies, sessionMgr);
      cmpiesView.cmpies = cmpyInfoViews;
    }

    return ok(views.html.cmpy.cmpies.render(cmpiesView));
  }


  @With({Authenticated.class, AdminAccess.class})
  public Result cmpysAdmin() {
    BaseView bv = new BaseView();
    return ok(views.html.cmpy.cmpiesAdmin.render(bv));
  }


  @With({Authenticated.class, AdminAccess.class})
  public Result godMode(String cmpyid, String mode) {
    Company cmpy = Company.find.byId(cmpyid);
    AccountCmpyLink.LinkType linkMode = AccountCmpyLink.LinkType.valueOf(mode);
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    SecurityMgr.godMode(cmpy, linkMode, sessionMgr.getCredentials());
    return redirect(routes.Application.home());
  }

  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, AdminAccess.class})
  public Result cmpysAdminDT() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    JsonNode jsonRequest = request().body().asJson();
    DataTablesForm dtRequest = Json.fromJson(jsonRequest, DataTablesForm.class);

    class CmpyRow {
      public String name;
      public String address;
      public String contact;
      public String type;
      public Map<String, String> actions;

      public CmpyRow() {
        actions = new TreeMap<>();
      }
    }
    DataTablesView<CmpyRow> dtv = new DataTablesView<>(dtRequest.draw);

    dtv.recordsTotal = Company.getRowCount();

    dtv.recordsFiltered = Company.getRowCountFiltered(dtRequest.search.value);

    List<Company> recs = Company.getFilteredPage(dtRequest.start, dtRequest.length, dtRequest.search.value);

    List<CmpyInfoView> cmpyInfoViews = CompanyController.buildCmpyInfoView(recs, sessionMgr);

    for (CmpyInfoView c : cmpyInfoViews) {
      CmpyRow row = new CmpyRow();
      row.name = c.cmpyName;
      row.address = c.getAddressShort();
      row.contact = c.getContactShort();
      row.type = c.getCompanyType();
      row.actions.put("Edit", controllers.routes.CompanyController.viewCompany(c.cmpyId).url());
      row.actions.put("Go as Admin",
                      controllers.routes.CompanyController.godMode(c.cmpyId, AccountCmpyLink.LinkType.ADMIN.name()).url());
      row.actions.put("Go as Power",
                      controllers.routes.CompanyController.godMode(c.cmpyId, AccountCmpyLink.LinkType.POWER.name()).url());
      row.actions.put("Go as Member",
                      controllers.routes.CompanyController.godMode(c.cmpyId, AccountCmpyLink.LinkType.MEMBER.name()).url());
      dtv.data.add(row);
    }

    return  ok(Json.toJson(dtv));
  }

  @With({Authenticated.class, Authorized.class})
  public Result viewCompany(String cmpyId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    CmpyInfoView cmpyView = new CmpyInfoView();
    ConsortiaView consView = new ConsortiaView();
    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    cmpyView.message = msg;
    cmpyView.isUMappedAdmin = SecurityMgr.isUmappedAdmin(sessionMgr);

    //bind html form to form bean
    Form<CompanyForm> cmpyForm = formFactory.form(CompanyForm.class);
    CompanyForm cmpyInfoForm = cmpyForm.bindFromRequest().get();

    if (cmpyId == null) {
      /* Billing data */
      cmpyView.billingInfo = new BillingInfoView((BillingSettingsForCmpy)null);
      cmpyView.billingInfo.availableSchedules = BillingSchedule.getSchedulesForCmpy();
      cmpyView.billingInfo.availablePlans = BillingPlan.plansPerSchedule(cmpyView.billingInfo.availableSchedules.get(0).getName());

      cmpyId = cmpyInfoForm.getInCmpyId();
    }

    if (cmpyId == null) {
      return  ok(views.html.cmpy.cmpy.render(cmpyView, consView));
    }

    Company cmpy = Company.find.byId(cmpyId);
    if (cmpy != null && SecurityMgr.canEditCmpy(cmpy, sessionMgr)) {
      cmpyView.cmpyId = cmpy.cmpyid;
      cmpyView.cmpyName = cmpy.name;
      cmpyView.comments = cmpy.comment;
      cmpyView.logoName = cmpy.logoname;
      cmpyView.logoUrl = cmpy.logourl;
      cmpyView.status = cmpy.status;
      cmpyView.tag = cmpy.getTag();
      cmpyView.cmpyType = cmpy.type;
      cmpyView.contact = cmpy.contact;
      cmpyView.numAccounts = cmpy.numaccounts;
      cmpyView.numTrips = cmpy.maxtrips;
      cmpyView.expiryDate = Utils.formatDateControl(cmpy.expirytimestamp);
      cmpyView.itineraryCheck = cmpy.itinerarycheck;
      cmpyView.enableCollaboration = cmpy.enablecollaboration;
      if (cmpy.acceptedagreementver != null &&
          Agreement.isActive(cmpy.acceptedagreementver) &&
          (cmpyView.isUMappedAdmin ||
           (cmpy.authorizeduserid != null && cmpy.authorizeduserid.equals(sessionMgr.getUserId())))) {
        cmpyView.currentAgreementId = cmpy.acceptedagreementver;
      }
      //check for api access
      cmpyView.canAccessApi = cmpy.allowapi;
      List<EmailToken> tkns = EmailToken.getCompanyActiveTokens(cmpy.getCmpyid());
      if (cmpy.allowapi && tkns != null &&tkns.size() == 1) {
        EmailToken tkn = tkns.get(0);

        if (ConfigMgr.getInstance().isProd()) {
          cmpyView.apiAccessCode = tkn.getToken() + "@api.umapped.com";
        }
        else {
          cmpyView.apiAccessCode = tkn.getToken() + "@testapi.umapped.com";
        }
      }

      if (cmpyView.isUMappedAdmin) {
        /* BILLING */
        if (SecurityMgr.isUmappedAdmin(sessionMgr)) {
          BillingSettingsForCmpy bsc = BillingSettingsForCmpy.find.byId(cmpyId);
          BillingInfoView biv = new BillingInfoView(bsc);
          biv.availableSchedules = BillingSchedule.getSchedulesForCmpy();

          if (bsc == null) { //Cmpy with no billing record
            biv.availablePlans = BillingPlan.plansPerSchedule(biv.availableSchedules.get(0).getName());
          } else {
            biv.availablePlans = BillingPlan.plansPerSchedule(biv.bsCmpy.getSchedule().getName());

          }
          cmpyView.billingInfo = biv;
        }
        /* END OF BILLING */




        cmpyView.flightTracking = cmpy.flighttracking;
        cmpyView.billingType = cmpy.billingtype;

        Map<String, CmpyAgreement> cmpyAgreements = new HashMap<>();

        List<CmpyAgreement> cmpyAgreements1 = CmpyAgreement.findByCmpyId(cmpy.cmpyid);
        for (CmpyAgreement ca : cmpyAgreements1) {
          if (!cmpyAgreements.containsKey(ca.agreementversion)) {
            cmpyAgreements.put(ca.agreementversion, ca);
          }
        }

        //get agreement info
        Agreement acceptedAgreement = null;
        cmpyView.acceptedAgreementVersion = APPConstants.NOT_APPLICABLE;

        if (cmpy.getAcceptedagreementver() != null) {
          acceptedAgreement = Agreement.find.byId(cmpy.getAcceptedagreementver());
          if (acceptedAgreement != null) {
            if (cmpyAgreements.containsKey(cmpy.getAcceptedagreementver())) {
              CmpyAgreement agreement = cmpyAgreements.get(cmpy.acceptedagreementver);
              if (agreement.status == APPConstants.STATUS_ACCEPTED) {
                cmpyView.acceptedAgreementVersion = "Accepted - " + acceptedAgreement.getAgreementversion()
                                                    + " - "
                                                    + acceptedAgreement.getName();
                long localTimezone = agreement.getCreatedtimestamp() +
                                     (sessionMgr.getTimezoneOffsetMins() * 60 * 1000);
                cmpyView.acceptedAgreementDate = "Accepted on: " + Utils.formatTimestampPrint(localTimezone);

              }
              else {
                cmpyView.acceptedAgreementVersion = "Declined - " + acceptedAgreement.getAgreementversion()
                                                    + " - " + acceptedAgreement.getName();
                long localTimezone = agreement.getCreatedtimestamp()+
                                     (sessionMgr.getTimezoneOffsetMins() * 60 * 1000);

                cmpyView.acceptedAgreementDate = "Declined on: " + Utils.formatTimestampPrint(localTimezone);
              }
            }
            else {
              cmpyView.acceptedAgreementVersion = "Pending";
            }
          }
        }

        cmpyView.agreements = new HashMap<String, String>();
        List<Agreement> activeAgreements = Agreement.activeAgreements();
        if (activeAgreements != null) {
          for (Agreement a : activeAgreements) {
            cmpyView.agreements.put(a.agreementversion, a.agreementversion + " - " + a.name);
          }
        }
        cmpyView.agreements.put(APPConstants.NOT_APPLICABLE, APPConstants.NOT_APPLICABLE);


        if (cmpy.getTargetagreementver() != null) {
          cmpyView.selectedAgreementVersion = cmpy.getTargetagreementver();
        }
        else {
          cmpyView.selectedAgreementVersion = APPConstants.NOT_APPLICABLE;
        }

        Connection conn = null;
        cmpyView.adminUsers = new ArrayList<>();
        UserView defaultUser = new UserView();
        defaultUser.userId = APPConstants.NOT_APPLICABLE;
        cmpyView.adminUsers.add(defaultUser);
        cmpyView.authorizedUserId = APPConstants.NOT_APPLICABLE;

        try {
          conn = DBConnectionMgr.getConnection4Publisher();
          List<UserProfileRS> adminUsers = UserProfileMgr.findAdminByCmpy(cmpy.cmpyid, conn);
          if (adminUsers != null) {
            for (UserProfileRS u : adminUsers) {
              UserView uv = new UserView();
              uv.userId = u.getUserid();
              uv.firstName = u.getFirstname();
              uv.lastName = u.getLastname();

              if (uv.firstName != null) {
                uv.name = uv.firstName + " ";
              }
              else {
                uv.name = "";
              }

              if (u.isCmpyIdOwner == 1 && u.cmpyId.equals(cmpy.cmpyid)) {
                uv.cmpyIdOwner = u.cmpyId;
              }
              else {
                uv.cmpyIdOwner = "";
              }

              uv.cmpyLinkType = u.getCmpyLinkType();

              if (uv.lastName != null) {
                uv.name = uv.name + uv.lastName;
              }
              cmpyView.adminUsers.add(uv);
              if (cmpy.getAuthorizeduserid() != null && cmpy.getAuthorizeduserid().equals(uv.userId)) {
                cmpyView.authorizedUserId = uv.userId;
              }
            }
          }
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        finally {
          try {
            if (conn != null) {
              conn.close();
            }
          }
          catch (Exception e) {
            e.printStackTrace();
          }
        }


        if (cmpy.allowapi) {
          //get api rules:
          List<CmpyApiParser> parsers = CmpyApiParser.findByCmpy(cmpy.cmpyid);
          cmpyView.cmpyParsers = new ArrayList<>();
          for (CmpyApiParser parser : parsers) {
            CmpyApiParserView parserView = new CmpyApiParserView();
            parserView.filePattern = parser.filepattern;
            parserView.name = parser.name;
            parserView.params = parser.params;
            parserView.parser = parser.parser;
            parserView.pk = parser.pk;
            parserView.parserType = parser.parsertype.ordinal();
            parserView.cmpyId = parser.cmpyid;
            cmpyView.cmpyParsers.add(parserView);
          }
        }
      }
      // CMPY Api tokens
      cmpyView.apicmpyTokens = ApiBookingController.getApiCmpyTokens(cmpy.cmpyid);
      if (cmpyInfoForm.getApiCmpyTokenPK() != null && cmpyInfoForm.getApiCmpyTokenPK() > 0 && cmpyView.apicmpyTokens != null ) {
        for (ApiCmpyTokenView v : cmpyView.apicmpyTokens) {
          if (v.pk == cmpyInfoForm.getApiCmpyTokenPK().longValue()) {
            cmpyView.selectedApiCmpyToken = v;
            break;
          }
        }
      }
      cmpyView.apiSrcTypeList = BkApiSrc.getAll();

      List<CmpyAddress> addresses = CmpyAddress.findMainActiveByCmpyId(cmpy.cmpyid);
      if (addresses != null && addresses.size() > 0) {
        CmpyAddress address = addresses.get(0);
        PoiAddressView adrView = new PoiAddressView();
        adrView.id = address.cmpyaddrid;
        adrView.city = address.city;
        adrView.state = address.state;
        adrView.country = address.country;
        adrView.email = address.email;
        adrView.facebook = address.facebook;
        adrView.twitter = address.twitter;
        adrView.phone = address.phone;
        adrView.web = address.web;
        adrView.fax = address.fax;
        adrView.streetAddr1 = address.streetaddr1;
        adrView.streetAddr2 = address.streetaddr2;
        adrView.zip = address.zipcode;
        adrView.formatAddr1 = address.formatAddress1();
        adrView.formatAddr2 = address.formatAddress2();
        adrView.locLat = String.valueOf(address.loclat);
        adrView.locLong = String.valueOf(address.loclong);
        cmpyView.address = adrView;
      }

      //get parent cmpy link
      cmpyView.isParentCmpy = Company.isParentCmpy(cmpy.getCmpyid());
      if (cmpy.getParentCmpyId() != null && cmpy.getParentCmpyId().trim().length() > 0) {
        Company parentCompany = Company.find.byId(cmpy.getParentCmpyId());
        if (parentCompany != null) {
          cmpyView.parentCmpyId = parentCompany.getCmpyId();
          cmpyView.parentCmpyName = parentCompany.getName();
        }
      }

      //get consortium if any
      List<ConsortiumCompany> consortiums = ConsortiumCompany.findByCmpyId(cmpy.getCmpyId());
      cmpyView.cmpyConsortiaMap = new LinkedHashMap<>();
      if (consortiums != null && consortiums.size() > 0) {
        for (ConsortiumCompany cc : consortiums) {
          Consortium consortium = Consortium.find.byId(cc.getConsId());
          if (consortium != null) {
            cmpyView.cmpyConsortiaMap.put(consortium.getConsId(), consortium.getName());
          }
        }
      }

      List<Consortium> consortia = Consortium.findAllSorted("name asc");
      consView.consortiaMap = new LinkedHashMap<>();
      if (consortia != null && consortia.size() > 0) {
        for (Consortium c : consortia) {
          consView.consortiaMap.put(c.getConsId(), c.getName());
        }
      }

      CompanySmtp cmpySmtp = CompanySmtp.findByPK(cmpy.getCmpyid(), 0);
      if (cmpySmtp != null) {
        cmpyView.emailConfig = new EmailConfigView();
        cmpyView.emailConfig.host = cmpySmtp.getHost();
        cmpyView.emailConfig.port = cmpySmtp.getPort();
        cmpyView.emailConfig.username = cmpySmtp.getUsername();
        cmpyView.emailConfig.password = cmpySmtp.getPassword();
        cmpyView.emailConfig.emailDefault = cmpySmtp.getEmailDefault();
        cmpyView.emailConfig.emailNotice = cmpySmtp.getEmailNotice();
      }


    }
    return ok(views.html.cmpy.cmpy.render(cmpyView, consView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result groups() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    GroupsView groupsView = new GroupsView();

    groupsView.isUMappedAdmin = SecurityMgr.isUmappedAdmin(sessionMgr);
    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      groupsView.message = msg;
    }
    //bind html form to form bean
    Form<GroupForm> groupForm = formFactory.form(GroupForm.class);
    GroupForm groupInfoForm = groupForm.bindFromRequest().get();

    if (groupInfoForm.getInCmpyId() != null) {
      Company cmpy = Company.find.byId(groupInfoForm.getInCmpyId());
      if (cmpy != null && (SecurityMgr.isCmpyAdmin(cmpy, sessionMgr) || SecurityMgr.isUmappedAdmin(sessionMgr))) {
        groupsView.cmpyName = cmpy.name;
        if (SecurityMgr.isCmpyAdmin(cmpy, sessionMgr)) {
          groupsView.isCmpyAdmin = true;
        }

        //get all users
        Connection conn = null;
        try {
          conn = DBConnectionMgr.getConnection4Publisher();
          List<UserProfileRS> users = UserProfileMgr.findByCmpy(cmpy.cmpyid, conn);
          if (users != null && users.size() > 0) {
            ArrayList<UserView> userViews = new ArrayList<UserView>();
            for (UserProfileRS u : users) {
              UserView uv = new UserView();
              uv.userId = u.getUserid();
              uv.firstName = u.getFirstname();
              uv.lastName = u.getLastname();
              uv.email = u.getEmail();
              uv.phone = u.getPhone();
              uv.mobile = u.getMobile();
              uv.fax = u.getFax();
              uv.web = u.getWeb();
              uv.facebook = u.getFacebook();
              uv.twitter = u.getTwitter();
              uv.profilePhoto = u.getProfilephoto();
              uv.profilePhotoUrl = u.getProfilephotourl();
              Timestamp lastLoginTs = AccountAuth.getLastSuccessLoginTs(u.getUserid());
              uv.lastLoginTimestamp = lastLoginTs != null ? String.valueOf(lastLoginTs.getTime()) : "";
              uv.lastLoginPrint = lastLoginTs != null ? Utils.formatTimestampPrint(lastLoginTs.getTime()) : "";
              if (uv.firstName != null) {
                uv.name = uv.firstName + " ";
              }
              else {
                uv.name = "";
              }

              if (uv.lastName != null) {
                uv.name = uv.name + uv.lastName;
              }
              userViews.add(uv);

            }
            groupsView.users = userViews;
          }
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        finally {
          try {
            if (conn != null) {
              conn.close();
            }
          }
          catch (Exception s) {
            s.printStackTrace();
          }

        }


        List<CmpyGroup> groups = CmpyGroup.findByCmpy(cmpy.cmpyid);
        groupsView.cmpyId = cmpy.cmpyid;
        if (groups != null && groups.size() > 0) {
          int count = 0;
          groupsView.groups = new ArrayList<GroupView>();
          for (CmpyGroup g : groups) {
            GroupView gv = new GroupView();
            gv.groupId = g.groupid;
            gv.name = g.name;
            gv.comment = g.comment;
            gv.groupIndex = String.valueOf(count);
            count++;


            try {
              conn = DBConnectionMgr.getConnection4Publisher();
              List<UserProfileRS> users = UserProfileMgr.findByGroup(g.groupid, conn);
              if (users != null && users.size() > 0) {
                ArrayList<UserView> userViews = new ArrayList<UserView>();
                for (UserProfile u : users) {
                  UserView uv = new UserView();
                  uv.userId = u.getUserid();
                  uv.firstName = u.getFirstname();
                  uv.lastName = u.getLastname();
                  uv.email = u.getEmail();
                  uv.phone = u.getPhone();
                  uv.mobile = u.getMobile();
                  uv.fax = u.getFax();
                  uv.web = u.getWeb();
                  uv.facebook = u.getFacebook();
                  uv.twitter = u.getTwitter();
                  uv.profilePhoto = u.getProfilephoto();
                  uv.profilePhotoUrl = u.getProfilephotourl();
                  Timestamp lastLoginTs = AccountAuth.getLastSuccessLoginTs(u.getUserid());
                  uv.lastLoginTimestamp = lastLoginTs != null ? String.valueOf(lastLoginTs.getTime()) : "";
                  uv.lastLoginPrint = lastLoginTs != null ? Utils.formatTimestampPrint(lastLoginTs.getTime()) : "";
                  if (uv.firstName != null) {
                    uv.name = uv.firstName + " ";
                  }
                  else {
                    uv.name = "";
                  }

                  if (uv.lastName != null) {
                    uv.name = uv.name + uv.lastName;
                  }
                  userViews.add(uv);

                }
                gv.users = userViews;
              }
            }
            catch (Exception e) {
              e.printStackTrace();
            }
            finally {
              try {
                if (conn != null) {
                  conn.close();
                }
              }
              catch (Exception s) {
                s.printStackTrace();
              }

            }
            groupsView.groups.add(gv);
          }
        }
        return ok(views.html.cmpy.groups.render(groupsView));
      }


    }

    BaseView view = new BaseView();
    view.message = "System Error - cannot be updated";
    return ok(views.html.common.message.render(view));
  }

  //POSTs
  @With({Authenticated.class, Authorized.class})
  public Result editCompany() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    //bind html form to form bean
    Form<CompanyForm> cmpyForm = formFactory.form(CompanyForm.class);
    CompanyForm cmpyInfoForm = cmpyForm.bindFromRequest().get();

    Company cmpy = null;
    boolean update = true;
    if (cmpyInfoForm.getInCmpyId() != null) {
      cmpy = Company.find.byId(cmpyInfoForm.getInCmpyId());
      if (cmpy != null && !SecurityMgr.canEditCmpy(cmpy, sessionMgr)) {
        BaseView view = new BaseView();
        view.message = "System Error - cannot be updated";
        return ok(views.html.common.message.render(view));
      }
    }

    if (cmpy == null) {
      if (SecurityMgr.isUmappedAdmin(sessionMgr)) {
        cmpy = Company.buildCompany(sessionMgr.getUserId());
        update = false;
      }
      else {
        BaseView view = new BaseView();
        view.message = "Unauthorized Action";
        return ok(views.html.common.message.render(view));
      }
    }

    try {
      Ebean.beginTransaction();
      cmpy.setName(cmpyInfoForm.getInName().trim());
      cmpy.setComment(cmpyInfoForm.getInComment());
      cmpy.setContact(cmpyInfoForm.getInContact());
      cmpy.setLastupdatedtimestamp(System.currentTimeMillis());
      cmpy.setModifiedby(sessionMgr.getUserId());
      cmpy.setItinerarycheck(cmpyInfoForm.getInItineraryCheck());


      if (SecurityMgr.isUmappedAdmin(sessionMgr)) {
        //set billing type and flight status
        cmpy.setFlighttracking(cmpyInfoForm.isInFlightTracking());
        cmpy.setBillingtype(cmpyInfoForm.getInBillingType());

        cmpy.setEnablecollaboration(cmpyInfoForm.inEnableCollaboration);

        cmpy.setType(cmpyInfoForm.getInType());
        cmpy.setNumAccounts(cmpyInfoForm.getInNumAccounts());
        if (cmpy.numaccounts > APPConstants.CMPY_MAX_NUM_ACCOUNTS) {
          cmpy.setNumAccounts(APPConstants.CMPY_MAX_NUM_ACCOUNTS);
        }

        int maxTrips = APPConstants.CMPY_MAX_NUM_TRIPS;
        try {
          maxTrips = cmpyInfoForm.getInNumTrips();
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        if (maxTrips != APPConstants.CMPY_MAX_NUM_TRIPS && maxTrips != 10 && maxTrips != 20 &&
            maxTrips != 50 &&
            maxTrips != 100) {
          maxTrips = APPConstants.CMPY_MAX_NUM_TRIPS;
        }

        cmpy.setMaxtrips(maxTrips);

        if (cmpyInfoForm.getInType() == null &&
            !cmpyInfoForm.getInType().equals(APPConstants.CMPY_TYPE_DESTINATION) &&
            !cmpyInfoForm.getInType().equals(APPConstants.CMPY_TYPE_TRAVEL)) {
          cmpy.setType(APPConstants.CMPY_TYPE_TRIAL);
        }

        if (cmpy.getType().equals(APPConstants.CMPY_TYPE_TRIAL)) {
          long timestamp = -1;
          if (cmpyInfoForm.getInCmpyExpiryDate() != null && cmpyInfoForm.getInCmpyExpiryDate().length() > 2) {
            timestamp = Utils.getMilliSecs(cmpyInfoForm.getInCmpyExpiryDate());
          }
          if (timestamp == -1) {
            DateTime dateTime = new DateTime(System.currentTimeMillis());
            timestamp = dateTime.plusMonths(1).getMillis();
          }
          cmpy.setExpirytimestamp(new Long(timestamp));
        }
        else {
          cmpy.setExpirytimestamp(new Long(-1));
        }

        //make sure the user is a cmpy admin
        cmpy.setAuthorizeduserid(null);
        if (cmpyInfoForm.getInAuthorizedUser() != null &&
            cmpyInfoForm.getInAuthorizedUser().trim().length() > 0 &&
            !cmpyInfoForm.getInAuthorizedUser().equals(APPConstants.NOT_APPLICABLE)) {
          List<UserCmpyLink> userCmpyLinks = UserCmpyLink.findActiveByUserIdCmpyId(cmpyInfoForm.getInAuthorizedUser(),
                                                                                   cmpy.cmpyid);
          boolean isAdmin = false;
          for (UserCmpyLink uc : userCmpyLinks) {
            if (uc.getLinktype() == UserCmpyLink.LinkType.ADMIN) {
              cmpy.setAuthorizeduserid(cmpyInfoForm.getInAuthorizedUser());
            }
          }
        }

        cmpy.setTargetagreementver(null);
        if (cmpyInfoForm.getInTargetAgreement() != null && Agreement.isActive(cmpyInfoForm.getInTargetAgreement())) {
          cmpy.setTargetagreementver(cmpyInfoForm.getInTargetAgreement());
        }

        cmpy.setTag(cmpyInfoForm.getInTag());
      }
      cmpy.save();

      if (SecurityMgr.isUmappedAdmin(sessionMgr)) {
        /* BILLING */
        if (cmpyInfoForm.billingPlan != null || cmpyInfoForm.billingSchedule.equals(BillingSchedule.Type.TRIAL)
            || cmpyInfoForm.billingSchedule.equals(BillingSchedule.Type.FREEMIUM) || cmpyInfoForm.billingSchedule.equals(BillingSchedule.Type.NONE)) {
          BillingSettingsForCmpy bsCmpy = BillingSettingsForCmpy.find.byId(cmpy.getCmpyid());
          boolean newBilling = false;
          if (bsCmpy == null) {
            bsCmpy = BillingSettingsForCmpy.buildCmpySettings(cmpy.getCmpyid(), sessionMgr.getUserId());
            newBilling = true;
          }
          if (cmpyInfoForm.billingPlan != null) {
            BillingPlan bp = BillingPlan.find.byId(cmpyInfoForm.billingPlan);
            bsCmpy.setPlan(bp);
            BillingSchedule bs = BillingSchedule.find.byId(bp.getSchedule());
            bsCmpy.setSchedule(bs);
          } else {
            bsCmpy.setPlan(null);
            BillingSchedule bs = BillingSchedule.find.byId(cmpyInfoForm.getBillingSchedule());
            bsCmpy.setSchedule(bs);
          }


          BillingPmntMethod bpm = BillingPmntMethod.find.byId(cmpyInfoForm.billingPmntMethod);
          bsCmpy.setPmntType(bpm);
          long bStartTs = Utils.getMilliSecs(cmpyInfoForm.billingStartDate);
          bsCmpy.setStartTs(bStartTs);

          if (cmpyInfoForm.serviceCutoffDate != null && cmpyInfoForm.serviceCutoffDate.trim().length() > 0) {
            bsCmpy.setCutoffTs(Utils.getMilliSecs(cmpyInfoForm.serviceCutoffDate));
          } else {
            bsCmpy.setCutoffTs(null);
          }

          bsCmpy.setRecurrent(cmpyInfoForm.billingRecurrent);
          if (newBilling) {
            bsCmpy.save();
          }
          else {
            bsCmpy.markModified(sessionMgr.getUserId());
            bsCmpy.update();
          }
        }
        /* END OF BILLING */
      }

      //check addr
      if ((cmpyInfoForm.getInCmpyAddress1() != null && cmpyInfoForm.getInCmpyAddress1().length() > 0) ||
          (cmpyInfoForm.getInCmpyCity() != null && cmpyInfoForm.getInCmpyCity().length() > 0) ||
          (cmpyInfoForm.getInCmpyCountry() != null && cmpyInfoForm.getInCmpyCountry().length() > 0) ||
          (cmpyInfoForm.getInCmpyEmail() != null && cmpyInfoForm.getInCmpyEmail().length() > 0) ||
          (cmpyInfoForm.getInCmpyFacebook() != null && cmpyInfoForm.getInCmpyFacebook().length() > 0) ||
          (cmpyInfoForm.getInCmpyFax() != null && cmpyInfoForm.getInCmpyFax().length() > 0) ||
          (cmpyInfoForm.getInCmpyLocLat() != null && Float.parseFloat(cmpyInfoForm.getInCmpyLocLat()) != 0) ||
          (cmpyInfoForm.getInCmpyLocLong() != null && Float.parseFloat(cmpyInfoForm.getInCmpyLocLong()) != 0) ||
          (cmpyInfoForm.getInCmpyPhone() != null && cmpyInfoForm.getInCmpyPhone().length() > 0) ||
          (cmpyInfoForm.getInCmpyState() != null && cmpyInfoForm.getInCmpyState().length() > 0) ||
          (cmpyInfoForm.getInCmpyTwitter() != null && cmpyInfoForm.getInCmpyTwitter().length() > 0) ||
          (cmpyInfoForm.getInCmpyZip() != null && cmpyInfoForm.getInCmpyZip().length() > 0) ||
          (cmpyInfoForm.getInCmpyWeb() != null && cmpyInfoForm.getInCmpyWeb().length() > 0)) {

        CmpyAddress cmpyAddr = null;

        if (cmpyInfoForm.getInCmpyAddrId() != null) {
          cmpyAddr = CmpyAddress.find.byId(cmpyInfoForm.getInCmpyAddrId());
          if (cmpyAddr != null && !cmpyAddr.cmpyid.equals(cmpyInfoForm.getInCmpyId())) {
            BaseView view = new BaseView();
            view.message = "System Error - cannot be updated";
            return ok(views.html.common.message.render(view));
          }
        }

        if (cmpyAddr == null) {
          cmpyAddr = new CmpyAddress();
          cmpyAddr.setCmpyaddrid(Utils.getUniqueId());
          cmpyAddr.setCmpyid(cmpy.getCmpyid());
          cmpyAddr.setCreatedby(sessionMgr.getUserId());
          cmpyAddr.setCreatedtimestamp(System.currentTimeMillis());
          cmpyAddr.setStatus(APPConstants.STATUS_ACTIVE);
        }

        cmpyAddr.setName(cmpy.name);

        cmpyAddr.setAddresstype(APPConstants.MAIN_ADDRESS);
        cmpyAddr.setStreetaddr1(cmpyInfoForm.getInCmpyAddress1());
        cmpyAddr.setStreetaddr2(cmpyInfoForm.getInCmpyAddress2());
        cmpyAddr.setCity(cmpyInfoForm.getInCmpyCity());
        cmpyAddr.setState(cmpyInfoForm.getInCmpyState());
        cmpyAddr.setZipcode(cmpyInfoForm.getInCmpyZip());
        cmpyAddr.setCountry(cmpyInfoForm.getInCmpyCountry());
        cmpyAddr.setPhone(cmpyInfoForm.getInCmpyPhone());
        cmpyAddr.setEmail(cmpyInfoForm.getInCmpyEmail());
        cmpyAddr.setFax(cmpyInfoForm.getInCmpyFax());
        cmpyAddr.setWeb(cmpyInfoForm.getInCmpyWeb());
        cmpyAddr.setFacebook(cmpyInfoForm.getInCmpyFacebook());
        cmpyAddr.setTwitter(cmpyInfoForm.getInCmpyTwitter());

        if (cmpyInfoForm.getInCmpyLocLat() != null) {
          cmpyAddr.setLoclat(Float.parseFloat(cmpyInfoForm.getInCmpyLocLat()));
        }

        if (cmpyInfoForm.getInCmpyLocLong() != null) {
          cmpyAddr.setLoclong(Float.parseFloat(cmpyInfoForm.getInCmpyLocLong()));
        }

        cmpyAddr.setStatus(APPConstants.STATUS_ACTIVE);
        cmpyAddr.setLastupdatedtimestamp(System.currentTimeMillis());
        cmpyAddr.setModifiedby(sessionMgr.getUserId());
        cmpyAddr.save();
      }
      Ebean.commitTransaction();
      if (update) {
        CompanyController.invalidateCache(cmpy.cmpyid);

        flash(SessionConstants.SESSION_PARAM_MSG, "Company - " + cmpy.getName() + " updated successfully.");

      }
      else {
        flash(SessionConstants.SESSION_PARAM_MSG, "Company - " + cmpy.getName() + " added successfully.");
      }
      return redirect(controllers.routes.CompanyController.viewCompany(cmpy.cmpyid));
    }
    catch (Exception e) {
      e.printStackTrace();
      Ebean.rollbackTransaction();
      BaseView view = new BaseView();
      view.message = "System Error - cannot be updated";
      return ok(views.html.common.message.render(view));
    }
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result deleteCompany() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    CmpyInfoView cmpyView = new CmpyInfoView();

    //bind html form to form bean
    Form<CompanyForm> cmpyForm = formFactory.form(CompanyForm.class);
    CompanyForm cmpyInfoForm = cmpyForm.bindFromRequest().get();

    Company cmpy = null;
    if (cmpyInfoForm.getInCmpyId() != null) {
      cmpy = Company.find.byId(cmpyInfoForm.getInCmpyId());
      if (cmpy != null && !SecurityMgr.isUmappedAdmin(sessionMgr)) {
        BaseView view = new BaseView();
        view.message = "System Error - cannot delete";
        return ok(views.html.common.message.render(view));
      }
      else if (cmpy.cmpyid.equals("0")) {
        BaseView view = new BaseView();
        view.message = "System Error - cannot delete the default cmpy";
        return ok(views.html.common.message.render(view));
      }

      try {
        cmpy.setLastupdatedtimestamp(System.currentTimeMillis());
        cmpy.setModifiedby(sessionMgr.getUserId());
        cmpy.setStatus(APPConstants.STATUS_DELETED);
        cmpy.save();
        //delete all links
        List<UserCmpyLink> links = UserCmpyLink.findActiveByCmpyId(cmpy.cmpyid);
        for (UserCmpyLink link : links) {
          link.delete();
        }

        flash(SessionConstants.SESSION_PARAM_MSG, "Company - " + cmpy.getName() + " deleted successfully.");
        return redirect(routes.CompanyController.companies());
      }
      catch (Exception e) {
        BaseView view = new BaseView();
        view.message = "System Error - cannot delete";
        CompanyController.invalidateCache(cmpy.cmpyid);
        return ok(views.html.common.message.render(view));
      }
    }
    BaseView view = new BaseView();
    view.message = "System Error - cannot delete";
    return ok(views.html.common.message.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result uploadCmpyLogo() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    //bind html form to form bean
    Form<LogoUploadForm> form = formFactory.form(LogoUploadForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return BaseJsonResponse.formError(form);
    }

    if (!sessionMgr.getCredentials().isCmpyAdmin() && !sessionMgr.getCredentials().isUmappedAdmin()) {
      return BaseJsonResponse.formError(form);
    }

    LogoUploadForm logoInfo = form.get();

    if(logoInfo.getInCmpyId() == null) {
      ObjectNode result = Json.newObject();
      result.put("msg", "Error - please resubmit");
      return ok(result);
    }

    if (logoInfo.getInCmpyPhotoName() == null || logoInfo.getInCmpyPhotoName().length() == 0) {
      ObjectNode result = Json.newObject();
      result.put("msg", "Error - please resubmit1");
      return ok(result);
    }
    Company cmpy = Company.find.byId(logoInfo.getInCmpyId());
    if (logoInfo.getInOriginPage() != null && !logoInfo.getInOriginPage().equals("SimpleTripInfo")) {
      if (cmpy == null || !SecurityMgr.canEditCmpy(cmpy, sessionMgr) || cmpy.status != APPConstants.STATUS_ACTIVE) {
        ObjectNode result = Json.newObject();
        result.put("msg", "Error - please resubmit2");
        return ok(result);
      }
    }

    try {
      String id = Utils.getUniqueId();
      String nextUrl = "/cmpy/cmpy?inCmpyId=" + cmpy.cmpyid;
      if (logoInfo.getInTripId() != null && logoInfo.getInOriginPage() != null && logoInfo.getInOriginPage().equals("SimpleTripInfo")) {
        nextUrl = routes.TripController.simpleTripInfo(logoInfo.getInTripId()).url();
      }
      String filename = id + "_" + logoInfo.getInCmpyPhotoName();

      String signedPutUrl = S3Util.authorizeS3Put(filename);
      String signedGetUrl = S3Util.authorizeS3Get(filename);
      S3Data s3Data = S3Util.authorizeS3(filename);

      if (signedGetUrl == null || signedPutUrl == null) {
        ObjectNode result = Json.newObject();
        result.put("msg", "Error - please resubmit4");
        return ok(result);
      }
      Ebean.beginTransaction();
      cmpy.setLogoname(filename);
      cmpy.setLogourl(signedGetUrl);
      cmpy.setLastupdatedtimestamp(System.currentTimeMillis());
      cmpy.setModifiedby(sessionMgr.getUserId());
      cmpy.save();
      Ebean.commitTransaction();
      ObjectNode result = Json.newObject();
      result.put("signed_request", signedPutUrl);
      result.put("url", nextUrl);
      result.put("policy", s3Data.policy);
      result.put("accessKey", s3Data.accessKey);
      result.put("key", s3Data.key);
      result.put("s3Bucket", s3Data.s3Bucket);
      result.put("signature", s3Data.signature);
      CompanyController.invalidateCache(cmpy.cmpyid);
      return ok(result);
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result toggleApiAccess() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    //bind html form to form bean
    Form<CompanyForm> cmpyForm = formFactory.form(CompanyForm.class);
    CompanyForm cmpyInfoForm = cmpyForm.bindFromRequest().get();

    Company cmpy = null;
    if (cmpyInfoForm.getInCmpyId() != null) {
      cmpy = Company.find.byId(cmpyInfoForm.getInCmpyId());
      if (cmpy != null && !SecurityMgr.isUmappedAdmin(sessionMgr)) {
        BaseView view = new BaseView();
        view.message = "System Error - cannot delete";
        return ok(views.html.common.message.render(view));
      }
      else if (cmpy.cmpyid.equals("0")) {
        BaseView view = new BaseView();
        view.message = "System Error - cannot update the default cmpy";
        return ok(views.html.common.message.render(view));
      }

      try {
        cmpy.setLastupdatedtimestamp(System.currentTimeMillis());
        cmpy.setModifiedby(sessionMgr.getUserId());

        EmailToken token = null;

        if (cmpyInfoForm.getInAllowApiAccess() != null &&
            cmpyInfoForm.getInAllowApiAccess().equals(APPConstants.TRUE)) {
          cmpy.setAllowapi(true);

          List<EmailToken> disabledTkns = EmailToken.getCompanyTokens(cmpy.getCmpyid());
          if(disabledTkns.size() >= 1) {
            token = disabledTkns.get(0); //Just grab the first one for now
          }

          if(token == null) {
            token = EmailToken.buildCmpyToken(cmpy.getCmpyid());
            while(true) {
              String tokenValue = Utils.shortenLCNumber(Long.parseLong(DBConnectionMgr.getUniqueId()));
              EmailToken dup = EmailToken.find.byId(tokenValue);
              if(dup == null) {
                token.setToken(tokenValue);
                break;
              }
            }
          } else {
            token.setState(APPConstants.STATUS_ACTIVE);
          }
          token.save();
          flash(SessionConstants.SESSION_PARAM_MSG, "Company - " + cmpy.getName() + " API Access Enabled.");
        }
        else {
          List<EmailToken> activeTkns = EmailToken.getCompanyActiveTokens(cmpy.getCmpyid());
          if(activeTkns.size() > 1) {
            Log.err("Company " + cmpy.getCmpyid() + " has more than one Email API token.");
          }
          if(activeTkns.size() == 1) { //Enabled
            token = activeTkns.get(0);
          }

          cmpy.setAllowapi(false);
          if(token != null) {
            token.setState(APPConstants.STATUS_DELETED);
            token.update();
          }
          flash(SessionConstants.SESSION_PARAM_MSG, "Company - " + cmpy.getName() + " API Access Disabled.");

        }
        cmpy.save();
        return redirect(controllers.routes.CompanyController.viewCompany(cmpy.cmpyid));
      }
      catch (Exception e) {
        BaseView view = new BaseView();
        view.message = "System Error - cannot delete";
        return ok(views.html.common.message.render(view));
      }
    }
    BaseView view = new BaseView();
    view.message = "System Error - cannot delete";
    return ok(views.html.common.message.render(view));
  }


  @With({Authenticated.class, Authorized.class})
  public Result newGroup() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean //workaround for multiple select bug in play form
    List<String> inUsers = new ArrayList<String>();
    Map<String, String> newData = new HashMap<String, String>();
    Map<String, String[]> urlFormEncoded = play.mvc.Controller.request().body().asFormUrlEncoded();
    if (urlFormEncoded != null) {
      for (String key : urlFormEncoded.keySet()) {
        String[] value = urlFormEncoded.get(key);
        if (value.length == 1) {
          if (key.equalsIgnoreCase("inGroupUsers")) {
            inUsers.add(value[0]);
          }
          newData.put(key, value[0]);
        }
        else if (value.length > 1) {
          if (key.equalsIgnoreCase("inGroupUsers")) {
            for (int i = 0; i < value.length; i++) {
              if (!inUsers.contains(value[i])) {
                inUsers.add(value[i]);
              }
            }
          }
          for (int i = 0; i < value.length; i++) {
            newData.put(key + "[" + i + "]", value[i]);
          }

        }
      }
    }
    // bind to the MyEntity form object

    Form<GroupForm> groupForm = formFactory.form(GroupForm.class).bind(newData);
    GroupForm groupInfoForm = groupForm.bindFromRequest().get();
    groupInfoForm.setInGroupUsers(inUsers);

    if (groupInfoForm.getInCmpyId() != null) {
      Company cmpy = Company.find.byId(groupInfoForm.getInCmpyId());
      if (cmpy != null && (SecurityMgr.isCmpyAdmin(cmpy, sessionMgr) || SecurityMgr.isUmappedAdmin(sessionMgr))) {
        try {
          Ebean.beginTransaction();
          CmpyGroup cmpyGroup = new CmpyGroup();
          if (groupInfoForm.getInGroupName() == null || groupInfoForm.getInGroupName().length() == 0) {
            BaseView baseView = new BaseView();
            baseView.message = "Group Name is a mandatory field.";
            return ok(views.html.common.message.render(baseView));
          }

          cmpyGroup.setGroupid(Utils.getUniqueId());
          cmpyGroup.setName(groupInfoForm.getInGroupName());
          cmpyGroup.setCmpyid(cmpy.cmpyid);
          cmpyGroup.setComment(groupInfoForm.getInGroupComments());
          cmpyGroup.setCreatedby(sessionMgr.getUserId());
          cmpyGroup.setModifiedby(sessionMgr.getUserId());
          cmpyGroup.setLastupdatedtimestamp(System.currentTimeMillis());
          cmpyGroup.setCreatedtimestamp(System.currentTimeMillis());
          cmpyGroup.setStatus(APPConstants.STATUS_ACTIVE);
          cmpyGroup.save();

          if (groupInfoForm.getInGroupUsers() != null && groupInfoForm.getInGroupUsers().size() > 0) {
            for (String userId : groupInfoForm.getInGroupUsers()) {
              List<UserCmpyLink> link = UserCmpyLink.findActiveByUserIdCmpyId(userId, cmpy.cmpyid);
              if (link != null && link.size() > 0) {
                CmpyGroupMember member = new CmpyGroupMember();
                member.setPk(Utils.getUniqueId());
                member.setGroupid(cmpyGroup.groupid);
                member.setUsercmpylinkpk(link.get(0).getPk());
                member.setCreatedby(sessionMgr.getUserId());
                member.setModifiedby(sessionMgr.getUserId());
                member.setLastupdatedtimestamp(System.currentTimeMillis());
                member.setCreatedtimestamp(System.currentTimeMillis());
                member.setStatus(APPConstants.STATUS_ACTIVE);
                member.save();
              }
            }
          }


          Ebean.commitTransaction();
          flash(SessionConstants.SESSION_PARAM_MSG,
                "Group - " + groupInfoForm.getInGroupName() + " created successfully.");
          return groups();

        }
        catch (Exception e) {
          e.printStackTrace();
          Ebean.rollbackTransaction();
          BaseView baseView = new BaseView();
          baseView.message = "System Error - please resubmit";
          return ok(views.html.common.message.render(baseView));
        }
      }
    }

    BaseView baseView = new BaseView();
    baseView.message = "Unauthorized Action";
    return ok(views.html.common.message.render(baseView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result removeGroupMember() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Form<GroupForm> groupForm = formFactory.form(GroupForm.class);
    GroupForm groupInfoForm = groupForm.bindFromRequest().get();

    if (groupInfoForm.getInCmpyId() != null) {
      Company cmpy = Company.find.byId(groupInfoForm.getInCmpyId());
      if (cmpy != null && (SecurityMgr.isCmpyAdmin(cmpy, sessionMgr) || SecurityMgr.isUmappedAdmin(sessionMgr))) {

        try {
          Ebean.beginTransaction();
          CmpyGroup group = CmpyGroup.find.byId(groupInfoForm.getInGroupId());
          if (group != null && group.cmpyid.equals(cmpy.cmpyid)) {
            List<UserCmpyLink> links = UserCmpyLink.findActiveByUserIdCmpyId(groupInfoForm.getInUserId(), cmpy.cmpyid);
            if (links != null) {
              for (UserCmpyLink link : links) {
                List<CmpyGroupMember> members = CmpyGroupMember.findByGroupIdUserLink(group.groupid, link.getPk());
                for (CmpyGroupMember m : members) {
                  m.delete();
                }
              }
              flash(SessionConstants.SESSION_PARAM_MSG,
                    "User - " + groupInfoForm.getInUserId() + " removed from group " + group.name + " successfully.");

            }
          }
          Ebean.commitTransaction();
          return groups();
        }
        catch (Exception e) {
          Ebean.rollbackTransaction();
          e.printStackTrace();
          BaseView baseView = new BaseView();
          baseView.message = "System Error - please resubmit";
          return ok(views.html.common.message.render(baseView));
        }
      }
    }

    BaseView baseView = new BaseView();
    baseView.message = "Unauthorized Action";
    return ok(views.html.common.message.render(baseView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result editGroup() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Form<GroupForm> groupForm = formFactory.form(GroupForm.class);
    GroupForm groupInfoForm = groupForm.bindFromRequest().get();

    if (groupInfoForm.getInCmpyId() != null) {
      Company cmpy = Company.find.byId(groupInfoForm.getInCmpyId());
      if (cmpy != null && cmpy.status == APPConstants.STATUS_ACTIVE &&
          (SecurityMgr.isCmpyAdmin(cmpy, sessionMgr) ||
           SecurityMgr.isUmappedAdmin(sessionMgr))) {

        try {
          Ebean.beginTransaction();
          CmpyGroup group = CmpyGroup.find.byId(groupInfoForm.getInGroupId());
          if (group != null && group.cmpyid.equals(cmpy.cmpyid) && group.status == APPConstants.STATUS_ACTIVE) {
            if (groupInfoForm.getInGroupName() != null && groupInfoForm.getInGroupName().length() > 0) {
              group.setName(groupInfoForm.getInGroupName());
            }
            group.setComment(groupInfoForm.getInGroupComments());
            group.setModifiedby(sessionMgr.getUserId());
            group.setLastupdatedtimestamp(System.currentTimeMillis());
            group.save();

            flash(SessionConstants.SESSION_PARAM_MSG, group.name + " updated successfully.");

          }
          Ebean.commitTransaction();
          return groups();
        }
        catch (Exception e) {
          Ebean.rollbackTransaction();
          e.printStackTrace();
          BaseView baseView = new BaseView();
          baseView.message = "System Error - please resubmit";
          return ok(views.html.common.message.render(baseView));
        }
      }
    }

    BaseView baseView = new BaseView();
    baseView.message = "Unauthorized Action";
    return ok(views.html.common.message.render(baseView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result deleteGroup() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Form<GroupForm> groupForm = formFactory.form(GroupForm.class);
    GroupForm groupInfoForm = groupForm.bindFromRequest().get();

    if (groupInfoForm.getInCmpyId() != null) {
      Company cmpy = Company.find.byId(groupInfoForm.getInCmpyId());
      if (cmpy != null && cmpy.status == APPConstants.STATUS_ACTIVE && (SecurityMgr.isCmpyAdmin(cmpy,
                                                                                                sessionMgr) ||
                                                                        SecurityMgr
                                                                            .isUmappedAdmin(sessionMgr))) {

        try {
          Ebean.beginTransaction();
          CmpyGroup group = CmpyGroup.find.byId(groupInfoForm.getInGroupId());
          if (group != null && group.cmpyid.equals(cmpy.cmpyid) && group.status == APPConstants.STATUS_ACTIVE) {

            group.delete();
            flash(SessionConstants.SESSION_PARAM_MSG, group.name + " deleted successfully.");
          }
          Ebean.commitTransaction();
          return groups();
        }
        catch (Exception e) {
          Ebean.rollbackTransaction();
          e.printStackTrace();
          BaseView baseView = new BaseView();
          baseView.message = "System Error - please resubmit";
          return ok(views.html.common.message.render(baseView));
        }
      }
    }

    BaseView baseView = new BaseView();
    baseView.message = "Unauthorized Action";
    return ok(views.html.common.message.render(baseView));
  }


  @With({Authenticated.class, Authorized.class})
  public Result manageGroupMembers() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean //workaround for multiple select bug in play form
    List<String> inUsers = new ArrayList<String>();
    Map<String, String> newData = new HashMap<String, String>();
    Map<String, String[]> urlFormEncoded = play.mvc.Controller.request().body().asFormUrlEncoded();
    if (urlFormEncoded != null) {
      for (String key : urlFormEncoded.keySet()) {
        String[] value = urlFormEncoded.get(key);
        if (value.length == 1) {
          if (key.equalsIgnoreCase("inGroupUsers")) {
            inUsers.add(value[0]);
          }
          newData.put(key, value[0]);
        }
        else if (value.length > 1) {
          if (key.equalsIgnoreCase("inGroupUsers")) {
            for (int i = 0; i < value.length; i++) {
              if (!inUsers.contains(value[i])) {
                inUsers.add(value[i]);
              }
            }
          }
          for (int i = 0; i < value.length; i++) {
            newData.put(key + "[" + i + "]", value[i]);
          }

        }
      }
    }
    // bind to the MyEntity form object

    Form<GroupForm> groupForm = formFactory.form(GroupForm.class).bind(newData);
    GroupForm groupInfoForm = groupForm.bindFromRequest().get();
    groupInfoForm.setInGroupUsers(inUsers);


    if (groupInfoForm.getInCmpyId() != null) {
      Company cmpy = Company.find.byId(groupInfoForm.getInCmpyId());
      if (cmpy != null && cmpy.status == APPConstants.STATUS_ACTIVE && (SecurityMgr.isCmpyAdmin(cmpy,
                                                                                                sessionMgr) ||
                                                                        SecurityMgr
                                                                            .isUmappedAdmin(sessionMgr))) {

        try {
          Ebean.beginTransaction();


          CmpyGroup group = CmpyGroup.find.byId(groupInfoForm.getInGroupId());
          if (group != null && group.cmpyid.equals(cmpy.cmpyid) && group.status == APPConstants.STATUS_ACTIVE) {
            if (groupInfoForm.getInGroupUsers() != null) {
              //get all current members
              List<CmpyGroupMember> currentMembers = CmpyGroupMember.findByGroupId(group.getGroupid());
              Map<String, CmpyGroupMember> currentMembersMap = new HashMap<String, CmpyGroupMember>();
              for (CmpyGroupMember c : currentMembers) {
                currentMembersMap.put(c.getUsercmpylinkpk(), c);
              }


              for (String userId : groupInfoForm.getInGroupUsers()) {
                List<UserCmpyLink> link = UserCmpyLink.findActiveByUserIdCmpyId(userId, cmpy.cmpyid);
                if (link != null && link.size() > 0) {
                  //check to see if this already exist
                  CmpyGroupMember mem = currentMembersMap.get(link.get(0).getPk());
                  if (mem == null) {
                    CmpyGroupMember member = new CmpyGroupMember();
                    member.setPk(Utils.getUniqueId());
                    member.setGroupid(group.groupid);
                    member.setUsercmpylinkpk(link.get(0).getPk());
                    member.setCreatedby(sessionMgr.getUserId());
                    member.setModifiedby(sessionMgr.getUserId());
                    member.setLastupdatedtimestamp(System.currentTimeMillis());
                    member.setCreatedtimestamp(System.currentTimeMillis());
                    member.setStatus(APPConstants.STATUS_ACTIVE);
                    member.save();
                  }
                  else {
                    currentMembers.remove(mem);
                  }
                }
              }

              //delete any members missing
              for (CmpyGroupMember c : currentMembers) {
                c.delete();
              }
            }

            flash(SessionConstants.SESSION_PARAM_MSG, group.name + " updated successfully.");
          }
          Ebean.commitTransaction();
          return groups();
        }
        catch (Exception e) {
          Ebean.rollbackTransaction();
          e.printStackTrace();
          BaseView baseView = new BaseView();
          baseView.message = "System Error - please resubmit";
          return ok(views.html.common.message.render(baseView));
        }
      }
    }

    BaseView baseView = new BaseView();
    baseView.message = "Unauthorized Action";
    return ok(views.html.common.message.render(baseView));
  }

  //not protected to allow pdf viewing - other details are validated
  public Result companyAgreement() {
    SessionMgr sessionMgr = new SessionMgr(session());
    //bind html form to form bean
    final DynamicForm form = formFactory.form().bindFromRequest();

    String cmpyId = form.get("inCmpyId");
    String ageementId = form.get("inAgreementId");
    String pdf = form.get("inPdf");

    String userId = sessionMgr.getUserId();

    if (cmpyId != null && ageementId != null && cmpyId.equals("WETU") && ageementId.toLowerCase().contains("wetu")) {
      AgreementView view = new AgreementView();
      view.agreementId = ageementId;
      view.cmpyId = "WETU";
      view.cmpyName = "";
      view.userName = "";
      view.userId = userId;
      Agreement a = Agreement.find.byId(ageementId);
      view.html = a.html;
      if (pdf != null && pdf.equals(APPConstants.TRUE)) {
        view.isPdf = true;
        Html html = (Html) views.html.cmpy.cmpyAgreementPdf.render(view);
        return pdfGenerator.ok(html, null);
      } else {
        view.isPdf = false;
        return ok(views.html.cmpy.cmpyAgreement.render(view));
      }
    } else if (cmpyId != null && userId != null && ageementId != null) {
      Company cmpy = Company.find.byId(cmpyId);
      if (cmpy != null &&
              (SecurityMgr.isCmpyAdmin(cmpy, sessionMgr) || SecurityMgr.isUmappedAdmin(sessionMgr)) &&
              ((cmpy.getAcceptedagreementver() != null && cmpy.getAcceptedagreementver().equals(ageementId)) ||
                      (cmpy.getTargetagreementver() != null && cmpy.getTargetagreementver().equals(ageementId)))) {
        Credentials cred = sessionMgr.getCredentials();
        AgreementView view = new AgreementView();
        view.agreementId = cmpy.targetagreementver;
        view.cmpyId = cmpy.cmpyid;
        view.cmpyName = cmpy.name;
        view.userName = cred.getFirstName() + " " + cred.getLastName();
        view.userId = userId;
        Agreement a = Agreement.find.byId(ageementId);
        view.html = a.html;

        if (pdf != null && pdf.equals(APPConstants.TRUE)) {
          view.isPdf = true;
          Html html = (Html) views.html.cmpy.cmpyAgreementPdf.render(view);
          return pdfGenerator.ok(html, null);
        } else {
          view.isPdf = false;
          return ok(views.html.cmpy.cmpyAgreement.render(view));
        }
      }
    }

    BaseView view = new BaseView();
    view.message = "Unable to display licence agreement - please contact help@umapped.com";
    return ok(views.html.error.error404.render(view));
  }


  @With({Authenticated.class, AdminAccess.class})
  public Result addParserRule() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<CmpyApiParserForm> cmpyForm = formFactory.form(CmpyApiParserForm.class);
    CmpyApiParserForm cmpyInfoForm = cmpyForm.bindFromRequest().get();
    boolean edit = true;
    CmpyApiParser parser = null;
    if (cmpyInfoForm != null && cmpyInfoForm.getInCmpyId() != null &&
        cmpyInfoForm.getInFilePattern() != null &&
        cmpyInfoForm.getInFilePattern() != null &&
        cmpyInfoForm.getInParser() != null &&
        !cmpyInfoForm.getInParser().isEmpty()) {
      if (cmpyInfoForm.getInParserId() != null && !cmpyInfoForm.getInParserId().isEmpty()) {
        parser = CmpyApiParser.find.byId(cmpyInfoForm.getInParserId());
      }

      if (parser == null) {
        parser = new CmpyApiParser();
        parser.setCreatedby(sessionMgr.getUserId());
        parser.setCreatedtimestamp(System.currentTimeMillis());
        parser.setPk(DBConnectionMgr.getUniqueId());
        parser.setCmpyid(cmpyInfoForm.getInCmpyId());
        edit = false;
      }
      parser.setModifiedby(sessionMgr.getUserId());
      parser.setLastupdatedtimestamp(System.currentTimeMillis());
      parser.setFilepattern(cmpyInfoForm.getInFilePattern().trim());
      parser.setName(cmpyInfoForm.getInName());
      parser.setParams(cmpyInfoForm.getInParams().trim());
      parser.setParser(cmpyInfoForm.getInParser().trim());
      parser.setParsertype(cmpyInfoForm.getInParserType());

      parser.save();
    }

    if (edit) {
      flash(SessionConstants.SESSION_PARAM_MSG, parser.getName() + " updated successfully");
    }
    else {
      flash(SessionConstants.SESSION_PARAM_MSG, parser.getName() + " added successfully");
    }

    return redirect(controllers.routes.CompanyController.viewCompany(cmpyInfoForm.getInCmpyId()));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result deleteParserRule() {
    //bind html form to form bean
    Form<CmpyApiParserForm> cmpyForm = formFactory.form(CmpyApiParserForm.class);
    CmpyApiParserForm cmpyInfoForm = cmpyForm.bindFromRequest().get();
    CmpyApiParser parser = null;
    if (cmpyInfoForm != null && cmpyInfoForm.getInParserId() != null) {
      parser = CmpyApiParser.find.byId(cmpyInfoForm.getInParserId());

      if (parser != null) {
        parser.delete();
        flash(SessionConstants.SESSION_PARAM_MSG, parser.getName() + " deleted successfully");
        return redirect(controllers.routes.CompanyController.viewCompany(null));
      }
    }
    BaseView view = new BaseView();
    view.message = "Error - this parser entry cannot be deleted";
    return ok(views.html.common.message.render(view));
  }

  //billing
  @With({Authenticated.class, Authorized.class})
  public Result getBilling() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    BillingView view = new BillingView();

    HashMap<String, String> agencyList = new HashMap<String, String>();
    Credentials cred = sessionMgr.getCredentials();

    if (cred.isCmpyAdmin()) {
      agencyList.put(cred.getCmpyId(), cred.getCmpyName());
    }

    view.cmpies = agencyList;
    Html a = views.html.cmpy.cmpyBilling.render(view);
    return ok(a);
  }

  @With({Authenticated.class, Authorized.class})
  public Result doBilling() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    BillingView view = new BillingView();

    HashMap<String, String> agencyList = new HashMap<>();
    Credentials cred = sessionMgr.getCredentials();
    if (cred.isCmpyAdmin()) {
      agencyList.put(cred.getCmpyId(), cred.getCmpyName());
    }
    view.cmpies = agencyList;

    // find the queries
    //bind html form to form bean
    Form<BillingForm> form = formFactory.form(BillingForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    BillingForm billingInfo = form.get();

    //use bean validator framework

    if (cred.isCmpyAdmin(billingInfo.getInCmpyId())) {
      DashboardController.buildBillingView(view, billingInfo, agencyList);
    }
    else {
      UserMessage.error("You are not authorized to perform this action");
    }

    Html a = views.html.cmpy.cmpyBilling.render(view);
    return ok(a);
  }

  public static CmpyInfoView getCmpyView(String cmpyId) {
    CmpyInfoView cmpyView = (CmpyInfoView) CacheMgr.get(APPConstants.CACHE_CMPY_VIEW_PREFIX + cmpyId);

    if (cmpyView == null) {
      Company c = Company.find.byId(cmpyId);
      if (c != null) {
        cmpyView = new CmpyInfoView();


        cmpyView.cmpyId = c.cmpyid;
        cmpyView.cmpyName = c.name;
        cmpyView.comments = c.comment;
        cmpyView.logoName = c.logoname;
        cmpyView.logoUrl = c.logourl;
        cmpyView.status = c.status;
        cmpyView.tag = c.tag;
        cmpyView.contact = c.contact;
        cmpyView.cmpyType = c.type;
        cmpyView.numAccounts = c.numaccounts;


        List<CmpyAddress> addresses = CmpyAddress.findMainActiveByCmpyId(c.cmpyid);
        if (addresses != null && addresses.size() > 0) {
          CmpyAddress address = addresses.get(0);
          PoiAddressView adrView = new PoiAddressView();
          adrView.id = address.cmpyaddrid;
          adrView.city = address.city;
          adrView.state = address.state;
          adrView.country = address.country;
          adrView.email = address.email;
          adrView.facebook = address.facebook;
          adrView.twitter = address.twitter;

          adrView.phone = address.phone;
          if (address.web.toUpperCase().startsWith("http")) {
            adrView.web = address.web;
          }
          else {
            adrView.web = "http://" + address.web;
          }
          adrView.fax = address.fax;
          adrView.streetAddr1 = address.streetaddr1;
          adrView.streetAddr2 = address.streetaddr2;
          adrView.zip = address.zipcode;
          adrView.formatAddr1 = address.formatAddress1();
          adrView.formatAddr2 = address.formatAddress2();
          adrView.locLat = String.valueOf(address.loclat);
          adrView.locLong = String.valueOf(address.loclong);
          cmpyView.address = adrView;

        }
        try {
          CacheMgr.set(APPConstants.CACHE_CMPY_VIEW_PREFIX + cmpyId, cmpyView, APPConstants.CACHE_EXPIRY_SECS);
        }
        catch (Exception e) {
          Log.log(LogLevel.ERROR, "saveCache (cmpies): " + cmpyId, e);
        }
      }
    }
    return cmpyView;
  }

  public static void invalidateCache(String cmpyId) {
    if (cmpyId != null) {
      try {
        CacheMgr.set(APPConstants.CACHE_CMPY_VIEW_PREFIX + cmpyId, null, APPConstants.CACHE_EXPIRY_SECS);
      }
      catch (Exception e) {
        Log.log(LogLevel.ERROR, "invalidateCache (cmpies): " + cmpyId, e);
      }
    }
  }

  public static List<CmpyInfoView> buildCmpyInfoView(List<Company> companies, SessionMgr sessionMgr) {
    ArrayList<CmpyInfoView> cmpyInfoViews = new ArrayList<>();
    for (Company c : companies) {
      if (SecurityMgr.canEditCmpy(c, sessionMgr)) {
        CmpyInfoView cmpyView = new CmpyInfoView();
        cmpyView.cmpyId = c.cmpyid;
        cmpyView.cmpyName = c.name;
        cmpyView.comments = c.comment;
        cmpyView.logoName = c.logoname;
        cmpyView.logoUrl = c.logourl;
        cmpyView.status = c.status;
        cmpyView.tag = c.tag;
        cmpyView.contact = c.contact;
        cmpyView.cmpyType = c.type;
        cmpyView.numAccounts = c.numaccounts;

        List<CmpyAddress> addresses = CmpyAddress.findMainActiveByCmpyId(c.cmpyid);
        if (addresses != null && addresses.size() > 0) {
          CmpyAddress address = addresses.get(0);
          PoiAddressView adrView = new PoiAddressView();
          adrView.id = address.cmpyaddrid;
          adrView.city = address.city;
          adrView.state = address.state;
          adrView.country = address.country;
          adrView.email = address.email;
          adrView.facebook = address.facebook;
          adrView.twitter = address.twitter;

          adrView.phone = address.phone;
          if (address.web.toUpperCase().startsWith("http")) {
            adrView.web = address.web;
          }
          else {
            adrView.web = "http://" + address.web;
          }
          adrView.fax = address.fax;
          adrView.streetAddr1 = address.streetaddr1;
          adrView.streetAddr2 = address.streetaddr2;
          adrView.zip = address.zipcode;
          adrView.formatAddr1 = address.formatAddress1();
          adrView.formatAddr2 = address.formatAddress2();
          adrView.locLat = String.valueOf(address.loclat);
          adrView.locLong = String.valueOf(address.loclong);
          cmpyView.address = adrView;

        }

        cmpyInfoViews.add(cmpyView);
      }
    }

    return cmpyInfoViews;
  }

  /* display merge cmpy tab */
  @With({Authenticated.class, AdminAccess.class})
  public Result mergeCmpy() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    MergeCmpiesView cmpiesView = new MergeCmpiesView();
    cmpiesView.isUMappedAdmin = SecurityMgr.isUmappedAdmin(sessionMgr);

    MergeCmpyForm mergeCmpyForm = formFactory.form(MergeCmpyForm.class).bindFromRequest().get();

    String term = mergeCmpyForm.getInTerm();

    String cmpyId = mergeCmpyForm.getInCmpyId();

    if (cmpyId == null) {
      BaseView view = new BaseView();
      view.message = "Invalid Company - try again";
      return ok(views.html.common.message.render(view));
    }
    Company company = Company.find.byId(cmpyId);
    if (company == null || company.getStatus() != APPConstants.STATUS_ACTIVE) {
      BaseView view = new BaseView();
      view.message = "Invalid Company - try again";
      return ok(views.html.common.message.render(view));
    }


    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    cmpiesView.message = msg;
    cmpiesView.cmpyId = company.cmpyid;
    cmpiesView.cmpyName = company.name;

    List<Company> companies = null;

    if (term != null && term.length() > 0) {
      companies = Company.findByKeyword(term);
      cmpiesView.term = term;
      if (companies == null || companies.size() == 0) {
        cmpiesView.message = "No Results Found.";
      }
      else {
        if (companies != null) {
          List<CmpyInfoView> cmpyInfoViews = CompanyController.buildCmpyInfoView(companies, sessionMgr);
          cmpiesView.cmpies = cmpyInfoViews;
        }
      }
    }

    return ok(views.html.cmpy.cmpyMerge.render(cmpiesView));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result doMergeCmpy() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Form<MergeCmpyForm> form = formFactory.form(MergeCmpyForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    MergeCmpyForm mergeCmpyForm = null;
    mergeCmpyForm = form.get();
    String srcCmpyId = mergeCmpyForm.getInCmpyId();
    String targetCmpyId = mergeCmpyForm.getInTargetCmpyId();

    if (srcCmpyId == null || targetCmpyId == null || srcCmpyId.equals("0") || targetCmpyId.equals("0")) {
      return UserMessage.message("Invalid Company - try again");
    }

    Company srcCompany = Company.find.byId(srcCmpyId);
    if (srcCompany == null || srcCompany.getStatus() != APPConstants.STATUS_ACTIVE) {
      return UserMessage.message("Invalid Company - try again");
    }

    Company targetCompany = Company.find.byId(targetCmpyId);
    if (targetCompany == null || targetCompany.getStatus() != APPConstants.STATUS_ACTIVE) {
      BaseView view = new BaseView();
      view.message = "Invalid Target Company - try again";
      return ok(views.html.common.message.render(view));
    }

    try {
      Ebean.beginTransaction();
      AccountCmpyLink.mergeCmpy(srcCompany.getCmpyid(), targetCompany.getCmpyid(), Account.findByLegacyId(sessionMgr.getUserId()).getUid());
      UserCmpyLink.mergeCmpy(srcCompany.getCmpyid(), targetCompany.getCmpyid(), sessionMgr.getUserId());
      CmpyApiParser.mergeCmpy(srcCompany.getCmpyid(), targetCompany.getCmpyid(), sessionMgr.getUserId());
      Destination.mergeCmpy(srcCompany.getCmpyid(), targetCompany.getCmpyid(), sessionMgr.getUserId());
      PoiMgr.mergeCmpy(srcCompany.getCmpyId(), targetCompany.getCmpyId(), sessionMgr.getUserId());
      Template.mergeCmpy(srcCompany.getCmpyid(), targetCompany.getCmpyid(), sessionMgr.getUserId());
      Trip.mergeCmpy(srcCompany.getCmpyid(), targetCompany.getCmpyid(), sessionMgr.getUserId());
      TripCmpy.mergeCmpy(srcCompany.getCmpyid(), targetCompany.getCmpyid(), sessionMgr.getUserId());
      CmpyGroup.mergeCmpy(srcCompany.getCmpyid(), targetCompany.getCmpyid(), sessionMgr.getUserId());
      TripDetail.mergeCmpy(srcCompany.getCmpyId(), targetCompany.getCmpyId(), sessionMgr.getUserId());
      TmpltDetails.mergeCmpy(srcCompany.getCmpyId(), targetCompany.getCmpyId(), sessionMgr.getUserId());

      //TODO add the audit
      Ebean.commitTransaction();
      BaseView view = new BaseView();
      view.message = "All Records have been merged from " + srcCompany.getName() + " to " + targetCompany.getName();
      return ok(views.html.common.message.render(view));
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      e.printStackTrace();
    }

    BaseView view = new BaseView();
    view.message = "Error merging " + srcCompany.getName() + " to " + targetCompany.getName();
    return ok(views.html.common.message.render(view));
  }

  /**
   * Link a company to a parent cmpy
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result linkParentCmpy() {
    final DynamicForm form = formFactory.form().bindFromRequest();
    String inCmpyId = form.get("inCmpyId");
    String parentCmpyId = form.get("parentCmpyId");
    String parentCmpyName = form.get("parentCmpyName");
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    if (inCmpyId != null && inCmpyId.trim().length() > 0 && parentCmpyId != null && parentCmpyId.trim().length() > 0 && parentCmpyName != null && parentCmpyName.trim().length() > 0) {
      int parentCmpyId1 = Integer.parseInt(parentCmpyId);

      Company inCmpy = Company.find.byId(inCmpyId);
      Company parentCompany = Company.findByCmpyID(parentCmpyId1);

      if (parentCompany != null && parentCompany.getStatus() == 0) {
        if (parentCompany.getCmpyid().equals(inCmpy.getCmpyid())) {
          BaseView view = new BaseView();
          view.message = "Cannot add " + parentCompany.getName()  + " to itself";
          return ok(views.html.common.message.render(view));
        } else if (parentCompany.getParentCmpyId() != null && parentCompany.getParentCmpyId().trim().length() > 0) {
          BaseView view = new BaseView();
          view.message = "Cannot add " + parentCompany.getName()  + " as parent - it is already linked to another parent company";
          return ok(views.html.common.message.render(view));
        }
        if (inCmpy != null && inCmpy.getStatus() == 0) {
          if (Company.isParentCmpy(inCmpyId)) {
            BaseView view = new BaseView();
            view.message = "Cannot add " + parentCompany.getName()  + " as parent. " + inCmpy.getName() + " - it is already linked to another parent company";
            return ok(views.html.common.message.render(view));
          }
          inCmpy.setParentCmpyId(parentCompany.getCmpyid());
          inCmpy.setLastupdatedtimestamp(System.currentTimeMillis());
          inCmpy.setModifiedby(sessionMgr.getUserId());
          inCmpy.update();

          //Add Company to all related Consortia of Parent Company
          try {
            Ebean.beginTransaction();

            List<ConsortiumCompany> parentConsortia = ConsortiumCompany.findByCmpyId(parentCompany.getCmpyId());

            if (parentConsortia != null && parentConsortia.size() > 0) {
              for (ConsortiumCompany consortia : parentConsortia) {
                ConsortiumCompany existingCons = ConsortiumCompany.findUniqueById(consortia.getConsId(), inCmpy.getCmpyId());
                if (existingCons == null) {
                  ConsortiumCompany cmpyCons = ConsortiumCompany.buildConsortiumCompany(consortia.getConsId(),
                                                                                        inCmpy.getCmpyId(),
                                                                                        sessionMgr.getUserId());
                  cmpyCons.save();
                }
              }
            }

            Ebean.commitTransaction();

          } catch (Exception e) {
            Ebean.rollbackTransaction();
            Log.err("Attempt to link Company to Parent Company consortium FAILED");
            e.printStackTrace();
          }


          flash(SessionConstants.SESSION_PARAM_MSG, parentCompany.getName() + " added as parent");
          return redirect(controllers.routes.CompanyController.viewCompany(inCmpy.cmpyid));
        }
      }
    } else if (inCmpyId != null && inCmpyId.trim().length() > 0) {
      Company inCmpy = Company.find.byId(inCmpyId);
      if (inCmpy != null && inCmpy.getStatus() == 0) {
        String parentId = inCmpy.getParentCmpyId();
        inCmpy.setParentCmpyId("");
        inCmpy.setLastupdatedtimestamp(System.currentTimeMillis());
        inCmpy.setModifiedby(sessionMgr.getUserId());
        inCmpy.update();

        List<String> deletedConsortia = new ArrayList<>();

        //Delete Links to Parent Company Consortia
        if (parentId != null && parentId.trim().length() > 0) {
          try {
            Ebean.beginTransaction();

            Company parentCmpyId1 = Company.find.byId(parentId);

            List<ConsortiumCompany> parentConsortia = ConsortiumCompany.findByCmpyId(parentCmpyId1.getCmpyId());

            if (parentConsortia != null && parentConsortia.size() > 0) {
              for (ConsortiumCompany parent : parentConsortia) {
                ConsortiumCompany cmpy = ConsortiumCompany.findUniqueById(parent.getConsId(), inCmpy.getCmpyId());
                if (cmpy != null) {
                  cmpy.delete();
                  String consName = Consortium.find.byId(parent.getConsId()).getName();

                  deletedConsortia.add(consName);
                }
              }
            }

            Ebean.commitTransaction();

          } catch (Exception e) {
            Ebean.rollbackTransaction();
            Log.err("Attempt to delete link to Parent Company consortium FAILED");
            e.printStackTrace();
          }
        }

        String message = "Parent cmpy is reset";

        if (deletedConsortia.size() > 0) {
          String consList = "";
          int i;
          for (i = 0; i < deletedConsortia.size() - 1; i++) {
             consList += deletedConsortia.get(i) + ", ";
          }
          consList += deletedConsortia.get(i) + ".";
          message = "Unlinked from Parent Company.  Also removed from Consortia:  " + consList;
        }

        flash(SessionConstants.SESSION_PARAM_MSG, message);
        return redirect(controllers.routes.CompanyController.viewCompany(inCmpy.cmpyid));

      }
    }


    BaseView view = new BaseView();
    view.message = "Parent Cmpy is not added";
    return ok(views.html.common.message.render(view));

  }

  /**
   * Set the email configurations for a company
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result emailConfig() {
    final DynamicForm form = formFactory.form().bindFromRequest();
    String cmpyId = form.get("inCmpyId");
    String host = form.get("inCmpyHost");
    String port = form.get("inCmpyPort");
    String username = form.get("inCmpyUsername");
    String password = form.get("inCmpyPassword");
    String emailDefault = form.get("inCmpyEmailDefault");
    String emailNotice = form.get("inCmpyEmailNotice");
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());


    CompanySmtp cmpySmtp = null;
    Company cmpy = null;

    if (cmpyId != null) {
      cmpySmtp = CompanySmtp.findByPK(cmpyId, 0);
      cmpy = Company.find.byId(cmpyId);
      if (cmpy != null && !SecurityMgr.canEditCmpy(cmpy, sessionMgr)) {
        BaseView view = new BaseView();
        view.message = "System Error - cannot be updated";
        return ok(views.html.common.message.render(view));
      }

      if (cmpySmtp == null) {
          cmpySmtp = CompanySmtp.build(0, cmpyId, sessionMgr.getAccountId());

      }
        try {
          cmpySmtp.setHost(host);
          cmpySmtp.setPort(Integer.parseInt(port));
          cmpySmtp.setUsername(username);
          cmpySmtp.setPassword(password);
          cmpySmtp.setEmailDefault(emailDefault);
          cmpySmtp.setEmailNotice(emailNotice);
          cmpySmtp.setModifiedTs(Timestamp.from(Instant.now()));
          cmpySmtp.setModifiedBy(sessionMgr.getAccountId());
          cmpySmtp.save();
          UmappedEmailFactory.load();
          flash(SessionConstants.SESSION_PARAM_MSG, "Email Configs for - " + cmpy.getName() + " Updated successfully.");
          return redirect(controllers.routes.CompanyController.viewCompany(cmpy.getCmpyid()));
        } catch (Exception e) {
          e.printStackTrace();
          BaseView view = new BaseView();
          view.message = "System Error - cannot Update Email Configs";
          return ok(views.html.common.message.render(view));
        }

    }

    BaseView view = new BaseView();
    view.message = "Email Config was not saved";
    return ok(views.html.common.message.render(view));

  }

  @With({Authenticated.class, AdminAccess.class})
  public Result deleteEmailConfig(String cmpyid) {
    if (cmpyid != null) {
      Company cmpy = Company.find.byId(cmpyid);
      CompanySmtp.deleteByPK(cmpyid, 0);
      UmappedEmailFactory.load();

      flash(SessionConstants.SESSION_PARAM_MSG, "Email Configs for - " + cmpy.getName() + " Updated successfully.");
      return redirect(controllers.routes.CompanyController.viewCompany(cmpyid));
    }
    BaseView view = new BaseView();
    view.message = "Email Config was not deleted";
    return ok(views.html.common.message.render(view));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result addConsortiumToCmpy(String cmpyid) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    final DynamicForm form = formFactory.form().bindFromRequest();
    int consId = Integer.valueOf(form.get("consortia"));
    if (cmpyid != null && consId > 0) {
      Company cmpy = Company.find.byId(cmpyid);
      Consortium cons = Consortium.find.byId(consId);
      if (cmpy != null && cons != null) {
        ConsortiumCompany tempCC = ConsortiumCompany.findUniqueById(consId, cmpy.getCmpyId());
        if (tempCC == null) {
          ConsortiumCompany newCC = ConsortiumCompany.buildConsortiumCompany(consId,
              cmpy.getCmpyId(),
              sessionMgr.getUserId());
          newCC.save();
          flash(SessionConstants.SESSION_PARAM_MSG, "Consortium added successfully.");
        } else {
          flash(SessionConstants.SESSION_PARAM_MSG, "Company already belongs to Consortium.");
        }
        return redirect(controllers.routes.CompanyController.viewCompany(cmpyid));
      }
    }
    BaseView view = new BaseView();
    view.message = "Consortium was not added";
    return ok(views.html.common.message.render(view));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result deleteConsortiumFromCmpy(String cmpyid, int consId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    if (cmpyid != null && consId > 0) {
      Company cmpy = Company.find.byId(cmpyid);
      Consortium cons = Consortium.find.byId(consId);
      if (cmpy != null && cons != null) {
        ConsortiumCompany tempCC = ConsortiumCompany.findUniqueById(consId, cmpy.getCmpyId());
        if (tempCC != null) {
          tempCC.delete();
          flash(SessionConstants.SESSION_PARAM_MSG, "Consortium deleted successfully.");
        } else {
          flash(SessionConstants.SESSION_PARAM_MSG, "Error occurred. Please try again.");
        }
        return redirect(controllers.routes.CompanyController.viewCompany(cmpyid));
      }
    }
    BaseView view = new BaseView();
    view.message = "Consortium was not deleted";
    return ok(views.html.common.message.render(view));
  }

    /**
     * Search operation
     * @param id (Optional) Find consortium by ID
     * @param name (Optional) Find consortium by Name
     * @param cursor (Optional) Return all consortia starting at cursor
     * @param count (Optional) Return maximum count consortia
     */
  @With({Authenticated.class, AdminAccess.class})
  public Result consortiumSearch(Integer id, String name, Integer cursor, Integer count) {

    //Faking request
    RequestMessageJson searchRequest = new RequestMessageJson();
    RequestHeaderJson requestHeader = new RequestHeaderJson();
    searchRequest.header = requestHeader;
    searchRequest.header.setReqId("0");
    searchRequest.header = requestHeader;
    ConsortiumJson requestConsortium = new ConsortiumJson();
    requestConsortium.searchTerm = name;
    requestConsortium.searchId = id;
    requestConsortium.count = count;
    requestConsortium.cursor = cursor;
    searchRequest.body = requestConsortium;

    ResponseMessageJson searchResponse = consortiumSearchResponse(searchRequest);
    return ok(searchResponse.toJson());
  }

  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, AdminAccess.class})
  public Result consortiaSearchDT() {
    JsonNode jsonRequest = request().body().asJson();
    DataTablesForm dtRequest = Json.fromJson(jsonRequest, DataTablesForm.class);
    SessionMgr    sessionMgr = SessionMgr.fromContext(ctx());

    Map<Integer, Company> companies = new HashMap();
    Map<Integer, String> consortiaMap = new HashMap();

    class ConsortiumRow {
      public String companyName;
      public String consortiumName;
      public String delete;
    }

    String keyword = "%%";
    if (dtRequest.search.value != null && !dtRequest.search.value.isEmpty()) {
      keyword = dtRequest.search.value;
    }

    DataTablesView<ConsortiumRow> dtv = new DataTablesView<>(dtRequest.draw);

    dtv.filterConsortium = Integer.valueOf(jsonRequest.get("consortium").asText());

    dtv.searchTerm = "";
    if (jsonRequest.get("keyword").asText() != null && jsonRequest.get("keyword").asText().length() > 0) {
      dtv.searchTerm = "%" + jsonRequest.get("keyword").asText().replace(" ", "%") + "%";
    }
    else {
      dtv.searchTerm = "%";
    }

    dtv.recordsTotal    = ConsortiumCompany.countFilteredConsortia(dtv.searchTerm, dtv.filterConsortium);
    dtv.recordsFiltered = ConsortiumCompany.countFilteredConsortia(dtv.searchTerm, dtv.filterConsortium);
    /*List<ConsortiumCompany> recs = ConsortiumCompany.findByConsortiumFromList(sessionMgr.getUserId(),
        dtRequest.search.value,
        dtRequest.start,
        dtRequest.length);*/
    List<Consortium> consortia = Consortium.find.all();
    for (Consortium c : consortia) {
      consortiaMap.put(c.getConsId(), c.getName());
    }


    List<ConsortiumCompany> consortiumCompanies =  ConsortiumCompany.findByTerm(dtv.searchTerm, dtv.filterConsortium, dtRequest.start, dtRequest.length);
    long currentTime = System.currentTimeMillis();
    for (ConsortiumCompany cc : consortiumCompanies) {
      ConsortiumRow row = new ConsortiumRow();

      Company company = companies.get(cc.getConsCmpyId());
      if (company == null) {
        company = Company.findByCmpyID(cc.getCompId());
        companies.put(cc.getCompId(), company);
      }
      row.companyName = company.getName();
      row.delete =  routes.CompanyController.deleteConsortiumCompany(company.getCmpyId(), cc.getConsId()).url();
      row.consortiumName = consortiaMap.get(cc.getConsId());
      dtv.data.add(row);
    }

    return  ok(Json.toJson(dtv));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result deleteConsortiumCompany(int cmpyId, int consId) {
    try {
      if (cmpyId < 0 || consId < 0) {
        Log.err("Invalid consortium company -  cons_id:" + consId + " cmpy_id:" + cmpyId );
        flash(SessionConstants.SESSION_PARAM_MSG, "Error - consortium company does not exist");
      }
      ConsortiumCompany cc = ConsortiumCompany.findUniqueById(consId, cmpyId);
      Company parentCmpy = null;
      Consortium consortium = Consortium.find.byId(consId);
      if (cc != null) {
        //Find all compnaies linked to parent company and delete their consortium-company link as welll
        parentCmpy = Company.findByCmpyID(cc.getCompId());
        String parentCmpyId = parentCmpy.getCmpyid();
        List<Company> childCmpies = Company.getChildCmpies(parentCmpyId);
        if (childCmpies != null && childCmpies.size() > 0) {
          for (Company child : childCmpies) {
            ConsortiumCompany cc1 = ConsortiumCompany.findUniqueById(consId, child.getCmpyId());
            if (cc1 != null) {
              cc1.delete();
            }
          }
        }
        cc.delete();
        flash(SessionConstants.SESSION_PARAM_MSG, parentCmpy.name + " (and associated companies) successfully deleted from Consortium: "+ consortium.getName());
      } else {
        Log.err("Failed to find consortium company to delete cons_id:" + consId + " cmpy_id:" + cmpyId );
        flash(SessionConstants.SESSION_PARAM_MSG, "Failed to find consortium company to delete");
      }

    } catch (Exception e) {
      Log.err("Attempt to delete consortium company FAILED");
      flash(SessionConstants.SESSION_PARAM_MSG, "Attempt to delete consortium company FAILED");
      e.printStackTrace();
    }
    return redirect(routes.CompanyController.consortia());
  }


  private static ResponseMessageJson consortiumSearchResponse(RequestMessageJson req) {
    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);

    ConsortiumJson consortiumJson = new ConsortiumJson();
    consortiumJson.operation = Operation.LIST;
    responseMessageJson.body = consortiumJson;

    ConsortiumJson reqConsortium = (ConsortiumJson) req.body;
    List<Consortium> consortia;
    List<ConsortiumCompany> consortiumCompanies;
    Map<Integer, Company> companies = new HashMap();

    if (reqConsortium.searchId != null) {
      consortia = new ArrayList<>(1);
      consortia.add(Consortium.find.byId(reqConsortium.searchId));
      consortiumCompanies = ConsortiumCompany.findByConsortiumId(reqConsortium.searchId);
    } else if (reqConsortium.searchTerm != null) {
      consortia = Consortium.findByName(reqConsortium.searchTerm);
      List<Integer> consIds = new ArrayList<>();
      for (Consortium c: consortia) {
        consIds.add(c.getConsId());
      }
      consortiumCompanies = ConsortiumCompany.findByConsortiumFromList(consIds);
    } else {
      consortia = Consortium.find.all();
      consortiumCompanies =  ConsortiumCompany.find.all();
    }

    if (consortia != null) {
      for (Consortium c : consortia) {
        consortiumJson.addConsortium(c.getConsId(), c.getName());
      }
    }

    if (consortiumCompanies != null) {
      for (ConsortiumCompany cc : consortiumCompanies) {
        Company company = companies.get(cc.getConsCmpyId());
        if (company == null) {
          company = Company.findByCmpyID(cc.getCompId());
          companies.put(cc.getCompId(), company);
        }
        consortiumJson.addConsortiumCompany(cc.getConsId(), cc.getCompId(), company.getName());
      }
    }

    return responseMessageJson;
  }

  private static ResponseMessageJson consortiumDeleteResponse(RequestMessageJson req) {
    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);
    ConsortiumJson responseConsortium = new ConsortiumJson();
    responseConsortium.operation = Operation.DELETE;
    List<ConsortiumJson.ConsortiumDetails> respConsortia = new ArrayList<>();
    List<ConsortiumJson.ConsortiumCompanyDetails> respConsortiumCompanies = new ArrayList<>();
    responseConsortium.consortia = respConsortia;
    responseConsortium.companies = respConsortiumCompanies;
    responseMessageJson.body = responseConsortium;

    ConsortiumJson requestConsortium = (ConsortiumJson) req.body;
    String successMsg = "";

    //Deleting all requested consortium to company associations
    try {
      for (ConsortiumJson.ConsortiumCompanyDetails ccd : requestConsortium.companies) {
        if (ccd.consortiumId == null || ccd.companyId == null) {
          continue;
        }
        ConsortiumCompany cc = ConsortiumCompany.findUniqueById(ccd.consortiumId, ccd.companyId);
        if (cc != null) {
          //Find all compnaies linked to parent company and delete their consortium-company link as welll
          Company parentCmpy = Company.findByCmpyID(cc.getCompId());
          Consortium consortium = Consortium.find.byId(cc.getConsId());
          String parentCmpyId = parentCmpy.getCmpyid();
          List<Company> childCmpies = Company.getChildCmpies(parentCmpyId);
          if (childCmpies != null && childCmpies.size() > 0) {
            for (Company child : childCmpies) {
              ConsortiumCompany cc1 = ConsortiumCompany.findUniqueById(ccd.consortiumId, child.getCmpyId());
              if (cc1 != null) {
                cc1.delete();
              }
            }
          }
          cc.delete();
          successMsg = "Successfully deleted Company: " + parentCmpy.getName() + " from consortia: " + consortium.getName();
          respConsortiumCompanies.add(ccd);
        } else {
          Log.err("Fail to find consortium company to delete cons_id:" + ccd.consortiumId + " cmpy_id:" + ccd.companyId );
          responseMessageJson.header.setRespCode(ReturnCode.HTTP_REQ_WRONG_DATA);
        }
      }
    } catch (Exception e) {
      Log.err("Attempt to delete consortium company FAIL");
      e.printStackTrace();
      responseMessageJson.header.setRespCode(ReturnCode.DB_DELETE_FAIL_CASCADE);
    }

    //Deleting all requested consortia
    try {
      for (ConsortiumJson.ConsortiumDetails cd : requestConsortium.consortia) {
        if (cd.id == null) {
          continue;
        }

        Consortium consortium = Consortium.find.byId(cd.id);
        if (consortium != null) {
          String name = consortium.getName();
          consortium.delete();
          successMsg = "Successfully deleted consortia: " + name;
          respConsortia.add(cd);
        } else {
          Log.err("Fail to find consortium to delete cons_id:" + cd.id);
          responseMessageJson.header.setRespCode(ReturnCode.HTTP_REQ_WRONG_DATA);
        }
      }
    }catch (Exception e) {
      Log.err("Attempt to delete consortium FAIL");
      e.printStackTrace();
      responseMessageJson.header.setRespCode(ReturnCode.DB_DELETE_FAIL_CASCADE);
    }

    if (!successMsg.isEmpty()) {
      flash(SessionConstants.SESSION_PARAM_MSG, successMsg);
    }
    return responseMessageJson;
  }

  private static ResponseMessageJson consortiumAddResponse(RequestMessageJson req) {
    SessionMgr sessionMgr = new SessionMgr(session());

    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);
    ConsortiumJson responseConsortium = new ConsortiumJson();
    responseConsortium.operation = Operation.ADD;
    responseMessageJson.body = responseConsortium;
    ConsortiumJson requestConsortium = (ConsortiumJson) req.body;

    List<ConsortiumJson.ConsortiumDetails> respConsortia = new ArrayList<>();
    List<ConsortiumJson.ConsortiumCompanyDetails> respConsortiumCompanies = new ArrayList<>();
    responseConsortium.consortia = respConsortia;
    responseConsortium.companies = respConsortiumCompanies;

    String successMsg = "";

    //Adding all requested consortia
    try {
      for (ConsortiumJson.ConsortiumDetails cd : requestConsortium.consortia) {
        if ((cd.id == null || cd.id == -1) && cd.name != null && cd.name.trim().length() > 0) { //Proper add request
          if (!Consortium.nameExists(cd.name)) {
            Consortium consortium = Consortium.buildConsortium(cd.name.trim(), sessionMgr.getUserId());
            consortium.save();
            cd.id = consortium.getConsId();
            respConsortia.add(cd);
            successMsg = "Successfully added consortium: " + consortium.getName();
          } else {
            Log.err("Duplicate consortium_name: " + cd.name);
            responseMessageJson.header.setRespCode(ReturnCode.HTTP_REQ_WRONG_DATA);
          }
        } else {
          Log.err("Fail to add new consortium consortium_name:" + cd.name);
          responseMessageJson.header.setRespCode(ReturnCode.HTTP_REQ_WRONG_DATA);
        }
      }
    }catch (Exception e) {
      Log.err("Attempt to add new consortium FAIL");
      e.printStackTrace();
      responseMessageJson.header.setRespCode(ReturnCode.DB_TRANSACTION_FAIL);
    }

    //Adding all requested companies to a consortium associations
    try {
      for (ConsortiumJson.ConsortiumCompanyDetails ccd : requestConsortium.companies) {
        if (ccd.companyId != null && ccd.consortiumId != null) {
          ConsortiumCompany consortiumCompany = ConsortiumCompany.buildConsortiumCompany(ccd.consortiumId,
                                                                                         ccd.companyId,
                                                                                         sessionMgr.getUserId());
          consortiumCompany.save();
          //Find all compnaies linked to parent company and add their consortium-company link as well.
          Company parentCmpy = Company.findByCmpyID(consortiumCompany.getCompId());
          Consortium consortium = Consortium.find.byId(consortiumCompany.getConsId());
          String parentCmpyId = parentCmpy.getCmpyid();
          List<Company> childCmpies = Company.getChildCmpies(parentCmpyId);
          if (childCmpies != null && childCmpies.size() > 0) {
            for (Company child : childCmpies) {

              ConsortiumCompany tempCC = ConsortiumCompany.findUniqueById(ccd.consortiumId, child.getCmpyId());
              if (tempCC == null) {
                ConsortiumCompany newCC = ConsortiumCompany.buildConsortiumCompany(ccd.consortiumId,
                                                                                    child.getCmpyId(),
                                                                                    sessionMgr.getUserId());
                newCC.save();
              }
            }
          }
          respConsortiumCompanies.add(ccd);
          successMsg = "Successfully added company: " + parentCmpy.getName() + " to consortium: "+ consortium.getName();
        }  else {
          Log.err("Fail to add company to a consortium id:" + ccd.consortiumId + " company name:" + ccd.companyName);
          responseMessageJson.header.setRespCode(ReturnCode.HTTP_REQ_WRONG_DATA);
        }
      }
    } catch (Exception e) {
      Log.err("Attempt to add consortium company association FAIL");
      e.printStackTrace();
      responseMessageJson.header.setRespCode(ReturnCode.DB_TRANSACTION_FAIL);
    }
    if (!successMsg.isEmpty()) {
      flash(SessionConstants.SESSION_PARAM_MSG, successMsg);
    }
    return responseMessageJson;
  }

  private static ResponseMessageJson consortiumUpdateResponse(RequestMessageJson req) {
    SessionMgr sessionMgr = new SessionMgr(session());

    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);
    ConsortiumJson responseConsortium = new ConsortiumJson();
    responseConsortium.operation = Operation.UPDATE;
    responseMessageJson.body = responseConsortium;
    ConsortiumJson requestConsortium = (ConsortiumJson) req.body;

    String successMsg = "";

    List<ConsortiumJson.ConsortiumDetails> respConsortia = new ArrayList<>();

    //Adding all requested consortia
    try {
      for (ConsortiumJson.ConsortiumDetails cd : requestConsortium.consortia) {
        if (cd.id == null || cd.id == -1 || cd.name == null || cd.name.trim().length() == 0) {
          continue; // If bad consortium entry sent - just ignoring it
        }

        Consortium consortium = Consortium.find.byId(cd.id);
        if (consortium != null) {
          consortium.setName(cd.name);
          consortium.update();
          respConsortia.add(cd);
          successMsg = cd.name + " successfully updated!";
        } else {
          Log.err("Fail to update consortium id:"+ cd.id +" consortium_name:" + cd.name);
          responseMessageJson.header.setRespCode(ReturnCode.DB_RECORD_NOT_FOUND);
        }
      }
    }catch (Exception e) {
      Log.err("Attempt to update consortium FAIL");
      e.printStackTrace();
      responseMessageJson.header.setRespCode(ReturnCode.DB_TRANSACTION_FAIL);
    }

    //Nothing to update in consortium companies, they can either be added or deleted
    responseConsortium.consortia = respConsortia;
    responseConsortium.companies = null;
    if (!successMsg.isEmpty()) {
      flash(SessionConstants.SESSION_PARAM_MSG, successMsg);
    }
    return responseMessageJson;
  }


  /**
   * Perform operation on the request (Operations Router)
   */
  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, AdminAccess.class})
  public Result consortiumExecute() {
    RequestMessageJson consRequest = RequestMessageJson.fromJson(request().body().asJson());
    ResponseMessageJson consResponse = null;

    ConsortiumJson consortiumJson = (ConsortiumJson) consRequest.body;
    switch(consortiumJson.operation) {
      case ADD:
      case COPY:
        consResponse = consortiumAddResponse(consRequest);
        break;
      case UPDATE:
        consResponse = consortiumUpdateResponse(consRequest);
        break;
      case DELETE:
        consResponse = consortiumDeleteResponse(consRequest);
        break;
      case LIST:
      case SEARCH:
        consResponse = consortiumSearchResponse(consRequest);
        break;
    }

    Log.debug("Resp:" + consResponse.toJson());
    return ok(consResponse.toJson());
  }


    /**
     * Companies requests router
     */
  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, AdminAccess.class})
  public Result companyExecute() {
    RequestMessageJson jsonRequest = RequestMessageJson.fromJson(request().body().asJson());
    ResponseMessageJson jsonResponse = null;

    CompanyJson companyJson = (CompanyJson) jsonRequest.body;
    switch(companyJson.operation) {
      case ADD:
      case COPY:
        jsonResponse = new ResponseMessageJson(jsonRequest.header, ReturnCode.NOT_IMPLEMENTED);
        break;
      case UPDATE:
        jsonResponse = new ResponseMessageJson(jsonRequest.header, ReturnCode.NOT_IMPLEMENTED);
        break;
      case DELETE:
        jsonResponse = new ResponseMessageJson(jsonRequest.header, ReturnCode.NOT_IMPLEMENTED);
        break;
      case LIST:
      case SEARCH:
        jsonResponse = companySearchResponse(jsonRequest);
        break;
    }

    Log.debug("Resp:" + jsonResponse.toJson());
    return ok(jsonResponse.toJson());
  }

  /**
   * Admin company search
   * @param req
   * @return
   */
  private static ResponseMessageJson companySearchResponse(RequestMessageJson req) {
    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);

    CompanyJson companyResponseJson = new CompanyJson();
    companyResponseJson.operation = Operation.LIST;
    responseMessageJson.body = companyResponseJson;

    CompanyJson companyRequestJson = (CompanyJson) req.body;
    companyResponseJson.companies = new ArrayList<>();
    List<Company> companies;

    if (companyRequestJson.searchId != null) {
      companies = new ArrayList<>(1);
      companies.add(Company.findByCmpyID(companyRequestJson.searchId));
    } else if (companyRequestJson.searchTerm != null) {
      companies = Company.findByKeyword(companyRequestJson.searchTerm);
    } else {
      companies = Company.find.all();
    }

    for (Company c : companies) {
      companyResponseJson.addCompany(c.getCmpyId(), c.getCmpyid(), c.getName());
    }

    return responseMessageJson;
  }

  /**
   * Traditional
   * @return
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result consortia() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    ConsortiaView cv = new ConsortiaView();
    cv.isUMappedAdmin = SecurityMgr.isUmappedAdmin(sessionMgr);

    List<Consortium> consortia = Consortium.findAllSorted("name asc");
    cv.consortiaMap = new LinkedHashMap<>();
    if (consortia != null && consortia.size() > 0) {
      for (Consortium c : consortia) {
        cv.consortiaMap.put(c.getConsId(), c.getName());
      }
    }

    cv.message = flash(SessionConstants.SESSION_PARAM_MSG);


    return  ok(views.html.cmpy.consortia.render(cv));
  }
}
