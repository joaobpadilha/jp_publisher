package controllers;

import actors.ActorsHelper;
import actors.MobilizerActor;
import actors.SupervisorActor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.mapped.common.CoreConstants;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.*;
import com.mapped.publisher.parse.Travel42;
import com.mapped.publisher.persistence.PageAttachType;
import com.mapped.publisher.persistence.cache.MobilizerStateCO;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import com.umapped.api.schema.common.RequestMessageJson;
import com.umapped.api.schema.common.ResponseMessageJson;
import com.umapped.api.schema.local.T42LoginJson;
import com.umapped.api.schema.local.WebSearchJson;
import com.umapped.api.schema.types.Operation;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.external.bing.web.BingWeb;
import com.umapped.external.bing.web.Value;
import models.publisher.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.owasp.html.Sanitizers;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.concurrent.HttpExecutionContext;
import play.libs.ws.WS;
import play.libs.ws.WSRequest;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by surge on 2014-12-10.
 */
public class MobilizerController
    extends Controller {

  @Inject
  FormFactory formFactory;
  @Inject
  HttpExecutionContext ec;

  private final static boolean DEBUG = false;

  private final static int FF_TIMEOUT = 120000; //10 seconds

  /**
   * Perform operation on the request (Operations Router)
   */
  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result t42Login() {
    final RequestMessageJson mobilizerRequest = RequestMessageJson.fromJson(request().body().asJson());
    ResponseMessageJson mobilizerResponse = null;
    final T42LoginJson loginJson = (T42LoginJson) mobilizerRequest.body;

    switch(loginJson.operation) {
      case AUTHENTICATE: //Upload is overloaded to handle processing request
        mobilizerResponse = travel42Login(mobilizerRequest);
        break;
      case STATE:
        mobilizerResponse = travel42LoginState(mobilizerRequest);
        break;
      default:
        mobilizerResponse = new ResponseMessageJson(mobilizerRequest.header, ReturnCode.NOT_SUPPORTED);
        break;
    }

    return ok(mobilizerResponse.toJson());
  }

  private static ResponseMessageJson travel42Login(final RequestMessageJson req) {
    final StopWatch sw = new StopWatch();
    sw.start();

    final SessionMgr sessionMgr = new SessionMgr(session());
    final ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);
    final T42LoginJson loginRequest = (T42LoginJson) req.body;

    final T42LoginJson responseBody = new T42LoginJson();
    responseMessageJson.body = responseBody;
    responseBody.operation = Operation.AUTHENTICATE;


    if (loginRequest.username == null ||
        loginRequest.password == null ||
        loginRequest.username.trim().length() == 0 ||
        loginRequest.password.trim().length() == 0) {
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA)
                                .withMsg("Travel42 Username and Password can not be empty");
      return responseMessageJson;
    }


    final MobilizerActor.MobilizerMessage msg =
        new MobilizerActor.MobilizerMessage(MobilizerActor.MobilizerCommand.T42_LOGIN);

    msg.setUuid(req.header.getReqId());
    msg.setUserId(sessionMgr.getUserId());
    msg.setUsername(loginRequest.username);
    msg.setPassword(loginRequest.password);
    ActorsHelper.tell(SupervisorActor.UmappedActor.MOBILIZER, msg);

    responseBody.jobId = req.header.getReqId();

    return responseMessageJson;
  }

  private static ResponseMessageJson travel42LoginState(final RequestMessageJson req) {
    final StopWatch sw = new StopWatch();
    sw.start();

    final ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);
    final T42LoginJson loginRequest = (T42LoginJson) req.body;

    final T42LoginJson responseBody = new T42LoginJson();
    responseMessageJson.body = responseBody;
    responseBody.operation = Operation.STATE;

    MobilizerStateCO stateCO = MobilizerStateCO.getFromCache(loginRequest.jobId);

    responseBody.jobId = req.header.getReqId();
    responseBody.state = stateCO.currState.name();

    if (stateCO.currState == MobilizerStateCO.State.SUCCESS) {
      responseBody.links = stateCO.links;
    }

    return responseMessageJson;
  }

  /**
   * Perform operation on the request (Operations Router)
   */
  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public CompletionStage<Result> mobilizerExecute() {
    final RequestMessageJson mobilizerRequest = RequestMessageJson.fromJson(request().body().asJson());
    CompletionStage<ResponseMessageJson> mobilizerResponse = null;
    final WebSearchJson webSearchJson = (WebSearchJson) mobilizerRequest.body;

    switch(webSearchJson.operation) {
      case UPLOAD: //Upload is overloaded to handle processing request
        mobilizerResponse = mobilizerResponse(mobilizerRequest);
        break;
      case ADD:
      case COPY:
        mobilizerResponse = mobilizedPageAddResponse(mobilizerRequest);
        break;
      case STATE: //Update is used to retrieve status and mobilized pages
        mobilizerResponse = mobilizerGetResult(mobilizerRequest);
        break;
      case DELETE:
        mobilizerResponse = CompletableFuture.completedFuture(new ResponseMessageJson(mobilizerRequest.header, ReturnCode.NOT_SUPPORTED));
        break;
      case LIST:
      case SEARCH:
        mobilizerResponse = webSearchResponse(mobilizerRequest);
        break;
    }

    return mobilizerResponse.thenApplyAsync(mobilizerResponse1 -> {
      //DEBUG CODE TO TEST TIMEOUTs
      //if (webSearchJson.operation == Operation.UPLOAD) {
      //  return new Status(play.core.j.JavaResults.ServiceUnavailable(), "Timeout :(", Codec.javaSupported("utf8"));
      //}
      return ok(mobilizerResponse1.toJson());
    });
  }

  /**
   * Looking for term in a list of predefined websites
   * @param req
   * @return
   */
  private CompletionStage<ResponseMessageJson> mobilizedPageAddResponse(final RequestMessageJson req) {
    final SessionMgr sessionMgr = new SessionMgr(session());
    final ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);
    final WebSearchJson webSearchRequestJson = (WebSearchJson) req.body;

    return  CompletableFuture.supplyAsync(() -> {
      StopWatch sw = new StopWatch();
      sw.start();

      Destination dest = null;
      Trip trip = null;

      if (webSearchRequestJson.id != null && !webSearchRequestJson.id.isEmpty()) {
        dest = Destination.find.byId(webSearchRequestJson.id);
        if (dest != null && !SecurityMgr.canAccessDestination(dest, sessionMgr)) {
          dest = null;
        }
      } else if (webSearchRequestJson.tripId != null && !webSearchRequestJson.tripId.isEmpty()) {
        trip = Trip.findByPK(webSearchRequestJson.tripId);
        if (trip != null && SecurityMgr.getTripAccessLevel(trip, sessionMgr.getCredentials().getAccount()).le(SecurityMgr.AccessLevel.READ)) {
          trip = null;
        }
      }

      if (trip == null && dest == null) {
        responseMessageJson.header.withCode(ReturnCode.AUTH_DOCUMENT_FAIL);
        return responseMessageJson;
      }


      if (webSearchRequestJson.pages == null || webSearchRequestJson.pages.size() == 0) {
        responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
        return responseMessageJson;
      }


      for (WebSearchJson.PageDetails pageDetails : webSearchRequestJson.pages) {
        String libPageId = pageDetails.resultId;
        LibraryPage libPage = LibraryPage.find.byId(libPageId);

        if (libPage == null) {
          responseMessageJson.header.withCode(ReturnCode.DB_RECORD_NOT_FOUND);
          return responseMessageJson;
        }
        if (dest != null) {
          addDestinationGuide(dest, sessionMgr, libPage, pageDetails);
        } else if (trip != null) {
          addTripNote (trip, sessionMgr, libPage, pageDetails, webSearchRequestJson.date);
        }


      }

      sw.stop();

      responseMessageJson.body = webSearchRequestJson; //We can reuse body as nothing has changed

      Log.debug("MOBILIZER: ADD Response prepared in:" + sw.getTime() + "ms");
      responseMessageJson.header.withCode(ReturnCode.SUCCESS);
      return responseMessageJson;
    }, ec.current());
  }

  private void addDestinationGuide (Destination dest, SessionMgr sessionMgr, LibraryPage libPage, WebSearchJson.PageDetails pageDetails) {
    DestinationGuide destGuide = DestinationGuide.buildGuide(dest, sessionMgr.getUserId());
    destGuide.setPageId(libPage.getPageId());
    int maxRow = DestinationGuide.maxPageRankByDestId(dest.destinationid);
    destGuide.setRank(++maxRow);
    destGuide.setName(pageDetails.title);
    destGuide.setLoclat(0.0f);
    destGuide.setLoclong(0.0f);
    destGuide.save();

    //Adding destination guide images
    List<LibraryPageImage> images = LibraryPageImage.byPageId(libPage.getPageId());
    int imgRank = 0;
    for(LibraryPageImage lpi: images) {
      DestinationGuideAttach dga = DestinationGuideAttach.buildGuideAttachment(destGuide, sessionMgr.getUserId());
      dga.setName(lpi.getFileImage().getFilename());
      dga.setAttachname(lpi.getFileImage().getFilename());
      dga.setAttachurl(lpi.getImageUrl());
      dga.setFileImage(lpi.getFileImage());
      dga.setAttachtype(PageAttachType.PHOTO_LINK);
      dga.setRank(++imgRank);
      if (lpi.getFileImage() != null) {
        dga.setFile(lpi.getFileImage().getFile());
      }
      dga.save();
    }

    DestinationPostController.invalidateCache(destGuide.destinationid);
  }

  private void addTripNote (Trip trip, SessionMgr sessionMgr, LibraryPage libPage, WebSearchJson.PageDetails pageDetails, String date) {
    TripNote tripNote = TripNote.buildTripNote(trip, sessionMgr.getUserId());
    tripNote.setPageId(libPage.getPageId());
    tripNote.setRank(0);
    tripNote.setName(pageDetails.title);
    tripNote.setLocLat(0.0f);
    tripNote.setLocLong(0.0f);

    if (libPage.getDomain().contains("indagare") && libPage.getContent() != null) {
      String s = Sanitizers.FORMATTING.and(Sanitizers.LINKS)
              .and(Sanitizers.STYLES)
              .and(Sanitizers.BLOCKS)
              .and(Sanitizers.TABLES)
              .sanitize(libPage.getContent().replaceAll("\n", "<br/>"));
      s = s.replace("<li></li>","")
      .replace("<li> </li>","")
      .replace("<li><br />   </li>","")
      .replace("<li><br />  </li>","");
      tripNote.setIntro(s);
    } else {
     tripNote.setIntro(libPage.getContent());
    }
    if (date != null && date.trim().length() > 0) {
      try {
        DateUtils du = new DateUtils();
        Date d = du.parseDate(date, "EEE MMM dd, yyyy");
        tripNote.setNoteTimestamp(d.getTime());
      }
      catch (Exception e) {
      }
    }
    tripNote.setImportTs(tripNote.getCreatedTimestamp());
    tripNote.setImportSrc(BookingSrc.ImportSrc.EXTERNAL_SITE);
    tripNote.save();

    //Adding destination guide images
    List<LibraryPageImage> images = LibraryPageImage.byPageId(libPage.getPageId());
    int imgRank = 0;
    for(LibraryPageImage lpi: images) {
      TripNoteAttach tna = TripNoteAttach.build(tripNote, sessionMgr.getUserId());
      tna.setName(lpi.getFileImage().getFilename());
      tna.setAttachName(lpi.getFileImage().getFilename());
      tna.setAttachUrl(lpi.getImageUrl());
      tna.setFileImage(lpi.getFileImage());
      tna.setAttachType(PageAttachType.PHOTO_LINK);
      tna.setRank(++imgRank);
      if (lpi.getFileImage() != null) {
        tna.setFile(lpi.getFileImage().getFile());
      }
      tna.save();
    }

    BookingNoteController.invalidateCache(trip.tripid);
  }

  private CompletionStage<ResponseMessageJson> mobilizerGetResult(final RequestMessageJson req) {

    final StopWatch sw = new StopWatch();
    sw.start();

    final SessionMgr sessionMgr = new SessionMgr(session());
    final ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);
    final WebSearchJson webSearchRequestJson = (WebSearchJson) req.body;

    Destination dest = Destination.find.byId(webSearchRequestJson.id);
    if (dest == null || !SecurityMgr.canAccessDestination(dest, sessionMgr)) {
      responseMessageJson.header.withCode(ReturnCode.AUTH_DOCUMENT_FAIL);
      return CompletableFuture.completedFuture(responseMessageJson);
    }

    final WebSearchJson responseBody = new WebSearchJson();
    responseMessageJson.body = responseBody;
    responseBody.operation = Operation.STATE;
    responseBody.id = webSearchRequestJson.id;
    responseBody.searchTerm = webSearchRequestJson.searchTerm;
    responseBody.cmpyId = webSearchRequestJson.cmpyId;

    MobilizerStateCO stateCO = MobilizerStateCO.getFromCache(webSearchRequestJson.jobId);

    responseBody.state = stateCO.currState.name();

    switch (stateCO.currState) {
      case SUCCESS:
        if (stateCO.pages == null) {
          responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_FAIL)
                                    .withMsg("travel42 cover - sub pages can't be found");
          CompletableFuture.completedFuture(responseMessageJson);
        }
        for (LibraryPage lp : stateCO.pages) {
          WebSearchJson.PageDetails page = new WebSearchJson.PageDetails();
          page.content = lp.getContent();
          page.title = lp.getTitle();
          page.author = lp.getAuthor();
          page.resultId = lp.getPageId();
          page.url = lp.getUrl();
          page.resultId = lp.getPageId();
          responseBody.addPage(page);
        }
        break;
      case UNKNOWN_ERROR:
      case STOPPED:
        responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_FAIL).withMsg("travel42 connection failed");
        return CompletableFuture.completedFuture(responseMessageJson);
      default:
        break;
    }

    return CompletableFuture.completedFuture(responseMessageJson);
  }

  private CompletionStage<ResponseMessageJson> mobilizerResponse(final RequestMessageJson req) {
    final StopWatch sw = new StopWatch();
    sw.start();

    final SessionMgr sessionMgr = new SessionMgr(session());
    Account account;
    if(sessionMgr.getAccount().isPresent()){
      account = sessionMgr.getAccount().get();
    } else {
      account = Account.find.byId(sessionMgr.getAccountId());
    }

    final ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);
    final WebSearchJson webSearchRequestJson = (WebSearchJson) req.body;

    Destination dest = null;
    Trip trip = null;
    if (webSearchRequestJson.id != null && !webSearchRequestJson.id.isEmpty()) {
       dest = Destination.find.byId(webSearchRequestJson.id);
       if (dest != null && !SecurityMgr.canAccessDestination(dest, sessionMgr)) {
        dest = null;
       }
    } else if (webSearchRequestJson.tripId != null && !webSearchRequestJson.tripId.isEmpty()) {
      trip = Trip.findByPK(webSearchRequestJson.tripId);
      if (trip == null || SecurityMgr.getTripAccessLevel(trip, sessionMgr.getCredentials().getAccount()).le(SecurityMgr.AccessLevel.READ)) {
        trip  = null;
      }
    }

    if (dest == null && trip == null) {
      responseMessageJson.header.withCode(ReturnCode.AUTH_DOCUMENT_FAIL);
      return CompletableFuture.completedFuture(responseMessageJson);
    }

    final WebSearchJson responseBody = new WebSearchJson();
    responseMessageJson.body = responseBody;
    responseBody.operation = Operation.UPLOAD;
    responseBody.id = webSearchRequestJson.id;
    responseBody.tripId = webSearchRequestJson.tripId;
    responseBody.date = webSearchRequestJson.date;
    responseBody.searchTerm = webSearchRequestJson.searchTerm;
    responseBody.cmpyId = webSearchRequestJson.cmpyId;

    final WebSearchJson.PageDetails pageToProcess = webSearchRequestJson.pages.get(0);


    final LibraryPage cachedPage = LibraryPage.findPage(pageToProcess.url);
    //If page exists in cache and is older than 30 days old - try to re-fetch it.
    if (!DEBUG &&
        cachedPage != null &&
        !Utils.isOlderThan(cachedPage.getCreatedtimestamp(), 30)) {
      pageToProcess.content = cachedPage.getContent();
      pageToProcess.title = cachedPage.getTitle();
      pageToProcess.author = cachedPage.getAuthor();
      pageToProcess.resultId = cachedPage.getPageId();
      responseBody.addPage(pageToProcess);

      Log.debug("MOBILIZER found CACHED version!");
      responseMessageJson.header.withCode(ReturnCode.SUCCESS);
      return CompletableFuture.completedFuture(responseMessageJson);
    }

    FileSrc.FileSrcType srcType = FileSrc.FileSrcType.IMG_MOBILIZER;

    /* Travel 42 check */
    if (Travel42.isTravel42(pageToProcess.url)) {
      if (Travel42.isTravel42Cover(pageToProcess.url)) {

        final MobilizerActor.MobilizerMessage msg =
            new MobilizerActor.MobilizerMessage(MobilizerActor.MobilizerCommand.T42_MOBILIZE_TRIP_PLAN);

        msg.setUuid(req.header.getReqId());
        msg.setUserId(sessionMgr.getUserId());
        msg.setUrl(pageToProcess.url);

        ActorsHelper.tell(SupervisorActor.UmappedActor.MOBILIZER, msg);

        responseBody.jobId = req.header.getReqId();
        responseMessageJson.header.withCode(ReturnCode.SUCCESS);
        return CompletableFuture.completedFuture(responseMessageJson);
      }
    }


    WSRequest urlRequest = getMobilizerRequestHolder(pageToProcess.url);

    return urlRequest
          .get()
          .thenApplyAsync(response -> {
              JsonNode ffResult = response.asJson();

              pageToProcess.content = ffResult.get("content").asText();
              pageToProcess.title = ffResult.get("title").asText();
              pageToProcess.author = ffResult.get("author").asText();
              responseBody.addPage(pageToProcess);
              LibraryPage lp;
              if (cachedPage != null) {
                lp = cachedPage;
                lp.withContent(pageToProcess.content)
                  .withAuthor(pageToProcess.author)
                  .withTitle(pageToProcess.title);
                lp.setCreatedtimestamp(System.currentTimeMillis());
                lp.update();
                Log.debug("Updating cache for library page: " + lp.getUrl());

              } else {
                lp = LibraryPage.buildLibraryPage(pageToProcess.url, sessionMgr.getUserId())
                                .withAuthor(pageToProcess.author)
                                .withContent(pageToProcess.content)
                                .withDomain(Utils.getDomainName(pageToProcess.url))
                                .withLocale(ffResult.get("language").asText())
                                .withSearchTerm(webSearchRequestJson.searchTerm)
                                .withTitle(pageToProcess.title);
                lp.save();
              }

            String modContent = extactAndSaveImages(lp.getContent(),
                                                    lp.getSearchTerm(),
                                                    lp.getPageId(),
                                                    lp.getUrl(),
                                                    lp.getTitle(),
                                                    account,
                                                    srcType);

            if (modContent != null) {

                lp.setContent(modContent);
                pageToProcess.content = modContent;
                lp.update();
              }
              else {
                Log.err("Failed to extract images from HTML content for page: " + lp.getPageId());
              }

              pageToProcess.resultId = lp.getPageId();

              sw.stop();

              Log.debug("MOBILIZER: Mobilization for <"+lp.getUrl()+"> done in:" + sw.getTime() + "ms");
              responseMessageJson.header.withCode(ReturnCode.SUCCESS);
              return responseMessageJson;
            }
          );
  }

  public static List<LibraryPage> saveMobilizedContent(String author, List<JsonNode> mobilizedPages) {

    if (mobilizedPages.size() == 0) {
      return null;
    }

    SessionMgr sessionMgr  = new SessionMgr(session());

    List<LibraryPage> pages = new ArrayList<>();
    for (JsonNode jn : mobilizedPages) {
      LibraryPage lp = saveMobilizedPage(author, jn, sessionMgr.getUserId());
      pages.add(lp);
    }
    return pages;
  }

  public static LibraryPage saveMobilizedPage(String author, JsonNode mobilizedPage, String userId) {
    Account     account     = Account.findActiveByLegacyId(userId);
    LibraryPage libraryPage = null;
    if (mobilizedPage != null && mobilizedPage.get("url") != null) {
      String      currUrl    = mobilizedPage.get("url").asText() + "&ff1";
      LibraryPage cachedPage = LibraryPage.findPage(currUrl);
      if (cachedPage == null) {
        libraryPage = LibraryPage.buildLibraryPage(currUrl, userId)
                                 .withAuthor(author)
                                 .withContent(mobilizedPage.get("content").asText())
                                 .withDomain(Utils.getDomainName(currUrl))
                                 .withLocale(mobilizedPage.get("language").asText())
                                 .withTitle(mobilizedPage.get("title").asText());
        libraryPage.save();

        String modContent = extactAndSaveImages(libraryPage.getContent(),
                                                libraryPage.getSearchTerm(),
                                                libraryPage.getPageId(),
                                                libraryPage.getUrl(),
                                                libraryPage.getTitle(),
                                                account,
                                                FileSrc.FileSrcType.IMG_MOBILIZER);
        if (modContent != null) {
          libraryPage.setContent(modContent);
          libraryPage.update();
        }
        else {
          Log.err("Failed to extract images from HTML content for page: " + libraryPage.getPageId());
        }
      }
      else {
        cachedPage.setContent(mobilizedPage.get("content").asText()); //Updating content in case it has changed
        cachedPage.withTitle(mobilizedPage.get("title").asText());//Updating content in case it has changed
        cachedPage.update();
        libraryPage = cachedPage;
      }
    }
    return libraryPage;
  }

  public static WSRequest getMobilizerRequestHolder(String url) {
    try {
      String domainName = Utils.getDomainName(url);

      String fiveFiltersUrl = ConfigMgr.getAppParameter( CoreConstants.FIVE_FILTERS) + "extract.php";
      SysContentSite siteConfig = SysContentSite.findByDomain(domainName);
      WSRequest urlRequest = WS.url(fiveFiltersUrl).setRequestTimeout(FF_TIMEOUT);

      if(url.toLowerCase().contains("indagare.com")) {
        String encodedUrl = URLEncoder.encode(url, "UTF-8");
        urlRequest.setQueryParameter("url", ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL) + routes.MobilizerController.mobilizeIndagare(encodedUrl).url());
      } else {
        urlRequest.setQueryParameter("url", url);
      }

      if (siteConfig != null && siteConfig.getParseConfig() != null) {
        urlRequest.setQueryParameter("siteconfig", siteConfig.getParseConfig()); //Preparing for FiveFilters v3.4
      }

      Log.debug("MobilizerController: Successfully mobilized content");

      return urlRequest;
    } catch (Exception e) {
      Log.err("MobilizerController: Could not mobilize content");
    }

    return null;
  }


  public Result mobilizeIndagare(String encodedUrl) {
    try {
      String decodedUrl = URLDecoder.decode(encodedUrl, "UTF-8");

      BasicCookieStore httpCookieStore = new BasicCookieStore();
      String url = "https://www.indagare.com/wp-admin/admin-ajax.php";
      HttpClient client = HttpClientBuilder.create().setDefaultCookieStore(httpCookieStore).build();

      HttpGet getSecurity = new HttpGet(decodedUrl);
      HttpResponse pageResponse = client.execute(getSecurity);
      String page = EntityUtils.toString(pageResponse.getEntity());

      Document security = Jsoup.parse(page.toString());
      String securityToken = security.getElementById("security").val();

      HttpClientUtils.closeQuietly(pageResponse);

      List<NameValuePair> form = new ArrayList<>();
      form.add(new BasicNameValuePair("action", "indlogin"));
      form.add(new BasicNameValuePair("username", "umapped@indagare.com"));
      form.add(new BasicNameValuePair("password", "grayChal<34"));
      form.add(new BasicNameValuePair("security", securityToken));
      UrlEncodedFormEntity entity = new UrlEncodedFormEntity(form, Consts.UTF_8);

      HttpPost post = new HttpPost(url);
      post.setHeader("accept", "application/json, text/javascript, */*; q=0.01");
      post.setHeader("accept-encoding", "gzip, deflate, br");
      post.setHeader("accept-language", "en-US,en;q=0.9,fr-FR;q=0.8,fr-CA;q=0.7,fr;q=0.6");
      post.setHeader("cache-control", "no-cache");
      post.setHeader("cookie", "WP-LastViewedPosts=a%3A1%3A%7Bi%3A0%3Bi%3A55211%3B%7D; _ga=GA1.2.1005813563.1517515995; _gid=GA1.2.1625749148.1517515995; __hstc=58719733.abb4ef9522c2757240f127f967317e6a.1517515997015.1517515997015.1517515997015.1; __hssrc=1; hubspotutk=abb4ef9522c2757240f127f967317e6a; wordpress_test_cookie=WP+Cookie+check; __unam=f4bfff9-1611e7752c9-6dd8dc2b-48; __hssc=58719733.14.1517515997016");
      post.setHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8");
      post.setHeader("origin", "https://www.indagare.com");
      post.setHeader("pragma", "no-cache");
      post.setHeader("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
      post.setHeader("x-requested-with", "XMLHttpRequest");
      post.setEntity(entity);

      HttpResponse loginResponse = client.execute(post);

      HttpGet get2 = new HttpGet(decodedUrl);
      HttpResponse contentResponse = client.execute(get2);

      String doc = EntityUtils.toString(contentResponse.getEntity());
      if (doc.contains("mapmarkerjson")) {
        doc = doc.replaceFirst("<div id=\"mapmarkerjson\".*.</div>","");
      }

      Log.debug("MobilizerController: Successful log in to Indagare for: " + decodedUrl);

      return ok(Html.apply(doc.toString()));
    } catch(Exception e) {
      Log.debug("MobilizerController: Could not log into Indagare");
    }
    return internalServerError();
  }

  /**
   * Performs request to FiveFilters to mobilize a page using Apache Http Client
   *
   * Synchronized retrieval of the the page. To be used from Actors
   * @return
   */
  public static JsonNode mobilizeUrl(String url) {
    JsonNode result = null;
    StopWatch sw = new StopWatch();
    sw.start();

    try {
      String fiveFiltersUrl = ConfigMgr.getAppParameter( CoreConstants.FIVE_FILTERS) + "extract.php";
      String domainName = Utils.getDomainName(url);
      SysContentSite siteConfig = SysContentSite.findByDomain(domainName);

      RequestConfig rc = RequestConfig.custom()
                                      .setConnectionRequestTimeout(FF_TIMEOUT)
                                      .setConnectTimeout(FF_TIMEOUT)
                                      .setSocketTimeout(FF_TIMEOUT)
                                      .build();


      try(CloseableHttpClient httpClient = HttpClientBuilder.create()
                                                        .setDefaultRequestConfig(rc)
                                                        .build()) {

        HttpPost postRequest = new HttpPost(fiveFiltersUrl);


        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("url", url));

        if (siteConfig != null && siteConfig.getParseConfig() != null) {
          nvps.add(new BasicNameValuePair("siteconfig", siteConfig.getParseConfig()));
        }

        postRequest.setEntity(new UrlEncodedFormEntity(nvps));
        HttpResponse response = httpClient.execute(postRequest);

        if (response.getStatusLine() != null &&
            response.getStatusLine().getStatusCode() == 200) {
          String body = EntityUtils.toString(response.getEntity());
          ObjectMapper mapper = new ObjectMapper();
          result = mapper.readTree(body);
        }
      } catch (Exception e) {
        Log.err("MobilizerController()::mobilizeUrl: HttpClient error.", e);
      }

    } catch (Exception e) {
      e.printStackTrace();
    }

    sw.stop();
    return result;
  }

  private static String extactAndSaveImages(String content,
                                            String searchTerm,
                                            String pageId,
                                            String pageUrl,
                                            String pageTitle, Account account, FileSrc.FileSrcType sourceType) {
    Document doc        = Jsoup.parse(content, "UTF-8");
    Elements images     = doc.getElementsByTag("img");
    int      imgCounter = 0;
    for (Element img : images) {
      ++imgCounter;

      String imgSrc = cleanupUrl(img.attr("src"));
      Log.debug("Found image url:" + imgSrc);
      try {
        StringBuilder fnBuilder = new StringBuilder();

        String fileName = Utils.getFilenameFromUlr(imgSrc);
        String ext = FilenameUtils.getExtension(fileName);
        if (fileName.length() < 64) {
          switch (ext.toLowerCase()) {
            case "png":
            case "gif":
            case "jpg":
            case "jpeg":
            case "bmp":
            case "tiff":
            case "tif":
              fnBuilder.append(fileName);
          }
        }

        //No name thus far
        if (fnBuilder.length() == 0) {
          String httpExtension = Utils.getExtension(imgSrc);
          fnBuilder.append("article_img_");
          fnBuilder.append(imgCounter);

          if(ext != null && ext.length() > 2) {
            fnBuilder.append(".");
            fnBuilder.append(httpExtension);
          } else if (httpExtension != null) {
            fnBuilder.append(".");
            fnBuilder.append(httpExtension);
          }
        }

        FileImage image = ImageController.downloadAndSaveImage(imgSrc,
                                                               fnBuilder.toString(),
                                                               sourceType,
                                                               searchTerm,
                                                               account,
                                                               false);

        if(image != null) {
          LibraryPageImage lpi = LibraryPageImage.build(pageId, image);
          lpi.save();
        }
        img.attr("src", image.getUrl());
      } catch(Exception e) {
        e.printStackTrace();
        return null;
      }
    }

    //Since we already have a parse tree - let's cleanup the links
    Elements links = doc.getElementsByTag("a");
    for (Element a: links) {
      a.attr("target","_blank");
    }

    String html =  doc.body().html();
    if (html != null) {
      //add attribution
      StringBuilder sb = new StringBuilder();
      if (pageUrl != null && pageTitle != null) {
        sb.append("<div style='padding-bottom:40px;font-size:14px'> <a  href='");
        sb.append(pageUrl);
        sb.append("' target=_blank>");
        if (Travel42.isTravel42(pageUrl)) {
          sb.append("Retrieved from: Travel 42 - " + pageTitle);
        } else {
          sb.append("Retrieved from: " + pageTitle);
        }
        sb.append("<br/>");
        sb.append("Url: " + pageUrl);
        sb.append("</a></div>");
      }

      sb.append(html);
      return sb.toString();
    }

    return null;
  }

  //Some urls have
  private static String cleanupUrl(String str){
    int offset = str.lastIndexOf("http://");
    if (offset == -1) {
      offset = str.lastIndexOf("https://");
    }
    if (offset > 0) {
      return str.substring(offset);
    }

    return str;
  }

  /**
   * Looking for term in a list of predefined websites
   * @param req
   * @return
   */
  private CompletionStage<ResponseMessageJson> webSearchResponse(final RequestMessageJson req) {
    final SessionMgr sessionMgr = new SessionMgr(session());
    final ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);
    final WebSearchJson webSearchRequestJson = (WebSearchJson) req.body;

    return  CompletableFuture.supplyAsync(() -> {
      StopWatch sw = new StopWatch();
      sw.start();

      Destination dest = null;
      Trip trip = null;
      if (webSearchRequestJson.id != null && !webSearchRequestJson.id.isEmpty()) {
        dest = Destination.find.byId(webSearchRequestJson.id);
        if (dest != null && !SecurityMgr.canAccessDestination(dest, sessionMgr)) {
          dest = null;
        }
      } else if (webSearchRequestJson.tripId != null && !webSearchRequestJson.tripId.isEmpty()) {
        trip = Trip.findByPK(webSearchRequestJson.tripId);
        if (trip == null || SecurityMgr.getTripAccessLevel(trip, sessionMgr.getCredentials().getAccount()).le(SecurityMgr.AccessLevel.READ)) {
          trip  = null;
        }
      }

      if (dest == null && trip == null) {
        responseMessageJson.header.withCode(ReturnCode.AUTH_DOCUMENT_FAIL);
        return responseMessageJson;
      }

      if (webSearchRequestJson.domains == null || webSearchRequestJson.domains.size() == 0) {
        List<SysContentSite> sites = getContentSites();
        List<String> siteUrls = new ArrayList<>();
        for (SysContentSite s : sites) {
          siteUrls.add(s.getUrl());
        }
        responseMessageJson.body = bingWebSearch(webSearchRequestJson.searchTerm, siteUrls);
      } else {
        responseMessageJson.body = bingWebSearch(webSearchRequestJson.searchTerm, webSearchRequestJson.domains);
      }

      sw.stop();
      Log.debug("WEBSEARCH: Response for ("+ webSearchRequestJson.searchTerm  +") prepared in:" + sw.getTime() + "ms");
      responseMessageJson.header.withCode(ReturnCode.SUCCESS);
      return responseMessageJson;
    }, ec.current());
  }

  public static List<SysContentSite> getContentSites(){
    List<SysContentSite> sites = (List) CacheMgr.get(APPConstants.CACHE_MOBILIZED_DOMAINS);

    if (sites == null) {
      sites = SysContentSite.findAllActive();
      CacheMgr.set(APPConstants.CACHE_MOBILIZED_DOMAINS, sites, APPConstants.CACHE_EXPIRY_SECS);
    }

    return sites;
  }

  private static List<SysContentSite> getContentSitesFromDomains(List<String> domains) {
    List<SysContentSite> sites = getContentSites();
    List<SysContentSite> result = new ArrayList<>();
    for(String domain: domains) {
      for(SysContentSite s : sites) {
        if (s.getDomain().equals(domain)) {
          result.add(s);
        }
      }
    }
    return result;
  }

  /**
   * Searches for term in the list of specified websites
   * @param term
   * @param sites
   * @return
   */
  private static WebSearchJson bingWebSearch(String term, List<String> sites) {
    WebSearchJson wsj = new WebSearchJson();
    wsj.operation = Operation.SEARCH;
    wsj.searchTerm = term;

    StringBuilder sb = new StringBuilder();
    sb.append(term);
    sb.append(" (");

    for(Iterator<String> it = sites.iterator(); it.hasNext();) {
      String currSite = it.next();
      sb.append("site:");
      sb.append(currSite);
      if (it.hasNext()) {
        sb.append(" OR ");
      }
    }
    sb.append(")");

    Log.debug("BING Search Query:" + sb.toString());


    HttpClient client = HttpClientBuilder.create().build();
    String url = "https://api.cognitive.microsoft.com/bing/v5.0/search?q="+sb.toString().replaceAll(" ", "%20").replaceAll(":", "%3A")+"&count=100&offset=0&mkt=en-us&safeSearch=Moderate";

    HttpGet get = new HttpGet(url);
    get.addHeader("accept", "application/json");
    get.setHeader("Content-Type", "multipart/form-data");
    get.setHeader("Ocp-Apim-Subscription-Key", APPConstants.BING_KEY_2);

    HttpResponse r = null;
    try {
      r = client.execute(get);
    } catch (IOException e) {
      e.printStackTrace();
    }

    HttpEntity entity = r.getEntity();
    BingWeb details = null;
    Log.log(LogLevel.INFO, "BING DOC STEP 1");


    if (r != null && r.getStatusLine() != null && r.getStatusLine().getStatusCode() == 200) {
      Log.log(LogLevel.INFO, "BING DOC STEP 2");

      try {
        String resp = EntityUtils.toString(entity);
        ObjectMapper mapper = new ObjectMapper();

        com.fasterxml.jackson.databind.ObjectReader reader = mapper.reader(BingWeb.class);
        details = reader.readValue(resp);

        for (Value v : details.getWebPages().getValue()) {

          // Populate the data from result object in to your custom objects.
          wsj.addPage(Utils.hash(v.getId()), v.getName(), ImageController.getDirectUrl(v.getUrl()) , v.getDisplayUrl(), v.getSnippet());

        }
      }
      catch (Exception e) {
        Log.log(LogLevel.ERROR, "BING SEARCH ERROR - Cannot read JSON from response: " , e);
        e.printStackTrace();
      }

    } else {
      Log.log(LogLevel.INFO, "BING STEP 5");

      Log.log(LogLevel.INFO, "BING SEARCH ERROR: " + r.getStatusLine().getStatusCode() + " " + r.getStatusLine().getReasonPhrase());
    }

    return wsj;
  }

}
