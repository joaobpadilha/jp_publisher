package controllers;

import actors.ActorsHelper;
import actors.FlightPollAlertsActor;
import actors.SupervisorActor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.msg.FlightNotifyMsg;
import com.mapped.publisher.msg.flightstat.event.AlertEvent;
import com.mapped.publisher.msg.flightstat.status.FlightStatusMsg;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.BaseView;
import models.publisher.FlightAlert;
import models.publisher.FlightAlertEvent;
import org.joda.time.DateTime;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.ws.WS;
import play.libs.ws.WSResponse;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * Created by twong on 2014-12-22.
 */
public class FlightStatusController
    extends Controller {

  @Inject
  FormFactory formFactory;

  public Result flightstatsAlert() {
    // String s = "{\"alert\":{\"event\":{\"type\":\"TIME_ADJUSTMENT\"},\"dataSource\":\"Airline (UA)\",
    // \"dateTimeRecorded\":\"2013-02-18T16:58:29.416Z\",\"rule\":{\"id\":\"124513936\",\"name\":\"Default\",
    // \"carrierFsCode\":\"UA\",\"flightNumber\":\"5312\",\"departureAirportFsCode\":\"EUG\",
    // \"arrivalAirportFsCode\":\"PDX\",\"departure\":\"2013-02-18T16:17:00.000\",\"arrival\":\"2013-02-18T16:58:00
    // .000\",\"ruleEvents\":{\"ruleEvent\":{\"type\":\"ALL_CHANGES\"}},\"nameValues\":null,
    // \"delivery\":{\"format\":\"json\",\"destination\":\"https://example.com/your_post_url\"}},
    // \"flightStatus\":{\"flightId\":\"288841476\",\"carrierFsCode\":\"OO\",\"flightNumber\":\"5312\",
    // \"departureAirportFsCode\":\"EUG\",\"arrivalAirportFsCode\":\"PDX\",
    // \"departureDate\":{\"dateLocal\":\"2013-02-18T16:17:00.000\",\"dateUtc\":\"2013-02-19T00:17:00.000Z\"},
    // \"arrivalDate\":{\"dateLocal\":\"2013-02-18T16:58:00.000\",\"dateUtc\":\"2013-02-19T00:58:00.000Z\"},
    // \"status\":\"L\",\"schedule\":{\"flightType\":\"J\",\"serviceClasses\":\"Y\",\"restrictions\":\"\",
    // \"uplines\":{\"upline\":{\"fsCode\":\"PDX\",\"flightId\":\"288876788\"}}},
    // \"operationalTimes\":{\"publishedDeparture\":{\"dateLocal\":\"2013-02-18T16:17:00.000\",
    // \"dateUtc\":\"2013-02-19T00:17:00.000Z\"},\"publishedArrival\":{\"dateLocal\":\"2013-02-18T16:58:00.000\",
    // \"dateUtc\":\"2013-02-19T00:58:00.000Z\"},\"scheduledGateDeparture\":{\"dateLocal\":\"2013-02-18T16:17:00
    // .000\",\"dateUtc\":\"2013-02-19T00:17:00.000Z\"},
    // \"estimatedGateDeparture\":{\"dateLocal\":\"2013-02-18T16:17:00.000\",\"dateUtc\":\"2013-02-19T00:17:00
    // .000Z\"},\"actualGateDeparture\":{\"dateLocal\":\"2013-02-18T16:11:00.000\",
    // \"dateUtc\":\"2013-02-19T00:11:00.000Z\"},\"flightPlanPlannedDeparture\":{\"dateLocal\":\"2013-02-18T16:27:00
    // .000\",\"dateUtc\":\"2013-02-19T00:27:00.000Z\"},
    // \"estimatedRunwayDeparture\":{\"dateLocal\":\"2013-02-18T16:18:00.000\",\"dateUtc\":\"2013-02-19T00:18:00
    // .000Z\"},\"actualRunwayDeparture\":{\"dateLocal\":\"2013-02-18T16:18:00.000\",
    // \"dateUtc\":\"2013-02-19T00:18:00.000Z\"},\"scheduledGateArrival\":{\"dateLocal\":\"2013-02-18T16:58:00.000\",
    // \"dateUtc\":\"2013-02-19T00:58:00.000Z\"},\"estimatedGateArrival\":{\"dateLocal\":\"2013-02-18T16:44:00.000\",
    // \"dateUtc\":\"2013-02-19T00:44:00.000Z\"},\"actualGateArrival\":{\"dateLocal\":\"2013-02-18T16:50:00.000\",
    // \"dateUtc\":\"2013-02-19T00:50:00.000Z\"},\"flightPlanPlannedArrival\":{\"dateLocal\":\"2013-02-18T16:56:00
    // .000\",\"dateUtc\":\"2013-02-19T00:56:00.000Z\"},
    // \"estimatedRunwayArrival\":{\"dateLocal\":\"2013-02-18T16:49:00.000\",\"dateUtc\":\"2013-02-19T00:49:00
    // .000Z\"},\"actualRunwayArrival\":{\"dateLocal\":\"2013-02-18T16:47:00.000\",
    // \"dateUtc\":\"2013-02-19T00:47:00.000Z\"}},\"codeshares\":{\"codeshare\":[{\"fsCode\":\"UA\",
    // \"flightNumber\":\"5312\",\"relationship\":\"X\"},{\"fsCode\":\"AC\",\"flightNumber\":\"4290\",
    // \"relationship\":\"Z\"}]},\"flightDurations\":{\"scheduledBlockMinutes\":\"41\",\"blockMinutes\":\"39\",
    // \"scheduledAirMinutes\":\"29\",\"airMinutes\":\"29\",\"scheduledTaxiOutMinutes\":\"10\",
    // \"taxiOutMinutes\":\"7\",\"scheduledTaxiInMinutes\":\"2\",\"taxiInMinutes\":\"3\"},
    // \"flightEquipment\":{\"scheduledEquipmentIataCode\":\"EM2\",\"actualEquipmentIataCode\":\"E120\"},
    // \"flightStatusUpdates\":{\"flightStatusUpdate\":[{\"updatedAt\":{\"dateUtc\":\"2013-02-17T16:22:06
    // .226-08:00\"},\"source\":\"Airline (UA)\",\"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"EGD\",
    // \"newDateLocal\":\"2013-02-18T16:17:00.000\"},{\"field\":\"EGA\",\"newDateLocal\":\"2013-02-18T16:58:00
    // .000\"}]}},{\"updatedAt\":{\"dateUtc\":\"2013-02-17T16:25:57.963-08:00\"},\"source\":\"ASDI\",
    // \"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"SRD\",\"newDateLocal\":\"2013-02-18T16:27:00.000\"},
    // {\"field\":\"ERD\",\"newDateLocal\":\"2013-02-18T16:27:00.000\"},{\"field\":\"SRA\",
    // \"newDateLocal\":\"2013-02-18T16:56:00.000\"},{\"field\":\"ERA\",\"newDateLocal\":\"2013-02-18T16:56:00
    // .000\"}]}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T15:11:07.237-08:00\"},\"source\":\"ASDI\",
    // \"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"ERD\",\"originalDateLocal\":\"2013-02-18T16:27:00
    // .000\",\"newDateLocal\":\"2013-02-18T16:29:00.000\"},{\"field\":\"ERA\",
    // \"originalDateLocal\":\"2013-02-18T16:56:00.000\",\"newDateLocal\":\"2013-02-18T16:55:00.000\"}]}},
    // {\"updatedAt\":{\"dateUtc\":\"2013-02-18T15:24:25.477-08:00\"},\"source\":\"Airline (UA)\",
    // \"updatedDateFields\":{\"updatedDateField\":{\"field\":\"EGA\",\"originalDateLocal\":\"2013-02-18T16:58:00
    // .000\",\"newDateLocal\":\"2013-02-18T16:55:00.000\"}}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T15:51:26
    // .401-08:00\"},\"source\":\"ASDI\",\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ERA\",
    // \"originalDateLocal\":\"2013-02-18T16:55:00.000\",\"newDateLocal\":\"2013-02-18T16:52:00.000\"}}},
    // {\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:23:24.276-08:00\"},\"source\":\"ASDI\",
    // \"updatedTextFields\":{\"updatedTextField\":{\"field\":\"AQP\",\"newText\":\"E120\"}},
    // \"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"ERD\",\"originalDateLocal\":\"2013-02-18T16:29:00
    // .000\",\"newDateLocal\":\"2013-02-18T16:18:00.000\"},{\"field\":\"ERA\",
    // \"originalDateLocal\":\"2013-02-18T16:52:00.000\",\"newDateLocal\":\"2013-02-18T16:46:00.000\"}]}},
    // {\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:23:35.561-08:00\"},\"source\":\"ASDI\",
    // \"updatedTextFields\":{\"updatedTextField\":{\"field\":\"STS\",\"originalText\":\"S\",\"newText\":\"A\"}},
    // \"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ARD\",\"newDateLocal\":\"2013-02-18T16:18:00
    // .000\"}}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:24:35.270-08:00\"},\"source\":\"Airline (UA)\",
    // \"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"AGD\",\"newDateLocal\":\"2013-02-18T16:11:00.000\"},
    // {\"field\":\"EGA\",\"originalDateLocal\":\"2013-02-18T16:55:00.000\",\"newDateLocal\":\"2013-02-18T16:44:00
    // .000\"}]}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:36:20.338-08:00\"},\"source\":\"ASDI\",
    // \"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ERA\",\"originalDateLocal\":\"2013-02-18T16:46:00
    // .000\",\"newDateLocal\":\"2013-02-18T16:44:00.000\"}}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:47:16
    // .500-08:00\"},\"source\":\"ASDI\",\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ERA\",
    // \"originalDateLocal\":\"2013-02-18T16:44:00.000\",\"newDateLocal\":\"2013-02-18T16:45:00.000\"}}},
    // {\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:54:27.034-08:00\"},\"source\":\"ASDI\",
    // \"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ERA\",\"originalDateLocal\":\"2013-02-18T16:45:00
    // .000\",\"newDateLocal\":\"2013-02-18T16:49:00.000\"}}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:55:09
    // .690-08:00\"},\"source\":\"ASDI\",\"updatedTextFields\":{\"updatedTextField\":{\"field\":\"STS\",
    // \"originalText\":\"A\",\"newText\":\"L\"}},\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ARA\",
    // \"newDateLocal\":\"2013-02-18T16:47:00.000\"}}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:58:29
    // .416-08:00\"},\"source\":\"Airline (UA)\",\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"AGA\",
    // \"newDateLocal\":\"2013-02-18T16:50:00.000\"}}}]}}},\"appendix\":{\"airlines\":{\"airline\":[{\"fs\":\"OO\",
    // \"iata\":\"OO\",\"icao\":\"SKW\",\"name\":\"SkyWest Airlines\",\"active\":\"true\"},{\"fs\":\"AC\",
    // \"iata\":\"AC\",\"icao\":\"ACA\",\"name\":\"Air Canada\",\"phoneNumber\":\"1-800-247-2262\",
    // \"active\":\"true\"},{\"fs\":\"UA\",\"iata\":\"UA\",\"icao\":\"UAL\",\"name\":\"United Airlines\",
    // \"phoneNumber\":\"1-800-864-8331\",\"active\":\"true\"}]},\"airports\":{\"airport\":[{\"fs\":\"EUG\",
    // \"iata\":\"EUG\",\"icao\":\"KEUG\",\"faa\":\"EUG\",\"name\":\"Eugene Airport\",
    // \"street1\":\"28855 Lockheed Drive\",\"street2\":\"\",\"city\":\"Eugene\",\"cityCode\":\"EUG\",
    // \"stateCode\":\"OR\",\"postalCode\":\"97402\",\"countryCode\":\"US\",\"countryName\":\"United States\",
    // \"regionName\":\"North America\",\"timeZoneRegionName\":\"America/Los_Angeles\",\"weatherZone\":\"ORZ008\",
    // \"localTime\":\"2013-02-18T17:06:42.698\",\"utcOffsetHours\":\"-8.0\",\"latitude\":\"44.119196\",
    // \"longitude\":\"-123.211968\",\"elevationFeet\":\"369\",\"classification\":\"3\",\"active\":\"true\"},
    // {\"fs\":\"PDX\",\"iata\":\"PDX\",\"icao\":\"KPDX\",\"faa\":\"PDX\",\"name\":\"Portland International
    // Airport\",\"street1\":\"7000 NE Airport Way\",\"city\":\"Portland\",\"cityCode\":\"PDX\",\"stateCode\":\"OR\",
    // \"postalCode\":\"97218\",\"countryCode\":\"US\",\"countryName\":\"United States\",
    // \"regionName\":\"North America\",\"timeZoneRegionName\":\"America/Los_Angeles\",\"weatherZone\":\"ORZ006\",
    // \"localTime\":\"2013-02-18T17:06:42.698\",\"utcOffsetHours\":\"-8.0\",\"latitude\":\"45.588995\",
    // \"longitude\":\"-122.592901\",\"elevationFeet\":\"30\",\"classification\":\"1\",\"active\":\"true\"}]}}}" +
    //            "";

    String s = "\"alert\":{\"event\":{\"type\":\"TIME_ADJUSTMENT\"},\"dataSource\":\"Airline (UA)\"," +
               "\"dateTimeRecorded\":\"2013-02-18T16:58:29.416Z\",\"rule\":{\"id\":\"125524541\"," +
               "\"name\":\"Default\",\"carrierFsCode\":\"UA\",\"flightNumber\":\"5312\"," +
               "\"departureAirportFsCode\":\"EUG\",\"arrivalAirportFsCode\":\"PDX\"," +
               "\"departure\":\"2013-02-18T16:17:00.000\",\"arrival\":\"2013-02-18T16:58:00.000\"," +
               "\"ruleEvents\":{\"ruleEvent\":{\"type\":\"ALL_CHANGES\"}},\"nameValues\":null," +
               "\"delivery\":{\"format\":\"json\",\"destination\":\"https://example.com/your_post_url\"}}," +
               "\"flightStatus\":{\"flightId\":\"288841476\",\"carrierFsCode\":\"OO\",\"flightNumber\":\"5312\"," +
               "\"departureAirportFsCode\":\"EUG\",\"arrivalAirportFsCode\":\"PDX\"," +
               "\"departureDate\":{\"dateLocal\":\"2013-02-18T16:17:00.000\",\"dateUtc\":\"2013-02-19T00:17:00" +
               ".000Z\"},\"arrivalDate\":{\"dateLocal\":\"2013-02-18T16:58:00.000\"," +
               "\"dateUtc\":\"2013-02-19T00:58:00.000Z\"},\"status\":\"L\",\"schedule\":{\"flightType\":\"J\"," +
               "\"serviceClasses\":\"Y\",\"restrictions\":\"\",\"uplines\":{\"upline\":{\"fsCode\":\"PDX\"," +
               "\"flightId\":\"288876788\"}}}," +
               "\"operationalTimes\":{\"publishedDeparture\":{\"dateLocal\":\"2013-02-18T16:17:00.000\"," +
               "\"dateUtc\":\"2013-02-19T00:17:00.000Z\"},\"publishedArrival\":{\"dateLocal\":\"2013-02-18T16:58:00" +
               ".000\",\"dateUtc\":\"2013-02-19T00:58:00.000Z\"}," +
               "\"scheduledGateDeparture\":{\"dateLocal\":\"2013-02-18T16:17:00.000\"," +
               "\"dateUtc\":\"2013-02-19T00:17:00.000Z\"}," +
               "\"estimatedGateDeparture\":{\"dateLocal\":\"2013-02-18T16:17:00.000\"," +
               "\"dateUtc\":\"2013-02-19T00:17:00.000Z\"}," +
               "\"actualGateDeparture\":{\"dateLocal\":\"2013-02-18T16:11:00.000\"," +
               "\"dateUtc\":\"2013-02-19T00:11:00.000Z\"}," +
               "\"flightPlanPlannedDeparture\":{\"dateLocal\":\"2013-02-18T16:27:00.000\"," +
               "\"dateUtc\":\"2013-02-19T00:27:00.000Z\"}," +
               "\"estimatedRunwayDeparture\":{\"dateLocal\":\"2013-02-18T16:18:00.000\"," +
               "\"dateUtc\":\"2013-02-19T00:18:00.000Z\"}," +
               "\"actualRunwayDeparture\":{\"dateLocal\":\"2013-02-18T16:18:00.000\"," +
               "\"dateUtc\":\"2013-02-19T00:18:00.000Z\"}," +
               "\"scheduledGateArrival\":{\"dateLocal\":\"2013-02-18T16:58:00.000\"," +
               "\"dateUtc\":\"2013-02-19T00:58:00.000Z\"}," +
               "\"estimatedGateArrival\":{\"dateLocal\":\"2013-02-18T16:44:00.000\"," +
               "\"dateUtc\":\"2013-02-19T00:44:00.000Z\"},\"actualGateArrival\":{\"dateLocal\":\"2013-02-18T16:50:00" +
               ".000\",\"dateUtc\":\"2013-02-19T00:50:00.000Z\"}," +
               "\"flightPlanPlannedArrival\":{\"dateLocal\":\"2013-02-18T16:56:00.000\"," +
               "\"dateUtc\":\"2013-02-19T00:56:00.000Z\"}," +
               "\"estimatedRunwayArrival\":{\"dateLocal\":\"2013-02-18T16:49:00.000\"," +
               "\"dateUtc\":\"2013-02-19T00:49:00.000Z\"}," +
               "\"actualRunwayArrival\":{\"dateLocal\":\"2013-02-18T16:47:00.000\"," +
               "\"dateUtc\":\"2013-02-19T00:47:00.000Z\"}},\"codeshares\":{\"codeshare\":[{\"fsCode\":\"UA\"," +
               "\"flightNumber\":\"5312\",\"relationship\":\"X\"},{\"fsCode\":\"AC\",\"flightNumber\":\"4290\"," +
               "\"relationship\":\"Z\"}]},\"flightDurations\":{\"scheduledBlockMinutes\":\"41\"," +
               "\"blockMinutes\":\"39\",\"scheduledAirMinutes\":\"29\",\"airMinutes\":\"29\"," +
               "\"scheduledTaxiOutMinutes\":\"10\",\"taxiOutMinutes\":\"7\",\"scheduledTaxiInMinutes\":\"2\"," +
               "\"taxiInMinutes\":\"3\"},\"flightEquipment\":{\"scheduledEquipmentIataCode\":\"EM2\"," +
               "\"actualEquipmentIataCode\":\"E120\"}," +
               "\"flightStatusUpdates\":{\"flightStatusUpdate\":[{\"updatedAt\":{\"dateUtc\":\"2013-02-17T16:22:06" +
               ".226-08:00\"},\"source\":\"Airline (UA)\"," +
               "\"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"EGD\"," +
               "\"newDateLocal\":\"2013-02-18T16:17:00.000\"},{\"field\":\"EGA\"," +
               "\"newDateLocal\":\"2013-02-18T16:58:00.000\"}]}},{\"updatedAt\":{\"dateUtc\":\"2013-02-17T16:25:57" +
               ".963-08:00\"},\"source\":\"ASDI\",\"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"SRD\"," +
               "\"newDateLocal\":\"2013-02-18T16:27:00.000\"},{\"field\":\"ERD\"," +
               "\"newDateLocal\":\"2013-02-18T16:27:00.000\"},{\"field\":\"SRA\"," +
               "\"newDateLocal\":\"2013-02-18T16:56:00.000\"},{\"field\":\"ERA\"," +
               "\"newDateLocal\":\"2013-02-18T16:56:00.000\"}]}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T15:11:07" +
               ".237-08:00\"},\"source\":\"ASDI\",\"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"ERD\"," +
               "\"originalDateLocal\":\"2013-02-18T16:27:00.000\",\"newDateLocal\":\"2013-02-18T16:29:00.000\"}," +
               "{\"field\":\"ERA\",\"originalDateLocal\":\"2013-02-18T16:56:00.000\"," +
               "\"newDateLocal\":\"2013-02-18T16:55:00.000\"}]}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T15:24:25" +
               ".477-08:00\"},\"source\":\"Airline (UA)\"," +
               "\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"EGA\"," +
               "\"originalDateLocal\":\"2013-02-18T16:58:00.000\",\"newDateLocal\":\"2013-02-18T16:55:00.000\"}}}," +
               "{\"updatedAt\":{\"dateUtc\":\"2013-02-18T15:51:26.401-08:00\"},\"source\":\"ASDI\"," +
               "\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ERA\"," +
               "\"originalDateLocal\":\"2013-02-18T16:55:00.000\",\"newDateLocal\":\"2013-02-18T16:52:00.000\"}}}," +
               "{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:23:24.276-08:00\"},\"source\":\"ASDI\"," +
               "\"updatedTextFields\":{\"updatedTextField\":{\"field\":\"AQP\",\"newText\":\"E120\"}}," +
               "\"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"ERD\"," +
               "\"originalDateLocal\":\"2013-02-18T16:29:00.000\",\"newDateLocal\":\"2013-02-18T16:18:00.000\"}," +
               "{\"field\":\"ERA\",\"originalDateLocal\":\"2013-02-18T16:52:00.000\"," +
               "\"newDateLocal\":\"2013-02-18T16:46:00.000\"}]}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:23:35" +
               ".561-08:00\"},\"source\":\"ASDI\",\"updatedTextFields\":{\"updatedTextField\":{\"field\":\"STS\"," +
               "\"originalText\":\"S\",\"newText\":\"A\"}}," +
               "\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ARD\",\"newDateLocal\":\"2013-02-18T16:18:00" +
               ".000\"}}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:24:35.270-08:00\"},\"source\":\"Airline (UA)\"," +
               "\"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"AGD\"," +
               "\"newDateLocal\":\"2013-02-18T16:11:00.000\"},{\"field\":\"EGA\"," +
               "\"originalDateLocal\":\"2013-02-18T16:55:00.000\",\"newDateLocal\":\"2013-02-18T16:44:00.000\"}]}}," +
               "{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:36:20.338-08:00\"},\"source\":\"ASDI\"," +
               "\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ERA\"," +
               "\"originalDateLocal\":\"2013-02-18T16:46:00.000\",\"newDateLocal\":\"2013-02-18T16:44:00.000\"}}}," +
               "{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:47:16.500-08:00\"},\"source\":\"ASDI\"," +
               "\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ERA\"," +
               "\"originalDateLocal\":\"2013-02-18T16:44:00.000\",\"newDateLocal\":\"2013-02-18T16:45:00.000\"}}}," +
               "{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:54:27.034-08:00\"},\"source\":\"ASDI\"," +
               "\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ERA\"," +
               "\"originalDateLocal\":\"2013-02-18T16:45:00.000\",\"newDateLocal\":\"2013-02-18T16:49:00.000\"}}}," +
               "{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:55:09.690-08:00\"},\"source\":\"ASDI\"," +
               "\"updatedTextFields\":{\"updatedTextField\":{\"field\":\"STS\",\"originalText\":\"A\"," +
               "\"newText\":\"L\"}},\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ARA\"," +
               "\"newDateLocal\":\"2013-02-18T16:47:00.000\"}}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:58:29" +
               ".416-08:00\"},\"source\":\"Airline (UA)\"," +
               "\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"AGA\",\"newDateLocal\":\"2013-02-18T16:50:00" +
               ".000\"}}}]}}},\"appendix\":{\"airlines\":{\"airline\":[{\"fs\":\"OO\",\"iata\":\"OO\"," +
               "\"icao\":\"SKW\",\"name\":\"SkyWest Airlines\",\"active\":\"true\"},{\"fs\":\"AC\",\"iata\":\"AC\"," +
               "\"icao\":\"ACA\",\"name\":\"Air Canada\",\"phoneNumber\":\"1-800-247-2262\",\"active\":\"true\"}," +
               "{\"fs\":\"UA\",\"iata\":\"UA\",\"icao\":\"UAL\",\"name\":\"United Airlines\"," +
               "\"phoneNumber\":\"1-800-864-8331\",\"active\":\"true\"}]},\"airports\":{\"airport\":[{\"fs\":\"EUG\"," +
               "\"iata\":\"EUG\",\"icao\":\"KEUG\",\"faa\":\"EUG\",\"name\":\"Eugene Airport\"," +
               "\"street1\":\"28855 Lockheed Drive\",\"street2\":\"\",\"city\":\"Eugene\",\"cityCode\":\"EUG\"," +
               "\"stateCode\":\"OR\",\"postalCode\":\"97402\",\"countryCode\":\"US\"," +
               "\"countryName\":\"United States\",\"regionName\":\"North America\"," +
               "\"timeZoneRegionName\":\"America/Los_Angeles\",\"weatherZone\":\"ORZ008\"," +
               "\"localTime\":\"2013-02-18T17:06:42.698\",\"utcOffsetHours\":\"-8.0\",\"latitude\":\"44.119196\"," +
               "\"longitude\":\"-123.211968\",\"elevationFeet\":\"369\",\"classification\":\"3\"," +
               "\"active\":\"true\"},{\"fs\":\"PDX\",\"iata\":\"PDX\",\"icao\":\"KPDX\",\"faa\":\"PDX\"," +
               "\"name\":\"Portland International Airport\",\"street1\":\"7000 NE Airport Way\"," +
               "\"city\":\"Portland\",\"cityCode\":\"PDX\",\"stateCode\":\"OR\",\"postalCode\":\"97218\"," +
               "\"countryCode\":\"US\",\"countryName\":\"United States\",\"regionName\":\"North America\"," +
               "\"timeZoneRegionName\":\"America/Los_Angeles\",\"weatherZone\":\"ORZ006\"," +
               "\"localTime\":\"2013-02-18T17:06:42.698\",\"utcOffsetHours\":\"-8.0\",\"latitude\":\"45.588995\"," +
               "\"longitude\":\"-122.592901\",\"elevationFeet\":\"30\",\"classification\":\"1\"," +
               "\"active\":\"true\"}]}}}" + "";
    JsonNode node = request().body().asJson();
    try {
      processAlert(node);

    }
    catch (Exception e) {
      Log.err("flightstatsAlert -  Error cannot process FlightStat Alert: " + node.toString(), e);
      e.printStackTrace();
    }
    return ok("");

  }

  public static void processAlert(JsonNode node)
      throws Exception {

    ObjectMapper mapper = new ObjectMapper();
    com.fasterxml.jackson.databind.ObjectReader r = mapper.readerFor(AlertEvent.class);
    AlertEvent jsonResp = r.readValue(node);


    if (jsonResp.getAlert() != null &&
        jsonResp.getAlert().getRule() != null &&
        jsonResp.getAlert().getRule().getId() != null) {
      //find alert
      FlightAlert alert = FlightAlert.findByRuleId(jsonResp.getAlert().getRule().getId());
      if (alert != null && alert.status == APPConstants.STATUS_ACTIVE) {
        //update alert

        DateTime arrivalDateTime = Utils.getDateTimeFromISO(jsonResp.getAlert()
                                                                    .getFlightStatus()
                                                                    .getArrivalDate()
                                                                    .getDateLocal());

        if (jsonResp.getAlert().getFlightStatus().getOperationalTimes() != null &&
            jsonResp.getAlert().getFlightStatus().getOperationalTimes().getEstimatedGateArrival() != null &&
            jsonResp.getAlert()
                    .getFlightStatus()
                    .getOperationalTimes()
                    .getEstimatedGateArrival()
                    .getDateLocal() != null) {
          arrivalDateTime = Utils.getDateTimeFromISO(jsonResp.getAlert()
                                                             .getFlightStatus()
                                                             .getOperationalTimes()
                                                             .getEstimatedGateArrival()
                                                             .getDateLocal());
        }
        else if (jsonResp.getAlert().getFlightStatus().getOperationalTimes() != null &&
                 jsonResp.getAlert().getFlightStatus().getOperationalTimes().getScheduledGateArrival() != null &&
                 jsonResp.getAlert()
                         .getFlightStatus()
                         .getOperationalTimes()
                         .getScheduledGateArrival()
                         .getDateLocal() != null) {
          arrivalDateTime = Utils.getDateTimeFromISO(jsonResp.getAlert()
                                                             .getFlightStatus()
                                                             .getOperationalTimes()
                                                             .getScheduledGateArrival()
                                                             .getDateLocal());
        }
        alert.setNewArriveTime(arrivalDateTime.getMillis());

        DateTime departDateTime = Utils.getDateTimeFromISO(jsonResp.getAlert()
                                                                   .getFlightStatus()
                                                                   .getDepartureDate()
                                                                   .getDateLocal());
        if (jsonResp.getAlert().getFlightStatus().getOperationalTimes() != null &&
            jsonResp.getAlert().getFlightStatus().getOperationalTimes().getEstimatedGateDeparture() != null &&
            jsonResp.getAlert()
                    .getFlightStatus()
                    .getOperationalTimes()
                    .getEstimatedGateDeparture()
                    .getDateLocal() != null) {
          departDateTime = Utils.getDateTimeFromISO(jsonResp.getAlert()
                                                            .getFlightStatus()
                                                            .getOperationalTimes()
                                                            .getEstimatedGateDeparture()
                                                            .getDateLocal());
        }
        else if (jsonResp.getAlert().getFlightStatus().getOperationalTimes() != null &&
                 jsonResp.getAlert().getFlightStatus().getOperationalTimes().getScheduledGateDeparture() != null &&
                 jsonResp.getAlert()
                         .getFlightStatus()
                         .getOperationalTimes()
                         .getScheduledGateDeparture()
                         .getDateLocal() != null) {
          departDateTime = Utils.getDateTimeFromISO(jsonResp.getAlert()
                                                            .getFlightStatus()
                                                            .getOperationalTimes()
                                                            .getScheduledGateDeparture()
                                                            .getDateLocal());
        }

        DateTime alertTimestamp = Utils.getDateTimeFromISO(jsonResp.getAlert().getDateTimeRecorded());

        alert.setNewDepartTime(departDateTime.getMillis());

        if (jsonResp.getAlert().getDateTimeRecorded() != null) {
          DateTime alertTime = Utils.getDateTimeFromISO(jsonResp.getAlert().getDateTimeRecorded());
          alert.setLastEventTimestamp(alertTime.getMillis());
        }
        alert.setLastupdatedtimestamp(System.currentTimeMillis());

        if (jsonResp.getAlert().getFlightStatus().getAirportResources() != null) {
          alert.setNewArriveGate(jsonResp.getAlert().getFlightStatus().getAirportResources().getArrivalGate());
          alert.setNewArriveTerminal(jsonResp.getAlert().getFlightStatus().getAirportResources().getArrivalTerminal());
          alert.setNewDepartGate(jsonResp.getAlert().getFlightStatus().getAirportResources().getDepartureGate());
          alert.setNewDepartTerminal(jsonResp.getAlert()
                                             .getFlightStatus()
                                             .getAirportResources()
                                             .getDepartureTerminal());
        }
        alert.save();

        //create notification
        FlightAlertEvent event = new FlightAlertEvent();
        event.setEventId(DBConnectionMgr.getUniqueLongId());
        event.setFlightAlert(alert);
        event.setCreatedby("FLIGHT_STATS");
        event.setCreatedtimestamp(System.currentTimeMillis());
        event.setStatus(APPConstants.STATUS_PENDING);

        if (alertTimestamp != null) {
          event.setAlertTimestamp(alertTimestamp.getMillis());
        }
        else {
          event.setAlertTimestamp(System.currentTimeMillis());
        }

        event.setAlertType(FlightAlertEvent.FlightAlertType.valueOf(jsonResp.getAlert().getEvent().getType()));
        event.setResponse(node.toString());
        event.setDepartTimestamp(departDateTime.getMillis());
        event.setArriveTimestamp(arrivalDateTime.getMillis());

        event.save();

        //send to Actor
        final FlightNotifyMsg msg = new FlightNotifyMsg();
        msg.flightAlertId = alert.getFlightAlertId();

        ActorsHelper.tell(SupervisorActor.UmappedActor.FLIGHT_NOTIFY, msg);
      }
    }
  }

  public Result flightstatsTestAlert() {
    // String s = "{\"alert\":{\"event\":{\"type\":\"TIME_ADJUSTMENT\"},\"dataSource\":\"Airline (UA)\",
    // \"dateTimeRecorded\":\"2013-02-18T16:58:29.416Z\",\"rule\":{\"id\":\"124513936\",\"name\":\"Default\",
    // \"carrierFsCode\":\"UA\",\"flightNumber\":\"5312\",\"departureAirportFsCode\":\"EUG\",
    // \"arrivalAirportFsCode\":\"PDX\",\"departure\":\"2013-02-18T16:17:00.000\",\"arrival\":\"2013-02-18T16:58:00
    // .000\",\"ruleEvents\":{\"ruleEvent\":{\"type\":\"ALL_CHANGES\"}},\"nameValues\":null,
    // \"delivery\":{\"format\":\"json\",\"destination\":\"https://example.com/your_post_url\"}},
    // \"flightStatus\":{\"flightId\":\"288841476\",\"carrierFsCode\":\"OO\",\"flightNumber\":\"5312\",
    // \"departureAirportFsCode\":\"EUG\",\"arrivalAirportFsCode\":\"PDX\",
    // \"departureDate\":{\"dateLocal\":\"2013-02-18T16:17:00.000\",\"dateUtc\":\"2013-02-19T00:17:00.000Z\"},
    // \"arrivalDate\":{\"dateLocal\":\"2013-02-18T16:58:00.000\",\"dateUtc\":\"2013-02-19T00:58:00.000Z\"},
    // \"status\":\"L\",\"schedule\":{\"flightType\":\"J\",\"serviceClasses\":\"Y\",\"restrictions\":\"\",
    // \"uplines\":{\"upline\":{\"fsCode\":\"PDX\",\"flightId\":\"288876788\"}}},
    // \"operationalTimes\":{\"publishedDeparture\":{\"dateLocal\":\"2013-02-18T16:17:00.000\",
    // \"dateUtc\":\"2013-02-19T00:17:00.000Z\"},\"publishedArrival\":{\"dateLocal\":\"2013-02-18T16:58:00.000\",
    // \"dateUtc\":\"2013-02-19T00:58:00.000Z\"},\"scheduledGateDeparture\":{\"dateLocal\":\"2013-02-18T16:17:00
    // .000\",\"dateUtc\":\"2013-02-19T00:17:00.000Z\"},
    // \"estimatedGateDeparture\":{\"dateLocal\":\"2013-02-18T16:17:00.000\",\"dateUtc\":\"2013-02-19T00:17:00
    // .000Z\"},\"actualGateDeparture\":{\"dateLocal\":\"2013-02-18T16:11:00.000\",
    // \"dateUtc\":\"2013-02-19T00:11:00.000Z\"},\"flightPlanPlannedDeparture\":{\"dateLocal\":\"2013-02-18T16:27:00
    // .000\",\"dateUtc\":\"2013-02-19T00:27:00.000Z\"},
    // \"estimatedRunwayDeparture\":{\"dateLocal\":\"2013-02-18T16:18:00.000\",\"dateUtc\":\"2013-02-19T00:18:00
    // .000Z\"},\"actualRunwayDeparture\":{\"dateLocal\":\"2013-02-18T16:18:00.000\",
    // \"dateUtc\":\"2013-02-19T00:18:00.000Z\"},\"scheduledGateArrival\":{\"dateLocal\":\"2013-02-18T16:58:00.000\",
    // \"dateUtc\":\"2013-02-19T00:58:00.000Z\"},\"estimatedGateArrival\":{\"dateLocal\":\"2013-02-18T16:44:00.000\",
    // \"dateUtc\":\"2013-02-19T00:44:00.000Z\"},\"actualGateArrival\":{\"dateLocal\":\"2013-02-18T16:50:00.000\",
    // \"dateUtc\":\"2013-02-19T00:50:00.000Z\"},\"flightPlanPlannedArrival\":{\"dateLocal\":\"2013-02-18T16:56:00
    // .000\",\"dateUtc\":\"2013-02-19T00:56:00.000Z\"},
    // \"estimatedRunwayArrival\":{\"dateLocal\":\"2013-02-18T16:49:00.000\",\"dateUtc\":\"2013-02-19T00:49:00
    // .000Z\"},\"actualRunwayArrival\":{\"dateLocal\":\"2013-02-18T16:47:00.000\",
    // \"dateUtc\":\"2013-02-19T00:47:00.000Z\"}},\"codeshares\":{\"codeshare\":[{\"fsCode\":\"UA\",
    // \"flightNumber\":\"5312\",\"relationship\":\"X\"},{\"fsCode\":\"AC\",\"flightNumber\":\"4290\",
    // \"relationship\":\"Z\"}]},\"flightDurations\":{\"scheduledBlockMinutes\":\"41\",\"blockMinutes\":\"39\",
    // \"scheduledAirMinutes\":\"29\",\"airMinutes\":\"29\",\"scheduledTaxiOutMinutes\":\"10\",
    // \"taxiOutMinutes\":\"7\",\"scheduledTaxiInMinutes\":\"2\",\"taxiInMinutes\":\"3\"},
    // \"flightEquipment\":{\"scheduledEquipmentIataCode\":\"EM2\",\"actualEquipmentIataCode\":\"E120\"},
    // \"flightStatusUpdates\":{\"flightStatusUpdate\":[{\"updatedAt\":{\"dateUtc\":\"2013-02-17T16:22:06
    // .226-08:00\"},\"source\":\"Airline (UA)\",\"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"EGD\",
    // \"newDateLocal\":\"2013-02-18T16:17:00.000\"},{\"field\":\"EGA\",\"newDateLocal\":\"2013-02-18T16:58:00
    // .000\"}]}},{\"updatedAt\":{\"dateUtc\":\"2013-02-17T16:25:57.963-08:00\"},\"source\":\"ASDI\",
    // \"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"SRD\",\"newDateLocal\":\"2013-02-18T16:27:00.000\"},
    // {\"field\":\"ERD\",\"newDateLocal\":\"2013-02-18T16:27:00.000\"},{\"field\":\"SRA\",
    // \"newDateLocal\":\"2013-02-18T16:56:00.000\"},{\"field\":\"ERA\",\"newDateLocal\":\"2013-02-18T16:56:00
    // .000\"}]}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T15:11:07.237-08:00\"},\"source\":\"ASDI\",
    // \"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"ERD\",\"originalDateLocal\":\"2013-02-18T16:27:00
    // .000\",\"newDateLocal\":\"2013-02-18T16:29:00.000\"},{\"field\":\"ERA\",
    // \"originalDateLocal\":\"2013-02-18T16:56:00.000\",\"newDateLocal\":\"2013-02-18T16:55:00.000\"}]}},
    // {\"updatedAt\":{\"dateUtc\":\"2013-02-18T15:24:25.477-08:00\"},\"source\":\"Airline (UA)\",
    // \"updatedDateFields\":{\"updatedDateField\":{\"field\":\"EGA\",\"originalDateLocal\":\"2013-02-18T16:58:00
    // .000\",\"newDateLocal\":\"2013-02-18T16:55:00.000\"}}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T15:51:26
    // .401-08:00\"},\"source\":\"ASDI\",\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ERA\",
    // \"originalDateLocal\":\"2013-02-18T16:55:00.000\",\"newDateLocal\":\"2013-02-18T16:52:00.000\"}}},
    // {\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:23:24.276-08:00\"},\"source\":\"ASDI\",
    // \"updatedTextFields\":{\"updatedTextField\":{\"field\":\"AQP\",\"newText\":\"E120\"}},
    // \"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"ERD\",\"originalDateLocal\":\"2013-02-18T16:29:00
    // .000\",\"newDateLocal\":\"2013-02-18T16:18:00.000\"},{\"field\":\"ERA\",
    // \"originalDateLocal\":\"2013-02-18T16:52:00.000\",\"newDateLocal\":\"2013-02-18T16:46:00.000\"}]}},
    // {\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:23:35.561-08:00\"},\"source\":\"ASDI\",
    // \"updatedTextFields\":{\"updatedTextField\":{\"field\":\"STS\",\"originalText\":\"S\",\"newText\":\"A\"}},
    // \"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ARD\",\"newDateLocal\":\"2013-02-18T16:18:00
    // .000\"}}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:24:35.270-08:00\"},\"source\":\"Airline (UA)\",
    // \"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"AGD\",\"newDateLocal\":\"2013-02-18T16:11:00.000\"},
    // {\"field\":\"EGA\",\"originalDateLocal\":\"2013-02-18T16:55:00.000\",\"newDateLocal\":\"2013-02-18T16:44:00
    // .000\"}]}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:36:20.338-08:00\"},\"source\":\"ASDI\",
    // \"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ERA\",\"originalDateLocal\":\"2013-02-18T16:46:00
    // .000\",\"newDateLocal\":\"2013-02-18T16:44:00.000\"}}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:47:16
    // .500-08:00\"},\"source\":\"ASDI\",\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ERA\",
    // \"originalDateLocal\":\"2013-02-18T16:44:00.000\",\"newDateLocal\":\"2013-02-18T16:45:00.000\"}}},
    // {\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:54:27.034-08:00\"},\"source\":\"ASDI\",
    // \"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ERA\",\"originalDateLocal\":\"2013-02-18T16:45:00
    // .000\",\"newDateLocal\":\"2013-02-18T16:49:00.000\"}}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:55:09
    // .690-08:00\"},\"source\":\"ASDI\",\"updatedTextFields\":{\"updatedTextField\":{\"field\":\"STS\",
    // \"originalText\":\"A\",\"newText\":\"L\"}},\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ARA\",
    // \"newDateLocal\":\"2013-02-18T16:47:00.000\"}}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:58:29
    // .416-08:00\"},\"source\":\"Airline (UA)\",\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"AGA\",
    // \"newDateLocal\":\"2013-02-18T16:50:00.000\"}}}]}}},\"appendix\":{\"airlines\":{\"airline\":[{\"fs\":\"OO\",
    // \"iata\":\"OO\",\"icao\":\"SKW\",\"name\":\"SkyWest Airlines\",\"active\":\"true\"},{\"fs\":\"AC\",
    // \"iata\":\"AC\",\"icao\":\"ACA\",\"name\":\"Air Canada\",\"phoneNumber\":\"1-800-247-2262\",
    // \"active\":\"true\"},{\"fs\":\"UA\",\"iata\":\"UA\",\"icao\":\"UAL\",\"name\":\"United Airlines\",
    // \"phoneNumber\":\"1-800-864-8331\",\"active\":\"true\"}]},\"airports\":{\"airport\":[{\"fs\":\"EUG\",
    // \"iata\":\"EUG\",\"icao\":\"KEUG\",\"faa\":\"EUG\",\"name\":\"Eugene Airport\",
    // \"street1\":\"28855 Lockheed Drive\",\"street2\":\"\",\"city\":\"Eugene\",\"cityCode\":\"EUG\",
    // \"stateCode\":\"OR\",\"postalCode\":\"97402\",\"countryCode\":\"US\",\"countryName\":\"United States\",
    // \"regionName\":\"North America\",\"timeZoneRegionName\":\"America/Los_Angeles\",\"weatherZone\":\"ORZ008\",
    // \"localTime\":\"2013-02-18T17:06:42.698\",\"utcOffsetHours\":\"-8.0\",\"latitude\":\"44.119196\",
    // \"longitude\":\"-123.211968\",\"elevationFeet\":\"369\",\"classification\":\"3\",\"active\":\"true\"},
    // {\"fs\":\"PDX\",\"iata\":\"PDX\",\"icao\":\"KPDX\",\"faa\":\"PDX\",\"name\":\"Portland International
    // Airport\",\"street1\":\"7000 NE Airport Way\",\"city\":\"Portland\",\"cityCode\":\"PDX\",\"stateCode\":\"OR\",
    // \"postalCode\":\"97218\",\"countryCode\":\"US\",\"countryName\":\"United States\",
    // \"regionName\":\"North America\",\"timeZoneRegionName\":\"America/Los_Angeles\",\"weatherZone\":\"ORZ006\",
    // \"localTime\":\"2013-02-18T17:06:42.698\",\"utcOffsetHours\":\"-8.0\",\"latitude\":\"45.588995\",
    // \"longitude\":\"-122.592901\",\"elevationFeet\":\"30\",\"classification\":\"1\",\"active\":\"true\"}]}}}" +
    //            "";

    //String s = "{\"alert\":{\"event\":{\"type\":\"TIME_ADJUSTMENT\"},\"dataSource\":\"Airline (UA)\",
    // \"dateTimeRecorded\":\"2013-02-18T16:58:29.416Z\",\"rule\":{\"id\":\"125524541\",\"name\":\"Default\",
    // \"carrierFsCode\":\"UA\",\"flightNumber\":\"5312\",\"departureAirportFsCode\":\"EUG\",
    // \"arrivalAirportFsCode\":\"PDX\",\"departure\":\"2013-02-18T16:17:00.000\",\"arrival\":\"2013-02-18T16:58:00
    // .000\",\"ruleEvents\":{\"ruleEvent\":{\"type\":\"ALL_CHANGES\"}},\"nameValues\":null,
    // \"delivery\":{\"format\":\"json\",\"destination\":\"https://example.com/your_post_url\"}},
    // \"flightStatus\":{\"flightId\":\"288841476\",\"carrierFsCode\":\"OO\",\"flightNumber\":\"5312\",
    // \"departureAirportFsCode\":\"EUG\",\"arrivalAirportFsCode\":\"PDX\",
    // \"departureDate\":{\"dateLocal\":\"2013-02-18T16:17:00.000\",\"dateUtc\":\"2013-02-19T00:17:00.000Z\"},
    // \"arrivalDate\":{\"dateLocal\":\"2013-02-18T16:58:00.000\",\"dateUtc\":\"2013-02-19T00:58:00.000Z\"},
    // \"status\":\"L\",\"schedule\":{\"flightType\":\"J\",\"serviceClasses\":\"Y\",\"restrictions\":\"\",
    // \"uplines\":{\"upline\":{\"fsCode\":\"PDX\",\"flightId\":\"288876788\"}}},
    // \"operationalTimes\":{\"publishedDeparture\":{\"dateLocal\":\"2013-02-18T16:17:00.000\",
    // \"dateUtc\":\"2013-02-19T00:17:00.000Z\"},\"publishedArrival\":{\"dateLocal\":\"2013-02-18T16:58:00.000\",
    // \"dateUtc\":\"2013-02-19T00:58:00.000Z\"},\"scheduledGateDeparture\":{\"dateLocal\":\"2013-02-18T16:17:00
    // .000\",\"dateUtc\":\"2013-02-19T00:17:00.000Z\"},
    // \"estimatedGateDeparture\":{\"dateLocal\":\"2013-02-18T16:17:00.000\",\"dateUtc\":\"2013-02-19T00:17:00
    // .000Z\"},\"actualGateDeparture\":{\"dateLocal\":\"2013-02-18T16:11:00.000\",
    // \"dateUtc\":\"2013-02-19T00:11:00.000Z\"},\"flightPlanPlannedDeparture\":{\"dateLocal\":\"2013-02-18T16:27:00
    // .000\",\"dateUtc\":\"2013-02-19T00:27:00.000Z\"},
    // \"estimatedRunwayDeparture\":{\"dateLocal\":\"2013-02-18T16:18:00.000\",\"dateUtc\":\"2013-02-19T00:18:00
    // .000Z\"},\"actualRunwayDeparture\":{\"dateLocal\":\"2013-02-18T16:18:00.000\",
    // \"dateUtc\":\"2013-02-19T00:18:00.000Z\"},\"scheduledGateArrival\":{\"dateLocal\":\"2013-02-18T16:58:00.000\",
    // \"dateUtc\":\"2013-02-19T00:58:00.000Z\"},\"estimatedGateArrival\":{\"dateLocal\":\"2013-02-18T16:44:00.000\",
    // \"dateUtc\":\"2013-02-19T00:44:00.000Z\"},\"actualGateArrival\":{\"dateLocal\":\"2013-02-18T16:50:00.000\",
    // \"dateUtc\":\"2013-02-19T00:50:00.000Z\"},\"flightPlanPlannedArrival\":{\"dateLocal\":\"2013-02-18T16:56:00
    // .000\",\"dateUtc\":\"2013-02-19T00:56:00.000Z\"},
    // \"estimatedRunwayArrival\":{\"dateLocal\":\"2013-02-18T16:49:00.000\",\"dateUtc\":\"2013-02-19T00:49:00
    // .000Z\"},\"actualRunwayArrival\":{\"dateLocal\":\"2013-02-18T16:47:00.000\",
    // \"dateUtc\":\"2013-02-19T00:47:00.000Z\"}},\"codeshares\":{\"codeshare\":[{\"fsCode\":\"UA\",
    // \"flightNumber\":\"5312\",\"relationship\":\"X\"},{\"fsCode\":\"AC\",\"flightNumber\":\"4290\",
    // \"relationship\":\"Z\"}]},\"flightDurations\":{\"scheduledBlockMinutes\":\"41\",\"blockMinutes\":\"39\",
    // \"scheduledAirMinutes\":\"29\",\"airMinutes\":\"29\",\"scheduledTaxiOutMinutes\":\"10\",
    // \"taxiOutMinutes\":\"7\",\"scheduledTaxiInMinutes\":\"2\",\"taxiInMinutes\":\"3\"},
    // \"flightEquipment\":{\"scheduledEquipmentIataCode\":\"EM2\",\"actualEquipmentIataCode\":\"E120\"},
    // \"flightStatusUpdates\":{\"flightStatusUpdate\":[{\"updatedAt\":{\"dateUtc\":\"2013-02-17T16:22:06
    // .226-08:00\"},\"source\":\"Airline (UA)\",\"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"EGD\",
    // \"newDateLocal\":\"2013-02-18T16:17:00.000\"},{\"field\":\"EGA\",\"newDateLocal\":\"2013-02-18T16:58:00
    // .000\"}]}},{\"updatedAt\":{\"dateUtc\":\"2013-02-17T16:25:57.963-08:00\"},\"source\":\"ASDI\",
    // \"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"SRD\",\"newDateLocal\":\"2013-02-18T16:27:00.000\"},
    // {\"field\":\"ERD\",\"newDateLocal\":\"2013-02-18T16:27:00.000\"},{\"field\":\"SRA\",
    // \"newDateLocal\":\"2013-02-18T16:56:00.000\"},{\"field\":\"ERA\",\"newDateLocal\":\"2013-02-18T16:56:00
    // .000\"}]}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T15:11:07.237-08:00\"},\"source\":\"ASDI\",
    // \"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"ERD\",\"originalDateLocal\":\"2013-02-18T16:27:00
    // .000\",\"newDateLocal\":\"2013-02-18T16:29:00.000\"},{\"field\":\"ERA\",
    // \"originalDateLocal\":\"2013-02-18T16:56:00.000\",\"newDateLocal\":\"2013-02-18T16:55:00.000\"}]}},
    // {\"updatedAt\":{\"dateUtc\":\"2013-02-18T15:24:25.477-08:00\"},\"source\":\"Airline (UA)\",
    // \"updatedDateFields\":{\"updatedDateField\":{\"field\":\"EGA\",\"originalDateLocal\":\"2013-02-18T16:58:00
    // .000\",\"newDateLocal\":\"2013-02-18T16:55:00.000\"}}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T15:51:26
    // .401-08:00\"},\"source\":\"ASDI\",\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ERA\",
    // \"originalDateLocal\":\"2013-02-18T16:55:00.000\",\"newDateLocal\":\"2013-02-18T16:52:00.000\"}}},
    // {\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:23:24.276-08:00\"},\"source\":\"ASDI\",
    // \"updatedTextFields\":{\"updatedTextField\":{\"field\":\"AQP\",\"newText\":\"E120\"}},
    // \"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"ERD\",\"originalDateLocal\":\"2013-02-18T16:29:00
    // .000\",\"newDateLocal\":\"2013-02-18T16:18:00.000\"},{\"field\":\"ERA\",
    // \"originalDateLocal\":\"2013-02-18T16:52:00.000\",\"newDateLocal\":\"2013-02-18T16:46:00.000\"}]}},
    // {\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:23:35.561-08:00\"},\"source\":\"ASDI\",
    // \"updatedTextFields\":{\"updatedTextField\":{\"field\":\"STS\",\"originalText\":\"S\",\"newText\":\"A\"}},
    // \"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ARD\",\"newDateLocal\":\"2013-02-18T16:18:00
    // .000\"}}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:24:35.270-08:00\"},\"source\":\"Airline (UA)\",
    // \"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"AGD\",\"newDateLocal\":\"2013-02-18T16:11:00.000\"},
    // {\"field\":\"EGA\",\"originalDateLocal\":\"2013-02-18T16:55:00.000\",\"newDateLocal\":\"2013-02-18T16:44:00
    // .000\"}]}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:36:20.338-08:00\"},\"source\":\"ASDI\",
    // \"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ERA\",\"originalDateLocal\":\"2013-02-18T16:46:00
    // .000\",\"newDateLocal\":\"2013-02-18T16:44:00.000\"}}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:47:16
    // .500-08:00\"},\"source\":\"ASDI\",\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ERA\",
    // \"originalDateLocal\":\"2013-02-18T16:44:00.000\",\"newDateLocal\":\"2013-02-18T16:45:00.000\"}}},
    // {\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:54:27.034-08:00\"},\"source\":\"ASDI\",
    // \"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ERA\",\"originalDateLocal\":\"2013-02-18T16:45:00
    // .000\",\"newDateLocal\":\"2013-02-18T16:49:00.000\"}}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:55:09
    // .690-08:00\"},\"source\":\"ASDI\",\"updatedTextFields\":{\"updatedTextField\":{\"field\":\"STS\",
    // \"originalText\":\"A\",\"newText\":\"L\"}},\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"ARA\",
    // \"newDateLocal\":\"2013-02-18T16:47:00.000\"}}},{\"updatedAt\":{\"dateUtc\":\"2013-02-18T16:58:29
    // .416-08:00\"},\"source\":\"Airline (UA)\",\"updatedDateFields\":{\"updatedDateField\":{\"field\":\"AGA\",
    // \"newDateLocal\":\"2013-02-18T16:50:00.000\"}}}]}}},\"appendix\":{\"airlines\":{\"airline\":[{\"fs\":\"OO\",
    // \"iata\":\"OO\",\"icao\":\"SKW\",\"name\":\"SkyWest Airlines\",\"active\":\"true\"},{\"fs\":\"AC\",
    // \"iata\":\"AC\",\"icao\":\"ACA\",\"name\":\"Air Canada\",\"phoneNumber\":\"1-800-247-2262\",
    // \"active\":\"true\"},{\"fs\":\"UA\",\"iata\":\"UA\",\"icao\":\"UAL\",\"name\":\"United Airlines\",
    // \"phoneNumber\":\"1-800-864-8331\",\"active\":\"true\"}]},\"airports\":{\"airport\":[{\"fs\":\"EUG\",
    // \"iata\":\"EUG\",\"icao\":\"KEUG\",\"faa\":\"EUG\",\"name\":\"Eugene Airport\",
    // \"street1\":\"28855 Lockheed Drive\",\"street2\":\"\",\"city\":\"Eugene\",\"cityCode\":\"EUG\",
    // \"stateCode\":\"OR\",\"postalCode\":\"97402\",\"countryCode\":\"US\",\"countryName\":\"United States\",
    // \"regionName\":\"North America\",\"timeZoneRegionName\":\"America/Los_Angeles\",\"weatherZone\":\"ORZ008\",
    // \"localTime\":\"2013-02-18T17:06:42.698\",\"utcOffsetHours\":\"-8.0\",\"latitude\":\"44.119196\",
    // \"longitude\":\"-123.211968\",\"elevationFeet\":\"369\",\"classification\":\"3\",\"active\":\"true\"},
    // {\"fs\":\"PDX\",\"iata\":\"PDX\",\"icao\":\"KPDX\",\"faa\":\"PDX\",\"name\":\"Portland International
    // Airport\",\"street1\":\"7000 NE Airport Way\",\"city\":\"Portland\",\"cityCode\":\"PDX\",\"stateCode\":\"OR\",
    // \"postalCode\":\"97218\",\"countryCode\":\"US\",\"countryName\":\"United States\",
    // \"regionName\":\"North America\",\"timeZoneRegionName\":\"America/Los_Angeles\",\"weatherZone\":\"ORZ006\",
    // \"localTime\":\"2013-02-18T17:06:42.698\",\"utcOffsetHours\":\"-8.0\",\"latitude\":\"45.588995\",
    // \"longitude\":\"-122.592901\",\"elevationFeet\":\"30\",\"classification\":\"1\",\"active\":\"true\"}]}}}" +
    //           "";

    String s = "{\"alert\":{\"event\":{\"type\":\"PRE_DEPARTURE_STATUS\",\"value\":\"360\"}," +
               "\"dataSource\":\"FlightHistory\",\"dateTimeRecorded\":\"2015-01-25T22:55:04.775Z\"," +
               "\"rule\":{\"id\":\"241530370\",\"name\":\"Default\",\"carrierFsCode\":\"AC\"," +
               "\"flightNumber\":\"858\",\"departureAirportFsCode\":\"YYZ\",\"arrivalAirportFsCode\":\"LHR\"," +
               "\"departure\":\"2015-01-25T23:55:00.000\",\"arrival\":\"2015-01-26T11:50:00.000\"," +
               "\"ruleEvents\":{\"ruleEvent\":[{\"type\":\"PRE_DEPARTURE_STATUS\",\"value\":\"360\"}," +
               "{\"type\":\"DEPARTURE_DELAY\",\"value\":\"15\"},{\"type\":\"DEPARTURE_DELAY_DELTA\"," +
               "\"value\":\"15\"},{\"type\":\"DEPARTURE_DELAY_WINDOW\",\"value\":\"360\"}]},\"nameValues\":null," +
               "\"delivery\":{\"format\":\"json\",\"destination\":\"http://dev.umapped.com/flightstats/alert\"}}," +
               "\"flightStatus\":{\"flightId\":\"492796432\",\"carrierFsCode\":\"AC\",\"flightNumber\":\"858\"," +
               "\"departureAirportFsCode\":\"YYZ\",\"arrivalAirportFsCode\":\"LHR\"," +
               "\"departureDate\":{\"dateLocal\":\"2015-01-25T23:55:00.000\",\"dateUtc\":\"2015-01-26T04:55:00" +
               ".000Z\"},\"arrivalDate\":{\"dateLocal\":\"2015-01-26T11:50:00.000\"," +
               "\"dateUtc\":\"2015-01-26T11:50:00.000Z\"},\"status\":\"S\",\"schedule\":{\"flightType\":\"J\"," +
               "\"serviceClasses\":\"RFJY\",\"restrictions\":\"\"}," +
               "\"operationalTimes\":{\"publishedDeparture\":{\"dateLocal\":\"2015-01-25T23:55:00.000\"," +
               "\"dateUtc\":\"2015-01-26T04:55:00.000Z\"},\"publishedArrival\":{\"dateLocal\":\"2015-01-26T11:50:00" +
               ".000\",\"dateUtc\":\"2015-01-26T11:50:00" +
               ".000Z\"},\"scheduledGateDeparture\":{\"dateLocal\":\"2015-01-25T23:55:00.000\"," +
               "\"dateUtc\":\"2015-01-26T04:55:00.000Z\"}," +
               "\"flightPlanPlannedDeparture\":{\"dateLocal\":\"2015-01-26T00:05:00.000\"," +
               "\"dateUtc\":\"2015-01-26T05:05:00.000Z\"}," +
               "\"estimatedRunwayDeparture\":{\"dateLocal\":\"2015-01-26T00:05:00.000\"," +
               "\"dateUtc\":\"2015-01-26T05:05:00.000Z\"}," +
               "\"scheduledGateArrival\":{\"dateLocal\":\"2015-01-26T11:50:00.000\"," +
               "\"dateUtc\":\"2015-01-26T11:50:00.000Z\"}," +
               "\"flightPlanPlannedArrival\":{\"dateLocal\":\"2015-01-26T11:11:00.000\"," +
               "\"dateUtc\":\"2015-01-26T11:11:00.000Z\"}," +
               "\"estimatedRunwayArrival\":{\"dateLocal\":\"2015-01-26T11:11:00.000\"," +
               "\"dateUtc\":\"2015-01-26T11:11:00.000Z\"}},\"codeshares\":{\"codeshare\":[{\"fsCode\":\"9W\"," +
               "\"flightNumber\":\"5048\",\"relationship\":\"L\"},{\"fsCode\":\"LH\",\"flightNumber\":\"6559\"," +
               "\"relationship\":\"L\"},{\"fsCode\":\"ME\",\"flightNumber\":\"4522\",\"relationship\":\"L\"}," +
               "{\"fsCode\":\"MS\",\"flightNumber\":\"9619\",\"relationship\":\"L\"},{\"fsCode\":\"SA\"," +
               "\"flightNumber\":\"7301\",\"relationship\":\"L\"},{\"fsCode\":\"SK\",\"flightNumber\":\"3882\"," +
               "\"relationship\":\"L\"},{\"fsCode\":\"TP\",\"flightNumber\":\"8277\",\"relationship\":\"L\"}," +
               "{\"fsCode\":\"UA\",\"flightNumber\":\"8412\",\"relationship\":\"L\"},{\"fsCode\":\"UL\"," +
               "\"flightNumber\":\"2858\",\"relationship\":\"L\"}]}," +
               "\"flightDurations\":{\"scheduledBlockMinutes\":\"415\",\"scheduledAirMinutes\":\"366\"," +
               "\"scheduledTaxiOutMinutes\":\"10\",\"scheduledTaxiInMinutes\":\"39\"}," +
               "\"airportResources\":{\"departureTerminal\":\"1\",\"departureGate\":\"E71\"," +
               "\"arrivalTerminal\":\"2\"},\"flightEquipment\":{\"scheduledEquipmentIataCode\":\"78" +
               "8\"},\"flightStatusUpdates\":{\"flightStatusUpdate\":[{\"updatedAt\":{\"dateUtc\":\"2015-01-22T18:07" +
               ":02.770-08:00\"},\"source\":\"Innovata\"},{\"updatedAt\":{\"dateUtc\":\"2015-01-24T21:01:10" +
               ".789-08:00\"},\"source\":\"ASDI\",\"updatedDateFields\":{\"updatedDateField\":[{\"field\":\"ERA\"," +
               "\"newDateLocal\":\"2015-01-26T11:11:00.000\"},{\"field\":\"ERD\"," +
               "\"newDateLocal\":\"2015-01-26T00:05:00.000\"},{\"field\":\"SRA\"," +
               "\"newDateLocal\":\"2015-01-26T11:11:00.000\"},{\"field\":\"SRD\"," +
               "\"newDateLocal\":\"2015-01-26T00:05:00.000\"}]}},{\"updatedAt\":{\"dateUtc\":\"2015-01-25T08:58:05" +
               ".507-08:00\"},\"source\":\"Airline Direct\"," +
               "\"updatedTextFields\":{\"updatedTextField\":{\"field\":\"DGT\",\"newText\":\"E71\"}}}]}}}," +
               "\"appendix\":{\"airlines\":{\"airline\":[{\"fs\":\"AC\",\"iata\":\"AC\",\"icao\":\"ACA\"," +
               "\"name\":\"Air Canada\",\"phoneNumber\":\"1-800-247-2262\",\"active\":\"true\"},{\"fs\":\"MS\"," +
               "\"iata\":\"MS\",\"icao\":\"MSR\",\"name\":\"EgyptAir\",\"active\":\"true\"},{\"fs\":\"UA\"," +
               "\"iata\":\"UA\",\"icao\":\"UAL\",\"name\":\"United Airlines\",\"phoneNumber\":\"1-800-864-8331\"," +
               "\"active\":\"true\"},{\"fs\":\"SA\",\"iata\":\"SA\",\"icao\":\"SAA\"," +
               "\"name\":\"South African Airways\",\"active\":\"true\"},{\"fs\":\"UL\",\"iata\":\"UL\"," +
               "\"icao\":\"ALK\",\"name\":\"SriLankan Airlines\",\"active\":\"true\"},{\"fs\":\"ME\",\"iata\":\"ME\"," +
               "\"icao\":\"MEA\",\"name\":\"Middle East Airlines\",\"active\":\"true\"},{\"fs\":\"SK\"," +
               "\"iata\":\"SK\",\"icao\":\"SAS\",\"name\":\"SAS\",\"active\":\"true\"},{\"fs\":\"LH\"," +
               "\"iata\":\"LH\",\"icao\":\"DLH\",\"name\":\"Lufthansa\",\"active\":\"true\"},{\"fs\":\"TP\"," +
               "\"iata\":\"TP\",\"icao\":\"TAP\",\"name\":\"TAP Portugal\",\"active\":\"true\"},{\"fs\":\"9W\"," +
               "\"iata\":\"9W\",\"icao\":\"JAI\"" +
               ",\"name\":\"Jet Airways (India)\",\"active\":\"true\"}]},\"airports\":{\"airport\":[{\"fs\":\"LHR\"," +
               "\"iata\":\"LHR\",\"icao\":\"EGLL\",\"name\":\"London Heathrow Airport\",\"city\":\"London\"," +
               "\"cityCode\":\"LON\",\"stateCode\":\"EN\",\"countryCode\":\"GB\",\"countryName\":\"United Kingdom\"," +
               "\"regionName\":\"Europe\",\"timeZoneRegionName\":\"Europe/London\"," +
               "\"localTime\":\"2015-01-25T22:55:18.209\",\"utcOffsetHours\":\"0.0\",\"latitude\":\"51.469603\"," +
               "\"longitude\":\"-0.453566\",\"elevationFeet\":\"80\",\"classification\":\"1\",\"active\":\"true\"}," +
               "{\"fs\":\"YYZ\",\"iata\":\"YYZ\",\"icao\":\"CYYZ\",\"name\":\"Pearson International Airport\"," +
               "\"street1\":\"\",\"street2\":\"\",\"city\":\"Toronto\",\"cityCode\":\"YTO\",\"stateCode\":\"ON\"," +
               "\"postalCode\":\"L5P 1B2\",\"countryCode\":\"CA\",\"countryName\":\"Canada\"," +
               "\"regionName\":\"North America\",\"timeZoneRegionName\":\"America/Toronto\"," +
               "\"localTime\":\"2015-01-25T17:55:18.209\",\"utcOffsetHours\":\"-5.0\",\"latitude\":\"43.681585\"," +
               "\"longitude\":\"-79.61146\",\"elevationFeet\":\"569\",\"classification\":\"1\"," +
               "\"active\":\"true\"}]}}}" +
               "";

    JsonNode node = request().body().asJson();
    Log.log(LogLevel.DEBUG, "FlightStatusController MSG: \n" + node.toString());


    try {
      // ObjectMapper obj = new ObjectMapper();
      // JsonNode node = obj.readTree(s);

      ObjectMapper mapper = new ObjectMapper();
      com.fasterxml.jackson.databind.ObjectReader r = mapper.readerFor(AlertEvent.class);
      AlertEvent jsonResp = r.readValue(node);
      Log.log(LogLevel.DEBUG, "FlightStatusController OK");


    }
    catch (Exception e) {
      e.printStackTrace();
      Log.err( "flightstatsAlert -  Error cannot process FlightStat Alert: ", e);
      e.printStackTrace();
    }
    return ok("");
  }

  /* trigger poll for status check

   */
  public Result checkFlightstat() {
    final DynamicForm form = formFactory.form().bindFromRequest();
    String auth = form.get("auth");
    if (auth == null || !auth.equals("umapped_flight_status_aeae0c80")) {
      BaseView view = new BaseView();
      view.message = "";
      return ok(views.html.error.error404.render(view));
    }
    //send to Actor
    final FlightPollAlertsActor.Command cmd = new FlightPollAlertsActor.Command(null);
    ActorsHelper.tell(SupervisorActor.UmappedActor.FLIGHT_POLL, cmd);
    return ok("");
  }

  /*
    Test alert track method
   */
  public Result flightstatsTestTrack() {
/*
    DateTime dateTime = new DateTime(1361902500000l);

    //send to Actor
    FlightTrackMsg msg = new FlightTrackMsg();
    msg.airlineCode = "AC";
    msg.arriveAirport="LHR";
    msg.departAirport="JFK";
    msg.departTimestamp = dateTime;
    msg.flightId = 100;
    msg.tripDetailsId = "651200521015032706";
    msg.tripId = "651199371005032709";
    msg.userId = "thierry";

    SupervisorActor.tell(SupervisorActor.Actor.FLIGHT_TRACK, msg);
*/

    String s = "{\"request\":{\"airline\":{\"requestedCode\":\"AC\",\"fsCode\":\"AC\"}," +
               "\"flight\":{\"requested\":\"424\",\"interpreted\":\"424\"},\"date\":{\"year\":\"2015\"," +
               "\"month\":\"2\",\"day\":\"11\",\"interpreted\":\"2015-02-11\"},\"utc\":{\"requested\":\"false\"," +
               "\"interpreted\":false},\"airport\":{\"requestedCode\":\"YYZ\",\"fsCode\":\"YYZ\"},\"codeType\":{}," +
               "\"extendedOptions\":{},\"url\":\"https://api.flightstats" +
               ".com/flex/flightstatus/rest/v2/json/flight/status/AC/424/dep/2015/2/11\"}," +
               "\"appendix\":{\"airlines\":[{\"fs\":\"AC\",\"iata\":\"AC\",\"icao\":\"ACA\",\"name\":\"Air Canada\"," +
               "\"phoneNumber\":\"1-800-247-2262\",\"active\":true}],\"airports\":[{\"fs\":\"YUL\",\"iata\":\"YUL\"," +
               "\"icao\":\"CYUL\",\"name\":\"Montreal-Pierre Elliott Trudeau International Airport\"," +
               "\"street1\":\"975 Romeo Vachon Street North\",\"city\":\"Montreal\",\"cityCode\":\"YMQ\"," +
               "\"stateCode\":\"QC\",\"countryCode\":\"CA\",\"countryName\":\"Canada\"," +
               "\"regionName\":\"North America\",\"timeZoneRegionName\":\"America/Toronto\"," +
               "\"localTime\":\"2015-02-11T08:54:43.996\",\"utcOffsetHours\":-5.0,\"latitude\":45.457715," +
               "\"longitude\":-73.749906,\"elevationFeet\":117,\"classification\":1,\"active\":true," +
               "\"delayIndexUrl\":\"https://api.flightstats" +
               ".com/flex/delayindex/rest/v1/json/airports/YUL?codeType=fs\",\"weatherUrl\":\"https://api.flightstats" +
               ".com/flex/weather/rest/v1/json/all/YUL?codeType=fs\"},{\"fs\":\"YYZ\",\"iata\":\"YYZ\"," +
               "\"icao\":\"CYYZ\",\"name\":\"Pearson International Airport\",\"street1\":\"\",\"street2\":\"\"," +
               "\"city\":\"Toronto\",\"cityCode\":\"YTO\",\"stateCode\":\"ON\",\"postalCode\":\"L5P 1B2\"," +
               "\"countryCode\":\"CA\",\"countryName\":\"Canada\",\"regionName\":\"North America\"," +
               "\"timeZoneRegionName\":\"America/Toronto\",\"localTime\":\"2015-02-11T08:54:43.971\"," +
               "\"utcOffsetHours\":-5.0,\"latitude\":43.681585,\"longitude\":-79.61146,\"elevationFeet\":569," +
               "\"classification\":1,\"active\":true,\"delayIndexUrl\":\"https://api.flightstats" +
               ".com/flex/delayindex/rest/v1/json/airports/YYZ?codeType=fs\",\"weatherUrl\":\"https://api.flightstats" +
               ".com/flex/weather/rest/v1/json/all/YYZ?codeType=fs\"}],\"equipments\":[{\"iata\":\"319\"," +
               "\"name\":\"Airbus Industrie A319\",\"turboProp\":false,\"jet\":true,\"widebody\":false," +
               "\"regional\":false}]},\"flightStatuses\":[{\"flightId\":500279579,\"carrierFsCode\":\"AC\"," +
               "\"flightNumber\":\"424\",\"departureAirportFsCode\":\"YYZ\",\"arrivalAirportFsCode\":\"YUL\"," +
               "\"departureDate\":{\"dateLocal\":\"2015-02-11T19:00:00.000\",\"dateUtc\":\"2015-02-12T00:00:00" +
               ".000Z\"},\"arrivalDate\":{\"dateLocal\":\"2015-02-11T20:18:00.000\"," +
               "\"dateUtc\":\"2015-02-12T01:18:00.000Z\"},\"status\":\"S\",\"schedule\":{\"flightType\":\"J\"," +
               "\"serviceClasses\":\"RFJY\",\"restrictions\":\"\"}," +
               "\"operationalTimes\":{\"publishedDeparture\":{\"dateLocal\":\"2015-02-11T19:00:00.000\"," +
               "\"dateUtc\":\"2015-02-12T00:00:00.000Z\"},\"publishedArrival\":{\"dateLocal\":\"2015-02-11T20:18:00" +
               ".000\",\"dateUtc\":\"2015-02-12T01:18:00.000Z\"}," +
               "\"scheduledGateDeparture\":{\"dateLocal\":\"2015-02-11T19:00:00.000\"," +
               "\"dateUtc\":\"2015-02-12T00:00:00.000Z\"}," +
               "\"flightPlanPlannedDeparture\":{\"dateLocal\":\"2015-02-11T19:11:00.000\"," +
               "\"dateUtc\":\"2015-02-12T00:11:00.000Z\"}," +
               "\"estimatedRunwayDeparture\":{\"dateLocal\":\"2015-02-11T19:11:00.000\"," +
               "\"dateUtc\":\"2015-02-12T00:11:00.000Z\"}," +
               "\"scheduledGateArrival\":{\"dateLocal\":\"2015-02-11T20:18:00.000\"," +
               "\"dateUtc\":\"2015-02-12T01:18:00.000Z\"}," +
               "\"flightPlanPlannedArrival\":{\"dateLocal\":\"2015-02-11T20:02:00.000\"," +
               "\"dateUtc\":\"2015-02-12T01:02:00.000Z\"}," +
               "\"estimatedRunwayArrival\":{\"dateLocal\":\"2015-02-11T20:02:00.000\"," +
               "\"dateUtc\":\"2015-02-12T01:02:00.000Z\"}},\"flightDurations\":{\"scheduledBlockMinutes\":78," +
               "\"scheduledAirMinutes\":51,\"scheduledTaxiOutMinutes\":11,\"scheduledTaxiInMinutes\":16}," +
               "\"airportResources\":{\"departureTerminal\":\"1\",\"departureGate\":\"D28\"}," +
               "\"flightEquipment\":{\"scheduledEquipmentIataCode\":\"319\"}}]}" + "";

    /*
    String s = "{\"request\":{\"airline\":{\"requestedCode\":\"CZ9\",\"error\":\"BAD_AIRLINE_CODE\"},\"flight\":{}," +
               "\"date\":{\"year\":\"2015\",\"month\":\"2\",\"day\":\"12\",\"interpreted\":\"2015-02-12\"}," +
               "\"utc\":{},\"airport\":{},\"codeType\":{},\"extendedOptions\":{},\"url\":\"https://api.flightstats" +
               ".com/flex/flightstatus/rest/v2/json/flight/status/CZ9/7543/dep/2015/2/12\"},\"appendix\":{}," +
               "\"error\":{\"httpStatusCode\":400,\"errorCode\":\"BAD_AIRLINE_CODE\"," +
               "\"errorId\":\"56be4405-468e-42fb-a9b3-5874c276fc5c\",\"errorMessage\":\"The given airline code was " +
               "invalid. Invalid airline code: 'CZ9'\"},\"flightStatuses\":[]}";
               */
    try {
      ObjectMapper mapper = new ObjectMapper();
      JsonNode json = mapper.readTree(s);

      com.fasterxml.jackson.databind.ObjectReader r = mapper.readerFor(FlightStatusMsg.class);
      FlightStatusMsg jsonResp = r.readValue(json);
      Log.debug(json.toString());

      if (jsonResp != null && jsonResp.getError() == null &&
          jsonResp.getFlightStatuses() != null &&
          jsonResp.getFlightStatuses().size() > 0) {
        com.mapped.publisher.msg.flightstat.status.FlightStatus fs = jsonResp.getFlightStatuses().get(0);

        //find the depart times
        DateTime departDate = Utils.getDateTimeFromISO(fs.getDepartureDate().getDateLocal());
        DateTime departDateUtc = Utils.getDateTimeFromISO(fs.getDepartureDate().getDateUtc());

        if (fs.getOperationalTimes() != null &&
            fs.getOperationalTimes().getScheduledGateDeparture() != null &&
            fs.getOperationalTimes().getScheduledGateDeparture().getDateLocal() != null &&
            fs.getOperationalTimes().getScheduledGateDeparture().getDateUtc() != null) {
          departDate = Utils.getDateTimeFromISO(fs.getOperationalTimes().getScheduledGateDeparture().getDateLocal());
          departDateUtc = Utils.getDateTimeFromISO(fs.getOperationalTimes().getScheduledGateDeparture().getDateUtc());
        }
        else if (fs.getOperationalTimes() != null &&
                 fs.getOperationalTimes().getEstimatedGateDeparture() != null &&
                 fs.getOperationalTimes().getEstimatedGateDeparture().getDateLocal() != null &&
                 fs.getOperationalTimes().getEstimatedGateDeparture().getDateUtc() != null) {
          departDate = Utils.getDateTimeFromISO(fs.getOperationalTimes().getEstimatedGateDeparture().getDateLocal());
          departDateUtc = Utils.getDateTimeFromISO(fs.getOperationalTimes().getEstimatedGateDeparture().getDateUtc());
        }

        //find the arrive times
        DateTime arriveDate = Utils.getDateTimeFromISO(fs.getArrivalDate().getDateLocal());
        DateTime arriveDateUtc = Utils.getDateTimeFromISO(fs.getArrivalDate().getDateUtc());

        if (fs.getOperationalTimes() != null &&
            fs.getOperationalTimes().getScheduledGateArrival() != null &&
            fs.getOperationalTimes().getScheduledGateArrival().getDateLocal() != null &&
            fs.getOperationalTimes().getScheduledGateArrival().getDateUtc() != null) {
          arriveDate = Utils.getDateTimeFromISO(fs.getOperationalTimes().getScheduledGateArrival().getDateLocal());
          arriveDateUtc = Utils.getDateTimeFromISO(fs.getOperationalTimes().getScheduledGateArrival().getDateUtc());
        }
        else if (fs.getOperationalTimes() != null &&
                 fs.getOperationalTimes().getEstimatedGateArrival() != null &&
                 fs.getOperationalTimes().getEstimatedGateArrival().getDateLocal() != null &&
                 fs.getOperationalTimes().getEstimatedGateArrival().getDateUtc() != null) {
          arriveDate = Utils.getDateTimeFromISO(fs.getOperationalTimes().getEstimatedGateArrival().getDateLocal());
          arriveDateUtc = Utils.getDateTimeFromISO(fs.getOperationalTimes().getEstimatedGateArrival().getDateUtc());
        }
        Log.debug("FS: " + jsonResp.getFlightStatuses()
                                   .get(0)
                                   .getOperationalTimes()
                                   .getEstimatedGateDeparture() + " " +
                  jsonResp.getFlightStatuses().get(0).getStatus());

      }
      else if (jsonResp != null && jsonResp.getError() != null) {
        Log.err(jsonResp.getError().getErrorCode());
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      Log.err( "FlightProcessPendingAlertsActor - Error parsing response AlertId: ");
      Log.err( "FlightProcessPendingAlertsActor - Error parsing response");
    }

    DateTime departTimestamp = new DateTime(System.currentTimeMillis());

    // curl -v  -X GET "https://api.flightstats
    // .com/flex/flightstatus/rest/v2/json/flight/status/CI/9327/dep/2015/2/6?appId=aeae0c80&appKey
    // =8106152200b485839efba94368e7fb36&utc=false&airport=yyz"
    StringBuilder urlQuery = new StringBuilder(
        "https://api.flightstats.com/flex/flightstatus/rest/v2/json/flight/status/");
    urlQuery.append("AC");//CZ7543
    urlQuery.append("/");
    urlQuery.append("424");
    urlQuery.append("/dep/");
    urlQuery.append(departTimestamp.getYear());
    urlQuery.append("/");
    urlQuery.append(departTimestamp.plusMonths(1).getMonthOfYear());
    urlQuery.append("/");
    urlQuery.append(departTimestamp.getDayOfMonth());

    String url = urlQuery.toString();// URLEncoder.encode( urlQuery.toString(),"ISO-8859-1");

    //make the GET request
    WS.url(url)
      .setQueryParameter("appId", "aeae0c80")
      .setQueryParameter("appKey", "8106152200b485839efba94368e7fb36")
      .setQueryParameter("utc", "false")
      .setQueryParameter("airport", "YYZ")
      .get()
      .thenApplyAsync((WSResponse response) -> {
          JsonNode json = response.asJson();

          try {
            ObjectMapper mapper = new ObjectMapper();
            com.fasterxml.jackson.databind.ObjectReader r = mapper.readerFor(FlightStatusMsg.class);
            FlightStatusMsg jsonResp = r.readValue(json);
            Log.debug(json.toString());
            if (jsonResp != null &&
                jsonResp.getError() == null &&
                jsonResp.getFlightStatuses() != null &&
                jsonResp.getFlightStatuses() != null &&
                jsonResp.getFlightStatuses().size() > 0) {
              Log.debug("FS: " + jsonResp.getFlightStatuses()
                                         .get(0)
                                         .getOperationalTimes()
                                         .getEstimatedGateDeparture() + " " +
                        jsonResp.getFlightStatuses().get(0).getStatus());

            }
            else if (jsonResp != null && jsonResp.getError() != null) {
              Log.err(jsonResp.getError().getErrorCode());
            }
            return json;
          }
          catch (Exception e) {
            e.printStackTrace();
            Log.err("FlightProcessPendingAlertsActor - Error parsing response " + "AlertId: ");
            Log.err("FlightProcessPendingAlertsActor - Error parsing response" + json.toString());
          }
          return null;
      });


    return ok("Alert Sent");
  }


}
