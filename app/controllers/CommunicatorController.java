package controllers;

import actors.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.AdminAccess;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.SendGridForm;
import com.mapped.publisher.js.BaseJsonResponse;
import com.mapped.publisher.persistence.clickatell.MOCallback;
import com.mapped.publisher.persistence.communicator.Communicator;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.BaseView;
import com.umapped.BodyParsers.Json100Kb;
import com.umapped.BodyParsers.TolerantText100Kb;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.persistence.digest.CommunicatorMsg;
import models.publisher.Account;
import models.publisher.AccountTripLink;
import models.publisher.Trip;
import models.publisher.TripDetail;
import org.apache.commons.lang3.time.StopWatch;
import org.h2.util.StringUtils;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import scala.concurrent.Promise;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Controller to support 2 way communication in communicator
 * Created by surge on 2016-02-01.
 */
public class CommunicatorController
    extends Controller {
  @Inject
  RedisMgr    redis;
  @Inject
  FormFactory formFactory;
  @com.google.inject.Inject HttpExecutionContext ec;
  /**
   * Sendgvoidrid delivered responses from members
   * @return
   */
  @BodyParser.Of(BodyParser.MultipartFormData.class)
  public CompletionStage<Result> sendgridIn() {
    return  CompletableFuture.supplyAsync(()->{
      StopWatch sw = new StopWatch();
      sw.start();
      Form<SendGridForm> emailForm = formFactory.form(SendGridForm.class);
      SendGridForm       message   = emailForm.bindFromRequest().get();
      preProcessReply(message);
      return ok();
    }, ec.current());
  }

  private static Pattern inAddressPattern = Pattern.compile("([igb])\\.([A-Za-z0-9]*)\\.([ut])\\.([A-Za-z0-9]*)@.*");

  boolean preProcessReply(SendGridForm form) {
    //1. Reverse where message should go
    Matcher m = inAddressPattern.matcher(form.getTo());
    if(m.find()) {
      CommunicatorReplyActor.Protocol cmd = CommunicatorReplyActor.Protocol.build(CommunicatorReplyActor
                                                                                              .Protocol.Command.REPLY);
      cmd.reply.fromEmail = form.getFrom();
      Long id = Utils.expandNumber(m.group(2));
      switch (m.group(1)) {
        case "i": //Internal
          Trip trip = Trip.find.byId(id.toString());
          if(trip == null) {
            return false;
          }
          cmd.reply.tripId = id.toString();
          cmd.reply.detailsId = "internal";
          break;
        case "g": //General
          Trip gTrip = Trip.find.byId(id.toString());
          if(gTrip == null) {
            return false;
          }
          cmd.reply.tripId = id.toString();
          cmd.reply.detailsId = "general";
          break;
        case "b": //Booking
          TripDetail td = TripDetail.findByPK(id.toString());
          if(td == null) {
            return false;
          }
          cmd.reply.tripId = td.getTripid();
          cmd.reply.detailsId = td.getDetailsid();
          break;
        default:
          return false;
      }

      switch (m.group(3)) {
        case "u":
          cmd.reply.userId = m.group(4);
          break;
        case "t":
          Long tid = Utils.expandNumber(m.group(4));
          cmd.reply.travelerId = tid.toString();
          break;
      }

      if(form.getHtml() != null && !form.getHtml().isEmpty()) {
        cmd.reply.isHtml = true;
        cmd.reply.message = form.getHtml();
      } else {
        cmd.reply.isHtml = false;
        cmd.reply.message = form.getText();
      }

      ActorsHelper.tell(SupervisorActor.UmappedActor.COMMUNICATOR_REPLY, cmd);
      return true;
    } else {
      Log.info("Incoming e-mail is probably a spam. Score: " + form.getSpam_score());
      return false;
    }
  }

  /**
   * Accept incoming Json as text and send it for processing later in REDIS
   * Beware: CORS Accessible
   * @return
   */
  @BodyParser.Of(TolerantText100Kb.class)
  public Result messageSent(String callback) {
    StopWatch sw = new StopWatch();
    sw.start();
    redis.publish(CommunicatorToAllActor.COMM2ALL_CHANNEL, request().body().asText());
    sw.stop();
    Log.debug("TwoWayComm: forwarded message in: " + sw.getTime() + "ms");
    BaseJsonResponse bjr = new BaseJsonResponse(ReturnCode.SUCCESS);
    bjr.setCallback(callback);
    return bjr.toJsonResult();
  }

  public Result redisBench() {
    long totalTime = 0l;
    final int repeats = 1000;
    List<Long> tstList = new ArrayList<>();

    for(int i = 0; i< 1000; i++){
      tstList.add(DBConnectionMgr.getUniqueKey());
    }

    if(CacheMgr.getRedis() == null) {
      Log.err("################### Redis is NULL ################");
    } else {
      Log.err("!!!!!!!!!!!!!!!!!!! Redis is SET !!!!!!!!!!!!!!!!!");
    }

    StringBuilder sb = new StringBuilder();

    StopWatch sw = new StopWatch();

    sw.start();
    for(int i = 0; i < repeats; i++) {
      CacheMgr.set(Integer.toString(i), tstList, 60);
    }
    sw.stop();

    sb.append("Memcached write time is ").append(sw.getTime()).append("ms.");
    sb.append(System.lineSeparator());
    totalTime = sw.getTime();

    sw.reset();
    sw.start();

    for(int i = 0; i < repeats; i++) {
      List<Long> resStr = (List<Long>) CacheMgr.get(Integer.toString(i));
      if(resStr == null|| !resStr.equals(tstList)) {
        Log.err("Memcached mismatch for idx" + i);
      }
    }
    sw.stop();
    sb.append("Memcached read time is ").append(sw.getTime()).append("ms.");
    sb.append(System.lineSeparator());
    totalTime += sw.getTime();
    sb.append("Memcached total time is ").append(totalTime).append("ms.");
    sb.append(System.lineSeparator());

    sw.reset();
    totalTime = 0l;

    sw.start();

    for(int i = 0; i < repeats; i++) {
      redis.setUnsafe(Integer.toString(i), tstList, 10);
    }

    sw.stop();
    sb.append("Redis write time is ").append(sw.getTime()).append("ms.");
    sb.append(System.lineSeparator());
    totalTime = sw.getTime();
    sw.reset();

    try {
      TimeUnit.SECONDS.sleep(1);
    } catch (InterruptedException ie){}

    sw.start();

    for(int i = 0; i < repeats; i++) {
      List<Long> resStr = (List<Long>) redis.getBin(Integer.toString(i));
      if(resStr == null || !resStr.equals(tstList)) {
        Log.err("Redis mismatch for idx" + i);
      }
    }

    sw.stop();
    totalTime += sw.getTime();
    sb.append("Redis read time is ").append(sw.getTime()).append("ms.");
    sb.append(System.lineSeparator());
    sb.append("Redis total time is ").append(totalTime).append("ms.");
    sb.append(System.lineSeparator());

    return ok(sb.toString());
  }

  /**
   * @return
   */
  //@BodyParser.Of(value = BodyParser.Json.class, maxLength = MAX_MESSAGE_SIZE)
  public Result smsStatus() {
    JsonNode jsonRequest = request().body().asJson();

    if(jsonRequest == null) {
      return ok("FAILED TO PARSE STATUS - NO JSON");
    }
    com.mapped.publisher.persistence.clickatell.Status status = Json.fromJson(jsonRequest,
                                                                              com.mapped.publisher.persistence.clickatell.Status.class);
    CommunicatorSMSActor.Protocol cmd = CommunicatorSMSActor.Protocol.build(CommunicatorSMSActor.Protocol.Command
                                                                                .STATUS);
    cmd.status = status.data;
    ActorsHelper.tell(SupervisorActor.UmappedActor.COMMUNICATOR_SMS, cmd);
    return ok("SUCCESS");
  }


  @BodyParser.Of(Json100Kb.class)
  public Result smsReply() {
    JsonNode jsonRequest = request().body().asJson();
    MOCallback payload = Json.fromJson(jsonRequest, MOCallback.class);

    CommunicatorSMSActor.Protocol cmd = CommunicatorSMSActor.Protocol.build(CommunicatorSMSActor.Protocol.Command
                                                                                .REPLY);
    cmd.reply = payload.data;
    ActorsHelper.tell(SupervisorActor.UmappedActor.COMMUNICATOR_SMS, cmd);
    return ok();
  }

  public  Result mobile(String tId, String tripId) {
    AccountTripLink tripLink = null;
    if (Account.isEndcoded(tId)) {
      tripLink = AccountTripLink.findByUid(Account.decodeId(tripId), tripId);
    } else {
      tripLink= AccountTripLink.findByLegacyId(tId, tripId);
    }

    if (tripLink == null) {
      return notFound();
    }
    boolean talkback;
    Trip trip = Trip.findByPK(tripId);

    if (trip == null) {
      return notFound();
    }
    talkback = SecurityMgr.hasCapability(trip, Capability.COMMUNICATOR_TRAVELER_TALKBACK);

    String communicatorToken = Communicator.Instance().getTravellerFirebaseToken(trip, tripLink);
    String communicatorURI = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.FIREBASE_URI);
    return ok(views.html.communicator.com.render(tripId, communicatorURI, communicatorToken, talkback));
  }

  public CompletionStage<Result> unreadMessageCount(String tId, String tripId) {
    StopWatch sw = new StopWatch();
    sw.start();
    if (tripId != null && tripId.contains("___")) {
      tripId = tripId.substring(0, tripId.indexOf("___"));
    }
    Promise               scalaPromise = Communicator.Instance().promiseUnreadCount(tId, tripId);
    CompletionStage<ObjectNode> futureJson   = scala.compat.java8.FutureConverters.toJava(scalaPromise.future());
    sw.stop();
    if(sw.getTime() >= 30000) {
      Log.err("unreadMessageCount(): Extra long time to get unread message count: " + sw.getTime() + " ms. " +
              "TripId: " + tripId + " Traveler: " + tId);
    } else {
      Log.debug("unreadMessageCount(): Prepared request in " + sw.getTime() + "ms.");
    }
    return futureJson.thenApplyAsync(on -> ok(on));
  }

  /*
   * add a msg for email digest of a user
   */
  public static void addMessage(RedisMgr redis, Account recipient, CommunicatorMsg msg) {
    if (recipient != null && msg != null) {
      if (redis.addToSet(APPConstants.CACHE_DIGEST_ACCOUNT_LIST_KEY, recipient.getUid().toString())) {
        if (!redis.appendToList(APPConstants.CACHE_DIGEST_MSGS_BY_ACCOUNT_PREFIX + recipient.getUid().toString(), msg)) {
          Log.err("Cannot set msg for account- " + recipient.getUid());
        }
      } else {
        Log.err("Cannot set account for digest- " + recipient.getUid());
      }
    }
  }



  @With({Authenticated.class, AdminAccess.class})
  public Result sendAgentDigest() {
    Set<String> values = redis.getSet(APPConstants.CACHE_DIGEST_ACCOUNT_LIST_KEY);
    int agentCount = 0;
    int travelerCount = 0;
    if (values != null) {
      for (String id: values) {
        if (StringUtils.isNumber(id)) {
          travelerCount++;
        } else {
          agentCount++;
        }
      }
    }

    CommunicatorDigestActor.Protocol protocol = CommunicatorDigestActor.Protocol.build(
            CommunicatorDigestActor.Protocol.Command.AGENT_DIGEST);
    ActorsHelper.tell(SupervisorActor.UmappedActor.COMMUNICATOR_DIGEST, protocol);

    BaseView view = new BaseView();
    view.message = "Digest triggered for " + agentCount + " agents and " + travelerCount + " travelers";
    return ok(views.html.common.message.render(view));

  }

  @With({Authenticated.class, AdminAccess.class})
  public Result cleanRedisDigest() {
    int agentCount = 0;
    int travelerCount = 0;
    Set<String> values = redis.getSet(APPConstants.CACHE_DIGEST_ACCOUNT_LIST_KEY);
    if (values != null && values.size() > 0) {
      for (String accountId:values) {
        if (StringUtils.isNumber(accountId)) {
          travelerCount++;
        } else {
          agentCount++;
        }
        redis.delKey(APPConstants.CACHE_DIGEST_MSGS_BY_ACCOUNT_PREFIX + accountId);
        redis.removeFromSet(APPConstants.CACHE_DIGEST_ACCOUNT_LIST_KEY, accountId);
      }
    }
    BaseView view = new BaseView();
    view.message = "Deleted " + agentCount + " agents and " + travelerCount + " travelers";
    return ok(views.html.common.message.render(view));
  }
}
