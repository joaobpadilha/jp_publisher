package controllers.mobile;

/**
 * Created by wei on 2017-08-16.
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.mapped.publisher.actions.MobileAPIVersionCheck;
import com.mapped.publisher.actions.MobileSessionAuth;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.parse.schemaorg.Reservation;
import com.mapped.publisher.persistence.PoiTypeInfo;
import com.mapped.publisher.persistence.communicator.Communicator;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.S3Util;
import com.mapped.publisher.utils.Utils;
import com.umapped.mobile.api.*;
import com.umapped.mobile.api.model.*;
import controllers.PoiController;
import models.publisher.Account;
import models.publisher.AccountSession;
import models.publisher.Company;
import models.publisher.Trip;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.lang3.tuple.Pair;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import scala.concurrent.Promise;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by wei on 2017-06-28.
 */
public class MobileAPIController
    extends Controller {

  @Inject MobileAuthenticateService service;

  @Inject MobileItineraryService itineraryService;

  @Inject MobileCommunicatorService communicatorService;

  @Inject MobileWeatherService weatherService;

  @With(MobileAPIVersionCheck.class) public Result auth() {
    AuthenticationRequest request = getRequest(AuthenticationRequest.class);

    if (request == null) {
      return badRequest(successResponse(MobileAPIResponseCode.INVALID_REQUEST, "Not AuthenticationRequest"));
    }

    if (request.getAppDevice() == null) {
      return badRequest(successResponse(MobileAPIResponseCode.REQUEST_MISSING_DATA, "Missing AppDevice Info"));
    }

    try {
      AuthenticationResponse response = service.authenticate(request.getEmail(),
                                                             request.getPassword(),
                                                             request.getPnToken(),
                                                             request.getAppDevice());

      return ok(successResponse(response));
    }
    catch (Exception e) {
      Log.err("Unexpected authentication error", e);
      return ok(successResponse(MobileAPIResponseCode.UNEXPECTED_ERROR, "Fail to authenticate"));
    }
  }

  @With(MobileAPIVersionCheck.class) public Result register() {
    RegistrationRequest request = getRequest(RegistrationRequest.class);

    if (request != null) {
      try {
        RegistrationResponse response = service.register(request, request.getPnToken(), request.getAppDevice());

        return ok(successResponse(response));
      }
      catch (Exception e) {
        Log.err("Unexpected authentication error", e);
        return ok(successResponse(MobileAPIResponseCode.UNEXPECTED_ERROR, "Fail to register"));
      }
    }
    return badRequest();
  }


  @With({MobileAPIVersionCheck.class, MobileSessionAuth.class}) public Result logout() {
    AccountSession session = (AccountSession) ctx().args.get("AccountSession");
    session.delete();
    return ok(successResponse(null));
  }

  @With({MobileAPIVersionCheck.class, MobileSessionAuth.class}) public Result getAccountFirebaseToken() {
    AccountSession session = (AccountSession) ctx().args.get("AccountSession");
    FirebaseUserToken token = null;
    try {
      if (session != null) {
        Account account = Account.find.byId(session.getAccountUid());
        if (account != null) {
          //&& Account.AccountType.PUBLISHER.equals(account.getAccountType())) {
          token = MobileFirebaseService.getIntance().createToken(session.getAccountUid());
        }
      }
    } catch (Exception e) {
      Log.err("Fail to get the token", e);
    }
    if (token != null) {
      return ok(successResponse(token));
    } else {
      return ok(successResponse(MobileAPIResponseCode.UNEXPECTED_ERROR, "Fail to get token"));
    }
  }

  @With({MobileAPIVersionCheck.class, MobileSessionAuth.class}) public Result getTripFirebaseToken(String tripId) {
    AccountSession session = (AccountSession) ctx().args.get("AccountSession");
    String token = null;
    try {
      Account account = Account.find.byId(session.getAccountUid());
      if (account != null && Account.AccountType.TRAVELER.equals(account.getAccountType())) {
        token = MobileFirebaseService.getIntance().createToken(session.getAccountUid(), tripId);
      }
    } catch (Exception e) {
      Log.err("Fail to get the token", e);
    }
    if (token != null) {
      return ok(successResponse(token));
    } else {
      return ok(successResponse(MobileAPIResponseCode.UNEXPECTED_ERROR, "Fail to get token"));
    }
  }

  @With({MobileAPIVersionCheck.class, MobileSessionAuth.class}) public Result getUpdatedItineraries(Long lastSyncTs) {
    AccountSession session = (AccountSession) ctx().args.get("AccountSession");
    GetItinerariesRequest request = new GetItinerariesRequest();
    request.setAccountId(session.getAccountUid());
    request.setLastSyncTimestamp(lastSyncTs);
    GetItinerariesResponse response = itineraryService.getItineraries(request);

    session.setModifiedTs(lastSyncTs);
    session.setSyncTs(lastSyncTs);
    session.save();
    return ok(successResponse(response));
  }

  //@With(AccountJwtAuth.class)
  public CompletionStage<Result> unreadMessageCount(Long accountId, String tripId) {
    StopWatch sw = new StopWatch();
    sw.start();
    if (tripId != null && tripId.contains("___")) {
      tripId = tripId.substring(0, tripId.indexOf("___"));
    }

    Pair<String, Account.AccountType> userTrip = communicatorService.getUserTrip(Account.find.byId(accountId),
                                                                                 Trip.findByPK(tripId));
    if (userTrip != null) {
      Promise scalaPromise = Communicator.Instance().promiseUnreadCount(userTrip.getLeft(), tripId);
      CompletionStage<ObjectNode> futureJson = scala.compat.java8.FutureConverters.toJava(scalaPromise.future());
      sw.stop();
      if (sw.getTime() >= 30000) {
        Log.err("unreadMessageCount(): Extra long time to get unread message count: " + sw.getTime() + " ms. " +
                "TripId: " + tripId + " Traveler: " + accountId);

      }
      else {
        Log.debug("unreadMessageCount(): Prepared request in " + sw.getTime() + "ms.");
      }
      return futureJson.thenApplyAsync(on -> ok(on));
    }
    else {
      ObjectMapper mapper = new ObjectMapper();
      ObjectNode on = mapper.createObjectNode();
      on.put("msgCount", 0);
      on.put("notifyCount", 0);
      return CompletableFuture.completedFuture(ok(successResponse(on)));
    }
  }

  //@With(AccountJwtAuth.class)
  public Result messenger(Long accountId, String tripId) {
    Pair<String, Boolean> setting = communicatorService.getCommunicatorSetting(accountId, tripId);
    if (setting == null) {
      return notFound();
    }
    String communicatorURI = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.FIREBASE_URI);
    return ok(views.html.communicator.com.render(tripId, communicatorURI, setting.getLeft(), setting.getRight()));
  }


  @With({MobileAPIVersionCheck.class, MobileSessionAuth.class}) public Result inviteUser() {
    AccountSession session = (AccountSession) ctx().args.get("AccountSession");
    InviteUserRequest request = getRequest(InviteUserRequest.class);
    if (request == null) {
      return badRequest();
    }

    try {
      InviteUserResponse response = itineraryService.inviteUser(request, session.getAccountUid());
      return ok(Json.toJson(response));
    }
    catch (Exception e) {
      return ok(successResponse(MobileAPIResponseCode.UNEXPECTED_ERROR, "Fail to invite new traveller"));
    }
  }

  //@With({MobileAPIVersionCheck.class, MobileSessionAuth.class})
  public Result getTripAnalysis(String tripId) {
    try {
      List<TripSegmentData> segmentDataList = itineraryService.getTripSegments(tripId);
      return ok(successResponse(new GetTripAnalysisResponse(segmentDataList)));
    }
    catch (Exception e) {
      Log.err("Fail to get trip analysis", e);
      return ok(successResponse(MobileAPIResponseCode.UNEXPECTED_ERROR, "Fail to get trip analysis result"));
    }
  }


  @With({MobileAPIVersionCheck.class})
  public Result poiAutocomplete(String type, String term) {
    try {
      Set<Integer> searchCmpyIds = new HashSet<>();
      searchCmpyIds.add(Company.PUBLIC_COMPANY_ID);
      PoiTypeInfo.PoiType pt = PoiTypeInfo.Instance().byName(type);
      List<Integer> searchTypes = new ArrayList<>(1);
      if (pt != null) {
        searchTypes.add(pt.getId());
      }
      ArrayNode data = PoiController.autocompleteHelper(term, searchTypes, searchCmpyIds);
      return ok(successResponse(data));
    } catch (Exception e) {
      Log.err("Fail to get POIs", e);
      return ok(successResponse(MobileAPIResponseCode.UNEXPECTED_ERROR, "Fail to get Pois"));
    }
  }

  @With({MobileAPIVersionCheck.class, MobileSessionAuth.class})
  public Result editReservation(String tripId) {
    AccountSession session = (AccountSession) ctx().args.get("AccountSession");

    Account account = Account.find.byId(session.getAccountUid());

    Pair<MobileAPIResponseCode, Itinerary> result = itineraryService.editReservation(account, tripId, request().body().asJson());
    if (result.getRight() != null) {
      return ok(successResponse(result.getRight()));
    } else {
      return ok(successResponse(result.getLeft(), null));
    }
  }

  @With({MobileAPIVersionCheck.class, MobileSessionAuth.class})
  public Result deleteReservation(String tripId, String reservationId) {
    AccountSession session = (AccountSession) ctx().args.get("AccountSession");
    Account account = Account.find.byId(session.getAccountUid());
    Pair<MobileAPIResponseCode, Itinerary> result = itineraryService.deleteReservation(account, tripId, reservationId);
    if (result.getRight() != null) {
      return ok(successResponse(result.getRight()));
    } else {
      return ok(successResponse(result.getLeft(), null));
    }
  }

  @With({MobileAPIVersionCheck.class, MobileSessionAuth.class})
  public Result s3Authorization() {
    S3AuthorizationRequest authRequest = getRequest(S3AuthorizationRequest.class);
    List<S3AuthorizationResponse.S3Authorization> authList = new ArrayList<>();
    for (String fileName: authRequest.getFileNames()) {
      S3AuthorizationResponse.S3Authorization auth = new S3AuthorizationResponse.S3Authorization();
      auth.name = fileName;
      auth.getUrl = Utils.authorizeS3Get(fileName);
      auth.putUrl = S3Util.authorizeS3Put(fileName);
      authList.add(auth);
    }
    return ok(successResponse(authList));
  }


  protected <T> T getRequest(Class<T> requestClass) {
    JsonNode requestBody = request().body().asJson();
    return getObjectMapper().convertValue(requestBody, requestClass);
  }

  public CompletionStage<Result> weatherForecast(String location) {
    return weatherService.getForcast(location).thenApply((forecast) -> {
      if (forecast == null) {
        return ok(successResponse(MobileAPIResponseCode.SUCCESS, null));
      }
      else {
        return ok(successResponse(forecast));
      }
    });
  }

  private ObjectMapper getObjectMapper() {
    return Json.mapper();
  }

  private JsonNode successResponse(Object payload) {
    return Json.toJson(new MobileAPIResponse(MobileAPIResponseCode.SUCCESS, null, payload));
  }

  private JsonNode successResponse(MobileAPIResponseCode code, String message) {
    return Json.toJson(new MobileAPIResponse(code, message));
  }
}
