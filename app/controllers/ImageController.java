package controllers;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.Status;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.ExpressionList;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.audit.AuditActionType;
import com.mapped.publisher.audit.AuditActorType;
import com.mapped.publisher.audit.AuditModuleType;
import com.mapped.publisher.audit.event.AuditTripDocCustom;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.ImageForm;
import com.mapped.publisher.js.BaseJsonResponse;
import com.mapped.publisher.persistence.*;
import com.mapped.publisher.persistence.redis.RedisKeys;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.utils.*;
import com.mapped.publisher.view.ImageResultsView;
import com.mapped.publisher.view.ImageView;
import com.mapped.publisher.view.UserMessage;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.common.BkApiSrcIdConstants;
import com.umapped.external.bing.image.BingImage;
import com.umapped.external.bing.image.Value;
import models.publisher.*;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import play.data.Form;
import play.data.FormFactory;
import play.data.validation.Constraints;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static com.mapped.publisher.persistence.RecordState.PENDING;
import static com.mapped.publisher.persistence.redis.RedisKeys.K_IMG_UPLOAD_MODEL;
import static com.mapped.publisher.persistence.redis.RedisKeys.K_IMG_UPLOAD_REDIRECT;
import static com.mapped.publisher.utils.S3Util.BUCKET_NAME;
import static models.publisher.FileImageMigrate.ImageUser.*;

// This sample uses the Apache HTTP client from HTTP Components

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-04-09
 * Time: 10:14 AM
 * To change this template use File | Settings | File Templates.
 */
public class ImageController
    extends Controller {

  private static final boolean DEBUG = false;

  @Inject
  private RedisMgr redis;

  private HttpExecutionContext ec;

  @Inject
  FormFactory formFactory;

  @Inject
  public ImageController(HttpExecutionContext ec) {
    this.ec = ec;
  }


  @With({Authenticated.class, Authorized.class})
  public CompletionStage<Result> getImages() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<ImageForm> imgForm = formFactory.form(ImageForm.class);
    final ImageForm imgInfo = imgForm.bindFromRequest().get();
    final ImageResultsView view = buildView(imgInfo);

    if (imgInfo == null || imgInfo.getInCmpyId() == null ||
        imgInfo.getInCmpyId().trim().length() == 0 ||
        ((imgInfo.getInKeyword() == null ||
          imgInfo.getInKeyword().trim().length() == 0) &&(imgInfo.getInWebUrl() == null ||
                                                          imgInfo.getInWebUrl().trim().length() < 3 ||
                                                          !imgInfo.getInWebUrl().contains(".")) )) {
      view.message = "A System Error has occurred - please try again";
      return CompletableFuture.completedFuture(ok(views.html.img.imgSearch.render(view)));
    }

    final ArrayList<String> cmpyList = new ArrayList<>();
    cmpyList.add(imgInfo.getInCmpyId());

    if (sessionMgr.getCredentials().isUmappedAdmin()) {
      cmpyList.add(APPConstants.DEFAULT_CMPY_ID);
    }

    if (cmpyList != null && cmpyList.size() > 0) {

      if (imgInfo.getInImgSearchType() == ImageLibrary.ImgSrc.WEB) {

        if (imgInfo.getInWebUrl() == null ||
            imgInfo.getInWebUrl().trim().length() < 3 ||
            !imgInfo.getInWebUrl().contains(".")) {
          view.message = "Website URL is invalid - please try again";
          return CompletableFuture.completedFuture(ok(views.html.img.imgSearch.render(view)));
        }

        CompletionStage<ImageResultsView> promiseSearchResults =  CompletableFuture.supplyAsync(() -> {

          ImageResultsView asyncView = buildView(imgInfo);
          try {

            List<ImageView> webresults = searchWeb(imgInfo.getInKeyword(), imgInfo.getInWebUrl());
            if (webresults != null) {
              asyncView.searchResults.put("Search Results - " + imgInfo.getInWebUrl(), webresults);
            }

            //return PDF.toBytes(views.html.external
            // .icbellagio.document.render(destView));
          }
          catch (Exception e) {
            e.printStackTrace();
          }
          return asyncView;
        }, ec.current());
        return promiseSearchResults.thenApplyAsync(imageResultsView -> ok(views.html.img.imgSearch.render(imageResultsView)));
      }
      else {

        CompletionStage<ImageResultsView> promiseSearchResults =  CompletableFuture.supplyAsync(() -> {
          ImageResultsView asyncView = buildView(imgInfo);
          try {
            List<ImageView> umappedResults = searchUMapped(imgInfo.getInKeyword(), cmpyList);
            if (umappedResults != null) {
              asyncView.umappedResults = umappedResults;
            }
            List<ImageView> wikiresults = searchWeb(imgInfo.getInKeyword(), APPConstants.WIKIMEDIA_URL);
            if (wikiresults != null) {
              asyncView.searchResults.put("WikiMedia Image Library", wikiresults);
            }

            //return PDF.toBytes(views.html.external
            // .icbellagio.document.render(destView));
          }
          catch (Exception e) {
            e.printStackTrace();
          }
          return asyncView;
        }, ec.current());


        return promiseSearchResults.thenApplyAsync(imageResultsView -> ok(views.html.img.imgSearch.render(imageResultsView)));
      }
    }
    else {
      view.message = "UnAuthorized access.";
      return CompletableFuture.completedFuture(ok(views.html.img.imgSearch.render(view)));
    }
  }

  public enum CropUse {
    TRIP_COVER,
    DOC_COVER
  }

  public static class FormImageCropper {
    /**
     * File ID to be modified
     */
    @Constraints.Required
    public Long fileId;
    @Constraints.Required
    public CropUse useCase;
    @Constraints.Required
    public String useId;
    public Integer width;
    public Integer height;
  }

  @With({Authenticated.class, Authorized.class})
  public Result imageCropperRestore() {
    Form<FormImageCropper> form = formFactory.form(FormImageCropper.class).bindFromRequest();
    if (form.hasErrors()) {
      return BaseJsonResponse.formError(form);
    }

    FormImageCropper cropInfo  = form.get();
    FileImage        fileImage = FileImage.find.byId(cropInfo.fileId);
    //Currently supporting only one type - 16x9 crop
    if (fileImage.getType() != FileImage.ImageType.WIDE_CROP_16x9) {
      return BaseJsonResponse.codeResponse(ReturnCode.LOGICAL_ERROR, "Unsupported crop restore size");
    }

    if (fileImage.getParent() == null) {
      return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "No original to restore to");
    }

    FileImage original = FileImage.find.byId(fileImage.getParent().getPk());
    switch (cropInfo.useCase) {
      case TRIP_COVER:
        Trip trip = Trip.findByPK(cropInfo.useId);
        trip.setFileImage(original);
        trip.setCoverurl(original.getUrl());
        trip.setCoverfilename(original.getFilename());
        trip.save();
        break;
      case DOC_COVER:
        Destination destination = Destination.find.byId(cropInfo.useId);
        destination.setFileImage(original);
        destination.setCoverurl(original.getUrl());
        destination.setCovername(original.getFilename());
        destination.update();
        break;
      default:
        return BaseJsonResponse.codeResponse(ReturnCode.NOT_SUPPORTED, "Unsupported use case");
    }

    return BaseJsonResponse.codeResponse(ReturnCode.SUCCESS);
  }

  @With({Authenticated.class, Authorized.class})
  public Result imageCropperSave() {
    Form<FormImageCropper> form = formFactory.form(FormImageCropper.class).bindFromRequest();
    if (form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    FormImageCropper cropInfo   = form.get();
    SessionMgr       sessionMgr = SessionMgr.fromContext(ctx());
    Account          account    = sessionMgr.getCredentials().getAccount();

    String imgName = null;
    try {
      FileImage original = FileImage.find.byId(cropInfo.fileId);
      FileInfo  origFile = original.getFile();

      if (original == null) {
        Log.err("ImageController::imageCropperSave: Didn't find image with ID: " + cropInfo.fileId);
        return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "Image not found");
      }

      if (original.getType() == FileImage.ImageType.WIDE_CROP_16x9) {
        Log.err("ImageController::imageCropperSave: Can't crop previously cropped images: " + cropInfo.fileId);
        return BaseJsonResponse.codeResponse(ReturnCode.LOGICAL_ERROR, "Image is already cropped");
      }

      List<FileImage> cropped      = FileImage.byTypeOfParent(cropInfo.fileId, FileImage.ImageType.WIDE_CROP_16x9);
      FileImage       newImageCrop = null;
      FileInfo        newFileCrop  = null;
      if (cropped != null) {
        for (FileImage fi : cropped) {
          //If already have crop from this user - then replacing it
          if (fi.getModifiedBy().equals(account.getUid())) {
            newImageCrop = fi;
            newFileCrop = fi.getFile();
          }
        }
      }

      if (newImageCrop == null || newFileCrop == null) {
        newImageCrop = FileImage.build(account, origFile.getSource(), 0);
        newImageCrop.setParent(origFile);
        newImageCrop.setType(FileImage.ImageType.WIDE_CROP_16x9);
        newImageCrop.setProcessed(true);
        newImageCrop.setSrcUrl(original.getSrcUrl());
        newImageCrop.setOrientation(FileImage.Orientation.LANDSCAPE);
        newImageCrop.setSearchTerm(original.getSearchTerm());

        newFileCrop = newImageCrop.getFile();
        newFileCrop.setFilename(origFile.getFilename());
        newFileCrop.setDescription(origFile.getDescription());
        newFileCrop.setSearchWords(origFile.getSearchWords());
        newFileCrop.setState(PENDING);
      }
      newFileCrop.setFiletype(LstFileType.find.byId("jpeg")); //Cropped image always arrives as jpeg
      newFileCrop.setFilename(ImageHelper.switchExtention(newFileCrop.getFilename(), "jpg"));
      String normalized = S3Util.normalizeFilename(origFile.getFilename());
      normalized = ImageHelper.switchExtention(normalized, "jpg");
      newFileCrop.setFilepath(ImageHelper.getImageKey(newImageCrop, normalized, account));
      newImageCrop.setWidth(cropInfo.width);
      newImageCrop.setHeight(cropInfo.height);
      newFileCrop.setAsPublicUrl();

      Http.MultipartFormData          body = request().body().asMultipartFormData();
      Http.MultipartFormData.FilePart<File> file = body.getFile("croppedImage");
      File                            img  = file.getFile();

      //If some kind of error - then return before saving anything
      if (!img.exists() || !img.canRead()) {
        Log.err("imageCropperSave: Error saving cropped image. Image does not exist or can't be read.");
        return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_FAIL, "Image data is missing in the request");
      }

      S3Util.S3FileInfo s3Info = S3Util.S3FileInfo.buildInfo(newFileCrop.getBucket(),
                                                             newFileCrop.getFilepath(),
                                                             img,
                                                             newFileCrop.getFiletype().getProperMimeType());
      s3Info.isCacheControlPublic = true;
      s3Info.cacheControlTimeout = TimeUnit.DAYS.toSeconds(365);

      if (S3Util.doesFileexist(newFileCrop.getBucket(), newFileCrop.getFilepath())) {
        Log.debug("ImageController::imageCropperSave: replaced existing cropped file");
      }

      if (S3Util.uploadS3Data(s3Info)) {
        Log.info("ImageController::imageCropperSave: Successfully uploaded cropped image data to as url:" +
                 newFileCrop.getUrl());

        //Making file public
        if (S3Util.setObjectPublic(newFileCrop.getBucket(), newFileCrop.getFilepath())) {
          Log.info("ImageController::imageCropperSave: Successfully made cropped image URL public:" + newFileCrop
              .getUrl());
        }
        else {
          Log.err("ImageController::imageCropperSave:  Failed to make url public. Filepath: " + newFileCrop
              .getFilepath());
        }

        updateImageMetadata(newFileCrop);
        newImageCrop.superSave();

        switch (cropInfo.useCase) {
          case TRIP_COVER:
            Trip trip = Trip.findByPK(cropInfo.useId);
            trip.setFileImage(newImageCrop);
            trip.setCoverurl(newImageCrop.getUrl());
            trip.setCoverfilename(newImageCrop.getFilename());
            trip.update();
            break;
          case DOC_COVER:
            Destination destination = Destination.find.byId(cropInfo.useId);
            destination.setFileImage(newImageCrop);
            destination.setCoverurl(newImageCrop.getUrl());
            destination.setCovername(newImageCrop.getFilename());
            destination.update();
            break;
          default:
            return BaseJsonResponse.codeResponse(ReturnCode.NOT_SUPPORTED, "Unsupported use case");
        }
        return BaseJsonResponse.codeResponse(ReturnCode.SUCCESS);
      }
      else {
        Log.err("ImageController::imageCropperSave:  Failed to upload cropped image data");
        return BaseJsonResponse.codeResponse(ReturnCode.STORAGE_S3_UPLOAD, "Error saving cropped image.");
      }
    }
    catch (Exception e) {
      Log.err("imageCropperSave: Error saving cropped image.", e);
      return BaseJsonResponse.codeResponse(ReturnCode.UNHANDLED_EXCEPTION, "Error saving cropped image.");
    }
  }


  private static String getFileName(String imgName) {
    String s3Bucket = BUCKET_NAME;
    String fileName = null;
    if (imgName.contains("://"+s3Bucket)) {
      fileName = imgName.substring(imgName.indexOf(".com/") + 5, imgName.indexOf("?"));
    } else if (imgName.contains("/"+s3Bucket+"/")) {
      fileName = imgName.substring(imgName.indexOf("/"+s3Bucket+"/") + (s3Bucket.length()+2), imgName.indexOf("?"));
    }
    return fileName;
  }

  private static ImageResultsView buildView(ImageForm imgInfo) {
    ImageResultsView view = new ImageResultsView();
    view.searchResults = new HashMap<>();
    if (imgInfo != null &&
        imgInfo.getInImgSearchType() == ImageLibrary.ImgSrc.WEB) {
      view.searchType = ImageLibrary.ImgSrc.WEB;
    }
    else {
      view.searchType = ImageLibrary.ImgSrc.UMAPPED;
    }
    view.cmpyId = imgInfo.getInCmpyId();
    view.keyword = imgInfo.getInKeyword();
    view.tripId = imgInfo.getInTripId();
    view.docId = imgInfo.getInDocId();
    view.pageId = imgInfo.getInPageId();
    view.vendorId = imgInfo.getInVendorId();
    view.templateId = imgInfo.getInTemplateId();
    view.tripNoteId = imgInfo.getInNoteId();
    view.multipleFiles = imgInfo.getInMultipleFiles();
    return view;
  }

  private static List<ImageView> searchWeb(String keyword, String webUrl) {

    List<ImageView> results = new ArrayList<>();

    if (keyword == null || keyword.trim().length() == 0) {
      //no keyword - try to just return the passed in url
      ImageView view = new ImageView();
      view.url = webUrl;
      view.title = null;
      view.licenceUrl = null;
      view.imgSrc = webUrl;
      view.height = 0;
      view.width = 0;

      if ((webUrl.lastIndexOf("/") + 1) < webUrl.length()) {
        view.fileName = webUrl.substring(webUrl.lastIndexOf("/") + 1);
      }

      view.thumbUrl = webUrl;
      results.add(view);
    } else {
      String searchParam = "";
      if (webUrl !=  null && webUrl.length() > 4) {
        searchParam = "site%3A"+webUrl+"%20"+keyword.replaceAll(" ", "%20");
      }
      else {
        searchParam = keyword.replaceAll(" ", "%20");
      }

      HttpClient client = HttpClientBuilder.create().build();
      String url = "https://api.cognitive.microsoft.com/bing/v5.0/images/search?q="+searchParam+"&offset=0&mkt=en-us&safeSearch=Moderate";

      HttpGet get = new HttpGet(url);
      get.addHeader("accept", "application/json");
      get.setHeader("Content-Type", "multipart/form-data");
      get.setHeader("Ocp-Apim-Subscription-Key", APPConstants.BING_KEY_2);

      HttpResponse r = null;
      try {
        r = client.execute(get);
      } catch (IOException e) {
        e.printStackTrace();
      }

      HttpEntity entity = r.getEntity();
      BingImage details = null;
      Log.log(LogLevel.INFO, "BING STEP 1");
      if (r != null && r.getStatusLine() != null && r.getStatusLine().getStatusCode() == 200) {
        Log.log(LogLevel.INFO, "BING STEP 2");

        try {
          String resp = EntityUtils.toString(entity);
          ObjectMapper mapper = new ObjectMapper();

          com.fasterxml.jackson.databind.ObjectReader reader = mapper.reader(BingImage.class);
          details = reader.readValue(resp);

          for (Value v : details.getValue()) {
            String imgUrl = getDirectUrl(v.getContentUrl());



            ImageView view = new ImageView();
            view.url = imgUrl;
            view.thumbUrl = v.getContentUrl();
            view.title = v.getName();
            view.licenceUrl = v.getHostPageDisplayUrl();
            view.imgSrc = v.getHostPageUrl();
            view.height = v.getHeight();
            view.width = v.getWidth();
            if (imgUrl != null && !imgUrl.equals(v.getContentUrl())) {
              view.fileName = imgUrl.substring(imgUrl.lastIndexOf("/") + 1);
            }
            if (view.fileName == null || !view.fileName.contains(".") || view.fileName.length() < 5) {
              String fileid = Utils.getUniqueId();
              String extension = ".jpg";
              if (v.getEncodingFormat() != null) {
                switch (v.getEncodingFormat().toLowerCase()) {
                  case  "bitmap":
                    extension = ".bmp";
                    break;
                  case "png":
                    extension = ".png";
                    break;
                  case "tiff":
                    extension = ".tiff";
                    break;
                  case "gif":
                    extension = ".gif";
                    break;
                  default:
                    extension = ".jpg";
                }
              }
              view.fileName = fileid + extension;
            }

            if (v.getThumbnailUrl() != null) {
              view.thumbUrl = v.getThumbnailUrl();
            }
            else {
              view.thumbUrl = view.url;

            }

            if (webUrl.equals(APPConstants.WIKIMEDIA_URL)) {
              // if the image is too big, try to get the scaled image
              if (v.getHeight() > 1024 || v.getWidth() > 1024) {
                //try to get the scaled image from wikipedia
                String s = v.getContentUrl();
                if (s.contains("/commons/") && !s.contains("/commons/thumb/")) {
                  s = s.replace("/commons/", "/commons/thumb/");
                  if ((s.lastIndexOf("/") + 1) < s.length()) {
                    String fileName = s.substring(s.lastIndexOf("/") + 1);
                    if (v.getWidth() > v.getHeight()) {
                      //portrait
                      fileName = "1024px-" + fileName;
                    }
                    else {
                      //landscape
                      fileName = "768px-" + fileName;
                    }
                    s = s + "/" + fileName;
                    view.url = s;
                  }
                }
              }
            }

            results.add(view);


          }
        }
        catch (Exception e) {
          Log.log(LogLevel.ERROR, "BING SEARCH ERROR - Cannot read JSON from response: " , e);
          e.printStackTrace();
        }

      } else {
        Log.log(LogLevel.INFO, "BING STEP 5");

        Log.log(LogLevel.INFO, "BING SEARCH ERROR: " + r.getStatusLine().getStatusCode() + " " + r.getStatusLine().getReasonPhrase());
      }
    }
    return results;
  }

  private static List<ImageView> searchS3BucketImage(String keyword, String s3Bucket, String s3Key, String s3Pwd, String s3Prefix, List<String> cmpyIds) {
    List<ImageView> results = new ArrayList<>();

    if (keyword != null && keyword.trim().length() > 0) {
      String[] keywords1;

      if (keyword.contains("+")) {
        keywords1 = keyword.trim().toLowerCase().split("\\+");
      } else {
        keywords1 = keyword.trim().toLowerCase().split(" ");
      }
      List<S3ImgIndex> images = S3ImgIndex.findByFilenameKeywords(s3Bucket, keywords1);

      if(images != null && images.size() > 0) {
        for(S3ImgIndex img: images) {
          ImageView view = new ImageView();
          String fileName = "https://s3.amazonaws.com/" + img.getBucket() + "/" + img.getFileNameOrig();
          view.url = fileName;
          view.title = img.getFileNameOrig();
          view.imgSrc = fileName;
          view.fileName = img.getFileNameOrig();
          view.thumbUrl = fileName;

          results.add(0, view);
        }
      }
    }

    return results;
  }

  private static List<ImageView> searchUMapped(String keyword, List<String> cmpyIds) {
    List<ImageView> results = null;
    ApiCmpyToken cmpyToken = ApiCmpyToken.findCmpyTokenByType(cmpyIds.get(0), BkApiSrc.getId(BkApiSrcIdConstants.S3_IMG_SEARCH));

    if(cmpyToken != null) {
      String token = cmpyToken.getToken(); //userid:pwd@bucketname/prefixes with bucketname containing slashes and colon being delimiter between access key & secret key.  anything before 1st slash is bucketname.  anything after is prefix
      String s3Bucket = token.substring(token.indexOf("@") + 1, token.indexOf("/")).trim();
      String s3Key = token.substring(0, token.indexOf(":")).trim();
      String s3Pwd = token.substring(token.indexOf(":") + 1, token.indexOf("@")).trim();
      String s3Prefix = token.substring(token.indexOf("/") + 1, token.length()).trim();
      results = searchS3BucketImage(keyword, s3Bucket, s3Key, s3Pwd, s3Prefix, cmpyIds);
    }

    if(results == null || (results != null && results.size() == 0)) {
      Connection conn = null;
      results = new ArrayList<>();

      if (keyword != null && keyword.trim().length() > 0) {
        String[] keywords1;

        if (keyword.contains("+")) {
          keywords1 = keyword.trim().toUpperCase().split("\\+");
        } else {
          keywords1 = keyword.trim().toUpperCase().split(" ");
        }
        try {
          conn = DBConnectionMgr.getConnection4Publisher();
          HashMap<String, String> imgResults = ImgMgr.searchPhotos(keyword, cmpyIds, conn);
          if (imgResults != null) {
            for (String fileName : imgResults.keySet()) {
              String fileUrl = imgResults.get(fileName);
              String origFileName = fileName;

              if (origFileName.contains("_")) {
                int i = origFileName.indexOf("_");
                if (i != -1) {
                  origFileName = origFileName.substring(i + 1);
                }
              }

              ImageView view = new ImageView();
              view.url = fileUrl;
              view.title = origFileName;
              view.imgSrc = fileUrl;
              view.fileName = fileName;
              view.thumbUrl = fileUrl;

              if (view.title != null && keywords1 != null) {
                boolean found = false;
                for (String k : keywords1) {
                  if (view.title.toUpperCase().contains(k)) {
                    results.add(0, view);
                    found = true;
                    break;
                  }
                }
                if (!found) {
                  results.add(view);

                }
              } else {
                results.add(view);
              }
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        } finally {
          if (conn != null) {
            try {
              conn.close();
            } catch (SQLException e) {
              e.printStackTrace();
            }
          }
        }
      }
    }
    return results;
  }

  //POST save the image
  public CompletionStage<Result> saveImage() {
    final SessionMgr sessionMgr = new SessionMgr(session());
    Account account;
    if(sessionMgr.getAccount().isPresent()) {
      account = sessionMgr.getAccount().get();
    } else {
      account = Account.find.byId(sessionMgr.getAccountId());
    }

    //bind html form to form bean
    final Form<ImageForm> imgForm = formFactory.form(ImageForm.class);
    final ImageForm imgInfo = imgForm.bindFromRequest().get();
    if (imgInfo == null ||
        imgInfo.getInCmpyId() == null ||
        imgInfo.getInCmpyId().trim().length() == 0 ||
        imgInfo.getInFileName() == null ||
        imgInfo.getInFileName().trim().length() == 0 ||
        imgInfo.getInImgUrl() == null ||
        imgInfo.getInImgUrl().trim().length() == 0) {
      return CompletableFuture.completedFuture(unauthorized("A System Error has occurred - please try again"));
    }

    if (imgInfo.getInFileName() != null) {
      imgInfo.setInFileName(imgInfo.getInFileName()
                                   .replace(",", "")
                                   .replace("/", "")
                                   .replace("-_-", "")
                                   .replace("\\", "")
                                   .replace("&", "")
                                   .replace("#", "")
                                   .replace("%", "")
                                   .replace("*", ""));
    }

    final Company cmpy = Company.find.byId(imgInfo.getInCmpyId());

    int operationType = -1;

    if (imgInfo.getInTripId() != null && imgInfo.getInTripId().trim().length() > 0) {
      Trip tripModel = Trip.find.byId(imgInfo.getInTripId());
      if (SecurityMgr.getTripAccessLevel(tripModel, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND)) {
        operationType = APPConstants.ADD_TRIP_COVER;
      }
    }
    else if (imgInfo.getInDocId() != null && imgInfo.getInDocId().trim().length() > 0) {
      Destination doc = Destination.find.byId(imgInfo.getInDocId());
      if (doc != null && SecurityMgr.canEditDestination(doc, sessionMgr)) {
        if (imgInfo.getInPageId() != null && imgInfo.getInPageId().trim().length() > 0) {
          DestinationGuide page = DestinationGuide.find.byId(imgInfo.getInPageId());
          if (page != null && doc.getDestinationid().equals(page.getDestinationid())) {
            operationType = APPConstants.ADD_PAGE_IMG;
          }
        }
        else {
          operationType = APPConstants.ADD_DOC_COVER;
        }
      }
    }
    else if (imgInfo.getInVendorId() != null && imgInfo.getInVendorId().trim().length() > 0) {
      Long poiId = Long.parseLong(imgInfo.getInVendorId());
      PoiRS prs = PoiController.getMergedPoi(poiId, cmpy.getCmpyId());

      //Editing public company need to create a new POI
      if (prs.getCmpyId() == Company.PUBLIC_COMPANY_ID && !sessionMgr.getCredentials().isUmappedAdmin()) {
        prs = PoiController.createNewPoi(poiId, prs.getName(), cmpy.getCmpyId(), prs.getTypeId(),
                                         sessionMgr.getUserId());
      }

      if(SecurityMgr.canEditPoi(prs, sessionMgr)) {
        operationType = APPConstants.ADD_VENDOR_IMG;
      }
    }
    else if (imgInfo.getInTemplateId() != null && imgInfo.getInTemplateId().trim().length() > 0) {
      Template template = Template.find.byId(imgInfo.getInTemplateId());
      if (SecurityMgr.canEditTemplate(template, sessionMgr)) {
        operationType = APPConstants.ADD_TEMPLATE_COVER;
      }
    } else if (imgInfo.getInNoteId() != null && imgInfo.getInNoteId().longValue() > 0) {
      TripNote tripNote = TripNote.find.byId(imgInfo.getInNoteId());
      Trip trip = tripNote.getTrip();
      if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND) ||
          tripNote.getCreatedBy().equals(sessionMgr.getUserId())) {
        operationType = APPConstants.ADD_NOTE_IMG;
      }
    }

    if (operationType == -1) {
      return CompletableFuture.completedFuture(unauthorized("A System Error has occurred - please try again"));
    }

    final Integer opsType = new Integer(operationType);
    final String userId = new String(sessionMgr.getUserId());

    try {
      CompletionStage<FileImage> promiseSaveFile =  CompletableFuture.supplyAsync(() -> {
        String imgUrl = imgInfo.getInImgUrl();
        FileSrc.FileSrcType type = FileSrc.FileSrcType.IMG_USER_DOWNLOAD;
        if(imgUrl.contains("/common/") || imgUrl.contains("/commons/")) {
          type = FileSrc.FileSrcType.IMG_VENDOR_WIKIPEDIA;
        }

        return downloadAndSaveImage(imgUrl, imgInfo.getInFileName(), type, null, account, false );
      }, ec.current());


      return promiseSaveFile.thenApplyAsync(fileImage -> {
        if(fileImage == null){
          return unauthorized("A System Error has occurred - please try again");
        }

        ImageResultsView view = buildView(imgInfo);
        String fileName = Utils.escapeFileName(imgInfo.getInFileName());
        view.message = "Photo added successfully";

        switch (opsType) {
          case APPConstants.ADD_TRIP_COVER:
            Trip trip = Trip.find.byId(imgInfo.getInTripId());
            if(trip != null) {
              trip.setFileImage(fileImage);
              trip.setCoverfilename(fileImage.getFile().getFilename());
              trip.setCoverurl(fileImage.getUrl());
              trip.setLastupdatedtimestamp(System.currentTimeMillis());
              trip.setModifiedby(userId);
              trip.save();
            }
            break;
          case APPConstants.ADD_DOC_COVER:
            try {
              Ebean.beginTransaction();
              Destination destination = Destination.find.byId(imgInfo.getInDocId());
              destination.setFileImage(fileImage);
              destination.setCovername(fileImage.getFile().getFilename());
              destination.setCoverurl(fileImage.getUrl());
              destination.setLastupdatedtimestamp(System.currentTimeMillis());
              destination.setModifiedby(userId);
              destination.save();

              Ebean.commitTransaction();

              //Create audit log
              audit:
              {
                Trip                  tripModel   = null;
                List<TripDestination> tdests      = TripDestination.findActiveByDest(destination.getDestinationid());

                if (tdests != null && tdests.size() != 0) {
                  tripModel = Trip.find.byId(tdests.get(0).getTripid());
                }

                if (tripModel != null) {
                  TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_DOC_CUSTOM,
                                                       AuditActionType.MODIFY,
                                                       AuditActorType.WEB_USER)
                                          .withTrip(tripModel)
                                          .withCmpyid(tripModel.cmpyid)
                                          .withUserid(sessionMgr.getUserId());

                  ((AuditTripDocCustom) ta.getDetails()).withGuideId(destination.getDestinationid())
                                                        .withName(destination.getName())
                                                        .withCoverImage(destination.getCovername())
                                                        .withText("Cover Image Added");
                  ta.save();
                }
              }
              DestinationPostController.invalidateCache(destination.destinationid);
            }
            catch (Exception e) {
              e.printStackTrace();

              Ebean.rollbackTransaction();
              Log.err("Error saving file. Cmpy: " + imgInfo.getInCmpyId() + " File: " + imgInfo.getInImgUrl(), e);
            }
            break;
          case APPConstants.ADD_PAGE_IMG:

            try {
              Ebean.beginTransaction();

              DestinationGuideAttach attach = DestinationGuideAttach.buildGuideAttachment(imgInfo.getInPageId(),
                                                                                          userId);
              attach.setFileImage(fileImage);
              attach.setAttachtype(PageAttachType.PHOTO_LINK);
              //set rank
              int maxRow = DestinationGuideAttach.maxPageRankByDestGuideId(imgInfo.getInDocId(),
                                                                           attach.getAttachtypeStr());
              attach.setRank(++maxRow);
              attach.setAttachurl(fileImage.getUrl());
              attach.setName(fileName);
              attach.setAttachname(fileImage.getFile().getFilename());
              attach.setComments(imgInfo.getInPhotoCaption());
              attach.save();
              Ebean.commitTransaction();
              DestinationPostController.invalidateCache(imgInfo.getInDocId());
            }
            catch (Exception e) {
              e.printStackTrace();

              Ebean.rollbackTransaction();
              Log.err("Error saving file. Cmpy: " + imgInfo.getInCmpyId() + " File: " + imgInfo.getInImgUrl(), e);
            }
            break;

          case APPConstants.ADD_VENDOR_IMG:
            try {
              Ebean.beginTransaction();

              Long    poiId = Long.parseLong(imgInfo.getInVendorId());
              PoiFile pf    = null;
              // Find if file was previously deleted or person is trying to add the same file twice
              List<PoiFile> currFiles = PoiFile.getCmpyForPoi(poiId, cmpy.getCmpyId());
              for (PoiFile cpf : currFiles) {
                if (cpf.getFileImage() != null && cpf.getFileImage().getPk().equals(fileImage.getPk())) {
                  pf = cpf;
                  break;
                }
              }

              if (pf != null) { //Previously file existed
                pf.setState(RecordState.ACTIVE);
                pf.setDescription(imgInfo.getInPhotoCaption());
                pf.updateModInfo(userId);
                pf.setFileImage(fileImage);
                pf.update();
                PoiController.incrementFileCount(poiId, cmpy.getCmpyId(), userId);
              }
              else { //New record for this poi with this filename
                pf = PoiFile.buildPoiFile(poiId, cmpy.getCmpyId(), fileName, imgInfo.getInPhotoCaption(), userId);
                if (cmpy.getCmpyId() == Company.PUBLIC_COMPANY_ID && sessionMgr.getCredentials().isUmappedAdmin()) {
                  pf.setSrcId(FeedSourcesInfo.byName("System"));
                }
                else {
                  pf.setSrcId(FeedSourcesInfo.byName("Web"));
                }
                pf.setFileImage(fileImage);
                pf.setNameSys(S3Util.normalizeFilename(fileImage.getFile().getFilename()));
                pf.setUrl(fileImage.getUrl());
                pf.setExtension(fileImage.getFile().getFiletype());
                pf.save();
                PoiController.incrementFileCount(poiId, cmpy.getCmpyId(), userId);
              }

              Ebean.commitTransaction();
              //TODO: Invalidate photo cache once implemented
            }
            catch (Exception e) {
              Ebean.rollbackTransaction();
              Log.err("Error saving poi photo. Cmpy: " + imgInfo.getInCmpyId() + " File: " + imgInfo.getInImgUrl());
              e.printStackTrace();
            }
            break;
          case APPConstants.ADD_TEMPLATE_COVER:
            Template tmplt = Template.find.byId(imgInfo.getInTemplateId());
            tmplt.setFileImage(fileImage);
            tmplt.setLastupdatedtimestamp(System.currentTimeMillis());
            tmplt.setModifiedby(userId);
            tmplt.save();
            break;
          case APPConstants.ADD_NOTE_IMG:
            TripNote       tripNote = TripNote.find.byId(imgInfo.getInNoteId());
            TripNoteAttach attach   = TripNoteAttach.build(tripNote, userId);
            attach.setFileImage(fileImage);
            attach.setAttachType(PageAttachType.PHOTO_LINK);
            //set rank
            int maxRow = TripNoteAttach.maxPageRankByNoteId(imgInfo.getInNoteId(), attach.getAttachType().getStrVal());
            attach.setRank(++maxRow);
            attach.setAttachUrl(fileImage.getUrl());
            attach.setName(fileName);
            attach.setAttachName(fileImage.getFile().getFilename());
            attach.setComments(imgInfo.getInPhotoCaption());
            attach.save();
            BookingNoteController.invalidateCache(tripNote.getTrip().tripid);
            break;
        }

        return ok(views.html.common.message.render(view));
      }, ec.current());
    }
    catch (Exception e) {
      Log.err("Caught exception in Image Controller");
      e.printStackTrace();
    }
    return CompletableFuture.completedFuture( unauthorized("A System Error has occurred - please try again"));
  }


  public static final String USER_AGENT_IE11 = "Mozilla/5.0 (IE 11.0; Windows NT 6.3; Trident/7.0; .NET4.0E; " +
                                               ".NET4.0C; rv:11.0) like Gecko";

  public static FileImage downloadAndSaveImage(final String url,
                                               final String filename,
                                               final FileSrc.FileSrcType srcType,
                                               final String description,
                                               final Account a,
                                               boolean oneShot) {

    StopWatch sw = new StopWatch();
    sw.start();

    //Check if fileImage from this Url already exists, probably simplest verification
    FileImage fileImage = FileImage.bySourceUrl(url);
    if(fileImage != null) {
      return fileImage;
    }


    BufferedImage img = null;
    String headerContentType = null;
    try {
      HttpClient client = HttpClientBuilder.create().build();
      HttpGet get = new HttpGet(url);
      //set the user-agent to a mainstream one instead of the default Java one
      get.addHeader("User-Agent", USER_AGENT_IE11);
      HttpResponse r = client.execute(get);
      InputStream is = r.getEntity().getContent();

      img = ImageIO.read(is);
      is.close();
      Log.debug("ImageController::downloadAndSaveImage: Loaded and decoded image from url: " + url + " in: " + sw
          .getTime() + "ms");
      if (img == null && !oneShot) {
        //try to toggle to https/http as applicable
        String swapProtocolUrl = null;
        if (url.contains("http://")) {
          swapProtocolUrl = url.replace("http://", "https://");
        }
        else if (url.contains("https://")) {
          swapProtocolUrl = url.replace("https://", "http://");
        }
        return downloadAndSaveImage(swapProtocolUrl, filename, srcType, description, a, true);
      }
    }
    catch (MalformedURLException e) {
      Log.err("ImageController::downloadAndSaveImage: Malformed URL for url:" + url, e);
    }
    catch (IOException e) {
      Log.err("ImageController::downloadAndSaveImage: Download problem for url:" + url, e);
    }


    if(img == null) {
      return null; // No image for you
    }

    LstFileType fileType = LstFileType.findFromFileName(filename);
    String imgType = fileType.getExtension();
    if (imgType.equals("uki")) {
      fileType = LstFileType.byMine(headerContentType);
      if(fileType.getExtension().equals("uki")) {
        imgType = "jpg";
      } else {
        imgType = fileType.getExtension();
      }
    }
    FileSrc src = FileSrc.find.byId(srcType);
    String bucket = src.getRealBucketName();


    byte[] imageBytes = null;
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
      ImageIO.write(img, imgType, baos);
      baos.flush();
      img.flush(); //Releasing all memory here
      imageBytes = baos.toByteArray();
    }
    catch (IOException ioe) {
      Log.err("ImageController::downloadAndSaveImage: Image Data Problem for image from url: " + url, ioe);
      return null;
    }

    //Check if image is already in our database
    if(imageBytes == null || imageBytes.length == 0) {
      Log.err("ImageController::downloadAndSaveImage: Image Data Problem. No image data. Url: " + url);
      return null;
    }

    //Let's check if we have already have this image based on hash
    byte[] md5 = null;
    try {
      MessageDigest md = MessageDigest.getInstance("MD5");
      md5 = md.digest(imageBytes);
      FileInfo fi = FileInfo.byHashAndBucket(md5, bucket);
      if (fi != null) {
        fileImage = FileImage.find.byId(fi.getPk());
        if (fileImage == null) {
          fileImage = FileImage.build(a, src, 0); //Never expires
          fileImage.setFile(fi);
          fileImage.setSrcUrl(url);
          fileImage.setPublic(true);
          fileImage.setWidth(img.getWidth());
          fileImage.setHeight(img.getHeight());
          fileImage.setSearchTerm(description);
          fileImage.setType(FileImage.ImageType.ORIGINAL);
          fileImage.setOrientation(FileImage.Orientation.getOrientation(img.getWidth(), img.getHeight()));
          fileImage.save();
          //I guess we need to create a new file image if we have it
        }
        return fileImage;
      }
    }
    catch (NoSuchAlgorithmException e) {
      Log.err("ImageController::downloadAndSaveImage: MD5 Algorithm not found. Url: " + url, e);
    }

    fileImage = FileImage.build(a, src, 0);

    fileImage.setWidth(img.getWidth());
    fileImage.setHeight(img.getHeight());
    fileImage.setSearchTerm(description);
    fileImage.setType(FileImage.ImageType.ORIGINAL);
    fileImage.setPublic(true);
    fileImage.setSrcUrl(url);
    fileImage.setOrientation(FileImage.Orientation.getOrientation(img.getWidth(), img.getHeight()));

    FileInfo fileInfo = fileImage.getFile();

    //We are still here so it means we have brand new image that we want to save to S3
    String filenameNormalized = S3Util.normalizeFilename(filename);
    S3Util.S3FileInfo s3FileInfo = new S3Util.S3FileInfo();
    s3FileInfo.bucket = bucket;
    s3FileInfo.mime = fileType.getProperMimeType();
    s3FileInfo.size = imageBytes.length;
    s3FileInfo.binData = imageBytes;
    s3FileInfo.hash = md5;
    s3FileInfo.key = ImageHelper.getImageKey(fileImage, filenameNormalized, a);
    s3FileInfo.cacheControlTimeout = TimeUnit.DAYS.toSeconds(365);
    s3FileInfo.isCacheControlPublic = true;
    s3FileInfo.isCacheControlNoTransform = false;

    fileInfo.setBucket(bucket);
    fileInfo.setFilepath(s3FileInfo.key);
    fileInfo.setDescription(description);
    fileInfo.setFiletype(fileType);
    fileInfo.setFilename(filename);
    fileInfo.setAsPublicUrl(); //This is the last call before save!

    boolean res = S3Util.uploadS3Data(s3FileInfo);
    if(!res) {
      Log.err("ImageController::downloadAndSaveImage: Upload failed. Url: " + url);
      return null;
    }

    ObjectMetadata meta = S3Util.getMetadata(bucket, s3FileInfo.key);
    if (!fileInfo.updateFromMetadata(meta)) {
      Log.err("ImageController::downloadAndSaveImage: Updated file information from S3 failed: " +
                s3FileInfo.key);
      fileInfo.setHash(s3FileInfo.hash);
      fileInfo.setFilesize(s3FileInfo.size);
    }

    if (!S3Util.setObjectPublic(bucket, s3FileInfo.key)) {
      Log.err("ImageController::downloadAndSaveImage: Failed to set object public. Bucket: " + bucket +
              " Key: " +  s3FileInfo.key);
      //TODO: ???? Should we delete object that we have uploaded
      return  null;
    }

    fileImage.superSave();

    //Legacy logic to be removed in the future:
    String urlPK = Utils.hash(url);

    ImageLibrary il = ImageLibrary.findByPK(urlPK);
    if (il == null) {
      il = new ImageLibrary();
      il.setImgid(urlPK);
    };
    il.setFileImage(fileImage);

    il.setCreatedtimestamp(System.currentTimeMillis());
    il.setStatus(APPConstants.STATUS_ACTIVE);
    il.setImgsrc(ImageLibrary.ImgSrc.fromFileSrc(srcType));
    il.setOrigurl(url);
    il.setS3filename(filename);
    il.setSearchTerm(description);
    il.setS3url(fileInfo.getUrl());
    il.setChecksum(meta.getETag());

    il.save();
    sw.stop();
    Log.debug("ImageController::downloadAndSaveImage: Processed URL:" + url + " in " + sw.getTime() + "ms to S3 URL:"
              + fileInfo.getUrl());
    return fileImage;
  }

  public static FileImage uploadHelper(UmappedModelWithImage model, final String filename, Account account,
                                       ObjectNode on,  String nextUrl) {
    //1. Setting up temporary file record
    FileSrc   src = FileSrc.find.byId(FileSrc.FileSrcType.IMG_USER_UPLOAD);
    FileImage fi  = FileImage.buildPending(account, src);
    fi.setType(FileImage.ImageType.ORIGINAL);
    fi.setPublic(false);
    fi.setProcessed(false);
    String      normalized = S3Util.normalizeFilename(filename);
    LstFileType fileType   = LstFileType.findFromFileName(filename);
    FileInfo file = fi.getFile();
    file.setBucket(src.getRealBucketName());
    file.setFilepath(ImageHelper.getImageKey(fi, normalized, account));
    file.setFilename(filename);
    file.setFiletype(fileType);
    file.setAsPublicUrl();

    //2. Now preparing upload authorization response
    try {
      String signedPutUrl = S3Util.authorizeS3Put(file.getBucket(), file.getFilepath());
      S3Data s3Data = S3Util.authorizeS3(file.getBucket(), file.getFilepath());
      on.put("signed_request", signedPutUrl);
      on.put("url", routes.ImageController.uploadConfirmation(file.getPk()).url());
      on.put("policy", s3Data.policy);
      on.put("accessKey", s3Data.accessKey);
      on.put("key", s3Data.key);
      on.put("s3Bucket", s3Data.s3Bucket);
      on.put("signature", s3Data.signature);

    }catch (Exception e) {
      Log.err("ImageController::uploadHelper: Failure to authorize upload", e);
      return null;
    }

    fi.superSave();
    model.setFileImage(fi);
    model.save();

    //3. Storing URL to which we redirect after image upload is confirmed
    if(nextUrl != null) {
      CacheMgr.getRedis().set(RedisKeys.K_IMG_UPLOAD_REDIRECT, file.getPk().toString(), nextUrl, RedisMgr.WriteMode.BLOCKING);
    }
    CacheMgr.getRedis().set(K_IMG_UPLOAD_MODEL, file.getPk().toString(), model, RedisMgr.WriteMode.FAST);


    return fi;
  }

  @With({Authenticated.class, Authorized.class})
  public Result uploadConfirmation(Long fileId) {
    Optional<String> url = redis.get(RedisKeys.K_IMG_UPLOAD_REDIRECT, fileId.toString());

    if(!url.isPresent()) {
      return UserMessage.message(ReturnCode.LOGICAL_ERROR, "Image upload confirmation failure. (No Redirect).");
    }

    uploadConfirmationHelper(fileId);
    return redirect(url.get());
  }

  public static boolean uploadConfirmationHelper(Long fileId) {

    boolean result = true;
    RedisMgr redis = CacheMgr.getRedis();
    FileInfo file = FileInfo.find.byId(fileId);
    boolean res = updateImageMetadata(file);
    if(res) {

      //Checking for duplicates right here before saving and if needed replace objects in the model
      FileInfo dupFile = FileInfo.byHash(file.getHash());
      if(dupFile != null && !dupFile.getPk().equals(file.getPk())) {
        try {
          Ebean.beginTransaction();
          //Setting model to use existing image
          Optional<UmappedModelWithImage> modelOp = redis.getBin(K_IMG_UPLOAD_MODEL,
                                                                 fileId.toString(),
                                                                 UmappedModelWithImage.class);
          if (modelOp.isPresent()) {
            UmappedModelWithImage model = modelOp.get();
            model.refresh();
            FileImage prevImg = FileImage.find.byId(dupFile.getPk());
            model.setFileImage(prevImg);
            model.save();
          }

          //Now we can delete newly uploaded image
          S3Util.deleteS3File(file.getBucket(), file.getFilepath());

          //Finally deleting image record
          FileImage fileImage = FileImage.find.byId(file.getPk());
          fileImage.delete();
          file.delete();

          Ebean.commitTransaction();
        } catch (Exception e) {
          Ebean.rollbackTransaction();
          Log.err("ImageController::uploadConfirmation(): Failure, while deduplicating image", e);
          result = false;
        }
      } else {
        file.save();
      }
      flash(SessionConstants.SESSION_PARAM_MSG, "Image uploaded and confirmed successfully");
    } else {
      //We should clear model and file here
      flash(SessionConstants.SESSION_PARAM_MSG, "Image upload failed. File was not found the server.");
      //Marking as locked to clearly mark it as failed upload as locked is not used for anything else.
      file.setState(RecordState.LOCKED);
      Optional<UmappedModelWithImage> modelOp = redis.getBin(K_IMG_UPLOAD_MODEL,
                                                             fileId.toString(),
                                                             UmappedModelWithImage.class);
      if(modelOp.isPresent()) {
        UmappedModelWithImage model = modelOp.get();
        model.refresh();
        model.setFileImage(null);
        model.save();
      }
      result = false;
      file.save();
    }

    redis.delete(K_IMG_UPLOAD_MODEL, fileId.toString(), RedisMgr.WriteMode.UNSAFE);
    redis.delete(K_IMG_UPLOAD_REDIRECT, fileId.toString(), RedisMgr.WriteMode.UNSAFE);

    return result;
  }


  /**
   *
   * @return false if no image object was found at S3
   */
  public static boolean updateImageMetadata(FileInfo fi) {
    if(fi.getState() == RecordState.ACTIVE) {
      return true; //Already confirmed
    }

    ObjectMetadata meta = S3Util.getMetadata(fi.getBucket(), fi.getFilepath());
    if (!fi.updateFromMetadata(meta)) {
      Log.err("ImageController::confirmUpload: File Upload Failure! File not found on S3: " + fi.getFilepath());
      return  false;
    }

    if (!S3Util.setObjectPublic(fi.getBucket(), fi.getFilepath())) {
      Log.err("ImageController::updateImageMetadata: Failed to set uploaded object public. Bucket: " + fi.getBucket() +
              " Key: " +  fi.getFilepath());
    }

    //TODO: ? Should we stream file into the buffer here and check width and height?
    fi.setState(RecordState.ACTIVE);
    return true;
  }

  public static String getDirectUrl (String bingUrl)  {
    try {
      String tempUrl = URLDecoder.decode(bingUrl, "UTF-8");
      if (tempUrl.contains("&r=")) {
        tempUrl = tempUrl.substring(tempUrl.indexOf("&r=") + 3);
        if (tempUrl.contains("&p=")) {
          tempUrl = tempUrl.substring(0, tempUrl.indexOf("&p="));
        }
        URL u = new URL(tempUrl); // this would check for the protocol
        u.toURI();
        return tempUrl;
      }
    } catch (Exception e) {
    }
    return  bingUrl;
  }


  <T extends UmappedModelWithImage> void migrateTableGeneric(ExpressionList<T> el,
                                                             FileImageMigrate.ImageUser usedBy,
                                                             ActorRef sourceActor,
                                                             String orderBy,
                                                             int rowCount) {

    if (DEBUG && rowCount == 0) {
      //Why debugging want to keep it sane
      el.setMaxRows(10);
      rowCount = 10;
    } else {
      el.setMaxRows(rowCount);
    }

    sourceActor.tell(ByteString.fromString(String.format("Migrating %s: %d covers found\n", usedBy.name(), rowCount)), null);

    AtomicInteger duplicateCounter = new AtomicInteger(0);

    //Only interested in trips with covers
    el.order(orderBy).findEach((T t) -> {
      StopWatch sw = new StopWatch();
      sw.start();

      ImageHelper ih = ImageHelper.migrateUrl(t.getImageUrl(),
                                              t.getCreatedByLegacy(),
                                              t.getCreatedEpoch(),
                                              usedBy,
                                              t.getPkAsString(),
                                              true);
      if (ih.isMigrateFileError()) {
        Log.err("migrateImages(): " + usedBy + ": failed for ID:" + t.getPkAsString());
      }
      else {
        t.setFileImage(ih.getFileImage());
        t.update();
      }

      if (ih.isMigrateDuplicate()) {
        duplicateCounter.incrementAndGet();
      }

      sw.stop();
      sourceActor.tell(ByteString.fromString(ih.toString()), null);
      sourceActor.tell(ByteString.fromString("Total Time: " + sw.getTime() + "ms\n"), null);
      sourceActor.tell(ByteString.fromString("----------------------------------------------------------------------------------------------\n\n"), null);
    });
    sourceActor.tell(ByteString.fromString("Found duplicates: " + duplicateCounter.get() + "\n"), null);
  }

/*  @With({Authenticated.class, AdminAccess.class})
  secured temporarily by obscure url to allow to curl migration
 */
  public Result migrateImages(String tableName, Integer rowcount) {
    Source<ByteString, ?> source = Source.<ByteString>actorRef(65535, OverflowStrategy.dropNew())
            .mapMaterializedValue(sourceActor -> {

              StopWatch totalSw = new StopWatch();
              totalSw.start();

              ExpressionList<? extends UmappedModelWithImage> el = null;
              FileImageMigrate.ImageUser usedBy;
              String orderBy = "createdtimestamp DESC";
              switch (tableName) {
                case "trip":
                  usedBy = FileImageMigrate.ImageUser.TRIP_COVER;
                  el = Trip.find.where().isNotNull("coverurl").isNull("cover_image_pk");
                  break;
                case "template":
                  usedBy = TMPLT_COVER;
                  el = Template.find.where().isNotNull("cover_url").isNull("cover_image_pk");
                  break;
                case "destination":
                  usedBy = DEST_COVER;
                  el = Destination.find.where().isNotNull("coverurl").isNull("cover_image_pk");
                  break;
                case "trip_note_attach":
                  usedBy = TRIP_NOTE_ATACH;
                  el = TripNoteAttach.find.where()
                          .isNotNull("attach_url")
                          .eq("attach_type", PageAttachType.PHOTO_LINK.getStrVal())
                          .isNull("image_pk");
                  orderBy = "created_timestamp DESC";
                  break;
                case "destination_guide_attach":
                  usedBy = DEST_GUID_ATACH;
                  el = DestinationGuideAttach.find.where()
                          .isNotNull("attachurl")
                          .eq("attachtype", PageAttachType.PHOTO_LINK.getStrVal())
                          .isNull("image_pk");
                  break;
                case "poi_file":
                  usedBy = POI_FILE;
                  LstFileType jpg = LstFileType.find.byId("jpg");
                  LstFileType png = LstFileType.find.byId("png");
                  LstFileType jpeg = LstFileType.find.byId("jpeg");

                  el = PoiFile.find.where().isNotNull("url")
                          .or(Expr.eq("extension",jpeg), Expr.or(Expr.eq("extension",jpg), Expr.eq("extension",png)))
                          .isNull("file_pk");
                  break;
                case "library_page":
                  usedBy = IMAGE_LIBRARY;
                  el = ImageLibrary.find.where().isNotNull("s3url")
                          .isNull("image_pk").isNotNull("page_id");
                  break;
                case "image_library":
                default:
                  usedBy = IMAGE_LIBRARY;
                  el = ImageLibrary.find.where().isNotNull("s3url")
                          .isNull("image_pk");
                  break;
              }

              if(el != null) {
                migrateTableGeneric(el, usedBy, sourceActor, orderBy, rowcount);
              } else {
                throw new NotImplementedException(String.format("Image migration for %s not implemented", usedBy.name()));
              }

              sourceActor.tell(ByteString.fromString("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"), null);
              totalSw.stop();
              sourceActor.tell(ByteString.fromString("Processed All Images in: " + totalSw.getTime()  + "ms\n"), null);
              sourceActor.tell(ByteString.fromString("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"), null);

              sourceActor.tell(new Status.Success(NotUsed.getInstance()), null);
              return null;
                    });
    return ok().chunked(source);


  }
}
