package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.recommendation.RecommendedItem;
import com.umapped.itinerary.analyze.recommendation.RecommendedItemType;
import com.umapped.itinerary.analyze.supplier.wcities.WCitiesCityLookup;
import com.umapped.itinerary.analyze.supplier.wcities.WCitiesGuideEnum;
import com.umapped.itinerary.analyze.supplier.wcities.filter.FlightCenterFilter;
import com.umapped.service.offer.*;
import models.publisher.Account;
import models.publisher.Company;
import models.publisher.Trip;
import models.publisher.TripNote;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

import com.avaje.ebean.Ebean;

/**
 * Created by wei on 2017-06-09.
 */
public class OfferController
    extends Controller {
  @Inject private OfferService offerService;
  @Inject @Named("ItineraryAnalayzeObjectMapper") private ObjectMapper objectMapper;

  @Inject
  private OfferProviderRepository offerProviderRepository;

  @Inject
  private FormFactory formFactory;

  @Inject
  WCitiesCityLookup cityLookup;

  @Inject
  UMappedCityLookup umcityLookup;

  @With({Authenticated.class, Authorized.class}) public Result getOffers(String tripId) {


    ItineraryOffer offer = offerService.getOffer(tripId);
    CacheMgr.set(APPConstants.CACHE_TRIP_OFFER + tripId, offer);
    return ok(objectMapper.convertValue(offer, JsonNode.class));
  }

  @With({Authenticated.class, Authorized.class})
  public Result getFilteredOffers(String tripId, String locationId, String type, String keyword, Integer startPos) {
    Log.debug("LocationId: " + locationId + ", type = " + type);
    ItineraryOffer offers = (ItineraryOffer) CacheMgr.get(APPConstants.CACHE_TRIP_OFFER + tripId);
    if (offers == null) {
      offers = offerService.getOffer(tripId);
      CacheMgr.set(APPConstants.CACHE_TRIP_OFFER + tripId, offers);
    }

    OfferResult result = null;
    for (LocationOffer offer : offers.getLocationOffers()) {
      if (locationId.equals(offer.getLocation().getId())) {
        result = getOfferResult(offer, type, keyword, startPos, offerProviderRepository);

        if (result != null) {
          return ok(objectMapper.convertValue(result, JsonNode.class));
        }
        break;
      }
    }
    return ok();
  }

  @With({Authenticated.class, Authorized.class})
  public Result getWCitiesFilteredOffers(String tripId, String locationId, String type, String keyword, Integer startPos) {
    Log.debug("LocationId: " + locationId + ", type = " + type);
    ItineraryOffer offers = (ItineraryOffer) CacheMgr.get(APPConstants.CACHE_TRIP_OFFER + tripId);
    if (offers == null) {
      offers = offerService.getOffer(tripId);
      CacheMgr.set(APPConstants.CACHE_TRIP_OFFER + tripId, offers);
    }

    OfferResult result = null;
    LocationOffer offer = null;
    for (LocationOffer offer1 : offers.getLocationOffers()) {
      if (locationId.equals(offer1.getLocation().getId())) {
        offer = offer1;
        break;
      }
    }
    if (offer == null && locationId != null) {
      UMappedCity umCity = umcityLookup.getCityById(locationId);
      if (umCity != null) {
        Trip t = Trip.find.byId(tripId);
        offer = new LocationOffer(umCity);
        LocalDate s = LocalDateTime.ofInstant(Instant.ofEpochMilli(t.getStarttimestamp()), ZoneOffset.UTC).toLocalDate();
        LocalDate e = LocalDateTime.ofInstant(Instant.ofEpochMilli(t.getEndtimestamp()), ZoneOffset.UTC).toLocalDate();

        offer.addStayPeriod(new StayPeriod(s,e));

      }
    }

    if (offer != null) {
      result = getOfferResult(offer, type, keyword, startPos, offerProviderRepository);

      if (result != null) {
        return ok(objectMapper.convertValue(result, JsonNode.class));
      }
    }
    return ok();
  }

  public static Integer autoAddAllWcities(Trip trip, Account account, OfferService offerService, OfferProviderRepository oPR) {
    if (trip == null || SecurityMgr.getTripAccessLevel(trip, account).lt(SecurityMgr.AccessLevel.APPEND)) {
      Log.log(LogLevel.ERROR, "autoAddAllWcities: Trip does not exist");
      return APPConstants.PROCESS_WCITIES_NOTE_ERROR;
    }

    //First Delete ALL AFAR Guides
    int deleteResponse = deleteAllWcitiesFromTrip(trip.tripid);

    if (deleteResponse == APPConstants.PROCESS_WCITIES_NOTE_SUCCESS) {

      ItineraryOffer offer = offerService.getOffer(trip.tripid);

      List<OfferResult> offerResultList = new ArrayList<>();
      for (LocationOffer locationOffer : offer.getLocationOffers()) {
        OfferResult offerResult = getOfferResult(locationOffer, "Content", null, 0, oPR);
        offerResultList.add(offerResult);
      }

      if (offerResultList.size() > 0) {
        //check if this cmpy has a custom wcities filter
        Company company = Company.find.byId(trip.getCmpyid());
        List<WCitiesGuideEnum> excluded = null;
        List<WCitiesGuideEnum> includded = null;
        if (company.getTag() != null) {
          //do to maybe change this to reflection so just the class needs to be specified and no code changes required
          if (company.getTag().contains(APPConstants.CMPY_TAG_WCITIES_FILTER_FLIGHTCENTER)) {
            FlightCenterFilter fc = new FlightCenterFilter();
            excluded = fc.getExcluded();
            includded = fc.getIncluded();
          }
        }

        for (OfferResult or : offerResultList) {
          if (or.getRecommendedItems() != null && or.getRecommendedItems().size() > 0) {
            for (RecommendedItem item : or.getRecommendedItems()) {
              boolean allowed = true;
              WCitiesGuideEnum currentEnum = WCitiesGuideEnum.getFromLabel(item.name);
              //skip the note if this is part of the excluded list
              if (excluded != null && currentEnum != null && excluded.contains(currentEnum)) {
                allowed = false; //explicit exclude
              }
              else if (includded != null && currentEnum != null) {
                allowed = false;  //explicit exclude by default
                if (includded.contains(currentEnum)) {
                  allowed = true;  //explicit include
                }
              }
              if (allowed) {
                try {
                  int response = RecommendationNoteController.insertWcitiesAsTripNote(trip, item, null, "NA", account);

                  if (response == APPConstants.PROCESS_WCITIES_NOTE_ERROR) {
                    Log.log(LogLevel.ERROR, "autoAddAllWcities: System Error - Wcities guide was not added");
                  }
                }
                catch (Exception e) {
                  Log.log(LogLevel.ERROR, "autoAddAllWcities: Error adding Wcities guide", e);
                }
              }
            }
          }
        }
      }
    }

    BookingController.invalidateCache(trip.tripid, null);
    return APPConstants.PROCESS_WCITIES_NOTE_SUCCESS;
  }


  public static OfferResult getOfferResult (LocationOffer offer, String type, String keyword, Integer startPos, OfferProviderRepository oPR) {

    OfferResult result = null;

    if (offer != null) {
      switch (RecommendedItemType.valueOf(type)) {
        case ContentHotel:
        case ContentAttraction:
        case ContentRestaurant:
        case ContentShopping:
        case ContentNightlife:
        case Content:
          ContentFilter contentFilter = new ContentFilter();
          contentFilter.setLocation(offer.getLocation())
              .setOfferType(RecommendedItemType.valueOf(type))
              .setStayPeriods(offer.getStayPeriods())
              .setStartPos(startPos)
              .setKeyword(keyword);
          result = filterContentOffer(contentFilter, oPR);
          break;
        case Activity:
        case Events:
          ActivityFilter activityFilter = new ActivityFilter();
          activityFilter.setLocation(offer.getLocation())
              .setStayPeriods(offer.getStayPeriods())
              .setOfferType(RecommendedItemType.valueOf(type));
          result = filterActivityOffer(activityFilter, oPR);
      }
    }
    return result;
  }

  /*
  * This helper method deletes all Wcities guides from the itinerary.
  * Mostly called as apart of the AutoPublish/AutoWcities process..
  *
  * Returns:
  * -1 = Error
  * 0 = Successful
  */
  public static int deleteAllWcitiesFromTrip (String tripId) {
    if (tripId != null) {
      try {
        List<TripNote> existingNotes = TripNote.findAllActiveWcitiesInTrip(tripId);
        if (existingNotes != null && existingNotes.size() > 0) {
          Ebean.beginTransaction();
          for (TripNote tn : existingNotes) {
            tn.delete();
          }
          Ebean.commitTransaction();
        }
        return APPConstants.PROCESS_WCITIES_NOTE_SUCCESS;
      } catch (Exception e) {
        Ebean.rollbackTransaction();
        Log.log(LogLevel.DEBUG, "deleteAllWcitiesFromTrip: Error while deleting all Wcities Guides ", e);
      }
    }
    return APPConstants.PROCESS_WCITIES_NOTE_ERROR;
  }


  @With({Authenticated.class, Authorized.class})
  public Result wcitiesAutocomplete() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    final DynamicForm form = formFactory.form().bindFromRequest();
    String city = form.get("term");
    if (city != null) {
      city += "%";
    }

    Map<String, String> cities = cityLookup.getCities(city);
    ObjectNode result = Json.newObject();
    ArrayNode array = result.arrayNode();
    if (cities != null) {
      for (String key: cities.keySet()) {
        ObjectNode classNode = Json.newObject();
        classNode.put("label", cities.get(key));
        classNode.put("id", key);
        array.add(classNode);
      }
    }

    return ok(array);

  }

  static OfferResult filterContentOffer(ContentFilter filter, OfferProviderRepository offerProviderRepository) {
    return offerProviderRepository.getContentOffer(filter);
  }

  static OfferResult filterActivityOffer(ActivityFilter filter, OfferProviderRepository offerProviderRepository) {
    return offerProviderRepository.getActivityOffer(filter);
  }
}
