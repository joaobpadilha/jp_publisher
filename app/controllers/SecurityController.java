package controllers;

import com.avaje.ebean.Ebean;
import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.actions.ValidSession;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.AgreementForm;
import com.mapped.publisher.form.ChangePwdForm;
import com.mapped.publisher.form.LoginForm;
import com.mapped.publisher.form.ResetPwdForm;
import com.mapped.publisher.persistence.RecordStatus;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.UmappedEmail;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.AgreementView;
import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.UserMessage;
import com.umapped.api.schema.types.ReturnCode;
import models.publisher.*;
import org.apache.commons.mail.EmailException;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-16
 * Time: 1:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class SecurityController
    extends Controller {

  @Inject
  FormFactory formFactory;


  @With({Authenticated.class, Authorized.class})
  public Result changePassword() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Form<ChangePwdForm> pwdForm     = formFactory.form(ChangePwdForm.class);
    ChangePwdForm       pwdInfoForm = pwdForm.bindFromRequest().get();

    if(pwdInfoForm.getInUserId() == null || pwdInfoForm.getInNewPwd() == null) {
      return UserMessage.message(ReturnCode.HTTP_REQ_MISSING_DATA);
    }

    if (!pwdInfoForm.getInNewPwd().equals(pwdInfoForm.getInConfirmPwd()) ||
        pwdInfoForm.getInNewPwd().length() < 8) {
      return UserMessage.message("Passwords are too short or don't match");
    }

    Account a = Account.findByLegacyId(pwdInfoForm.getInUserId());
    UserProfile u = UserProfile.find.byId(pwdInfoForm.getInUserId());
    if(a == null || u == null) {
      return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND);
    }

    AccountController.AccountHelper ah = AccountController.AccountHelper.build(sessionMgr.getUserId());
    ah.setAccount(a);
    ah.setUserProfile(u);

    //Changing my own password
    if (pwdInfoForm.getInUserId().equals(sessionMgr.getUserId())) {
        List<UserPwd> userPwdList = UserPwd.authenticate(pwdInfoForm.getInUserId(),
                                                         Utils.hashPwd(pwdInfoForm.getInOrigPwd(),
                                                                       String.valueOf(u.getCreatedtimestamp())));
        if (userPwdList != null && userPwdList.size() > 0) {
          boolean res = ah.resetPassword(pwdInfoForm.getInNewPwd());
          if(!res) {
            return UserMessage.message(ReturnCode.UNHANDLED_EXCEPTION);
          }
          return UserMessage.message("Password successfully changed.");

        }
        else {
          BaseView baseView = new BaseView();
          baseView.message = "Incorrect password - please try again or contact your administrator.";
          return ok(views.html.common.message.render(baseView));
        }

    }
    else if (SecurityMgr.isUmappedAdmin(sessionMgr)) {
      boolean res = ah.resetPassword(pwdInfoForm.getInNewPwd());
      if(!res) {
        return UserMessage.message(ReturnCode.UNHANDLED_EXCEPTION);
      }
      return UserMessage.message("Password successfully changed.");
    }
    return UserMessage.message(ReturnCode.AUTH_ACCOUNT_EDIT_FAIL);
  }

  public static void auditActivity(String userid, int auditType) {
    //catch all exceptions - an audit error should not prevent user login
    try {
      UserAudit audit = new UserAudit();
      audit.setPk(DBConnectionMgr.getUniqueId());
      audit.setActivitytype(auditType);
      audit.setCreatedby(userid);
      audit.setCreatedtimestamp(System.currentTimeMillis());
      audit.save();
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "auditActivity: " + userid + " - " + auditType, e);
    }

  }

  //post
  public Result authenticate() {
    boolean payg = false;
    //bind html form to form bean
    Form<LoginForm> form = formFactory.form(LoginForm.class).bindFromRequest();

    //use bean validator framework
    if (form.hasErrors()) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.login.render(baseView));
    }
    LoginForm loginInfo = form.get();

    try {
      UserProfile u = UserProfile.find.byId(loginInfo.getInUserId());
      //try to see if the user provided an email address instead
      if (u == null && Utils.isValidEmailAddress(loginInfo.getInUserId())) {
        List<UserProfile> users = UserProfile.findActiveByEmail(loginInfo.getInUserId());
        if (users != null && users.size() == 1) {
          u = users.get(0);
        }
      }

      if(u == null || u.status == APPConstants.STATUS_DELETED) {
        BaseView view1 = new BaseView("Invalid user id/password combination - please retry");
        return ok(views.html.login.render(view1));
      }

      boolean hasEligiblePlans = false;


      //let's see if the user has a service cutoff
      BillingSettingsForUser billing     = BillingSettingsForUser.findSettingsByUserId(u.getUserid());
      long                   currentTime = System.currentTimeMillis();
      if (billing != null) {
        if (billing.getCutoffTs() != null && billing.getCutoffTs() < currentTime) {
          Account a = Account.findActiveByLegacyId(u.getUserid());
          if (a != null) {
            AccountProp accountProp = AccountProp.findByPk(a.getUid(), AccountProp.PropertyType.BILLING_INFO);
            if (accountProp != null && accountProp.billingProperties != null && accountProp.billingProperties.eligiblePlans != null
                && accountProp.billingProperties.eligiblePlans.hasEligblePlan()) {
              hasEligiblePlans = true;
            } else {
              SecurityController.auditActivity(u.userid, APPConstants.USER_AUDIT_LOGON_LOCKED);
              BaseView view1 = new BaseView();
              view1.message = "Your account is currently not active - please contact your administrator or our support"
                  + " desk";
              return ok(views.html.login.render(view1));
            }
          } else {
            SecurityController.auditActivity(u.userid, APPConstants.USER_AUDIT_LOGON_LOCKED);
            BaseView view1 = new BaseView();
            view1.message = "Your account is currently not active - please contact your administrator or our support"
                + " desk";
            return ok(views.html.login.render(view1));
          }
        }
        //check if this is pay a fyou go that requires a credit card
        BillingPlan bp = billing.getPlan();
        if (bp == null && billing.getAgent() != null && !billing.getAgent().isEmpty()) {
          BillingSettingsForUser asu = BillingSettingsForUser.findSettingsByUserId(billing.getAgent());
          bp = asu.getPlan();
        }
        if (bp != null && bp.getSchedule() == BillingSchedule.Type.TRIP && bp.getBillingType() == BillingPlan.BillType.CC) {
          payg = true;
        }
      }

      //TODO: Convert to accounts here
      if(u.status != APPConstants.STATUS_ACTIVE && !hasEligiblePlans) {
        SecurityController.auditActivity(u.userid, APPConstants.USER_AUDIT_LOGON_LOCKED);
        BaseView view1 = new BaseView();
        view1.message = "Your account is currently not active - please contact your administrator or our support desk";
        return ok(views.html.login.render(view1));
      }



      AccountController.AccountHelper ah = AccountController.AccountHelper.build(loginInfo.getInUserId());
      Account a = Account.findByLegacyId(u.getUserid());
      ah.setAccount(a).setUserProfile(u);

      List<UserPwd> userPwds = UserPwd.authenticate(u.getUserid(),
                                                    Utils.hashPwd(loginInfo.getInPwd(),
                                                                  String.valueOf(u.getCreatedtimestamp())));
      if (userPwds != null && userPwds.size() > 0) {
        Credentials cred = SecurityMgr.getCredentials(a);
        if (cred == null) {
          SecurityController.auditActivity(u.userid, APPConstants.USER_AUDIT_LOGON_LOCKED);
          BaseView view1 = new BaseView();
          view1.message = "System Error - please retry";
          return ok(views.html.login.render(view1));
        }

        if (!cred.isUmappedAdmin() && !cred.hasCompany()) {
          BaseView view1 = new BaseView();
          view1.message = "Your account is currently not active - please contact your administrator or our support"
                          + " desk";
          SecurityController.auditActivity(u.userid, APPConstants.USER_AUDIT_LOGON_LOCKED);
          return ok(views.html.login.render(view1));
        }

        if (hasEligiblePlans) {
          cred.setSubscribedToBilling(false);
        }

        //check to see if we need to agree to a new agreement
        if (cred.getCmpyLinkType() == AccountCmpyLink.LinkType.ADMIN) {

          Company cmpy = Company.find.byId(cred.getCmpyId());
          if (cmpy.getTargetagreementver() != null && (cmpy.getAcceptedagreementver() == null || !cmpy
              .getAcceptedagreementver().equals(cmpy.getTargetagreementver()))) {
            if (cmpy.getAuthorizeduserid() != null && cmpy.getAuthorizeduserid().equals(u.getUserid())) {
              SessionMgr sessionMgr = new SessionMgr(session());
              sessionMgr.setUserId(a.getLegacyId());
              sessionMgr.setAccountId(a.getUid());
              sessionMgr.setTimezoneOffsetMins(loginInfo.getTimezoneOffset());
              cred.updateCache();
              return redirect(routes.SecurityController.displayAgreement(cmpy.cmpyid));
            }
          }
        }

        SessionMgr sessionMgr = new SessionMgr(session());
        sessionMgr.setSessionExpiry();
        sessionMgr.setUserId(a.getLegacyId());
        sessionMgr.setAccountId(a.getUid());
        sessionMgr.setTimezoneOffsetMins(loginInfo.getTimezoneOffset());
        sessionMgr.setSessionId(cred.getSessionId());
        if (payg) {
          cred.setPerTripBilling(true);
        }

        ah.authSuccess();
        if (!cred.isUmappedAdmin()) {
          cred.setBillingStatus(BillingApiController.getBillingState(cred));
        }
        cred.saveCache();

        if (cred.getBillingStatus() == Credentials.BillingStatus.CREDIT_CARD_EXPIRING) {
          sessionMgr.setParam(SessionConstants.SESSION_PARAM_MSG, "Your credit card is expiring soon - please update your billing information.");
        } else {
          sessionMgr.setParam(SessionConstants.SESSION_PARAM_MSG, " Welcome " + u.getFirstname());
        }
        return redirect("/home");
      }
      else {
        boolean locked = ah.authFail();
        if (locked) {
          BaseView view1 = new BaseView();
          view1.message = "Your account has been locked. Please reset your password or contact support@umapped.com";
          return ok(views.html.login.render(view1));
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      session().clear();
      BaseView view1 = new BaseView();
      view1.message = "Invalid user id/password combination - please retry";
      return ok(views.html.login.render(view1));
    }

    BaseView view1 = new BaseView();
    view1.message = "Invalid user id/password combination - please retry";
    return ok(views.html.login.render(view1));

  }

  /**
   * Processes form with the request to reset the password
   *
   * @return
   */
  public Result resetPwd() {
    //bind html form to form bean
    Form<ResetPwdForm> resetPwdForm = formFactory.form(ResetPwdForm.class).bindFromRequest();

    //use bean validator framework
    if (resetPwdForm.hasErrors()) {
      BaseView baseView = new BaseView("Errors on the form - please resubmit");
      return ok(views.html.resetPwd.render(baseView));
    }

    try {
      ResetPwdForm resetInfo = resetPwdForm.get();
      //TODO: Discuss how this should change when userId is not a requirement
      Account a = Account.findByLegacyId(resetInfo.getInUserId());
      if (a == null) {
        BaseView baseView = new BaseView("Requested account does not exist.");
        return ok(views.html.resetPwd.render(baseView));
      }

      if (a.getState() == RecordStatus.DELETED) {
        BaseView baseView = new BaseView("User has been disabled. Please contact your administrator.");
        return ok(views.html.resetPwd.render(baseView));
      }

      if (a.getAccountType() == Account.AccountType.UMAPPED_ADMIN) {
        BaseView baseView = new BaseView("Password reset feature not available for umapped administrators. Please contact Umapped support.");
        return ok(views.html.resetPwd.render(baseView));
      }

      if (!(a.getEmail().equalsIgnoreCase(resetInfo.getInEmail()) ||
            (a.getEmailBackup() != null && a.getEmailBackup().equalsIgnoreCase(resetInfo.getInEmail())))) {
        BaseView baseView = new BaseView("Invalid email/password combination - please retry");
        return ok(views.html.resetPwd.render(baseView));
      }

      return resetPasswordHelper(a, a, false);
    }
    catch (Exception e) {
      Log.err("resetPwd - Failure to process password reset request", e);
      BaseView bindErrorView = new BaseView("System Error - please retry or contact your administrator.");
      return ok(views.html.resetPwd.render(bindErrorView));
    }
  }



  /**
   * Displays Form to reset password after validating reset token
   * @return
   */
  public Result resetPwdLink(String token) {
    //use bean validator framework
    if (token == null || token.length() == 0) {
      BaseView baseView = new BaseView("Errors on the form - please resubmit");
      return ok(views.html.resetPwd.render(baseView));
    }

    try {
      AccountAuth auth = AccountAuth.findByResetToken(token);
      if (auth.getResetExpire() != null && auth.getResetExpire() < System.currentTimeMillis()) {
        auth.clearPasswordReset();
        BaseView bv = new BaseView("Password reset link expired.");
        return ok(views.html.resetPwd.render(bv));
      }

      BaseView baseView = new BaseView("Please change your password.");
      baseView.linkId = token;
      return ok(views.html.resetPwdLink.render(baseView));
    }
    catch (Exception e) {
      BaseView view1 = new BaseView("Invalid link - please retry or contact your administrator.");
      return ok(views.html.resetPwd.render(view1));
    }
  }

  /**
   * Validates reset token and if password is correct - resets the password in the database
   *
   * @return
   */
  public Result resetNewPwd() {
    //bind html form to form bean
    Form<ResetPwdForm> resetPwdForm = formFactory.form(ResetPwdForm.class).bindFromRequest();
    //use bean validator framework
    if (resetPwdForm.hasErrors()) {
      BaseView bv = new BaseView("Unknown error with submitted request.");
      return ok(views.html.resetPwd.render(bv));
    }

    try {
      ResetPwdForm resetInfo = resetPwdForm.get();
      if (resetInfo.getInLinkId() == null) {
        BaseView bv = new BaseView("Password reset link is corrupted.");
        return ok(views.html.resetPwd.render(bv));
      }

      AccountAuth auth = AccountAuth.findByResetToken(resetInfo.getInLinkId());
      if (auth == null) {
        BaseView bv = new BaseView("Password reset not requested.");
        return ok(views.html.resetPwd.render(bv));
      }

      if (auth.getResetExpire() != null && auth.getResetExpire() < System.currentTimeMillis()) {
        auth.clearPasswordReset();
        BaseView bv = new BaseView("Password reset link expired.");
        return ok(views.html.resetPwd.render(bv));
      }

      Account a = Account.find.byId(auth.getUid());
      if (a.getState() != RecordStatus.LOCKED) {
        BaseView bv = new BaseView("Invalid link - please retry or contact your administrator.");
        return ok(views.html.resetPwd.render(bv));
      }

      if (resetInfo.getInNewPwd() != null && resetInfo.getInNewPwd().length() < 8) {
        BaseView bv = new BaseView("Password must be longer than 8 characters.");
        bv.linkId = resetInfo.getInLinkId();
        return ok(views.html.resetPwdLink.render(bv));
      }

      if (resetInfo.getInNewPwd() != null && resetInfo.getInNewPwd().length() > 1024) {
        BaseView bv = new BaseView("Your password is too ambitious we support only passwords shorter than 1024 " +
                                   "characters.");
        bv.linkId = resetInfo.getInLinkId();
        return ok(views.html.resetPwdLink.render(bv));
      }


      if (resetInfo.getInConfirmPwd() == null ||
          resetInfo.getInNewPwd() == null ||
          !resetInfo.getInConfirmPwd().equals(resetInfo.getInNewPwd())) {
        BaseView bv = new BaseView("Your passwords do not match. Please retry.");
        bv.linkId = resetInfo.getInLinkId();
        return ok(views.html.resetPwdLink.render(bv));
      }

      UserProfile u = UserProfile.findByPK(a.getLegacyId());

      AccountController.AccountHelper ah = AccountController.AccountHelper.build(a.getUid());
      ah.setAccount(a)
        .setUserProfile(u);

      boolean res = ah.resetPassword(resetInfo.getInNewPwd());
      if(!res) {
        BaseView bv = new BaseView("System error. Failed to reset password.");
        return ok(views.html.resetPwd.render(bv));
      }

      BaseView baseView = new BaseView("Your password has been set successfully. Please login.");
      return ok(views.html.login.render(baseView));
    }
    catch (Exception e) {
      BaseView view1 = new BaseView("System error. Failed to reset password");
      return ok(views.html.resetPwd.render(view1));
    }
  }

  //TODO: Move into AccountHelper class
  private Result resetPasswordHelper(Account a, Account requestedBy, boolean isAdmin) {
    AccountAuth auth = AccountAuth.getPassword(a.getUid());
    if (auth == null) {
      auth = AccountAuth.build(requestedBy.getUid());
      auth.setAuthType(AccountAuth.AAuthType.PASSWORD);
      auth.setUid(a.getUid());
      auth.setSalt(UUID.randomUUID().toString());
    }

    a.setState(RecordStatus.LOCKED);
    a.setModifiedTs(Timestamp.from(Instant.now()));
    a.save();

    String resetToken = UUID.randomUUID().toString();

    if (a.getLegacyId() != null) { //TODO: Remove when legacy user profiles are no longer needed
      UserProfile u = UserProfile.find.byId(a.getLegacyId());
      if (u != null) {
        u.setStatus(APPConstants.STATUS_ACCOUNT_LOCKED);
        u.setPwdresetid(resetToken);
        u.setPwdresetexpiry(System.currentTimeMillis() + APPConstants.PWD_RESET_EXPIRY);
        u.setModifiedby(requestedBy.getLegacyId());
        u.update();
        SecurityController.auditActivity(u.userid, APPConstants.USER_AUDIT_PASSWORD_RESET_REQ);
      }
    }

    auth.setResetToken(resetToken);
    auth.setResetExpire(System.currentTimeMillis() + APPConstants.PWD_RESET_EXPIRY);
    auth.save();

    String subject   = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_PWD_RESET_SUBJECT);
    String hostUrl   = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);
    String fromEmail = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_PWD_RESET_FROM);

    StringBuilder htmlPart = new StringBuilder();
    if(isAdmin) {
      htmlPart.append("<p>Your password has been reset by an administrator.</p> <p> Click <a href='" );
    }else {
      htmlPart.append("<p>You have requested a password reset.</p> <p> Click <a href='");
    }
    htmlPart.append(hostUrl)
            .append(routes.SecurityController.resetPwdLink(resetToken).url())
            .append("'> here </a> to reset your password </p> <p>If you " +
                    "did not reset your password, please contact your administrator or our support desk at " +
                    "help@umapped.com.</p><p>Please note that this link will expire.</p>");

    try {
      List<String> emails = new ArrayList<>();
      emails.add(a.getEmail());

      UmappedEmail email = UmappedEmail.buildDefault();
      email.withHtml(htmlPart.toString())
           .withSubject(subject)
           .withToList(emails)
           .withEmailType(EmailLog.EmailTypes.PASSWORD_RESET)
           .withAccountUid(a.getUid())
           .setFrom(fromEmail, "Umapped")
           .addTo(a.getEmail());



      String emailId = email.buildAndSend();
      Log.debug("resetPwd(): Sent password reset email ID: " + emailId + " to: " + a.getEmail());
      if(isAdmin) {
        BaseView baseView = new BaseView("A password reset email has been sent.");
        return ok(views.html.common.message.render(baseView));
      }

      BaseView successView = new BaseView("You will receive an email to complete your password reset shortly.");
      return ok(views.html.login.render(successView));
    }
    catch (EmailException ee) {
      Log.err("resetPasswordHelper(): Failed send password reset email: " + a.getEmail(), ee);
      if(isAdmin) {
        BaseView baseView = new BaseView("System error. Please contact Umapped support at support@umapped.com");
        return ok(views.html.common.message.render(baseView));
      }

      BaseView emailSendErrorView = new BaseView("System Error. Please retry or contact your administrator.");
      return ok(views.html.resetPwd.render(emailSendErrorView));
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result resetPwdAdmin() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<ResetPwdForm> resetPwdForm = formFactory.form(ResetPwdForm.class).bindFromRequest();

    //use bean validator framework
    if (resetPwdForm.hasErrors()) {
      BaseView baseView = new BaseView("Errors on the form - please resubmit");
      return ok(views.html.login.render(baseView));
    }

    try {
      ResetPwdForm resetInfo = resetPwdForm.get();
      if ((resetInfo.getInCmpyId() == null ||
           resetInfo.getInCmpyId().length() == 0) &&
          !SecurityMgr.isUmappedAdmin(sessionMgr)) {
        return UserMessage.message(ReturnCode.AUTH_ADMIN_FAIL);
      }

      if (resetInfo.getInCmpyId() != null &&
          resetInfo.getInCmpyId().length() > 0 &&
          !SecurityMgr.isUmappedAdmin(sessionMgr)) {

        Company cmpy = Company.find.byId(resetInfo.getInCmpyId());
        if (cmpy == null || !SecurityMgr.isCmpyAdmin(cmpy, sessionMgr)) {
          return UserMessage.message(ReturnCode.AUTH_ADMIN_FAIL, "Company Admins Only");
        }

        AccountCmpyLink acl = AccountCmpyLink.findActiveAccountAndCmpy(resetInfo.getInAccountId(),
                                                                       resetInfo.getInCmpyId());
        if(acl == null) {
          return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND, "Account is not linked to this company.");
        }
      }

      Account a = Account.find.byId(resetInfo.getInAccountId());
      if(a == null) {
        return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND, "System error. Account not found.");
      }

      if(a.getState() == RecordStatus.DELETED) {
        return UserMessage.message(ReturnCode.HTTP_REQ_WRONG_DATA, "Account marked as deleted. Can't reset password.");
      }

      Account currAdminAccount = Account.findByLegacyId(sessionMgr.getUserId());
      return resetPasswordHelper(a, currAdminAccount, true);
    }
    catch (Exception e) {
      Log.err("resetPwdAdmin(): Failed to initiate password reset", e);
      BaseView baseView = new BaseView("System error. Please contact Umapped support at support@umapped.com");
      return ok(views.html.common.message.render(baseView));
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result logout() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //For "God Mode" we need to remove user company Link
    UserProfile up = UserProfile.findByPK(sessionMgr.getUserId());
    if (up.getUsertype() == APPConstants.USER_ACCOUNT_TYPE_UMAPPED) {
      Account a = Account.findByLegacyId(sessionMgr.getUserId());
      if(a != null) {
        AccountController.AccountHelper ah = AccountController.AccountHelper.build(sessionMgr.getUserId());
        ah.setAccount(a);
        ah.unlinkFromAll();

      } else {
        UserCmpyLink.hardDeleteForUser(up.getUserid());
      }
    }

    sessionMgr.logout();
    flash(SessionConstants.SESSION_PARAM_MSG, "You have been logged out successfully.");
    return redirect(routes.Application.login());
  }

  public Result displayAgreement(String cmpyId) {
    SessionMgr sessionMgr = new SessionMgr(session());
    String     userId     = sessionMgr.getUserId();

    if (cmpyId != null && userId != null) {
      Company cmpy = Company.find.byId(cmpyId);
      if (cmpy != null && cmpy.authorizeduserid != null && cmpy.authorizeduserid.equals(userId) && SecurityMgr
          .isCmpyAdmin(
          cmpy,
          sessionMgr) && cmpy.getTargetagreementver() != null && Agreement.isActive(cmpy.getTargetagreementver()) &&
          (cmpy.getAcceptedagreementver() == null || !cmpy.getTargetagreementver()
                                                          .equals(cmpy.getAcceptedagreementver()))) {
        List<UserCmpyLink> userCmpyLinks = UserCmpyLink.findActiveByUserIdCmpyId(userId, cmpy.cmpyid);

        AgreementView view = new AgreementView();
        view.agreementId = cmpy.targetagreementver;
        view.cmpyId = cmpy.cmpyid;
        view.cmpyName = cmpy.name;
        view.userName = sessionMgr.getCredentials().getFirstName() + " " + sessionMgr.getCredentials().getLastName();
        view.userId = userId;
        Agreement a = Agreement.find.byId(cmpy.getTargetagreementver());
        view.html = a.html;

        return ok(views.html.agreement.render(view));
      }
    }
    flash(SessionConstants.SESSION_PARAM_MSG, "Unable to display licence agreement - please contact help@umapped.com");
    return redirect(routes.Application.login());
  }

  public Result acceptAgreement() {
    SessionMgr          sessionMgr = new SessionMgr(session());
    Form<AgreementForm> form       = formFactory.form(AgreementForm.class).bindFromRequest();
    if (form.hasErrors()) {
      return UserMessage.formErrors(form);
    }
    Credentials cred = sessionMgr.getCredentials();
    try {
      AgreementForm agreementInfo = form.get();
      Company       cmpy          = Company.find.byId(agreementInfo.getInCmpyId());
      if (agreementInfo.getInAuthorized() == null || !agreementInfo.getInAuthorized().equals(sessionMgr.getUserId())) {
        flash(SessionConstants.SESSION_PARAM_MSG,
              "Unable to process licence agreement - you need to be authorized to accept the licence agreement. " +
              "Please contact help@umapped.com");
        return redirect(routes.Application.login());
      }
      else if (cmpy != null && cmpy.getTargetagreementver() != null && cmpy.authorizeduserid != null && cmpy
          .authorizeduserid
          .equals(sessionMgr.getUserId()) && Agreement.isActive(cmpy.getTargetagreementver()) && agreementInfo
                                                                                                     .getInAgreementId() != null && cmpy
                   .getTargetagreementver()
                   .equals(agreementInfo.getInAgreementId()) && SecurityMgr.isCmpyAdmin(cmpy, sessionMgr)) {
        if (agreementInfo.getInAccept() != null && agreementInfo.getInAccept().equals(APPConstants.TRUE)) {
          try {
            Ebean.beginTransaction();
            cmpy.setAcceptedagreementver(cmpy.getTargetagreementver());
            cmpy.setLastupdatedtimestamp(System.currentTimeMillis());
            cmpy.setModifiedby(sessionMgr.getUserId());
            cmpy.save();

            CmpyAgreement cmpyAgreement = new CmpyAgreement();
            cmpyAgreement.setPk(DBConnectionMgr.getUniqueId());
            cmpyAgreement.setCmpyid(cmpy.cmpyid);
            cmpyAgreement.setAcceptauthorized(true);
            cmpyAgreement.setAgreementversion(cmpy.getTargetagreementver());
            cmpyAgreement.setStatus(APPConstants.STATUS_ACCEPTED);
            cmpyAgreement.setCreatedipaddr(request().remoteAddress());
            cmpyAgreement.setCreatedby(sessionMgr.getUserId());
            cmpyAgreement.setModifiedby(sessionMgr.getUserId());
            cmpyAgreement.setCreatedtimestamp(System.currentTimeMillis());
            cmpyAgreement.setLastupdatedtimestamp(System.currentTimeMillis());
            cmpyAgreement.save();

            AccountController.AccountHelper ah = AccountController.AccountHelper.build(sessionMgr.getUserId());
            UserProfile u = UserProfile.find.byId(sessionMgr.getUserId());
            Account a = Account.findByLegacyId(sessionMgr.getUserId());
            ah.setAccount(a).setUserProfile(u).authSuccess();

            Ebean.commitTransaction();

            sessionMgr.setSessionExpiry();
            cred.updateCache();
            sessionMgr.setSessionId(cred.getSessionId());
            flash(SessionConstants.SESSION_PARAM_MSG, " Welcome " + cred.getFirstName());
            SecurityController.auditActivity(u.userid, APPConstants.USER_AUDIT_LOGON);

            return redirect("/home");
          }
          catch (Exception e) {
            Ebean.rollbackTransaction();
          }
        }
        else {
          CmpyAgreement cmpyAgreement = new CmpyAgreement();
          cmpyAgreement.setPk(DBConnectionMgr.getUniqueId());
          cmpyAgreement.setCmpyid(cmpy.cmpyid);
          cmpyAgreement.setAcceptauthorized(true);
          cmpyAgreement.setAgreementversion(cmpy.getTargetagreementver());
          cmpyAgreement.setStatus(APPConstants.STATUS_DECLINED);
          cmpyAgreement.setCreatedipaddr(request().remoteAddress());
          cmpyAgreement.setCreatedby(sessionMgr.getUserId());
          cmpyAgreement.setModifiedby(sessionMgr.getUserId());
          cmpyAgreement.setCreatedtimestamp(System.currentTimeMillis());
          cmpyAgreement.setLastupdatedtimestamp(System.currentTimeMillis());
          cmpyAgreement.save();
          flash(SessionConstants.SESSION_PARAM_MSG,
                "You have declined the licence agreement - please contact help@umapped.com if you have any " +
                "questions.");
          sessionMgr.logout();
          return redirect(routes.Application.login());
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    flash(SessionConstants.SESSION_PARAM_MSG, "Unable to process licence agreement - please contact help@umapped.com");
    return redirect(routes.Application.login());
  }

  @With({Authenticated.class, Authorized.class})
  public Result supportRedirect() {
    SessionMgr          sessionMgr = new SessionMgr(session());
    Credentials cred = sessionMgr.getCredentials();
    List<ConsortiumCompany> consortiums =  null;
    Company company = null;
    //check for consortium so we can redirect to specific support portal
    if (!cred.isUmappedAdmin() && cred.getMainCmpy() != null) {
      company = Company.find.byId(cred.getMainCmpy());
      consortiums = ConsortiumCompany.findByCmpyId(company.getCmpyId());
    }


    if (!ConfigMgr.isProduction()) {
      BaseView baseView = new BaseView();
      baseView.message = "Cannot access our support system from non-production environment";
      return ok(views.html.error.error404.render(baseView));
    }



    String freshDeskAPI = "70d58122e10abd0b4d8bf81ee417a97a";
    String supportUrl   = "https://umapped.freshdesk.com/login/sso";

    if (company != null && company.getName().toLowerCase().trim().equals("ski.com")) {
      supportUrl = "http://ski.com.umapped.com/login/sso";
    } else {
      if (consortiums != null && consortiums.size() > 0) {
        Consortium consortium = Consortium.find.byId(consortiums.get(0).getConsId());
        if (consortium != null) {
          switch (consortium.getName().toLowerCase()) {
            case "virtuoso":
              supportUrl = "http://virtuoso-support.umapped.com/login/sso";
              break;
            case "travel leaders":
              supportUrl = "http://travelleaders-support.umapped.com/login/sso";

              break;
            case "ensemble":
              supportUrl = "http://ensemble-support.umapped.com/login/sso";

              break;
            case "protrip app: protravel international + ics":
              supportUrl = "http://protravel-support.umapped.com/login/sso";

              break;
            case "tzelltrip app: tzell travel group + ics":
              supportUrl = "http://tzell-support.umapped.com/login/sso";

              break;
          }
        }
      }
    }

    long timeInSeconds = System.currentTimeMillis()/1000;

    String      hash;
    UserProfile u          = UserProfile.find.byId(sessionMgr.getUserId());
    if (u == null || u.getEmail() == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Error accessing our support system";
      return ok(views.html.error.error404.render(baseView));
    }

    if (u.getEmail()
         .toLowerCase()
         .endsWith("umapped.com") && u.getUsertype() != APPConstants.USER_ACCOUNT_TYPE_UMAPPED) {
      BaseView baseView = new BaseView();
      baseView.message = "Error accessing our support system";
      return ok(views.html.error.error404.render(baseView));
    }

    String name = u.getFirstname() + " " + u.getLastname();

    try {
      hash = SecurityController.getHMACHash(name, u.getEmail(), timeInSeconds, freshDeskAPI);
      supportUrl = supportUrl + "?name=" + name + "&email=" + u.getEmail() + "&timestamp=" + timeInSeconds + "&hash=" + hash;

    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "supportRedirect(): Cannot redirect to support system: " + sessionMgr.getUserId());
      BaseView baseView = new BaseView();
      baseView.message = "Error accessing our support system";
      return ok(views.html.error.error404.render(baseView));
    }


    return redirect(supportUrl);
  }


  @With(ValidSession.class)
  public Result creditCardSetup() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    BaseView view = new BaseView();
    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      sessionMgr.setParam(SessionConstants.SESSION_PARAM_MSG, msg);
    }
    return ok(views.html.creditcard.render(view));
  }


  public static String hashToHexString(byte[] byteData)
  {
    StringBuffer hexString = new StringBuffer();
    for (int i = 0; i < byteData.length; i++) {
      String hex = Integer.toHexString(0xff & byteData[i]);
      // NB! E.g.: Integer.toHexString(0x0C) will return "C", not "0C"
      if (hex.length() == 1) {
        hexString.append('0');
      }
      hexString.append(hex);
    }
    return hexString.toString();
  }

  private static String getHMACHash(String name,String email,long timeInMillis, String sharedSecret) throws Exception {
    byte[] keyBytes = sharedSecret.getBytes();
    String movingFact =name+sharedSecret+email+timeInMillis;
    byte[] text = movingFact.getBytes();

    String hexString = "";
    Mac hmacMD5;
    try {
      hmacMD5 = Mac.getInstance("HmacMD5");
      SecretKeySpec macKey = new SecretKeySpec(keyBytes, "RAW");
      hmacMD5.init(macKey);
      byte[] hash =  hmacMD5.doFinal(text);
      hexString = hashToHexString(hash);

    } catch (Exception nsae) {
      System.out.println("Caught the exception");
    }
    return hexString;

  }
}
