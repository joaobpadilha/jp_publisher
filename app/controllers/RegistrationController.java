package controllers;

import com.avaje.ebean.Ebean;
import com.google.inject.Inject;
import com.mapped.common.EmailMgr;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.form.NewUserRegistrationForm;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.UmappedEmail;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.NewRegistrationView;
import com.mapped.publisher.view.UserMessage;
import com.umapped.api.schema.types.Operation;
import models.publisher.*;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.twirl.api.Html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by twong on 2014-05-20.
 */
public class RegistrationController
    extends Controller {

  @Inject
  FormFactory formFactory;

  public Result register() {

    final DynamicForm form = formFactory.form().bindFromRequest();
    String authCode = form.get("inAuthCode");

    if (authCode == null) {    //validate authcode
      BaseView baseView = new BaseView();
      baseView.message = "The invitation you received is not valid or has expired.";
      return ok(views.html.error.error.render(baseView));
    }

    UserInvite invite = UserInvite.find.byId(authCode);
    if (invite != null &&
        invite.transitionState(UserInvite.InviteEvents.E_CHECK) == UserInvite.InviteState.CHECKED) {
      NewRegistrationView view = new NewRegistrationView();
      view.authCode = invite.pk;
      view.email = invite.email;
      if (invite.getFirstname() != null && invite.getFirstname().trim().length() > 0) {
        view.firstName = invite.getFirstname().trim();
      }

      if (invite.getLastname() != null && invite.getLastname().trim().length() > 0) {
        view.lastName = invite.getLastname().trim();
      }

      return ok(views.html.register.newuser.render(view));
    }
    else {
      BaseView baseView = new BaseView();
      baseView.message = "The link you received is not valid or has expired.";
      return ok(views.html.error.error.render(baseView));
    }
  }

  public Result doRegister() {
    NewRegistrationView view = new NewRegistrationView();
    //bind html form to form bean
    Form<NewUserRegistrationForm> form = formFactory.form(NewUserRegistrationForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    NewUserRegistrationForm newRegForm = form.get();

    //check to make sure the auth code is valid
    if (newRegForm == null ||
        newRegForm.getInAuthCode() == null ||
        newRegForm.getInAuthCode().trim().length() == 0) {    //validate authcode
      BaseView baseView = new BaseView();
      baseView.message = "The inviation you received is not valid or has expired.";
      return ok(views.html.error.error.render(baseView));

    }

    UserInvite invite = UserInvite.find.byId(newRegForm.getInAuthCode());
    if (invite == null || invite.transitionState(UserInvite.InviteEvents.E_CHECK) != UserInvite.InviteState.CHECKED) {
      BaseView baseView = new BaseView();
      baseView.message = "The link you received is not valid or has expired.";
      return ok(views.html.error.error.render(baseView));
    }

    //validate all info
    view.cmpyName = newRegForm.getInCmpyName();
    view.firstName = newRegForm.getInFirstName();
    view.lastName = newRegForm.getInLastName();
    view.email = newRegForm.getInEmail();
    view.authCode = newRegForm.getInAuthCode();
    view.phoneNumber = newRegForm.getInPhoneNumber();
    if (newRegForm.hasErrors()) {
      StringBuilder sb = new StringBuilder();
      if (newRegForm.getInAuthCode() == null || newRegForm.getInAuthCode().trim().length() == 0) {
        sb.append("Invalid Request");
      }
      else {
        if (newRegForm.getInCmpyName() == null || newRegForm.getInCmpyName().trim().length() == 0) {
          sb.append("Company Name is mandatory. \n");
        }
        else if (newRegForm.getInFirstName() == null || newRegForm.getInFirstName().trim().length() == 0) {
          sb.append("First Name is mandatory. \n");
        }
        else if (newRegForm.getInLastName() == null || newRegForm.getInLastName().trim().length() == 0) {
          sb.append("Last Name is mandatory. \n");
        }
        else if (newRegForm.getInEmail() == null || newRegForm.getInEmail().trim().length() == 0) {
          sb.append("Email is mandatory. \n");
        }
        else if (newRegForm.getInConfirmEmail() == null || newRegForm.getInConfirmEmail().trim().length() == 0) {
          sb.append("Confirmation Email is mandatory. \n");
        }
        else if (newRegForm.getInEmail() != null && newRegForm.getInEmail().trim().length() > 0) {
          if (!Utils.isValidEmailAddress(newRegForm.getInEmail())) {
            sb.append("Invalid Email address. \n");
          }
          else if (newRegForm.getInConfirmEmail() != null && !newRegForm.getInEmail()
                                                                        .equalsIgnoreCase(newRegForm
                                                                                              .getInConfirmEmail())) {
            sb.append("The email address does not match. \n");
          }
        }
      }
      sb.append("Please try again.")  ;
      view.message = sb.toString();
      return ok(views.html.register.newuser.render(view));
    }

    String origUserId = RegistrationController.getUserId(newRegForm.getInEmail());
    String userId = origUserId;

    boolean autocreateuser = true;
    String msg = null;
    if (origUserId == null || origUserId.contains("00000")) {
      msg = "We cannot create a userid with your first and last names. Please contact us at support@umapped"
                     + ".com and we will be happy to help  with your account.";
      autocreateuser = false;
    }

    //check to make sure a duplicate company with the same name does not already exists
    if (Company.countByCmpyName(newRegForm.getInCmpyName().trim()) > 0) {
      msg = "Your company is already a Umapped subscriber. Please contact us at support@umapped.com and we "
                     + "will be happy to set up your account.";
      autocreateuser = false;
    }

    //check to see if the email is already registered
    Account a = Account.findByEmail(newRegForm.getInEmail());
    if(a != null && !a.isTraveler()) {
      view.message = "Your email is already associated with a Umapped account. Please contact us at support@umapped"
                     + ".com and we will be happy to help  with your account.";
      autocreateuser = false;
    }

    //create a user profile (first and loop in case of concurrency)
    userId = AccountController.AccountHelper.generateLegacyId(userId);
    if(userId == null) {
      msg = "Enexpected problem with User ID - please try again.";
      autocreateuser = false;
    }

    if (autocreateuser) {
      AccountController.AccountHelper ah = AccountController.AccountHelper.build("system");
      ah.setAccountType(Account.AccountType.PUBLISHER);
      ah.setOperation(Operation.ADD);

      try {
        Ebean.beginTransaction();
        ah.crupAccount(userId, newRegForm.getInFirstName(), newRegForm.getInLastName(), newRegForm.getInEmail(), null);
        ah.crupAccountContact();
        ah.addPhone(newRegForm.getInPhoneNumber());
        ah.contactPersist();

        //create a company record
        //company is set as a trial company with a 2 year expiry and an initial limit of 5 trips and 5 users
        long expiryTimestamp = 2 * 365 * 24 * 60 * 60;
        expiryTimestamp = expiryTimestamp * 1000;
        expiryTimestamp = expiryTimestamp + System.currentTimeMillis();

        Company cmpy = Company.buildCompany(userId);

        cmpy.setName(newRegForm.getInCmpyName());
        cmpy.setLastupdatedtimestamp(System.currentTimeMillis());
        cmpy.setModifiedby(userId);
        cmpy.setType(APPConstants.CMPY_TYPE_TRIAL);
        cmpy.setNumaccounts(APPConstants.CMPY_DEFAULT_NUM_ACCOUNTS);
        cmpy.setNumAccounts(APPConstants.CMPY_DEFAULT_NUM_ACCOUNTS);
        cmpy.setMaxtrips(APPConstants.CMPY_DEFAULT_NUM_TRIPS);
        cmpy.setBillingtype(Company.BillingType.UNDEFINED);
        cmpy.setFlighttracking(false);
        cmpy.setExpirytimestamp(expiryTimestamp);
        cmpy.setItinerarycheck(false);
        cmpy.setEnablecollaboration(true);

        //set user agreement
        cmpy.setTargetagreementver("1.8"); //TODO - change to configurable version
        cmpy.setAuthorizeduserid(userId);
        cmpy.save();

        //create a company link
        ah.linkToCompany(cmpy, AccountCmpyLink.LinkType.ADMIN);
        view.userId = userId;

        //update the trip_agent table with the actual userid for the invite id
        view.tripCount = UserController.linkTripsToUser(userId, invite.getPk(), userId);

        //update the new user invite
        invite.setNewuserid(userId);
        invite.transitionState(UserInvite.InviteEvents.E_ACCEPT);
        invite.save();

        //send confirmation and umapped internal email
        String hostUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);
        view.pwdResetLink = hostUrl + routes.SecurityController.resetPwdLink(ah.resetToken).url();
        RegistrationController.sendConfirmationEmail(view);
        Ebean.commitTransaction();
        return ok(views.html.register.newusersuccess.render(view));
      } catch (Exception e) {
        Log.err("RegistrationController:doRegister - " + view.email, e);
        Ebean.rollbackTransaction();
        autocreateuser = false;
      }
    }

    //send email to support for manual sign up
    try {
      Html supportEmail = views.html.email.userSignup.render(view, msg, invite.getCreatedby());
      List<String> emails = new ArrayList<>();
      emails.add("support@umapped.com");
      UmappedEmail emailSender = UmappedEmail.buildDefault();
      emailSender.addTo("support@umapped.com", "Umapped Support");

      emailSender.withFromEmail("signup@umapped.com")
              .withFromName("New User registration")
              .withToEmail("support@umapped.com")
              .withToList(emails)
              .withEmailType(EmailLog.EmailTypes.COLLABORATOR_SIGN_UP)
              .withSubject("New User Collaboration Registration - Create Account")
              .withHtml(supportEmail.toString())
              .buildAndSend();

      return ok(views.html.register.newusersuccess.render(view));

    } catch (Exception e) {
      Log.err("RegistrationController:doRegister - " + view.email, e);
    }

    view.message = "An error has occurred - please try again.";
    return ok(views.html.register.newuser.render(view));

  }

  public Result confirm() {
    NewRegistrationView view = new NewRegistrationView();
    return ok(views.html.register.newusersuccess.render(view));
  }

  public static String getUserId(String firstName, String lastName) {
    //user id is first character of first name + 5 characters of last name
    //if last name is less than 5, pad with 0
    //check db for duplicates and append a counter
    firstName = firstName.trim().replaceAll("[^\\p{L}0-9]", "").toLowerCase();
    lastName = lastName.trim().replaceAll("[^\\p{L}0-9]", "").toLowerCase();


    StringBuilder sb = new StringBuilder();

    //if the last name is less than 26 char - first char of first name+lastname up to a max of 17 characters
    //if the last name is more than 26 char - first name+ first char of lastname

    //if userid is less than 5, pad with 0
    if (lastName.length() > 0 && lastName.length() < 26 && firstName.length() > 0) {
      sb.append(firstName.substring(0, 1));
      if (lastName.length() > 13) {
        sb.append(lastName.substring(0, 13));
      } else {
        sb.append(lastName);
      }
    } else if(firstName.length() > 0 && lastName.length() > 0) {
      if (firstName.length() > 13) {
        sb.append(firstName.substring(0, 13));
      } else {
        sb.append(firstName);
      }
      sb.append(lastName.substring(0,1));
    }

    String s = sb.toString();
    if (s.length() < 5) {
      for (int i = 0; i < (5 - s.length()); i++) {
        sb.append("0");
      }
    }




    return sb.toString().toLowerCase().trim();
  }


  public static String getUserId(String email) {
    //user id is first character of first name + 5 characters of last name
    //if last name is less than 5, pad with 0
    //check db for duplicates and append a counter
    String userid = email.trim().toLowerCase().substring(0,email.indexOf("@"));
    if (userid != null && userid.length() > 0) {
      userid = userid.replaceAll("[^\\p{L}0-9]", "");
      if (userid.length() >= 15) {
        userid = userid.substring(0,14);
      }
      StringBuilder sb = new StringBuilder(userid);
      if (userid.length() < 5) {
        for (int i = 0; i < (5 - userid.length()); i++) {
          sb.append("0");
        }
      }
      return sb.toString().toLowerCase().trim();
    }
    return null;
  }


  public static void sendConfirmationEmail(NewRegistrationView view)
      throws Exception {
    String smtp = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_HOST);
    String user = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_USER_ID);
    String pwd = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_PASSWORD);
    String fromEmail = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_PWD_RESET_FROM);
    String subject = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_NEW_ACCOUNT_SUBJECT);
    String emailEnabled = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_ENABLED);

    if (emailEnabled != null && emailEnabled.equalsIgnoreCase("Y")) {
      //initialize email mgr
      EmailMgr emailMgr = new EmailMgr(smtp, user, pwd);
      emailMgr.init();

      String emailBody = views.html.email.registrationconfirm.render(view).toString();

      List<Map<String, String>> body = new ArrayList<Map<String, String>>();
      Map<String, String> token = new HashMap<String, String>();
      token.put(com.mapped.common.CoreConstants.HTML, emailBody);
      body.add(token);

      emailMgr.send(fromEmail, "Umapped", view.email, subject, body);

      //send umapped onboard email
       if (ConfigMgr.getInstance().isProd()) {
        try {
          //send a notification to helpdesk of new user
          //initialize email mgr
          emailMgr = new EmailMgr(smtp, user, pwd);
          emailMgr.init();
          emailMgr.send(fromEmail, "Umapped", "support@umapped.com", subject + " " + view.email, body);

          emailMgr = new EmailMgr(smtp, user, pwd);
          emailMgr.init();
          emailMgr.send(fromEmail, "Umapped", "sales@umapped.com", subject + " " + view.email, body);
        }
        catch (Exception e) {
          Log.log(LogLevel.ERROR, "sendConfirmationEmail:", "Cannot send onboarding email for: " + view.email);
        }
      }
    }
  }

}
