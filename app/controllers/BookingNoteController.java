package controllers;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.audit.AuditActionType;
import com.mapped.publisher.audit.AuditActorType;
import com.mapped.publisher.audit.AuditModuleType;
import com.mapped.publisher.audit.event.AuditTripDocCustom;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.*;
import com.mapped.publisher.js.BaseJsonResponse;
import com.mapped.publisher.persistence.PageAttachType;
import com.mapped.publisher.persistence.bookingapi.BookingAPIMgr;
import com.mapped.publisher.persistence.bookingapi.BookingRS;
import com.mapped.publisher.persistence.redis.RedisKeys;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;
import com.mapped.publisher.view.UmNoteView;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.external.afar.AfarImage;
import com.umapped.external.afar.Highlight;
import com.umapped.itinerary.analyze.ItineraryAnalyzeResult;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.segment.TripSegmentAnalyzeResult;
import com.umapped.persistence.notes.insurance.InsuranceNote;
import com.umapped.persistence.notes.StructuredNote;
import com.umapped.persistence.reservation.UmTraveler;
import com.umapped.service.offer.OfferProviderRepository;
import com.umapped.service.offer.OfferService;
import com.umapped.service.offer.UMappedCity;
import com.umapped.service.offer.UMappedCityLookup;
import models.publisher.*;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.joda.time.DateTime;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.*;
import play.twirl.api.Html;
import scala.concurrent.java8.FuturesConvertersImpl;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.umapped.persistence.notes.utils.StructuredNoteUtils.cast;
import static controllers.DestinationController.getT42PoiDescription;
import static controllers.DestinationController.t42GuideToPageBlock;

/**
 * Created by twong on 15-04-25.
 */
public class BookingNoteController
    extends Controller {

  @Inject
  FormFactory formFactory;

  @Inject
  OfferService offerService;

  @Inject
  UMappedCityLookup umappedCityLookup;


  @Inject
  private OfferProviderRepository offerProviderRepository;

  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result addAfarSelectedContent(String tripId, String date) {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    DateUtils du = new DateUtils();

    Long d = null;
    try {
      if (date != null) {
        d = du.parseDate(date, "EEE MMM dd, yyyy").getTime();
      }
    } catch (Exception e) {}

    JsonNode                                jsonRequest = request().body().asJson();
    DestinationController.T42SelectedGuides guides      = Json.fromJson(jsonRequest,
            DestinationController.T42SelectedGuides.class);
    Trip tripModel = Trip.findByPK(tripId);

    List<String> addedPois = new ArrayList<>();

    if(guides.destinations != null) {
      for (Integer t42DestId : guides.destinations.keySet()) {
        Map<String, List<Integer>> guideTypes = guides.destinations.get(t42DestId);
        for(String a: guides.afar) {
          AfarPoi poi = AfarPoi.findById(a);
          for (String type : guideTypes.keySet()) {
            if(!addedPois.contains(a)) {
              List<Integer> guideSeqNums = guideTypes.get(type);
              afarGuides2Note(tripModel, d, poi, guideSeqNums, type, sessionMgr.getCredentials().getAccount());
              addedPois.add(a);
            }
          }
        }
      }
    }

    if(guides.hotels != null) {
      for(String hotelId: guides.hotels) {
        //T42Hotel hotel = T42Hotel.findByKey(hotelId);
        AfarPoi hotel= AfarPoi.findById(hotelId);
        afarHotel2Note(tripModel, d, hotel, sessionMgr.getCredentials().getAccount());
      }
    }

    invalidateCache(tripModel.tripid);

    sw.stop();
    Log.debug("Added T42 Guides in: " + sw.getTime() + "ms");
    return ok();
  }


  /**
   * Eating JSON for lunch to convert into guides
   * @return
   */
  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result addT42SelectedContent(String tripId, String date) {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    DateUtils du = new DateUtils();

    Long d = null;
    try {
      if (date != null) {
        d = du.parseDate(date, "EEE MMM dd, yyyy").getTime();
      }
    } catch (Exception e) {}

    JsonNode                                jsonRequest = request().body().asJson();
    DestinationController.T42SelectedGuides guides      = Json.fromJson(jsonRequest,
                                                                        DestinationController.T42SelectedGuides.class);
    Trip tripModel = Trip.findByPK(tripId);

    if(guides.destinations != null) {
      for (Integer t42DestId : guides.destinations.keySet()) {
        Map<String, List<Integer>> guideTypes = guides.destinations.get(t42DestId);
        T42Destination             t42Dest    = T42Destination.find.byId(t42DestId);

        for (String type : guideTypes.keySet()) {
          List<Integer> guideSeqNums = guideTypes.get(type);
          t42Guides2Note(tripModel, d, t42Dest, guideSeqNums, type, sessionMgr.getUserId());
        }
      }
    }

    if(guides.hotels != null) {
      for(String hotelId: guides.hotels) {
        T42Hotel hotel = T42Hotel.findByKey(hotelId);
        t42Hotel2Note(tripModel, d, hotel, sessionMgr.getUserId());
      }
    }

    invalidateCache(tripModel.tripid);

    sw.stop();
    Log.debug("Added T42 Guides in: " + sw.getTime() + "ms");
    return ok();
  }

  public static void afarHotel2Note(Trip trip, Long d,
                                   AfarPoi hotel, Account a) {
    if(hotel == null) {
      return;
    }

    Highlight h = hotel.unmarshal(hotel.getData(), Highlight.class);

    StringBuilder sb = new StringBuilder();
    sb.append("<!-- Afar Hotel Block ID: ")
            .append(h.getSlug())
            .append(" -->")
            .append(System.lineSeparator())
            .append("<img class=\"afarLogo\" style=\"float:left;width:100px;\"src=\"")
            .append(ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.IMG_URL))
            .append("/img/suppliers/afar_logo.png\"><h5 style=\"margin-bottom:10px;padding-top:18px;margin-left:120px;\">You can continue planning your trip at <a href=\"https://www.afar.com/saveplango\" target=\"_blank\">Afar's Trip Planner</a></h5>\n")
            .append(h.getDescription());

    TripNote tn = TripNote.buildTripNote(trip, a.getLegacyId());
    tn.setName(h.getTitle() + " (" + hotel.getAfarCityId() + ")");
    tn.setIntro(sb.toString());
    /*tn.setLocLat(Float.parseFloat(h.getLat()));
    tn.setLocLong(Float.parseFloat(h.getLng()));*/
    tn.setTag(APPConstants.AFAR_NOTE_TAG);
    tn.setType(TripNote.NoteType.CONTENT);

    int currRows = TripNote.tripNoteCount(trip.tripid);
    tn.setRank(++currRows);
    tn.setImportTs(System.currentTimeMillis());
    //tn.setImportSrc(BookingSrc.ImportSrc.TRAVEL42_HOTEL);
    //tn.setImportSrcId(hotel.getHotelId());
    tn.setNoteTimestamp(d);
    tn.save();

    for(AfarImage ai : h.getImages()) {
      String filename = ai.getImageUrl().substring(ai.getImageUrl().lastIndexOf("/") + 1, ai.getImageUrl().indexOf("?"));
      FileImage fi = ImageController.downloadAndSaveImage(ai.getImageUrl(), filename, FileSrc.FileSrcType.IMG_USER_DOWNLOAD, "Attribution: " + ai.getAttribution(), a, false);
      if(fi != null) {
        TripNoteAttach attach = TripNoteAttach.build(tn, a.getLegacyId());
        attach.setFileImage(fi);
        attach.setAttachType(PageAttachType.PHOTO_LINK);
        if (ai.getAttribution() != null && !ai.getAttribution().isEmpty()) {
          attach.setComments("Attribution: " + ai.getAttribution());
        }
        attach.setName(h.getTitle());
        attach.setAttachUrl(ai.getImageUrl());
        attach.setAttachName(ai.getImageUrl().substring(ai.getImageUrl().lastIndexOf("/") + 1, ai.getImageUrl().indexOf("?")));
        attach.save();
      }
    }
  }

  public static void t42Hotel2Note(Trip trip, Long d,
                                   T42Hotel hotel, String userId) {
    if(hotel == null) {
      return;
    }

    StringBuilder sb = new StringBuilder();
    sb.append("<h1>")
      .append(hotel.getHotelName())
      .append("</h1>")
      .append(System.lineSeparator())
      .append("<!-- Travel42 Hotel Block ID: ")
      .append(hotel.getHotelId())
      .append(" -->")
      .append(System.lineSeparator())
      .append(hotel.getStarReview());

    TripNote tn = TripNote.buildTripNote(trip, userId);
    tn.setName(hotel.getHotelName() + " (" + hotel.getGeoDisplay() + ")");
    tn.setIntro(sb.toString());
    tn.setLocLat(hotel.getLatitude());
    tn.setLocLong(hotel.getLongitude());
    tn.setTag("Travel42");
    int currRows = TripNote.tripNoteCount(trip.tripid);
    tn.setRank(++currRows);
    tn.setImportTs(System.currentTimeMillis());
    tn.setImportSrc(BookingSrc.ImportSrc.TRAVEL42_HOTEL);
    tn.setImportSrcId(hotel.getHotelId());
    tn.setNoteTimestamp(d);
    tn.save();
  }

  public static void afarGuides2Note(Trip trip,
                                     Long d,
                                     AfarPoi poi,
                                     List<Integer> seqNumbers,
                                     String rootGuideIndex,
                                     Account a) {
    StringBuilder sb = new StringBuilder();
    T42GuideIndex gi = T42GuideIndex.findByKey("#" + rootGuideIndex + "#");

    int infoBlockCount = 0;
    Highlight h = poi.unmarshal(poi.getData(), Highlight.class);
    sb.append("<img class=\"afarLogo\" style=\"float:left;width:100px;\"src=\"")
      .append(ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.IMG_URL))
      .append("/img/suppliers/afar_logo.png\"><h5 style=\"margin-bottom:10px;padding-top:18px;margin-left:120px;\">You can continue planning your trip at <a href=\"https://www.afar.com/saveplango\" target=\"_blank\">Afar's Trip Planner</a></h5>\n");
    for (Integer seq : seqNumbers) {
      infoBlockCount++;
      sb.append(h.getDescription());
    }
    if (infoBlockCount != 0) {
      TripNote tn = TripNote.buildTripNote(trip, a.getLegacyId());
      tn.setName(h.getTitle() + " - " + poi.getAfarCityId() + " " + gi.getClassName());
      tn.setIntro(sb.toString());
      tn.setType(TripNote.NoteType.CONTENT);
      //tn.setStreetAddr(h.getLocationAddress());
      /*if (h.getLat() != null) {
        tn.setLocLat(Float.parseFloat(h.getLat()));
      }
      if (h.getLng() != null) {
        tn.setLocLong(Float.parseFloat(h.getLng()));
      }
      tn.setTag(APPConstants.AFAR_NOTE_TAG);
      }*/
      tn.setTag(APPConstants.AFAR_NOTE_TAG);
      int currRows = TripNote.tripNoteCount(trip.tripid);
      tn.setRank(++currRows);
      tn.setImportTs(System.currentTimeMillis());
      //tn.setImportSrc(BookingSrc.ImportSrc.TRAVEL42_DESTINATION);
      //tn.setImportSrcId(poi.getAfarPoiId());
      //t42DestCompleteNote(tn, td);

      tn.setNoteTimestamp(d);
      tn.save();

      if (h.getImages() != null && h.getImages().size() > 0) {
        for (AfarImage ai : h.getImages()) {
          try {
            String filename = ai.getImageUrl().substring(ai.getImageUrl().lastIndexOf("/") + 1, ai.getImageUrl().indexOf("?"));
            FileImage fi = ImageController.downloadAndSaveImage(ai.getImageUrl(), filename, FileSrc.FileSrcType.IMG_USER_DOWNLOAD, "Attribution: " + ai.getAttribution(), a, false);
            if (fi != null) {
              TripNoteAttach attach = TripNoteAttach.build(tn, a.getLegacyId());
              attach.setFileImage(fi);
              attach.setAttachType(PageAttachType.PHOTO_LINK);
              if (ai.getAttribution() != null && !ai.getAttribution().isEmpty()) {
                attach.setComments("Attribution: " + ai.getAttribution());
              }
              attach.setName(h.getTitle());
              attach.setAttachUrl(ai.getImageUrl());
              attach.setAttachName(ai.getImageUrl().substring(ai.getImageUrl().lastIndexOf("/") + 1, ai.getImageUrl().indexOf("?")));
              attach.save();
            }
          } catch (Exception e) {

          }
        }
      }

      //Lookup T42 image
      /*List<T42DestinationImage> images = T42DestinationImage.byGeoPlaceKey(td.getGeoPlaceKey());
      for(T42DestinationImage tdi : images){
        FileImage fi = tdi.getFileImage();
        TripNoteAttach tna = TripNoteAttach.build(tn, userId);
        tna.setName(fi.getFile().getFilename());
        tna.setAttachName(fi.getFile().getFilename());
        tna.setAttachType(PageAttachType.PHOTO_LINK);
        tna.setAttachUrl(fi.getUrl());
        tna.setFileImage(fi);
        tna.setComments(fi.getFile().getDescription());
        tna.setRank(tdi.getRank());
        tna.save();
      }*/
    }
  }

  public static void t42Guides2Note(Trip trip,
                                    Long d,
                                    T42Destination td,
                                    List<Integer> seqNumbers,
                                    String rootGuideIndex,
                                    String userId) {

    StringBuilder sb = new StringBuilder();
    T42GuideIndex gi = T42GuideIndex.findByKey("#" + rootGuideIndex + "#");
    if(gi != null) {
      sb.append("<h1>").append(gi.getClassName()).append("</h1>\n");
    }

    int infoBlockCount = 0;
    for (Integer seq : seqNumbers) {
      T42Guide guide = T42Guide.findByKey(td.getGeoPlaceKey(), seq);
      if (guide == null) {
        continue;
      }
      switch (guide.getRowType()) {
        case "POI":
          if(guide.getPoiPlaceKey() != null)
            t42PoiGuideToNote(trip, d, td, guide, userId);
          break;
        default:
          infoBlockCount++;
          sb.append("<!-- Travel42 Guide Block Destination ID: ");
          sb.append(td.getGeoPlaceKey());
          sb.append(" Sequence Number: ");
          sb.append(seq);
          sb.append(" -->");
          sb.append(t42GuideToPageBlock(guide));
      }
    }
    if (infoBlockCount != 0) {
      TripNote tn = TripNote.buildTripNote(trip, userId);
      tn.setName(td.getDisplay() + " " + gi.getClassName());
      tn.setIntro(sb.toString());
      if (td.getLatitude() != null) {
        tn.setLocLat(td.getLatitude());
      }
      if (td.getLongitude() != null) {
        tn.setLocLong(td.getLongitude());
      }
      tn.setTag("Travel42");
      int currRows = TripNote.tripNoteCount(trip.tripid);
      tn.setRank(++currRows);
      tn.setImportTs(System.currentTimeMillis());
      tn.setImportSrc(BookingSrc.ImportSrc.TRAVEL42_DESTINATION);
      tn.setImportSrcId(td.getGeoPlaceKey().toString());
      t42DestCompleteNote(tn, td);
      tn.setNoteTimestamp(d);
      tn.save();

      //Lookup T42 image
      List<T42DestinationImage> images = T42DestinationImage.byGeoPlaceKey(td.getGeoPlaceKey());
      for(T42DestinationImage tdi : images){
        FileImage fi = tdi.getFileImage();
        TripNoteAttach tna = TripNoteAttach.build(tn, userId);
        tna.setName(fi.getFile().getFilename());
        tna.setAttachName(fi.getFile().getFilename());
        tna.setAttachType(PageAttachType.PHOTO_LINK);
        tna.setAttachUrl(fi.getUrl());
        tna.setFileImage(fi);
        tna.setComments(fi.getFile().getDescription());
        tna.setRank(tdi.getRank());
        tna.save();
      }
    }
  }

  /**
   * Convert a single T42 POI Guide to Page
   *
   * @param guide
   */
  private static void t42PoiGuideToNote(Trip trip, Long d, T42Destination td, T42Guide guide, String userId) {
    TripNote tn = TripNote.buildTripNote(trip, userId);

    T42Poi p = T42Poi.find.byId(guide.getPoiPlaceKey().getPoiPlaceKey());
    if(p.getLatitude() != null) {
      tn.setLocLat(p.getLatitude());
    }
    if(p.getLongitude() != null) {
      tn.setLocLong(p.getLongitude());
    }
    tn.setTag("Travel42");
    tn.setIntro(getT42PoiDescription(guide, p));
    tn.setStreetAddr(p.getAddress1() + " " + p.getAddress2());
    tn.setZipcode(p.getZipCode());
    t42DestCompleteNote(tn, td);
    tn.setName(p.getPoiName());
    tn.setImportTs(System.currentTimeMillis());
    tn.setImportSrc(BookingSrc.ImportSrc.TRAVEL42_POI);
    tn.setImportSrcId(p.getPoiPlaceKey().toString());
    int currRows = TripNote.tripNoteCount(trip.tripid);
    tn.setRank(++currRows);
    tn.setNoteTimestamp(d);
    tn.save();
  }

  private static void t42DestCompleteNote(TripNote tn, T42Destination td) {
    switch (td.getType()) {
      case "City":
        tn.setCity(td.getName());
        if(td.getParentGeoPlaceKey() != null) {
          T42Destination parent = T42Destination.find.byId(td.getParentGeoPlaceKey());
          if (parent.getType().equals("State")) {
            tn.setState(parent.getName());
          }
        }
      case "State":
      case "Country":
      default:
        T42Destination country = T42Destination.findCountryByCode(td.getISOCode());
        tn.setCountry(country.getName());
        break;
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result redirectNote(String tripId, Long tripNoteId, String tripDetailId, String inStartDate) {

    if (tripNoteId != null) {
      TripNote note = TripNote.find.byId(tripNoteId);

      if (note != null) {
        return redirect(routes.BookingNoteController.getStructuredNote(tripId, tripNoteId, tripDetailId, inStartDate, note.getType().name()));

      }
    }

    BaseView baseView = new BaseView();
    baseView.message = "Permission Error - you are not authorized to access this page";
    return ok(views.html.common.message.render(baseView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result getNote(String tripId, Long tripNoteId, String tripDetailId, String inStartDate) {
    return redirect(routes.BookingNoteController.getStructuredNote(tripId, tripNoteId, tripDetailId, inStartDate, "DEFAULT"));
  }


  @With({Authenticated.class, Authorized.class})
  public Result getStructuredNote(String tripId, Long tripNoteId, String tripDetailId, String inStartDate, String inType ) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    TripNote.NoteType noteType = TripNote.NoteType.valueOf(inType);

    NoteBaseView view = noteType.instantiateView();


    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    Trip trip = null;
    List<TripBookingDetailView> detailsViews = new ArrayList<>();

    if (tripId != null) {
      trip = Trip.findByPK(tripId);
      if (trip != null && trip.status != APPConstants.STATUS_DELETED) {
        SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(trip, sessionMgr);
        if (accessLevel.ge(SecurityMgr.AccessLevel.APPEND)) {
          view.tripId = tripId;
          view.noteId = new Long(0);
          view.tripName = trip.name;
          view.noteType = noteType;
          view.accessLevel = accessLevel;
          if (SecurityMgr.isCmpyAdmin(trip.cmpyid, sessionMgr)) {
            view.isCmpyAdmin = true;
          }
          else {
            view.isCmpyAdmin = false;
          }
          final DynamicForm form = formFactory.form().bindFromRequest();
          String scrollDiv = form.get("inScrollTo");
          if (scrollDiv == null) {
            scrollDiv = flash(SessionConstants.SESSION_PARAM_SCROLL_TO_DIV);
          }
          if (scrollDiv != null && scrollDiv.length() > 0) {
            view.scrollToId = scrollDiv;
          }

          //get a list all trip details
          List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
          if (tripDetails != null) {
            for (TripDetail tripDetail : tripDetails) {
              detailsViews.add(BookingController.buildBookingView(tripId, tripDetail, false));
            }
          }
        }
      }
      final DynamicForm form = formFactory.form().bindFromRequest();
      String scrollDiv = form.get("inScrollTo");
      if (scrollDiv == null) {
        scrollDiv = flash(SessionConstants.SESSION_PARAM_SCROLL_TO_DIV);
      }
      if (scrollDiv != null && scrollDiv.length() > 0) {
        view.scrollToId = scrollDiv;
      }

      if (tripNoteId != null && tripNoteId > 0L) {
        TripNote tripNote = TripNote.find.byId(tripNoteId);
        Trip tripModel = null;
        if (tripNote != null) {
          tripModel = tripNote.getTrip();
        }

        if (tripNote == null || tripNote.getStatus() == APPConstants.STATUS_DELETED || tripModel == null ||
            !SecurityMgr.getTripAccessLevel(tripModel, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND)) {
          BaseView baseView = new BaseView();
          baseView.message = "Permission Error - you are not authorized to access this page";
          return ok(views.html.common.message.render(baseView));
        }

        view.message = msg;
        if (scrollDiv == null) {
          view.scrollToId = String.valueOf(tripNote.getNoteId());
        } else {
          view.scrollToId = scrollDiv;
        }

        if (noteType == TripNote.NoteType.DEFAULT || noteType == TripNote.NoteType.CONTENT) {
          view = buildNoteView(tripModel, tripNote, sessionMgr);
          view.noteType = noteType;
        } else {
          view = cast(view.buildView(tripNote, view), noteType.getNoteView());
          view.noteType = noteType;
        }
        view.loggedInUserId = sessionMgr.getUserId();

        if (view.linkedTripDetailsId != null) {
          //set the tab
          tripDetailId = view.linkedTripDetailsId;
        }

        if (view.tripId != null) {
          view.tripAttachments = WebItineraryController.getTripAttachments(view.tripId, sessionMgr.getTimezoneOffsetMins());
        }


        try {
          java.lang.reflect.Method render = null;
          try {
            final Class<?> clazz = Class.forName(noteType.getNoteTemplate());
            render = clazz.getDeclaredMethod("render", noteType.getNoteView(), List.class, String.class, com.mapped.publisher.view.TabType.Bound.class);
          } catch (NoSuchMethodException e) {
            e.printStackTrace();
          } catch (ClassNotFoundException e) {
            e.printStackTrace();
          }
          if (render != null) {
            Html html = null;
            html = (Html) render.invoke(null, cast(view, noteType.getNoteView()), detailsViews, tripDetailId, getTabType(tripDetailId));

            invalidateCache(tripModel.tripid);
            return ok(html);
          }
        } catch (IllegalAccessException e) {
          e.printStackTrace();
        } catch (InvocationTargetException e) {
          e.printStackTrace();
        }

      } else {
        if (trip != null && trip.status != APPConstants.STATUS_DELETED) {
          if (inStartDate != null && inStartDate.trim().length() > 0) {
            try {
              DateUtils du = new DateUtils();
              Date d = du.parseDate(inStartDate, "EEE MMM dd, yyyy");
              view.startDate = Utils.formatDateControlYYYY(d.getTime());
            }
            catch (Exception e) {

            }
          }
          view.tripStartDatePrint = Utils.formatDateControlYYYY(trip.getStarttimestamp());
          SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(trip, sessionMgr);
          if (accessLevel.ge(SecurityMgr.AccessLevel.APPEND)) {
            view.linkedTripDetailsId = tripDetailId;

            try {
              java.lang.reflect.Method render = null;
              try {
                final Class<?> clazz = Class.forName(noteType.getNoteTemplate());
                render = clazz.getDeclaredMethod("render", noteType.getNoteView(), List.class, String.class, com.mapped.publisher.view.TabType.Bound.class);
              } catch (NoSuchMethodException e) {
                e.printStackTrace();
              } catch (ClassNotFoundException e) {
                e.printStackTrace();
              }
              if (render != null) {
                Html html = null;
                html = (Html) render.invoke(null, cast(view, noteType.getNoteView()), detailsViews, tripDetailId, getTabType(tripDetailId));

                return ok(html);
              }
            } catch (IllegalAccessException e) {
              e.printStackTrace();
            } catch (InvocationTargetException e) {
              e.printStackTrace();
            }

          }
        }
      }
    }
    BaseView baseView = new BaseView();
    baseView.message = "Permission Error - you are not authorized to access this page";
    return ok(views.html.common.message.render(baseView));
  }


  @With({Authenticated.class, Authorized.class})
  public Result getSummary(String tripId, String tripDetailId, String inStartDate) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    BaseView baseView = new BaseView();

    if (tripId == null ||
            tripId.length() == 0) {
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    Trip tripModel = Trip.find.byId(tripId);
    if (tripModel == null || !SecurityMgr.getTripAccessLevel(tripModel, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND)) {
      baseView.message = "Cannot access this guide  - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    Company c = Company.find.byId(tripModel.getCmpyid());
    TripNote tripNote = processTripSummary(tripModel, c, sessionMgr.getCredentials().getAccount(), tripDetailId, null, offerService, umappedCityLookup);
    if (tripNote != null) {
      String summaryMsg = " Updated Successfully";
      if (tripNote.getCreatedTimestamp() == tripNote.getLastUpdatedTimestamp()) {
        summaryMsg = " Added Successfully";
      }
      flash(SessionConstants.SESSION_PARAM_MSG, tripNote.getName() + summaryMsg);
      //go to the itinerary page

      return redirect(routes.BookingController.bookings(tripModel.tripid, new TabType.Bound(TabType.ITINERARY), null));
    } else {
      baseView.message = "System Error - Please contact support@umapped.com";
      return ok(views.html.common.message.render(baseView));
    }


  }

  public static TripNote processTripSummary(Trip trip, Company c, Account account, String tripDetailId, String layout, OfferService offerService, UMappedCityLookup umappedCityLookup) {
    if (trip == null) {
      return null;
    }
    TripBookingView tripBookingView = new TripBookingView();

    List<TripDetail> tripDetails = TripDetail.findActiveByTripId(trip.getTripid());
    if (tripDetails != null) {
      for (TripDetail tripDetail : tripDetails) {
        tripBookingView.addBookingDetailView(tripDetail.detailtypeid, BookingController.buildBookingView(trip.getTripid(), tripDetail, false));
      }
    }

    try {
      TripNote tripNote = null;
      tripNote = TripNote.find.byId(Long.parseLong(trip.getTripid()));
      Long tripNoteId = Long.parseLong(trip.getTripid());
      String summaryMsg = "";
      //NEW NOTE
      if (tripNote == null) {

        tripNote = TripNote.buildTripNote(trip, account.getLegacyId());
        tripNote.setNoteId(Long.parseLong(trip.getTripid()));
        int maxRow = TripNote.tripNoteCount(trip.getTripid());

        //if this is a new note and we came from a booking, set the default booking to the one passed so it is lined properly
        if ((tripNote.getTripDetailId() == null || tripNote.getTripDetailId().trim().length() == 0) && tripDetailId != null && tripDetailId.trim().length() > 0) {
          tripNote.setTripDetailId(tripDetailId);
        }
      } else { //MODIFY EXISTING PAGE (DESTINATION GUIDE)

        if (tripNote == null || !trip.tripid.equals(tripNote.getTrip().getTripid())) {
          return null;
        }
        if (tripNote.getStatus() != APPConstants.STATUS_ACTIVE) {
          tripNote.setStatus(APPConstants.STATUS_ACTIVE);
        }
      }

     if (layout != null && layout.contains(APPConstants.CMPY_TAG_SUMMARY_NOTE_1)) {
        StringBuilder lastNamesStr = new StringBuilder();
        StringBuilder cityStr = new StringBuilder();
        StringBuilder pnrStr = new StringBuilder();

        try {
          ItineraryAnalyzeResult analyzeResult = offerService.analyzeTrip(trip.getTripid());
          List<Location> cities = new ArrayList<>();
          if (analyzeResult != null) {
            Location startLocation = null;
            Location endLocation = null;
            for (TripSegmentAnalyzeResult result : analyzeResult.getSegmentResults()) {
              if (result.getSegment().getStartLocation() != null) {
                if (startLocation == null) {
                  startLocation = result.getSegment().getStartLocation();
                }
                if (!cities.contains(result.getSegment().getStartLocation()))
                  cities.add(result.getSegment().getStartLocation());
              }
              if (result.getSegment().getEndLocation() != null) {
                if (endLocation == null) {
                  endLocation = result.getSegment().getEndLocation();
                }
                if (!cities.contains(result.getSegment().getEndLocation()))
                  cities.add(result.getSegment().getEndLocation());
              }
            }
            if (startLocation != null) {
              cities.remove(startLocation);
            }
            if (endLocation != null) {
              cities.remove(endLocation);
            }
            for (Location l : cities) {
              UMappedCity uMappedCity = umappedCityLookup.getCity(l.getGeoLocation());
              if (uMappedCity != null && !cityStr.toString().contains(uMappedCity.getName())) {
                if (!cityStr.toString().isEmpty()) {
                  cityStr.append(", ");
                }
                cityStr.append(uMappedCity.getName());
              }

            }
          }
        } catch (Exception e) {
          Log.err("Process trip summary template 1 error: " + trip.getTripid(), e);
        }
        if (tripDetails != null) {
          for (TripDetail td : tripDetails) {
            if (td.getReservation() != null && td.getReservation().getTravelers() != null) {
              for (UmTraveler t : td.getReservation().getTravelers()) {
                if ((t.getGivenName() != null && !t.getGivenName().isEmpty()) || (t.getFamilyName() != null && !t.getFamilyName().isEmpty()) ) {
                  String name = null;
                  if (t.getGivenName() != null && !t.getGivenName().isEmpty()) {
                    name = t.getGivenName();
                  }
                  if (t.getFamilyName() != null && !t.getFamilyName().isEmpty() ) {
                    if (name != null) {
                      name += " " + t.getFamilyName();
                    } else {
                      name = t.getFamilyName();
                    }
                  }
                  if (name != null && !lastNamesStr.toString().contains(name)) {
                    if (!lastNamesStr.toString().isEmpty()) {
                      lastNamesStr.append("/");
                    }
                    lastNamesStr.append(name);
                  }
                }
              }
            }
            if (td.getBkApiSrcId() != null) {
              BookingRS apiRec = BookingAPIMgr.findByPk(td.getBkApiSrcId());
              if (apiRec != null && !pnrStr.toString().contains(apiRec.getSrcRecLocator())) {
                if (!pnrStr.toString().isEmpty()) {
                  pnrStr.append("/");
                }
                pnrStr.append(apiRec.getSrcRecLocator());
              }
            }
          }
        }
        if (lastNamesStr.toString().isEmpty()) {
          tripNote.setName("Trip Summary");
        } else {
          tripNote.setName(lastNamesStr.toString());
        }
        tripNote.setIntro(views.html.trip.tripSummary1.render(Utils.formatDatePrint(trip.getStarttimestamp()), pnrStr.toString(), cityStr.toString(), tripBookingView).toString());
      } else  if (c != null && c.getName().toLowerCase().contains("indagare")) {
       tripNote.setName("Itinerary Overview");
       tripNote.setIntro(views.html.external.indagare.trip.tripSummary.render(tripBookingView).toString());

     } else {
        tripNote.setName("Trip Summary");
        tripNote.setIntro(views.html.trip.tripSummary.render(tripBookingView).toString());
      }
      //check if we need to link to a booking
      tripNote.setNoteTimestamp(null);
      tripNote.setTripDetailId(null);

      tripNote.setCreatedTimestamp(trip.getCreatedtimestamp());

      tripNote.setModifiedBy(account.getLegacyId());
      tripNote.setLastUpdatedTimestamp(System.currentTimeMillis());
      tripNote.setRank(0);


      tripNote.save();


      try {
        audit:
        {

          if (trip != null) {
            TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_BOOKING_NOTE,
                    AuditActionType.MODIFY,
                    AuditActorType.WEB_USER)
                    .withTrip(trip)
                    .withCmpyid(trip.getCmpyid())
                    .withUserid(account.getLegacyId());

            ((AuditTripDocCustom) ta.getDetails()).withGuideId(String.valueOf(tripNote.getNoteId()))
                    .withName(tripNote.getName())
                    .withCoverImage("")
                    .withText("Added New Note " + tripNote.getName());
            ta.save();
          } else {
            Log.log(LogLevel.ERROR, "Trip ID can not be determined while adding new noteaudit:" + tripNote.getNoteId());
          }
        }
      } catch (Exception e) {

      }

      invalidateCache(trip.getTripid());

      return tripNote;
    } catch (Exception e) {
      Log.log(LogLevel.DEBUG, "Error inserting note", e);
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
    }
    return null;
  }



  @With({Authenticated.class, Authorized.class})
  public Result createNote(String tripDetailId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<TripNoteForm> tripNoteForm = formFactory.form(TripNoteForm.class);
    TripNoteForm tripNoteInfo = tripNoteForm.bindFromRequest().get();

    //use bean validator framework
    if (
        tripNoteInfo.getInTripId() == null ||
        tripNoteInfo.getInTripId().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    Trip tripModel = Trip.find.byId(tripNoteInfo.getInTripId());
    //TODO: Serguei: canEditDestination is incorrect logic here, it must be can access destination and then either can
    //TODO: Serguei: edit the page or create new page in the exisiting guide.
    if (tripModel == null || !SecurityMgr.getTripAccessLevel(tripModel, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND)) {
      BaseView baseView = new BaseView();
      baseView.message = "Cannot access this guide  - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      Ebean.beginTransaction();
      TripNote tripNote = null;
      //NEW NOTE
      if (tripNoteInfo.getInTripNoteId() == null || tripNoteInfo.getInTripNoteId() == 0) {

        tripNote = TripNote.buildTripNote(tripModel, sessionMgr.getUserId());
        int maxRow = TripNote.tripNoteCount(tripModel.tripid);
        tripNote.setRank(++maxRow);

        //if this is a new note and we came from a booking, set the default booking to the one passed so it is lined properly
        if ((tripNoteInfo.getInTripDetailsId() == null || tripNoteInfo.getInTripDetailsId().trim().length() == 0) && tripDetailId != null && tripDetailId.trim().length() > 0) {
          tripNoteInfo.setInTripDetailsId(tripDetailId);
        }
      }
      else { //MODIFY EXISTING PAGE (DESTINATION GUIDE)
        tripNote = TripNote.find.byId(tripNoteInfo.getInTripNoteId());
        if (tripNote == null ||
                (tripNote.getType() != null && tripNote.getType() == TripNote.NoteType.CONTENT) ||
            tripNote.getStatus() != APPConstants.STATUS_ACTIVE ||
            !tripModel.tripid.equals(tripNote.getTrip().getTripid())) {
          BaseView baseView = new BaseView();
          baseView.message = "System Error - No permission to modify this page";
          Ebean.rollbackTransaction();

          return ok(views.html.common.message.render(baseView));
        }
      }

      //check if we need to link to a booking
      tripNote.setNoteTimestamp(null);

      if (tripNoteInfo.getInTripDetailsId() != null && tripNoteInfo.getInTripDetailsId().trim().length() == 0) {
        //none selected - reset the detailId
        tripNote.setTripDetailId(null);
      } else if (tripNoteInfo.getInTripDetailsId() != null && tripNoteInfo.getInTripDetailsId().trim().length() > 1) {
        TripDetail td = TripDetail.findByPK(tripNoteInfo.getInTripDetailsId());
        if (td != null && td.getStatus() != APPConstants.STATUS_DELETED && td.getTripid().equals(tripModel.getTripid()
        )) {
          tripNote.setTripDetailId(td.getDetailsid());
        }
      }
      //set date if applicable
      if (tripNoteInfo.getInTripNoteDate() != null && tripNoteInfo.getInTripNoteDate().trim().length() > 0 && (tripNote.getTripDetailId() == null || tripNote.getTripDetailId().trim().length() == 0)) {
        StringBuilder sb = new StringBuilder();
        sb.append(tripNoteInfo.getInTripNoteDate());
        if (tripNoteInfo.getInTripNoteTime() != null && tripNoteInfo.getInTripNoteTime().trim().length() > 0) {
          sb.append(" ");
          sb.append(tripNoteInfo.getInTripNoteTime());
        }

        long timestamp = Utils.getMilliSecs(sb.toString());
        if (timestamp > 0) {
          tripNote.setNoteTimestamp(timestamp);
        }
      }
      tripNote.setTag(tripNoteInfo.getInTripNoteTag());

      //Default Note
      if (tripNoteInfo.inNoteType == TripNote.NoteType.DEFAULT) {
        if (tripNoteInfo.getInTripNoteLandmark() != null && !tripNoteInfo.getInTripNoteLandmark().equals("Enter a Landmark")) {
          tripNote.setLandmark(tripNoteInfo.getInTripNoteLandmark());
        } else {
          tripNote.setLandmark(null);
        }
        tripNote.setStreetAddr(tripNoteInfo.getInTripNoteAddr());
        tripNote.setCity(tripNoteInfo.getInTripNoteCity());
        tripNote.setCountry(tripNoteInfo.getInTripNoteCountry());
        tripNote.setIntro(tripNoteInfo.getInTripNoteIntro());
        tripNote.setDescription(tripNoteInfo.getInTripNoteDesc());

        tripNote.setZipcode(tripNoteInfo.getInTripNoteZip());

        if (tripNoteInfo.getInTripNoteLocLat() != null) {
          try {
            float lat = Float.parseFloat(tripNoteInfo.getInTripNoteLocLat());
            tripNote.setLocLat(lat);
          } catch (NumberFormatException ne) {
            tripNote.setLocLat(0);

          }
        } else {
          tripNote.setLocLat(0);
        }

        if (tripNoteInfo.getInTripNoteLocLong() != null) {
          try {
            float locLong = Float.parseFloat(tripNoteInfo.getInTripNoteLocLong());
            tripNote.setLocLong(locLong);
          } catch (NumberFormatException ne) {
            tripNote.setLocLong(0);

          }
        } else {
          tripNote.setLocLong(0);
        }
        tripNote.setName(tripNoteInfo.getInTripNoteName());

      } else { //All other Types of Notes

        tripNote.setLocLat(0);
        tripNote.setLocLat(0);
        tripNote.setLocLong(0);
        tripNote.setLocLong(0);

        TripNote.NoteType noteType = TripNote.NoteType.valueOf(tripNoteInfo.inNoteType.name());

        //Get and populate note based on NoteType
        StructuredNote note = noteType.getValue().populateNoteByType(tripNote, tripNoteInfo);

        if (note.name != null || !note.name.isEmpty()) {
          tripNote.setName(note.name);
        }
        tripNote.setNote(note); //Also sets the Intro while setting the note
      }

      tripNote.setModifiedBy(sessionMgr.getUserId());
      tripNote.setLastUpdatedTimestamp(System.currentTimeMillis());
      tripNote.setType(tripNoteInfo.inNoteType);

      if (tripNote.getIntro() != null) {
        Matcher m = Pattern.compile("Tel:[+0-9().\\-]{3,}(&|<|[A-Za-z]|\\s)").matcher(tripNote.getIntro());

        while (m.find()) {
          String tel = m.group().substring(0, m.group().length() - 1);
          tripNote.setIntro(tripNote.getIntro().replace(tel, "<a href=\"" + tel.replace("Tel", "tel").trim() + "\" target=\"_blank\">" + tel.replace("Tel:", "Tel: ").trim() + "</a>"));
        }
      }

      tripNote.save();

      Ebean.commitTransaction();

      try {
        audit:
        {

          if (tripModel != null) {
            TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_BOOKING_NOTE,
                                                 AuditActionType.MODIFY,
                                                 AuditActorType.WEB_USER)
                                    .withTrip(tripModel)
                                    .withCmpyid(tripModel.cmpyid)
                                    .withUserid(sessionMgr.getUserId());

            ((AuditTripDocCustom) ta.getDetails()).withGuideId(String.valueOf(tripNote.getNoteId()))
                                                  .withName(tripNote.getName())
                                                  .withCoverImage("")
                                                  .withText("Added New Note " + tripNote.getName());
            ta.save();
          }
          else {
            Log.log(LogLevel.ERROR, "Trip ID can not be determined while adding new noteaudit:" + tripNote.getNoteId());
          }
        }
      } catch (Exception e) {

      }

      invalidateCache(tripModel.tripid);
      flash(SessionConstants.SESSION_PARAM_SCROLL_TO_DIV, String.valueOf(tripNote.getNoteId()));

      if (tripNoteInfo.getInTripNoteId() == null || tripNoteInfo.getInTripNoteId() == 0) {
        flash(SessionConstants.SESSION_PARAM_MSG, tripNote.getName() + " added successfully");
        //stay on the page
        return redirect(routes.BookingNoteController.getStructuredNote(tripModel.tripid, tripNote.getNoteId(),
                                                             tripNote.getTripDetailId(), null, tripNote.getType().name()));
      }
      else {
        flash(SessionConstants.SESSION_PARAM_MSG, tripNote.getName() + " updated successfully");
        //go to the itinerary page
        flash(SessionConstants.SESSION_PARAM_SCROLL_TO_DIV, String.valueOf(tripNote.getNoteId()));

        return redirect(routes.BookingController.bookings(tripModel.tripid, new TabType.Bound(TabType.ITINERARY), null));
      }
    }
    catch (Exception e) {
      e.printStackTrace();

      Ebean.rollbackTransaction();
      Log.log(LogLevel.DEBUG, "Error inserting note", e);
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

  }

  @With({Authenticated.class, Authorized.class})
  public Result copyPage(String tripId, String destGuideId, String tripDetailId, String noteDate) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Trip tripModel = Trip.findByPK(tripId);
    if (tripModel != null && SecurityMgr.getTripAccessLevel(tripModel, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND)) {
      Destination dest = null;
      DestinationGuide guide = DestinationGuide.find.byId(destGuideId);
      if (guide != null) {
        dest = Destination.find.byId(guide.getDestinationid());
      }
      if (dest != null && guide != null && guide.status == APPConstants.STATUS_ACTIVE) {
        long currentTime = System.currentTimeMillis();
        try {
          Ebean.beginTransaction();
          int maxRow = TripNote.tripNoteCount(tripId);

          String origGuidePK = new String(guide.getDestinationguideid());
          TripNote newGuide = TripNote.buildTripNote(tripModel, sessionMgr.getUserId());

          newGuide.setCity(guide.city);
          newGuide.setCountry(guide.country);

          StringBuilder sb = new StringBuilder();
          if (guide.getIntro() != null && !guide.getIntro().isEmpty()) {
            sb.append(guide.getIntro());
          }
          if (guide.getDescription() != null && !guide.getDescription().isEmpty()) {
            if (!sb.toString().isEmpty()) {
              sb.append("<br/>");
            }
            sb.append(guide.getDescription());
          }
          newGuide.setIntro(sb.toString());

          newGuide.setLandmark(guide.landmark);
          newGuide.setLocLat(guide.loclat);
          newGuide.setLocLong(guide.loclong);
          newGuide.setName(guide.name);
          newGuide.setRank(maxRow + 1);
          newGuide.setState(guide.state);
          newGuide.setStreetAddr(guide.streetaddr1);
          newGuide.setTag(guide.tag);
          newGuide.setZipcode(guide.zipcode);
          newGuide.setPageId(guide.getPageId());

          //set date if needed
          if (noteDate != null && noteDate.trim().length() > 0) {
            try {
              DateUtils du = new DateUtils();
              Date d = du.parseDate(noteDate, "EEE MMM dd, yyyy");
              newGuide.setNoteTimestamp(d.getTime());
            }
            catch (Exception e) {
            }
          }

          if (tripDetailId != null && tripDetailId.trim().length() > 0) {
            TripDetail td = TripDetail.findByPK(tripDetailId);
            if (td != null && td.status == APPConstants.STATUS_ACTIVE && td.tripid.equals(tripModel.tripid)) {
              newGuide.setTripDetailId(td.getDetailsid());
            }
          }

          newGuide.save();

          //save any attachments
          List<DestinationGuideAttach> guideFiles = DestinationGuideAttach.findActiveByDestGuide(origGuidePK);
          for (DestinationGuideAttach guideFile : guideFiles) {
            TripNoteAttach.duplicateFromGuideAttach(guideFile, newGuide, sessionMgr.getUserId());
          }

          Ebean.commitTransaction();

          try {
            TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_BOOKING_NOTE,
                                                 AuditActionType.COPY,
                                                 AuditActorType.WEB_USER)
                                    .withTrip(tripModel)
                                    .withCmpyid(tripModel.cmpyid)
                                    .withUserid(sessionMgr.getUserId());

            ((AuditTripDocCustom) ta.getDetails()).withGuideId(String.valueOf(newGuide.getNoteId()))
                                                  .withName(newGuide.getName())
                                                  .withCoverImage("")
                                                  .withText("Copied Notes " + newGuide.getName());

            ta.save();;
          } catch (Exception e) {

          }

          invalidateCache(tripId);
          flash(SessionConstants.SESSION_PARAM_MSG, newGuide.getName() + " copied successfully");

          return redirect(routes.BookingNoteController.getNote(tripId, newGuide.getNoteId(), null, null));
        }
        catch (Exception e) {
          Ebean.rollbackTransaction();
        }
      }
    }
    BaseView baseView = new BaseView();
    baseView.message = "System Error - please resubmit";
    return ok(views.html.common.message.render(baseView));


  }


  @With({Authenticated.class, Authorized.class})
  public Result deleteNote(String tripId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Long tripNoteId = null;
    final DynamicForm form = formFactory.form().bindFromRequest();
    String inNoteId = form.get("inTripNoteId");
    try {
      tripNoteId = Long.parseLong(inNoteId);
    } catch (Exception e) {

    }

    Trip tripModel = Trip.findByPK(tripId);
    TripNote tripNote = TripNote.find.byId(tripNoteId);

    if (!canEditNote(tripModel, tripNote, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      tripNote.setStatus(APPConstants.STATUS_DELETED);
      tripNote.setLastUpdatedTimestamp(System.currentTimeMillis());
      tripNote.setModifiedBy(sessionMgr.getUserId());
      tripNote.save();

      try {
        audit:
        {

          if (tripModel != null) {
            TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_BOOKING_NOTE,
                                                 AuditActionType.DELETE,
                                                 AuditActorType.WEB_USER)
                                    .withTrip(tripModel)
                                    .withCmpyid(tripModel.cmpyid)
                                    .withUserid(sessionMgr.getUserId());

            ((AuditTripDocCustom) ta.getDetails()).withGuideId(String.valueOf(tripNote.getNoteId())).withName(tripNote.getName());

            ta.save();;
          }
          else {
            Log.log(LogLevel.ERROR, "Trip ID can not be determined while adding new noteaudit:" + tripNote.getNoteId());
          }
        }

        invalidateCache(tripId);


        flash(SessionConstants.SESSION_PARAM_MSG, tripNote.getName() + " deleted successfully");
        return redirect(routes.BookingController.bookings(tripId, new TabType.Bound(TabType.ITINERARY), null));
      } catch (Exception e) {
        e.printStackTrace();
      }

    } catch (Exception e1) {

    }
    BaseView baseView = new BaseView();
    baseView.message = "Errors on the form - please resubmit";
    return ok(views.html.common.message.render(baseView));

  }


  @With({Authenticated.class, Authorized.class})
  public Result createPendingPhoto(String tripId, Long tripNoteId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    //bind html form to form bean
    Form<FileUploadForm> form = formFactory.form(FileUploadForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_WRONG_DATA, "Error. Please refresh page and resubmit");
    }

    FileUploadForm destPhotoInfo = form.get();
    String uploadType = destPhotoInfo.getInType();

    if (uploadType.equals("IMG")) {
      if (destPhotoInfo.getInDestPhotoName() == null || destPhotoInfo.getInDestPhotoName().length() == 0) {
        return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_MISSING_DATA, "Image missing. Please refresh page and " +
            "resubmit");
      }
    } else if (uploadType.equals("FILE")) {
      if (destPhotoInfo.getInDestFileName() == null || destPhotoInfo.getInDestFileName().length() == 0) {
        return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_MISSING_DATA,  "File missing. Please refresh page and " +
            "resubmit");
      }
    }


    Account a;
    if(sessionMgr.getAccount().isPresent()) {
      a = sessionMgr.getAccount().get();
    } else {
      a = Account.find.byId(sessionMgr.getAccountId());
    }

    Trip tripModel = Trip.findByPK(tripId);
    TripNote tripNote = TripNote.find.byId(tripNoteId);
    if (!canEditNote(tripModel, tripNote, sessionMgr)) {
      return BaseJsonResponse.codeResponse(ReturnCode.AUTH_TRIP_FAIL, "No authorization to edit trip note");
    }

    TripNoteAttach attach = TripNoteAttach.build(tripNote, sessionMgr.getUserId());
    String nextUrl = routes.BookingNoteController.uploadPhotoSuccess(tripId, tripNoteId, attach.getPk()).url();

    ObjectNode result = Json.newObject();

    if (uploadType.equals("IMG")) {
      FileImage fileImage = ImageController.uploadHelper(attach, destPhotoInfo.getInDestPhotoName(), a, result, nextUrl);

      if(fileImage != null) {
        attach.setName(fileImage.getFilename());
        attach.setFileImage(fileImage);
        attach.setAttachUrl(fileImage.getUrl());
        attach.setAttachName(fileImage.getFilename());
        attach.setStatus(APPConstants.STATUS_PENDING);
        attach.setAttachType(PageAttachType.PHOTO_LINK);
        int maxRow = TripNoteAttach.maxPageRankByNoteId(tripNoteId, attach.getAttachType().getStrVal());
        attach.setRank(++maxRow);
        attach.setComments(destPhotoInfo.getInDestPhotoCaption());
        attach.update();
        return ok(result);
      }
    } else if (uploadType.equals("FILE")) {
      FileInfo file = FileController.uploadHelper(attach, destPhotoInfo.getInDestFileName(), a, result, nextUrl);

      if (file != null) {
        attach.setName(destPhotoInfo.getInDestFileName());
        attach.setFileInfo(file);
        attach.setAttachUrl(file.getUrl());
        attach.setAttachName(file.getFilename());
        attach.setStatus(APPConstants.STATUS_PENDING);
        attach.setAttachType(PageAttachType.FILE_LINK);
        int maxRow = TripNoteAttach.maxPageRankByNoteId(tripNoteId, attach.getAttachType().getStrVal());
        attach.setRank(++maxRow);
        attach.setComments(destPhotoInfo.getInDestFileCaption());
        attach.update();
        return ok(result);
      }
    }

    return BaseJsonResponse.codeResponse(ReturnCode.STORAGE_S3_UPLOAD, "3rd party service provider error.");
  }

  @With({Authenticated.class, Authorized.class})
  public Result uploadPhotoSuccess(String tripId, Long tripNoteId, Long attachId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    //bind html form to form bean
    if (attachId == null || attachId.longValue() == 0) {
      return UserMessage.message(ReturnCode.HTTP_REQ_MISSING_DATA, "Browser did not submit all required data.");
    }

    Trip tripModel = Trip.findByPK(tripId);
    TripNote tripNote = TripNote.find.byId(tripNoteId);

    if (!canEditNote(tripModel, tripNote, sessionMgr)) {
      return UserMessage.message(ReturnCode.AUTH_TRIP_FAIL, "No Authorization to upload image.");
    }

    try {
      TripNoteAttach attach = TripNoteAttach.find.byId(attachId);
      if (attach == null || attach.getTripNote().getNoteId().longValue() != tripNote.getNoteId().longValue()) {
        return UserMessage.message(ReturnCode.LOGICAL_ERROR, "Image ID mismatch. Please resubmit.");
      }

      //Error happened during previous step of confirmation
      if(attach.getFileImage() == null && attach.getFileInfo() == null) {
        attach.delete();
        return UserMessage.message(ReturnCode.LOGICAL_ERROR, "File upload failure. Please retry or contact support.");
      }

      String successMsg = "Photo uploaded successfully";

      if (attach.getAttachType() == PageAttachType.FILE_LINK) {
        successMsg = "File uploaded successfully";
      }

      Ebean.beginTransaction();
      attach.setStatus(APPConstants.STATUS_ACTIVE);
      attach.markModified(sessionMgr.getUserId());
      attach.save();
      Ebean.commitTransaction();
      invalidateCache(tripId);
      flash(SessionConstants.SESSION_PARAM_MSG, successMsg);
      return redirect(routes.BookingNoteController.getNote(tripId, tripNoteId, null, null));
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result reorderPhotos(String tripId, Long tripNoteId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    final DynamicForm form = formFactory.form().bindFromRequest();
    String inDestFileIds = form.get("inDestFileIds");
    Trip tripModel = Trip.findByPK(tripId);
    TripNote tripNote = TripNote.find.byId(tripNoteId);

    if (!canEditNote(tripModel, tripNote, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    if (inDestFileIds != null && inDestFileIds.trim().length() > 0) {
      String[] destFileIds = inDestFileIds.split(",");
      try {
        Ebean.beginTransaction();
        int count = 0;
        for (String id : destFileIds) {
          TripNoteAttach file = TripNoteAttach.find.byId(Long.parseLong(id));
          if (file != null && file.status == APPConstants.STATUS_ACTIVE &&
              file.getTripNote().getNoteId().longValue() == tripNoteId.longValue()) {
            file.setRank(count);
            file.setLastUpdatedTimestamp(System.currentTimeMillis());
            file.setModifiedBy(sessionMgr.getUserId());
            file.save();
          }
          count++;
        }
        Ebean.commitTransaction();
        BaseView baseView = new BaseView();
        invalidateCache(tripId);
        return ok(views.html.common.message.render(baseView));

      }
      catch (Exception e) {
        Ebean.rollbackTransaction();
        Log.log(LogLevel.ERROR, "reorderPhotos: " + tripNoteId, e);

      }

    }
    BaseView baseView = new BaseView();
    baseView.message = "Cannot reorder Photos";
    return ok(views.html.common.message.render(baseView));
  }


  @With({Authenticated.class, Authorized.class})
  public Result reorderNotes() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    final DynamicForm form = formFactory.form().bindFromRequest();
    String tripId = form.get("tripId");
    String inNoteIds = form.get("inNoteIds");
    String inTabType = form.get("inSelectedTab");
    String inScrollTo = form.get("inScrollTo");

    if (inNoteIds != null && tripId != null && inNoteIds.trim().length() > 0) {
      String[] noteIds = inNoteIds.split(",");
      Trip tripModel = Trip.find.byId(tripId);
      if (tripModel != null) {
        SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
        if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND)) {
          BaseView baseView = new BaseView();
          baseView.message = "System Error - You are not allowed to reorder the notes";
          return ok(views.html.common.message.render(baseView));
        }
        //save all the attachments incrementing the modified timestamp by 1 tick for ordering purposes.
        long modifiedTimestamp = System.currentTimeMillis();
        List<TripNote>  tripNotes = TripNote.getNotesByTripId(tripId);
        Map<Long, TripNote> tripNoteMap = new HashMap<>();



        if (tripNotes != null) {
          for (TripNote ta:tripNotes) {
            tripNoteMap.put(ta.getNoteId(), ta);
          }
        }
        //sometimes we are reordering within a day in the itinerary - the passed in list of id will also contain bookings
        boolean reorderingWithinItinerary = false;
        for (String s : noteIds) {
          TripNote ta = tripNoteMap.get(Long.parseLong(s));
          if (ta == null) {
            //this is not a note, so we are parsing trip detail also
            reorderingWithinItinerary = true;
          }
        }

        String tripDetailId = null;

        TripDetail tripDetail = null;
        try {
          Ebean.beginTransaction();
          int rank = 0;
          for (String s : noteIds) {
            TripNote ta = tripNoteMap.get(Long.parseLong(s));
            if (ta != null) {
              tripDetailId =ta.getTripDetailId();
              ta.setRank(rank++);

              //we will relink if the tripDetailId on the note does not match
              if (reorderingWithinItinerary) {
                if (tripDetail != null) {
                  ta.setTripDetailId(tripDetail.getDetailsid());
                  ta.setNoteTimestamp(null);
                }
                else if (tripDetail == null && ta.getTripDetailId() != null) {
                  //we moved a note from a booking to a standalone - so we need to reset the id and make sure we set the date properly
                  TripDetail td = TripDetail.findByPK(ta.getTripDetailId());
                  if (td != null) {
                    ta.setTripDetailId(null);
                    DateTime dt = new DateTime(td.starttimestamp);
                    ta.setNoteTimestamp(dt.withTimeAtStartOfDay().getMillis());
                  }
                }
              }

              ta.save();
            } else {
              //this is not a note, let's try to get the trip detail;
              tripDetail = TripDetail.findByPK(s);
              if (tripDetail != null && tripDetail.getStatus() == 0 && tripDetail.getTripid().equals(tripModel.tripid)) {
                tripDetail.setRank(rank++);
                tripDetail.save();
                BookingController.invalidateCache(tripDetail.getTripid(), tripDetail.getDetailsid());
              }
            }
          }
          invalidateCache(tripModel.tripid);
          Ebean.commitTransaction();
          if (inScrollTo != null && inScrollTo.trim().length() > 0) {
            flash(SessionConstants.SESSION_PARAM_SCROLL_TO_DIV, inScrollTo);
          }
          if(inTabType.equals(TabType.ITINERARY.name())) {
            return redirect(routes.BookingController.bookings(tripId, new TabType.Bound(TabType.ITINERARY), null));
          } else {
            return redirect(routes.BookingController.bookings(tripId, getTabType(tripDetailId), tripDetailId));
          }

        } catch (Exception e) {
          Ebean.rollbackTransaction();
          Log.log(LogLevel.ERROR, "reorder Attachments: " + tripId, e);
        }

      }
    }

    BaseView baseView = new BaseView();
    baseView.message = "System Error - You are not allowed to re-order attachment";
    return ok(views.html.common.message.render(baseView));

  }

  @With({Authenticated.class, Authorized.class})
  public Result newLink(String tripId, Long tripNoteId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<DestinationGuideUrlForm> destGuideUrlForm = formFactory.form(DestinationGuideUrlForm.class);
    DestinationGuideUrlForm destGuideUrlInfo = destGuideUrlForm.bindFromRequest().get();

    if (tripId == null || tripNoteId == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    Trip tripModel = Trip.findByPK(tripId);
    TripNote tripNote = TripNote.find.byId(tripNoteId);

    if (!canEditNote(tripModel, tripNote, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    //use bean validator framework
    if (destGuideUrlInfo.getInDestGuideUrlTitle() == null ||
        destGuideUrlInfo.getInDestGuideUrlTitle().length() == 0 ||
        destGuideUrlInfo.getInDestGuideUrl() == null ||
        destGuideUrlInfo.getInDestGuideUrl().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Invalid data - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }


    try {
      Ebean.beginTransaction();
      TripNoteAttach attach = null;
      attach = new TripNoteAttach();

      attach.setFileId(DBConnectionMgr.getUniqueLongId());
      attach.setTripNote(tripNote);
      attach.setStatus(APPConstants.STATUS_ACTIVE);
      attach.setCreatedBy(sessionMgr.getUserId());
      attach.setCreatedTimestamp(System.currentTimeMillis());

      //TODO validate youtube and vimeo link for video id

      attach.setAttachType(PageAttachType.WEB_LINK);

      if (destGuideUrlInfo.getInDestGuideUrlType() != null) {
        try {
          if (destGuideUrlInfo.getInDestGuideUrlType().equals(PageAttachType.VIDEO_LINK.getStrVal())) {
            if (destGuideUrlInfo.getInDestGuideUrl() != null &&
                !destGuideUrlInfo.getInDestGuideUrl().toLowerCase().contains("youtube.com") &&
                !destGuideUrlInfo.getInDestGuideUrl().toLowerCase().contains("youtu.be") &&
                 destGuideUrlInfo.getInDestGuideUrl().toLowerCase().contains("vimeo.com")) {
              BaseView baseView = new BaseView();
              baseView.message = "System Error - please resubmit";
              return ok(views.html.common.message.render(baseView));
            }
            else {
              attach.setAttachType(PageAttachType.VIDEO_LINK);
            }
          }
        }
        catch (NumberFormatException ne) {
          ne.printStackTrace();
        }
      }
      //set rank
      int maxRow = TripNoteAttach.maxPageRankByNoteId(tripNote.getNoteId(), attach.getAttachType().getStrVal());
      attach.setRank(++maxRow);

      attach.setName(destGuideUrlInfo.getInDestGuideUrlTitle());
      if (destGuideUrlInfo.getInDestGuideUrl() != null && !destGuideUrlInfo.getInDestGuideUrl()
                                                                           .toLowerCase()
                                                                           .startsWith("http")) {
        destGuideUrlInfo.setInDestGuideUrl("http://" + destGuideUrlInfo.getInDestGuideUrl());
      }

      attach.setAttachUrl(destGuideUrlInfo.getInDestGuideUrl());
      attach.setAttachName(destGuideUrlInfo.getInDestGuideUrlTitle());
      attach.setComments(destGuideUrlInfo.getInDestGuideUrlCaption());
      attach.setLastUpdatedTimestamp(System.currentTimeMillis());
      attach.setModifiedBy(sessionMgr.getUserId());
      attach.save();
      Ebean.commitTransaction();

      flash(SessionConstants.SESSION_PARAM_MSG, attach.getAttachName() + " added successfully");


      //save new id for retieval
      invalidateCache(tripId);


      return redirect(routes.BookingNoteController.getNote(tripId, tripNoteId, null, null));

    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

  }


  @With({Authenticated.class, Authorized.class})
  public Result deleteLink(String tripId, Long tripNoteId, Long tripAttachId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    if (tripId == null || tripNoteId == null || tripAttachId == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    Trip tripModel = Trip.findByPK(tripId);
    TripNote tripNote = TripNote.find.byId(tripNoteId);
    TripNoteAttach tripAttach = TripNoteAttach.find.byId(tripAttachId);

    if (!canEditNote(tripModel, tripNote, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    if (tripAttach == null || tripAttach.getTripNote().getNoteId().longValue() != tripNote.getNoteId().longValue()) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    tripAttach.setStatus(APPConstants.STATUS_DELETED);
    tripAttach.setLastUpdatedTimestamp(System.currentTimeMillis());
    tripAttach.setModifiedBy(sessionMgr.getUserId());
    tripAttach.save();

    invalidateCache(tripId);
    flash(SessionConstants.SESSION_PARAM_MSG, tripAttach.getAttachName() + " deleted successfully");
    if (tripNote.getType() == TripNote.NoteType.DEFAULT) {
      return redirect(routes.BookingNoteController.getNote(tripId,tripNoteId, null, null));
    }
    return redirect(routes.BookingNoteController.getStructuredNote(tripId, tripNoteId, tripNote.getTripDetailId(), null, tripNote.getType().name()));
  }

  public static void invalidateCache(String tripId) {
    if (tripId != null) {
      try {
        BookingController.invalidateCache(tripId, null); //Will wipe everything under the trip
      }
      catch (Exception e) {
        Log.err("BookingNoteController: invalidateCache() trip: " + tripId, e);
      }
    }
  }

  /**
   *
   * @param tripModel
   * @param sessionMgr
   * @return ! never null !
   */
  public static List<TripBookingDetailView> getAllTripNotesView (Trip tripModel, SessionMgr sessionMgr,
                                                                 boolean filterOutTraveler) {
    List<TripBookingDetailView> results = null;
    Optional<List> optNotes = CacheMgr.getRedis().hashGetBin(RedisKeys.H_TRIP, tripModel.getTripid(),
                                                             RedisKeys.HF_TRIP_NOTES_DETAIL_VIEW.getKey(),
                                                             List.class);
    if (optNotes.isPresent()) {
      results = optNotes.get();
    }

    if (results == null) {
      results = new ArrayList<>();
      List <TripNote> notes = TripNote.getNotesByTripId(tripModel.getTripid());
      if (notes != null && notes.size() > 0 ) {
        for (TripNote tripNote:notes) {
          results.add(buildNoteView(tripModel, tripNote, sessionMgr));
        }
      }
      if (results.size() > 0) {
        CacheMgr.getRedis().hashSet(RedisKeys.H_TRIP,
                                    tripModel.getTripid(),
                                    RedisKeys.HF_TRIP_NOTES_DETAIL_VIEW.getKey(),
                                    results, RedisMgr.WriteMode.FAST);
      }
    } else {
      if (sessionMgr != null) {
        for (TripBookingDetailView view : results) {
          if (sessionMgr != null) {
            //set the access level for shared trips
            view.loggedInUserId = sessionMgr.getUserId();
            SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
            view.accessLevel = accessLevel;
            if (SecurityMgr.isCmpyAdmin(tripModel.cmpyid, sessionMgr)) {
              view.isCmpyAdmin = true;
            }
            else {
              view.isCmpyAdmin = false;
            }
          }
        }
      }
    }

    if(filterOutTraveler) {
      //TODO: TR_COLAB: Needs to be changed to take accounts into consideration
      Iterator<TripBookingDetailView> vIt = results.iterator();
      while(vIt.hasNext()) {
        TripBookingDetailView nv = vIt.next();
        if(Utils.isInteger(nv.createdById)) { //TODO: This is a simplistic check for traveler
          vIt.remove();
        }
      }
    }
    results = filterMagnatech(results, tripModel);



    return results;
  }

  /*
    Stupid logic added to remove Magnatech garbage for Teplis for now
   */
  public static List<TripBookingDetailView> filterMagnatech(List<TripBookingDetailView> notes, Trip trip) {
    try {
      if (notes != null && notes.size() > 0) {
        boolean foundNote = false;
        for (TripBookingDetailView note : notes) {
          //let's see if we find the specific string we are looking for
          if (isAffectedMagnatecNote(note.getIntro())) {
            foundNote = true;
            break;
          }
        }
        //if we found it, let's make sure that this belongs to a company that we want to filter for
        if (foundNote) {
          if (shouldMagnatechNoteBeCleaned(trip)) {
            //now we process the notes... we are making sure there is only 1 top note and the top note does not have the passenger info
            TripBookingDetailView noteToClean = null;
            List<TripBookingDetailView> cleaned = new ArrayList<>();
            for (TripBookingDetailView note : notes) {
              //let's see if we find the specific string we are looking for
              if (isAffectedMagnatecNote(note.getIntro())) {
                if (noteToClean == null) {//only clean first one - ignore the rest
                  noteToClean = note;
                  note.name = "Company Details";
                  note.intro = getMagnatecCleannote(note.getIntro());
                  cleaned.add(note);
                }
              } else {
                cleaned.add(note);
              }
            }
            if (!cleaned.isEmpty()) {
              return cleaned;
            }
          }
        }

      }
    } catch (Exception e) {
      Log.err("BookingNoteController: filterMagnatech Error: trip: " + trip.tripid, e);
    }
    return notes;
  }

  public static boolean isAffectedMagnatecNote (String s) {
    if (s != null && s.contains("PASSENGER/COMPANY DETAILS") && s.contains("<td style=\"border:none;\"><b>Invoice #:</b></td>")) {
      return true;
    }
    return false;
  }

  public static String getMagnatecCleannote (String s) {
    String s1 = s.substring(0, s.indexOf("<td style=\"border:none;\"><b>Invoice #:</b></td>"));
    String s2 = s.substring(s.indexOf("<td style=\"border:none;\"><b>Invoice #:</b></td>"));
    String s3 = s2.substring(s2.indexOf("</table>"));

    StringBuilder s4 = new StringBuilder();
    s4.append(s1);
    s4.append("<td style=\"border:none;\"></td><td style=\"border:none;\"></td></tr>");
    s4.append(s3);
    return s4.toString();
  }

  public static boolean shouldMagnatechNoteBeCleaned (Trip trip) {
    Company c = Company.find.byId(trip.getCmpyid());
    if (c != null && c.getTag() != null && c.getTag().contains(APPConstants.CMPY_TAG_FILTER_MANGNATECH_NOTE)) {
      return true;
    }
    return false;
  }
  public static TripBookingDetailView buildNoteView (Trip tripModel, TripNote tripNote, SessionMgr sessionMgr) {
    TripBookingDetailView view = new TripBookingDetailView();
    view.tripId = tripModel.tripid;
    view.tripName = tripModel.name;
    view.noteId = tripNote.getNoteId();
    view.city = tripNote.getCity();
    view.country = tripNote.getCountry();
    view.description = tripNote.getDescription();
    view.state = tripNote.getState();
    view.intro = tripNote.getIntro();
    view.landmark = tripNote.getLandmark();
    view.name = tripNote.getName();
    view.locStartLat = tripNote.getLocLat();
    view.locStartLong = tripNote.getLocLong();
    view.rank = tripNote.getRank();
    view.providerAddr1 = tripNote.getStreetAddr();
    view.cmpyId = tripModel.cmpyid;
    view.createdById = tripNote.getCreatedBy();
    view.tag = tripNote.getTag();
    //Set boolean for Page break and LAST-ITEM tags
    if (tripNote.getTag() != null && !tripNote.getTag().isEmpty()) {
      if (tripNote.getTag().toLowerCase().contains(APPConstants.TAG_PAGE_BREAK_BEFORE)) {
        view.tagChecks.pageBreakBefore = true;
      }
      if (tripNote.getTag().toLowerCase().contains(APPConstants.TAG_PAGE_BREAK_AFTER)) {
        view.tagChecks.pageBreakAfter = true;
      }
      if (tripNote.getTag().contains(APPConstants.NOTE_TAG_LAST_ITEM)) {
        view.tagChecks.lastItem = true;
      }
    }
    view.zip = tripNote.getZipcode();
    view.tripStartDatePrint = Utils.formatDateControlYYYY(tripModel.getStarttimestamp());
    view.startDateMs = 0;
    view.detailsId = String.valueOf(tripNote.getNoteId());
    view.linkedTripDetailsId = tripNote.getTripDetailId();
    if (tripNote.getTripDetailId() != null && tripNote.getTripDetailId().trim().length() > 0) {
      TripDetail td = TripDetail.findByPK(tripNote.getTripDetailId());
      if (td != null && td.getStatus() == APPConstants.STATUS_ACTIVE && td.getTripid().equals(tripModel.getTripid())) {
        view.providerName = td.getName();
      }
    }
    view.noteType = tripNote.getType();

    StringBuilder sb = new StringBuilder();
    if (tripNote.getLandmark() != null &&
        tripNote.getLandmark().trim().length() > 0 &&
        !tripNote.getLandmark().equals("Enter a Landmark")) {
      sb.append(tripNote.getLandmark());
    }
    if (tripNote.getCity() != null && tripNote.getCity().trim().length() > 0) {
      if (sb.toString().trim().length() > 0) {
        sb.append(", ");
      }
      sb.append(tripNote.getCity());
    }
    if (tripNote.getZipcode() != null && tripNote.getZipcode().trim().length() > 0) {
      if (sb.toString().trim().length() > 0) {
        sb.append(", ");
      }
      sb.append(tripNote.getZipcode());
    }
    if (tripNote.getCountry() != null && tripNote.getCountry().trim().length() > 0) {
      if (sb.toString().trim().length() > 0) {
        sb.append(", ");
      }
      sb.append(tripNote.getCountry());
    }

    view.providerAddr2 = sb.toString();

    if (tripNote.getNoteTimestamp() != null && tripNote.getNoteTimestamp() > 0) {
      view.startDatePrint = Utils.formatDatePrint(tripNote.getNoteTimestamp());
      view.startTime = Utils.getTimeString(tripNote.getNoteTimestamp());
      view.startDateMs = tripNote.getNoteTimestamp();
      view.startMonth = Utils.getMonthStringPrint(tripNote.getNoteTimestamp());
      view.startDay = Utils.getDayMonthStringPrint(tripNote.getNoteTimestamp());
      view.startDate = Utils.formatDateControlYYYY(tripNote.getNoteTimestamp());
      Calendar cal = Calendar.getInstance();
      cal.setTimeInMillis(tripNote.getNoteTimestamp());
      view.startYear = String.valueOf(cal.get(Calendar.YEAR));
      view.startMonth = String.valueOf(cal.get(Calendar.MONTH));
      view.startDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));

      view.startHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
      view.startMin = String.valueOf(cal.get(Calendar.MINUTE));
    }

    //get any attachments
    List<TripNoteAttach> attachments = TripNoteAttach.findByNoteId(tripNote.getNoteId());
    if (attachments != null) {
      List<AttachmentView> photoAttachments = new ArrayList<>();
      List<AttachmentView> linkAttachments = new ArrayList<>();
      List<AttachmentView> videoAttachments = new ArrayList<>();
      List<AttachmentView> fileAttachments = new ArrayList<>();
      for (TripNoteAttach attach : attachments) {
        AttachmentView attachView = new AttachmentView();
        attachView.id = String.valueOf(attach.fileId);
        attachView.attachId = attach.fileId;
        attachView.createdById = attach.createdBy;
        attachView.name = attach.name;
        attachView.comments = attach.comments;
        attachView.attachName = attach.getAttachName();
        attachView.attachUrl = attach.getAttachUrl();

        switch (attach.getAttachType()) {
          case PHOTO_LINK:
            attachView.image = attach.getFileImage();
            if (attachView.image != null) { //Overwriting if we have image data
              FileInfo file = attachView.image.getFile();
              attachView.name = file.getFilename();
              attachView.comments = file.getDescription();
              attachView.attachUrl = file.getUrl();

            }
            photoAttachments.add(attachView);
            break;
          case FILE_LINK:
            //fileAttachments.add(attachView);
            linkAttachments.add(attachView);
            break;
          case VIDEO_LINK:
            videoAttachments.add(attachView);
            if (attach.getAttachUrl().toLowerCase().contains("youtube.com") ||
                attach.getAttachUrl().toLowerCase().contains("youtu.be")) {
              //try to get the static image
              attach.getAttachUrl().toLowerCase().contains("youtube.com");
              String pattern = "(?<=(?:v|i)=)[a-zA-Z0-9-]+(?=&)|(?<=(?:v|i)\\/)[^&\\n]+|(?<=embed\\/)[^\"&\\n]+|" +
                               "(?<=‌​(?:v|i)=)[^&\\n]+|(?<=youtu.be\\/)[^&\\n]+";

              Pattern compiledPattern = Pattern.compile(pattern);
              Matcher matcher         = compiledPattern.matcher(attach.getAttachUrl());

              if (matcher.find()) {
                String videoId = matcher.group();
                attachView.youTubeCover = "https://img.youtube.com/vi/" + videoId + "/0.jpg";
                attachView.attachUrl = "https://www.youtube.com/embed/" + videoId + "?autoplay=true";
              }
            }
            break;
          case WEB_LINK:
            linkAttachments.add(attachView);
            break;
          default:
            break;
        }


        if (attachView.attachUrl != null && !attachView.attachUrl.toLowerCase().startsWith("http")) {
          attachView.attachUrl = "http://" + attachView.attachUrl;
        }

      }
      if (photoAttachments.size() > 0) {
        view.photos = photoAttachments;
      }
      if (videoAttachments.size() > 0) {
        view.videos = videoAttachments;
      }
      if (linkAttachments.size() > 0) {
        view.links = linkAttachments;
      }
      if (fileAttachments.size() > 0) {
        view.files = fileAttachments;
      }
    }

    if (sessionMgr != null) {
      //set the access level for shared trips
      view.loggedInUserId = sessionMgr.getUserId();
      SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
      view.accessLevel = accessLevel;
      if (SecurityMgr.isCmpyAdmin(tripModel.cmpyid, sessionMgr)) {
        view.isCmpyAdmin = true;
      }
      else {
        view.isCmpyAdmin = false;
      }
    }

    return view;
  }

  public static boolean canEditNote (Trip tripModel, TripNote tripNote, SessionMgr sessionMgr) {

    if (tripModel == null || tripModel.getStatus() == APPConstants.STATUS_DELETED ||
        SecurityMgr.getTripAccessLevel(tripModel,sessionMgr).lt(SecurityMgr.AccessLevel.APPEND) || tripNote == null || tripNote.getStatus() == APPConstants.STATUS_DELETED || !tripNote
        .getTrip()
        .getTripid()
        .equals(tripModel.getTripid())) {
     return false;
    }

    //if user only have append access, make sure user can access the note
    if (SecurityMgr.getTripAccessLevel(tripModel,sessionMgr).equals(SecurityMgr.AccessLevel.APPEND) && !tripNote.getCreatedBy().equals(sessionMgr.getUserId())) {
      return false;
    }

    return true;

  }

  public static TabType.Bound getTabType (String tripDetailId) {
    if (tripDetailId != null) {
      TripDetail td = TripDetail.findByPK(tripDetailId);
      if (td != null) {
        switch (td.getDetailtypeid().getRootLevelType()) {
          case FLIGHT:
            return new TabType.Bound(TabType.FLIGHTS);
          case HOTEL:
            return new TabType.Bound(TabType.HOTELS);
          case TRANSPORT:
            return new TabType.Bound(TabType.TRANSPORTS);
          case CRUISE:
            return new TabType.Bound(TabType.CRUISES);
          case ACTIVITY:
            return new TabType.Bound(TabType.ACTIVITIES);
          default:
            break;
        }
      }
    }
    return  new TabType.Bound(TabType.NOTES);
  }


  @With({Authenticated.class, Authorized.class})
  public Result notePhotos(Long notesId) {
    if (notesId != null) {
      TripNote td = TripNote.find.byId(notesId);
      if (td == null) {
        BaseView baseView = new BaseView();
        baseView.message = "No record found.";
        return badRequest(views.html.common.message.render(baseView));
      }

      PoiView view = new PoiView();
      view.photos = new ArrayList<>();
      List<TripNoteAttach> photos = TripNoteAttach.findPhotosByNoteId(td.getNoteId());

      for (TripNoteAttach f : photos) {
        AttachmentView v = new AttachmentView();
        v.id = Long.toString(f.getFileId());
        v.name = f.getName();
        v.attachName = f.getAttachName();
        v.attachUrl = f.getAttachUrl();
        v.attachType = f.getAttachType().getStrVal();
        v.image = f.getFileImage();
        if(v.image != null) {
          FileInfo fi = v.image.getFile();
          v.attachUrl = fi.getUrl();
          v.attachName = fi.getFilename();
        }
        v.comments = f.getComments();
        view.photos.add(v);
      }

      if (view.photos.size() > 0) {
        return ok(views.html.poi.photoModal.render(view));
      }
    }
    BaseView baseView = new BaseView();
    baseView.message = "No photos available.";
    return ok(views.html.common.message.render(baseView));
  }

  //From DestinationController
  @With({Authenticated.class, Authorized.class})
  public Result copyNoteToDocSearch() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    DestinationSearchView view = new DestinationSearchView();

    //bind html form to form bean
    Form<DestinationSearchForm> destSearchForm = formFactory.form(DestinationSearchForm.class);
    DestinationSearchForm destSearchInfo = destSearchForm.bindFromRequest().get();

    if (destSearchInfo == null ||
            destSearchInfo.getInKeyword() == null ||
            destSearchInfo.getInKeyword().length() == 0 ||
            destSearchInfo.getInNoteId() == null ||
            destSearchInfo.getInCmpyId() == null ||
            destSearchInfo.getInCmpyId().trim().length() == 0) {
      //return error msg - search term is mandatory
      view.message = "Search term is required.";
      return ok(views.html.guide.searchDestination.render(view));

    }
    else {
      TripNote note = TripNote.find.byId(Long.parseLong(destSearchInfo.getInNoteId()));
      if (note == null) {
        view.message = "A System Error has occurred.";
        return ok(views.html.guide.searchDestination.render(view));
      }

      Trip origTrip = Trip.find.byId(note.getTrip().tripid);

      if (origTrip == null || !origTrip.cmpyid.equals(destSearchInfo.getInCmpyId()) || !SecurityMgr.canAccessCmpy(
              destSearchInfo.getInCmpyId(),
              sessionMgr)) {
        view.message = "Unauthorized Access.";
        return ok(views.html.guide.searchDestination.render(view));
      }
      view.destGuideId = note.getNoteId().toString();
      view.tripNote = true;


      List<String> cmpies = new ArrayList<String>();
      cmpies.add(destSearchInfo.getInCmpyId());
      List<DestinationType> destinationTypes = DestinationType.findActive();
      HashMap<Integer, String> destinationTypeDesc = new HashMap<Integer, String>();
      if (destinationTypes != null) {
        for (DestinationType u : destinationTypes) {
          destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());
        }
      }

      List<Destination> destList = Destination.findActiveDestAllTypesByKeyword(cmpies, destSearchInfo.getInKeyword());
      DestinationController.buildDestinations(view, destList, destSearchInfo, destinationTypeDesc);

      return ok(views.html.guide.destinationCopySearch.render(view));
    }

  }

  public static DestinationGuide addNoteAsPage(String destinationId, TripNote note, String userId) {
    long currentTime = System.currentTimeMillis();
    try {
      Ebean.beginTransaction();
      int maxRow = DestinationGuide.maxPageRankByDestId(destinationId);

      String newGuidePK = DBConnectionMgr.getUniqueId();
      Long origNotePK = note.getNoteId();
      DestinationGuide newGuide = new DestinationGuide();

      newGuide.setCity(note.getCity());
      newGuide.setCountry(note.getCountry());
      newGuide.setDescription(note.getDescription());
      newGuide.setIntro(note.getIntro());
      newGuide.setLandmark(note.getLandmark());
      newGuide.setLoclat(note.getLocLat());
      newGuide.setLoclong(note.getLocLong());
      newGuide.setName(note.getName());
      newGuide.setRank(maxRow + 1);
      newGuide.setState(note.getState());
      newGuide.setStreetaddr1(note.getStreetAddr());
      newGuide.setTag(note.getTag());
      //newGuide.setTemplateid(note.templateid);
      newGuide.setZipcode(note.getZipcode());
      //newGuide.setRecommendationtype(note.recommendationtype);

      newGuide.setDestinationguideid(newGuidePK);
      newGuide.setDestinationid(destinationId);
      newGuide.setModifiedby(userId);
      newGuide.setCreatedby(userId);
      newGuide.setStatus(APPConstants.STATUS_ACTIVE);
      newGuide.setCreatedtimestamp(currentTime);
      newGuide.setLastupdatedtimestamp(currentTime);
      newGuide.setPageId(note.getPageId());
      newGuide.save();

      //save any attachments
      List<TripNoteAttach> noteFiles = TripNoteAttach.findByNoteId(origNotePK);
      for (TripNoteAttach noteFile : noteFiles) {
        String newGuideFilePK = DBConnectionMgr.getUniqueId();
        DestinationGuideAttach newGuideFile = new DestinationGuideAttach();

        newGuideFile.setAttachname(noteFile.getAttachName());
        newGuideFile.setAttachtype(noteFile.getAttachType());
        newGuideFile.setAttachurl(noteFile.getImageUrl());
        newGuideFile.setFileImage(noteFile.getFileImage());

        newGuideFile.setComments(noteFile.comments);
        //newGuideFile.setHeight(noteFile.);
        newGuideFile.setName(noteFile.name);
        //newGuideFile.setParentid(noteFile.parentid);
        newGuideFile.setRank(noteFile.rank);
        newGuideFile.setTag(noteFile.tag);
        //newGuideFile.setWidth(noteFile.width);

        newGuideFile.setFileid(newGuideFilePK);
        newGuideFile.setDestinationguideid(newGuidePK);
        newGuideFile.setModifiedby(userId);
        newGuideFile.setCreatedby(userId);
        newGuideFile.setStatus(APPConstants.STATUS_ACTIVE);
        newGuideFile.setCreatedtimestamp(currentTime);
        newGuideFile.setLastupdatedtimestamp(currentTime);
        newGuideFile.save();
      }
      Ebean.commitTransaction();
      return newGuide;
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
    }

    return null;
  }






  //From DestinationPostController
  @With({Authenticated.class, Authorized.class})
  public Result copyNoteToDoc() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<DestinationGuideForm> destGuideForm = formFactory.form(DestinationGuideForm.class);
    DestinationGuideForm destGuideInfo = destGuideForm.bindFromRequest().get();
    if (destGuideInfo != null &&
            destGuideInfo.getInDestId() != null &&
            destGuideInfo.getInDestId().length() > 0 &&
            destGuideInfo.getInDestGuideId() != null) {
      TripNote sourceGuide = TripNote.find.byId(Long.parseLong(destGuideInfo.getInDestGuideId()));

      Destination targetDestination = Destination.find.byId(destGuideInfo.getInDestId());

      if (sourceGuide != null &&
              targetDestination != null &&
              SecurityMgr.canEditDestination(targetDestination, sessionMgr)) {

        Trip sourceDest = Trip.find.byId(sourceGuide.getTrip().tripid);
        //make sure this is going to the same cmpy or going to a trip document
        if (sourceDest != null &&
                (sourceDest.getCmpyid().equals(targetDestination.getCmpyid()) ||
                        targetDestination.destinationtypeid == APPConstants.TRIP_DOC_DESTINATION_TYPE ||
                        targetDestination.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE)) {
          DestinationGuide targetGuide = addNoteAsPage(targetDestination.getDestinationid(),
                  sourceGuide,
                  sessionMgr.getUserId());

          Trip tripModel = null;
          List<TripDestination> tdests = TripDestination.findActiveByDest(targetDestination.getDestinationid());
          if (tdests != null && tdests.size() != 0) {
            tripModel = Trip.find.byId(tdests.get(0).getTripid());
          }

          //Create audit log for a trip operation
          if (targetDestination.destinationtypeid == APPConstants.TRIP_DOC_DESTINATION_TYPE ||
                  targetDestination.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE) {

            audit:
            {


              if (tripModel != null) {
                TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_DOC_CUSTOM,
                        AuditActionType.MODIFY,
                        AuditActorType.WEB_USER)
                        .withTrip(tripModel)
                        .withCmpyid(tripModel.cmpyid)
                        .withUserid(sessionMgr.getUserId());

                ((AuditTripDocCustom) ta.getDetails()).withGuideId(targetDestination.getDestinationid())
                        .withName(targetDestination.getName())
                        .withCoverImage(targetDestination.getCovername())
                        .withText("Copied Existing Note: " + targetGuide.getName());
                ta.save();;
              }
              else {
                Log.log(LogLevel.ERROR, "Trip ID can not be determined while adding new destination page audit:" + targetDestination.getDestinationid());
              }
            }
          }

          if (targetGuide != null) {
            invalidateCache(targetDestination.getDestinationid());

            if (targetDestination.destinationtypeid == APPConstants.TRIP_DOC_DESTINATION_TYPE ||
                    targetDestination.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE) {
              flash(SessionConstants.SESSION_PARAM_MSG, "Note copied successfully to " + targetDestination.name + ".");

              return redirect(routes.TripController.guides(tripModel.tripid,
                      new DestinationSearchView.Tab.Bound(DestinationSearchView.Tab.CUSTOM_DOC)));
            }
            else {
              BaseView baseView = new BaseView();
              baseView.message = "Note copied successfully to " + targetDestination.name + ".";
              return ok(views.html.common.message.render(baseView));
            }
          }
        }
      }
    }


    BaseView baseView = new BaseView();
    baseView.message = "System Error - please resubmit";
    return ok(views.html.common.message.render(baseView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result addWCitiesAutoContent(String tripId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Trip trip = Trip.findByPK(tripId);
    if (trip == null || SecurityMgr.getTripAccessLevel(trip, sessionMgr.getCredentials().getAccount()).lt(SecurityMgr
                                                                                                              .AccessLevel.APPEND)) {
      BaseView baseView = new BaseView();
      baseView.message = "Security Exception - no access to trip";
      return ok(views.html.common.message.render(baseView));
    }
    int result = OfferController.autoAddAllWcities(trip,
                                                   sessionMgr.getCredentials().getAccount(),
                                                   offerService,
                                                   offerProviderRepository);
    String summaryMsg = "Wcities added successfully";

    if (result != APPConstants.PROCESS_WCITIES_NOTE_SUCCESS) {
      summaryMsg = "Error adding Wcities to this trip";
    }
    flash(SessionConstants.SESSION_PARAM_MSG, summaryMsg);
    //go to the itinerary page

    return redirect(routes.BookingController.bookings(trip.tripid, new TabType.Bound(TabType.WCITIES), null));

  }

  @With({Authenticated.class, Authorized.class})
  public Result editNoteDate() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    final DynamicForm form = formFactory.form().bindFromRequest();

    String inTripId = form.get("inTripId");
    String inNoteId = form.get("inTripNoteId");
    String inDate = form.get("inTripNoteDate");

    String tripId = inTripId.trim();
    String noteId = inNoteId.trim();
    String date = inDate.trim();

    if (tripId == null) {
      BaseView baseView = new BaseView();
      baseView.message = "System Error - no permission to edit Trip";
      return ok(views.html.common.message.render(baseView));
    }
    if (noteId == null) {
      BaseView baseView = new BaseView();
      baseView.message = "System Error - no permission to edit Note";
      return ok(views.html.common.message.render(baseView));
    }

    Trip tripModel = Trip.find.byId(tripId);

    try {
      Ebean.beginTransaction();
      TripNote note = TripNote.find.byId(Long.parseLong(noteId));
      if (note != null) {
        if (date != null && !date.isEmpty()) {
          StringBuilder sb = new StringBuilder();
          sb.append(date);

          long timestamp = Utils.getMilliSecs(sb.toString());
          if (timestamp > 0) {
            note.setNoteTimestamp(timestamp);
          }
        } else {
          note.setNoteTimestamp(null);
        }
        note.setModifiedBy(sessionMgr.getUserId());
        note.setLastUpdatedTimestamp(System.currentTimeMillis());

        note.update();

        Ebean.commitTransaction();
      } else {
        BaseView baseView = new BaseView();
        baseView.message = "System Error - Note does not exist";
        return ok(views.html.common.message.render(baseView));
      }

      try {
        audit:
        {

          if (tripModel != null) {
            TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_BOOKING_NOTE,
                AuditActionType.MODIFY,
                AuditActorType.WEB_USER)
                .withTrip(tripModel)
                .withCmpyid(tripModel.cmpyid)
                .withUserid(sessionMgr.getUserId());

            ((AuditTripDocCustom) ta.getDetails()).withGuideId(String.valueOf(note.getNoteId()))
                .withName(note.getName())
                .withCoverImage("")
                .withText("Updated Note " + note.getName());
            ta.save();
          }
          else {
            Log.log(LogLevel.ERROR, "Trip ID can not be determined while adding new noteaudit:" + note.getNoteId());
          }
        }
      } catch (Exception e) {

      }

      invalidateCache(tripModel.tripid);
      flash(SessionConstants.SESSION_PARAM_SCROLL_TO_DIV, String.valueOf(note.getNoteId()));
      flash(SessionConstants.SESSION_PARAM_MSG, note.getName() + " date updated successfully");

    } catch (Exception e) {
      Ebean.rollbackTransaction();
      flash(SessionConstants.SESSION_PARAM_MSG, "System Error - Please retry");
    }

    return redirect(routes.BookingController.bookings(tripModel.tripid, new TabType.Bound(TabType.ITINERARY), null));
  }
}
