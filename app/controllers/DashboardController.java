package controllers;

import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.AdminAccess;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.form.BillingForm;
import com.mapped.publisher.persistence.BillingMgr;
import com.mapped.publisher.persistence.BillingTripRS;
import com.mapped.publisher.persistence.BillingTripTravellerRS;
import com.mapped.publisher.persistence.DashboardMgr;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;
import models.publisher.*;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import play.data.Form;
import play.data.FormFactory;
import play.db.DB;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-20
 * Time: 2:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class DashboardController
    extends Controller {

  @Inject
  FormFactory formFactory;

  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public Result publisherCmpyStats() {
    StatsView view = new StatsView();
    Connection conn = null;

    try {
      conn = DBConnectionMgr.getConnection4Publisher();

      DateTime currentDate = new DateTime(System.currentTimeMillis());
      long currentMonthStart = currentDate.minusDays(currentDate.getDayOfMonth() - 1).getMillis();
      long currentMonthEnd = currentDate.getMillis();
      Map<String, CmpyStatsView> currentStats = DashboardMgr.getCmpyStats(currentMonthStart, currentMonthEnd, conn);

      DateTime prevDate = currentDate.minusMonths(1);
      prevDate = prevDate.minusDays(prevDate.getDayOfMonth() - 1);
      long prevMonthStart = prevDate.getMillis();
      long prevMonthEnd = prevDate.plusMonths(1).getMillis();
      Map<String, CmpyStatsView> prevStats = DashboardMgr.getCmpyStats(prevMonthStart, prevMonthEnd, conn);

      DateTime prevDate1 = prevDate.minusMonths(1);
      long prevMonthStart1 = prevDate1.getMillis();
      long prevMonthEnd1 = prevDate1.plusMonths(1).getMillis();
      Map<String, CmpyStatsView> prevStats1 = DashboardMgr.getCmpyStats(prevMonthStart1, prevMonthEnd1, conn);

      DateTime prevDate2 = prevDate.minusMonths(2);
      long prevMonthStart2 = prevDate2.getMillis();
      long prevMonthEnd2 = prevDate2.plusMonths(1).getMillis();
      Map<String, CmpyStatsView> prevStats2 = DashboardMgr.getCmpyStats(prevMonthStart2, prevMonthEnd2, conn);

      DateTime prevDate3 = prevDate.minusMonths(3);
      long prevMonthStart3 = prevDate3.getMillis();
      long prevMonthEnd3 = prevDate3.plusMonths(1).getMillis();
      Map<String, CmpyStatsView> prevStats3 = DashboardMgr.getCmpyStats(prevMonthStart3, prevMonthEnd3, conn);

      view.cmpyActivityByMonth = new HashMap<>();
      view.cmpyActivityByMonth.put(Utils.getMonthStringPrint(prevMonthStart), prevStats.values());
      view.cmpyActivityByMonth.put(Utils.getMonthStringPrint(prevMonthStart1), prevStats1.values());
      view.cmpyActivityByMonth.put(Utils.getMonthStringPrint(prevMonthStart2), prevStats2.values());
      view.cmpyActivityByMonth.put(Utils.getMonthStringPrint(prevMonthStart3), prevStats3.values());

      view.cmpyActivityByMonth.put(Utils.getMonthStringPrint(currentMonthStart), currentStats.values());


      view.dates = new ArrayList<>();
      view.dates.add(Utils.getMonthStringPrint(currentMonthStart));
      view.dates.add(Utils.getMonthStringPrint(prevMonthStart));
      view.dates.add(Utils.getMonthStringPrint(prevMonthStart1));
      view.dates.add(Utils.getMonthStringPrint(prevMonthStart2));
      view.dates.add(Utils.getMonthStringPrint(prevMonthStart3));

      view.summaryActivityByMonth = new HashMap<>();
      //current month
      if (currentStats != null) {
        ActivityStatsView activityStatsView = view.summaryActivityByMonth.get(Utils.getMonthStringPrint(
            currentMonthStart));
        if (activityStatsView == null) {
          activityStatsView = new ActivityStatsView();
        }
        for (CmpyStatsView cmpyStats : currentStats.values()) {
          activityStatsView.numGuidesCreated += cmpyStats.numGuidesCreated;
          activityStatsView.numGuidesDeleted += cmpyStats.numGuidesDeleted;
          activityStatsView.numGuidesPagesCreated += cmpyStats.numGuidesPagesCreated;
          activityStatsView.numGuidesPagesDeleted += cmpyStats.numGuidesPagesDeleted;
          activityStatsView.numGuidesPagesModified += cmpyStats.numPoiDeleted;
          activityStatsView.numPoiCreated += cmpyStats.numPoiCreated;
          activityStatsView.numPoiDeleted += cmpyStats.numPoiDeleted;
          activityStatsView.numPoiModified += cmpyStats.numPoiModified;
          activityStatsView.numTripsCreated += cmpyStats.numTripsCreated;
          activityStatsView.numTripsDeleted += cmpyStats.numTripsDeleted;
          activityStatsView.numTripsPending += cmpyStats.numTripsPending;
          activityStatsView.numToursCreated += cmpyStats.numToursCreated;
          activityStatsView.numToursDeleted += cmpyStats.numToursDeleted;
          activityStatsView.numToursPending += cmpyStats.numToursPending;
          activityStatsView.numTripsPublished += cmpyStats.numTripsPublished;
          activityStatsView.numTripsPublishedActivity += cmpyStats.numTripsPublishedActivity;
        }
        view.summaryActivityByMonth.put(Utils.getMonthStringPrint(currentMonthStart), activityStatsView);
      }

      if (prevStats != null) {
        ActivityStatsView activityStatsView = view.summaryActivityByMonth.get(Utils.getMonthStringPrint(prevMonthStart));
        if (activityStatsView == null) {
          activityStatsView = new ActivityStatsView();
        }
        for (CmpyStatsView cmpyStats : prevStats.values()) {
          activityStatsView.numGuidesCreated += cmpyStats.numGuidesCreated;
          activityStatsView.numGuidesDeleted += cmpyStats.numGuidesDeleted;
          activityStatsView.numGuidesPagesCreated += cmpyStats.numGuidesPagesCreated;
          activityStatsView.numGuidesPagesDeleted += cmpyStats.numGuidesPagesDeleted;
          activityStatsView.numGuidesPagesModified += cmpyStats.numPoiDeleted;
          activityStatsView.numPoiCreated += cmpyStats.numPoiCreated;
          activityStatsView.numPoiDeleted += cmpyStats.numPoiDeleted;
          activityStatsView.numPoiModified += cmpyStats.numPoiModified;
          activityStatsView.numTripsCreated += cmpyStats.numTripsCreated;
          activityStatsView.numTripsDeleted += cmpyStats.numTripsDeleted;
          activityStatsView.numTripsPending += cmpyStats.numTripsPending;
          activityStatsView.numToursCreated += cmpyStats.numToursCreated;
          activityStatsView.numToursDeleted += cmpyStats.numToursDeleted;
          activityStatsView.numToursPending += cmpyStats.numToursPending;
          activityStatsView.numTripsPublished += cmpyStats.numTripsPublished;
          activityStatsView.numTripsPublishedActivity += cmpyStats.numTripsPublishedActivity;
        }
        view.summaryActivityByMonth.put(Utils.getMonthStringPrint(prevMonthStart), activityStatsView);
      }

      if (prevStats1 != null) {
        ActivityStatsView activityStatsView = view.summaryActivityByMonth.get(Utils.getMonthStringPrint(prevMonthStart1));
        if (activityStatsView == null) {
          activityStatsView = new ActivityStatsView();
        }
        for (CmpyStatsView cmpyStats : prevStats1.values()) {
          activityStatsView.numGuidesCreated += cmpyStats.numGuidesCreated;
          activityStatsView.numGuidesDeleted += cmpyStats.numGuidesDeleted;
          activityStatsView.numGuidesPagesCreated += cmpyStats.numGuidesPagesCreated;
          activityStatsView.numGuidesPagesDeleted += cmpyStats.numGuidesPagesDeleted;
          activityStatsView.numGuidesPagesModified += cmpyStats.numPoiDeleted;
          activityStatsView.numPoiCreated += cmpyStats.numPoiCreated;
          activityStatsView.numPoiDeleted += cmpyStats.numPoiDeleted;
          activityStatsView.numPoiModified += cmpyStats.numPoiModified;
          activityStatsView.numTripsCreated += cmpyStats.numTripsCreated;
          activityStatsView.numTripsDeleted += cmpyStats.numTripsDeleted;
          activityStatsView.numTripsPending += cmpyStats.numTripsPending;
          activityStatsView.numToursCreated += cmpyStats.numToursCreated;
          activityStatsView.numToursDeleted += cmpyStats.numToursDeleted;
          activityStatsView.numToursPending += cmpyStats.numToursPending;
          activityStatsView.numTripsPublished += cmpyStats.numTripsPublished;
          activityStatsView.numTripsPublishedActivity += cmpyStats.numTripsPublishedActivity;
        }
        view.summaryActivityByMonth.put(Utils.getMonthStringPrint(prevMonthStart1), activityStatsView);
      }

      if (prevStats2 != null) {
        ActivityStatsView activityStatsView = view.summaryActivityByMonth.get(Utils.getMonthStringPrint(prevMonthStart2));
        if (activityStatsView == null) {
          activityStatsView = new ActivityStatsView();
        }
        for (CmpyStatsView cmpyStats : prevStats1.values()) {
          activityStatsView.numGuidesCreated += cmpyStats.numGuidesCreated;
          activityStatsView.numGuidesDeleted += cmpyStats.numGuidesDeleted;
          activityStatsView.numGuidesPagesCreated += cmpyStats.numGuidesPagesCreated;
          activityStatsView.numGuidesPagesDeleted += cmpyStats.numGuidesPagesDeleted;
          activityStatsView.numGuidesPagesModified += cmpyStats.numPoiDeleted;
          activityStatsView.numPoiCreated += cmpyStats.numPoiCreated;
          activityStatsView.numPoiDeleted += cmpyStats.numPoiDeleted;
          activityStatsView.numPoiModified += cmpyStats.numPoiModified;
          activityStatsView.numTripsCreated += cmpyStats.numTripsCreated;
          activityStatsView.numTripsDeleted += cmpyStats.numTripsDeleted;
          activityStatsView.numTripsPending += cmpyStats.numTripsPending;
          activityStatsView.numToursCreated += cmpyStats.numToursCreated;
          activityStatsView.numToursDeleted += cmpyStats.numToursDeleted;
          activityStatsView.numToursPending += cmpyStats.numToursPending;
          activityStatsView.numTripsPublished += cmpyStats.numTripsPublished;
          activityStatsView.numTripsPublishedActivity += cmpyStats.numTripsPublishedActivity;
        }
        view.summaryActivityByMonth.put(Utils.getMonthStringPrint(prevMonthStart2), activityStatsView);
      }

      if (prevStats3 != null) {
        ActivityStatsView activityStatsView = view.summaryActivityByMonth.get(Utils.getMonthStringPrint(prevMonthStart3));
        if (activityStatsView == null) {
          activityStatsView = new ActivityStatsView();
        }
        for (CmpyStatsView cmpyStats : prevStats1.values()) {
          activityStatsView.numGuidesCreated += cmpyStats.numGuidesCreated;
          activityStatsView.numGuidesDeleted += cmpyStats.numGuidesDeleted;
          activityStatsView.numGuidesPagesCreated += cmpyStats.numGuidesPagesCreated;
          activityStatsView.numGuidesPagesDeleted += cmpyStats.numGuidesPagesDeleted;
          activityStatsView.numGuidesPagesModified += cmpyStats.numPoiDeleted;
          activityStatsView.numPoiCreated += cmpyStats.numPoiCreated;
          activityStatsView.numPoiDeleted += cmpyStats.numPoiDeleted;
          activityStatsView.numPoiModified += cmpyStats.numPoiModified;
          activityStatsView.numTripsCreated += cmpyStats.numTripsCreated;
          activityStatsView.numTripsDeleted += cmpyStats.numTripsDeleted;
          activityStatsView.numTripsPending += cmpyStats.numTripsPending;
          activityStatsView.numToursCreated += cmpyStats.numToursCreated;
          activityStatsView.numToursDeleted += cmpyStats.numToursDeleted;
          activityStatsView.numToursPending += cmpyStats.numToursPending;
          activityStatsView.numTripsPublished += cmpyStats.numTripsPublished;
          activityStatsView.numTripsPublishedActivity += cmpyStats.numTripsPublishedActivity;
        }
        view.summaryActivityByMonth.put(Utils.getMonthStringPrint(prevMonthStart3), activityStatsView);
      }



      //get created cmpy stats per month
      List<Company> cmpyCM = Company.findNewCmpies(currentMonthStart, currentMonthEnd);
      if (cmpyCM != null) {
        ActivityStatsView a = view.summaryActivityByMonth.get(Utils.getMonthStringPrint(currentMonthStart));
        a.newCmpies = new ArrayList<CmpyInfoView>();
        a.newSelfSignupCmpies = new ArrayList<CmpyInfoView>();
        a.numCollabInvites = UserInvite.allCountByDate(currentMonthStart, currentMonthEnd);
        a.numCollabAccepted = UserInvite.acceptedCountByDate(currentMonthStart, currentMonthEnd);
        for (Company c : cmpyCM) {
          UserProfile u = UserProfile.findByPK(c.createdby);
          if (u != null && u.getUsertype() == 0) {
            //created by umapped admin
            CmpyInfoView ci = new CmpyInfoView();
            ci.cmpyId = c.cmpyid;
            ci.cmpyName = c.name;
            ci.expiryDate = Utils.formatDatePrint(c.createdtimestamp);
            a.newCmpies.add(ci);
          }
          else {
            CmpyInfoView ci = new CmpyInfoView();
            ci.cmpyId = c.cmpyid;
            ci.cmpyName = c.name;
            ci.expiryDate = Utils.formatDatePrint(c.createdtimestamp);
            ci.authorizedUserId = c.createdby;
            ci.firstName = u.firstname + " " + u.lastname + "  (" + c.createdby + ")";

            //find the userinvite
            UserInvite invites = UserInvite.findByEmail(u.getEmail());
            if (invites != null) {
              ci.lastName = invites.createdby;
            }

            a.newSelfSignupCmpies.add(ci);
          }
        }
      }

      List<Company> cmpyPM = Company.findNewCmpies(prevMonthStart, prevMonthEnd);
      if (cmpyPM != null) {
        ActivityStatsView a = view.summaryActivityByMonth.get(Utils.getMonthStringPrint(prevMonthStart));
        a.newCmpies = new ArrayList<CmpyInfoView>();
        a.newSelfSignupCmpies = new ArrayList<CmpyInfoView>();
        a.numCollabInvites = UserInvite.allCountByDate(prevMonthStart, prevMonthEnd);
        a.numCollabAccepted = UserInvite.acceptedCountByDate(prevMonthStart, prevMonthEnd);
        for (Company c : cmpyPM) {
          UserProfile u = UserProfile.findByPK(c.createdby);
          if (u != null && u.getUsertype() == 0) {
            //created by umapped admin
            CmpyInfoView ci = new CmpyInfoView();
            ci.cmpyId = c.cmpyid;
            ci.cmpyName = c.name;
            ci.expiryDate = Utils.formatDatePrint(c.createdtimestamp);
            a.newCmpies.add(ci);
          }
          else {
            CmpyInfoView ci = new CmpyInfoView();
            ci.cmpyId = c.cmpyid;
            ci.cmpyName = c.name;
            ci.expiryDate = Utils.formatDatePrint(c.createdtimestamp);
            ci.authorizedUserId = c.createdby;
            ci.firstName = u.firstname + " " + u.lastname + "  (" + c.createdby + ")";

            //find the userinvite
            UserInvite invites = UserInvite.findByEmail(u.getEmail());
            if (invites != null) {
              ci.lastName = invites.createdby;
            }

            a.newSelfSignupCmpies.add(ci);
          }
        }
      }

      List<Company> cmpyPM1 = Company.findNewCmpies(prevMonthStart1, prevMonthEnd1);
      if (cmpyPM1 != null) {
        ActivityStatsView a = view.summaryActivityByMonth.get(Utils.getMonthStringPrint(prevMonthStart1));
        a.newCmpies = new ArrayList<CmpyInfoView>();
        a.newSelfSignupCmpies = new ArrayList<CmpyInfoView>();
        a.numCollabInvites = UserInvite.allCountByDate(prevMonthStart1, prevMonthEnd1);
        a.numCollabAccepted = UserInvite.acceptedCountByDate(prevMonthStart1, prevMonthEnd1);
        for (Company c : cmpyPM1) {
          UserProfile u = UserProfile.findByPK(c.createdby);
          if (u != null && u.getUsertype() == 0) {
            //created by umapped admin
            CmpyInfoView ci = new CmpyInfoView();
            ci.cmpyId = c.cmpyid;
            ci.cmpyName = c.name;
            ci.expiryDate = Utils.formatDatePrint(c.createdtimestamp);
            a.newCmpies.add(ci);
          }
          else {
            CmpyInfoView ci = new CmpyInfoView();
            ci.cmpyId = c.cmpyid;
            ci.cmpyName = c.name;
            ci.expiryDate = Utils.formatDatePrint(c.createdtimestamp);
            ci.authorizedUserId = c.createdby;
            ci.firstName = u.firstname + " " + u.lastname + "  (" + c.createdby + ")";

            //find the userinvite
            UserInvite invites = UserInvite.findByEmail(u.getEmail());
            if (invites != null) {
              ci.lastName = invites.createdby;
            }

            a.newSelfSignupCmpies.add(ci);
          }
        }

        //expired and expiring trials
        DateTime expiryTime = new DateTime();
        List<Company> expired = Company.findTrials(expiryTime.plusDays(30).getMillis());
        long currentTime = System.currentTimeMillis();
        if (expired != null && expired.size() > 0) {
          view.expiredTrials = new ArrayList<CmpyInfoView>();
          view.expiringTrials = new ArrayList<CmpyInfoView>();

          for (Company c : expired) {
            if (c.getExpirytimestamp() < currentTime) {
              CmpyInfoView ci = new CmpyInfoView();
              ci.cmpyId = c.cmpyid;
              ci.cmpyName = c.name;
              ci.expiryDate = Utils.formatDatePrint(c.expirytimestamp);
              view.expiredTrials.add(ci);
            }
            else {
              CmpyInfoView ci = new CmpyInfoView();
              ci.cmpyId = c.cmpyid;
              ci.cmpyName = c.name;
              ci.expiryDate = Utils.formatDatePrint(c.expirytimestamp);
              view.expiringTrials.add(ci);
            }
          }
        }
      }



    }
    catch (Exception e) {
      e.printStackTrace();
    }
    finally {
      try {
        if (conn != null) {
          conn.close();
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }

    Html a = views.html.stats.cmpiesActivity.render(view);
    return ok(a);

  }


  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public Result publisherUserStats() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    StatsView view = new StatsView();

    Connection conn = null;

    try {
      conn = DBConnectionMgr.getConnection4Publisher();

      DateTime currentDate = new DateTime(System.currentTimeMillis());
      long currentMonthStart = currentDate.minusDays(currentDate.getDayOfMonth() - 1).getMillis();
      long currentMonthEnd = currentDate.getMillis();
      Map<String, UserStatsView> currentStats = DashboardMgr.getUserStats(currentMonthStart, currentMonthEnd, conn);

      DateTime prevDate = currentDate.minusMonths(1);
      prevDate = prevDate.minusDays(prevDate.getDayOfMonth() - 1);
      long prevMonthStart = prevDate.getMillis();
      long prevMonthEnd = prevDate.plusMonths(1).getMillis();
      Map<String, UserStatsView> prevStats = DashboardMgr.getUserStats(prevMonthStart, prevMonthEnd, conn);

      DateTime prevDate1 = prevDate.minusMonths(1);
      long prevMonthStart1 = prevDate1.getMillis();
      long prevMonthEnd1 = prevDate1.plusMonths(1).getMillis();
      Map<String, UserStatsView> prevStats1 = DashboardMgr.getUserStats(prevMonthStart1, prevMonthEnd1, conn);

      DateTime prevDate2 = prevDate.minusMonths(2);
      long prevMonthStart2 = prevDate2.getMillis();
      long prevMonthEnd2 = prevDate2.plusMonths(1).getMillis();
      Map<String, UserStatsView> prevStats2 = DashboardMgr.getUserStats(prevMonthStart2, prevMonthEnd2, conn);

      DateTime prevDate3 = prevDate.minusMonths(3);
      long prevMonthStart3 = prevDate3.getMillis();
      long prevMonthEnd3 = prevDate3.plusMonths(1).getMillis();
      Map<String, UserStatsView> prevStats3 = DashboardMgr.getUserStats(prevMonthStart3, prevMonthEnd3, conn);

      view.userActivityByMonth = new HashMap<String, Collection<UserStatsView>>();
      view.userActivityByMonth.put(Utils.getMonthStringPrint(prevMonthStart3), prevStats.values());
      view.userActivityByMonth.put(Utils.getMonthStringPrint(prevMonthStart2), prevStats.values());
      view.userActivityByMonth.put(Utils.getMonthStringPrint(prevMonthStart), prevStats.values());
      view.userActivityByMonth.put(Utils.getMonthStringPrint(prevMonthStart1), prevStats1.values());
      view.userActivityByMonth.put(Utils.getMonthStringPrint(currentMonthStart), currentStats.values());

      view.dates = new ArrayList<String>();
      view.dates.add(Utils.getMonthStringPrint(currentMonthStart));
      view.dates.add(Utils.getMonthStringPrint(prevMonthStart));
      view.dates.add(Utils.getMonthStringPrint(prevMonthStart1));
      view.dates.add(Utils.getMonthStringPrint(prevMonthStart2));
      view.dates.add(Utils.getMonthStringPrint(prevMonthStart3));

      //get created users
      view.users = new HashMap<>();

      view.users.put(Utils.getMonthStringPrint(currentMonthStart),getNewUsers(currentMonthStart, currentMonthEnd));
      view.users.put(Utils.getMonthStringPrint(prevMonthStart),getNewUsers(prevMonthStart, prevMonthEnd));
      view.users.put(Utils.getMonthStringPrint(prevMonthStart1),getNewUsers(prevMonthStart1, prevMonthEnd1));
      view.users.put(Utils.getMonthStringPrint(prevMonthStart2),getNewUsers(prevMonthStart2, prevMonthEnd2));
      view.users.put(Utils.getMonthStringPrint(prevMonthStart3),getNewUsers(prevMonthStart3, prevMonthEnd3));

    }
    catch (Exception e) {
      e.printStackTrace();
    }
    finally {
      try {
        if (conn != null) {
          conn.close();
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }

    Html a = views.html.stats.usersActivity.render(view);
    return ok(a);

  }

  private static UsersView getNewUsers (long start, long end) {
    UsersView usersView = new UsersView();
    usersView.users = new ArrayList<>();
    List <UserProfile> c = UserProfile.findActiveUsers(start, end);
    if (c != null) {

      for (UserProfile u: c) {
        UserView uv = new UserView();
        uv.userId = u.userid;
        uv.firstName = u.firstname;
        uv.lastName = u.lastname;
        uv.createdby = u.createdby;
        uv.createdon = Utils.formatDatePrint(u.createdtimestamp);
        List<UserCmpyLink> links = UserCmpyLink.findActiveByUserId(u.userid);
        if (links != null && links.size() > 0) {
          Company cmpy = links.get(0).getCmpy();
          uv.cmpyName = cmpy.getName();
          uv.cmpyId = cmpy.getCmpyid();
        }
        usersView.users.add(uv);
      }
    }
    return usersView;
  }

  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public Result publisherCurrentStats() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    StatsView view = new StatsView();

    Connection conn = null;

    try {
      conn = DBConnectionMgr.getConnection4Publisher();

      DateTime currentDate = new DateTime(System.currentTimeMillis());
      long currentMonthStart = currentDate.minusDays(currentDate.getDayOfMonth() - 1).getMillis();
      long currentMonthEnd = currentDate.getMillis();
      ActivityStatsView currentStats = DashboardMgr.getCurrentStats(currentMonthStart, currentMonthEnd, true, conn);


      view.summaryActivityByMonth = new HashMap<String, ActivityStatsView>();

      view.summaryActivityByMonth.put(Utils.getMonthStringPrint(currentMonthStart), currentStats);

      view.dates = new ArrayList<String>();
      view.dates.add(Utils.getMonthStringPrint(currentMonthStart));


    }
    catch (Exception e) {
      e.printStackTrace();
    }
    finally {
      try {
        if (conn != null) {
          conn.close();
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }

    Html a = views.html.stats.currentAdmin.render(view);
    return ok(a);

  }


  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public Result publisherMobileStats() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    StatsView view = new StatsView();

    Connection conn = null;

    try {
      conn = DB.getConnection("web");

      DateTime currentDate = new DateTime(System.currentTimeMillis());
      long currentMonthStart = currentDate.minusDays(currentDate.getDayOfMonth() - 1).getMillis();
      long currentMonthEnd = currentDate.getMillis();
      MobileStatsView currentStats = DashboardMgr.getCurrentMobileStats(currentMonthStart, currentMonthEnd, true, conn);

      DateTime prevDate = currentDate.minusMonths(1);
      prevDate = prevDate.minusDays(prevDate.getDayOfMonth() - 1);
      long prevMonthStart = prevDate.getMillis();
      long prevMonthEnd = prevDate.plusMonths(1).getMillis();
      MobileStatsView prevStats = DashboardMgr.getCurrentMobileStats(prevMonthStart, prevMonthEnd, false, conn);

      view.summaryMobileActivityByMonth = new HashMap<String, MobileStatsView>();

      view.summaryMobileActivityByMonth.put(Utils.getMonthStringPrint(currentMonthStart), currentStats);
      view.summaryMobileActivityByMonth.put(Utils.getMonthStringPrint(prevMonthStart), prevStats);

      view.dates = new ArrayList<String>();
      view.dates.add(Utils.getMonthStringPrint(currentMonthStart));
      view.dates.add(Utils.getMonthStringPrint(prevMonthStart));


    }
    catch (Exception e) {
      e.printStackTrace();
    }
    finally {
      try {
        if (conn != null) {
          conn.close();
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }

    Html a = views.html.stats.mobileAdmin.render(view);
    return ok(a);

  }


  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public Result getBilling() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    BillingView view = new BillingView();

    HashMap<String, String> agencyList = new HashMap<String, String>();
    List<Company> cmpies = Company.findBillable();
    if (cmpies != null) {
      for (Company c : cmpies) {
        agencyList.put(c.getCmpyid(), c.getName());
      }
    }

    view.cmpies = agencyList;


    Html a = views.html.stats.billing.render(view);
    return ok(a);

  }


  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public Result doBilling() {
    BillingView view = new BillingView();
    HashMap<String, String> agencyList = new HashMap<String, String>();
    List<Company> cmpies = Company.findBillable();
    if (cmpies != null) {
      for (Company c : cmpies) {
        agencyList.put(c.getCmpyid(), c.getName());
      }
    }

    view.cmpies = agencyList;

    // find the queries
    //bind html form to form bean
    Form<BillingForm> form = formFactory.form(BillingForm.class).bindFromRequest();

    //use bean validator framework
    if (!form.hasErrors()) {
      BillingForm billingInfo = form.get();
      buildBillingView(view, billingInfo, agencyList);
    }
    return ok(views.html.stats.billing.render(view));
  }


  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public Result doBillingCSV() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    BillingView view = new BillingView();
    HashMap<String, String> agencyList = new HashMap<>();
    List<Company> cmpies = Company.findBillable();
    if (cmpies != null) {
      for (Company c : cmpies) {
        agencyList.put(c.getCmpyid(), c.getName());
      }
    }

    view.cmpies = agencyList;

    // find the queries
    //bind html form to form bean
    Form<BillingForm> form = formFactory.form(BillingForm.class).bindFromRequest();
    //use bean validator framework
    if (!form.hasErrors()) {
      BillingForm billingInfo = form.get();
      buildBillingView(view, billingInfo, agencyList);
    }

    String fileName = view.cmpyName.replace("\\", "_").replace(".", "");
    Html a = views.html.stats.billingCSV.render(view);

    response().setContentType("text/csv");
    response().setHeader("Content-disposition", "attachment; filename=" + fileName + ".csv");

    return ok(a.toString());

  }

  public static void buildBillingView(BillingView view, BillingForm billingInfo, HashMap<String, String> agencyList) {
    HashMap<String, Integer> existingTransactionCount = new HashMap<String, Integer>();

    view.cmpyName = agencyList.get(billingInfo.getInCmpyId());
    view.cmpyId = billingInfo.getInCmpyId();
    long starttimestamp = Utils.getMilliSecs(billingInfo.getInStartDate());
    long endtimestamp = Utils.getMilliSecs(billingInfo.getInEndDate());
    //go to 23:59:59 for end timestamp
    endtimestamp += (23 * 60 * 60 * 1000) + (59 * 60 * 1000) + (59 * 1000) + 999;

    String dateFormat = "MMM dd, yyyy";
    SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);

    if (starttimestamp > 1 && endtimestamp > 1 && endtimestamp > starttimestamp) {

      if (starttimestamp > 0) {
        Date d = new Date(starttimestamp);
        view.startDatePrint = dateFormatter.format(d);
        view.startDate = billingInfo.getInStartDate();
      }

      if (endtimestamp > 0) {
        Date d = new Date(endtimestamp);
        view.endDatePrint = dateFormatter.format(d);
        view.endDate = billingInfo.getInEndDate();
      }

      Connection conn = null;
      try {
        conn = DBConnectionMgr.getConnection4Publisher();

        ArrayList<BillingTripRS> trips = BillingMgr.findNewTripsPublished(starttimestamp,
                                                                          endtimestamp,
                                                                          billingInfo.getInCmpyId(),
                                                                          conn);
        ArrayList<BillingTripTravellerRS> tripTravellers = BillingMgr.findNewTripTravellerPublished(starttimestamp,
                                                                                                    endtimestamp,
                                                                                                    billingInfo
                                                                                                        .getInCmpyId(),
                                                                                                    conn);
        ArrayList<BillingTripTravellerRS> tripAgents = BillingMgr.findNewTripTravelAgentPublished(starttimestamp,
                                                                                                  endtimestamp,
                                                                                                  billingInfo
                                                                                                      .getInCmpyId(),
                                                                                                  conn);

        ArrayList<BillingTripTravellerRS> newTripTravellers = BillingMgr.findNewTravellerExistingTrip(starttimestamp,
                                                                                                      endtimestamp,
                                                                                                      billingInfo
                                                                                                          .getInCmpyId(),
                                                                                                      conn);
        ArrayList<BillingTripTravellerRS> newTripAgents = BillingMgr.findNewTravelAgentExistingTrip(starttimestamp,
                                                                                                    endtimestamp,
                                                                                                    billingInfo
                                                                                                        .getInCmpyId(),
                                                                                                    conn);


        view.numOfTravellers = 0;
        view.numOfTrips = 0;
        view.numOfAgentsExistingTrips = 0;
        view.numOfTravellersExistingTrips = 0;
        view.trips = new HashMap<String, BillingDetailView>();
        view.addedAgents = new HashMap<String, ArrayList<BillingAgentsView>>();
        view.addedTravellers = new HashMap<String, ArrayList<BillingAgentsView>>();
        view.numOfTransactions = 0;
        view.addedTravellersTransactions = new HashMap<String, Integer>();


        if (trips != null && trips.size() > 0) {

          for (BillingTripRS rec : trips) {
            String tripId = TourController.getCompositePK(rec.getTripId(), rec.getGroupId());

            BillingDetailView detail = view.trips.get(tripId);
            if (detail == null) {
              detail = new BillingDetailView();
              detail.tripId = tripId;
              detail.createdBy = rec.getCreatedBy();
              detail.firstName = rec.getFirstName();
              detail.groupId = rec.getGroupId();
              detail.groupName = rec.getGroupName();
              detail.lastName = rec.getLastName();
              detail.tripName = rec.getTripName();
              detail.publishCount = 1;
              detail.numOfTransactions = 1;
              view.numOfTransactions++;
              try {
                Date d = new Date(rec.getCreatedTimestamp());
                detail.publishedDate = dateFormatter.format(d);
              }
              catch (Exception e) {
                e.printStackTrace();
              }
              detail.travellers = new ArrayList<TripPassengerView>();
              detail.agents = new ArrayList<TripPassengerView>();

              view.trips.put(tripId, detail);
              view.numOfTrips++;
            }
            else {
              detail.publishCount++;
            }
          }
        }
        StringBuilder errorMsg = new StringBuilder();

        if (tripTravellers != null && tripTravellers.size() > 0) {

          for (BillingTripTravellerRS rec : tripTravellers) {
            String tripId = TourController.getCompositePK(rec.getTripId(), rec.getGroupId());

            BillingDetailView detail = view.trips.get(tripId);
            if (detail == null) {
              errorMsg.append("Could not find trip - " + rec.getTripId() + " - " + rec.getEmail());
              errorMsg.append("</br>");
            }
            else {
              ArrayList<TripPassengerView> passengers = detail.travellers;
              TripPassengerView passengerView = new TripPassengerView();
              passengerView.email = rec.getEmail();
              passengerView.id = String.valueOf(rec.getPassengerPk());
              passengerView.createdBy = rec.getUserId();
              if (rec.getUserId() != null && StringUtils.isNumeric(rec.getUserId())) {
                Account a = Account.find.byId(Long.parseLong(rec.getUserId()));
                if (a != null) {
                  if (a.getLegacyId() != null && !a.getLegacyId().isEmpty()) {
                    passengerView.createdBy = a.getLegacyId();
                  } else {
                    passengerView.createdBy = a.getEmail();
                  }
                }
              }
              if (passengerView.createdBy != null && passengerView.createdBy.equalsIgnoreCase("system")) {
                passengerView.createdBy = "Self-Registration";
              }
              passengers.add(passengerView);
              if (passengers.size() > 1) {
                detail.numOfTransactions++;
                view.numOfTransactions++;
              }

              view.numOfTravellers++;
            }
          }
        }


        if (tripAgents != null && tripAgents.size() > 0) {

          for (BillingTripTravellerRS rec : tripAgents) {
            String tripId = TourController.getCompositePK(rec.getTripId(), rec.getGroupId());

            BillingDetailView detail = view.trips.get(tripId);
            if (detail == null) {
              errorMsg.append("Could not find trip - " + rec.getTripId() + " - " + rec.getEmail());
              errorMsg.append("</br>");
            }
            else {
              ArrayList<TripPassengerView> travelAgents = detail.agents;
              TripPassengerView passengerView = new TripPassengerView();
              passengerView.email = rec.getEmail();
              passengerView.id = String.valueOf(rec.getPassengerPk());
              passengerView.createdBy = rec.getUserId();
              if (passengerView.createdBy != null && passengerView.createdBy.equalsIgnoreCase("system")) {
                passengerView.createdBy = "Self-Registration";
              }
              travelAgents.add(passengerView);
              view.numOfAgents++;
            }
          }
        }

        //process new travellers and agents to already published trips
        if (newTripTravellers != null && newTripTravellers.size() > 0) {

          for (BillingTripTravellerRS rec : newTripTravellers) {
            String tripId = TourController.getCompositePK(rec.getTripId(), rec.getGroupId());
            if (view.trips.get(tripId) == null) {
              ArrayList<BillingAgentsView> travellers = view.addedTravellers.get(tripId);
              if (travellers == null) {
                travellers = new ArrayList<BillingAgentsView>();
              }

              BillingAgentsView passengerView = new BillingAgentsView();
              passengerView.tripName = rec.getTripName();
              passengerView.groupName = rec.getGroupName();
              passengerView.tripId = rec.getTripId();
              passengerView.userId = rec.getUserId();
              if (rec.getUserId() != null && StringUtils.isNumeric(rec.getUserId())) {
                Account a = Account.find.byId(Long.parseLong(rec.getUserId()));
                if (a != null) {
                  if (a.getLegacyId() != null && !a.getLegacyId().isEmpty()) {
                    passengerView.userId = a.getLegacyId();
                  } else {
                    passengerView.userId = a.getEmail();
                  }
                }
              }
              passengerView.email = rec.getEmail();
              if (passengerView.userId != null && passengerView.userId.equalsIgnoreCase("system")) {
                passengerView.userId = "Self-Registration";
              }
              try {
                passengerView.publishedDate = dateFormatter.format(rec.getCreatedTimestamp());
              }
              catch (Exception e) {
                e.printStackTrace();
              }

              travellers.add(passengerView);
              view.numOfTravellersExistingTrips++;
              view.addedTravellers.put(tripId, travellers);

              //check to see if we need to count these as transactions
              int existingCount = 0;
              if (existingTransactionCount.containsKey(tripId)) {
                existingCount = existingTransactionCount.get(tripId);
              }
              else {
                existingCount = BillingMgr.numOfExistingTravellers(starttimestamp,
                                                                   rec.getTripId(),
                                                                   rec.getGroupId(),
                                                                   conn);
              }

              existingCount++;
              //we only count it there is more than 1 transaction
              int newTxnCount = 0;
              if (view.addedTravellersTransactions.containsKey(tripId)) {
                newTxnCount = view.addedTravellersTransactions.get(tripId);
              }

              if (existingCount > 1) {
                newTxnCount++;
                view.numOfTransactions++;
              }
              existingTransactionCount.put(tripId, existingCount);
              view.addedTravellersTransactions.put(tripId, newTxnCount);

            }
          }
        }


        if (newTripAgents != null && newTripAgents.size() > 0) {

          for (BillingTripTravellerRS rec : newTripAgents) {
            String tripId = TourController.getCompositePK(rec.getTripId(), rec.getGroupId());
            if (view.trips.get(tripId) == null) {
              ArrayList<BillingAgentsView> travellers = view.addedAgents.get(tripId);
              if (travellers == null) {
                travellers = new ArrayList<BillingAgentsView>();
              }

              BillingAgentsView passengerView = new BillingAgentsView();
              passengerView.tripName = rec.getTripName();
              passengerView.groupName = rec.getGroupName();
              passengerView.tripId = rec.getTripId();
              passengerView.email = rec.getEmail();
              passengerView.userId = rec.getUserId();

              try {
                passengerView.publishedDate = dateFormatter.format(rec.getCreatedTimestamp());
              }
              catch (Exception e) {
                e.printStackTrace();
              }
              if (passengerView.userId != null && passengerView.userId.equalsIgnoreCase("system")) {
                passengerView.userId = "Self-Registration";
              }
              travellers.add(passengerView);
              view.numOfAgentsExistingTrips++;
              view.addedAgents.put(tripId, travellers);


            }
          }
        }

        if (errorMsg.length() > 0) {
          view.message = errorMsg.toString();
        }


      }
      catch (Exception e) {
        e.printStackTrace();
        view.message = "Error - Could not retrieve billing records.";
      }
      finally {
        if (conn != null) {
          try {
            conn.close();
          }
          catch (SQLException se) {
            se.printStackTrace();
          }
        }
      }

    }
    else {
      view.message = "Invalid Date Range";
    }
  }
}
