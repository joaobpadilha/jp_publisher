package controllers;

import com.amazonaws.services.s3.model.CopyObjectResult;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import com.avaje.ebean.Transaction;
import com.google.inject.Inject;
import com.mapped.common.CoreConstants;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.AdminAccess;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.form.UserProfileForm;
import com.mapped.publisher.parse.schemaorg.umapped.SocialLink;
import com.mapped.publisher.persistence.RecordStatus;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.S3Util;
import com.mapped.publisher.utils.UmappedEmail;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.NewRegistrationView;
import com.umapped.api.schema.types.Operation;
import com.umapped.api.schema.types.PhoneNumber;
import com.umapped.api.schema.types.Privacy;
import com.umapped.persistence.accountprop.AccountContact;
import models.publisher.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.EmailException;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

/**
 * This controller should bit by bit replace UserController.
 * Created by surge on 2016-02-24.
 */
public class AccountController
    extends Controller {

  @Inject
  FormFactory formFactory;

  public static class AccountHelper {
    Account             account;
    Account             modifiedBy;
    UserProfile         userProfile;
    Account.AccountType accountType;
    String              resetToken;
    Operation           operation;
    Company             accountCmpy;
    AccountProp         contact;
    AccountProp         tripPreferences;
    AccountProp         accountPreferences;

    /**
     * For use only in code base not converted from legacy
     *
     * @param modifedUserId
     * @return
     */
    public static AccountHelper build(String modifedUserId) {
      AccountHelper ah = new AccountHelper();
      ah.modifiedBy = Account.findByLegacyId(modifedUserId);
      //In some cases we need password reset token
      ah.resetToken = UUID.randomUUID().toString();
      return ah;
    }

    public static AccountHelper build(Long modifiedByUid) {
      AccountHelper ah = new AccountHelper();
      ah.modifiedBy = Account.find.byId(modifiedByUid);
      //In some cases we need password reset token
      ah.resetToken = UUID.randomUUID().toString();
      return ah;
    }

    public static String generateLegacyId(String desiredId) {
      int retryCount = 0;
      //loop to handle concurrency and unique userid
      //give up after 5 to prevent loop
      int useridSuffix = 0;

      while (retryCount++ < 5) {
        try {
          //try to increment a suffix to avoid duplicate ids

          int count = Account.getContUserProfiles(desiredId);
          if (count > 0) {
            if (useridSuffix == 0) {
              useridSuffix = count;
            }
            else {
              if (count > useridSuffix) {
                useridSuffix = count;
              }
              else {
                useridSuffix++;
              }
            }

            String  userId = desiredId + String.valueOf(useridSuffix);
            Account a      = Account.findByLegacyId(userId);
            if (a == null) {
              return userId;
            }
          }
          return desiredId;
        }
        catch (Exception e) {
          e.printStackTrace();
          Log.err("generateLegacyId(): Failed to generate legacy id", e);
          return null;
        }
      }
      return null;
    }

    public boolean setProfilePhoto(FileInfo file) {
      try {
        if (userProfile != null) {
          userProfile.setProfilephoto(file.getFilename());
          userProfile.setProfilephotourl(file.getUrl());
          userProfile.setLastupdatedtimestamp(System.currentTimeMillis());
          userProfile.setModifiedby(modifiedBy.getLegacyId());
          userProfile.update();
        }

        if (account != null) {
          AccountProp accountProp = AccountProp.findByPk(account.getUid(), AccountProp.PropertyType.PHOTO);
          if (accountProp == null) {
            accountProp = AccountProp.build(account.getUid(), AccountProp.PropertyType.PHOTO, modifiedBy.getUid());
          }
          accountProp.setlVal(file.getPk());
          accountProp.save();
        }
      } catch (Exception e) {
        Log.err("setProfilePhoto(): Fail", e);
        return  false;
      }
      return true;
    }

    public AccountProp getContact() {
      return contact;
    }

    public AccountHelper setContact(AccountProp contact) {
      this.contact = contact;
      return this;
    }

    public AccountProp getTripPreferences() {
      return tripPreferences;
    }

    public AccountHelper setTripPreferences(AccountProp preference) {
      this.tripPreferences = preference;
      return this;
    }

    public AccountProp getAccountPreferences() {
      return accountPreferences;
    }

    public AccountHelper setAccountPreferences(AccountProp preference) {
      this.accountPreferences = preference;
      return this;
    }

    public Company getAccountCmpy() {
      return accountCmpy;
    }

    public AccountHelper setAccountCmpy(Company accountCmpy) {
      this.accountCmpy = accountCmpy;
      return this;
    }

    public boolean unlinkFromAll() {
      if (account == null) {
        Log.err("unlinkFromAll(): failed since account was not set");
        return false;
      }

      Transaction t = Ebean.beginTransaction();
      try {
        AccountCmpyLink acl = AccountCmpyLink.find.byId(account.getUid());
        if (acl != null) {
          acl.delete();
        }

        if (account.getLegacyId() != null) {
          UserCmpyLink.hardDeleteForUser(account.getLegacyId());
        }

        t.commit();
        return true;
      }
      catch (Exception e) {
        Log.err("unlinkFromAll(): Failed to unlink companies from user" + account.getLegacyId(), e);
        return false;
      }
    }

    public boolean unlinkFromCompany() {
      if (account == null) {
        Log.err("unlinkFromCompany(): failed since account was not set");
        return false;
      }
      Transaction t = Ebean.beginTransaction();
      try {
        AccountCmpyLink acl = AccountCmpyLink.find.byId(account.getUid());

        if (acl == null || !acl.getCmpyid().equals(accountCmpy.getCmpyid())) {
          return false;
        }

        acl.delete();

        if (account.getLegacyId() != null) {
          List<UserCmpyLink> cmpyLinks = UserCmpyLink.findByUserIdCmpyId(account.getLegacyId(),
                                                                         accountCmpy.getCmpyid());
          if (cmpyLinks != null) {
            for (UserCmpyLink uc : cmpyLinks) {
              uc.delete();
            }
          }
        }

        t.commit();
        return true;
      }
      catch (Exception e) {
        Log.err("unlinkFromCompany(): Failed to unlink company from user" + account.getLegacyId(), e);
        return false;
      }
    }

    public boolean linkToCompany(Company company, String linkType) {
      try {
        int                      linkTypeId = Integer.parseInt(linkType);
        UserCmpyLink.LinkType    lt         = UserCmpyLink.LinkType.fromInt(linkTypeId);
        AccountCmpyLink.LinkType aclt       = AccountCmpyLink.LinkType.fromUserCmpyLinkType(lt);
        return linkToCompany(company, aclt);
      }
      catch (Exception e) {
        Log.err("AccountHelper::linkToCompany() Failed Parsing Link Type", e);
        return false;
      }
    }

    public boolean linkToCompany(Company company, AccountCmpyLink.LinkType linkType) {
      try {
        AccountCmpyLink acl = AccountCmpyLink.build(account.getUid(), company.getCmpyid(), modifiedBy.getUid());
        acl.setLinkType(linkType);
        acl.save();

        if (userProfile != null) {
          UserCmpyLink ucl = UserCmpyLink.build(userProfile,
                                                company,
                                                UserCmpyLink.LinkType.fromAccountLinkType(linkType),
                                                modifiedBy.getLegacyId());
          ucl.save();
        }
        accountCmpy = company;
        return true;
      }
      catch (Exception e) {
        Log.err("AccountHelper::linkToCompany() Failed to link user to the company", e);
        return false;
      }
    }

    public boolean lockAccount() {
      if(account == null || userProfile == null) {
        return false;
      }

      Transaction t = Ebean.beginTransaction();
      try {
        AccountAuth auth = AccountAuth.getPassword(account.getUid());
        if (auth == null) {
          auth = AccountAuth.build(modifiedBy.getUid());
        }
        auth.setUid(account.getUid());
        auth.setAuthType(AccountAuth.AAuthType.PASSWORD);
        auth.setSalt(UUID.randomUUID().toString()); //Using UUID for salts
        auth.setResetToken(resetToken);
        auth.setResetExpire(System.currentTimeMillis() + APPConstants.PWD_SETUP_EXPIRY);
        auth.save();


        userProfile.setStatus(APPConstants.STATUS_ACCOUNT_LOCKED);
        userProfile.setPwdresetid(resetToken);
        userProfile.setPwdresetexpiry(System.currentTimeMillis() + APPConstants.PWD_SETUP_EXPIRY);
        userProfile.update();
        t.commit();
        return true;
      } catch ( Exception e) {
        t.rollback();
        Log.err("lockAccount(): Failed", e);
        return false;
      }
    }

    public boolean sendLinkEmail() {
      //Notifying new Umapped Admin of the new account via Email
      String fromEmail = ConfigMgr.getAppParameter(CoreConstants.EMAIL_PWD_RESET_FROM);
      String subject   = ConfigMgr.getAppParameter(CoreConstants.EMAIL_NEW_CMPY_LINK_SUBJECT);
      subject = subject + accountCmpy.name;
      StringBuffer htmlPart = new StringBuffer();

      htmlPart.append("<p>Your account privileges have been updated; you are now linked to  ");
      htmlPart.append(accountCmpy.name);
      htmlPart.append("</p>");

      if (modifiedBy.getAccountType() != Account.AccountType.UMAPPED_ADMIN) {
        htmlPart.append("<p>Your Umapped account has been updated by ").append(modifiedBy.getFullName()).append("</p>");
        htmlPart.append("<p> Email: " + modifiedBy.getEmail() + "</p>");
        AccountProp contacts = AccountProp.findByPk(modifiedBy.getUid(), AccountProp.PropertyType.CONTACT);
        if (contacts != null && contacts.accountContact.phones != null) {
          for (PhoneNumber.PhoneType pnt : contacts.accountContact.phones.keySet()) {
            PhoneNumber pn = contacts.accountContact.phones.get(pnt);
            htmlPart.append("<p>" + pnt.name() + ": " + pn.getNumber() + "</p>");
          }
        }
        if (modifiedBy.getMsisdn() != null) {
          htmlPart.append("<p> Mobile: " + modifiedBy.getMsisdn() + "</p>");
        }
      }
      htmlPart.append("<p>Please contact our customer service desk at help@umapped.com if you have any questions.</p>");

      List<String> emails = new ArrayList<>();
      emails.add(account.getEmail());

      try {
        UmappedEmail email = UmappedEmail.buildDefault();
        email.withHtml(htmlPart.toString())
             .withSubject(subject).withToList(emails)
             .withEmailType(EmailLog.EmailTypes.LINK_ACCOUNT)
             .withAccountUid(account.getUid())
             .setFrom(fromEmail, "Umapped")
             .addTo(account.getEmail());

        String emailId = email.buildAndSend();
        Log.debug("AccountHelper::sendLinkEmail(): Sent password reset email ID: " + emailId + " to: " + account
            .getEmail());
        return true;
      }
      catch (EmailException ee) {
        Log.err("AccountHelper::sendLinkEmail(): Cannot send email. " + account.getEmail(), ee);
        return false;
      }
    }

    public boolean sendUnlinkEmail() {
      if (accountCmpy == null) {
        Log.err("sendUnlinkEmail(): attempt to send unlink email with no company set");
        return false;
      }
      //Notifying new Umapped Admin of the new account via Email
      String subject   = ConfigMgr.getAppParameter(CoreConstants.EMAIL_DELETE_CMPY_LINK_SUBJECT);
      String fromEmail = ConfigMgr.getAppParameter(CoreConstants.EMAIL_PWD_RESET_FROM);
      subject = subject + accountCmpy.name;

      StringBuffer htmlPart = new StringBuffer();
      htmlPart.append("<p>Your account privileges have been updated; you are no longer linked to  ")
              .append(accountCmpy.getName())
              .append(" </p>");


      if (modifiedBy.getAccountType() != Account.AccountType.UMAPPED_ADMIN) {

        htmlPart.append("<p>Your Umapped account has been updated by ").append(modifiedBy.getFullName()).append("</p>");

        htmlPart.append("<p> Email: " + modifiedBy.getEmail() + "</p>");

        AccountProp contacts = AccountProp.findByPk(modifiedBy.getUid(), AccountProp.PropertyType.CONTACT);
        if (contacts != null && contacts.accountContact.phones != null) {
          for (PhoneNumber.PhoneType pnt : contacts.accountContact.phones.keySet()) {
            PhoneNumber pn = contacts.accountContact.phones.get(pnt);
            htmlPart.append("<p> " + pnt.name() + ": " + pn.getNumber() + "</p>");
          }
        }

        if (modifiedBy.getMsisdn() != null) {
          htmlPart.append("<p> Mobile: " + modifiedBy.getMsisdn() + "</p>");
        }
      }
      htmlPart.append("<p>Please contact our customer service desk at help@umapped.com if you have any questions.</p>");

      List<String> emails = new ArrayList<>();
      emails.add(account.getEmail());

      try {
        UmappedEmail email = UmappedEmail.buildDefault();
        email.withHtml(htmlPart.toString())
             .withSubject(subject).withToList(emails)
             .withEmailType(EmailLog.EmailTypes.UNLINK_ACCOUNT)
             .withAccountUid(account.getUid())
             .setFrom(fromEmail, "Umapped")
             .addTo(account.getEmail());

        String emailId = email.buildAndSend();
        Log.debug("AccountHelper::sendUnlinkEmail(): Sent company unlinke email ID: " + emailId + " to: " +
                  account.getEmail());
        return true;
      }
      catch (EmailException ee) {
        Log.err("AccountHelper::sendUnlinkEmail(): Cannot send email. " + account.getEmail(), ee);
        return false;
      }
    }

    public boolean sendEmail() {
      //Notifying new Umapped Admin of the new account via Email
      String       hostUrl   = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);
      String       subject   = ConfigMgr.getAppParameter(CoreConstants.EMAIL_NEW_UMAPPED_ACCOUNT_SUBJECT);
      String       fromEmail = ConfigMgr.getAppParameter(CoreConstants.EMAIL_PWD_RESET_FROM);
      StringBuffer htmlPart  = new StringBuffer();
      htmlPart.append("<p>Your Umapped Admin account has been set up by Umapped.</p>")
              .append("<p> Your user id is : ")
              .append(account.getLegacyId())
              .append("</p>")
              .append("<p> Click ")
              .append("<a href='")
              .append(hostUrl)
              .append(routes.SecurityController.resetPwdLink(resetToken).url())
              .append("' > here </a> to setup your password </p> <p>If you did not request a Umapped account, " +
                      "please contact our support desk at help@umapped.com.</p><p>Please note that this link will " +
                      "expire in 14 days.</p>");

      List<String> emails = new ArrayList<>();
      emails.add(account.getEmail());

      try {
        UmappedEmail email = UmappedEmail.buildDefault();
        email.withHtml(htmlPart.toString())
             .withSubject(subject).withToList(emails).withEmailType(EmailLog.EmailTypes.NEW_ACCOUNT)
             .withAccountUid(account.getUid())
             .setFrom(fromEmail, "Umapped")
             .addTo(account.getEmail());

        String emailId = email.buildAndSend();
        Log.debug("AccountHelper::sendEmail(): Sent password reset email ID: " + emailId + " to: " + account.getEmail());
        return true;
      }
      catch (EmailException ee) {
        Log.err("AccountHelper::sendEmail(): Cannot send email. " + account.getEmail(), ee);
        return false;
      }
    }

    public boolean sendWelcomeEmail() {
      String hostUrl   = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);
      String fromEmail = ConfigMgr.getAppParameter(CoreConstants.EMAIL_PWD_RESET_FROM);
      String subject   = ConfigMgr.getAppParameter(CoreConstants.EMAIL_NEW_ACCOUNT_SUBJECT);

      NewRegistrationView newRegistrationView = new NewRegistrationView();
      newRegistrationView.userId = account.getLegacyId();
      newRegistrationView.pwdResetLink = hostUrl + routes.SecurityController.resetPwdLink(resetToken).url();

      List<String> emails = new ArrayList<>();
      emails.add(account.getEmail());

      try {
        UmappedEmail ue = UmappedEmail.buildDefault();
        ue.withHtml(views.html.email.registrationconfirm.render(newRegistrationView).toString())
          .withSubject(subject)
          .withToList(emails)
          .withEmailType(EmailLog.EmailTypes.WELCOME)
          .withAccountUid(account.getUid())
          .setFrom(fromEmail, "Umapped")
          .addTo(account.getEmail());

        String emailId = ue.buildAndSend();
        Log.debug("AccountHelper::sendWelcomeEmail(): Sent Welcome email ID: " + emailId + " to: " +
                  account.getEmail());
        return true;
      }
      catch (EmailException ee) {
        Log.err("AccountHelper::sendWelcomeEmail(): Failed", ee);
        return false;
      }
    }

    public Operation getOperation() {
      return operation;
    }

    public AccountHelper setOperation(Operation operation) {
      this.operation = operation;
      return this;
    }

    public String getResetToken() {
      return resetToken;
    }

    public AccountHelper setResetToken(String resetToken) {
      this.resetToken = resetToken;
      return this;
    }

    /**
     * Provided a form with account information creates or updates database.
     *
     * @param profileForm - information about the account
     */
    public boolean processAccountForm(UserProfileForm profileForm) {
      if (modifiedBy == null) {
        Log.err("processAccountForm(): called with null modified by account information");
        return false;
      }

      if (operation == Operation.UPDATE && (userProfile == null || account == null)) {
        Log.err("AccountHelper::processAccountForm(): either account or user profile is null during UPDATE");
        return false;
      }

      Transaction t = Ebean.beginTransaction();
      try {
        crupAccount(profileForm.getInUserId(),
                    profileForm.getInUserFirstName(),
                    profileForm.getInUserLastName(),
                    profileForm.getInUserEmail(),
                    profileForm.getInUserMobile());

        crupAccountContact();
        addPhone(profileForm.getInUserPhone());
        addFax(profileForm.getInUserFax());
        addExtension(profileForm.getInUserExtension());
        addFacebook(profileForm.getInUserFacebook());
        addTwitter(profileForm.getInUserTwitter());
        addUrl(profileForm.getInUserWeb());
        contactPersist();

        t.commit();
        return true;
      }
      catch (Exception e) {
        t.rollback();
        Log.err("formToAccountHelper(): Problem creating/updating user", e);
        return false;
      }
    }

    /**
     * Creates or updates account with common parameters
     *
     * @param legacyId
     * @param firstName
     * @param lastName
     * @param email
     * @param mobile
     * @return
     */
    public boolean crupAccount(String legacyId, String firstName, String lastName, String email, String mobile) {

      if (operation == Operation.ADD) {
        //check to see if the account already exist as a traveler
        account = Account.findByEmail(email);
        if (account == null) {
          account = Account.build(modifiedBy.getUid());
        }
        account.setAccountType(accountType);
        account.setState(RecordStatus.LOCKED);
        account.setLegacyId(legacyId);
      }
      else {
        account.markModified(modifiedBy.getUid());
      }

      account.setFirstName(firstName);
      account.setLastName(lastName);
      account.setDefaultInitials();

      account.setEmail(email);
      account.setMsisdn(mobile);
      account.save();


      if (operation == Operation.ADD) {
        userProfile = new UserProfile();
        userProfile.setUserid(legacyId);
        userProfile.setStatus(APPConstants.STATUS_ACCOUNT_LOCKED);
        userProfile.setCreatedby(modifiedBy.getLegacyId());
        userProfile.setCreatedtimestamp(System.currentTimeMillis());

        switch (accountType) {
          case UMAPPED_ADMIN:
            userProfile.setUsertype(APPConstants.USER_ACCOUNT_TYPE_UMAPPED);
            break;
          case PUBLISHER:
          default:
            userProfile.setUsertype(APPConstants.USER_ACCOUNT_TYPE_REGULAR);
            break;
        }

        userProfile.setPwdresetid(resetToken);
        userProfile.setPwdresetexpiry(System.currentTimeMillis() + APPConstants.PWD_SETUP_EXPIRY);
        userProfile.setLastlogintimestamp(userProfile.getCreatedtimestamp());
      }

      userProfile.setFirstname(firstName);
      userProfile.setLastname(lastName);
      userProfile.setEmail(email);
      userProfile.setMobile(mobile);

      userProfile.setModifiedby(modifiedBy.getLegacyId());
      userProfile.setLastupdatedtimestamp(System.currentTimeMillis());
      userProfile.save();

      if (operation == Operation.ADD) {
        //precaution delete the auth if any  that was created via mobile
        List <AccountAuth> existing = AccountAuth.getAuthsForType(account.getUid(), AccountAuth.AAuthType.PASSWORD);
        if (existing != null && existing.size() > 0) {
          for (AccountAuth a: existing) {
            a.delete();
          }
        }

        AccountAuth auth = AccountAuth.build(modifiedBy.getUid());
        auth.setUid(account.getUid());
        auth.setAuthType(AccountAuth.AAuthType.PASSWORD);
        auth.setSalt(UUID.randomUUID().toString()); //Using UUID for salts
        auth.setResetToken(resetToken);
        auth.setResetExpire(System.currentTimeMillis() + APPConstants.PWD_SETUP_EXPIRY);
        auth.save();
      }

      return true;
    }

    public void crupAccountContact() {
      contact = AccountProp.findByPk(account.getUid(), AccountProp.PropertyType.CONTACT);
      if (contact == null) {
        contact = AccountProp.build(account.getUid(), AccountProp.PropertyType.CONTACT, modifiedBy.getUid());
      }
      else {
        contact.accountContact = new AccountContact(); //Essentially resetting
        contact.markModified(modifiedBy.getUid());
      }
    }

    public void crupTripPreferences() {
      contact = AccountProp.findByPk(account.getUid(), AccountProp.PropertyType.TRIP_PREFERENCE);
      if (contact == null) {
        contact = AccountProp.build(account.getUid(), AccountProp.PropertyType.TRIP_PREFERENCE, modifiedBy.getUid());
      }
      else {
        contact.accountContact = new AccountContact(); //Essentially resetting
        contact.markModified(modifiedBy.getUid());
      }
    }

    public void crupUserPreferences() {
      contact = AccountProp.findByPk(account.getUid(), AccountProp.PropertyType.ACCOUNT_PREFERENCE);
      if (contact == null) {
        contact = AccountProp.build(account.getUid(), AccountProp.PropertyType.ACCOUNT_PREFERENCE, modifiedBy.getUid());
      }
      else {
        contact.accountContact = new AccountContact(); //Essentially resetting
        contact.markModified(modifiedBy.getUid());
      }
    }

    public void addPhone(String phoneNum) {
      if (phoneNum != null && phoneNum.length() > 0) {
        PhoneNumber phone = new PhoneNumber(phoneNum, PhoneNumber.PhoneType.BUSINESS, Privacy.PUBLIC);
        contact.accountContact.addPhone(phone);
        userProfile.setPhone(phoneNum);
      } else {
        userProfile.setPhone(null);
      }
    }

    public void addFax(String faxNum) {
      if (faxNum != null && faxNum.length() > 0) {
        PhoneNumber phone = new PhoneNumber(faxNum, PhoneNumber.PhoneType.FAX, Privacy.PUBLIC);
        contact.accountContact.addPhone(phone);
        userProfile.setFax(faxNum);
      } else {
        userProfile.setFax(null);
      }
    }

    public void addExtension(String ext) {
      if (ext != null && ext.trim().length() > 0) {
        PhoneNumber phone = new PhoneNumber(ext.trim(), PhoneNumber.PhoneType.EXT, Privacy.PUBLIC);
        contact.accountContact.addPhone(phone);
      }

    }

    public void addFacebook(String fb) {
      if (fb != null && fb.length() > 0) {
        if (fb.contains("http")) {
          SocialLink socialLink = new SocialLink("Facebook", null, fb);
          contact.accountContact.addSocialLink(socialLink);
        }
        else {
          SocialLink socialLink = new SocialLink("Facebook", fb, null);
          contact.accountContact.addSocialLink(socialLink);
        }
        userProfile.setFacebook(fb);
      } else {
        userProfile.setFacebook(null);
      }
    }

    public void addTwitter(String twitter) {
      if (twitter != null && twitter.length() > 0) {
        if (twitter.contains("http")) {
          SocialLink socialLink = new SocialLink("Twitter", null, twitter);
          contact.accountContact.addSocialLink(socialLink);
        }
        else {
          SocialLink socialLink = new SocialLink("Twitter", twitter, null);
          contact.accountContact.addSocialLink(socialLink);
        }
        userProfile.setTwitter(twitter);
      } else {
        userProfile.setTwitter(null);
      }
    }

    public void addUrl(String url) {
      if (url != null && url.length() > 0) {
        contact.accountContact.addUrl(url);
        userProfile.setWeb(url);
      } else {
        userProfile.setWeb(null);
      }
    }

    public void contactPersist() {
      if (contact.accountContact.hasRecords()) {
        contact.prePersist();
        contact.save();
      }

      if (userProfile != null) {
        userProfile.update();
      }
    }

    public boolean authFail() {
      if (account != null) {
        AccountAuth aa = AccountAuth.getPassword(account.getUid());
        if (aa != null) {
          aa.setFailLastTs(Timestamp.from(Instant.now()));
          if (aa.getFailCount() != null) {
            aa.setFailCount(aa.getFailCount() + 1);
          } else {
            aa.setFailCount(1);
          }
          aa.update();
        }
      }

      if (userProfile != null) {
        //update failed login count
        //maybe suspend account if too many
        userProfile.refresh();
        userProfile.setLastlogintimestamp(System.currentTimeMillis());
        userProfile.setFailedlogincount(userProfile.getFailedlogincount() + 1);
        userProfile.update();
        //if the failed count
        if (userProfile.getFailedlogincount() > 6) {
          //let's lock the profile
          account.refresh();
          account.setState(RecordStatus.LOCKED);
          account.save();
          userProfile.refresh();
          userProfile.setStatus(APPConstants.STATUS_ACCOUNT_LOCKED);
          userProfile.save();
          return true;
        }
        SecurityController.auditActivity(userProfile.userid, APPConstants.USER_AUDIT_LOGON_FAILED);
      }
      return false;
    }

    public void authSuccess() {
      if (account != null) {
        AccountAuth aa = AccountAuth.getPassword(account.getUid());
        if (aa != null) {
          aa.updateOnSuccess();
        }
      }

      if (userProfile != null) {
        userProfile.refresh();
        userProfile.setLastlogintimestamp(System.currentTimeMillis());
        userProfile.setFailedlogincount(0);
        userProfile.update();
        SecurityController.auditActivity(userProfile.getUserid(), APPConstants.USER_AUDIT_LOGON);
      }
    }

    public boolean fromLegacyId(String legacyId) {
      account = Account.findByLegacyId(legacyId);
      userProfile = UserProfile.findByPK(legacyId);
      return account != null && userProfile != null;
    }

    public boolean deleteUser() {

      Transaction t = Ebean.beginTransaction();
      try {
        if (account != null) {
          account.setState(RecordStatus.DELETED);
          account.markModified(modifiedBy.getUid());
          account.update();
        }

        if (userProfile != null) {
          userProfile.setStatus(APPConstants.STATUS_DELETED);
          userProfile.setLastupdatedtimestamp(System.currentTimeMillis());
          userProfile.setModifiedby(modifiedBy.getLegacyId());
          userProfile.save();
        }
        t.commit();
      }
      catch (Exception e) {
        t.rollback();
        Log.err("deleteUser(): Failed.", e);
        return false;
      }

      return true;
    }

    public boolean resetPassword(String newPassword) {
      if (account == null || userProfile == null || modifiedBy == null) {
        Log.err("resetPassword(): Not properly configured Helper class - not all variables are set.");
        return false;
      }

      Transaction t = Ebean.beginTransaction();

      try {
        Ebean.beginTransaction();
        AccountAuth aa = AccountAuth.getPassword(account.getUid());
        if (aa == null) {
          aa = AccountAuth.build(modifiedBy.getUid());
          aa.setUid(account.getUid());
        }
        else {
          aa.markModified(modifiedBy.getUid());
        }
        aa.setAuthType(AccountAuth.AAuthType.PASSWORD);
        aa.setSalt(UUID.randomUUID().toString());
        aa.setValue(Utils.hashPwd(newPassword, String.valueOf(aa.getSalt())));
        aa.setFailCount(0);
        aa.setResetToken(null);
        aa.save();

        //UserProfile password reset logic
        List<UserPwd> userPwdList = UserPwd.getActivePwd(account.getLegacyId());
        if (userPwdList != null) {
          for (UserPwd userPwd : userPwdList) {
            userPwd.setStatus(APPConstants.STATUS_DELETED);
            userPwd.setLastupdatedtimestamp(System.currentTimeMillis());
            userPwd.setModifiedby(modifiedBy.getLegacyId());
            userPwd.save();
          }
        }

        if (account.getState() == RecordStatus.LOCKED) {
          userProfile.setModifiedby(modifiedBy.getLegacyId());
          userProfile.setLastupdatedtimestamp(System.currentTimeMillis());
          userProfile.setStatus(APPConstants.STATUS_ACTIVE);
          userProfile.setFailedlogincount(0);
          userProfile.save();
          account.setState(RecordStatus.ACTIVE);
          account.setModifiedBy(modifiedBy.getUid());
          account.setModifiedTs(Timestamp.from(Instant.now()));
          account.save();
        }

        //create the new password
        UserPwd userPwd = new UserPwd();
        userPwd.setPk(Utils.getUniqueId());
        userPwd.setUserId(account.getLegacyId());
        userPwd.setStatus(APPConstants.STATUS_ACTIVE);
        userPwd.setCreatedby(modifiedBy.getLegacyId());
        userPwd.setCreatedtimestamp(System.currentTimeMillis());
        userPwd.setModifiedby(modifiedBy.getLegacyId());
        userPwd.setLastupdatedtimestamp(System.currentTimeMillis());
        userPwd.setPassword(Utils.hashPwd(newPassword, String.valueOf(userProfile.getCreatedtimestamp())));
        userPwd.save();

        t.commit();
        SecurityController.auditActivity(account.getLegacyId(), APPConstants.USER_AUDIT_PASSWORD_CHANGED);
        //TODO: ACCOUNT AUDIT - TO BE IMPLEMENTED
        return true;
      }
      catch (Exception e) {
        t.rollback();
        Log.err("resetPassword(): Error while resetting password.", e);
        return false;
      }
    }

    public Account getAccount() {
      return account;
    }

    public AccountHelper setAccount(Account account) {
      this.account = account;
      return this;
    }

    public Account getModifiedBy() {
      return modifiedBy;
    }

    public AccountHelper setModifiedBy(Account modifiedBy) {
      this.modifiedBy = modifiedBy;
      return this;
    }

    public UserProfile getUserProfile() {
      return userProfile;
    }

    public AccountHelper setUserProfile(UserProfile userProfile) {
      this.userProfile = userProfile;
      return this;
    }

    public Account.AccountType getAccountType() {
      return accountType;
    }

    public AccountHelper setAccountType(Account.AccountType accountType) {
      this.accountType = accountType;
      return this;
    }
  }


  /**
   * Main migration initiator from user profiles
   *
   * @return
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result migrate() {

    Chunks<String> chunks = StringChunks.whenReady(out -> {

      out.write("Umapped Account Migration" + System.lineSeparator());

      List<UserProfile> profilesToMigrate = UserProfile.userProfileOutOfSync();

      Transaction t = Ebean.beginTransaction();
      t.setBatchMode(true);
      t.setBatchSize(200);

      SqlUpdate su = Ebean.createSqlUpdate("SET CONSTRAINTS ALL DEFERRED");
      Ebean.execute(su);

      Map<UserProfile, Account> converted = new HashMap<>();

      //Pass #1 Creating accounts
      for (UserProfile up : profilesToMigrate) {
        Account a = Account.findByLegacyId(up.userid);
        if (a == null) {
          a = Account.build(0l); //By default just goes as system user
        }

        a.setFirstName(up.getFirstname());
        a.setLastName(up.getLastname());
        a.setInitials(up.getInitials()); //Default initials
        a.setEmail(up.getEmail());
        a.setMsisdn(up.getMobile());
        a.setMsisdnValid(false);
        a.setCreatedTs(new Timestamp(up.getCreatedtimestamp()));
        a.setModifiedTs(new Timestamp(up.getLastupdatedtimestamp()));
        a.setLegacyId(up.getUserid());

        switch (up.getUsertype()) {
          case APPConstants.USER_ACCOUNT_TYPE_REGULAR:
            a.setAccountType(Account.AccountType.PUBLISHER);
            break;
          case APPConstants.USER_ACCOUNT_TYPE_UMAPPED:
            a.setAccountType(Account.AccountType.UMAPPED_ADMIN);
            break;
        }
        switch (up.getStatus()) {
          case APPConstants.STATUS_DELETED:
            a.setState(RecordStatus.DELETED);
            break;
          case APPConstants.STATUS_ACTIVE:
            a.setState(RecordStatus.ACTIVE);
            break;
          case APPConstants.STATUS_ACCOUNT_LOCKED:
            a.setState(RecordStatus.LOCKED);
            break;
        }

        a.save();
        converted.put(up, a);
      }
      t.commit();
      out.write("Created " + converted.size() + " accounts from: " + profilesToMigrate.size() + System.lineSeparator());

      //Pass #2: Fixing created/modfified by records
      t = Ebean.beginTransaction();
      t.setBatchMode(true);
      t.setBatchSize(200);
      try {
        for (UserProfile up : converted.keySet()) {
          if (up.createdby.equalsIgnoreCase("system")) {
            up.setCreatedby("system");
          }
          if (up.modifiedby.equalsIgnoreCase("system")) {
            up.setModifiedby("system");
          }

          Account curr       = converted.get(up);
          Account createdBy  = Account.findByLegacyId(up.createdby);
          Account modifiedBy = Account.findByLegacyId(up.modifiedby);

          if (createdBy == null) {
            Log.err("Can't find createdBy user: " + up.createdby);
          }
          else {
            curr.setCreatedBy(createdBy.getUid());
          }

          if (modifiedBy == null) {
            Log.err("Can't find modifiedBy user: " + up.modifiedby);
          }
          else {
            curr.setModifiedBy(modifiedBy.getUid());
          }
          curr.update();
        }
        t.commit();
      }
      catch (Exception e) {
        Log.err("Unexpected error:", e);
        t.rollback();
        return;
      }

      out.write("Updated created/updated metadata on accounts" + System.lineSeparator());

      //Pass #3: Porting Authentication
      t = Ebean.beginTransaction();
      t.setBatchMode(true);
      t.setBatchSize(200);
      for (UserProfile up : converted.keySet()) {
        Account a   = converted.get(up);
        UserPwd pwd = UserPwd.getUserPwd(up.getUserid());

        if (pwd == null) {
          Log.err("User password missing: " + up.getUserid());
          continue;
        }

        if (pwd.createdby.equalsIgnoreCase("system")) {
          pwd.setCreatedby("system");
        }
        if (pwd.modifiedby.equalsIgnoreCase("system")) {
          pwd.setModifiedby("system");
        }

        Account createdBy  = Account.findByLegacyId(pwd.getCreatedby());
        Account modifiedBy = Account.findByLegacyId(pwd.getModifiedby());

        AccountAuth au = AccountAuth.getPassword(a.getUid());

        if (au == null) {
          //Assuming that the last person who modified is the one who set password
          au = AccountAuth.build(a.getModifiedBy());
        }

        au.setUid(a.getUid());
        au.setAuthType(AccountAuth.AAuthType.PASSWORD);
        au.setValue(pwd.getPassword());
        au.setSalt(up.getCreatedtimestamp().toString()); //Fixed version salt set from profile timestamp
        au.setCreatedTs(new Timestamp(pwd.getCreatedtimestamp()));
        au.setModifiedTs(new Timestamp(pwd.getLastupdatedtimestamp()));
        au.setCreatedBy(createdBy.getUid());
        au.setModifiedBy(modifiedBy.getUid());
        au.save();
      }
      t.commit();
      out.write("Created/Updated Passwords" + System.lineSeparator());


      //Pass #4: Porting other UP parameters
      t = Ebean.beginTransaction();
      t.setBatchMode(true);
      t.setBatchSize(200);
      for (UserProfile up : converted.keySet()) {
        Account a = converted.get(up);

        AccountProp contact = AccountProp.findByPk(a.getUid(), AccountProp.PropertyType.CONTACT);

        if (contact == null) {
          contact = AccountProp.build(a.getUid(), AccountProp.PropertyType.CONTACT, a.getCreatedBy());
        }
        AccountContact ac = contact.accountContact;
        if (up.getFacebook() != null && up.getFacebook().trim().length() > 0) {
          if (up.getFacebook().contains("http")) {
            SocialLink socialLink = new SocialLink("Facebook", null, up.getFacebook());
            ac.addSocialLink(socialLink);
          }
          else {
            SocialLink socialLink = new SocialLink("Facebook", up.getFacebook(), null);
            ac.addSocialLink(socialLink);
          }
        }

        if (up.getTwitter() != null && up.getTwitter().trim().length() > 0) {
          if (up.getTwitter().contains("http")) {
            SocialLink socialLink = new SocialLink("Twitter", null, up.getTwitter());
            ac.addSocialLink(socialLink);
          }
          else {
            SocialLink socialLink = new SocialLink("Twitter", up.getTwitter(), null);
            ac.addSocialLink(socialLink);
          }
        }
        if (up.getWeb() != null && up.getWeb().trim().length() > 0) {
          ac.addUrl(up.getWeb().trim());
        }
        if (up.getPhone() != null && up.getPhone().trim().length() > 0) {
          PhoneNumber phoneNumber = new PhoneNumber();
          phoneNumber.setNumber(up.getPhone());
          phoneNumber.setPhoneType(PhoneNumber.PhoneType.BUSINESS);
          ac.addPhone(phoneNumber);
        }
        if (up.getFax() != null && up.getFax().trim().length() > 0) {
          PhoneNumber phoneNumber = new PhoneNumber();
          phoneNumber.setNumber(up.getFax());
          phoneNumber.setPhoneType(PhoneNumber.PhoneType.FAX);
          ac.addPhone(phoneNumber);
        }
        if (ac.hasRecords()) {
          contact.save();
        }
      }
      t.commit();
      out.write("Created/Updated Account Contacts" + System.lineSeparator());

      //Pass #5: User Profile Photos
      t = Ebean.beginTransaction();
      t.setBatchMode(true);
      t.setBatchSize(20);
      FileSrc fSrc       = FileSrc.find.byId(FileSrc.FileSrcType.ACCOUNT);
      String  bucket     = fSrc.getRealBucketName();
      int     photoCount = 0;

      for (UserProfile up : converted.keySet()) {
        Account     a         = converted.get(up);
        AccountProp photoProp = AccountProp.build(a.getUid(), AccountProp.PropertyType.PHOTO, a.getCreatedBy());

        String fileName = up.getProfilephoto();
        if (fileName == null) {
          continue; //Nothing to see here
        }

        ObjectMetadata oldPhotoMeta = S3Util.getMetadata(up.getProfilephotourl());
        if (oldPhotoMeta != null) {
          FileInfo file = FileInfo.byHash(oldPhotoMeta.getETag());
          if (file != null) { //Someone already has file with this hash
            photoProp.setlVal(file.getPk());
            photoProp.save();
            Log.info("AccountController:migrate: Duplicate Photo:" + up.getProfilephotourl());
            continue;
          }
        }
        else {
          Log.info("AccountController:migrate: S3 file access error" + up.getProfilephotourl());
          continue;
        }

        FileInfo file = FileInfo.buildFileInfo(a.getUid(), FileSrc.FileSrcType.ACCOUNT, 0);
        file.setExpire(null); //Does not expire
        photoProp.setlVal(file.getPk());


        int oldSubstr = up.getProfilephoto().indexOf('_');
        if (oldSubstr != -1) {
          fileName = up.getProfilephoto().substring(oldSubstr + 1);
        }

        String newKey = Utils.shortenNumber(a.getUid()) + "/" + Utils.shortenNumber(file.getPk()) + "_" + fileName;

        //Check if file is already in new bucket
        FileInfo fi = FileInfo.byBucketAndKeyFromSource(fSrc, bucket, newKey);
        if (fi != null) {
          continue;
        }

        CopyObjectResult res = S3Util.s3CopyObject(up.getProfilephotourl(), bucket, newKey, null);
        if (res != null) {
          if (S3Util.setObjectPublic(bucket, newKey)) {
            file.setFilesize(FileInfo.UNKNOWN_FILESIZE); //Unknown for now
            ObjectMetadata meta = S3Util.getMetadata(bucket, newKey);
            if (file.updateFromMetadata(meta)) {
              Log.debug("AccountController: Updated file information successfully: " + newKey);
            }
            file.setFilename(fileName);
            file.setFilepath(newKey);
            file.setFiletype(LstFileType.findFromFileName(fileName));
            file.setAsPublicUrl();
            file.setBucket(bucket);
            file.save();
            photoProp.save();
            Log.info("Migrated account photo: #" + photoCount++ + " as " + file.getUrl());
          }
          else {
            Log.err("AccountController: Skipping object: " + file.getFilename() + " Can't set public access");
          }
        }
        else {
          Log.err("Conversion error");
        }
      }
      t.commit();
      out.write("Moved " + photoCount + " account photos" + System.lineSeparator());

      //Pass #6: User company link to Account Company Link
      t = Ebean.beginTransaction();
      t.setBatchMode(true);
      t.setBatchSize(200);
      int linkedAccounts = 0;
      for (UserProfile up : converted.keySet()) {
        Account            a        = converted.get(up);
        List<UserCmpyLink> oldLinks = UserCmpyLink.findActiveByUserId(up.getUserid());
        if (oldLinks == null || oldLinks.size() == 0) {
          Log.err("AccountController: User: " + up.getUserid() + " has no company links");
          continue;
        }
        UserCmpyLink ucl = oldLinks.get(0); //All other links are gone now

        if (ucl.createdby.equalsIgnoreCase("system")) {
          ucl.setCreatedby("system");
        }
        if (ucl.modifiedby.equalsIgnoreCase("system")) {
          ucl.setModifiedby("system");
        }

        Account createdBy  = Account.findByLegacyId(ucl.getCreatedby());
        Account modifiedBy = Account.findByLegacyId(ucl.getModifiedby());

        AccountCmpyLink acl = AccountCmpyLink.find.byId(a.getUid());
        if (acl == null) {
          acl = AccountCmpyLink.build(a.getUid(), ucl.getCmpy().getCmpyid(), createdBy.getUid());
        }

        switch (ucl.getLinktype()) {
          case SUSPENDED:
            acl.setLinkType(AccountCmpyLink.LinkType.SUSPENDED);
            break;
          case ADMIN:
            acl.setLinkType(AccountCmpyLink.LinkType.ADMIN);
            break;
          case POWER:
            acl.setLinkType(AccountCmpyLink.LinkType.POWER);
            break;
          default:
          case MEMBER:
            acl.setLinkType(AccountCmpyLink.LinkType.MEMBER);
            break;
        }

        acl.setCreatedBy(createdBy.getUid());
        acl.setModifiedBy(modifiedBy.getUid());
        acl.setCreatedTs(new Timestamp(ucl.getCreatedtimestamp()));
        acl.setModifiedTs(new Timestamp(ucl.getLastupdatedtimestamp()));

        acl.save();
        linkedAccounts++;
      }
      t.commit();
      out.write("Lined " + linkedAccounts + " accounts to companies." + System.lineSeparator());
      out.close();

    });
    return ok(chunks);
  }




  /**
   * Main migration initiator from user profiles
   *
   * @return
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result migrateTravelers() {

    Chunks<String> chunks = StringChunks.whenReady(out -> {
      out.write("Umapped Traveler Account Migration" + System.lineSeparator());

      List<TripPassenger> passengers = TripPassenger.findAllActive();
      int i = 0;
      int newAccount = 0;
      int max = passengers.size();
      for (TripPassenger tp: passengers) {
        Transaction t = Ebean.beginTransaction();
        t.setBatchMode(true);
        t.setBatchSize(200);

        //do we already have an account
        Account traveler = Account.findByEmail(tp.email);
        boolean save = false;
        if (traveler == null) {
          //we do not - so we create a traveler account with status pending
          traveler = Account.build(0L);
          traveler.setEmail(tp.email);
          traveler.setAccountType(Account.AccountType.TRAVELER);
          traveler.setState(RecordStatus.PENDING);
          save = true;
          newAccount++;
        }
        if (traveler.getAccountType()== Account.AccountType.TRAVELER) {
          //let's see if we have more info only for traveler
          String fName = getFirstName(tp.name);
          String lName = getLastName(tp.name);
          if (traveler.getFirstName() == null || (fName != null && !traveler.getFirstName().equals(fName) && traveler.getFirstName().length() < fName.length())) {
            traveler.setFirstName(fName);
            save = true;
          }
          if (traveler.getLastName() == null || (lName != null && !traveler.getLastName().equals(lName) && traveler.getLastName().length() < lName.length())) {
            traveler.setLastName(lName);
            save = true;
          }
          if (tp.getMsisdn() != null && (traveler.getMsisdn() == null || !traveler.getMsisdn().equals(tp.getMsisdn()))) {
            traveler.setMsisdn(tp.getMsisdn());
            save = true;
          }
          if (save) {
            traveler.save();
          }
        }

        //let's create the link
        long agentUid = 0L;
        Account agent = null;
        if (tp.getCreatedby() != null && !tp.getCreatedby().toLowerCase().trim().equals("system")){
          agent = Account.findByLegacyId(tp.getCreatedby());
          if (agent != null) {
            agentUid = agent.getUid();
          }
        }

        //for re runnability, we check to see if the link already exists to avoid a duplicate key
        AccountTripLink.ATLPk accountLinkPk = new AccountTripLink.ATLPk();
        accountLinkPk.setTripid(tp.getTripid());
        accountLinkPk.setUid(traveler.getUid());
        AccountTripLink existingTripLink = AccountTripLink.find.byId(accountLinkPk);
        if (existingTripLink == null) {
          AccountTripLink tripLink = AccountTripLink.build(agentUid, traveler.getUid(), tp.getPk(), tp.getGroupid(), tp.getTripid(), AccountDetailLink.DetailLinkType.TRAVELER);
          tripLink.save();
        }

        //let's create a link to the agent who created added the traveler to the trip
        if (agent != null) {
          if (!AccountLink.doesLinkExists(agent.getUid(), traveler.getUid())) {
            //if there is not link, let's create one
            AccountLink accountLink = AccountLink.build(0L);
            accountLink.setUidFrom(agent.getUid());
            accountLink.setUidTo(traveler.getUid());
            accountLink.setLinkType(AccountLink.LinkType.TRAVELER);
            accountLink.save();
          }
        }


        i++;
        if (i%100 == 0) {
          out.write("New Account inserted " + newAccount + System.lineSeparator());
          out.write("Processing " + i + " of " + max + " " + System.lineSeparator());
          Log.debug("New Account inserted " + newAccount );
          Log.debug("Processing " + i + " of " + max );
        }
        t.commit();
      }
      out.write("New Account inserted " + newAccount + System.lineSeparator());
      out.write("Processing " + i + " of " + max + " " + System.lineSeparator());
      out.close();
      Log.debug("New Account inserted " + newAccount );
      Log.debug("Processing " + i + " of " + max );
    });
    return ok(chunks);
  }

  public static AccountTripLink addTravelerToTrip (String createdBy, String groupId, String tripId, String name, String email, String msisdn) {
    String firstName = getFirstName(name);
    String lastName = getLastName(name);

    return addTravelerToTrip(createdBy, groupId, tripId, firstName, lastName, email,msisdn);
  }


    public static AccountTripLink addTravelerToTrip (String createdBy, String groupId, String tripId, String firstName, String lastName, String email, String msisdn) {
    //TODO: Traveler Migration - remove legacy id
    String legacyId = DBConnectionMgr.getUniqueId();

    //check to see if we need to create the traveler account
    Account traveler = Account.findByEmail(email);
    Account agent = null;
    if (StringUtils.isNumeric(createdBy)) {
      agent = Account.find.byId(Long.parseLong(createdBy));
    } else {
      agent = Account.findActiveByLegacyId(createdBy);
    }
    long agentUid = 0L;
    if (agent != null) {
      agentUid = agent.getUid();
    }

    try {
      Ebean.beginTransaction();
      if (traveler == null) {
        //create the traveler account
        traveler = Account.build(agentUid);
        traveler.setEmail(email);
        traveler.setAccountType(Account.AccountType.TRAVELER);
        traveler.setState(RecordStatus.PENDING);
        traveler.setFirstName(firstName);
        traveler.setLastName(lastName);
        traveler.setMsisdn(msisdn);
        traveler.save();
      } else if (traveler.getAccountType() == Account.AccountType.TRAVELER) {
        //check if we need to update the traveler name
        boolean update = false;

        if (traveler.getFirstName() == null || (firstName != null && !traveler.getFirstName().equals(firstName))) {
          traveler.setFirstName(firstName);
          update = true;
        }
        if (traveler.getLastName() == null || (lastName != null && !traveler.getLastName().equals(lastName) )) {
          traveler.setLastName(lastName);
          update = true;
        }

        if (msisdn != null && (traveler.getMsisdn() == null || !traveler.getMsisdn().equals(msisdn))) {
          traveler.setMsisdn(msisdn);
          update = true;
        }

        if (update) {
          traveler.setModifiedBy(agent.getUid());
          traveler.setModifiedTs(Timestamp.from(Instant.now()));
          traveler.save();
        }

      }

      //let's create the trip link now
      AccountTripLink.ATLPk accountLinkPk = new AccountTripLink.ATLPk();
      accountLinkPk.setTripid(tripId);
      accountLinkPk.setUid(traveler.getUid());
      AccountTripLink existingTripLink = AccountTripLink.find.byId(accountLinkPk);
      AccountTripLink tripLink = null;
      if (existingTripLink == null) {
        tripLink = AccountTripLink.build(agentUid, traveler.getUid(), legacyId, groupId, tripId, AccountDetailLink.DetailLinkType.TRAVELER);
        tripLink.save();
      }

      try {
        //let's create a link to the agent who created added the traveler to the trip
        if (agent != null) {
          if (!AccountLink.doesLinkExists(agent.getUid(), traveler.getUid())) {
            //if there is not link, let's create one
            AccountLink accountLink = AccountLink.build(0L);
            accountLink.setUidFrom(agent.getUid());
            accountLink.setUidTo(traveler.getUid());
            accountLink.setLinkType(AccountLink.LinkType.TRAVELER);
            accountLink.save();
          }
        }
      } catch (Exception e) {

      }
      Ebean.commitTransaction();
      return tripLink;
    } catch (Exception e) {
      Ebean.rollbackTransaction();
    }

    return null;

  }

  public static String getFirstName (String s) {
    if (s != null && s.indexOf(" ") > 1) {
      return s.substring(0,s.indexOf(" ")).trim();
    }
    return null;
  }

  public static String getLastName (String s) {
    if (s != null && s.indexOf(" ") > 1 && s.indexOf(" ") < s.length()) {
      return s.substring(s.indexOf(" ")+1).trim();
    }
    return null;
  }


}
