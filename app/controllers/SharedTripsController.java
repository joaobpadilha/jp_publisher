package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import com.mapped.common.EmailMgr;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.audit.AuditActionType;
import com.mapped.publisher.audit.AuditActorType;
import com.mapped.publisher.audit.AuditModuleType;
import com.mapped.publisher.audit.event.AuditCollaboration;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.DataTablesForm;
import com.mapped.publisher.form.TripSearchForm;
import com.mapped.publisher.form.sharedTrip.FormChangeSharedTripAccessLevel;
import com.mapped.publisher.form.sharedTrip.FormResendTripShareEmail;
import com.mapped.publisher.form.sharedTrip.FormRevokeTripShareFromUser;
import com.mapped.publisher.form.sharedTrip.FormShareTripWithUser;
import com.mapped.publisher.persistence.CompanyMgr;
import com.mapped.publisher.persistence.SharedTripRS;
import com.mapped.publisher.persistence.TripMgr;
import com.mapped.publisher.persistence.communicator.MessengerUpdateHelper;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;
import com.umapped.api.schema.types.ReturnCode;
import models.publisher.*;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.sql.Connection;
import java.util.*;

/**
 * Created by twong on 2014-05-28.
 */
public class SharedTripsController extends Controller{

  @Inject
  FormFactory formFactory;

  @With({Authenticated.class, Authorized.class})
  public Result sharedTrips () {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<TripSearchForm> tripForm = formFactory.form(TripSearchForm.class);
    TripSearchForm tripSearchForm = tripForm.bindFromRequest().get();

    String term="";
    if (tripSearchForm.getInSearchTerm() != null && tripSearchForm.getInSearchTerm().length() > 0) {
      term = "%" + tripSearchForm.getInSearchTerm().replace(" ", "%") + "%";
    } else
      term = "%";



    String cmpyId = tripSearchForm.getInCmpyId();

    TripsView view = new TripsView();
    view.searchResults = new ArrayList<>();


    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      view.message = msg;
    }

    Credentials cred = sessionMgr.getCredentials();

    Map<String, String> availableCmpies = SharedTripsController.getSharingCompanies(sessionMgr.getUserId());

    ArrayList<String> cmpies = new ArrayList<String>();
    HashMap<String, Company> cmpiesMap = new HashMap<>();


    HashMap<String, String> agencyList = new HashMap<String, String>();
    if (availableCmpies != null)  {

      for (String  id: availableCmpies.keySet()) {
        agencyList.put(id, availableCmpies.get(id));
        //for future look up
        Company c = new Company();
        c.setName(availableCmpies.get(id));
        c.setCmpyid(id);
        cmpiesMap.put(id, c);
      }
      cmpies.addAll(availableCmpies.keySet());
    }

    if (agencyList.size() > 1)
      agencyList.put("All", "All");

    view.tripAgencyList = agencyList;

    //filter cmpy
    if (cmpyId != null && !cmpyId.equalsIgnoreCase("All") ) {
      //filter cmpies to only cmpy selected
      if (cmpies.contains(cmpyId)) {
        cmpies = new ArrayList<String>();
        cmpies.add(cmpyId);
      }
    }
    view.searchTerm = tripSearchForm.getInSearchTerm();
    if (cmpyId == null || cmpyId.trim().length() == 0) {
      view.cmpyId = "All";
    } else {
      view.cmpyId = cmpyId;
    }


    int startPos = tripSearchForm.getInStartPos();



    view.tableName  = tripSearchForm.getInTableName();
    view.isTour = false;
    view.isSharedTrip = true;

    view.searchStatus = tripSearchForm.getSearchStatus();

    ArrayList<Integer> statuses =  new ArrayList<Integer>();
    if (view.searchStatus == 0) {
      statuses.add(0);
    }  else if (view.searchStatus == 1) {
      statuses.add(1);
    }   else {
      view.searchStatus = 3;
      statuses.add(0);
      statuses.add(1);
    }


    if (cmpies.size() > 0 && startPos != -1) {
      List<TripInfoView> trips =  SharedTripsController.searchSharedTrips(sessionMgr.getUserId(), term, cmpies, statuses, startPos, cmpiesMap);
      if (trips != null && trips.size() > 0) {
        view.searchResults.addAll(trips);
        view.searchResultsCount = trips.size();
        view.nextPos = -1;
        view.prevPos = -1;
        if (startPos >= APPConstants.MAX_RESULT_COUNT) {
          view.prevPos = startPos - APPConstants.MAX_RESULT_COUNT + 1;
          if (view.prevPos < 0)
            view.prevPos = -1;
        }
        if (trips.size() == APPConstants.MAX_RESULT_COUNT) {
          //there are more records
          view.nextPos = startPos + APPConstants.MAX_RESULT_COUNT;
        }
      } else {
        view.message = "No shared trips found. Please try your search again.";
      }
    }


    if (startPos > 0) {
      //continuation
      return ok(views.html.trip.searchTripsMore.render(view));

    } else {
      //new search
      return ok(views.html.sharedTrips.searchSharedTrips.render(view));

    }
  }


  @With({Authenticated.class, Authorized.class})
  public Result recentSharedTrips () {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<TripSearchForm> tripForm = formFactory.form(TripSearchForm.class);
    TripSearchForm tripSearchForm = tripForm.bindFromRequest().get();

    Credentials cred = sessionMgr.getCredentials();

    int startPos = tripSearchForm.getInStartPos();

    TripsView view = buildRecentTripsView(startPos, sessionMgr, tripSearchForm.getInTableName());

    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      view.message = msg;
    }

    return ok(views.html.sharedTrips.recentSharedTripsMore.render(view));



  }

  public static HashMap getSharingCompanies(String userId) {
    HashMap<String, String> results = new HashMap<String, String>();
    Connection conn = null;
    try {
      conn = DBConnectionMgr.getConnection4Publisher();

      results = CompanyMgr.findSharingCmpies(userId, conn);

    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "getSharingCompanies - User id: " + userId, e);
    }
    finally {
      if (conn != null) {
        try {
          conn.close();
        }
        catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    return results;
  }

  public static List<TripInfoView> searchSharedTrips (String userId, String keyword, List<String> cmpies, ArrayList<Integer> statuses , int startPos, HashMap<String, Company> cmpiesMap) {
    List <TripInfoView> results = new ArrayList<TripInfoView>();
    ArrayList <Trip> t = new ArrayList<Trip>();
    HashMap<String, SecurityMgr.AccessLevel> accessLevel = new HashMap<String, SecurityMgr.AccessLevel>();

    Connection conn = null;
    try {
      conn = DBConnectionMgr.getConnection4Publisher();

      List<SharedTripRS> t2 = TripMgr.findSharedTrips(keyword,
                                              cmpies,
                                              userId,
                                              APPConstants.MAX_RESULT_COUNT,
                                              startPos,
                                              statuses,
                                              conn);
      if (t2 != null && t2.size() > 0) {
        for (SharedTripRS s : t2) {
          if (s.trip != null) {
            t.add(s.trip);
            accessLevel.put(s.trip.tripid, s.accessLevel);
          }
        }
        results = TripController.buildTripView(t, cmpiesMap, null);

        if (results != null && results.size() > 0) {
          for (TripInfoView tripInfoView : results) {
            if (accessLevel.containsKey(tripInfoView.tripId)) {
              switch  (accessLevel.get(tripInfoView.tripId)) {
                case READ:
                  tripInfoView.accessLevelVal = "Read Access";
                  break;
                case APPEND:
                  tripInfoView.accessLevelVal = "Contribute Access";
                  break;
                case APPEND_N_PUBLISH:
                  tripInfoView.accessLevelVal = "Contribute and Publish Access";
                  break;
                case OWNER:
                  tripInfoView.accessLevelVal = "Full Access";
                  break;
                default:
                  tripInfoView.accessLevelVal = "";

              }
            }
          }
        }
      }
    } catch (Exception e) {
      Log.log(LogLevel.ERROR, "searchSharedTrips - User: " + userId, e  );
    } finally {
      if (conn != null) {
        try {
          conn.close();
        }   catch (Exception e) {
          e.printStackTrace();
        }
      }
    }

    return results;

  }

  public static CollaboratorsView buildCollaboratorsView(Trip trip, SecurityMgr.AccessLevel accessLevel, String userId) {
    CollaboratorsView view = new CollaboratorsView();

    view.collaborators = TripShare.getTripCollaborators(trip);
    view.invites = TripInvite.getTripInvites(trip);
    List<TripGroup> tripGroups = TripGroup.findActiveByTripId(trip.tripid);
    view.groupsEnabled = tripGroups.size() > 0;
    view.tripId = trip.getTripid();
    view.accessLevel = accessLevel;
    view.canInvite = SecurityMgr.canInviteOthers(trip.tripid, userId, accessLevel);
    view.currUserId = userId;
    return view;
  }


  @With({Authenticated.class, Authorized.class})
  public Result changeSharedTripAccessLevel() {

    FormChangeSharedTripAccessLevel form       = (formFactory.form(FormChangeSharedTripAccessLevel.class)).bindFromRequest().get();
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    SecurityMgr.AccessLevel origAccessLevel;

    Trip tripModel = Trip.findByPK(sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID));
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND_N_PUBLISH)) {
      //Clearly someone exploits the system here as all previous search forms, etc check OWNER access level
      BaseView baseView = new BaseView();
      baseView.message = "Permission Error: You are not permitted to chance access rights for this user.";
      return ok(views.html.common.message.render(baseView));
    }

    if(!form.isValid()){
      BaseView baseView = new BaseView();
      baseView.message = "System Error: Request can not be understood.";
      return ok(views.html.common.message.render(baseView));
    }

    TripShare tripShare = TripShare.getSharedTripForUser(tripModel.getTripid(), form.inShareUserId);

    /* If not the owner and trip share not created by this user, can not change permissions*/
    if (accessLevel.lt(SecurityMgr.AccessLevel.OWNER) && !tripShare.createdby.equals(sessionMgr.getUserId())) {
      BaseView baseView = new BaseView();
      baseView.message = "Permission Error: You are not permitted to chance access rights for users added by others.";
      return ok(views.html.common.message.render(baseView));
    }


    Account a = Account.findByLegacyId(form.inShareUserId);
    if (a == null || tripShare == null) {
      return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND,
                                 "Internal Error. Please refresh this page and resubmit");
    }
    origAccessLevel = tripShare.getAccesslevel();

    tripShare.setAccesslevel(form.inAccessLevel);
    tripShare.setModifiedby(sessionMgr.getUserId());
    tripShare.setLastupdatedtimestamp(System.currentTimeMillis());
    tripShare.update();

    SecurityMgr.chanceAccessLevel(a, tripModel.tripid, form.inAccessLevel);

    //Create audit log
    audit:
    {
      TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_COLLABORATION,
                                           AuditActionType.MODIFY,
                                           AuditActorType.WEB_USER)
                              .withTrip(tripModel)
                              .withCmpyid(tripModel.cmpyid)
                              .withUserid(sessionMgr.getUserId());

      ((AuditCollaboration) ta.getDetails()).withAction(AuditCollaboration.CollaboraionAction.CHANGE_SHARE_ACCESS)
                                            .withTripShare(tripShare);
      ta.save();
    }

    try {
      MessengerUpdateHelper.build(tripModel, sessionMgr.getAccountId())
                           .setAddDefaultRooms(true)
                           .modifyUserTripAccessState(a.getLegacyId(), true);
    }catch (Exception e) {
      Log.log(LogLevel.ERROR, "TripSharedController:changeSharedTripAccessLevel error changing permissions for user: " + a.getLegacyId() + " Trip: " + tripModel.tripid, e);
    }

    flash(SessionConstants.SESSION_PARAM_MSG, "Successfully changed access permissions for " +
                                              tripShare.user.getFirstname() + " " +
                                              tripShare.user.getLastname());
    if (form.inOriginPage != null && form.inOriginPage.equalsIgnoreCase("review")) {
      return redirect(routes.TourController.reviewTour());
    }
    else {
      return redirect(routes.TripController.tripInfo(tripModel.tripid));
    }
  }


  @With({Authenticated.class, Authorized.class})
  public Result revokeTripShareFromUser() {
    /*
     * Steps to revoke user's access to the trip:
     * 1. Check that user has rights to revoke share from this specific user (TODO: What are the rules?)
     * 2. Find trip
     * 3. Retrieve trip share information
     * 4. Mark status INACTIVE
     * 5. Save tripshare
     * 6. Remove trip from user's credentials
     * 7. Redirect to trip info page
     */

    FormRevokeTripShareFromUser form       = (formFactory.form(FormRevokeTripShareFromUser.class)).bindFromRequest().get();
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());


    Trip tripModel = Trip.findByPK(sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID));
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND_N_PUBLISH)) {
      //Clearly someone exploits the system here as all previous search forms, etc check OWNER access level
      BaseView baseView = new BaseView();
      baseView.message = "Permission Error: You are not permitted to revoke access rights for this user.";
      return ok(views.html.common.message.render(baseView));
    }

    if(!form.isValid()){
      BaseView baseView = new BaseView();
      baseView.message = "System Error: Request can not be understood.";
      return ok(views.html.common.message.render(baseView));
    }

    TripShare tripShare = TripShare.getSharedTripForUser(tripModel.getTripid(), form.inShareUserId);

    /* If not the owner and trip share not created by this user, can not change permissions*/
    if (accessLevel.lt(SecurityMgr.AccessLevel.OWNER) && !tripShare.createdby.equals(sessionMgr.getUserId())) {
      BaseView baseView = new BaseView();
      baseView.message = "Permission Error: You are not permitted to revoke access for users invited by others.";
      return ok(views.html.common.message.render(baseView));
    }


    Account a = Account.findByLegacyId(form.inShareUserId);
    if (a == null || tripShare == null) {
      return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND,
                                 "Internal Error. Please refresh this page and resubmit");
    }

    tripShare.setStatus(APPConstants.STATUS_DELETED);
    tripShare.setAccesslevel(SecurityMgr.AccessLevel.NONE);
    tripShare.setModifiedby(sessionMgr.getUserId());
    tripShare.setLastupdatedtimestamp(System.currentTimeMillis());
    tripShare.update();

    SecurityMgr.removeSharedTrip(a, tripModel.tripid);

    MessengerUpdateHelper.build(tripModel, sessionMgr.getAccountId())
                         .modifyUserTripAccessState(a.getLegacyId(), false);

    //Create audit log
    audit:
    {
      TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_COLLABORATION,
                                           AuditActionType.DELETE,
                                           AuditActorType.WEB_USER)
                              .withTrip(tripModel)
                              .withCmpyid(tripModel.cmpyid)
                              .withUserid(sessionMgr.getUserId());

      ((AuditCollaboration) ta.getDetails()).withAction(AuditCollaboration.CollaboraionAction.REVOKE_SHARE)
                                            .withTripShare(tripShare);
      ta.save();;
    }

    flash(SessionConstants.SESSION_PARAM_MSG, "Successfully revoked the invite for " +
                                              tripShare.user.getFirstname() + " " +
                                              tripShare.user.getLastname());

    if (form.inOriginPage != null && form.inOriginPage.equalsIgnoreCase("review")) {
      return redirect(routes.TourController.reviewTour());
    }
    else {
      return redirect(routes.TripController.tripInfo(tripModel.tripid));
    }
  }


  @With({Authenticated.class, Authorized.class})
  public Result shareTripWithUser() {
    /*
     * Steps to add user to a trip:
     * 1. Find trip from session
     * 2. Find user from "shareUserId" form value
     * 3. Find AccessLevel from "inAccessLevel"
     * 4. Find note to the user from "inShareNote"
     * 5. Build new trip share object with those parameters
     * 6. Save to the db
     * 7. If shared users' session exists append shared trip to his shared trip list
     * 8. Send e-mail and if trip is published, send itinerary email
     * 9. Display share result
     */

    FormShareTripWithUser form       = (formFactory.form(FormShareTripWithUser.class)).bindFromRequest().get();
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Trip tripModel = Trip.findByPK(sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID));
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND_N_PUBLISH) ||
        !SecurityMgr.canInviteOthers(tripModel.tripid, sessionMgr.getUserId(), accessLevel)) {
      //Clearly someone exploits the system here as all previous search forms, etc check APPEND_N_PUBLISH access level
      return ok("<div id='inviteResult' class='alert alert-error'>No permission to add user for this trip</div>");
    }

    if(!form.isValid()){
      //try to see if the data is passed via flash session from a redirect from a new user invite
      form.inShareNote = flash(SessionConstants.SESSION_PARAM_NOTES);
      form.inShareUserId = flash(SessionConstants.SESSION_PARAM_USERID);
      String s = flash(SessionConstants.SESSION_PARAM_ACCESS_LEVEL);
      if (s != null) {
       form.inAccessLevel =  SecurityMgr.AccessLevel.valueOf(s);
      }

      flash(SessionConstants.SESSION_PARAM_NOTES,"");
      flash(SessionConstants.SESSION_PARAM_USERID,"");
      flash(SessionConstants.SESSION_PARAM_ACCESS_LEVEL, "");

      if (!form.isValid()) {
        return ok("<div id='inviteResult' class='alert alert-warning'><em>Error parsing request.</em> Refresh browser and try again.</div>");
      }

    }

    if (form.inAccessLevel.gt(accessLevel)) {
      return ok("<div id='inviteResult' class='alert alert-error'>Can not set access level more permissive than your own</div>");
    }

    UserProfile senderUser = UserProfile.findByPK(sessionMgr.getUserId());


    Account a = Account.findByLegacyId(form.inShareUserId);
    UserProfile up = UserProfile.findByPK(form.inShareUserId);
    if (a == null || up == null) {
      return ok("<div id='inviteResult' class='alert alert-warning'><em>Error parsing request.</em> Refresh browser and try again.</div>");
    }


    TripShare tripShare = TripShare.getSharedTripForUser(tripModel.tripid, a.getLegacyId());

    boolean isNewShare = false;
    //No previous trip share found - build a new one
    if (tripShare == null) {
      tripShare = TripShare.buildTripShare(sessionMgr.getUserId(), tripModel, up, form.inShareNote, form.inAccessLevel);
      tripShare.save();
      isNewShare = true;
    }
    else {
      tripShare.setModifiedby(senderUser.getUserid());
      tripShare.setLastupdatedtimestamp(System.currentTimeMillis());
      tripShare.setSenttimestamp(System.currentTimeMillis());
      tripShare.setAccesslevel(form.inAccessLevel);
      tripShare.setStatus(APPConstants.STATUS_ACTIVE);
      if (form.inShareNote.length() > 0) {
        tripShare.setNote(form.inShareNote);
      }
      tripShare.update();
    }

    SecurityMgr.addSharedTrip(a, tripModel.tripid, form.inAccessLevel);

    //Create audit log
    audit:
    {
      TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_COLLABORATION,
                                           isNewShare?AuditActionType.ADD:AuditActionType.MODIFY,
                                           AuditActorType.WEB_USER)
                              .withTrip(tripModel)
                              .withCmpyid(tripModel.cmpyid)
                              .withUserid(sessionMgr.getUserId());

      ((AuditCollaboration) ta.getDetails()).withAction(AuditCollaboration.CollaboraionAction.SHARE)
                                            .withTripShare(tripShare);
      ta.save();
    }

    sendUmappedUserTripShareEmail(tripShare, tripModel, senderUser);
    MessengerUpdateHelper.build(tripModel, sessionMgr.getAccountId())
                         .setAddDefaultRooms(true)
                         .modifyUserTripAccessState(up.getUserid(), true);


    //if the trip is already published, also send the itinerary
    try {
      //if trip has already been published, send the email notification now
      //create share  + invite

      if (TripPublishHistory.countPublished(tripModel.getTripid()) > 0) {
        ArrayList agents = new ArrayList<>();
        TripAgent tripAgent = new TripAgent();
        tripAgent.setName(a.getFullName());
        tripAgent.setStatus(APPConstants.STATUS_ACTIVE);
        tripAgent.setEmail(a.getEmail());


        agents.add(tripAgent);
        int returnCode = TripController.sendUMappedAgentEmailNotitication(agents, tripModel, senderUser, null, null);
        if (returnCode != 0) {
          flash(SessionConstants.SESSION_PARAM_MSG, "Error - cannot send notification email");
        }
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }

    return ok("<div id='inviteResult' class='alert alert-success'>User added to the trip</div>");
  }


  @With({Authenticated.class, Authorized.class})
  public Result resendTripShareEmail() {
    FormResendTripShareEmail form       = (formFactory.form(FormResendTripShareEmail.class)).bindFromRequest().get();
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Trip tripModel = Trip.findByPK(sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID));
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND_N_PUBLISH)) {
      //Clearly someone exploits the system here as all previous search forms, etc check OWNER access level
      BaseView baseView = new BaseView();
      baseView.message = "Permission Error: No permission to resend invitation to this user for this trip.";
      return ok(views.html.common.message.render(baseView));
    }

    if(!form.isValid()){
      BaseView baseView = new BaseView();
      baseView.message = "Error parsing request. Refresh browser and try again.";
      return ok(views.html.common.message.render(baseView));
    }

    UserProfile senderUser = UserProfile.findByPK(sessionMgr.getUserId());

    UserProfile userProfile = UserProfile.findByPK(form.inShareUserId);
    if (userProfile == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Error parsing request. Refresh browser and try again.";
      return ok(views.html.common.message.render(baseView));
    }

    TripShare tripShare = TripShare.getSharedTripForUser(tripModel.tripid, userProfile.getUserid());

    if (tripShare == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Error parsing request. Refresh browser and try again.";
      return ok(views.html.common.message.render(baseView));
    }

    /* If not the owner and trip share not created by this user, can not resend invite */
    if (accessLevel.lt(SecurityMgr.AccessLevel.OWNER) && !tripShare.createdby.equals(sessionMgr.getUserId())) {
      BaseView baseView = new BaseView();
      baseView.message = "Permission Error: You are not permitted to resend invites, sent by others.";
      return ok(views.html.common.message.render(baseView));
    }

    tripShare.setModifiedby(senderUser.getUserid());
    tripShare.setLastupdatedtimestamp(System.currentTimeMillis());
    tripShare.setSenttimestamp(System.currentTimeMillis());
    tripShare.setStatus(APPConstants.STATUS_ACTIVE);
    if (form.inShareNote != null && form.inShareNote.trim().length() > 0) {
      tripShare.setNote( form.inShareNote.trim());
    }
    tripShare.update();

    //Create audit log
    audit:
    {
      TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_COLLABORATION,
                                           AuditActionType.MODIFY,
                                           AuditActorType.WEB_USER)
                              .withTrip(tripModel)
                              .withCmpyid(tripModel.cmpyid)
                              .withUserid(sessionMgr.getUserId());

      ((AuditCollaboration) ta.getDetails()).withAction(AuditCollaboration.CollaboraionAction.RESEND_SHARE)
                                            .withTripShare(tripShare);
      ta.save();;
    }

    sendUmappedUserTripShareEmail(tripShare, tripModel, senderUser);

    BaseView baseView = new BaseView();
    baseView.message = "Trip invitation has been resent.";
    return ok(views.html.common.message.render(baseView));
  }


  /**
   * Send trip sharing information to the user
   *
   * @param tripShare information about shared trip
   * @param tripModel - trip details, including company to which it belongs
   * @return
   */
  public static boolean sendUmappedUserTripShareEmail(TripShare tripShare, Trip tripModel, UserProfile senderUser) {
    if (tripShare == null || tripModel == null) {
      return false;
    }

    String smtp = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_HOST);
    String user = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_USER_ID);
    String pwd = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_PASSWORD);

    String hostUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);
    String emailEnabled = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_ENABLED);

    String sender = "UMapped";

    Company cmpy = Company.find.byId(tripModel.getCmpyid());
    if (cmpy != null && cmpy.name != null && cmpy.name.trim().length() > 0) {
      sender = cmpy.name + " via Umapped";
    }

    if (emailEnabled == null || !emailEnabled.equalsIgnoreCase("Y")) {
      Log.log(LogLevel.INFO, "Email disabled");
      return false;
    }

    try {
      ctx().changeLang("en-US");
    } catch (Exception e) {
      //no context
    }
    String emailBody = views.html.trip.collaborate.emailBodyShare
        .render(tripModel, tripShare, senderUser, hostUrl).toString();
    String fromAddress = "no-reply@umapped.com";//senderUser.getEmail();
    List<String> toEmails = new ArrayList<String>();
    toEmails.add(tripShare.user.getEmail());
    String subject = "A trip was shared with you: " + tripModel.getName();


    List<Map<String, String>> body = new ArrayList<Map<String, String>>();
    Map<String, String> token = new HashMap<String, String>();
    token.put(com.mapped.common.CoreConstants.HTML, emailBody);
    body.add(token);

    Log.log(LogLevel.DEBUG,
            "Sending trip share invite e-mail to:<" + tripShare.user.getEmail() + "> tripId:" + tripModel.tripid + " " +
            "accessLevel:" + tripShare.accesslevel);

    EmailMgr emailMgr = new EmailMgr(smtp, user, pwd);
    try {
      emailMgr.init();
      emailMgr.send(fromAddress, sender, toEmails, subject, body);
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "Failure sending trip share e-mail about trip: " + tripModel.tripid, e);
      return false;
    }

    return true;
  }

  public static TripsView buildRecentTripsView (int startPos, SessionMgr sessionMgr, String tableName) {
    TripsView view = new TripsView();
    view.searchResults = new ArrayList<TripInfoView>();
    view.tableName  = tableName;
    view.isTour = false;
    view.isSharedTrip = true;

    view.searchResults = searchRecentTrips(startPos, sessionMgr);
    if (view.searchResults != null && view.searchResults.size() > 0) {
      view.searchResultsCount = view.searchResults.size();
      view.nextPos = -1;
      view.prevPos = -1;
      if (startPos >= APPConstants.MAX_RESULT_COUNT) {
        view.prevPos = startPos - APPConstants.MAX_RESULT_COUNT + 1;
        if (view.prevPos < 0)
          view.prevPos = -1;
      }
      if (view.searchResults.size() == APPConstants.MAX_RESULT_COUNT) {
        //there are more records
        view.nextPos = startPos + APPConstants.MAX_RESULT_COUNT;
      }
      view.noResults = false;
      Collections.sort(view.searchResults, TripInfoView.TripInfoViewNameComparator);
    } else {
      view.message = "No shared trips found. Please try your search again.";
      if (startPos == 0 ) {
        view.noResults = true;
      }
    }

    return view;
  }

  public static List <TripInfoView> searchRecentTrips ( int startPos, SessionMgr sessionMgr)  {
    Map<String, String> availableCmpies = SharedTripsController.getSharingCompanies(sessionMgr.getUserId());
    HashMap<String, Company> cmpiesMap = new HashMap<String, Company>();
    if (availableCmpies != null)  {
      for (String  id: availableCmpies.keySet()) {
        Company c = new Company();
        c.setName(availableCmpies.get(id));
        c.setCmpyid(id);
        cmpiesMap.put(id, c);
      }
    }

    List <TripInfoView> trips = null;


    Connection conn = null;
    ArrayList <Trip> t = new ArrayList<Trip>();
    HashMap<String, SecurityMgr.AccessLevel> accessLevel = new HashMap<String, SecurityMgr.AccessLevel>();
    try {
      conn = DBConnectionMgr.getConnection4Publisher();

      List<SharedTripRS> t2 = TripMgr.findRecentSharedTrips(sessionMgr.getUserId(),
                                                            APPConstants.MAX_RESULT_COUNT,
                                                            startPos,
                                                            conn);


      for (SharedTripRS s : t2) {
        if (s.trip != null) {
          t.add(s.trip);
          accessLevel.put(s.trip.tripid, s.accessLevel);
        }
      }


      if (t2 != null && t2.size() > 0) {
        trips = TripController.buildTripView(t, cmpiesMap, null);
      }
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "recentSharedTrips - User: " + sessionMgr.getUserId(), e);
    }
    finally {
      if (conn != null) {
        try {
          conn.close();
        }
        catch (Exception e) {
          e.printStackTrace();
        }
      }
    }


    if (trips != null && trips.size() > 0) {

      for (TripInfoView tripInfoView : trips) {
        if (accessLevel.containsKey(tripInfoView.tripId)) {
          switch (accessLevel.get(tripInfoView.tripId)) {
            case READ:
              tripInfoView.accessLevelVal = "Read Access";
              break;
            case APPEND:
              tripInfoView.accessLevelVal = "Contribute Access";
              break;
            case APPEND_N_PUBLISH:
              tripInfoView.accessLevelVal = "Contribute and Publish Access";
              break;
            case OWNER:
              tripInfoView.accessLevelVal = "Full Access";
              break;
            default:
              tripInfoView.accessLevelVal = "";

          }
        }
      }
    }

    return trips;
  }

  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result dashboard() {
    JsonNode jsonRequest = request().body().asJson();
    DataTablesForm dtRequest = Json.fromJson(jsonRequest, DataTablesForm.class);
    SessionMgr    sessionMgr = SessionMgr.fromContext(ctx());

    class TripRow {
      public String imgUrl;
      public String tripName;
      public String tripDate;
      public String status;
      public String createdBy;
      public String company;
      public String accessLevel;
      public String id;
    }

    DataTablesView<TripRow> dtv = new DataTablesView<>(dtRequest.draw);


    Connection conn = null;
    try {
      String sortCol = jsonRequest.get("columns").get(Integer.parseInt(jsonRequest.get("order").get(0).get("column").asText())).get("data").asText();
      String sortOrder = jsonRequest.get("order").get(0).get("dir").asText();

      conn = DBConnectionMgr.getConnection4Publisher();
      dtv.recordsTotal    = TripMgr.findDashboardSharedTripsCount(sessionMgr.getUserId(), conn);
      dtv.recordsFiltered = TripMgr.findDashboardSharedTripsCountFiltered(sessionMgr.getUserId(), dtRequest.search.value, conn);
      List<SharedTripRS> recs = TripMgr.findDashboardSharedTrips(sessionMgr.getUserId(),
                                                                 dtRequest.length,
                                                                 dtRequest.start,
                                                                 dtRequest.search.value,
                                                                 conn,
                                                                 sortCol,
                                                                 sortOrder);
      if (recs != null && recs.size() > 0) {
        Map<String, String> availableCmpies = SharedTripsController.getSharingCompanies(sessionMgr.getUserId());
        HashMap<String, Company> cmpiesMap = new HashMap<String, Company>();
        if (availableCmpies != null)  {
          for (String  id: availableCmpies.keySet()) {
            Company c = new Company();
            c.setName(availableCmpies.get(id));
            c.setCmpyid(id);
            cmpiesMap.put(id, c);
          }
        }

        for (SharedTripRS rec : recs) {
          TripRow row = new TripRow();
          Trip trip = rec.getTrip();
          row.id =  routes.TripController.simpleTripInfo(trip.getTripid()).url();
          row.tripName = "<a onclick=\"editTrip('"+ routes.TripController.simpleTripInfo(trip.getTripid()).url()  + "')\">" + trip.getName() + "</a>";
          row.tripDate = Utils.formatDatePrint(trip.getStarttimestamp()) + " - " + Utils.formatDatePrint(trip.getEndtimestamp());
          row.imgUrl = trip.getImageUrl();
          if (trip.getStatus() == 0) {
            row.status = "Published";
          } else {
            row.status = "Pending";
          }
          row.createdBy = trip.getCreatedby();

          SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(trip, sessionMgr);
          switch (accessLevel) {
            case READ:
              row.accessLevel = "Read Access";
              break;
            case APPEND:
              row.accessLevel = "Contribute Access";
              break;
            case APPEND_N_PUBLISH:
              row.accessLevel = "Contribute and Publish Access";
              break;
            case OWNER:
              row.accessLevel = "Full Access";
              break;
            default:
              row.accessLevel = "";

          }
          row.company = cmpiesMap.get(trip.getCmpyid()).getName();


          dtv.data.add(row);
        }
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      Log.log(LogLevel.ERROR, "recentSharedTrips - User: " + sessionMgr.getUserId(), e);
    }
    finally {
      if (conn != null) {
        try {
          conn.close();
        }
        catch (Exception e) {
          e.printStackTrace();
        }
      }
    }




    return  ok(Json.toJson(dtv));
  }
}
