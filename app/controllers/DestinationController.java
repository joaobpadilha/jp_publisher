package controllers;

import actors.ActorsHelper;
import actors.SupervisorActor;
import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.DataTablesForm;
import com.mapped.publisher.form.DestinationForm;
import com.mapped.publisher.form.DestinationGuideForm;
import com.mapped.publisher.form.DestinationSearchForm;
import com.mapped.publisher.msg.DocPdfMsg;
import com.mapped.publisher.msg.DocPdfRec;
import com.mapped.publisher.persistence.DocumentsHelper;
import com.mapped.publisher.persistence.PageAttachType;
import com.mapped.publisher.persistence.TripMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.S3Util;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.external.afar.*;
import models.publisher.*;
import models.publisher.AfarCity;
import models.publisher.Destination;
import org.apache.commons.lang3.time.StopWatch;
import org.owasp.html.Sanitizers;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.sql.Connection;
import java.time.Instant;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static models.publisher.DestinationGuide.buildGuide;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-01
 * Time: 12:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class DestinationController
    extends Controller {
  private final static boolean DEBUG_PDF_TEMPLATE = false;

  @Inject
  FormFactory formFactory;

  @With({Authenticated.class, Authorized.class})
  public Result newDestination(String inDestId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    DestinationView view = new DestinationView();

    HashMap<String, String> agencyList = new HashMap<>();
    Credentials cred = sessionMgr.getCredentials();
    view.addCapabilities(cred.getCapabilities());
    if(cred.isCmpyPowerMember() || cred.isCmpyAdmin()) {
      agencyList.put(cred.getCmpyId(), cred.getCmpyName());
    }

    view.tripAgencyList = agencyList;

    List<DestinationType> destinationTypes = DestinationType.findActive();
    HashMap<Integer, String> destinationTypeDesc = new HashMap<>();
    if (destinationTypes != null) {
      ArrayList<GenericTypeView> destinationTypeList = new ArrayList<>();
      for (DestinationType u : destinationTypes) {
        if (u.destinationtypeid != APPConstants.TOUR_DOC_DESTINATION_TYPE &&
            u.destinationtypeid != APPConstants.TRIP_DOC_DESTINATION_TYPE) {
          GenericTypeView typeView = new GenericTypeView();
          typeView.id = String.valueOf(u.getDestinationtypeid());
          typeView.name = u.getName();
          destinationTypeList.add(typeView);
          destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());
        }
      }
      view.destinationTypeList = destinationTypeList;
    }

    Form<DestinationForm> destForm = formFactory.form(DestinationForm.class);
    DestinationForm destInfo = destForm.bindFromRequest().get();
    String destId = null;

    if (inDestId != null) {
      destId = inDestId;
    }
    else if (destInfo == null || destInfo.getInDestId() == null || destInfo.getInDestId().length() == 0) {
      //look for id in flash session
      destId = flash(SessionConstants.SESSION_DEST_ID);
    }
    else {
      destId = destInfo.getInDestId();
    }


    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    view.message = msg;
    view.displayMap = false;

    if (destId != null && destId.length() > 0) {
      //retrieve dest info
      try {
        Destination dest = Destination.find.byId(destId);
        if (dest != null && dest.getStatus() == APPConstants.STATUS_DELETED) {
          BaseView baseView = new BaseView();
          baseView.message = "Cannot access this document.";
          return ok(views.html.common.message.render(baseView));
        }


        if (dest != null && dest.getStatus() != APPConstants.STATUS_DELETED) {
          if (!SecurityMgr.canEditDestination(dest, sessionMgr)) {
            BaseView baseView = new BaseView();
            baseView.message = "Cannot access this guide - please resubmit";
            return ok(views.html.common.message.render(baseView));
          }
          view.id = dest.destinationid;
          view.name = dest.name;
          view.destinationType = String.valueOf(dest.destinationtypeid);
          view.destinationTypeDesc = destinationTypeDesc.get(dest.destinationtypeid);
          view.cmpyId = dest.cmpyid;
          view.cmpyName = view.tripAgencyList.get(dest.cmpyid);
          view.city = dest.city;
          view.country = dest.country;
          view.intro = dest.intro;
          view.locLat = String.valueOf(dest.loclat);
          view.locLong = String.valueOf(dest.loclong);
          view.tag = dest.tag;
          view.createdById = dest.getCreatedby();

          view.isPrivate = false;
          if (dest.accessLevel == Destination.AccessLevel.PRIVATE) {
            view.isPrivate = true;
          }

          if (SecurityMgr.isCmpyAdmin(view.cmpyId, sessionMgr) || view.createdById.equals(cred.getUserId())) {
            view.createdByThisUser = true;
          }
          else {
            view.createdByThisUser = false;
          }


          if (cred.getCmpyName() != null && cred.getCmpyName().toLowerCase().contains("indagare")) {
            view.addMobilizedDomain("www.indagare.com", "Indagare");
          } else {
            for (SysContentSite s : MobilizerController.getContentSites()) {
              view.addMobilizedDomain(s.getDomain(), s.getName());
            }
          }

          //get covers
          if (dest.hasCover()) {
            view.coverUrl = dest.getCoverurl();
            view.cover = dest.getCoverView();
            if (view.cover.url.contains("?")) {
              view.cover.url += "&v="+ Instant.now().toEpochMilli();
            } else {
              view.cover.url += "?v="+ Instant.now().toEpochMilli();
            }
          }

          //check for pages
          List<DestinationGuide> guides = DestinationGuide.findActiveDestGuides(dest.destinationid);
          if (guides != null) {
            List<DestinationGuideView> guideViews = new ArrayList<DestinationGuideView>();
            for (DestinationGuide destGuide : guides) {
              DestinationGuideView guideView = new DestinationGuideView();
              guideView.destinationId = destGuide.destinationid;
              guideView.id = destGuide.destinationguideid;
              guideView.city = destGuide.city;
              guideView.country = destGuide.country;
              guideView.description = destGuide.description;
              guideView.intro = destGuide.intro;
              guideView.landmark = destGuide.landmark;
              guideView.name = destGuide.name;
              guideView.locLat = String.valueOf(destGuide.loclat);
              guideView.locLong = String.valueOf(destGuide.loclong);
              guideView.tag = destGuide.tag;
              if (destGuide.loclat != 0 && destGuide.loclong != 0) {
                view.displayMap = true;
              }
              guideView.rank = destGuide.rank;
              guideView.streetAddr1 = destGuide.streetaddr1;
              //get cover if any
              //get any attachments
              List<DestinationGuideAttach> covers = DestinationGuideAttach.getCover(destGuide.destinationguideid);
              if (covers != null && covers.size() == 1) {

                guideView.coverName = covers.get(0).getName();
                guideView.coverUrl = covers.get(0).getAttachurl();
              }
              guideViews.add(guideView);
            }
            view.guides = guideViews;
          }

          //get list of Travel 42 reports if available
          Map<String, String> travel42Reports =(Map<String, String>)CacheMgr.get(APPConstants.CACHE_T42_REPORT_USER +
                                                                                  sessionMgr.getUserId());
          if (travel42Reports != null && travel42Reports.size() > 0) {
            //find the custom trip view and set the report list
            view.travel42Reports = travel42Reports;
          }
        }
      }
      catch (Exception e) {
        BaseView baseView = new BaseView();
        baseView.message = "System Error - please resubmit";
        return ok(views.html.common.message.render(baseView));
      }
    }
    return ok(views.html.guide.newDestination.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result destination() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    DestinationSearchView view = new DestinationSearchView();
    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    view.message = msg;
    view.displayMap = false;
    HashMap<String, String> agencyList = new HashMap<>();
    Credentials cred = sessionMgr.getCredentials();

    if(cred.hasCompany()) {
      agencyList.put(cred.getCmpyId(), cred.getCmpyName());
      view.cmpyId = cred.getCmpyId();
    }

    view.tripAgencyList = agencyList;
    view.isAdminOfACompany = cred.isCmpyAdmin();

    HashMap<Integer, String> destinationTypeDesc = new HashMap<Integer, String>();
    List<DestinationType> destinationTypes = DestinationType.findActive();
    if (destinationTypes != null) {
      ArrayList<GenericTypeView> destinationTypeList = new ArrayList<GenericTypeView>();
      for (DestinationType u : destinationTypes) {
        GenericTypeView typeView = new GenericTypeView();
        typeView.id = String.valueOf(u.getDestinationtypeid());
        typeView.name = u.getName();
        destinationTypeList.add(typeView);
        destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());

      }
      view.destinationTypeList = destinationTypeList;
    }

    //get search keyword
    //bind html form to form bean
    Form<DestinationSearchForm> destSearchForm = formFactory.form(DestinationSearchForm.class);
    DestinationSearchForm destSearchInfo = destSearchForm.bindFromRequest().get();
    List<Destination> destList = null;
    List<DocumentsHelper.UmPage> pages = null;
    int startPos = destSearchInfo.getInStartPos();
    if (startPos < 0) {
      startPos = 0;
    }

    List<Integer> types = new ArrayList<Integer>();
    types.add(APPConstants.CITY_DESTINATION_TYPE);
    types.add(APPConstants.COUNTRY_DESTINATION_TYPE);
    types.add(APPConstants.PLACE_DESTINATION_TYPE);
    types.add(APPConstants.TOUR_DESTINATION_TYPE);
    types.add(APPConstants.ACTIVITY_DESTINATION_TYPE);
    types.add(APPConstants.GENERAL_DESTINATION_TYPE);
    types.add(APPConstants.CMPY_INFO_DESTINATION_TYPE);


    if (destSearchInfo == null || destSearchInfo.getInKeyword() == null ) {
      //get from sessions
      String s = sessionMgr.getParam(SessionConstants.SESSION_PARAM_KEYWORD);
      if (s != null && s.trim().length() > 0) {
        destSearchInfo.setInKeyword(s);
      }
      s = sessionMgr.getParam(SessionConstants.SESSION_PARAM_SEARCH_TITLE_ONLY);
      if (s != null && s.trim().length() > 0) {
        destSearchInfo.setInSearchTitleOnly(Boolean.parseBoolean(s));
      } else
        destSearchInfo.setInSearchTitleOnly(true);
    }
    else {
      //save the keyword in session.
      sessionMgr.setParam(SessionConstants.SESSION_PARAM_KEYWORD, destSearchInfo.getInKeyword());
      if(destSearchInfo.getInKeyword().length() == 0) {
        sessionMgr.setParam(SessionConstants.SESSION_PARAM_SEARCH_TITLE_ONLY, "true");
        destSearchInfo.setInSearchTitleOnly(true);

      } else
        sessionMgr.setParam(SessionConstants.SESSION_PARAM_SEARCH_TITLE_ONLY, Boolean.toString(destSearchInfo.isInSearchTitleOnly()));

    }


    if (destSearchInfo == null ||
        destSearchInfo.getInKeyword() == null ||
        destSearchInfo.getInKeyword().length() == 0) {
      try {
        destList = Destination.findMostRecentActiveForCmpy(types, view.cmpyId, startPos, APPConstants.MAX_RESULT_COUNT, "lastupdatedtimestamp desc,destinationTypeId,name asc");
        pages = DocumentsHelper.findRecentActivePagesForCmpy(types, view.cmpyId, startPos, APPConstants.MAX_RESULT_COUNT, sessionMgr);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    else {

      destList = Destination.findActiveDestByKeywordForCmpy(view.cmpyId,
                                                            types,
                                                            destSearchInfo.getInKeyword(),
                                                            startPos,
                                                            APPConstants.MAX_RESULT_COUNT,
                                                            destSearchInfo.isInSearchTitleOnly(),
                                                    "lastupdatedtimestamp desc,destinationTypeId,name asc");

      if (destSearchInfo.isInSearchTitleOnly()) {
        pages =  DocumentsHelper.findPagesByName(destSearchInfo.getInKeyword(), view.cmpyId, types, sessionMgr);
      } else {
        pages = DocumentsHelper.findPagesByTerm(destSearchInfo.getInKeyword(), view.cmpyId, types, sessionMgr);
      }

      Collections.sort(destList, Destination.DestinationComparator);

      view.keyword = destSearchInfo.getInKeyword();
      view.cmpyId = destSearchInfo.getInCmpyId();
      List<String> searchDestTypes = new ArrayList<String>();
      for (Integer i : types) {
        searchDestTypes.add(String.valueOf(i));
      }
      view.destinationTypes = searchDestTypes;
    }

    List<String> addedIds = new ArrayList<String>();
    if (destList != null && destList.size() > 0) {
      List<DestinationView> destViews = new ArrayList<DestinationView>();
      for (Destination dest : destList) {
        if (SecurityMgr.canAccessDestination(dest, sessionMgr) && !addedIds.contains(dest.destinationid)) {
          addedIds.add(dest.destinationid);
          DestinationView destView = new DestinationView();
          destView.id = dest.destinationid;
          destView.name = dest.name;
          destView.destinationType = String.valueOf(dest.destinationtypeid);
          destView.destinationTypeDesc = destinationTypeDesc.get(dest.destinationtypeid);

          destView.cmpyId = dest.cmpyid;
          destView.cmpyName = view.tripAgencyList.get(dest.cmpyid);
          destView.city = dest.city;
          destView.country = dest.country;
          destView.createdById = dest.getCreatedby();

          destView.isPrivate = false;
          if (dest.accessLevel == Destination.AccessLevel.PRIVATE) {
            destView.isPrivate = true;
          }


          if (dest.intro != null && dest.intro.length() > 0) {
            String escapedIntro = Sanitizers.FORMATTING.and(Sanitizers.LINKS).sanitize(dest.intro);
            if (escapedIntro.length() > 200) {
              String s = escapedIntro.substring(0, 199);
              try {
                int lastspace = s.lastIndexOf(' ');
                if (lastspace > 0) {
                  s = s.substring(0, lastspace);
                  s = s + "...";
                  destView.intro = s;
                }
                else {
                  destView.intro = escapedIntro;
                }
              }
              catch (Exception e) {
                destView.intro = escapedIntro;
              }
            }
            else {
              destView.intro = escapedIntro;
            }
          } else if (dest.description != null && dest.description.length() > 0) {
            String escapedDesc = Sanitizers.FORMATTING.and(Sanitizers.LINKS).sanitize(dest.description);

            if (escapedDesc.length() > 200) {
              String s = escapedDesc.substring(0, 199);
              try {
                int lastspace = s.lastIndexOf(' ');
                if (lastspace > 0) {
                  s = s.substring(0, lastspace);
                  s = s + "...";
                  destView.intro = s;
                }
                else {
                  destView.intro = escapedDesc;
                }
              }
              catch (Exception e) {
                destView.intro = escapedDesc;
              }
            }
            else {
              destView.intro = escapedDesc;
            }
          }
          destView.locLat = String.valueOf(dest.loclat);
          destView.locLong = String.valueOf(dest.loclong);

          //get covers
          if (dest.hasCover()) {
            destView.coverUrl = dest.getCoverurl();
            destView.cover = dest.getCoverView();
            if (destView.cover.url.contains("?")) {
              destView.cover.url += "&v="+ Instant.now().toEpochMilli();
            } else {
              destView.cover.url += "?v="+ Instant.now().toEpochMilli();
            }
          }

          if (dest.loclat != 0 && dest.loclong != 0) {
            view.displayMap = true;
          }

          if (SecurityMgr.canEditDestination(dest, sessionMgr)) {
            destView.canEdit = true;
          }

          if (SecurityMgr.isCmpyAdmin(destView.cmpyId, sessionMgr)) {
            destView.createdByThisUser = true;
            destViews.add(destView);
          }
          else {
            if (!destView.isPrivate) {
              destView.createdByThisUser = false;
              if (destView.createdById.equals(cred.getUserId())) {
                destView.createdByThisUser = true;
              }
              destViews.add(destView);
            }
            else if (destView.isPrivate && destView.createdById.equals(cred.getUserId())) {
              destView.createdByThisUser = true;
              destViews.add(destView);
            }
          }

        }
      }
      view.destinationList = destViews;
    }

    //Pages
    addedIds = new ArrayList<>();
    if (pages != null && pages.size() > 0) {
      List<DestinationGuideView> destViews = new ArrayList<>();
      for (DocumentsHelper.UmPage page : pages) {
        if (SecurityMgr.canAccessDestination(page.destination, sessionMgr) &&
            !addedIds.contains(page.page.destinationguideid)) {
          addedIds.add(page.page.destinationid);

          DestinationGuideView destView = new DestinationGuideView();
          destView.id = page.page.destinationguideid;
          destView.destinationId = page.page.destinationid;

          destView.name = page.page.name;
          destView.destinationName = page.destination.name;


          destView.city = page.page.city;
          destView.country = page.page.country;
          if (page.page.intro != null && page.page.intro.length() > 0) {
            String escapedGuideIntro = Sanitizers.FORMATTING.and(Sanitizers.LINKS).sanitize(page.page.intro);
            if (escapedGuideIntro.length() > 200) {
              String s = escapedGuideIntro.substring(0, 199);
              try {
                int lastspace = s.lastIndexOf(' ');
                if (lastspace > 0) {
                  s = s.substring(0, lastspace);
                  s = s + "...";
                  destView.intro = s;
                }
                else {
                  destView.intro = escapedGuideIntro;
                }
              }
              catch (Exception e) {
                destView.intro = escapedGuideIntro;
              }

            }
            else {
              destView.intro = escapedGuideIntro;
            }
          } else if (page.page.description != null && page.page.description.length() > 0) {
            String escapedGuideDesc = Sanitizers.FORMATTING.and(Sanitizers.LINKS).sanitize(page.page.description);

            if (escapedGuideDesc.length() > 200) {
              String s = escapedGuideDesc.substring(0, 199);
              try {
                int lastspace = s.lastIndexOf(' ');
                if (lastspace > 0) {
                  s = s.substring(0, lastspace);
                  s = s + "...";
                  destView.intro = s;
                }
                else {
                  destView.intro = escapedGuideDesc;
                }
              }
              catch (Exception e) {
                destView.intro = escapedGuideDesc;
              }

            }
            else {
              destView.intro = escapedGuideDesc;
            }
          }
          destView.locLat = String.valueOf(page.page.loclat);
          destView.locLong = String.valueOf(page.page.loclong);

          //get covers
          if (page.destination.hasCover()) {
            destView.coverUrl = page.destination.getCoverurl();
          }


          if (SecurityMgr.canEditDestination(page.destination, sessionMgr)) {
            destView.canEdit = true;
          }

          destViews.add(destView);
        }
      }
      view.destinationGuideList = destViews;
    }

    view.nextPos = 0;
    view.prevPos = -1;
    if (view.destinationList != null) {
      if (view.destinationList.size() == APPConstants.MAX_RESULT_COUNT) {
        //there are more records
        view.nextPos = startPos + APPConstants.MAX_RESULT_COUNT - 1;
      }
    }
    if (startPos >= (APPConstants.MAX_RESULT_COUNT - 1)) {
      view.prevPos = startPos - APPConstants.MAX_RESULT_COUNT + 1;
      if (view.prevPos < 0) {
        view.prevPos = -1;
      }
    }
    view.searchTitleOnly = destSearchInfo.isInSearchTitleOnly();

    return ok(views.html.guide.destination.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result newDestinationGuide(String inDestId, String inDestGuideId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    DestinationGuideView view = new DestinationGuideView();

    //bind html form to form bean
    Form<DestinationGuideForm> destGuideForm = formFactory.form(DestinationGuideForm.class);
    DestinationGuideForm destGuideInfo = destGuideForm.bindFromRequest().get();
    String destGuideId = null;

    if (inDestId != null) {
      view.destinationId = inDestId;
    }

    if (inDestGuideId != null) {
      destGuideId = inDestGuideId;
    }
    else {
      destGuideId = destGuideInfo.getInDestGuideId();
    }

    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    view.message = msg;

    if (destGuideId != null && destGuideId.length() > 0) {
      Destination dest = Destination.find.byId(inDestId);
      DestinationGuide destGuide = DestinationGuide.find.byId(destGuideId);
      if (dest == null || destGuide == null) {
        return UserMessage.message(ReturnCode.HTTP_REQ_WRONG_DATA);
      }
      if(!SecurityMgr.canAccessDestination(dest, sessionMgr)) {
        return UserMessage.message(ReturnCode.AUTH_DOCUMENT_FAIL);
      }

      if (dest.destinationtypeid == APPConstants.TRIP_DOC_DESTINATION_TYPE ||
          dest.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE) {
        //this belongs to a custom trip document
        String tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);
        Trip trip = Trip.find.byId(tripId);
        view.accessLevel = SecurityMgr.getTripAccessLevel(trip, sessionMgr);
      }

      view.loggedInUserId = sessionMgr.getUserId();
      view.destinationName = dest.name;
      view.destinationId = destGuide.destinationid;
      view.id = destGuide.destinationguideid;
      view.city = destGuide.city;
      view.country = destGuide.country;
      view.description = destGuide.description;
      view.state = destGuide.state;
      view.intro = destGuide.intro;
      view.landmark = destGuide.landmark;
      view.name = destGuide.name;
      view.locLat = String.valueOf(destGuide.loclat);
      view.locLong = String.valueOf(destGuide.loclong);
      view.rank = destGuide.rank;
      view.streetAddr1 = destGuide.streetaddr1;
      view.cmpyId = dest.cmpyid;
      view.createdById = destGuide.createdby;
      view.tag = destGuide.tag;
      view.zipCode =destGuide.zipcode;
      view.isMobilized = false;
      view.recommendationType = destGuide.getRecommendationtype();
      if (destGuide.getDestguidetimestamp() != null && destGuide.getDestguidetimestamp() > 0) {
        view.date = Utils.getDateString(destGuide.getDestguidetimestamp());
        view.time = Utils.getTimeString(destGuide.getDestguidetimestamp());
      }

      //If the destination guide page is linking to a library of pages - get that page and replace description
      if (destGuide.getPageId() != null) {
        view.isMobilized = true;
        LibraryPage lp = LibraryPage.find.byId(destGuide.getPageId());
        view.description = lp.getContent();
      }

      //get any attachments
      List<DestinationGuideAttach> attachments = DestinationGuideAttach.findActiveByDestGuide(destGuide
                                                                                                  .destinationguideid);
      if (attachments != null) {
        List<AttachmentView> photoAttachments = new ArrayList<>();
        List<AttachmentView> linkAttachments = new ArrayList<>();
        List<AttachmentView> videoAttachments = new ArrayList<>();
        List<AttachmentView> fileAttachments = new ArrayList<>();
        for (DestinationGuideAttach attach : attachments) {
          AttachmentView attachView = new AttachmentView();
          attachView.id = attach.fileid;
          attachView.name = attach.name;
          attachView.comments = attach.getComments();
          attachView.attachUrl = attach.getAttachurl();
          attachView.createdById = attach.getCreatedby();
          if (attachView.attachUrl != null && !attachView.attachUrl.toLowerCase().startsWith("http")) {
            attachView.attachUrl = "http://" + attachView.attachUrl;
          }
          attachView.attachName = attach.getAttachname();

          if (attach.getAttachtype() == PageAttachType.VIDEO_LINK) {
            videoAttachments.add(attachView);
            if (attach.getAttachurl().toLowerCase().contains("youtube.com") ||
                attach.getAttachurl().toLowerCase().contains("youtu.be")) {
              //try to get the static image
              String pattern = "(?<=(?:v|i)=)[a-zA-Z0-9-]+(?=&)|(?<=(?:v|i)\\/)[^&\\n]+|(?<=embed\\/)[^\"&\\n]+|" +
                               "(?<=‌​(?:v|i)=)[^&\\n]+|(?<=youtu.be\\/)[^&\\n]+";

              Pattern compiledPattern = Pattern.compile(pattern);
              Matcher matcher = compiledPattern.matcher(attach.getAttachurl());

              if (matcher.find()) {
                String videoId = matcher.group();
                attachView.youTubeCover = "http://img.youtube.com/vi/" + videoId + "/0.jpg";
              }
            }
          }
          else if (attach.getAttachtype() == PageAttachType.WEB_LINK) {
            linkAttachments.add(attachView);
          }
          else if (attach.getAttachtype() == PageAttachType.FILE_LINK) {
            fileAttachments.add(attachView);
          }
          else {
            photoAttachments.add(attachView);
          }
        }
        if (photoAttachments.size() > 0) {
          view.photos = photoAttachments;
        }
        if (videoAttachments.size() > 0) {
          view.videos = videoAttachments;
        }
        if (linkAttachments.size() > 0) {
          view.links = linkAttachments;
        }
        if (fileAttachments.size() > 0) {
          view.files = fileAttachments;
        }
      }

      if (dest.destinationtypeid == APPConstants.TRIP_DOC_DESTINATION_TYPE ||
          dest.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE) {
        if (SecurityMgr.isCmpyAdmin(dest.cmpyid, sessionMgr)) {
          view.isCmpyAdmin = true;
        }
        else {
          view.isCmpyAdmin = false;
        }

        //get the associated trip
        List<TripDestination> trips = TripDestination.findActiveByDest(dest.destinationid);
        if (trips != null && trips.size() > 0) {
          TripDestination tripDest = trips.get(0);
          Trip tripModel = Trip.findByPK(tripDest.tripid);
          //set the access level for shared trips
          SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
          view.accessLevel = accessLevel;

          view.tripId = tripDest.tripid;
          view.tripStartDate = Utils.formatDateControl(tripModel.getStarttimestamp());
          return ok(views.html.trip.guidePage.render(view));
        }
      }
    }
    return ok(views.html.guide.newDestinationGuide.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result destinationList() {
    final DynamicForm form = formFactory.form().bindFromRequest();
    String term = form.get("term");

    String keyword = term;

    ArrayList<String> keywords = new ArrayList<String>();
    String[] results = term.split(",");


    if (results != null) {
      int count = 0;
      for (String s : results) {
        if (count == 0) {
          keyword = s;
        }
        else {
          if (s != null && s.length() > 1) {
            keywords.add(s);
          }
        }
        count++;
      }
    }

    ObjectNode result = Json.newObject();
    ArrayNode array = result.arrayNode();

    if (keyword != null && keyword.length() > 0) {
      String type = form.get("type");
      int destType = APPConstants.ACTIVITY_DESTINATION_TYPE;
      if (type != null && type.equals(String.valueOf(APPConstants.TOUR_DESTINATION_TYPE))) {
        destType = APPConstants.TOUR_DESTINATION_TYPE;
      }

      String cmpyId = form.get("cmpyId");
      String city = form.get("city");
      String country = form.get("country");

      ArrayList<String> cities = new ArrayList<String>();
      if (city != null && city.length() > 0) {
        results = city.split(",");
        if (results != null) {
          for (String s : results) {
            cities.add(s);
          }
        }

      }
      ArrayList<String> countries = new ArrayList<String>();
      if (country != null && country.length() > 0) {
        results = country.split(",");
        if (results != null) {
          for (String s : results) {
            countries.add(s);
          }
        }
      }

      List<Destination> destinations = Destination.findActiveDestByTypeCityCmpy(cmpyId,
                                                                                destType,
                                                                                cities,
                                                                                countries,
                                                                                keyword);

      if (destinations != null) {
        for (Destination d : destinations) {
          ObjectNode classNode = Json.newObject();
          classNode.put("label", d.name);
          classNode.put("id", d.destinationid);
          array.add(classNode);

        }
      }
    }

    return ok(array);

  }

  @With({Authenticated.class, Authorized.class})
  public Result destinationListAutocomplete() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Credentials cred = sessionMgr.getCredentials();

    final DynamicForm form = formFactory.form().bindFromRequest();
    String term = form.get("term");

    String keyword = term;

    ArrayList<String> keywords = new ArrayList<String>();
    String[] results = term.split(",");


    if (results != null) {
      int count = 0;
      for (String s : results) {
        if (count == 0) {
          keyword = s;
        }
        else {
          if (s != null && s.length() > 1) {
            keywords.add(s);
          }
        }
        count++;
      }
    }

    ObjectNode result = Json.newObject();
    ArrayNode array = result.arrayNode();

    if (keyword != null && keyword.length() > 0) {
      String cmpyId = form.get("cmpyId");

      ArrayList<String> cmpies = new ArrayList<>();
      if (SecurityMgr.canAccessCmpy(cmpyId, sessionMgr)) {
        cmpies.add(cmpyId);
      } else {
        //collaborator, add the other cmpies in
        cmpies.add(sessionMgr.getCredentials().getCmpyId());
      }

      List<Destination> destinations = Destination.findActiveDestByTypeCmpies(cmpies, keyword);
      if (destinations != null) {
        for (Destination d : destinations) {
          if (SecurityMgr.isCmpyAdmin(d.cmpyid, sessionMgr)
              || d.accessLevel == Destination.AccessLevel.PUBLIC
              || (d.accessLevel == Destination.AccessLevel.PRIVATE && d.createdby.equals(cred.getUserId()))) {
            ObjectNode classNode = Json.newObject();
            classNode.put("label", d.name);
            classNode.put("id", d.destinationid);
            array.add(classNode);
          }
        }
      }
    }

    return ok(array);

  }

  @With({Authenticated.class, Authorized.class})
  public Result destinationSearch() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    DestinationSearchView view = new DestinationSearchView();

    //bind html form to form bean
    Form<DestinationSearchForm> destSearchForm = formFactory.form(DestinationSearchForm.class);
    DestinationSearchForm destSearchInfo = destSearchForm.bindFromRequest().get();

    if (destSearchInfo == null ||
        destSearchInfo.getInKeyword() == null ||
        destSearchInfo.getInKeyword().length() == 0) {
      //return error msg - search term is mandatory
      view.message = "Search term is required.";
      return ok(views.html.guide.searchDestination.render(view));
    }
    else {
      Trip trip = Trip.find.byId(destSearchInfo.getInTripId());
      if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).lt(SecurityMgr.AccessLevel.APPEND)) {
        //return error msg - search term is mandatory
        view.message = "Permission Error - you are not authorised to access document search for this trip";
        return ok(views.html.guide.searchDestination.render(view));
      }
      view.tripId = trip.tripid;
      int startPos = destSearchInfo.getInStartPos();
      if (startPos < 0) {
        startPos = 0;
      }
      List<Integer> types = new ArrayList<Integer>();
      types.add(APPConstants.CITY_DESTINATION_TYPE);
      types.add(APPConstants.COUNTRY_DESTINATION_TYPE);
      types.add(APPConstants.PLACE_DESTINATION_TYPE);
      types.add(APPConstants.TOUR_DESTINATION_TYPE);
      types.add(APPConstants.ACTIVITY_DESTINATION_TYPE);
      types.add(APPConstants.CMPY_INFO_DESTINATION_TYPE);
      types.add(APPConstants.GENERAL_DESTINATION_TYPE);



      List<DestinationType> destinationTypes = DestinationType.findActive();
      HashMap<Integer, String> destinationTypeDesc = new HashMap<Integer, String>();
      if (destinationTypes != null) {
        for (DestinationType u : destinationTypes) {
          destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());
        }
      }

      List<String> cmpies = new ArrayList<String>();

      /* Searching for destionations (documents, guides) from all the companies user is authorised */
      cmpies.add(sessionMgr.getCredentials().getCmpyId());

      List<Destination> destList = Destination.findActiveDestByKeyword(cmpies,
                                                                       types,
                                                                       destSearchInfo.getInKeyword(),
                                                                       startPos,
                                                                       APPConstants.MAX_RESULT_COUNT,
                                                                       true,
                                                              "name asc");
      buildDestinations(view, destList, destSearchInfo, destinationTypeDesc);
      return ok(views.html.guide.searchDestination.render(view));
    }
  }

  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result destinationSearchDT() {
    JsonNode jsonRequest = request().body().asJson();
    DataTablesForm dtRequest = Json.fromJson(jsonRequest, DataTablesForm.class);
    SessionMgr    sessionMgr = SessionMgr.fromContext(ctx());

    class DocRow {
      public String imgUrl;
      public String docName;
      public String type;
      public String about;
      public String createdBy;
      public  boolean canEdit;
      //public String company;
      public String id;
    }

    DataTablesView<DocRow> dtv = new DataTablesView<>(dtRequest.draw);

    dtv.searchTerm = "";
    if (jsonRequest.get("keyword").asText() != null && jsonRequest.get("keyword").asText().length() > 0) {
      dtv.searchTerm = "%" + jsonRequest.get("keyword").asText().replace(" ", "%") + "%";
    }
    else {
      dtv.searchTerm = "%";
    }

    if (jsonRequest.get("titleOnly") != null) {
      dtv.inSearchTitleOnly = Boolean.valueOf(jsonRequest.get("titleOnly").asText());
    } else {
      dtv.inSearchTitleOnly = false;
    }

    dtv.destType = 0;
    if (jsonRequest.get("destType") != null) {
      dtv.destType = Integer.parseInt(jsonRequest.get("destType").asText());
    }

    String orderBy = "name asc";
    if(dtRequest.order  != null && dtRequest.order.size() >= 1) {
      int colId = dtRequest.order.get(0).column;
      if ( dtRequest.columns != null && dtRequest.columns.get(colId) != null && !dtRequest.columns.get(colId).name.isEmpty()) {
        orderBy = dtRequest.columns.get(colId).name + " " + dtRequest.order.get(0).dir;
      }
    }


    Credentials cred = sessionMgr.getCredentials();

    String tripId = jsonRequest.get("tripId").asText();

    HashMap<Integer, String> destinationTypeDesc = new HashMap<Integer, String>();
    List<DestinationType> destinationTypes = DestinationType.findActive();
    if (destinationTypes != null) {
      ArrayList<GenericTypeView> destinationTypeList = new ArrayList<GenericTypeView>();
      for (DestinationType u : destinationTypes) {
        GenericTypeView typeView = new GenericTypeView();
        typeView.id = String.valueOf(u.getDestinationtypeid());
        typeView.name = u.getName();
        destinationTypeList.add(typeView);
        destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());

      }
    }

    Trip trip = Trip.find.byId(tripId);


    List<Integer> types = new ArrayList<Integer>();
    if (dtv.destType == 0) {
      types.add(APPConstants.CITY_DESTINATION_TYPE);
      types.add(APPConstants.COUNTRY_DESTINATION_TYPE);
      types.add(APPConstants.PLACE_DESTINATION_TYPE);
      types.add(APPConstants.TOUR_DESTINATION_TYPE);
      types.add(APPConstants.ACTIVITY_DESTINATION_TYPE);
      types.add(APPConstants.GENERAL_DESTINATION_TYPE);
      types.add(APPConstants.CMPY_INFO_DESTINATION_TYPE);
    } else {
      types.add(dtv.destType);
    }

    if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND)) {

      List<String> cmpies = new ArrayList<String>();

      /* Searching for destionations (documents, guides) from all the companies user is authorised */
      cmpies.add(sessionMgr.getCredentials().getCmpyId());

      List<Destination> destList = Destination.findActiveDestByKeyword(cmpies,
                                                                        types,
                                                                        dtv.searchTerm,
                                                                        dtRequest.start,
                                                                        dtRequest.length,
                                                                        dtv.inSearchTitleOnly,
                                                                        orderBy);

      dtv.recordsFiltered = Destination.countActiveDestByKeyword(cmpies,
          types,
          dtv.searchTerm,
          dtv.inSearchTitleOnly);

      if (destList != null && destList.size() > 0) {
        for (Destination rec : destList) {
          DocRow row = new DocRow();
          row.id = "/trips/newTrip/addGuide?inTripId="+tripId+"&inDestId="+rec.destinationid;
          row.canEdit = true;
          row.docName = rec.getName();
          StringBuilder sb = new StringBuilder();
          sb.append(destinationTypeDesc.get(rec.destinationtypeid));
          if ((rec.getCity() != null && rec.getCity().length() > 0) || (rec.getCountry() != null && rec.getCountry().length() > 0)) {
            sb.append("<br/>");
            if ((rec.getCity() != null && rec.getCity().length() > 0) && (rec.getCountry() != null && rec.getCountry().length() > 0)) {
              sb.append(rec.getCity());
              sb.append(", ");
              sb.append(rec.getCountry());
            } else {
              sb.append(rec.getCountry());
            }
          }
          row.type = sb.toString();
          if (rec.hasCover()) {
            row.imgUrl = rec.getCoverurl();
          }
          if (row.imgUrl == null && rec.getImageUrl() != null) {
            row.imgUrl = rec.getImageUrl();
          }
          if (rec.intro != null && rec.intro.length() > 0) {
            String escapedIntro = Sanitizers.FORMATTING.and(Sanitizers.LINKS).sanitize(rec.intro);
            if (escapedIntro.length() > 200) {
              String s = escapedIntro.substring(0, 199);
              try {
                int lastspace = s.lastIndexOf(' ');
                if (lastspace > 0) {
                  s = s.substring(0, lastspace);
                  s = s + "...";
                  //sb.append("<br/><br/>");
                  //sb.append(s);
                  row.about = s;
                } else {
                  //sb.append("<br/><br/>");
                  //sb.append(escapedIntro);
                  row.about = escapedIntro;
                }
              } catch (Exception e) {
                //sb.append("<br/><br/>");
                //sb.append(escapedIntro);
                row.about = escapedIntro;
              }
            } else {
              //sb.append("<br/><br/>");
              //sb.append(escapedIntro);
              row.about = escapedIntro;
            }
          } else if (rec.description != null && rec.description.length() > 0) {
            String escapedDesc = Sanitizers.FORMATTING.and(Sanitizers.LINKS).sanitize(rec.description);

            if (escapedDesc.length() > 200) {
              String s = escapedDesc.substring(0, 199);
              try {
                int lastspace = s.lastIndexOf(' ');
                if (lastspace > 0) {
                  s = s.substring(0, lastspace);
                  s = s + "...";
                  //sb.append("<br/><br/>");
                  //sb.append(s);
                  row.about = s;
                } else {
                  //sb.append("<br/><br/>");
                  //sb.append(escapedDesc);
                  row.about = escapedDesc;
                }
              } catch (Exception e) {
                //sb.append("<br/><br/>");
                //sb.append(escapedDesc);
                row.about = escapedDesc;
              }
            } else {
              //sb.append("<br/><br/>");
              //sb.append(escapedDesc);
              row.about = escapedDesc;
            }
          }
          //row.about = sb.toString();

          row.createdBy = rec.getCreatedby();

          if (SecurityMgr.isCmpyAdmin(sessionMgr.getCredentials().getCmpyId(), sessionMgr)) {
            dtv.data.add(row);
          }
          else {
            if (rec.accessLevel != Destination.AccessLevel.PRIVATE) {
              dtv.data.add(row);
            }
            else if (rec.accessLevel == Destination.AccessLevel.PRIVATE && rec.createdby.equals(cred.getUserId())) {
              dtv.data.add(row);
            }
          }
        }
      }
      return ok(Json.toJson(dtv));
    }

    return ok();
  }


  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result searchAllDocuments() {
    JsonNode jsonRequest = request().body().asJson();
    DataTablesForm dtRequest = Json.fromJson(jsonRequest, DataTablesForm.class);
    SessionMgr    sessionMgr = SessionMgr.fromContext(ctx());

    class DocRow {
      public String imgUrl;
      public String docName;
      public String type;
      public String about;
      public String createdBy;
      public String createdTime;
      public String modifiedBy;
      public String modifiedTime;
      public  boolean canEdit;
      //public String company;
      public String id;
    }

    DataTablesView<DocRow> dtv = new DataTablesView<>(dtRequest.draw);

    dtv.searchTerm = "";
    if (jsonRequest.get("keyword").asText() != null && jsonRequest.get("keyword").asText().length() > 0) {
      dtv.searchTerm = "%" + jsonRequest.get("keyword").asText().replace(" ", "%") + "%";
    }
    else {
      dtv.searchTerm = "%";
    }

    if (jsonRequest.get("titleOnly") != null) {
      dtv.inSearchTitleOnly = Boolean.valueOf(jsonRequest.get("titleOnly").asText());
    } else {
      dtv.inSearchTitleOnly = false;
    }

    dtv.destType = 0;
    if (jsonRequest.get("destType") != null) {
      dtv.destType = Integer.parseInt(jsonRequest.get("destType").asText());
    }

    String orderBy = "lastupdatedtimestamp desc,destinationTypeId,name asc";
    if(dtRequest.order  != null && dtRequest.order.size() >= 1) {
      int colId = dtRequest.order.get(0).column;
      if ( dtRequest.columns != null && dtRequest.columns.get(colId) != null && !dtRequest.columns.get(colId).name.isEmpty()) {
        orderBy = dtRequest.columns.get(colId).name + " " + dtRequest.order.get(0).dir;
      }
    }

    Credentials cred = sessionMgr.getCredentials();

    String cmpyId = jsonRequest.get("cmpyId").asText();
    String cmpyName = "";
    Company cmpy = Company.find.byId(cmpyId);
    if (cmpy != null) {
      cmpyName = cmpy.getName();
    }

    HashMap<String, String> agencyList = new HashMap<>();
    if(cred.hasCompany()) {
      Company c = cred.getCompany();
      agencyList.put(c.cmpyid, c.getName());
    }

    HashMap<Integer, String> destinationTypeDesc = new HashMap<Integer, String>();
    List<DestinationType> destinationTypes = DestinationType.findActive();
    if (destinationTypes != null) {
      ArrayList<GenericTypeView> destinationTypeList = new ArrayList<GenericTypeView>();
      for (DestinationType u : destinationTypes) {
        GenericTypeView typeView = new GenericTypeView();
        typeView.id = String.valueOf(u.getDestinationtypeid());
        typeView.name = u.getName();
        destinationTypeList.add(typeView);
        destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());

      }
    }

    List<Integer> types = new ArrayList<Integer>();
    if (dtv.destType == 0) {
      types.add(APPConstants.CITY_DESTINATION_TYPE);
      types.add(APPConstants.COUNTRY_DESTINATION_TYPE);
      types.add(APPConstants.PLACE_DESTINATION_TYPE);
      types.add(APPConstants.TOUR_DESTINATION_TYPE);
      types.add(APPConstants.ACTIVITY_DESTINATION_TYPE);
      types.add(APPConstants.GENERAL_DESTINATION_TYPE);
      types.add(APPConstants.CMPY_INFO_DESTINATION_TYPE);
    } else {
      types.add(dtv.destType);
    }

    List<Destination> destList = new ArrayList<>();

    if (SecurityMgr.isCmpyAdmin(cmpyId, sessionMgr)) {
      destList = Destination.findActiveDestByKeywordForCmpy(cmpyId,
          types,
          dtv.searchTerm,
          dtRequest.start,
          dtRequest.length,
          dtv.inSearchTitleOnly, orderBy);

      dtv.recordsFiltered = Destination.countActiveDestByKeywordForCmpy(cmpyId,
          types,
          dtv.searchTerm,
          dtv.inSearchTitleOnly);
    }
    else {
      destList = Destination.findActiveDestByKeywordForUser(cmpyId,
          types,
          dtv.searchTerm,
          dtRequest.start,
          dtRequest.length,
          dtv.inSearchTitleOnly, orderBy, cred.getUserId());

      dtv.recordsFiltered = Destination.countActiveDestByKeywordForUser(cmpyId,
          types,
          dtv.searchTerm,
          dtv.inSearchTitleOnly, cred.getUserId());
    }


    if (destList != null && destList.size() > 0) {
      for (Destination rec : destList) {
        DocRow row = new DocRow();
        ///Trip trip = rec.getTrip()                                      ;
        if (SecurityMgr.canEditDestination(rec, sessionMgr)) {
          row.id = routes.DestinationController.newDestination(rec.destinationid).url();
          row.canEdit = true;
        } else {
          row.id = "/guide/destinationModal?inDestId="+rec.destinationid;
          row.canEdit = false;
        }
        row.docName = rec.getName();
        StringBuilder sb = new StringBuilder();

        if(agencyList !=null && agencyList.size() > 1) {
          sb.append(cmpyName);
          sb.append("<br/>");
        }
        sb.append(destinationTypeDesc.get(rec.destinationtypeid));
        if ((rec.getCity() !=null && rec.getCity().length() > 0) || (rec.getCountry() !=null && rec.getCountry().length() > 0)) {
          sb.append("<br/>");
          if ((rec.getCity() !=null && rec.getCity().length() > 0) && (rec.getCountry() !=null && rec.getCountry().length() > 0)) {
            sb.append(rec.getCity());
            sb.append(", ");
            sb.append(rec.getCountry());
          } else {
            sb.append(rec.getCountry());
          }
        }
        row.type = sb.toString();
        if (rec.hasCover()) {
          row.imgUrl = rec.getCoverurl();
          /*row.cover = rec.getCoverView();
          if (row.cover.url.contains("?")) {
            row.cover.url += "&v="+ Instant.now().toEpochMilli();
          } else {
            row.cover.url += "?v="+ Instant.now().toEpochMilli();
          }*/
        }
        if (row.imgUrl == null && rec.getImageUrl() != null) {
          row.imgUrl = rec.getImageUrl();
        }
        if (rec.intro != null && rec.intro.length() > 0) {
          String escapedIntro = Sanitizers.FORMATTING.and(Sanitizers.LINKS).sanitize(rec.intro);
          if (escapedIntro.length() > 200) {
            String s = escapedIntro.substring(0, 199);
            try {
              int lastspace = s.lastIndexOf(' ');
              if (lastspace > 0) {
                s = s.substring(0, lastspace);
                s = s + "...";
                row.about = s;
              }
              else {
                row.about = escapedIntro;
              }
            }
            catch (Exception e) {
              row.about = escapedIntro;
            }
          }
          else {
            row.about = escapedIntro;
          }
        } else if (rec.description != null && rec.description.length() > 0) {
          String escapedDesc = Sanitizers.FORMATTING.and(Sanitizers.LINKS).sanitize(rec.description);

          if (escapedDesc.length() > 200) {
            String s = escapedDesc.substring(0, 199);
            try {
              int lastspace = s.lastIndexOf(' ');
              if (lastspace > 0) {
                s = s.substring(0, lastspace);
                s = s + "...";
                row.about = s;
              }
              else {
                row.about = escapedDesc;
              }
            }
            catch (Exception e) {
              row.about = escapedDesc;
            }
          }
          else {
            row.about = escapedDesc;
          }
        }
        //row.about = rec.description;

        row.createdBy = rec.getCreatedby() ;//+ "<br/><em>("+UserCmpyLink.getUserAccessLevel(rec.getCreatedby(), rec.cmpyid).toLowerCase()+")</em>";
        row.createdTime = rec.getCreatedTs().toString();
        row.modifiedBy = rec.getModifiedby() ;//+ "<br/><em>("+UserCmpyLink.getUserAccessLevel(rec.getModifiedby(), rec.cmpyid).toLowerCase()+")</em>";;
        row.modifiedTime = rec.getModifiedTs().toString();

        dtv.data.add(row);
      }
    }

    return  ok(Json.toJson(dtv));
  }

  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result searchAllPages() {
    JsonNode jsonRequest = request().body().asJson();
    DataTablesForm dtRequest = Json.fromJson(jsonRequest, DataTablesForm.class);
    SessionMgr    sessionMgr = SessionMgr.fromContext(ctx());

    class PageRow {
      public String imgUrl;
      public String docName;
      public String pageName;
      public String about;
      public String createdBy;
      public String createdTime;
      public String modifiedBy;
      public String modifiedTime;
      public boolean canEdit;
      //public String company;
      public String id;
    }

    DataTablesView<PageRow> dtv = new DataTablesView<>(dtRequest.draw);
    List<DocumentsHelper.UmPage> pages = null;

    dtv.searchTerm = "";
    if (jsonRequest.get("keyword").asText() != null && jsonRequest.get("keyword").asText().length() > 0) {
      dtv.searchTerm = "%" + jsonRequest.get("keyword").asText().replace(" ", "%") + "%";
    }
    else {
      dtv.searchTerm = "%";
    }

    if (jsonRequest.get("titleOnly") != null) {
      dtv.inSearchTitleOnly = Boolean.valueOf(jsonRequest.get("titleOnly").asText());
    } else {
      dtv.inSearchTitleOnly = false;
    }

    String orderBy = "lastupdatedtimestamp desc, name asc";
    if(dtRequest.order  != null && dtRequest.order.size() >= 1) {
      int colId = dtRequest.order.get(0).column;
      if ( dtRequest.columns != null && dtRequest.columns.get(colId) != null && !dtRequest.columns.get(colId).name.isEmpty()) {
        orderBy = dtRequest.columns.get(colId).name + " " + dtRequest.order.get(0).dir;
      }
    }

    Credentials cred = sessionMgr.getCredentials();

    String cmpyId = jsonRequest.get("cmpyId").asText();
    String cmpyName = "";
    Company cmpy = Company.find.byId(cmpyId);
    if (cmpy != null) {
      cmpyName = cmpy.getName();
    }

    HashMap<String, String> agencyList = new HashMap<>();
    if(cred.hasCompany()) {
      Company c = cred.getCompany();
      agencyList.put(c.cmpyid, c.getName());
    }

    HashMap<Integer, String> destinationTypeDesc = new HashMap<Integer, String>();
    List<DestinationType> destinationTypes = DestinationType.findActive();
    if (destinationTypes != null) {
      ArrayList<GenericTypeView> destinationTypeList = new ArrayList<GenericTypeView>();
      for (DestinationType u : destinationTypes) {
        GenericTypeView typeView = new GenericTypeView();
        typeView.id = String.valueOf(u.getDestinationtypeid());
        typeView.name = u.getName();
        destinationTypeList.add(typeView);
        destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());

      }
    }

    List<Integer> types = new ArrayList<Integer>();
    types.add(APPConstants.CITY_DESTINATION_TYPE);
    types.add(APPConstants.COUNTRY_DESTINATION_TYPE);
    types.add(APPConstants.PLACE_DESTINATION_TYPE);
    types.add(APPConstants.TOUR_DESTINATION_TYPE);
    types.add(APPConstants.ACTIVITY_DESTINATION_TYPE);
    types.add(APPConstants.GENERAL_DESTINATION_TYPE);
    types.add(APPConstants.CMPY_INFO_DESTINATION_TYPE);


    if (dtv.inSearchTitleOnly) {
      pages =  DocumentsHelper.findPagesByNameDT(dtv.searchTerm, cmpyId, types, sessionMgr, dtRequest.start, dtRequest.length, orderBy);
      dtv.recordsFiltered = DestinationGuide.countByNameForCompany(dtv.searchTerm, cmpyId, types, sessionMgr);
    } else {
      pages = DocumentsHelper.findPagesByTermDT(dtv.searchTerm, cmpyId, types, sessionMgr, dtRequest.start, dtRequest.length, orderBy);
      dtv.recordsFiltered = DestinationGuide.countByTermForCompany(dtv.searchTerm, cmpyId, types, sessionMgr);
    }

    //Pages
    List<String> addedIds = new ArrayList<>();
    if (pages != null && pages.size() > 0) {
      for (DocumentsHelper.UmPage page : pages) {
        if (SecurityMgr.canAccessDestination(page.destination, sessionMgr) && !addedIds.contains(page.page.destinationguideid)) {
          addedIds.add(page.page.destinationid);
          PageRow row = new PageRow();
          ///Trip trip = rec.getTrip()                                      ;
          if (SecurityMgr.canEditDestination(page.destination, sessionMgr)) {
            row.id = routes.DestinationController.newDestinationGuide(page.page.destinationid, page.page.destinationguideid).url();
            row.canEdit = true;
          } else {
            row.id = "/guide/destinationModal?inDestId="+page.page.destinationid;
            row.canEdit = false;
          }

          row.docName = page.destination.name;
          StringBuilder sb = new StringBuilder();
          sb.append(page.page.name);

          if ((page.page.getCity() !=null && page.page.getCity().length() > 0) || (page.page.getCountry() !=null && page.page.getCountry().length() > 0)) {
            sb.append("<br/>");
            if ((page.page.getCity() !=null && page.page.getCity().length() > 0) && (page.page.getCountry() !=null && page.page.getCountry().length() > 0)) {
              sb.append(page.page.getCity());
              sb.append(", ");
              sb.append(page.page.getCountry());
            } else {
              sb.append(page.page.getCountry());
            }
          }
          row.pageName = sb.toString();

          if (page.destination.hasCover()) {
            row.imgUrl = page.destination.getCoverurl();
          }
          if (row.imgUrl == null && page.destination.getImageUrl() != null) {
            row.imgUrl = page.destination.getImageUrl();
          }
          if (page.page.intro != null && page.page.intro.length() > 0) {
            String escapedIntro = Sanitizers.FORMATTING.and(Sanitizers.LINKS).sanitize(page.page.intro);
            if (escapedIntro.length() > 200) {
              String s = escapedIntro.substring(0, 199);
              try {
                int lastspace = s.lastIndexOf(' ');
                if (lastspace > 0) {
                  s = s.substring(0, lastspace);
                  s = s + "...";
                  row.about = s;
                } else {
                  row.about = escapedIntro;
                }
              } catch (Exception e) {
                row.about = escapedIntro;
              }
            } else {
              row.about = escapedIntro;
            }
          } else if (page.page.description != null && page.page.description.length() > 0) {
            String escapedDesc = Sanitizers.FORMATTING.and(Sanitizers.LINKS).sanitize(page.page.description);

            if (escapedDesc.length() > 200) {
              String s = escapedDesc.substring(0, 199);
              try {
                int lastspace = s.lastIndexOf(' ');
                if (lastspace > 0) {
                  s = s.substring(0, lastspace);
                  s = s + "...";
                  row.about = s;
                } else {
                  row.about = escapedDesc;
                }
              } catch (Exception e) {
                row.about = escapedDesc;
              }
            } else {
              row.about = escapedDesc;
            }
          }

          row.createdBy = page.page.getCreatedby();
          row.createdTime = page.page.getCreatedTs().toString();
          row.modifiedBy = page.page.getModifiedby();
          row.modifiedTime = page.page.getModifiedTs().toString();
          row.createdBy = page.page.getCreatedby();// + "<br/><em>("+UserCmpyLink.getUserAccessLevel(page.page.getCreatedby(), page.destination.cmpyid).toLowerCase()+")</em>";
          row.createdTime = page.page.getCreatedTs().toString();
          row.modifiedBy = page.page.getModifiedby();// + "<br/><em>("+UserCmpyLink.getUserAccessLevel(page.page.getModifiedby(), page.destination.cmpyid).toLowerCase()+")</em>";;
          row.modifiedTime = page.page.getModifiedTs().toString();

          dtv.data.add(row);
        }
      }
    }

    return  ok(Json.toJson(dtv));
  }



  @With({Authenticated.class, Authorized.class})
  public Result destinationCopySearch() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    DestinationSearchView view = new DestinationSearchView();

    //bind html form to form bean
    Form<DestinationSearchForm> destSearchForm = formFactory.form(DestinationSearchForm.class);
    DestinationSearchForm destSearchInfo = destSearchForm.bindFromRequest().get();

    if (destSearchInfo == null ||
        destSearchInfo.getInKeyword() == null ||
        destSearchInfo.getInKeyword().length() == 0 ||
        destSearchInfo.getInDestGuideId() == null ||
        destSearchInfo.getInCmpyId() == null ||
        destSearchInfo.getInCmpyId().trim().length() == 0) {
      //return error msg - search term is mandatory
      view.message = "Search term is required.";
      return ok(views.html.guide.searchDestination.render(view));

    }
    else {
      DestinationGuide guide = DestinationGuide.find.byId(destSearchInfo.getInDestGuideId());
      if (guide == null) {
        view.message = "A System Error has occurred.";
        return ok(views.html.guide.searchDestination.render(view));
      }

      Destination origDest = Destination.find.byId(guide.destinationid);

      if (origDest == null || !origDest.cmpyid.equals(destSearchInfo.getInCmpyId()) || !SecurityMgr.canAccessCmpy(
          destSearchInfo.getInCmpyId(),
          sessionMgr)) {
        view.message = "Unauthorized Access.";
        return ok(views.html.guide.searchDestination.render(view));
      }
      view.destGuideId = guide.destinationguideid;


      List<String> cmpies = new ArrayList<String>();
      cmpies.add(destSearchInfo.getInCmpyId());
      List<DestinationType> destinationTypes = DestinationType.findActive();
      HashMap<Integer, String> destinationTypeDesc = new HashMap<Integer, String>();
      if (destinationTypes != null) {
        for (DestinationType u : destinationTypes) {
          destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());
        }
      }

      List<Destination> destList = Destination.findActiveDestAllTypesByKeyword(cmpies, destSearchInfo.getInKeyword());
      buildDestinations(view, destList, destSearchInfo, destinationTypeDesc);

      return ok(views.html.guide.destinationCopySearch.render(view));
    }

  }

  @With({Authenticated.class, Authorized.class})
  public Result destinationPageSearch() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Credentials cred = sessionMgr.getCredentials();

    DestinationSearchView view = new DestinationSearchView();
    //bind html form to form bean
    Form<DestinationSearchForm> destSearchForm = formFactory.form(DestinationSearchForm.class);
    DestinationSearchForm destSearchInfo = destSearchForm.bindFromRequest().get();

    if (destSearchInfo == null || destSearchInfo.getInKeyword() == null ||
        destSearchInfo.getInKeyword().length() == 0 ||
        (destSearchInfo.getInDestIds() == null  && !destSearchInfo.isInTripNote() )) {
      //return error msg - search term is mandatory
      view.message = "Search term is required.";
      return ok(views.html.guide.destinationPageSearch.render(view));

    }
    else {
      if (!destSearchInfo.isInTripNote()) {
        Destination destination = Destination.find.byId(destSearchInfo.getInDestIds());
        if (destination == null || !SecurityMgr.canAccessDestination(destination, sessionMgr)) {
          view.message = "A System Error has occurred.";
          return ok(views.html.guide.destinationPageSearch.render(view));
        }
        view.destId = destination.destinationid;
      }

      view.tripNoteDate = destSearchInfo.getInTripNoteDate();
      view.cmpyId = sessionMgr.getCredentials().getCmpyId();

      List<Integer> types = new ArrayList<>();
      types.add(APPConstants.CITY_DESTINATION_TYPE);
      types.add(APPConstants.COUNTRY_DESTINATION_TYPE);
      types.add(APPConstants.PLACE_DESTINATION_TYPE);
      types.add(APPConstants.TOUR_DESTINATION_TYPE);
      types.add(APPConstants.ACTIVITY_DESTINATION_TYPE);
      types.add(APPConstants.GENERAL_DESTINATION_TYPE);
      types.add(APPConstants.CMPY_INFO_DESTINATION_TYPE);

      List<DestinationType> destinationTypes = DestinationType.findActive();
      HashMap<Integer, String> destinationTypeDesc = new HashMap<>();
      if (destinationTypes != null) {
        for (DestinationType u : destinationTypes) {
          destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());
        }
      }


      List<DocumentsHelper.UmPage> pages;
      if(destSearchInfo.isInSearchTitleOnly()) {
        pages = DocumentsHelper.findPagesByName(destSearchInfo.getInKeyword(),view.cmpyId,types, sessionMgr);
      } else {
        pages = DocumentsHelper.findPagesByTerm(destSearchInfo.getInKeyword(),view.cmpyId,types, sessionMgr);
      }

      List<String> addedIds = new ArrayList<>();
      List<DestinationGuideView> destViews = new ArrayList<>();

      if(pages != null && pages.size() > 0) {
        for (DocumentsHelper.UmPage page : pages) {
          if (SecurityMgr.canAccessDestination(page.destination, sessionMgr) &&
              !addedIds.contains(page.page.destinationguideid)) {
            if (!destSearchInfo.isInTripNote() || (destSearchInfo.isInTripNote() && page.page.getPageId() == null)) {

              Destination dest = Destination.find.byId(page.page.destinationid);
              if (SecurityMgr.isCmpyAdmin(dest.cmpyid, sessionMgr)
                  || dest.accessLevel == Destination.AccessLevel.PUBLIC
                  || (dest.accessLevel == Destination.AccessLevel.PRIVATE && dest.createdby.equals(cred.getUserId()))) {

                addedIds.add(page.page.destinationid);

                DestinationGuideView destView = new DestinationGuideView();
                destView.id = page.page.destinationguideid;
                destView.destinationId = page.page.destinationid;


                destView.name = page.page.name;
                destView.destinationName = page.destination.name;


                destView.city = page.page.city;
                destView.country = page.page.country;
                if (page.page.intro != null && page.page.intro.length() > 0) {
                  String escapedGuideIntro = Sanitizers.FORMATTING.and(Sanitizers.LINKS).sanitize(page.page.intro);

                  if (escapedGuideIntro.length() > 200) {
                    String s = escapedGuideIntro.substring(0, 199);
                    try {
                      int lastspace = s.lastIndexOf(' ');
                      if (lastspace > 0) {
                        s = s.substring(0, lastspace);
                        s = s + "...";
                        destView.intro = s;
                      } else {
                        destView.intro = escapedGuideIntro;
                      }
                    } catch (Exception e) {
                      destView.intro = escapedGuideIntro;
                    }

                  } else {
                    destView.intro = escapedGuideIntro;
                  }
                } else if (page.page.description != null && page.page.description.length() > 0) {
                  String escapedGuideDesc = Sanitizers.FORMATTING.and(Sanitizers.LINKS).sanitize(page.page.description);

                  if (escapedGuideDesc.length() > 200) {
                    String s = escapedGuideDesc.substring(0, 199);
                    try {
                      int lastspace = s.lastIndexOf(' ');
                      if (lastspace > 0) {
                        s = s.substring(0, lastspace);
                        s = s + "...";
                        destView.intro = s;
                      } else {
                        destView.intro = escapedGuideDesc;
                      }
                    } catch (Exception e) {
                      destView.intro = escapedGuideDesc;
                    }

                  } else {
                    destView.intro = escapedGuideDesc;
                  }
                }
                destView.locLat = String.valueOf(page.page.loclat);
                destView.locLong = String.valueOf(page.page.loclong);

                //get covers
                if (page.destination.hasCover()) {
                  destView.coverUrl = page.destination.getCoverurl();
                }
                destView.tag = page.page.tag;


                if (SecurityMgr.canEditDestination(page.destination, sessionMgr)) {
                  destView.canEdit = true;
                }

                destViews.add(destView);
              }
            }
          }
        }
        view.destinationGuideList = destViews;
        view.tripNote = destSearchInfo.isInTripNote();
        view.tripId = destSearchInfo.getInTripId();
        view.tripDetailId =destSearchInfo.getInTripDetailId();
      }

      return ok(views.html.guide.destinationPageSearch.render(view));
    }
  }

  public static void buildDestinations(DestinationSearchView view,
                                        List<Destination> destList,
                                        DestinationSearchForm destSearchInfo,
                                        HashMap<Integer, String> destinationTypeDesc) {

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Credentials cred = sessionMgr.getCredentials();

    if (destList != null && destList.size() > 0) {
      List<DestinationView> destViews = new ArrayList<>();
      for (Destination dest : destList) {
        if (destSearchInfo.getInDestIds() == null || !destSearchInfo.getInDestIds().contains(dest.destinationid)) {

          if (SecurityMgr.isCmpyAdmin(dest.cmpyid, sessionMgr)
              || dest.accessLevel == Destination.AccessLevel.PUBLIC || dest.accessLevel == null
              || (dest.accessLevel == Destination.AccessLevel.PRIVATE && dest.createdby.equals(cred.getUserId()))) {
            destViews.add(buildDestinationView(dest, view, destinationTypeDesc));
          }
        }
      }
      view.destinationList = destViews;
    }
  }

  public static DestinationView buildDestinationView(Destination dest,
                                                      DestinationSearchView view,
                                                      HashMap<Integer, String> destinationTypeDesc) {
    DestinationView destView = new DestinationView();
    destView.id = dest.destinationid;
    destView.name = dest.name;
    destView.destinationType = String.valueOf(dest.destinationtypeid);
    destView.destinationTypeDesc = destinationTypeDesc.get(dest.destinationtypeid);
    destView.cmpyId = dest.cmpyid;
    destView.city = dest.city;
    destView.country = dest.country;
    if (dest.intro != null) {
      String escapedIntro = Sanitizers.FORMATTING.and(Sanitizers.LINKS).sanitize(dest.intro);

      if (escapedIntro.length() > 100) {
        String s = escapedIntro.substring(0, 99);
        try {
          int lastspace = s.lastIndexOf(' ');
          if (lastspace > 0) {
            s = s.substring(0, lastspace);
            s = s + "...";
            destView.intro = s;
          }
          else {
            destView.intro = escapedIntro;
          }
        }
        catch (Exception e) {
          destView.intro = escapedIntro;
        }
      }
      else {
        destView.intro = escapedIntro;
      }
    }
    destView.locLat = String.valueOf(dest.loclat);
    destView.locLong = String.valueOf(dest.loclong);
    //get covers
    if (dest.hasCover()) {
      destView.coverUrl = dest.getCoverurl();
    }
    if (dest.loclat != 0 && dest.loclong != 0) {
      view.displayMap = true;
    }
    return destView;
  }


  @With({Authenticated.class, Authorized.class})
  public Result templateDestinationSearch() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    DestinationSearchView view = new DestinationSearchView();

    //bind html form to form bean
    Form<DestinationSearchForm> destSearchForm = formFactory.form(DestinationSearchForm.class);
    DestinationSearchForm destSearchInfo = destSearchForm.bindFromRequest().get();

    if (destSearchInfo == null || destSearchInfo.getInKeyword() == null || destSearchInfo.getInKeyword()
                                                                                         .length() == 0) {
      //return error msg - search term is mandatory
      view.message = "Search term is required.";
      return ok(views.html.guide.searchDestination.render(view));

    }
    else {
      Template template = Template.find.byId(destSearchInfo.getInTripId());
      if (template == null || !SecurityMgr.canAccessTemplate(template, sessionMgr)) {
        //return error msg - search term is mandatory
        view.message = "Error - please resubmit";
        return ok(views.html.guide.searchDestination.render(view));
      }
      view.tripId = template.templateid;
      List<Integer> types = new ArrayList<Integer>();
      types.add(APPConstants.CITY_DESTINATION_TYPE);
      types.add(APPConstants.COUNTRY_DESTINATION_TYPE);
      types.add(APPConstants.PLACE_DESTINATION_TYPE);
      types.add(APPConstants.TOUR_DESTINATION_TYPE);
      types.add(APPConstants.ACTIVITY_DESTINATION_TYPE);
      int startPos = destSearchInfo.getInStartPos();
      if (startPos < 0) {
        startPos = 0;
      }

      List<DestinationType> destinationTypes = DestinationType.findActive();
      HashMap<Integer, String> destinationTypeDesc = new HashMap<Integer, String>();
      if (destinationTypes != null) {
        for (DestinationType u : destinationTypes) {
          destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());
        }
      }

      List<TmpltGuide> activeGuides = TmpltGuide.findByTemplate(template.templateid);
      ArrayList<String> activeDestinationIds = new ArrayList<String>();
      if (activeGuides != null) {
        for (TmpltGuide t : activeGuides) {
          activeDestinationIds.add(t.destinationid);
        }
      }

      List<String> cmpies = new ArrayList<String>();
      cmpies.add(template.cmpyid);
      List<Destination> destList = Destination.findActiveDestByKeyword(cmpies,
                                                                       types,
                                                                       destSearchInfo.getInKeyword(),
                                                                       startPos,
                                                                       APPConstants.MAX_RESULT_COUNT,
                                                              true,
                                                              "name asc");
      if (destList != null && destList.size() > 0) {
        List<DestinationView> destViews = new ArrayList<DestinationView>();
        for (Destination dest : destList) {
          if (destSearchInfo.getInDestIds() == null ||
              !destSearchInfo.getInDestIds().contains(dest.destinationid) ||
              !activeDestinationIds.contains(dest.destinationid)) {
            destViews.add(buildDestinationView(dest, view, destinationTypeDesc));
          }
        }
        view.destinationList = destViews;
      }
      return ok(views.html.guide.searchTemplateDestination.render(view));
    }
  }

  //public access
  public Result destinationModal() {
    final DynamicForm form = formFactory.form().bindFromRequest();
    String destId = form.get("inDestId");
    List<DestinationType> destinationTypes = DestinationType.findActive();
    HashMap<Integer, String> destinationTypeDesc = new HashMap<Integer, String>();
    if (destinationTypes != null) {
      for (DestinationType u : destinationTypes) {
        destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());
      }
    }

    if (destId != null && destId.length() > 0) {
      //retrieve dest info
      try {
        DestinationView destView = getDestView(destId, destinationTypeDesc);
        if (destView != null) {
          return ok(views.html.guide.destinationModal.render(destView));
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    BaseView baseView = new BaseView();
    baseView.message = "Error";
    return ok(views.html.common.message.render(baseView));
  }

  public static DestinationView getDestView(String destId, Map<Integer, String> destinationTypeDesc) {

      DestinationView destView = null;

      // TODO: Serguei: Uncomment this again
      if (!DEBUG_PDF_TEMPLATE) {
        destView = (DestinationView) CacheMgr.get(APPConstants.CACHE_DEST_VIEW_PREFIX + destId);
        if (destView != null) {
          //get cmpyInfo
          destView.cmpyInfo = CompanyController.getCmpyView(destView.cmpyId);
          return destView;
        }
      }

    Destination dest = Destination.find.byId(destId);
    return getDestView(dest, destinationTypeDesc);
  }


  public static DestinationView getDestView(Destination dest, Map<Integer, String> destinationTypeDesc) {
    try {
      DestinationView destView = null;

      if(dest == null) {
        return destView;
      }

      // TODO: Serguei: Uncomment this again
      if (!DEBUG_PDF_TEMPLATE) {
        destView = (DestinationView) CacheMgr.get(APPConstants.CACHE_DEST_VIEW_PREFIX + dest.getDestinationid());
        if (destView != null) {
          //get cmpyInfo
          destView.cmpyInfo = CompanyController.getCmpyView(destView.cmpyId);
          return destView;
        }
      }
      if(destView == null) {
        destView = new DestinationView();
      }

      destView.id = dest.destinationid;
      destView.name = dest.name;
      destView.destinationType = String.valueOf(dest.destinationtypeid);
      destView.destinationTypeDesc = destinationTypeDesc.get(dest.destinationtypeid);
      destView.cmpyId = dest.cmpyid;
      destView.city = dest.city;
      destView.country = dest.country;
      destView.intro = dest.intro;
      destView.locLat = String.valueOf(dest.loclat);
      destView.locLong = String.valueOf(dest.loclong);
      destView.coverUrl = dest.getCoverurl();
      destView.coverName = dest.getCovername();
      destView.tag = dest.tag;

      for(SysContentSite s : MobilizerController.getContentSites()) {
        destView.addMobilizedDomain(s.getDomain(), s.getName());
      }

      //check for pages
      List<DestinationGuide> guides = DestinationGuide.findActiveDestGuides(dest.destinationid);
      if (guides != null) {
        List<DestinationGuideView> guideViews = new ArrayList<>();
        for (DestinationGuide destGuide : guides) {
          DestinationGuideView guideView = new DestinationGuideView();
          guideView.destinationId = destGuide.destinationid;
          guideView.id = destGuide.destinationguideid;
          guideView.city = destGuide.city;
          guideView.country = destGuide.country;
          guideView.state = destGuide.state;

          //escape all potential html script tag
          if (destGuide.getPageId() == null) {
            guideView.description = destGuide.description;
            guideView.isMobilized = false;
          } else {
            guideView.isMobilized = true;
            LibraryPage lp = LibraryPage.find.byId(destGuide.getPageId());
            guideView.description = lp.getContent();
            //TODO: Serguei: Replace {} mustache with S3 direct URLs
          }
          guideView.intro = destGuide.intro;
          guideView.landmark = destGuide.landmark;
          guideView.name = destGuide.name;
          guideView.locLat = String.valueOf(destGuide.loclat);
          guideView.locLong = String.valueOf(destGuide.loclong);
          guideView.tag = destGuide.tag;
          if (destGuide.loclat != 0 && destGuide.loclong != 0) {
            destView.displayMap = true;
          }
          guideView.rank = destGuide.rank;
          guideView.streetAddr1 = destGuide.streetaddr1;
          guideView.recommendationType = destGuide.getRecommendationtype();
          guideView.zipCode =destGuide.zipcode;
          //create the milestone here....

          //get cover if any
          List<DestinationGuideAttach> covers = DestinationGuideAttach.getCover(destGuide.destinationguideid);
          if (covers != null && covers.size() == 1) {
            guideView.coverName = covers.get(0).getName();
            guideView.coverUrl = covers.get(0).getAttachurl();
          }
          //get any attachments
          if (destGuide.getDestguidetimestamp() != null && destGuide.getDestguidetimestamp() > 0) {
            guideView.setupTimestamp(destGuide.getDestguidetimestamp());
          }

          List<DestinationGuideAttach> attachments = DestinationGuideAttach.findActiveByDestGuide(destGuide
                                                                                                      .destinationguideid);
          List<AttachmentView> images = new ArrayList<>();
          List<AttachmentView> urls = new ArrayList<>();
          List<AttachmentView> videos = new ArrayList<>();
          List<AttachmentView> files = new ArrayList<>();


          for (DestinationGuideAttach attach : attachments) {
            AttachmentView attachView = new AttachmentView();
            attachView.id = attach.fileid;
            attachView.name = attach.name;
            attachView.comments = Utils.escapeHtml(attach.comments);
            attachView.createdById = attach.createdby;

            attachView.attachUrl = attach.getAttachurl();
            if (attachView.attachUrl != null && !attachView.attachUrl.toLowerCase().startsWith("http")) {
              attachView.attachUrl = "http://" + attachView.attachUrl;
            }

            attachView.attachName = attach.getAttachname();

            if (attach.getAttachtype() == PageAttachType.VIDEO_LINK) {
              if (attach.getAttachurl().toLowerCase().contains("youtube.com") ||
                  attach.getAttachurl().toLowerCase().contains("youtu.be")) {
                //try to get the static image
                attach.getAttachurl().toLowerCase().contains("youtube.com");
                String pattern = "(?<=(?:v|i)=)[a-zA-Z0-9-]+(?=&)|(?<=(?:v|i)\\/)[^&\\n]+|(?<=embed\\/)[^\"&\\n]+|" +
                                 "(?<=‌​(?:v|i)=)[^&\\n]+|(?<=youtu.be\\/)[^&\\n]+";

                Pattern compiledPattern = Pattern.compile(pattern);
                Matcher matcher = compiledPattern.matcher(attach.getAttachurl());

                if (matcher.find()) {
                  String videoId = matcher.group();
                  attachView.attachUrl = "https://www.youtube.com/embed/" + videoId + "?feature=player_detailpage";
                }
              }
              videos.add(attachView);
            }
            else if (attach.getAttachtype() == PageAttachType.WEB_LINK) {
              urls.add(attachView);
            }
            else if (attach.getAttachtype() == PageAttachType.FILE_LINK) {
              files.add(attachView);
            }
            else {
              //attachView.attachUrl = attach.fileid + "_" + attach.attachname;
              images.add(attachView);
            }
          }

          guideView.links = urls;
          guideView.photos = images;
          guideView.videos = videos;
          guideView.files = files;
          guideViews.add(guideView);
        }
        destView.guides = groupByDate(guideViews);
      }
      //get cmpyInfo
      destView.cmpyInfo = CompanyController.getCmpyView(dest.cmpyid);

      //save to cache
      try {
        CacheMgr.set(APPConstants.CACHE_DEST_VIEW_PREFIX + dest.destinationid, destView, APPConstants.CACHE_EXPIRY_SECS);
      }
      catch (Exception e) {
        e.printStackTrace();
        Log.err("getDestView: " + dest.destinationid, e);
      }

      return destView;
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  //group guideviews by date
  public static List<DestinationGuideView> groupByDate(List<DestinationGuideView> guideViews) {
    List<DestinationGuideView>        orderedViews = new ArrayList<>();
    Map<String, DestinationGuideView> groupedViews = new HashMap<>();
    if (guideViews != null) {
      for (DestinationGuideView dest : guideViews) {
        if (dest.date != null && dest.date.trim().length() > 0) {
          DestinationGuideView dateView = null;
          if (groupedViews.containsKey(dest.date)) {
            dateView = groupedViews.get(dest.date);
          }
          else {
            //build the parent object
            dateView = new DestinationGuideView();
            dateView.name = Utils.formatDatePrint(dest.timestamp);
            dateView.id = dest.id;
            dateView.groupedViews = new ArrayList<>();
            dateView.photos = new ArrayList<>();
            dateView.timestamp = dest.timestamp;
            orderedViews.add(dateView);
            groupedViews.put(dest.date, dateView);
          }
          dateView.groupedViews.add(dest);
          if (dest.photos != null) {
            dateView.photos.addAll(dest.photos);
          }
        }
        else {
          orderedViews.add(dest);
        }
      }
    }

    //sort the grouped views if any
    for (String key : groupedViews.keySet()) {
      DestinationGuideView groupedView = groupedViews.get(key);
      if (groupedView.groupedViews != null) {
        Collections.sort(groupedView.groupedViews, DestinationGuideView.DestinationGuideViewComparator);
      }
    }

    return orderedViews;
  }

  //public access
  public Result loadPdf() {
    final DynamicForm form = formFactory.form().bindFromRequest();
    String destId = form.get("inDestId");
    String tripId = form.get("inTripId");
    String accountId = form.get("inAid");
    String hostUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);

    if (destId == null || destId.length() == 0) {
      destId = flash(SessionConstants.SESSION_DEST_ID);
      if (destId == null || destId.length() == 0) {
        BaseView baseView = new BaseView();
        baseView.message = "Error";
        return ok(views.html.error.error404.render(baseView));
      }
    }

    Destination dest = Destination.find.byId(destId);
    if (dest == null || dest.status == APPConstants.STATUS_DELETED) {
      BaseView baseView = new BaseView();
      baseView.message = "Error";
      return ok(views.html.error.error404.render(baseView));
    }

    //new feature to allow the redirect to an existing PDF hosted by us
    if (dest.tag != null &&
        (dest.tag.startsWith("http://static-umapped-com.s3.amazonaws.com") ||
         dest.tag.startsWith("https://static-umapped-com.s3.amazonaws.com"))) {
      return redirect(dest.tag);
    }

    if (tripId == null || tripId.trim().length() == 0) {
      tripId = flash(SessionConstants.SESSION_PARAM_TRIPID);
    }
    if (tripId != null && tripId.trim().length() > 0) {
      Trip trip = Trip.find.byId(tripId);
      if (trip == null || trip.status == APPConstants.STATUS_DELETED) {
        BaseView baseView = new BaseView();
        baseView.message = "Error";
        return ok(views.html.error.error404.render(baseView));
      }
    }

    BaseView view = new BaseView();
    String cacheKey = destId;
    if (tripId != null) {
      cacheKey += "_" +tripId;
    }

    String fileName = "document/" + cacheKey + "/document_" + dest.getCreatedTs().getTime() + ".pdf";
    if (DEBUG_PDF_TEMPLATE) {
      fileName = "document/" + cacheKey + "/document.html";
    }

      //check if there already is a PDF generation in progess
    DocPdfRec rec = null;
    if (!DEBUG_PDF_TEMPLATE) {
      rec = (DocPdfRec) CacheMgr.get(APPConstants.CACHE_DOC_PDF + cacheKey);
    }
    if (rec != null && (System.currentTimeMillis() - rec.startTimestamp) < APPConstants.PDF_TIMEOUT) {
      fileName = rec.fileName;
    } else {
      //invoke background scheduler
      final DocPdfMsg inMsg = new DocPdfMsg();
      inMsg.lang = ctx().lang();
      inMsg.destinationId = destId;
      inMsg.tripId = tripId;
      inMsg.fileName = fileName;
      inMsg.htmlDebug = DEBUG_PDF_TEMPLATE;
      if(accountId != null) {
        inMsg.accountId = Account.decodeId(accountId);
      }
      ActorsHelper.tell(SupervisorActor.UmappedActor.PDF_DOC, inMsg);

      rec = new DocPdfRec();
      rec.destId = destId;
      rec.tripId = tripId;
      rec.fileName = inMsg.fileName;
      rec.startTimestamp = System.currentTimeMillis();
      CacheMgr.set(APPConstants.CACHE_DOC_PDF + cacheKey, rec, APPConstants.CACHE_PDF_EXPIRY_SECS);
    }

    //return refresh page

    view.url = hostUrl + "/guide/getPdf?inDestId=" + destId + "&inFileName=" + fileName;
    if (tripId != null && tripId.length() > 0) {
      view.url += "&tripId=" + tripId;
    }
    return ok(views.html.common.loadDocument.render(view));
  }

  //public access
  public Result retrievePdf() {
    final DynamicForm form = formFactory.form().bindFromRequest();
    String inDestId = form.get("inDestId");
    String inTripId = form.get("tripId");
    String inFileName = form.get("inFileName");
    BaseView view = new BaseView();
    if (inDestId != null && inFileName != null) {
      String cacheKey = inDestId;
      if (inTripId != null) {
        cacheKey += "_" +inTripId;
      }

      DocPdfRec rec = (DocPdfRec) CacheMgr.get(APPConstants.CACHE_DOC_PDF + cacheKey);
      if (rec != null && (System.currentTimeMillis() - rec.startTimestamp) < APPConstants.PDF_TIMEOUT) {
        //sleep again
        String hostUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);
        view.url = hostUrl + "/guide/getPdf?inDestId=" + inDestId + "&inFileName=" + inFileName;
        if (inTripId != null && inTripId.length() > 0) {
          view.url += "&tripId=" + inTripId;
        }        return ok(views.html.common.loadDocument.render(view));

      } else if (rec == null && S3Util.doesPDFexist(inFileName)) {
        boolean skipRedirect =false;
        String browserAgent = request().getHeader("User-Agent");
        if (browserAgent != null && browserAgent.toLowerCase().contains("trident")) {
          skipRedirect = true;
        }
        view.url = S3Util.getPDFUrl(inFileName)+"?" + System.currentTimeMillis();
        return ok(views.html.common.downloadDocument.render(view, skipRedirect));
      }
    }

    view.message = "Cannot generate PDF - please contact support@umapped.com";

    return ok(views.html.error.error500.render(view));

  }

  //utils methods for copying

  public static Destination copyDestination(Destination d, String name, String userId) {
    String newPK = DBConnectionMgr.getUniqueId();
    long currentTime = System.currentTimeMillis();
    String origPK = new String(d.getDestinationid());

    try {
      Ebean.beginTransaction();
      Destination newD = new Destination();

      newD.setCity(d.city);
      newD.setCmpyid(d.cmpyid);
      newD.setCountry(d.country);
      newD.setCovername(d.getCovername());
      newD.setCoverurl(d.getCoverurl());
      newD.setDescription(d.description);
      if (d.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE) {
        newD.setDestinationtypeid(APPConstants.ACTIVITY_DESTINATION_TYPE);
      } else {
        newD.setDestinationtypeid(d.destinationtypeid);
      }
      newD.setIntro(d.intro);
      newD.setLoclat(d.loclat);
      newD.setLoclong(d.loclong);
      newD.setState(d.state);
      newD.setStreetaddr1(d.streetaddr1);
      newD.setTag(d.tag);
      newD.setZipcode(d.zipcode);

      newD.setName(name);
      newD.setDestinationid(newPK);
      newD.setModifiedby(userId);
      newD.setCreatedby(userId);
      newD.setStatus(APPConstants.STATUS_ACTIVE);
      newD.setCreatedtimestamp(currentTime);
      newD.setLastupdatedtimestamp(currentTime);

      newD.setCoverImage(d.getCoverImage());
      newD.setAccessLevel(d.getAccessLevel());
      newD.save();

      List<DestinationGuide> guides = DestinationGuide.findActiveDestGuides(origPK);
      for (DestinationGuide guide : guides) {
        String newGuidePK = DBConnectionMgr.getUniqueId();
        String origGuidePK = new String(guide.getDestinationguideid());

        DestinationGuide newGuide = new DestinationGuide();

        newGuide.setCity(guide.city);
        newGuide.setCountry(guide.country);
        newGuide.setDescription(guide.description);
        newGuide.setIntro(guide.intro);
        newGuide.setLandmark(guide.landmark);
        newGuide.setLoclat(guide.loclat);
        newGuide.setLoclong(guide.loclong);
        newGuide.setName(guide.name);
        newGuide.setRank(guide.rank); //initialize
        newGuide.setState(guide.state);
        newGuide.setStreetaddr1(guide.streetaddr1);
        newGuide.setTag(guide.tag);
        newGuide.setTemplateid(guide.templateid);
        newGuide.setZipcode(guide.zipcode);

        newGuide.setDestinationguideid(newGuidePK);
        newGuide.setDestinationid(newPK);
        newGuide.setModifiedby(userId);
        newGuide.setCreatedby(userId);
        newGuide.setStatus(APPConstants.STATUS_ACTIVE);
        newGuide.setCreatedtimestamp(currentTime);
        newGuide.setLastupdatedtimestamp(currentTime);
        newGuide.setPageId(guide.getPageId());
        newGuide.save();

        //save any attachments
        List<DestinationGuideAttach> guideFiles = DestinationGuideAttach.findActiveByDestGuide(origGuidePK);
        for (DestinationGuideAttach guideFile : guideFiles) {
          guideFile.duplicate(newGuidePK, userId);
        }
      }
      Ebean.commitTransaction();
      return newD;
    }
    catch (Exception e) {
      e.printStackTrace();
      Ebean.rollbackTransaction();
    }
    return null;
  }

  public static DestinationGuide copyDestinationGuide(String destinationId, DestinationGuide guide, String userId) {
    long currentTime = System.currentTimeMillis();


    try {
      Ebean.beginTransaction();
      int maxRow = DestinationGuide.maxPageRankByDestId(destinationId);

      String newGuidePK = DBConnectionMgr.getUniqueId();
      String origGuidePK = new String(guide.getDestinationguideid());
      DestinationGuide newGuide = new DestinationGuide();

      newGuide.setCity(guide.city);
      newGuide.setCountry(guide.country);
      newGuide.setDescription(guide.description);
      newGuide.setIntro(guide.intro);
      newGuide.setLandmark(guide.landmark);
      newGuide.setLoclat(guide.loclat);
      newGuide.setLoclong(guide.loclong);
      newGuide.setName(guide.name);
      newGuide.setRank(maxRow + 1);
      newGuide.setState(guide.state);
      newGuide.setStreetaddr1(guide.streetaddr1);
      newGuide.setTag(guide.tag);
      newGuide.setTemplateid(guide.templateid);
      newGuide.setZipcode(guide.zipcode);
      newGuide.setRecommendationtype(guide.recommendationtype);

      newGuide.setDestinationguideid(newGuidePK);
      newGuide.setDestinationid(destinationId);
      newGuide.setModifiedby(userId);
      newGuide.setCreatedby(userId);
      newGuide.setStatus(APPConstants.STATUS_ACTIVE);
      newGuide.setCreatedtimestamp(currentTime);
      newGuide.setLastupdatedtimestamp(currentTime);
      newGuide.setPageId(guide.getPageId());
      newGuide.save();

      //save any attachments
      List<DestinationGuideAttach> guideFiles = DestinationGuideAttach.findActiveByDestGuide(origGuidePK);
      for (DestinationGuideAttach guideFile : guideFiles) {

        guideFile.duplicate(newGuidePK, userId);
      }
      Ebean.commitTransaction();
      return newGuide;
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
    }

    return null;
  }

  @With({Authenticated.class, Authorized.class})
  public Result afarSearch(String term) {
    List<AfarCity> cities = AfarCity.findByTerm(term);
    Log.debug("Searching Afar for:" + term);
    return ok(views.html.guide.afar.searchResults.render(cities));
  }

  @With({Authenticated.class, Authorized.class})
  public Result afarDestinationGuides(String city) {

    T42DestinationGuidesView view = new T42DestinationGuidesView();

    AfarCity aCity = AfarCity.findByCity(city);
    if(aCity == null) {
      return badRequest();
    }

    com.umapped.external.afar.Destination d = aCity.unmarshal(aCity.getData(), com.umapped.external.afar.Destination.class);

    T42Destination dest = new T42Destination();
    dest.setGeoPlaceKey(0);
    dest.setType("City");
    dest.setISOCode("US");
    dest.setName(aCity.getAfarCityId());
    dest.setDisplay(aCity.getAfarCityId());
    dest.setActive(true);
    //dest.setLatitude(d.getLocation().getLat());
    //dest.setLongitude(d.getLocation().getLng());

    view.dest = dest;

    List<AfarPoi> pois = AfarPoi.findPoisByCity(city);
    List<T42Guide> guides = new ArrayList<>();
    List<T42Hotel> hotels = new ArrayList<>();
    Set<String> tabs = new HashSet<>();

    for(AfarPoi poi: pois) {
      Highlight h = poi.unmarshal(poi.getData(), Highlight.class);
      if(h != null && h.getCategories() != null && h.getCategories().size() > 0) {
        if (h.getCategories().get(0).equals("Essential") && !tabs.contains("OVER")){
          T42Guide overTab = new T42Guide();
          overTab.setId("0", String.valueOf(0));
          overTab.setLevelNbr(1);
          overTab.setActive(true);
          overTab.setClassId(T42GuideIndex.findByKey("#DG-OVER#"));
          overTab.setContent("");
          overTab.setRowType("HIER");
          tabs.add("OVER");
          guides.add(overTab);
        } else if((h.getCategories().get(0).equals("Do") || h.getCategories().get(0).equals("Shop")) && !tabs.contains("SEE")) {
          T42Guide seeTab = new T42Guide();
          seeTab.setId("1", String.valueOf(1));
          seeTab.setLevelNbr(1);
          seeTab.setActive(true);
          seeTab.setClassId(T42GuideIndex.findByKey("#DG-SEEDO#"));
          seeTab.setContent("");
          seeTab.setRowType("HIER");
          tabs.add("SEE");
          guides.add(seeTab);
        } else if((h.getCategories().get(0).equals("Eat") || h.getCategories().get(0).equals("Drink")) && !tabs.contains("DINE")) {
          T42Guide dineTab = new T42Guide();
          dineTab.setId("2", String.valueOf(2));
          dineTab.setLevelNbr(1);
          dineTab.setActive(true);
          dineTab.setClassId(T42GuideIndex.findByKey("#DG-DINE#"));
          dineTab.setContent("");
          dineTab.setRowType("HIER");
          tabs.add("DINE");
          guides.add(dineTab);
        }

        if(tabs.size() == 3) {
          break;
        }
      }
    }

    if(guides.size() == 2) {
      if(!guides.get(0).getClassId().getClassId().equals("#DG-OVER#") || !guides.get(0).getClassId().getClassId().equals("#DG-SEEDO#")) {
        Collections.swap(guides, 0, 1);
      }
    } else if(guides.size() == 3) {
      if(!guides.get(0).getClassId().getClassId().equals("#DG-OVER#")) {
        if(guides.get(1).getClassId().getClassId().equals("#DG-OVER#")) {
          Collections.swap(guides, 0, 1);
        } else if(guides.get(2).getClassId().getClassId().equals("#DG-OVER#")) {
          Collections.swap(guides, 0, 2);
        }

        if(guides.get(2).getClassId().getClassId().equals("#DG-SEEDO#")) {
          Collections.swap(guides, 1, 2);
        }
      }
    }

    for(int i = 0; i < pois.size(); i++) {
      Highlight h = pois.get(i).unmarshal(pois.get(i).getData(), Highlight.class);
      if(h != null && h.getCategories() != null && h.getCategories().size() > 0) {
        if ((h.getCategories().get(0).equals("Do") || h.getCategories().get(0).equals("Shop"))) {
          T42Guide g = new T42Guide();
          g.setId("0", String.valueOf(i + tabs.size()));
          g.setLevelNbr(2);
          g.setActive(true);
          g.setRefClassId(T42GuideIndex.findByKey("#DG-SEEDO#"));
          g.setRowType("AFAR");
          g.setAfarKey(h.getSlug());
          if (h.getCategories().get(0).equals("Do")) {
            g.setClassId(T42GuideIndex.findByKey("#DG-SEEDO-SITESEE#"));
          } else {
            g.setClassId(T42GuideIndex.findByKey("#DG-SEEDO-SHOP#"));
          }
          g.setContent(h.getTitle() + " - " + h.getDescription());
          guides.add(g);
        } else if (h.getCategories().get(0).equals("Eat") || h.getCategories().get(0).equals("Drink")) {
          T42Guide g = new T42Guide();
          g.setId("0", String.valueOf(i + tabs.size()));
          g.setLevelNbr(2);
          g.setActive(true);
          g.setRefClassId(T42GuideIndex.findByKey("#DG-DINE#"));
          g.setClassId(T42GuideIndex.findByKey("#DG-DINE-DINEOVW#"));
          g.setRowType("AFAR");
          g.setAfarKey(h.getSlug());
          g.setContent(h.getTitle() + " - " + h.getDescription());
          guides.add(g);
        } else if (h.getCategories().get(0).equals("Stay")) {
          T42Hotel hotel = new T42Hotel();
          hotel.setHotelName(h.getTitle());
          hotel.setActive(true);
          hotel.setStarRating(-1);
          hotel.setHotelId(h.getSlug());
          hotel.setGeoPlaceKey(i + tabs.size());
          /*hotel.setLatitude(h.getLat());
          hotel.setLongitude(h.getLng());*/
          hotel.setGeoDisplay(aCity.getAfarCityId());
          hotel.setAfarKey(h.getSlug());
          hotel.setStarReview(aCity.getAfarCityId() + " - " + h.getDescription());
          hotels.add(hotel);
        } else if (h.getCategories().get(0).equals("Essential")) {
          T42Guide g = new T42Guide();
          g.setId("0", String.valueOf(i + tabs.size()));
          g.setLevelNbr(2);
          g.setActive(true);
          g.setRefClassId(T42GuideIndex.findByKey("#DG-OVER#"));
          g.setClassId(T42GuideIndex.findByKey("#DG-OVER-INTRO#"));
          g.setRowType("AFAR");
          g.setAfarKey(aCity.getAfarCityId().toLowerCase().replace(" ", "-") + "-" + h.getSlug());
          g.setContent(h.getTitle() + " - " + h.getDescription());
          guides.add(g);
        }
      }
    }

    view.addGuides(guides);
    view.addHotels(hotels);

    return ok(views.html.guide.t42.destinationGuides.render(view));
  }

  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result afarAddSelectedGuides(String destId) {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Destination destination = Destination.find.byId(destId);
    if (destination == null || !SecurityMgr.canAccessDestination(destination, sessionMgr)) {
      return unauthorized();
    }

    JsonNode       jsonRequest = request().body().asJson();
    T42SelectedGuides guides   = Json.fromJson(jsonRequest, T42SelectedGuides.class);
    if(guides.destinations != null) {
      for (Integer t42DestId : guides.destinations.keySet()) {
        Map<String, List<Integer>> guideTypes = guides.destinations.get(t42DestId);
        T42Destination             t42Dest    = T42Destination.find.byId(t42DestId);

        for (String type : guideTypes.keySet()) {
          List<Integer> guideSeqNums = guideTypes.get(type);
          //t42Guides2Page(destination, t42Dest, guideSeqNums, type, sessionMgr.getUserId());
        }
      }
    }

    sw.stop();
    Log.debug("Added T42 Guides in: " + sw.getTime() + "ms");
    return ok();
  }

  @With({Authenticated.class, Authorized.class})
  public Result t42DestinationSearch(String term) {
    List<T42Destination> destinations = T42Destination.findByTerm(term);
    Log.debug("Searching T42 for:" + term);
    return ok(views.html.guide.t42.searchResults.render(destinations));
  }

  @With({Authenticated.class, Authorized.class})
  public Result t42DestinationGuides(Integer geoPlaceKey) {
    StopWatch sw = new StopWatch();
    sw.start();

    T42DestinationGuidesView view = new T42DestinationGuidesView();

    view.dest = T42Destination.find.byId(geoPlaceKey);
    if(view.dest== null) {
      return badRequest();
    }

    List<T42Guide> guides = T42Guide.findByPlaceKey(geoPlaceKey);
    view.addGuides(guides);

    List<T42Hotel> hotels = T42Hotel.findStarredByPlaceKey(geoPlaceKey);
    view.addHotels(hotels);

    sw.stop();
    Log.debug("Prepared T42 Guides in: " + sw.getTime() + "ms for" + view.dest.getDisplay());
    return ok(views.html.guide.t42.destinationGuides.render(view));
  }

  public static class T42SelectedGuides{
    public  Map<Integer, Map<String, List<Integer>>> destinations;
    public  List<String> hotels;
    public  List<String> afar;
  }

  /**
   * Eating JSON for lunch to convert into guides
   * @return
   */
  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result t42AddSelectedGuides(String destId) {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Destination destination = Destination.find.byId(destId);
    if (destination == null || !SecurityMgr.canAccessDestination(destination, sessionMgr)) {
      return unauthorized();
    }

    JsonNode       jsonRequest = request().body().asJson();
    T42SelectedGuides guides   = Json.fromJson(jsonRequest, T42SelectedGuides.class);
    if(guides.destinations != null) {
      for (Integer t42DestId : guides.destinations.keySet()) {
        Map<String, List<Integer>> guideTypes = guides.destinations.get(t42DestId);
        T42Destination             t42Dest    = T42Destination.find.byId(t42DestId);

        for (String type : guideTypes.keySet()) {
          List<Integer> guideSeqNums = guideTypes.get(type);
          t42Guides2Page(destination, t42Dest, guideSeqNums, type, sessionMgr.getUserId());
        }
      }
    }

    if(guides.hotels != null) {
      for(String hotelId: guides.hotels) {
        T42Hotel hotel = T42Hotel.findByKey(hotelId);
        t42Hotel2Page(destination, hotel, sessionMgr.getUserId());
      }
    }

    sw.stop();
    Log.debug("Added T42 Guides in: " + sw.getTime() + "ms");
    return ok();
  }

  public static void t42Hotel2Page(Destination dest,
                                   T42Hotel hotel,
                                   String userId) {
    if(hotel == null) {
      return;
    }

    StringBuilder sb = new StringBuilder();
    sb.append("<h1>")
      .append(hotel.getHotelName())
      .append("</h1>")
      .append(System.lineSeparator())
      .append("<!-- Travel42 Hotel Block ID: ")
      .append(hotel.getHotelId())
      .append(" -->")
      .append(System.lineSeparator())
      .append(hotel.getStarReview());


    DestinationGuide dg = DestinationGuide.buildGuide(dest, userId);
    dg.setName(hotel.getHotelName() + " (" + hotel.getGeoDisplay() + ")");
    dg.setIntro(sb.toString());
    dg.setLoclat(hotel.getLatitude());
    dg.setLoclong(hotel.getLongitude());
    dg.setTag("Travel42");
    int currRows =  DestinationGuide.maxPageRankByDestId(dest.destinationid);
    dg.setRank(++currRows);
    dg.save();
  }



  public static void t42Guides2Page(Destination dest,
                                     T42Destination td,
                                     List<Integer> seqNumbers,
                                     String rootGuideIndex,
                                     String userId) {

    StringBuilder sb = new StringBuilder();
    T42GuideIndex gi = T42GuideIndex.findByKey("#" + rootGuideIndex + "#");
    if(gi != null) {
      sb.append("<h1>").append(gi.getClassName()).append("</h1>\n");
    }

    int infoBlockCount = 0;
    for (Integer seq : seqNumbers) {
      T42Guide guide = T42Guide.findByKey(td.getGeoPlaceKey(), seq);
      if (guide == null) {
        continue;
      }
      switch (guide.getRowType()) {
        case "POI":
          if(guide.getPoiPlaceKey() != null)
          t42PoiGuideToPage(dest, td, guide, userId);
          break;
        default:
          infoBlockCount++;
          sb.append("<!-- Travel42 Guide Block Destination ID: ");
          sb.append(td.getGeoPlaceKey());
          sb.append(" Sequence Number: ");
          sb.append(seq);
          sb.append(" -->");
          sb.append(t42GuideToPageBlock(guide));
      }
    }
    if (infoBlockCount != 0) {
      T42Guide         rootGuide = T42Guide.rootLevelGuide(td.getGeoPlaceKey(), gi.getClassId());
      DestinationGuide dg        = buildGuide(dest, userId);
      dg.setName(td.getDisplay() + " " + gi.getClassName());
      dg.setIntro((dest.getDescription() != null) ? dest.getDescription() : "");
      dg.setDescription(sb.toString());
      if (td.getLatitude() != null) {
        dg.setLoclat(td.getLatitude());
      }
      if (td.getLongitude() != null) {
        dg.setLoclong(td.getLongitude());
      }
      dg.setTag("Travel42");
      fillDestinationGuide(dg, td);
      dg.setRank(rootGuide.getId().getSequenceNbr());
    /*
    switch (gi.getClassName()) {
      case "Dining":
        dg.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_FOOD);
        break;
      case "See & Do":
        dg.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_SIGHTS);
        break;
      case "Events":
        dg.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_GOINGS_ON);
        break;
      case "Facts":
      case "BEtiquette":
      case "Hotels":
      case "Intelliguide":
      case "Overview":
      case "Security":
      default:
        dg.setRecommendationtype(APPConstants.c);
        break;
    }
    */
      dg.save();


      //Lookup T42 image
      List<T42DestinationImage> images = T42DestinationImage.byGeoPlaceKey(td.getGeoPlaceKey());
      for(T42DestinationImage tdi : images){
        FileImage fi = tdi.getFileImage();
        DestinationGuideAttach tna = DestinationGuideAttach.buildGuideAttachment(dg, userId);
        tna.setFileImage(fi);
        tna.setName(fi.getFile().getFilename());
        tna.setComments(fi.getFile().getDescription());
        tna.setAttachtype(PageAttachType.PHOTO_LINK);
        tna.setAttachname(fi.getFile().getFilename());
        tna.setAttachurl(fi.getUrl());
        tna.setRank(tdi.getRank());
        tna.save();
      }
    }
  }

  public static String getT42PoiDescription(T42Guide guide, T42Poi p) {
    StringBuilder sb = new StringBuilder();
    sb.append(guide.getContent());
    sb.append("<br/>");
    if(p.getHrsOper() != null && p.getHrsOper().length() > 0) {
      sb.append("<h2>Hours of Operation</h2><br/>\n").append(p.getHrsOper());
    }

    if(p.getFeesDetail() != null && p.getFeesDetail().length() > 0) {
      sb.append("<h2>Fees</h2><br/>\n").append(p.getFeesDetail());
    }

    if(p.getPmtAccept() != null && p.getPmtAccept().length() > 0) {
      sb.append("<h2>Payment Details</h2><br/>\n").append(p.getPmtAccept());

      if(p.getPmtAcceptDetail() != null && p.getPmtAcceptDetail().length() > 0) {
        sb.append("<br/>").append(p.getPmtAcceptDetail());
      }
    }

    if(p.getResvPolicy() != null && p.getResvPolicy().length() > 0) {
      sb.append("<h2>Reservation Policy</h2><br/>\n").append(p.getResvPolicy());
    }
    return sb.toString();
  }


  /**
   * Convert a single T42 POI Guide to Page
   *
   * @param guide
   */
  private static void t42PoiGuideToPage(Destination dest, T42Destination td, T42Guide guide, String userId) {
    DestinationGuide dg = buildGuide(dest, userId);
    T42Poi p = T42Poi.find.byId(guide.getPoiPlaceKey().getPoiPlaceKey());
   // T42Poi p = guide.getPoiPlaceKey(); //For some reason lazy fetching in eBean does not work

    if(p.getLatitude() != null) {
      dg.setLoclat(p.getLatitude());
    }
    if(p.getLongitude() != null) {
      dg.setLoclong(p.getLongitude());
    }
    dg.setTag("Travel42");

    dg.setIntro((p.getShortDesc()!=null)?p.getShortDesc():"");

    dg.setDescription(getT42PoiDescription(guide, p));

    dg.setStreetaddr1(p.getAddress1() + " " + p.getAddress2());
    dg.setZipcode(p.getZipCode());

    fillDestinationGuide(dg, td);

    dg.setName(p.getPoiName());
    dg.setRank(guide.getId().getSequenceNbr());

    switch (guide.getClassId().getTopClassName()) {
      case "Dining":
        dg.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_FOOD);
        break;
      case "See & Do":
        switch (guide.getClassId().getRefClassName()) {
          case "Shopping":
            dg.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_SHOPPING);
            break;
          case "Sightseeing":
            dg.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_SIGHTS);
            break;
          case "Spectator Sports":
          case "Performing Arts":
          case "Nightlife":
            dg.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_GOINGS_ON);
            break;
          case "Recreation":
          case "See & Do":
          case "Itinerary":
            dg.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_OTHER);
            break;
          default:
            dg.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_SIGHTS);
        }
        break;
      default:
        dg.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_UNDEFINED);
        break;
    }

    /* DestinationGuide Fields Not Used
  public Long destguidetimestamp;
  public String templateid;
  public String landmark;
     */
    dg.save();
  }


  private static void fillDestinationGuide(DestinationGuide dg, T42Destination td) {
    switch (td.getType()) {
      case "City":
        dg.setCity(td.getName());
        if(td.getParentGeoPlaceKey() != null) {
          T42Destination parent = T42Destination.find.byId(td.getParentGeoPlaceKey());
          if (parent.getType().equals("State")) {
            dg.setState(parent.getName());
          }
        }
      case "State":
      case "Country":
      default:
        T42Destination country = T42Destination.findCountryByCode(td.getISOCode());
        dg.setCountry(country.getName());
        break;
    }
  }

  public static String t42GuideToPageBlock(T42Guide guide) {
    String headline;
    switch(guide.getRowType()) {
      case "POI":
        headline = guide.getClassId().getClassName() + " > " + guide.getPoiPlaceKey().getPoiName();
        break;
      case "FACT":
        headline = guide.getClassId().getClassName();
        break;
      case "HIER":
        headline = guide.getClassId().getClassName();
        break;
      case "GEO":
        headline = guide.getClassId().getClassName();
        break;
      case "EVENT":
        headline = guide.getClassId().getClassName();
        break;
      default:
        headline = "";
        Log.err("Unknown T42 row type: " + guide.getRowType() + " for place: " + guide.getId().getGeoPlaceKey() +
                " seqNum: " + guide.getId().getSequenceNbr());
    }

    StringBuilder wrapHtml = new StringBuilder();
    wrapHtml
        .append("<div t42dest='")
        .append(guide.getId().getGeoPlaceKey())
        .append("' t42seq='")
        .append(guide.getId().getSequenceNbr())
        .append("'>")
        .append("<h1>")
        .append(headline)
        .append("</h1>")
        .append(guide.getContent())
        .append("</div>");
    return wrapHtml.toString();
  }

}
