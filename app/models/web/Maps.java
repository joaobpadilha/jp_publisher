package models.web;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-25
 * Time: 10:32 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "maps")
public class Maps
    extends Model {
  public static Model.Finder<String, Maps> find = new Finder(Maps.class);
  public String name;
  public String note;
  public String date;
  public long   status;
  public long   lastupdatetimestamp;
  public long   createtimestamp;
  public String createdby;
  public String modifiedby;
  public String maptype;
  public String startdate;
  public String enddate;
  public long   synctimestamp;
  public String tag;
  @Id
  public String pk;
}
