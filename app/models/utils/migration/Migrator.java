package models.utils.migration;

/**
 * Migrator for an entity.  This is migration is a mutator that changes the
 * entity directly as it is likely invoked during PostLoad from the entity
 * instanace itself
 * 
 * @author wei
 *
 * @param <T>
 */
public interface Migrator<T> {
  /**
   * 
   * @param entity
   * @return true if the migration operation is applied
   *         false if it doesn't need to be migrated
   */
  boolean migrate(T entity);
}
