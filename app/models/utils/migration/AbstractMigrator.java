package models.utils.migration;

abstract public class AbstractMigrator<T> implements Migrator<T> {

  @Override
  public boolean migrate(T entity) {
    if (isMigrated(entity)) {
      return false;
    }
    else {
      return doMigrate(entity);
    }
  }

  /**
   * Migration logic implementation
   * @param entity
   * @return
   */
  abstract protected  boolean doMigrate(T entity);
  
  abstract protected boolean isMigrated(T entity);
}
