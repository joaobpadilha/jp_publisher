package models.utils;

import org.apache.poi.ss.formula.functions.T;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;

import models.utils.migration.Migrator;

/**
 * Factory class for persistence related Util class
 * This works with PersistenceUtilModule for any Dependency Injection
 * 
 * This can be used outside of play application so that it can be tested easily
 * 
 * @author wei
 *
 */
public class PersistenceUtilFactory {
  private static Injector injector = Guice.createInjector(new PersistenceUtilModule());
  
  @SuppressWarnings("hiding")
  static public <T> T get(Class<T> type) {
    return injector.getInstance(type);
  }
  
  static public Migrator<?> getMigrator(Class<?> entityClass) {
    return injector.getInstance(Key.get(Migrator.class, Names.named(entityClass.getName())));
  }
}
