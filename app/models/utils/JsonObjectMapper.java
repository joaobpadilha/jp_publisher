package models.utils;

import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Provide a central mapper for Json object marshall/unmarshall for models
 * Although EBean has internal support via Jackson for json marshal/unmarshal,
 * we use our own to have better control on the configuration is needed
 * 
 * Note: could consider configure the Jackson ObjectMapper via Ebean's ServerConfig
 * @author wei
 *
 */
public interface JsonObjectMapper {
  /**
   * Unmarshall a json tree to a sepcific class
   * @param json
   * @param type
   * @return
   */
  public <T> T unmarshal(JsonNode json, Class<T> type);
  
  /**
   * Unmarshall a json tree to a sepcific class
   * @param json
   * @param type
   * @return
   */
  public <T> T unmarshal(String json, Class<T> type);
  
  /**
   * Unmarshall a json tree to a sepcific class
   * @param json
   * @param type
   * @return
   */
  public <T> T unmarshal(Map<String, Object> jsonTree, Class<T> type);
  
  /**
   * Marshall an object to String
   * @param value
   * @return
   */
  public String marshalToString(Object value);
  
  /**
   * Marshall an object to json node
   * @param value
   * @return
   */
  public JsonNode marshal(Object value);
  
  /**
   * Marshall an object to json node
   * @param value
   * @return
   */
  public Map<String, Object> marshalToMap(Object value);
}
