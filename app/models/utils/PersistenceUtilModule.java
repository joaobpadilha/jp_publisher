package models.utils;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

import models.publisher.TmpltDetails;
import models.publisher.TripDetail;
import models.publisher.utils.reservation.migration.TmpltDetailsMigrator;
import models.publisher.utils.reservation.migration.TripDetailMigrator;
import models.utils.migration.Migrator;

/**
 * The DI container for Persistence related module.
 * 
 * @author wei
 *
 */
public class PersistenceUtilModule extends AbstractModule {

  @Override
  protected void configure() {
    bind(JsonObjectMapper.class).to(JsonObjectMapperImpl.class);
    bind(Migrator.class).annotatedWith(Names.named(TripDetail.class.getName())).to(TripDetailMigrator.class);
    bind(Migrator.class).annotatedWith(Names.named(TmpltDetails.class.getName())).to(TmpltDetailsMigrator.class);
  }
}
