package models.utils;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Singleton;

@Singleton
public class JsonObjectMapperImpl implements JsonObjectMapper {
  private ObjectMapper om;
  
  public JsonObjectMapperImpl() {
    om = new ObjectMapper();
    om.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
  }

  @Override
  public <T> T unmarshal(JsonNode json, Class<T> type) {
    try {
      return om.treeToValue(json, type);
    }
    catch (JsonProcessingException e) {
      throw new JsonObjectMappingException(e);
    }
  }

  @Override
  public <T> T unmarshal(Map<String, Object> jsonTree, Class<T> type) {
      return om.convertValue(jsonTree, type);
  }
  
  @Override
  public JsonNode marshal(Object value) {
    return om.valueToTree(value);
  }

  @SuppressWarnings("unchecked")
  @Override
  public Map<String, Object> marshalToMap(Object value) {
    return om.convertValue(value, Map.class);
  }

  @Override
  public <T> T unmarshal(String json, Class<T> type) {
    try {
      return om.readValue(json, type);
    }
    catch (IOException e) {
      throw new JsonObjectMappingException(e);
    }   
  }

  @Override
  public String marshalToString(Object value) {
    try {
      return om.writeValueAsString(value);
    }
    catch (JsonProcessingException e) {
      throw new JsonObjectMappingException(e);
    }
  }
}
