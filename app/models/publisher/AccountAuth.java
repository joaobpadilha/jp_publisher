package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.SqlRow;
import com.avaje.ebean.annotation.DbJsonB;
import com.avaje.ebean.annotation.EnumValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.utils.Log;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 * Created by surge on 2016-02-24.
 */
@Entity
@Table(name = "account_auth")
public class AccountAuth
    extends Model {

  @Transient
  public static final ObjectMapper om;

  public enum AAuthType {
    /**
     * General User Password
     */
    @EnumValue("PWD")
    PASSWORD,
    /**
     * Json Web Token
     */
    @EnumValue("JWT")
    JWT,
    /**
     * Google OAuth
     */
    @EnumValue("GOO")
    GOOGLE,
    /**
     * Facebook
     */
    @EnumValue("FBK")
    FACEBOOK,
    /**
     * Twitter
     */
    @EnumValue("TWT")
    TWITTER
  }

  public static Model.Finder<Long, AccountAuth> find = new Finder(AccountAuth.class);

  static {
    om = new ObjectMapper();
    //Perform any kind of configuration for object mapper in this area
  }

  @Transient
  AuthMeta authMeta;
  @Id
  Long     pk;
  /**
   * Userid
   */
  Long uid;
  /**
   * 3-letter type with values like PWD, GOO, FBK, TWT, JWT
   */
  AAuthType authType;
  /**
   * Primary value for the authentication type
   */
  String    value;
  /**
   * Password salt
   */
  String    salt;
  /**
   * Here we will store additional parameters required for OAuth or JWT, etc.
   */
  @DbJsonB
  JsonNode meta;
  /**
   * Timestamp when authentication method expires
   */
  Long expire;
  /**
   * Number of successful authentication attempts for this entry (total)
   */
  Integer successCount;
  /**
   * Timestamp of the last successful login
   */
  Timestamp successLastTs;
  /**
   * Number of failed authentication attempts for this entry (total)
   */
  Integer   failCount;
  /**
   * Last failed login attempt timestamp
   */
  Timestamp failLastTs;
  /**
   * Moving average error rate (will be updated on each login)
   */
  Float     failRate;
  /**
   * Password reset token
   */
  String    resetToken;
  /**
   * Password reset token expiration
   */
  Long      resetExpire;
  Long      createdBy;
  Long      modifiedBy;
  Timestamp createdTs;
  Timestamp modifiedTs;
  @Version
  int version;

  public static class AuthMeta {

  }

  /**
   * @param userid userid who creates the record
   * @return
   */
  public static AccountAuth build(Long userid) {
    AccountAuth aa = new AccountAuth();
    aa.setPk(DBConnectionMgr.getUniqueLongId());
    aa.setCreatedTs(Timestamp.from(Instant.now()));
    aa.setModifiedTs(aa.createdTs);
    aa.setCreatedBy(userid);
    aa.setModifiedBy(userid);
    aa.setSuccessCount(0);
    aa.setFailCount(0);
    return aa;
  }

  public static AccountAuth findByResetToken(String token) {
    return find.where().eq("reset_token", token).findUnique();
  }

  /**
   * If someone inserts multiple password records for the same user this function will fail with an exception
   *
   * @param uid
   * @return
   */
  public static AccountAuth getPassword(Long uid) {
    return find.where().eq("uid", uid).eq("auth_type", AAuthType.PASSWORD).findUnique();
  }

  /**
   * Return record for a specific authentication type
   *
   * @param uid
   * @param type
   * @return
   */
  public static AccountAuth getByType(Long uid, AAuthType type) {
    return find.where().eq("uid", uid).eq("auth_type", type).findUnique();
  }

  public static List<AccountAuth> getAllByType(Long uid, AAuthType type) {
    return find.where().eq("uid", uid).eq("auth_type", type).findList();
  }

  public static List<AccountAuth> getAuthsForType(Long uid, AAuthType type) {
    return find.where().eq("uid", uid).eq("auth_type", type).findList();
  }

  public static Timestamp getLastSuccessLoginTs(String userId) {
    String sql = "select auth.success_last_ts "
                 +"from account_auth auth join account on account.uid=auth.uid and account.legacy_id = :userId "
                 +"where auth.auth_type='PWD'";

    try {
      SqlRow lastLogin = Ebean.createSqlQuery(sql).setParameter("userId", userId).findUnique();
      if (lastLogin != null) {
        // find one record
        return lastLogin.getTimestamp("success_last_ts");
      }
    } catch (PersistenceException ex) {
      Log.err("Shoud not return more than one row", ex);
    }
    return null;
  }
  public AuthMeta getAuthMeta() {
    return authMeta;
  }

  public AccountAuth setAuthMeta(AuthMeta authMeta) {
    this.authMeta = authMeta;
    return this;
  }

  public Long getPk() {
    return pk;
  }

  public AccountAuth setPk(Long pk) {
    this.pk = pk;
    return this;
  }

  public Long getUid() {
    return uid;
  }

  public AccountAuth setUid(Long uid) {
    this.uid = uid;
    return this;
  }

  public AAuthType getAuthType() {
    return authType;
  }

  public AccountAuth setAuthType(AAuthType authType) {
    this.authType = authType;
    return this;
  }

  public String getValue() {
    return value;
  }

  public AccountAuth setValue(String value) {
    this.value = value;
    return this;
  }

  public String getSalt() {
    return salt;
  }

  public AccountAuth setSalt(String salt) {
    this.salt = salt;
    return this;
  }

  public JsonNode getMeta() {
    return meta;
  }

  public AccountAuth setMeta(JsonNode meta) {
    this.meta = meta;
    return this;
  }

  public Long getExpire() {
    return expire;
  }

  public AccountAuth setExpire(Long expire) {
    this.expire = expire;
    return this;
  }

  public Timestamp getSuccessLastTs() {
    return successLastTs;
  }

  public AccountAuth setSuccessLastTs(Timestamp successLastTs) {
    this.successLastTs = successLastTs;
    return this;
  }

  public Integer getFailCount() {
    return failCount;
  }

  public AccountAuth setFailCount(Integer failCount) {
    this.failCount = failCount;
    return this;
  }

  public Timestamp getFailLastTs() {
    return failLastTs;
  }

  public AccountAuth setFailLastTs(Timestamp failLastTs) {
    this.failLastTs = failLastTs;
    return this;
  }

  public Float getFailRate() {
    return failRate;
  }

  public AccountAuth setFailRate(Float failRate) {
    this.failRate = failRate;
    return this;
  }

  public String getResetToken() {
    return resetToken;
  }

  public AccountAuth setResetToken(String resetToken) {
    this.resetToken = resetToken;
    return this;
  }

  public Long getResetExpire() {
    return resetExpire;
  }

  public AccountAuth setResetExpire(Long resetExpire) {
    this.resetExpire = resetExpire;
    return this;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public AccountAuth setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public AccountAuth setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
    return this;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public AccountAuth setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
    return this;
  }

  public Timestamp getModifiedTs() {
    return modifiedTs;
  }

  public AccountAuth setModifiedTs(Timestamp modifiedTs) {
    this.modifiedTs = modifiedTs;
    return this;
  }

  public int getVersion() {
    return version;
  }

  public AccountAuth setVersion(int version) {
    this.version = version;
    return this;
  }

  public void markModified(Long accountId) {
    setModifiedTs(Timestamp.from(Instant.now()));
    setModifiedBy(accountId);
  }

  public void clearPasswordReset() {
    setResetExpire(null);
    setResetToken(null);
    update();
  }

  @PrePersist
  public void prePersist() {
    switch (authType) {
      case PASSWORD:
      case JWT:
        return; //No need for JSON in password records
      default:
        meta = om.valueToTree(authMeta);
        break;
    }
  }

  @PostLoad
  public void postLoad() {
    switch (authType) {
      case PASSWORD:
      case JWT:
        return; //No need for JSON in password records
      default:
        break;
    }
    try {
      ObjectReader or = om.readerFor(AuthMeta.class);
      authMeta = or.treeToValue(meta, AuthMeta.class);
      //json = om.treeToValue(data, PoiJson.class);
    }
    catch (JsonProcessingException e) {
      Log.err("AuthMeta: Failed to parse JSON tree");
    }
  }

  public void updateOnSuccess() {
    setSuccessLastTs(Timestamp.from(Instant.now()));
    if (getSuccessCount() == null) {
      setSuccessCount(1);
    }
    else {
      setSuccessCount(getSuccessCount() + 1);
    }
    //reset failed count
    setFailCount(0);

    update();
  }

  public Integer getSuccessCount() {
    return successCount;
  }

  public AccountAuth setSuccessCount(Integer successCount) {
    this.successCount = successCount;
    return this;
  }
}
