package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.Update;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.List;

/**
 * Created by ryan on 29/08/17.
 */
@Entity @Table(name = "s3_img_index")

public class S3ImgIndex extends Model{

    public static Model.Finder<Long, S3ImgIndex> find = new Finder(S3ImgIndex.class);

    @Id
    private long pk;
    @Constraints.Required
    private String bucket;
    private String fileNameOrig;
    private String fileNameLower;
    private long modifiedTs;
    @Version
    private int version;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public long getPk() {
        return pk;
    }

    public void setPk(long pk) {
        this.pk = pk;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getFileNameOrig() {
        return fileNameOrig;
    }

    public void setFileNameOrig(String fileNameOrig) {
        this.fileNameOrig = fileNameOrig;
    }

    public String getFileNameLower() {
        return fileNameLower;
    }

    public void setFileNameLower(String fileNameLower) {
        this.fileNameLower = fileNameLower;
    }

    public static List<S3ImgIndex> findByBucket(String bucket) {
        return find.where().eq("bucket", bucket).findList();
    }

    public static List<S3ImgIndex> findByFilenameOrig(String bucket, String filenameorig) {
        return find.where().eq("bucket", bucket).eq("fileNameOrig", filenameorig).findList();
    }

    public static int deleteByTimestamp(String bucket, long timestamp) {
        String s = "DELETE FROM S3ImgIndex WHERE bucket =:b and modified_ts !=:ts";
        Update<S3ImgIndex> delete = Ebean.createUpdate(S3ImgIndex.class, s);

        delete.set("b", bucket.trim());
        delete.set("ts", timestamp);

        return delete.execute();
    }


    public static List<S3ImgIndex> findByFilenameLower(String bucket, String filenamelower) {
        return find.where().eq("bucket", bucket).eq("fileNameLower", filenamelower).findList();
    }

    public static List<S3ImgIndex> findByFilenameKeywords(String bucket, String[] keyword) {
        String terms = "";
        for(String s : keyword) {
            terms += "%" + s + "%";
        }
        return find.where().eq("bucket", bucket).contains("fileNameLower", terms).setMaxRows(50).findList();
    }

    public long getModifiedTs() {
        return modifiedTs;
    }

    public void setModifiedTs(long modifiedTs) {
        this.modifiedTs = modifiedTs;
    }
}
