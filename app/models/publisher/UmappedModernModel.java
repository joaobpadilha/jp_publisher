package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.persistence.IUmappedModel;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * Modern model has several common properties and utility methods
 * <p>
 * Created by surge on 2016-11-24.
 */
@MappedSuperclass
public abstract class UmappedModernModel
    extends Model
    implements IUmappedModel<Long, UmappedModernModel> {
  @Id
  Long pk;
  @CreatedTimestamp
  Timestamp createdTs;
  Long createdBy;
  Timestamp modifiedTs;
  Long      modifiedBy;
  @Version
  int version;

  @Override
  public String getPkAsString() {
    return pk.toString();
  }

  @Override
  public Long getCreatedByPk() {
    return getCreatedBy();
  }

  @Override
  public Long getModifiedByPk() {
    return getModifiedBy();
  }

  @Override
  public String getCreatedByLegacy() {
    Account a = Account.find.byId(getCreatedBy());
    return a.getLegacyId();
  }

  @Override
  public String getModifiedByLegacy() {
    Account a = Account.find.byId(getModifiedBy());
    return a.getLegacyId();
  }

  @Override
  public Long getCreatedEpoch() {
    return getCreatedTs().getTime();
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  @Override
  public Long getModifiedEpoch() {
    return getModifiedTs().getTime();
  }

  public Timestamp getModifiedTs() {
    return modifiedTs;
  }

  public UmappedModernModel setModifiedTs(Timestamp modifiedTs) {
    this.modifiedTs = modifiedTs;
    return this;
  }

  public UmappedModernModel setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
    return this;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public UmappedModernModel setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
    return this;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public UmappedModernModel setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public Long getPk() {
    return pk;
  }

  public UmappedModernModel setPk(Long pk) {
    this.pk = pk;
    return this;
  }

  /**
   * Sets up base fields.
   * Keep protected as it should be called from local builders only
   *
   * @param accountId
   * @return
   */
  UmappedModernModel configureBase(Long accountId) {
    setPk(DBConnectionMgr.getUniqueLongId());
    setCreatedTs(Timestamp.from(Instant.now()));
    setModifiedTs(createdTs);
    setCreatedBy(accountId);
    setModifiedBy(accountId);
    return this;
  }

  public UmappedModernModel markModified(Long accountId) {
    setModifiedBy(accountId);
    setModifiedTs(Timestamp.from(Instant.now()));
    return this;
  }

  public int getVersion() {
    return version;
  }

  public UmappedModernModel setVersion(int version) {
    this.version = version;
    return this;
  }
}
