package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by surge on 2016-11-24.
 */
@Entity
@Table(name = "reservation_package")
public class DbReservationPackage
    extends UmappedModernModel {

  public final static Model.Finder<Long, DbReservationPackage> find = new Finder(DbReservationPackage.class);


  @Override
  public UmappedModernModel getByPk(Long pk) {
    return find.byId(pk);
  }
}
