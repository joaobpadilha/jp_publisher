package models.publisher;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.publisher.api.msg.EmailJson;
import com.mapped.publisher.utils.Log;
import play.data.validation.Constraints;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-24
 * Time: 9:09 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity @Table(name = "Email_Audit")
public class EmailAudit
    extends Model {
  public static Model.Finder<String, EmailAudit> find = new Finder(EmailAudit.class);
  @Id
  public String pk;
  @Constraints.Required
  public String fromemail;
  public String toemail;
  public String subject;
  public String json;
  public String body;
  public int numattachments;
  public int status;
  public Long createdtimestamp;
  @Version
  public int version;

  @Transient
  private EmailJson emailJsonCache = null;

  public EmailJson getEmailJson() {
    if (this.json == null) {
      return null;
    }
    if (emailJsonCache != null) {
      return emailJsonCache;
    }

    ObjectMapper m = new ObjectMapper();

    try {
      emailJsonCache = m.readValue(this.json, EmailJson.class);
    }
    catch (Exception je) {
      Log.err("Failed to unmarshal EmailJson in EmailAudit model from JSON:", je);
    }

    return emailJsonCache;
  }

  public void setEmailJson(EmailJson emailJson) {
    ObjectMapper m = new ObjectMapper();
    m.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    //Needs this black magic to avoid picking up methods that looks like real fields
    m.setVisibility(m.getSerializationConfig()
                     .getDefaultVisibilityChecker()
                     .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                     .withIsGetterVisibility(JsonAutoDetect.Visibility.NONE));
    try {
      //result = m.writerWithDefaultPrettyPrinter().writeValueAsString(idw);
      String newJson = m.writeValueAsString(emailJson);
      this.setJson(newJson);
      emailJsonCache = emailJson;
    }
    catch (JsonProcessingException jpe) {
      Log.err("Failed to generate EmailJson JSON text data:", jpe);
    }
  }


  public String getPk() {
    return pk;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public String getFromemail() {
    return fromemail;
  }

  public void setFromemail(String fromemail) {
    this.fromemail = fromemail;
  }

  public String getToemail() {
    return toemail;
  }

  public void setToemail(String toemail) {
    this.toemail = toemail;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getJson() {
    return json;
  }

  public void setJson(String json) {
    this.json = json;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public int getNumattachments() {
    return numattachments;
  }

  public void setNumattachments(int numattachments) {
    this.numattachments = numattachments;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}

