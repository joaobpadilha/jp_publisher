package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.EnumValue;
import com.mapped.publisher.utils.S3Util;
import com.mapped.publisher.view.ImageView;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 * Metadata for files of type Image
 * <p>
 * Created by surge on 2016-10-26.
 */
@Entity
@Table(name = "file_image")
public class FileImage
    extends Model {

  public final static Model.Finder<Long, FileImage> find = new Model.Finder(FileImage.class);

  public enum ImageType {
    @EnumValue("ORG")
    ORIGINAL,
    @EnumValue("OPT")
    OPTIMIZED,
    @EnumValue("SIZ")
    RESIZED,
    @EnumValue("WCR")
    WIDE_CROP_16x9,
    @EnumValue("SQR")
    SQUARE,
    @EnumValue("THM")
    THUMBNAIL;
    public String getShort() {
      switch (this) {
        case ORIGINAL:
          return "ORG";
        case OPTIMIZED:
          return "OPT";
        case RESIZED:
          return "SIZ";
        case SQUARE:
          return "SQR";
        case WIDE_CROP_16x9:
          return "WCR";
        case THUMBNAIL:
          return "LCK";
        default:
          return "UNK";
      }
    }
  }

  public enum Orientation {
    @EnumValue("LND")
    LANDSCAPE,
    @EnumValue("PRT")
    PORTRAIT,
    @EnumValue("SQR")
    SQUARE,
    @EnumValue("UNK")
    UNKNOWN;

    public String getShort() {
      switch (this) {
        case LANDSCAPE:
          return "LND";
        case PORTRAIT:
          return "PRT";
        case SQUARE:
          return "SQR";
        default:
          return "UNK";
      }
    }

    /**
     * Returns orientation, with square being returned, if dimensions difference is less than 5%.
     * @param width width of the image
     * @param height height of the image
     * @return
     */
    public static Orientation getOrientation(int width, int height) {
      if( width == 0 || height == 0) {
        return UNKNOWN;
      }
      float ratio = (float)width/(float)height;
      if(ratio > 1.05) {
        return LANDSCAPE;
      }
      if(ratio < 0.95 ) {
        return PORTRAIT;
      }
      return SQUARE;
    }
  }

  @Id
  Long     pk;
  /**
   * This Image Metadata belongs to this phisical file
   */
  @OneToOne
  @JoinColumn(name = "file_pk", nullable = true, referencedColumnName = "pk")
  FileInfo file;
  /**
   * If this image is derived from the original - this links to the original image
   */
  @ManyToOne
  @JoinColumn(name = "parent_pk", nullable = true, referencedColumnName = "pk")
  FileInfo parent;
  /**
   * Image Width in Pixels
   */
  Integer     width;
  /**
   * Image Height in Pixels
   */
  Integer     height;
  /**
   * Flag to indicate if the image is public (i.e. available to be found)
   */
  boolean     isPublic;
  /**
   * If image was downloaded, this is where source url goes
   */
  String      srcUrl;
  /**
   * This is "just in case" field, it will    store legacy url of the old upload/web download location
   */
  String      legacyUrl;
  /**
   * Flags if the image was processed (optimized and resized)
   */
  boolean     isProcessed;
  /**
   * Flag to indicate that user wanted to hide this image (if image is private)
   */
  boolean     hidden;
  /**
   * Search term (either if downloaded via search or description obtained from services like Google Image Recognition)
   */
  String      searchTerm;
  /**
   * 3-letter abriviation of the type: ORG - ORiGinal, OPT - OPTimized (size and compression), SIZ - reSIZe, WCR - Wide
   * CRop (16:9), SCR - Square CRop, THM - square THuMbnail
   */
  ImageType   type;
  /**
   * 3-letter abriviation of the orientation: LND - LaNDscape, PRT - PoRtraighT, SQR - Square
   */
  Orientation orientation;
  /**
   * Image license type
   */
  String      license;
  /**
   * Image attribution name (i.e. Serguei Moutovkin)
   */
  String      attrName;
  /**
   * Image attribution url (for example http://flickr.com/mutovkin)
   */
  String      attrUrl;
  /**
   * When image was modified (in case image is re-generated)
   */
  Timestamp   modifiedTs;
  /**
   * Foreign key on account (in case image is re-cropped/re-generated)
   */
  Long        modifiedBy;
  @Version
  int version;

  public ImageView toImageView(){
    ImageView iv = new ImageView();
    iv.id = getPk();
    iv.url = getUrl();
    iv.imgSrc = getSrcUrl();
    iv.note = getFile().getDescription();
    iv.fileName = getFile().getFilename();
    iv.width = getWidth();
    iv.height = getHeight();
    iv.cropped = (getType() == ImageType.WIDE_CROP_16x9);
    return iv;
  }

  public static List<FileImage> byTypeOfParent(Long parentId, ImageType type) {
    return find.where().eq("parent_pk", parentId).eq("type", type.getShort()).findList();
  }

  public static FileImage build(FileInfo master, Account by) {
    FileImage fi = new FileImage();
    fi.setFile(master);
    fi.markModified(by);
    return fi;
  }

  /**
   * Build with all required objects built as well
   * @param by
   * @return
   */
  public static FileImage build(Account by, FileSrc src, int expireDays) {
    FileInfo file = FileInfo.buildFileInfo(by, src, expireDays);
    FileImage image = new FileImage();

    image.setFile(file);
    image.setModifiedBy(by);
    image.setModifiedTs(file.getCreatedTs());

    return image;
  }

  public static FileImage buildPending(Account by, FileSrc src) {
    FileInfo file = FileInfo.buildPending(by.getUid(), src);
    return build(file, by);
  }

  public static FileImage bySourceUrl(final String url){
    return find.where().eq("src_url", url).eq("type", ImageType.ORIGINAL.getShort()).setMaxRows(1).findUnique();
  }

  /**
   * Saves all models
   * @return
   */
  public FileImage superSave() {
    file.save();
    if (parent != null) {
      parent.update();
    }
    save();
    return this;
  }

  public FileImage markModified(Account by) {
    this.setModifiedTs(Timestamp.from(Instant.now()));
    this.setModifiedBy(by);
    return this;
  }

  public FileImage setModifiedBy(Account modifiedBy) {
    this.modifiedBy = modifiedBy.getUid();
    return this;
  }

  public Long getPk() {
    return pk;
  }

  public FileImage setPk(Long pk) {
    this.pk = pk;
    return this;
  }

  public FileInfo getFile() {
    return file;
  }

  public String getFilename(){
    return getFile().getFilename();
  }

  public String getFilenameNormalized() {
    return S3Util.normalizeFilename(getFile().getFilename());
  }

  public FileImage setFile(FileInfo file) {
    this.setPk(file.getPk());
    this.file = file;
    return this;
  }

  @Transient
  public String getUrl() {
    return getFile().getUrl();
  }

  public String getLegacyUrl() {
    return legacyUrl;
  }

  public FileImage setLegacyUrl(String legacyUrl) {
    this.legacyUrl = legacyUrl;
    return this;
  }

  public String getSearchTerm() {
    return searchTerm;
  }

  public FileImage setSearchTerm(String searchTerm) {
    this.searchTerm = searchTerm;
    return this;
  }

  public boolean isHidden() {
    return hidden;
  }

  public FileImage setHidden(boolean hidden) {
    this.hidden = hidden;
    return this;
  }

  public FileInfo getParent() {
    return parent;
  }

  public FileImage setParent(FileInfo parent) {
    this.parent = parent;
    return this;
  }

  public Integer getWidth() {
    return width;
  }

  public FileImage setWidth(Integer width) {
    this.width = width;
    return this;
  }

  public Integer getHeight() {
    return height;
  }

  public FileImage setHeight(Integer height) {
    this.height = height;
    return this;
  }

  public boolean isPublic() {
    return isPublic;
  }

  public FileImage setPublic(boolean aPublic) {
    isPublic = aPublic;
    return this;
  }

  public String getSrcUrl() {
    return srcUrl;
  }

  public FileImage setSrcUrl(String srcUrl) {
    this.srcUrl = srcUrl;
    return this;
  }

  public boolean isProcessed() {
    return isProcessed;
  }

  public FileImage setProcessed(boolean processed) {
    isProcessed = processed;
    return this;
  }

  public ImageType getType() {
    return type;
  }

  public FileImage setType(ImageType type) {
    this.type = type;
    return this;
  }

  public Orientation getOrientation() {
    return orientation;
  }

  public FileImage setOrientation(Orientation orientation) {
    this.orientation = orientation;
    return this;
  }

  public String getLicense() {
    return license;
  }

  public FileImage setLicense(String license) {
    this.license = license;
    return this;
  }

  public String getAttrName() {
    return attrName;
  }

  public FileImage setAttrName(String attrName) {
    this.attrName = attrName;
    return this;
  }

  public String getAttrUrl() {
    return attrUrl;
  }

  public FileImage setAttrUrl(String attrUrl) {
    this.attrUrl = attrUrl;
    return this;
  }

  public Timestamp getModifiedTs() {
    return modifiedTs;
  }

  public FileImage setModifiedTs(Timestamp modifiedTs) {
    this.modifiedTs = modifiedTs;
    return this;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public FileImage setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
    return this;
  }

  public int getVersion() {
    return version;
  }

  public FileImage setVersion(int version) {
    this.version = version;
    return this;
  }
}
