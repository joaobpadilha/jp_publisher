package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import com.mapped.publisher.common.Credentials;
import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-20
 * Time: 3:15 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity @Table(name = "cmpy_api_parser")
public class CmpyApiParser
    extends Model {
  public static Model.Finder<String, CmpyApiParser> find = new Finder(CmpyApiParser.class);
  public String params; //;; separated name value pair e.g. param1=value 1, param2=value 222, param3 = value 3132323
  @Id
  public String pk;

  @Constraints.Required
  public String cmpyid;
  public String name;
  public String filepattern;
  public String parser;
  @Enumerated(value = EnumType.ORDINAL)
  public ParserType parsertype;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @Version
  public int version;

  public static enum ParserType {
    ITINERARY,
    BOOKINGS
  }

  public static List<CmpyApiParser> findByCmpy(String cmpyid) {
    return find.where().eq("cmpyid", cmpyid).findList();
  }

  public static List<CmpyApiParser> findAllUserAPIs(Credentials credentials) {
    if (!credentials.hasCompany()) {
      return new ArrayList<>();
    }
    return find.where().eq("cmpyid", credentials.getCmpyId()).findList();
  }

  public static int mergeCmpy(String srcCmpyId, String targetCmpyId, String userId) {
    String s = "UPDATE cmpy_api_parser set cmpyid = :targetCmpyId, lastupdatedtimestamp = :timestamp, " +
               "" + "modifiedby = :userid  where cmpyid = :srcCmpyId";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("timestamp", System.currentTimeMillis());
    update.setParameter("userid", userId);
    update.setParameter("srcCmpyId", srcCmpyId);
    update.setParameter("targetCmpyId", targetCmpyId);

    return Ebean.execute(update);
  }

  public ParserType getParsertype() {
    return parsertype;
  }

  public void setParsertype(ParserType parsertype) {
    this.parsertype = parsertype;
  }

  public void setParsertype(int val) {
    switch(val) {
      case 0:
        this.setParsertype(ParserType.ITINERARY);
        break;
      case 1:
        this.setParsertype(ParserType.BOOKINGS);
        break;
    }
  }

  public String getParams() {
    return params;
  }

  public void setParams(String params) {
    this.params = params;
  }

  public String getPk() {
    return pk;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public String getCmpyid() {
    return cmpyid;
  }

  public void setCmpyid(String cmpyid) {
    this.cmpyid = cmpyid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFilepattern() {
    return filepattern;
  }

  public void setFilepattern(String filepattern) {
    this.filepattern = filepattern;
  }

  public String getParser() {
    return parser;
  }

  public void setParser(String parser) {
    this.parser = parser;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public Map<String, String> getParameters() {
    Map<String, String> parameters = new HashMap<String, String>();
    if (params != null) {

      String[] pairs = params.split(";;");
      if (pairs != null) {
        for (int i = 0; i < pairs.length; i++) {
          String[] values = pairs[i].split("=");
          if (values != null && values.length == 2) {
            String name = values[0].replace("\n", "");
            name = name.replace("\r", "");

            String val = values[1].replace("\n", "");
            val = val.replace("\r", "");

            parameters.put(name.trim(), val.trim());
          }
        }
      }
    }
    return parameters;
  }
}
