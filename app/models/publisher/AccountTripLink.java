package models.publisher;

import com.avaje.ebean.*;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.persistence.RecordStatus;
import controllers.AccountController;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import javax.persistence.Version;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 * Created by surge on 2016-05-04.
 */
@Entity
@Table(name = "account_trip_link")
public class AccountTripLink
    extends Model {

  public static Finder<AccountTripLink.ATLPk, AccountTripLink> find = new Finder<>(AccountTripLink.class);
  @Id
  AccountTripLink.ATLPk pk;
  /**
   * Note: that
   */
  @Enumerated
  AccountDetailLink.DetailLinkType linkType;
  String legacyId;
  String legacyGroupId;
  Timestamp createdTs;
  Long createdBy;
  Timestamp modifiedTs;
  Long modifiedBy;
  int clickCounter;
  Timestamp firstClickTs;
  Timestamp lastClickTs;
  @Version
  int version;

  @Embeddable
  public static class ATLPk {
    Long uid;
    String tripid;

    @Override
    public int hashCode() {
      return uid.hashCode() * tripid.hashCode();
    }

    @Override
    public boolean equals(Object o) {
      if (o == null) {
        return false;
      }
      if (!(o instanceof AccountTripLink.ATLPk)) {
        return false;
      }
      AccountTripLink.ATLPk ok = (AccountTripLink.ATLPk) o;
      return uid.equals(ok.uid) && tripid.equals(ok.tripid);
    }

    public Long getUid() {
      return uid;
    }

    public void setUid(Long uid) {
      this.uid = uid;
    }

    public String getTripid() {
      return tripid;
    }

    public void setTripid(String tripid) {
      this.tripid = tripid;
    }
  }

  public ATLPk getPk() {
    return pk;
  }

  public AccountTripLink setPk(ATLPk pk) {
    this.pk = pk;
    return this;
  }

  public AccountDetailLink.DetailLinkType getLinkType() {
    return linkType;
  }

  public AccountTripLink setLinkType(AccountDetailLink.DetailLinkType linkType) {
    this.linkType = linkType;
    return this;
  }

  public String getLegacyId() {
    return legacyId;
  }

  public AccountTripLink setLegacyId(String legacyId) {
    this.legacyId = legacyId;
    return this;
  }

  public String getLegacyGroupId() {
    return legacyGroupId;
  }

  public void setLegacyGroupId(String legacyGroupId) {
    this.legacyGroupId = legacyGroupId;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public AccountTripLink setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
    return this;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public AccountTripLink setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public Timestamp getModifiedTs() {
    return modifiedTs;
  }

  public AccountTripLink setModifiedTs(Timestamp modifiedTs) {
    this.modifiedTs = modifiedTs;
    return this;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public AccountTripLink setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
    return this;
  }

  public int getClickCounter() {
    return clickCounter;
  }

  public void setClickCounter(int clickCounter) {
    this.clickCounter = clickCounter;
  }

  public Timestamp getFirstClickTs() {
    return firstClickTs;
  }

  public void setFirstClickTs(Timestamp firstClickTs) {
    this.firstClickTs = firstClickTs;
  }

  public Timestamp getLastClickTs() {
    return lastClickTs;
  }

  public void setLastClickTs(Timestamp lastClickTs) {
    this.lastClickTs = lastClickTs;
  }

  public int getVersion() {
    return version;
  }

  public AccountTripLink setVersion(int version) {
    this.version = version;
    return this;
  }

  public static AccountTripLink findByLegacyId(String legacyId) {
    List<AccountTripLink> links = find.where().eq("legacy_id", legacyId).findList();
    if (links != null && links.size() == 1) {
      return links.get(0);
    }
    return null;
  }

  public static AccountTripLink findByLegacyId(String legacyId, String tripId) {
    List<AccountTripLink> links = find.where()
            .eq("legacy_id", legacyId)
            .eq("tripid", tripId)
            .findList();
    if (links != null && links.size() == 1) {
      return links.get(0);
    }
    return null;
  }

  public static AccountTripLink findByUid(Long uid, String tripId) {
    List<AccountTripLink> links = find.where()
            .eq("uid", uid)
            .eq("tripid", tripId)
            .findList();
    if (links != null && links.size() == 1) {
      return links.get(0);
    }
    return null;
  }

  public static List<AccountTripLink> findByTrip(String tripId) {
    return find.where().eq("tripid", tripId).findList();
  }

  public static List<AccountTripLink> findAllByUid(long uid) {
    return find.where().eq("uid", uid).findList();
  }

  public static List<AccountTripLink> findByTripNoGroup(String tripId) {
    return find.where().eq("tripid", tripId).isNull("legacy_group_id").findList();
  }

  public static List<AccountTripLink> findByTripGroup(String tripId, String legacyGroupId) {
    return find.where().eq("tripid", tripId).eq("legacy_group_id", legacyGroupId).findList();
  }

  public static List<AccountTripLink> findByEmailTrip(String email, String tripId) {
    String sql = "uid in" +
            "(select uid from account where email = ?)";

    Object[] params = {email.toLowerCase()};

    List<AccountTripLink> links = find.where()
            .eq("tripid", tripId)
            .raw(sql, params)
            .findList();

    return links;
  }

  public static AccountTripLink build(Long userid, Long travelerId, String legacyId, String legacyGroupId, String tripId, AccountDetailLink.DetailLinkType linkType) {
    AccountTripLink a = new AccountTripLink();
    ATLPk pk = new ATLPk();
    pk.setTripid(tripId);
    pk.setUid(travelerId);
    a.setPk(pk);
    a.setLinkType(linkType);
    a.setLegacyId(legacyId);
    a.setLegacyGroupId(legacyGroupId);
    a.setCreatedTs(Timestamp.from(Instant.now()));
    a.setModifiedTs(a.createdTs);
    a.setCreatedBy(userid);
    a.setModifiedBy(userid);
    return a;
  }

  public static List<AccountTripLink> passengersForAlert(long alertId) {
    String sql = "tripid IN " +
            "      (SELECT tripid " +
            "       FROM trip_detail " +
            "       WHERE detailsid IN " +
            "              (SELECT details_id" +
            "               FROM flight_alert_booking" +
            "               WHERE flight_alert_id = ?) " +
            "             AND status = 0 ) ";

    Object[] params = {alertId};

    return find.where()
            .raw(sql, params)
            .findList();
  }

}
