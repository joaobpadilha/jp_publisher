package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbJsonB;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.mapped.publisher.persistence.RecordState;
import com.mapped.publisher.utils.Log;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.UmReservation;

import org.postgis.Point;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Reservation record
 * Created by surge on 2016-11-23.
 */
@Entity
@Table(name = "reservation")
public class DbReservation
    extends UmappedModernModel {
  public final static Model.Finder<Long, DbReservation> find = new Finder(DbReservation.class);
  @Transient
  public static final ObjectMapper         om;

  static {
    om = new ObjectMapper();
    //Perform any kind of configuration for object mapper in this area
    om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    //Pretty-print for debugging
    om.enable(SerializationFeature.INDENT_OUTPUT);


  }

  /**
   * What trip/template/proposal owns this reservation
   */
  @ManyToOne
  @Column(name = "package_pk")
  @JoinColumn(name = "reservation_pk", nullable = true, referencedColumnName = "pk")
  private             DbReservationPackage reservationPackage;
  /**
   * Old style detail type
   */
  @Enumerated(value = EnumType.ORDINAL)
  private             ReservationType      type;
  /**
   * Reservation name
   */
  private             String               name;
  /**
   * Poi ID
   */
  private             Long                 poiId;
  /**
   * Numeric Company ID as in poi table
   */
  private Integer              poiCmpyId;
  /**
   * Poi ID
   */
  private Long                 locStartPoiId;
  /**
   * Numeric Company ID as in poi table
   */
  private Integer              locStartCmpyId;
  /**
   * Name override
   */
  private String               locStartName;
  /**
   * PostGIS point for this location
   */
  private Point                locStartPoint;
  /**
   * Use Timezone UTC for locations with unknown TZ
   */
  private Timestamp            locStartTs;
  /**
   * Explicit Timezone Name (Java compatible name)
   */
  private String               locStartTz;
  /**
   * Poi ID
   */
  private Long                 locFinishPoiId;
  /**
   * Numeric Company ID as in poi table
   */
  private Integer              locFinishCmpyId;
  /**
   * Name override
   */
  private String               locFinishName;
  /**
   * PostGIS point for this location
   */
  private Point                locFinishPoint;
  /**
   * Use Timezone UTC for locations with unknown TZ
   */
  private Timestamp            locFinishTs;
  /**
   * Explicit Timezone Name (Java compatible name)
   */
  private String               locFinishTz;
  @DbJsonB
  private JsonNode             data;
  private String               importSrc;
  private String               importSrcId;
  private Long                 importTs;
  private Long                 pkApiSrcId;
  /**
   * Legacy ID (trip_detail ID to be used only for migrated records)
   */
  private String               legacyId;
  /**
   * Standard Umapped Record State
   */
  private RecordState          state;
  /**
   * Next record in the sort order (Linked List style)
   */
  private Long                 next;

  @Transient
  private UmReservation jsonData;

  @PostLoad
  public void postLoad() {
    try {
      ObjectReader or = om.readerFor(UmReservation.class);
      jsonData = or.treeToValue(data, UmReservation.class);
    }
    catch (JsonProcessingException e) {
      Log.err("DbReservation: Failed to parse JSON tree");
    }
  }

  @PrePersist
  public void prePersist() {
    data = om.valueToTree(jsonData);
  }

  public DbReservationPackage getReservationPackage() {
    return reservationPackage;
  }

  public DbReservation setReservationPackage(DbReservationPackage reservationPackage) {
    this.reservationPackage = reservationPackage;
    return this;
  }

  public ReservationType getType() {
    return type;
  }

  public DbReservation setType(ReservationType type) {
    this.type = type;
    return this;
  }

  public String getName() {
    return name;
  }

  public DbReservation setName(String name) {
    this.name = name;
    return this;
  }

  public Long getPoiId() {
    return poiId;
  }

  public DbReservation setPoiId(Long poiId) {
    this.poiId = poiId;
    return this;
  }

  public Integer getPoiCmpyId() {
    return poiCmpyId;
  }

  public DbReservation setPoiCmpyId(Integer poiCmpyId) {
    this.poiCmpyId = poiCmpyId;
    return this;
  }

  public Long getLocStartPoiId() {
    return locStartPoiId;
  }

  public DbReservation setLocStartPoiId(Long locStartPoiId) {
    this.locStartPoiId = locStartPoiId;
    return this;
  }

  public Integer getLocStartCmpyId() {
    return locStartCmpyId;
  }

  public DbReservation setLocStartCmpyId(Integer locStartCmpyId) {
    this.locStartCmpyId = locStartCmpyId;
    return this;
  }

  public String getLocStartName() {
    return locStartName;
  }

  public DbReservation setLocStartName(String locStartName) {
    this.locStartName = locStartName;
    return this;
  }

  public Point getLocStartPoint() {
    return locStartPoint;
  }

  public DbReservation setLocStartPoint(Point locStartPoint) {
    this.locStartPoint = locStartPoint;
    return this;
  }

  public Timestamp getLocStartTs() {
    return locStartTs;
  }

  public DbReservation setLocStartTs(Timestamp locStartTs) {
    this.locStartTs = locStartTs;
    return this;
  }

  public String getLocStartTz() {
    return locStartTz;
  }

  public DbReservation setLocStartTz(String locStartTz) {
    this.locStartTz = locStartTz;
    return this;
  }

  public Long getLocFinishPoiId() {
    return locFinishPoiId;
  }

  public DbReservation setLocFinishPoiId(Long locFinishPoiId) {
    this.locFinishPoiId = locFinishPoiId;
    return this;
  }

  public Integer getLocFinishCmpyId() {
    return locFinishCmpyId;
  }

  public DbReservation setLocFinishCmpyId(Integer locFinishCmpyId) {
    this.locFinishCmpyId = locFinishCmpyId;
    return this;
  }

  public String getLocFinishName() {
    return locFinishName;
  }

  public DbReservation setLocFinishName(String locFinishName) {
    this.locFinishName = locFinishName;
    return this;
  }

  public Point getLocFinishPoint() {
    return locFinishPoint;
  }

  public DbReservation setLocFinishPoint(Point locFinishPoint) {
    this.locFinishPoint = locFinishPoint;
    return this;
  }

  public Timestamp getLocFinishTs() {
    return locFinishTs;
  }

  public DbReservation setLocFinishTs(Timestamp locFinishTs) {
    this.locFinishTs = locFinishTs;
    return this;
  }

  public String getLocFinishTz() {
    return locFinishTz;
  }

  public DbReservation setLocFinishTz(String locFinishTz) {
    this.locFinishTz = locFinishTz;
    return this;
  }

  public JsonNode getData() {
    return data;
  }

  public DbReservation setData(JsonNode data) {
    this.data = data;
    return this;
  }

  public String getImportSrc() {
    return importSrc;
  }

  public DbReservation setImportSrc(String importSrc) {
    this.importSrc = importSrc;
    return this;
  }

  public String getImportSrcId() {
    return importSrcId;
  }

  public DbReservation setImportSrcId(String importSrcId) {
    this.importSrcId = importSrcId;
    return this;
  }

  public Long getImportTs() {
    return importTs;
  }

  public DbReservation setImportTs(Long importTs) {
    this.importTs = importTs;
    return this;
  }

  public Long getPkApiSrcId() {
    return pkApiSrcId;
  }

  public DbReservation setPkApiSrcId(Long pkApiSrcId) {
    this.pkApiSrcId = pkApiSrcId;
    return this;
  }

  public String getLegacyId() {
    return legacyId;
  }

  public DbReservation setLegacyId(String legacyId) {
    this.legacyId = legacyId;
    return this;
  }

  public RecordState getState() {
    return state;
  }

  public DbReservation setState(RecordState state) {
    this.state = state;
    return this;
  }

  public Long getNext() {
    return next;
  }

  public DbReservation setNext(Long next) {
    this.next = next;
    return this;
  }

  @Override
  public DbReservation getByPk(Long pk) {
    return find.byId(pk);
  }

}
