package models.publisher;

import com.avaje.ebean.BeanState;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.APPConstants;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-19
 * Time: 3:09 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity @Table(name = "activity")
public class ActivityBooking
    extends Model {
  public static Model.Finder<String, ActivityBooking> find = new Finder(ActivityBooking.class);
  public String comments;
  public String contact;
  public String destinationid;
  @Id
  public String detailsid;
  public String name;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @Version
  public int version;

  /**
   * Takes this object makes it possible to re-insert as new record
   * @return
   */
  public ActivityBooking prepareForReuse(String createdBy, String modifiedBy) {
    BeanState state = Ebean.getBeanState(this);
    state.resetForInsert();
    setDetailsid(DBConnectionMgr.getUniqueId());
    setModifiedby(modifiedBy);
    setCreatedby(createdBy);
    setLastupdatedtimestamp(System.currentTimeMillis());
    setCreatedtimestamp(lastupdatedtimestamp);
    return this;
  }

  public static ActivityBooking build(String detailId, String userId) {
    ActivityBooking activityBooking = new ActivityBooking();
    activityBooking.setDetailsid(detailId);
    activityBooking.setCreatedby(userId);
    activityBooking.setModifiedby(userId);
    activityBooking.setCreatedtimestamp(System.currentTimeMillis());
    activityBooking.setLastupdatedtimestamp(activityBooking.createdtimestamp);
    activityBooking.setVersion(0);
    return activityBooking;
  }


  public static ActivityBooking findByPK(String pk) {
    return find.byId(pk);
  }


  /**
   * Helper method to get all activities with destination guides
   * @param tripId
   * @return
   */
  public static List<ActivityBooking> getDocumentedActivities(String tripId) {
    com.avaje.ebean.Query<TripDetail> subQuery = Ebean
        .createQuery(TripDetail.class)
        .select("detailsid")
        .where()
        .eq("tripid", tripId)
        .eq("status", APPConstants.STATUS_ACTIVE) //We don't need dead souls here
        .query();
    return find
        .where()
        .in("detailsid", subQuery)
        .isNotNull("destinationid")
        .findList();
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }

  public String getDetailsid() {
    return detailsid;
  }

  public void setDetailsid(String detailsid) {
    this.detailsid = detailsid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public String getDestinationid() {
    return destinationid;
  }

  public void setDestinationid(String destinationid) {
    this.destinationid = destinationid;
  }
}
