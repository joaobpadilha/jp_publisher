package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import com.mapped.publisher.common.APPConstants;
import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by twong on 2014-12-26.
 */
@Entity
@Table(name="Flight_Alert_Event")
public class FlightAlertEvent extends Model {

  public Long departTimestamp;
  public String departTerminal;
  public String departGate;
  public Long arriveTimestamp;
  public String arriveTerminal;
  public String arriveGate;

  @Id
  public Long eventId;

  @Enumerated(value = EnumType.STRING)
  public FlightAlertType alertType;
  @Constraints.Required
  public String createdby;
  public Long createdtimestamp;
  public Long alertTimestamp;
  public int status;
  public String response;

  @OneToOne @JoinColumn(name = "flight_alert_id")
  public FlightAlert flightAlert;

  @Version
  public int version;

  public static Model.Finder<Long,FlightAlertEvent> find = new Finder(FlightAlertEvent.class);

  public enum FlightAlertType {
    PRE_DEPARTURE_STATUS,
    PRE_ARRIVAL_STATUS,
    DEPARTED_LATE,
    ARRIVED_LATE,
    EN_ROUTE,
    LANDED,
    CANCELLED,
    DIVERTED,
    DEPARTURE_DELAY,
    ARRIVAL_DELAY,
    BAGGAGE,
    TIME_ADJUSTMENT,
    DEPARTURE_GATE,
    ARRIVAL_GATE,
    GATE_ADJUSTMENT,
    EQUIPMENT_ADJUSTMENT,
    UNKNOWN
  }

  public static int countNotificationByAlert(long alertId) {
    return find.where().eq("flight_alert_id",alertId).findCount();
  }

  public static List<FlightAlertEvent> findPending(long alertId) {
    return find.where().eq("flight_alert_id",alertId).eq("status", APPConstants.STATUS_PENDING).findList();
  }

  public static FlightAlertEvent findLatestByAlertId (long alertId) {
    List<FlightAlertEvent> events = find.where().eq("flight_alert_id",alertId).orderBy("createdtimestamp desc").findList();
    if (events != null && events.size() > 0) {
      return find.where().eq("flight_alert_id", alertId).orderBy("createdtimestamp desc").findList().get(0);
    }
    return null;
  }

  public static int setEventProcessed (long alertId, List<Long> eventIds ) {
    String s = "UPDATE Flight_Alert_Event set status = 0 where flight_alert_id = :alertId and status = :pending; ";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("alertId", alertId);
    update.setParameter("pending", APPConstants.STATUS_PENDING);

    return Ebean.execute(update);
  }


  public Long getDepartTimestamp() {
    return departTimestamp;
  }

  public void setDepartTimestamp(Long departTimestamp) {
    this.departTimestamp = departTimestamp;
  }

  public String getDepartTerminal() {
    return departTerminal;
  }

  public void setDepartTerminal(String departTerminal) {
    this.departTerminal = departTerminal;
  }

  public String getDepartGate() {
    return departGate;
  }

  public void setDepartGate(String departGate) {
    this.departGate = departGate;
  }

  public Long getArriveTimestamp() {
    return arriveTimestamp;
  }

  public void setArriveTimestamp(Long arriveTimestamp) {
    this.arriveTimestamp = arriveTimestamp;
  }

  public String getArriveTerminal() {
    return arriveTerminal;
  }

  public void setArriveTerminal(String arriveTerminal) {
    this.arriveTerminal = arriveTerminal;
  }

  public String getArriveGate() {
    return arriveGate;
  }

  public void setArriveGate(String arriveGate) {
    this.arriveGate = arriveGate;
  }

  public Long getEventId() {
    return eventId;
  }

  public void setEventId(Long eventId) {
    this.eventId = eventId;
  }

  public FlightAlertType getAlertType() {
    return alertType;
  }

  public void setAlertType(FlightAlertType alertType) {
    this.alertType = alertType;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getResponse() {
    return response;
  }

  public void setResponse(String response) {
    this.response = response;
  }

  public FlightAlert getFlightAlert() {
    return flightAlert;
  }

  public void setFlightAlert(FlightAlert flightAlert) {
    this.flightAlert = flightAlert;
  }

  public Long getAlertTimestamp() {
    return alertTimestamp;
  }

  public void setAlertTimestamp(Long alertTimestamp) {
    this.alertTimestamp = alertTimestamp;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
