package models.publisher;

import com.avaje.ebean.BeanState;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.mapped.persistence.util.DBConnectionMgr;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Created by surge on 2014-08-11.
 */
@Entity @Table(name = "booking_cruise")
public class BookingCruise
    extends Model {
  public static Model.Finder<String, BookingCruise> find = new Finder(BookingCruise.class);

  @Id
  private String detailsid;
  @Constraints.Required
  private String name;
  private String cmpyName;
  private String category;
  private String cabinNumber;
  private String deck;
  private String bedding;
  private String meal;
  private Long createdtimestamp;
  private Long lastupdatedtimestamp;
  private String createdby;
  private String modifiedby;
  @Version
  private int version;

  /**
   * Takes this object makes it possible to re-insert as new record
   * @return
   */
  public BookingCruise prepareForReuse(String createdBy, String modifiedBy) {
    BeanState state = Ebean.getBeanState(this);
    state.resetForInsert();
    setDetailsid(DBConnectionMgr.getUniqueId());
    setModifiedby(modifiedBy);
    setCreatedby(createdBy);
    setLastupdatedtimestamp(System.currentTimeMillis());
    setCreatedtimestamp(lastupdatedtimestamp);
    return this;
  }

  public static BookingCruise build(String detailId, String userId) {
    BookingCruise cruiseBooking = new BookingCruise();
    cruiseBooking.setDetailsid(detailId);
    cruiseBooking.setCreatedby(userId);
    cruiseBooking.setModifiedby(userId);
    cruiseBooking.setCreatedtimestamp(System.currentTimeMillis());
    cruiseBooking.setLastupdatedtimestamp(cruiseBooking.createdtimestamp);
    cruiseBooking.version = 0;
    return cruiseBooking;
  }

  public String getDetailsid() {
    return detailsid;
  }

  public void setDetailsid(String detailsid) {
    this.detailsid = detailsid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCmpyName() {
    return cmpyName;
  }

  public void setCmpyName(String cmpyName) {
    this.cmpyName = cmpyName;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getCabinNumber() {
    return cabinNumber;
  }

  public void setCabinNumber(String cabinNumber) {
    this.cabinNumber = cabinNumber;
  }

  public String getDeck() {
    return deck;
  }

  public void setDeck(String deck) {
    this.deck = deck;
  }

  public String getBedding() {
    return bedding;
  }

  public void setBedding(String bedding) {
    this.bedding = bedding;
  }

  public String getMeal() {
    return meal;
  }

  public void setMeal(String meal) {
    this.meal = meal;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }
}
