package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Expression;
import com.mapped.publisher.common.Capability;
import com.mapped.publisher.utils.Log;
import org.apache.commons.lang3.time.StopWatch;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2015-12-11.
 */
@Entity
@Table(name = "sys_capability")
public class SysCapability
    extends Model {

  public static Finder<Integer, SysCapability> find = new Finder(SysCapability.class);

  @Id
  @Column(name = "pk")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_sys_capability_pk")
  Integer    pk;
  /**
   * Feature name
   */
  @Column(name = "name")
  @Enumerated(value = EnumType.STRING)
  Capability capability;
  /**
   * Human readable name (summary)
   */
  String  summary;
  /**
   * Description for the feature
   */
  String  description;
  /**
   * Indicates whether or not to show capability to the user
   */
  Boolean userControlled;
  /**
   * Whether or not disable capability from appearing in UIs, will not remove individual capabilities
   */
  Boolean enabled;
  Long    modifiedTs;
  String  modifiedBy;
  @Version
  int version;

  public static void syncEnumToDb() {
    StopWatch sw = new StopWatch();
    sw.start();
    for (Capability c : Capability.values()) {
      SysCapability sc = find.where().eq("name", c.name()).findUnique();
      if (sc == null) {
        sc = new SysCapability();
        sc.setCapability(c);
        sc.setSummary("");
        sc.setDescription("");
        sc.setSummary("");
        sc.setModifiedTs(System.currentTimeMillis());
        sc.setModifiedBy("system");
        sc.setUserControlled(false);
        sc.setEnabled(false);
        try {
          sc.save();
        }
        catch (Exception e) {
          Log.debug("Failed to sync");
          e.printStackTrace();
          //Others might be trying to create capabilities at the same time
        }
      }
    }
    sw.stop();
    Log.debug("Finished sync of Capabilities Enum with the Database in " + sw.getTime() + "ms");
  }


  /**
   * List of all capabilities enabled or disabled in the plan
   * @param planId
   * @return
   */
  public static List<SysCapability> getPlanCapabilities(Long planId, boolean enabled) {
    com.avaje.ebean.Query<BillingPlanFeature> planFeatures = Ebean
        .createQuery(BillingPlanFeature.class)
        .select("pk.featurePk")
        .where()
        .eq("pk.planId", planId)
        .query();

      com.avaje.ebean.Query<SysFeatureCapability> capIds = Ebean
          .createQuery(SysFeatureCapability.class)
          .select("pk.capPk")
          .where()
          .in("feat_pk", planFeatures)
          .query();

      return find
          .where()
          .eq("enabled", enabled)
          .in("pk", capIds)
          .findList();
  }

  public static List<SysCapability> getUserPlanCapabilities(String userId) {
    com.avaje.ebean.Query<BillingSettingsForUser> userPlan = Ebean
        .createQuery(BillingSettingsForUser.class)
        .select("plan")
        .where()
        .eq("userid", userId)
        .query();

    com.avaje.ebean.Query<BillingSettingsForUser> cmpyId = Ebean
        .createQuery(BillingSettingsForUser.class)
        .select("cmpyid")
        .where()
        .eq("userid", userId)
        .eq("bill_to","COMPANY")
        .query();

    com.avaje.ebean.Query<BillingSettingsForUser> agentId = Ebean
        .createQuery(BillingSettingsForUser.class)
        .select("agent")
        .where()
        .eq("userid", userId)
        .eq("bill_to","AGENT")
        .query();

    com.avaje.ebean.Query<BillingSettingsForUser> agentPlan = Ebean
        .createQuery(BillingSettingsForUser.class)
        .select("plan")
        .where()
        .in("userid", agentId)
        .query();

    com.avaje.ebean.Query<BillingSettingsForCmpy> cmpyPlan = Ebean
        .createQuery(BillingSettingsForCmpy.class)
        .select("plan")
        .where()
        .in("cmpyid", cmpyId)
        .query();

    com.avaje.ebean.Query<BillingPlanFeature> planFeatures = Ebean
        .createQuery(BillingPlanFeature.class)
        .select("pk.featurePk")
        .where()
        .or(Expr.in("pk.planId", userPlan), Expr.or(Expr.in("pk.planId", cmpyPlan),Expr.in("pk.planId", agentPlan)))
        .query();

    com.avaje.ebean.Query<SysFeatureCapability> capIds = Ebean
        .createQuery(SysFeatureCapability.class)
        .select("pk.capPk")
        .where()
        .in("feat_pk", planFeatures)
        .query();

    return find
        .where()
        .eq("enabled", true)
        .in("pk", capIds)
        .findList();
  }


  public static List<SysCapability> getUserEnabledCapabilities(String userId) {
    com.avaje.ebean.Query<UserCapability> userCapIds = Ebean
        .createQuery(UserCapability.class)
        .select("pk.capPk")
        .where()
        .eq("pk.userId", userId)
        .eq("enabled", true)
        .query();

    return find
        .where()
        .eq("enabled", true)
        .in("pk", userCapIds)
        .findList();
  }

  public static List<SysCapability> getUserDisabledCapabilities(String userId) {
    com.avaje.ebean.Query<UserCapability> userCapIds = Ebean
        .createQuery(UserCapability.class)
        .select("pk.capPk")
        .where()
        .eq("pk.userId", userId)
        .eq("enabled", false)
        .query();

    return find
        .where()
        .in("pk", userCapIds)
        .findList();
  }

  public static List<SysCapability> getFeatureCapabilities(Integer featurePk) {
    com.avaje.ebean.Query<SysFeatureCapability> subQuery = Ebean
        .createQuery(SysFeatureCapability.class)
        .select("pk.capPk")
        .where()
        .eq("feat_pk", featurePk)
        .query();

    return find.where().in("pk", subQuery).findList();
  }

  public static SysCapability getCapabilityByName (Capability capability) {
    return find.where().eq("name", capability).findUnique();
  }

  public void markModified(String userId) {
    setModifiedBy(userId);
    setModifiedTs(System.currentTimeMillis());
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public Boolean getEnabled() {
    return enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public Capability getCapability() {
    return capability;
  }

  public void setCapability(Capability capability) {
    this.capability = capability;
  }

  public Integer getPk() {
    return pk;
  }

  public void setPk(Integer pk) {
    this.pk = pk;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Boolean getUserControlled() {
    return userControlled;
  }

  public void setUserControlled(Boolean userControlled) {
    this.userControlled = userControlled;
  }

  public Long getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Long modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
