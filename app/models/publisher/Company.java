/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-09
 * Time: 10:38 AM
 * To change this template use File | Settings | File Templates.
 */
package models.publisher;

import com.avaje.ebean.*;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import play.data.validation.Constraints;

import javax.persistence.*;
import javax.persistence.Version;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Entity @Table(name = "company")
public class Company
    extends Model {
  public final static int NO_COMPANY_ID = -1;
  public final static int PUBLIC_COMPANY_ID = 0;

  public static Model.Finder<String, Company> find = new Finder(Company.class);
  public String tag;
  public String type;
  public String comment;
  public String logourl;
  public String logoname;
  public String contact;
  public String authorizeduserid;
  public String acceptedagreementver;
  public String targetagreementver;
  private String apiaccessid;
  public String apiaccesskey;
  public boolean flighttracking;
  public String parentCmpyId;
  @Enumerated(value = EnumType.STRING)
  public BillingType billingtype;
  @Id
  public String cmpyid;
  @Constraints.Required
  public String name;
  public int status;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public int numaccounts;
  public int maxtrips;
  public Long expirytimestamp;
  public boolean itinerarycheck;
  public boolean allowapi;
  public String createdby;
  public String modifiedby;
  public boolean enablecollaboration;

  @Version
  public int version;
  @Column(name = "cmpy_id")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_company_id")
  //@SequenceGenerator(name = "seq_company_id", sequenceName = "cmpy_id")
  private int cmpyId;

  /**
   * States of the invitation state machine
   */
  public enum BillingType {
    UNDEFINED,
    MONTHLY,
    PER_TRIP
  }

  /**
   * Number of active company records
   * @return
   */
  public static int getRowCount() {
    return find.where().eq("status", 0).ne("cmpyid", "0").findCount();
  }

  public static int getRowCountFiltered(String filter) {
    return find.where()
               .ne("cmpyid", "0")
               .or(Expr.ilike("name", "%"+filter+"%"),Expr.ilike("contact", "%"+filter+"%"))
               .eq("status", 0)
               .findCount();
  }

  public static List<Company> getFilteredPage(int start, int count, String filter) {
    return find.where()
               .ne("cmpyid", "0")
               .or(Expr.ilike("name", "%"+filter+"%"),Expr.ilike("contact", "%"+filter+"%"))
               .eq("status", 0)
               .orderBy("name asc")
               .setFirstRow(start).setMaxRows(count)
               .findList();
  }

  public static List<Company> getChildCmpies(String parentCmpyId) {
    return find.where()
        .eq("parent_cmpy_id", parentCmpyId)
        .eq("status", 0)
        .orderBy("name asc")
        .findList();
  }

  /**
   * Number of active company records
   * @return
   */
  public static int getRowCount_Billing(BillingSchedule.Type bst) {

    com.avaje.ebean.Query<BillingSettingsForCmpy> subQuery;
    subQuery = Ebean.createQuery(BillingSettingsForCmpy.class);

    if (bst == null || bst == BillingSchedule.Type.NONE) {
      subQuery.select("cmpyId")
              .where().ne("schedule.name", BillingSchedule.Type.NONE);
      return find.where()
                 .eq("status", 0)
                 .ne("cmpyid", "0")
                 .not(Expr.in("cmpyid", subQuery))
                 .findCount();
    }


    subQuery.select("cmpyId")
            .where().eq("schedule.name", bst);
    return find.where()
               .eq("status", 0)
               .ne("cmpyid", "0")
               .in("cmpyid", subQuery)
               .findCount();
  }

  public static int getRowCountFiltered_Billing(BillingSchedule.Type bst, String filter) {

    com.avaje.ebean.Query<BillingSettingsForCmpy> subQuery;
    subQuery = Ebean.createQuery(BillingSettingsForCmpy.class);

    if (bst == null || bst == BillingSchedule.Type.NONE) {
      subQuery.select("cmpyId")
              .where().ne("schedule.name", BillingSchedule.Type.NONE);
      return find.where()
                 .eq("status", 0)
                 .ne("cmpyid", "0")
                 .not(Expr.in("cmpyid", subQuery))
                 .ilike("name", "%"+filter+"%")
                 .findCount();
    }

    subQuery.select("cmpyId")
            .where().eq("schedule.name", bst);

    return find.where()
               .eq("status", 0)
               .ne("cmpyid", "0")
               .in("cmpyid", subQuery)
               .ilike("name", "%"+filter+"%")
               .findCount();
  }

  public static List<Company> getFilteredPage_Billing(BillingSchedule.Type bst, int start, int count, String filter) {
    com.avaje.ebean.Query<BillingSettingsForCmpy> subQuery = Ebean.createQuery(BillingSettingsForCmpy.class);

    if (bst == null || bst == BillingSchedule.Type.NONE) {
      subQuery.select("cmpyId")
              .where().ne("schedule.name", BillingSchedule.Type.NONE);
      return find.where()
                 .eq("status", 0)
                 .ne("cmpyid", "0")
                 .not(Expr.in("cmpyid", subQuery))
                 .ilike("name", "%"+filter+"%")
                 .orderBy("name asc")
                 .setFirstRow(start).setMaxRows(count)
                 .findList();
    }

    subQuery.select("cmpyId")
            .where().eq("schedule.name", bst);
    return find.where()
               .eq("status", 0)
               .ne("cmpyid", "0")
               .in("cmpyid", subQuery)
               .ilike("name", "%"+filter+"%")
               .orderBy("name asc")
               .setFirstRow(start).setMaxRows(count)
               .findList();
  }

  public static Company findByCmpyID(int companyid) {
    return find.where().eq("cmpy_id", companyid).findUnique();
  }

  public static Company findByCmpyName(String cmpyName) {

    List<Company> companies =  find.where().eq("name", cmpyName).eq("status", 0).findList();
    if (companies != null && companies.size() == 1) {
      return companies.get(0);
    }
    return null;
  }

  public static Company findParentCompany(String cmpyid) {
    com.avaje.ebean.Query<Company> subQuery = Ebean.createQuery(Company.class);
    subQuery.select("parentCmpyId").where().eq("cmpyid", cmpyid);
    return find.where().in("cmpyid", subQuery).findUnique();
  }

  public static Company buildCompany(String userId) {
    Company company = new Company();
    company.setCmpyid(Utils.getUniqueId());
    company.setCreatedby(userId);
    company.setCreatedtimestamp(System.currentTimeMillis());
    company.setStatus(APPConstants.STATUS_ACTIVE);

    Connection conn = null;
    try {
      conn = DBConnectionMgr.getConnection4Publisher();
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery("select nextval('seq_company_id')");
      while (rs.next()) {
        company.setCmpyId(rs.getInt(1));
      }
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "Failed to obtain database connection for Company ID sequence:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      try {
        conn.close();
      }
      catch (SQLException sqle) {
        Log.log(LogLevel.ERROR, "Can't close connection for some reason" + sqle.getMessage());
        sqle.printStackTrace();
      }
    }

    return company;
  }

  public static List<Company> findMostRecentActive() {
    return find.where()
               .eq("status", 0)
               .ne("cmpyid", "0")
               .orderBy("lastupdatedtimestamp desc")
               .setMaxRows(20)
               .findList();
  }

  public static List<Company> findByIds(List<String> cmpyIds) {
    return find.where()
               .eq("status", 0)
               .ne("cmpyid", "0")
               .in("cmpyid", cmpyIds)
               .orderBy("lastupdatedtimestamp desc")
               .findList();
  }

  public static List<Company> findByKeyword(String term) {
    return find.where()
               .ne("cmpyid", "0")
               .icontains("name", term)
               .eq("status", 0)
               .orderBy("name asc")
               .setMaxRows(20)
               .findList();
  }

  public static List<Company> findByTag(String term) {
    return find.where()
            .ne("cmpyid", "0")
            .contains("tag", term)
            .eq("status", 0)
            .orderBy("name asc")
            .setMaxRows(2)
            .findList();
  }

  public static Company findByApiAccessId(String accessId) {
    return find.where()
               .ieq("apiaccessid", accessId)
               .eq("status", APPConstants.STATUS_ACTIVE)
               .eq("allowapi", true)
               .setMaxRows(1)
               .findUnique();
  }

  public static List<Company> findBillable() {
    return find.where().eq("status", 0).ne("cmpyid", "0").ne("type", "3").orderBy("name asc").findList();
  }

  public static List<Company> findTrials(long expirytimestamp) {
    return find.where()
               .eq("status", 0)
               .ne("cmpyid", "0")
               .eq("type", "3")
               .lt("expirytimestamp", expirytimestamp)
               .orderBy("expirytimestamp asc")
               .orderBy("name asc")
               .findList();
  }

  public static List<Company> findNewCmpies(long startTimestamp, long endTimestamp) {
    return find.where()
               .eq("status", 0)
               .ne("cmpyid", "0")
               .ge("createdtimestamp", startTimestamp)
               .lt("createdtimestamp", endTimestamp)
               .orderBy("createdby asc")
               .orderBy("createdtimestamp asc")
               .findList();
  }

  public static int countByCmpyName(String name) {
    name = name.trim();
    return find.where().eq("status", 0).ilike("name", name).findCount();
  }

  public static List<Company> findAll() {
    return find.all();
  }

  public static boolean isCollaborationEnabled(String cmpyid) {
    Company cmpy = find.where().eq("cmpyid", cmpyid).findUnique();
    if (cmpy != null) {
      return cmpy.enablecollaboration;
    }
    return false;
  }

  public static boolean isParentCmpy(String cmpyid) {
    int cmpyCount = find.where().eq("parent_cmpy_id", cmpyid).findCount();
    if (cmpyCount > 0) {
      return true;
    }
    return false;
  }

  public int getCmpyId() {
    return cmpyId;
  }

  public void setCmpyId(int cmpyId) {
    this.cmpyId = cmpyId;
  }

  public boolean isEnablecollaboration() {
    return enablecollaboration;
  }

  public void setEnablecollaboration(boolean enablecollaboration) {
    this.enablecollaboration = enablecollaboration;
  }

  public String toString() {
    return name;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getCmpyid() {
    return cmpyid;
  }

  public void setCmpyid(String cmpyid) {
    this.cmpyid = cmpyid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public String getLogourl() {
    return logourl;
  }

  public void setLogourl(String logourl) {
    this.logourl = logourl;
  }

  public String getLogoname() {
    return logoname;
  }

  public void setLogoname(String logoname) {
    this.logoname = logoname;
  }

  public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }

  public int getNumaccounts() {
    return numaccounts;
  }

  public void setNumaccounts(int numaccounts) {
    this.numaccounts = numaccounts;
  }

  public void setNumAccounts(int numaccounts) {
    this.numaccounts = numaccounts;
  }

  public int getMaxtrips() {
    return maxtrips;
  }

  public void setMaxtrips(int maxtrips) {
    this.maxtrips = maxtrips;
  }

  public Long getExpirytimestamp() {
    return expirytimestamp;
  }

  public void setExpirytimestamp(Long expiryTimestamp) {
    this.expirytimestamp = expiryTimestamp;
  }

  public String getAuthorizeduserid() {
    return authorizeduserid;
  }

  public void setAuthorizeduserid(String authorizeduserid) {
    this.authorizeduserid = authorizeduserid;
  }

  public String getAcceptedagreementver() {
    return acceptedagreementver;
  }

  public void setAcceptedagreementver(String acceptedagreementver) {
    this.acceptedagreementver = acceptedagreementver;
  }

  public String getTargetagreementver() {
    return targetagreementver;
  }

  public void setTargetagreementver(String targetagreementver) {
    this.targetagreementver = targetagreementver;
  }

  @Deprecated
  public String getApiaccessid() {
    return apiaccessid;
  }

  @Deprecated
  public void setApiaccessid(String apiaccessid) {
    this.apiaccessid = apiaccessid;
  }

  public String getApiaccesskey() {
    return apiaccesskey;
  }

  public void setApiaccesskey(String apiaccesskey) {
    this.apiaccesskey = apiaccesskey;
  }

  public boolean isAllowapi() {
    return allowapi;
  }

  public void setAllowapi(boolean allowapi) {
    this.allowapi = allowapi;
  }

  public boolean isItinerarycheck() {
    return itinerarycheck;
  }

  public void setItinerarycheck(boolean itinerarycheck) {
    this.itinerarycheck = itinerarycheck;
  }

  public boolean isFlighttracking() {
    return flighttracking;
  }

  public void setFlighttracking(boolean flighttracking) {
    this.flighttracking = flighttracking;
  }

  public BillingType getBillingtype() {
    return billingtype;
  }

  public void setBillingtype(BillingType billingtype) {
    this.billingtype = billingtype;
  }

  public boolean display12hrClock() {
    if (tag != null && tag.contains("24hrclock")) {
      return false;
    } else if (name != null && name.contains("Top Atlant")) {
      return false;
    }

    return true;
  }

  public String getParentCmpyId() {
    return parentCmpyId;
  }

  public void setParentCmpyId(String parentCmpyId) {
    this.parentCmpyId = parentCmpyId;
  }
}
