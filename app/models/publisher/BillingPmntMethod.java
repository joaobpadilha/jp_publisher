package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.*;

/**
 * Created by surge on 2015-03-26.
 */
@Entity @Table(name = "bil_payment_type")
public class BillingPmntMethod
    extends Model {
  public static Model.Finder<Type, BillingPmntMethod> find = new Finder(BillingPmntMethod.class);

  @Id @Column(name = "name", length = 15) @Enumerated(value = EnumType.STRING)
  private Type name;

  /**
   * Created by surge on 2015-03-26.
   */
  public static enum Type {
    BANK_TRANSFER,
    CREDIT_CARD,
    CHEQUE
  }

  public Type getName() {
    return name;
  }

  public void setName(Type name) {
    this.name = name;
  }

}
