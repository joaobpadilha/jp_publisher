package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-07-24
 * Time: 2:33 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity @Table(name = "tmplt_flight")
public class TmpltFlight
    extends Model {
  public static Model.Finder<String, TmpltFlight> find = new Finder(TmpltFlight.class);
  public String comments;
  @Id
  public String detailsid;
  public String flightid;
  public String departureairport;
  public String arrivalairport;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @Version
  public int version;

  public static TmpltFlight buildFlight(String detailsId, String userId) {
    TmpltFlight tmpltFlight = new TmpltFlight();
    tmpltFlight.setDetailsid(detailsId);
    tmpltFlight.setCreatedtimestamp(System.currentTimeMillis());
    tmpltFlight.setLastupdatedtimestamp(tmpltFlight.createdtimestamp);
    tmpltFlight.setCreatedby(userId);
    tmpltFlight.setModifiedby(userId);
    tmpltFlight.version = 0;
    return tmpltFlight;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getDetailsid() {
    return detailsid;
  }

  public void setDetailsid(String detailsid) {
    this.detailsid = detailsid;
  }

  public String getFlightid() {
    return flightid;
  }

  public void setFlightid(String flightid) {
    this.flightid = flightid;
  }

  public String getDepartureairport() {
    return departureairport;
  }

  public void setDepartureairport(String departureairport) {
    this.departureairport = departureairport;
  }

  public String getArrivalairport() {
    return arrivalairport;
  }

  public void setArrivalairport(String arrivalairport) {
    this.arrivalairport = arrivalairport;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
