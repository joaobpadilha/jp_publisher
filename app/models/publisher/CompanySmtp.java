package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import play.data.validation.Constraints;
import play.db.DB;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.sql.*;
import java.time.Instant;
import java.util.List;

/**
 * Created by george on 2016-08-17.
 */

@Entity @Table(name = "company_smtp")
public class CompanySmtp extends Model {

  public static Model.Finder<CmpySmtpPk, CompanySmtp> find = new Finder(CompanySmtp.class);

  private String emailNotice;

  @Id
  private CmpySmtpPk pk;

  @Constraints.Required
  private String host;
  private int port;
  private String username;
  private String password;
  private String emailDefault;
  private long createdBy;
  private Timestamp createdTs;
  private long modifiedBy;
  private Timestamp modifiedTs;

  @Version
  private int version;


  @Embeddable
  public static class CmpySmtpPk {
    @Column(name = "consortium") Integer consortium;
    @Column(name = "cmpyid") String cmpyid;

    public CmpySmtpPk (int consortium, String cmpyid) {
      this.cmpyid =cmpyid;
      this.consortium = consortium;
    }

    @Override
    public int hashCode() {
      return consortium.hashCode() * cmpyid.hashCode();
    }

    @Override
    public boolean equals(Object o) {
      if (o == null) {
        return false;
      }
      if (!(o instanceof CmpySmtpPk)) {
        return false;
      }
      CmpySmtpPk ok = (CmpySmtpPk) o;

      return consortium.equals(ok.consortium) && cmpyid.equals(ok.cmpyid);
    }
  }
  public static CompanySmtp build(int consortium, String cmpyId, Long createdBy) {
    CmpySmtpPk pk = new CmpySmtpPk(consortium, cmpyId);


    CompanySmtp smtp = new CompanySmtp();
    smtp.setPk(pk);
    smtp.setCreatedBy(createdBy);
    smtp.setModifiedBy(createdBy);
    smtp.setCreatedTs(Timestamp.from(Instant.now()));
    smtp.setModifiedTs(Timestamp.from(Instant.now()));
    return smtp;
  }

  public static CompanySmtp findByPK (String cmpyId, int consortium) {
    CmpySmtpPk pk = new CmpySmtpPk(consortium, cmpyId);

    return find.byId(pk);
  }

  public static void deleteByPK (String cmpyId, int consortium) {
    CmpySmtpPk pk = new CmpySmtpPk(consortium, cmpyId);
    find.deleteById(pk);
  }


  public CmpySmtpPk getPk() {
    return pk;
  }

  public void setPk(CmpySmtpPk pk) {
    this.pk = pk;
  }

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public int getPort() {
    return port;
  }

  public void setPort(int port) {
    this.port = port;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getEmailDefault() {
    return emailDefault;
  }

  public void setEmailDefault(String emailDefault) {
    this.emailDefault = emailDefault;
  }

  public String getEmailNotice() {
    return emailNotice;
  }

  public void setEmailNotice(String emailNotice) {
    this.emailNotice = emailNotice;
  }

  public long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(long createdBy) {
    this.createdBy = createdBy;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
  }

  public long getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(long modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public Timestamp getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Timestamp modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
