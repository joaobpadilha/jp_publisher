package models.publisher;

import com.avaje.ebean.BeanState;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.persistence.PageAttachType;
import com.mapped.publisher.utils.Utils;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-17
 * Time: 10:41 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity @Table(name = "trip_attachment")
public class TripAttachment
    extends Model {
  public static Model.Finder<String, TripAttachment> find = new Finder(TripAttachment.class);
  public String name;
  public String description;

  @Id
  public String pk;
  @Constraints.Required
  public String tripid;
  public String filename;
  public String origfilename;
  public String fileurl;
  public int attachtype;
  public String filetype;
  public int status;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @Version
  public int version;

  @ManyToOne
  @JoinColumn(name = "file_pk", nullable = true, referencedColumnName = "pk")
  FileInfo  file;

  /**
   * Takes this object makes it possible to re-insert as new record
   * @return
   */
  public TripAttachment prepareForReuse(String createdBy, String modifiedBy) {
    BeanState state = Ebean.getBeanState(this);
    state.resetForInsert();
    setPk(DBConnectionMgr.getUniqueId());
    setModifiedby(modifiedBy);
    setCreatedby(createdBy);
    setLastupdatedtimestamp(System.currentTimeMillis());
    setCreatedtimestamp(lastupdatedtimestamp);
    return this;
  }

  public static TripAttachment buildTripAttachment(UserProfile userProfile,
                                                   Trip trip,
                                                   String fileName,
                                                   String sysFileName,
                                                   String fileURL,
                                                   String comments) {

    TripAttachment tripAttachment = new TripAttachment();
    tripAttachment.setPk(Utils.getUniqueId());
    tripAttachment.setAttachtype(APPConstants.TRIP_ATTACH_FILE);
    tripAttachment.setStatus(APPConstants.STATUS_PENDING);
    tripAttachment.setTripid(trip.tripid);
    tripAttachment.setCreatedby(userProfile.userid);
    tripAttachment.setModifiedby(userProfile.userid);
    tripAttachment.setCreatedtimestamp(System.currentTimeMillis());
    tripAttachment.setLastupdatedtimestamp(tripAttachment.createdtimestamp);
    tripAttachment.setDescription(comments);
    tripAttachment.setOrigfilename(fileName);
    tripAttachment.setFilename(sysFileName);
    tripAttachment.setFileurl(fileURL);
    tripAttachment.setFiletype("");

    if (fileName.trim().toLowerCase().endsWith("doc")) {
      tripAttachment.setFiletype("application/msword");
    }
    else if (fileName.trim().toLowerCase().endsWith("pdf")) {
      tripAttachment.setFiletype("application/pdf");
    }
    else if (fileName.trim().toLowerCase().endsWith("docx")) {
      tripAttachment.setFiletype("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    }

    return tripAttachment;
  }

  public static int activeCountByTripId(String tripId) {
    return find.where().eq("tripid", tripId).eq("status", 0).findCount();
  }

  public static List<TripAttachment> findByTripId(String tripId) {
    return find.where().eq("tripid", tripId).eq("status", 0).orderBy("lastupdatedtimestamp desc").findList();
  }

  public static List<TripAttachment> findByTripIdFileName(String tripId, String filename) {
    return find.where()
               .eq("tripid", tripId)
               .ieq("origfilename", filename)
               .eq("status", 0)
               .orderBy("createdtimestamp desc")
               .findList();
  }

  public static List<TripAttachment> findByTripIdFileNameAnyStatus(String tripId, String filename) {
    return find.where()
               .eq("tripid", tripId)
               .ieq("origfilename", filename)
               .orderBy("createdtimestamp desc")
               .findList();
  }

  public static List<TripAttachment> findForDetailId(String detailid){
    com.avaje.ebean.Query<TripDetailAttach> attachIds = Ebean
        .createQuery(TripDetailAttach.class)
        .select("detailAttachId.attachId")
        .where()
        .eq("detailAttachId.detailsId", detailid)
        .query()
        .orderBy("rank");

    return find.where().in("pk", attachIds).eq("status", APPConstants.STATUS_ACTIVE).findList();
  }

  public String getPk() {
    return pk;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public String getTripid() {
    return tripid;
  }

  public void setTripid(String tripid) {
    this.tripid = tripid;
  }

  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public String getOrigfilename() {
    return origfilename;
  }

  public void setOrigfilename(String origfilename) {
    this.origfilename = origfilename;
  }

  public String getFileurl() {
    return fileurl;
  }

  public void setFileurl(String fileurl) {
    this.fileurl = fileurl;
  }

  public int getAttachtype() {
    return attachtype;
  }

  public void setAttachtype(int attachtype) {
    this.attachtype = attachtype;
  }

  public String getFiletype() {
    return filetype;
  }

  public void setFiletype(String filetype) {
    this.filetype = filetype;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public FileInfo getFile() {
    return file;
  }

  public TripAttachment setFile(FileInfo file) {
    this.file = file;
    return this;
  }

  public FileInfo getFileInfo() {
    return getFile();
  }


  public TripAttachment setFileInfo(FileInfo file) {
    setFile(file);

    if(file != null) {
      if (this.getFilename() == null || this.getFilename().isEmpty()) {
        setFilename(file.getFilename());
      }
      if (this.getOrigfilename() == null || this.getOrigfilename().isEmpty()) {
        setOrigfilename(file.getFilename());
      }
      setAttachtype(APPConstants.TRIP_ATTACH_FILE);
      setFileurl(file.getUrl());
    }
    return this;
  }
}
