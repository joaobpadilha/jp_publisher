package models.publisher;

import org.joda.time.DateTime;
import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by twong on 2014-12-26.
 */
@Entity @Table(name = "Flight_Alert")
public class FlightAlert
    extends Model {
  public static Model.Finder<Long, FlightAlert> find = new Finder(FlightAlert.class);
  public String arrivalAirport;
  public Long origArriveTime;
  public String departTimeZone;
  public String arriveTimeZone;
  public Long newDepartTime;
  public Long newArriveTime;
  public String newDepartTerminal;
  public String newDepartGate;
  public String newArriveTerminal;
  public String newArriveGate;
  public Long lastEventTimestamp;
  public String departAirportName;
  public String arrivalAirportName;
  public String airlineName;
  public Long newDepartTimeUtc;
  public Long newArriveTimeUtc;
  @Id
  public Long flightAlertId;
  @Constraints.Required
  public String ruleId;
  public String airlineCode;
  public long flightId;
  public String response;
  public String departAirport;
  public Long origDepartTime;
  public int status;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  @Version
  public int version;

  public enum FlightAlertStatus {
    ACTIVE,
    PENDING,
    CONFIRMED,
    ERROR
  }

  public static FlightAlert findAlert(String airlineCode,
                                      long flightId,
                                      String departAirport,
                                      DateTime origDepartDate) {
    long startDate = origDepartDate.withTimeAtStartOfDay().getMillis();
    long endDate = origDepartDate.plusDays(1).withTimeAtStartOfDay().getMillis();
    ArrayList<Integer> statuses = new ArrayList<>();
    statuses.add(FlightAlertStatus.ACTIVE.ordinal());
    statuses.add(FlightAlertStatus.CONFIRMED.ordinal());
    statuses.add(FlightAlertStatus.PENDING.ordinal());

    List<FlightAlert> alertList = find.where()
                                      .eq("airline_code", airlineCode)
                                      .eq("flight_id", flightId)
                                      .eq("depart_airport", departAirport)
                                      .ge("orig_depart_time", startDate)
                                      .lt("orig_depart_time", endDate)
                                      .in("status", statuses)
                                      .setMaxRows(2)
                                      .findList();

    if (alertList != null && alertList.size() > 0) {
      return alertList.get(0);
    }
    else {
      return null;
    }

  }

  public static FlightAlert findAlert(String airlineCode,
                                      long flightId,
                                      String departAirport,
                                      String arriveAirport,
                                      DateTime origDepartDate) {
    long startDate = origDepartDate.withTimeAtStartOfDay().getMillis();
    long endDate = origDepartDate.plusDays(1).withTimeAtStartOfDay().getMillis();
    ArrayList<Integer> statuses = new ArrayList<>();
    statuses.add(FlightAlertStatus.ACTIVE.ordinal());
    statuses.add(FlightAlertStatus.CONFIRMED.ordinal());
    statuses.add(FlightAlertStatus.PENDING.ordinal());

    List<FlightAlert> alertList = find.where()
                                      .eq("airline_code", airlineCode)
                                      .eq("flight_id", flightId)
                                      .eq("depart_airport", departAirport)
                                      .eq("arrival_airport", arriveAirport)
                                      .ge("orig_depart_time", startDate)
                                      .lt("orig_depart_time", endDate)
                                      .in("status", statuses)
                                      .setMaxRows(2)
                                      .findList();

    if (alertList != null && alertList.size() > 0) {
      return alertList.get(0);
    }
    else {
      return null;
    }

  }

  public static FlightAlert findByRuleId(String ruleId) {
    return find.where().eq("rule_id", ruleId).eq("status", 0).findUnique();
  }

  public static List<FlightAlert> findByStatusAndOrigDepart(FlightAlertStatus status,
                                                            long startTimestamp,
                                                            long endTimestamp) {
    return find.where()
               .eq("status", status.ordinal())
               .ge("orig_depart_time", startTimestamp)
               .lt("orig_depart_time", endTimestamp)
               .findList();
  }

  public static List<FlightAlert> findByStatusAndUTCDepart(FlightAlertStatus status,
                                                           long startTimestamp,
                                                           long endTimestamp) {
    return find.where()
               .eq("status", status.ordinal())
               .ge("new_depart_time_utc", startTimestamp)
               .lt("new_depart_time_utc", endTimestamp)
               .findList();
  }

  public static List<FlightAlert> findActiveUTCDepart(long startTimestamp, long endTimestamp) {
    ArrayList<Integer> statuses = new ArrayList<>();
    statuses.add(FlightAlertStatus.ACTIVE.ordinal());
    statuses.add(FlightAlertStatus.CONFIRMED.ordinal());

    return find.where()
               .eq("rule_id", "")
               .in("status", statuses)
               .ge("new_depart_time_utc", startTimestamp)
               .lt("new_depart_time_utc", endTimestamp)
               .findList();
  }

  public static FlightAlert findLatestByFlight (String airlineCode, int flightId) {

    if (airlineCode != null && flightId > 0) {
      List<FlightAlert> distinctFlights = FlightAlert.find.select("departAirport").setDistinct(true).where()
                      .eq("airline_code", airlineCode.toUpperCase())
                      .eq("flight_id", flightId)
                      .isNotNull("new_depart_time")
                      .isNotNull("new_arrive_time")
                      .findList();

      if (distinctFlights != null && distinctFlights.size() == 1) {
        List<FlightAlert> alerts = FlightAlert.find.where()
                                                   .eq("airline_code", airlineCode.toUpperCase())
                                                   .eq("flight_id", flightId)
                                                   .isNotNull("new_depart_time")
                                                   .isNotNull("new_arrive_time")
                                                   .orderBy("lastupdatedtimestamp desc")
                                                   .setMaxRows(1)
                                                   .findList();

        if (alerts != null && alerts.size() > 0) {
          return alerts.get(0);
        }
      }
    }

    return null;
  }
  public String getArrivalAirport() {
    return arrivalAirport;
  }

  public void setArrivalAirport(String arrivalAirport) {
    this.arrivalAirport = arrivalAirport;
  }

  public Long getOrigArriveTime() {
    return origArriveTime;
  }

  public void setOrigArriveTime(Long origArriveTime) {
    this.origArriveTime = origArriveTime;
  }

  public String getDepartTimeZone() {
    return departTimeZone;
  }

  public void setDepartTimeZone(String departTimeZone) {
    this.departTimeZone = departTimeZone;
  }

  public String getArriveTimeZone() {
    return arriveTimeZone;
  }

  public void setArriveTimeZone(String arriveTimeZone) {
    this.arriveTimeZone = arriveTimeZone;
  }

  public Long getNewDepartTime() {
    return newDepartTime;
  }

  public void setNewDepartTime(Long newDepartTime) {
    this.newDepartTime = newDepartTime;
  }

  public Long getNewArriveTime() {
    return newArriveTime;
  }

  public void setNewArriveTime(Long newArriveTime) {
    this.newArriveTime = newArriveTime;
  }

  public String getNewDepartTerminal() {
    return newDepartTerminal;
  }

  public void setNewDepartTerminal(String newDepartTerminal) {
    this.newDepartTerminal = newDepartTerminal;
  }

  public String getNewDepartGate() {
    return newDepartGate;
  }

  public void setNewDepartGate(String newDepartGate) {
    this.newDepartGate = newDepartGate;
  }

  public String getNewArriveTerminal() {
    return newArriveTerminal;
  }

  public void setNewArriveTerminal(String newArriveTerminal) {
    this.newArriveTerminal = newArriveTerminal;
  }

  public String getNewArriveGate() {
    return newArriveGate;
  }

  public void setNewArriveGate(String newArriveGate) {
    this.newArriveGate = newArriveGate;
  }

  public Long getLastEventTimestamp() {
    return lastEventTimestamp;
  }

  public void setLastEventTimestamp(Long lastEventTimestamp) {
    this.lastEventTimestamp = lastEventTimestamp;
  }

  public Long getFlightAlertId() {
    return flightAlertId;
  }

  public void setFlightAlertId(Long flightAlertId) {
    this.flightAlertId = flightAlertId;
  }

  public String getRuleId() {
    return ruleId;
  }

  public void setRuleId(String ruleId) {
    this.ruleId = ruleId;
  }

  public String getAirlineCode() {
    return airlineCode;
  }

  public void setAirlineCode(String airlineCode) {
    this.airlineCode = airlineCode;
  }

  public long getFlightId() {
    return flightId;
  }

  public void setFlightId(long flightId) {
    this.flightId = flightId;
  }

  public String getDepartAirport() {
    return departAirport;
  }

  public void setDepartAirport(String departAirport) {
    this.departAirport = departAirport;
  }

  public Long getOrigDepartTime() {
    return origDepartTime;
  }

  public void setOrigDepartTime(Long origDepartTime) {
    this.origDepartTime = origDepartTime;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getResponse() {
    return response;
  }

  public void setResponse(String response) {
    this.response = response;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public String getDepartAirportName() {
    return departAirportName;
  }

  public void setDepartAirportName(String departAirportName) {
    this.departAirportName = departAirportName;
  }

  public String getArrivalAirportName() {
    return arrivalAirportName;
  }

  public void setArrivalAirportName(String arrivalAirportName) {
    this.arrivalAirportName = arrivalAirportName;
  }

  public String getAirlineName() {
    return airlineName;
  }

  public void setAirlineName(String airlineName) {
    this.airlineName = airlineName;
  }

  public Long getNewDepartTimeUtc() {
    return newDepartTimeUtc;
  }

  public void setNewDepartTimeUtc(Long newDepartTimeUtc) {
    this.newDepartTimeUtc = newDepartTimeUtc;
  }

  public Long getNewArriveTimeUtc() {
    return newArriveTimeUtc;
  }

  public void setNewArriveTimeUtc(Long newArriveTimeUtc) {
    this.newArriveTimeUtc = newArriveTimeUtc;
  }
}
