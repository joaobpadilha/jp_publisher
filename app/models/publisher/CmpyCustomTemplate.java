package models.publisher;

import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-01-22
 * Time: 8:20 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="cmpy_custom_template")
public class CmpyCustomTemplate  extends Model {
    public String path;
    public String note;

    //;; separated name value pair e.g. param1=value 1, param2=value 222, param3 = value 3132323

    @Id
    public String pk;

    @Constraints.Required
    public String cmpyid;
    public String name;
    public boolean isfiletemplate;
    public boolean isactive;
    public int status;
    public int templatetype;

    public Long createdtimestamp;
    public Long lastupdatedtimestamp;

    public String createdby;
    public String modifiedby;

    @Version
    public int version;

    public static Model.Finder<String,CmpyCustomTemplate> find = new Finder(CmpyCustomTemplate.class);

    public static List<CmpyCustomTemplate> findByCmpy (String cmpyid) {
        return find.where().eq("cmpyid", cmpyid).findList();
    }

    public static enum TEMPLATE_TYPE  {DOC_PDF, BOOKING_PDF, WEB_LOGIN, WEB_MAIN, EMAIL_PASSENGER, EMAIL_AGENCY, TRIP_PDF, BOOKINGS_DOC_PDF, FULL_EMAIL_PASSENGER};


    public static CmpyCustomTemplate findActiveByCmpyType (String cmpyid, int templateType) {
        List<CmpyCustomTemplate> results =  find.where().eq("cmpyid", cmpyid).eq("templatetype",templateType).eq("isactive",true).eq("status", 0).findList();
        if (results != null && results.size() > 0) {
            return results.get(0);
        }

        return null;
    }

    public int getTemplatetype() {
        return templatetype;
    }

    public void setTemplatetype(int templatetype) {
        this.templatetype = templatetype;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getCmpyid() {
        return cmpyid;
    }

    public void setCmpyid(String cmpyid) {
        this.cmpyid = cmpyid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isIsfiletemplate() {
        return isfiletemplate;
    }

    public void setIsfiletemplate(boolean isfiletemplate) {
        this.isfiletemplate = isfiletemplate;
    }

    public boolean isIsactive() {
        return isactive;
    }

    public void setIsactive(boolean isactive) {
        this.isactive = isactive;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getCreatedtimestamp() {
        return createdtimestamp;
    }

    public void setCreatedtimestamp(Long createdtimestamp) {
        this.createdtimestamp = createdtimestamp;
    }

    public Long getLastupdatedtimestamp() {
        return lastupdatedtimestamp;
    }

    public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
        this.lastupdatedtimestamp = lastupdatedtimestamp;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(String modifiedby) {
        this.modifiedby = modifiedby;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
