package models.publisher;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import com.mapped.publisher.persistence.RecordState;
import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.*;

/**
 * Library of pages cached with destination guide contents
 * Created by surge on 2014-12-11.
 */
@Entity @Table(name = "library_page")
public class LibraryPage
    extends Model {
  public static Model.Finder<String, LibraryPage> find = new Finder(LibraryPage.class);

  @Id
  private String pageId;
  @Constraints.Required
  private String domain;
  @Constraints.Required
  private String url;
  private String searchTerm;
  private String author;
  @Constraints.Required
  private String locale;
  private String title;
  @Constraints.Required
  private String content;
  @Column(name = "state") @Enumerated(EnumType.STRING)
  private RecordState state;
  @Constraints.Required
  private long createdtimestamp;
  @Constraints.Required
  private String createdby;
  @Version
  private int version;


  public static LibraryPage buildLibraryPage(String url, String userId) {
    LibraryPage lp = new LibraryPage();
    lp.setPageId(urlHash(url));
    lp.setUrl(url);
    lp.setCreatedby(userId);
    lp.setState(RecordState.ACTIVE);
    lp.setCreatedtimestamp(System.currentTimeMillis());
    return lp;
  }

  public static LibraryPage findPage(String url) {
    return find.byId(urlHash(url));
  }

  public static String urlHash(String url) {
    return Hashing.sha256()
                  .hashString(url, Charsets.UTF_8)
                  .toString();
  }

  public RecordState getState() {
    return state;
  }

  public void setState(RecordState state) {
    this.state = state;
  }

  public String getPageId() {
    return pageId;
  }

  public void setPageId(String pageId) {
    this.pageId = pageId;
  }

  public String getDomain() {
    return domain;
  }

  public void setDomain(String domain) {
    this.domain = domain;
  }

  public LibraryPage withDomain(String domain) {
    setDomain(domain);
    return this;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getSearchTerm() {
    return searchTerm;
  }

  public void setSearchTerm(String searchTerm) {
    this.searchTerm = searchTerm;
  }

  public LibraryPage withSearchTerm(String in) {
    setSearchTerm(in);
    return this;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public LibraryPage withAuthor(String in) {
    setAuthor(in);
    return this;
  }

  public String getLocale() {
    return locale;
  }

  public void setLocale(String locale) {
    this.locale = locale;
  }

  public LibraryPage withLocale(String in) {
    setLocale(in);
    return this;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public LibraryPage withTitle(String in) {
    setTitle(in);
    return this;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public LibraryPage withContent(String in) {
    setContent(in);
    return this;
  }

  public long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
