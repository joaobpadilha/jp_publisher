package models.publisher;

import com.avaje.ebean.Expr;
import com.mapped.publisher.persistence.PoiImportRS;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2015-02-27.
 */
@Entity @Table(name = "poi_import")
public class PoiImport
    extends Model {
  public static Model.Finder<Long, PoiImport> find = new Finder(PoiImport.class);

  @Id
  private Long importId;
  @Enumerated(value = EnumType.STRING)
  private PoiImportRS.ImportState state;
  private Integer srcId;
  private Long poiId;
  //private byte[] hash;
  private Integer typeId;
  private Long importTs;
  @Column(name = "src_reference")
  private String srcReference;
  private String srcFilename;
  private String name;
  private String countryName;
  private String countryCode;
  private String phone;
  private String region;
  private String city;
  private String address;
  private String postalCode;
  private Float locLat;
  private Float locLong;
  private Integer score;
  @Version
  private int version;

  public static int getRowCount(int srcId, PoiImportRS.ImportState state) {
    return find.where().and(Expr.eq("src_id", srcId),
                            Expr.raw("state = cast(? as um_poi_import_state)", state.name())).findCount();
  }

  public static int getRowCountFiltered(int srcId, PoiImportRS.ImportState state, String filter) {
    return find.where()
               .and(Expr.eq("src_id", srcId), Expr.raw("state = cast(? as um_poi_import_state)", state.name()))
               .ilike("name", "%" + filter + "%")
               .findCount();
  }

  public static List<PoiImport> getFilteredPage(int srcId, PoiImportRS.ImportState state, int start, int count, String filter) {
    return find.where().and(Expr.eq("src_id", srcId),
                            Expr.raw("state = cast(? as um_poi_import_state)", state.name())).ilike("name", "%"+filter+"%")
               .orderBy("import_id").setFirstRow(start).setMaxRows(count)
               .findList();
  }

  public Long getImportId() {
    return importId;
  }

  public void setImportId(Long importId) {
    this.importId = importId;
  }

  public PoiImportRS.ImportState getState() {
    return state;
  }

  public void setState(PoiImportRS.ImportState state) {
    this.state = state;
  }

  public Integer getSrcId() {
    return srcId;
  }

  public void setSrcId(Integer srcId) {
    this.srcId = srcId;
  }

  public Long getPoiId() {
    return poiId;
  }

  public void setPoiId(Long poiId) {
    this.poiId = poiId;
  }

  public Integer getTypeId() {
    return typeId;
  }

  public void setTypeId(Integer typeId) {
    this.typeId = typeId;
  }

  public Long getImportTs() {
    return importTs;
  }

  public void setImportTs(Long importTs) {
    this.importTs = importTs;
  }

  public String getSrcReference() {
    return srcReference;
  }

  public void setSrcReference(String srcReference) {
    this.srcReference = srcReference;
  }

  public String getSrcFilename() {
    return srcFilename;
  }

  public void setSrcFilename(String srcFilename) {
    this.srcFilename = srcFilename;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCountryName() {
    return countryName;
  }

  public void setCountryName(String countryName) {
    this.countryName = countryName;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public Float getLocLat() {
    return locLat;
  }

  public void setLocLat(Float locLat) {
    this.locLat = locLat;
  }

  public Float getLocLong() {
    return locLong;
  }

  public void setLocLong(Float locLong) {
    this.locLong = locLong;
  }

  public Integer getScore() {
    return score;
  }

  public void setScore(Integer score) {
    this.score = score;
  }

  public int getVersion() {
    return version;
  }
  //private String raw; -- Will never use raw from this object.

  public void setVersion(int version) {
    this.version = version;
  }

}
