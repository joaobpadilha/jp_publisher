package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.EnumValue;

import javax.persistence.*;

/**
 * Created by surge on 2015-11-09.
 */
@Entity @Table(name = "email_type")
public class EmailType
    extends Model {

  public static Model.Finder<EmailTypeName, EmailType> find = new Finder(EmailType.class);

  @Id
  @Enumerated(value = EnumType.STRING)
  @Column(name = "name", length = 15)
  private EmailTypeName name;

  public static enum EmailTypeName {
    @EnumValue("PARSE_USER")
    PARSE_USER,
    @EnumValue("PARSE_CMPY")
    PARSE_CMPY,
    @EnumValue("PARSE_TRVL")
    PARSE_TRVL,
    @EnumValue("FEED_POI")
    FEED_POI,
    @EnumValue("FEED_TMPLT")
    FEED_TMPLT
  }

  public EmailTypeName getName() {
    return name;
  }

  public void setName(EmailTypeName name) {
    this.name = name;
  }
}
