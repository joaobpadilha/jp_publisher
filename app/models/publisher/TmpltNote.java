package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import com.mapped.publisher.common.BookingSrc;
import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

@Entity @Table(name = "tmplt_note")
public class TmpltNote
    extends Model {

  public static Finder<Long, TmpltNote> find = new Finder(TmpltNote.class);
  @Id
  public Long noteId;
  @OneToOne @JoinColumn(name = "template_id")
  public Template template;
  public String tmpltDetailId;

  public float locLat;
  public float locLong;

  public String tag;
  public String intro;
  public String description;
  public String streetAddr;
  public String city;
  public String zipcode;
  public String state;
  public String country;
  public String landmark;
  public Long noteTimestamp;
  public String pageId;

  @Enumerated(value = EnumType.STRING)
  public BookingSrc.ImportSrc importSrc;

  public String importSrcId;
  public long importTs;

  @Constraints.Required
  public String name;
  public int status;
  public int rank;

  public String createdBy;
  public Long   createdTs;
  public Long   modifiedTs;
  public String modifiedBy;
  @Version
  public int version;

  public static int maxPageRankByDestId(String tripId) {
    return find.where().eq("trip_Id", tripId).findCount();
  }

  public static List<TmpltNote> getNotesByTripId(String tripId) {
    return find.where()
               .eq("trip_id", tripId)
               .eq("status", 0)
               .orderBy()
               .asc("note_timestamp")
               .orderBy()
               .asc("rank")
               .order()
               .asc("created_timestamp")
               .findList();
  }

  public static TmpltNote getNotesByTripIdNameTag(String tripId, String noteName, String tag) {
    List<TmpltNote> n = find.where()
                            .eq("trip_id", tripId)
                            .eq("status", 0)
                            .eq("name", noteName)
                            .eq("tag", tag)
                            .findList();
    if (n != null && n.size() > 0) {
      return n.get(0);
    }
    return null;
  }

  public static int deleteByTripDetailId(String tripDetailId, String userId) {
    String s = "UPDATE trip_note set status = -1 , last_updated_timestamp = :timestamp, " +
               "" + "modified_by = :userid  where trip_detail_id = :detailid";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("timestamp", System.currentTimeMillis());
    update.setParameter("userid", userId);
    update.setParameter("detailid", tripDetailId);

    return Ebean.execute(update);
  }

  public Template getTemplate() {
    return template;
  }

  public void setTemplate(Template template) {
    this.template = template;
  }

  public String getTmpltDetailId() {
    return tmpltDetailId;
  }

  public void setTmpltDetailId(String tmpltDetailId) {
    this.tmpltDetailId = tmpltDetailId;
  }

  public Long getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Long createdTs) {
    this.createdTs = createdTs;
  }

  public Long getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Long modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public float getLocLat() {
    return locLat;
  }

  public void setLocLat(float locLat) {
    this.locLat = locLat;
  }

  public float getLocLong() {
    return locLong;
  }

  public void setLocLong(float locLong) {
    this.locLong = locLong;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public String getIntro() {
    return intro;
  }

  public void setIntro(String intro) {
    this.intro = intro;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getStreetAddr() {
    return streetAddr;
  }

  public void setStreetAddr(String streetAddr) {
    this.streetAddr = streetAddr;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getZipcode() {
    return zipcode;
  }

  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getLandmark() {
    return landmark;
  }

  public void setLandmark(String landmark) {
    this.landmark = landmark;
  }

  public Long getNoteTimestamp() {
    return noteTimestamp;
  }

  public void setNoteTimestamp(Long noteTimestamp) {
    this.noteTimestamp = noteTimestamp;
  }

  public Long getNoteId() {
    return noteId;
  }

  public void setNoteId(Long noteId) {
    this.noteId = noteId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public String getPageId() {
    return pageId;
  }

  public void setPageId(String pageId) {
    this.pageId = pageId;
  }

  public BookingSrc.ImportSrc getImportSrc() {
    return importSrc;
  }

  public void setImportSrc(BookingSrc.ImportSrc importSrc) {
    this.importSrc = importSrc;
  }

  public String getImportSrcId() {
    return importSrcId;
  }

  public void setImportSrcId(String importSrcId) {
    this.importSrcId = importSrcId;
  }

  public long getImportTs() {
    return importTs;
  }

  public void setImportTs(long importTs) {
    this.importTs = importTs;
  }
}
