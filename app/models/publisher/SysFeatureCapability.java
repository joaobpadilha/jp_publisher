package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.SqlUpdate;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2015-12-11.
 */
@Entity
@Table(name = "sys_feature_capability")
public class SysFeatureCapability
    extends Model {
  public static Finder<SysFeatureCapabilityID, SysFeatureCapability> find = new Finder(SysFeatureCapability.class);

  @Id
  SysFeatureCapabilityID pk;
  Long   createdTs;
  String createdBy;
  Long   modifiedTs;
  String modifiedBy;
  @Version
  int version;

  @Embeddable
  public static class SysFeatureCapabilityID {

    /**
     * Plan ID
     */
    @Column(name = "cap_pk")
    public Integer capPk;
    /**
     * Feature ID
     */
    @Column(name = "feat_pk ")
    public Integer featurePk;

    public Integer getCapPk() {
      return capPk;
    }

    public void setCapPk(Integer capPk) {
      this.capPk = capPk;
    }

    public Integer getFeaturePk() {
      return featurePk;
    }

    public void setFeaturePk(Integer featurePk) {
      this.featurePk = featurePk;
    }

    @Override
    public int hashCode() {
      return capPk.hashCode() * featurePk.hashCode();
    }

    @Override
    public boolean equals(Object o) {
      if (o == null) {
        return false;
      }
      if (!(o instanceof SysFeatureCapabilityID)) {
        return false;
      }
      SysFeatureCapabilityID ok = (SysFeatureCapabilityID) o;
      return ok.capPk.equals(capPk) && ok.featurePk.equals(featurePk);
    }
  }

  public static SysFeatureCapability buildFeatureCapablity(Integer featurePk, Integer capabilityPk, String userId) {
    SysFeatureCapability   fCap = new SysFeatureCapability();
    SysFeatureCapabilityID id   = new SysFeatureCapabilityID();
    id.capPk = capabilityPk;
    id.featurePk = featurePk;

    fCap.setPk(id);
    fCap.setCreatedBy(userId);
    fCap.setCreatedTs(System.currentTimeMillis());
    fCap.setModifiedBy(userId);
    fCap.setModifiedTs(fCap.createdTs);
    return fCap;
  }

  public static List<SysFeatureCapability> findByFeature(Integer featId) {
    return find.where().eq("feat_pk", featId).findList();
  }

  public static SysFeatureCapability findByPk(Integer featId, Integer capPK) {
    SysFeatureCapabilityID id = new SysFeatureCapabilityID();
    id.featurePk = featId;
    id.capPk = capPK;
    return find.byId(id);
  }

  public static int deleteAllForFeature(Integer featId) {
    String s = "DELETE FROM sys_feature_capability " + "WHERE feat_pk = :featId";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("featId", featId);
    return Ebean.execute(update);
  }

  public void markModified(String userId) {
    setModifiedTs(System.currentTimeMillis());
    setModifiedBy(userId);
  }

  public SysFeatureCapabilityID getPk() {
    return pk;
  }

  public void setPk(SysFeatureCapabilityID pk) {
    this.pk = pk;
  }

  public Long getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Long createdTs) {
    this.createdTs = createdTs;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Long getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Long modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }


}
