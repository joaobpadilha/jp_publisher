package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-18
 * Time: 6:34 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "cmpy_group")
public class CmpyGroup
    extends Model {
  public String tag;
  public String comment;

  @Id
  public String groupid;

  @Constraints.Required
  public String cmpyid;

  public String name;
  public int status;

  public Long createdtimestamp;
  public Long lastupdatedtimestamp;

  public String createdby;
  public String modifiedby;

  @Version
  public int version;

  public static Model.Finder<String, CmpyGroup> find = new Finder(CmpyGroup.class);

  public static List<CmpyGroup> findByCmpy(String cmpyId) {
    return find.where().eq("status", 0).eq("cmpyid", cmpyId).orderBy("name asc").findList();
  }

  public static int mergeCmpy(String srcCmpyId, String targetCmpyId, String userId) {
    String s = "UPDATE cmpy_group set cmpyid = :targetCmpyId, lastupdatedtimestamp = :timestamp, " +
               "" + "modifiedby = :userid  where cmpyid = :srcCmpyId and status = 0";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("timestamp", System.currentTimeMillis());
    update.setParameter("userid", userId);
    update.setParameter("srcCmpyId", srcCmpyId);
    update.setParameter("targetCmpyId", targetCmpyId);

    return Ebean.execute(update);
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getGroupid() {
    return groupid;
  }

  public void setGroupid(String groupid) {
    this.groupid = groupid;
  }

  public String getCmpyid() {
    return cmpyid;
  }

  public void setCmpyid(String cmpyid) {
    this.cmpyid = cmpyid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
