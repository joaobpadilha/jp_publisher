package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2015-11-06.
 */
@Entity
@Table(name = "file_email_attach")
public class FileEmailAttach
    extends Model {

  public static Finder<FileEmailAttachId, FileEmailAttach> find = new Finder(FileEmailAttach.class);
  @Id
  private FileEmailAttachId attachId;

  @Embeddable
  public static class FileEmailAttachId {

    @ManyToOne
    @JoinColumn(name = "file_pk", nullable = false, referencedColumnName = "pk")
    public Long filePk;
    @ManyToOne
    @JoinColumn(name = "email_pk", nullable = false, referencedColumnName = "pk")
    public Long emailPk;

    public Long getFilePk() {
      return filePk;
    }

    public void setFilePk(Long filePk) {
      this.filePk = filePk;
    }

    public Long getEmailPk() {
      return emailPk;
    }

    public void setEmailPk(Long emailPk) {
      this.emailPk = emailPk;
    }

    @Override
    public int hashCode() {
      return filePk.hashCode() * emailPk.hashCode();
    }

    @Override
    public boolean equals(Object o) {
      if (o == null) {
        return false;
      }
      if (!(o instanceof FileEmailAttachId)) {
        return false;
      }

      FileEmailAttachId ok = (FileEmailAttachId) o;
      if (ok.filePk.equals(filePk) && ok.emailPk.equals(emailPk)) {
        return true;
      }
      return false;
    }
  }

  public static FileEmailAttach newAttachment(EmailParseLog email, FileInfo file) {
    FileEmailAttach   attach = new FileEmailAttach();
    FileEmailAttachId id     = new FileEmailAttachId();
    id.emailPk = email.pk;
    id.filePk = file.pk;
    attach.setAttachId(id);
    return attach;
  }

  public FileEmailAttachId getAttachId() {
    return attachId;
  }

  public void setAttachId(FileEmailAttachId attachId) {
    this.attachId = attachId;
  }

  public List<FileEmailAttach> findByEmailId(Long emailPk) {
    return find.where().eq("email_pk", emailPk).findList();
  }

  public List<FileEmailAttach> findByFileId(Long filePk) {
    return find.where().eq("file_pk", filePk).findList();
  }
}
