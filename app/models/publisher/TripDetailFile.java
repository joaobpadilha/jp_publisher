package models.publisher;

import com.avaje.ebean.BeanState;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.SqlUpdate;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2015-03-05.
 */
@Entity
@Table(name = "trip_detail_file")
public class TripDetailFile
    extends Model {


  public static Finder<TripDetailFileId, TripDetailFile> find = new Finder(TripDetailFile.class);

  @Id
  private TripDetailFileId fileId;
  private int              rank;
  @Version
  private int              version;

  @Embeddable
  public static class TripDetailFileId {
    @Column(name = "detailsid")
    public String detailsId;
    @Column(name = "poi_file_id")
    public long   poiFileId;

    public String getDetailsId() {
      return detailsId;
    }

    public TripDetailFileId setDetailsId(String detailsId) {
      this.detailsId = detailsId;
      return this;
    }

    public long getPoiFileId() {
      return poiFileId;
    }

    public TripDetailFileId setPoiFileId(long poiFileId) {
      this.poiFileId = poiFileId;
      return this;
    }

    @Override
    public int hashCode() {
      return detailsId.hashCode() * ((int) poiFileId);
    }

    @Override
    public boolean equals(Object o) {
      if (o == null) {
        return false;
      }
      if (!(o instanceof TripDetailFileId)) {
        return false;
      }
      TripDetailFileId ok = (TripDetailFileId) o;
      return (ok.detailsId.equals(detailsId)) && (ok.poiFileId == poiFileId);
    }
  }


  /**
   * Takes this object makes it possible to re-insert as new record
   * @return
   */
  public TripDetailFile prepareForReuse(String detailsId, Long poiFileId) {
    BeanState state = Ebean.getBeanState(this);
    state.resetForInsert();
    setFileId(new TripDetailFileId().setDetailsId(detailsId).setPoiFileId(poiFileId));
    return this;
  }

  public static List<TripDetailFile> getBookingFiles(String detailsId) {
    if (detailsId == null) {
      return null;
    }
    return find.where().eq("detailsid", detailsId).orderBy("rank asc").findList();
  }


  public static int deleteBookingFiles(String detailsId) {
    String    s      = "DELETE FROM trip_detail_file WHERE detailsid = :detailsid";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("detailsid", detailsId);
    return Ebean.execute(update);
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public TripDetailFileId getFileId() {
    return fileId;
  }

  public void setFileId(TripDetailFileId fileId) {
    this.fileId = fileId;
  }

  public void setFileId(String detailsId, long poiFileId) {
    this.fileId = new TripDetailFileId();
    this.fileId.detailsId = detailsId;
    this.fileId.poiFileId = poiFileId;
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }
}
