package models.publisher;

import com.avaje.ebean.Model;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.utils.Utils;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.sql.Timestamp;
import java.time.Instant;

@Entity @Table(name = "bk_api_audit")

public class BkAPIAudit extends Model{
  @Id
  private Long pk;
  @Constraints.Required
  private String srcRecLocator;
  private String srcCmpyId;
  private Integer srcId;
  private Timestamp createdTs;
  private String data;

  @Version
  private int version;

  public static Model.Finder<Integer, BkAPIAudit> find = new Model.Finder(BkAPIAudit.class);

  public Long getPk() {
    return pk;
  }

  public void setPk(Long pk) {
    this.pk = pk;
  }

  public String getSrcRecLocator() {
    return srcRecLocator;
  }

  public void setSrcRecLocator(String srcRecLocator) {
    this.srcRecLocator = srcRecLocator;
  }

  public String getSrcCmpyId() {
    return srcCmpyId;
  }

  public void setSrcCmpyId(String srcCmpyId) {
    this.srcCmpyId = srcCmpyId;
  }

  public Integer getSrcId() {
    return srcId;
  }

  public void setSrcId(Integer srcId) {
    this.srcId = srcId;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public static BkAPIAudit build(String srcCmpyId, String srcRecLocator, int srcId, String text) {
    if (srcCmpyId != null && srcRecLocator !=null && text != null) {
      BkAPIAudit bkAPIAudit = new BkAPIAudit();
      bkAPIAudit.setPk(DBConnectionMgr.getUniqueKey());
      bkAPIAudit.setCreatedTs(Timestamp.from(Instant.now()));
      bkAPIAudit.setSrcCmpyId(srcCmpyId);
      bkAPIAudit.setSrcRecLocator(srcRecLocator);
      bkAPIAudit.setSrcId(srcId);
      bkAPIAudit.setData(text);
      return bkAPIAudit;
    }
    return null;
  }
}
