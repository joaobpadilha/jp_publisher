package models.publisher;

import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by twong on 15-08-24.
 */
@Entity @Table(name = "bk_api_src")

public class BkApiSrc extends Model {
  public static Map<Integer, String> SRC_NAMES = new HashMap<>();
  public static Map<Integer, String> PUBLIC_SRC_NAMES = new HashMap<>();


  public static Model.Finder<Integer, BkApiSrc> find = new Finder(BkApiSrc.class);

  private String description;

  @Id
  private int srcId;
  @Constraints.Required
  private String name;
  private int state;
  private long createdTs;
  private Boolean isPrivate;

  @Version
  private int version;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getSrcId() {
    return srcId;
  }

  public void setSrcId(int srcId) {
    this.srcId = srcId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getState() {
    return state;
  }

  public void setState(int state) {
    this.state = state;
  }

  public long getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(long createdTs) {
    this.createdTs = createdTs;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public static String getName (int srcId) {
    synchronized (SRC_NAMES) {
      if (SRC_NAMES.size() == 0) {
        List<BkApiSrc> all = BkApiSrc.find.all();
        for (BkApiSrc src : all) {
          SRC_NAMES.put(src.getSrcId(), src.getName());
        }
      }
    }
    return SRC_NAMES.get(srcId);
  }

  public static int getId (String name) {
    synchronized (SRC_NAMES) {
      if (SRC_NAMES.size() == 0) {
        List<BkApiSrc> all = BkApiSrc.find.all();
        for (BkApiSrc src : all) {
          SRC_NAMES.put(src.getSrcId(), src.getName());
        }
      }
    }
    for (int k: SRC_NAMES.keySet()) {
      if (SRC_NAMES.get(k).equals(name)) {
        return k;
      }
    }
    return -1;
  }

  public static Map<Integer, String> getAll () {
    synchronized (SRC_NAMES) {
      if (SRC_NAMES.size() == 0) {
        List<BkApiSrc> all = BkApiSrc.find.all();
        for (BkApiSrc src : all) {
          SRC_NAMES.put(src.getSrcId(), src.getName());
        }
      }
    }
    return SRC_NAMES;
  }

  public static Map<Integer, String> getAllPublic () {
    synchronized (PUBLIC_SRC_NAMES) {
      if (PUBLIC_SRC_NAMES.size() == 0) {
        List<BkApiSrc> all = BkApiSrc.find.all();
        for (BkApiSrc src : all) {
          if (!src.isPrivate) {
            PUBLIC_SRC_NAMES.put(src.getSrcId(), src.getName());
          }
        }
      }
    }
    return PUBLIC_SRC_NAMES;
  }

  public static List<String> getAllNames () {
    synchronized (SRC_NAMES) {
      if (SRC_NAMES.size() == 0) {
        List<BkApiSrc> all = BkApiSrc.find.all();
        for (BkApiSrc src : all) {
          SRC_NAMES.put(src.getSrcId(), src.getName());
        }
      }
    }
    List<String> names = new ArrayList<>();
    if (SRC_NAMES != null) {
      names.addAll(SRC_NAMES.values());
    }
    return names;
  }


}
