package models.publisher;

import com.avaje.ebean.*;
import com.avaje.ebean.annotation.DbJsonB;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.avaje.ebean.annotation.EnumValue;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.NoteBaseView;
import com.umapped.persistence.notes.insurance.InsuranceNote;
import com.umapped.persistence.notes.StructuredNote;
import com.umapped.persistence.notes.tableNote.TableNote;
import play.data.validation.Constraints;

import javax.persistence.*;
import javax.persistence.Version;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by twong on 15-04-24.
 */
@Entity
@Table(name = "trip_note")

public class TripNote
    extends Model
    implements JsonSupport {
  public static Model.Finder<Long, TripNote> find = new Finder(TripNote.class);

  public static enum NoteType {
    @EnumValue("DEF")
    DEFAULT(com.umapped.persistence.notes.DefaultNote.class, "views.html.trip.bookings.tripNotes.tripNote", com.mapped.publisher.view.TripBookingDetailView.class),
    @EnumValue("INS")
    INSURANCE(InsuranceNote.class, "views.html.trip.bookings.tripNotes.insuranceNote", com.mapped.publisher.view.InsuranceNoteView.class),
    @EnumValue("TAB")
    TABLENOTE(TableNote.class, "views.html.trip.bookings.tripNotes.tableNote", com.mapped.publisher.view.TableNoteView.class),
    @EnumValue("CNT")
    CONTENT(com.umapped.persistence.notes.DefaultNote.class, "views.html.trip.bookings.tripNotes.tripNote", com.mapped.publisher.view.TripBookingDetailView.class);

    private Class<? extends StructuredNote> noteClass;
    private String noteTemplate;
    private Class<? extends NoteBaseView> noteView;

    NoteType(Class<? extends StructuredNote> noteClass, String noteTemplate, Class<? extends NoteBaseView> noteView) {
      this.noteClass = noteClass;
      this.noteTemplate = noteTemplate;
      this.noteView = noteView;
    }

    public Class<? extends NoteBaseView> getNoteView() {
      return noteView;
    }

    public String getNoteTemplate() {
      return noteTemplate;
    }

    public StructuredNote getValue() {
      try {
        return this.noteClass.newInstance();
      } catch (InstantiationException e) {
        e.printStackTrace();
        return null;
      } catch (IllegalAccessException e) {
        e.printStackTrace();
        return null;
      }
    }


    public NoteBaseView instantiateView() {
      try {
        return this.noteView.newInstance();
      } catch (InstantiationException e) {
        e.printStackTrace();
        return null;
      } catch (IllegalAccessException e) {
        e.printStackTrace();
        return null;
      }
    }

  }

  @Id
  Long                 noteId;
  String               tripDetailId;
  float                locLat;
  float                locLong;
  String               tag;
  String               intro;
  String               description;
  String               streetAddr;
  String               city;
  String               zipcode;
  String               state;
  String               country;
  String               landmark;
  Long                 noteTimestamp;
  String               pageId;
  @Enumerated(value = EnumType.STRING)
  BookingSrc.ImportSrc importSrc;
  String               importSrcId;
  long                 importTs;

  @Constraints.Required
  String name;
  @OneToOne
  @JoinColumn(name = "trip_Id")
  Trip   trip;
  int    status;
  int    rank;
  Long   createdTimestamp;
  Long   lastUpdatedTimestamp;
  String createdBy;
  String modifiedBy;
  @Enumerated(value = EnumType.STRING)
  NoteType type;

  @DbJsonB
  private Map<String, Object> noteData;

  @Transient
  private StructuredNote note;

  @Version
  int    version;

  /**
   * Takes this object makes it possible to re-insert as new record
   * @return
   */
  public TripNote prepareForReuse(String createdBy, String modifiedBy) {
    BeanState state = Ebean.getBeanState(this);
    state.resetForInsert();
    setNoteId(DBConnectionMgr.getUniqueLongId());
    setModifiedBy(modifiedBy);
    setCreatedBy(createdBy);
    setLastUpdatedTimestamp(System.currentTimeMillis());
    setCreatedTimestamp(lastUpdatedTimestamp);
    return this;
  }

  public static TripNote buildTripNote(Trip trip, String userId) {
    TripNote tn = new TripNote();
    tn.setTrip(trip);
    tn.setNoteId(DBConnectionMgr.getUniqueLongId());
    tn.setStatus(APPConstants.STATUS_ACTIVE);
    tn.setCreatedBy(userId);
    tn.setCreatedTimestamp(System.currentTimeMillis());
    tn.setModifiedBy(userId);
    tn.setLastUpdatedTimestamp(tn.createdTimestamp);
    return tn;
  }


  public static int tripNoteCount(String tripId) {
    return find.where().eq("trip_Id", tripId).findCount();
  }

  public static List<TripNote> getNotesByTripId(String tripId) {
    return find
        .where()
        .eq("trip_id", tripId)
        .eq("status", 0)
        .orderBy()
        .asc("note_timestamp")
        .orderBy()
        .asc("rank")
        .order()
        .asc("created_timestamp")
        .findList();
  }

  public static TripNote getNotesByTripIdNameTag(String tripId, String noteName, String tag) {
    List<TripNote> n = find
        .where()
        .eq("trip_id", tripId)
        .eq("status", 0)
        .eq("name", noteName)
        .eq("tag", tag)
        .findList();
    if (n != null && n.size() > 0) {
      return n.get(0);
    }
    return null;
  }

  public static List<TripNote> getNotesByTripIdName(String tripId, String noteName, Long timestamp) {
    List<TripNote> n = null;
    if ( timestamp == null || timestamp == 0) {
      n = find.where().eq("trip_id", tripId).eq("status", 0).eq("name", noteName).findList();
    } else {
      n = find.where()
              .eq("trip_id", tripId)
              .eq("status", 0)
              .eq("name", noteName)
              .eq("note_timestamp", timestamp)
              .findList();
    }
    if (n != null && n.size() > 0) {
      return n;
    }
    return null;
  }

  public static List<TripNote> getNotesByTripIdNames(String tripId, List<String> noteName) {
    List<TripNote> n = null;
    n = find.where().eq("trip_id", tripId).eq("status", 0).in("name", noteName).orderBy("createdTimestamp desc").findList();

    if (n != null && n.size() > 0) {
      return n;
    }
    return null;
  }

  public static TripNote getNotesByTripIdImportSrcId(String tripId, String importSrcId) {
    List<TripNote> n = find
        .where()
        .eq("trip_id", tripId)
        .eq("status", 0)
        .eq("import_src_id", importSrcId)
        .findList();
    if (n != null && n.size() > 0) {
      return n.get(0);
    }
    return null;
  }

    public static int deleteByTripDetailId(String tripDetailId, String userId) {
    String s = "UPDATE trip_note set status = -1 , last_updated_timestamp = :timestamp, " + "modified_by = :userid  " +
               "where trip_detail_id = :detailid";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("timestamp", System.currentTimeMillis());
    update.setParameter("userid", userId);
    update.setParameter("detailid", tripDetailId);

    return Ebean.execute(update);
  }

  public static int resetTripDetailId(String tripDetailId, String userId, long timestamp) {
    String s = "UPDATE trip_note set trip_detail_id = null , note_timestamp = :noteTimestamp, last_updated_timestamp = :timestamp, " + "modified_by = :userid  " +
            "where trip_detail_id = :detailid";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    if (timestamp > 0) {
      update.setParameter("noteTimestamp", timestamp);
    } else {
      update.setParameter("noteTimestamp", null);
    }
    update.setParameter("timestamp", System.currentTimeMillis());
    update.setParameter("userid", userId);
    update.setParameter("detailid", tripDetailId);

    return Ebean.execute(update);
  }

  public static List<TripNote> getNotesByTripIdImportSrc(String tripId, List<String> importSrcIds) {
    return find
        .where()
        .eq("trip_id", tripId)
        .eq("status", 0)
        .in("import_src_id", importSrcIds)
        .findList();
  }

  public static List<TripNote> getDeleteNotesByTripIdImportSrc(String tripId, String importSrcId, String userid) {
    return find
        .where()
        .eq("trip_id", tripId)
        .ne("status", 0)
        .eq("import_src_id", importSrcId)
        .ne("modifiedBy", userid)
        .ne("modifiedBy", "System")
        .ne("modifiedBy", "system")
        .ne("modifiedBy", "API")
        .eq("import_src", "API")
        .findList();
  }

  public static List<TripNote> getNotesByTripIdNameDetailId(String tripId, String name, String tripDetailId) {
    return find
        .where()
        .eq("trip_id", tripId)
        .eq("name", name)
        .eq("trip_detail_id", tripDetailId)
        .eq("status", 0)
        .findList();
  }

  public static TripNote findActiveUniqueAfarGuideInTrip(String tripId, String name) {
    return find
        .where()
        .eq("trip_id", tripId)
        .eq("name", name)
        .eq("type", TripNote.NoteType.CONTENT)
        .icontains("tag", "Afar")
        .eq("status", 0)
        .findUnique();
  }

  public static List<TripNote> findAllActiveAfarGuidesInTrip(String tripId) {
    return find
        .where()
        .eq("trip_id", tripId)
        .eq("type", TripNote.NoteType.CONTENT)
        .icontains("tag", APPConstants.AFAR_NOTE_TAG)
        .eq("status", 0)
        .findList();
  }

  public static List<TripNote> findAllActiveWcitiesInTrip(String tripId) {
    return find
        .where()
        .eq("trip_id", tripId)
        .eq("type", TripNote.NoteType.CONTENT)
        .icontains("tag", APPConstants.WCITIES_NOTE_TAG)
        .eq("status", 0)
        .findList();
  }

  public String getTripDetailId() {
    return tripDetailId;
  }

  public void setTripDetailId(String tripDetailId) {
    this.tripDetailId = tripDetailId;
  }

  public float getLocLat() {
    return locLat;
  }

  public void setLocLat(float locLat) {
    this.locLat = locLat;
  }

  public float getLocLong() {
    return locLong;
  }

  public void setLocLong(float locLong) {
    this.locLong = locLong;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public String getIntro() {
    return intro;
  }

  public void setIntro(String intro) {
    this.intro = intro;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getStreetAddr() {
    return streetAddr;
  }

  public void setStreetAddr(String streetAddr) {
    this.streetAddr = streetAddr;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getZipcode() {
    return zipcode;
  }

  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getLandmark() {
    return landmark;
  }

  public void setLandmark(String landmark) {
    this.landmark = landmark;
  }

  public Long getNoteTimestamp() {
    return noteTimestamp;
  }

  public void setNoteTimestamp(Long noteTimestamp) {
    this.noteTimestamp = noteTimestamp;
  }

  public Long getNoteId() {
    return noteId;
  }

  public void setNoteId(Long noteId) {
    this.noteId = noteId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Trip getTrip() {
    return trip;
  }

  public void setTrip(Trip trip) {
    this.trip = trip;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }

  public Long getCreatedTimestamp() {
    return createdTimestamp;
  }

  public void setCreatedTimestamp(Long createdTimestamp) {
    this.createdTimestamp = createdTimestamp;
  }

  public Long getLastUpdatedTimestamp() {
    return lastUpdatedTimestamp;
  }

  public void setLastUpdatedTimestamp(Long lastUpdatedTimestamp) {
    this.lastUpdatedTimestamp = lastUpdatedTimestamp;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public String getPageId() {
    return pageId;
  }

  public void setPageId(String pageId) {
    this.pageId = pageId;
  }

  public BookingSrc.ImportSrc getImportSrc() {
    return importSrc;
  }

  public void setImportSrc(BookingSrc.ImportSrc importSrc) {
    this.importSrc = importSrc;
  }

  public String getImportSrcId() {
    return importSrcId;
  }

  public void setImportSrcId(String importSrcId) {
    this.importSrcId = importSrcId;
  }

  public long getImportTs() {
    return importTs;
  }

  public void setImportTs(long importTs) {
    this.importTs = importTs;
  }

  public NoteType getType() {
    return type;
  }

  public void setType(NoteType type) {
    this.type = type;
  }

  public Map<String, Object> getNoteData() {
    return noteData;
  }

  public void setNoteData(Map<String, Object> noteData) {
    this.noteData = noteData;
  }

  public StructuredNote getNote() {
    if (note == null) {
      if (noteData != null) {
        note = unmarshal(noteData, StructuredNote.class);
      }
    }
    return note;
  }

  public void setNote(StructuredNote note) {
    this.note = note;
    intro = note.renderNoteByType(note);
  }

  /**
   * This is sync the transient reservation back to the persisted field reservationData
   * before the persiste happends
   *
   * @throws IOException
   */
  @PrePersist
  @PreUpdate
  public void preparePersist() throws IOException {
    if (note != null) {
      noteData = marshal(note);
    }
    if (this.type == null) {
      this.type = NoteType.DEFAULT;
    }
  }
}
