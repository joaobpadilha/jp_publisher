package models.publisher.utils.reservation.migration;

import com.mapped.publisher.utils.Log;
import com.umapped.persistence.reservation.flight.UmFlight;
import com.umapped.persistence.reservation.flight.UmFlightReservation;
import com.umapped.persistence.reservation.transfer.UmTransfer;
import com.umapped.persistence.reservation.transfer.UmTransferReservation;

import models.publisher.FlightBooking;
import models.publisher.TmpltDetails;
import models.publisher.TmpltFlight;
import models.publisher.TmpltTransport;
import models.publisher.TransportBooking;
import models.publisher.TripDetail;

public class UmTransferReservationBuilder {
  public UmTransferReservation build(TripDetail td, TransportBooking transport) {
    UmTransferReservation reservation = new UmTransferReservation();

    UmTransfer transfer = reservation.getTransfer();
    transfer.getProvider().name = transport.name;
    transfer.getProvider().contactPoint = transport.contact;
    transfer.getDropoffLocation().name = transport.dropofflocation;
    transfer.getPickupLocation().name = transport.pickuplocation;

    reservation.setStartDateTime(td.getStarttimestamp());
    reservation.setEndDateTime(td.getEndtimestamp());

    reservation.setNotesPlainText(td.getComments());
    return reservation;
  }

  public UmTransferReservation build(TmpltDetails template, TmpltTransport transport) {
    UmTransferReservation reservation = new UmTransferReservation();

    UmTransfer transfer = reservation.getTransfer();
    transfer.getProvider().name = transport.name;
    transfer.getProvider().contactPoint = transport.contact;
    transfer.getDropoffLocation().name = transport.dropofflocation;
    transfer.getPickupLocation().name = transport.pickuplocation;

    reservation.setStartDateTime(template.getStarttimestamp());
    reservation.setEndDateTime(template.getEndtimestamp());

    reservation.setNotesPlainText(template.getComments());
    return reservation;
  }

}
