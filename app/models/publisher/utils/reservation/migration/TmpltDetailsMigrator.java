package models.publisher.utils.reservation.migration;

import java.util.EnumMap;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.umapped.persistence.enums.ReservationType;

import models.publisher.TmpltDetails;
import models.utils.migration.AbstractMigrator;
import models.utils.migration.Migrator;

@Singleton
public class TmpltDetailsMigrator extends AbstractMigrator<TmpltDetails> {

  private EnumMap<ReservationType, Migrator<TmpltDetails>> migratorMap;
  private Migrator<TmpltDetails> NoOpMigrator = (entity) -> { return false; };
  
  @Inject
  public TmpltDetailsMigrator() {
    migratorMap = new EnumMap<>(ReservationType.class);
    migratorMap.put(ReservationType.FLIGHT, new FlightTmpltDetailsMigrator());
    migratorMap.put(ReservationType.HOTEL, new HotelTmpltDeteailMigrator());
    migratorMap.put(ReservationType.CRUISE, new CruiseTmpltDetailMigrator());
    migratorMap.put(ReservationType.TRANSPORT, new TransportTmpltDetailMigrator());
    migratorMap.put(ReservationType.ACTIVITY, new ActivityTmpltDetailMigrator());
    
    // set migrator for sub types of activity and transport
    for (ReservationType rt : ReservationType.values()) {
      ReservationType prt = rt.getParent();
      if (ReservationType.ACTIVITY == prt)
        migratorMap.put(rt, migratorMap.get(ReservationType.ACTIVITY));
      else if (ReservationType.TRANSPORT == prt)
        migratorMap.put(rt, migratorMap.get(ReservationType.TRANSPORT));
    }
  }

  /**
   * If the reservationData is not null, it has been migrated
   */
  @Override
  public boolean isMigrated(TmpltDetails entity) {
    return entity.getReservationData() != null;
  }

  @Override
  protected boolean doMigrate(TmpltDetails entity) {
    Migrator<TmpltDetails> m = migratorMap.getOrDefault(entity.detailtypeid, NoOpMigrator);
    return m.migrate(entity);
  }
}
