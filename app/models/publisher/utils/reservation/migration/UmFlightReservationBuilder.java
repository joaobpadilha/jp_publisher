package models.publisher.utils.reservation.migration;

import java.util.ArrayList;
import java.util.List;

import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.reservation.UmTraveler;
import com.umapped.persistence.reservation.flight.UmFlight;
import com.umapped.persistence.reservation.flight.UmFlightReservation;
import com.umapped.persistence.reservation.flight.UmFlightTraveler;

import models.publisher.FlightBooking;
import models.publisher.TmpltDetails;
import models.publisher.TmpltFlight;
import models.publisher.TripDetail;
import org.apache.commons.lang3.StringUtils;

/**
 * Builder to build the UmFlightReservation from TripDetail comment and
 * FlightBooking
 * 
 * @author wei
 *
 */
public class UmFlightReservationBuilder {
  public UmFlightReservation build(TripDetail td, FlightBooking fb) {
    UmFlightReservation reservation = new UmFlightReservation();

    try {
      UmFlight flight = reservation.getFlight();
      if (fb != null) {
        flight.flightNumber = fb.getFlightid();
      }

      reservation.setStartDateTime(td.getStarttimestamp());
      reservation.setEndDateTime(td.getEndtimestamp());

      parseComment(reservation, td.getComments());
    } // if it fails the parsing, set the comments to the notes field
    catch (Throwable t) {
      Log.err("Fail to migrate Flight", t);
      reservation.setNotesPlainText(td.getComments());
    }
    return reservation;
  }

  public UmFlightReservation build(TmpltDetails template, TmpltFlight tf) {
    UmFlightReservation reservation = new UmFlightReservation();

    try {
      UmFlight flight = reservation.getFlight();
      
      if (tf != null) {
        flight.flightNumber = tf.getFlightid();
      }
      
      reservation.setStartDateTime(template.getStarttimestamp());
      reservation.setEndDateTime(template.getEndtimestamp());

      parseComment(reservation, template.getComments());
      // remove passenger information in template
      reservation.setTravelers(null);
    } // if it fails the parsing, set the comments to the notes field
    catch (Throwable t) {
      Log.err("Fail to migrate Flight template", t);
      reservation.setNotesPlainText(template.getComments());
    }
    return reservation;    
  }
  
  public void parseComment(UmFlightReservation reservation, String comment) {
    if (comment == null || comment.length() == 0) {
      return;
    }

    List<UmTraveler> travelers = new ArrayList<>();
    UmFlight flight = reservation.getFlight();

    reservation.setTravelers(travelers);

    String[] tokens = comment.split("\n");
    StringBuilder sb = new StringBuilder();

    boolean hasPassengers = comment.contains("Passengers:");

    UmFlightTraveler ft = null;

    for (String token : tokens) {
      if (token.trim().length() == 0 || token.contains("Passengers:")) {
        sb.append(System.lineSeparator());
        continue;
      }
      if (token.startsWith("Departure Terminal:")) {
        flight.departureTerminal = token.substring(token.indexOf("Departure Terminal:") + 19).trim();
        continue;
      }
      if (token.startsWith("Departure Terminal")) {
        flight.departureTerminal = token.substring(token.indexOf("Departure Terminal") + 18).trim();
        continue;
      }

      if (token.startsWith("Arrival Terminal:")) {
        flight.arrivalTerminal = token.substring(token.indexOf("Arrival Terminal:") + 17).trim();
        continue;
      }
      if (token.startsWith("Arrival Terminal")) {
        flight.arrivalTerminal = token.substring(token.indexOf("Arrival Terminal") + 16).trim();
        continue;
      }

      if (hasPassengers) {
        if (token.startsWith("Name:")) {
          ft = new UmFlightTraveler();
          travelers.add(ft);
          parseName(getValue(token), ft);
          continue;
        }

        if (token.startsWith("Seat") && ft != null) {
          ft.setSeat(getValue(token));
          continue;
        }

        if (token.startsWith("Class") && ft != null) {
          ft.getSeatClass().seatCategory = getValue(token);
          continue;
        }

        if (token.startsWith("eTicket") && ft != null) {
          ft.setTicketNumber(getValue(token));
          continue;
        }

        if (token.startsWith("Frequent Flyer") && ft != null) {
          ft.getProgram().membershipNumber = getValue(token);
          continue;
        }

        if (token.startsWith("Meal") && ft != null) {
          ft.setMeal(getValue(token));
          continue;
        }
      }

      sb.append(token).append(System.lineSeparator());
    }
    String resetComment = Utils.cleanSpecials(sb.toString());
    reservation.setNotesPlainText(resetComment);
  }

  public void parseName(String name, UmFlightTraveler ft) {
    if (name != null) {
      boolean firstNameFirst = false;
      name = name.trim();
      if (name.endsWith(" MR") || name.endsWith(" MRS") || name.endsWith(" MS") || name.endsWith(" MISS")) {
        firstNameFirst = false;
        name = name.replace(" MR", "").trim();
        name = name.replace(" MRS", "").trim();
        name = name.replace(" MS", "").trim();
        name = name.replace(" MISS", "").trim();
      } else if (name.startsWith("MR ") || name.startsWith("MRS ") || name.startsWith("MS ") || name.startsWith("MISS ")) {
        firstNameFirst = true;
        name = name.replace("MR ", "").trim();
        name = name.replace("MRS ", "").trim();
        name = name.replace("MS ", "").trim();
        name = name.replace("MISS ", "").trim();
      } else if (name.endsWith(" Mr") || name.endsWith(" Mrs") || name.endsWith(" Ms") || name.endsWith(" Miss")) {
        firstNameFirst = false;
        name = name.replace(" Mr", "").trim();
        name = name.replace(" Mrs", "").trim();
        name = name.replace(" Ms", "").trim();
        name = name.replace(" Miss", "").trim();
      } else if (name.startsWith("Mr ") || name.startsWith("Mrs ") || name.startsWith("Ms ") || name.startsWith("Miss ")) {
        firstNameFirst = true;
        name = name.replace("Mr ", "").trim();
        name = name.replace("Mrs ", "").trim();
        name = name.replace("Ms ", "").trim();
        name = name.replace("Miss ", "").trim();
      } else if (StringUtils.isAlphaSpace(name)) {
        firstNameFirst = true;
      }


      if (name.indexOf("/") > 0 && name.indexOf("/") != name.length()) {
        ft.setFamilyName(name.substring(0, name.indexOf("/")).trim());
        ft.setGivenName(name.substring(name.indexOf("/") + 1).trim());
      }
      else {
        if (name.split("\\w+").length > 1) {
          if (firstNameFirst) {
            if (name.indexOf("/") > 0 && name.indexOf("/") != name.length()) {
              ft.setGivenName(name.substring(0, name.indexOf("/")).trim());
              ft.setFamilyName(name.substring(name.indexOf("/") + 1).trim());
            } else if (name.indexOf(",") > 0 && name.indexOf(",") != name.length()) {
              ft.setGivenName(name.substring(0, name.indexOf(",")).trim());
              ft.setFamilyName(name.substring(name.indexOf(",") + 1).trim());
            } else {
              ft.setFamilyName(name.substring(name.lastIndexOf(" ") + 1).trim());
              ft.setGivenName(name.substring(0, name.lastIndexOf(' ')).trim());
            }
          } else {
            if (name.indexOf("/") > 0 && name.indexOf("/") != name.length()) {
              ft.setFamilyName(name.substring(0, name.indexOf("/")).trim());
              ft.setGivenName(name.substring(name.indexOf("/") + 1).trim());
            } else if (name.indexOf(",") > 0 && name.indexOf(",") != name.length()) {
              ft.setFamilyName(name.substring(0, name.indexOf(",")).trim());
              ft.setGivenName(name.substring(name.indexOf(",") + 1).trim());
            } else {
              ft.setGivenName(name.substring(name.lastIndexOf(" ") + 1).trim());
              ft.setFamilyName(name.substring(0, name.lastIndexOf(' ')).trim());
            }
          }

        }
        else {
          // use the given name here
          ft.setGivenName(name);
        }
      }
    }
  }

  public String getValue(String s) {
    if (s != null && s.contains(":")) {
      if (s.startsWith("Name")) {
        s = Utils.cleanSpecials1(s);
      } else {
        s = Utils.cleanSpecials(s);
      }
      return s.substring(s.indexOf(":") + 1).trim();
    }
    return "";
  }
}
