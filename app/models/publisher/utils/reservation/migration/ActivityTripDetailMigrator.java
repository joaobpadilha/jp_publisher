package models.publisher.utils.reservation.migration;

import com.mapped.publisher.utils.Log;
import com.umapped.persistence.reservation.activity.UmActivityReservation;

import models.publisher.ActivityBooking;
import models.publisher.TripDetail;
import models.utils.migration.Migrator;

public class ActivityTripDetailMigrator implements Migrator<TripDetail> {
  private UmActivityReservationBuilder builder = new UmActivityReservationBuilder();
  
  @Override
  public boolean migrate(TripDetail entity) {
    
    ActivityBooking activity = ActivityBooking.find.byId(entity.detailsid);
    Log.info("Migrate a activity trip_detail");
    UmActivityReservation reservation = builder.build(entity, activity);
    entity.setReservation(reservation);
    return true;
  }
}
