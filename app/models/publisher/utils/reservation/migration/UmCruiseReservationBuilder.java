package models.publisher.utils.reservation.migration;

import java.util.ArrayList;
import java.util.List;

import com.umapped.persistence.reservation.UmTraveler;
import com.umapped.persistence.reservation.cruise.UmCruiseReservation;
import com.umapped.persistence.reservation.cruise.UmCruiseTraveler;

import models.publisher.BookingCruise;
import models.publisher.TmpltCruise;
import models.publisher.TmpltDetails;
import models.publisher.TripDetail;

public class UmCruiseReservationBuilder {

  public UmCruiseReservation build(TripDetail entity, BookingCruise cruise) {
    UmCruiseReservation reservation = new UmCruiseReservation();
    
    UmCruiseTraveler traveler = new UmCruiseTraveler();
    traveler.setBedding(cruise.getBedding());
    traveler.setCabinNumber(cruise.getCabinNumber());
    traveler.setCategory(cruise.getCategory());
    traveler.setDeckNumber(cruise.getDeck());
    traveler.setDiningPlan(cruise.getMeal());
    List<UmTraveler> travelers = new ArrayList<>();
    travelers.add(traveler);
    reservation.setTravelers(travelers);
    reservation.getCruise().getProvider().name = cruise.getCmpyName();
    reservation.getCruise().name = cruise.getName();
    reservation.setNotesPlainText(entity.getComments());
    
    return reservation;    
  }
  
  public UmCruiseReservation build(TmpltDetails entity, TmpltCruise cruise) {
    UmCruiseReservation reservation = new UmCruiseReservation();
    
    reservation.getCruise().getProvider().name = cruise.getCmpyName();
    reservation.getCruise().name = cruise.getName();
    reservation.setNotesPlainText(entity.getComments());
    
    return reservation;    
  }
}
