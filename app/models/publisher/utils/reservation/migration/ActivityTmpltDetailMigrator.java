package models.publisher.utils.reservation.migration;

import com.mapped.publisher.utils.Log;
import com.umapped.persistence.reservation.activity.UmActivityReservation;

import models.publisher.TmpltActivity;
import models.publisher.TmpltDetails;
import models.utils.migration.Migrator;

public class ActivityTmpltDetailMigrator implements Migrator<TmpltDetails> {
  private UmActivityReservationBuilder builder = new UmActivityReservationBuilder();

  @Override
  public boolean migrate(TmpltDetails entity) {
    TmpltActivity activity = TmpltActivity.find.byId(entity.detailsid);
    Log.info("Migrate a activity trip_detail");
    UmActivityReservation reservation = builder.build(entity, activity);
    entity.setReservation(reservation);
    return true;
  }
}
