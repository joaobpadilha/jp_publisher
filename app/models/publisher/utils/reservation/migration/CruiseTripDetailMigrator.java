package models.publisher.utils.reservation.migration;

import com.mapped.publisher.utils.Log;
import com.umapped.persistence.reservation.cruise.UmCruiseReservation;

import models.publisher.BookingCruise;
import models.publisher.TripDetail;
import models.utils.migration.Migrator;

public class CruiseTripDetailMigrator implements Migrator<TripDetail> {
  private UmCruiseReservationBuilder builder = new UmCruiseReservationBuilder();
  
  @Override
  public boolean migrate(TripDetail entity) {
    
    BookingCruise cruise = BookingCruise.find.byId(entity.detailsid);
    //Log.info("Migrate a cruise trip_detail");
    UmCruiseReservation reservation = builder.build(entity, cruise);
    entity.setReservation(reservation);
    return true;
  }
}
