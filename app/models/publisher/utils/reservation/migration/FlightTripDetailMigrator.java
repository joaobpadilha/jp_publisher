package models.publisher.utils.reservation.migration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.umapped.persistence.reservation.flight.UmFlightReservation;

import models.publisher.FlightBooking;
import models.publisher.TripDetail;
import models.utils.migration.Migrator;

class FlightTripDetailMigrator implements Migrator<TripDetail> {
  private static Logger Log = LoggerFactory.getLogger(FlightTripDetailMigrator.class);
  
  private UmFlightReservationBuilder builder = new UmFlightReservationBuilder();
  
  @Override
  public boolean migrate(TripDetail flight) {
    FlightBooking flightBooking = FlightBooking.find.byId(flight.getDetailsid());
    Log.info("Migrate a flight trip_detail");
    UmFlightReservation reservation = builder.build(flight, flightBooking);
    flight.setReservation(reservation);
    return true;
  }
 
  
}
