package models.publisher.utils.reservation.migration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.umapped.persistence.reservation.flight.UmFlightReservation;

import models.publisher.TmpltDetails;
import models.publisher.TmpltFlight;
import models.utils.migration.Migrator;

public class FlightTmpltDetailsMigrator implements Migrator<TmpltDetails> {
  private static Logger Log = LoggerFactory.getLogger(FlightTmpltDetailsMigrator.class);
  
  private UmFlightReservationBuilder builder = new UmFlightReservationBuilder();
  
  @Override
  public boolean migrate(TmpltDetails flight) {
    TmpltFlight flightBooking = TmpltFlight.find.byId(flight.getDetailsid());
    Log.info("Migrate a flight tmplt_detail");
    UmFlightReservation reservation = builder.build(flight, flightBooking);
    flight.setReservation(reservation);
    return true;
  }
 
  
}
