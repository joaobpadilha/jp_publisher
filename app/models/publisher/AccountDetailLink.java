package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.EnumValue;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 * Created by surge on 2016-02-24.
 */
@Entity
@Table(name = "account_detail_link")
public class AccountDetailLink
    extends Model {
  public enum DetailLinkType {
    @EnumValue("TRV")
    TRAVELER,
    @EnumValue("REC")
    RECOMMENDER
  }

  public static Finder<ADLPk, AccountDetailLink> find = new Finder<>(AccountDetailLink.class);
  @Id
  ADLPk          pk;
  @Enumerated
  DetailLinkType linkType;
  Timestamp createdTs;
  Long      createdBy;
  Timestamp modifiedTs;
  Long      modifiedBy;
  @Version
  int version;

  @Embeddable
  public static class ADLPk {
    Long   uid;
    String detailId;

    @Override
    public int hashCode() {
      return uid.hashCode() * detailId.hashCode();
    }

    @Override
    public boolean equals(Object o) {
      if (o == null) {
        return false;
      }
      if (!(o instanceof ADLPk)) {
        return false;
      }
      ADLPk ok = (ADLPk) o;
      return uid.equals(ok.uid) && detailId.equals(ok.detailId);
    }

    public Long getUid() {
      return uid;
    }

    public void setUid(Long uid) {
      this.uid = uid;
    }

    public String getDetailId() {
      return detailId;
    }

    public void setDetailId(String detailId) {
      this.detailId = detailId;
    }
  }

  public static List<AccountDetailLink> accountDetails(Long uid) {
    return find.where().eq("uid", uid).findList();
  }

  public static List<AccountDetailLink> detailAccounts(String detailsid) {
    return find.where().eq("detail_id", detailsid).findList();
  }

  public static AccountDetailLink build(Long uid, String detailId, Long createdBy) {
    AccountDetailLink acl   = new AccountDetailLink();
    ADLPk             aclPk = new ADLPk();
    aclPk.setUid(uid);
    aclPk.setDetailId(detailId);
    acl.setPk(aclPk);
    acl.setCreatedTs(Timestamp.from(Instant.now()));
    acl.setCreatedBy(createdBy);
    acl.setModifiedTs(acl.createdTs);
    acl.setModifiedBy(createdBy);
    return acl;
  }

  public ADLPk getPk() {
    return pk;
  }

  public void setPk(ADLPk pk) {
    this.pk = pk;
  }

  public DetailLinkType getLinkType() {
    return linkType;
  }

  public void setLinkType(DetailLinkType linkType) {
    this.linkType = linkType;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
  }

  public Timestamp getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Timestamp modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
