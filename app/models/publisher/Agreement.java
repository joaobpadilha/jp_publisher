package models.publisher;

import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.sql.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-16
 * Time: 1:01 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="Agreement")
public class Agreement  extends Model {
    public Date enddate;

    @Id
    public String agreementversion;

    @Constraints.Required
    public String name;
    public String html;
    public Date effectivedate;

    public int status;
    public Long createdtimestamp;
    public Long lastupdatedtimestamp;
    public String createdby;
    public String modifiedby;

    @Version
    public int version;

    public static Model.Finder<String,Agreement> find = new Finder(Agreement.class);

    public static List<Agreement> activeAgreements () {
        return find.where().eq("status", 0).le("effectivedate", new Date(System.currentTimeMillis())).orderBy("version").findList();
    }

    public static boolean isActive (String agreementversion) {
        List <Agreement> results =  find.where().eq("status", 0).le("effectivedate",new Date(System.currentTimeMillis())).eq("agreementversion", agreementversion).orderBy("version").findList();
        if (results != null & results.size() > 0) {
            for (Agreement agreement:results) {
                Date currentDate =  new Date(System.currentTimeMillis());
                if (agreement.effectivedate.before(currentDate) && (agreement.enddate == null || agreement.enddate.after(currentDate))) {
                    return true;
                }
            }
        }

        return false;
    }



    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date endDate) {
        this.enddate = endDate;
    }

    public String getAgreementversion() {
        return agreementversion;
    }

    public void setAgreementVersion(String agreementVersion) {
        this.agreementversion = agreementversion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public Date getEffectivedate() {
        return effectivedate;
    }

    public void setEffectivedate(Date effectiveDate) {
        this.effectivedate = effectiveDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getCreatedtimestamp() {
        return createdtimestamp;
    }

    public void setCreatedtimestamp(Long createdtimestamp) {
        this.createdtimestamp = createdtimestamp;
    }

    public Long getLastupdatedtimestamp() {
        return lastupdatedtimestamp;
    }

    public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
        this.lastupdatedtimestamp = lastupdatedtimestamp;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(String modifiedby) {
        this.modifiedby = modifiedby;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
