package models.publisher;

import com.mapped.publisher.common.APPConstants;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2015-11-09.
 */
@Entity @Table(name = "email_token")
public class EmailToken
    extends Model {

  public static Model.Finder<String, EmailToken> find = new Finder(EmailToken.class);
  /**
   * String token
   */
  @Id String token;
  /**
   * Old style token state (0 - active, -1 - deleted)
   */
  Integer state;
  /**
   * Type of the email
   */
  //@ManyToOne @JoinColumn(name = "type")
  @Enumerated(value = EnumType.STRING)
  EmailType.EmailTypeName type;
  /**
   * ID to the table corresponding to the token type
   */
  String belongId;
  /**
   * When token was generated
   */
  Long createdTs;

  public static EmailToken buildCmpyToken(String cmpyId) {
    EmailToken tkn = new EmailToken();
    tkn.setBelongId(cmpyId);
    tkn.setState(APPConstants.STATUS_ACTIVE);
    //EmailType type = EmailType.find.byId(EmailType.EmailTypeName.PARSE_CMPY);
    tkn.setType(EmailType.EmailTypeName.PARSE_CMPY);
    tkn.setCreatedTs(System.currentTimeMillis());
    return tkn;
  }

  public static List<EmailToken> getCompanyTokens(String cmpyId) {
    return find.where()
               .eq("type", "PARSE_CMPY")
               .eq("belong_id", cmpyId)
               .findList();
  }

  public static List<EmailToken> getCompanyActiveTokens(String cmpyId) {
    return find.where()
               .eq("type","PARSE_CMPY")
               .eq("belong_id",cmpyId)
               .eq("state", APPConstants.STATUS_ACTIVE)
               .findList();
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Integer getState() {
    return state;
  }

  public void setState(Integer state) {
    this.state = state;
  }

  public EmailType.EmailTypeName getType() {
    return type;
  }

  public void setType(EmailType.EmailTypeName type) {
    this.type = type;
  }

  public String getBelongId() {
    return belongId;
  }

  public void setBelongId(String belongId) {
    this.belongId = belongId;
  }

  public Long getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Long createdTs) {
    this.createdTs = createdTs;
  }
}
