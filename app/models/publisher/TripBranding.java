package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;
import com.avaje.ebean.SqlUpdate;
import com.mapped.publisher.view.TripBrandingView;
import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.*;

import play.data.validation.Constraints;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by twong on 2014-07-15.
 */
@Entity
@Table(name = "Trip_Branding")

public class TripBranding extends Model {

  public static Model.Finder<TripBrandingPK, TripBranding> find = new Finder(TripBranding.class);


  @EmbeddedId
  public TripBrandingPK pk;

  @Constraints.Required
  public boolean isenabled;
  public boolean ismainbrand;
  public int position;

  public long createdtimestamp;
  public String createdby;

  public long lastupdatedtimestamp;
  public String modifiedby;

  @Version
  public int version;

  public static List<TripBranding> findActiveByTripId(String tripId) {
    return find.where().eq("tripid", tripId).findList();
  }

  public static TripBranding findActiveByPK(String tripId, String cmpyId) {
    return find.where().eq("tripid", tripId).eq("cmpyid", cmpyId).findUnique();
  }

  public static List<TripBranding> findEnabledByTripId(String tripId) {
    return find.where().eq("tripid", tripId).eq("isenabled", true).findList();
  }

  public static List<TripBranding> findMainByTripId(String tripId) {
    return find.where().eq("tripid", tripId).eq("ismainbrand", true).findList();
  }

  public static  TripBranding findPrimaryByTripId(String tripid) {
    return find.where().eq("tripid", tripid).eq("ismainbrand", true).findUnique();
  }

  public TripBrandingPK getPk() {
    return pk;
  }

  public void setPk(TripBrandingPK pk) {
    this.pk = pk;
  }

  public boolean getIsenabled() {
    return isenabled;
  }

  public void setIsenabled(boolean isenabled) {
    this.isenabled = isenabled;
  }

  public boolean getIsmainbrand() {
    return ismainbrand;
  }

  public void setIsmainbrand(boolean ismainbrand) {
    this.ismainbrand = ismainbrand;
  }

  public int getPosition() {
    return position;
  }

  public void setPosition(int position) {
    this.position = position;
  }

  public long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }



}
