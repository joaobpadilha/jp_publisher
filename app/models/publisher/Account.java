package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbJsonB;
import com.avaje.ebean.annotation.EnumValue;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.primitives.Longs;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.persistence.RecordStatus;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Base64;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by surge on 2016-02-24.
 */
@Entity
@Table(name = "account")
public class Account
    extends Model {
  @Transient
  public static final ObjectMapper om;

  public static final Long AID_UNDEFINED = -1L;
  public static final Long AID_PUBLISHER = 0L;
  public static final String LEGACY_PUBLISHER_USERID = "system";

  /**
   * Defines primary account roles that user can be linked against
   * Account belongs only to a single type
   */
  public enum AccountType {
    /**
     * Special account - only for used inside Umapped backend software implementation
     */
    @EnumValue(value = "SYS")
    SYSTEM,
    /**
     * Umapped Platform Administrator
     */
    @EnumValue(value = "UMA")
    UMAPPED_ADMIN,
    /**
     * Trip Publishing Account
     */
    @EnumValue(value = "PUB")
    PUBLISHER,
    /**
     * Traveller Account
     */
    @EnumValue(value = "TRV")
    TRAVELER
  }


  public static Pattern                     usernameValidationRe = Pattern.compile("^[a-zA-Z0-9_]{5,15}$");
  public static Model.Finder<Long, Account> find                 = new Finder(Account.class);

  static {
    om = new ObjectMapper();
    //Perform any kind of configuration for object mapper in this area
  }

  @Id
  Long      uid;
  String    firstName;
  String    middleName;
  String    lastName;
  /**
   * For messaging and or references
   */
  String    initials;
  /**
   * May be... for @mentions or need to think how to do this, clean slate
   */
  String    username;
  String    email;
  String    emailBackup;
  /**
   * We will set year 2400 as the year when we don't know the birthdday year
   */
  Timestamp birthday;
  /**
   * Primary mobile phone number(for app/sms)
   */
  String    msisdn;
  /**
   * Flag to indicate that SMS validation success
   */
  Boolean   msisdnValid;
  /**
   * User's timezone
   */
  String    tz;
  /**
   * Locale preference as used in Java
   */
  String    locale;
  /**
   * User's default Airport, will allow to know location better
   */
  String aeroportCode;
  /**
   * 3-letter type (Mapped to enum)
   */
  @Enumerated
  AccountType  accountType;
  /**
   * 3-letter account record state
   */
  @Enumerated
  RecordStatus state;
  /**
   * Legacy identifier, some time in the future far far away we can drop this column
   */
  @Column(name = "legacy_id")
  String       legacyId;
  @DbJsonB
  JsonNode     meta;
  Timestamp createdTs;
  Long      createdBy;
  Timestamp modifiedTs;
  Long      modifiedBy;
  @Version
  int version;

  public String getLoggingId() {
    switch (accountType) {
      case SYSTEM:
      case UMAPPED_ADMIN:
      case PUBLISHER:
        return legacyId;

      case TRAVELER:
      default:
        return uid.toString();
    }
  }

  /**
   * Stuff that goes into JSON
   */
  public static class AccountMeta {
  }

  /**
   * Encodes account ID into URL friendly, Email Friendly, Firebase Key Friendly BASE64 string
   * @param aid
   * @return
   */
  public static String encodeId(Long aid) {
    byte[] bytes = Longs.toByteArray(aid);
    return '~' + Base64.getUrlEncoder().withoutPadding().encodeToString(bytes);
  }

  /**
   * Decodes account ID from BASE64 representation
   * @param aid
   * @return
   */
  public static Long decodeId(String aid) {
    if(aid == null || !aid.startsWith("~")) {
      return null;
    }
    byte[] bytes = Base64.getUrlDecoder().decode(aid.substring(1));
    return Longs.fromByteArray(bytes);
  }

  public static Account findByEncodedId(String aid) {
    Long lAid = decodeId(aid);
    return find.byId(lAid);
  }

  public boolean isTraveler() {
    return accountType == AccountType.TRAVELER;
  }

  public static Account build(Long userid) {
    Account a = new Account();
    a.setUid(DBConnectionMgr.getUniqueLongId());
    a.setCreatedTs(Timestamp.from(Instant.now()));
    a.setModifiedTs(a.createdTs);
    a.setCreatedBy(userid);
    a.setModifiedBy(userid);
    return a;
  }

  //legacy account look up
  public static Account findByLegacyTripLink (String tripid, String legacyLinkId, String groupId) {
    if (groupId == null) {
      String sql = "uid in (select t1.uid from account_trip_link t1 where t1.legacyid = ? and t1.tripid = ?";
      Object[] params = {legacyLinkId, tripid};

      return find.where()
                 .raw(sql, params)
                 .findUnique();
    } else {
      String sql = "uid in (select t1.uid from account_trip_link t1 where t1.legacy_id = ? and t1.tripid = ? and t1.legacy_group_id = ?";
      Object[] params = {legacyLinkId, tripid, groupId};

      return find.where()
                 .raw(sql, params)
                 .findUnique();
    }
  }

  public String getEncodedId() {
    return Account.encodeId(getUid());
  }

  public static Account findByLegacyId(String legacyId) {
    return find.where().eq("lower(legacy_id)", legacyId.toLowerCase()).findUnique();
  }

  public static Account findActiveByLegacyId(String legacyId) {
    return find.where().eq("lower(legacy_id)", legacyId.toLowerCase()).ne("state", RecordStatus.DELETED).findUnique();
  }

  public static Account findByEmail(String email) {
    return find.where().eq("lower(email)", email.toLowerCase()).findUnique();
  }

  public static int getContUserProfiles(String userId) {
    userId += "%";
    return find.where().ilike("legacy_id", userId).findCount();
  }

  public String getLocale() {
    return locale;
  }

  public Account setLocale(String locale) {
    this.locale = locale;
    return this;
  }

  public void markModified(Long modifiedBy) {
    setModifiedBy(modifiedBy);
    setModifiedTs(Timestamp.from(Instant.now()));
  }

  public String getFullName() {
    StringBuilder sb = new StringBuilder();
    if (firstName != null) {
      sb.append(firstName).append(" ");
    }
    if (middleName != null) {
      sb.append(middleName).append(" ");
    }
    if (lastName != null) {
      sb.append(lastName);
    }
    return sb.toString();
  }

  public RecordStatus getState() {
    return state;
  }

  public void setState(RecordStatus state) {
    this.state = state;
  }

  public Long getUid() {
    return uid;
  }

  public void setUid(Long uid) {
    this.uid = uid;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getInitials() {
    return initials;
  }

  public void setInitials(String initials) {
    this.initials = initials;
  }

  public void setDefaultInitials() {
    if (firstName != null && lastName != null) {
      setInitials((firstName.substring(0, 1) + lastName.substring(0, 1)).toUpperCase());
    }
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getEmailBackup() {
    return emailBackup;
  }

  public void setEmailBackup(String emailBackup) {
    this.emailBackup = emailBackup;
  }

  public Timestamp getBirthday() {
    return birthday;
  }

  public void setBirthday(Timestamp birthday) {
    this.birthday = birthday;
  }

  public String getMsisdn() {
    return msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }

  public Boolean getMsisdnValid() {
    return msisdnValid;
  }

  public void setMsisdnValid(Boolean msisdnValid) {
    this.msisdnValid = msisdnValid;
  }

  public String getTz() {
    return tz;
  }

  public void setTz(String tz) {
    this.tz = tz;
  }

  public String getAeroportCode() {
    return aeroportCode;
  }

  public void setAeroportCode(String aeroportCode) {
    this.aeroportCode = aeroportCode;
  }

  public AccountType getAccountType() {
    return accountType;
  }

  public void setAccountType(AccountType accountType) {
    this.accountType = accountType;
  }

  public String getLegacyId() {
    return legacyId;
  }

  public void setLegacyId(String legacyId) {
    this.legacyId = legacyId;
  }

  public JsonNode getMeta() {
    return meta;
  }

  public void setMeta(JsonNode meta) {
    this.meta = meta;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
  }

  public Timestamp getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Timestamp modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public static List<Account> passengersForFlightAlert(long alertId) {
    String sql = "uid in" +
            "(select atl.uid from account_trip_link atl where atl.tripid IN " +
            "      (SELECT td.tripid " +
            "       FROM trip_detail td" +
            "       WHERE td.detailsid IN " +
            "              (SELECT fab.details_id" +
            "               FROM flight_alert_booking fab" +
            "               WHERE fab.flight_alert_id = ?) " +
            "             AND td.status = 0 )) ";

    Object[] params = {alertId};

    return find.where()
            .raw(sql, params)
            .findList();
  }

  public static List<Account> passengersByEmail(String email, Long uuid) {
    String sql = "uid in " +
                 "(select atl.uid from account_trip_link atl where atl.created_by = ? ) ";

    Object[] params = {uuid};

    return find.where()
               .ilike("email", email)
               .raw(sql, params)
               .findList();
  }

  public static boolean isEndcoded (String id) {
    if (id != null && id.startsWith("~")) {
     return true;
    }
    return false;
  }

}
