package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2015-11-24.
 */
@Entity @Table(name = "t42_hotel")
public class T42Hotel
    extends Model {
  public static Finder<String, T42Hotel> find = new Finder(T42Hotel.class);
  @Id String hotelId;
  String hotelName;
  Integer geoPlaceKey;
  String geoDisplay;
  Float latitude;
  Float longitude;
  Integer starRating;
  String starReview;
  Boolean active;
  @Version int version;

  @Transient //Temp
  String afarKey;

  public static T42Hotel findByKey(String hotelId) {
    return find.byId(hotelId);
  }


  public static List<T42Hotel> findStarredByPlaceKey(Integer geoPlaceKey) {
    return find.where().eq("geo_place_key", geoPlaceKey).ge("star_rating", 1).order("star_rating DESC").findList();
  }

  public static List<T42Hotel> findByPlaceKey(Integer geoPlaceKey) {
    return find.where().eq("geo_place_key", geoPlaceKey).order("star_rating DESC").findList();
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public String getHotelId() {
    return hotelId;
  }

  public void setHotelId(String hotelId) {
    this.hotelId = hotelId;
  }

  public String getStarReview() {
    return starReview;
  }

  public void setStarReview(String starReview) {
    this.starReview = starReview;
  }


  public String getHotelName() {
    return hotelName;
  }

  public void setHotelName(String hotelName) {
    this.hotelName = hotelName;
  }

  public Integer getStarRating() {
    return starRating;
  }

  public void setStarRating(Integer starRating) {
    this.starRating = starRating;
  }

  public void setStarRating(String starRating) {
    if(starRating == null || starRating.length() == 0) {
      return;
    }

    try {
      this.starRating = Integer.parseInt(starRating);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }


  public Integer getGeoPlaceKey() {
    return geoPlaceKey;
  }

  public void setGeoPlaceKey(Integer geoPlaceKey) {
    this.geoPlaceKey = geoPlaceKey;
  }

  public void setGeoPlaceKey(String geoPlaceKey) {
    try {
      this.geoPlaceKey = Integer.parseInt(geoPlaceKey);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public String getGeoDisplay() {
    return geoDisplay;
  }

  public void setGeoDisplay(String geoDisplay) {
    this.geoDisplay = geoDisplay;
  }

  public Float getLatitude() {
    return latitude;
  }

  public void setLatitude(Float latitude) {
    this.latitude = latitude;
  }

  public void setLatitude(String latitude) {
    if(latitude == null || latitude.length() == 0) {
      return;
    }

    try {
      this.latitude = Float.parseFloat(latitude);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public Float getLongitude() {
    return longitude;
  }

  public void setLongitude(Float longitude) {
    this.longitude = longitude;
  }

  public void setLongitude(String longitude) {
    if(longitude == null || longitude.length() == 0) {
      return;
    }
    try {
      this.longitude = Float.parseFloat(longitude);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public String getAfarKey() {
    return afarKey;
  }

  public void setAfarKey(String afarKey) {
    this.afarKey = afarKey;
  }
}
