package models.publisher;

import com.avaje.ebean.*;
import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbJsonB;
import com.umapped.external.afar.*;
import com.umapped.service.offer.UMappedCity;
import com.umapped.service.offer.UMappedCityLookup;
import org.postgis.Point;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.umapped.external.afar.Destination;

/**
 * Created by george on 2017-11-01.
 */
@Entity @Table(name = "afar_city")
public class AfarCity extends Model implements JsonSupport{

    public static Model.Finder<Integer, AfarCity> find = new Finder<>(AfarCity.class);


    //To be deleted
    //private String afarCityId;

    @Id
    private int afarId;
    private int afarCountryId;
    private String umCityId;
    private int afarDestId;
    private String name;
    private String slug;
    private String description;
    private String imageUrl;
    private String photoCredit;
    @Column(name="location")
    private Point location;
    @DbJsonB
    private Map<String, Object> cityLists;
    @DbJsonB
    private Map<String, Object> data;

    public int getAfarId() {
        return afarId;
    }

    public void setAfarId(int afarId) {
        this.afarId = afarId;
    }

    public int getAfarCountryId() {
        return afarCountryId;
    }

    public void setAfarCountryId(int afarCountryId) {
        this.afarCountryId = afarCountryId;
    }

    public int getAfarDestId() {
        return afarDestId;
    }

    public void setAfarDestId(int afarDestId) {
        this.afarDestId = afarDestId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPhotoCredit() {
        return photoCredit;
    }

    public void setPhotoCredit(String photoCredit) {
        this.photoCredit = photoCredit;
    }

    public Point getLocation() {
        return location;
    }

    public void setLocation(Point location) {
        this.location = location;
    }

    public Map<String, Object> getCityLists() {
        return cityLists;
    }

    public void setCityLists(Map<String, Object> cityLists) {
        this.cityLists = cityLists;
    }

    public String getUmCityId() {
        return umCityId;
    }

    public void setUmCityId(String umCityId) {
        this.umCityId = umCityId;
    }

    //To be deleted
    public String getAfarCityId() {
        return name;
    }

    //To be deleted
    public void setAfarCityId(String afarCityId) {
        this.name = afarCityId;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data){
        this.data = data;
    }

    public Map<String, Object> marshalCityLists(IdList d) {
        return marshal(d);
    }

    public IdList unmarshalCityLists(Map<String, Object> data) {
        return unmarshal(data, IdList.class);
    }

    public Map<String, Object> marshalData(Destination d) {
        return marshal(d);
    }

    public Destination unmarshalData(Map<String, Object> data) {
        return unmarshal(data, Destination.class);
    }

    public static List<AfarCity> findByTerm(String term) {
        return find.where().ilike("name", "%"+term+"%").findList();
    }

    public static AfarCity findByCity(String city) {
        return find.where().eq("name", city).findUnique();
    }

    public static AfarCity findByUmCity(UMappedCity umCity) {
        //UMappedCityLookup umCityLookup = new UMappedCityLookup();
        //UMappedCity umCity = umCityLookup.getCityById(umCityId);
        if (umCity != null) {
            return find.where().eq("um_city_id", umCity.getId()).ieq("name", umCity.getName()).findUnique();
        }
        return null;
    }

    public static List<AfarCity> findCitiesWithPoisByName(String name) {
        com.avaje.ebean.Query<AfarCityList> citesWithLists = Ebean
            .createQuery(AfarCityList.class)
            .select("afarCityId")
            .where()
            .query();

        return find.where().ilike("name", name).in("afar_id", citesWithLists).findList();
    }
}
