package models.publisher;

import com.avaje.ebean.BeanState;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.mapped.persistence.util.DBConnectionMgr;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-19
 * Time: 3:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity @Table(name = "transport_booking")
public class TransportBooking
    extends Model {
  public static Model.Finder<String, TransportBooking> find = new Finder(TransportBooking.class);
  public String comments;
  public String contact;
  public String pickuplocation;
  public String dropofflocation;
  @Id
  public String detailsid;
  public String name;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @Version
  public int version;

  /**
   * Takes this object makes it possible to re-insert as new record
   * @return
   */
  public TransportBooking prepareForReuse(String createdBy, String modifiedBy) {
    BeanState state = Ebean.getBeanState(this);
    state.resetForInsert();
    setDetailsid(DBConnectionMgr.getUniqueId());
    setModifiedby(modifiedBy);
    setCreatedby(createdBy);
    setLastupdatedtimestamp(System.currentTimeMillis());
    setCreatedtimestamp(lastupdatedtimestamp);
    return this;
  }

  public static TransportBooking build(String detailId, String userId) {
    TransportBooking transportBooking = new TransportBooking();
    transportBooking.setDetailsid(detailId);
    transportBooking.setCreatedby(userId);
    transportBooking.setModifiedby(userId);
    transportBooking.setCreatedtimestamp(System.currentTimeMillis());
    transportBooking.setLastupdatedtimestamp(transportBooking.createdtimestamp);
    transportBooking.setVersion(0);
    return transportBooking;
  }

  public static TransportBooking findByPK(String pk) {
    return find.byId(pk);
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }

  public String getDetailsid() {
    return detailsid;
  }

  public void setDetailsid(String detailsid) {
    this.detailsid = detailsid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public String getPickuplocation() {
    return pickuplocation;
  }

  public void setPickuplocation(String pickuplocation) {
    this.pickuplocation = pickuplocation;
  }

  public String getDropofflocation() {
    return dropofflocation;
  }

  public void setDropofflocation(String dropofflocation) {
    this.dropofflocation = dropofflocation;
  }
}
