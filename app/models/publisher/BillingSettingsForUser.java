package models.publisher;

import com.avaje.ebean.Expr;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freshbooks.model.Client;
import com.mapped.publisher.persistence.billing.BraintreeSunbscription;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2015-03-26.
 */
@Entity @Table(name = "bil_settings_user")
public class BillingSettingsForUser
    extends Model {

  /**
   * These settings are used anywhere in the model, but are used to retrieve records based on one of the paramters
   */
  public static enum BillingUserType {
    UB_NO_CONF("Not Configured"),
    UB_WITH_ASSISTANTS("Being Assisted"),
    UB_PER_TRIP("Billed Per Trip"),
    UB_PER_MONTH("Billed Monthly"),
    UB_PER_YEAR("Billed Yearly"),
    UB_TRIAL("Trialist"),
    UB_NONE("Unbillable"),
    UB_NON_RECURRENT("Non-recurrent"),
    UB_ALL("All");
    private String label;
    private BillingUserType(String label) {
      this.label = label;
    }

    public String getLabel() {
      return label;
    }
  }

  public static Model.Finder<String, BillingSettingsForUser> find = new Finder(BillingSettingsForUser.class);

  @Id @Column(name = "userid")
  private String userId;
  @ManyToOne @JoinColumn(name = "schedule")
  private BillingSchedule schedule;
  @ManyToOne @JoinColumn(name = "plan", nullable = true)
  private BillingPlan plan;
  @ManyToOne @JoinColumn(name = "bill_to")
  private BillingEntity billTo;

  @ManyToOne(optional = true) @JoinColumn(name = "pmnt_type", nullable = true, referencedColumnName = "name")
  private BillingPmntMethod pmntType;
  /*Agent User ID*/
  private String agent;
  private String cmpyid;
  private Long startTs;
  private Long cutoffTs;
  private Boolean recurrent;
  private String meta;
  private Long syncTs;
  private Long createdTs;
  private String createdBy;
  private Long modifiedTs;
  private String modifiedBy;
  @Version
  private int version;

  /**
   * If in the future we will have various types of integrations each will be
   * assigned to a dedicated variable in this wrapper class. For now freshbooks
   * is the only type.
   */
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static class MetaDataWrapper {
    public Client freshbooks;
    public BraintreeSunbscription braintree;
  }

  public static int getPlanUseCount(Long planId) {
    return find.where().eq("plan.planId", planId).findCount();
  }

  public MetaDataWrapper getMetaDataWrapper() {
    if (this.meta == null) {
      return null;
    }
    ObjectMapper m = new ObjectMapper();
    m.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    MetaDataWrapper md = null;
    try {
      md = m.readValue(this.meta, MetaDataWrapper.class);
    }
    catch (Exception je) {
      Log.err("Failed to unmarshal Freshbooks Client Data from JSON:", je);
    }
    return md;
  }

  public Client getFreshbookClientData() {
    MetaDataWrapper md = getMetaDataWrapper();
    if (md != null)
      return md.freshbooks;
    else
      return null;
  }

  public void setFreshbookClientData(Client client) {
    ObjectMapper m = new ObjectMapper();
    m.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    //Needs this black magic to avoid picking up methods that looks like real fields
    m.setVisibilityChecker(m.getSerializationConfig()
                            .getDefaultVisibilityChecker()
                            .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                            .withIsGetterVisibility(JsonAutoDetect.Visibility.NONE)
                            .withGetterVisibility(JsonAutoDetect.Visibility.NONE));
    String result = null;

    MetaDataWrapper idw = getMetaDataWrapper();
    if (idw == null) {
      idw = new MetaDataWrapper();
    }

    idw.freshbooks = client;

    try {
      //result = m.writerWithDefaultPrettyPrinter().writeValueAsString(idw);
      result = m.writeValueAsString(idw);
    }
    catch (JsonProcessingException jpe) {
      Log.err("Failed to generate Freshbooks Client JSON Data:", jpe);
    }

    this.setMeta(result);
  }

  public void setBraintreeData(BraintreeSunbscription braintreeData) {
    ObjectMapper m = new ObjectMapper();
    m.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    //Needs this black magic to avoid picking up methods that looks like real fields
    m.setVisibilityChecker(m.getSerializationConfig()
                            .getDefaultVisibilityChecker()
                            .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                            .withIsGetterVisibility(JsonAutoDetect.Visibility.NONE)
                            .withGetterVisibility(JsonAutoDetect.Visibility.NONE));
    String result = null;

    MetaDataWrapper idw = getMetaDataWrapper();

    if (idw == null) {
      idw = new MetaDataWrapper();
    }

    idw.braintree = braintreeData;

    try {
      //result = m.writerWithDefaultPrettyPrinter().writeValueAsString(idw);
      result = m.writeValueAsString(idw);
    }
    catch (JsonProcessingException jpe) {
      Log.err("Failed to generate Braintree Client JSON Data:", jpe);
    }

    this.setMeta(result);
  }

  public BraintreeSunbscription getBraintree() {
    MetaDataWrapper md = getMetaDataWrapper();
    if (md != null)
      return md.braintree;
    else
      return null;
  }

  public String getMeta() {
    return meta;
  }

  public void setMeta(String meta) {
    this.meta = meta;
  }

  public Long getSyncTs() {
    return syncTs;
  }

  public void setSyncTs(Long syncTs) {
    this.syncTs = syncTs;
  }

  public static List<Object> findBillableUserIds() {
    return find.where().eq("bill_to", BillingEntity.Type.USER).findIds();
  }

  public static BillingSettingsForUser findSettingsByUserId(String userId) {
    return find.where().eq("userid",userId).findUnique();
  }

  public static List<BillingSettingsForUser> findBillableUsers() {
    return find.where().eq("bill_to", BillingEntity.Type.USER).findList();
  }

  public static List<BillingSettingsForUser> findBillableOutOfSyncUsers() {
    return find.where()
               .eq("bill_to", BillingEntity.Type.USER)
               .raw("sync_ts < modified_ts OR sync_ts IS NULL")
               .findList();
  }


  public String getStartDate() {
    return Utils.getISO8601Date(startTs);
  }

  public String getCutoffDate() {
    return Utils.getISO8601Date(cutoffTs);
  }

  public static int getRowCount(BillingUserType ut) {
    switch (ut) {
      case UB_NO_CONF:
        return UserProfile.getRowCount_BillingNoConf();
      case UB_WITH_ASSISTANTS:
        return UserProfile.getRowCount_BillingWithAss();
      case UB_PER_TRIP:
        return UserProfile.getRowCount_BillingPerSched(BillingSchedule.Type.TRIP);
      case UB_TRIAL:
        return UserProfile.getRowCount_BillingPerSched(BillingSchedule.Type.TRIAL);
      case UB_NONE:
        return UserProfile.getRowCount_BillingPerSched(BillingSchedule.Type.NONE);
      case UB_PER_MONTH:
        return UserProfile.getRowCount_BillingPerSched(BillingSchedule.Type.MONTHLY);
      case UB_PER_YEAR:
        return UserProfile.getRowCount_BillingPerSched(BillingSchedule.Type.YEARLY);
      case UB_NON_RECURRENT:
        return UserProfile.getRowCount_BillingNonRecurrent();
      default:
        return UserProfile.getRowCount();
    }
  }

  public static int getRowCountFiltered(BillingUserType ut, String filter) {
    switch (ut) {
      case UB_NO_CONF:
        return UserProfile.getRowCountFiltered_BillingNoConf(filter);
      case UB_WITH_ASSISTANTS:
        return UserProfile.getRowCountFiltered_BillingWithAss(filter);
      case UB_PER_TRIP:
        return UserProfile.getRowCountFiltered_BillingPerSched(BillingSchedule.Type.TRIP, filter);
      case UB_TRIAL:
        return UserProfile.getRowCountFiltered_BillingPerSched(BillingSchedule.Type.TRIAL, filter);
      case UB_NONE:
        return UserProfile.getRowCountFiltered_BillingPerSched(BillingSchedule.Type.NONE, filter);
      case UB_PER_MONTH:
        return UserProfile.getRowCountFiltered_BillingPerSched(BillingSchedule.Type.MONTHLY, filter);
      case UB_PER_YEAR:
        return UserProfile.getRowCountFiltered_BillingPerSched(BillingSchedule.Type.YEARLY, filter);
      case UB_NON_RECURRENT:
        return UserProfile.getRowCountFiltered_BillingNonRecurrent(filter);
      default:
        return UserProfile.getRowCountFiltered(filter);
    }
  }

  public static List<UserProfile> getProfilesForType(BillingUserType ut, int start, int count, String filter, String sortOrder) {
    switch (ut) {
      case UB_NO_CONF:
        return UserProfile.getPage_BillingNoConf(start, count, filter, sortOrder);
      case UB_WITH_ASSISTANTS:
        return UserProfile.getPage_BillingWithAss(start, count, filter, sortOrder);
      case UB_PER_TRIP:
        return UserProfile.getPage_BillingPerSched(BillingSchedule.Type.TRIP, start, count, filter, sortOrder);
      case UB_TRIAL:
        return UserProfile.getPage_BillingPerSched(BillingSchedule.Type.TRIAL, start, count, filter, sortOrder);
      case UB_NONE:
        return UserProfile.getPage_BillingPerSched(BillingSchedule.Type.NONE, start, count, filter, sortOrder);
      case UB_PER_MONTH:
        return UserProfile.getPage_BillingPerSched(BillingSchedule.Type.MONTHLY, start, count, filter, sortOrder);
      case UB_PER_YEAR:
        return UserProfile.getPage_BillingPerSched(BillingSchedule.Type.YEARLY, start, count, filter, sortOrder);
      case UB_NON_RECURRENT:
        return UserProfile.getPage_BillingNonRecurrent(start, count, filter, sortOrder);
      default:
        return UserProfile.getPage_Billing(start, count, filter, sortOrder);
    }
  }

  public static BillingSettingsForUser buildUserSettings(String userId, String createdBy) {
    BillingSettingsForUser bsu = new BillingSettingsForUser();
    bsu.setUserId(userId);
    long ts = System.currentTimeMillis();
    bsu.setCreatedBy(createdBy);
    bsu.setModifiedBy(createdBy);
    bsu.setCreatedTs(ts);
    bsu.setModifiedTs(ts);
    bsu.setSyncTs(0l);
    return bsu;
  }

  public void markModified(String userId) {
    setModifiedBy(userId);
    setModifiedTs(System.currentTimeMillis());
  }


  public void setSyncTsToModTs() {
    setSyncTs(modifiedTs);
  }


  public static List<BillingSettingsForUser> getAgentAndMinions(String userId) {
    return find.where().or(Expr.eq("userid", userId),
                           Expr.and(Expr.eq("agent", userId), Expr.eq("bill_to", BillingEntity.Type.AGENT)))
                        .orderBy("schedule desc") //Yearly users go first
               .findList();
  }

  public static List<BillingSettingsForUser> getMinions(String userId) {
    return find.where().eq("agent", userId).eq("bill_to", BillingEntity.Type.AGENT)
        .orderBy("schedule desc") //Yearly users go first
        .findList();
  }

  public static List<BillingSettingsForUser> getCmpyAgents(String cmpyid) {
    return find.where().eq("bill_to", BillingEntity.Type.COMPANY).eq("cmpyid", cmpyid)
               .orderBy("schedule desc") //Yearly users go first
               .findList();
  }

  public static BillingSettingsForUser getByBraintreeSubscriptionId (String subId) {
    if (subId != null) {
      String meta = "%braintree%" + subId +"%";
      List<BillingSettingsForUser> l =  find.where().like("meta", meta).setMaxRows(1).findList();
      if (l != null && l.size() > 0) {
        return l.get(0);
      }
    }
    return null;
  }

  public String getAgent() {
    return agent;
  }

  public void setAgent(String agent) {
    this.agent = agent;
  }

  public String getCmpyid() {
    return cmpyid;
  }

  public void setCmpyid(String cmpyid) {
    this.cmpyid = cmpyid;
  }

  public Boolean getRecurrent() {
    return recurrent;
  }

  public void setRecurrent(Boolean recurrent) {
    if (recurrent == null) {
      this.recurrent = false;
    } else {
      this.recurrent = recurrent;
    }
  }

  public BillingEntity getBillTo() {
    return billTo;
  }

  public void setBillTo(BillingEntity billTo) {
    this.billTo = billTo;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public BillingSchedule getSchedule() {
    return schedule;
  }

  public void setSchedule(BillingSchedule schedule) {
    this.schedule = schedule;
  }

  public BillingPlan getPlan() {
    return plan;
  }

  public void setPlan(BillingPlan plan) {
    this.plan = plan;
  }

  public BillingPmntMethod getPmntType() {
    return pmntType;
  }

  public void setPmntType(BillingPmntMethod pmntType) {
    this.pmntType = pmntType;
  }

  public Long getStartTs() {
    return startTs;
  }

  public void setStartTs(Long startTs) {
    this.startTs = startTs;
  }

  public Long getCutoffTs() {
    return cutoffTs;
  }

  public void setCutoffTs(Long cutoffTs) {
    this.cutoffTs = cutoffTs;
  }

  public Long getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Long createdTs) {
    this.createdTs = createdTs;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Long getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Long modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
