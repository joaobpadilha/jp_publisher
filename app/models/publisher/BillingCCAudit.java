package models.publisher;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbJsonB;
import com.avaje.ebean.annotation.EnumValue;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.persistence.billing.BraintreeSunbscription;
import com.mapped.publisher.utils.Log;
import com.umapped.persistence.accountprop.BraintreeBillingInfo;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 * Created by twong on 2016-10-27.
 */
@Entity
@Table(name = "bil_cc_audit")
public class BillingCCAudit
    extends Model {
  @Transient
  public static final ObjectMapper om;

  /**
   * Defines the transaction type
   */
  public enum TxnType {
    /**
     * New Subscription
     */
    @EnumValue(value = "SBN")
    NEW_SUBSCRIPTION,
    /**
     * New Addon Subscription
     */
    @EnumValue(value = "AON")
    NEW_ADDON,
    /**
     * New CC transaction
     */
    @EnumValue(value = "TXN")
    TRANSACTION,
    /**
     * Subscription Cancellation
     */
    @EnumValue(value = "SBX")
    CANCEL_SUBSCRIPTION,
    /**
     * Add On Cancellation
     */
    @EnumValue(value = "AOX")
    CANCEL_ADDON,
    /**
     * Subscription Update
     */
    @EnumValue(value = "SBU")
    UPDATE_SUBSCRIPTION,
    /**
     * Add On Update
     */
    @EnumValue(value = "AOU")
    UPDATE_ADDON,
    /**
     * New Address
     */
    @EnumValue(value = "ADN")
    NEW_ADDRESS,
    /**
     * Update Address
     */
    @EnumValue(value = "ADU")
    UPDATE_ADDRESS,
    /**
     * New Customer
     */
    @EnumValue(value = "CUN")
    NEW_CUSTOMER,
    /**
     * Update Customer
     */
    @EnumValue(value = "CUU")
    UPDATE_CUSTOMER,
    /**
     * Sync Customer
     */
    @EnumValue(value = "CUS")
    SYNC_CUSTOMER,
    /**
     * New Payment
     */
    @EnumValue(value = "PYN")
    NEW_PAYMENT,
    /**
     * Update Payment
     */
    @EnumValue(value = "PYU")
    UPDATE_PAYMENT,
    /**
     * Cancel Payment
     */
    @EnumValue(value = "PYX")
    CANCEL_PAYMENT,
    /**
     * Webhook Event - SUBSCRIPTION_CANCELED
     */
    @EnumValue(value = "NSX")
    NOTIFY_SUBSCRIPTION_CANCELED,
    /**
     * Webhook Event - SUBSCRIPTION_CHARGED_SUCCESSFULLY
     */
    @EnumValue(value = "NSC")
    NOTIFY_SUBSCRIPTION_CHARGED_OK,
    /**
     * Webhook Event - SUBSCRIPTION_CHARGED_UNSUCCESSFULLY
     */
    @EnumValue(value = "NSF")
    NOTIFY_SUBSCRIPTION_CHARGED_FAILED,
    /**
     * Webhook Event -
     */
    @EnumValue(value = "NSE")
    NOTIFY_SUBSCRIPTION_EXPIRED,
    /**
     * Webhook Event -
     */
    @EnumValue(value = "NST")
    NOTIFY_SUBSCRIPTION_TRIAL_ENDED,
    /**
     * Webhook Event -
     */
    @EnumValue(value = "NSA")
    NOTIFY_SUBSCRIPTION_ACTIVE,
    /**
     * Webhook Event -
     */
    @EnumValue(value = "NSD")
    NOTIFY_SUBSCRIPTION_PAST_DUE,
    /**
     * Webhook Event -
     */
    @EnumValue(value = "NSC")
    NOTIFY_SUBSCRIPTION_CHARGED,
    /**
     * Webhook Event - DISPUTE LOST
     */
    @EnumValue(value = "NDL")
    NOTIFY_DISPUTE_LOST,
    /**
     * Webhook Event - DISPUTE Opened
     */
    @EnumValue(value = "NDO")
    NOTIFY_DISPUTE_OPENED,
    /**
     * Webhook Event - DISPUTE Won
     */
    @EnumValue(value = "NDW")
    NOTIFY_DISPUTE_WON,
    /**
     * Webhook Event - Merchant Connected
     */
    @EnumValue(value = "NMC")
    NOTIFY_PARTNER_MERCHANT_CONNECTED,
    /**
     * Webhook Event - Merchant Declined
     */
    @EnumValue(value = "NMX")
    NOTIFY_PARTNER_MERCHANT_DECLINED,
    /**
     * Webhook Event - Merchant disconnected
     */
    @EnumValue(value = "NMD")
    NOTIFY_PARTNER_MERCHANT_DISCONNECTED,
    /**
     * Webhook Event - Disbursement
     */
    @EnumValue(value = "NIN")
    NOTIFY_DISBURSEMENT,
    /**
     * Webhook Event - Disbursement Exception
     */
    @EnumValue(value = "NIE")
    NOTIFY_DISBURSEMENT_EXCEPTION,
    /**
     * Webhook Event - TRANSACTION_DISBURSED
     */
    @EnumValue(value = "NID")
    NOTIFY_TRANSACTION_DISBURSED,



  }

  /**
   * Defines the CC processor
   */
  public enum Processor {
    /**
     * New Subscription
     */
    @EnumValue(value = "BAI")
    BRAINTREE
  }

  /**
   * Defines the transaction status
   */
  public enum Status {
    /**
     * Submitted to Processor
     */
    @EnumValue(value = "SUB")
    SUBMITTED,
    /**
     * Accepted by Processor
     */
    @EnumValue(value = "OK")
    OK,
    /**
     * Error from Processor
     */
    @EnumValue(value = "ERR")
    ERROR,
    /**
     * Notify from Processor
     */
    @EnumValue(value = "NTY")
    NOTIFY
  }

  public static Model.Finder<Long, BillingCCAudit> find                 = new Finder(BillingCCAudit.class);

  static {
    om = new ObjectMapper();
    //Perform any kind of configuration for object mapper in this area
  }


  Long planId;
  Long invoiceUid;
  Long billedUid;
  String gatewayResp;
  @DbJsonB JsonNode meta;

  @Id
  Long pk;

  @Constraints.Required
  Long txnId;

  @Enumerated
  Status status;

  @Enumerated
  TxnType txnType;

  @Enumerated
  Processor ccProcessor;

  Timestamp createdTs;
  Long      createdBy;

  @Version
  int version;

  public Long getPlanId() {
    return planId;
  }

  public BillingCCAudit setPlanId(Long planId) {
    this.planId = planId;
    return this;
  }

  public Long getInvoiceUid() {
    return invoiceUid;
  }

  public BillingCCAudit setInvoiceUid(Long invoiceUid) {
    this.invoiceUid = invoiceUid;
    return this;
  }

  public Long getBilledUid() {
    return billedUid;
  }

  public BillingCCAudit setBilledUid(Long billedUid) {
    this.billedUid = billedUid;
    return this;
  }

  public String getGatewayResp() {
    return gatewayResp;
  }

  public BillingCCAudit setGatewayResp(String gatewayResp) {
    this.gatewayResp = gatewayResp;
    return this;
  }

  public JsonNode getMeta() {
    return meta;
  }

  public BillingCCAudit setMeta(JsonNode meta) {
    this.meta = meta;
    return this;
  }

  public Long getPk() {
    return pk;
  }

  public BillingCCAudit setPk(Long pk) {
    this.pk = pk;
    return this;
  }

  public Long getTxnId() {
    return txnId;
  }

  public BillingCCAudit setTxnId(Long txnId) {
    this.txnId = txnId;
    return this;
  }

  public Status getStatus() {
    return status;
  }

  public BillingCCAudit setStatus(Status status) {
    this.status = status;
    return this;
  }

  public TxnType getTxnType() {
    return txnType;
  }

  public BillingCCAudit setTxnType(TxnType txnType) {
    this.txnType = txnType;
    return this;
  }

  public Processor getCcProcessor() {
    return ccProcessor;
  }

  public BillingCCAudit setCcProcessor(Processor ccProcessor) {
    this.ccProcessor = ccProcessor;
    return this;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public BillingCCAudit setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
    return this;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public BillingCCAudit setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public int getVersion() {
    return version;
  }

  public BillingCCAudit setVersion(int version) {
    this.version = version;
    return this;
  }


  public BillingCCAudit setMeta(BraintreeSunbscription braintreeSunbscription) {
    if (braintreeSunbscription != null) {
      meta = om.valueToTree(braintreeSunbscription);
    }
    return this;
  }

  public BillingCCAudit setMeta(BraintreeBillingInfo braintreeBillingInfo) {
    if (braintreeBillingInfo != null) {
      meta = om.valueToTree(braintreeBillingInfo);
    }
    return this;
  }

  public static BillingCCAudit buildBraintree(Long createdUserId, Long txnId) {
    if (txnId != null) {
      return build(createdUserId, Processor.BRAINTREE).setTxnId(txnId);
    } else {
      return build(createdUserId, Processor.BRAINTREE);
    }
  }

  public static BillingCCAudit build(Long createdUserId, Processor processorType) {
    BillingCCAudit audit = new BillingCCAudit()
                              .setPk(DBConnectionMgr.getUniqueLongId())
                              .setTxnId(DBConnectionMgr.getUniqueLongId())
                              .setCcProcessor(processorType)
                              .setCreatedBy(createdUserId)
                              .setCreatedTs(Timestamp.from(Instant.now()));

    return audit;
  }

  public static BillingCCAudit build(BillingCCAudit audit) {
    BillingCCAudit newAudit = new BillingCCAudit()
        .setPk(DBConnectionMgr.getUniqueLongId())
        .setTxnId(audit.getTxnId())
        .setTxnType(audit.getTxnType())
        .setBilledUid(audit.getBilledUid())
        .setInvoiceUid(audit.getInvoiceUid())
        .setPlanId(audit.getPlanId())
        .setCcProcessor(audit.getCcProcessor())
        .setCreatedBy(audit.getCreatedBy())
        .setCreatedTs(Timestamp.from(Instant.now()));

    return newAudit;
  }

  public  static int getRowCount () {
    return find.findCount();
  }

  public static int getRowCountFiltered (Account a ) {
    if (a != null) {
      return find.where()
                 .or(Expr.eq("created_by", a.getUid()),
                     Expr.or(Expr.eq("invoice_uid", a.getUid()), Expr.eq("billed_uid", a.getUid())))
                 .findCount();
    } else {
      return getRowCount();
    }
  }

  public static int getRowCountFiltered (Long txnId ) {
    if (txnId != null) {
      return find.where()
                 .eq("txn_id", txnId)
                 .findCount();
    } else {
      return getRowCount();
    }
  }

  public static List<BillingCCAudit> getAudit (int startRow, int limit, Account a, Long txnId ) {
    if (a != null)
      return find.where()
                 .or(Expr.eq("created_by", a.getUid()),
                     Expr.or(Expr.eq("invoice_uid", a.getUid()), Expr.eq("billed_uid", a.getUid())))
                 .setFirstRow(startRow)
                 .setMaxRows(limit)
                 .orderBy("created_ts desc")
                .findList();
    else if (txnId != null && txnId > 0) {
      return find.where()
                 .eq("txn_id", txnId)
                 .setFirstRow(startRow)
                 .setMaxRows(limit)
                 .orderBy("created_ts desc")
                 .findList();
    } else
      return find.where().setFirstRow(startRow).setMaxRows(limit).orderBy("created_ts desc").findList();

  }

  @PostPersist
  public void postPersist() {
    if (this.getStatus() == Status.ERROR) {
      Log.err("BRAINTREE AUDIT ERROR - Txn Id: " + txnId + " Txn Type: " + txnType );
    } else if (this.getStatus() == Status.NOTIFY) {
      Log.err("BRAINTREE AUDIT NOTIFY - Txn Id: " + txnId + " Txn Type: " + txnType );
    }
  }

}
