package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.*;

/**
 * Created by surge on 2015-12-11.
 */
@Entity
@Table(name = "user_capability")
public class UserCapability
    extends Model {

  public static Finder<UserCapabilityID, UserCapability> find = new Finder(UserCapability.class);

  @Id
  UserCapabilityID pk;
  Boolean enabled;
  Long    createdTs;
  String  createdBy;
  Long    modifiedTs;
  String  modifiedBy;
  @Version
  int version;

  @Embeddable
  public static class UserCapabilityID {
    /**
     * Plan ID
     */
    @Column(name = "cap_pk")
    public Integer capPk;
    /**
     * Feature ID
     */
    @Column(name = "userid ")
    public String  userId;

    public Integer getCapPk() {
      return capPk;
    }

    public void setCapPk(Integer capPk) {
      this.capPk = capPk;
    }

    public String getUserId() {
      return userId;
    }

    public void setUserId(String userId) {
      this.userId = userId;
    }

    @Override
    public int hashCode() {
      return capPk.hashCode() * userId.hashCode();
    }

    @Override
    public boolean equals(Object o) {
      if (o == null) {
        return false;
      }
      if (!(o instanceof UserCapabilityID)) {
        return false;
      }
      UserCapabilityID ok = (UserCapabilityID) o;
      return ok.capPk.equals(capPk) && ok.userId.equals(userId);
    }
  }

  public UserCapabilityID getPk() {
    return pk;
  }

  public void setPk(UserCapabilityID pk) {
    this.pk = pk;
  }

  public Boolean getEnabled() {
    return enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public Long getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Long createdTs) {
    this.createdTs = createdTs;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Long getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Long modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
