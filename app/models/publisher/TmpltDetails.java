package models.publisher;

import com.avaje.ebean.*;
import com.avaje.ebean.annotation.DbJsonB;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.UmReservation;

import models.utils.PersistenceUtilFactory;
import models.utils.migration.Migrator;
import play.data.validation.Constraints;

import javax.persistence.*;
import javax.persistence.Version;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.avaje.ebean.Expr.isNull;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-07-24
 * Time: 2:28 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity @Table(name = "tmplt_details")
public class TmpltDetails
    extends Model 
    implements JsonSupport {
  public static final float UNDEFINED_LOC_LATLONG = 0.0f;
  public static final int UNDEFINED_OFFSET_DAY = -1;

  public static Model.Finder<String, TmpltDetails> find = new Finder(TmpltDetails.class);
  public String comments;
  public int rank;
  @Id
  public String detailsid;
  @Constraints.Required
  public String templateid;
  public int status;

  @Enumerated(EnumType.ORDINAL)
  public ReservationType detailtypeid;
  public Long            starttimestamp;
  public Long            endtimestamp;
  public Long            createdtimestamp;
  public Long            lastupdatedtimestamp;
  public String          createdby;
  public String          modifiedby;

  @Version
  public int version;
  private int dayOffset;
  private int duration;
  private float locStartLong;
  private float locStartLat;
  private String locStartName;
  private float locFinishLong;
  private float locFinishLat;
  private String locFinishName;
  private Long poiId;
  private Integer poiCmpyId;
  private Long locStartPoiId;
  private Integer locStartPoiCmpyId;
  private Long locFinishPoiId;
  private Integer locFinishPoiCmpyId;
  private String name;
  private String meta;
  
  @DbJsonB
  private Map<String, Object>    reservationData;
  
  @Transient
  private UmReservation reservation;

  /**
   * Cache of POJO metadata
   */
  @Transient
  private MetaDataWrapper metadata = null;

  /**
   * Booking metadata - all additional information that does not fit into standard fields
   */
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static class MetaDataWrapper {
    //TODO: Add subclasses here
  }

  public int getPoiUseCount(Long poiId, Integer cmpyId) {
    return find.where()
               .or(Expr.or(Expr.and(Expr.eq("poi_id", poiId), Expr.eq("poi_cmpy_id", cmpyId)),
                           Expr.and(Expr.eq("loc_start_poi_id", poiId), Expr.eq("loc_start_poi_cmpy_id", cmpyId))),
                   Expr.and(Expr.eq("loc_finish_poi_id", poiId), Expr.eq("loc_finish_poi_cmpy_id", cmpyId)))
               .findCount();
  }


  public MetaDataWrapper getMetadata() {
    if (this.meta == null) {
      return null;
    }

    if (this.metadata != null) {
      return metadata;
    }

    ObjectMapper m = new ObjectMapper();
    m.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    try {
      metadata = m.readValue(this.meta, MetaDataWrapper.class);
    }
    catch (Exception je) {
      Log.err("Failed to unmarshal Template Metadata from JSON:", je);
    }
    return metadata;
  }

  public void setMetadata(MetaDataWrapper md) {
    ObjectMapper m = new ObjectMapper();
    m.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    //Needs this black magic to avoid picking up methods that looks like real fields
    m.setVisibility(m.getSerializationConfig()
                     .getDefaultVisibilityChecker()
                     .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                     .withIsGetterVisibility(JsonAutoDetect.Visibility.NONE)
                     .withGetterVisibility(JsonAutoDetect.Visibility.NONE));
    String result = null;

    try {
      //result = m.writerWithDefaultPrettyPrinter().writeValueAsString(idw);
      result = m.writeValueAsString(md);
    }
    catch (JsonProcessingException jpe) {
      Log.err("Failed to generate Template metadata JSON Data:", jpe);
    }

    this.metadata = md;
    this.setMeta(result);
  }

  public static List<TmpltDetails> findForSpecificTripDay(String templateid, int dayOffset) {
    return find.where().eq("templateid",templateid).eq("day_offset", dayOffset).findList();
  }

  public static int markDeleted(String templateid) {
    Update<TmpltDetails> upd = Ebean.createUpdate(TmpltDetails.class,
                                                  "UPDATE tmplt_details " +
                                                  "SET  status = -1 " +
                                                  "WHERE templateid=:tmpltId");
    upd.set("tmpltId", templateid);
    return upd.execute();
  }

  public static int wipeForTemplate(String templateid) {
    Update<TmpltDetails> upd = Ebean.createUpdate(TmpltDetails.class, "DELETE FROM tmplt_details WHERE templateid=:tmpltId");
    upd.set("tmpltId", templateid);
    return upd.execute();
  }

  public static int mergeCmpy(Integer srcCmpyId, Integer targetCmpyId, String userId) {
    int recUpdated = 0;
    String poiCmpyMain = "UPDATE tmplt_details " +
                         "SET poi_cmpy_id = :targetCmpyId, lastupdatedtimestamp = :timestamp, modifiedby = :userid " +
                         "WHERE poi_cmpy_id = :srcCmpyId and status = 0";
    String poiCmpyStart = "UPDATE tmplt_details " +
                          "SET loc_start_poi_cmpy_id = :targetCmpyId, lastupdatedtimestamp = :timestamp, " +
                          "modifiedby = :userid " +
                          "WHERE loc_start_poi_cmpy_id = :srcCmpyId and status = 0";
    String poiCmpyFinish = "UPDATE tmplt_details " +
                           "SET loc_finish_poi_cmpy_id = :targetCmpyId, lastupdatedtimestamp = :timestamp, " +
                           "modifiedby = :userid  " +
                           "WHERE loc_finish_poi_cmpy_id = :srcCmpyId and status = 0";

    SqlUpdate upMain = Ebean.createSqlUpdate(poiCmpyMain);
    upMain.setParameter("timestamp", System.currentTimeMillis());
    upMain.setParameter("userid", userId);
    upMain.setParameter("srcCmpyId", srcCmpyId);
    upMain.setParameter("targetCmpyId", targetCmpyId);
    recUpdated += Ebean.execute(upMain);

    SqlUpdate upStart = Ebean.createSqlUpdate(poiCmpyStart);
    upStart.setParameter("timestamp", System.currentTimeMillis());
    upStart.setParameter("userid", userId);
    upStart.setParameter("srcCmpyId", srcCmpyId);
    upStart.setParameter("targetCmpyId", targetCmpyId);
    recUpdated += Ebean.execute(upStart);

    SqlUpdate upFinish = Ebean.createSqlUpdate(poiCmpyFinish);
    upFinish.setParameter("timestamp", System.currentTimeMillis());
    upFinish.setParameter("userid", userId);
    upFinish.setParameter("srcCmpyId", srcCmpyId);
    upFinish.setParameter("targetCmpyId", targetCmpyId);

    recUpdated += Ebean.execute(upFinish);
    return recUpdated;
  }

  /**
   * Get all trip_details records to be converted
   *
   * @return
   */
  public static List<TmpltDetails> getNotPoiConverted() {
    return find.where().or(isNull("loc_start_poi_id"), isNull("loc_finish_poi_id")).findList();
  }

  public static TmpltDetails buildTmpltDetails(String userId) {
    TmpltDetails tmpltDetails = new TmpltDetails();
    tmpltDetails.setDetailsid(Utils.getUniqueId());
    tmpltDetails.setCreatedtimestamp(System.currentTimeMillis());
    tmpltDetails.setLastupdatedtimestamp(tmpltDetails.createdtimestamp);
    tmpltDetails.setCreatedby(userId);
    tmpltDetails.setModifiedby(userId);
    return tmpltDetails;
  }

  public static List<TmpltDetails> findByTemplateId(String templateid) {
    return find.where().eq("templateid", templateid).eq("status", 0).orderBy("day_offset,rank asc").findList();
  }

  public static List<TmpltDetails> findByTemplateIdType(String templateid, ReservationType type) {
    return find.where().eq("templateid", templateid).eq("status", 0).eq("detailtypeid", type).findList();
  }

  public static List<TmpltDetails> findCuisesByPoiIdDates(long shipPoiId, long starttimestamp, long endtimestamp) {
    return find.where().eq("poi_id", shipPoiId).eq("detailtypeid", ReservationType.CRUISE).eq("status", 0)
               .ge("starttimestamp", starttimestamp).le("starttimestamp", endtimestamp)
               .orderBy("starttimestamp asc, name asc").findList();
  }



  public static List <TmpltDetails> findCruisesWrongPort() {
    String sql = "loc_start_poi_id not in (select poi_id from poi where type_id = 7 and src_id = 4)";


    return find.where()
               .eq("detailtypeid", 16)
               .raw(sql)
               .findList();


  }

  public static int countByStartPoiId (long poiId) {
    return find.where().eq("loc_start_poi_id", poiId).eq("status", 0).findCount();
  }

  public String getMeta() {
    return meta;
  }

  public void setMeta(String meta) {
    this.meta = meta;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getPoiId() {
    return poiId;
  }

  public void setPoiId(Long poiId) {
    this.poiId = poiId;
  }

  public Integer getPoiCmpyId() {
    return poiCmpyId;
  }

  public void setPoiCmpyId(Integer poiCmpyId) {
    this.poiCmpyId = poiCmpyId;
  }

  public Long getLocStartPoiId() {
    return locStartPoiId;
  }

  public void setLocStartPoiId(Long locStartPoiId) {
    this.locStartPoiId = locStartPoiId;
  }

  public Integer getLocStartPoiCmpyId() {
    return locStartPoiCmpyId;
  }

  public void setLocStartPoiCmpyId(Integer locStartPoiCmpyId) {
    this.locStartPoiCmpyId = locStartPoiCmpyId;
  }

  public Long getLocFinishPoiId() {
    return locFinishPoiId;
  }

  public void setLocFinishPoiId(Long locFinishPoiId) {
    this.locFinishPoiId = locFinishPoiId;
  }

  public Integer getLocFinishPoiCmpyId() {
    return locFinishPoiCmpyId;
  }

  public void setLocFinishPoiCmpyId(Integer locFinishPoiCmpyId) {
    this.locFinishPoiCmpyId = locFinishPoiCmpyId;
  }

  public int getDuration() {
    return duration;
  }

  public void setDuration(int duration) {
    this.duration = duration;
  }

  public int getDayOffset() {
    return dayOffset;
  }

  public void setDayOffset(int dayOffset) {
    this.dayOffset = dayOffset;
  }

  public String getLocStartName() {
    return locStartName;
  }

  public void setLocStartName(String locStartName) {
    this.locStartName = locStartName;
  }

  public float getLocFinishLong() {
    return locFinishLong;
  }

  public void setLocFinishLong(float locFinishLong) {
    this.locFinishLong = locFinishLong;
  }

  public float getLocFinishLat() {
    return locFinishLat;
  }

  public void setLocFinishLat(float locFinishLat) {
    this.locFinishLat = locFinishLat;
  }

  public String getLocFinishName() {
    return locFinishName;
  }

  public void setLocFinishName(String locFinishName) {
    this.locFinishName = locFinishName;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getDetailsid() {
    return detailsid;
  }

  public void setDetailsid(String detailsid) {
    this.detailsid = detailsid;
  }

  public String getTemplateid() {
    return templateid;
  }

  public void setTemplateid(String templateId) {
    this.templateid = templateId;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public ReservationType getDetailtypeid() {
    return detailtypeid;
  }

  public void setDetailtypeid(ReservationType detailtypeid) {
    this.detailtypeid = detailtypeid;
  }

  public float getLocStartLong() {
    return locStartLong;
  }

  public void setLocStartLong(float loclong) {
    this.locStartLong = loclong;
  }

  public float getLocStartLat() {
    return locStartLat;
  }

  public void setLocStartLat(float loclat) {
    this.locStartLat = loclat;
  }

  public Long getStarttimestamp() {
    return starttimestamp;
  }

  public void setStarttimestamp(Long starttimestamp) {
    this.starttimestamp = starttimestamp;
  }

  public Long getEndtimestamp() {
    return endtimestamp;
  }

  public void setEndtimestamp(Long endtimestamp) {
    this.endtimestamp = endtimestamp;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }

  public Map<String, Object> getReservationData() {
    return reservationData;
  }

  public void setReservationData(Map<String, Object> reservationData) {
    this.reservationData = reservationData;
  }
  
  public UmReservation getReservation() {
    if (reservation == null) {
      if (reservationData != null) {
        reservation = unmarshal(reservationData, UmReservation.class);
      }
    }
    return reservation;
  }
  
  public void setReservation(UmReservation reservation) {
    this.reservation = reservation;
    this.markAsDirty();
  }

  //Added by George:
  public static TmpltDetails findByPK(String pk) {
    return find.byId(pk);
  }
  
  /**
   * This is sync the transient reservation back to the persisted field reservationData
   * before the persiste happends
   * 
   * @throws IOException
   */
  @PrePersist
  @PreUpdate
  public void preparePersist() throws IOException { 
    if (reservation != null) {
        reservationData = marshal(reservation);
    }
  }
    
  @SuppressWarnings("unchecked")
  @PostLoad
  public void migrationIfNeeded() throws IOException {
   Migrator<TmpltDetails> m = (Migrator<TmpltDetails>) PersistenceUtilFactory.getMigrator(TmpltDetails.class);
   m.migrate(this);
  }
}
