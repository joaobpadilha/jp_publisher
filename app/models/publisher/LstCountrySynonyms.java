package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2014-10-21.
 */
@Entity
@Table(name = "lst_country_synonyms")
public class LstCountrySynonyms
    extends Model {
  public static Finder<String, LstCountrySynonyms> find = new Finder<>(LstCountrySynonyms.class);
  @Id
  CtrySynonymKey key;

  @Embeddable
  public static class CtrySynonymKey {
    @Column(name = "alpha3")
    String alpha3;
    @Column(name = "locale")
    String locale;
    @Column(name = "name")
    String name;

    public String getAlpha3() {
      return alpha3;
    }

    public void setAlpha3(String alpha3) {
      this.alpha3 = alpha3;
    }

    public String getLocale() {
      return locale;
    }

    public void setLocale(String locale) {
      this.locale = locale;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    @Override
    public int hashCode() {
      return alpha3.hashCode() * locale.hashCode() * name.hashCode();
    }

    @Override
    public boolean equals(Object o) {
      if (o == null) {
        return false;
      }
      if (!(o instanceof CtrySynonymKey)) {
        return false;
      }
      CtrySynonymKey ok = (CtrySynonymKey) o;
      if (!ok.alpha3.equals(alpha3)) {
        return false;
      }
      if (!ok.locale.equals(locale)) {
        return false;
      }
      if (!ok.name.equals(name)) {
        return false;
      }
      return true;
    }
  }

  public static List<LstCountrySynonyms> getAll() {
    return find.all();
  }

  public static List<LstCountrySynonyms> getAllForLocale(String locale) {
    return find.where().eq("locale", locale.toLowerCase()).findList();
  }

  public CtrySynonymKey getKey() {
    return key;
  }

  public void setKey(CtrySynonymKey key) {
    this.key = key;
  }

  public String getAlpha3() {
    return key.alpha3;
  }

  public void setAlpha3(String alpha3) {
    this.key.alpha3 = alpha3;
  }

  public String getLocale() {
    return key.locale;
  }

  public void setLocale(String locale) {
    this.key.locale = locale;
  }

  public String getName() {
    return key.name;
  }

  public void setName(String name) {
    this.key.name = name;
  }
}
