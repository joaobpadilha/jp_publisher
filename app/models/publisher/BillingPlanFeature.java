package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.SqlUpdate;

import javax.persistence.*;

/**
 * Created by surge on 2015-12-11.
 */
@Entity
@Table(name = "bil_plan_feature")
public class BillingPlanFeature
    extends Model {
  public static Finder<BillingPlanFeatureID, BillingPlanFeature> find = new Finder(BillingPlanFeature.class);
  @Id
  BillingPlanFeatureID pk;

  @Embeddable
  public static class BillingPlanFeatureID {

    /**
     * Plan ID
     */
    @Column(name = "plan_id")
    public Long    planId;
    /**
     * Feature ID
     */
    @Column(name = "feat_pk ")
    public Integer featurePk;

    public Long getPlanId() {
      return planId;
    }

    public void setPlanId(Long planId) {
      this.planId = planId;
    }

    public Integer getFeaturePk() {
      return featurePk;
    }

    public void setFeaturePk(Integer featurePk) {
      this.featurePk = featurePk;
    }

    @Override
    public int hashCode() {
      return planId.hashCode() * featurePk.hashCode();
    }

    @Override
    public boolean equals(Object o) {
      if (o == null) {
        return false;
      }
      if (!(o instanceof BillingPlanFeatureID)) {
        return false;
      }
      BillingPlanFeatureID ok = (BillingPlanFeatureID) o;
      return ok.planId.equals(planId) && ok.featurePk.equals(featurePk);
    }
  }

  public static BillingPlanFeature build(Long planId, Integer featurePk) {
    BillingPlanFeature   bpf   = new BillingPlanFeature();
    BillingPlanFeatureID bpfId = new BillingPlanFeatureID();
    bpfId.planId = planId;
    bpfId.featurePk = featurePk;
    bpf.setPk(bpfId);
    return bpf;
  }

  public static int deleteAllForPlan(Long planId) {
    String s = "DELETE FROM bil_plan_feature " + "WHERE plan_id = :planId";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("planId", planId);
    return Ebean.execute(update);

  }

  public BillingPlanFeatureID getPk() {
    return pk;
  }

  public void setPk(BillingPlanFeatureID pk) {
    this.pk = pk;
  }

}
