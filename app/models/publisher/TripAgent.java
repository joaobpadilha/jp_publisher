package models.publisher;

import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-14
 * Time: 1:46 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="trip_agent")
public class TripAgent extends Model {
    public String comments;
    public String agencyname;
    public String groupid;


    @Id
    public String pk;

    @Constraints.Required
    public String tripid;
    public String name;
    public String email;


    public int status;
    public Long createdtimestamp;
    public Long lastupdatedtimestamp;
    public String createdby;
    public String modifiedby;

    @Version
    public int version;

    public static Model.Finder<String,TripAgent> find = new Finder(TripAgent.class);


    public static boolean isAgentInTrip (String tripid, String email) {
        List<TripAgent> list = find.where().eq("tripid", tripid).eq("email", email).eq("status", 0).findList();
        if (list != null && list.size() > 0) {
            return true;
        }

        return false;

    }

    public static boolean isAgentInTripDefaultGroup (String tripid, String email) {
        List<TripAgent> list = find.where().eq("tripid", tripid).isNull("groupid").eq("email", email).eq("status", 0).findList();
        if (list != null && list.size() > 0) {
            return true;
        }

        return false;

    }

    public static  List<TripAgent> getByTripId (String tripid) {
        return find.where().eq("tripid", tripid).eq("status", 0).findList();
    }

    public static  List<TripAgent> getByTripIdDefaultGroup (String tripid) {
        return find.where().eq("tripid", tripid).isNull("groupid").eq("status", 0).findList();
    }

    public static  List<TripAgent> getByTripIdGroupId (String tripid, String groupId) {
        return find.where().eq("tripid", tripid).eq("groupId", groupId).eq("status", 0).findList();
    }


    public static  List<TripAgent> getByEmail (String email) {
        return find.where().eq("email", email).eq("status", 0).findList();
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }



    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getTripid() {
        return tripid;
    }

    public void setTripid(String tripid) {
        this.tripid = tripid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getCreatedtimestamp() {
        return createdtimestamp;
    }

    public void setCreatedtimestamp(Long createdtimestamp) {
        this.createdtimestamp = createdtimestamp;
    }

    public Long getLastupdatedtimestamp() {
        return lastupdatedtimestamp;
    }

    public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
        this.lastupdatedtimestamp = lastupdatedtimestamp;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(String modifiedby) {
        this.modifiedby = modifiedby;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getAgencyname() {
        return agencyname;
    }

    public void setAgencyname(String agencyname) {
        this.agencyname = agencyname;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }
}

