package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by surge on 2014-10-21.
 */
@Entity @Table(name = "lst_country")
public class LstCountry
    extends Model {
  public static Model.Finder<String, LstCountry> find = new Finder(LstCountry.class);

  @Id @Column(name = "alpha3")
  private String alpha3;
  private String name;
  private String alpha2;
  private Integer countryCode;
  private Integer regionCode;
  private Integer subregionCode;
  private String phoneCode;
  @Column(name = "fips10_4")
  private String fips104;
  private String inet;
  private String notes;
  private String continentId;

  public static List<LstCountry> getAll() {
    return find.all();
  }

  public String getAlpha3() {
    return alpha3;
  }

  public void setAlpha3(String alpha3) {
    this.alpha3 = alpha3;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAlpha2() {
    return alpha2;
  }

  public void setAlpha2(String alpha2) {
    this.alpha2 = alpha2;
  }

  public Integer getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(Integer countryCode) {
    this.countryCode = countryCode;
  }

  public Integer getRegionCode() {
    return regionCode;
  }

  public void setRegionCode(Integer regionCode) {
    this.regionCode = regionCode;
  }

  public Integer getSubregionCode() {
    return subregionCode;
  }

  public void setSubregionCode(Integer subregionCode) {
    this.subregionCode = subregionCode;
  }

  public String getPhoneCode() {
    return phoneCode;
  }

  public void setPhoneCode(String phoneCode) {
    this.phoneCode = phoneCode;
  }

  public String getFips104() {
    return fips104;
  }

  public void setFips104(String fips104) {
    this.fips104 = fips104;
  }

  public String getInet() {
    return inet;
  }

  public void setInet(String inet) {
    this.inet = inet;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public String getContinentId() {
    return continentId;
  }

  public void setContinentId(String continentId) {
    this.continentId = continentId;
  }
}
