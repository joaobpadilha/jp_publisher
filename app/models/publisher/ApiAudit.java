package models.publisher;

import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-24
 * Time: 8:59 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity @Table(name = "Api_Audit")
public class ApiAudit
    extends Model {
  public static Model.Finder<String, ApiAudit> find = new Finder(ApiAudit.class);
  @Id
  public String pk;
  @Constraints.Required
  public String cmpyid;
  public String tripid;
  public String userid;
  public int apitype;
  public int numattachments;
  public Long createdtimestamp;
  @Version
  public int version;

  public String getPk() {
    return pk;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public String getCmpyid() {
    return cmpyid;
  }

  public void setCmpyid(String cmpyid) {
    this.cmpyid = cmpyid;
  }

  public String getTripid() {
    return tripid;
  }

  public void setTripid(String tripid) {
    this.tripid = tripid;
  }

  public String getUserid() {
    return userid;
  }

  public void setUserid(String userid) {
    this.userid = userid;
  }

  public int getApitype() {
    return apitype;
  }

  public void setApitype(int apitype) {
    this.apitype = apitype;
  }

  public int getNumattachments() {
    return numattachments;
  }

  public void setNumattachments(int numattachments) {
    this.numattachments = numattachments;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
