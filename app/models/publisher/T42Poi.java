package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Created by surge on 2015-11-24.
 */
@Entity @Table(name = "t42_poi")
public class T42Poi
    extends Model {

  public static Model.Finder<Integer, T42Poi> find = new Finder(T42Poi.class);

  @Id Integer poiPlaceKey;
  String classId;
  String poiName;
  String address1;
  String address2;
  String zipCode;
  Float latitude;
  Float longitude;
  Integer geoPlaceKey;
  String geoDisplay;
  String phone;
  String tollfree;
  String fax;
  String website;
  String shortDesc;
  String hrsOper;
  Integer costRate1;
  Integer costRate2;
  String feesDetail;
  String pmtAccept;
  String pmtAcceptDetail;
  String resvPolicy;
  String resvDetail;
  String dressCode;
  String comments;
  Boolean active;
  @Version int version;

  public static T42Poi findByKey(String placeKey) {
    Integer key = null;
    try {
      key = Integer.parseInt(placeKey);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
      return null;
    }
    return find.byId(key);
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public Integer getPoiPlaceKey() {
    return poiPlaceKey;
  }

  public void setPoiPlaceKey(Integer poiPlaceKey) {
    this.poiPlaceKey = poiPlaceKey;
  }

  public void setPoiPlaceKey(String poiPlaceKey) {
    try {
      this.poiPlaceKey = Integer.parseInt(poiPlaceKey);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public String getClassId() {
    return classId;
  }

  public void setClassId(String classId) {
    this.classId = classId;
  }

  public String getPoiName() {
    return poiName;
  }

  public void setPoiName(String poiName) {
    this.poiName = poiName;
  }

  public String getAddress1() {
    return address1;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public String getAddress2() {
    return address2;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public Float getLatitude() {
    return latitude;
  }

  public void setLatitude(String latitude) {
    if (latitude.length() == 0) {
      return;
    }
    try {
      this.latitude = Float.parseFloat(latitude);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public void setLatitude(Float latitude) {
    this.latitude = latitude;
  }

  public Float getLongitude() {
    return longitude;
  }

  public void setLongitude(String longitude) {
    if (longitude.length() == 0) {
      return;
    }

    try {
      this.longitude = Float.parseFloat(longitude);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public void setLongitude(Float longitude) {
    this.longitude = longitude;
  }

  public Integer getGeoPlaceKey() {
    return geoPlaceKey;
  }

  public void setGeoPlaceKey(String geoPlaceKey) {
    try {
      this.geoPlaceKey = Integer.parseInt(geoPlaceKey);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public void setGeoPlaceKey(Integer geoPlaceKey) {
    this.geoPlaceKey = geoPlaceKey;
  }

  public String getGeoDisplay() {
    return geoDisplay;
  }

  public void setGeoDisplay(String geoDisplay) {
    this.geoDisplay = geoDisplay;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getTollfree() {
    return tollfree;
  }

  public void setTollfree(String tollfree) {
    this.tollfree = tollfree;
  }

  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  public String getWebsite() {
    return website;
  }

  public void setWebsite(String website) {
    this.website = website;
  }

  public String getShortDesc() {
    return shortDesc;
  }

  public void setShortDesc(String shortDesc) {
    this.shortDesc = shortDesc;
  }

  public String getHrsOper() {
    return hrsOper;
  }

  public void setHrsOper(String hrsOper) {
    this.hrsOper = hrsOper;
  }

  public Integer getCostRate1() {
    return costRate1;
  }

  public void setCostRate1(String costRate) {
    try {
      this.costRate1 = Integer.parseInt(costRate);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public void setCostRate1(Integer costRate1) {
    this.costRate1 = costRate1;
  }

  public void setCostRate2(String costRate) {
    try {
      this.costRate2 = Integer.parseInt(costRate);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public Integer getCostRate2() {
    return costRate2;
  }

  public void setCostRate2(Integer costRate2) {
    this.costRate2 = costRate2;
  }

  public String getFeesDetail() {
    return feesDetail;
  }

  public void setFeesDetail(String feesDetail) {
    this.feesDetail = feesDetail;
  }

  public String getPmtAccept() {
    return pmtAccept;
  }

  public void setPmtAccept(String pmtAccept) {
    this.pmtAccept = pmtAccept;
  }

  public String getPmtAcceptDetail() {
    return pmtAcceptDetail;
  }

  public void setPmtAcceptDetail(String pmtAcceptDetail) {
    this.pmtAcceptDetail = pmtAcceptDetail;
  }

  public String getResvPolicy() {
    return resvPolicy;
  }

  public void setResvPolicy(String resvPolicy) {
    this.resvPolicy = resvPolicy;
  }

  public String getResvDetail() {
    return resvDetail;
  }

  public void setResvDetail(String resvDetail) {
    this.resvDetail = resvDetail;
  }

  public String getDressCode() {
    return dressCode;
  }

  public void setDressCode(String dressCode) {
    this.dressCode = dressCode;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
