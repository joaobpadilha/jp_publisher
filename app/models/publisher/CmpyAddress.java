package models.publisher;

import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-15
 * Time: 11:26 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="cmpy_addr")
public class CmpyAddress extends Model {
    public float loclat;
    public float loclong;

    public  String web;
    public  String facebook;
    public  String email;
    public  String twitter;
    public  String phone;
    public  String fax;

    public  String streetaddr1;
    public  String streetaddr2;
    public  String city;
    public  String zipcode;
    public  String state;

    public  String country;
    public  String addresstype;

    @Id
    public String cmpyaddrid;

    @Constraints.Required
    public String cmpyid;
    public String name;
    public int status;
    public Long createdtimestamp;
    public Long lastupdatedtimestamp;
    public String createdby;
    public String modifiedby;

    @Version
    public int version;

    public static Model.Finder<String,CmpyAddress> find = new Finder(CmpyAddress.class);

    public static List<CmpyAddress> findAll() {
        return find.all();
    }

    public static CmpyAddress findByPK(String pk) {
        return find.byId (pk);
    }

    public static List<CmpyAddress> findActiveByCmpyId(String cmpyId) {
        return find.where().eq("cmpyid", cmpyId).eq("status", 0).findList();
    }

    public static List<CmpyAddress> findMainActiveByCmpyId(String cmpyId) {
        return find.where().eq("cmpyid",cmpyId).eq("addresstype", "1").eq("status", 0).findList();
    }

    public String formatAddress() {
        StringBuffer sb = new StringBuffer();
        if (streetaddr1 != null && streetaddr1.trim().length() > 0)
            sb.append(streetaddr1);

        if (city != null && city.trim().length() > 0) {
            if (sb.length() > 0)
                sb.append(", ");
            sb.append(city);
        }

        if (state != null && state.trim().length() > 0) {
            if (sb.length() > 0)
                sb.append(", ");
            sb.append(state);
        }

        if (country != null && country.trim().length() > 0) {
            if (sb.length() > 0)
                sb.append(", ");
            sb.append(country);
        }
        return sb.toString();
    }

    public String formatAddress1() {
        StringBuffer sb = new StringBuffer();
        if (streetaddr1 != null && streetaddr1.trim().length() > 0)
            sb.append(streetaddr1);

        if (streetaddr2 != null && streetaddr2.trim().length() > 0) {
            if (sb.length() > 0)
                sb.append(", ");
            sb.append(streetaddr2);
        }

        return sb.toString();
    }

    public String formatAddress2() {
        StringBuffer sb = new StringBuffer();

        if (city != null && city.trim().length() > 0) {
            sb.append(city);
        }

        if (state != null && state.trim().length() > 0) {
            if (sb.length() > 0)
                sb.append(", ");
            sb.append(state);
        }

        if (country != null && country.trim().length() > 0) {
            if (sb.length() > 0)
                sb.append(", ");
            sb.append(country);
        }
        return sb.toString();
    }


    public float getLoclat() {
        return loclat;
    }

    public void setLoclat(float loclat) {
        this.loclat = loclat;
    }

    public float getLoclong() {
        return loclong;
    }

    public void setLoclong(float loclong) {
        this.loclong = loclong;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getStreetaddr1() {
        return streetaddr1;
    }

    public void setStreetaddr1(String streetaddr1) {
        this.streetaddr1 = streetaddr1;
    }

    public String getStreetaddr2() {
        return streetaddr2;
    }

    public void setStreetaddr2(String streetaddr2) {
        this.streetaddr2 = streetaddr2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddresstype() {
        return addresstype;
    }

    public void setAddresstype(String addresstype) {
        this.addresstype = addresstype;
    }

    public String getCmpyaddrid() {
        return cmpyaddrid;
    }

    public void setCmpyaddrid(String cmpyaddrid) {
        this.cmpyaddrid = cmpyaddrid;
    }

    public String getCmpyid() {
        return cmpyid;
    }

    public void setCmpyid(String cmpyid) {
        this.cmpyid = cmpyid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getCreatedtimestamp() {
        return createdtimestamp;
    }

    public void setCreatedtimestamp(Long createdtimestamp) {
        this.createdtimestamp = createdtimestamp;
    }

    public Long getLastupdatedtimestamp() {
        return lastupdatedtimestamp;
    }

    public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
        this.lastupdatedtimestamp = lastupdatedtimestamp;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(String modifiedby) {
        this.modifiedby = modifiedby;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
