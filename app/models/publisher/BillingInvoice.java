package models.publisher;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freshbooks.model.Invoice;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.utils.Log;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2015-03-26.
 */
@Entity @Table(name = "bil_invoice")
public class BillingInvoice
    extends Model {

  public static Model.Finder<Long, BillingInvoice> find = new Finder(BillingInvoice.class);

  @Id
  private Long invId;
  @Column(name = "userid")
  private String userId;
  @Column(name = "cmpyid")
  private String cmpyId;
  private String data;
  private long startTs;
  private long endTs;
  private long dueTs;
  @Enumerated(value = EnumType.STRING)
  private InvoiceState state;
  private Float amount;
  @Column(name = "pmnt_ts")
  private Long paymentTs;
  @Column(name = "pmnt_type")
  private BillingPmntMethod paymentType;
  @Column(name = "pmnt_token")
  private String paymentToken;
  private String metadata;
  private Long createdTs;
  private Long modifiedTs;
  private String createdBy;
  private String modifiedBy;
  @Version
  private int version;

  public static enum InvoiceState {
    /**
     * Error state is when one of the required fields is not available
     */
    ERROR,
    /**
     * Invoice exists only locally
     */
    LOCAL,
    /**
     * Invoice was pushed to a billing vendor
     */
    PUSHED,
    /**
     * Invoice was sent to the subscriber
     */
    SENT,
    /**
     * Invoice was paid in full
     */
    PAID,
    /**
     * Invoice payment is overdue
     */
    OVERDUE
  }

  /**
   * If in the future we will have various types of invoices each will be
   * assigned to a dedicated variable in this wrapper class. For now freshbooks
   * is the only type.
   */
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static class InvoiceDataWrapper {
    public Invoice freshbooks;
  }

  public static List<BillingInvoice> getAllBetweenTs(long startTs, long finishTs) {
    return find.where().ge("start_ts", startTs).le("end_ts", finishTs).findList();
  }

  public static List<BillingInvoice> getSendable(long startTs, long finishTs) {
    return find.where()
               .ge("start_ts", startTs)
               .le("end_ts", finishTs)
               .eq("state", "PUSHED")
               .findList();
  }

  public static boolean invoiceExistsForCmpy(String cmpyId, long startTs) {
    return find.where().eq("start_ts", startTs).eq("cmpyid", cmpyId).findCount() > 0;
  }

  public static boolean invoiceExistsForUser(String userId, long startTs) {
    return find.where().eq("start_ts", startTs).eq("userid", userId).findCount() > 0;
  }

  public static boolean invoiceExistsForUser(String userId) {
    return find.where().eq("userid", userId).findCount() > 0;
  }

  public static BillingInvoice buildInvoice(String createadBy) {
    BillingInvoice bi = new BillingInvoice();
    bi.setInvId(DBConnectionMgr.getUniqueLongId());
    bi.setCreatedBy(createadBy);
    bi.setModifiedBy(createadBy);
    long ts = System.currentTimeMillis();
    bi.setCreatedTs(ts);
    bi.setModifiedTs(ts);
    return bi;
  }

  public void markModified(String modifiedBy){
    this.setModifiedBy(modifiedBy);
    this.setModifiedTs(System.currentTimeMillis());
  }

  public Float getAmount() {
    return amount;
  }

  public void setAmount(Float amount) {
    this.amount = amount;
  }

  public InvoiceState getState() {
    return state;
  }

  public void setState(InvoiceState state) {
    this.state = state;
  }

  public Invoice getFreshbookInvoiceData() {
    ObjectMapper m = new ObjectMapper();
    InvoiceDataWrapper idw = null;
    try {
      idw = m.readValue(this.data, InvoiceDataWrapper.class);
    }
    catch (Exception je) {
      Log.err("Failed to unmarshal Freshbooks Invoice Data from JSON:", je);
    }

    return idw.freshbooks;
  }

  public void setFreshbookInvoiceData(Invoice invoice) {
    ObjectMapper m = new ObjectMapper();
    m.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    //Needs this black magic to avoid picking up methods that looks like real fields
    m.setVisibility(m.getSerializationConfig()
                     .getDefaultVisibilityChecker()
                     .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                     .withIsGetterVisibility(JsonAutoDetect.Visibility.NONE));
    String result = null;

    InvoiceDataWrapper idw = new InvoiceDataWrapper();
    idw.freshbooks = invoice;

    try {
      //result = m.writerWithDefaultPrettyPrinter().writeValueAsString(idw);
      result = m.writeValueAsString(idw);
    }
    catch (JsonProcessingException jpe) {
      Log.err("Failed to generate Invoice JSON Data:", jpe);
    }

    this.setData(result);
  }

  public long getInvId() {
    return invId;
  }

  public void setInvId(long invId) {
    this.invId = invId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getCmpyId() {
    return cmpyId;
  }

  public void setCmpyId(String cmpyId) {
    this.cmpyId = cmpyId;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  public long getStartTs() {
    return startTs;
  }

  public void setStartTs(long startTs) {
    this.startTs = startTs;
  }

  public long getEndTs() {
    return endTs;
  }

  public void setEndTs(long endTs) {
    this.endTs = endTs;
  }

  public long getDueTs() {
    return dueTs;
  }

  public void setDueTs(long dueTs) {
    this.dueTs = dueTs;
  }

  public Long getPaymentTs() {
    return paymentTs;
  }

  public void setPaymentTs(Long paymentTs) {
    this.paymentTs = paymentTs;
  }

  public BillingPmntMethod getPaymentType() {
    return paymentType;
  }

  public void setPaymentType(BillingPmntMethod paymentType) {
    this.paymentType = paymentType;
  }

  public String getPaymentToken() {
    return paymentToken;
  }

  public void setPaymentToken(String paymentToken) {
    this.paymentToken = paymentToken;
  }

  public String getMetadata() {
    return metadata;
  }

  public void setMetadata(String metadata) {
    this.metadata = metadata;
  }

  public Long getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Long createdTs) {
    this.createdTs = createdTs;
  }

  public Long getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Long modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

}
