package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbJsonB;
import com.avaje.ebean.annotation.EnumValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.mapped.publisher.utils.Log;
import com.umapped.persistence.accountprop.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * Created by surge on 2016-02-24.
 */
@Entity
@Table(name = "account_prop")
public class AccountProp
    extends Model {
  @Transient
  public static final ObjectMapper om;

  /**
   * 3-letter type to identify property set
   */
  public enum PropertyType {
    @EnumValue("CNT")
    CONTACT,
    @EnumValue("PHT")
    PHOTO,
    @EnumValue("TPR")
    TRIP_PREFERENCE,
    @EnumValue("APR")
    ACCOUNT_PREFERENCE,
    @EnumValue("BIL")
    BILLING_INFO
  }

  public static Model.Finder<APPk, AccountProp> find = new Finder(AccountProp.class);

  static {
    om = new ObjectMapper();
    //Perform any kind of configuration for object mapper in this area
  }

  @Transient
  public AccountContact accountContact;
  public TripPrefs tripPreferences;
  public AccountPrefs accountPreferences;
  public AccountBilling billingProperties;

  @Id
  APPk     pk;
  /**
   * Extended property set
   */
  @DbJsonB
  JsonNode meta;
  /**
   * Primary string value for the property
   */
  String    sVal;
  /**
   * Primary Long value (for using in FK references)
   */
  Long      lVal;
  Timestamp createdTs;
  Long      createdBy;
  Timestamp modifiedTs;
  Long      modifiedBy;
  @Version
  int version;

  @Embeddable
  public static class APPk {
    @Column(name = "uid")
    Long uid;

    /**
     * 3-letter type to identify property set
     */
    @Column(name = "prop_type")
    @Enumerated
    PropertyType propType;

    public Long getUid() {
      return uid;
    }

    public void setUid(Long uid) {
      this.uid = uid;
    }

    public PropertyType getPropType() {
      return propType;
    }

    public void setPropType(PropertyType propType) {
      this.propType = propType;
    }

    @Override
    public int hashCode() {
      return uid.hashCode() * propType.hashCode();
    }

    @Override
    public boolean equals(Object o) {
      if (o == null) {
        return false;
      }
      if (!(o instanceof APPk)) {
        return false;
      }
      APPk ok = (APPk) o;
      return uid.equals(ok.uid) && propType.equals(ok.propType);
    }
  }

  public static AccountProp build(Long uid, PropertyType type, Long createdBy) {
    AccountProp a   = new AccountProp();
    APPk        apk = new APPk();
    apk.setUid(uid);
    apk.setPropType(type);
    a.setPk(apk);
    a.setCreatedTs(Timestamp.from(Instant.now()));
    a.setCreatedBy(createdBy);
    a.setModifiedTs(a.createdTs);
    a.setModifiedBy(createdBy);
    switch (type) {
      case CONTACT:
        a.accountContact = new AccountContact();
        break;
      case TRIP_PREFERENCE:
        a.tripPreferences = new TripPrefs();
        break;
      case ACCOUNT_PREFERENCE:
        a.accountPreferences = new AccountPrefs();
        break;
      case BILLING_INFO:
        a.billingProperties = new AccountBilling();
      default:
        break;
    }
    return a;
  }

  public static FileInfo getAccountPhoto(Long uid) {
    AccountProp ap = findByPk(uid, PropertyType.PHOTO);
    if (ap != null && ap.getlVal() != null) {
      return FileInfo.find.byId(ap.getlVal());
    }
    return null;
  }

  public static AccountProp findByPk(Long uid, PropertyType prop) {
    APPk apPk = new APPk();
    apPk.setUid(uid);
    apPk.setPropType(prop);
    return find.byId(apPk);
  }

  public Long getlVal() {
    return lVal;
  }

  public void setlVal(Long lVal) {
    this.lVal = lVal;
  }

  public void markModified(Long modifiedBy) {
    setModifiedBy(modifiedBy);
    setModifiedTs(Timestamp.from(Instant.now()));
  }

  public String getsVal() {
    return sVal;
  }

  public void setsVal(String sVal) {
    this.sVal = sVal;
  }

  @PrePersist
  public void prePersist() {
    switch (pk.propType) {
      case CONTACT:
        accountContact.prePersist();
        setMeta(om.valueToTree(accountContact));
        return;
      case TRIP_PREFERENCE:
        tripPreferences.prePersist();
        setMeta(om.valueToTree(tripPreferences));
        return;
      case ACCOUNT_PREFERENCE:
        accountPreferences.prePersist();
        setMeta(om.valueToTree(accountPreferences));
        return;
      case BILLING_INFO:
        billingProperties.prePersist();
        setMeta(om.valueToTree(billingProperties));
        return;
      default:
        setMeta(null);
        break;
    }
  }

  @PostLoad
  public void postLoad() {
    ObjectReader or;
    try {
      switch (pk.propType) {
        case CONTACT:
          or = om.readerFor(AccountContact.class);
          accountContact = or.treeToValue(meta, AccountContact.class);
          return;
        case TRIP_PREFERENCE:
          or = om.readerFor(TripPrefs.class);
          tripPreferences = or.treeToValue(meta, TripPrefs.class);
          return;
        case ACCOUNT_PREFERENCE:
          or = om.readerFor(AccountPrefs.class);
          accountPreferences = or.treeToValue(meta, AccountPrefs.class);
          return;
        case BILLING_INFO:
          or = om.readerFor(AccountBilling.class);
          billingProperties = or.treeToValue(meta, AccountBilling.class);
        default:
          break;
      }
    }
    catch (JsonProcessingException e) {
      Log.err("AccountProp: Failed to parse JSON tree");
    }
  }

  public APPk getPk() {
    return pk;
  }

  public void setPk(APPk pk) {
    this.pk = pk;
  }

  public JsonNode getMeta() {
    return meta;
  }

  public void setMeta(JsonNode meta) {
    this.meta = meta;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
  }

  public Timestamp getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Timestamp modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
