package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.SqlUpdate;

import javax.persistence.*;
import java.util.List;

/**
 * Created by george on 2016-04-04.
 */
@Entity
@Table(name = "trip_detail_attach")
public class TripDetailAttach
    extends Model {
  public static Finder<TripDetailAttachId, TripDetailAttach> find = new Finder(TripDetailAttach.class);
  @Id
  private TripDetailAttachId detailAttachId;
  private int rank;
  @Version
  private int version;

  @Embeddable
  public static class TripDetailAttachId {
    @Column(name = "detail_id")
    public String detailsId;
    @Column(name = "attach_id")
    public String attachId;

    public String getDetailsId() {
      return detailsId;
    }

    public void setDetailsId(String detailsId) {
      this.detailsId = detailsId;
    }

    public String getAttachId() {
      return attachId;
    }

    public void setAttachId(String attachId) {
      this.attachId = attachId;
    }

    @Override
    public int hashCode() {
      return detailsId.hashCode() * (int) (Long.parseLong(attachId));
    }

    @Override
    public boolean equals(Object o) {
      if (o == null) {
        return false;
      }
      if (!(o instanceof TripDetailAttachId)) {
        return false;
      }
      TripDetailAttachId ok = (TripDetailAttachId) o;
      return (ok.detailsId.equals(detailsId)) && (ok.attachId == attachId);
    }
  }

  public static int maxPageRankByDetailId(String detailId) {
    return find.where().eq("detail_id", detailId).findCount();
  }

  public static List<TripDetailAttach> findActiveByDetailId(String detailid) {
    return find.where().eq("detail_id", detailid).orderBy("rank").findList();
  }

  public static int deleteFile(String detailId, String attachId) {
    String    s      = "DELETE FROM trip_detail_attach WHERE detail_id = :detailsid and attach_id = :attachid";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("detailsid", detailId);
    update.setParameter("attachid", attachId);
    return Ebean.execute(update);
  }

  public static int deleteAllFiles(String attachId) {
    String    s      = "DELETE FROM trip_detail_attach WHERE attach_id = :attachid";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("attachid", attachId);
    return Ebean.execute(update);
  }

  public TripDetailAttachId getDetailAttachId() {
    return detailAttachId;
  }

  public void setDetailAttachId(TripDetailAttachId detailAttachId) {
    this.detailAttachId = detailAttachId;
  }

  public void setDetailAttachId(String detailsId, String attachId) {
    this.detailAttachId = new TripDetailAttachId();
    this.detailAttachId.detailsId = detailsId;
    this.detailAttachId.attachId = attachId;
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
