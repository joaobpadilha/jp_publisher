package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.EnumValue;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * Created by surge on 2016-02-24.
 */
@Entity
@Table(name = "account_group_member")
public class AccountGroupMember
    extends Model {

  public enum MemberType {
    @EnumValue("OWN")
    OWNER
  }

  public static Model.Finder<AGMPk, AccountGroupMember> find = new Finder(AccountGroupMember.class);
  @Id
  AGMPk      pk;
  @Enumerated
  MemberType memberType;
  Timestamp createdTs;
  Long      createdBy;
  Timestamp modifiedTs;
  Long      modifiedBy;
  @Version
  int version;

  @Embeddable
  public static class AGMPk {
    @Column(name = "uid")
    Long uid;
    @Column(name = "group_pk")
    Long groupPk;

    @Override
    public int hashCode() {
      return uid.hashCode() * groupPk.hashCode();
    }

    @Override
    public boolean equals(Object o) {
      if (o == null) {
        return false;
      }
      if (!(o instanceof AGMPk)) {
        return false;
      }
      AGMPk ok = (AGMPk) o;

      return uid.equals(ok.uid) && groupPk.equals(ok.groupPk);
    }

    public Long getGroupPk() {
      return groupPk;
    }

    public void setGroupPk(Long groupPk) {
      this.groupPk = groupPk;
    }

    public Long getUid() {
      return uid;
    }

    public void setUid(Long uid) {
      this.uid = uid;
    }
  }

  public static AccountGroupMember findByPk(Long uid, Long groupPk) {
    AGMPk agmPk = new AGMPk();
    agmPk.setUid(uid);
    agmPk.setGroupPk(groupPk);
    return find.byId(agmPk);
  }

  public static AccountGroupMember build(Long uid, Long groupPk, Long createdBy) {
    AccountGroupMember agm   = new AccountGroupMember();
    AGMPk              agmPk = new AGMPk();
    agmPk.setUid(uid);
    agmPk.setGroupPk(groupPk);
    agm.setPk(agmPk);

    agm.setCreatedTs(Timestamp.from(Instant.now()));
    agm.setCreatedBy(createdBy);
    agm.setModifiedTs(agm.createdTs);
    agm.setModifiedBy(createdBy);
    return agm;
  }

  public AGMPk getPk() {
    return pk;
  }

  public void setPk(AGMPk pk) {
    this.pk = pk;
  }

  public MemberType getMemberType() {
    return memberType;
  }

  public void setMemberType(MemberType memberType) {
    this.memberType = memberType;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
  }

  public Timestamp getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Timestamp modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
