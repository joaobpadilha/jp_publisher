package models.publisher;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * Created by surge on 2016-02-24.
 */
@Entity
@Table(name = "account_prop_default")
public class AccountPropDefault
    extends Model {
  @Transient
  public static final ObjectMapper om;
  public static Model.Finder<PDPk, AccountPropDefault> find = new Finder(AccountPropDefault.class);

  static {
    om = new ObjectMapper();
    //Perform any kind of configuration for object mapper in this area
  }

  @Id
  PDPk pk;
  String    defaultValue;
  JsonNode  defaultJson;
  Timestamp createdTs;
  Long      createdBy;
  Timestamp modifiedTs;
  Long      modifiedBy;
  @Version
  int version;

  @Embeddable
  public static class PDPk {
    @Enumerated
    @Column(name = "account_type")
    Account.AccountType      accountType;
    @Enumerated
    @Column(name = "prop_type")
    AccountProp.PropertyType propertyType;

    @Override
    public int hashCode() {
      return accountType.hashCode() * propertyType.hashCode();
    }

    @Override
    public boolean equals(Object o) {

      if (o == null) {
        return false;
      }
      if (!(o instanceof PDPk)) {
        return false;
      }
      PDPk ok = (PDPk) o;

      return accountType.equals(ok.accountType) && propertyType.equals(ok.propertyType);
    }

    public AccountProp.PropertyType getPropertyType() {
      return propertyType;
    }

    public void setPropertyType(AccountProp.PropertyType propertyType) {
      this.propertyType = propertyType;
    }

    public Account.AccountType getAccountType() {
      return accountType;
    }

    public void setAccountType(Account.AccountType accountType) {
      this.accountType = accountType;
    }
  }

  @PrePersist
  public void prePersist() {
    //json.prepareForDb();
    //data = om.valueToTree(json);
  }

  @PostLoad
  public void postLoad() {
    /*
    try {
      ObjectReader or = om.readerFor(PoiJson.class);
      json = or.treeToValue(data, PoiJson.class);
      //json = om.treeToValue(data, PoiJson.class);
    }
    catch (JsonProcessingException e) {
      Log.err("Poi: Failed to parse JSON tree");
    }
    */
  }


  public static AccountPropDefault findByPk(Account.AccountType accountType,
                                            AccountProp.PropertyType propertyType) {
    PDPk pdPk = new PDPk();
    pdPk.setAccountType(accountType);
    pdPk.setPropertyType(propertyType);
    return find.byId(pdPk);
  }
  public static AccountPropDefault build(Account.AccountType accountType,
                                         AccountProp.PropertyType propertyType,
                                         Long createdBy) {
    AccountPropDefault apd = new AccountPropDefault();
    PDPk pdPk = new PDPk();
    pdPk.setAccountType(accountType);
    pdPk.setPropertyType(propertyType);
    apd.setPk(pdPk);
    apd.setCreatedTs(Timestamp.from(Instant.now()));
    apd.setCreatedBy(createdBy);
    apd.setModifiedTs(apd.createdTs);
    apd.setModifiedBy(createdBy);
    return apd;
  }

  public PDPk getPk() {
    return pk;
  }

  public void setPk(PDPk pk) {
    this.pk = pk;
  }

  public String getDefaultValue() {
    return defaultValue;
  }

  public void setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
  }

  public JsonNode getDefaultJson() {
    return defaultJson;
  }

  public void setDefaultJson(JsonNode defaultJson) {
    this.defaultJson = defaultJson;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
  }

  public Timestamp getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Timestamp modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
