package models.publisher;

import com.avaje.ebean.BeanState;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.SqlUpdate;
import com.mapped.persistence.util.DBConnectionMgr;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-18
 * Time: 9:42 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "trip_destination")
public class TripDestination
    extends Model {
  public static Model.Finder<String, TripDestination> find = new Finder(TripDestination.class);
  public String  comments;
  @Id
  public String  tripdestid;
  @Constraints.Required
  public String  tripid;
  public String  destinationid;
  public String  name;
  public int     status;
  public int     rank;
  public Long    createdtimestamp;
  public Long    lastupdatedtimestamp;
  public String  createdby;
  public String  modifiedby;
  public boolean mergebookings;
  @Version
  public int     version;

  public static List<TripDestination> getAllByTripId(String tripid) {
    return find.where().eq("tripid", tripid).orderBy("rank").findList();
  }

  public static List<TripDestination> getActiveByTripId(String tripid) {
    return find.where().eq("tripid", tripid).eq("status", 0).orderBy("rank").findList();
  }

  public static TripDestination getActiveTripDocByTripId(String tripid) {
    List<TripDestination> dests = find.where().eq("tripid", tripid).eq("status", 0).eq("tripdestid", tripid).findList();
    if (dests != null && dests.size() == 1) {
      return dests.get(0);
    }
    return null;
  }

  public static List<TripDestination> findActiveByDest(String destId) {
    return find.where().eq("status", 0).eq("destinationid", destId).orderBy("rank asc").findList();
  }

  public static int deleteByDestinationId(String destinationId, String userId) {
    // example that uses 'named' parameters
    String s = "UPDATE trip_destination set status = -1, lastupdatedtimestamp = :timestamp, modifiedby = " +
               ":userid" + "  where destinationid = :id";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("id", destinationId);
    update.setParameter("timestamp", System.currentTimeMillis());
    update.setParameter("userid", userId);
    return Ebean.execute(update);
  }

  /**
   * Takes this object makes it possible to re-insert as new record
   *
   * @return
   */
  public TripDestination prepareForReuse(String createdBy, String modifiedBy) {
    BeanState state = Ebean.getBeanState(this);
    state.resetForInsert();
    setTripdestid(DBConnectionMgr.getUniqueId());
    setModifiedby(modifiedBy);
    setCreatedby(createdBy);
    setLastupdatedtimestamp(System.currentTimeMillis());
    setCreatedtimestamp(lastupdatedtimestamp);
    return this;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getTripdestid() {
    return tripdestid;
  }

  public void setTripdestid(String tripdestid) {
    this.tripdestid = tripdestid;
  }

  public String getTripid() {
    return tripid;
  }

  public void setTripid(String tripid) {
    this.tripid = tripid;
  }

  public String getDestinationid() {
    return destinationid;
  }

  public void setDestinationid(String destinationid) {
    this.destinationid = destinationid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public boolean isMergebookings() {
    return mergebookings;
  }

  public void setMergebookings(boolean mergebookings) {
    this.mergebookings = mergebookings;
  }
}
