package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.utils.Utils;
import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by twong on 2014-05-20.
 */
@Entity @Table(name = "user_invite")
public class UserInvite
    extends Model {

  /**
   * State machine state transitions definition
   * i (rows) - are current states
   * j (columns) - are events
   * value is the next state
   */
  //@formatter:off
  @Transient
  public static final InviteState fsm[][] = {
     //E_AGE                        E_EXPIRE                     E_REINVITE                //E_CHECK                    //E_ACCEPT
      {InviteState.INVITE_OLD,      InviteState.EXPIRED,         InviteState.INVITE_FRESH, InviteState.CHECKED,         InviteState.ACCEPTED},        //INVITE_FRESH
      {InviteState.INVITE_OLD,      InviteState.EXPIRED,         InviteState.INVITE_FRESH, InviteState.CHECKED,         InviteState.ACCEPTED},        //INVITE_OLD
      {InviteState.EXPIRED,         InviteState.EXPIRED,         InviteState.INVITE_FRESH, InviteState.EXPIRED_CHECKED, InviteState.EXPIRED},         //EXPIRED
      {InviteState.CHECKED,         InviteState.EXPIRED,         InviteState.INVITE_FRESH, InviteState.CHECKED,         InviteState.ACCEPTED},        //CHECKED
      {InviteState.EXPIRED_CHECKED, InviteState.EXPIRED_CHECKED, InviteState.INVITE_FRESH, InviteState.EXPIRED_CHECKED, InviteState.EXPIRED_CHECKED}, //EXPIRED_CHECKED
      {InviteState.ACCEPTED,        InviteState.ACCEPTED,        InviteState.ACCEPTED,     InviteState.ACCEPTED,        InviteState.ACCEPTED},        //ACCEPTED
  };
  //@formatter:on
  @Transient
  public static final long expireOffset = 1000 * 60 * 60 * 24 * 7; //7 Days
  public static Model.Finder<String, UserInvite> find = new Finder(UserInvite.class);
  public String newuserid;
  public String firstname;
  public String lastname;
  @Id
  public String pk;
  @Constraints.Required
  public String email;
  @Enumerated(value = EnumType.STRING)
  public InviteState state;
  public Long senttimestamp;
  public Long createdtimestamp;
  public Long expirytimestamp;
  public String createdby;
  public Long modifiedtimestamp;
  public String modifiedby;
  @Version
  public int version;

  /**
   * States of the invitation state machine
   */
  public enum InviteState {
    /**
     * Invite is within short (24H) period of being sent
     */
    INVITE_FRESH,
    /**
     * Invite is older than short (24H) period of being sent
     */
    INVITE_OLD,
    /**
     * User did not register before expiration timestamp
     */
    EXPIRED,
    /**
     * User clicked on the link in the invite e-mail
     */
    CHECKED,
    /**
     * Checked link when it was already in expired state
     */
    EXPIRED_CHECKED,
    /**
     * Final state - user registered, no more transitions
     */
    ACCEPTED
  }

  /**
   * Events that trigger transitions in the
   */
  public enum InviteEvents {
    /**
     * Initial time after the invite has been issued has passed INIVITE_FRESH -> INVITE_OLD
     */
    E_AGE,
    /**
     * From any state, except ACCEPTED to EXPIRED
     */
    E_EXPIRE,
    /**
     * New invitation sent any state except ACCEPTED to INVITE_FRESH
     */
    E_REINVITE,
    /**
     * User clicked on the link but did not register INVITE_FRESH/INVITE_OLD -> CHECKED
     */
    E_CHECK,
    /**
     * User registered CHECKED -> ACCEPTED
     */
    E_ACCEPT
  }

  public static UserInvite buildUserInvite(String agentUserId, String firstName, String lastName, String email) {
    UserInvite userInvite = new UserInvite();
    userInvite.pk = Utils.getUniqueId();

    userInvite.createdby = agentUserId;
    userInvite.modifiedby = userInvite.createdby;
    userInvite.createdtimestamp = System.currentTimeMillis();
    userInvite.modifiedtimestamp = userInvite.createdtimestamp;
    userInvite.senttimestamp = userInvite.createdtimestamp;
    userInvite.expirytimestamp = userInvite.senttimestamp + expireOffset; //7 Days until invite expires
    userInvite.state = InviteState.INVITE_FRESH;
    userInvite.firstname = firstName;
    userInvite.lastname = lastName;
    userInvite.email = email;
    userInvite.newuserid = buildNewUserId(firstName, lastName, email);

    return userInvite;
  }

  /**
   * Very dumb generator of user Ids
   * presence of
   * @param firstName
   * @param lastName
   * @param email
   * @return
   */
  public static String buildNewUserId(String firstName, String lastName, String email) {
    String newUsername;
    String[] emailParts = email.toLowerCase().split("@");
    if (emailParts[0].length() > 6 && emailParts.length < 20) {
      newUsername = emailParts[0];
    }
    else {
      newUsername = firstName.toLowerCase().charAt(0) + lastName.toLowerCase();
    }

    String altUsername = newUsername;
    Random generator = new Random();
    while (UserProfile.findByPK(altUsername) != null) {
      altUsername = newUsername + generator.nextInt(999);;
    }

    return newUsername;
  }

  public static boolean isValidAuthCode(String pk) {
    UserInvite invite = find.byId(pk);
    if (invite != null &&
        (invite.state != InviteState.EXPIRED && invite.state != InviteState.EXPIRED_CHECKED && invite.state != InviteState.ACCEPTED) &&
        invite.getExpirytimestamp() >= System.currentTimeMillis()) {
      return true;
    }

    return false;
  }

  public Long getExpirytimestamp() {
    return expirytimestamp;
  }

  public void setExpirytimestamp(Long expiryTimestamp) {
    this.expirytimestamp = expiryTimestamp;
  }

  public static List<UserInvite> findActiveByKeyword(String keyword) {
    if (keyword != null) {
      keyword = keyword.replaceAll(" ", "%");
      keyword = "%" + keyword + "%";
    }
    else {
      keyword = "%";
    }

    String sql = "(firstname ilike ? or lastname ilike ? or email ilike ? or pk like ?)";
    List<String> keywords = new ArrayList<String>();
    keywords.add(keyword);
    keywords.add(keyword);
    keywords.add(keyword);
    keywords.add(keyword);

    final List<String> activeStates = Arrays.asList(InviteState.INVITE_FRESH.toString(),
                                                    InviteState.INVITE_OLD.toString(),
                                                    InviteState.CHECKED.toString());


    return find.where().in("state", activeStates).raw(sql, keywords.toArray()).orderBy("email asc").findList();
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public InviteState getState() {
    if (expirytimestamp < System.currentTimeMillis()) {
      state = fsm[state.ordinal()][InviteEvents.E_EXPIRE.ordinal()];
      save();
    }
    return state;
  }

  public void setState(InviteState state) {
    this.state = state;
  }

  /**
   * Method is used to transition between states of the state machine in a controlled fashion
   * Use this instead of @see setState()
   * @param event incoming event
   * @return newly reached state, which can be the same as before
   */
  public InviteState transitionState(InviteEvents event) {
    /* Before each FSM transition check if state needs to auto-transition */
    if (expirytimestamp < System.currentTimeMillis()) {
      state = fsm[state.ordinal()][InviteEvents.E_EXPIRE.ordinal()];
    }

    InviteState newState = fsm[state.ordinal()][event.ordinal()];
    modifiedtimestamp = System.currentTimeMillis();
    state = newState;
    return state;
  }

  public boolean isValid(){
    if (expirytimestamp < System.currentTimeMillis()) {
      state = fsm[state.ordinal()][InviteEvents.E_EXPIRE.ordinal()];
      save();
    }
    switch (state) {
      case EXPIRED:
      case EXPIRED_CHECKED:
      case ACCEPTED:
        return false;
      default:
        return true;
    }
  }

  /**
   * Updates state of the current record
   */
  private boolean updateState(InviteState newState) {
    String sql = "UPDATE user_invite SET state = :newState WHERE pk = :pk";
    SqlUpdate update = Ebean.createSqlUpdate(sql);
    update.setParameter("newState", newState.toString());
    update.setParameter("pk", pk);
    return Ebean.execute(update) > 0;
  }

  public static UserInvite findByEmail(String email) {
    return find.where().eq("email", email).findUnique();
  }

  public static int allCountByDate(long startDate, long endDate) {
    return find.where().gt("createdtimestamp",startDate).lt("createdtimestamp",endDate).findList().size();
  }

  public static int acceptedCountByDate(long startDate, long endDate) {
    return find.where().gt("createdtimestamp",startDate).lt("createdtimestamp",endDate).eq("state", "ACCEPTED").findList().size();
  }


  public Long getSenttimestamp() {
    return senttimestamp;
  }

  public void setSenttimestamp(Long senttimestamp) {
    this.senttimestamp = senttimestamp;
  }

  public String getPk() {
    return pk;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getNewuserid() {
    return newuserid;
  }

  public void setNewuserid(String newuserid) {
    this.newuserid = newuserid;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public Long getModifiedtimestamp() {
    return modifiedtimestamp;
  }

  public void setModifiedtimestamp(Long modifiedtimestamp) {
    this.modifiedtimestamp = modifiedtimestamp;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
