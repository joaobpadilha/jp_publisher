package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.Update;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2015-05-05.
 */
@Entity @Table(name = "tmplt_import")
public class TmpltImport
    extends Model {
  public static Finder<Long, TmpltImport> find = new Finder<>(TmpltImport.class);

  @Id @Column(name="import_id") @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tmplt_import_id")
  private Long importId;
  private String tmpltId;
  private byte[] hash;
  @ManyToOne @JoinColumn(name = "src_id")
  private FeedSrc feedSrc;
  @Enumerated(value = EnumType.STRING)
  private TmpltType.Type tmplt_type;
  private Long importTs;
  private Long syncTs;
  private String name;
  private Long startTs;
  private Long finishTs;
  private String raw;
  private String srcUrl;
  private String srcReferenceId;
  @Version
  private int version;

  public static TmpltImport buildTmpltImport(String feedSrcName, TmpltType.Type type, String name) {
    TmpltImport tmpltImport = new TmpltImport();
    tmpltImport.setImportTs(System.currentTimeMillis());
    tmpltImport.setTmplt_type(type);
    tmpltImport.setName(name);
    tmpltImport.setFeedSrc(FeedSrc.findByName(feedSrcName));
    return tmpltImport;
  }

  public static TmpltImport findFromSrcByReferenceId(int srcId, String srcReferenceId) {
    return find.where()
       .eq("src_reference_id", srcReferenceId)
       .eq("src_id", srcId)
       .findUnique();
  }

  public static List<TmpltImport> findBySrcReferenceId(String srcReferenceId, TmpltType.Type type) {
    return find.where().eq("src_reference_id", srcReferenceId).eq("tmplt_type", type).findList();
  }

  public static List<TmpltImport> findByTemplateId(String tmpltId, TmpltType.Type type) {
    return find.where().eq("tmplt_id", tmpltId).eq("tmplt_type", type).findList();
  }

  public static List<TmpltImport> findUnsynced(TmpltType.Type type) {

    String sql = "select import_id " +
                 " from tmplt_import " +
                 " where sync_ts is null and tmplt_type = :tmpltType order by import_id;";

    RawSql rawSql = RawSqlBuilder.parse(sql).columnMapping("import_id", "importId").create();

    com.avaje.ebean.Query<TmpltImport> query = Ebean.find(TmpltImport.class);
    query.setRawSql(rawSql).setParameter("tmpltType", type);

    return query.findList();
  }

  public static List<TmpltImport> findBySrc(int srcId) {
    return find.where().eq("src_id", srcId).findList();
  }

  public static List<TmpltImport> findByTmpltTypeAndGtStartTS(TmpltType.Type tmpltType, long startTs) {
    return find.where().eq("tmplt_type", tmpltType).gt("start_ts", startTs).findList();
  }


  public static List<TmpltImport> findNewBySrc(int srcId) {
    return find.where().eq("src_id", srcId).isNull("sync_ts").findList();
  }

  public static TmpltImport findBySrcReferenceId(String srcReferenceId) {
    return find.where().eq("src_reference_id", srcReferenceId).findUnique();
  }

  public FeedSrc getFeedSrc() {
    return feedSrc;
  }

  public static int deleteFromSrc(int srcId) {
    Update<PoiImport> upd = Ebean.createUpdate(PoiImport.class, "DELETE FROM tmplt_import WHERE src_id=:srcId");
    upd.set("srcId", srcId);
    return upd.execute();
  }

  public static int deleteFromSrcBeforeBarrierTime(int srcId, long barrier) {
    Update<PoiImport> upd = Ebean.createUpdate(PoiImport.class, "DELETE FROM tmplt_import WHERE src_id=:srcId AND " +
                                                                "import_ts<:barrier");
    upd.set("srcId", srcId);
    upd.set("barrier", barrier);
    return upd.execute();
  }


  public void setFeedSrc(FeedSrc feedSrc) {
    this.feedSrc = feedSrc;
  }

  public Long getImportId() {
    return importId;
  }

  public void setImportId(Long importId) {
    this.importId = importId;
  }

  public String getTmpltId() {
    return tmpltId;
  }

  public void setTmpltId(String tmpltId) {
    this.tmpltId = tmpltId;
  }

  public byte[] getHash() {
    return hash;
  }

  public void setHash(byte[] hash) {
    this.hash = hash;
  }

  public TmpltType.Type getTmplt_type() {
    return tmplt_type;
  }

  public void setTmplt_type(TmpltType.Type tmplt_type) {
    this.tmplt_type = tmplt_type;
  }

  public Long getImportTs() {
    return importTs;
  }

  public void setImportTs(Long importTs) {
    this.importTs = importTs;
  }

  public Long getSyncTs() {
    return syncTs;
  }

  public void setSyncTs(Long syncTs) {
    this.syncTs = syncTs;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getStartTs() {
    return startTs;
  }

  public void setStartTs(Long startTs) {
    this.startTs = startTs;
  }

  public Long getFinishTs() {
    return finishTs;
  }

  public void setFinishTs(Long finishTs) {
    this.finishTs = finishTs;
  }

  public String getRaw() {
    return raw;
  }

  public void setRaw(String raw) {
    this.raw = raw;
  }

  public String getSrcUrl() {
    return srcUrl;
  }

  public void setSrcUrl(String srcUrl) {
    this.srcUrl = srcUrl;
  }

  public String getSrcReferenceId() {
    return srcReferenceId;
  }

  public void setSrcReferenceId(String srcReferenceId) {
    this.srcReferenceId = srcReferenceId;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
