package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbJsonB;
import com.umapped.external.afar.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
import java.util.Map;

/**
 * Created by george on 2017-11-01.
 */
@Entity @Table(name = "afar_city_list")
public class AfarCityList extends Model implements JsonSupport {

  public static Model.Finder<Integer, AfarCityList> find = new Finder<>(AfarCityList.class);

  @Id
  private int afarId;
  private int afarCityId;
  private int afarDestId;
  private String name;
  private String slug;
  private String type;
  @DbJsonB
  private Map<String, Object> pois;
  @DbJsonB
  private Map<String, Object> data;

  public static List<AfarCityList> findByCityId(Integer afarCityId) {
    return find.where().eq("afar_city_id", afarCityId).orderBy("pois desc").findList();
  }

  public static List<AfarCityList> findFromIdList(List<Integer> listIds) {
    return find.where().in("afar_id", listIds).findList();
  }

  public int getAfarId() {
    return afarId;
  }

  public void setAfarId(int afarId) {
    this.afarId = afarId;
  }

  public int getAfarCityId() {
    return afarCityId;
  }

  public void setAfarCityId(int afarCityId) {
    this.afarCityId = afarCityId;
  }

  public int getAfarDestId() {
    return afarDestId;
  }

  public void setAfarDestId(int afarDestId) {
    this.afarDestId = afarDestId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Map<String, Object> getPois() {
    return pois;
  }

  public void setPois(Map<String, Object> pois) {
    this.pois = pois;
  }

  public Map<String, Object> getData() {
    return data;
  }

  public void setData(Map<String, Object> data) {
    this.data = data;
  }

  public Map<String, Object> marshalPois(IdList d) {
    return marshal(d);
  }

  public IdList unmarshalPois(Map<String, Object> data) {
    return unmarshal(data, IdList.class);
  }

  public Map<String, Object> marshalData(AfarList d) {
    return marshal(d);
  }

  public AfarList unmarshalData(Map<String, Object> data) {
    return unmarshal(data, AfarList.class);
  }
}
