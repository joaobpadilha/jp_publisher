package models.publisher;

import com.mapped.publisher.common.APPConstants;
import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-11-18
 * Time: 11:47 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity @Table(name = "trip_group")
public class TripGroup
    extends Model {
  public static Model.Finder<String, TripGroup> find = new Finder(TripGroup.class);
  public String comments;
  @Id
  public String groupid;
  @Constraints.Required
  public String tripid;
  public String name;
  public int allowselfregistration;
  public int publishstatus;
  public int status;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @Version
  public int version;

  public static TripGroup findByPK(String pk) {
    List<TripGroup> groups = find.where().eq("groupid", pk).eq("status", 0).findList();

    if (groups != null && groups.size() > 0) {
      return groups.get(0);
    }
    else {
      return null;
    }
  }

  public static List<TripGroup> findActiveByName(String tripId, String name) {
    return find.where().eq("tripid", tripId).eq("status", 0).ieq("name", name).findList();
  }

  public static List<TripGroup> findActiveByTripId(String tripId) {
    return find.where().eq("tripid", tripId).eq("status", APPConstants.STATUS_ACTIVE).orderBy("name asc").findList();
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getGroupid() {
    return groupid;
  }

  public void setGroupid(String groupid) {
    this.groupid = groupid;
  }

  public String getTripid() {
    return tripid;
  }

  public void setTripid(String tripid) {
    this.tripid = tripid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAllowselfregistration() {
    return allowselfregistration;
  }

  public void setAllowselfregistration(int allowSelfRegistration) {
    this.allowselfregistration = allowSelfRegistration;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public int getPublishstatus() {
    return publishstatus;
  }

  public void setPublishstatus(int publishStatus) {
    this.publishstatus = publishStatus;
  }
}
