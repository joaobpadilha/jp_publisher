package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Created by surge on 2015-11-24.
 */
@Entity @Table(name = "t42_guide_index")
public class T42GuideIndex
    extends Model {
  public static Model.Finder<String, T42GuideIndex> find = new Finder(T42GuideIndex.class);

  @Id String classId;
  String refClassId;
  String topClassId;
  String className;
  String refClassName;
  String topClassName;
  Integer sequenceNbr;
  Integer levelNbr;
  Boolean active;
  @Version int version;

  public static T42GuideIndex findByKey(String placeKey) {
    return find.byId(placeKey);
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public String getClassId() {
    return classId;
  }

  public String getIdName() {
    return classId.replaceAll("#","");
  }

  public void setClassId(String classId) {
    this.classId = classId;
  }

  public String getRefClassId() {
    return refClassId;
  }

  public void setRefClassId(String refClassId) {
    if (refClassId.length() == 0) {
      this.refClassId = null;
    }
    else {
      this.refClassId = refClassId;
    }
  }

  public String getTopClassId() {
    return topClassId;
  }

  public void setTopClassId(String topClassId) {
    if (topClassId.length() == 0) {
      this.topClassId = null;
    }
    else {
      this.topClassId = topClassId;
    }
  }

  public String getClassName() {
    return className;
  }

  public void setClassName(String className) {
    this.className = className;
  }

  public String getRefClassName() {
    return refClassName;
  }

  public void setRefClassName(String refClassName) {
    this.refClassName = refClassName;
  }

  public String getTopClassName() {
    return topClassName;
  }

  public void setTopClassName(String topClassName) {
    this.topClassName = topClassName;
  }

  public Integer getSequenceNbr() {
    return sequenceNbr;
  }

  public void setSequenceNbr(String sequenceNbr) {
    try {
      this.sequenceNbr = Integer.parseInt(sequenceNbr);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public void setSequenceNbr(Integer sequenceNbr) {
    this.sequenceNbr = sequenceNbr;
  }

  public Integer getLevelNbr() {
    return levelNbr;
  }

  public void setLevelNbr(Integer levelNbr) {
    this.levelNbr = levelNbr;
  }

  public void setLevelNbr(String levelNbr) {
    try {
      this.levelNbr = Integer.parseInt(levelNbr);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
