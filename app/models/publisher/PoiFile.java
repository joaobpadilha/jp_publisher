package models.publisher;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Model;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.persistence.IUmappedModel;
import com.mapped.publisher.persistence.IUmappedModelWithImage;
import com.mapped.publisher.persistence.RecordState;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 * Created by surge on 2014-10-16.
 */
@Entity
@Table(name = "poi_file")
public class PoiFile
    extends UmappedModelWithImage implements IUmappedModel<Long, PoiFile> {

  public static Model.Finder<Long, PoiFile> find = new Finder(PoiFile.class);

  @ManyToOne
  @JoinColumn(name = "file_pk", nullable = true, referencedColumnName = "pk")
  FileInfo file;

  public FileInfo getFile() {
    return file;
  }

  public PoiFile setFile(FileInfo file) {
    this.file = file;
    return this;
  }

  @Id
  @Column(name = "file_id")
  private long        fileId;
  private long        poiId;
  private int         consortiumId;
  private int         cmpyId;
  private int         srcId;
  private String      name;
  private String      nameSys;
  private String      description;
  @Constraints.Required
  @ManyToOne
  @JoinColumn(name = "extension")
  private LstFileType extension;
  private String      url;
  private int         priority;
  @Column(name = "state")
  @Enumerated(EnumType.STRING)
  private RecordState state;
  private long        createdtimestamp;
  private long        lastupdatedtimestamp;
  private String      createdby;
  private String      modifiedby;

  public static List<PoiFile> getDeleted(long poiId, int cmpyId, String nameSys) {
    return find.where()
               .eq("poi_id", poiId)
               .eq("cmpy_id", cmpyId)
               .eq("name_sys", nameSys)
               .eq("state", "DELETED")
               .findList();
  }

  public static PoiFile buildPoiFile(long poiId, int cmpyId, String name, String desc, String userId) {
    PoiFile pf = new PoiFile();
    pf.setFileId(DBConnectionMgr.getUniqueLongId());
    pf.setState(RecordState.ACTIVE);
    pf.setPoiId(poiId);
    pf.setCmpyId(cmpyId);
    pf.setName(name);
    pf.setDescription(desc);
    pf.setCreatedby(userId);
    pf.setModifiedby(userId);
    pf.setCreatedtimestamp(System.currentTimeMillis());
    pf.setLastupdatedtimestamp(pf.getCreatedtimestamp());
    return pf;
  }

  public long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public static List<PoiFile> getAllForPoiSrs(long poiId, int consortiumId, int cmpyId, int srcId) {
    return find.where()
               .eq("poi_id", poiId)
               .eq("consortium_id", consortiumId)
               .eq("cmpy_id", cmpyId)
               .eq("src_id", srcId)
               .findList();
  }

  public static List<PoiFile> getAllForPoi(Long poiId, List<Integer> consortiumId, List<Integer> cmpyId) {
    return find.where().eq("poi_id", poiId).in("consortium_id", consortiumId).in("cmpy_id", cmpyId).findList();
  }

  public static List<PoiFile> getAllTypedForPoi(Long poiId,
                                                List<Integer> consortiumId,
                                                List<Integer> cmpyId,
                                                List<String> extensions) {
    return find.where().eq("poi_id", poiId).in("consortium_id", consortiumId).in("cmpy_id", cmpyId)
               //.in("extension", extensions) //TODO: Serguei: Investigate why this fails
               .findList();
  }

  public static List<PoiFile> getAllForPoiSrcOrCmpy(Long poiId, Integer srcId, Integer cmpyId) {
    return find.where()
               .eq("poi_id", poiId)
               .or(Expr.eq("src_id", srcId), Expr.eq("cmpy_id", cmpyId))
               .orderBy("lastupdatedtimestamp asc")
               .findList();
  }

  /**
   * Company specific files
   *
   * @param poiId  - poi id
   * @param cmpyId - list of companies
   * @return
   */
  public static List<PoiFile> getCmpyForPoi(Long poiId, List<Integer> cmpyId) {
    return find.where().eq("poi_id", poiId).in("cmpy_id", cmpyId).orderBy("lastupdatedtimestamp asc").findList();
  }

  public static List<PoiFile> getCmpyForPoi(Long poiId, Integer cmpyId) {
    return find.where().eq("poi_id", poiId).eq("cmpy_id", cmpyId).eq("state","ACTIVE").orderBy("lastupdatedtimestamp asc").findList();
  }

  public static List<PoiFile> getCmpyTypedForPoi(Long poiId, List<Integer> cmpyId, List<String> extensions) {
    return find.where().eq("poi_id", poiId).in("cmpy_id", cmpyId)
              //.in("extension", extensions) //TODO: Serguei: Investigate why this fails
              .orderBy("lastupdatedtimestamp asc").findList();

  }

  public void updateModInfo(String userid) {
    setLastupdatedtimestamp(System.currentTimeMillis());
    setModifiedby(userid);
  }

  public RecordState getState() {
    return state;
  }

  public void setState(RecordState state) {
    this.state = state;
  }

  @Override
  public String toString() {
    return "FID:" + fileId + "(" + poiId + "," + consortiumId + "," + cmpyId + "," + srcId + ") =>" + name + ";";
  }

  public int getSrcId() {
    return srcId;
  }

  public void setSrcId(int srcId) {
    this.srcId = srcId;
  }

  public String getNameSys() {
    return nameSys;
  }

  public void setNameSys(String nameSys) {
    this.nameSys = nameSys;
  }

  public Long getFileId() {
    return fileId;
  }

  public void setFileId(long fileId) {
    this.fileId = fileId;
  }

  public long getPoiId() {
    return poiId;
  }

  public void setPoiId(long poiId) {
    this.poiId = poiId;
  }

  public int getConsortiumId() {
    return consortiumId;
  }

  public void setConsortiumId(int consortiumId) {
    this.consortiumId = consortiumId;
  }

  public int getCmpyId() {
    return cmpyId;
  }

  public void setCmpyId(int cmpyId) {
    this.cmpyId = cmpyId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public LstFileType getExtension() {
    return extension;
  }

  public void setExtension(LstFileType extension) {
    this.extension = extension;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public int getPriority() {
    return priority;
  }

  public void setPriority(int priority) {
    this.priority = priority;
  }

  public long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  @Override
  public String getImageUrl() {
    if(getFileImage() == null) {
      return getUrl();
    } else {
      return getFileImage().getUrl();
    }
  }

  @Override
  public FileImage getFileImage() {
    return (getFile() != null)?FileImage.find.byId(getFile().getPk()):null;
  }

  @Override
  public IUmappedModelWithImage setFileImage(FileImage image) {
    if(image == null) {
      setFile(null);
    } else {
      setFile(image.getFile());
      setNameSys(image.getFile().getFilepath());
      setExtension(image.getFile().getFiletype());
      setUrl(image.getUrl());
    }
    return this;
  }

  @Override
  public PoiFile getByPk(Long pk) {
    return find.byId(pk);
  }

  @Override
  public Long getPk() {
    return getFileId();
  }

  @Override
  public String getPkAsString() {
    return getPk().toString();
  }

  @Override
  public Long getCreatedByPk() {
    return Account.findByLegacyId(getCreatedby()).getUid();
  }

  @Override
  public Long getModifiedByPk() {
    return Account.findByLegacyId(getModifiedby()).getUid();
  }

  @Override
  public String getCreatedByLegacy() {
    return getCreatedby();
  }

  @Override
  public String getModifiedByLegacy() {
    return getModifiedby();
  }

  @Override
  public Long getCreatedEpoch() {
    return getCreatedtimestamp();
  }

  @Override
  public Timestamp getCreatedTs() {
    return Timestamp.from(Instant.ofEpochMilli(getCreatedtimestamp()));
  }

  @Override
  public Long getModifiedEpoch() {
    return getLastupdatedtimestamp();
  }

  @Override
  public Timestamp getModifiedTs() {
    return Timestamp.from(Instant.ofEpochMilli(getLastupdatedtimestamp()));
  }

}
