package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.EnumValue;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2015-03-26.
 */
@Entity @Table(name = "bil_schedule_type")
public class BillingSchedule
    extends Model {
  public static Finder<Type, BillingSchedule> find = new Finder<>(BillingSchedule.class);

  @Id
  @Enumerated(value = EnumType.STRING)
  @Column(name = "name", length = 15)
  private Type name;

  private Boolean assignableCmpy;
  private Boolean assignableUser;
  private String longName;
  private Boolean indirect;

  /**
   * Created by surge on 2015-03-26.
   */
  public static enum Type {
    /**
     * Each trip is billed separately
     */
    @EnumValue("TRIP")
    TRIP("Per Trip"),
    /**
     * Monthly billing
     */
    @EnumValue("MONTHLY")
    MONTHLY("Monthly"),
    /**
     * 1-Year subscription plan
     */
    @EnumValue("YEARLY")
    YEARLY("Annually"),
    /**
     * Use the same plan as company (applicable to users only)
     */
    @EnumValue("COMPANY")
    COMPANY("Charge Company"),
    /**
     * Charge associated agent
     */
    @EnumValue("AGENT")
    AGENT("Charge Agent"),
    /**
     * Full Featured Trial User with expiration date
     */
    @EnumValue("TRIAL")
    TRIAL("Trial"),
    /**
     * Freemium user, with limited functionality
     */
    @EnumValue("FREEMIUM")
    FREEMIUM("Freemium"),
    /**
     * Non-billable user with full features
     */
    @EnumValue("NONE")
    NONE("Non-billable");

    private String schedName;

    private Type(String name) {
      this.schedName = name;
    }

    public String getScheduleName() {
      return schedName;
    }
  }

  public static List<BillingSchedule> getSchedulesForCmpy() {
    return find.where().eq("assignable_cmpy", true).findList();
  }

  public static List<BillingSchedule> getSchedulesForUser() {
    return find.where().eq("assignable_user", true).findList();
  }

  public static List<BillingSchedule> getDirect() {
    return find.where().eq("indirect", false).findList();
  }

  public static List<BillingSchedule> getIndirectForUser() {
    return find.where().eq("indirect", true).eq("assignable_user", true).findList();
  }

  public String getLongName() {
    return longName;
  }

  public void setLongName(String longName) {
    this.longName = longName;
  }

  public Boolean getIndirect() {
    return indirect;
  }

  public void setIndirect(Boolean indirect) {
    this.indirect = indirect;
  }

  public Boolean getAssignableUser() {
    return assignableUser;
  }

  public void setAssignableUser(Boolean assignableUser) {
    this.assignableUser = assignableUser;
  }

  public String name() {
    return name.name();
  }

  public Type getName() {
    return name;
  }

  public void setName(Type name) {
    this.name = name;
  }

  public Boolean getAssignableCmpy() {
    return assignableCmpy;
  }

  public void setAssignableCmpy(Boolean assignableCmpy) {
    this.assignableCmpy = assignableCmpy;
  }

}
