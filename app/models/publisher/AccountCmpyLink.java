package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.SqlUpdate;
import com.avaje.ebean.annotation.EnumValue;
import com.mapped.publisher.persistence.RecordStatus;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 * Created by surge on 2016-02-24.
 */
@Entity
@Table(name = "account_cmpy_link")
public class AccountCmpyLink
    extends Model {

  public enum LinkType {
    /**
     * Suspended user (no real meaning - for backwards compatibility)
     */
    @EnumValue("SUS")
    SUSPENDED,
    //No real meaning for this value yet, may be in the future, placeholder for now
    @EnumValue("ADM")
    ADMIN,
    @EnumValue("MBR")
    MEMBER,
    @EnumValue("POW")
    POWER;

    public static  LinkType fromUserCmpyLinkType(UserCmpyLink.LinkType type) {
      switch (type){
        case ADMIN:
          return ADMIN;
        case MEMBER:
          return MEMBER;
        case POWER:
          return POWER;
        default:
        case SUSPENDED:
          return SUSPENDED;
      }
    }

    public String  getShort() {
      switch (this) {
        case SUSPENDED:
          return "SUS";
        case ADMIN:
          return "ADM";
        case MEMBER:
          return "MBR";
        case POWER:
          return "POW";
        default:
          return "UNK";
      }
    }
  }

  public enum CompanyRole {
    @EnumValue("OWN")
    OWNER,
    @EnumValue("AST")
    ASSISTANT,
    @EnumValue("AGT")
    AGENT,
    @EnumValue("BIL")
    BILLING,
    @EnumValue("UNK")
    UNKNOWN
  }

  public static Model.Finder<Long, AccountCmpyLink> find = new Finder(AccountCmpyLink.class);

  @Id
  Long uid;
  String cmpyid;
  @Enumerated
  LinkType     linkType;
  @Enumerated
  RecordStatus state;
  @Enumerated
  CompanyRole cmpyRole;
  String    note;
  Timestamp createdTs;
  Long      createdBy;
  Timestamp modifiedTs;
  Long      modifiedBy;
  @Version
  int version;

  public static AccountCmpyLink build(Long uid, String cmpyid, Long createdBy) {
    AccountCmpyLink acl = new AccountCmpyLink();
    acl.setUid(uid);
    acl.setCmpyid(cmpyid);
    acl.setCreatedTs(Timestamp.from(Instant.now()));
    acl.setCreatedBy(createdBy);
    acl.setModifiedTs(acl.createdTs);
    acl.setModifiedBy(createdBy);
    acl.setState(RecordStatus.ACTIVE);
    acl.setCmpyRole(CompanyRole.UNKNOWN);
    return acl;
  }

  public static AccountCmpyLink findActiveAccountAndCmpy(Long uid, String cmpyId) {
    return find.where().eq("uid",uid).eq("cmpyid", cmpyId).findUnique();
  }

  public static AccountCmpyLink findActiveAccount(Long uid) {
    return find.where().eq("uid",uid).eq("state",RecordStatus.ACTIVE).findUnique();
  }

  public static int activeCountByCmpyId(String cmpyId) {
    return find.where().eq("cmpyid", cmpyId).eq("state", RecordStatus.ACTIVE).findCount();
  }

  public static int mergeCmpy(String srcCmpyId, String targetCmpyId, long userId) {
    String s = "UPDATE account_cmpy_link set cmpyid = :targetCmpyId, modified_ts = :timestamp, " +
               "" + "modified_by = :userid  where cmpyid = :srcCmpyId and state = :recstate";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("timestamp", Timestamp.from(Instant.now()));
    update.setParameter("userid", userId);
    update.setParameter("srcCmpyId", srcCmpyId);
    update.setParameter("targetCmpyId", targetCmpyId);
    update.setParameter("recstate", RecordStatus.ACTIVE);

    return Ebean.execute(update);
  }

  public static List<AccountCmpyLink> getActiveCmpyAccountsByType(String cmpyId, LinkType type) {

    return find.where()
               .eq("cmpyid", cmpyId)
               .eq("link_type", type.getShort())
               .eq("state", RecordStatus.ACTIVE.getShort())
               .findList();
  }

  public CompanyRole getCmpyRole() {
    return cmpyRole;
  }

  public void setCmpyRole(CompanyRole cmpyRole) {
    this.cmpyRole = cmpyRole;
  }

  public Long getUid() {
    return uid;
  }

  public void setUid(Long uid) {
    this.uid = uid;
  }

  public String getCmpyid() {
    return cmpyid;
  }

  public void setCmpyid(String cmpyid) {
    this.cmpyid = cmpyid;
  }

  public LinkType getLinkType() {
    return linkType;
  }

  public void setLinkType(LinkType linkType) {
    this.linkType = linkType;
  }

  public RecordStatus getState() {
    return state;
  }

  public void setState(RecordStatus state) {
    this.state = state;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
  }

  public Timestamp getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Timestamp modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
