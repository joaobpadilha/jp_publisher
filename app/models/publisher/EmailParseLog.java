package models.publisher;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Model;
import com.mapped.persistence.util.DBConnectionMgr;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 * Created by surge on 2015-11-03.
 */
@Entity
@Table(name = "email_parse_log")
public class EmailParseLog
    extends Model {

  public enum EmailSource {
    DIRECT,
    FORWARD_ATTACH,
    FORWARD,
    INDIRECT,
    REPLY,
    UNSURE
  }

  public enum EmailState {
    NEW,
    AUTH,
    PERSISTED,
    SCHEDULED,
    PREPROCESSING,
    PARSING,
    PARSED_LOCAL,
    ERROR,
    USER_DISABLED,
    USER_NO_TRIP_PERMISSIONS,
    WM_SENT,
    WM_SUCCESS,
    WM_PARTIAL_SUCCESS,
    WM_UNSUPPORTED,
    WM_NO_DATA,
    WM_INSUFFICIENT_DATA,
    WM_NO_ITEMS,
    WM_EXTERNAL_ERROR,
    WM_INTERNAL_ERROR,
    WM_UNRECOGNIZED_FORMAT,
    WM_REPEATED_FAILURE,
    WM_ACCOUNT_SUSPENDED,
    WM_QUOTA_EXCEDEED,
    /**
     * OLD WORLDMATE STATUS TO SHOW PARSE STATUS
     */
    @Deprecated PARSED_WM

  }

  public static Model.Finder<Long, EmailParseLog> find = new Finder(EmailParseLog.class);
  @Id
  Long       pk;
  /**
   * Access Token
   */
  @ManyToOne
  @JoinColumn(name = "token", nullable = false, referencedColumnName = "token")
  EmailToken token;
  /**
   * Email address from which email was received (pure form)
   */
  String fromAddr;
  /**
   * Name featured in the from field
   */
  String fromName;
  /**
   * If we are able to extract original domain where booking came from
   */
  String origDomain;
  /**
   * Did we get email directly
   */
  @Enumerated(value = EnumType.STRING)
  EmailSource src;
  /**
   * Original email subject
   */
  String subject;
  /**
   * Raw file attachment
   */
  @OneToOne(optional = true)
  @JoinColumn(name = "file", nullable = true, referencedColumnName = "pk")
  FileInfo file;
  /**
   * Sendgrid Spam Assasin Spam Score
   */
  Integer spamScore;
  /**
   * Sendgrid Spam Assasin Report
   */
  String  spamReport;
  /**
   * Current state (will be enumeration from Java Enum type)
   */
  @Enumerated(value = EnumType.STRING)
  EmailState state;
  /**
   * Number of attachments found in the email
   */
  Integer attachCount;
  /**
   * Parser used for this e-mail to extract bookings
   */
  String  parser;
  /**
   * Number of bookings extracted from the e-mail
   */
  Integer bkCount;
  /**
   * (FUTURE USE Play 2.4+)
   * Details from each of the parser and errors
   */
  @Transient
  String meta;
  /**
   * When email was received
   */
  Timestamp createdTs;
  /**
   * Time taken to process email
   */
  Long      parseMs;
  /**
   * Account id who sent the email
   */
  @ManyToOne(optional = false)
  @JoinColumn(name = "aid", nullable = false, referencedColumnName = "uid")
  Account account;
  /**
   * Intended to allow linking to a specific company if company api was used
   */
  @Version
  int     version;

  public static List<EmailParseLog> findByUseridEmail(String userId) {
    return find.where().eq("userid", userId).orderBy("created_ts desc").findList();
  }

  /**
   * Total number of Emails;
   *
   * @return
   */
  public static int getRowCount(String userid) {
    return find.where().eq("userid", userid).findCount();
  }

  /**
   * Total number of filtered emails as used by DataTables
   */
  public static int getRowCountFiltered(String userid, String filter) {
    return find.where()
               .eq("userid", userid)
               .or(Expr.or(Expr.icontains("subject", filter), Expr.icontains("state", filter)),
                   Expr.or(Expr.icontains("parser", filter), Expr.icontains("subject", filter)))
               .findCount();
  }

  public static List<EmailParseLog> getPage(String userid, long accountid, int start, int count, String filter) {
    return find.where()
               .or(Expr.eq("userid", userid),Expr.eq("aid", accountid))
               .or(Expr.or(Expr.icontains("subject", filter), Expr.icontains("state", filter)),
                   Expr.or(Expr.icontains("parser", filter), Expr.icontains("subject", filter)))
               .orderBy("created_ts desc")
               .setFirstRow(start)
               .setMaxRows(count)
               .findList();
  }

  public static EmailParseLog buildRecord() {
    EmailParseLog epa = new EmailParseLog();
    epa.setPk(DBConnectionMgr.getUniqueLongId());
    epa.setCreatedTs(Timestamp.from(Instant.now()));
    epa.setState(EmailState.NEW);
    epa.setSrc(EmailSource.UNSURE);
    epa.setAttachCount(0);
    return epa;
  }

  /**
   * Method for backwards compatibility until we are ready to switch away to Account completely
   * TODO: 2016-10-26 - Code calling this method needs to be migrated to account
   * @return
   */
  @Deprecated
  public String getUserid() {
    return account.getLegacyId();
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public EmailParseLog setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
    return this;
  }

  public Account getAccount() {
    return account;
  }

  public EmailParseLog setAccount(Account account) {
    this.account = account;
    return this;
  }

  public String getSpamReport() {
    return spamReport;
  }

  public EmailParseLog setSpamReport(String spamReport) {
    this.spamReport = spamReport;
    return this;
  }

  public Integer getSpamScore() {
    return spamScore;
  }

  public void setSpamScore(String newScore) {
    if (newScore == null) {
      return;
    }
    try {
      setSpamScore(Integer.parseInt(newScore));
    }
    catch (Exception e) {
      //Do nothing on purpose
    }
  }

  public void setSpamScore(Integer spamScore) {
    this.spamScore = spamScore;
  }

  public EmailToken getToken() {
    return token;
  }

  public EmailParseLog setToken(EmailToken token) {
    this.token = token;
    return this;
  }

  public String getFromName() {
    return fromName;
  }

  public EmailParseLog setFromName(String fromName) {
    this.fromName = fromName;
    return this;
  }

  public FileInfo getFile() {
    return file;
  }

  public EmailParseLog setFile(FileInfo file) {
    this.file = file;
    return this;
  }

  public void addParseMs(long parseMs) {
    if (this.getParseMs() == null) {
      this.setParseMs(new Long(parseMs));
    }
    else {
      this.setParseMs(this.getParseMs() + parseMs);
    }
  }

  public Long getParseMs() {
    return parseMs;
  }

  public EmailParseLog setParseMs(Long parseMs) {
    this.parseMs = parseMs;
    return this;
  }

  public EmailState getState() {
    return state;
  }

  public EmailParseLog setState(EmailState state) {
    this.state = state;
    return this;
  }

  public Long getPk() {
    return pk;
  }

  public EmailParseLog setPk(Long pk) {
    this.pk = pk;
    return this;
  }

  public String getPkString() {
    return Long.toString(pk);
  }

  public EmailSource getSrc() {
    return src;
  }

  public EmailParseLog setSrc(EmailSource src) {
    this.src = src;
    return this;
  }

  public String getSubject() {
    return subject;
  }

  public EmailParseLog setSubject(String subject) {
    this.subject = subject;
    return this;
  }

  public Integer getAttachCount() {
    return attachCount;
  }

  public void setAttachCount(Integer attachCount) {
    this.attachCount = attachCount;
  }

  public void addParser(String parser) {
    if (getParser() == null) {
      setParser(parser);
      return;
    }

    setParser(getParser() + ", " + parser);
  }

  public String getParser() {
    return parser;
  }

  public EmailParseLog setParser(String parser) {
    this.parser = parser;
    return this;
  }

  public void addBkCount(int more) {
    if (getBkCount() == null) {
      setBkCount(new Integer(more));
      return;
    }
    setBkCount(getBkCount() + more);
  }

  public Integer getBkCount() {
    return bkCount;
  }

  public void setBkCount(Integer bkCount) {
    this.bkCount = bkCount;
  }

  public String getMeta() {
    return meta;
  }

  public EmailParseLog setMeta(String meta) {
    this.meta = meta;
    return this;
  }

  public String getFromAddr() {
    return fromAddr;
  }

  public EmailParseLog setFromAddr(String fromAddr) {
    this.fromAddr = fromAddr;
    return this;
  }

  public String getOrigDomain() {
    return origDomain;
  }

  public EmailParseLog setOrigDomain(String origDomain) {
    this.origDomain = origDomain;
    return this;
  }

  public int getVersion() {
    return version;
  }

  public EmailParseLog setVersion(int version) {
    this.version = version;
    return this;
  }
}
