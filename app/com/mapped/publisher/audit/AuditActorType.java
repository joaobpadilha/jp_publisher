package com.mapped.publisher.audit;

/**
 * Actors of the audit system.
 * All parties which can generate auditable events.
 *
 * Created by Serguei Moutovkin on 2014-05-21.
 */
public enum AuditActorType {
  /**
   * Anyone using website
   */
  WEB_USER,
  /**
   * Users that submit changes via E-mail
   */
  EMAIL_USER,
  /**
   * System generated changes like scheduled processes, etc
   */
  SYSTEM
}
