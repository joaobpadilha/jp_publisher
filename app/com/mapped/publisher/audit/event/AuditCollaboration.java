package com.mapped.publisher.audit.event;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.mapped.publisher.common.SecurityMgr;
import models.publisher.TripInvite;
import models.publisher.TripShare;

/**
 * Created by surge on 2014-06-10.
 */
@JsonTypeName("TRIP_COLLABORATION")
public class AuditCollaboration
    extends AuditEvent {

  public CollaboraionAction currentAction;
  public String inviteId;
  public String shareId;
  public String invitedFirstName;
  public String invitedLastName;
  public String invitedUserId;
  public String email;
  public SecurityMgr.AccessLevel accessLevel;
  public String tripId;
  public String modifiedBy;

  /**
   * Collaboration related actions
   */
  public enum CollaboraionAction {
    /**
     * New user being invited
     */
    INVITE,
    RESEND_INVITE,
    /**
     * Existing user being shared with
     */
    SHARE,
    RESEND_SHARE,
    /**
     * Revoke invite
     */
    REVOKE_INVITE,
    REVOKE_SHARE,
    /**
     * Change access level
     */
    CHANGE_INVITE_ACCESS,
    CHANGE_SHARE_ACCESS
  }

  public AuditCollaboration withAction(CollaboraionAction action) {
    this.currentAction = action;
    return this;
  }

  public AuditCollaboration withInviteId(String inviteId) {
    this.inviteId = inviteId;
    return this;
  }

  public AuditCollaboration withShareId(String shareId) {
    this.shareId = shareId;
    return this;
  }

  public AuditCollaboration withName(String firstName, String lastName) {
    this.invitedFirstName = firstName;
    this.invitedLastName = lastName;
    return this;
  }

  public AuditCollaboration withInvitedUserId(String invitedUserId) {
    this.invitedUserId = invitedUserId;
    return this;
  }

  public AuditCollaboration withAccessLevel(SecurityMgr.AccessLevel accessLevel) {
    this.accessLevel = accessLevel;
    return this;
  }

  public AuditCollaboration withTripId(String tripId) {
    this.tripId = tripId;
    return this;
  }

  public AuditCollaboration withModifiedBy(String userId) {
    this.modifiedBy = userId;
    return this;
  }

  public AuditCollaboration withTripInvite(TripInvite tripInvite) {
    inviteId = tripInvite.pk;
    invitedFirstName = tripInvite.getUserInvite().getFirstname();
    invitedLastName = tripInvite.getUserInvite().getLastname();
    modifiedBy = tripInvite.getModifiedby();
    invitedUserId = tripInvite.userInvite.getPk();
    accessLevel = tripInvite.accesslevel;
    tripId = tripInvite.getTripid();
    email = tripInvite.userInvite.email;
    return this;
  }

  public AuditCollaboration withTripShare(TripShare tripShare) {
    shareId = tripShare.getPk();
    invitedFirstName = tripShare.user.getFirstname();
    invitedLastName = tripShare.user.getLastname();
    modifiedBy = tripShare.getModifiedby();
    invitedUserId = tripShare.getUser().getUserid();
    accessLevel = tripShare.accesslevel;
    tripId = tripShare.tripid;
    email = tripShare.user.email;
    return this;
  }

}
