package com.mapped.publisher.audit.event;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Created by Serguei Moutovkin on 2014-05-21.
 */
@JsonTypeName("TRIP_DOC_GUIDE")
public class AuditTripDocGuide
    extends AuditEvent {

  //In existing system guides are called "destination"
  public String guideId;

  public String name;

  public AuditTripDocGuide withGuideId(String guideId) {
    this.guideId = guideId;
    return this;
  }

  public AuditTripDocGuide withName(String name) {
    this.name = name;
    return this;
  }
}
