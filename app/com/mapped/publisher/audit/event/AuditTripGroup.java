package com.mapped.publisher.audit.event;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Created by Serguei Moutovkin on 2014-05-21.
 */
@JsonTypeName("TRIP_GROUP")
public class AuditTripGroup
    extends AuditEvent {
  public String groupId;
  public String name;

  public AuditTripGroup withId(String groupId) {
    this.groupId = groupId;
    return this;
  }

  public AuditTripGroup withName(String name) {
    this.name = name;
    return this;
  }
}
