package com.mapped.publisher.audit.event;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Created by Serguei Moutovkin on 2014-05-19.
 */
@JsonTypeName("TRIP_TRAVELLER")
public class AuditTripTraveller
    extends AuditEvent {
  public String tripParticipantId;
  public String tripParticipantEmail;
  public String tripParticipantName;
  public AuditTripGroup group;

  public AuditTripTraveller() {
    group = new AuditTripGroup();
    group.name = "Default";
    group.groupId = "";
  }

  public AuditTripTraveller withEmail(String email)
  {
    this.tripParticipantEmail = email;
    return this;
  }

  public AuditTripTraveller withName(String name) {
    this.tripParticipantName = name;
    return this;
  }

  public AuditTripTraveller withId(String tripParticipantId){
    this.tripParticipantId = tripParticipantId;
    return this;
  }

  public AuditTripTraveller withGroupName(String groupName){
    this.group.name = groupName;
    return this;
  }

  public AuditTripTraveller withGroupId(String groupId) {
    this.group.groupId = groupId;
    return this;
  }
}
