package com.mapped.publisher.audit.event;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Created by Serguei Moutovkin on 2014-05-20.
 */
@JsonTypeName("TRIP_AGENT")
public class AuditTripAgent
    extends AuditEvent {
  public String tripAgentId;
  public String tripAgentName;
  public String tripAgentEmail;
  public AuditTripGroup group;

  public AuditTripAgent() {
    group = new AuditTripGroup();
    group.name = "Default";
    group.groupId = "";
  }

  public AuditTripAgent withId(String id) {
    this.tripAgentId = id;
    return this;
  }

  public AuditTripAgent withName(String name) {
    this.tripAgentName = name;
    return this;
  }

  public AuditTripAgent withEmail(String email) {
    this.tripAgentEmail = email;
    return this;
  }

  public AuditTripAgent withGroupName(String groupName){
    this.group.name = groupName;
    return this;
  }

  public AuditTripAgent withGroupId(String groupId) {
    this.group.groupId = groupId;
    return this;
  }
}
