package com.mapped.publisher.audit.event;

/**
 * Created by Serguei Moutovkin on 2014-05-13.
 */
public abstract class AuditEvent {
  /**
   * Miscellaneous text for audit logging.
   * The intention is to use this field for things like place of the audit, ex
   * @note Will not be displayed to the user
   */
  public String text = "";

  public AuditEvent withText(String text) {
    this.text = text;
    return this;
  }
}
