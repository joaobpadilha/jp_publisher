
package com.mapped.publisher.parse.ensemble;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}title"/>
 *         &lt;element ref="{}supplier"/>
 *         &lt;element ref="{}ship"/>
 *         &lt;element ref="{}embarkationdate" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}consumer_disclaimer"/>
 *         &lt;element ref="{}promo"/>
 *         &lt;element ref="{}agent_alert"/>
 *         &lt;element ref="{}booking_instruction"/>
 *         &lt;element ref="{}agent_disclaimer"/>
 *         &lt;element ref="{}special_interest" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}image"/>
 *         &lt;element ref="{}itinerary" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}form" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="subid" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="offercountry" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="numdays" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="hosted" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="agentexclusive" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="book_by_date" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="pub_start_date" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="pub_end_date" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="modified_on" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "title",
    "supplier",
    "ship",
    "embarkationdate",
    "consumerDisclaimer",
    "promo",
    "agentAlert",
    "bookingInstruction",
    "agentDisclaimer",
    "specialInterest",
    "image",
    "itinerary",
    "form"
})
@XmlRootElement(name = "offer")
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
public class Offer {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String title;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected Supplier supplier;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected Ship ship;
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected List<String> embarkationdate;
    @XmlElement(name = "consumer_disclaimer", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String consumerDisclaimer;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String promo;
    @XmlElement(name = "agent_alert", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String agentAlert;
    @XmlElement(name = "booking_instruction", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String bookingInstruction;
    @XmlElement(name = "agent_disclaimer", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String agentDisclaimer;
    @XmlElement(name = "special_interest")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected List<SpecialInterest> specialInterest;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected Image image;
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected List<Itinerary> itinerary;
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected List<Form> form;
    @XmlAttribute(name = "type")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String type;
    @XmlAttribute(name = "id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String id;
    @XmlAttribute(name = "subid")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String subid;
    @XmlAttribute(name = "status")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String status;
    @XmlAttribute(name = "offercountry")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String offercountry;
    @XmlAttribute(name = "numdays")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String numdays;
    @XmlAttribute(name = "hosted")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String hosted;
    @XmlAttribute(name = "agentexclusive")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String agentexclusive;
    @XmlAttribute(name = "book_by_date")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String bookByDate;
    @XmlAttribute(name = "pub_start_date")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String pubStartDate;
    @XmlAttribute(name = "pub_end_date")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String pubEndDate;
    @XmlAttribute(name = "modified_on")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String modifiedOn;

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the supplier property.
     * 
     * @return
     *     possible object is
     *     {@link Supplier }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public Supplier getSupplier() {
        return supplier;
    }

    /**
     * Sets the value of the supplier property.
     * 
     * @param value
     *     allowed object is
     *     {@link Supplier }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setSupplier(Supplier value) {
        this.supplier = value;
    }

    /**
     * Gets the value of the ship property.
     * 
     * @return
     *     possible object is
     *     {@link Ship }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public Ship getShip() {
        return ship;
    }

    /**
     * Sets the value of the ship property.
     * 
     * @param value
     *     allowed object is
     *     {@link Ship }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setShip(Ship value) {
        this.ship = value;
    }

    /**
     * Gets the value of the embarkationdate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the embarkationdate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmbarkationdate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public List<String> getEmbarkationdate() {
        if (embarkationdate == null) {
            embarkationdate = new ArrayList<String>();
        }
        return this.embarkationdate;
    }

    /**
     * Gets the value of the consumerDisclaimer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getConsumerDisclaimer() {
        return consumerDisclaimer;
    }

    /**
     * Sets the value of the consumerDisclaimer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setConsumerDisclaimer(String value) {
        this.consumerDisclaimer = value;
    }

    /**
     * Gets the value of the promo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getPromo() {
        return promo;
    }

    /**
     * Sets the value of the promo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setPromo(String value) {
        this.promo = value;
    }

    /**
     * Gets the value of the agentAlert property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getAgentAlert() {
        return agentAlert;
    }

    /**
     * Sets the value of the agentAlert property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setAgentAlert(String value) {
        this.agentAlert = value;
    }

    /**
     * Gets the value of the bookingInstruction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getBookingInstruction() {
        return bookingInstruction;
    }

    /**
     * Sets the value of the bookingInstruction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setBookingInstruction(String value) {
        this.bookingInstruction = value;
    }

    /**
     * Gets the value of the agentDisclaimer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getAgentDisclaimer() {
        return agentDisclaimer;
    }

    /**
     * Sets the value of the agentDisclaimer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setAgentDisclaimer(String value) {
        this.agentDisclaimer = value;
    }

    /**
     * Gets the value of the specialInterest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specialInterest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecialInterest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpecialInterest }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public List<SpecialInterest> getSpecialInterest() {
        if (specialInterest == null) {
            specialInterest = new ArrayList<SpecialInterest>();
        }
        return this.specialInterest;
    }

    /**
     * Gets the value of the image property.
     * 
     * @return
     *     possible object is
     *     {@link Image }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public Image getImage() {
        return image;
    }

    /**
     * Sets the value of the image property.
     * 
     * @param value
     *     allowed object is
     *     {@link Image }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setImage(Image value) {
        this.image = value;
    }

    /**
     * Gets the value of the itinerary property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itinerary property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItinerary().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Itinerary }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public List<Itinerary> getItinerary() {
        if (itinerary == null) {
            itinerary = new ArrayList<Itinerary>();
        }
        return this.itinerary;
    }

    /**
     * Gets the value of the form property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the form property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getForm().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Form }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public List<Form> getForm() {
        if (form == null) {
            form = new ArrayList<Form>();
        }
        return this.form;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the subid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getSubid() {
        return subid;
    }

    /**
     * Sets the value of the subid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setSubid(String value) {
        this.subid = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the offercountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getOffercountry() {
        return offercountry;
    }

    /**
     * Sets the value of the offercountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setOffercountry(String value) {
        this.offercountry = value;
    }

    /**
     * Gets the value of the numdays property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getNumdays() {
        return numdays;
    }

    /**
     * Sets the value of the numdays property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setNumdays(String value) {
        this.numdays = value;
    }

    /**
     * Gets the value of the hosted property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getHosted() {
        return hosted;
    }

    /**
     * Sets the value of the hosted property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setHosted(String value) {
        this.hosted = value;
    }

    /**
     * Gets the value of the agentexclusive property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getAgentexclusive() {
        return agentexclusive;
    }

    /**
     * Sets the value of the agentexclusive property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setAgentexclusive(String value) {
        this.agentexclusive = value;
    }

    /**
     * Gets the value of the bookByDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getBookByDate() {
        return bookByDate;
    }

    /**
     * Sets the value of the bookByDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setBookByDate(String value) {
        this.bookByDate = value;
    }

    /**
     * Gets the value of the pubStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getPubStartDate() {
        return pubStartDate;
    }

    /**
     * Sets the value of the pubStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setPubStartDate(String value) {
        this.pubStartDate = value;
    }

    /**
     * Gets the value of the pubEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getPubEndDate() {
        return pubEndDate;
    }

    /**
     * Sets the value of the pubEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setPubEndDate(String value) {
        this.pubEndDate = value;
    }

    /**
     * Gets the value of the modifiedOn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getModifiedOn() {
        return modifiedOn;
    }

    /**
     * Sets the value of the modifiedOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setModifiedOn(String value) {
        this.modifiedOn = value;
    }

}
