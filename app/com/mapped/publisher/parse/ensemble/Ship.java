
package com.mapped.publisher.parse.ensemble;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="AmadeusShipCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="SabreShipCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "value"
})
@XmlRootElement(name = "ship")
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
public class Ship {

    @XmlValue
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String value;
    @XmlAttribute(name = "id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String id;
    @XmlAttribute(name = "name")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String name;
    @XmlAttribute(name = "AmadeusShipCode")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String amadeusShipCode;
    @XmlAttribute(name = "SabreShipCode")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String sabreShipCode;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the amadeusShipCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getAmadeusShipCode() {
        return amadeusShipCode;
    }

    /**
     * Sets the value of the amadeusShipCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setAmadeusShipCode(String value) {
        this.amadeusShipCode = value;
    }

    /**
     * Gets the value of the sabreShipCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getSabreShipCode() {
        return sabreShipCode;
    }

    /**
     * Sets the value of the sabreShipCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setSabreShipCode(String value) {
        this.sabreShipCode = value;
    }

}
