package com.mapped.publisher.parse;

/**
 * Created by Serguei Moutovkin on 2014-05-08.
 */
public class ParseError
{
    /**
     * Error severity.
     * Can be used to highlight parts of the report
     */
    public ParseErrorSeverity severity;

    /**
     * Error type.
     * Supposed to give description to the user
     */
    public ParseErrorType errorType;

    /**
     * Plain text from extracted document.
     */
    public String text;

    public ParseError(ParseErrorSeverity severity,
                      ParseErrorType type,
                      String text)
    {
        this.severity = severity;
        this.errorType = type;
        this.text = text;
    }

    public enum ParseErrorSeverity
    {
        PARSE_ERROR_SEVERITY_CRITICAL(0, "Not recognized"),
        PARSE_ERROR_SEVERITY_NORMAL(3, "Missing required information"),
        PARSE_ERROR_SEVERITY_MINOR(7, "Ambiguous");

        private String description;
        private int level;

        ParseErrorSeverity(int level, String description) {
            this.level = level;
            this.description = description;
        }

        public String getDescription()
        {
            return this.description;
        }
        public int getLevel() { return this.level; }
     }

    public enum ParseErrorType
    {
        PARSE_ERROR_TYPE_MISSING_DATE("Date not recognized or missing"),
        PARSE_ERROR_TYPE_MISSING_TIME("Time not recognized or missing"),
        PARSE_ERROR_TYPE_MISSING_START_TIME("Start time not recognized or missing"),
        PARSE_ERROR_TYPE_MISSING_STOP_TIME("Stop time not recognized or missing"),
        PARSE_ERROR_TYPE_UNEXPECTED_DATA("Possible sub element not recognized"),
        PARSE_ERROR_TYPE_UNPROCESSED("Unprocessed text element");

        private String description;

        ParseErrorType(String description)
        {
            this.description = description;
        }

        public String getDescription()
        {
            return this.description;
        }

    }

}
