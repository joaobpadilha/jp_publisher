package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by twong on 19/06/17.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
public class Rate extends Reservation{

    @Override
    public DateTime getStartDateTime() {
        return null;
    }

    @Override
    public DateTime getFinishDateTime() {
        return null;
    }

}
