package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by surge on 2015-05-12.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Brand
    extends Thing {

  /**
   * [URL] or [ImageObject]	An associated logo.
   */
  public String logo;
}
