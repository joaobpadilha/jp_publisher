package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the schema.org FlightReservation specification from:
 * http://schema.org/FlightReservation
 * <p/>
 * Google version of the specification:
 * https://developers.google.com/schemas/reference/flight-reservation
 * <p/>
 * Only minimal subset of the specification is implemented here.
 * <p/>
 * Created by surge on 2015-05-05.
 */
@JsonTypeName("FlightReservation")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type", visible = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class FlightReservation
    extends Reservation {
  /**
   * (required) The number or id of the reservation.
   */
  public String reservationNumber;

  /**
   * (required) Current status of the reservation.
   */
  public String reservationStatus;

  /**
   * Web page where reservation can be viewed.
   */
  public String url;

  /**
   * [URL] (recommended for Google Now/Search Answers) Webpage where the passenger can check in.
   */
  public String checkinUrl;

  /**
   * Umapped Schema extension place to keep baggage URL
   * [URL]
   */
  public String baggageUrl;

  /**
   * [Flight]	(required) The flight the reservation is for.
   */
  public Flight reservationFor;

  /**
   * [Text]	The number or id of the ticket.
   */
  public String ticketNumber;
  /**
   * [URL]	Where the boarding pass can be downloaded.
   */
  public String ticketDownloadUrl;
  /**
   * [URL]	Where the boarding pass can be printed.
   */
  public String ticketPrintUrl;

  /**
   * [Text] or [URL]	If the barcode image is hosted on your site, the value of the field is URL of the image,
   * or a barcode or QR URI, such as "barcode128:AB34" (ISO-15417 barcodes), "qrCode:AB34" (QR codes),
   * "aztecCode:AB34" (Aztec codes), "barcodeEAN:1234" (EAN codes) and "barcodeUPCA:1234" (UPCA codes).
   */
  public String ticketToken;

  /**
   * [Text]	Additional information about the boarding pass.
   */
  public String additionalTicketText;

  /**
   * [Text]	The location of the reserved seat (e.g., 27B).
   */
  public String airplaneSeat;

  /**
   * 	[AirplaneSeatClass]	The cabin/class of the airplaneSeat.
   */
  public AirplaneSeatClass airplaneSeatClass;

  /**
   * [Text]	The airline-specific indicator of boarding order / preference.
   */
  public String boardingGroup;

  public String passengerPriorityStatus;

  public String passengerSequenceNumber;

  public String securityScreening;

  public String meal;


  public List<FlightPassenger> passengers;

  public void addPassenger (FlightPassenger p) {
    if (p != null) {
      if (this.passengers == null) {
        passengers = new ArrayList<>();
      }
    }
    passengers.add(p);
  }


  @Override
  public DateTime getStartDateTime() {
    return (reservationFor != null) ? reservationFor.departureTime : null;
  }

  @Override
  public DateTime getFinishDateTime() {
    return (reservationFor != null) ? reservationFor.arrivalTime : null;
  }
}
