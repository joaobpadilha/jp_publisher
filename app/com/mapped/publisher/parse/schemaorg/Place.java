package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by surge on 2015-05-12.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Place
    extends Thing {

  /**
   * Physical address of the item.
   */
  public PostalAddress address;

  public String aggregateRating;

  public Place containedIn;

  public Event event;

  public Event events;

  public String faxNumber;

  public GeoCoordinates geo;

  public String globalLocationNumber;

  public String hasMap;

  public String interactionCount;

  public String isicV4;

  public ImageObject logo;

  public String map;

  public String maps;

  public String openingHoursSpecific;

  /**
   * A photograph of this place. Supersedes photos.
   * photo	Photograph  or ImageObject
   */
  public List<ImageObject> photo;

  public String review;

  public String reviews;

  /**
   * [Text] The telephone number.
   */
  public String telephone;
  public String email;

  public String getContactInfo() {
    StringBuilder sb = new StringBuilder();
    if (this.telephone != null && this.telephone.trim().length() > 0) {
      sb.append("Telephone: ");
      sb.append(this.telephone);
      sb.append("\n");
    }
    if (this.faxNumber != null && this.faxNumber.trim().length() > 0) {
      sb.append("Fax: ");
      sb.append(this.faxNumber);
      sb.append("\n");
    }
    if (this.email != null && this.email.trim().length() > 0) {
      sb.append("Email: ");
      sb.append(this.telephone);
      sb.append("\n");
    }
    if (this.url != null && this.url.trim().length() > 0) {
      sb.append("Web: ");
      sb.append(this.url);
      sb.append("\n");
    }

    return sb.toString();
  }

  /*

aggregateRating	AggregateRating 	The overall rating, based on a collection of reviews or ratings, of the item.
containedIn	Place 	The basic containment relation between places.
event	Event 	Upcoming or past event associated with this place, organization, or action. Supersedes events.
faxNumber	Text 	The fax number.
geo	GeoShape  or
GeoCoordinates 	The geo coordinates of the place.
globalLocationNumber	Text 	The Global Location Number (GLN, sometimes also referred to as International Location
Number or ILN) of the respective organization, person, or place. The GLN is a 13-digit number used to identify
parties and physical locations.
hasMap	Map  or
URL 	A URL to a map of the place. Supersedes map, maps.
interactionCount	Text 	A count of a specific user interactions with this item—for example, 20 UserLikes, 5
UserComments, or 300 UserDownloads. The user interaction type should be one of the sub types of UserInteraction.
isicV4	Text 	The International Standard of Industrial Classification of All Economic Activities (ISIC), Revision 4
code for a particular organization, business person, or place.
logo	URL  or
ImageObject 	An associated logo.
openingHoursSpecification	OpeningHoursSpecification 	The opening hours of a certain place.

review	Review 	A review of the item. Supersedes reviews.


   */

}
