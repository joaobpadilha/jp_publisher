package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Type name: http://schema.org/Thing
 * <p>
 * Created by surge on 2015-05-06.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Thing
    implements Serializable {

  /**
   * An additional type for the item, typically used for adding more specific types from external vocabularies in
   * microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax,
   * it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may
   * have only weaker understanding of extra types, in particular those defined externally.
   */
  public String additionalType;

  /**
   * An alias for the item.
   */
  public String alternateName;

  /**
   * A short description of the item.
   */
  public String description;

  /**
   * An image of the item.
   * Umapped default is to assume an array of images
   * <p>
   * (This can be a URL or a fully described ImageObject.)
   */
  public List<ImageObject> image;

  /**
   * The string shown to the user on the UI element tied to the action.
   */
  public String name;

  /**
   * TODO: Not implemented fully
   * [Action] Indicates a potential Action, which describes an idealized action in which this thing would play an
   * 'object' role.
   */
  public String potentialAction;

  /**
   * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's
   * Wikipedia page, Freebase page, or official website.
   */
  public String sameAs;

  /**
   * Target url for the action. If no explicit handler field is provided the action handler is expanded into a
   * WebActionHandler with this url as the WebActionHandler url.
   */
  public String url;

  /**
   * Internal object Id to track if an object changes
   */
  public String umId;

  /**
   * Position if things need to be ordered
   * Umapped extension to Schema.org
   */
  public Integer position;

  public void addImage(ImageObject io) {
    if (image == null) {
      image = new ArrayList<>();
    }
    if (io != null) {
      image.add(io);
    }
  }
}
