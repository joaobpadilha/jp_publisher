package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * Created by surge on 2016-06-30.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BusTrip
    extends Thing {

  /**
   * The stop or station from which the bus arrives.
   * <p>
   * BusStation  or
   */
  public BusStop  arrivalBusStop;
  /**
   * The expected arrival time.
   */
  public DateTime arrivalTime;
  /**
   * The name of the bus (e.g. Bolt Express).
   * [Text]
   */
  public String   busName;

  /**
   * The unique identifier for the bus.
   * [Text]
   */
  public String   busNumber;
  /**
   * The stop or station from which the bus departs.
   * [BusStop]
   */
  public BusStop  departureBusStop;
  /**
   * The expected departure time.
   */
  public DateTime departureTime;

  /**
   * The service provider, service operator, or service performer; the goods producer. Another party (a seller) may
   * offer those services or goods on behalf of the provider. A provider may also serve as the seller. Supersedes
   * carrier.
   */
  public Organization provider;
}
