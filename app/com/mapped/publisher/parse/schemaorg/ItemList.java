package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by surge on 2016-07-04.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ItemList
    extends Thing {

  /**
   * For itemListElement values, you can use simple strings (e.g. "Peter", "Paul", "Mary"), existing entities, or
   * use ListItem. Text values are best if the elements in the list are plain strings. Existing entities are best for
   * a simple, unordered list of existing things in your data. ListItem is used with ordered lists when you want to
   * provide additional context about the element in that list or when the same item might be in different places in
   * different lists.
   * Note: The order of elements in your mark-up is not sufficient for indicating the order or elements.
   * Use ListItem with a 'position' property in such cases.
   */
  public List<ListItem> itemListElement;


  /**
   * Type of ordering (e.g. Ascending, Descending, Unordered).
   */
  public String itemListOrder;

  /**
   * The number of items in an ItemList. Note that some descriptions might not fully describe all items in a list (e.g.,
   * multi-page pagination); in such cases, the numberOfItems would be for the entire list.
   */
  public Integer numberOfItems;


  public ItemList() {
    itemListElement = new ArrayList<>();
  }

  public void addListItem(Thing t, Integer position, String group) {
    ListItem li = new ListItem();
    li.item = t;
    li.position = position;
    li.group = group;
    itemListElement.add(li);
  }

  public void addListItem(ListItem li){
    itemListElement.add(li);
  }


  public void sort(Comparator<ListItem> c) {
    Collections.sort(itemListElement, c);
    int pos = 0;

    for(ListItem li: itemListElement) {
      li.position = pos++;
    }
  }
}
