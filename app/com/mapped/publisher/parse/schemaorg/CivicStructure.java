package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * A public structure, such as a town hall or concert hall.
 * Created by surge on 2016-06-30.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CivicStructure
    extends Place {


  /**
   * The general opening hours for a business. Opening hours can be specified as a weekly time range, starting
   * with days, then times per day. Multiple days can be listed with commas ',' separating each day. Day or time ranges
   * are specified using a hyphen '-'.wzxhzdk:0- Days are specified using the following two-letter combinations:
   * wzxhzdk:1Mowzxhzdk:2, wzxhzdk:3Tuwzxhzdk:4, wzxhzdk:5Wewzxhzdk:6, wzxhzdk:7Thklzzwxh:0008,
   * klzzwxh:0009Frklzzwxh:0010, klzzwxh:0011Saklzzwxh:0012, klzzwxh:0013Suklzzwxh:0014.klzzwxh:0015-
   * Times are specified using 24:00 time. For example, 3pm is specified as klzzwxh:001615:00klzzwxh:0017.
   * klzzwxh:0018- Here is an example: klzzwxh:0019klzzwxh:0024span itemprop
   * =klzzwxh:0025openingHoursklzzwxh:0026 content=klzzwxh:0027Tu,Th
   * 16:00-20:00klzzwxh:0028klzzwxh:0029Tuesdays
   * and Thursdays 4-8pmklzzwxh:0030/spanklzzwxh:0031klzzwxh:0020. klzzwxh:0021- If a business is open 7 days a
   * week, then it can be specified as klzzwxh:0022klzzwxh:0032span
   * itemprop=klzzwxh:0033openingHoursklzzwxh:0034
   * content=klzzwxh:0035Mo-Suklzzwxh:0036klzzwxh:0037Monday through Sunday, all
   * dayklzzwxh:0038/spanklzzwxh:0039klzzwxh:0023.
   */
  public String openingHours;
}
