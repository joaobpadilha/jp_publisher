package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by ryan on 15-08-18.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TrainTrip
    extends Thing {
  public String arrivalPlatform;

  public Place arrivalStation;

  public DateTime arrivalTime;

  public String departPlatform;

  public Place departStation;

  public DateTime departTime;

  public Organization provider;

  public String trainName;

  public String trainNumber;
}
