package com.mapped.publisher.parse.schemaorg;

/**
 * Created by surge on 2015-05-12.
 */
public enum EventType {
  BusinessEvent,
  ChildrenEvent,
  ComedyEvent,
  DanceEvent,
  EducationEvent,
  Festival,
  FoodEvent,
  LiteraryEvent,
  MovieShowing,
  MusicEvent,
  SaleEvent,
  SocialEvent,
  SportsEvent,
  TheaterEvent,
  VisualArtsEvent;
}
