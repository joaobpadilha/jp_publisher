package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by twong on 19/06/17.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
public class RoomType extends LodgingReservation {
}
