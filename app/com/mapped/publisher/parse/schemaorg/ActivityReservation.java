package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by ryan on 15-08-17.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ActivityReservation
    extends Reservation {

  public DateTime startTime;

  public DateTime finishTime;

  public Place startLocation;

  public Place finishLocation;

  public Activity reservationFor;

  @Override
  public DateTime getStartDateTime() {
    return startTime;
  }

  @Override
  public DateTime getFinishDateTime() {
    return finishTime;
  }
}
