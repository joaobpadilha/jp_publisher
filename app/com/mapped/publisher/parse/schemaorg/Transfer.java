package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by ryan on 15-08-18.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Transfer
    extends Thing {

  public Place        pickupLocation;
  public DateTime     pickupTime;
  public Place        dropoffLocation;
  public DateTime     dropoffTime;
  public Organization provider;
  public Integer      partySize;
}
