package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.List;

/**
 * Created by ryan on 15-08-18.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CruiseShipReservation
    extends Reservation {

  public String category;

  public String cabinNumber;

  public String deckNumber;

  public String diningPlan;

  public String cabinType;

  public String bedding;

  public String amenities;

  public CruiseShip reservationFor;

  public List<CruiseStop> stops;

  @Override
  public DateTime getStartDateTime() {
    return (reservationFor != null) ? reservationFor.departTime : null;
  }

  @Override
  public DateTime getFinishDateTime() {
    return (reservationFor != null) ? reservationFor.arrivalTime : null;
  }
}
