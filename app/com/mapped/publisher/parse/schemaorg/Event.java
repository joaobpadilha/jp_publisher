package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
 *
 * https://schema.org/Event
 * https://developers.google.com/gmail/markup/reference/types/Event
 *
 * Created by surge on 2015-05-12.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Event
    extends Thing {

  /**
   * [Organization] or [Person] A person or organization attending the event. Supersedes attendees.
   */
  public Person attendee;
  public Person attendees;

  /**
   * [Organization] or [Person]
   * A performer at the event—for example, a presenter, musician, musical group or actor. Supersedes performers.
   */
  public Person performer;

  /**
   * [DateTime] The time admission will commence.
   */
  public DateTime doorTime;

  /**
   * 	[PostalAddress] or [Place] 	The location of the event, organization or action.
   */
  public Place location;

  /**
   * [DateTime]	The start date and time of the item (in ISO 8601 date format).
   */
  public DateTime startDate;

  /**
   * [DateTime] 	The end date and time of the item (in ISO 8601 date format).
   */
  public DateTime endDate;

  /**
   * [Organization] or [Person] An organizer of an Event.
   */
  public Organization organizer;

  /**
   * [Text] The typical expected age range, e.g. '7-9', '11-'. workPerformed	CreativeWork 	A work performed in some
   * event, for example a play performed in a TheaterEvent.
   */
  public String typicalAgeRange;

  /**
   * [Duration]	The duration of the item (movie, audio recording, event, etc.) in ISO 8601 date format.
   */
  public String duration;

  /**
   * [EventStatusType] An eventStatus of an event represents its status; particularly useful when an event is
   * cancelled or rescheduled.
   */
  public String eventStatus;

/*

offers	Offer 	An offer to provide this item—for example, an offer to sell a product, rent the DVD of a movie, or give away tickets to an event.
previousStartDate	Date 	Used in conjunction with eventStatus for rescheduled or cancelled events. This property contains the previously scheduled start date. For rescheduled events, the startDate property should be used for the newly scheduled start date. In the (rare) case of an event that has been postponed and rescheduled multiple times, this field may be repeated.
recordedIn	CreativeWork 	The CreativeWork that captured all or part of this Event.
Inverse property: recordedAt.
subEvent	Event 	An Event that is part of this event. For example, a conference event includes many presentations, each of which is a subEvent of the conference. Supersedes subEvents.
superEvent	Event 	An event that this event is a part of. For example, a collection of individual music performances might each have a music festival as their superEvent.

 */

}
