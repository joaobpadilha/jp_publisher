package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Wrapper for ordering things
 * Created by surge on 2016-07-04.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ListItem
    extends Thing {
  /**
   * An entity represented by an entry in a list or data feed (e.g. an 'artist' in a list of 'artists')’.
   */
  public Thing    item;
  /**
   * A link to the ListItem that follows the current one.
   */
  public ListItem nextItem;
  /**
   * The position of an item in a series or sequence of items.
   */
  public Integer  position;
  /**
   * A link to the ListItem that preceeds the current one.
   */
  public ListItem previousItem;
  /**
   * Group name - this should allow to sort items into groups
   *
   * @note Umapped Extension to Schema.org
   */
  public String   group;
}
