package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * The mailing address.
 * https://schema.org/PostalAddress
 * <p>
 * Created by surge on 2015-05-12.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PostalAddress
    extends ContactPoint {
  /**
   * [Country] The country. For example, USA. You can also provide the two-letter ISO 3166-1 alpha-2 country code.
   */
  public String addressCountry;

  /**
   * Text 	The locality. For example, Mountain View.
   */
  public String addressLocality;
  /**
   * Text 	The region. For example, CA.
   */
  public String addressRegion;
  /**
   * Text 	The post office box number for PO box addresses.
   */
  public String postOfficeBoxNumber;
  /**
   * Text 	The postal code. For example, 94043.
   */
  public String postalCode;
  /**
   * Text 	The street address. For example, 1600 Amphitheatre Pkwy.
   */
  public String streetAddress;

  /**
   * Umapped Specific: three-letter ISO 3166-1 alpha-3
   *
   * @return
   */
  public String a3Country;

  public boolean isEmpty() {
    return addressCountry == null || addressCountry.length() == 0 ||
           addressLocality == null || addressLocality.length() == 0;
  }


  public String toStringSingleLine() {
    if ((streetAddress != null &&
         streetAddress.trim().length() > 0) ||
        (addressLocality != null &&
         addressLocality.trim().length() > 0)) {
      StringBuilder sb = new StringBuilder();

      if (streetAddress != null && !streetAddress.trim().isEmpty()) {
        sb.append(streetAddress.trim());
      }
      if (addressLocality != null && addressLocality.trim().length() > 0) {
        if (sb.toString().trim().length() > 0) {
          sb.append(", ");
        }
        sb.append(addressLocality.trim());
      }

      if (addressRegion != null && addressRegion.trim().length() > 0) {
        if (sb.toString().trim().length() > 0) {
          sb.append(", ");
        }
        sb.append(addressRegion.trim());
        sb.append(' ');
      }
      if (postalCode != null && postalCode.trim().length() > 0) {
        if (sb.toString().trim().length() > 0) {
          sb.append(", ");
        }
        sb.append(postalCode.trim());
      }
      if (addressCountry != null && addressCountry.trim().length() > 0) {
        if (sb.toString().trim().length() > 0) {
          sb.append(", ");
        }
        sb.append(addressCountry.trim());
      }
      return sb.toString();
    }
    return "";

  }

  public String toString() {
    if ((streetAddress != null &&
         streetAddress.trim().length() > 0) ||
        (addressLocality != null &&
         addressLocality.trim().length() > 0)) {
      StringBuilder sb = new StringBuilder();

      if (streetAddress != null) {
        sb.append(streetAddress.trim());
      }
      if (addressLocality != null && addressLocality.trim().length() > 0) {
        sb.append(System.lineSeparator()).append(addressLocality.trim());
      }

      if (addressRegion != null && addressRegion.trim().length() > 0) {
        if (sb.toString().trim().length() > 0) {
          sb.append(", ");
        }
        sb.append(addressRegion.trim());
        sb.append(' ');
      }
      if (postalCode != null && postalCode.trim().length() > 0) {
        sb.append(postalCode.trim());
      }
      if (addressCountry != null && addressCountry.trim().length() > 0) {
        sb.append(System.lineSeparator()).append(addressCountry.trim());
      }
      return sb.toString();
    }
    return "";

  }

}
