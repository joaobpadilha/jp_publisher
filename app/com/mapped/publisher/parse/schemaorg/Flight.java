package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Type name: http://schema.org/Flight
 *
 * Created by surge on 2015-05-11.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Flight
    extends Thing {

  /**
   * The unique identifier for a flight, not including the airline IATA code. For example,
   * if describing United flight 110, the flightNumber is '110'. The IATA code can be set on the Airline.
   */
  public String flightNumber;

  /**
   * IATA coded airline
   */
  public Airline airline;


  public Airline operatedBy;

  /**
   * The airport where the flight originates.
   */
  public Airport departureAirport;

  /**
   * Identifier of the flight's departure gate.
   */
  public String departureGate;
  /**
   * Identifier of the flight's departure terminal.
   */
  public String departureTerminal;
  /**
   * The expected departure time.
   */
  public DateTime departureTime;

  /**
   * The airport where the flight terminates.
   */
  public Airport arrivalAirport;

  /**
   * Identifier of the flight's arrival gate.
   */
  public String arrivalGate;
  /**
   * Identifier of the flight's arrival terminal.
   */
  public String arrivalTerminal;
  /**
   * The expected arrival time.
   */
  public DateTime arrivalTime;

  /**
   * The time when a passenger can check into the flight online.
   */
  public String webCheckinTime;

  /**
   * Boarding time
   */
  public String boardingTime;

  /**
   * Description of the meals that will be provided or available for purchase.
   */
  public String mealService;

  public Organization carrier;

  public Organization provider;

  /**
   * The kind of aircraft (e.g., "Boeing 747").
   */
  public Thing aircraft;

  /**
   * The estimated time the flight will take.
   */
  public String estimatedFlightDuration;

  /**
   * The distance of the flight
   */
  public String flightDistance;
}
