package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by surge on 2016-07-11.
 */
public enum Privacy {
  /**
   * Private reservations will be visible only by the owner of the reservation (not currently used)
   */
  @JsonProperty("https://umapped.com/PrivacyPrivate")
  PRIVATE("https://umapped.com/PrivacyPrivate"),
  /**
   * All travellers
   */
  @JsonProperty("https://umapped.com/PrivacyTraveler")
  TRAVELER("https://umapped.com/PrivacyTraveler"),
  /**
   * Shared  will be visible by travelers and agents alike
   */
  @JsonProperty("https://umapped.com/PrivacyShared")
  SHARED("https://umapped.com/PrivacyShared"),
  /**
   * Visible to anyone (not currently used)
   */
  @JsonProperty("https://umapped.com/PrivacyPublic")
  PUBLIC("https://umapped.com/PrivacyPublic");

  String ns;

  Privacy(final String namespace) {
    ns = namespace;
  }

  public String getNamespace() {
    return ns;
  }
}
