package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * A reservation for bus travel.Note: This type is for information about actual reservations, e.g. in confirmation
 * emails or HTML pages with individual confirmations of reservations. For offers of tickets, use
 * http://schema.org/Offer.
 * <p>
 * https://schema.org/BusReservation
 * <p>
 * Created by surge on 2016-06-30.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BusReservation
    extends Reservation {


  public BusTrip reservationFor;

  @Override
  public DateTime getStartDateTime() {
    return (reservationFor != null) ? reservationFor.departureTime : null;
  }

  @Override
  public DateTime getFinishDateTime() {
    return (reservationFor != null) ? reservationFor.arrivalTime : null;
  }
}
