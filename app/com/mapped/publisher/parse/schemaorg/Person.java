package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.sql.Date;

/**
 * Created by surge on 2015-05-08.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Person
    extends Thing {
  /** [Text] 	An additional name for a Person, can be used for a middle name. */
  public String additionalName;

  public Organization affiliation;

  /**
   * [PostalAddress] Physical address of the item.
   */
  public PostalAddress address;

  public String alumniOf;

  public String award;

  public String awards;

  public Date birthDate;

  public Place birthPlace;

  public Organization brand;

  public Person children;

  public Person colleague;

  public Person colleagues;

  public ContactPoint contactPoint;

  public ContactPoint contactPoints;

  public Date deathDate;

  public Place deathPlace;

  public String duns;

  /** [Text] 	Email address. */
  public String email;

  /**
   * [Text] 	Family name. In the U.S., the last name of an Person. This can be used along with givenName instead of the name property.
   */
  public String familyName;

  public String faxNumber;

  public Person follows;

  public String gender;

  /**
   * [Text] 	Given name. In the U.S., the first name of a Person. This can be used along with familyName instead of the name property.
   */
  public String givenName;

  public String globalLocationNumber;

  public Place hasPOS;

  public String height;

  public ContactPoint homeLocation;

  public String honorificPrefix;

  public String honorificSuffix;

  public String interactionCount;

  public String isicV4;

  public String jobTitle;

  public Person knows;

  public String makesOffer;

  public Organization memberOf;

  public String naics;

  public String nationality;

  public String netWorth;

  public Product owns;

  public Person parent;

  public Person parents;

  public Event performerIn;

  public Person relatedTo;

  public String seeks;

  public Person sibling;

  public Person siblings;

  public Person spouse;

  public String taxId;

  public String telephone;

  public String vatId;

  public String weight;

  public Place workLocation;

  public Organization worksFor;

/* More properties to be implemented


affiliation	Organization 	An organization that this person is affiliated with. For example, a school/university, a club, or a team.
alumniOf	EducationalOrganization 	An educational organizations that the person is an alumni of.
Inverse property: alumni.
award	Text 	An award won by this person or for this creative work. Supersedes awards.
birthDate	Date 	Date of birth.
birthPlace	Place 	The place where the person was born.
brand	Brand  or
Organization 	The brand(s) associated with a product or service, or the brand(s) maintained by an organization or business person.
children	Person 	A child of the person.
colleague	Person 	A colleague of the person. Supersedes colleagues.
contactPoint	ContactPoint 	A contact point for a person or organization. Supersedes contactPoints.
deathDate	Date 	Date of death.
deathPlace	Place 	The place where the person died.
duns	Text 	The Dun & Bradstreet DUNS number for identifying an organization or business person.

faxNumber	Text 	The fax number.
follows	Person 	The most generic uni-directional social relation.
gender	Text 	Gender of the person.
globalLocationNumber	Text 	The Global Location Number (GLN, sometimes also referred to as International Location Number or ILN) of the respective organization, person, or place. The GLN is a 13-digit number used to identify parties and physical locations.
hasPOS	Place 	Points-of-Sales operated by the organization or person.
height	Distance  or
QuantitativeValue 	The height of the item.
homeLocation	ContactPoint  or
Place 	A contact location for a person's residence.
honorificPrefix	Text 	An honorific prefix preceding a Person's name such as Dr/Mrs/Mr.
honorificSuffix	Text 	An honorific suffix preceding a Person's name such as M.D. /PhD/MSCSW.
interactionCount	Text 	A count of a specific user interactions with this item—for example, 20 UserLikes, 5 UserComments, or 300 UserDownloads. The user interaction type should be one of the sub types of UserInteraction.
isicV4	Text 	The International Standard of Industrial Classification of All Economic Activities (ISIC), Revision 4 code for a particular organization, business person, or place.
jobTitle	Text 	The job title of the person (for example, Financial Manager).
knows	Person 	The most generic bi-directional social/work relation.
makesOffer	Offer 	A pointer to products or services offered by the organization or person.
memberOf	Organization  or
ProgramMembership 	An Organization (or ProgramMembership) to which this Person or Organization belongs.
Inverse property: member.
naics	Text 	The North American Industry Classification System (NAICS) code for a particular organization or business person.
nationality	Country 	Nationality of the person.
netWorth	PriceSpecification 	The total financial value of the organization or person as calculated by subtracting assets from liabilities.
owns	Product  or
OwnershipInfo 	Products owned by the organization or person.
parent	Person 	A parent of this person. Supersedes parents.
performerIn	Event 	Event that this person is a performer or participant in.
relatedTo	Person 	The most generic familial relation.
seeks	Demand 	A pointer to products or services sought by the organization or person (demand).
sibling	Person 	A sibling of the person. Supersedes siblings.
spouse	Person 	The person's spouse.
taxID	Text 	The Tax / Fiscal ID of the organization or person, e.g. the TIN in the US or the CIF/NIF in Spain.
telephone	Text 	The telephone number.
vatID	Text 	The Value-added Tax ID of the organization or person.
weight	QuantitativeValue 	The weight of the product or person.
workLocation	ContactPoint  or
Place 	A contact location for a person's place of work.
worksFor	Organization 	Organizations that the person works for.
 */

}
