package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * A contact point—for example, a Customer Complaints department.
 *
 * https://schema.org/ContactPoint
 *
 * Created by surge on 2015-05-12.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ContactPoint
    extends Thing {
  /**
   * [AdministrativeArea] The location served by this contact point (e.g., a phone number intended for Europeans vs.
   * North Americans or only within the United States).
   */
  public String areaServed;
  /**
   * [Language] 	A language someone may use with the item.
   */
  public String availableLanguage;
  /**
   * [ContactPointOption] 	An option available on this contact point (e.g. a toll-free number or support for
   * hearing-impaired callers).
   */
  public String contactOption;
  /**
   * [Text] 	A person or organization can have different contact points, for different purposes. For example,
   * a sales contact point, a PR contact point and so on. This property is used to specify the kind of contact point.
   */
  public String contactType;
  /**
   * [Text] 	Email address.
   */
  public String email;

  /**
   * [Text] The fax number.
   */
  public String faxNumber;

  /**
   * OpeningHoursSpecification 	The hours during which this contact point is available.
   */
  public String hoursAvailable;

  /**
   * [Product]  or [Text] 	The product or service this support contact point is related to (such as product support
   * for a particular product line). This can be a specific product or product line (e.g. "iPhone") or a general
   * category of products or services (e.g. "smartphones").
   */
  public String productSupported;

  /**
   * [Text]	The telephone number.
   */
  public String telephone;

}
