package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by surge on 2016-07-11.
 */
public enum Editable {
  @JsonProperty("https://umapped.com/Editable")
  EDITABLE("https://umapped.com/Editable"),
  @JsonProperty("https://umapped.com/ReadOnly")
  LOCKED("https://umapped.com/ReadOnly");
  String ns;

  Editable(String namespace) {
    ns = namespace;
  }

  public String getNamespace() {
    return ns;
  }
}
