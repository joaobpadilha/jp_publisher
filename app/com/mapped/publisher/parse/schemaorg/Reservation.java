package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2015-05-11.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
abstract public class Reservation
    extends Thing {

  /**
   * This field is not part of canonical Reservation class, however in order to accomodate legacy data, sub-reservations
   * are moved into the main Reservations class
   */
  public List<Reservation> subReservation;

  public List<Reservation> rates;

  public Boolean autoExpand;

  public String additionalTicketText;

  public DateTime bookingDate;
  /**
   * [DateTime]  The date and time the reservation was booked.
   * A combination of date and time of day in the form [-]CCYY-MM-DDThh:mm:ss[Z|(+|-)hh:mm]
   * (see Chapter 5.4 of ISO 8601).
   */
  public DateTime bookingTime;

  /**
   * [Person]  or [Organization] An entity that arranges for an exchange between a buyer and a seller. In most cases
   * a broker never acquires or releases ownership of a product or service involved in an exchange. If it is not
   * clear whether an entity is a broker, seller, or buyer, the latter two terms are preferred. Supersedes bookingAgent.
   */
  public Person broker;
  public Organization bookingAgent;

  public String checkinUrl;

  /**
   * Minimal information who created this booking
   */
  public Organization creator;

  public DateTime dateModified;

  /**
   * Google Now Cards Specific
   */
  public ProgramMembership loyaltyProgram;

  /**
   * [DateTime] 	The date and time the reservation was modified.
   */
  public DateTime modifiedTime;

  public String numSeats;

  public String price;

  /**
   * ProgramMembership	Any membership in a frequent flyer, hotel loyalty program,
   * etc. being applied to the reservation.
   */
  public ProgramMembership programMembership;

  /**
   * [ProgramMembership] Any membership in a frequent flyer, hotel loyalty program,
   * etc. being applied to the reservation.
   * This is the most commonly used of programMembership and loyaltyProgram
   */
  public ProgramMembership programMembershipUsed;

  /**
   * The service provider, service operator, or service performer; the goods producer.
   * Another party (a seller) may offer those services or goods on behalf of the provider.
   * A provider may also serve as the seller.
   * Supersedes carrier
   */
  public Organization provider;

  /**
   * [Person] or [Organization]	(required) The passenger.
   */
  public Person underName;

  /**
   * The thing -- flight, event, restaurant,etc. being reserved.
   */
  public Thing reservationFor;

  /**
   * A unique identifier for the reservation.
   */
  public String reservationId;

  /**
   * Text	(required) The number or id of the reservation.
   */
  public String reservationNumber;

  /**
   * [ReservationStatusType] The current status of the reservation.
   */
  public String reservationStatus;

  public String reservedTicket;

  public String seat;

  public String seatingType;

  public String seatRow;

  public String seatSection;

  public String ticketDownloadUrl;

  public String ticketNumber;

  public String ticketPrintUrl;

  public String ticketToken;

  public String totalPrice;

  /**
   * [Text] The currency (in 3-letter ISO 4217 format) of the price or a price component,
   * when attached to PriceSpecification and its subtypes.
   */
  public String priceCurrency;

  /**
   * [URL] Web page where reservation can be confirmed.
   */
  public String confirmReservationUrl;
  /**
   * [URL] Web page where reservation can be cancelled.
   */
  public String cancelReservationUrl;

  /**
   * [URL]	(recommended for Google Now/Search Answers) Web page where reservation can be modified.
   */
  public String modifyReservationUrl;

  /**
   * Items that can be downloaded for this reservation
   * Umapped Extension to Schema.org specification
   * Could have been DataDownload, MediaObject to simplify
   */
  public List<MediaObject> downloadable;

  /**
   * Indicates selected level of visibility for this booking
   * Umapped Extension to Schema.org specification
   */
  public Privacy privacy;

  /**
   * Indicates whether the item is editable
   * Umapped Extension to Schema.org specification
   */
  public Editable editable;

  /**
   * Rates/Price related fields from UmReservation object
   *
   */
  public String rateName;
  public String ratePrice;
  public String rateDescription;
  public String rateType;
  public String taxes;
  public String fees;
  public String total;
  public String currency;
  public String important;
  public String subtotal;

  public String serviceType;
  public String cancellationPolicy;
  public String remarksAndSpecialRequests;
  public String tag;


  public void addDownloadable(MediaObject mo) {
    if(downloadable == null) {
      downloadable = new ArrayList<>(1);
    }
    downloadable.add(mo);
  }

  /**
   * Helps to add sub reservations
   * @param r
   */
  public void addSubReservation(Reservation r) {
    if(subReservation == null) {
      subReservation = new ArrayList<>(1);
    }
    subReservation.add(r);
  }

  /**
   * Helps to add rates
   * @param r
   */
  public void addRates(Reservation r) {
    if(rates == null) {
      rates = new ArrayList<>(1);
    }
    rates.add(r);
  }


  /*
reservedTicket	Ticket 	A ticket associated with the reservation.
totalPrice	Number  or Text  or PriceSpecification 	The total price for the reservation or ticket, including
applicable taxes, shipping, etc.
 */


  /**
   * Implementations should return start date and time
   * @return
   */
  @JsonIgnore
  abstract public DateTime getStartDateTime();

  /**
   * Implementations should return end date and time
   * @return
   */
  @JsonIgnore
  abstract public DateTime getFinishDateTime();


  @JsonIgnore
  public boolean isEndBeforeStart() {
    DateTime sDt = getStartDateTime();
    DateTime eDt = getFinishDateTime();
    if(sDt == null || eDt == null) {
      return false;
    }

    int cr = sDt.getTime().compareTo(eDt.getTime());
    return cr >= 0;
  }
}
