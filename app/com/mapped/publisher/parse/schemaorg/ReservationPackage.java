package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2015-05-22.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ReservationPackage
    extends Reservation {

  @JsonProperty("@context")
  public final String context = "http://schema.org";

  public List<Reservation> reservation;

  /**
   * Trip (or sub-trip) start date
   * Umapped Extensions
   */
  public DateTime startDate;
  /**
   * Trip (or sub-trip) end date
   * Umapped Extension
   */
  public DateTime endDate;

  /**
   * Extension to the Schema.org specification.
   * Primarily for output!
   */
  public ItemList sorted;

  /**
   * Extension to the Schema.org specification.
   * To support multiple travelers
   */
  public List<Person> additionalTravelers;

  /**
   * Extension to the Schema.org specification.
   * To support cc of the additional emails
   */
  public List<Person> ccPersons;

  /**
   * Extension to the Schema.org specification.
   * To support email note
   */
  public String emailNote;

  public ReservationPackage() {
    reservation = new ArrayList<>();
    subReservation = new ArrayList<>();
  }

  @Override
  public DateTime getStartDateTime() {
    return startDate;
  }

  @Override
  public DateTime getFinishDateTime() {
    return endDate;
  }
}
