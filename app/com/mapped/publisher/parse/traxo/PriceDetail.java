package com.mapped.publisher.parse.traxo;

/**
 * Created by twong on 10/06/17.
 */
public class PriceDetail {
    public String type;
    public String name;
    public String value;
    public String units;

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        sb.append(" ");
        sb.append(value);
        sb.append(" ");
        sb.append(units);
        return sb.toString();
    }
}
