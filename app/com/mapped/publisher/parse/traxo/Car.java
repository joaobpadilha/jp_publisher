package com.mapped.publisher.parse.traxo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by twong on 10/06/17.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Car extends Segment {
    public String car_company;
    public String car_description;
    public String car_type;
    public String dropoff_address1;
    public String dropoff_address2;
    public String dropoff_admin_code;
    public String dropoff_city_name;
    public String dropoff_country;
    public String dropoff_datetime;
    public String dropoff_lat;
    public String dropoff_lon;
    public String dropoff_postal_code;
    public String dropoff_time_zone_id;
    public String hours_of_operation;
    public String normalized_car_company;
    public String pickup_address1;
    public String pickup_address2;
    public String pickup_admin_code;
    public String pickup_city_name;
    public String pickup_country;
    public String pickup_datetime;
    public String pickup_lat;
    public String pickup_lon;
    public String pickup_postal_code;
    public String pickup_time_zone_id;


}
