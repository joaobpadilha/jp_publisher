package com.mapped.publisher.parse.traxo;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by twong on 10/06/17.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DataObject {
    public String created;
    public String from_address;
    public String id;
    public String mailbox_id;
    public String modified;
    public List<Segment> segments;
    public String source;
    public String status;
    public String subject;
    public String to_address;
    public String user_address;
}
