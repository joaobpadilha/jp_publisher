package com.mapped.publisher.parse.extractor.booking;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.util.CommonPDFUtils;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.mapped.publisher.utils.Log;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFTextStripperByArea;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by george on 2018-05-29.
 */
public class OpenSkiesExtractor extends BookingExtractor {

  private static PDDocument pdfDocument;
  private static Object passengerPagePdf;
  private String file;
  private String file2;
  private TripVO trip;
  private static Integer passengerPage;
  private static Integer numOfPassengers;
  private static Integer numOfFlightLegs;
  private static Boolean completeLegs;
  private static Map<Integer, List<String>> passengerSeatMap;


  private Map<String, AirportVO> airports;
  private List<String> mealPlans;
  private List<String> addedMealsForCities;
  private List<String> airportNames;
  private List<FlightVO> flights;
  private List<TransportVO> transports;
  private List<PassengerVO> passengers;
  private String client;
  private String flightDate;
  private String aircraft;
  private String routing;

  private static String[] dateTimeFormat = {"dd MMM yyyy hh:mm a", "EEE dd MMM yy HH:mm"};
  private static DateUtils du = new DateUtils();
  private static Calendar now = Calendar.getInstance();

  private enum BOOKING_TYPE {
    HOTEL,
    FLIGHT,
    TOUR,
    TRANSFER,
    CRUISE,
    TRAIN,
    INSURANCE,
    CAR,
    UNDEFINED
  }

  private static Pattern timePattern = Pattern.compile("[0-9]{1,2}:[0-9]{2}\\s[APap][mM]");
  private static Pattern phonePattern = Pattern.compile("[0-9]{3}-[0-9]{3}-[0-9]{4}");
  private static Pattern addressPattern = Pattern.compile("[0-9]+\\s[A-Za-z]+");
  private static Pattern flightDurationPattern = Pattern.compile("[0-9]{2}:[0-9]{2}");

  private final static Pattern datePattern = Pattern.compile("[0-9]{2}\\s[A-Za-z]{3}\\s[0-9]{4}");



  public void initImpl(Map<String, String> params) {

  }

  public TripVO extractDataImpl(InputStream input)
      throws Exception {
    trip = new TripVO();
    pdfDocument = null;
    passengerPagePdf = null;
    passengerPage = 0;
    passengerSeatMap = new LinkedHashMap<>();
    airports = new LinkedHashMap<>();
    mealPlans = new ArrayList<>();
    addedMealsForCities = new ArrayList<>();
    airportNames = new ArrayList<>();
    flights = new ArrayList<>();
    transports = new ArrayList<>();
    passengers = new ArrayList<>();
    completeLegs = true;
    
    file = getPDFContentAsString(input);
    processFile();
    processAllFlightVO();
    processAllTransportVO();
    if (!completeLegs && passengerPagePdf != null) {
      file2 = getPassengersFromPDFAsString(passengerPagePdf);
    }
    try {
      if (pdfDocument != null) {
        pdfDocument.close();
      }
    } catch (IOException e) {
      Log.err("Open Skies - Error while closing PDF: " + e);
    }
    addPassengersToFlightVO();
    trip.setImportSrc(BookingSrc.ImportSrc.OPEN_SKIES_PDF);
    trip.setImportTs(System.currentTimeMillis());

    return trip;
  }

  private void processFile() {
    String[] tokens = file.split("\n");
    String agentCmpy = tokens[0];
    int cmpyRowCount = 0;
    for (int j = 1; j < tokens.length; j++) {
      if (tokens[j].trim().toUpperCase().startsWith("CLIENT ITINERARY")) {
        break;
      } else {
        cmpyRowCount++;
      }
    }

    for (int i = 0; i < tokens.length; i++) {
      String s = tokens[i];

      if (s.trim().toUpperCase().startsWith("CLIENT:")) {
        if (client == null) {
          client = s.trim().substring(7).trim();
        }
      }

      if (s.trim().toUpperCase().startsWith("FLIGHT DATE(S):")) {
        if (flightDate == null) {
          flightDate = s.trim().substring(15).trim();
        }
      }

      if (client != null && !client.isEmpty() && flightDate != null && !flightDate.isEmpty()) {
        String tripName = "Itinerary for " + client + " on " + flightDate;
        trip.setName(tripName);
      }

      if (s.trim().toUpperCase().startsWith("AIRCRAFT:")) {
        if (aircraft == null) {
          aircraft = s.trim().substring(9).trim();
        }
      }

      if (s.trim().toUpperCase().startsWith("ROUTING:")) {
        if (routing == null) {
          routing = s.trim().substring(8).trim();
          if (!tokens[i+1].isEmpty() && (tokens[i+1].trim().contains("-") || !isEndOfHeaderSection(tokens[i+1].trim(), agentCmpy))) {
            routing += tokens[i+1].trim();
          }
        }
      }

      StringBuilder noteTitle = new StringBuilder();
      if (routing != null && !routing.isEmpty()) {
        String[] temp = routing.split("-");
        noteTitle.append(temp[0].trim());
        noteTitle.append("-");
        if (temp.length > 1) {
          int lastInd = temp.length - 1;
          noteTitle.append(temp[lastInd].trim());
        }
      }
      if (flightDate != null && !flightDate.isEmpty()) {
        noteTitle.append(" on ");
        noteTitle.append(flightDate);
      }

      if (s.trim().toUpperCase().startsWith("PROPOSED ROUTING & TIMES")) {
        int j = i;
        for (j = j + 2; j < tokens.length && !isEndOfFlightSection(tokens[j], agentCmpy); j++) {
          if (isAFlightLine(tokens[j])) {
            String extraLine = null;
            if (!isEndOfFlightSection(tokens[j + 1], agentCmpy) && !isAFlightLine(tokens[j + 1])) {
              extraLine = tokens[j + 1];
            }
            parseFlight(tokens[j], extraLine);
          }
        }
        i = j;
          
      }

      if (s.trim().toUpperCase().startsWith("AIRPORT NAME FACILITY NAME STREET")) {
        int j = i;
        for (j = j + 1; j < tokens.length && !isEndOfAirportSection(tokens[j], agentCmpy); j++) {
          String line1 = tokens[j];
          j++;
          String line2 = tokens[j];
          parseAirport(line1, line2);
        }
        i = j - 1;
      }

      if (s.trim().toUpperCase().startsWith("CATERING")) {
        int j = i;
        for (j = j + 1; j < tokens.length && !isEndOfMealSection(tokens[j], agentCmpy); j++) {
          parseMeal(tokens[j], addedMealsForCities.size());
        }
        i = j - 1;

        if (mealPlans.size() < flights.size()) {
          int k = j;
          for (k = k + 1; k < tokens.length && mealPlans.size() < flights.size(); k++) {
            String temp = tokens[k].toLowerCase();
            if (temp.contains("provided") || temp.contains("snacks") || temp.contains("beverages") || temp.contains("standard")
                || temp.contains("tba") || temp.contains("breakfast") || temp.contains("lunch") || temp.contains("dinner")
                || temp.contains("food") || temp.contains("drink") || temp.contains("soup") || temp.contains("cream")
                || temp.contains("cracker") || temp.contains("cheese") || temp.contains("mushroom")) {
              parseMeal(temp, addedMealsForCities.size());
            }
          }
        }
      }

      //TRANSFERS DISABLED - CLIENT DOES NOT WANT TO INCLUDE TRANSFERS FOR NOW

      /*if (s.trim().toUpperCase().startsWith("TRANSPORTATION")) {
        int j = i;
        for (j = j + 1; j < tokens.length && !isEndOfTransferSection(tokens[j], agentCmpy); j++) {
          String line1 = tokens[j];
          j++;
          String line2 = tokens[j];
          j++;
          String line3 = "";
          if (!isEndOfTransferSection(tokens[j], agentCmpy)) {
            line3 = tokens[j];
            j++;
          }

          String line4 = "";
          if (!isEndOfTransferSection(tokens[j], agentCmpy)) {
            line4 = tokens[j];
          }
          parseTransfer(line1, line2, line4);
        }
        i = j - 1;
      }*/

      if (s.trim().toUpperCase().contains("PASSENGERS HOST CELL")) {
        int j = i;
        for (j = j + 1; j < tokens.length && !isEndOfPassengerSection(tokens[j]); j++) {
          if (tokens[j].contains(" x")) {
            parsePassenger(tokens[j]);
          }
        }
        i = j - 1;
      }

      if (s.trim().toUpperCase().startsWith("NOTES")) {
        int j = i;
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        for (j = j + 1; j < tokens.length && !isEndOfNotesSection(tokens[j]); j++) {
          String line = tokens[j];
          if (!line.trim().isEmpty()) {
            if (line.trim().startsWith("Contact:") && line.contains(" Confirmed")) {
              int ind = line.indexOf(" Confirmed");
              if (line.substring(0, ind).trim().length() > 11) {
                sb.append(line.substring(0, ind).trim());
                sb.append("<br/>");
              }
              if (line.substring(ind + 1).trim().length() > 16) {
                sb2.append(line.substring(ind + 1).trim());
                sb2.append("<br/>");
              }
            } else if (line.trim().startsWith("Phone:") && line.contains(" Phone: ")) {
              int ind = line.indexOf(" Phone: ");
              if (line.substring(0, ind).trim().length() > 9) {
                sb.append(line.substring(0, ind).trim());
                sb.append("<br/>");
              }
              if (line.substring(ind + 1).trim().length() > 9) {
                sb2.append(line.substring(ind + 1).trim());
                sb2.append("<br/>");
              }
            } else if (line.trim().startsWith("Cell:") && line.contains(" Toll Free: ")) {
              int ind = line.indexOf(" Toll Free: ");
              if (line.substring(0, ind).trim().length() > 8) {
                sb.append(line.substring(0, ind).trim());
                sb.append("<br/>");
              }
              if (line.substring(ind + 1).trim().length() > 13) {
                sb2.append(line.substring(ind + 1).trim());
                sb2.append("<br/>");
              }
            } else if (line.trim().startsWith("Fax:") && line.contains(" Fax: ")) {
              int ind = line.indexOf(" Fax: ");
              if (line.substring(0, ind).trim().length() > 7) {
                sb.append(line.substring(0, ind).trim());
                sb.append("<br/>");
              }
              if (line.substring(ind + 1).trim().length() > 7) {
                sb2.append(line.substring(ind + 1).trim());
                sb2.append("<br/>");
              }
            } else if (line.trim().startsWith("E-Mail:") && line.contains(" E-Mail: ")) {
              int ind = line.indexOf(" E-Mail: ");
              if (line.substring(0, ind).trim().length() > 10) {
                sb.append(line.substring(0, ind).trim());
                sb.append("<br/>");
              }
              if (line.substring(ind + 1).trim().length() > 10) {
                sb2.append(line.substring(ind + 1).trim());
                sb2.append("<br/>");
              }
            } else if (line.trim().startsWith("Last Updated:")) {
              if (line.trim().length() > 16) {
                sb2.append(line.trim());
                sb2.append("<br/>");
              }
            } else if (line.trim().startsWith("Updated By:")) {
              if (line.trim().length() > 14) {
                sb2.append(line.trim());
                sb2.append("<br/>");
              }
            } else {
              sb.append(line);
              sb.append("<br/>");
            }
          }
        }
        if (sb2.length() > 1) {
          sb.append("<br/><br/>");
          sb.append(sb2);
        }
        parseNote("NOTES: " + noteTitle.toString(), sb2.toString());
        i = j - 1;
      }

      if (s.trim().toUpperCase().startsWith("TRAVEL REMINDERS")) {
        int j = i;
        StringBuilder sb = new StringBuilder();
        for (j = j + 1; j < tokens.length && !isEndOfReminderSection(tokens[j]); j++) {
          if (!tokens[j].trim().isEmpty()) {
            sb.append(tokens[j]);
            sb.append("<br/>");
          }
        }
        parseNote("TRAVEL REMINDERS: " + noteTitle.toString(), sb.toString());
        i = j - 1;
      }

      if (s.trim().toUpperCase().startsWith("COMMENTS")) {
        int j = i;
        StringBuilder sb = new StringBuilder();
        for (j = j + 1; j < tokens.length; j++) {
          if (!tokens[j].trim().isEmpty()) {
            sb.append(tokens[j]);
            sb.append("<br/>");
          }
        }
        parseNote("COMMENTS" + noteTitle.toString(), sb.toString());
        i = j;
      }
    }
  }

  private boolean isAFlightLine(String line) {
    if (line != null && !line.isEmpty()) {
      Matcher m = datePattern.matcher(line);
      if (m.find()) {
        return true;
      }
    }
    return false;
  }

  private boolean isEndOfHeaderSection(String line, String cmpyName) {
    if (line.trim().toLowerCase().startsWith("all times are local to departure or arrival city")
        || line.trim().toUpperCase().startsWith("PROPOSED ROUTING ")
        || isEndOfFlightSection(line, cmpyName) || isEndOfAirportSection(line, cmpyName)
        || isEndOfMealSection(line, cmpyName) || isEndOfTransferSection(line, cmpyName)
        || isEndOfPassengerSection(line) || isEndOfNotesSection(line)) {
      return true;
    }
    return false;
  }

  private boolean isEndOfFlightSection(String line, String cmpyName) {
    if (line.trim().toLowerCase().startsWith("all times are local to departure or arrival city")
        || line.trim().toUpperCase().startsWith("AIRPORT NAME FACILITY NAME STREET") || line.trim().startsWith(cmpyName)) {
      return true;
    }
    return false;
  }

  private boolean isEndOfAirportSection(String line, String cmpyName) {
    if (line.trim().startsWith(cmpyName) || line.trim().toUpperCase().startsWith("CATERING")) {
      return true;
    }
    return false;
  }

  private boolean isEndOfPassengerSection(String line) {
    if (line.trim().toUpperCase().startsWith("TOTAL PASSENGERS") || line.trim().toUpperCase().startsWith("NOTES")
        || line.trim().toUpperCase().startsWith("TRAVEL REMINDERS") || line.trim().toUpperCase().startsWith("COMMENTS")) {
      String sub = line.trim().substring(16);
      String[] pTotals = sub.trim().split(" ");
      if (pTotals != null && pTotals.length > 0) {
        numOfFlightLegs = pTotals.length;
        Set<String> legSet = new HashSet<>(Arrays.asList(pTotals));
        if (legSet.size() > 1) {
          completeLegs = false;
        }
      }
      return true;
    }
    return false;
  }

  private boolean isEndOfNotesSection(String line) {
    if (line.trim().toUpperCase().startsWith("TRAVEL REMINDERS") || line.trim().toUpperCase().startsWith("COMMENTS")) {
      return true;
    }
    return false;
  }

  private boolean isEndOfReminderSection(String line) {
    if (line.trim().toUpperCase().startsWith("COMMENTS")) {
      return true;
    }
    return false;
  }

  private boolean isEndOfMealSection(String line, String cmpyName) {
    if (line.trim().toLowerCase().startsWith("all charges/gratuities pre-paid by")
        || line.trim().startsWith(cmpyName) || line.trim().toUpperCase().startsWith("TRANSPORTATION")) {
      return true;
    }
    return false;
  }

  private boolean isEndOfTransferSection(String line, String cmpyName) {
    if (line.trim().toLowerCase().startsWith("all charges/gratuities pre-paid by")
        || line.trim().startsWith(cmpyName) || line.trim().toUpperCase().startsWith("PASSENGER MANIFEST")) {
      return true;
    }
    return false;
  }

  private void processAllFlightVO () {
    int k = 0;
    for (String key : airports.keySet()) {
      AirportVO airportVO = airports.get(key);
      String name = airportNames.get(k);
      String[] nameCode = name.split("__");
      String airportName = nameCode[0];
      String note = airportName;
      if (nameCode[2] != null && !nameCode[2].isEmpty()) {
        note += "\r\n" + nameCode[2];
      }
      if (nameCode[3] != null && !nameCode[3].isEmpty()) {
        note += "\r\n" + nameCode[3];
      }
      if (nameCode[4] != null && !nameCode[4].isEmpty()) {
        String[] temp = nameCode[4].split("/");
        note += "\r\n" + temp[0];
        if (temp.length > 1) {
          note += " | " + temp[1];
        }
      }
      airportVO.setName(airportName);
      airportVO.setCode(nameCode[1]);
      airportVO.setNote(note);
      airports.put(key, airportVO);
      k++;
    }

    for (int i = 0; i < flights.size(); i++) {
      FlightVO flight = flights.get(i);

      AirportVO depart = airports.get(flight.getDepartureAirport().getName().trim());
      AirportVO arrive = airports.get(flight.getArrivalAirport().getName().trim());
      String note = "";
      if (depart != null) {
        flight.setDepartureAirport(depart);
        if (depart.getNote() != null && !depart.getNote().trim().isEmpty()) {
          note += "Departure Airport: \r\n" + depart.getNote() + "\r\n";
        }
      }
      if (arrive != null) {
        if (!note.trim().isEmpty()) {
          note += "\r\n";
        }
        flight.setArrivalAirport(arrive);
        if (arrive.getNote() != null && !arrive.getNote().trim().isEmpty()) {
          note += "Arrival Airport: \r\n" + arrive.getNote() + "\r\n";
        }
      }

      String flightNote = note;
      if (flight.getNote() != null && !flight.getNote().isEmpty()) {
        flightNote = flight.getNote() + "\r\n\r\n" + note;
      }
      flight.setNote(flightNote);

      trip.addFlight(flight);
    }
  }

  private void processAllTransportVO () {
    for (int i = 0; i < flights.size() && i < transports.size(); i++) {
      FlightVO flight = flights.get(i);
      TransportVO transport = transports.get(i);

      Timestamp flightArriveTime = flight.getArrivalTime();
      transport.setPickupDate(flightArriveTime);

      trip.addTransport(transport);
    }
  }

  private void addPassengersToFlightVO () {
    for (int i = 0; i < trip.getFlights().size(); i++) {
      FlightVO flight = trip.getFlights().get(i);
      String mealPlan = mealPlans.get(i);

      for (int j = 0; j < passengers.size(); j++) {
        PassengerVO passengerVO = passengers.get(j);
        passengerVO.setMeal(mealPlan);

        PassengerVO newPassengerVO = new PassengerVO();
        newPassengerVO.setFullName(passengerVO.getFullName());
        newPassengerVO.setFirstName(passengerVO.getFirstName());
        newPassengerVO.setLastName(passengerVO.getLastName());
        newPassengerVO.setRank(passengerVO.getRank());
        newPassengerVO.setMeal(mealPlan);
        if(!completeLegs && passengerSeatMap != null && passengerSeatMap.size() > 0) {
          if (passengerSeatMap.size() > j) {
            List<String> seatMap = passengerSeatMap.get(j);
            if (seatMap != null && seatMap.size() > i) {
              String seat = seatMap.get(i);
              if (seat != null && !seat.trim().isEmpty() && !seat.trim().equalsIgnoreCase("-")) {
                flight.addPassenger(newPassengerVO);
              }
            }
          }
        } else {
          flight.addPassenger(newPassengerVO);
        }
      }
    }
  }

  private void parseFlight(String flightData, String extraLine) {
    int ind1 = 0;
    int ind2 = 0;
    int ind3 = 0;
    int ind4 = 0;
    int ind5 = 0;

    String s = flightData.trim();
    Matcher m = datePattern.matcher(s);
    String date = null;
    if (m.find()) {
      date = m.group();
      ind1 = m.end();
    }

    String departTime = null;
    String arriveTime = null;
    Matcher m1 = timePattern.matcher(s);
    if (m1.find()) {
      departTime = m1.group();
      ind2 = m1.start();
      ind3 = m1.end();
    }

    if (m1.find()) {
      arriveTime = m1.group();
      ind4 = m1.start();
      ind5 = m1.end();
    }

    String depart;
    String arrive;
    String flightDurationSub = s.substring(ind5 + 1).trim();
    String flightDuration = "N/A";

    Matcher m2 = flightDurationPattern.matcher(flightDurationSub);
    if (m2.find()) {
      flightDuration = m2.group();
    }

    depart = s.substring(ind1+1, ind2).trim().replace("  ", " ");
    arrive = s.substring(ind3+1, ind4).trim().replace("  ", " ");

    FlightVO flightVO = new FlightVO();

    flightVO.setNumber(aircraft);

    Timestamp departTimestamp = getTimestamp(date, departTime);
    flightVO.setDepatureTime(departTimestamp);

    Timestamp arriveTimestamp = getTimestamp(date, arriveTime);
    flightVO.setArrivalTime(arriveTimestamp);

    String[] departSplit = depart.split(",");
    String[] arriveSplit = arrive.split(",");

    String departCity = departSplit[0].trim();
    String arriveCity = arriveSplit[0].trim();
    String departCountry = buildFlightState(departSplit.length > 1 && departSplit[1].trim() != null ? departSplit[1].trim() : "", extraLine);
    String arriveCountry = buildFlightState(arriveSplit.length > 1 && arriveSplit[1].trim() != null ? arriveSplit[1].trim() : "", extraLine);

    String departName = departCity + ", " + departCountry;
    String arriveName = arriveCity + ", " + arriveCountry;

    AirportVO departAirportVO = new AirportVO();
    departAirportVO.setName(departName);
    departAirportVO.setCity(departCity);
    departAirportVO.setCountry(departCountry);
    flightVO.setDepartureAirport(departAirportVO);

    AirportVO arriveAirportVO = new AirportVO();
    arriveAirportVO.setName(arriveName);
    arriveAirportVO.setCity(arriveCity);
    arriveAirportVO.setCountry(arriveCountry);
    flightVO.setArrivalAirport(arriveAirportVO);

    //only use depart airports
    if (!airports.containsKey(departName)) {
      airports.put(departName, departAirportVO);
    }

    flightVO.setNote("Duration: " + flightDuration);
    flightVO.setRank(flights.size());

    flights.add(flightVO);
  }
  
  private String buildFlightState (String state, String extra){
    String newState = state;
    if (extra != null && !extra.isEmpty()) {
      if (!CommonPDFUtils.NORTH_AMERICAN_STATES.contains(state.trim().toUpperCase())) {
        String[] extraSplit = extra.trim().split(" ");
        if (extraSplit.length > 0) {
          String contState = state.trim();
          for (String temp : extraSplit) {
            String tempState = state.trim() + " " + temp.trim();
            contState += " " + temp.trim();
            if (CommonPDFUtils.NORTH_AMERICAN_STATES.contains(tempState.toUpperCase())) {
              newState = tempState.toUpperCase();
              break;
            }
            if (CommonPDFUtils.NORTH_AMERICAN_STATES.contains(contState.toUpperCase())) {
              newState = contState.toUpperCase();
              break;
            }
          }
        }
      }
    }
    return newState.trim();
  }

  private void parseAirport(String line1, String line2) {
    int ind1 = line1.indexOf('/');

    String airportName = line1.substring(0, ind1).trim().replace("  ", " ");
    String airportCode = line2.trim();
    if (line2.trim().length() > 4 ) {
      airportCode = line2.substring(0, 5).trim();
    }
    String sub1 = line1.substring(ind1+1).trim();
    int addInd = 0;
    int telInd = 0;

    String addMatch = "";

    Matcher m = addressPattern.matcher(sub1);
    if (m.find()) {
      addInd = m.start();
      addMatch = m.group();
    }

    Matcher m1 = phonePattern.matcher(sub1);
    if (m1.find()) {
      telInd = m1.start();
    }

    String fac = sub1.substring(0, addInd).trim();
    String add = sub1.substring(addInd, addInd + addMatch.length()).trim();
    String tel = "";
    if (telInd > addInd) {
      tel = sub1.substring(telInd);
    } else {
      tel = sub1.substring(addInd + addMatch.length());
    }

    String airport = airportName + "__" + airportCode + "__" + fac + "__" + add + "__" + tel;
    if (!airportNames.contains(airport)) {
      airportNames.add(airport);
    }
  }

  private void parseTransfer(String line1, String line2, String line4) {
    int ind = line1.indexOf("ARRIVAL");
    String name = line1.substring(0, ind).trim();
    TransportVO transportVO = new TransportVO();
    transportVO.setName(name);
    transportVO.setPickupLocation(line4);
    transportVO.setDropoffLocation(line2);
    transportVO.setRank(transports.size());
    transports.add(transportVO);
  }

  private void parseMeal(String line, int index) {
    String airport = "";
    String airportCity = "";
    if (airports != null && airports.size() > index) {
      List<String> keyList = new ArrayList<>(airports.keySet());
      for (String addedCity : addedMealsForCities) {
        if (line.toUpperCase().contains(addedCity.trim().toUpperCase())) {
          airport = addedCity;
        }
      }
      if (airport.isEmpty()) {
        airport = keyList.get(index);
      }
      if (!airport.isEmpty()) {
        String[] temp = airport.split(",");
        airportCity = temp[0].trim();
      }
    }

    if (!airport.isEmpty() && !addedMealsForCities.contains(airportCity)) {
      addedMealsForCities.add(airportCity);
    }
    int ind = line.trim().indexOf(airportCity);
    String mealPlan = line.substring(ind + airportCity.length()).trim();
    mealPlans.add(mealPlan);
  }

  private void parsePassenger(String line) {
    String s = line.trim();
    int ind = s.length() - 1;
    if (s.contains(" x ")) {
      ind = s.indexOf(" x ");
    }
    String name = s.substring(0, ind);
    String[] nameSplit = name.split(",");

    PassengerVO passengerVO = new PassengerVO();
    passengerVO.setFullName(s);
    passengerVO.setFirstName(nameSplit[1].trim());
    passengerVO.setLastName(nameSplit[0].trim());
    passengerVO.setRank(passengers.size());

    passengerSeatMap.put(passengerSeatMap.size(), new ArrayList<>());
    passengers.add(passengerVO);
    numOfPassengers = passengers.size();
  }

  private void parseNote(String title, String note) {
    if (note != null && !note.isEmpty()) {
      NoteVO noteVO = new NoteVO();
      noteVO.setName(title);
      noteVO.setNote(note);
      noteVO.setRank(trip.getNotes().size());
      trip.addNoteVO(noteVO);
    }
  }

  @Override
  public ArrayList<ParseError> getParseErrors() {
    return null;
  }

  @Override
  public void postProcessingImpl(String tripId, String userId) {
  }

  private Timestamp getTimestamp(String date, String time) {
    try {
      Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime());
      return t;
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Get the PDF content as String
   *
   * @param fileInputStream
   * @return pdf content as String
   */
  private static final String getPDFContentAsString(InputStream fis) {
    StringBuilder contents = new StringBuilder();
    try {
      PDFParser parser = new PDFParser(fis);
      parser.parse();

      pdfDocument = parser.getPDDocument();

      PDFTextStripperByArea stripper = new PDFTextStripperByArea();

      stripper.setSortByPosition(true);
      Rectangle rect = new Rectangle(0, 0, 650, 700);
      stripper.addRegion("body", rect);

      java.util.List<?> allPages = pdfDocument.getDocumentCatalog().getAllPages();

      for (int i = 0; i < allPages.size(); i++) {
        Object pdPage = allPages.get(i);
        stripper.extractRegions((PDPage) pdPage);
        String pageContent = stripper.getTextForRegion("body");
        contents.append(pageContent);

        //Save the Page number with Passenger info listed during initial parsing
        /*if (pageContent != null && !pageContent.isEmpty() && pageContent.contains("PASSENGER MANIFEST")) {
          if (passengerPage == null || passengerPage < 1) {
            passengerPage = i + 1;
          }
        }*/
        if (pageContent != null && !pageContent.isEmpty() && pageContent.contains("PASSENGER MANIFEST")) {
          if (passengerPagePdf == null) {
            passengerPagePdf = pdPage;
          }
        }

      }

    }
    catch (IOException e) {
      Log.err("Open Skies - Error while extracting PDF: " + e);
    }
    finally
    {
      /*try {
        if (pdfDocument != null) {
          pdfDocument.close();
        }
      } catch (IOException e) {
        Log.err("Open Skies - Error while closing PDF: " + e);
      }*/
    }
    return contents.toString();
  }

  /**
   * Get the PDF content as String
   *
   * @param fileInputStream
   * @return pdf content as String
   */
  private static final String getPassengersFromPDFAsString(Object pdfPage) {
    StringBuilder contents = new StringBuilder();
    try {
      PDFTextStripperByArea stripper = new PDFTextStripperByArea();

      stripper.setSortByPosition(true);
      int rectHeight = 1 * 10;
      int x = 270;
      int y = 230;
      int w = 30;
      int h = 10;
      List<Integer> multiCols = new ArrayList<>();
      for (int i = 0; i < numOfPassengers; i++) {
        for (int j = 0; j < numOfFlightLegs; j++) {
          Rectangle rect = new Rectangle(x, y, w, h);
          stripper.addRegion("body", rect);

          if (pdfPage != null) {
            stripper.extractRegions((PDPage) pdfPage);
            String pageContent = stripper.getTextForRegion("body");
            if (passengerSeatMap.get(i) != null) {
              if (pageContent == null || pageContent.trim().isEmpty()) {
                passengerSeatMap.get(i).add("-");
              } else {
                if (pageContent.trim().contains(" ")) {
                  multiCols.add(j);
                  String[] temp = pageContent.trim().split(" ");
                  for (String s : temp) {
                    passengerSeatMap.get(i).add(s.trim());
                    j++;
                  }
                  j--;
                } else {
                  passengerSeatMap.get(i).add(pageContent.trim());
                }
              }
            }
            contents.append(pageContent);
          }
          x += w;
        }

        if (i == 0) {
          if (!passengerSeatMap.get(0).contains("-") || !passengerSeatMap.get(0).contains("x")) {
            passengerSeatMap.put(0, new ArrayList<>());
            i--;
          }
        }

        x = 270;
        y += h;
      }

      String head = "Leg:\t";
      for (int n = 0; n < numOfFlightLegs; n++) {
        head += String.valueOf(n);
        head += " ";
      }
      for (Integer key : passengerSeatMap.keySet()) {
        String s = String.valueOf(key) + " : \t";
        for (String val : passengerSeatMap.get(key)) {
          s += val + " ";
        }
      }
    }
    catch (IOException e) {
      Log.err("Open Skies: getPassengersFromPDFAsString - Error while extracting PDF: " + e);
    }
    finally
    {

    }
    return contents.toString();
  }

}
