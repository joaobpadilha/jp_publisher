package com.mapped.publisher.parse.extractor.booking;


/**
 * Created by twong on 15-10-04.
 */


import actors.ActorsHelper;
import actors.RecLocatorActor;
import actors.SupervisorActor;
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.form.RecordLocatorForm;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFTextStripperByArea;
import org.joda.time.DateTime;
import org.owasp.html.Sanitizers;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CalypsoExtractor
    extends BookingExtractor {

  private String file;
  private TripVO trip;

  private static String[] dateTimeFormat = {"EEEE dd MMMM yyyy h:mma", "EEEE dd MMMM yyyy hmma", "EEEE dd MMMM yyyy hhmma"};
  private static DateUtils du = new DateUtils();
  private static Calendar now = Calendar.getInstance();

  private List<String> header = new ArrayList<>();
  private List<PassengerVO> travelers = new ArrayList<>();

  private String date = "";

  private String maxDate = "";

  private int rank = -1;

  private enum BOOKING_TYPE {
    HOTEL,
    FLIGHT,
    TOUR,
    TRANSFER,
    CAR,
    CRUISE,
    TRAIN,
    INSURANCE,
    DATE,
    ASSISTANCE,
    CONNECTED,
    OWN_ARRANGEMENT,
    TICKET,
    TERMS,
    UNDEFINED
  }

  ;

  private static Pattern timePattern = Pattern.compile("[0-9]{1,2}(?:[:]|)[0-9]{2}[a,p,am,pm]");
  private static Pattern hotelCheckoutPattern = Pattern.compile(
      "Check-Out\\sDate:\\s[A-Za-z]{3}\\s[0-9]{2}\\s[A-Za-z]{3}\\s[0-9]{2}\\sat\\s[0-9]{2}:[0-9]{2}");

  private final static Pattern datePattern=Pattern.compile("(?:MONDAY|TUESDAY|WEDNESDAY|THURSDAY|FRIDAY|SATURDAY|SUNDAY|MON|TUE|WED|THU|FRI|SAT|SUN).[0-9]{2}.(?:JANUARY|FEBRUARY|MARCH|APRIL|MAY|JUNE|JULY|AUGUST|SEPTEMBER|OCTOBER|NOVEMBER|DECEMBER|JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC).[0-9]{4}");
  private static Pattern numPattern = Pattern.compile("[0-9]{1,3}");

  private static Pattern amadeusPattern = Pattern.compile("Flight PNR.*.AM/[0-9A-Z]{6}");
  private String amadeusPNR = null;
  private boolean isQuote = false;



  protected void initImpl(Map<String, String> params) {

  }

  protected TripVO extractDataImpl(InputStream input)
      throws Exception {
    trip = new TripVO();
    file = getPDFContentAsString(input);

    if (file.contains("COST OF TOUR BY PASSENGER")) {
      isQuote =true;
    }

      Matcher m = amadeusPattern.matcher(file);
    if (m.find()) {
      String s = m.group();
      amadeusPNR = s.substring(s.indexOf("AM/") + 3);
    }

      //lets get the repeat header info
    processFile();
    if (file != null) {
      if (file.contains("qantasvacations.com")) {
        trip.setImportSrc(BookingSrc.ImportSrc.TRAVEL2_QANTAS);
      } else if (file.contains("islandsinthesun.com")) {
        trip.setImportSrc(BookingSrc.ImportSrc.TRAVEL2_ISLANDS);
      } else {
        trip.setImportSrc(BookingSrc.ImportSrc.TRAVEL2);
      }
    }
    trip.setImportTs(System.currentTimeMillis());

    if (isQuote) {
      //handle quote
      try {
        processQuote();
      } catch (Exception e) {
        e.printStackTrace();
      }

    }

    return trip;
  }

  private void  processFile() {
    String[] tokens = file.split("\n");
    for (int i = 0; i < tokens.length; i++) {
      String s = tokens[i];
      if (header.isEmpty()) {
        //let's process the header
        boolean processPassenger = false;
        while (true) {
          if (s.toUpperCase().contains("PREPARED") || s.toUpperCase().contains("ITINERARY")) {
            if (s.toUpperCase().contains("PREPARED")) {
              processPassenger = true;
            }
            break;
          } else {

            header.add(s);
          }
          i++;
          if (i < tokens.length) {
            s = tokens[i];
          } else {
            break;
          }
        }
        //get the passengers
        if (!header.isEmpty() && processPassenger) {
          for (String h: header) {
            if (h.contains("Booking")) {
              h = h.substring(0, h.indexOf("Booking"));
            } else if (h.contains("Flight")) {
              h = h.substring(0, h.indexOf("Flight"));
            } else if (h.contains("Issued")) {
              h = h.substring(0, h.indexOf("Issued"));
            }

            StringBuilder names1 = new StringBuilder();

            for (char c: h.toCharArray()) {
              if (Character.isUpperCase(c) || Character.isSpaceChar(c)) {
                names1.append(c);
              }
            }

            String[] nameTokens = names1.toString().split(" ");
            StringBuilder sb = new StringBuilder();
            for (String token:nameTokens) {
              if (h.contains(token + " ")) {
                sb.append(token);
                sb.append(" ");
              }
            }

            String name = sb.toString().trim();
            if (name.trim().length() > 0) {
              PassengerVO passengerVO = new PassengerVO();
              if (name.indexOf(" ") > 0) {
                name = name.substring(name.indexOf(" ") + 1);
              }
              passengerVO.setFullName(name);
              travelers.add(passengerVO);
            }
          }
        }
      } else if (!header.contains(s)) {
        BOOKING_TYPE booking_type = getBookingType(s);
        if (booking_type != BOOKING_TYPE.UNDEFINED && booking_type != BOOKING_TYPE.DATE) {
          if (booking_type == BOOKING_TYPE.HOTEL) {
            i = parseHotel(++i, tokens);
          }
          else if (booking_type == BOOKING_TYPE.TRANSFER) {
            i = parseTransfers(++i, tokens);
          }  else if (booking_type == BOOKING_TYPE.CAR) {
            i = parseCar(++i, tokens);
          }
          else if (booking_type == BOOKING_TYPE.TOUR) {
            i = parseTours(++i, tokens);
          }
          else if (booking_type == BOOKING_TYPE.FLIGHT) {
            i = parseFlight(i, tokens);
          } else if (booking_type == BOOKING_TYPE.ASSISTANCE) {

            i = parseNote(++i, tokens, "After Hours Assistance");
          } else if (booking_type == BOOKING_TYPE.OWN_ARRANGEMENT) {
            i = parseNote(++i, tokens,"Own Arrangements");
          } else if (booking_type == BOOKING_TYPE.CRUISE) {
            i = handleCruise(++i, tokens);
          } else if (booking_type == BOOKING_TYPE.TRAIN) {
            i = parseRail(++i, tokens);
          } else if (booking_type == BOOKING_TYPE.TICKET) {
            i = parseTicket(++i, tokens);
          }  else if (booking_type == BOOKING_TYPE.TERMS) {
            i = parseNote(i, tokens, "Terms and Conditions");
          }
        }
      }

    }

  }

  private BOOKING_TYPE getBookingType(String s) {
    if (s != null) {
      s = s.trim();
      String upper = s.toUpperCase();
      Matcher m = datePattern.matcher(upper);
      if (m.find()) {
        date = m.group();
        rank =0;//initialize rank for the new day;

        //let's check if this is the max date to prevent cross timezone as the last time to mess things up PUB-1026
        if (!maxDate.isEmpty()) {
          Timestamp d = getTimestamp(date, "12:00am");
          Timestamp mD = getTimestamp(maxDate, "12:00am");
          if (d.after(mD)) {
            maxDate = date;
          }
        } else {
          maxDate = date;
        }

        return BOOKING_TYPE.DATE;
      } else if (upper.equals("ACCOMMODATION")) {
        rank++;
        return BOOKING_TYPE.HOTEL;
      }
      else if (upper.equals("FLIGHT - DEPARTING")) {
        rank++;
        return BOOKING_TYPE.FLIGHT;
      }
      else if (upper.equals("TRANSFER") || upper.equals("BUS/SHUTTLE TRANSFER") || upper.equals("PRIVATE CAR")) {
        rank++;
        return BOOKING_TYPE.TRANSFER;
      }
      else if (upper.equals("CAR") || upper.equals("CAR RENTAL")) {
        rank++;
        return BOOKING_TYPE.CAR;
      }
      else if (upper.equals("SIGHTSEEING") || upper.equals("TOUR")) {
        rank++;
        return BOOKING_TYPE.TOUR;
      }
      else if (upper.equals("OWN ARRANGEMENTS")) {
        rank++;
        return BOOKING_TYPE.OWN_ARRANGEMENT;
      }
      else if (s.contains("After Hours Assistance")) {
        rank++;
        return BOOKING_TYPE.ASSISTANCE;
      }else if (s.contains("You acknowledge that you have received the terms and conditions")) {
        rank++;
        return BOOKING_TYPE.TERMS;
      }else if (s.contains("Stay connected") || s.contains("Tired of being overcharged")) {
        rank++;
        return BOOKING_TYPE.CONNECTED;
      } else if (upper.equals("CRUISE")) {
        rank++;
        return BOOKING_TYPE.CRUISE;
      } else if (upper.equals("RAIL")) {
        rank++;
        return BOOKING_TYPE.TRAIN;
      } else if (upper.equals("TICKET")) {
        rank ++;
        return BOOKING_TYPE.TICKET;
      }

    }
    return BOOKING_TYPE.UNDEFINED;
  }

  private boolean containsDay(String s) {
    if (s != null) {
      String s1 = s.toLowerCase();
      if (s1.contains("monday") || s1.contains("tuesday") || s1.contains("wednesday") || s1.contains("thursday") ||
              s1.contains( "friday") || s1.contains("saturday") || s1.contains("sunday") ||
              s.contains("MON ") || s.contains("TUE ") || s.contains("WED ") || s.contains("THU ") ||
              s.contains( "FRI ") || s.contains("SAT ") || s.contains("SUN ")  ) {
        return true;
      }
    }

    return false;
  }

  @Override
  public ArrayList<ParseError> getParseErrors() {
    return null;
  }

  private boolean skipLine (String s) {
    if (header.contains(s) || s.startsWith("AND PARTY") || (s.contains(" Page ") && s.contains(" of ") || s.startsWith("Issued "))) {
      return true;
    }
    return false;
  }


  public int parseNote (int i, String[] tokens, String title) {
    NoteVO note = new NoteVO();
    note.setName(title);
    String lTitle = title.toLowerCase();
    if (lTitle.contains("assistance") || lTitle.contains("pre-travel") || lTitle.contains("terms and conditions") || lTitle.contains("own arrangement")) {
      note.setTimestamp(getTimestamp(date, "11:59p"));
    } else {
      note.setTimestamp(getTimestamp(date, "12:00a"));
    }
    note.setRank(rank);

    StringBuilder sb = new StringBuilder();
    int startIndex = i;
    while (true) {
      String s = tokens[i].trim();
      if (!skipLine(s)) {
        if (title.contains("Assistance") && s.startsWith("From ")) {
          //we have an issue parsing the contact table
          //From Australia From New Zealand From Tahiti From FijiQantas Airways 13 13 13 0800 808 767 43 06 65 672 2880Air New Zealand 13 24 76 0800 737 000 54 07 40 672 2955Virgin Australia 13 67 89 0800 670 000Jetstar 13 15 38 0800 800 995Air Tahiti Nui 1300 732 415 09 308 3360  46 03 03Fiji Airways 0800 230 150 0800 800 178  672 0888

        } else {
          if (s.equals(s.toUpperCase()) && s.length() > 0 && !s.equals("\n") && !s.equals("\r")) {
            sb.append("<br/>");
          }
          if (s.length() > 0 && !s.equals("\n") && !s.equals("\r")) {
            sb.append(Sanitizers.FORMATTING.sanitize(s)); //sanitize since notes are html snippets
            if (appendWhitespace(tokens, i).equals(" ")) {
              sb.append(" ");
            } else {
              sb.append("<br/>");
            }
          }
        }
      }
      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED)   {
          break;
        }
        else {
          i++;
        }

      }
    }
    note.setNote(sb.toString());
    trip.addNoteVO(note);
    return i;
  }

  public int parseTicket (int i, String[] tokens) {
    NoteVO note = new NoteVO();
    note.setTimestamp(getTimestamp(date, "12:00a"));

    note.setRank(rank);


    String name = tokens[i].trim();
    note.setName(name);
    i++;


    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (!skipLine(s)) {
        if (s.equals(s.toUpperCase()) && note.getName().equals(name) && !s.contains("P/U:")){
          note.setName(s);
          sb.append(name.replace(" Confirmed", "\nStatus: Confirmed").replace(" Quote", "\nStatus: Quote"));sb.append("\n");
          //sb.append(s);sb.append("\n");

        } else {
          if (s.contains("Confirmed")) {
            s = s.replace("Confirmed", "").trim();
            if (note.getName() == null || note.getName().trim().length() == 0) {
              note.setName(s);
            }
          }
          sb.append(s);
          sb.append(appendWhitespace(tokens, i));
        }
      }
      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }
    }

    if (note.getName() == null) {
      note.setName("Ticket");
    }
    note.setNote(sb.toString().replace("\n","<br/>"));

    //check to see if we can find the hotel that this should be attached to so that we can set the time properly
    if (trip.getHotels() != null && note.getTimestamp() != null) {
      String noteDate = Utils.formatDateControl(note.getTimestamp().getTime());
      for (HotelVO hotel: trip.getHotels()) {
        String hotelCheckin = Utils.formatDateControl(hotel.getCheckin().getTime());
        if (note.getName().equals(hotel.getName()) || note.getNote().toLowerCase().contains(hotel.getName().toLowerCase()) || noteDate.equals(hotelCheckin)) {
          note.setRank(hotel.getRank() + 1);
          break;
        }
      }
    }
    trip.addNoteVO(note);
    return i;
  }

  public int parseHotel(int i, String[] tokens) {
    HotelVO hotel = new HotelVO();
    hotel.setCheckin(getTimestamp(date, "12:00a"));
    hotel.setRank(rank);

    DateTime checkintDate = new DateTime(getTimestamp(date,"12:00a"));
    String checkoutDateStr = Utils.formatTimestamp(checkintDate.plusDays(1).getMillis(),"EEEE dd MMMM yyyy");
    hotel.setCheckout(getTimestamp(checkoutDateStr, "12:00a"));

    StringBuilder sb = new StringBuilder();
    int startIndex = i;
    while (true) {
      String s = tokens[i];
      s = s.replace("Confirmed", "")
              .replace("-mTS","");

      if (!skipLine(s)) {
        if (s.equals(s.toUpperCase()) && hotel.getName() == null && !s.contains("P/U:") ) {
          hotel.setName(cleanName(s));
          if (s.length() > hotel.getName().length()) {
            String bookingCode = s.substring(s.indexOf(hotel.getName()) + hotel.getName().length()).trim();
            if (bookingCode != null && bookingCode.length() > 0 && !bookingCode.endsWith(" PM") && !bookingCode.endsWith(" AM")) {
              sb.append("\nBooking Code: ");
              sb.append(bookingCode);
              sb.append("\n\n");
            }
          }

        }
        else if (s.contains("Check-in:") || s.contains("Check in:") || s.contains("Check In:")) {
          hotel.setCheckin(getTimestamp(date, parseTimestamp(s.substring(9).trim())));
        }
        else if (s.contains("For") && s.contains("night")) {
          String n = s.substring(4, s.indexOf("night")).trim();
          try {
            int numDays = Integer.parseInt(n);
            checkoutDateStr = Utils.formatTimestamp(checkintDate.plusDays(numDays).getMillis(),"EEEE dd MMMM yyyy");
            hotel.setCheckout(getTimestamp(checkoutDateStr, "12:00p"));
          } catch (Exception e) {

          }
        }
        else if (s.contains("Checkout required by")) {
          hotel.setCheckout(getTimestamp(checkoutDateStr, parseTimestamp(s)));
        }
        else if (s.contains("Supplier Reference:")) {
          hotel.setConfirmation(s.substring(s.indexOf("Supplier Reference:") + 19).trim());
        }
        else {
          if (s.contains("TICKET")) {
            sb.append("\n");
          }
          sb.append(s);
          if (s.contains("Meals Provided:")) {
            sb.append("\n\n");
          } else {
            sb.append(appendWhitespace(tokens, i));
          }
        }
      }

      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }
    }
    hotel.setNote(sb.toString());

    if (hotel.isValid()) {
      trip.addHotel(hotel);
    }

    return i;


  }

  public int parseFlight(int i, String[] tokens) {
    FlightVO flight = new FlightVO();
    StringBuilder sb = new StringBuilder();
    flight.setRank(rank);


    while (true) {
      String s = tokens[i];
      if (!skipLine(s)) {
        if (s.contains("Depart")) {
          s = s.replace("Confirmed","");
          flight.setDepatureTime(getTimestamp(date, parseTimestamp(s)));
          if (s.contains(" on ")) {
            String airportName = s.substring(s.indexOf("Depart")+6, s.indexOf(" on "));
            String terminal = null;
            if (airportName != null) {
              if (airportName.contains("Domestic Terminal")) {
                terminal = airportName.substring(airportName.indexOf("Domestic Terminal"));
                terminal = terminal.replace("Terminal", "");
                airportName = airportName.substring(0, airportName.indexOf("Domestic Terminal")).trim();
              } else if (airportName.contains("International Terminal")) {
                terminal = airportName.substring(airportName.indexOf("International Terminal"));
                terminal = terminal.replace("Terminal", "");
                airportName = airportName.substring(0, airportName.indexOf("International Terminal")).trim();
              } else if (airportName.contains("Terminal")) {
                terminal = airportName.substring(airportName.indexOf("Terminal"));
                terminal = terminal.replace("Terminal", "");
                airportName = airportName.substring(0, airportName.indexOf("Terminal")).trim();
              }

              AirportVO airportVO = new AirportVO();
              airportVO.setName(airportName);
              airportVO.setTerminal(terminal);
              flight.setDepartureAirport(airportVO);
            }

          }
          if (s.contains(" flight ")) {
            String fNum = s.substring(s.indexOf(" flight ") + 8).trim();
            if (fNum.length() > 2) {
              flight.setCode(fNum.substring(0, 2));
              flight.setNumber(fNum.substring(2));
              if (flight.getNumber() != null && flight.getNumber().length() == 2) {
                flight.setNumber("0" + flight.getNumber());
              } else if (flight.getNumber() != null && flight.getNumber().length() == 1) {
                flight.setNumber("00" + flight.getNumber());
              }
            }
          }
        } else if (s.contains("Arrive")) {
          flight.setArrivalTime(getTimestamp(date, parseTimestamp(s)));
          String airportName = s.substring(s.indexOf("Arrive ")+7);
          String terminal = null;
          if (airportName != null) {
            if (airportName.contains("Domestic Terminal")) {
              terminal = airportName.substring(airportName.indexOf("Domestic Terminal"));
              terminal = terminal.replace("Terminal", "");
              airportName = airportName.substring(0, airportName.indexOf("Domestic Terminal")).trim();
            } else if (airportName.contains("International Terminal")) {
              terminal = airportName.substring(airportName.indexOf("International Terminal"));
              terminal = terminal.replace("Terminal", "");
              airportName = airportName.substring(0, airportName.indexOf("International Terminal")).trim();
            } else if (airportName.contains("Terminal")) {
              terminal = airportName.substring(airportName.indexOf("Terminal"));
              terminal = terminal.replace("Terminal", "");
              airportName = airportName.substring(0, airportName.indexOf("Terminal")).trim();
            }

            AirportVO airportVO = new AirportVO();
            airportVO.setName(airportName);
            airportVO.setTerminal(terminal);
            flight.setArrivalAirport(airportVO);
          }


        } else if (s.contains("PNR Number:")) {
          flight.setReservationNumber(s.substring(s.indexOf("PNR Number:") + 11).trim());
        } else {
          if (!s.contains("Flight - Arriving")) {
            sb.append(s);
            sb.append("\n");
          }
        }

      }


      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (flight.getArrivalTime() == null && getBookingType(nextLine) == BOOKING_TYPE.DATE) {
          i++;
        } else if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }
      }
    }

    //flights with the amaedus PNR will be retrieved via the PNR lookup to avoid duplicates
    if (flight.isValid()
            && (travelers.isEmpty() ||
            amadeusPNR == null ||
            amadeusPNR.isEmpty() ||
            flight.getReservationNumber() == null ||
            !flight.getReservationNumber().equals(amadeusPNR) ||
            (flight.getDepatureTime() != null && flight.getDepatureTime().getTime() <  Instant.now().toEpochMilli())
        )) {
      if (flight.getName() != null && flight.getName().contains("Confirmed")) {
        flight.setName(flight.getName().substring(0, flight.getName().indexOf("Confirmed")).trim());
      }
      //add the travelers to the flight
      if (!travelers.isEmpty()) {
        for (PassengerVO pVO: travelers) {
          flight.addPassenger(pVO);
        }
      }
      trip.addFlight(flight);
    }

    return i;
  }

  public String cleanFirstName  (String firstName) {
    firstName = firstName.replace("MISS","");
    firstName = firstName.replace("MS","");
    firstName = firstName.replace("MR","");
    firstName = firstName.replace("MRS", "");
    firstName = firstName.trim();
    return firstName;
  }

  public int parseTransfers(int i, String[] tokens) {
    TransportVO transport = new TransportVO();
    transport.setBookingType(ReservationType.TRANSPORT);
    transport.setCmpyName("");
    transport.setRank(rank);

    String name = tokens[i].trim();
    Matcher timeMatcher = timePattern.matcher(tokens[i]);
    Timestamp start = null;
    Timestamp end = null;
    String startStr = null;
    while (timeMatcher.find()) {
      String s2 = timeMatcher.group();
      if (startStr == null) {
        startStr = s2;
        start = getTimestamp(date, s2);
      } else {
        Timestamp t = getTimestamp(date, s2);
        if (t.after(start)) {
          end = t;
        }
      }
    }
    if (startStr != null && name.startsWith(startStr)) {
      name = name.replace(startStr, "");
      if (name.startsWith("m ")) {
        name = name.substring(2).trim();
      }
    }


    if (start == null) {
      start= getTimestamp(date, "12:00a");
    }

    transport.setPickupDate(start);
    transport.setDropoffDate(end);
    transport.setName(name);

    if (transport.getName().contains(" Confirmed")) {
      transport.setName(transport.getName().replace("Confirmed","").trim());
    }
    i++;


    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (!skipLine(s)) {
        if (s.equals(s.toUpperCase()) && transport.getName() == null && !s.contains("P/U:")) {
          transport.setCmpyName(s.trim());
        } else if (s.contains("Supplier Reference:")) {
          transport.setConfirmationNumber(s.substring(s.indexOf("Supplier Reference:") + 19).trim());
        } else if (s.equals(s.toUpperCase()) && transport.getName().equals(name) && !s.contains("P/U:")){
          transport.setName(cleanName(s));
          sb.append(name.replace(" Confirmed", "\nStatus: Confirmed").replace(" Quote", "\nStatus: Quote"));sb.append("\n");
          //sb.append(s);sb.append("\n");
          if (s.length() > transport.getName().length()) {
            String bookingCode = s.substring(s.indexOf(transport.getName()) + transport.getName().length()).trim();
            if (bookingCode != null && bookingCode.length() > 0 && !bookingCode.endsWith(" PM") && !bookingCode.endsWith(" AM")) {
              sb.append("\nBooking Code: ");
              sb.append(bookingCode);
              sb.append("\n\n");
            }
          }
        } else {
          if (s.contains("Confirmed")) {
            s = s.replace("Confirmed", "").trim();
            if (transport.getName() == null || transport.getName().trim().length() == 0) {
              transport.setName(s);
            }
          }
          sb.append(s);
          sb.append(appendWhitespace(tokens, i));
        }
      }
      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }
    }
    if (transport.getName() == null) {
      transport.setName("Transfer");
    }

    transport.setNote(sb.toString());

    if (transport.isValid()) {
      trip.addTransport(transport);
    }

    return i;
  }

  public int parseRail(int i, String[] tokens) {
    TransportVO transport = new TransportVO();
    transport.setBookingType(ReservationType.RAIL);
    transport.setRank(rank);

    String name = tokens[i].trim();
    Matcher timeMatcher = timePattern.matcher(tokens[i]);
    Timestamp start = null;
    Timestamp end = null;
    String startStr = null;
    while (timeMatcher.find()) {
      String s2 = timeMatcher.group();
      if (startStr == null) {
        startStr = s2;
        start = getTimestamp(date, s2);
      } else {
        Timestamp t = getTimestamp(date, s2);
        if (t.after(start)) {
          end = t;
        }
      }
    }
    if (startStr != null && name.startsWith(startStr)) {
      name = name.replace(startStr, "");
      if (name.startsWith("m ")) {
        name = name.substring(2).trim();
      }
    }


    if (start == null) {
      start= getTimestamp(date, "12:00a");
    }

    transport.setPickupDate(start);
    transport.setDropoffDate(end);
    transport.setName(name);
    i++;


    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (!skipLine(s)) {
        if (s.equals(s.toUpperCase()) && transport.getName() == null && !s.contains("P/U:")) {
          transport.setCmpyName(s.trim());
        } else if (s.contains("Supplier Reference:")) {
          transport.setConfirmationNumber(s.substring(s.indexOf("Supplier Reference:") + 19).trim());
        } else if (s.equals(s.toUpperCase()) && transport.getName().equals(name) && !s.contains("P/U:")){
          transport.setName(cleanName(s));
          sb.append(name.replace(" Confirmed", "\nStatus: Confirmed").replace(" Quote", "\nStatus: Quote"));sb.append("\n");
          sb.append(s);sb.append("\n");
          if (s.length() > transport.getName().length()) {
            String bookingCode = s.substring(s.indexOf(transport.getName()) + transport.getName().length()).trim();
            if (bookingCode != null && bookingCode.length() > 0 && !bookingCode.endsWith(" PM") && !bookingCode.endsWith(" AM")) {
              sb.append("\nBooking Code: ");
              sb.append(bookingCode);
              sb.append("\n\n");
            }
          }
        } else {
          if (s.contains("Confirmed")) {
            s = s.replace("Confirmed", "").trim();
            if (transport.getName() == null || transport.getName().trim().length() == 0) {
              transport.setName(s);
            }
          }
          sb.append(s);
          sb.append(appendWhitespace(tokens, i));
        }
      }
      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }
    }
    if (transport.getName() == null) {
      transport.setName("Transfer");
    }

    transport.setNote(sb.toString());

    if (transport.isValid()) {
      trip.addTransport(transport);
    }

    return i;
  }

  public int handleCruise(int i, String[] tokens) {
    //let's look for next couple of lines to see if this is a 1 day trip or a proper cruise
    boolean treatAsActivity = false;
    int numOfNights = 0;
    for (int j = i; i < tokens.length; j++) {
      String s = tokens[i];
      if (s != null && (s.toLowerCase().contains("night") || s.toLowerCase().contains("day"))) {
        Matcher matcher = numPattern.matcher(s);
        if (matcher.find()) {
          String m = matcher.group();
          System.out.println("----- " + m);
          if (StringUtils.isNumeric(m)) {
            numOfNights = Integer.parseInt(m);
            if (numOfNights == 1) {
              treatAsActivity = true;
            }

          }
        }
        break;
      } else if ((j - i) > 5) { //if we scanned 5 lines - we should give up
         break;
      }
    }

    if (treatAsActivity || numOfNights == 0) {
      return parseCruiseAsActivity(i, tokens);
    } else {
      return parseCruise(i, tokens, numOfNights);
    }
  }

  public int parseCruise(int i, String[] tokens, int numOfNights) {
    if (numOfNights < 0) {
      numOfNights = 0;
    }
    CruiseVO cruise = new CruiseVO();
    cruise.setRank(rank);

    String name = tokens[i].trim();
    if (name.indexOf(" for ") > 0 && name.contains("nights")) {
      name = name.substring(0, name.indexOf(" for "));
    }
    cruise.setTimestampDepart(getTimestamp(date, "12:00a"));
    cruise.setTimestampArrive(Timestamp.from(cruise.getTimestampDepart().toInstant().plus(numOfNights, ChronoUnit.DAYS)));

    cruise.setName(name);
    i++;


    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (!skipLine(s)) {
        if (s.equals(s.toUpperCase()) && cruise.getCompanyName() == null && !s.contains("P/U:")) {
          System.out.println("------- " + s);
          cruise.setCompanyName(s.trim());
        } else if (s.contains("Supplier Reference:")) {
          cruise.setConfirmationNo(s.substring(s.indexOf("Supplier Reference:") + 19).trim());
        }else {
          if (s.contains("Confirmed")) {
            s = s.replace("Confirmed", "").trim();
            if (cruise.getName() == null || cruise.getName().trim().length() == 0) {
              cruise.setName(s);
            }
          }
          sb.append(s);
          sb.append(appendWhitespace(tokens, i));
        }
      }
      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }
    }


    cruise.setNote(sb.toString());

    if (cruise.isValid()) {
      trip.addCruise(cruise);
    }

    return i;
  }

  public int parseCruiseAsActivity(int i, String[] tokens) {
    ActivityVO activity = new ActivityVO();
    activity.setBookingType(ReservationType.ACTIVITY);
    activity.setRank(rank);

    String name = tokens[i].trim();
    if (name.indexOf(" for ") > 0 && name.contains("nights")) {
      name = name.substring(0, name.indexOf(" for "));
    }

    activity.setStartDate(getTimestamp(date, "12:00a"));
    activity.setEndDate(null);
    activity.setName(name);
    i++;


    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      System.out.println(s);
      if (!skipLine(s)) {
        if (s.contains("Supplier Reference:")) {
          activity.setConfirmation(s.substring(s.indexOf("Supplier Reference:") + 19).trim());
        } else if (s.contains("Pick up from")) {
          activity.setStartLocation(s.substring(s.indexOf("Pick up from") + 12 ).trim());
          sb.append(s);
          sb.append(appendWhitespace(tokens, i));
        }  else if (s.equals(s.toUpperCase()) && activity.getName().equals(name) && !s.contains("P/U:")){
          activity.setName(cleanName(s));
          sb.append(name.replace(" Confirmed", "\nStatus: Confirmed").replace(" Quote", "\nStatus: Quote"));sb.append("\n");
          sb.append(s);sb.append("\n");
          if (s.length() > activity.getName().length()) {
            String bookingCode = s.substring(s.indexOf(activity.getName()) + activity.getName().length()).trim();
            if (bookingCode != null && bookingCode.length() > 0 && !bookingCode.endsWith(" PM") && !bookingCode.endsWith(" AM")) {
              sb.append("\nBooking Code: ");
              sb.append(bookingCode);
              sb.append("\n\n");
            }
          }
        } else {
          if (s.contains("Confirmed")) {
            s = s.replace("Confirmed", "").trim();
            if (activity.getName() == null || activity.getName().trim().length() == 0) {
              activity.setName(s);
            }
          }
          sb.append(s);
          sb.append(appendWhitespace(tokens, i));
        }
      }
      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }
    }


    activity.setNote(sb.toString());

    if (activity.isValid()) {
      trip.addActivity(activity);
    }

    return i;
  }

  public int parseCar(int i, String[] tokens) {
    TransportVO transport = new TransportVO();
    transport.setBookingType(ReservationType.CAR_RENTAL);
    transport.setRank(rank);

    DateTime pickupDate = new DateTime(getTimestamp(date,"12:00p"));
    String dropoffDateStr = Utils.formatTimestamp(pickupDate.plusDays(1).getMillis(),"EEEE dd MMMM yyyy");


    transport.setPickupDate(getTimestamp(date,"12:00a"));



    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (!skipLine(s)) {
        if (s.equals(s.toUpperCase()) && transport.getName() == null && !s.contains("P/U:")) {
          transport.setCmpyName(s.trim());
          transport.setName(s.trim());
          sb.append(s);sb.append("\n");

        } else if (s.contains(" picked up at ")) {
          transport.setPickupDate(getTimestamp(date,parseTimestamp(s)));
        }
        else if (s.contains(" for ") && s.contains(" days")) {
          try {
            String num = s.substring(s.indexOf(" for ") + 5, s.indexOf(" days")).trim();
            int numDays = Integer.parseInt(num);
            dropoffDateStr = Utils.formatTimestamp(pickupDate.plusDays(numDays).getMillis(),"EEEE dd MMMM yyyy");
          } catch (Exception e) {

          }
          sb.append(s);sb.append("\n");
        } else if (s.contains(" returned at ")) {
          transport.setDropoffDate(getTimestamp(dropoffDateStr, parseTimestamp(s)));
          if (transport.getDropoffDate() != null && transport.getPickupDate() != null) {
            LocalDateTime d = LocalDateTime.ofInstant(transport.getDropoffDate().toInstant(), ZoneId.systemDefault());
            LocalDateTime p = LocalDateTime.ofInstant(transport.getPickupDate().toInstant(), ZoneId.systemDefault());
            if (d.getHour() > p.getHour()) {
              //since the drop off time is after the pickup time, we adjust the date by 1 day
              //i.e the drop off date is one less day than the number of days of the rental because the dropoff time is after
              transport.setDropoffDate(new Timestamp(transport.getDropoffDate().toInstant()
                                                                              .minus(1, ChronoUnit.DAYS)
                                                                              .toEpochMilli()));
            }
          }
        } else if (s.startsWith("Pick up from")) {
          transport.setPickupLocation(s.substring(s.indexOf("Pick up from") + 12).trim());
          sb.append("\n");sb.append(s);sb.append("\n");
        } else if (s.startsWith("Drop off at")) {
          transport.setDropoffLocation(s.substring(s.indexOf("Drop off at") + 11).trim());
          sb.append("\n");sb.append(s);sb.append("\n");
        }  else if (s.contains("Supplier Reference:")) {
          transport.setConfirmationNumber(s.substring(s.indexOf("Supplier Reference:") + 19).trim());
        }else {
          if (s.contains("Code:") || s.contains("Billing Account Number:") || s.startsWith("All ") || s.contains("Includes:")) {
            sb.append("\n");
          }
          sb.append(s);
          sb.append(appendWhitespace(tokens, i));
        }
      }
      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }
    }
    if (transport.getName() == null || transport.getName().isEmpty()) {
      transport.setName("Car Rental");
    }

    transport.setNote(sb.toString());

    if (transport.isValid()) {
      trip.addTransport(transport);
    }

    return i;
  }

  public int parseTours(int i, String[] tokens) {
    ActivityVO activity = new ActivityVO();
    activity.setBookingType(ReservationType.ACTIVITY);
    activity.setRank(rank);

    String name = tokens[i].trim();
    Matcher timeMatcher = timePattern.matcher(tokens[i]);
    Timestamp start = null;
    Timestamp end = null;
    String startStr = null;
    while (timeMatcher.find()) {
      String s2 = timeMatcher.group();
      if (startStr == null) {
        startStr = s2;
        start = getTimestamp(date, s2);
      } else {
        Timestamp t = getTimestamp(date, s2);
        if (t.after(start)) {
          end = t;
        }
      }
    }
    if (startStr != null && name.startsWith(startStr)) {
      name = name.replace(startStr, "");
      if (name.startsWith("m ")) {
        name = name.substring(2).trim();
      }
    }

    if (start == null) {
      start= getTimestamp(date, "12:00a");
    }

    activity.setStartDate(start);
    activity.setEndDate(end);
    activity.setName(name);
    if (activity.getName().contains(" Confirmed")) {
      activity.setName(activity.getName().replace("Confirmed","").trim());
    }

    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (!skipLine(s)) {
        if (s.startsWith("Start time:")) {
          timeMatcher = timePattern.matcher(tokens[i]);
          start = null;
          end = null;
          while (timeMatcher.find()) {
            String s2 = timeMatcher.group();
            if (start == null) {
              start = getTimestamp(date, s2);
            } else {
              Timestamp t = getTimestamp(date, s2);
              if (t.after(start)) {
                end = t;
              }
            }
          }
          if (start != null) {
            activity.setStartDate(start);
          }
          if (end != null) {
            activity.setEndDate(end);
          }
        } else if (s.contains("Supplier Reference:")) {
          activity.setConfirmation(s.substring(s.indexOf("Supplier Reference:") + 19).trim());
        }  else if (!s.contains("~~~~~") && s.equals(s.toUpperCase()) && activity.getName().equals(name) && !s.contains("P/U:")){
          activity.setName(cleanName(s));
          sb.append(name.replace(" Confirmed", "\nStatus: Confirmed").replace(" Quote", "\nStatus: Quote"));sb.append("\n");
          sb.append(s);sb.append("\n");
          if (s.length() > activity.getName().length()) {
            String bookingCode = s.substring(s.indexOf(activity.getName()) + activity.getName().length()).trim();
            if (bookingCode != null && bookingCode.length() > 0 && !bookingCode.equals("LTD") && !bookingCode.endsWith(" PM") && !bookingCode.endsWith(" AM")) {
              sb.append("\nBooking Code: ");
              sb.append(bookingCode);
              sb.append("\n\n");
            }
          }
        } else if ((name == null || name.isEmpty()) && s.contains("Confirmed")) {
          name = s.substring(0, s.indexOf("Confirmed")).trim();
          timeMatcher = timePattern.matcher(name);
          start = null;
          while (timeMatcher.find()) {
            String s2 = timeMatcher.group();
            if (start == null) {
              start = getTimestamp(date, s2);
              if (name.indexOf("am") > 0 ) {
                name = name.substring(name.indexOf("am") + 2).trim();
              } else if (name.indexOf("pm") > 0 ) {
                name = name.substring(name.indexOf("pm") + 2).trim();
              }
            }
          }
          if (start != null) {
            activity.setStartDate(start);
          }
          activity.setName(name);

        }else {
          if (!s.equals(name)) {
            sb.append(s);
            sb.append(appendWhitespace(tokens, i));
          }
        }
      }
      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }

    }
    if (activity.getName() == null || activity.getName().isEmpty()) {
      activity.setName("Activity");
    }
    activity.setName(activity.getName().replace(" Confirmed","").replace(" Quote",""));
    activity.setNote(sb.toString());

    if (activity.isValid()) {
      trip.addActivity(activity);
    }

    return i;

  }

  private String parseTimestamp(String s) {
    if (s != null) {
      if (s.contains("pm")) {
        s = s.replace("pm", "p");
      } else if (s.contains("am")) {
        s = s.replace("am", "a");
      }
      if (s.contains(".")) {
        s=s.replace(".", ":");
      }

      Matcher m = timePattern.matcher(s);
      if (m.find()) {
        return m.group();
      }

      if (s.contains("12:00") || s.toLowerCase().contains("midday")) {
        return "12:00p";
      }
    }
    return "12:00a";
  }

  private Timestamp parseTimestamp(String s, String keyword) {
    if (s != null && keyword != null) {
      String date = s.substring(s.indexOf(keyword) + keyword.length());
      String time = "";
      if (date.contains(" at ")) {
        time = date.substring(date.indexOf(" at ") + 4).trim();
        if (time.length() > 5) {
          Matcher m = timePattern.matcher(time);
          if (m.find()) {
            time = m.group();
          }
        }
        date = date.substring(0, date.indexOf(" at "));
      }
      else if (date.length() > 13) {
        date = date.substring(0, 14).trim();
      }
      if (time.length() == 5) {
        return getTimestamp(date.trim(), time);
      }
      else {
        return getTimestamp(date.trim(), "00:00");
      }
    }
    return null;
  }

  private Timestamp getTimestamp(String date, String time) {
    try {
      if (time.contains("a") && !time.contains("am")) {
        time = time.replace("a", "am");
      } else if (time.contains("p") && !time.contains("pm")) {
        time = time.replace("p", "pm");
      }
      Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime());
      return t;
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Get the PDF content as String
   *
   * @param fileInputStream
   * @return pdf content as String
   */
  private static final String getPDFContentAsString(InputStream fis) {
    PDDocument pdfDocument = null;
    StringBuilder contents = new StringBuilder();
    try {
      PDFParser parser = new PDFParser(fis);
      parser.parse();

      pdfDocument = parser.getPDDocument();

      PDFTextStripperByArea stripper = new PDFTextStripperByArea();

      stripper.setSortByPosition(true);
      Rectangle rect = new Rectangle(0, 40, 650, 730);
      stripper.addRegion("body", rect);

      List<?> allPages = pdfDocument.getDocumentCatalog().getAllPages();

      for (Object pdPage : allPages) {
        stripper.extractRegions((PDPage) pdPage);
        contents.append(stripper.getTextForRegion("body"));
      }
    }
    catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (pdfDocument != null) {
        try {
          pdfDocument.close();
        } catch (Exception e) {

        }
      }
    }
    return contents.toString();
  }

  private String appendWhitespace (String [] lines, int pos) {
    //decide to append a white space or a line break based on whether the current line ends with a. and the next line starts with a capital
    if (lines.length > pos + 1) {
      String curLine = lines[pos].trim();
      String nextLine = lines[pos + 1].trim();
      if (curLine.contains("TEL:") || curLine.endsWith(".") || curLine.length() == 0 || nextLine.length() == 0
              || (Character.isUpperCase(curLine.charAt(curLine.length() - 1)) && Character.isUpperCase(nextLine.charAt(0)))
              || (!Character.isAlphabetic(curLine.charAt(curLine.length() - 1)) && curLine.charAt(curLine.length() - 1) != '-'  && Character.isUpperCase(nextLine.charAt(0)))
              || nextLine.startsWith("IMPORTANT INFORMATION") || nextLine.startsWith("Conditions:")
              || nextLine.startsWith("Not included") || nextLine.startsWith("Not Included") || nextLine.startsWith("NOT INCLUDED")
              || nextLine.startsWith("MEETING POINT") || nextLine.startsWith("PORTERAGE FEE")
              || nextLine.startsWith("DURATION") || nextLine.startsWith("DEPARTS") || nextLine.startsWith("TOUR ITINERARY")) {
        return "\n\n";
      } else if (!finishesWithSingleChar (curLine) && numChar(nextLine, "-") < 3 && Character.isUpperCase(curLine.charAt(0))  && nextLine.length() > 0 && Character.isUpperCase(nextLine.charAt(0)) && numChar(curLine, " ") > 3  && numChar(nextLine, " ") > 3) {
        return "\n";
      } else if (curLine.contains("For ") && curLine.contains(" adults")) {
        return "\n";
      } else if (nextLine.startsWith("* ") || nextLine.startsWith("- ") ) {
        return "\n";
      } else {
        return " ";
      }
    }
    return "\n";
  }

  private int numChar (String s, String c) {
    int i = 0;

    if (s != null && s.trim().contains(c)) {
      i = s.split(c).length;
    }
    return i;
  }

  private boolean finishesWithSingleChar (String s) {

    if (s != null && s.contains(" ")) {
      String[] tokens = s.split(" ");
      if (tokens[tokens.length - 1].length() == 1) {
        return true;
      }
    }
    return false;
  }

  private String cleanName (String s) {
    //attempt to remove the booking or confim code from the hotel name
    //HOLIDAY INN DARLING HARBOUR 2FBF
    if (s != null) {
      String[] tokens = s.trim().split(" ");
      if (tokens.length > 1) {
        String lastToken = tokens[tokens.length - 1];
        boolean skipLastToken = false;
        if (lastToken.length() > 2 && !lastToken.contains("LTD")) {
          if(!StringUtils.isAlpha(lastToken)) {
            //token has some numbers in it - unlikely to be a name
            skipLastToken = true;
          } else if (!lastToken.contains("A") && !lastToken.contains("E") && !lastToken.contains("I") && !lastToken.contains("O") && !lastToken.contains("U")) {
            //no vowels - unlikely to be a name
            skipLastToken = true;
          } else if (lastToken.equals("INBOUND")) {
            skipLastToken = true;
          }
        }

        if (skipLastToken) {
          return s.replace(lastToken,"").trim();
        }
      }

    }

    return s;
  }




  @Override
  protected void postProcessingImpl(String tripId, String userId) {
    if (tripId != null && amadeusPNR != null && !amadeusPNR.isEmpty() && travelers != null && !travelers.isEmpty() && userId != null) {
      //check to see if there are flights that need to be augmented using the record locator
      List<String> lastNames = new ArrayList<>();
      //since we are not sure whether the last name can have a space or not, we are doing a couple of checks...
      for (PassengerVO passengerVO : travelers) {
        String[] tokens = passengerVO.getFullName().split(" ");
        if (tokens.length == 2 && !lastNames.contains(tokens[1].trim())) {
          //there is a single last name... so we can just use that one
          lastNames.add(tokens[1].trim());
          break;
        }
      }
      if (lastNames.isEmpty()) {
        //ok no single last name passengers... so we need to try again and this time maybe get a couple of options
        for (PassengerVO passengerVO : travelers) {
          String[] tokens = passengerVO.getFullName().split(" ");
          if (tokens.length > 2) {
            for (int i = 1; i < tokens.length; i++) {
              StringBuilder lastname = new StringBuilder();
              for (int j = i; j < tokens.length; j++) {
                lastname.append(tokens[j]);
              }
              if (!lastNames.contains(lastname.toString())) {
                lastNames.add(lastname.toString());
              }
            }
          }
        }
      }

      for (String lastName : lastNames) {
        RecordLocatorForm form = new RecordLocatorForm();
        form.setUserId(userId);
        form.setInRecordLocator(amadeusPNR);
        form.setInTripId(tripId);
        form.setInProvider(RecordLocatorForm.Provider.AMADEUS);
        form.setInLastName(lastName);
        RecLocatorActor.Command cmd = new RecLocatorActor.Command(form);
        ActorsHelper.tell(SupervisorActor.UmappedActor.RECORD_LOCATOR, cmd);
      }
    }
  }

  private void processQuote() {
    String[] tokens = file.split("\n");
    boolean processQuote = false;
    int startPos = 0;
    int endPos = 0;
    int startTableDetails = 0;
    int startDetails = 0;
    int endDetails = 0;
    boolean hasAir = false;
    boolean hasLand = false;
    boolean hasOther = false;

    for (int i = 0; i < tokens.length; i++) {
      String line = tokens[i];
      if (line.contains("COST OF TOUR BY PASSENGER")) {
        startPos = i;
      } else if (line.contains("ITINERARY")) {
        endPos = i;
        break;
      } else if (line.contains("Passenger") && line.contains("Age") && line.contains("Gateway") && line.contains("Total")) {
        startTableDetails = i;
        if (line.contains("Air")) {
          hasAir = true;
        }
        if (line.contains("Land")) {
          hasLand = true;
        }
        if (line.contains("Other")) {
          hasOther = true;
        }
      } else if (line.contains("PAYMENT DETAILS")) {
        startDetails = i;
      } else if (line.contains("TOTAL DUE:") || line.contains("NET DUE:")) {
        endDetails = i;
      }
    }
    StringBuilder sb = new StringBuilder();
    int ageIndex = -1;

    //process detail costs
    if (startTableDetails > 0 && startDetails > 0 && startDetails > startTableDetails) {
      sb.append("<table>\n");
      sb.append("<tr>\n");
      sb.append("<td width='200' style='text-align:center;background-color:#C8CBCF;border-right: 1px solid #ffffff;'> "); sb.append("Passenger"); sb.append("</td>\n");
      sb.append("<td width='60' style='text-align:center;background-color:#C8CBCF;border-right: 1px  solid  #ffffff;'> "); sb.append("Gateway"); sb.append("</td>\n");
      if (hasAir) {
        sb.append(
            "<td width='75' style='text-align:center;background-color:#C8CBCF;border-right: 1px  solid  #ffffff;'> ");
        sb.append("Air");
        sb.append("</td>\n");
      }
      if (hasLand) {
        sb.append(
            "<td width='75' style='text-align:center;background-color:#C8CBCF;border-right: 1px  solid  #ffffff;'> ");
        sb.append("Land");
        sb.append("</td>\n");
      }
      if (hasOther) {
        sb.append(
            "<td width='75' style='text-align:center;background-color:#C8CBCF;border-right: 1px  solid #ffffff;'> ");
        sb.append("Other");
        sb.append("</td>\n");
      }
      sb.append("<td width='100' style='text-align:center;background-color:#C8CBCF'> "); sb.append("Total"); sb.append(
          "</td>\n");

      sb.append("</tr>\n");
      for (int i = startTableDetails + 1; i < startDetails; i++) {
        sb.append("<tr>\n");
        String line = tokens[i];
        String[] cols = line.split(" ");
        if (cols != null) {
          if (cols.length == 7) { //passenger line with no name
            sb.append("<td  style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append("-"); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[2]); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[3]); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[4]); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[5]); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[6]); sb.append("</td>\n");
          } else if (cols.length == 4) { //last line
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append("Total"); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(""); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[0]); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[1]); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[2]); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[3]); sb.append("</td>\n");
          }  else if (cols.length == 1) { //last line
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append("Total"); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(""); sb.append("</td>\n");
            if (hasAir)
              sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append("</td>\n");
            if (hasLand)
              sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> ");  sb.append("</td>\n");
            if (hasOther)
              sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> ");  sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[0]); sb.append("</td>\n");
          } else {
            //let's figure out where the age - is
              for (int j = 0; j < cols.length; j++) {
                String s = cols[j];
                if (s.equals("-") || StringUtils.isNumeric(s)) {
                  ageIndex = j;
                  break;
                }
              }
            if (ageIndex > 0 && (ageIndex + 2) < cols.length) {
              StringBuilder name = new StringBuilder();
              for (int j = 0; j < ageIndex; j++) {
                String s = cols[j];
                if (s.equals("-") || StringUtils.isNumeric(s)) {
                  break;
                } else {
                  name.append(s);name.append(" ");
                }
              }
              sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(name.toString().trim()); sb.append("</td>\n");
              for (int j = ageIndex + 1; j < cols.length - 1; j++) {
                sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[j]); sb.append("</td>\n");
              }
              sb.append("<td style='text-align:center;background-color:#E1E4E6;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[cols.length - 1]); sb.append("</td>\n");
            }
          }
        }
        sb.append("</tr>\n");
      }
      sb.append("</table>\n");
      sb.append("<br><br>\n");
    }

    //process detail costs
    if (startDetails > 0 && endDetails > 0 && endDetails > startDetails) {
      sb.append("PAYMENT DETAILS<br>");
      for (int i = startDetails + 1; i < endDetails + 1; i++) {
        String s = tokens[i];
        sb.append(s.replace("LESS COMMISSION", "<br/>LESS COMMISSION").replace("TOTAL PRICE","<br/>TOTAL PRICE"));
        sb.append("\n");
        sb.append("<br>\n");
      }
      sb.append("<br>\n");
    }

    //process disclaimer
    if (endDetails > 0 && endPos > 0 && endPos > endDetails) {
      for (int i = endDetails + 1; i < endPos; i++) {
        if (tokens[i].contains("-----")) {
          sb.append("<br>\n");
          sb.append(tokens[i]);
          sb.append("<br>\n");
        } else if (tokens[i].contains("Kind Regards")) {
          sb.append(tokens[i]);
          sb.append("<br>\n");
        } else {
          sb.append(tokens[i]);
          sb.append(appendWhitespace(tokens, i).replace("\n", "<br>"));
        }

      }
    }

    NoteVO noteVO = new NoteVO();
    noteVO.setName("COST OF TOUR BY PASSENGER");
    noteVO.setNote(sb.toString());
    noteVO.setRank(0);
    trip.getNotes().add(0, noteVO);

  }

  public static void main(String[] args) {
    try {
     String dir ="/home/twong/Dropbox/sync/wip/pnrs/calypso/23382T_ivc.pdf";//"/volumes/data2/Downloads/012549_itn.pdf";//"/home/twong/temp/calypso/011824_itn.pdf";//"/volumes/data2/Downloads/travel2_087204.pdf";// "/users/twong/Dropbox/UMapped_Tech/Clients Documents/Travel 2 invoices/8.pdf";//"/volumes/data2/Downloads/travel2_087204.pdf";//"/volumes/data2/Downloads/t2-prop1.pdf";//"/users/twong/Dropbox/UMapped_Share/Integration Samples/Travel2/US518303_itn.pdf"; //"/users/twong/Dropbox/UMapped_Share/Companies/Travel2/reumappedtravel2nextsteps/196402_itn.pdf";
     // String dir = "/users/twong/downloads/travel2.pdf";
      File file = new File(dir);
      InputStream ins = new FileInputStream(file);

      //System.out.println(getPDFContentAsString(ins));

      CalypsoExtractor p = new CalypsoExtractor();
      p.initImpl(null);
      TripVO t = p.extractData(ins);
      //p.postProcessingImpl("1", "2");
      System.out.println(t);

    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }
}
