package com.mapped.publisher.parse.extractor.booking;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-03-24
 * Time: 12:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class AmadeusExtractor2 extends BookingExtractor {
    private static String[] dateTimeFormat = {"dd MMM yyyy", "dd MMM yyyy ", "E dd MMM yyyy h:mma",  "E dd MMM yyyy", "E dd MMM yyyy ", "E dd MMMMM yyyy h:mm a" , "dd MMM yyyy h:mm a", "E dd MMM yyyy h:mm a" , "dd MMMMM yyyy h:mm a"} ;
    private static Pattern dayDatePattern = Pattern.compile("(MONDAY|TUESDAY|WEDNESDAY|THURSDAY|FRIDAY|SATURDAY|SUNDAY).[0-9]?[0-9].(JANUARY|FEBRUARY|MARCH|APRIL|MAY|JUNE|JULY|AUGUST|SEPTEMBER|OCTOBER|NOVEMBER|DECEMBER).20[0-9]?[0-9]", Pattern.CASE_INSENSITIVE);

    private DateUtils du = new DateUtils();
    protected void initImpl(Map<String, String> params) {

    }


    protected TripVO extractDataImpl(InputStream input) throws Exception {
        try {
            TripVO trip = new TripVO();
            StringBuffer buffer = new StringBuffer();
            PDDocument document = PDDocument.load(input);
            String reservationCode = "";


            PDFTextStripper textStripper = new PDFTextStripper();
            textStripper.setSortByPosition(false);

            String s1 = textStripper.getText(document);
            String[] data = s1.split("\n");

            String itineraryMarker = "Itinerary Booking Reference:";


            for (int i = 0; i < data.length; i++) {
                String s = data[i];


                boolean foundFlight = false;
                String departDate = null;
                if (s.contains (itineraryMarker)) {
                    String ref1 =  s.substring(s.indexOf(itineraryMarker) + itineraryMarker.length());
                    if (ref1 != null)
                        reservationCode = ref1.trim();
                } else if (s.contains(" to ")) {
                    //process flight Info
                    departDate = getDepartDate(s);
                    // process flight info
                    if (departDate != null) {
                        //process flight info
                        String duration = null;
                        String departAirport = null;
                        String arriveAirport = null;
                        String departTime = null;
                        String arriveTime = null;
                        String departTerminal = null;
                        String arriveTerminal = null;
                        String airline = null;
                        String flightNum = null;
                        String bookingCode = null;
                        String departCity = null;
                        String departCountry = null;
                        String arriveCity = null;
                        String arriveCountry = null;

                        List<String> passengers = new ArrayList<String>(); //this is a list of passengers

                        String arriveDate = departDate;
                        i++;
                        while (true) {

                            String flightInfo = data [i];
                            if (flightInfo.contains ("Depart:")) {
                                if (flightInfo.indexOf(" AM ") >= 0) {
                                    departTime = flightInfo.substring(flightInfo.indexOf("Depart:") + 7,  flightInfo.indexOf(" AM ") + 3);
                                } else if (flightInfo.indexOf(" PM ") >= 0) {
                                    departTime = flightInfo.substring(flightInfo.indexOf("Depart:") + 7,  flightInfo.indexOf(" PM ") + 3);
                                }
                                if (departTime != null) {
                                    departTime = departTime.trim();
                                }

                                if (flightInfo.contains(" - ")) {
                                    String airportInfo = flightInfo.substring(flightInfo.indexOf(" - ") + 3);
                                    if (airportInfo.contains(",")) {
                                        String[] tokens = airportInfo.split(",");
                                        if (tokens != null && tokens.length == 2) {
                                            departAirport = tokens[0].trim();
                                            departTerminal = tokens[1].trim();
                                        } else {
                                            departAirport = airportInfo.trim();
                                        }
                                    } else {
                                        departAirport = airportInfo.trim();
                                    }
                                    //get depart city and country
                                    if (departTime != null) {
                                        String departInfo = flightInfo.substring(flightInfo.indexOf(departTime) + departTime.length() , flightInfo.indexOf(" - "));
                                        if (departInfo !=  null) {
                                            String[] tokens = departInfo.split(",");
                                            if (tokens != null && tokens.length == 2) {
                                                departCity = tokens [0];
                                                departCountry = tokens [1];
                                            } else
                                                departCity = departInfo;

                                            if (departCity != null)
                                                departCity = departCity.trim();

                                            if (departCountry != null)
                                                departCountry = departCountry.trim();
                                        }
                                    }
                                }
                            } else if (flightInfo.contains ("Arrive:")) {
                                if (flightInfo.toLowerCase().indexOf("am") >= 0) {
                                    arriveTime = flightInfo.substring(flightInfo.indexOf("Arrive:") + 7,  flightInfo.toLowerCase().indexOf("am") + 2);
                                } else if (flightInfo.toLowerCase().indexOf("pm") >= 0) {
                                    arriveTime = flightInfo.substring(flightInfo.indexOf("Arrive:") + 7,  flightInfo.toLowerCase().indexOf("pm") + 2);
                                }
                                if (arriveTime != null) {
                                    arriveTime = arriveTime.trim();
                                }

                                if (flightInfo.contains(" - ")) {
                                    String airportInfo = flightInfo.substring(flightInfo.indexOf(" - ") + 3);
                                    if (airportInfo.contains(",")) {
                                        String[] tokens = airportInfo.split(",");
                                        if (tokens != null && tokens.length == 2) {
                                            arriveAirport = tokens[0].trim();
                                            arriveTerminal = tokens[1].trim();
                                        } else {
                                            arriveAirport = airportInfo.trim();
                                        }
                                    } else {
                                        arriveAirport = airportInfo.trim();
                                    }
                                    //get arrive city and country
                                    if (arriveTime != null) {
                                        String arriveInfo = flightInfo.substring(flightInfo.indexOf(arriveTime) + arriveTime.length() , flightInfo.indexOf(" - "));
                                        if (arriveInfo !=  null) {
                                            String[] tokens = arriveInfo.split(",");
                                            if (tokens != null && tokens.length == 2) {
                                                arriveCity = tokens [0];
                                                arriveCountry = tokens [1];
                                            } else
                                                arriveCity = arriveInfo;

                                            if (arriveCity != null)
                                                arriveCity = arriveCity.trim();

                                            if (arriveCountry != null)
                                                arriveCountry = arriveCountry.trim();
                                        }
                                    }
                                }
                            } else if (flightInfo.contains ("Arrival Date:")) {
                                arriveDate = flightInfo.substring(flightInfo.indexOf(":") + 1);
                                if (arriveDate != null) {
                                    arriveDate = arriveDate.trim();
                                } else {
                                    arriveDate = departDate;
                                }
                            } else if (flightInfo.contains ("Airline:")) {
                                String airlineInfo = flightInfo.substring(flightInfo.indexOf("Airline:") + 8);
                                if (airlineInfo.contains("Duration:")) {
                                    airlineInfo = flightInfo.substring(0, flightInfo.indexOf("Duration"));
                                    if (airlineInfo != null && airlineInfo.contains(" / ")) {
                                        String[] tokens = airlineInfo.split(" / ");
                                        if (tokens != null && tokens.length == 2) {
                                            airline = tokens[0];
                                            flightNum = tokens[1];
                                        } else {
                                            flightNum = airlineInfo;
                                        }
                                    }
                                    duration = flightInfo.substring(flightInfo.indexOf("Duration:") + 9);
                                    if (duration != null) {
                                        duration = duration.trim();
                                    }
                                } else {
                                    if (airlineInfo != null && airlineInfo.contains(" / ")) {
                                        String[] tokens = airlineInfo.split(" / ");
                                        if (tokens != null && tokens.length == 2) {
                                            airline = tokens[0];
                                            flightNum = tokens[1];
                                        } else {
                                            flightNum = airlineInfo;
                                        }
                                    }
                                }

                            } else if (flightInfo.contains ("Aircraft:") && flightInfo.indexOf("Aircraft:") > 2) {
                                bookingCode = flightInfo.substring(0, flightInfo.indexOf("Aircraft:"));
                            }




                            if (i == data.length || (i + 1) == data.length) {
                                break;
                            } else {
                                String nextLine = data[i + 1];
                                String nextDepartDate = getDepartDate(nextLine);
                                if (nextDepartDate != null || nextLine.startsWith("  Hotel") || nextLine.startsWith("  Tour")) {

                                    break;
                                } else
                                    i++;
                            }


                        }

                        try {
                            FlightVO flightVO = new FlightVO();
                            flightVO.setArrivalAirport(new AirportVO());
                            flightVO.setDepartureAirport(new AirportVO());
                            flightVO.setArrivalTime(getTimestamp(arriveDate, arriveTime));
                            flightVO.setDepatureTime(getTimestamp(departDate, departTime));
                            flightVO.setReservationNumber(bookingCode);
                            if (flightNum != null && flightNum.length() > 3) {
                                flightVO.setCode(flightNum.substring(0, 2));
                                flightVO.setNumber(flightNum.substring(2));
                            } else
                                flightVO.setNumber(flightNum);

                            flightVO.setName(airline);
                            flightVO.getArrivalAirport().setName(arriveAirport);
                            flightVO.getArrivalAirport().setTerminal(arriveTerminal);
                            flightVO.getArrivalAirport().setCity(arriveCity);
                            flightVO.getArrivalAirport().setCountry(arriveCountry);
                            flightVO.getDepartureAirport().setName(departAirport);
                            flightVO.getDepartureAirport().setTerminal(departTerminal);
                            flightVO.getDepartureAirport().setCity(departCity);
                            flightVO.getDepartureAirport().setCountry(departCountry);

                            if (flightVO.getDepartureAirport() != null && flightVO.getDepatureTime() != null && flightVO.getCode() != null) {
                                trip.addFlight(flightVO);
                            }

                        } catch (Exception e) {
                          e.printStackTrace();
                        }



                    }

                } else if (s.contains (" Hotel")) {
                    String hotelName = null;
                    String checkIn = null;
                    String checkOut = null;
                    String info = null;
                    String telephone = null;
                    String confirmation = null;
                    String city = null;

                    i++;
                    while (true) {

                        String hotelInfo = data [i];
                        if (hotelInfo.contains ("Check In Date:") && hotelInfo.contains ("Check Out Date:")) {
                            city = data[i - 1];
                            if (city != null)
                                city = city.trim();
                            checkIn = hotelInfo.substring(hotelInfo.indexOf("Check In Date:") + 14, hotelInfo.indexOf("Check Out Date:") );
                            checkOut = hotelInfo.substring(hotelInfo.indexOf("Check Out Date:") + 15);
                            if (checkIn != null)
                                checkIn = checkIn.trim();

                            if (checkOut != null)
                                checkOut = checkOut.trim();

                        } else if (hotelInfo.contains("Hotel Name:")) {
                            String hotelNameInfo = hotelInfo.substring(hotelInfo.indexOf("Hotel Name:") + 11);
                            if (hotelNameInfo.contains("Telephone:")) {
                                hotelNameInfo = hotelNameInfo.substring (0, hotelNameInfo.indexOf("Telephone:"));
                                telephone = hotelInfo.substring (hotelInfo.indexOf("Telephone:") + 10);
                            }

                            if (hotelNameInfo != null)
                                hotelName = hotelNameInfo.trim();
                        } else if (hotelInfo.contains("Confirmation No:")) {
                            confirmation = hotelInfo.substring(hotelInfo.indexOf("Confirmation No:") + 16);
                        } else if (hotelInfo.contains("Supplementary Information:")) {

                            info = hotelInfo.substring(hotelInfo.indexOf("Supplementary Information:") + 26);
                        }


                        if (i == data.length || (i + 1) == data.length) {
                            break;
                        } else {
                            String nextLine = data[i + 1];
                            String nextDepartDate = getDepartDate(nextLine);
                            if (nextDepartDate != null || nextLine.startsWith("  Hotel") || nextLine.startsWith("  Tour")) {
                                break;
                            } else
                                i++;
                        }

                    }

                    if (city != null && hotelName != null)
                        hotelName = hotelName + " - " + city;

                    HotelVO hotelVO = new HotelVO();
                    hotelVO.setHotelName(hotelName);
                    hotelVO.setCheckin(getTimestamp(checkIn, "3:00 pm"));
                    hotelVO.setCheckout(getTimestamp(checkOut, "12:00 pm"));
                    hotelVO.setConfirmation(confirmation);


                    StringBuffer note = new StringBuffer();
                    if (info != null) {
                        note.append(info);
                        note.append("\n");
                    }
                    if (telephone != null) {
                        note.append("Phone: ");
                        note.append(telephone);
                        note.append("\n");

                    }
                    hotelVO.setNote(note.toString());

                    trip.addHotel(hotelVO);


                }  else if (s.contains (" Tour")) {

                    String tourName = null;
                    String startDate = null;
                    String endDate = null;
                    String info = null;

                    i++;
                    while (true) {

                        String tourInfo = data [i];
                        if (tourInfo.contains ("Start Date:") && tourInfo.contains ("End Date:")) {
                            startDate = tourInfo.substring(tourInfo.indexOf("Start Date:") + 11, tourInfo.indexOf("End Date:") );
                            endDate = tourInfo.substring(tourInfo.indexOf("End Date:") + 9);
                            if (startDate != null)
                                startDate = startDate.trim();

                            if (endDate != null)
                                endDate = endDate.trim();
                        } else if (tourInfo.contains("Tour Type:")) {
                            info = tourInfo.substring(tourInfo.indexOf("Tour Type:") + 10);

                        } else if (tourInfo.contains("Tour Information:")) {
                            tourName = tourInfo.substring(tourInfo.indexOf("Tour Information:") + 17);

                            if (tourName != null)
                                tourName = tourName.trim();
                        }


                        if (i == data.length || (i + 1) == data.length) {
                            break;
                        } else {
                            String nextLine = data[i + 1];
                            String nextDepartDate = getDepartDate(nextLine);
                            if (nextDepartDate != null || nextLine.startsWith("  Hotel") || nextLine.startsWith("  Tour")) {
                                break;
                            } else
                                i++;
                        }


                    }

                    ActivityVO activityVO = new ActivityVO();
                    activityVO.setName(tourName);
                    activityVO.setStartDate(getTimestamp(startDate, ""));
                    activityVO.setEndDate(getTimestamp(endDate, ""));

                    StringBuffer note = new StringBuffer();
                    if (info != null) {
                        note.append(info);
                        note.append("\n");
                    }

                    activityVO.setNote(note.toString());
                    trip.addActivity(activityVO);

                }


            }


            document.close();
          trip.setImportSrc(BookingSrc.ImportSrc.AMADEUS_PDF);
          trip.setImportSrcId(reservationCode);
          trip.setImportTs(System.currentTimeMillis());
            return trip;

        } catch (Exception e) {
          e.printStackTrace();
        } finally {
            if (input != null) {
                input.close();
            }
        }

        return null;
    }

    private  String getDepartDate (String s) {
        if (s != null && s.contains (" to ")) {
            int count = 0;
            Matcher m = dayDatePattern.matcher(s);
            while (m.find()) {
                if (count ==0 ) {
                    String departDate = m.group();
                    return departDate;
                }
                count++;
            }
        }

        return null;

    }

    private Timestamp getTimestamp (String date, String time) {
        try {
            Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime() );
            return t;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ArrayList<ParseError> getParseErrors()
    {
        return null;
    }

  @Override
  public void postProcessingImpl(String tripId, String userId) {
  }
}
