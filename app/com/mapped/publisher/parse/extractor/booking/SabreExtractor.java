package com.mapped.publisher.parse.extractor.booking;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.mapped.publisher.utils.Log;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.pdfbox.util.PDFTextStripperByArea;
import org.joda.time.DateTime;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-11-25
 * Time: 8:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class SabreExtractor extends BookingExtractor {
  private static ArrayList<String> months = null;
  private static ArrayList<String> monthsES = null;
  private static ArrayList<String> days = null;
  private static ArrayList<String> daysES = null;

  private static enum LANG  {EN,ES};

  private LANG lang = LANG.EN;


  private static String[] dateTimeFormat = {"dd MMM yyyy", "E dd MMM yyyy h:mma", "E dd MMM yyyy", "E dd MMM yyyy hh:mm"};
    private static Pattern dayDatePattern = Pattern.compile("(MONDAY|TUESDAY|WEDNESDAY|THURSDAY|FRIDAY|SATURDAY|SUNDAY|DOMINGO|LUNES|MARTES|MIÉRCOLES|JUEVES|VIERNES|SÁBADO).[0-9]?[0-9].(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC|ENERO|ENE|MARZO|ABR|MAYO|AGOSTO|AGO|SET|DIC)", Pattern.CASE_INSENSITIVE);
    private static Pattern dateYearPattern = Pattern.compile("[0-9]?[0-9].(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC|ENERO|ENE|MARZO|ABR|MAYO|AGOSTO|AGO|SET|DIC).[0-9]?[0-9]?[0-9]?[0-9]?", Pattern.CASE_INSENSITIVE);
    private static Pattern phonePattern = Pattern.compile("[0-9]*[-\\s\\.][0-9]*[-\\s\\.][0-9]*[-\\s\\.][0-9]{3,}.[\\s]");  //99 9999 99999 9999
    private static Pattern phonePattern1 = Pattern.compile("[0-9]*[-\\s\\.][0-9]*[-\\s\\.][0-9]{3,}.[\\s]");                // 99 999 99999999
  private static Pattern flightCodePattern = Pattern.compile("[A-Z0-9]{2}.*.[0-9]{2,}");                // 99 999 99999999
  private static final char NBSP =  '\u00a0';

    protected void initImpl(Map<String, String> params) {

    }


    protected TripVO extractDataImpl(InputStream input) throws Exception {
      if (months == null) {
        months = new ArrayList<String>();
        months.add("JAN");
        months.add("FEB");
        months.add("MAR");
        months.add("APR");
        months.add("MAY");
        months.add("JUN");
        months.add("JUL");
        months.add("AUG");
        months.add("SEP");
        months.add("OCT");
        months.add("NOV");
        months.add("DEC");
      }

      if (monthsES == null) {
        monthsES = new ArrayList<String>();
        monthsES.add("JAN");
        monthsES.add("FEB");
        monthsES.add("MAR");
        monthsES.add("ABR");
        monthsES.add("MAY");
        monthsES.add("JUN");
        monthsES.add("JUL");
        monthsES.add("AGO");
        monthsES.add("SET");
        monthsES.add("OCT");
        monthsES.add("NOV");
        monthsES.add("DIC");
      }

      if (days == null) {
        days = new ArrayList<String>();
        days.add("MONDAY");
        days.add("TUESDAY");
        days.add("WEDNESDAY");
        days.add("THURSDAY");
        days.add("FRIDAY");
        days.add("SATURDAY");
        days.add("SUNDAY");
      }

      if (daysES == null) {
        daysES = new ArrayList<String>();
        daysES.add("LUNES");
        daysES.add("MARTES");
        daysES.add("MIÉRCOLES");
        daysES.add("JUEVES");
        daysES.add("VIERNES");
        daysES.add("SÁBADO");
        daysES.add("DOMINGO");
      }


        try {
            TripVO trip = new TripVO();
            StringBuffer buffer = new StringBuffer();
            DateUtils du = new DateUtils();


            PDDocument document = PDDocument.load(input);

            //get the text in the sorted order of the rendering
            PDFTextStripper stripper1 = new PDFTextStripper();
            stripper1.setSortByPosition(true);
            String s1 = stripper1.getText(document);
            String[] controlData = s1.split("\n \n");


            //get the text in the actual order of the pdf
            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition(false);
            Rectangle rect = new Rectangle(0, 0, 650, 780);
            stripper.addRegion("body", rect);
            List<?> allPages = document.getDocumentCatalog().getAllPages();

            for (Object pdPage : allPages) {
                stripper.extractRegions((PDPage) pdPage);
                buffer.append(stripper.getTextForRegion("body"));
            }

            String dataBuf = buffer.toString().replace(NBSP, ' ');
            String[] data = dataBuf.split("\n");
          ArrayList <String> miscStrings = new ArrayList<String>();

          String tripStartDate = "";
            String tripEndDate = "";

            String preparedFor = "";
            String reservationCode = "";
            HashMap<String, String> reservationCodes = new HashMap<String, String>();


            //TODO: resolve hotel address.
            DateTime dt = new DateTime();

            int startYear = dt.getYear();
            int startMonth = dt.getMonthOfYear();
            int endYear = dt.getYear();
            int endMonth = dt.getMonthOfYear();

            //try to get the date out properly
            if (s1 != null && (s1.contains("TRIP TO") || s1.contains("DESTINO"))) {
              String dateStr = null;
              if (s1.contains("DESTINO")) {
                dateStr = s1.substring(0, s1.indexOf("DESTINO"));
                lang = LANG.ES;

              } else
               dateStr = s1.substring(0, s1.indexOf("TRIP TO"));


              if (dateStr != null && dateStr.trim().length() > 0) {
                int count = 0;
                Matcher m = dateYearPattern.matcher(dateStr);
                while (m.find()) {
                  if (count == 0)
                    tripStartDate = m.group();
                  else if (count == 1) {
                    tripEndDate = m.group();
                    break;
                  }
                  count++;
                }

                //get the month and year to figure out what year these bookings are in
                if (tripStartDate != null && tripStartDate.length() == 11) {
                  if (lang == LANG.ES)
                    startMonth = monthsES.indexOf(tripStartDate.substring(3, 6));
                  else
                    startMonth = months.indexOf(tripStartDate.substring(3, 6));

                  startYear = Integer.parseInt(tripStartDate.substring(7));
                }
                if (tripEndDate != null && tripEndDate.length() == 11) {
                  if (lang == LANG.ES)
                    endMonth = monthsES.indexOf(tripEndDate.substring(3, 6));
                  else
                    endMonth = months.indexOf(tripEndDate.substring(3, 6));

                  endYear = Integer.parseInt(tripEndDate.substring(7));
                }
              }
            }


            for (int i = 0; i < data.length; i++) {
                String s = data[i];
                String s11 = "";
                if ((i+ 1) < data.length) {
                  s11 = data [i+1];
                }
                if (s.startsWith("PREPARED FOR") ||s.startsWith("PREPARADO PARA") ) {
                    if ((i + 1) < data.length) {
                        preparedFor = data[i + 1];
                        i++;
                    }
                } else if (s.startsWith("RESERVATION CODE")) {
                    reservationCode = s.replace("RESERVATION CODE", "").trim();
                } else if (s.startsWith("CÓDIGO DE RESERVACIÓN")) {
                  reservationCode = s.replace("CÓDIGO DE RESERVACIÓN", "").trim();
                } else if (s.startsWith("AIRLINE RESERVATION CODE")) {
                    String airlineReservationCode = s.replace("AIRLINE RESERVATION CODE", "").trim();

                    if (airlineReservationCode.contains("(") && airlineReservationCode.contains(")")) {
                       String [] tokens = airlineReservationCode.split("\\)");
                      String previousReserve = null;
                      for (String token: tokens) {
                        token = token.replace(",","").trim();
                        if (token.startsWith("(") && previousReserve != null) {
                          String reserve = previousReserve;
                          String airline = token.substring(token.indexOf("(") + 1);
                          reservationCodes.put(airline, reserve);

                        } else {
                          String reserve = token.substring(0, token.indexOf("(")).trim();
                          String airline = token.substring(token.indexOf("(") + 1);
                          previousReserve = reserve;
                          reservationCodes.put(airline, reserve);
                        }
                      }

                    }

                } else if ((s.contains("DEPARTURE:") || s.contains("PARTIDA:")) && (s.contains("flight") || s11.contains("flight") || s.contains("vuelo") || s11.contains("vuelo")) ) {


                    //process flight info
                    String duration = null;
                    String departAirport = null;
                    String arriveAirport = null;
                    String departTime = null;
                    String arriveTime = null;
                    String departTerminal = null;
                    String arriveTerminal = null;
                    String airlineCode = null;
                    String flightNumber = null;
                    String defClass = null;
                    StringBuilder note = new StringBuilder();

                    List<String> passengers = new ArrayList<String>(); //this is a list of passengers
                    String passengerTitle = null;

                    String departDate = null;
                    String arriveDate = null;

                    //process all flight info
                    Matcher m = dayDatePattern.matcher(s);
                    if (m.find()) {
                        departDate = m.group();
                        departDate = SabreExtractor.appendYear(departDate, startMonth, startYear, endYear);

                    }

                    //look for arrival date
                    if (m.find()) {
                        arriveDate = m.group();
                        arriveDate = SabreExtractor.appendYear(arriveDate, startMonth, startYear, endYear);

                    }


                    i++;
                    //there is an arrival date and the first line wraps.
                    if (data[i].toLowerCase().contains("please verify") ||
                        data[i].toLowerCase().contains("por favor verifique")) {
                        i++;
                    }
                    String airLine = data[i];
                    i++;
                    while (true) {
                      if (i > data.length) {
                        break;
                      }
                      String flight = data[i];
                      Matcher f = flightCodePattern.matcher(flight);
                      if (f.find()) {
                        if (flight != null && flight.length() > 4 && flight.trim().indexOf(" ") > 0) {
                          airlineCode = flight.substring(0, flight.indexOf(" "));
                          flightNumber = flight.substring(flight.indexOf(" ") + 1);
                          break;
                        }
                      }
                      i++;
                    }
                    i++;

                    StringBuilder passengerBuffer = null;
                    while (true) {
                        String flightData = data[i];

                        if (flightData.startsWith("Class:")) {
                            if ((i + 1) < data.length) {
                               defClass = data[ i+ 1];
                                i++;
                            }
                        } else if (flightData.startsWith("Duration:") || flightData.startsWith("Duración:")) {
                            if ((i + 1) < data.length) {
                              if (flightData.startsWith("Duración:"))
                                note.append("Duración: ");
                              else
                                note.append("Duration: ");

                              note.append(data[ i+ 1]);
                              note.append("\n");
                              i++;
                            }
                        } else if (flightData.startsWith("Operated by:")) {
                        if ((i + 1) < data.length) {
                           note.append("Operated by: ");

                          note.append(data[ i+ 1]);
                          note.append("\n");
                          i++;
                        }
                      } else if (flightData.trim().length() == 3) {
                            if (departAirport == null) {
                                departAirport = flightData;
                            } else if (arriveAirport == null) {
                                //sometimes the country wraps to the next line - make sure we got the airport code and not the country
                                /*
                                    PUJ
                                    PUNTA CANA, DOMINICAN
                                    REP
                                    MIA
                                    MIAMI INTERNTNL,
                                    FL
                                 */
                                if ((i + 1) < data.length && data[i + 1].trim().length() == 3) {
                                    arriveAirport = data[i + 1];
                                    i++;
                                } else {
                                    arriveAirport = flightData;
                                }

                            }
                        } else if (flightData.contains("Departing At:") || flightData.contains("Sale a la(s):")) {
                            if ((i + 1) < data.length) {
                                departTime = data[i + 1];
                                i++;
                            }

                        } else if (flightData.contains("Arriving At:") || flightData.contains("Llega a la(s):")) {
                            if ((i + 1) < data.length) {
                                arriveTime = data[i + 1];
                                i++;
                            }

                        } else if (flightData.contains("Terminal:")) {
                            if ((i + 1) < data.length) {
                                if (arriveTime != null) {
                                    arriveTerminal = data[i + 1];
                                    i++;
                                } else if (departTime != null) {
                                    departTerminal = data[i + 1];
                                    i++;
                                }

                            }
                        } else if (flightData.startsWith("Aircraft:")) {
                          note.append("Aircraft: ");
                          boolean processAircraft = true;
                          i++;
                          int maxLines = 0;
                          while (true) {
                            String s2 = data [i];
                            if (StringUtils.isNumeric(s2) && processAircraft) {
                              processAircraft =false;
                              note.append("\n");
                              note.append("Distance: ");note.append(s2);note.append("\n");
                            } else if (StringUtils.isNumeric(s2) && !processAircraft) {
                              processAircraft =false;
                              note.append("Stops: ");note.append(s2);note.append("\n");
                            } else {
                              note.append(" ");
                              note.append(s2);
                            }

                            if ((i+1) >= data.length || data[i+1].contains("Departing") || maxLines > 4) {
                              break;
                            } else
                              i++;

                            maxLines++;
                          }

                        } else if (flightData.trim().startsWith("»")) {
                           //start processing passenger
                            if (passengerBuffer != null && passengerBuffer.length() > 0) {
                              //flush
                              passengers.add(passengerBuffer.toString());
                              passengerBuffer = null;
                            }

                            if (flightData.trim().length() > 50) {
                                passengers.add(flightData.substring(1).trim());

                            } else {
                                passengerBuffer = new StringBuilder();
                                passengerBuffer.append(flightData.substring(1).trim());passengerBuffer.append(" ");
                            }

                        } else if (passengerBuffer != null) {
                          if (flightData.toLowerCase().contains("distance (") || flightData.toLowerCase().contains("stop") || flightData.toLowerCase().contains("duration")
                              || flightData.toLowerCase().contains("millaje") || flightData.toLowerCase().contains("escala") || flightData.toLowerCase().contains("duración")) {
                            passengers.add(passengerBuffer.toString());
                            passengerBuffer = null;

                          } else {
                            passengerBuffer.append(flightData.trim());
                            passengerBuffer.append(" ");
                          }
                        } else if (flightData.startsWith("Passenger Name:") || flightData.startsWith("Nombre del pasajero:")) {
                          passengerTitle = flightData.trim();

                        }


                        if ((i + 1) >= data.length) {
                          if (passengerBuffer != null && passengerBuffer.length() > 0) {
                            //flush
                            passengers.add(passengerBuffer.toString());
                          }
                            break;
                        } else {
                            String nextLine = data[i + 1];
                            if ((nextLine.contains("Notes") || nextLine.contains("Notas")) && passengerBuffer != null && passengerBuffer.length() > 0) {
                              //flush
                              passengers.add(passengerBuffer.toString());
                              passengerBuffer = null;
                            }
                            if (nextLine.contains("OTHER:") || nextLine.contains("DEPARTURE:") || nextLine.contains("PARTIDA:") ||  nextLine.contains("PICK UP:") || nextLine.contains("CHECK IN:")) {
                              if (passengerBuffer != null && passengerBuffer.length() > 0) {
                                //flush
                                passengers.add(passengerBuffer.toString());
                              }
                                break;
                            } else {
                                i++;
                            }
                        }
                    }

                    try {
                        FlightVO flightVO = new FlightVO();
                        flightVO.setArrivalAirport(new AirportVO());
                        flightVO.setDepartureAirport(new AirportVO());
                        flightVO.setPassengers(new ArrayList<PassengerVO>());
                        flightVO.setReservationNumber(reservationCodes.get(airlineCode));
                        flightVO.setCode(airlineCode);
                        flightVO.setNumber(flightNumber);
                        flightVO.setName(airLine);
                      if (lang == LANG.ES && departDate != null) {
                        String[] strings = departDate.split(" ");
                        StringBuilder sb = new StringBuilder();
                        for (String token: strings){
                          if (daysES.contains(token.trim().toUpperCase())) {
                            sb.append(days.get(daysES.indexOf(token)));
                            sb.append(" ");
                          } else if (monthsES.contains(token.trim().toUpperCase())) {
                            sb.append(months.get(monthsES.indexOf(token)));
                            sb.append(" ");
                          } else {
                            sb.append(token);sb.append(" ");
                          }
                        }
                        departDate = sb.toString().trim();
                      }

                        if (departTime != null) {
                            if (departTime.toLowerCase().contains("am")) {
                                departTime = departTime.substring(0, departTime.indexOf("am") + 2);
                            } else if (departTime.toLowerCase().contains("pm")) {
                                departTime = departTime.substring(0, departTime.indexOf("pm") + 2);
                            }
                           flightVO.setDepatureTime(new Timestamp(du.parseDate(departDate + " " + departTime, dateTimeFormat).getTime()));
                       }
                        if (arriveTime != null) {
                            String time = arriveTime;
                            if (arriveTime.toLowerCase().contains("am")) {
                                time = arriveTime.substring(0, arriveTime.indexOf("am") + 2);
                            } else if (arriveTime.toLowerCase().contains("pm")) {
                                time = arriveTime.substring(0, arriveTime.indexOf("pm") + 2);
                            }

                            if (time != null) {

                                long arrivalTime = 0;
                                if (arriveDate != null) {
                                  if (lang == LANG.ES) {
                                    String[] strings = arriveDate.split(" ");
                                    StringBuilder sb = new StringBuilder();
                                    for (String token: strings){
                                      if (daysES.contains(token.trim().toUpperCase())) {
                                        sb.append(days.get(daysES.indexOf(token)));
                                        sb.append(" ");
                                      } else if (monthsES.contains(token.trim().toUpperCase())) {
                                        sb.append(months.get(monthsES.indexOf(token)));
                                        sb.append(" ");
                                      } else {
                                        sb.append(token);sb.append(" ");
                                      }
                                    }
                                    arriveDate = sb.toString().trim();
                                  }
                                    arrivalTime = du.parseDate(arriveDate + " " + time, dateTimeFormat).getTime();
                                } else {
                                    arrivalTime = du.parseDate(departDate + " " + time, dateTimeFormat).getTime();

                                }

                                flightVO.setArrivalTime(new Timestamp(arrivalTime));
                            }
                        }

                        flightVO.getDepartureAirport().setCode(departAirport);
                        flightVO.getDepartureAirport().setTerminal(departTerminal);

                        flightVO.getArrivalAirport().setCode(arriveAirport);
                        flightVO.getArrivalAirport().setTerminal(arriveTerminal);

                        flightVO.setNotes(note.toString());

                        for (String passenger : passengers) {
                            PassengerVO passengerVO = new PassengerVO();
                            if (defClass != null) {
                                passengerVO.setSeatClass(defClass);
                            }
                            passengerVO.setNote(parsePassenger(passenger, passengerTitle, passengerVO));

                            flightVO.getPassengers().add(passengerVO);
                        }

                        if (flightVO.getDepartureAirport() != null && flightVO.getDepatureTime() != null && flightVO.getCode() != null) {
                            trip.addFlight(flightVO);
                        }
                    } catch (Exception e) {
                      e.printStackTrace();
                    }

                } else if (s.contains("PICK UP:")) {
                    //process car rental
                    String pickupDate = null;
                    String dropoffDate = null;
                    String pickupTime = null;
                    String dropoffTime = null;
                    String numDays = null;
                    String confirmation = null;
                    String cmpy = null;
                    String pickupAirport = null;
                    String dropoffAirport = null;
                    String carType = null;


                    int count = 0;
                    Matcher m = dayDatePattern.matcher(s);
                    while (m.find()) {
                        if (count == 0)
                            pickupDate = m.group();
                        else if (count == 1) {
                            dropoffDate = m.group();
                            break;
                        }
                        count++;
                    }

                    if (pickupDate != null) {

                        if (dropoffDate != null && s.indexOf(dropoffDate) > 0) {
                            numDays = s.substring(s.indexOf(dropoffDate) + dropoffDate.length()).trim();
                        }
                        pickupDate = SabreExtractor.appendYear(pickupDate, startMonth, startYear, endYear);
                        dropoffDate = SabreExtractor.appendYear(dropoffDate, startMonth, startYear, endYear);


                        i++;
                        if (i < data.length) {
                            while (true) {
                                String cmpyData = data[i];
                                if (cmpy == null) {
                                    cmpy = cmpyData + " ";
                                } else
                                    cmpy += cmpyData + " ";


                                if ((i + 1) >= data.length) {
                                    break;
                                } else {
                                    String nextLine = data[i + 1];
                                    if (nextLine.contains("OTHER:") || nextLine.contains("Confirmation:") || nextLine.contains("DEPARTURE:") || nextLine.contains("PARTIDA:") || nextLine.contains("PICK UP:") || nextLine.contains("CHECK IN:")) {
                                        break;
                                    } else {
                                        i++;
                                    }
                                }
                            }
                        }

                        i++;
                        if (i < data.length) {
                            while (true) {
                                String carData = data[i];
                                if (carData.contains("Confirmation:")) {
                                    if ((i + 1) < data.length) {
                                        confirmation = data[i + 1];
                                        i++;
                                    }
                                } else if (carData.trim().length() == 3) {  //asume pickup and drop off locations are 3 char airport code
                                    if (pickupAirport == null) {
                                        pickupAirport = carData;
                                    } else if (dropoffAirport == null) {
                                        //sometimes the country wraps to the next line - make sure we got the airport code and not the country
                                /*
                                    PUJ
                                    PUNTA CANA, DOMINICAN
                                    REP
                                    MIA
                                    MIAMI INTERNTNL,
                                    FL
                                 */
                                        if ((i + 1) < data.length && data[i + 1].trim().length() == 3) {
                                            dropoffAirport = data[i + 1];
                                            i++;
                                        } else {
                                            dropoffAirport = carData;
                                        }
                                    }

                                } else if (carData.toLowerCase().contains("car type:")) {
                                    if ((i + 1) < data.length) {
                                        carType = data[i + 1];
                                        i++;
                                    }
                                } else if (carData.contains("Pick Up Time:")) {
                                    if ((i + 1) < data.length) {
                                        pickupTime = data[i + 1];
                                        i++;
                                    }
                                } else if (carData.contains("Drop Off Time:")) {
                                    if ((i + 1) < data.length) {
                                        dropoffTime = data[i + 1];
                                        i++;
                                    }
                                }


                                if ((i + 1) >= data.length) {
                                    break;
                                } else {
                                    String nextLine = data[i + 1];
                                    if (nextLine.contains("OTHER:") || nextLine.contains("DEPARTURE:") || nextLine.contains("PARTIDA:") || nextLine.contains("PICK UP:") || nextLine.contains("CHECK IN:")) {
                                        break;
                                    } else {
                                        i++;
                                    }
                                }
                            }
                        }

                        try {
                            TransportVO transportVO = new TransportVO();
                            transportVO.setCmpyName(cmpy);
                            transportVO.setConfirmationNumber(confirmation);
                            transportVO.setPickupLocation(pickupAirport);
                            transportVO.setDropoffLocation(dropoffAirport);
                            transportVO.setPickupDate(new Timestamp(du.parseDate(pickupDate + " " + pickupTime, dateTimeFormat).getTime()));
                            transportVO.setDropoffDate(new Timestamp(du.parseDate(dropoffDate + " " + dropoffTime, dateTimeFormat).getTime()));

                            StringBuffer note = new StringBuffer();
                            note.append("Num of days: ");
                            note.append(numDays);
                            note.append("\n");
                            note.append("Car Type: ");
                            note.append(carType);
                            note.append("\n");
                            transportVO.setNote(note.toString());

                            if (transportVO.getCmpyName() != null && transportVO.getPickupDate() != null) {
                                trip.addTransport(transportVO);
                            }
                        } catch (Exception e) {
                          e.printStackTrace();
                        }
                    }


                } else if (s.contains("CHECK IN:")) {
                    String title = s;
                    //process car rental
                    String checkin = null;
                    String checkout = null;
                    String numDays = null;
                    String confirmation = null;
                    String hotel = null;
                    String numRooms = null;
                    String numGuests = null;
                    String hotelDetails = "";
                    ArrayList <String> hotelInfoTokens = new ArrayList<String>();
                    String hotelAddr = null;

                    int count = 0;

                  if (!s.contains("CHECK OUT")) {
                    //this is split on multiple lines
                    //we assemble the line
                    StringBuilder sb = new StringBuilder();
                    sb.append(s);
                    for (int counter = 0; counter < 4; counter++) {
                      sb.append(" ");
                      sb.append(data[++i]);
                      if(sb.toString().contains("NIGHT")) {
                        break;
                      }
                    }
                    s = sb.toString();
                  }

                    Matcher m = dayDatePattern.matcher(s);
                    while (m.find()) {
                        if (count == 0)
                            checkin = m.group();
                        else if (count == 1) {
                            checkout = m.group();
                            break;
                        }
                        count++;
                    }

                    if (checkout != null) {
                        numDays = s.substring(s.indexOf(checkout) + checkout.length()).trim();
                    }

                    checkin = SabreExtractor.appendYear(checkin, startMonth, startYear, endYear);

                    checkout = SabreExtractor.appendYear(checkout, startMonth, startYear, endYear);


                    if (checkin != null && checkout != null) {

                        i++;
                        if (i < data.length) {
                            while (true) {
                                String hotelData = data[i];
                                hotelInfoTokens.add(hotelData);


                                if ((i + 1) >= data.length) {
                                    break;
                                } else {
                                    String nextLine = data[i + 1];
                                    if (nextLine.contains("OTHER:") || nextLine.contains("Confirmation:") || nextLine.contains("Room") || nextLine.contains("DEPARTURE:") || nextLine.contains("PICK UP:") || nextLine.contains("CHECK IN:")) {
                                        break;
                                    } else {
                                        i++;
                                    }
                                }
                            }

                            //assume the last 2 tokens is the hotel adddress
                            StringBuffer hotelNameB = new StringBuffer();
                            if (hotelInfoTokens.size() > 3) {
                                int tokenCount = 0;
                                StringBuffer hotelAddrB = new StringBuffer();

                                for (String token:hotelInfoTokens) {
                                    if (tokenCount < hotelInfoTokens.size() - 2) {
                                        hotelNameB.append(token);
                                        hotelNameB.append(" ");
                                    } else {
                                       hotelAddrB.append(token);
                                       hotelAddrB.append("\n");
                                    }
                                    tokenCount++;
                                }
                                if (hotelAddrB.length() > 0) {
                                    hotelAddr = hotelAddrB.toString();
                                }

                            } else {
                                for (String token:hotelInfoTokens) {
                                    hotelNameB.append(token);
                                    hotelNameB.append(" ");
                                }
                            }
                            hotel = hotelNameB.toString();
                        }

                        i++;
                        if (i < data.length) {
                            while (true) {
                                String hotelData = data[i];

                                if (hotelData.contains("Room(s):")) {
                                    numRooms = hotelData;
                                } else if (hotelData.toLowerCase().contains("room details:")) {
                                    i++;
                                    if (i < data.length) {
                                        while (true) {
                                            String hotelNote = data[i];
                                            if (hotelDetails.trim().length() == 0) {
                                                hotelDetails = "Room Details:\n";
                                            }
                                            if (!hotelDetails.contains(hotelNote) && !miscStrings.contains(hotelNote)) {
                                              if (hotelNote.contains(":")) {
                                                hotelDetails = hotelDetails + "\n" + hotelNote;
                                              }
                                              else {
                                                hotelDetails = hotelDetails + " " + hotelNote;
                                              }
                                            }

                                            if ((i + 1) >= data.length) {
                                                break;
                                            } else {
                                                String nextLine = data[i + 1];
                                                if (nextLine.contains("OTHER:") || hotel.contains(nextLine) || nextLine.contains("DEPARTURE:") || nextLine.contains("PICK UP:") || nextLine.contains("CHECK IN:")) {

                                                    break;
                                                } else {
                                                    i++;
                                                }
                                            }
                                        }
                                    }
                                }

                                if ((i + 1) >= data.length) {
                                    break;
                                } else {
                                    String nextLine = data[i + 1];
                                    if (nextLine.contains("OTHER:") || nextLine.contains("DEPARTURE:") || nextLine.contains("PARTIDA:") || nextLine.contains("PICK UP:") || nextLine.contains("CHECK IN:")) {
                                        break;
                                    } else {
                                        i++;
                                    }
                                }
                            }
                        }

                        //for some reason the phone/fax/confirmation is not found in the natural order
                        //search for them in the sorted version of the text extracted from the PDF  (look for phone by regex pattern)
                        String phone = null;
                        String fax = null;
                        for (String token : controlData) {
                            if (token.contains(title)) {
                                String[] hotelToken = token.split("\n");

                                for (int ti = 0; ti < hotelToken.length; ti++) {
                                    String hotelInfo = hotelToken[ti];
                                    if (hotelInfo.contains("Fax ")) { //look for something that matches a phone pattern 9-9999-888888
                                        Matcher m1 = phonePattern.matcher(hotelInfo);
                                        if (m1.find()) {
                                            fax = m1.group();
                                        } else {
                                            m1 = phonePattern1.matcher(hotelInfo);
                                            if (m1.find()) {
                                                fax = m1.group();
                                            }

                                        }

                                        if (fax == null) {
                                            //try the next line
                                            if (ti + 1 < hotelToken.length) {
                                                Matcher m2 = phonePattern.matcher(hotelToken[ti + 1]);
                                                if (m2.find()) {
                                                    fax = m2.group();
                                                    ti++;
                                                } else {
                                                    m2 = phonePattern1.matcher(hotelToken[ti + 1]);
                                                    if (m2.find()) {
                                                        fax = m2.group();
                                                        ti++;
                                                    }
                                                }
                                            }
                                        }
                                    } else if (hotelInfo.contains("Phone ")) { //look for something that matches a phone pattern 9-9999-888888
                                        Matcher m1 = phonePattern.matcher(hotelInfo);
                                        if (m1.find()) {
                                            phone = m1.group();
                                        } else {
                                            m1 = phonePattern1.matcher(hotelInfo);
                                            if (m1.find()) {
                                                phone = m1.group();
                                            }
                                        }

                                        if (phone == null) {
                                            //try the next line
                                            if (ti + 1 < hotelToken.length) {
                                                Matcher m2 = phonePattern.matcher(hotelToken[ti + 1]);
                                                if (m2.find()) {
                                                    phone = m2.group();
                                                    ti++;
                                                } else {
                                                    m2 = phonePattern1.matcher(hotelToken[ti + 1]);
                                                    if (m2.find()) {
                                                        phone = m2.group();
                                                        ti++;
                                                    }
                                                }
                                            }
                                        }

                                    } else if (hotelInfo.contains("Confirmation:")) {
                                        if (ti + 1 < hotelToken.length) {
                                            confirmation = hotelToken[ti + 1];
                                            if (confirmation != null && confirmation.trim().indexOf(" ") > 1) {
                                                confirmation = confirmation.substring(0, confirmation.trim().indexOf(" "));
                                            }
                                            ti++;
                                        }
                                    }
                                }
                                break;


                            }
                        }

                        HotelVO hotelVO = new HotelVO();
                        hotelVO.setHotelName(hotel);
                        hotelVO.setCheckin(new Timestamp(du.parseDate(checkin + " 12:00am", dateTimeFormat).getTime()));
                        hotelVO.setCheckout(new Timestamp(du.parseDate(checkout + " 12:00am", dateTimeFormat).getTime()));
                        hotelVO.setConfirmation(confirmation);


                        StringBuffer note = new StringBuffer();
                        if (hotelAddr != null) {
                            note.append("Address: \n");
                            note.append(hotelAddr);
                            note.append("\n");
                        }
                        if (phone != null) {
                            note.append("Phone: ");
                            note.append(phone);
                            note.append("\n");

                        }
                        if (fax != null) {
                            note.append("Fax: ");
                            note.append(fax);
                            note.append("\n");

                        }
                        note.append("Num of days: ");
                        note.append(numDays);
                        note.append("\n");

                        if (hotelDetails != null && hotelDetails.trim().length() > 1) {
                            note.append("\n");
                            note.append(hotelDetails);
                            note.append("\n");
                        }
                        hotelVO.setNote(note.toString());

                        trip.addHotel(hotelVO);
                    }

                }else if (s.contains("DEPARTURE:") || s.contains("PARTIDA:")) {


                  //process flight info
                  StringBuilder departPort = new StringBuilder();
                  StringBuilder arrivePort = new StringBuilder();
                  String departTime = null;
                  String arriveTime = null;
                  String cabin = null;
                  String confirmation = null;
                  StringBuilder note = new StringBuilder();
                  StringBuilder name = new StringBuilder();
                  String status = null;


                  String departDate = null;
                  String arriveDate = null;

                  //process all flight info
                  Matcher m = dayDatePattern.matcher(s);
                  if (m.find()) {
                    departDate = m.group();
                    departDate = SabreExtractor.appendYear(departDate, startMonth, startYear, endYear);

                  }

                  //look for arrival date
                  if (m.find()) {
                    arriveDate = m.group();
                    arriveDate = SabreExtractor.appendYear(arriveDate, startMonth, startYear, endYear);

                  }
                  i++;

                  while (true) {
                    String lineData = data[i];
                    if (lineData != null && lineData.trim().length() > 0) {
                      name.append(lineData);name.append(" ");
                    }
                    if ((i + 1) >= data.length) {
                      break;
                    } else {
                      String nextLine = data[i + 1];
                      if (nextLine.contains("Confirmation:") || nextLine.contains("Status:")) {
                        break;
                      } else {
                        i++;
                      }
                    }
                  }

                  while (true) {
                    String lineData = data[i];
                    if ((i + 1) >= data.length) {
                      break;
                    }
                    if (lineData.contains("Confirmation")){
                      confirmation = data [++i];
                    } else if (lineData.contains("Status")) {
                      status = data [++i];
                    } else if (StringUtils.isAllUpperCase(lineData) && lineData.trim().length() == 3 && departPort.toString().trim().length() == 0) {
                      while (true) {
                        if ((i + 1) >= data.length) {
                          break;
                        }
                        i++;
                        String l = data[i];
                        departPort.append(l);departPort.append(" ");
                        if ((i + 1) >= data.length) {
                          break;
                        }
                        String nl = data [i + 1];
                        if ((StringUtils.isAllUpperCase(nl) && nl.trim().length() == 3) || nl.contains("CABIN") || nl.contains("Remarks") || nl.contains("Departing") || nl.contains("Arriving")) {
                          break;
                        }
                      }

                    } else if (StringUtils.isAllUpperCase(lineData) && lineData.trim().length() == 3 && arrivePort.toString().trim().length() == 0) {
                      while (true) {
                        if ((i + 1) >= data.length) {
                          break;
                        }
                        i++;
                        String l = data[i];
                        arrivePort.append(l);arrivePort.append(" ");
                        if ((i + 1) >= data.length) {
                          break;
                        }
                        String nl = data [i + 1];
                        if ((StringUtils.isAllUpperCase(nl) && nl.trim().length() == 3) || nl.contains("CABIN") || nl.contains("Remarks") || nl.contains("Departing") || nl.contains("Arriving")) {
                          break;
                        }
                      }
                    } else if (lineData.contains("CABIN") && cabin == null) {
                      cabin = data [i];
                      if (cabin != null && cabin.contains("CABIN")) {
                        cabin = cabin.replaceAll("CABIN", "").trim();
                      }
                    } else if (lineData.contains("Remarks")) {
                      while (true) {
                        if ((i + 1) >= data.length) {
                          break;
                        }
                        i++;
                        String l = data[i];
                        note.append(l);note.append("\n");
                        if ((i + 1) >= data.length) {
                          break;
                        }
                        String nl = data [i + 1];
                        if ((StringUtils.isAllUpperCase(nl) && nl.trim().length() == 3) ||  nl.contains("Departing") || nl.contains("Arriving")) {
                          break;
                        }
                      }
                    } else if (lineData.contains("Departing At:")) {
                      departTime = data[++i];
                    } else if (lineData.contains("Arriving At:")) {
                      arriveTime = data[++i];
                    }

                    if ((i + 1) >= data.length) {
                      break;
                    } else {
                      String nextLine = data[i + 1];
                      if (nextLine.contains("OTHER:") || nextLine.contains("DEPARTURE:") || nextLine.contains("PARTIDA:") || nextLine.contains("PICK UP:") || nextLine.contains("CHECK IN:")) {
                        break;
                      } else {
                        i++;
                      }
                    }
                  }




                  try {
                    CruiseVO cruiseVO = new CruiseVO();
                    cruiseVO.setName(name.toString().trim());
                    cruiseVO.setCabinNumber(cabin);
                    cruiseVO.setNote(note.toString());
                    cruiseVO.setPortArrive(arrivePort.toString().trim());
                    cruiseVO.setPortDepart(departPort.toString().trim());
                    cruiseVO.setRecordLocator(confirmation);

                    if (lang == LANG.ES && departDate != null) {
                      String[] strings = departDate.split(" ");
                      StringBuilder sb = new StringBuilder();
                      for (String token: strings){
                        if (daysES.contains(token.trim().toUpperCase())) {
                          sb.append(days.get(daysES.indexOf(token)));
                          sb.append(" ");
                        } else if (monthsES.contains(token.trim().toUpperCase())) {
                          sb.append(months.get(monthsES.indexOf(token)));
                          sb.append(" ");
                        } else {
                          sb.append(token);sb.append(" ");
                        }
                      }
                      departDate = sb.toString().trim();
                    }

                    if (departTime != null) {
                      if (departTime.toLowerCase().contains("am")) {
                        departTime = departTime.substring(0, departTime.indexOf("am") + 2);
                      } else if (departTime.toLowerCase().contains("pm")) {
                        departTime = departTime.substring(0, departTime.indexOf("pm") + 2);
                      }
                      cruiseVO.setTimestampDepart(new Timestamp(du.parseDate(departDate + " " + departTime, dateTimeFormat).getTime()));
                    }
                    if (arriveTime != null) {
                      String time = arriveTime;
                      if (arriveTime.toLowerCase().contains("am")) {
                        time = arriveTime.substring(0, arriveTime.indexOf("am") + 2);
                      } else if (arriveTime.toLowerCase().contains("pm")) {
                        time = arriveTime.substring(0, arriveTime.indexOf("pm") + 2);
                      }

                      if (time != null) {

                        long arrivalTime = 0;
                        if (arriveDate != null) {
                          if (lang == LANG.ES) {
                            String[] strings = arriveDate.split(" ");
                            StringBuilder sb = new StringBuilder();
                            for (String token: strings){
                              if (daysES.contains(token.trim().toUpperCase())) {
                                sb.append(days.get(daysES.indexOf(token)));
                                sb.append(" ");
                              } else if (monthsES.contains(token.trim().toUpperCase())) {
                                sb.append(months.get(monthsES.indexOf(token)));
                                sb.append(" ");
                              } else {
                                sb.append(token);sb.append(" ");
                              }
                            }
                            arriveDate = sb.toString().trim();
                          }
                          arrivalTime = du.parseDate(arriveDate + " " + time, dateTimeFormat).getTime();
                        } else {
                          arrivalTime = du.parseDate(departDate + " " + time, dateTimeFormat).getTime();

                        }

                        cruiseVO.setTimestampArrive(new Timestamp(arrivalTime));
                      }
                    }



                    if (cruiseVO.getName() != null && cruiseVO.getTimestampDepart() != null ) {
                      trip.addCruise(cruiseVO);
                    }
                  } catch (Exception e) {
                    e.printStackTrace();
                  }

                } else {
                  miscStrings.add(s);
                }
            }




			/*
            List<PDPage> pages = document.getDocumentCatalog().getAllPages();
            PDPage singlePage = pages.get(0);
                BufferedImage buffImage = singlePage.convertToImage(BufferedImage.TYPE_INT_RGB, 96);;
                ImageIO.write(buffImage, "png", new File("a.png"));
            */

            document.close();
          trip.setImportSrc(BookingSrc.ImportSrc.SABRE_PDF);
          trip.setImportSrcId(reservationCode);
          trip.setImportTs(System.currentTimeMillis());

            return trip;
        } finally {
            if (input != null) {
                input.close();
            }
        }


    }


    public static String appendYear(String dateStr, int startMonth, int startYear, int endYear) {
        //check what year shoud be appended to the date - the year could potentially roll over e.g. Nov 2013 to Feb 2014

        if (startYear > 0 && endYear > 0) {
            if (startYear != endYear && dateStr != null && dateStr.length() > 1) {
                //figure out if this is before of after the year change
              String monthStr = dateStr.substring(dateStr.length() - 3);
              int month = -1;
              month = months.indexOf(monthStr);

              if (monthsES.contains(monthStr) && month == -1)
                 month = monthsES.indexOf(monthStr);


                if (month >= startMonth) {
                    dateStr = dateStr + " " + String.valueOf(startYear);
                } else
                    dateStr = dateStr + " " + String.valueOf(endYear);

            } else if (dateStr != null) {
                dateStr = dateStr + " " + String.valueOf(startYear);
            }
        }

        return dateStr;
    }

    @Override
    public ArrayList<ParseError> getParseErrors()
    {
        return null;
    }

  @Override
  public void postProcessingImpl(String tripId, String userId) {
  }

  public String parsePassenger (String passengerInfo, String title, PassengerVO passenger) {
    try {
      if (title != null && passengerInfo != null && passengerInfo.length() > 0) {
        StringBuilder sb = new StringBuilder();
        String[] titles = title.split(":");

        //if 6 - no frequen flyer if 7 - frequent flyer
        //extract name - name is at the beginning and is all upper case
        int i = 0;
        for (char c : passengerInfo.toCharArray()) {
          if (Character.isLowerCase(c) || Character.isDigit(c)) {
            if (Character.isUpperCase(passengerInfo.charAt(i - 1))) {
              i--; //move back one
            }
            break;
          }
          i++;
        }

        if ( i > 0 && i < passengerInfo.length()) {
          passenger.setFullName(passengerInfo.substring(0, i).trim());
        }

        String s = passengerInfo.substring(i).trim();

        if (Character.isDigit(s.charAt(0))) { //seat number
          i = 0;
          boolean found = false;
          for (char c: s.toCharArray()) {
            if (Character.isWhitespace(c) || !Character.isLetterOrDigit(c)) {
              found = true;
              break;
            }
            i++;
          }
          if (found) {
            passenger.setSeat(s.substring(0, i).trim());

            s = s.substring(i).trim();
            //try to find the class - should be between 2 occurences of word Confirmed
              String[] tokens = s.split("Confirmed");
              if (tokens.length == 3) {
                passenger.setSeatClass(tokens[1]);
            }
          }
        } else if (passengerInfo.contains("Check-In Required") || passengerInfo.toLowerCase().contains("requerida")) {
          passenger.setSeat("Check-In Required");
          //check for class now - typically between here and next occurence of confirmed
          int indexSeat = passengerInfo.indexOf("Check-In Required") + 17;
          int indexStatus = passengerInfo.indexOf("Confirmed");
          if (indexSeat < passengerInfo.length() && indexStatus > indexSeat) {
            passenger.setSeatClass(passengerInfo.substring(indexSeat, indexStatus));
          }
        } else {
          passenger.setSeat("N/A");
        }

        StringBuilder etickets = new StringBuilder();

        if (title.contains("eTicket") || title.contains("Recibo")) {
          String[] tokens = s.split(" ");
          for (String token : tokens) {
            //look for the eticket number a 13 digit string
            if (token.length() > 8 && token.length() >= 13 && StringUtils.isNumeric(token.trim().substring(0,13))) {
              if (etickets.toString().length() > 0) {
                etickets.append(" ");
              }
              etickets.append(token.trim());
            }
          }
        }

        String eticket = etickets.toString();
        if (etickets.toString().length() > 0) {
          passenger.seteTicketNumber(eticket);

          //try to find the meal last piece of text after the eticket
          if (title.contains("Meals:") && s.length() > (s.indexOf(eticket) + eticket.length())) {
            passenger.setMeal(s.substring(s.indexOf(eticket) + eticket.length()));
          }
        }

        //try to find the frequent flyer format GN11111 / Air line - look for a pattern what starts with UPPER CASE and contain numbers.
        if (titles.length == 7) {
          StringBuilder frequent = new StringBuilder();

          //typically the feq flyer is between the status and the eticket - the freq flyer contains only digit or uppercase - we are scanning backwards until the pattern breaks
          if (eticket != null && s.contains(eticket)) {
            int endIndex = s.indexOf(eticket) - 1;
            int startIndex = -1;
            for (int k = endIndex; k > 0; k-- ) {
              if (Character.isLowerCase(s.charAt(k))) {
                startIndex = k + 1;
                break;
              }

            }

            if (startIndex > 0 && startIndex < endIndex) {
              frequent.append(s.substring(startIndex, endIndex).trim());
            }
          }

          //if we can't find the frequent - try to look for a mix of digit and upper case letter like 50E2T06 /AMERICAN
          if (frequent.length() == 0) {

            String[] tokens = s.split(" ");
            for (String token : tokens) {
              boolean frequentFound = false;
              boolean foundUpper = false;
              boolean foundDigit = false;
              if (frequent.length() == 0 && token.length() > 5) {
                for (int k = 0; k < token.length(); k++) {
                  if (Character.isUpperCase(token.charAt(k))) {
                    foundUpper = true;
                  }
                  else if (Character.isDigit(token.charAt(k))) {
                    foundDigit = true;
                  }
                }
                if (foundUpper && foundDigit && token.length() > 5) {
                  frequent.append(token);
                  frequentFound = true;
                }
              }
              else if (frequentFound) {
                if (token.length() > 2 && Character.isDigit(token.charAt(0)) && Character.isDigit(token.charAt(1))) {
                  frequentFound = false;
                  break;
                }
                else {
                  frequent.append(token);
                  frequent.append(" ");
                }
              }
            }
          }
          if (frequent.length() > 0) {
            passenger.setFrequentFlyer(frequent.toString().trim());
          }
        }
        return sb.toString();
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
    return passengerInfo;

  }

  public static Date parseDate (String date, LANG lang, DateUtils du) {
    try {
      if (lang == LANG.ES) {
        for (String format : dateTimeFormat) {
          try {
            Locale locale = new Locale("es_MX");
            SimpleDateFormat dateFormatter = new SimpleDateFormat(format, locale);
            return dateFormatter.parse(date);

          } catch (ParseException pe) {
            Log.err("Failure while parsing date for Sabre");
            pe.printStackTrace();
          }

        }
      }
      else {

        return du.parseDate(date, dateTimeFormat);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return DateTime.now().toDate();

  }

  public static void main(String[] args) {
    try {
      String dir = "/home/twong/Dropbox/UMapped_Tech/Clients Documents/sabre/TripCase_Cameron Dawson.pdf";// "/users/twong/Dropbox/UMapped_Tech/Clients Documents/Travel 2 invoices/8.pdf";//"/volumes/data2/Downloads/travel2_087204.pdf";//"/volumes/data2/Downloads/t2-prop1.pdf";//"/users/twong/Dropbox/UMapped_Share/Integration Samples/Travel2/US518303_itn.pdf"; //"/users/twong/Dropbox/UMapped_Share/Companies/Travel2/reumappedtravel2nextsteps/196402_itn.pdf";
      // String dir = "/users/twong/downloads/travel2.pdf";
      File file = new File(dir);
      InputStream ins = new FileInputStream(file);

      //System.out.println(getPDFContentAsString(ins));

      SabreExtractor p = new SabreExtractor();
      p.initImpl(null);
      TripVO t = p.extractData(ins);
      //p.postProcessingImpl("1", "2");
      System.out.println(t);

    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }

}
