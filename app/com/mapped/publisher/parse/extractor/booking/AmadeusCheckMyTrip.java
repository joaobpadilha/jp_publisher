package com.mapped.publisher.parse.extractor.booking;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.joda.time.DateTime;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by twong on 2014-10-08.
 */
public class AmadeusCheckMyTrip extends BookingExtractor  {

  private static String[] dateTimeFormat = {"dd MMM yyyy", "dd MMM yyyy ", "E dd MMM yyyy h:mma",  "E dd MMM yyyy", "E dd MMM yyyy ", "E dd MMMMM yyyy HH:mm" , "dd MMM yyyy HH:mm a", "E dd MMM yyyy HH:mm" , "dd MMMMM yyyy HH:mm", "E, MMMMM dd, yyyy HH:mm", "E, MMMMM dd, yyyy", "E, MMMMM dd, yyyy hh:mm a", "EEE dd MMM yyyy h:mm a", "EEE dd MMM yyyy 0h:mm a"} ;

  private static Pattern dayDatePattern = Pattern.compile("(MONDAY|TUESDAY|WEDNESDAY|THURSDAY|FRIDAY|SATURDAY|SUNDAY).*.(JANUARY|FEBRUARY|MARCH|APRIL|MAY|JUNE|JULY|AUGUST|SEPTEMBER|OCTOBER|NOVEMBER|DECEMBER).*.[0-9]?[0-9].*.20[0-9]?[0-9]", Pattern.CASE_INSENSITIVE);
  private static Pattern dayDatePattern1 = Pattern.compile("(MONDAY|TUESDAY|WEDNESDAY|THURSDAY|FRIDAY|SATURDAY|SUNDAY).[0-9]?[0-9].(JANUARY|FEBRUARY|MARCH|APRIL|MAY|JUNE|JULY|AUGUST|SEPTEMBER|OCTOBER|NOVEMBER|DECEMBER).20[0-9]?[0-9]", Pattern.CASE_INSENSITIVE);

  private static Pattern airportPattern = Pattern.compile("[A-Z]{3}");
  private static Pattern flightNumberPattern = Pattern.compile("[A-Z]{2,3}.[0-9]{1,6}");
  private static Pattern flightBreakPattern = Pattern.compile("[A-Z- ]{1,100}.to.[A-Z- ]{1,100}"); //section break e.g.CHIANG MAI to DENPASAR-BALI
  private static Pattern timestampPattern = Pattern.compile("[A-Za-z]{3}\\s[0-9]{2}\\s[A-Za-z]{3}\\s[0-9]{4}.*.[0-9]{2}:[0-9]{2}",Pattern.CASE_INSENSITIVE); //section break e.g.CHIANG MAI to DENPASAR-BALI

  private DateUtils du = new DateUtils();
  protected void initImpl(Map<String, String> params) {

  }


  protected TripVO extractDataImpl(InputStream input) throws Exception {
    try {
      TripVO trip = new TripVO();
      StringBuffer buffer = new StringBuffer();
      PDDocument document = PDDocument.load(input);
      String reservationCode = "";


      PDFTextStripper textStripper = new PDFTextStripper();
      textStripper.setSortByPosition(true);

      String s1 = textStripper.getText(document);
      String[] data = s1.split("\n");

      String itineraryMarker = "Booking reservation number:";


      for (int i = 0; i < data.length; i++) {
        String s = data[i];


        boolean foundFlight = false;
        String departDate = null;
        if (s.contains(itineraryMarker)) {
          String ref1 = s.substring(s.indexOf(itineraryMarker) + itineraryMarker.length());
          if (ref1 != null)
            reservationCode = ref1.trim();
        }
        else if (s.contains ("duration") && (i + 1) < data.length && data[i + 1].startsWith("Dep:")) {
          //process flight Info
          departDate = getDepartDate(s);
          // process flight info
          if (departDate != null) {

            //process flight info
            String duration = null;
            String departAirport = null;
            String arriveAirport = null;
            String departTime = null;
            String arriveTime = null;
            String departTerminal = null;
            String arriveTerminal = null;
            String airline = null;
            String flightNum = null;
            String bookingCode = null;
            String departCity = null;
            String departCountry = null;
            String arriveCity = null;
            String arriveCountry = null;
            StringBuilder notes = new StringBuilder();

            List<String> passengers = new ArrayList<String>(); //this is a list of passengers

            String arriveDate = departDate;

            if (s.contains("duration")) {
              duration = s.substring(s.indexOf("duration") + 8).trim();
              if (duration != null)
                notes.append("Duration: ");notes.append(duration);notes.append("\n");
            }

            i++;

            int depLine = -1;
            int arrLine = -1;

            while (true) {

              String flightInfo = data[i];
              if (flightInfo.contains("Dep:")) {
                depLine = i;
                if (flightInfo.contains(" AM ") || flightInfo.contains(" PM ")) {
                  String[] tokens = flightInfo.split(" ");
                  departTime = tokens[1] + " " + tokens[2];
                } else {
                  String[] tokens = flightInfo.split(" ");
                  departTime = tokens[1];
                }


                String t = flightInfo.substring(flightInfo.indexOf(departTime) + departTime.length());
                if (t != null && t.contains(",")) {
                  departCity = t.substring(0, t.indexOf(","));
                  if (t.contains("|")) {
                    departCountry = t.substring(t.indexOf(",") + 1, t.indexOf("|"));
                  }
                }

                if (t.contains("Terminal")) {
                  departTerminal = t.substring(t.indexOf("Terminal"));
                }

                Matcher airportCodeMatch = airportPattern.matcher(t);
                while (airportCodeMatch.find()) {
                  departAirport = airportCodeMatch.group();
                }
              }
              else if (depLine > 0 && i == (depLine + 1) && !flightInfo.startsWith("Arr:")) {
                //continue processing depart info
                if (departTerminal == null) {
                  if (flightInfo.contains("Terminal")) {
                    departTerminal = flightInfo.substring(flightInfo.indexOf("Terminal"));
                  }
                }
                if (departAirport == null) {
                  Matcher airportCodeMatch = airportPattern.matcher(flightInfo);
                  while (airportCodeMatch.find()) {
                    departAirport = airportCodeMatch.group();
                  }
                }

              }
              else if (flightInfo.contains("Arr:")) {
                arrLine = i;
                if (flightInfo.contains(" AM ") || flightInfo.contains(" PM ")) {
                  String[] tokens = flightInfo.split(" ");
                  arriveTime = tokens[1] + " " + tokens[2];
                } else {
                  String[] tokens = flightInfo.split(" ");
                  arriveTime = tokens[1];
                }

                String t = flightInfo.substring(flightInfo.indexOf(arriveTime) + arriveTime.length());
                //get arrival date
                if (t.contains("(+")) {
                  String dayStr = t.substring(t.indexOf("(+") + 2, t.indexOf(" day")).trim();
                  if (t.length() > 6)
                    t = t.substring(t.indexOf(" day")+6).trim();
                  try {
                    int day = Integer.parseInt(dayStr);
                    arriveDate =getArrivalDate(departDate, day);
                  } catch (Exception e) {
                    e.printStackTrace();
                  }
                } else if (t.contains("(-")) {
                  String dayStr = t.substring(t.indexOf("(-") + 2, t.indexOf(" day")).trim();
                  if (t.length() > 6)
                    t = t.substring(t.indexOf(" day")+6).trim();

                  try {
                    int day = Integer.parseInt(dayStr) * -1 ;
                    arriveDate =getArrivalDate(departDate, day);
                  } catch (Exception e) {
                    e.printStackTrace();
                  }
                } else {
                  arriveDate = departDate;
                }

                if (t != null && t.contains(",")) {
                  arriveCity = t.substring(0, t.indexOf(","));
                  if (t.contains("|")) {
                    arriveCountry = t.substring(t.indexOf(",") + 1, t.indexOf("|"));
                  }
                }

                if (t.contains("Terminal")) {
                  arriveTerminal = t.substring(t.indexOf("Terminal"));
                }

                Matcher airportCodeMatch = airportPattern.matcher(t);
                while (airportCodeMatch.find()) {
                  arriveAirport = airportCodeMatch.group();
                }


              }  else if (arrLine > 0 && i == (arrLine + 1) && !flightInfo.startsWith("Confirmation Number:") && !flightInfo.startsWith("Flight Number:") && !flightInfo.contains(":") ) {
                //continue processing depart info
                if (arriveTerminal == null) {
                  if (flightInfo.contains("Terminal")) {
                    arriveTerminal = flightInfo.substring(flightInfo.indexOf("Terminal"));
                  }
                }
                if (arriveAirport == null) {
                  Matcher airportCodeMatch = airportPattern.matcher(flightInfo);
                  while (airportCodeMatch.find()) {
                    arriveAirport = airportCodeMatch.group();
                  }
                }
              }
              else if (flightInfo.startsWith("Confirmation Number:")) {
                bookingCode = flightInfo.substring(21).trim();
              }
              else if (flightInfo.startsWith("Flight Number:")) {
                Matcher flightNumMatcher = flightNumberPattern.matcher(flightInfo);
                while (flightNumMatcher.find()) {
                  flightNum = flightNumMatcher.group();
                }
              }
              else if (flightInfo.trim().length() > 0) {
                notes.append(flightInfo);
                notes.append("\n");
              }


              if (i == data.length || (i + 1) == data.length) {
                break;
              }
              else {
                int j = i + 1;
                String nextLine = data[j];
                if (nextLine != null && nextLine.equals(flightInfo)) {
                  if (!(i == data.length || (i + 1) == data.length)) {
                    i++;
                    nextLine = data[i + 1];
                  }
                }

                String nextLineLower = nextLine.toLowerCase();
                String nextDepartDate = getDepartDate(nextLine);

                Matcher flightBreak = flightBreakPattern.matcher(nextLine);
                if (flightBreak.find()) {
                  break;
                }
                else if (nextDepartDate != null || nextLine.startsWith("Dep:") || nextLine.startsWith("GENERAL REMARKS") || nextLine.startsWith("CONDITIONS OF CONTRACT") || nextLineLower
                    .startsWith("tour information") || nextLineLower.startsWith("hotel information") || nextLineLower.startsWith(
                    "tour information") || nextLineLower.startsWith("rail information") || nextLineLower.startsWith(
                    "transfer information") || nextLineLower.startsWith("car") || nextLineLower.startsWith("hotel") ||  (nextLineLower.contains("from ") && nextLineLower.contains(" night")) ) {
                  break;
                }
                else
                  i++;
              }


            }

            try {
              FlightVO flightVO = new FlightVO();
              flightVO.setArrivalAirport(new AirportVO());
              flightVO.setDepartureAirport(new AirportVO());
              flightVO.setArrivalTime(getTimestamp(arriveDate, arriveTime));
              flightVO.setDepatureTime(getTimestamp(departDate, departTime));
              flightVO.setReservationNumber(bookingCode);
              if (flightNum != null && flightNum.length() > 3) {
                flightVO.setCode(flightNum.substring(0, 2));
                flightVO.setNumber(flightNum.substring(2));
              }
              else
                flightVO.setNumber(flightNum);

              flightVO.setName(airline);
              if  (arriveAirport != null && arriveAirport.trim().length() == 3) {
                flightVO.getArrivalAirport().setCode(arriveAirport);
              }
              flightVO.getArrivalAirport().setName(arriveAirport);
              flightVO.getArrivalAirport().setTerminal(arriveTerminal);
              flightVO.getArrivalAirport().setCity(arriveCity);
              flightVO.getArrivalAirport().setCountry(arriveCountry);
              if  (departAirport != null && departAirport.trim().length() == 3) {
                flightVO.getDepartureAirport().setCode(departAirport);
              }
              flightVO.getDepartureAirport().setName(departAirport);
              flightVO.getDepartureAirport().setTerminal(departTerminal);
              flightVO.getDepartureAirport().setCity(departCity);
              flightVO.getDepartureAirport().setCountry(departCountry);
              flightVO.setNotes(notes.toString());

              if (flightVO.getDepartureAirport() != null && flightVO.getDepatureTime() != null && flightVO.getCode() != null) {
                trip.addFlight(flightVO);
              }

            }
            catch (Exception e) {
              e.printStackTrace();
            }
          }

        } else if (s.contains("FROM") && s.contains("NIGHT")) { //hotels

          HotelVO hotelVO = new HotelVO();
          StringBuilder sb = new StringBuilder();
          i++;
          while (true) {
            String hotelInfo = data[i];
            String sub = "";
            if (hotelInfo.contains(": ")) {
              sub = hotelInfo.substring(hotelInfo.lastIndexOf(": ") + 2).trim();
            }
            if (hotelInfo.contains("Hotel:")) {
              hotelVO.setHotelName(sub);
            }
            else if (hotelInfo.contains("Address:")) {
              sb.append(sub);
              sb.append("\n");
            }
            else if (hotelInfo.contains("Reservation number:")) {
              hotelVO.setConfirmation(sub);
            }
            else if (hotelInfo.contains("Telephone:")) {
              sb.append("Telephone: ");
              sb.append(sub);
              sb.append("\n");
            }
            else if (hotelInfo.contains("Fax:")) {
              sb.append("Fax: ");
              sb.append(sub);
              sb.append("\n");
            }
            else if (hotelInfo.contains("Email:")) {
              sb.append("Email: ");
              sb.append(sub);
              sb.append("\n");
            }
            else if (hotelInfo.contains("Check-in:")) {
              hotelVO.setCheckin(getTimestamp(sub, "03:00 PM"));
            }
            else if (hotelInfo.contains("Check-out:")) {
              hotelVO.setCheckout(getTimestamp(sub, "12:00 PM"));
            }
            else if (hotelInfo.contains("Total Stay:") || hotelInfo.contains("Rate:") || hotelInfo.contains(
                "Total Price:") || hotelInfo.contains("Reservation Name:") || hotelInfo.contains("Guarantee:")) {
              //do nothing
            }
            else {
              if ( (hotelVO.getHotelName() != null && !hotelInfo.equals(hotelVO.getHotelName()))  && !sb.toString().contains(hotelInfo)) {
                sb.append(hotelInfo);
                sb.append("\n");
              }
            }

            //break out
            if (i == data.length || (i + 1) == data.length || hotelInfo.startsWith("Hotel notes:")) {
              break;
            }
            else {
              int j = i + 1;
              String nextLine = data[j];
              if (nextLine != null && nextLine.equals(hotelInfo)) {
                if (!(i == data.length || (i + 1) == data.length)) {
                  i++;
                  nextLine = data[i + 1];
                }
              }

              String nextLineLower = nextLine.toLowerCase();

              if (nextLine.startsWith("Dep:") || nextLine.startsWith("GENERAL REMARKS") || nextLine.startsWith("CONDITIONS OF CONTRACT") ||  nextLineLower
                  .startsWith("tour information") || nextLineLower.startsWith("hotel information") || nextLineLower.startsWith(
                  "rail information") || nextLineLower.startsWith("transfer information") || nextLineLower.startsWith("car") ||
                  (nextLineLower.contains("from ") && nextLineLower.contains(" night"))|| (hotelVO.getHotelName() != null && nextLine.equals(hotelVO.getHotelName()))) {
                break;
              }
              else
                i++;
            }
          }

          if (hotelVO.isValid()) {
            hotelVO.setNote(sb.toString());
            boolean insert = true;
            if (trip.getHotels() != null) {
              for (HotelVO vo : trip.getHotels()) {
                if (vo.getHotelName().equals(hotelVO.getHotelName()) && vo.getCheckin().equals(hotelVO.getCheckin())) {
                  insert = false;
                }
              }
            }
            if (insert) {
              trip.addHotel(hotelVO);
            }
          }


        }  else if (s.contains("Car Rental:") ) { //car rental

          TransportVO carVO = new TransportVO();
          carVO.setBookingType(ReservationType.CAR_RENTAL);
          StringBuilder sb = new StringBuilder();
          boolean pickup = false;
          boolean dropoff = false;

          while (true) {
            String carInfo = data[i];
            String sub = "";
            if (carInfo.contains(": ")) {
              sub = carInfo.substring(carInfo.lastIndexOf(": ") + 2).trim();
            }
            if (carInfo.contains("Car Rental:")) {
              carVO.setName(sub);
            }
            else if (carInfo.contains("Confirmation Number:")) {
              carVO.setConfirmationNumber(sub);
            }
            else if (carInfo.startsWith("Pick-Up:")) {
              Matcher m = timestampPattern.matcher(sub);
              if (m != null && m.find()) {
                String time = m.group();
                carVO.setPickupLocation(sub.replace(time, ""));
                carVO.setPickupDate(getTimestamp(time));
              } else {
                carVO.setPickupLocation(sub);
              }
            }
            else if (carInfo.startsWith("Drop-Off:")) {
              Matcher m = timestampPattern.matcher(sub);
              if (m != null && m.find()) {
                String time = m.group();
                carVO.setDropoffLocation(sub.replace(time,""));
                carVO.setDropoffDate(getTimestamp(time));
              } else {
                carVO.setDropoffLocation(sub);
              }
            }
            else if (carInfo.contains("Total Stay:") || carInfo.contains(
                "Estimated total price") || carInfo.contains("Reservation Name:") || carInfo.contains("Guarantee:")) {
              //do nothing
            }
            else {
              Matcher m = timestampPattern.matcher(carInfo);
              if (m != null && m.find()) {
                Timestamp ts = getTimestamp(carInfo);
                if (carVO.getPickupDate() == null) {
                  carVO.setPickupDate(ts);
                } else {
                  carVO.setDropoffDate(ts);
                }
              } else {
                sb.append(carInfo);
                sb.append("\n");
              }
            }

            //break out
            if (i == data.length || (i + 1) == data.length) {
              break;
            }
            else {
              int j = i + 1;
              String nextLine = data[j];
              if (nextLine != null && nextLine.equals(carInfo)) {
                if (!(i == data.length || (i + 1) == data.length)) {
                  i++;
                  nextLine = data[i + 1];
                }
              }

              String nextLineLower = nextLine.toLowerCase();

              if (nextLine.startsWith("Dep:") || nextLine.startsWith("GENERAL REMARKS") || nextLine.startsWith("CONDITIONS OF CONTRACT") ||  nextLineLower
                  .startsWith("tour information") || nextLineLower.startsWith("hotel information") || nextLineLower.startsWith(
                  "rail information") || nextLineLower.startsWith("transfer information") || nextLineLower.startsWith("car") || (nextLineLower.contains("from ") && nextLineLower.contains(" night"))) {
                break;
              }
              else
                i++;
            }
          }

          if (carVO.isValid()) {
            carVO.setNote(sb.toString());
            trip.addTransport(carVO);

          }


        }
      }

      trip.setImportSrc(BookingSrc.ImportSrc.AMADEUS_PDF);
      trip.setImportSrcId(reservationCode);
      trip.setImportTs(System.currentTimeMillis());


      document.close();
      return trip;

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (input != null) {
        input.close();
      }
    }

    return null;
  }

  private  String getDepartDate (String s) {
    if (s != null) {
      int count = 0;
      Matcher m = dayDatePattern.matcher(s);
      while (m.find()) {
        if (count ==0 ) {
          String departDate = m.group();
          return departDate;
        }
        count++;
      }

      m = dayDatePattern1.matcher(s);
      while (m.find()) {
        if (count ==0 ) {
          String departDate = m.group();
          return departDate;
        }
        count++;
      }
    }

    //try the second pattern

    return null;

  }

  private  String getArrivalDate (String d, int offset) {
    if (d != null) {
      try {
        Timestamp t = new Timestamp(du.parseDate(d,dateTimeFormat).getTime());
        long newTimestamp = t.getTime() + (offset * 24 * 60 * 60 * 1000);

        String dateFormat = "E dd MMM yyyy";

        Date date = new Date(newTimestamp);

        DateTime dt = new DateTime(date);
        if (dt.getYear() == 70 || dt.getYear() == 1970) {
          return "";
        }

        SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
        return dateFormatter.format(date);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    return null;

  }

  private Timestamp getTimestamp (String date, String time) {
    try {
      Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime() );
      return t;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  private Timestamp getTimestamp (String timestamp) {
    try {
      Timestamp t = new Timestamp(du.parseDate(timestamp, dateTimeFormat).getTime() );
      return t;
    } catch (Exception e) {
    }
    return null;
  }

  @Override
  public ArrayList<ParseError> getParseErrors()
  {
    return null;
  }

  @Override
  public void postProcessingImpl(String tripId, String userId) {
  }

  public static void main (String[] args) {
    try {
      FileInputStream fis = new FileInputStream(new File("/volumes/data2/Downloads/CheckMyTrip_5.pdf"));
      AmadeusCheckMyTrip a = new AmadeusCheckMyTrip();
      a.init(null);
      TripVO vo = a.extractDataImpl(fis);
      System.out.println(vo);
    } catch (Exception e) {

    }
  }

}
