package com.mapped.publisher.parse.extractor.booking;

import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.valueObject.TransportVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFTextStripperByArea;

import java.awt.*;
import java.io.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ryan on 31/08/16.
 */
public class AutoEuropeExtractor
    extends BookingExtractor {


  private static String[] dateTimeFormat = {"dd-MMM-yy hh:mm a", "MMM d yyyy HH:mm", "MMM dd yyyy HH:mm",
                                            "MMM d yyyy h:mma", "MMM dd yyyy h:mma", "MMM d yyyy hh:mma",
                                            "MMM dd yyyy hh:mma", "d MMM yyyy h:mma", "dd MMM yyyy h:mma",
                                            "d MMM yyyy hh:mma", "dd MMM yyyy hh:mma", "EEEE dd MMM yyyy h:mma",
                                            "EEEE MMM dd yyyy h:mma", "M/dd/yyyy", "M/dd/yyyy h:mm:ss a",
                                            "dd MMM yyyy", "dd MMM yyyy ", "E dd MMM yyyy h:mma", "E dd MMM yyyy",
                                            "E dd MMM yyyy ", "E dd MMMMM yyyy HH:mm", "dd MMM yyyy HH:mm a",
                                            "E dd MMM yyyy HH:mm", "dd MMMMM yyyy HH:mm", "E, MMMMM dd, yyyy HH:mm",
                                            "E, MMMMM dd, yyyy", "E, MMMMM dd, yyyy hh:mm a",
                                            "EEE dd MMM yyyy h:mm a", "EEE dd MMM yyyy 0h:mm a", "MMM dd yyyy h:mma"};

  private static DateUtils du = new DateUtils();

  private static String pdfLeft;
  private static String pdfRight;

  public static TransportVO parseTransport() {
    TransportVO tr = new TransportVO();

    try {
      BufferedReader leftBufReader = new BufferedReader(new StringReader(pdfLeft));
      BufferedReader rightBufReader = new BufferedReader(new StringReader(pdfRight));
      String leftLine = null;
      String rightLine = null;

      StringBuilder sbnote = new StringBuilder();
      StringBuilder sbPickupLoc = new StringBuilder();
      sbPickupLoc.append("Pickup:\n");

      StringBuilder sbDropoffLoc = new StringBuilder();
      sbDropoffLoc.append("Dropoff:\n");
      StringBuilder sbInclude = new StringBuilder();
      StringBuilder sbExclude = new StringBuilder();
      StringBuilder sbPricing = new StringBuilder();


      Pattern date = Pattern.compile("(?<!As\\sOf\\s)[0-3][0-9]-[A-Za-z]{3}-[0-9]{2}");
      Pattern time = Pattern.compile("[0-9][0-9]*:[0-9]{2}\\s(AM|PM)[\\s]?(?!-)");
      Pattern companyName = Pattern.compile("\\s\\s[A-Za-z]+");

      Matcher dateMatcher;
      Matcher timeMatcher;
      Matcher companyMatcher;

      String pickupDate = "";
      String dropoffDate = "";
      String pickupTime = "";
      String dropoffTime = "";
      String pickupLoc = "";
      String dropoffLoc = "";

      Boolean company = false;
      Boolean carInfo = false;
      Boolean rentalInfo = false;
      Boolean businessAccount = false;
      Boolean location = false;
      Boolean rental = false;
      Boolean rentalIncludes = false;

      tr.setBookingType(ReservationType.CAR_RENTAL);

      while ((leftLine = leftBufReader.readLine()) != null) {
        dateMatcher = date.matcher(leftLine);
        timeMatcher = time.matcher(leftLine);

        if (pickupDate.equals("") && dateMatcher.find()) {
          pickupDate = dateMatcher.group().trim();
        }

        if (location) {
          if (leftLine.contains("Comments::::")) {
            location = false;
            if (leftLine.trim().length() > 12){
              sbnote.append(leftLine);
              sbnote.append("\n");
            }
          }
          else if (leftLine.contains("Your rental includes:")) {
            location = false;
          } else {
            sbPickupLoc.append(leftLine);
            sbPickupLoc.append("\n");
          }
        }

        if (pickupTime.equals("") && timeMatcher.find()) {
          pickupTime = timeMatcher.group().trim();
          sbPickupLoc.append(leftLine.substring(0, leftLine.indexOf(pickupTime)).trim());
          pickupLoc = sbPickupLoc.toString();
          sbPickupLoc.append("\n");
          location = true;
        }

        if (!pickupDate.equals("") && !pickupTime.equals("")) {
          tr.setPickupDate(getTimestamp(pickupDate, pickupTime));
        }

        if (leftLine.contains("TEL: ")) {
          carInfo = false;
        }

        if (carInfo) {
          if (leftLine.contains("39 COMMERCIAL STREET") && leftLine.length() > "39 COMMERCIAL STREET".length()) {
            sbnote.append(leftLine.substring("39 COMMERCIAL STREET".length() + 1, leftLine.length()).trim());
          }
          else if (leftLine.contains("P.O. BOX 7006") && leftLine.length() > "P.O. BOX 7006".length()) {
            sbnote.append(leftLine.substring("P.O. BOX 7006".length() + 1, leftLine.length()).trim());
          }
          else if (leftLine.contains("PORTLAND, MAINE") && leftLine.length() > "PORTLAND, MAINE".length()) {
            sbnote.append(leftLine.substring("PORTLAND, MAINE".length() + 1, leftLine.length()).trim());
          }
          else if (leftLine.contains("04112-7006") && leftLine.length() > "04112-7006".length()) {
            sbnote.append(leftLine.substring("04112-7006".length() + 1, leftLine.length()).trim());
          }
          else {
            sbnote.append(leftLine);
          }
          sbnote.append("\n");
        }

        if (leftLine.contains("voucher number")) {
          String[] voucherNum = leftLine.split(" ");
          sbnote.append("Auto Europe voucher number ");
          sbnote.append(voucherNum[voucherNum.length - 1]);
          sbnote.append("\n\n");
          carInfo = true;
        }

        if (rentalInfo) {
          if (leftLine.contains("RESERVATIONS:")) {
            sbnote.append(leftLine.substring("RESERVATIONS:".length() + 1, leftLine.length()).trim());
          }
          else if (leftLine.contains("Business Account:")) {
            if (leftLine.contains("Rate Code:")) {
              sbnote.append(leftLine.substring(leftLine.indexOf("Rate Code:"),
                                               leftLine.lastIndexOf("Business Account:")));
              sbnote.append("\n");
              sbnote.append(leftLine.substring(leftLine.indexOf("Business Account:"), leftLine.length()));
            }
            else {
              sbnote.append("Business Account:");
            }
            businessAccount = true;
          }
          else if (businessAccount) {
            businessAccount = false;
            if (leftLine.split(" ").length > 1) {
              if (leftLine.contains("Flight Info:")) {
                sbnote.append(leftLine.substring(leftLine.indexOf("Flight Info:"), leftLine.length()));
                rentalInfo = false;
              }
              else {
                sbnote.append(leftLine.split(" ")[1]);
              }
            }
            else {
              sbnote.append(leftLine);
            }
          }
          else if (leftLine.contains("Flight Info:")) {
            rentalInfo = false;
            sbnote.append(leftLine);
          }
          else {
            if (leftLine.split(" ")[0].contains("1-800")) {
              leftLine = leftLine.replace(leftLine.split(" ")[0], "");
              sbnote.append(leftLine);
            }
            else {
              sbnote.append(leftLine);
            }
          }
          sbnote.append("\n");
        }

        if (leftLine.contains("Rental Period")) {
          rentalInfo = true;
          sbnote.append("\nRental Period");
          sbnote.append("\n");
        }

        if (rental) {
          if (leftLine.contains("Additional fees to be paid locally, if required:")) {
            rental = false;
            break;
          }
          else {
            if (!leftLine.contains("Your rental includes:")) {
              if (leftLine.trim().startsWith("*")) {
                sbInclude.append("\n");
              }
              sbInclude.append(leftLine + " ");

            }
          }
        }

        if (leftLine.contains("Your rental includes:") && !rentalIncludes) {
          location = false;
          rental = true;
          rentalIncludes = true;
          sbInclude.append(leftLine);
        }
      }

      Boolean pricing = false;
      Boolean notEnd = false;
      Boolean endLine = false;
      location = false;
      rental = false;
      rentalIncludes = false;
      sbnote.append("\n");

      while ((rightLine = rightBufReader.readLine()) != null) {
        dateMatcher = date.matcher(rightLine);
        timeMatcher = time.matcher(rightLine);

        if (dropoffDate.equals("") && dateMatcher.find()) {
          dropoffDate = dateMatcher.group().trim();
        }

        if (location) {
          if (rightLine.contains("Your rental excludes:")) {
            location = false;
          }
          else {
            sbDropoffLoc.append(rightLine);
            sbDropoffLoc.append("\n");
          }
        }

        if (dropoffTime.equals("") && timeMatcher.find()) {
          dropoffTime = timeMatcher.group().trim();
          sbDropoffLoc.append(rightLine.substring(0, rightLine.indexOf(dropoffTime)).trim());
          dropoffLoc = sbDropoffLoc.toString();
          sbDropoffLoc.append("\n");
          location = true;
        }

        if (pricing) {
          if (rightLine.contains("Driver Information")) {
            pricing = false;
          }
          else if (!rightLine.endsWith("_")) {
            sbPricing.append(rightLine);
            sbPricing.append("\n");
          }
        }

        if (rightLine.contains("reservation number")) {
          String[] resNum = rightLine.split(" ");
          if (!resNum[resNum.length - 1].contains("number")) {
            tr.setConfirmationNumber(resNum[resNum.length - 1]);
          }
          pricing = true;
        }

        if (!dropoffDate.equals("") && !dropoffTime.equals("")) {
          tr.setDropoffDate(getTimestamp(dropoffDate, dropoffTime));
        }

        if (company) {
          companyMatcher = companyName.matcher(rightLine);
          if (companyMatcher.find()) {
            tr.setName(companyMatcher.group().trim());
          }
          company = false;
        }

        if (rightLine.contains("Rental Company:")) {
          company = true;
        }

        if (rental) {
          if (endLine && !rightLine.startsWith("*") && !rightLine.contains("Your rental excludes:")) {
            rightBufReader.mark(1000);
            String rightLine2 = null;
            while ((rightLine2 = rightBufReader.readLine()) != null) {
              if (rightLine2.startsWith("*")) {
                rightBufReader.reset();
                notEnd = true;
                break;
              }
            }

            if (!notEnd) {
              break;
            }
            else {
              notEnd = false;
              sbExclude.append(rightLine + " ");

            }
          }
          else if (rightLine.endsWith(".")) {
            sbExclude.append(rightLine);
            endLine = true;
          }
          else if (!rightLine.contains("Your rental excludes:")) {
            if (rightLine.trim().startsWith("*")) {
              sbExclude.append("\n");
            }
            sbExclude.append(rightLine + " ");
            endLine = false;
          }
        }

        if (rightLine.contains("Your rental excludes:") && !rentalIncludes) {
          rental = true;
          rentalIncludes = true;
          sbExclude.append(rightLine);
        }
      }

      tr.setPickupLocation(pickupLoc.replace("Pickup:","").trim());
      tr.setDropoffLocation(dropoffLoc.replace("Dropoff:","").trim());


      if (!sbPickupLoc.toString().isEmpty()) {
        sbnote.append("\n");
        sbnote.append(sbPickupLoc);
      }
      if (!sbDropoffLoc.toString().isEmpty()) {
        sbnote.append("\n");
        sbnote.append(sbDropoffLoc);
      }
      if (!sbPricing.toString().isEmpty()) {
        sbnote.append("\n");
        sbnote.append(sbPricing);
      }

      if (!sbInclude.toString().isEmpty()) {
        sbnote.append("\n\n");
        sbnote.append(sbInclude);
      }

      if (!sbExclude.toString().isEmpty()) {
        sbnote.append("\n\n");
        sbnote.append(sbExclude);
      }


      tr.setNote(sbnote.toString().trim());
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    return tr;
  }

  private static final void getPDFContentAsString(InputStream fis) {
    PDDocument pdfDocument = null;
    StringBuilder contents = new StringBuilder();
    try {
      PDFParser parser = new PDFParser(fis);
      parser.parse();

      pdfDocument = parser.getPDDocument();

      //left
      PDFTextStripperByArea stripper = new PDFTextStripperByArea();

      stripper.setSortByPosition(true);
      Rectangle rect = new Rectangle(0, 50, 300, 700);
      stripper.addRegion("body", rect);

      java.util.List<?> allPages = pdfDocument.getDocumentCatalog().getAllPages();

      for (Object pdPage : allPages) {
        stripper.extractRegions((PDPage) pdPage);
        contents.append(stripper.getTextForRegion("body"));
      }
      pdfLeft = contents.toString();

      //right
      contents = new StringBuilder();
      stripper = new PDFTextStripperByArea();

      stripper.setSortByPosition(true);
      rect = new Rectangle(300, 50, 300, 700);
      stripper.addRegion("body", rect);

      allPages = pdfDocument.getDocumentCatalog().getAllPages();

      for (Object pdPage : allPages) {
        stripper.extractRegions((PDPage) pdPage);
        contents.append(stripper.getTextForRegion("body"));
      }
      pdfRight = contents.toString();

    }
    catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (pdfDocument != null) {
        try {
          pdfDocument.close();
        } catch (Exception e) {

        }
      }
    }
  }

  private static Timestamp getTimestamp(String date, String time) {
    try {
      Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime());
      return t;
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  protected void initImpl(Map<String, String> params) {

  }

  protected TripVO extractDataImpl(InputStream input)
      throws Exception {
    TripVO trip = new TripVO();
    getPDFContentAsString(input);
    TransportVO transportVO = parseTransport();
    if (transportVO != null && transportVO.isValid()) {
      trip.addTransport(transportVO);
    }

    return trip;
  }

  @Override
  public ArrayList<ParseError> getParseErrors() {
    return null;
  }

  @Override
  protected void postProcessingImpl(String tripId, String userId) {

  }


  public static void main(String[] args) {
    try {
      String dir = "//home/twong/Downloads/Auto-Europe-Test2.pdf";
      // String dir = "/users/twong/downloads/travel2.pdf";
      File file = new File(dir);
      InputStream ins = new FileInputStream(file);

      //System.out.println(getPDFContentAsString(ins));

      AutoEuropeExtractor p = new AutoEuropeExtractor();
      p.initImpl(null);
      TripVO t = p.extractData(ins);
      //p.postProcessingImpl("1", "2");
      System.out.println(t);

    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }
}

