package com.mapped.publisher.parse.extractor.booking;

import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by twong on 2015-02-04.
 */
public abstract class EmailBodyExtractor {

  /** Slogan that might appear on each page to be removed */
  public final static String CONF_SLOGAN = "SLOGAN";
  private String cmpyId = null;
  private String userId = null;
  private String parserName = "";
  private String parserEnum = "";

  public static enum Parsers {
    TRIPCASE(com.mapped.publisher.parse.extractor.booking.email.TripCaseExtractor.class, "Sabre TripCase", true),
    APOLLO(com.mapped.publisher.parse.extractor.booking.email.ApolloExtractor.class, "Apollo", true),
    WORLDSPAN(com.mapped.publisher.parse.extractor.booking.email.WorldspanExtractor.class, "Worldspan", true),
    CENTRAV(com.mapped.publisher.parse.extractor.booking.email.CentravExtractor.class, "Centrav", true),
    JSONLD(com.mapped.publisher.parse.extractor.booking.email.JsonLdExtractor.class, "JSON-LD", true),
    MICRODATA(com.mapped.publisher.parse.extractor.booking.email.MicrodataExtractor.class, "Microdata", true),
    ALPINE_ADVENTURES(com.mapped.publisher.parse.extractor.booking.email.AlpineAdventuresExtractor.class, "Alpine Adventures", true);


    /**
     * Class to instantiate the extractor
     */
    private Class<? extends EmailBodyExtractor> extractor;

    /**
     * Means access to this extractor is open to all companies
     */
    private boolean open;

    private String description;

    Parsers(Class<? extends EmailBodyExtractor> extractor, String desc, boolean open) {
      this.extractor = extractor;
      this.open = open;
      this.description = desc;
    }

    public static Parsers fromClass(String classPath) {
      for (Parsers p : values()) {
        if (p.extractor.getCanonicalName().equals(classPath)) {
          return p;
        }
      }
      return null;
    }

    public EmailBodyExtractor makeExtractor() {
      try {
        EmailBodyExtractor e = extractor.newInstance();
        e.setParserName(this.getExtractorName());
        e.setParserEnum(this.name());
        return e;
      }
      catch (Exception e) {
        return null;
      }
    }

    public String getExtractorName() {
      return this.extractor.getCanonicalName();
    }

    public boolean isOpen() {
      return this.open;
    }

    public String getDescription() {
      return this.description;
    }
  }

  /* Parameter configurable by the administrator */

  public String getParserEnum() {
    return parserEnum;
  }

  public void setParserEnum(String parserEnum) {
    this.parserEnum = parserEnum;
  }

  public void init(Map<String, String> params) {
    this.initImpl(params);
  }

  protected abstract void initImpl(Map<String, String> params);

  public TripVO extractData(String input)
      throws Exception {
    try {
      return this.extractDataImpl(input);
    }
    finally {

    }
  }

  protected abstract TripVO extractDataImpl(String input)
      throws Exception;

  /**
   * Access to the list of parse errors.
   *
   * @return list of parse errors, null if class does not support error reporting
   * @note must be called only after calling @see extractData() method
   */
  abstract public ArrayList<ParseError> getParseErrors();

  public String getCmpyId() {
    return cmpyId;
  }

  public void setCmpyId(String cmpyId) {
    this.cmpyId = cmpyId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getParserName() {
    return parserName;
  }

  public void setParserName(String parserName) {
    this.parserName = parserName;
  }
}
