package com.mapped.publisher.parse.extractor.booking.valueObject;

import com.umapped.persistence.reservation.UmReservation;
import com.umapped.persistence.reservation.UmTraveler;
import com.umapped.persistence.reservation.flight.UmFlightReservation;
import com.umapped.persistence.reservation.flight.UmFlightTraveler;

import models.publisher.TripDetail;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-10-25
 * Time: 9:35 AM
 * To change this template use File | Settings | File Templates.
 */
public class PassengerVO
    extends BaseVO {

  private UmFlightTraveler delegate;

  private String title;

  public PassengerVO() {
    delegate = new UmFlightTraveler();
  }

  public PassengerVO(UmTraveler traveler) {
    delegate = cast(traveler, UmFlightTraveler.class);
  }

  /**
   * @return the firstName
   */
  public String getFirstName() {
    return delegate.getGivenName();
  }

  /**
   * @param firstName the firstName to set
   */
  public void setFirstName(String firstName) {
    if (firstName != null) {
      if (firstName.endsWith("MR")) {
        firstName = firstName.substring(0, firstName.lastIndexOf("MR"));
      } else if (firstName.endsWith("MS")) {
        firstName = firstName.substring(0, firstName.lastIndexOf("MS"));
      } else if (firstName.endsWith("MRS")) {
        firstName = firstName.substring(0, firstName.lastIndexOf("MRS"));
      }

    }

    delegate.setGivenName(firstName);
  }

  /**
   * @return the lastName
   */
  public String getLastName() {
    return delegate.getFamilyName();
  }

  public void setFullName(String fullName) {
    if (fullName == null || fullName.isEmpty()) {
      return;
    }
    String[] splited = null;
    if (fullName.contains("/")) {
      splited = fullName.split("/");
      if (splited.length == 2) {
        setLastName(splited[0]);
        setFirstName(splited[1]);

      }
      else if (splited.length == 1) {
        setFirstName(splited[0]);
      }
    } else {
      splited = fullName.split("\\s+");
      if (splited != null) {
        if (splited.length >= 2) {
          setFirstName(splited[0]);

          StringBuilder sb = new StringBuilder();
          for (int idx = 1; idx < splited.length; idx++) {
            sb.append(splited[idx]);
            sb.append(" ");
          }
          setLastName(sb.toString().trim());


        } else if (splited.length == 1) {
          setFirstName(splited[0]);
        }
      }
    }

  }

  /**
   * @param lastName the lastName to set
   */
  public void setLastName(String lastName) {
    if (lastName != null) {
      if (lastName.endsWith("MR")) {
        lastName = lastName.substring(0, lastName.lastIndexOf("MR"));
      } else if (lastName.endsWith("MS")) {
        lastName = lastName.substring(0, lastName.lastIndexOf("MS"));
      } else if (lastName.endsWith("MRS")) {
        lastName = lastName.substring(0, lastName.lastIndexOf("MRS"));
      }

    }
    delegate.setFamilyName(lastName);
  }

  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return the eTicketNumber
   */
  public String geteTicketNumber() {
    return delegate.getTicketNumber();
  }

  /**
   * @param eTicketNumber the eTicketNumber to set
   */
  public void seteTicketNumber(String eTicketNumber) {
    delegate.setTicketNumber(eTicketNumber);
  }

  public String getFrequentFlyer() {
    return delegate.getProgram().membershipNumber;
  }

  public void setFrequentFlyer(String frequentFlyer) {
    delegate.getProgram().membershipNumber = frequentFlyer;
  }

  public String toString() {
    String nl = System.getProperty("line.separator");
    StringBuilder strBuilder = new StringBuilder(">>> Passenger (Value Object)" + nl);
    strBuilder.append("First Name:" + getFirstName() + nl);
    strBuilder.append("Last Name:" + getLastName() + nl);
    strBuilder.append("Title:" + title + nl);
    strBuilder.append("eTicket#:" + geteTicketNumber() + nl);
    strBuilder.append("Frequent Flyer#:" + getFrequentFlyer() + nl);

    strBuilder.append("Note:" + nl + getFullNotes() + nl);

    return strBuilder.toString();
  }

  public static List<PassengerVO> getPassengerVO(UmReservation reservation) {
    List<UmTraveler> travelers = reservation.getTravelers();
  
    if (travelers == null) {
      return new ArrayList<>(0);
    } else {
      List<PassengerVO> passengers = travelers.stream()
          .map(PassengerVO::new)
          .collect(Collectors.toList());
      return passengers;
    }  
  }
  
  public static List<UmTraveler> getTravelers(List<PassengerVO> passengers) {
    if (passengers != null) {
      return passengers.stream()
      .map(PassengerVO::map)
      .collect(Collectors.toList());
    } 
    else {
      return null;
    }
  }

   
  public static UmFlightTraveler map(PassengerVO p) {
    return p.delegate;
  }
  

  public String getFullName () {
    String name = "";
    if (getFirstName() != null) {
      name = getFirstName() + " ";
    }
    if (getLastName() != null) {
      name += getLastName();
    }

    return name;
  }

  public String getSeat() {
    return delegate.getSeat();
  }

  public void setSeat(String seat) {
    delegate.setSeat(seat);
  }

  public String getSeatClass() {
    return delegate.getSeatClass().seatCategory;
  }

  public void setSeatClass(String seatClass) {
    delegate.getSeatClass().seatCategory = seatClass;
  }

  public String getMeal() {
    return delegate.getMeal();
  }

  public void setMeal(String meal) {
    delegate.setMeal(meal);
  }
}
