package com.mapped.publisher.parse.extractor.booking.valueObject;

import com.umapped.persistence.reservation.flight.UmFlight;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-10-25
 * Time: 9:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class FlightVO
    extends ReservationVO {

  private String            airlinePoiId;
  private String            code;
  private String            number;
  private Timestamp         depatureTime;
  private AirportVO         departureAirport;
  private Timestamp         arrivalTime;
  private AirportVO         arrivalAirport;
  private String            reservationNumber;
  private List<PassengerVO> passengers;

  private String            departureGate;
  private String            arrivalGate;
  private String            airline;
  private String            operatedBy;
  private String            aircraft;

  public FlightVO() {
    this(true);
  }

  public FlightVO(boolean commentAsContainer) {
    super(commentAsContainer);
  }

  public String getAirlinePoiId() {
    return airlinePoiId;
  }

  public FlightVO setAirlinePoiId(String airlinePoiId) {
    this.airlinePoiId = airlinePoiId;
    return this;
  }

  public String getNotes() {
    return getFullNotes();
  }

  public void setNotes(String notes) {
    this.note = notes;
  }

  /**
   * @return the code
   */
  public String getCode() {
    return code;
  }

  /**
   * @param code the code to set
   */
  public void setCode(String code) {
    this.code = code;
    if (this.code != null) {
      this.code = this.code.trim().toUpperCase();
    }
  }

  /**
   * @return the number
   */
  public String getNumber() {
    return number;
  }

  /**
   * @param number the number to set
   */
  public void setNumber(String number) {
    this.number = number;
  }

  /**
   * @return the depatureTime
   */
  public Timestamp getDepatureTime() {
    return depatureTime;
  }

  /**
   * @param depatureTime the depatureTime to set
   */
  public void setDepatureTime(Timestamp depatureTime) {
    this.depatureTime = depatureTime;
  }

  /**
   * @return the departureAirport
   */
  public AirportVO getDepartureAirport() {
    return departureAirport;
  }

  /**
   * @param departureAirport the departureAirport to set
   */
  public void setDepartureAirport(AirportVO departureAirport) {
    this.departureAirport = departureAirport;
  }

  /**
   * @return the arrivalTime
   */
  public Timestamp getArrivalTime() {
    return arrivalTime;
  }

  /**
   * @param arrivalTime the arrivalTime to set
   */
  public void setArrivalTime(Timestamp arrivalTime) {
    this.arrivalTime = arrivalTime;
  }

  /**
   * @return the arrivalAirport
   */
  public AirportVO getArrivalAirport() {
    return arrivalAirport;
  }

  /**
   * @param arrivalAirport the arrivalAirport to set
   */
  public void setArrivalAirport(AirportVO arrivalAirport) {
    this.arrivalAirport = arrivalAirport;
  }

  /**
   * @return the reservationNumber
   */
  public String getReservationNumber() {
    return reservationNumber;
  }

  /**
   * @param reservationNumber the reservationNumber to set
   */
  public void setReservationNumber(String reservationNumber) {
    this.reservationNumber = reservationNumber;
  }

  /**
   * @return the passengers
   */
  public List<PassengerVO> getPassengers() {
    return passengers;
  }

  /**
   * @param passengers the passengers to set
   */
  public void setPassengers(List<PassengerVO> passengers) {
    this.passengers = passengers;
  }


  public void addPassenger(PassengerVO pvo) {
    if (passengers == null) {
      passengers = new ArrayList<PassengerVO>();
    }
    if (pvo != null) {
      passengers.add(pvo);
    }
  }

  public void setDepartureGate(String departureGate) {
    this.departureGate = departureGate;
    // for displaying it in comments until UI has a separate field
    addProperty("Departure Gate", departureGate);
  }

  public String getDepartureGate() {
    return departureGate;
  }

  public void setArrivalGate(String arrivalGate) {
    this.arrivalGate = arrivalGate;

    // for displaying it in comments until UI has a separate field
    addProperty("Arrival   Gate", arrivalGate);
  }

  public String getArrivalGate() {
    return arrivalGate;
  }

  public void setAirCraft(String airCraft) {
    this.aircraft = airCraft;
    // for displaying it in comments until UI has a separate field
    addProperty("Aircraft", airCraft);
  }

  public String getAirCraft() {
    return aircraft;
  }

  public void setOperatedBy(String airlineName) {
    this.operatedBy = airlineName;
    // for displaying it in comments until UI has a separate field
    addProperty("Operated By", airlineName);
  }

  public String getOperatedBy() {
    return operatedBy;
  }

  public void setAirline(String airlineName) {
    this.airline = airlineName;
    // for displaying it in comments until UI has a separate field
    addProperty("Airline", airlineName);
  }

  public String getAirline() {
    return airline;
  }

  public String toString() {
    String        nl         = System.getProperty("line.separator");
    StringBuilder strBuilder = new StringBuilder(">>> Flight (Value Object)" + nl);
    strBuilder.append("Name:" + name + nl);
    strBuilder.append("Code:" + code + nl);
    strBuilder.append("Number:" + number + nl);
    strBuilder.append("Departure:" + depatureTime + nl);
    strBuilder.append("Arrival:" + arrivalTime + nl);
    strBuilder.append("Reservation Number:" + reservationNumber + nl);
    strBuilder.append("Notes:" + nl + getFullNotes() + nl);
    if (departureAirport != null) {
      strBuilder.append("Depart Airport" + departureAirport.toString());
    }
    if (arrivalAirport != null) {
      strBuilder.append("Arrive Airport" + arrivalAirport.toString());
    }

    return strBuilder.toString();
  }


  public boolean isValid() {
    if (code != null &&
        code.trim().length() > 0 &&
        number != null &&
        number.trim().length() > 0 &&
        !number.equals("null") &&
        departureAirport != null &&
        depatureTime != null) {
      return true;
    }

    return false;
  }
}
