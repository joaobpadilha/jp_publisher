package com.mapped.publisher.parse.extractor.booking.valueObject;

import com.umapped.persistence.reservation.UmPostalAddress;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-10-25
 * Time: 9:37 AM
 * To change this template use File | Settings | File Templates.
 */
public class HotelVO
    extends ReservationVO {
  private Timestamp checkin;
  private Timestamp checkout;
  private String confirmation;
  private UmPostalAddress address;
  private String phone;
  private String fax;
  private String url;
  private String membershipProgram;
  private String membershipNumber;

  private List<HotelTravelerVO> travelers;

  public HotelVO() {
    this(true);
  }

  public HotelVO(boolean commentAsContainer) {
    super(commentAsContainer);
  }
  public String getHotelName() {
    return name;
  }

  public void setHotelName(String hotelName) {
    this.name = hotelName;
  }

  public Timestamp getCheckin() {
    return checkin;
  }

  public void setCheckin(Timestamp checkin) {
    this.checkin = checkin;
  }

  public Timestamp getCheckout() {
    return checkout;
  }

  public void setCheckout(Timestamp checkout) {
    this.checkout = checkout;
  }

  public String getConfirmation() {
    return confirmation;
  }

  public void setConfirmation(String confirmation) {
    this.confirmation = confirmation;
  }

  public void setAddress(String streetAddress, String locality, String region, String country) {
      address = new UmPostalAddress();
      address.streetAddress = streetAddress;
      address.addressLocality = locality;
      address.addressRegion = region;
      address.addressCountry = country;
  }

  public UmPostalAddress getAddress() {
     return address;
  }


  public void setFax(String fax) {
      this.fax = fax;
  }

  public String getFax() {
    return fax;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getPhone() {
    return phone;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getUrl() {
    return url;
  }

  public void setProgramMemberhsip(String program, String membershipNumber) {
    this.membershipProgram = program;
    this.membershipNumber = membershipNumber;
    // for displaying it in comments until UI has a separate field
    if (program != null) {
      addProperty(program, membershipNumber);
    }
    else {
      addProperty("Membership:", membershipNumber);
    }
}

  public String getMembershipProgram() {
    return membershipProgram;
  }

  public String getMembershipNumber() {
    return membershipNumber;
  }

  public List<HotelTravelerVO> getTravelers() {
    return travelers;
  }

  public HotelVO setTravelers(List<HotelTravelerVO> travelers) {
    this.travelers = travelers;
    return this;
  }

  public String toString() {
    String nl = System.getProperty("line.separator");
    StringBuilder strBuilder = new StringBuilder(">>> Hotel (Value Object)" + nl);
    strBuilder.append("Name        :" + name + nl);
    strBuilder.append("Checkin     :" + checkin + nl);
    strBuilder.append("Checkout    :" + checkout + nl);
    strBuilder.append("Confirmation:" + confirmation + nl);
    strBuilder.append("Note        :" + nl + getFullNotes() + nl);
    return strBuilder.toString();
  }

  public boolean isValid() {
    if (name != null && name.trim().length() > 0 && checkin != null) {
      return true;
    }
    return false;
  }

  public static class HotelTravelerVO {
    public String firstName;
    public String lastName;
    public String roomType;
    public String amenities;
    public String status;
    public String membershipNumber;
    public String bedding;
  }
}
