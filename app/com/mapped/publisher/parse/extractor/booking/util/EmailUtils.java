package com.mapped.publisher.parse.extractor.booking.util;

import com.mapped.publisher.parse.extractor.booking.EmailBodyExtractor;
import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by twong on 2015-02-04.
 */
public class EmailUtils {

  public static Map<String, EmailBodyExtractor> getExtractors(List<String> bodies, List<String> htmls)
      throws Exception {

    Map<String, EmailBodyExtractor> extractors = new HashMap<>();

    for (String b : bodies) {
      EmailBodyExtractor e = forPlain(b);
      if (e != null) {
        extractors.put(b, e);
      }
    }

    for (String h : htmls) {
      EmailBodyExtractor e = forHtml(h);
      if (e != null) {
        extractors.put(h, e);
      }
    }

    return extractors;
  }


  private static final Pattern base64attachPatthern = Pattern.compile("--boundary.*?\r\n" +
                                                                      "Content-Type:\\s+text\\/html.*?\r\n\r\n([A-Za-z0-9\\+\\/\r\n=]+)----boundary", Pattern.MULTILINE | Pattern.DOTALL);

  private static String decodeIfNeded(String s)
      throws IOException {

    if (s.contains("base64")) {
      Matcher m = base64attachPatthern.matcher(s);
      if(m.find()) {
        String ss = m.group(1);
        byte[] decoded = Base64.decodeBase64(ss);
        String result =  new String(decoded, "UTF-8");
        return result;
      }
    }

    return s;
  }

  private static EmailBodyExtractor forPlain(String plain) {
    if (plain == null) {
      return null;
    }
    String lower = plain.toLowerCase();
    if (lower.contains("centrav.com")) {
      return EmailBodyExtractor.Parsers.CENTRAV.makeExtractor();
    }
    else if (plain.contains("LV:") && plain.contains("AR:")) {
      return EmailBodyExtractor.Parsers.APOLLO.makeExtractor();
    }
    else if (plain.contains("application/ld+json")) {
      return EmailBodyExtractor.Parsers.JSONLD.makeExtractor();
    }
    else if (plain.contains("http://schema.org")) {
      return EmailBodyExtractor.Parsers.MICRODATA.makeExtractor();
    }

    return null;
  }

  private static EmailBodyExtractor forHtml(String html) {
    if (html == null) {
      return null;
    }

    if (html.contains("application/ld+json")) {
      return EmailBodyExtractor.Parsers.JSONLD.makeExtractor();
    }
    else if (html.contains("http://schema.org")) {
      return EmailBodyExtractor.Parsers.MICRODATA.makeExtractor();
    }
    else if (html.contains("worldspan.com")) {
      return EmailBodyExtractor.Parsers.WORLDSPAN.makeExtractor();
    }
    else if (html.contains("services.tripcase.com")) {
      return EmailBodyExtractor.Parsers.TRIPCASE.makeExtractor();
    } else if (html.contains("Inntopia") || html.contains("www.skimax") || html.contains("skimax.com.au") || html.contains("Alpine Adventures - Anywhere Adventures") || html.contains("http://www.alpineadventures.net")) {
      return EmailBodyExtractor.Parsers.ALPINE_ADVENTURES.makeExtractor();
    }

    return null;
  }

}
