package com.mapped.publisher.parse.extractor.booking;


/**
 * Created by twong on 15-10-04.
 */


import actors.ActorsHelper;
import actors.RecLocatorActor;
import actors.SupervisorActor;
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.form.RecordLocatorForm;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFTextStripperByArea;
import org.joda.time.DateTime;
import org.owasp.html.Sanitizers;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Travel2Extractor
    extends BookingExtractor {

  private String file;
  private TripVO trip;

  private static String[] dateTimeFormat = {"EEEE dd MMMM yyyy h:mma", "EEEE dd MMMM yyyy hmma", "EEEE dd MMMM yyyy hhmma"};
  private static DateUtils du = new DateUtils();
  private static Calendar now = Calendar.getInstance();

  private List<String> header = new ArrayList<>();
  private List<PassengerVO> travelers = new ArrayList<>();

  private String date = "";

  private String maxDate = "";

  private int rank = -1;

  private enum BOOKING_TYPE {
    HOTEL,
    FLIGHT,
    TOUR,
    TRANSFER,
    CAR,
    CRUISE,
    TRAIN,
    INSURANCE,
    DATE,
    ASSISTANCE,
    CONNECTED,
    OWN_ARRANGEMENT,
    TICKET,
    UNDEFINED
  }

  ;

  private static Pattern timePattern = Pattern.compile("[0-9]{1,2}(?:[:]|)[0-9]{2}[a,p,am,pm]");
  private static Pattern hotelCheckoutPattern = Pattern.compile(
      "Check-Out\\sDate:\\s[A-Za-z]{3}\\s[0-9]{2}\\s[A-Za-z]{3}\\s[0-9]{2}\\sat\\s[0-9]{2}:[0-9]{2}");

  private final static Pattern datePattern=Pattern.compile("(?:MONDAY|TUESDAY|WEDNESDAY|THURSDAY|FRIDAY|SATURDAY|SUNDAY).[0-9]{2}.(?:JANUARY|FEBRUARY|MARCH|APRIL|MAY|JUNE|JULY|AUGUST|SEPTEMBER|OCTOBER|NOVEMBER|DECEMBER).[0-9]{4}");
  private static Pattern numPattern = Pattern.compile("[0-9]{1,3}");

  private static Pattern amadeusPattern = Pattern.compile("Flight PNR.*.AM/[0-9A-Z]{6}");
  private String amadeusPNR = null;
  private boolean isQuote = false;



  protected void initImpl(Map<String, String> params) {

  }

  protected TripVO extractDataImpl(InputStream input)
      throws Exception {
    trip = new TripVO();
    trip.setNoPOIMatch(); //do not match poi
    file = getPDFContentAsString(input);

    if (file.contains("COST OF TOUR BY PASSENGER")) {
      isQuote =true;
    }

    //
      Matcher m = amadeusPattern.matcher(file);
    if (m.find()) {
      String s = m.group();
      amadeusPNR = s.substring(s.indexOf("AM/") + 3);
      // for now don't try to import from amadeus
      amadeusPNR = null;
    }


      //lets get the repeat header info
    processFile();
    if (file != null) {
      if (file.contains("qantasvacations.com")) {
        trip.setImportSrc(BookingSrc.ImportSrc.TRAVEL2_QANTAS);
      } else if (file.contains("islandsinthesun.com")) {
        trip.setImportSrc(BookingSrc.ImportSrc.TRAVEL2_ISLANDS);
      } else {
        trip.setImportSrc(BookingSrc.ImportSrc.TRAVEL2);
      }
    }
    trip.setImportTs(System.currentTimeMillis());

    if (isQuote) {
      //handle quote
      try {
        processQuote();
      } catch (Exception e) {
        e.printStackTrace();
      }
      switch (trip.getImportSrc()) {
        case TRAVEL2:
          addTravel2QuoteInfo();
          break;
        case TRAVEL2_QANTAS:
          addQantasQuoteInfo();
          break;
        case TRAVEL2_ISLANDS:
          addIslandQuoteInfo();
          break;
      }
    }

    return trip;
  }

  private void  processFile() {
    String[] tokens = file.split("\n");
    boolean addedPreTravel = false;
    for (int i = 0; i < tokens.length; i++) {
      String s = tokens[i];
      if (header.isEmpty()) {
        //let's process the header
        boolean processPassenger = false;
        while (true) {
          Matcher m = datePattern.matcher(s);
          String dateStr = null;
          if (m.find()) {
            dateStr = m.group();
          }

          if (s.contains("PREPARED") || s.contains("ITINERARY")) {
            if (s.contains("PREPARED")) {
              processPassenger = true;
            }
            break;
          } else if (dateStr != null && s.trim().equals(dateStr.trim())) {
            //first date - so we need to back up 1 line and start processing the itinerary
            i--;
            break;
          } else {

            header.add(s);
          }
          i++;
          if (i < tokens.length) {
            s = tokens[i];
          } else {
            break;
          }
        }
        //get the passengers
        if (!header.isEmpty() && processPassenger) {
          for (String h: header) {
            if (h.contains("Booking")) {
              h = h.substring(0, h.indexOf("Booking"));
            } else if (h.contains("Flight")) {
              h = h.substring(0, h.indexOf("Flight"));
            } else if (h.contains("Issued")) {
              h = h.substring(0, h.indexOf("Issued"));
            }

            StringBuilder names1 = new StringBuilder();

            for (char c: h.toCharArray()) {
              if (Character.isUpperCase(c) || Character.isSpaceChar(c)) {
                names1.append(c);
              }
            }

            String[] nameTokens = names1.toString().split(" ");
            StringBuilder sb = new StringBuilder();
            for (String token:nameTokens) {
              if (h.contains(token + " ")) {
                sb.append(token);
                sb.append(" ");
              }
            }

            String name = sb.toString().trim();
            if (name.trim().length() > 0) {
              PassengerVO passengerVO = new PassengerVO();
              if (name.indexOf(" ") > 0) {
                name = name.substring(name.indexOf(" ") + 1);
              }
              passengerVO.setFullName(name);
              travelers.add(passengerVO);
            }
          }
        }
      } else if (!header.contains(s)) {
        BOOKING_TYPE booking_type = getBookingType(s);
        if (booking_type != BOOKING_TYPE.UNDEFINED && booking_type != BOOKING_TYPE.DATE) {
          if (booking_type == BOOKING_TYPE.HOTEL) {
            i = parseHotel(++i, tokens);
          }
          else if (booking_type == BOOKING_TYPE.TRANSFER) {
            i = parseTransfers(++i, tokens);
          }  else if (booking_type == BOOKING_TYPE.CAR) {
            i = parseCar(++i, tokens);
          }
          else if (booking_type == BOOKING_TYPE.TOUR) {
            i = parseTours(++i, tokens);
          }
          else if (booking_type == BOOKING_TYPE.FLIGHT) {
            i = parseFlight(i, tokens);
          } else if (booking_type == BOOKING_TYPE.ASSISTANCE) {
            if(!addedPreTravel) {
              addPreTravelInfo();
              addedPreTravel = true;
            }
            i = parseNote(++i, tokens, "After Hours Assistance");
          } else if (booking_type == BOOKING_TYPE.OWN_ARRANGEMENT) {
            i = parseNote(++i, tokens,"Own Arrangements");
          } else if (booking_type == BOOKING_TYPE.CRUISE) {
            i = handleCruise(++i, tokens);
          } else if (booking_type == BOOKING_TYPE.TRAIN) {
            i = parseRail(++i, tokens);
          } else if (booking_type == BOOKING_TYPE.TICKET) {
            i = parseTicket(++i, tokens);
          }
        }
      }

    }
    if (!addedPreTravel && !isQuote) {
      addPreTravelInfo();
    }
  }

  private BOOKING_TYPE getBookingType(String s) {
    if (s != null) {
      s = s.trim();
      Matcher m = datePattern.matcher(s);
      if (m.find()) {
        date = m.group();
        rank =0;//initialize rank for the new day;

        //let's check if this is the max date to prevent cross timezone as the last time to mess things up PUB-1026
        if (!maxDate.isEmpty()) {
          Timestamp d = getTimestamp(date, "12:00am");
          Timestamp mD = getTimestamp(maxDate, "12:00am");
          if (d.after(mD)) {
            maxDate = date;
          }
        } else {
          maxDate = date;
        }

        return BOOKING_TYPE.DATE;
      } else if (s.startsWith("ACCOMMODATION")) {
        rank++;
        return BOOKING_TYPE.HOTEL;
      }
      else if (s.startsWith("Flight - Departing")) {
        rank++;
        return BOOKING_TYPE.FLIGHT;
      }
      else if (s.startsWith("TRANSFER") || s.startsWith("BUS/SHUTTLE TRANSFER") || s.startsWith("PRIVATE CAR")) {
        rank++;
        return BOOKING_TYPE.TRANSFER;
      }
      else if (s.startsWith("CAR")) {
        rank++;
        return BOOKING_TYPE.CAR;
      }
      else if (s.startsWith("SIGHTSEEING")) {
        rank++;
        return BOOKING_TYPE.TOUR;
      }
      else if (s.equals("Own Arrangements")) {
        rank++;
        return BOOKING_TYPE.OWN_ARRANGEMENT;
      }
      else if (s.contains("After Hours Assistance")) {
        rank++;
        return BOOKING_TYPE.ASSISTANCE;
      }else if (s.contains("Stay connected") || s.contains("Tired of being overcharged")) {
        rank++;
        return BOOKING_TYPE.CONNECTED;
      } else if (s.startsWith("CRUISE")) {
        rank++;
        return BOOKING_TYPE.CRUISE;
      } else if (s.startsWith("RAIL")) {
        rank++;
        return BOOKING_TYPE.TRAIN;
      } else if (s.startsWith("TICKET")) {
        rank ++;
        return BOOKING_TYPE.TICKET;
      }

    }
    return BOOKING_TYPE.UNDEFINED;
  }

  private boolean containsDay(String s) {
    if (s != null) {
      s = s.toLowerCase();
      if (s.contains("monday") || s.contains("tuesday") || s.contains("wednesday") || s.contains("thursday") ||
              s.contains( "friday") || s.contains("saturday") || s.contains("sunday")) {
        return true;
      }
    }

    return false;
  }

  @Override
  public ArrayList<ParseError> getParseErrors() {
    return null;
  }

  private boolean skipLine (String s) {
    if (header.contains(s) || s.startsWith("AND PARTY") || (s.contains(" Page ") && s.contains(" of "))) {
      return true;
    }
    return false;
  }


  public int parseNote (int i, String[] tokens, String title) {
    NoteVO note = new NoteVO();
    note.setName(title);
    String lTitle = title.toLowerCase();
    if (lTitle.contains("assistance") || lTitle.contains("pre-travel") || lTitle.contains("terms and conditions") || lTitle.contains("own arrangement")) {
      note.setTimestamp(getTimestamp(date, "11:59p"));
    } else {
      note.setTimestamp(getTimestamp(date, "12:00a"));
    }
    note.setRank(rank);

    StringBuilder sb = new StringBuilder();
    int startIndex = i;
    while (true) {
      String s = tokens[i].trim();
      if (!skipLine(s)) {
        if (title.contains("Assistance") && s.startsWith("From ")) {
          //we have an issue parsing the contact table
          //From Australia From New Zealand From Tahiti From FijiQantas Airways 13 13 13 0800 808 767 43 06 65 672 2880Air New Zealand 13 24 76 0800 737 000 54 07 40 672 2955Virgin Australia 13 67 89 0800 670 000Jetstar 13 15 38 0800 800 995Air Tahiti Nui 1300 732 415 09 308 3360  46 03 03Fiji Airways 0800 230 150 0800 800 178  672 0888

        } else {
          if (s.equals(s.toUpperCase()) && s.length() > 0 && !s.equals("\n") && !s.equals("\r")) {
            sb.append("<br/>");
          }
          if (s.length() > 0 && !s.equals("\n") && !s.equals("\r")) {
            sb.append(Sanitizers.FORMATTING.sanitize(s)); //sanitize since notes are html snippets
            if (appendWhitespace(tokens, i).equals(" ")) {
              sb.append(" ");
            } else {
              sb.append("<br/>");
            }
          }
        }
      }
      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED)   {
          break;
        }
        else {
          i++;
        }

      }
    }
    note.setNote(sb.toString());
    trip.addNoteVO(note);
    return i;
  }

  public int parseTicket (int i, String[] tokens) {
    NoteVO note = new NoteVO();
    note.setTimestamp(getTimestamp(date, "12:00a"));

    note.setRank(rank);


    String name = tokens[i].trim();
    note.setName(name);
    i++;


    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (!skipLine(s)) {
        if (s.equals(s.toUpperCase()) && note.getName().equals(name)){
          note.setName(s);
          sb.append(name.replace(" Confirmed", "\nStatus: Confirmed").replace(" Quote", "\nStatus: Quote"));sb.append("\n");
          //sb.append(s);sb.append("\n");

        } else {
          if (s.contains("Confirmed")) {
            s = s.replace("Confirmed", "").trim();
            if (note.getName() == null || note.getName().trim().length() == 0) {
              note.setName(s);
            }
          }
          sb.append(s);
          sb.append(appendWhitespace(tokens, i));
        }
      }
      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }
    }

    if (note.getName() == null) {
      note.setName("Ticket");
    }
    note.setNote(sb.toString().replace("\n","<br/>"));

    //check to see if we can find the hotel that this should be attached to so that we can set the time properly
    if (trip.getHotels() != null && note.getTimestamp() != null) {
      String noteDate = Utils.formatDateControl(note.getTimestamp().getTime());
      for (HotelVO hotel: trip.getHotels()) {
        String hotelCheckin = Utils.formatDateControl(hotel.getCheckin().getTime());
        if (note.getName().equals(hotel.getName()) || note.getNote().toLowerCase().contains(hotel.getName().toLowerCase()) || noteDate.equals(hotelCheckin)) {
          note.setRank(hotel.getRank() + 1);
          break;
        }
      }
    }
    trip.addNoteVO(note);
    return i;
  }

  public int parseHotel(int i, String[] tokens) {
    HotelVO hotel = new HotelVO();
    hotel.setCheckin(getTimestamp(date, "4:00p"));
    hotel.setRank(rank);

    DateTime checkintDate = new DateTime(getTimestamp(date,"12:00p"));
    String checkoutDateStr = Utils.formatTimestamp(checkintDate.plusDays(1).getMillis(),"EEEE dd MMMM yyyy");
    hotel.setCheckout(getTimestamp(checkoutDateStr, "12:00p"));

    StringBuilder sb = new StringBuilder();
    int startIndex = i;
    while (true) {
      String s = tokens[i];
      s = s.replace("Confirmed", "");
      if (!skipLine(s)) {
        if (s.equals(s.toUpperCase()) && (hotel.getName() == null || hotel.getName().trim().isEmpty()) ) {
          hotel.setName(cleanName(s));
          if (s.length() > hotel.getName().length()) {
            String bookingCode = s.substring(s.indexOf(hotel.getName()) + hotel.getName().length()).trim();
            if (bookingCode != null && bookingCode.length() > 0) {
              sb.append("\nBooking Code: ");
              sb.append(bookingCode);
              sb.append("\n\n");
            }
          }

        }
        else if (s.contains("Check-in:") || s.contains("Check in:") || s.contains("Check In:")) {
          hotel.setCheckin(getTimestamp(date, parseTimestamp(s.substring(9).trim())));
        }
        else if (s.contains("For") && s.contains("night")) {
          String n = s.substring(4, s.indexOf("night")).trim();
          try {
            int numDays = Integer.parseInt(n);
            checkoutDateStr = Utils.formatTimestamp(checkintDate.plusDays(numDays).getMillis(),"EEEE dd MMMM yyyy");
            hotel.setCheckout(getTimestamp(checkoutDateStr, "12:00p"));
          } catch (Exception e) {

          }
        }
        else if (s.contains("Checkout required by")) {
          hotel.setCheckout(getTimestamp(checkoutDateStr, parseTimestamp(s)));
        }
        else if (s.contains("Supplier Reference:")) {
          hotel.setConfirmation(s.substring(s.indexOf("Supplier Reference:") + 19).trim());
        }
        else {
          if (s.contains("TICKET")) {
            sb.append("\n");
          }
          sb.append(s);
          if (s.contains("Meals Provided:")) {
            sb.append("\n\n");
          } else {
            sb.append(appendWhitespace(tokens, i));
          }
        }
      }

      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }
    }
    hotel.setNote(sb.toString());

    if (hotel.isValid()) {
      trip.addHotel(hotel);
    }

    return i;


  }

  public int parseFlight(int i, String[] tokens) {
    FlightVO flight = new FlightVO();
    StringBuilder sb = new StringBuilder();
    flight.setRank(rank);


    while (true) {
      String s = tokens[i];
      if (!skipLine(s)) {
        if (s.contains("Depart")) {
          s = s.replace("Confirmed","");
          flight.setDepatureTime(getTimestamp(date, parseTimestamp(s)));
          if (s.contains(" on ")) {
            String airportName = s.substring(s.indexOf("Depart")+6, s.indexOf(" on "));
            String terminal = null;
            if (airportName != null) {
              if (airportName.contains("Domestic Terminal")) {
                terminal = airportName.substring(airportName.indexOf("Domestic Terminal"));
                terminal = terminal.replace("Terminal", "");
                airportName = airportName.substring(0, airportName.indexOf("Domestic Terminal")).trim();
              } else if (airportName.contains("International Terminal")) {
                terminal = airportName.substring(airportName.indexOf("International Terminal"));
                terminal = terminal.replace("Terminal", "");
                airportName = airportName.substring(0, airportName.indexOf("International Terminal")).trim();
              } else if (airportName.contains("Terminal")) {
                terminal = airportName.substring(airportName.indexOf("Terminal"));
                terminal = terminal.replace("Terminal", "");
                airportName = airportName.substring(0, airportName.indexOf("Terminal")).trim();
              }

              AirportVO airportVO = new AirportVO();
              airportVO.setName(airportName);
              airportVO.setTerminal(terminal);
              flight.setDepartureAirport(airportVO);
            }

          }
          if (s.contains(" flight ")) {
            String fNum = s.substring(s.indexOf(" flight ") + 8).trim();
            if (fNum.length() > 2) {
              flight.setCode(fNum.substring(0, 2));
              flight.setNumber(fNum.substring(2));
              if (flight.getNumber() != null && flight.getNumber().length() == 2) {
                flight.setNumber("0" + flight.getNumber());
              } else if (flight.getNumber() != null && flight.getNumber().length() == 1) {
                flight.setNumber("00" + flight.getNumber());
              }
            }
          }
        } else if (s.contains("Arrive")) {
          flight.setArrivalTime(getTimestamp(date, parseTimestamp(s)));
          String airportName = s.substring(s.indexOf("Arrive ")+7);
          String terminal = null;
          if (airportName != null) {
            if (airportName.contains("Domestic Terminal")) {
              terminal = airportName.substring(airportName.indexOf("Domestic Terminal"));
              terminal = terminal.replace("Terminal", "");
              airportName = airportName.substring(0, airportName.indexOf("Domestic Terminal")).trim();
            } else if (airportName.contains("International Terminal")) {
              terminal = airportName.substring(airportName.indexOf("International Terminal"));
              terminal = terminal.replace("Terminal", "");
              airportName = airportName.substring(0, airportName.indexOf("International Terminal")).trim();
            } else if (airportName.contains("Terminal")) {
              terminal = airportName.substring(airportName.indexOf("Terminal"));
              terminal = terminal.replace("Terminal", "");
              airportName = airportName.substring(0, airportName.indexOf("Terminal")).trim();
            }

            AirportVO airportVO = new AirportVO();
            airportVO.setName(airportName);
            airportVO.setTerminal(terminal);
            flight.setArrivalAirport(airportVO);
          }


        } else if (s.contains("PNR Number:")) {
          flight.setReservationNumber(s.substring(s.indexOf("PNR Number:") + 11).trim());
        } else {
          if (!s.contains("Flight - Arriving")) {
            sb.append(s);
            sb.append("\n");
          }
        }

      }


      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (flight.getArrivalTime() == null && getBookingType(nextLine) == BOOKING_TYPE.DATE) {
          i++;
        } else if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }
      }
    }

    //flights with the amaedus PNR will be retrieved via the PNR lookup to avoid duplicates
    if (flight.isValid()
            && (travelers.isEmpty() ||
            amadeusPNR == null ||
            amadeusPNR.isEmpty() ||
            flight.getReservationNumber() == null ||
            !flight.getReservationNumber().equals(amadeusPNR) ||
            (flight.getDepatureTime() != null && flight.getDepatureTime().getTime() <  Instant.now().toEpochMilli())
        )) {
      if (flight.getName() != null && flight.getName().contains("Confirmed")) {
        flight.setName(flight.getName().substring(0, flight.getName().indexOf("Confirmed")).trim());
      }
      //add the travelers to the flight
      if (!travelers.isEmpty()) {
        for (PassengerVO pVO: travelers) {
          flight.addPassenger(pVO);
        }
      }
      trip.addFlight(flight);
    }

    return i;
  }

  public String cleanFirstName  (String firstName) {
    firstName = firstName.replace("MISS","");
    firstName = firstName.replace("MS","");
    firstName = firstName.replace("MR","");
    firstName = firstName.replace("MRS", "");
    firstName = firstName.trim();
    return firstName;
  }

  public int parseTransfers(int i, String[] tokens) {
    TransportVO transport = new TransportVO();
    transport.setBookingType(ReservationType.TRANSPORT);
    transport.setCmpyName("");
    transport.setRank(rank);

    String name = tokens[i].trim();
    Matcher timeMatcher = timePattern.matcher(tokens[i]);
    Timestamp start = null;
    Timestamp end = null;
    String startStr = null;
    while (timeMatcher.find()) {
      String s2 = timeMatcher.group();
      if (startStr == null) {
        startStr = s2;
        start = getTimestamp(date, s2);
      } else {
        Timestamp t = getTimestamp(date, s2);
        if (t.after(start)) {
          end = t;
        }
      }
    }
    if (startStr != null && name.startsWith(startStr)) {
      name = name.replace(startStr, "");
      if (name.startsWith("m ")) {
        name = name.substring(2).trim();
      }
    }


    if (start == null) {
      start= getTimestamp(date, "12:00a");
    }

    transport.setPickupDate(start);
    transport.setDropoffDate(end);
    transport.setName(name);
    i++;


    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (!skipLine(s)) {
        if (s.equals(s.toUpperCase()) && transport.getName() == null) {
          transport.setCmpyName(s.trim());
        } else if (s.contains("Supplier Reference:")) {
          transport.setConfirmationNumber(s.substring(s.indexOf("Supplier Reference:") + 19).trim());
        } else if (s.equals(s.toUpperCase()) && transport.getName().equals(name)){
          transport.setName(cleanName(s));
          sb.append(name.replace(" Confirmed", "\nStatus: Confirmed").replace(" Quote", "\nStatus: Quote"));sb.append("\n");
          //sb.append(s);sb.append("\n");
          if (s.length() > transport.getName().length()) {
            String bookingCode = s.substring(s.indexOf(transport.getName()) + transport.getName().length()).trim();
            if (bookingCode != null && bookingCode.length() > 0) {
              sb.append("\nBooking Code: ");
              sb.append(bookingCode);
              sb.append("\n\n");
            }
          }
        } else {
          if (s.contains("Confirmed")) {
            s = s.replace("Confirmed", "").trim();
            if (transport.getName() == null || transport.getName().trim().length() == 0) {
              transport.setName(s);
            }
          }
          sb.append(s);
          sb.append(appendWhitespace(tokens, i));
        }
      }
      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }
    }
    if (transport.getName() == null) {
      transport.setName("Transfer");
    }

    transport.setNote(sb.toString());

    if (transport.isValid()) {
      trip.addTransport(transport);
    }

    return i;
  }

  public int parseRail(int i, String[] tokens) {
    TransportVO transport = new TransportVO();
    transport.setBookingType(ReservationType.RAIL);
    transport.setRank(rank);

    String name = tokens[i].trim();
    Matcher timeMatcher = timePattern.matcher(tokens[i]);
    Timestamp start = null;
    Timestamp end = null;
    String startStr = null;
    while (timeMatcher.find()) {
      String s2 = timeMatcher.group();
      if (startStr == null) {
        startStr = s2;
        start = getTimestamp(date, s2);
      } else {
        Timestamp t = getTimestamp(date, s2);
        if (t.after(start)) {
          end = t;
        }
      }
    }
    if (startStr != null && name.startsWith(startStr)) {
      name = name.replace(startStr, "");
      if (name.startsWith("m ")) {
        name = name.substring(2).trim();
      }
    }


    if (start == null) {
      start= getTimestamp(date, "12:00a");
    }

    transport.setPickupDate(start);
    transport.setDropoffDate(end);
    transport.setName(name);
    i++;


    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (!skipLine(s)) {
        if (s.equals(s.toUpperCase()) && transport.getName() == null) {
          transport.setCmpyName(s.trim());
        } else if (s.contains("Supplier Reference:")) {
          transport.setConfirmationNumber(s.substring(s.indexOf("Supplier Reference:") + 19).trim());
        } else if (s.equals(s.toUpperCase()) && transport.getName().equals(name)){
          transport.setName(cleanName(s));
          sb.append(name.replace(" Confirmed", "\nStatus: Confirmed").replace(" Quote", "\nStatus: Quote"));sb.append("\n");
          sb.append(s);sb.append("\n");
          if (s.length() > transport.getName().length()) {
            String bookingCode = s.substring(s.indexOf(transport.getName()) + transport.getName().length()).trim();
            if (bookingCode != null && bookingCode.length() > 0) {
              sb.append("\nBooking Code: ");
              sb.append(bookingCode);
              sb.append("\n\n");
            }
          }
        } else {
          if (s.contains("Confirmed")) {
            s = s.replace("Confirmed", "").trim();
            if (transport.getName() == null || transport.getName().trim().length() == 0) {
              transport.setName(s);
            }
          }
          sb.append(s);
          sb.append(appendWhitespace(tokens, i));
        }
      }
      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }
    }
    if (transport.getName() == null) {
      transport.setName("Transfer");
    }

    transport.setNote(sb.toString());

    if (transport.isValid()) {
      trip.addTransport(transport);
    }

    return i;
  }

  public int handleCruise(int i, String[] tokens) {
    //let's look for next couple of lines to see if this is a 1 day trip or a proper cruise
    boolean treatAsActivity = false;
    int numOfNights = 0;
    for (int j = i; i < tokens.length; j++) {
      String s = tokens[i];
      if (s != null && (s.toLowerCase().contains("night") || s.toLowerCase().contains("day"))) {
        Matcher matcher = numPattern.matcher(s);
        if (matcher.find()) {
          String m = matcher.group();
          System.out.println("----- " + m);
          if (StringUtils.isNumeric(m)) {
            numOfNights = Integer.parseInt(m);
            if (numOfNights == 1) {
              treatAsActivity = true;
            }

          }
        }
        break;
      } else if ((j - i) > 5) { //if we scanned 5 lines - we should give up
         break;
      }
    }

    if (treatAsActivity || numOfNights == 0) {
      return parseCruiseAsActivity(i, tokens);
    } else {
      return parseCruise(i, tokens, numOfNights);
    }
  }

  public int parseCruise(int i, String[] tokens, int numOfNights) {
    if (numOfNights < 0) {
      numOfNights = 0;
    }
    CruiseVO cruise = new CruiseVO();
    cruise.setRank(rank);

    String name = tokens[i].trim();
    if (name.indexOf(" for ") > 0 && name.contains("nights")) {
      name = name.substring(0, name.indexOf(" for "));
    }
    cruise.setTimestampDepart(getTimestamp(date, "12:00a"));
    cruise.setTimestampArrive(Timestamp.from(cruise.getTimestampDepart().toInstant().plus(numOfNights, ChronoUnit.DAYS)));

    cruise.setName(name);
    i++;


    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (!skipLine(s)) {
        if (s.equals(s.toUpperCase()) && cruise.getCompanyName() == null) {
          System.out.println("------- " + s);
          cruise.setCompanyName(s.trim());
        } else if (s.contains("Supplier Reference:")) {
          cruise.setConfirmationNo(s.substring(s.indexOf("Supplier Reference:") + 19).trim());
        }else {
          if (s.contains("Confirmed")) {
            s = s.replace("Confirmed", "").trim();
            if (cruise.getName() == null || cruise.getName().trim().length() == 0) {
              cruise.setName(s);
            }
          }
          sb.append(s);
          sb.append(appendWhitespace(tokens, i));
        }
      }
      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }
    }


    cruise.setNote(sb.toString());

    if (cruise.isValid()) {
      trip.addCruise(cruise);
    }

    return i;
  }

  public int parseCruiseAsActivity(int i, String[] tokens) {
    ActivityVO activity = new ActivityVO();
    activity.setBookingType(ReservationType.ACTIVITY);
    activity.setRank(rank);

    String name = tokens[i].trim();
    if (name.indexOf(" for ") > 0 && name.contains("nights")) {
      name = name.substring(0, name.indexOf(" for "));
    }

    activity.setStartDate(getTimestamp(date, "12:00a"));
    activity.setEndDate(null);
    activity.setName(name);
    i++;


    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      System.out.println(s);
      if (!skipLine(s)) {
        if (s.contains("Supplier Reference:")) {
          activity.setConfirmation(s.substring(s.indexOf("Supplier Reference:") + 19).trim());
        } else if (s.contains("Pick up from")) {
          activity.setStartLocation(s.substring(s.indexOf("Pick up from") + 12 ).trim());
          sb.append(s);
          sb.append(appendWhitespace(tokens, i));
        }  else if (s.equals(s.toUpperCase()) && activity.getName().equals(name)){
          activity.setName(cleanName(s));
          sb.append(name.replace(" Confirmed", "\nStatus: Confirmed").replace(" Quote", "\nStatus: Quote"));sb.append("\n");
          sb.append(s);sb.append("\n");
          if (s.length() > activity.getName().length()) {
            String bookingCode = s.substring(s.indexOf(activity.getName()) + activity.getName().length()).trim();
            if (bookingCode != null && bookingCode.length() > 0) {
              sb.append("\nBooking Code: ");
              sb.append(bookingCode);
              sb.append("\n\n");
            }
          }
        } else {
          if (s.contains("Confirmed")) {
            s = s.replace("Confirmed", "").trim();
            if (activity.getName() == null || activity.getName().trim().length() == 0) {
              activity.setName(s);
            }
          }
          sb.append(s);
          sb.append(appendWhitespace(tokens, i));
        }
      }
      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }
    }


    activity.setNote(sb.toString());

    if (activity.isValid()) {
      trip.addActivity(activity);
    }

    return i;
  }

  public int parseCar(int i, String[] tokens) {
    TransportVO transport = new TransportVO();
    transport.setBookingType(ReservationType.CAR_RENTAL);
    transport.setRank(rank);

    DateTime pickupDate = new DateTime(getTimestamp(date,"12:00p"));
    String dropoffDateStr = Utils.formatTimestamp(pickupDate.plusDays(1).getMillis(),"EEEE dd MMMM yyyy");


    transport.setPickupDate(getTimestamp(date,"12:00a"));



    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (!skipLine(s)) {
        if (s.equals(s.toUpperCase()) && transport.getName() == null) {
          transport.setCmpyName(s.trim());
          transport.setName(s.trim());
          sb.append(s);sb.append("\n");

        } else if (s.contains(" picked up at ")) {
          transport.setPickupDate(getTimestamp(date,parseTimestamp(s)));
        }
        else if (s.contains(" for ") && s.contains(" days")) {
          try {
            String num = s.substring(s.indexOf(" for ") + 5, s.indexOf(" days")).trim();
            int numDays = Integer.parseInt(num);
            dropoffDateStr = Utils.formatTimestamp(pickupDate.plusDays(numDays).getMillis(),"EEEE dd MMMM yyyy");
          } catch (Exception e) {

          }
          sb.append(s);sb.append("\n");
        } else if (s.contains(" returned at ")) {
          transport.setDropoffDate(getTimestamp(dropoffDateStr, parseTimestamp(s)));
          if (transport.getDropoffDate() != null && transport.getPickupDate() != null) {
            LocalDateTime d = LocalDateTime.ofInstant(transport.getDropoffDate().toInstant(), ZoneId.systemDefault());
            LocalDateTime p = LocalDateTime.ofInstant(transport.getPickupDate().toInstant(), ZoneId.systemDefault());
            if (d.getHour() > p.getHour()) {
              //since the drop off time is after the pickup time, we adjust the date by 1 day
              //i.e the drop off date is one less day than the number of days of the rental because the dropoff time is after
              transport.setDropoffDate(new Timestamp(transport.getDropoffDate().toInstant()
                                                                              .minus(1, ChronoUnit.DAYS)
                                                                              .toEpochMilli()));
            }
          }
        } else if (s.startsWith("Pick up from")) {
          transport.setPickupLocation(s.substring(s.indexOf("Pick up from") + 12).trim());
          sb.append("\n");sb.append(s);sb.append("\n");
        } else if (s.startsWith("Drop off at")) {
          transport.setDropoffLocation(s.substring(s.indexOf("Drop off at") + 11).trim());
          sb.append("\n");sb.append(s);sb.append("\n");
        }  else if (s.contains("Supplier Reference:")) {
          transport.setConfirmationNumber(s.substring(s.indexOf("Supplier Reference:") + 19).trim());
        }else {
          if (s.contains("Code:") || s.contains("Billing Account Number:") || s.startsWith("All ") || s.contains("Includes:")) {
            sb.append("\n");
          }
          sb.append(s);
          sb.append(appendWhitespace(tokens, i));
        }
      }
      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }
    }
    if (transport.getName() == null || transport.getName().isEmpty()) {
      transport.setName("Car Rental");
    }

    transport.setNote(sb.toString());

    if (transport.isValid()) {
      trip.addTransport(transport);
    }

    return i;
  }

  public int parseTours(int i, String[] tokens) {
    ActivityVO activity = new ActivityVO();
    activity.setBookingType(ReservationType.ACTIVITY);
    activity.setRank(rank);

    String name = tokens[i].trim();
    Matcher timeMatcher = timePattern.matcher(tokens[i]);
    Timestamp start = null;
    Timestamp end = null;
    String startStr = null;
    while (timeMatcher.find()) {
      String s2 = timeMatcher.group();
      if (startStr == null) {
        startStr = s2;
        start = getTimestamp(date, s2);
      } else {
        Timestamp t = getTimestamp(date, s2);
        if (t.after(start)) {
          end = t;
        }
      }
    }
    if (startStr != null && name.startsWith(startStr)) {
      name = name.replace(startStr, "");
      if (name.startsWith("m ")) {
        name = name.substring(2).trim();
      }
    }

    if (start == null) {
      start= getTimestamp(date, "12:00a");
    }

    activity.setStartDate(start);
    activity.setEndDate(end);
    activity.setName(name);

    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (!skipLine(s)) {
        if (s.startsWith("Start time:")) {
          timeMatcher = timePattern.matcher(tokens[i]);
          start = null;
          end = null;
          while (timeMatcher.find()) {
            String s2 = timeMatcher.group();
            if (start == null) {
              start = getTimestamp(date, s2);
            } else {
              Timestamp t = getTimestamp(date, s2);
              if (t.after(start)) {
                end = t;
              }
            }
          }
          if (start != null) {
            activity.setStartDate(start);
          }
          if (end != null) {
            activity.setEndDate(end);
          }
        } else if (s.contains("Supplier Reference:")) {
          activity.setConfirmation(s.substring(s.indexOf("Supplier Reference:") + 19).trim());
        }  else if (!s.contains("~~~~~") && s.equals(s.toUpperCase()) && activity.getName().equals(name)){
          activity.setName(cleanName(s));
          sb.append(name.replace(" Confirmed", "\nStatus: Confirmed").replace(" Quote", "\nStatus: Quote"));sb.append("\n");
          sb.append(s);sb.append("\n");
          if (s.length() > activity.getName().length()) {
            String bookingCode = s.substring(s.indexOf(activity.getName()) + activity.getName().length()).trim();
            if (bookingCode != null && bookingCode.length() > 0 && !bookingCode.equals("LTD")) {
              sb.append("\nBooking Code: ");
              sb.append(bookingCode);
              sb.append("\n\n");
            }
          }
        } else if ((name == null || name.isEmpty()) && s.contains("Confirmed")) {
          name = s.substring(0, s.indexOf("Confirmed")).trim();
          timeMatcher = timePattern.matcher(name);
          start = null;
          while (timeMatcher.find()) {
            String s2 = timeMatcher.group();
            if (start == null) {
              start = getTimestamp(date, s2);
              if (name.indexOf("am") > 0 ) {
                name = name.substring(name.indexOf("am") + 2).trim();
              } else if (name.indexOf("pm") > 0 ) {
                name = name.substring(name.indexOf("pm") + 2).trim();
              }
            }
          }
          if (start != null) {
            activity.setStartDate(start);
          }
          activity.setName(name);

        }else {
          if (!s.equals(name)) {
            sb.append(s);
            sb.append(appendWhitespace(tokens, i));
          }
        }
      }
      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }

    }
    if (activity.getName() == null || activity.getName().isEmpty()) {
      activity.setName("Activity");
    }
    activity.setName(activity.getName().replace(" Confirmed","").replace(" Quote",""));
    activity.setNote(sb.toString());

    if (activity.isValid()) {
      trip.addActivity(activity);
    }

    return i;

  }

  private String parseTimestamp(String s) {
    if (s != null) {
      if (s.contains("pm")) {
        s = s.replace("pm", "p");
      } else if (s.contains("am")) {
        s = s.replace("am", "a");
      }
      if (s.contains(".")) {
        s=s.replace(".", ":");
      }

      Matcher m = timePattern.matcher(s);
      if (m.find()) {
        return m.group();
      }

      if (s.contains("12:00") || s.toLowerCase().contains("midday")) {
        return "12:00p";
      }
    }
    return "12:00a";
  }

  private Timestamp parseTimestamp(String s, String keyword) {
    if (s != null && keyword != null) {
      String date = s.substring(s.indexOf(keyword) + keyword.length());
      String time = "";
      if (date.contains(" at ")) {
        time = date.substring(date.indexOf(" at ") + 4).trim();
        if (time.length() > 5) {
          Matcher m = timePattern.matcher(time);
          if (m.find()) {
            time = m.group();
          }
        }
        date = date.substring(0, date.indexOf(" at "));
      }
      else if (date.length() > 13) {
        date = date.substring(0, 14).trim();
      }
      if (time.length() == 5) {
        return getTimestamp(date.trim(), time);
      }
      else {
        return getTimestamp(date.trim(), "00:00");
      }
    }
    return null;
  }

  private Timestamp getTimestamp(String date, String time) {
    try {
      if (time.contains("a") && !time.contains("am")) {
        time = time.replace("a", "am");
      } else if (time.contains("p") && !time.contains("pm")) {
        time = time.replace("p", "pm");
      }
      Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime());
      return t;
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Get the PDF content as String
   *
   * @return pdf content as String
   */
  private static final String getPDFContentAsString(InputStream fis) {
    PDDocument pdfDocument = null;
    StringBuilder contents = new StringBuilder();
    try {
      PDFParser parser = new PDFParser(fis);
      parser.parse();

      pdfDocument = parser.getPDDocument();

      PDFTextStripperByArea stripper = new PDFTextStripperByArea();

      stripper.setSortByPosition(true);
      Rectangle rect = new Rectangle(0, 40, 650, 730);
      stripper.addRegion("body", rect);

      java.util.List<?> allPages = pdfDocument.getDocumentCatalog().getAllPages();

      for (Object pdPage : allPages) {
        stripper.extractRegions((PDPage) pdPage);
        contents.append(stripper.getTextForRegion("body"));
      }
    }
    catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (pdfDocument != null) {
        try {
          pdfDocument.close();
        } catch (Exception e) {

        }
      }
    }
    return contents.toString();
  }

  private String appendWhitespace (String [] lines, int pos) {
    //decide to append a white space or a line break based on whether the current line ends with a. and the next line starts with a capital
    if (lines.length > pos + 1) {
      String curLine = lines[pos].trim();
      String nextLine = lines[pos + 1].trim();
      if (curLine.contains("TEL:") || curLine.endsWith(".") || curLine.length() == 0 || nextLine.length() == 0
              || (Character.isUpperCase(curLine.charAt(curLine.length() - 1)) && Character.isUpperCase(nextLine.charAt(0)))
              || (!Character.isAlphabetic(curLine.charAt(curLine.length() - 1)) && curLine.charAt(curLine.length() - 1) != '-'  && Character.isUpperCase(nextLine.charAt(0))) ) {
        return "\n\n";
      } else if (!finishesWithSingleChar (curLine) && numChar(nextLine, "-") < 3 && Character.isUpperCase(curLine.charAt(0))  && nextLine.length() > 0 && Character.isUpperCase(nextLine.charAt(0)) && numChar(curLine, " ") > 3  && numChar(nextLine, " ") > 3) {
        return "\n";
      } else if (curLine.contains("For ") && curLine.contains(" adults")) {
        return "\n";
      }else {
        return " ";
      }
    }
    return "\n";
  }

  private int numChar (String s, String c) {
    int i = 0;

    if (s != null && s.trim().contains(c)) {
      i = s.split(c).length;
    }
    return i;
  }

  private boolean finishesWithSingleChar (String s) {

    if (s != null && s.contains(" ")) {
      String[] tokens = s.split(" ");
      if (tokens[tokens.length - 1].length() == 1) {
        return true;
      }
    }
    return false;
  }

  private String cleanName (String s) {
    //attempt to remove the booking or confim code from the hotel name
    //HOLIDAY INN DARLING HARBOUR 2FBF
    if (s != null) {
      String[] tokens = s.trim().split(" ");
      if (tokens.length > 1) {
        String lastToken = tokens[tokens.length - 1];
        boolean skipLastToken = false;
        if (lastToken.length() > 2 && !lastToken.contains("LTD")) {
          if(!StringUtils.isAlpha(lastToken)) {
            //token has some numbers in it - unlikely to be a name
            skipLastToken = true;
          } else if (!lastToken.contains("A") && !lastToken.contains("E") && !lastToken.contains("I") && !lastToken.contains("O") && !lastToken.contains("U")) {
            //no vowels - unlikely to be a name
            skipLastToken = true;
          } else if (lastToken.equals("INBOUND")) {
            skipLastToken = true;
          }
        }

        if (skipLastToken) {
          return s.replace(lastToken,"").trim();
        }
      }

    }

    return s;
  }


  public void  addPreTravelInfo () {
    StringBuilder sb = new StringBuilder();

    sb.append("If you have any questions regarding this booking, prior to departure, " +
              "please contact your travel agent. If you need emergency assistance while traveling, " +
              "please refer to our phone numbers page at the end of this itinerary.");
    sb.append("<br/><br/>");

    sb.append("This itinerary is the land documentation you are required to hold for travel. You must keep the " +
              "complete itinerary as confirmation of your booking for the duration of your vacation. If no vouchers " +
              "are included with your documents, this means that all suppliers have agreed to accept the e-document " +
              "itinerary. If vouchers have been provided, please exchange these for your land arrangements as " +
              "specified. If we have issued tickets for your air travel, please present your ticket or E-ticket " +
              "itinerary receipt upon check-in for your flights.");
    sb.append("<br/><br/>");

    sb.append("We strongly recommend that you reconfirm your arrangements with the various tour operators. Although " +
              "your services are confirmed, unless otherwise stated, there is always a possibility of a last minute " +
              "change. Additionally, the day prior to the service, the supplier will be able to advise you of the " +
              "exact pickup time. If you would like help with this, the hotel concierge or front desk should be able " +
              "to assist you.");
    sb.append("<br/><br/>");

    sb.append("You will need a current passport valid for at least 6 months after the return date. For travel to " +
              "Australia, an entry visa (ETA) is required. Other visas may be required to other destinations. Please " +
              "check with the appropriate government office, web-site or your travel agent.");
    sb.append("<br/><br/>");

    sb.append("We suggest that you check your flights 24 hours prior to departure directly online with the airline or" +
              " with your travel agent. Please check-in for your international flights at least 3 hours prior to " +
              "departure; domestic flights about 90 minutes prior.");
    sb.append("<br/><br/>");

    sb.append("Luggage allowance: The allowed number of bags varies from airline to airline and from Economy Class to" +
              " First/Business Class. Please visit the airline's web-site or call the airline for the exact details " +
              "of the baggage and carry-on allowances (including size and weight restrictions). Fees may apply for " +
              "additional pieces of luggage or for bags above weight limitation for free check-in.");
    sb.append("<br/><br/>");

    sb.append(" If you are traveling by small aircraft, helicopter, train, catamaran, " +
              "launch or coach during your trip, there may be additional luggage restrictions. Please check with your" +
              " travel agent consultant or the transport services directly for more details.");
    sb.append("<br/><br/>");


    NoteVO note = new NoteVO();
    note.setName("Important Pre-Travel Information");
    note.setTimestamp(getTimestamp(maxDate, "11:59p"));
    note.setRank(rank++);
    note.setNote(sb.toString());
    trip.addNoteVO(note);
  }

  public void  addTravel2QuoteInfo () {
    StringBuilder sb = new StringBuilder();

    sb.append("Thank you for asking Travel 2 to provide you with a customized itinerary for your dream travel experience.\n" +
              "\n" +
              "Please review this document carefully to ensure that we are providing the best possible travel " +
              "products and services to suit your individual needs.\n" +
              "\n" +
              "FOR QUOTES: no space has been requested or held. To book a non-refundable deposit of $300 per person " +
              "is required. Some products, such as those outside of our normal product line, " +
              "require a higher deposit amount; this will be advised at time of booking.\n" +
              "We reserve the right to change our prices, itineraries and routing without notice. If you have asked " +
              "us to price airfare, then US/Foreign Taxes, including September 11 Security Fee are included. Prices " +
              "are current at the time of BOOKING. Airlines and hotels frequently change their prices as a result of " +
              "fare increases and/or fuel surcharges. PRICES MAY INCREASE BEFORE FULL PAYMENT FOR THOSE AND OTHER " +
              "REASONS AND, BY PAYING YOUR DEPOSIT, YOU AFFIRMATIVELY AGREE TO THE POSSIBILITY\n" +
              "OF SUCH INCREASES. AFTER FULL PAYMENT, PRICES ARE GUARANTEED EXCEPT ONLY DUE TO AN INCREASE IN " +
              "GOVERNMENT IMPOSED TAXES AND FEES AND BY MAKING SUCH PAYMENT YOU AFFIRMATIVELY AGREE TO THE " +
              "POSSIBILITY OF SUCH INCREASE.\n" +
              "We offer a Price Protection Policy for an additional $25 per person. Purchase of the Price Protection " +
              "Policy at time of deposit guarantees your price for the land arrangements as quoted will not increase " +
              "due to currency fluctuation. Please note: Any changes made to land itineraries after deposit is " +
              "received will cause the itinerary to be re-priced using the existing currency exchange rates. The " +
              "Price Protection Policy specifically excludes any new government taxes or fees made or imposed after " +
              "the date of your original quotation.  Airfares are protected from the date that the airfare is paid in" +
              " full and the tickets are issued. Changes made to air itineraries after ticketing will incur airline " +
              "change fees.\n" +
              "\n" +
              "FINAL PAYMENT: Final payment is required 45 days prior to departure. We accept most credit cards for " +
              "payments. Personal checks are also acceptable for payment, but must be received at least 21 days prior" +
              " to travel.\n" +
              "\n" +
              "AMENDMENT FEES: There is a $100 amendment fee each time an itinerary is revised after invoicing. " +
              "Amendments made after documents have been issued will incur a $150 fee to cover courier costs, " +
              "communication costs and additional time to co-ordinate the changes.\n" +
              "\n" +
              "CANCELLATION FEES: Cancellation fees are based the following schedule:\n" +
              "45 Days or more prior to departure - Loss of deposit; Between 44-21 Days - 20% of total cost;\n" +
              "Between 20-10 Days - 35% of total cost; Between 9-1 Days - 60% of total cost; Day of departure - 100% " +
              "of total cost.\n" +
              "Separate cancellation policies/fees apply to select products and may be up to 100% of total price.\n" +
              "\n" +
              "Unused travel arrangements or travel arrangements cancelled, amended or altered once travel has " +
              "commenced do not qualify for any refund.\n" +
              "\n" +
              "INSURANCE AND \"NO WORRIES\" WAIVERS: We offer you affordable and comprehensive insurance plans. " +
              "Please click here for policy details. Please note if you wish to take advantage of the Pre Existing " +
              "Medical Condition Coverage, your insurance premium must be paid within 14 days of your initial deposit" +
              " payment for your trip. Insurance and the \"No Worries Waiver\" may not be added after final payment" +
              ".\n" +
              "\n" +
              "\n" +
              "\n" +
              "FOLLOW-UP CHECK LIST:\n" +
              "Names - The correct spelling of your names must be provided as they appear on your passports. Name " +
              "changes resulting in the reissuing of tickets will incur a $150 per person charge, " +
              "as well as any fees charged by the airlines.\n" +
              "\n" +
              "Passports and Visitors Visas - All passengers are required to travel with a passport valid for six " +
              "months after their return. Visitors to Australia are required to have an entry visa. For a minimal fee" +
              " of $25, we can issue an Australian visa for U.S. and Canadian citizens and require a copy of your " +
              "passport to process.\n" +
              "\n" +
              "International Flight and Ticket Numbers - Please note if we are not issuing the international tickets," +
              " correct flight information and ticket numbers are required in order for us to arrange applicable " +
              "transfers and domestic air tickets.\n" +
              "\n" +
              "Special Meal requests - Please advise of any special meal requests or dietary requirements.\n" +
              "\n" +
              "Seating Preference - Please note that seat requests can never be guaranteed, " +
              "however we will contact the airlines with your preferences. Some airlines charge a fee to book " +
              "advanced seat assignments.\n" +
              "\n" +
              "Bedding request - Please advise if you prefer double, twin, triple or a single bedded rooms.\n" +
              "\n" +
              "For further information on Travel 2, please visit our website, " +
              "listed below or email us at info@travel2-us.com. Our hours are Monday through Friday, " +
              "6:30 A.M. to 5:30 P.M. and Saturday 7:00 A.M. to 4:00P.M., Pacific Standard Time. Our toll free phone " +
              "number is 888\n" +
              "280 2536.\n" +
              "Thank you for considering Travel 2, we are confident this trip will be your best travel experience " +
              "ever! For full Terms and Conditions, visit http://www.travel2us.com/terms.asp\n" +
              "");

    NoteVO note = new NoteVO();
    note.setName("IMPORTANT INFORMATION");
    note.setTimestamp(getTimestamp(maxDate, "11:59p"));
    note.setRank(rank++);
    note.setNote(sb.toString().replace("\n","<br>"));
    trip.addNoteVO(note);
  }

  public void  addQantasQuoteInfo () {
    StringBuilder sb = new StringBuilder();

    sb.append("Thank you for asking Qantas Vacations to provide you with a customized itinerary for your dream travel experience.\n" +
              "\n" +
              "Please review this document carefully to ensure that we are providing the best possible travel " +
              "products and services to suit your individual needs.\n" +
              "\n" +
              "FOR QUOTES: no space has been requested or held. To book a non-refundable deposit of $300 per person " +
              "is required. Some products, such as those outside of our normal product line, " +
              "require a higher deposit amount; this will be advised at time of booking.\n" +
              "We reserve the right to change our prices, itineraries and routing without notice. If you have asked " +
              "us to price airfare, then US/Foreign\n" +
              "Taxes, including September 11 Security Fee are included. Prices are current at the time of BOOKING. " +
              "Airlines and hotels frequently change their prices as a result of fare increases and/or fuel " +
              "surcharges. PRICES MAY INCREASE BEFORE FULL PAYMENT FOR THOSE AND OTHER REASONS AND, " +
              "BY PAYING YOUR DEPOSIT, YOU AFFIRMATIVELY AGREE TO THE POSSIBILITY\n" +
              "OF SUCH INCREASES. AFTER FULL PAYMENT, PRICES ARE GUARANTEED EXCEPT ONLY DUE TO AN INCREASE IN " +
              "GOVERNMENT IMPOSED TAXES AND FEES AND BY MAKING SUCH PAYMENT YOU AFFIRMATIVELY AGREE TO THE " +
              "POSSIBILITY OF SUCH INCREASE.\n" +
              "\n" +
              "We offer a Price Protection Policy for an additional $25 per person. Purchase of the Price Protection " +
              "Policy at time of deposit guarantees your price for the land arrangements as quoted will not increase " +
              "due to currency fluctuation. Please note: Any changes made to land itineraries after deposit is " +
              "received will cause the itinerary to be re-priced using the existing currency exchange rates. The " +
              "Price Protection Policy specifically excludes any new government taxes or fees made or imposed after " +
              "the date of your original quotation.  Airfares are protected from the date that the airfare is paid in" +
              " full and the tickets are issued. Changes made to air itineraries after ticketing will incur airline " +
              "change fees.\n" +
              "\n" +
              "FINAL PAYMENT: Final payment is required 45 days prior to departure. We accept most credit cards for " +
              "payments. Personal checks are also acceptable for payment, but must be received at least 21 days prior" +
              " to travel.\n" +
              "\n" +
              "CANCELLATION FEES: Cancellation fees are based the following schedule:\n" +
              "45 Days or more prior to departure - Loss of deposit; Between 44-21 Days - 20% of total cost;\n" +
              "Between 20-10 Days - 35% of total cost; Between 9-1 Days - 60% of total cost; Day of departure - 100% " +
              "of total cost.\n" +
              "Separate cancellation policies/fees apply to select products and may be up to 100% of total price.\n" +
              "\n" +
              "Unused travel arrangements or travel arrangements cancelled, amended or altered once travel has " +
              "commenced do not qualify for any refund.\n" +
              "\n" +
              "INSURANCE AND \"NO WORRIES\" WAIVERS: We offer you affordable and comprehensive insurance plans. " +
              "Please click here for policy details. Please note if you wish to take advantage of the Pre Existing " +
              "Medical Condition Coverage, your insurance premium must be paid within 14 days of your initial deposit" +
              " payment for your trip. Insurance and the \"No Worries Waiver\" may not be added after final payment" +
              ".\n" +
              "\n" +
              "\n" +
              "\n" +
              "\n" +
              "FOLLOW-UP CHECK LIST:\n" +
              "Names - The correct spelling of your names must be provided as they appear on your passports. Name " +
              "changes resulting in the reissuing of tickets will incur a $150 per person charge, " +
              "as well as any fees charged by the airlines.\n" +
              "\n" +
              "Passports and Visitors Visas - All passengers are required to travel with a passport valid for six " +
              "months after their return. Visitors to Australia are required to have an entry visa. For a minimal fee" +
              " of $25, we can issue an Australian visa for U.S. and Canadian citizens and require a copy of your " +
              "passport to process.\n" +
              "\n" +
              "International Flight and Ticket Numbers - Please note if we are not issuing the international tickets," +
              " correct flight information and ticket numbers are required in order for us to arrange applicable " +
              "transfers and domestic air tickets.\n" +
              "\n" +
              "Special Meal requests - Please advise of any special meal requests or dietary requirements.\n" +
              "\n" +
              "Seating Preference - Please note that seat requests can never be guaranteed, " +
              "however we will contact the airlines with your preferences. Some airlines charge a fee to book " +
              "advanced seat assignments.\n" +
              "\n" +
              "Bedding request - Please advise if you prefer double, twin, triple or a single bedded rooms.\n" +
              "\n" +
              "For further information on Qantas Vacations, please visit our website, " +
              "listed below or email us at reservations@qantasvacations.com. Our hours are Monday through Friday, " +
              "6:30 A.M. to 5:30 P.M. and Saturday 7:00 A.M. to 4:00P.M., Pacific Standard Time. Our toll free phone " +
              "number is 800 641 8772.\n" +
              "Thank you for considering Qantas Vacations, we are confident this trip will be your best travel " +
              "experience ever! For full Terms and Conditions, visit http://www.qantasvacations" +
              ".com/booking-conditions.asp\n" +
              "");

    NoteVO note = new NoteVO();
    note.setName("IMPORTANT INFORMATION");
    note.setTimestamp(getTimestamp(maxDate, "11:59p"));
    note.setRank(rank++);
    note.setNote(sb.toString().replace("\n","<br>"));
    trip.addNoteVO(note);
  }

  public void  addIslandQuoteInfo () {
    StringBuilder sb = new StringBuilder();

    sb.append("Thank you for asking Islands in the Sun to provide you with a customized itinerary for your dream travel experience.\n" +
              "\n" +
              "Please review this document carefully to ensure that we are providing the best possible travel " +
              "products and services to suit your individual needs.\n" +
              "\n" +
              "FOR QUOTES: no space has been requested or held. To book a non-refundable deposit of $300 per person " +
              "is required. Some products, such as those outside of our normal product line, " +
              "require a higher deposit amount; this will be advised at time of booking.\n" +
              "We reserve the right to change our prices, itineraries and routing without notice. If you have asked " +
              "us to price airfare, then US/Foreign Taxes, including September 11 Security Fee are included. Prices " +
              "are current at the time of BOOKING. Airlines and hotels frequently change their prices as a result of " +
              "fare increases and/or fuel surcharges. PRICES MAY INCREASE BEFORE FULL PAYMENT FOR THOSE AND OTHER " +
              "REASONS AND, BY PAYING YOUR DEPOSIT, YOU AFFIRMATIVELY AGREE TO THE POSSIBILITY OF SUCH INCREASES. " +
              "AFTER FULL PAYMENT, PRICES ARE GUARANTEED EXCEPT ONLY DUE TO AN INCREASE IN GOVERNMENT IMPOSED TAXES " +
              "AND FEES AND BY MAKING SUCH PAYMENT YOU AFFIRMATIVELY AGREE TO THE POSSIBILITY OF SUCH INCREASE.\n" +
              "We offer a Price Protection Policy for an additional $25 per person. Purchase of the Price Protection " +
              "Policy at time of deposit guarantees your price for the land arrangements as quoted will not increase " +
              "due to currency fluctuation. Please note: Any changes made to land itineraries after deposit is " +
              "received will cause the itinerary to be re-priced using the existing currency exchange rates. The " +
              "Price Protection Policy specifically excludes any new government taxes or fees made or imposed after " +
              "the date of your original quotation.  Airfares are protected from the date that the airfare is paid in" +
              " full and the tickets are issued. Changes made to air itineraries after ticketing will incur airline " +
              "change fees.\n" +
              "\n" +
              "FINAL PAYMENT: Final payment is required 45 days prior to departure. We accept most credit cards for " +
              "payments. Personal checks are also acceptable for payment, but must be received at least 21 days prior" +
              " to travel.\n" +
              "\n" +
              "AMENDMENT FEES: There is a $100 amendment fee each time an itinerary is revised after invoicing. " +
              "Amendments made after documents have been issued will incur a $150 fee to cover courier costs, " +
              "communication costs and additional time to co-ordinate the changes.\n" +
              "\n" +
              "CANCELLATION FEES: Cancellation fees are based the following schedule:\n" +
              "45 Days or more prior to departure - Loss of deposit; Between 44-21 Days - 20% of total cost;\n" +
              "Between 20-10 Days - 35% of total cost; Between 9-1 Days - 60% of total cost; Day of departure - 100% " +
              "of total cost.\n" +
              "Separate cancellation policies/fees apply to select products and may be up to 100% of total price.\n" +
              "\n" +
              "Unused travel arrangements or travel arrangements cancelled, amended or altered once travel has " +
              "commenced do not qualify for any refund.\n" +
              "\n" +
              "INSURANCE AND \"NO WORRIES\" WAIVERS: We offer you affordable and comprehensive insurance plans. " +
              "Please click here for policy details. Please note if you wish to take advantage of the Pre Existing " +
              "Medical Condition Coverage, your insurance premium must be paid within 14 days of your initial deposit" +
              " payment for your trip. Insurance and the \"No Worries Waiver\" may not be added after final payment" +
              ".\n" +
              "\n" +
              "\n" +
              "FOLLOW-UP CHECK LIST:\n" +
              "Names - The correct spelling of your names must be provided as they appear on your passports. Name " +
              "changes resulting in the reissuing of tickets will incur a $150 per person charge, " +
              "as well as any fees charged by the airlines.\n" +
              "\n" +
              "Passports and Visitors Visas - All passengers are required to travel with a passport valid for six " +
              "months after their return. Visitors to Australia are required to have an entry visa. For a minimal fee" +
              " of $25, we can issue an Australian visa for U.S. and Canadian citizens and require a copy of your " +
              "passport to process.\n" +
              "\n" +
              "International Flight and Ticket Numbers - Please note if we are not issuing the international tickets," +
              " correct flight information and ticket numbers are required in order for us to arrange applicable " +
              "transfers and domestic air tickets.\n" +
              "\n" +
              "Special Meal requests - Please advise of any special meal requests or dietary requirements.\n" +
              "\n" +
              "Seating Preference - Please note that seat requests can never be guaranteed, " +
              "however we will contact the airlines with your preferences. Some airlines charge a fee to book " +
              "advanced seat assignments.\n" +
              "\n" +
              "Bedding request - Please advise if you prefer double, twin, triple or a single bedded rooms.\n" +
              "\n" +
              "For further information on Islands in the Sun, please visit our website, " +
              "listed below or email us at info@islandsinthesun.com. Our hours are Monday through Friday, " +
              "6:30 A.M. to 5:30 P.M. and Saturday 7:00 A.M. to 4:00P.M., Pacific Standard Time. Our toll free phone " +
              "number is 866 384 2154.\n" +
              "Thank you for considering Islands in the Sun, we are confident this trip will be your best travel " +
              "experience ever! For full Terms and Conditions, visit http://www.islandsinthesun.com/terms.asp\n" +
              "");

    NoteVO note = new NoteVO();
    note.setName("IMPORTANT INFORMATION");
    note.setTimestamp(getTimestamp(maxDate, "11:59p"));
    note.setRank(rank++);
    note.setNote(sb.toString().replace("\n","<br>"));
    trip.addNoteVO(note);
  }

  @Override
  protected void postProcessingImpl(String tripId, String userId) {
    if (tripId != null && amadeusPNR != null && !amadeusPNR.isEmpty() && travelers != null && !travelers.isEmpty() && userId != null) {
      //check to see if there are flights that need to be augmented using the record locator
      List<String> lastNames = new ArrayList<>();
      //since we are not sure whether the last name can have a space or not, we are doing a couple of checks...
      for (PassengerVO passengerVO : travelers) {
        String[] tokens = passengerVO.getFullName().split(" ");
        if (tokens.length == 2 && !lastNames.contains(tokens[1].trim())) {
          //there is a single last name... so we can just use that one
          lastNames.add(tokens[1].trim());
          break;
        }
      }
      if (lastNames.isEmpty()) {
        //ok no single last name passengers... so we need to try again and this time maybe get a couple of options
        for (PassengerVO passengerVO : travelers) {
          String[] tokens = passengerVO.getFullName().split(" ");
          if (tokens.length > 2) {
            for (int i = 1; i < tokens.length; i++) {
              StringBuilder lastname = new StringBuilder();
              for (int j = i; j < tokens.length; j++) {
                lastname.append(tokens[j]);
              }
              if (!lastNames.contains(lastname.toString())) {
                lastNames.add(lastname.toString());
              }
            }
          }
        }
      }

      for (String lastName : lastNames) {
        RecordLocatorForm form = new RecordLocatorForm();
        form.setUserId(userId);
        form.setInRecordLocator(amadeusPNR);
        form.setInTripId(tripId);
        form.setInProvider(RecordLocatorForm.Provider.AMADEUS);
        form.setInLastName(lastName);
        RecLocatorActor.Command cmd = new RecLocatorActor.Command(form);
        ActorsHelper.tell(SupervisorActor.UmappedActor.RECORD_LOCATOR, cmd);
      }
    }
  }

  private void processQuote() {
    String[] tokens = file.split("\n");
    boolean processQuote = false;
    int startPos = 0;
    int endPos = 0;
    int startTableDetails = 0;
    int startDetails = 0;
    int endDetails = 0;
    boolean hasAir = false;
    boolean hasLand = false;
    boolean hasOther = false;

    for (int i = 0; i < tokens.length; i++) {
      String line = tokens[i];
      if (line.contains("COST OF TOUR BY PASSENGER")) {
        startPos = i;
      } else if (line.contains("ITINERARY")) {
        endPos = i;
        break;
      } else if (line.contains("Passenger") && line.contains("Age") && line.contains("Gateway") && line.contains("Total")) {
        startTableDetails = i;
        if (line.contains("Air")) {
          hasAir = true;
        }
        if (line.contains("Land")) {
          hasLand = true;
        }
        if (line.contains("Other")) {
          hasOther = true;
        }
      } else if (line.contains("PAYMENT DETAILS")) {
        startDetails = i;
      } else if (line.contains("TOTAL DUE:") || line.contains("NET DUE:")) {
        endDetails = i;
      }
    }
    StringBuilder sb = new StringBuilder();
    int ageIndex = -1;

    //process detail costs
    if (startTableDetails > 0 && startDetails > 0 && startDetails > startTableDetails) {
      sb.append("<table>\n");
      sb.append("<tr>\n");
      sb.append("<td width='200' style='text-align:center;background-color:#C8CBCF;border-right: 1px solid #ffffff;'> "); sb.append("Passenger"); sb.append("</td>\n");
      sb.append("<td width='60' style='text-align:center;background-color:#C8CBCF;border-right: 1px  solid  #ffffff;'> "); sb.append("Gateway"); sb.append("</td>\n");
      if (hasAir) {
        sb.append(
            "<td width='75' style='text-align:center;background-color:#C8CBCF;border-right: 1px  solid  #ffffff;'> ");
        sb.append("Air");
        sb.append("</td>\n");
      }
      if (hasLand) {
        sb.append(
            "<td width='75' style='text-align:center;background-color:#C8CBCF;border-right: 1px  solid  #ffffff;'> ");
        sb.append("Land");
        sb.append("</td>\n");
      }
      if (hasOther) {
        sb.append(
            "<td width='75' style='text-align:center;background-color:#C8CBCF;border-right: 1px  solid #ffffff;'> ");
        sb.append("Other");
        sb.append("</td>\n");
      }
      sb.append("<td width='100' style='text-align:center;background-color:#C8CBCF'> "); sb.append("Total"); sb.append(
          "</td>\n");

      sb.append("</tr>\n");
      for (int i = startTableDetails + 1; i < startDetails; i++) {
        sb.append("<tr>\n");
        String line = tokens[i];
        String[] cols = line.split(" ");
        if (cols != null) {
          if (cols.length == 7) { //passenger line with no name
            sb.append("<td  style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append("-"); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[2]); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[3]); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[4]); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[5]); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[6]); sb.append("</td>\n");
          } else if (cols.length == 4) { //last line
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append("Total"); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(""); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[0]); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[1]); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[2]); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[3]); sb.append("</td>\n");
          }  else if (cols.length == 1) { //last line
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append("Total"); sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(""); sb.append("</td>\n");
            if (hasAir)
              sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append("</td>\n");
            if (hasLand)
              sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> ");  sb.append("</td>\n");
            if (hasOther)
              sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> ");  sb.append("</td>\n");
            sb.append("<td style='text-align:center;background-color:#E1E4E6;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[0]); sb.append("</td>\n");
          } else {
            //let's figure out where the age - is
              for (int j = 0; j < cols.length; j++) {
                String s = cols[j];
                if (s.equals("-") || StringUtils.isNumeric(s)) {
                  ageIndex = j;
                  break;
                }
              }
            if (ageIndex > 0 && (ageIndex + 2) < cols.length) {
              StringBuilder name = new StringBuilder();
              for (int j = 0; j < ageIndex; j++) {
                String s = cols[j];
                if (s.equals("-") || StringUtils.isNumeric(s)) {
                  break;
                } else {
                  name.append(s);name.append(" ");
                }
              }
              sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(name.toString().trim()); sb.append("</td>\n");
              for (int j = ageIndex + 1; j < cols.length - 1; j++) {
                sb.append("<td style='text-align:center;background-color:#E1E4E6;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[j]); sb.append("</td>\n");
              }
              sb.append("<td style='text-align:center;background-color:#E1E4E6;border-bottom: 1px solid #ffffff;'> "); sb.append(cols[cols.length - 1]); sb.append("</td>\n");
            }
          }
        }
        sb.append("</tr>\n");
      }
      sb.append("</table>\n");
      sb.append("<br><br>\n");
    }

    //process detail costs
    if (startDetails > 0 && endDetails > 0 && endDetails > startDetails) {
      sb.append("PAYMENT DETAILS<br>");
      for (int i = startDetails + 1; i < endDetails + 1; i++) {
        String s = tokens[i];
        sb.append(s.replace("LESS COMMISSION", "<br/>LESS COMMISSION").replace("TOTAL PRICE","<br/>TOTAL PRICE"));
        sb.append("\n");
        sb.append("<br>\n");
      }
      sb.append("<br>\n");
    }

    //process disclaimer
    if (endDetails > 0 && endPos > 0 && endPos > endDetails) {
      for (int i = endDetails + 1; i < endPos; i++) {
        if (tokens[i].contains("-----")) {
          sb.append("<br>\n");
          sb.append(tokens[i]);
          sb.append("<br>\n");
        } else if (tokens[i].contains("Kind Regards")) {
          sb.append(tokens[i]);
          sb.append("<br>\n");
        } else {
          sb.append(tokens[i]);
          sb.append(appendWhitespace(tokens, i).replace("\n", "<br>"));
        }

      }
    }

    NoteVO noteVO = new NoteVO();
    noteVO.setName("COST OF TOUR BY PASSENGER");
    noteVO.setNote(sb.toString());
    noteVO.setRank(0);
    trip.getNotes().add(0, noteVO);

  }

  public static void main(String[] args) {
    try {
     String dir = "/home/twong/Downloads/241033_adv.pdf";//"/home/twong/Downloads/Calypso1.pdf";// "/users/twong/Dropbox/UMapped_Tech/Clients Documents/Travel 2 invoices/8.pdf";//"/volumes/data2/Downloads/travel2_087204.pdf";//"/volumes/data2/Downloads/t2-prop1.pdf";//"/users/twong/Dropbox/UMapped_Share/Integration Samples/Travel2/US518303_itn.pdf"; //"/users/twong/Dropbox/UMapped_Share/Companies/Travel2/reumappedtravel2nextsteps/196402_itn.pdf";
     // String dir = "/users/twong/downloads/travel2.pdf";
      File file = new File(dir);
      InputStream ins = new FileInputStream(file);

      //System.out.println(getPDFContentAsString(ins));

      Travel2Extractor p = new Travel2Extractor();
      p.initImpl(null);
      TripVO t = p.extractData(ins);
      //p.postProcessingImpl("1", "2");
      System.out.println(t);

    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }
}
