package com.mapped.publisher.parse.extractor.booking.topatlantico;

/**
 * Created by twong on 15-04-06.
 */

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.lang3.time.DateUtils;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ryan on 15-04-04.
 */
public class SoltourParser {
  private String file;
  private TripVO trip;

  private static String[] dateTimeFormat = {"dd/MM/yy HH:mm", "dd/MM/yyyy HH:mm",};
  Pattern datePattern = Pattern.compile("[0-9]{2}/[0-9]{2}/[0-9]{4}");


  private static DateUtils du = new DateUtils();
  private static Calendar now = Calendar.getInstance();

  public SoltourParser(String file) {
    this.file = file;
    trip = new TripVO();
  }

  public TripVO getTrip() {
    if (file.contains("Hora Llegada ")) {
      parseFlight();
    } else {
      parseFlight2();
    }
    parseHotel();
    parseServices();
    trip.setImportSrc(BookingSrc.ImportSrc.TA_SOLTOURS_PDF);
    trip.setImportTs(System.currentTimeMillis());
    return trip;
  }

  //Used with fuerteventura 2 adultos e 2 crianÃ§as_soltour.pdf
  public void parseFlight() {
    Matcher dates = Pattern.compile("Día:\\s[0-9]{2}/[0-9]{2}/[0-9]{2}").matcher(file);
    Matcher departTimes = Pattern.compile("Hora\\sSalida:\\s[0-9]{2}:[0-9]{2}").matcher(file);
    Matcher arriveTimes = Pattern.compile("Hora\\sLlegada\\s[0-9]{2}:[0-9]{2}").matcher(file);
    Matcher locations = Pattern.compile("Desde[A-Za-z.\\s]+\\([A-Z]{3}\\)\\sa\\s[A-Za-z.\\s]+\\([A-Z]{3}\\)").matcher(file);
    Matcher flightInfos = Pattern.compile("V.?.o:\\s[A-Z]{1,3}[0-9]{3,5}\\sde\\s[A-Z.\\s]+").matcher(file);

    while(true) {
      FlightVO flight = new FlightVO();

      flight.setDepartureAirport(new AirportVO());
      flight.setArrivalAirport(new AirportVO());

      String departDate = "";
      String arriveDate= "";
      if(dates.find()) {
        departDate = dates.group().substring(dates.group().indexOf(":") + 2);
        arriveDate = dates.group().substring(dates.group().indexOf(":") + 2);
      }

      if(departTimes.find())
        flight.setDepatureTime(getTimestamp(departDate, departTimes.group().substring(departTimes.group().indexOf(":") + 2)));

      if(arriveTimes.find())
        flight.setArrivalTime(getTimestamp(arriveDate, arriveTimes.group().substring("Hora Llegada".length()).trim()));

      if(locations.find()) {
        flight.getDepartureAirport().setCity(locations.group().substring("Desde".length(), locations.group().indexOf("(")).trim());
        flight.getDepartureAirport().setCode(locations.group().substring(locations.group().indexOf("(") + 1, locations.group().indexOf(")")).trim());

        flight.getArrivalAirport().setCity(locations.group().substring(locations.group().indexOf(")") + 4, locations.group().lastIndexOf("(")).trim());
        flight.getArrivalAirport().setCode(locations.group().substring(locations.group().lastIndexOf("(") + 1, locations.group().lastIndexOf(")")).trim());
      }

      if(flightInfos.find()) {
        if(Character.isLetter(flightInfos.group().charAt(7))) {
          flight.setCode(flightInfos.group().substring(flightInfos.group().indexOf(":") + 2, flightInfos.group().indexOf(":") + 5));
          flight.setNumber(flightInfos.group().substring(flightInfos.group().indexOf(":") + 5, flightInfos.group().indexOf(":") + 8));
        }
        else if(Character.isLetter(flightInfos.group().charAt(6))) {
          flight.setCode(flightInfos.group().substring(flightInfos.group().indexOf(":") + 2, flightInfos.group().indexOf(":") + 4));
          flight.setNumber(flightInfos.group().substring(flightInfos.group().indexOf(":") + 4, flightInfos.group().indexOf(":") + 8));
        }
        else if(Character.isLetter(flightInfos.group().charAt(5))) {
          flight.setCode(flightInfos.group().substring(flightInfos.group().indexOf(":") + 2, flightInfos.group().indexOf(":") + 3));
          flight.setNumber(flightInfos.group().substring(flightInfos.group().indexOf(":") + 3, flightInfos.group().indexOf(":") + 8));
        }
      }

      if(dates.hitEnd() && departTimes.hitEnd() && arriveTimes.hitEnd() && locations.hitEnd() && flightInfos.hitEnd())
        break;

      if (flight.isValid())
        trip.addFlight(flight);
    }
  }

  //Used with punta cana_soltour.pdf
  public void parseFlight2() {
    Matcher depart = Pattern.compile("(Día\\sy\\shora\\sde\\sSaida\\s:[\\s]+|Día\\sy\\shora\\sde\\ssalida:[\\s]+)[0-9]{2}/[0-9]{2}/[0-9]{2}\\sa\\slas\\s[0-9]{2}:[0-9]{2}").matcher(file);
    Matcher arrive = Pattern.compile("(Día\\sy\\shora\\sde\\sChegada\\s:[\\s]+|Día\\sy\\shora\\sde\\sllegada:[\\s]+)[0-9]{2}/[0-9]{2}/[0-9]{2}\\sa\\slas\\s[0-9]{2}:[0-9]{2}").matcher(file);
    Matcher locations = Pattern.compile("Desde[A-Za-z.\\s]+\\([A-Z]{3}\\)\\sa\\s[A-Za-z.\\s]+\\([A-Z]{3}\\)").matcher(file);
    Matcher flightInfos = Pattern.compile("V.?.o:\\s[A-Z]{1,3}[0-9]{3,5}\\sde\\s[A-Z.\\s]+").matcher(file);

    while(true) {
      FlightVO flight = new FlightVO();

      flight.setDepartureAirport(new AirportVO());
      flight.setArrivalAirport(new AirportVO());

      if(depart.find())
        flight.setDepatureTime(getTimestamp(depart.group().substring(depart.group().indexOf(":") + 2, depart.group().indexOf(":") + 10), depart.group().substring(depart.group().lastIndexOf(":") - 2, depart.group().lastIndexOf(":") + 3)));

      if(arrive.find())
        flight.setArrivalTime(getTimestamp(arrive.group().substring(arrive.group().indexOf(":") + 2, arrive.group().indexOf(":") + 10), arrive.group().substring(arrive.group().lastIndexOf(":") - 2, arrive.group().lastIndexOf(":") + 3)));

      if(locations.find()) {
        flight.getDepartureAirport().setCity(locations.group().substring("Desde".length(), locations.group().indexOf("(")).trim());
        flight.getDepartureAirport().setCode(locations.group().substring(locations.group().indexOf("(") + 1, locations.group().indexOf(")")).trim());

        flight.getArrivalAirport().setCity(locations.group().substring(locations.group().indexOf(")") + 4, locations.group().lastIndexOf("(")).trim());
        flight.getArrivalAirport().setCode(locations.group().substring(locations.group().lastIndexOf("(") + 1, locations.group().lastIndexOf(")")).trim());
      }

      if(flightInfos.find()) {
        if(Character.isLetter(flightInfos.group().charAt(7))) {
          flight.setCode(flightInfos.group().substring(flightInfos.group().indexOf(":") + 2, flightInfos.group().indexOf(":") + 5));
          flight.setNumber(flightInfos.group().substring(flightInfos.group().indexOf(":") + 5, flightInfos.group().indexOf(":") + 8));
        }
        else if(Character.isLetter(flightInfos.group().charAt(6))) {
          flight.setCode(flightInfos.group().substring(flightInfos.group().indexOf(":") + 2, flightInfos.group().indexOf(":") + 4));
          flight.setNumber(flightInfos.group().substring(flightInfos.group().indexOf(":") + 4, flightInfos.group().indexOf(":") + 8));
        }
        else if(Character.isLetter(flightInfos.group().charAt(5))) {
          flight.setCode(flightInfos.group().substring(flightInfos.group().indexOf(":") + 2, flightInfos.group().indexOf(":") + 3));
          flight.setNumber(flightInfos.group().substring(flightInfos.group().indexOf(":") + 3, flightInfos.group().indexOf(":") + 8));
        }
      }

      if(depart.hitEnd() && arrive.hitEnd() && locations.hitEnd() && flightInfos.hitEnd())
        break;

      if (flight.isValid())
         trip.addFlight(flight);
    }
  }

  public void parseHotel() {


    //second pass to enrich the hotel
    String[] tokens = file.split("\n");
    for (int i = 0; i < tokens.length; i++) {
      if ((i + 1) < tokens.length) {
        String line = tokens[i];
        String nextLine = tokens[i + 1];

        if (nextLine.contains("Data da Entrada:") && nextLine.contains("Data da Saida:") && nextLine.contains("Noites:")) {
          HotelVO hotel = new HotelVO();
          StringBuilder note = new StringBuilder();
          String tel = "";
          String fax = "";

          hotel.setHotelName(line.trim());
          Matcher dates = datePattern.matcher(nextLine);
          if (dates.find()) {
            String checkinDate = dates.group();
            if (checkinDate != null) {
              hotel.setCheckin(getTimestamp(checkinDate, "15:00"));
            }
            String checkoutDate = dates.group();
            if (checkoutDate != null) {
              hotel.setCheckout(getTimestamp(checkoutDate, "15:00"));
            }
          }
          i++;
          int j = 0;
          while (true) {
            j++;
            if ((i + 1) >= tokens.length) {
              break;
            }
            String l = tokens[i];
            if (l.contains("Tipo")) {
              note.append(l);note.append("\n");
            } else if (l.contains("Acomodação")) {
              note.append(l);note.append("\n");
            } else if (l.contains("Regime")) {
              note.append(l);note.append("\n");
            } else if (l.contains("Dirección")) {
              if (l.contains("Telefone")) {
                tel = l.substring(l.indexOf("Telefone:"));
                note.append(l.replace(tel, ""));
                note.append("\n");
              } else {
                note.append(l);
                note.append("\n");
              }
            } else if (l.contains("Telefone")) {
              tel = l.substring(l.indexOf("Telefone:"));
              note.append(l.replace(tel,""));note.append("\n");
            } else if (l.contains("Fax")) {
              fax = l.substring(l.indexOf("Fax"));
              note.append(l.replace(fax,""));note.append("\n");
            }
            if (l.contains("OUTROS SERVIÇOS") || l.contains("*") || l.contains("SEGUROS CONTRATADOS") || l.contains("SERVIÇOS AÉREOS") || l.contains("DADOS DOS PASSAGEIROS")) {
              break;
            } else if (j > 10) {
              break;
            }

            i++;



          }
          if (hotel.isValid()) {
            if (tel.length() > 0) {
              note.append(tel);note.append("\n");
            }
            if (fax.length() > 0) {
              note.append(fax);note.append("\n");
            }
            hotel.setNote(note.toString());
            trip.addHotel(hotel);
          }
        }

      }
    }
  }

  public void parseServices() {


    if (file.contains("OUTROS SERVIÇOS")) {
      String s = file.substring(file.indexOf("OUTROS SERVIÇOS"));
      if (s.contains("SEGUROS CONTRATADOS")) {
        s = s.substring(0, s.indexOf("SEGUROS CONTRATADOS"));
      }
      //second pass to enrich the hotel
      String[] tokens = s.split("\n");

      for (int i = 0; i < tokens.length; i++) {
        String s1 = tokens[i];
        if (s1.contains("Quantidade")) {
          String name = s1.substring(0, s1.indexOf("Quantidade")).trim();
          StringBuilder sb = new StringBuilder();
          sb.append(s1.substring(s1.indexOf("Quantidade")));
          sb.append("\n\n");
          sb.append(name);
          boolean isTransfer = false;
          if (i > 0) {
            String title = tokens[i - 1];
            if (title.toLowerCase().equals("traslados") || title.toLowerCase().equals("transferes")) {
              isTransfer = true;
            }
          }

          while (true) {
            i++;
            if (i < tokens.length) {
              String s2 = tokens[i];
              if (s2.contains("Quantidade")) {
                i--;
                break;
              } else {
                s2 = s2.replaceAll("\n", "");
                sb.append(s2);
              }
            } else {
              break;
            }

          }
          if (isTransfer) {
            TransportVO transportBooking = new TransportVO();
            transportBooking.setBookingType(ReservationType.PRIVATE_TRANSFER);
            transportBooking.setName(name);
            transportBooking.setNote(sb.toString());
            //for transfers try to use the arrival time first
            if (trip.getFlights() != null && trip.getFlights().size() > 0) {
              FlightVO flightVO = trip.getFlights().get(0);
              if (flightVO.getArrivalTime() != null) {
                transportBooking.setPickupDate(flightVO.getArrivalTime());
                trip.addTransport(transportBooking);
              }
            }

            if (transportBooking.getPickupDate() == null &&trip.getHotels() != null && trip.getHotels().size() > 0) {
              HotelVO hotelVO = trip.getHotels().get(0);
              transportBooking.setPickupDate(hotelVO.getCheckin());
              trip.addTransport(transportBooking);
            }

          } else {
            ActivityVO activityVO = new ActivityVO();
            activityVO.setBookingType(ReservationType.ACTIVITY);
            activityVO.setName(name);
            activityVO.setNote(sb.toString());

            if (trip.getHotels() != null && trip.getHotels().size() > 0) {
              HotelVO hotelVO = trip.getHotels().get(0);
              activityVO.setStartDate(hotelVO.getCheckin());
              trip.addActivity(activityVO);
            } else if (trip.getFlights() != null && trip.getFlights().size() > 0) {
              FlightVO flightVO = trip.getFlights().get(0);
              if (flightVO.getArrivalTime() != null) {
                activityVO.setStartDate(flightVO.getArrivalTime());
                trip.addActivity(activityVO);
              }
            }
          }
        }
      }
    }
  }



  private Timestamp getTimestamp (String date, String time) {
    try {
      Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime() );
      return t;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
