package com.mapped.publisher.parse.extractor.booking.email;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.EmailBodyExtractor;
import com.mapped.publisher.parse.extractor.booking.valueObject.AirportVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.FlightVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.TransportVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.utils.Log;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by twong on 2015-02-06.
 */
public class ApolloExtractor extends EmailBodyExtractor {

  public Pattern timePattern = Pattern.compile("[0-9]{3,}(A|P|N|M)");
  public Pattern datePattern = Pattern.compile("[0-9]{2}\\s[A-Za-z]{3}\\s[0-9]{2}\\s");
  public Pattern datePatternNoYear = Pattern.compile("[0-9]{2}\\s[A-Za-z]{3}");
  public Pattern datePatternNoYear1 = Pattern.compile("[0-9]{2}[A-Za-z]{3}");
  public Pattern numPattern = Pattern.compile("\\s[0-9]");
  public static final Pattern fwdLinePrefixPattern = Pattern.compile("^[\\s>]*(.*)$");


  private static Pattern flightNumber = Pattern.compile("\\s[0-9]{2,}\\s");


  private String[] dateTimeFormat = {"dd MMM yy", "dd MM yy - EEEE", "dd MMM", "hmma", "hhmma", "yyyy dd MMM hmma", "yyyy dd MMM hhmma", "yyyy dd MMM","dd MMM yyyy","ddMMM yyyy"} ;

  private Map <Timestamp, List<String>> flights = new HashMap<>();
  private Map <Timestamp, List<String>> cars = new HashMap<>();
   private      DateUtils du = new DateUtils();

  @Override
  protected void initImpl(Map<String, String> params) {

  }

  @Override
  protected TripVO extractDataImpl(String input) throws Exception {
     TripVO tripVO = null;

    //first pass extract the relevant text blocks
    String [] tokens = input.split("\n");
    boolean isFlight =false;
    boolean isCar = false;
    Timestamp processingDate = null;
    String prevLine = null;
    StringBuilder block = null;
    int i = 0;
    String recordLocator = null;

    /**
     * Cleaning up all lines to strip forward
     */
    try {
      if (tokens != null) {
        for (int idx = 0; idx < tokens.length; idx++) {
          String currLine = tokens[idx];
          if (currLine != null) {
            Matcher m = fwdLinePrefixPattern.matcher(currLine);
            if (m != null && m.matches() && m.groupCount() == 1) {
              tokens[idx] = m.group(1).trim();
            }
          }
        }
      } else {
        Log.err("No lines in the document");
      }
    } catch(Exception e) {
      e.printStackTrace();
    }


    for (String curLine : tokens) {
      //there is an empty line before this block - so this is the start of something else...
      if (curLine.contains("AIRLINE RECORD LOCATOR:")) {
        recordLocator = curLine.substring(23);
      }
      if (prevLine == null || prevLine.length() == 0) {
        //let's see if this is a date
        boolean isNextFlight =false;
        boolean isNextCar = false;

        Timestamp nextDate = getTimestamp(curLine);
        if (nextDate == null && (i + 2)< tokens.length) {
          //let's look at the next line
          String nextLine = tokens[i + 1];
          String nextnextLine = tokens[i + 2];

          if (nextLine.contains("LV:") || nextnextLine.contains("LV:")) {
            isNextFlight = true;
          } else if (nextLine.contains("PICKUP-")) {
            isNextCar =true;
          }
        }

        //cleanup current processing date and reintialize for next block
        if (processingDate != null && block != null) {
          if (isFlight) {
            List <String> l = flights.get(processingDate);
            if (l == null)
              l = new ArrayList<>();

            l.add(block.toString());
            flights.put(processingDate, l);
          } else if (isCar) {
            List <String> l = cars.get(processingDate);
            if (l == null)
              l = new ArrayList<>();

            l.add(block.toString());
            cars.put(processingDate, l);
          }

          if (!isNextCar && !isNextFlight && nextDate == null ) {
            processingDate = null;
          }
          block = null;
          isCar = false;
          isFlight = false;
        }

        if (nextDate!=null) {
          processingDate = nextDate;
        }

        if (isNextFlight) {
          isFlight = true;
          block = new StringBuilder();
          block.append(curLine);block.append("\n");
        } else if (isNextCar) {
          isCar = true;
          block =new StringBuilder();
          block.append(curLine);block.append("\n");
        }
      } else {
        if (isFlight || isCar) {
          block.append(curLine);block.append("\n");
        } else if (processingDate != null) {
          //let's figure out if this is  a new block right after the date
          if ((i + 2)< tokens.length) {
            //let's look at the next line
            String nextLine = tokens[i + 1];
            String nextnextLine = tokens[i + 2];

            if (nextLine.contains("LV:") || nextnextLine.contains("LV:")) {
              isFlight = true;
            } else if (nextLine.contains("PICKUP-")) {
              isCar =true;

            }
            if (isFlight ||  isCar) {
                block = new StringBuilder();
                block.append(curLine);block.append("\n");

            }
          }
        }
      }

      i++;
      prevLine = curLine;
    }


    //second passs, process each extracted text block
    if (flights.size() > 0 || cars.size() > 0) {
      tripVO = new TripVO();
      for (FlightVO vo: processFlight()) {
        if (recordLocator != null) {
          vo.setReservationNumber(recordLocator);
        }
        tripVO.addFlight(vo);
      }
      for (TransportVO vo: processCars()) {
        tripVO.addTransport(vo);
      }
    }

    tripVO.setImportSrc(BookingSrc.ImportSrc.APOLLO);
    tripVO.setImportTs(System.currentTimeMillis());
    return tripVO;
  }

  private List<FlightVO> processFlight () {
    List<FlightVO> flightVOs = new ArrayList<>();

    for (Timestamp date : flights.keySet()) {
      DateTime d = new DateTime(date.getTime());
      List<String > list = flights.get(date);
      for (String flight : list) {
        FlightVO vo = new FlightVO();
        StringBuilder noteSb = new StringBuilder();

        String[] tokens = flight.split("\n");
        for (int i = 0; i < tokens.length; i++) {
          String s = tokens[i].trim();
          if (i == 0) {
            //that's the first line with the airline and flight number
            //find the flight number
            Matcher flightNumM = flightNumber.matcher(s);
            if (flightNumM != null && flightNumM.find()) {
              vo.setNumber(flightNumM.group().trim());
              vo.setCode(s.substring(0, s.indexOf(vo.getNumber())).trim());

              if (vo.getCode() != null) {
                String s1 = s.substring(s.indexOf(vo.getCode()) + vo.getCode().trim().length() );
                noteSb.append(s1.trim());noteSb.append("\n");
              }
            }
          }
          else if (s.contains("LV:") && vo.getCode() != null) {
            Matcher departTimeM = timePattern.matcher(s);
            if (departTimeM != null && departTimeM.find()) {
              String departTimeStr = departTimeM.group().trim();
              Timestamp t = parseTimestamp(departTimeStr);
              if (t != null) {
                Timestamp departTimestamp = new Timestamp(date.getTime() + t.getTime());
                String airport = s.substring(3, s.indexOf(departTimeStr));
                if (airport != null) {
                  AirportVO airportVO = vo.getDepartureAirport();
                  if (airportVO == null)
                    airportVO = new AirportVO();

                  airportVO.setName(airport.trim());
                  vo.setDepartureAirport(airportVO);
                  vo.setDepatureTime(departTimestamp);
                }
                String s1 = s.substring(s.indexOf(departTimeStr) + departTimeStr.length());
                noteSb.append(s1.trim());noteSb.append("\n");
              }
            }

          }
          else if (s.contains("AR:") && vo.getCode() != null) {
            Matcher arriveTimeM = timePattern.matcher(s);
            if (arriveTimeM != null && arriveTimeM.find()) {
              String arriveTimeStr = arriveTimeM.group().trim();
              Timestamp t = parseTimestamp(arriveTimeStr);
              if (t != null) {
                String airport = s.substring(3, s.indexOf(arriveTimeStr));
                if (airport != null) {
                  AirportVO airportVO = vo.getArrivalAirport();
                  if (airportVO == null)
                    airportVO = new AirportVO();

                  airportVO.setName(airport.trim());
                  vo.setArrivalAirport(airportVO);

                  Timestamp arriveTimestamp = null;

                  if (s.contains("ARVL DATE")) {
                    String s1 = s.substring(s.indexOf("ARVL DATE"));
                    Matcher arriveDateM = datePatternNoYear.matcher(s);
                    if (arriveDateM != null && arriveDateM.find()) {
                      String newArriveDate = arriveDateM.group().trim();
                      StringBuilder sb = new StringBuilder();
                      String time1 = arriveTimeStr;
                      if (time1.contains("P")) {
                        time1 = time1.replaceAll("P", "PM");
                      } else if (time1.contains("A")) {
                        time1 = time1.replaceAll("A", "AM");
                      } else if (time1.contains("N")) {
                        time1 = time1.replaceAll("N", "PM");
                      } else if (time1.contains("M")) {
                        time1 = time1.replaceAll("M", "AM");
                      }

                      sb.append(d.getYear() + " " + newArriveDate + " " + time1);
                      arriveTimestamp = parseDate(sb.toString());
                      if (arriveTimestamp.getTime() < vo.getDepatureTime().getTime() && vo.getDepatureTime() != null) {
                        sb = new StringBuilder();
                        sb.append((d.getYear() + 1) + " " + newArriveDate + " " + time1);
                        arriveTimestamp = parseDate(sb.toString());
                      }
                      String s2 = s.substring(s.indexOf(arriveTimeStr) + arriveTimeStr.length());
                      noteSb.append(s2.trim());noteSb.append("\n");
                    }
                  }

                  if (arriveTimestamp == null) {
                    arriveTimestamp = new Timestamp(date.getTime() + t.getTime());
                  }
                  vo.setArrivalTime(arriveTimestamp);
                }
              }
            }
            else if (s.contains("DEPART TERMINAL") && vo.getCode() != null) {
              AirportVO airportVO = vo.getDepartureAirport();
              if (airportVO == null)
                airportVO = new AirportVO();

              airportVO.setTerminal(s.substring(15));
              vo.setDepartureAirport(airportVO);
            }
          } else if (s.trim().length() > 0) {
            noteSb.append(s.trim());noteSb.append("\n");
          }


        }
        if (vo.isValid()) {
          vo.setNotes(noteSb.toString());
          flightVOs.add(vo);
        }
      }
    }
    return flightVOs;
  }

  private List<TransportVO> processCars () {
    List<TransportVO> transportVOs = new ArrayList<>();
    for (Timestamp date : cars.keySet()) {
      DateTime dateTime = new DateTime(date.getTime());

      List<String > list = cars.get(date);
      for (String info : list) {
        TransportVO vo = new TransportVO();
        vo.setPickupDate(date);

        String[] tokens = info.split("\n");
        for (int i = 0; i < tokens.length; i++) {
          String s = tokens[i].trim();
          if (i == 0) {
            //try to get the name
            Matcher m = numPattern.matcher(s);
            if (m != null && m.find()) {
              String delimiter = m.group();
              String title = s.substring(0, s.indexOf(delimiter)).trim();
              if (title != null) {
                vo.setName(title);
              }
              if (s.contains("DROP")) {
                String s1 = s.substring(s.indexOf("DROP"));
                Matcher m1 = datePatternNoYear1.matcher(s1);
                if (m1 != null && m1.find()) {
                  String d1 = m1.group().trim();
                  StringBuilder sb = new StringBuilder();
                  sb.append(d1); sb.append(" "); sb.append(dateTime.getYear());
                  Timestamp t = parseDate(sb.toString());
                  if (t.getTime() < vo.getPickupDate().getTime() && vo.getPickupDate() != null) {
                    sb = new StringBuilder();
                    sb.append(d1); sb.append(" "); sb.append((dateTime.getYear() + 1));
                    t = parseDate(sb.toString());
                  }
                  vo.setDropoffDate(t);
                }
              }
            }
          } else if (s.contains("PICKUP")) {
            String s1 = s.substring(7);
            String []t = s1.split(" ");
            StringBuilder  sb = new StringBuilder();
            for (String s2 :t) {
              if (s2.trim().length() > 0) {
                sb.append(s2);
                sb.append(" ");
              }
            }
            vo.setPickupLocation(sb.toString());
          } else if (s.contains("CONFIRMATION-")) {
            String s1 = s.substring(13);
            vo.setConfirmationNumber(s1);
          }
        }

        if (vo.isValid()) {
          vo.setBookingType(ReservationType.CAR_RENTAL);
          vo.setNote(info);
          transportVOs.add(vo);
        }
      }
    }
    return transportVOs;
  }

  private Timestamp getTimestamp (String s) {
    Matcher m = datePattern.matcher(s);
    if (m != null && m.find()) {
      String d = m.group().trim();
      try {
        java.sql.Timestamp t = new java.sql.Timestamp(du.parseDate(d, dateTimeFormat).getTime());
        return t;
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    return null;
  }

  private Timestamp parseDate (String timeStr) {

    try {
      java.sql.Timestamp t = new java.sql.Timestamp(du.parseDate(timeStr, dateTimeFormat).getTime());
      return t;
    } catch (Exception e) {
      e.printStackTrace();
    }


    return null;
  }

  private Timestamp parseTimestamp (String timeStr) {
    timeStr = timeStr.toUpperCase();
    if (timeStr.contains("P"))
      timeStr = timeStr.replaceAll("P", "PM");
    else if (timeStr.contains("N"))
      timeStr = timeStr.replaceAll("N", "PM");
    else if (timeStr.contains("A"))
      timeStr = timeStr.replaceAll("A","AM");
     else if (timeStr.contains("M"))
      timeStr = timeStr.replaceAll("M","AM");
    try {
      java.sql.Timestamp t = new java.sql.Timestamp(du.parseDate(timeStr, dateTimeFormat).getTime());
      return t;
    } catch (Exception e) {
      e.printStackTrace();
    }


    return null;
  }

  @Override
  public ArrayList<ParseError> getParseErrors()
  {
    return null;
  }




  public static void main (String [] args) {
    String s = "On Wednesday, Feb 4, 2015 at 4:16 PM, Suzy Nicholson <suzyn@travelsociety.com>, wrote:\n" +
               "\n" +
               "HARNETT/CAROL A                    \n" +
               "\n" +
               "\n" +
               " CAROL HARNETT                                                             \n" +
               " 126 HOPMEADOW ST                                                          \n" +
               " APT 7C                                                                    \n" +
               " WEATOGUE CT   06089                                                       \n" +
               "\n" +
               "\n" +
               " 10 APR 15 - FRIDAY   \n" +
               "  AMERICAN      687 ECONOMY       EQUIP-AIRBUS A320 JET                 \n" +
               "  DEPART TERMINAL- 1               \n" +
               "  LV: LAS VEGAS          635A     NONSTOP       MILES-  256   CONFIRMED \n" +
               "  AR: PHOENIX            741A     ELAPSED TIME- 1:06    ARVL DATE-27 AUG              \n" +
               "  ARRIVAL TERMINAL-4               \n" +
               "  OPERATED BY-US AIRWAYS          FREQ FLYER-AA          B8F9288\n" +
               "  US AIRWAYS                                                  \n" +
               "\n" +
               "  AMERICAN     2781 ECONOMY       EQUIP-CR9                             \n" +
               "  DEPART TERMINAL- 4               \n" +
               "  LV: PHOENIX           1000A     NONSTOP       MILES-  509   CONFIRMED \n" +
               "  AR: SAN LUIS OBSPO    1147A     ELAPSED TIME- 1:47                  \n" +
               "  OPERATED BY-US AIRWAYS EXPRESS  FREQ FLYER-AA          B8F9288\n" +
               "  US AIRWAYS EXPRESS-SKYWEST AIRLINES                         \n" +
               "\n" +
               "                            SURFACE TRANSPORTATION                         \n" +
               "\n" +
               " 16 APR 15 - THURSDAY \n" +
               "  AMERICAN     1184 ECONOMY       EQUIP-MD-80 JET                       \n" +
               "  DEPART TERMINAL- A               \n" +
               "  LV: SAN JOSE CA       1200N     NONSTOP       MILES- 1438   CONFIRMED \n" +
               "  AR: DALLAS/FT WOR      530P     ELAPSED TIME- 3:30                  \n" +
               "  FOOD TO PURCHASE                SEAT-25B                        \n" +
               "                                  FREQ FLYER-AA          B8F9288\n" +
               "\n" +
               "  AMERICAN     1572 ECONOMY       EQUIP-738                             \n" +
               "  LV: DALLAS/FT WOR      645P     NONSTOP       MILES- 1471   CONFIRMED \n" +
               "  AR: HARTFORD          1123P     ELAPSED TIME- 3:38                  \n" +
               "  FOOD TO PURCHASE-MOVIE          SEAT-26C                        \n" +
               "                                  FREQ FLYER-AA          B8F9288\n" +
               "                                  \n" +
               "  BUDGET                     1 FULL SIZE2/4 DR   DROP-15APR   CONFIRMED \n" +
               "  PICKUP-SAN DIEGO           SAN DIEGO INTL AIRPORT    \n" +
               "  RATE-      215.00          WEEKLY     GUARANTEED                      \n" +
               "  MILEAGE-UNL/FM                        CODE-DC       EXTRA DAY 72.00   \n" +
               "  PHONE-619-542-8001  FAX 619-542-8680          \n" +
               "                             CONFIRMATION-04337735US3        \n" +
               "\n" +
               "\n" +
               "\n" +
               "    DATE OF ISSUE: FEB 04 2015    INVOICE NUMBER: ITIN        XPRF5A  889\n" +
               "AIRLINE RECORD LOCATOR:AA -ECLLUA ";

    ApolloExtractor e = new ApolloExtractor();
    try {
      TripVO vo = e.extractDataImpl(s);
      System.out.println(vo);
    } catch (Exception e1) {
      e1.printStackTrace();
    }
  }
}
