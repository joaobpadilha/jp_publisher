package com.mapped.publisher.parse.extractor.booking.email;

import com.fasterxml.jackson.databind.JsonNode;
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.*;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import play.libs.Json;

import java.io.File;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by twong on 2015-03-12.
 */
public class CheckMyTripExtractor {

  private static String[] dateTimeFormat = {"yyyyMMddHHmm"};

  private static DateUtils du = new DateUtils();

  public static String getResponse(String recLocator, String lastName) {
    try {
      //  getItinerary("DelMauro", "2K85MQ");
      // String html = FileUtils.readFileToString(new File ("/volumes/data2/temp/amadeus13.txt"));
      // TripVO vo = parseResponse(html);
      // System.out.println(vo);

      CookieStore httpCookieStore = new BasicCookieStore();

      HttpClient httpClient = HttpClientBuilder.create().setDefaultCookieStore(httpCookieStore).build();
      HttpGet httpGet = new HttpGet("https://www.checkmytrip.com/cmt/apf/cmtng/index?LANGUAGE=GB&SITE=NCMTNCMT");
      httpGet.setHeader("User-Agent",
                        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) " +
                        "Chrome/55.0.2883.95 Safari/537.36");
      httpGet.setHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
      httpGet.setHeader("Accept-Language","en-US,en;q=0.5");
      httpGet.setHeader("Accept-Encoding","gzip, deflate, br");
      HttpResponse r = httpClient.execute(httpGet);

      if (r.getStatusLine() != null) {
        List<Cookie> cookies = httpCookieStore.getCookies();

        String JSESSIONID = "";
        String D_SID = "99.238.201.155:JiGQ4eTtxhrDbHzFjRh8NwL9A0J65E5bMt9PUIfWAg8";
        String D_PID = "D5895DB8-A493-30A6-859C-D4A879BBB200";
        String D_IID = "781D4DDC-58AA-3D51-9F0D-EBE5A09CEE73";
        String D_UID = "DF2B8EBE-75D9-3BD9-89C8-757D48476EC6";
        String D_HID = "p2CLvrpESVtweL5Og6D0CwSCAOOuvdZ2G1SagoSY/Mg";
        String D_ZID = "EEDABEFF-5F6E-358F-A767-A8067CF6C64C";
        String D_ZUID = "09E70236-8565-38CA-9B5E-E00CE4F76B8F";

        if (cookies.size() == 1) {
          Cookie c  = cookies.get(0);
          if (c.getName().equals("JSESSIONID")) {
            JSESSIONID = c.getValue();
          }
        }
        String index = EntityUtils.toString(r.getEntity());
        Connection.Response jsonResp = Jsoup.connect(
            "https://www.checkmytrip.com/cmt/apf/pnr/retrieve?SITE=NCMTNCMT&LANGUAGE=GB&OCTX=&APPVERSION=V5")
                                            //"https://www.checkmytrip
                                            // .com/cmt/apf/pnr/retrieve?SITE=NCMTNCMT&LANGUAGE=GB&OCTX=")
                                            //.cookie("JSESSIONID", sessionId)
                                            //.cookie("D_SID", D_SID)
                                            .cookie("JSESSIONID", JSESSIONID)
                                            //.cookie("D_PID", D_PID)
                                            //.cookie("D_IID", D_IID)
                                            //.cookie("D_UID", D_UID)
                                            //.cookie("D_HID", D_HID)
                                            //.cookie("D_ZID", D_ZID)
                                            //.cookie("D_ZUID", D_ZUID)
                                            .data("data",
                                                  "{\"recLoc\":\"" + recLocator + "\",\"lastName\":\"" + lastName + "\"," +
                                                  "\"checkSessionCache\":true," + "\"silent\":false}")
                                            .header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                                            .referrer(
                                                "https://www.checkmytrip" +
                                                ".com/cmt/apf/cmtng/index?LANGUAGE=GB&SITE=NCMTNCMT")
                                            .header("User-Agent",
                                                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) " +
                                                    "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.89 " +
                                                    "Safari/537.36")
                                            .header("X-Requested-With", "XMLHttpRequest")
                                            .timeout(1000 * 60 * 5)
                                            .method(Connection.Method.POST)

                                            .execute();

        if (jsonResp.statusCode() == 200) {
          Document doc = jsonResp.parse();
          String s = doc.toString();

          if (s.contains("<data>") && s.contains("</data>")) {
            String s1 = s.substring(s.indexOf("<data>") + 6, s.indexOf("</data>"));
            if (s1.contains("model")) {
              return (s1.trim());
            }
          } else {
            Log.err("AMADEUS EXTRACTOR ISSUE: " + recLocator + " for " + lastName);
          }
        }


      }


    }
    catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  public static TripVO getItinerary(String lastName, String recordLocator) {

    try {
      String response = getResponse(recordLocator, lastName);
      if (response != null) {
        TripVO tripVO = parseResponse(response);
        if (tripVO != null) {
          if (tripVO.getFlights() != null) {
            for (FlightVO f : tripVO.getFlights()) {
              if (f.getReservationNumber() == null || f.getReservationNumber()
                                                       .trim()
                                                       .length() == 0 || f.getReservationNumber().equals("NOSYNC")) {
                f.setReservationNumber(recordLocator);
              }
            }
          }
          tripVO.setImportSrc(BookingSrc.ImportSrc.AMADEUS_CHECKMYTRIPS);
          tripVO.setImportSrcId(recordLocator);
          tripVO.setImportTs(System.currentTimeMillis());
        }
        return tripVO;
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  public static TripVO parseResponse(String response) {
    try {
      if (response != null) {
        response = response.replaceAll("&amp;", "&");
        JsonNode jsonNode = Json.parse(response).get("model");


        TripVO tripVO = new TripVO();

        HashMap<String, String> flightRecLocator = new HashMap<>();
        HashMap<String, List<PassengerVO>> airSegmentPrefs = new HashMap<>();
        HashMap<String, HashMap<String, String>> segmentTicketNum = new HashMap<>();
        HashMap<String, String> segmentBaggage = new HashMap<>();


        HashMap<String, HashMap<String, String>> frequentFlyer = new HashMap<>(); //key is airlinecode, second key is
        // first name + last name

        JsonNode travellersNode = jsonNode.get("travellers");
        if (travellersNode != null) {
          for (JsonNode fSegment : travellersNode) {

            String firstName = fSegment.get("firstName").asText();
            String lastName = fSegment.get("lastName").asText();
            Iterator<Map.Entry<String, JsonNode>> nodeIterator = fSegment.get("frequentFlyers").fields();

            while (nodeIterator.hasNext()) {

              Map.Entry<String, JsonNode> entry = (Map.Entry<String, JsonNode>) nodeIterator.next();
              JsonNode ffNode = entry.getValue();
              String airline = ffNode.get("airline").asText();
              String ff = ffNode.get("number").asText();
              HashMap<String, String> map1 = frequentFlyer.get(airline);
              if (map1 == null) {
                map1 = new HashMap<>();
              }
              if (!map1.containsKey(firstName + lastName)) {
                map1.put(firstName + lastName, ff);
              }
              frequentFlyer.put(airline, map1);
            }
          }
        }

        JsonNode airlineRecLocs = jsonNode.get("airlineRecLocs");
        if (airlineRecLocs.isArray()) {
          for (JsonNode fSegment : airlineRecLocs) {
            if (fSegment.get("airline") != null) {
              String flightNum = fSegment.get("airline").get("code").asText() + fSegment.get("flightNumber").asText();
              flightRecLocator.put(flightNum, fSegment.get("recLoc").textValue());
            }
          }
        }

        JsonNode listTickets = jsonNode.get("listTickets");
        if (listTickets != null) {
          if (listTickets.get("listTicketInformation") != null && listTickets.get("listTicketInformation").isArray()) {
            for (JsonNode fSegment : listTickets.get("listTicketInformation")) {
              StringBuilder sb = new StringBuilder();


              if (fSegment.get("faFh") != null) {
                String documentNumber = null;
                if (fSegment.get("faFh").get("documentNumber") != null) {
                  documentNumber = fSegment.get("faFh").get("documentNumber").asText();
                }
                if (documentNumber != null && documentNumber.trim().length() > 0 && !documentNumber.equals("null")) {
                  sb.append("Document Number: ");
                  sb.append(documentNumber);
                  sb.append("\n");
                }

              }
              //save all segments per passenger number
              if (fSegment.get("listSegmentIds") != null && fSegment.get("listSegmentIds").isArray() && sb.toString()
                                                                                                          .trim()
                                                                                                          .length() >
                                                                                                        0) {


                for (JsonNode n : fSegment.get("listSegmentIds")) {
                  String segId = n.asText();
                  HashMap<String, String> details = segmentTicketNum.get(segId);
                  if (details == null) {
                    details = new HashMap<>();
                  }

                  if (fSegment.get("listTraveller") != null && fSegment.get("listTraveller").isArray()) {
                    for (JsonNode passengerId : fSegment.get("listTraveller")) {
                      details.put(passengerId.asText(), sb.toString());
                      segmentTicketNum.put(segId, details);
                    }
                  }
                }
              }
            }
          }

        }

        if (jsonNode.get("originalTicket") != null) {
          JsonNode baggageNode = jsonNode.get("originalTicket").get("baggageAllowance");
          if (baggageNode != null) {
            Iterator<Map.Entry<String, JsonNode>> nodeIterator = baggageNode.get("airSegmentPreferences").fields();

            while (nodeIterator.hasNext()) {
              Map.Entry<String, JsonNode> entry = (Map.Entry<String, JsonNode>) nodeIterator.next();
              String segNum = entry.getKey();
              JsonNode bNode = entry.getValue();
              boolean inKilos = bNode.get("inKilos").asBoolean();
              boolean inPieces = bNode.get("inPieces").asBoolean();
              String pieces = bNode.get("value").asText();
              if (inPieces) {
                pieces = pieces + " " + "piece(s)";
              }
              else if (inKilos) {
                pieces = pieces + " " + "KG";
              }

              segmentBaggage.put(segNum, pieces);
            }
          }
        }


        JsonNode airSegmentsPref = jsonNode.get("travellers");
        if (airSegmentsPref != null) {
          int i = 1;
          for (JsonNode seg : airSegmentsPref) {
            if (seg.get("airSegmentPreferences") != null && seg.get("airSegmentPreferences").fields() != null) {
              Iterator<Map.Entry<String, JsonNode>> nodeIterator = seg.get("airSegmentPreferences").fields();

              while (nodeIterator.hasNext()) {

                Map.Entry<String, JsonNode> entry = (Map.Entry<String, JsonNode>) nodeIterator.next();
                List<PassengerVO> travellers = null;
                if (airSegmentPrefs.containsKey(entry.getKey())) {
                  travellers = airSegmentPrefs.get(entry.getKey());
                }
                if (travellers == null) {
                  travellers = new ArrayList<>();
                }
                PassengerVO pVO = new PassengerVO();
                StringBuilder sb = new StringBuilder();
                pVO.setFirstName(seg.get("firstName").asText());
                pVO.setLastName(seg.get("lastName").asText());


                if (entry.getValue().get("frequentFlyers").asText() != null && !entry.getValue()
                                                                                     .get("frequentFlyers")
                                                                                     .asText()
                                                                                     .trim()
                                                                                     .equals("null") && entry.getValue()
                                                                                                             .get(
                                                                                                                 "frequentFlyers")
                                                                                                             .asText()
                                                                                                             .trim()
                                                                                                             .length
                                                                                                                 () >
                                                                                                        0) {
                  pVO.setFrequentFlyer(entry.getValue().get("frequentFlyers").asText());
                }

                if (entry.getValue().get("seatAssignment").asText() != null && !entry.getValue()
                                                                                     .get("seatAssignment")
                                                                                     .asText()
                                                                                     .trim()
                                                                                     .equals("null") && entry.getValue()
                                                                                                             .get(
                                                                                                                 "seatAssignment")
                                                                                                             .asText()
                                                                                                             .trim()
                                                                                                             .length
                                                                                                                 () >
                                                                                                        0) {
                  pVO.setSeat(entry.getValue().get("seatAssignment").asText());
                }

                //try to find the ticket stuff
                HashMap<String, String> tickets = segmentTicketNum.get(entry.getKey());
                if (tickets != null) {
                  //find the passenger
                  String s1 = tickets.get(String.valueOf(i));
                  if (s1 != null) {
                    String[] tokens = s1.split("\n");
                    if (tokens.length > 0) {
                      sb.append("\n");
                      for (int ti = 0; ti < tokens.length; ti++) {
                        String t = tokens[ti];
                        if (t.startsWith("Document Number:")) {
                          String eTicket = t.substring(17);
                          if (eTicket != null && eTicket.length() > 0) {
                            pVO.seteTicketNumber(eTicket);
                          }
                        }
                        else {
                          sb.append(t);
                          sb.append("\n");
                        }
                      }
                    }
                  }
                }

                pVO.setNote(sb.toString());
                travellers.add(pVO);

                airSegmentPrefs.put(entry.getKey(), travellers);
              }

            }
            i++;

          }
        }

        JsonNode airItineraries = jsonNode.get("airItineraries");
        if (airItineraries != null && airItineraries.isArray()) {

          for (JsonNode flight : airItineraries) {
            JsonNode segments = flight.get("segments");
            if (segments.isArray()) {
              for (JsonNode fSegment : segments) {
                FlightVO vo = processFlight(fSegment, flightRecLocator, airSegmentPrefs, segmentBaggage);
                if (vo != null && vo.isValid()) {
                  //see if there are other frequent flyer to add
                  Map<String, String> ffMap = frequentFlyer.get(vo.getCode());
                  //if there is only 1 use it since it might be alliance related
                  if (ffMap == null && frequentFlyer.size() == 1) {
                    ffMap = frequentFlyer.values().iterator().next();
                  }
                  if (vo.getPassengers() != null && ffMap != null) {
                    for (PassengerVO pVO : vo.getPassengers()) {
                      String ffNum = ffMap.get(pVO.getFirstName() + pVO.getLastName());
                      if (ffNum != null && (pVO.getFrequentFlyer() == null || !pVO.getFrequentFlyer().equals(ffNum))) {
                        pVO.setFrequentFlyer(ffNum);
                      }
                    }
                  }
                  tripVO.addFlight(vo);
                }
              }
            }
            else {
              FlightVO vo = processFlight(segments, flightRecLocator, airSegmentPrefs, segmentBaggage);
              if (vo != null && vo.isValid()) {
                tripVO.addFlight(vo);
              }
            }
          }
        }


        JsonNode hotelItineraries = jsonNode.get("hotels");
        if (hotelItineraries != null && hotelItineraries.isArray()) {
          for (JsonNode hotel : hotelItineraries) {
            HotelVO vo = processHotel(hotel);
            if (vo != null && vo.isValid()) {
              tripVO.addHotel(vo);
            }
          }
        }

        JsonNode carItineraries = jsonNode.get("cars");
        if (carItineraries != null && carItineraries.isArray()) {
          for (JsonNode car : carItineraries) {
            TransportVO vo = processCar(car);
            if (vo != null && vo.isValid()) {
              tripVO.addTransport(vo);
            }
          }
        }

        //extracl miscellaneous sections
        JsonNode miscItineraries = jsonNode.get("misc");
        if (miscItineraries != null && miscItineraries.isArray()) {
          for (JsonNode misc : miscItineraries) {
            NoteVO vo = processMiscAsNote(misc);
            if (vo != null) {
              tripVO.addNoteVO(vo);
            }
          }
        }

        //extract general remarks
        StringBuilder sb = new StringBuilder();
        JsonNode travelAgents = jsonNode.get("travelAgents");
        if (travelAgents != null) {
          for (JsonNode travelAgent : travelAgents) {
            JsonNode generalRemarks = travelAgent.get("generalRemarks");
            if (generalRemarks != null) {
              for (JsonNode generalRemark : generalRemarks) {
                String remark = generalRemark.get("value").asText();
                if (remark != null && !remark.isEmpty()) {
                  sb.append(remark);
                  sb.append("<br/>");
                }
              }
            }

          }
        }
        if (sb.toString().length() > 0) {
          //create a note
          NoteVO noteVO = new NoteVO();
          noteVO.setName("General Remarks");
          noteVO.setNote(sb.toString());
          tripVO.addNoteVO(noteVO);
        }

        return tripVO;
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public static void main(String[] args) {
    try {
      //  getItinerary("DelMauro", "2K85MQ");
      // String html = FileUtils.readFileToString(new File ("/volumes/data2/temp/amadeus13.txt"));
      // TripVO vo = parseResponse(html);
      // System.out.println(vo);

      CookieStore httpCookieStore = new BasicCookieStore();

      HttpClient httpClient = HttpClientBuilder.create().setDefaultCookieStore(httpCookieStore).build();
      HttpGet httpGet = new HttpGet("https://www.checkmytrip.com/cmt/apf/cmtng/index?LANGUAGE=GB&SITE=NCMTNCMT");
      httpGet.setHeader("User-Agent",
                        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) " +
                        "Chrome/55.0.2883.95 Safari/537.36");
      httpGet.setHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
      httpGet.setHeader("Accept-Language","en-US,en;q=0.5");
      httpGet.setHeader("Accept-Encoding","gzip, deflate, br");
      HttpResponse r = httpClient.execute(httpGet);

      if (r.getStatusLine() != null) {
        List<Cookie> cookies = httpCookieStore.getCookies();

        String JSESSIONID = "";
        String D_SID = "99.238.201.155:JiGQ4eTtxhrDbHzFjRh8NwL9A0J65E5bMt9PUIfWAg8";
        String D_PID = "D5895DB8-A493-30A6-859C-D4A879BBB200";
        String D_IID = "781D4DDC-58AA-3D51-9F0D-EBE5A09CEE73";
        String D_UID = "DF2B8EBE-75D9-3BD9-89C8-757D48476EC6";
        String D_HID = "p2CLvrpESVtweL5Og6D0CwSCAOOuvdZ2G1SagoSY/Mg";
        String D_ZID = "EEDABEFF-5F6E-358F-A767-A8067CF6C64C";
        String D_ZUID = "09E70236-8565-38CA-9B5E-E00CE4F76B8F";

        if (cookies.size() == 1) {
          Cookie c  = cookies.get(0);
          if (c.getName().equals("JSESSIONID")) {
            JSESSIONID = c.getValue();
          }
        }
        String index = EntityUtils.toString(r.getEntity());
        if (index.contains(" defer>")) {
          String index1 = index.substring(0, index.indexOf(" defer>"));
          if (index1 != null) {
            //load preceding files
            /*
            getUrl("https://www.checkmytrip.com/modules/css/print.css", httpCookieStore);
            getUrl("https://www.checkmytrip.com/aria/aria-templates-1.4-14D.js", httpCookieStore);
            getUrl("https://www.checkmytrip.com/modules/cmtng/cmtngskin.js", httpCookieStore);
            getUrl("https://www.checkmytrip.com/website-statics-master.411.31.js", httpCookieStore);
            getUrl("https://www.checkmytrip.com/LoaderShared-master.411.31.js?devMode=false", httpCookieStore);
            getUrl("https://www.checkmytrip.com/Loader-master.411.31.js?devMode=false", httpCookieStore);
*/
            String jsUrl = index1.substring(index1.lastIndexOf("src="));
            jsUrl = jsUrl.substring(5, jsUrl.lastIndexOf("\""));
            HttpGet httpGetJS1 = new HttpGet("https://www.checkmytrip.com" + jsUrl);
            httpGetJS1.setHeader("User-Agent",
                              "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) " +
                              "Chrome/55.0.2883.95 Safari/537.36");
            httpGetJS1.setHeader("Referer", "https://www.checkmytrip.com/cmt/apf/cmtng/index?LANGUAGE=GB&SITE=NCMTNCMT");
            httpGetJS1.setHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            httpGetJS1.setHeader("Accept-Language","en-US,en;q=0.5");
            httpGetJS1.setHeader("Accept-Encoding","gzip, deflate, br");

            r = httpClient.execute(httpGetJS1);

            String XJU = null;
            String XAH = null;

            if (r.getStatusLine() != null) {
              String index3 = EntityUtils.toString(r.getEntity());

              Header[] headers = r.getAllHeaders();
              for (Header h : headers) {
                if (h.getName().equals("X-JU")) {
                  XJU = h.getValue();
                } else if (h.getName().equals("X-AH")) {
                  XAH = h.getValue();
                }
              }

            }
            if (XJU != null && XAH != null) {
              /*
              getUrl("https://www.checkmytrip.com/modules/css/application_282bdb28cf254d2a9ecc644ae6595273.css", httpCookieStore);
              getUrl("https://www.checkmytrip.com/modules/cmtng/application_06d51e783edb7e89231ce08e0e0d8d0d.js", httpCookieStore);
              getUrl("https://www.checkmytrip.com/modules/cmtng/js/lib/json2_fe481be663e50915098c754222c2d64c.js", httpCookieStore);
              getUrl("https://www.checkmytrip.com/modules/cmtng/js/lib/PhoneFormat_a982788e54e82490f42f8e33bd69b4e5.js", httpCookieStore);
              getUrl("https://www.checkmytrip.com/aria/modules-93397c91d830d9b7f91fe1486e4b8cf2.js", httpCookieStore);
              getUrl("https://www.checkmytrip.com/modules/core/utils/ItineraryUtil_d1dd8c70713c4ab4f2e97ce24282e9b7.js", httpCookieStore);
              getUrl("https://www.checkmytrip.com/modules/cmtng/img/cookie/icons_sprited.png", httpCookieStore);
              getUrl("https://www.checkmytrip.com/modules/cmtng/img/backgrounds/bg-header_new.png", httpCookieStore);
              getUrl("https://www.checkmytrip.com/modules/cmtng/img/pageHeader-title-bgrd_new.png", httpCookieStore);
              getUrl("https://www.checkmytrip.com/modules/cmtng/img/logo-cmtng_new.png", httpCookieStore);
              getUrl("https://www.checkmytrip.com/modules/cmtng/img/backgrounds/bg-main_new.png", httpCookieStore);
              getUrl("https://www.checkmytrip.com/modules/cmtng/img/cmtlogo-animated.gif", httpCookieStore);

              HttpClient httpClient1 = HttpClientBuilder.create().setDefaultCookieStore(httpCookieStore).build();

              HttpGet httpGetJS3 = new HttpGet("https://www.checkmytrip.com/modules/cmtng/js/AppCtrl_44ba3b1e4ca07f1e485be4856eee8cd4.js");
              httpGetJS3.setHeader("User-Agent",
                                   "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) " +
                                   "Chrome/55.0.2883.95 Safari/537.36");
              httpGetJS3.setHeader("Referer", "https://www.checkmytrip.com/cmt/apf/cmtng/index?LANGUAGE=GB&SITE=NCMTNCMT");
              httpGetJS3.setHeader("Accept-Language","en-US,en;q=0.5");
              httpGetJS3.setHeader("Accept-Encoding","gzip, deflate, br");
              httpGetJS3.setHeader("X-Distil-Ajax",XAH);
              httpGetJS3.setHeader("X-Requested-With", "XMLHttpRequest");

              HttpResponse r31 = httpClient1.execute(httpGetJS3);
              System.out.println("1");
              Thread.sleep(300);

              HttpClient httpClient2 = HttpClientBuilder.create().setDefaultCookieStore(httpCookieStore).build();

              HttpGet httpGetJS4 = new HttpGet("https://www.checkmytrip.com/aria/utils-006a79c314caf1fcb3d0438f5b02ab13.js");
              httpGetJS4.setHeader("User-Agent",
                                   "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) " +
                                   "Chrome/55.0.2883.95 Safari/537.36");
              httpGetJS4.setHeader("Referer", "https://www.checkmytrip.com/cmt/apf/cmtng/index?LANGUAGE=GB&SITE=NCMTNCMT");
              httpGetJS4.setHeader("Accept-Language","en-US,en;q=0.5");
              httpGetJS4.setHeader("Accept-Encoding","gzip, deflate, br");
              httpGetJS4.setHeader("X-Distil-Ajax",XAH);
              httpGetJS4.setHeader("X-Requested-With", "XMLHttpRequest");

              HttpResponse r41 = httpClient2.execute(httpGetJS4);
              System.out.println("2 " );
*/



              //HttpClient httpClient3 = HttpClientBuilder.create().setDefaultCookieStore(httpCookieStore).build();

              HttpPost httpPostJS2 = new HttpPost("https://www.checkmytrip.com" + XJU);
              ArrayList<NameValuePair> postParameters = new ArrayList<>();
              postParameters.add(new BasicNameValuePair("PID", XJU.substring(XJU.indexOf("=") + 1)));
              httpPostJS2.setEntity(new UrlEncodedFormEntity(postParameters, Consts.UTF_8));
              httpPostJS2.setHeader("User-Agent",
                                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) " + "Chrome/55.0.2883.95 Safari/537.36");
              httpPostJS2.setHeader("Referer", "https://www.checkmytrip.com/cmt/apf/cmtng/index?LANGUAGE=GB&SITE=NCMTNCMT");
              httpPostJS2.setHeader("X-Distil-Ajax",XAH);
              httpPostJS2.setHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
              httpPostJS2.setHeader("Accept-Language","en-US,en;q=0.5");
              httpPostJS2.setHeader("Accept-Encoding","gzip, deflate, br");
              httpPostJS2.setHeader("Content-Type","text/plain;charset=UTF-8");
              HttpResponse r1 = httpClient.execute(httpPostJS2);
              System.out.println("3");

              if (r1.getStatusLine() != null) {
                String index3 = EntityUtils.toString(r1.getEntity());

 /*
                String XJU1 = null;
                String XAH1 = null;

                Header[] headers = r1.getAllHeaders();
                for (Header h : headers) {
                  String s11= h.getName();
                  String s12 = h.getValue();
                  if (h.getName().equals("X-JU")) {
                    XJU1 = h.getValue();
                  } else if (h.getName().equals("X-AH")) {
                    XAH1 = h.getValue();
                  }
                  System.out.println("e");
                }


                HttpPost httpPostJS3 = new HttpPost("https://www.checkmytrip.com" + XJU1);
                postParameters = new ArrayList<>();
                postParameters.add(new BasicNameValuePair("PID", XJU.substring(XJU.indexOf("=") + 1)));
                //httpPostJS2.setEntity(new UrlEncodedFormEntity(postParameters, Consts.UTF_8));
                httpPostJS3.setHeader("User-Agent",
                                      "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) " + "Chrome/55.0.2883.95 Safari/537.36");
                httpPostJS3.setHeader("Referer", "https://www.checkmytrip.com/cmt/apf/cmtng/index?LANGUAGE=GB&SITE=NCMTNCMT");
                httpPostJS3.setHeader("X-Distil-Ajax",XAH1);
                httpPostJS3.setHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9;q=0.8");
                httpPostJS3.setHeader("Accept-Language","en-US,en;q=0.5");
                httpPostJS3.setHeader("Accept-Encoding","gzip, deflate, br");

                HttpResponse r2 = httpClient.execute(httpPostJS3);
*/
                cookies = httpCookieStore.getCookies();
                for (Cookie c : cookies) {
                  System.out.println(c.getName() + " " + c.getValue());
                  if (c.getName().equals("JSESSIONID")) {
                    JSESSIONID = c.getValue();
                  }
                  else if (c.getName().equals("D_SID")) {
                    D_SID = c.getValue();
                  }
                  else if (c.getName().equals("D_PID")) {
                    D_PID = c.getValue();
                  }
                  else if (c.getName().equals("D_IID")) {
                    D_IID = c.getValue();
                  }
                  else if (c.getName().equals("D_UID")) {
                    D_UID = c.getValue();
                  }
                  else if (c.getName().equals("D_HID")) {
                    D_HID = c.getValue();
                  }
                  else if (c.getName().equals("D_ZID")) {
                    D_ZID = c.getValue();
                  }
                  else if (c.getName().equals("D_ZUID")) {
                    D_ZUID = c.getValue();
                  }
                }


                Connection.Response jsonResp = Jsoup.connect(
                    "https://www.checkmytrip.com/cmt/apf/pnr/retrieve?SITE=NCMTNCMT&LANGUAGE=GB&OCTX=&APPVERSION=V5")
                                                    //"https://www.checkmytrip
                                                    // .com/cmt/apf/pnr/retrieve?SITE=NCMTNCMT&LANGUAGE=GB&OCTX=")
                                                    //.cookie("JSESSIONID", sessionId)
                                                    //.cookie("D_SID", D_SID)
                                                    .cookie("JSESSIONID", JSESSIONID)
                                                    //.cookie("D_PID", D_PID)
                                                    //.cookie("D_IID", D_IID)
                                                    //.cookie("D_UID", D_UID)
                                                    //.cookie("D_HID", D_HID)
                                                    //.cookie("D_ZID", D_ZID)
                                                    //.cookie("D_ZUID", D_ZUID)
                                                    .data("data",
                                                          "{\"recLoc\":\"" + "2J2ET8" + "\",\"lastName\":\"" + "Sherman" + "\"," +
                                                          "\"checkSessionCache\":true," + "\"silent\":false}")
                                                    .header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                                                    .referrer(
                                                        "https://www.checkmytrip" +
                                                        ".com/cmt/apf/cmtng/index?LANGUAGE=GB&SITE=NCMTNCMT")
                                                    .header("User-Agent",
                                                            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) " +
                                                            "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.89 " +
                                                            "Safari/537.36")
                                                    .header("X-Distil-Ajax", XAH)
                                                    .header("X-Requested-With", "XMLHttpRequest")
                                                    .timeout(1000 * 60 * 5)
                                                    .method(Connection.Method.POST)

                                                    .execute();

                if (jsonResp.statusCode() == 200) {
                  Document doc = jsonResp.parse();
                  String s = doc.toString();

                  if (s.contains("<data>") && s.contains("</data>")) {
                    String s1 = s.substring(s.indexOf("<data>") + 6, s.indexOf("</data>"));
                    if (s1.contains("model")) {
                      System.out.println(s1.trim());
                    }
                  }
                }

              }
            }

          }
        }


      }


    }
    catch (Exception e) {
e.printStackTrace();
    }

  }

  public static void getUrl (String url, CookieStore httpCookieStore) throws Exception {
    HttpClient httpClient = HttpClientBuilder.create().setDefaultCookieStore(httpCookieStore).build();

    HttpGet httpGetPre = new HttpGet("https://www.checkmytrip.com/modules/cmtng/js/AppCtrl_44ba3b1e4ca07f1e485be4856eee8cd4.js");
    httpGetPre.setHeader("User-Agent",
                         "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) " +
                         "Chrome/55.0.2883.95 Safari/537.36");
    httpGetPre.setHeader("Referer", "https://www.checkmytrip.com/cmt/apf/cmtng/index?LANGUAGE=GB&SITE=NCMTNCMT");
    httpGetPre.setHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
    httpGetPre.setHeader("Accept-Language","en-US,en;q=0.5");
    httpGetPre.setHeader("Accept-Encoding","gzip, deflate, br");
    HttpResponse r = httpClient.execute(httpGetPre);
    System.out.println("--- "  + url );
    Thread.sleep(20);
  }

  private static HotelVO processHotel(JsonNode hSegment) {
    StringBuilder sb = new StringBuilder();
    HotelVO hotelVO = new HotelVO();


    hotelVO.setConfirmation(hSegment.get("confirmationNumber").asText());

    if (hSegment.get("departureDate") != null) {
      if (hSegment.get("departureDate").get("formatTimeAsHHMM").asText().equals("0000")) {
        hotelVO.setCheckin(getTimestamp(hSegment.get("departureDate").get("formatDateAsYYYYMMdd").asText() + "1500"));
      }
      else {
        hotelVO.setCheckin(getTimestamp(hSegment.get("departureDate").get("formatDateTimeAsYYYYMMddHHMM").asText()));

      }
    }
    if (hSegment.get("arrivalDate") != null) {
      if (hSegment.get("arrivalDate").get("formatTimeAsHHMM").asText().equals("0000")) {
        hotelVO.setCheckout(getTimestamp(hSegment.get("arrivalDate").get("formatDateAsYYYYMMdd").asText() + "1200"));
      }
      else {
        hotelVO.setCheckout(getTimestamp(hSegment.get("arrivalDate").get("formatDateTimeAsYYYYMMddHHMM").asText()));
      }
    }

    if (hSegment.get("hotelInformation") != null) {
      if (hSegment.get("hotelInformation").get("address") != null && hSegment.get("hotelInformation")
                                                                             .get("address")
                                                                             .get("firstLine") != null) {
        JsonNode addr = hSegment.get("hotelInformation").get("address");
        sb.append("Address:\n");
        sb.append(addr.get("firstLine").asText());
        sb.append("\n");
        if (addr.get("secondLine") != null && !addr.get("secondLine").asText().equals("null")) {
          sb.append(addr.get("secondLine").asText());
          sb.append("\n");
        }
        if (addr.get("city") != null && !addr.get("city").asText().equals("null")) {

          sb.append(addr.get("city").asText());
          sb.append(" ");
        }
        if (addr.get("state") != null && !addr.get("state").asText().equals("null")) {
          sb.append(addr.get("state").asText());
          sb.append(" ");
        }
        if (addr.get("zipCode") != null && !addr.get("zipCode").asText().equals("null")) {
          sb.append(addr.get("zipCode").asText());
        }
        sb.append("\n");
      }

      hotelVO.setHotelName(hSegment.get("hotelInformation").get("name").asText());

      if (hSegment.get("hotelInformation").get("phoneNumber") != null) {
        sb.append("Phone: ");
        sb.append(hSegment.get("hotelInformation").get("phoneNumber").asText());
        sb.append("\n");
      }
      if (hSegment.get("hotelInformation").get("faxNumber") != null) {
        sb.append("Fax: ");
        sb.append(hSegment.get("hotelInformation").get("faxNumber").asText());
        sb.append("\n");
      }
      sb.append("\n");
    }


    if (hSegment.get("cancellationPolicy") != null && hSegment.get("cancellationPolicy")
                                                              .asText()
                                                              .trim()
                                                              .length() > 0 && !hSegment.get("cancellationPolicy")
                                                                                        .asText()
                                                                                        .contains("null")) {
      sb.append("\n");
      sb.append("Cancellation Policy:");
      sb.append(hSegment.get("cancellationPolicy").asText());
      sb.append("\n");
    }

    if (hotelVO.isValid()) {
      hotelVO.setNote(sb.toString());
      return hotelVO;
    }

    return null;
  }

  private static TransportVO processCar(JsonNode cSegment) {
    StringBuilder sb = new StringBuilder();
    TransportVO carVO = new TransportVO();
    carVO.setBookingType(ReservationType.CAR_RENTAL);


    carVO.setConfirmationNumber(cSegment.get("confirmationNumber").asText());
    carVO.setName(cSegment.get("companyName").asText());

    if (cSegment.get("departureDate") != null) {
      carVO.setPickupDate(getTimestamp(cSegment.get("departureDate").get("formatDateTimeAsYYYYMMddHHMM").asText()));

    }
    if (cSegment.get("departureLocation") != null) {
      carVO.setPickupLocation(cSegment.get("departureLocation").get("locationName").asText() + " " + cSegment.get(
          "arrivalLocation").get("formattedCityName").asText());

    }

    if (cSegment.get("arrivalDate") != null) {
      carVO.setDropoffDate(getTimestamp(cSegment.get("arrivalDate").get("formatDateTimeAsYYYYMMddHHMM").asText()));

    }
    if (cSegment.get("arrivalLocation") != null) {
      carVO.setPickupLocation(cSegment.get("arrivalLocation").get("locationName").asText() + " " + cSegment.get(
          "arrivalLocation").get("formattedCityName").asText());

    }

    if (cSegment.get("pickupCarCompanyBean") != null) {

      if (cSegment.get("pickupCarCompanyBean").get("address") != null) {
        carVO.setPickupLocation(cSegment.get("pickupCarCompanyBean").get("address").asText());
      }
    }

    if (cSegment.get("dropoffCarCompanyBean") != null) {
      if (cSegment.get("dropoffCarCompanyBean").get("address") != null) {
        carVO.setDropoffLocation(cSegment.get("dropoffCarCompanyBean").get("address").asText());
      }
    }

    if (cSegment.get("car") != null) {
      sb.append("Car Details:\n");
      if (cSegment.get("car").get("className") != null) {
        sb.append(cSegment.get("car").get("className").asText());
        sb.append("\n");
      }
      if (cSegment.get("car").get("transmissionName") != null) {
        sb.append(cSegment.get("car").get("transmissionName").asText());
        sb.append("\n");
      }
      if (cSegment.get("car").get("typeName") != null) {
        sb.append(cSegment.get("car").get("typeName").asText());
        sb.append("\n");
      }
      if (cSegment.get("car").get("airConditioningName") != null) {
        sb.append(cSegment.get("car").get("airConditioningName").asText());
        sb.append("\n");
      }
      sb.append("\n");
    }

    if (cSegment.get("remark") != null && !cSegment.get("remark").asText().equals("null")) {
      sb.append("Remarks:\n");
      sb.append(cSegment.get("remark").asText());
      sb.append("\n");
    }


    if (carVO.isValid()) {
      carVO.setNote(sb.toString());
      return carVO;
    }

    return null;
  }

  private static FlightVO processFlight(JsonNode fSegment,
                                        HashMap<String, String> recLocators,
                                        HashMap<String, List<PassengerVO>> prefs,
                                        Map<String, String> baggageInfo) {
    StringBuilder sb = new StringBuilder();
    FlightVO flightVO = new FlightVO();
    AirportVO arrivalAirport = new AirportVO();
    if (fSegment.get("arrivalLocation") != null) {
      arrivalAirport.setCode(fSegment.get("arrivalLocation").get("locationCode").textValue());
      arrivalAirport.setCity(fSegment.get("arrivalLocation").get("formattedCityName").textValue());
      arrivalAirport.setName(fSegment.get("arrivalLocation").get("locationName").textValue());
      arrivalAirport.setCountry(fSegment.get("arrivalLocation").get("countryName").textValue());
    }
    if (fSegment.get("arrivalTerminal") != null && fSegment.get("arrivalTerminal").asText() != null && !fSegment.get(
        "arrivalTerminal").asText().equalsIgnoreCase("null")) {
      arrivalAirport.setTerminal(fSegment.get("arrivalTerminal").asText());
    }

    flightVO.setArrivalAirport(arrivalAirport);

    AirportVO departAirport = new AirportVO();
    if (fSegment.get("departureLocation") != null) {
      departAirport.setCode(fSegment.get("departureLocation").get("locationCode").textValue());
      departAirport.setCity(fSegment.get("departureLocation").get("formattedCityName").textValue());
      departAirport.setName(fSegment.get("departureLocation").get("locationName").textValue());
      departAirport.setCountry(fSegment.get("departureLocation").get("countryName").textValue());
    }
    if (fSegment.get("departureTerminal") != null && fSegment.get("departureTerminal")
                                                             .asText() != null && !fSegment.get("departureTerminal")
                                                                                           .asText()
                                                                                           .equalsIgnoreCase("null")) {
      departAirport.setTerminal(fSegment.get("departureTerminal").asText());
    }

    flightVO.setDepartureAirport(departAirport);

    if (fSegment.get("airline") != null) {
      flightVO.setName(fSegment.get("airline").get("name").textValue());
      flightVO.setCode(fSegment.get("airline").get("code").textValue());
    }
    flightVO.setNumber(fSegment.get("flightNumber").textValue());

    if (fSegment.get("departureDate") != null) {
      flightVO.setDepatureTime(getTimestamp(fSegment.get("departureDate")
                                                    .get("formatDateTimeAsYYYYMMddHHMM")
                                                    .asText()));
    }
    if (fSegment.get("arrivalDate") != null) {
      flightVO.setArrivalTime(getTimestamp(fSegment.get("arrivalDate").get("formatDateTimeAsYYYYMMddHHMM").asText()));
    }

    if (fSegment.get("equipmentName") != null) {
      sb.append("Aircraft: ");
      sb.append(fSegment.get("equipmentName").textValue()).append("\n");
    }
    if (fSegment.get("formattedFlightTime") != null) {
      sb.append("Duration: ");
      sb.append(fSegment.get("formattedFlightTime").textValue()).append("\n");
    }

    if (baggageInfo != null && baggageInfo.containsKey(fSegment.get("itemId").asText())) {
      sb.append("Baggage Allowance: ");
      sb.append(fSegment.get("itemId").asText());
      sb.append("\n");
    }
    else {
      if (fSegment.get("travellerTypesInfos") != null && fSegment.get("travellerTypesInfos")
                                                                 .get("ADT") != null && fSegment.get(
          "travellerTypesInfos").get("ADT").get("baggageAllowance") != null) {
        JsonNode baggageNode = fSegment.get("travellerTypesInfos").get("ADT").get("baggageAllowance");
        if (baggageNode != null && baggageNode.get("value") != null) {
          sb.append("Baggage Allowance: ");
          sb.append(baggageNode.get("value").asText());
          sb.append(" ");
          if (baggageNode.get("unitOfMeasure") != null && baggageNode.get("unitOfMeasure").asText().equals("PC")) {
            sb.append(" piece(s) per traveler");
          }
          else {
            sb.append(baggageNode.get("unitOfMeasure").asText());
            sb.append(" per traveler");
          }
        }
        sb.append("\n");
      }
    }


    flightVO.setNotes(sb.toString());
    int itemId = fSegment.get("itemId").asInt();

    if (flightVO.isValid()) {
      flightVO.setReservationNumber(recLocators.get(flightVO.getCode() + flightVO.getNumber()));
      List<PassengerVO> passengerVOs = prefs.get(String.valueOf(itemId));
      if (fSegment.get("cabins") != null && fSegment.get("cabins").isArray() && fSegment.get("cabins").size() == 1) {
        if (fSegment.get("cabins").get(0).get("name") != null) {
          String cabin = fSegment.get("cabins").get(0).get("name").asText();
          for (PassengerVO pVO : passengerVOs) {
            pVO.setSeatClass(cabin);
          }
        }
      }
      flightVO.setPassengers(passengerVOs);
    }


    return flightVO;
  }


  private static NoteVO processMiscAsNote(JsonNode misc) {
    StringBuilder notes = new StringBuilder();
    String departCity = null;
    String departLoc = null;
    String departCountry = null;

    String arriveCity = null;
    String arriveLoc = null;
    String arriveCountry = null;

    Timestamp departTS = null;
    Timestamp arriveTS = null;


    if (misc.get("departureLocation") != null) {
      StringBuilder depart = new StringBuilder();
      if (misc.get("departureLocation").get("locationName") != null) {
        departLoc = misc.get("departureLocation").get("locationName").asText();
        if (departLoc != null && !departLoc.isEmpty()) {
          depart.append(departLoc);
        }
      }
      if (misc.get("departureLocation").get("cityName") != null) {
        departCity = misc.get("departureLocation").get("cityName").asText();
        if (departCity != null && !departCity.isEmpty()) {
          if (!depart.toString().isEmpty()) {
            depart.append(", ");
          }
          depart.append(departCity);
        }
      }
      if (misc.get("departureLocation").get("countryName") != null) {
        departCountry = misc.get("departureLocation").get("countryName").asText();
        if (departCountry != null && !departCountry.isEmpty()) {
          if (!depart.toString().isEmpty()) {
            depart.append(", ");
          }
          depart.append(departCountry);
        }
      }
      if (!depart.toString().isEmpty()) {
        notes.append("Departure Information: ");
        notes.append(depart);
        notes.append("<br>");
      }
    }
    if (misc.get("departureDate") != null && misc.get("departureDate").get("formatDateTimeAsYYYYMMddHHMM") != null) {
      departTS = getTimestamp(misc.get("departureDate").get("formatDateTimeAsYYYYMMddHHMM").asText());
      if (departTS != null) {
        notes.append("Departure Time: ");
        notes.append(Utils.formatDateTimePrint(departTS.getTime()));
        notes.append("<br><br>");
      }
    }


    if (misc.get("arrivalLocation") != null) {
      StringBuilder arrive = new StringBuilder();
      if (misc.get("arrivalLocation").get("locationName") != null) {
        arriveLoc = misc.get("arrivalLocation").get("locationName").asText();
        if (arriveLoc != null && !arriveLoc.isEmpty()) {
          arrive.append(arriveLoc);
        }
      }
      if (misc.get("arrivalLocation").get("cityName") != null) {
        arriveCity = misc.get("arrivalLocation").get("cityName").asText();
        if (arriveCity != null && !arriveCity.isEmpty()) {
          if (!arrive.toString().isEmpty()) {
            arrive.append(", ");
          }
          arrive.append(arriveCity);
        }
      }
      if (misc.get("arrivalLocation").get("countryName") != null) {
        arriveCountry = misc.get("arrivalLocation").get("countryName").asText();
        if (arriveCountry != null && !arriveCountry.isEmpty()) {
          if (!arrive.toString().isEmpty()) {
            arrive.append(", ");
          }
          arrive.append(arriveCountry);
        }
      }
      if (!arrive.toString().isEmpty()) {
        notes.append("Arrival Information: ");
        notes.append(arrive);
        notes.append("<br>");
      }
    }
    if (misc.get("arrivalDate") != null && misc.get("arrivalDate").get("formatDateTimeAsYYYYMMddHHMM") != null) {
      arriveTS = getTimestamp(misc.get("arrivalDate").get("formatDateTimeAsYYYYMMddHHMM").asText());
      if (arriveTS != null) {
        notes.append("Arrival Time: ");
        notes.append(Utils.formatDateTimePrint(arriveTS.getTime()));
        notes.append("<br><br>");
      }
    }

    String txt = null;
    if (misc.get("freeText") != null && misc.get("freeText").asText() != null && !misc.get("freeText")
                                                                                      .asText()
                                                                                      .isEmpty()) {
      txt = misc.get("freeText").asText();
    }

    String remark = null;
    if (misc.get("remark") != null && misc.get("remark").asText() != null && !misc.get("remark").asText().isEmpty()) {
      remark = misc.get("remark").asText();
    }
    if (txt != null) {
      notes.append(txt);
      notes.append("<br>");
    }
    if (remark != null && !notes.toString().contains(txt)) {
      notes.append(remark);
      notes.append("<br>");
    }

    NoteVO noteVO = new NoteVO();
    noteVO.setName("Miscellaneous");
    noteVO.setTimestamp(departTS);
    noteVO.setNote(notes.toString());
    return noteVO;
  }

  private static Timestamp getTimestamp(String timestamp) {
    try {
      Timestamp t = new Timestamp(du.parseDate(timestamp, dateTimeFormat).getTime());
      return t;
    }
    catch (Exception e) {
    }
    return null;
  }
}
