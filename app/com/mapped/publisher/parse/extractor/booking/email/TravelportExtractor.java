package com.mapped.publisher.parse.extractor.booking.email;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.EmailBodyExtractor;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by twong on 2015-02-27.
 */
public class TravelportExtractor extends EmailBodyExtractor {

  private static String[] dateTimeFormat = {"yyyy-MM-dd kk:mm", "yyyy-MM-dd h:mm a", "yyyy-MM-dd kk:mm",
                                            "M/dd/yyyy h:mm:ss a", "dd MMM yyyy", "dd MMM yyyy ",
                                            "E dd MMM yyyy h:mma", "E dd MMM yyyy", "E dd MMM yyyy ",
                                            "E dd MMMMM yyyy HH:mm", "dd MMM yyyy HH:mm a", "E dd MMM yyyy HH:mm",
                                            "dd MMMMM yyyy HH:mm", "E, MMMMM dd, yyyy HH:mm", "E, MMMMM dd, yyyy",
                                            "E, MMMMM dd, yyyy hh:mm a", "EEE dd MMM yyyy h:mm a",
                                            "EEE dd MMM yyyy 0h:mm a", "EEEE, MMMM dd, yyyy h:mm a"};

  private static DateUtils du = new DateUtils();

  protected void initImpl(Map<String, String> params) {
  }

  protected TripVO extractDataImpl(String input) throws Exception {

    return null;
  }

  @Override
  public ArrayList<ParseError> getParseErrors()
  {
    return null;
  }

  public static TripVO getInfo(String info){

    TripVO trip = new TripVO();
    info = info.replaceAll("&amp;","&");

    Matcher passengers = Pattern.compile("\"first_name\"\\s:\\s\"[A-Z]+\"").matcher(info);

    List<FlightVO> flights = getFlights(info);
    List<HotelVO> hotels = getHotels(info);
    List<ActivityVO> activities = getActivities(info);
    List<TransportVO> cars = getCarRentals(info);

    for(FlightVO flight : flights)
      trip.addFlight(flight);

    for(HotelVO hotel : hotels)
      trip.addHotel(hotel);

    for(ActivityVO activity : activities)
      trip.addActivity(activity);

    for(TransportVO car : cars)
      trip.addTransport(car);

    //try to enhance the bookings
    Document doc = Jsoup.parse(info);
    Elements elements =  doc.getElementsByClass("segBorder");

    for (Element e1: elements) {
      boolean found = false;
      Elements innerElements =  e1.getElementsByClass("segInnerPadding");
      if (innerElements != null && innerElements.size() > 0) {
        Element e = innerElements.get(0);
        String mainText = e1.text();
        String txt = e.text();


        if (txt != null) {
          for (FlightVO flightVO : trip.getFlights()) {
            if (flightVO.getDepartureAirport() != null && flightVO.getDepartureAirport().getCode() != null && flightVO.getArrivalAirport() != null && flightVO.getArrivalAirport().getCode() != null) {
              StringBuilder regex = new StringBuilder();
              regex.append("\\(");
              regex.append(flightVO.getDepartureAirport().getCode());
              regex.append("\\).*.\\(");
              regex.append(flightVO.getArrivalAirport().getCode());
              regex.append("\\)");
              Matcher airports = Pattern.compile(regex.toString()).matcher(txt);

              if (txt.contains(flightVO.getNumber()+ " ") && airports.find()) {
                enhanceFlight(flightVO, e);
                found = true;
                break;
              }
            }
          }
          if (!found) {
            for (HotelVO hotelVO : trip.getHotels()) {
              if ((txt.toLowerCase().contains(hotelVO.getHotelName().toLowerCase()) || mainText.toLowerCase()
                                                                                               .contains(hotelVO
                                                                                                             .getHotelName()
                                                                                                                .toLowerCase())) && txt
                      .contains("Night") && hotelVO.getConfirmation() == null) {
                enhanceHotel(hotelVO, e);
                found = true;
                break;
              }
            }
          }
          if (!found) {
            for (TransportVO carVO : trip.getTransport()) {
              String sdate = Utils.formatTimestamp(carVO.getPickupDate().getTime(),"MMMM dd, YYYY");
              String edate = Utils.formatTimestamp(carVO.getPickupDate().getTime(),"MMMM dd, YYYY");
              String lower = mainText.toLowerCase();
              if (mainText.contains(carVO.getName()) && sdate != null && edate != null && lower.contains(sdate.toLowerCase()) && lower.contains(edate.toLowerCase())) {
                enhanceCar(carVO, e);
                found = true;
                break;
              }
            }
          }
        }
      }
    }
    trip.setImportSrc(BookingSrc.ImportSrc.TRAVELPORT__VIEWTRIPS);
    trip.setImportTs(System.currentTimeMillis());
    return trip;
  }

  public static List<FlightVO> getFlights(String origInfo) {

    //narrow the search string to only the flight segment to prevent duplicate issues
    String info = "";
    if (origInfo.contains("\"air_segments\"")) {
      info = origInfo.substring(origInfo.indexOf("air_segments"));
      if (info.contains("]")) {
        info = info.substring(0, info.indexOf("]"));
      }
    }

    List<FlightVO> flights = new ArrayList<FlightVO>();

    Matcher destinations = Pattern.compile("\"destination\":\"[A-Z]{3}\"").matcher(info);
    Matcher originations = Pattern.compile("\"origination\":\"[A-Z]{3}\"").matcher(info);
    Matcher departureDates = Pattern.compile("\"departure_date\":\"[0-9]{4}-[0-9]{2}-[0-9]{2}").matcher(info);
    Matcher arrivalDates = Pattern.compile("\"arrival_date\":\"[0-9]{4}-[0-9]{2}-[0-9]{2}").matcher(info);
    Matcher serviceClasses = Pattern.compile("\"service_class\":\"[A-Z]\"").matcher(info);
    Matcher partySizes = Pattern.compile("\"party_size\":[0-9]+").matcher(info);
    Matcher flightNumbers = Pattern.compile("\"flight_number\":\"[0-9]+\"").matcher(info);
    Matcher carrierCodes = Pattern.compile("\"carrier_code\":\"[a-zA-Z0-9]+\"").matcher(info);
    Matcher flightDurations = Pattern.compile("\"flight_duration\":[0-9]+").matcher(info);

    String departureDate = "";
    String departureTime = "";
    String arrivalDate = "";
    String arrivalTime = "";

    //if(departureDates.find())
    //    System.out.println(departureDates.group().substring(departureDates.group().indexOf(":") + 1));

    while(true) {

      FlightVO flight = new FlightVO();

      flight.setDepartureAirport(new AirportVO());
      flight.setArrivalAirport(new AirportVO());

      if(destinations.find())
        flight.getArrivalAirport().setCode(destinations.group().substring(destinations.group().length() - 4, destinations.group().length() - 1));

      if(originations.find())
        flight.getDepartureAirport().setCode(originations.group().substring(originations.group().length() - 4, originations.group().length() - 1));

      if(departureDates.find()) {
        String departureDateTime = departureDates.group().substring(departureDates.group().indexOf(":") + 1);
        departureDateTime = departureDateTime.replace("\"","");
        if (departureDateTime.contains("T")) {
          departureDateTime = departureDateTime.replace("T", " ");
          departureDate = departureDateTime.substring(0, departureDateTime.indexOf(" "));
          departureTime = departureDateTime.substring(departureDateTime.indexOf(" ") + 1);
        } else {
          departureDate = departureDateTime;
          departureTime = "00:00";
        }

        flight.setDepatureTime(getTimestamp(departureDate, departureTime));
      }

      if(arrivalDates.find()) {
        String arrivalDateTime = arrivalDates.group().substring(arrivalDates.group().indexOf(":") + 1);
        arrivalDateTime = arrivalDateTime.replace("\"","");
        if (arrivalDateTime.contains("T")) {
          arrivalDateTime = arrivalDateTime.replace("T", " ");
          arrivalDate = arrivalDateTime.substring(0, arrivalDateTime.indexOf(" "));
          arrivalTime = arrivalDateTime.substring(arrivalDateTime.indexOf(" ") + 1);
        } else {
          arrivalDate = arrivalDateTime;
          arrivalTime = "00:00";
        }

        flight.setArrivalTime(getTimestamp(arrivalDate, arrivalTime));
      }

      if(serviceClasses.find());
      if(partySizes.find());
      if(flightDurations.find());

      if(flightNumbers.find()) {
        String s = flightNumbers.group();
        if (s != null && s.contains(":")) {
          s = s.replaceAll("\"","");
          flight.setNumber(s.substring(s.indexOf(":")+1));
        }
      }


      if(carrierCodes.find()) {
        String s = carrierCodes.group();
        if (s != null && s.contains(":")) {
          s = s.replaceAll("\"","");
          flight.setCode(s.substring(s.indexOf(":") + 1));
        }
      }

      if(destinations.hitEnd() && originations.hitEnd() && departureDates.hitEnd() && arrivalDates.hitEnd() &&
          flightNumbers.hitEnd() && carrierCodes.hitEnd())
        break;

      if (flight.isValid()) {
        flights.add(flight);
      }
    }

    return flights;
  }

  public static List<HotelVO> getHotels(String origInfo) {

    //narrow the search string to only the flight segment to prevent duplicate issues
    String info = "";
    if (origInfo.contains("\"hotel_segments\"")) {
      info = origInfo.substring(origInfo.indexOf("hotel_segments"));
      if (info.contains("]")) {
        info = info.substring(0, info.indexOf("]"));
      }
    }

    List<HotelVO> hotels = new ArrayList<HotelVO>();

    Matcher hotelNames = Pattern.compile("\"name\":\"[A-Za-z0-9\\s,-]+\"").matcher(info);
    Matcher checkinDates = Pattern.compile("\"check_in_date\":\"[0-9]{4}-[0-9]{2}-[0-9]{2}\"").matcher(info);
    Matcher checkoutDates = Pattern.compile("\"check_out_date\":\"[0-9]{4}-[0-9]{2}-[0-9]{2}\"").matcher(info);

    while(true) {

      HotelVO hotel = new HotelVO();

      if(hotelNames.find())
        hotel.setHotelName(hotelNames.group().substring(8, hotelNames.group().length() - 1).trim());

      if(checkinDates.find()) {
        String checkinDate = checkinDates.group().substring(checkinDates.group().indexOf(":") + 1);
        checkinDate = checkinDate.replace("\"","");
        hotel.setCheckin(getTimestamp(checkinDate, "15:00"));
      }

      if(checkoutDates.find()) {
        String checkoutDate = checkoutDates.group().substring(checkoutDates.group().indexOf(":") + 1);
        checkoutDate = checkoutDate.replace("\"","");
        hotel.setCheckout(getTimestamp(checkoutDate, "12:00"));
      }

      if(hotelNames.hitEnd() && checkinDates.hitEnd() && checkoutDates.hitEnd())
        break;

      if (hotel.isValid()) {
        hotels.add(hotel);
      }
    }

    return hotels;
  }

  public static List<ActivityVO> getActivities (String origInfo) {
    //narrow the search string to only the flight segment to prevent duplicate issues
    String info = "";
    if (origInfo.contains("\"tour_segments\"")) {
      info = origInfo.substring(origInfo.indexOf("tour_segments"));
      if (info.contains("]")) {
        info = info.substring(0, info.indexOf("]"));
      }
    }

    List<ActivityVO> activities = new ArrayList<ActivityVO>();

    Matcher vendorCode = Pattern.compile("\"vendor_code\":\".*\"").matcher(info);
    Matcher rawData = Pattern.compile("\"raw_data\":\".*\"").matcher(info);
    Matcher rate = Pattern.compile("\"rate\":\".*\"").matcher(info);
    Matcher partySize = Pattern.compile("\"party_size\":\".*\"").matcher(info);


    Matcher startDate = Pattern.compile("\"start_date\":\"[0-9]{4}-[0-9]{2}-[0-9]{2}\"").matcher(info);
    Matcher endDate = Pattern.compile("\"end_date\":\"[0-9]{4}-[0-9]{2}-[0-9]{2}\"").matcher(info);
    Matcher startLocation = Pattern.compile("\"airport\":\".*\"").matcher(info);

    StringBuffer note = new StringBuffer();
    while(true) {
      ActivityVO activity = new ActivityVO();

      if(startDate.find()) {
        String date = startDate.group().substring(startDate.group().indexOf(":") + 1);
        date = date.replace("\"","");
        activity.setStartDate(getTimestamp(date,"00:00"));
      }

      if(endDate.find()) {
        String date = endDate.group().substring(endDate.group().indexOf(":") + 1);
        date = date.replace("\"","");
        activity.setEndDate(getTimestamp(date, "00:00"));
      }

      if(startLocation.find()) {
        String s = startLocation.group().substring(startLocation.group().indexOf(":") + 1).replaceAll("\"","").trim();
        if (s.length() > 0) {
          note.append("\n Start Location: ");note.append(s);note.append("\n");
        }
      }

      if (vendorCode.find()) {
        String s = vendorCode.group().substring(vendorCode.group().indexOf(":") + 1).replaceAll("\"","").trim();
        activity.setName(s);
      }

      if (rawData.find()) {
        String s = rawData.group().substring(rawData.group().indexOf(":") + 1).replaceAll("\"","").trim();
        if (s.length() > 0) {
          note.append("\n Note:");note.append(s);note.append("\n");
        }
      }

      if (rate.find()) {
        String s = rate.group().substring(rate.group().indexOf(":") + 1).replaceAll("\"","").trim();
        if (s.length() > 0) {
          note.append("\n Rate:");note.append(s);note.append("\n");
        }      }

      if (partySize.find()) {
        String s = partySize.group().substring(partySize.group().indexOf(":") + 1).replaceAll("\"","").trim();
        if (s.length() > 0) {
          note.append("\n Party Size:");note.append(s);note.append("\n");
        }
      }

      if(startDate.hitEnd() && endDate.hitEnd() && startLocation.hitEnd())
        break;

      if (activity.isValid()) {
        activity.setNote(note.toString());
        activities.add(activity);
      }
    }

    return activities;
  }

  public static List<TransportVO> getCarRentals (String origInfo) {
    //narrow the search string to only the flight segment to prevent duplicate issues
    String info = "";
    if (origInfo.contains("\"car_segments\"")) {
      info = origInfo.substring(origInfo.indexOf("car_segments"));
      if (info.contains("]")) {
        info = info.substring(0, info.indexOf("]"));
      }
    }

    List<TransportVO> cars = new ArrayList<TransportVO>();

    Matcher name = Pattern.compile("\"name\":\".*\"").matcher(info);
    Matcher rawData = Pattern.compile("\"raw_data\":\".*\"").matcher(info);
    Matcher rate = Pattern.compile("\"rate\":\".*\"").matcher(info);
    Matcher partySize = Pattern.compile("\"party_size\":\".*\"").matcher(info);


    Matcher startDate = Pattern.compile("\"start_date\":\"[0-9]{4}-[0-9]{2}-[0-9]{2}\"").matcher(info);
    Matcher endDate = Pattern.compile("\"return_date\":\"[0-9]{4}-[0-9]{2}-[0-9]{2}\"").matcher(info);
    Matcher startLocation = Pattern.compile("\"start_location\":\".*\"").matcher(info);
    Matcher returnLocation = Pattern.compile("\"return_location\":\".*\"").matcher(info);


    StringBuffer note = new StringBuffer();
    while(true) {
      TransportVO car = new TransportVO();

      if(startDate.find()) {
        String date = startDate.group().substring(startDate.group().indexOf(":") + 1);
        date = date.replace("\"","");
        car.setPickupDate(getTimestamp(date, "00:00"));
      }

      if(endDate.find()) {
        String date = endDate.group().substring(endDate.group().indexOf(":") + 1);
        date = date.replace("\"","");
        car.setDropoffDate(getTimestamp(date, "00:00"));
      }

      if(startLocation.find()) {
        String s = startLocation.group().substring(startLocation.group().indexOf(":") + 1).replaceAll("\"","").trim();
        if (s.length() > 0) {
          if (s.startsWith(",")) {
            s = s.replace(",","");
          }
          car.setPickupLocation(s.trim());
        }
      }

      if(name.find()) {
        String s = name.group().substring(name.group().indexOf(":") + 1).replaceAll("\"","").trim();
        if (s.length() > 0) {
          car.setName(s);
        }
      }

      if (returnLocation.find()) {
        String s = returnLocation.group().substring(returnLocation.group().indexOf(":") + 1).replaceAll("\"","").trim();
        if (s.length() > 0) {
          car.setDropoffLocation(s);
        }
      }



      if(name.hitEnd() && startDate.hitEnd() && endDate.hitEnd() && startLocation.hitEnd())
        break;

      if (car.isValid()) {
        car.setBookingType(ReservationType.CAR_RENTAL);
        cars.add(car);
      }
    }

    return cars;

  }

  private static Timestamp getTimestamp (String date, String time) {
    try {
      Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime() );
      return t;
    } catch (Exception e) {
    }
    return null;
  }

  private static Timestamp getTimestamp (String timestamp) {
    try {
      Timestamp t = new Timestamp(du.parseDate(timestamp, dateTimeFormat).getTime() );
      return t;
    } catch (Exception e) {
    }
    return null;
  }

  public static Document getItinerary (String name, String locator) {
    try {
      Connection.Response homeResp= Jsoup.connect("https://www.viewtrip.com/VTHome.aspx")
                                         .method(Connection.Method.GET)
                                         .execute();
      String aspSessionId = homeResp.cookie("ASP.NET_SessionId"); // you will need
     Document homeHtml = homeResp.parse();

      String __VIEWSTATEGENERATOR = homeHtml.select("input[name=__VIEWSTATEGENERATOR]").first().attr("value");
      String __EVENTTARGET = "lbtnViewReservation";//homeHtml.select("input[name=__EVENTTARGET]").first().attr("value");
      String __EVENTARGUMENT ="";// homeHtml.select("input[name=__EVENTARGUMENT]").first().attr("value");
      String __LASTFOCUS = "";//homeHtml.select("input[name=__LASTFOCUS]").first().attr("value");
      String __VIEWSTATE = homeHtml.select("input[name=__VIEWSTATE]").first().attr("value");
      String __EVENTVALIDATION = homeHtml.select("input[name=__EVENTVALIDATION]").first().attr("value");

      Connection.Response loginResp= Jsoup.connect("https://www.viewtrip.com/VTHome.aspx")
                                          .followRedirects(false)
                                          .timeout(3*1000)
                                          .cookie("ASP.NET_SessionId",aspSessionId)
                                          .cookie("Ctry", "")
                                          .cookie("Save", "1")
                                          .cookie("hr", "12")
                                          .cookie("Fsz", "default")
                                          .cookie("Settings", "hr=12&Fsz=default")
                                          .cookie("Z","-5")
                                          .cookie("__qca", "P0-2098068348-1425044412325")
                                          .cookie("__utma", "71541004.1874588717.1424710718.1424958915.1425044472.6")
                                          .cookie("__utmc", "71541004")
                                          .cookie("__utmz", "71541004.1424711214.2.2.utmcsr=1G_0GX2|utmccn=Itinerary|utmcmd=email")
                                          .cookie("__utma", "1.803640470.1424710718.1425044412.1425045723.10")
                                          .cookie("__utmb", "1.2.10.1425045723")
                                          .cookie("__utmc", "1")
                                          .cookie("__utmz", "1.1424711214.2.2.utmcsr=1G_0GX2|utmccn=Itinerary|utmcmd=email")
                                          .data("__EVENTTARGET",__EVENTTARGET)
                                          .data("__EVENTARGUMENT",__EVENTARGUMENT)
                                          .data("__LASTFOCUS",__LASTFOCUS)
                                          .data("__VIEWSTATE",__VIEWSTATE)
                                          .data("__EVENTVALIDATION",__EVENTVALIDATION)
                                          .data("__VIEWSTATEGENERATOR",__VIEWSTATEGENERATOR)
                                          .data("ddlLanguage","en-US")
                                          .data("txtLoginEmail","Email Address")
                                          .data("pwPWord","Password")
                                          .data("txtPassword","password")
                                          .data("remember","on")
                                          .data("txtEmailAddress","Email Address")
                                          .data("txtAnswer","answer")
                                          .data("txtChangePasswordSW","Password")
                                          .data("txtChangePasswordP","password")
                                          .data("txtConfirmPasswordSW","Confirm Password")
                                          .data("txtConfirmPassword","confirm password")
                                          .data("CustomHiddenField","")
                                          .data("txtStyleSheet","default")
                                          .data("txtIdEUuser","False")
                                          .data("txtLocator", locator)
                                          .data("txtLName", name)
                                          .method(Connection.Method.POST)
                                          .execute();

      Document printDoc1 = loginResp.parse();

      if (loginResp.statusCode() == 302) {
        try {
          Thread.sleep(1000);//1 sec delay
        }
        catch (InterruptedException ie) {

        }

        int count = 0;
        while (count < 3) {
          count ++;
          try {
            Connection.Response itinResp = Jsoup.connect("https://www.viewtrip.com/Itin.aspx")
                                                .followRedirects(false)
                                                .referrer("https://www.viewtrip.com/VTWaitPage.aspx")
                                                .cookie("ASP.NET_SessionId", aspSessionId)
                                                .cookie("Core", "1G")
                                                .cookie("VTLang", "en-US")
                                                .cookie("Ctry", "")
                                                .cookie("Save", "1")
                                                .cookie("hr", "12")
                                                .cookie("Fsz", "default")
                                                .cookie("Settings", "hr=12&Fsz=default")
                                                .cookie("Z", "-5")
                                                .timeout(20 * 1000)
                                                .method(Connection.Method.GET)
                                                .execute();
            if (itinResp.statusCode() == 200) {
              Document printDoc = itinResp.parse();
              if (printDoc != null) {
                return printDoc;
              }
            }
            else if (itinResp.statusCode() == 302) { //might not be ready yet - so sleep for a sec
              try {
                Thread.sleep(1000);//1 sec delay
              }
              catch (InterruptedException ie) {

              }
            }
          } catch (Exception e) {
            try {
              Thread.sleep(1000);//1 sec delay
            }
            catch (InterruptedException ie) {

            }
          }
        }
      }
    } catch (Exception e){
    }

    return null;

  }

  public static void main(String[] args) {

    try {
/*
      Document doc = getItinerary("GUARINO", "n8tpqh");
      if (doc != null) {
        TripVO tripVO = getInfo(doc.toString());
        System.out.println(tripVO);
      }

*/
      String s = FileUtils.readFileToString(new File("/Volumes/data2/temp/travelport9.html"));
      TripVO tripVO = getInfo(s);

      System.out.println(tripVO);

    }

    catch(Exception e) {
      e.printStackTrace();
    }
  }

  public static void enhanceFlight(FlightVO flightVO, Element e) {
    StringBuilder sb = new StringBuilder();

    String departTime = "12:00 AM";
    String arriveTime = "12:00 AM";


    Elements tables = e.select("table");
    if (tables != null && tables.size() > 0) {
      //process first table
      Elements rows = tables.get(0).select("tr");
      if (rows != null && rows.size() > 0) {
        for (Element row : rows) {
          Elements cols = row.select("td");
          if (cols.size() == 2) {
            String col1 = cols.get(0).text();
            String col2 = cols.get(1).text();
            if (col1.contains("Confirmation Number")) {
              flightVO.setReservationNumber(col2);
            } else if (col1.contains("Class of Service:")) {
              sb.append(col1);sb.append(" ");sb.append(col2);sb.append("\n");
            }

          } else if (cols.size() == 3) {
            String col1 = cols.get(0).text();
            String col2 = cols.get(1).text();
            String col3 = cols.get(2).text();

            if (col1.contains("Depart") && col3.contains("Terminal")) {
              Elements elements = cols.get(2).select("nobr");
              if (elements != null && elements.size() > 0)
              flightVO.getDepartureAirport().setTerminal(elements.get(0).text());
            }
          }
        }
      }

      if (rows != null && rows.size() > 0) {
        for (Element row : rows) {
          Elements cols = row.select("td");
          if (cols.size() >1) {
            String col1 = cols.get(0).text();
            String col2 = cols.get(1).text();
            if (col1.contains("Depart:")) {
              departTime = col2.trim();
              if (departTime.contains("AM")) {
                departTime = departTime.substring(0, departTime.indexOf("AM") + 2);
              } else if (departTime.contains("PM")) {
                departTime = departTime.substring(0, departTime.indexOf("PM") + 2);
              }
            }
            else if (col1.contains("Arrive:")) {
              arriveTime = col2.trim();
              if (arriveTime.contains("AM")) {
                arriveTime = arriveTime.substring(0, arriveTime.indexOf("AM") + 2);
              } else if (arriveTime.contains("PM")) {
                arriveTime = arriveTime.substring(0, arriveTime.indexOf("PM") + 2);
              }
            }
          }
        }
      }

      if (tables.size() >1) {
        //see if the second table is for seats
        Element secondTable = tables.get(1);
        String temp = getTextNoBr(secondTable);
        String[] tokens = temp.split("\n");
        boolean pSpecialReq = false;
        for (String s : tokens) {
          if (!s.contains("Click here for Flight Service Information")) {
            if (s.contains("Click here for Special Services Requested")) {
              pSpecialReq = true;
            } else if (s.contains("Remarks")){
              pSpecialReq = false;
            }
            if (pSpecialReq) {
              if (s.contains("Ticket Number"))
                sb.append(s);sb.append("\n");
            } else {
              if (!s.contains("In-Flight Services") && s.trim().length() > 0) {
                sb.append(s);sb.append("\n");
              }
            }
          }

        }
      }


    }

    //see if we can get a seat
    String seatId = null;
    String text = e.toString();
    if (text.contains("SeatPax")) {
      seatId = text.substring(text.indexOf("SeatPax"));
      seatId = seatId.substring(0, seatId.indexOf("\">"));
      Element seatDiv = e.getElementById(seatId);
      if (seatDiv.hasText()) {
        sb.append("Seat: "); sb.append(seatDiv.text());
        sb.append("\n");
      }
    }

    flightVO.setDepatureTime(getTimestamp(Utils.formatTimestamp(flightVO.getDepatureTime().getTime(), "yyyy-MM-dd"), departTime));
    flightVO.setArrivalTime(getTimestamp(Utils.formatTimestamp(flightVO.getArrivalTime().getTime(), "yyyy-MM-dd"),
                                         arriveTime));

    flightVO.setNotes(sb.toString());
  }

  public static void enhanceHotel(HotelVO hotelVO, Element e) {
    String checkinTime = "";
    String checkouTime = "";

    StringBuilder sb = new StringBuilder();
    Elements tables = e.select("table");
    if (tables != null && tables.size() > 0) {
      Elements rows = tables.get(0).select("tr");
      if (rows != null && rows.size() > 0) {
        for (Element row: rows) {
          Elements cols = row.select("td");
          if (cols != null && cols.size() > 1) {
            Element col1 = cols.get(0);
            Element col2 = cols.get(1);
            if (col1.text().contains("Confirmation Number")) {
              hotelVO.setConfirmation(col2.text());
            } else if (col1.hasClass("vcard")) {
              sb.append(getText(col1));sb.append("\n");sb.append("\n");
            } else if (col1.text().contains("Check in")) {
              checkinTime = col2.text().trim();
            } else if (col1.text().contains("Check In Time:")) {
              checkinTime += " " + col2.text().trim();
            } else if (col1.text().contains("Check Out")) {
              checkouTime = col2.text().trim();
            } else if (col1.text().contains("Check Out Time:")) {
              checkouTime += " " + col2.text().trim();
            }

            if (col2.text().contains("Phone:") || col2.text().contains("Fax:")) {
              sb.append(getText(col2));sb.append("\n");
            }
            if (col1.text().contains("Phone:") || col1.text().contains("Fax:")) {
              sb.append(getText(col1));sb.append("\n");
            }

          } else if (cols != null && cols.size() == 1){
            Element col1 = cols.get(0);
            if (col1.text().contains("Phone:") || col1.text().contains("Fax:")) {
              sb.append(getText(col1));sb.append("\n");
            }
          }
        }
      }

      //try to get additional notes
      if (tables.size() >1) {
        Element table = tables.get(1);
        if (table.toString().contains("Click here to show or hide additional information")) {
          Element row = table.select("tr").first();
          Element col = row.select("td").first();
          Elements innerTables = col.select("table");
          if (innerTables != null && innerTables.size() > 0) {
            for (Element innerTable:innerTables) {
              if (innerTables.indexOf(innerTable) > 0) {
                sb.append("\n");
              }
              String s = getText(innerTable);
              if (!sb.toString().contains(s)) {
                sb.append(getText(innerTable));
                sb.append("\n");
              }
            }

          }

        }
      }
    }

    if (checkinTime.trim().length() > 5) {
      Timestamp t = getTimestamp(checkinTime);
      if (t != null) {
        hotelVO.setCheckin(t);
      }
    }

    if (checkouTime.trim().length() > 5) {
      Timestamp t = getTimestamp(checkouTime);
      if (t != null) {
        hotelVO.setCheckout(t);
      }
    }
    hotelVO.setNote(sb.toString());
  }

  public static void enhanceCar(TransportVO carVO, Element e) {
    StringBuilder sb = new StringBuilder();
    Elements tables = e.select("table");
    if (tables != null && tables.size() > 0) {
      Elements rows = tables.get(0).select("tr");
      if (rows != null && rows.size() > 0) {
        int i = 0;
        for (Element row: rows) {
          Elements cols = row.select("td");
          if (cols != null && cols.size() > 1) {
            Element col1 = cols.get(0);
            Element col2 = cols.get(1);
            if (i == 0) {
              if (col1.hasText()) {
                carVO.setCmpyName(col1.text());
              }
              if (cols.size() > 2) {
                Element col3 = cols.get(2);
                if (col3.hasText()) {
                  sb.append(col3.text());
                  sb.append("\n");
                }
              }

            } else if (col1.text().contains("Confirmation Number")) {
              carVO.setConfirmationNumber(col2.text());
            } else if (col1.text().contains("Pick Up:")) {
              String pAddr = getText(col1);
              if (col1.toString().contains("\"adr\"")) {
                Elements addr = col1.select("span.adr");
                if (addr != null && addr.hasText()) {
                  carVO.setPickupLocation(addr.text());
                }

                sb.append(getText(col1));
                sb.append("\n");
                sb.append("\n");
              }
              //looks for the time
              Timestamp pickuptime = getTimestamp(col2.text());
              if (pickuptime != null) {
                carVO.setPickupDate(pickuptime);
              }

            } else if (col1.text().contains("Corporate discount number:") ||col1.text().contains("Mileage:") ) {
              sb.append(getText(col1));sb.append(" ");sb.append(getText(col2));sb.append("\n");sb.append("\n");
            } else if (col1.text().contains("Return:")) {
              String pAddr = getText(col1);
              if (col1.toString().contains("\"adr\"")) {
                Elements addr = col1.select("span.adr");
                if (addr != null && addr.hasText()) {
                  carVO.setDropoffLocation(addr.text());
                }
                sb.append(pAddr);
                sb.append("\n");
                sb.append("\n");
              }
              Timestamp dropoffTime = getTimestamp(col2.text());
              if (dropoffTime != null) {
                carVO.setDropoffDate(dropoffTime);
              }
            }



          }
          i++;
        }
      }
    }
    carVO.setNote(sb.toString());
  }

  public static String getText(Element parentElement) {
    StringBuilder working = new StringBuilder();
    for (Node child : parentElement.childNodes()) {
      if (child instanceof TextNode) {
        working.append(((TextNode) child).text());
      }
      if (child instanceof Element) {
        Element childElement = (Element)child;
        // do more of these for p or other tags you want a new line for
        if (childElement.tag().getName().equalsIgnoreCase("br") || childElement.tag().getName().equalsIgnoreCase("tr")) {
          working.append("\n");
        }

        working.append(getText(childElement));
      }
    }

    return working.toString().trim();
  }

  public static String getTextNoBr(Element parentElement) {
    StringBuilder working = new StringBuilder();
    for (Node child : parentElement.childNodes()) {
      if (child instanceof TextNode) {
        working.append(((TextNode) child).text());
      }
      if (child instanceof Element) {
        Element childElement = (Element)child;
        // do more of these for p or other tags you want a new line for
        if (childElement.tag().getName().equalsIgnoreCase("tr") || childElement.tag().getName().equalsIgnoreCase("table") || childElement.tag().getName().equalsIgnoreCase("div")) {
          working.append("\n");
        }

        working.append(getTextNoBr(childElement));
      }
    }

    return working.toString().trim();
  }

}
