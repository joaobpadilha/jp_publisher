package com.mapped.publisher.parse.extractor.booking.email;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.EmailBodyExtractor;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by twong on 2015-02-06.
 */
public class WorldspanExtractor
    extends EmailBodyExtractor {

  String recordLocator = null;
  private Pattern datePattern = Pattern.compile("[A-Za-z]{5,10}\\s[0-9]{1,2}\\s[A-Za-z]{3,15}\\s[0-9]{4}");
  private String[] dateTimeFormat = {"EEEE dd MMMM yyyy h:mm a", "EEEE, MMMM dd, yyyy", "EEEE, MMMM dd, yyyy h:mm a"};
  private DateUtils du = new DateUtils();

  public static void main(String[] args) {

    try {

      String s = FileUtils.readFileToString(new File("/Volumes/data2/temp/worldspan.html"));

      WorldspanExtractor extractor = new WorldspanExtractor();
      TripVO tripVO = extractor.extractDataImpl( s);
      if (tripVO != null && tripVO.getFlights() != null) {
        for (FlightVO f : tripVO.getFlights()) {
          System.out.print(f.getCode() + " " + f.getNumber());
          if (f.getDepatureTime() != null) {
            System.out.print(" Depart: " + Utils.formatDateTimePagePrint(f.getDepatureTime().getTime()));
          }
          if (f.getArrivalTime() != null) {
            System.out.print(" Arrive: " + Utils.formatDateTimePagePrint(f.getArrivalTime().getTime()));
          }
          System.out.println("");
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }

  @Override
  protected void initImpl(Map<String, String> params) {

  }

  @Override
  protected TripVO extractDataImpl(String htmlInput)
      throws Exception {
    htmlInput = htmlInput.replaceAll("</p>", "</p>\n");

    htmlInput = htmlInput.replaceAll("<br>", "\n");
    htmlInput = htmlInput.replaceAll("<br/>", "\n");

    Document doc = Jsoup.parse(htmlInput);

    TripVO tripVO = new TripVO();


    for (Element table : doc.select("table")) {
      if (table.hasAttr("border") && table.hasAttr("cellspacing") && table.hasAttr("cellpadding") && table.hasAttr(
          "border")) {
        Elements rows = table.select("tr");
        Element row = rows.get(0);
        Elements tds = row.select("td");

        if (rows.size() > 1) {
          String date = tds.get(0).text();
          String type = tds.get(1).text();
          if (type != null) {
            if (type.toLowerCase().trim().equals("air")) {
              FlightVO vo = processFlight(rows, date);
              if (vo != null) {
                tripVO.addFlight(vo);
              }
            }
            else if (type.toLowerCase().trim().equals("hotel")) {
              HotelVO vo = processHotel(rows, date);
              if (vo != null) {
                tripVO.addHotel(vo);
              }
            }
            else if (type.toLowerCase().trim().equals("car")) {
              TransportVO vo = processCar(rows, date);
              if (vo != null) {
                tripVO.addTransport(vo);
              }
            }
          }
        }
      }
      else {
        //look for trip locator
        Elements rows = table.select("tr");
        if (rows.size() == 1) {
          Element row = rows.get(0);
          Elements tds = row.select("td");
          if (tds.size() == 1 && getText(tds.get(0)).contains("Trip Locator:")) {
            recordLocator = getText(tds.get(0));
          }
        }
      }

    }
    tripVO.setImportSrc(BookingSrc.ImportSrc.WORLDSPAN_EMAIL);
    tripVO.setImportSrcId(recordLocator);
    tripVO.setImportTs(System.currentTimeMillis());
    return tripVO;

  }

  @Override
  public ArrayList<ParseError> getParseErrors() {
    return null;
  }

  public FlightVO processFlight(Elements rows, String date) {
    FlightVO vo = new FlightVO();

    StringBuilder departInfo = null;
    StringBuilder arriveInfo = null;
    StringBuilder notes = new StringBuilder();
    if (recordLocator != null && recordLocator.trim().length() > 0) {
      notes.append(recordLocator);
      notes.append("\n");
    }

    String flight = null;

    boolean processingDepart = false;
    boolean processingArrive = false;
    boolean processingNote = false;

    rows.remove(0);
    for (int i = 0; i < rows.size(); i++) {
      Element row = rows.get(i);
      Elements cells = row.select("td");

      if (i == 0) {
        flight = cells.get(0).text();
      }
      else {
        if (cells.get(0).text().equals("Depart:")) {
          departInfo = new StringBuilder();
          processingDepart = true;
          processingNote = false;
          processingArrive = false;
        }
        else if (cells.get(0).text().equals("Arrive:")) {
          arriveInfo = new StringBuilder();
          processingArrive = true;
          processingDepart = false;
          processingNote = false;
        }
        else if (cells.get(0).text().equals("Remarks:")) {
          processingNote = true;
          processingArrive = false;
          processingDepart = false;
        }

        if (processingArrive && cells.size() > 1) {
          if (cells.get(1).text().trim().length() > 0) {
            arriveInfo.append(getText(cells.get(1)));
            arriveInfo.append("\n");
          }
          else if (cells.get(0).text().trim().length() > 0) {
            processingArrive = false;
          }
        }
        else if (processingDepart && cells.size() > 1 && cells.get(1).text().trim().length() > 0) {
          if (cells.get(1).text().trim().length() > 0) {
            departInfo.append(getText(cells.get(1)));
            departInfo.append("\n");
          }
          else if (cells.get(0).text().trim().length() > 0) {
            processingDepart = false;
          }

        }
        else if (processingNote && cells.size() > 1 && cells.get(1).text().trim().length() > 0) {
          if (cells.get(1).text().trim().length() > 0) {
            notes.append(getText(cells.get(1)));
            notes.append("\n");
          }
          else if (cells.get(1).text().trim().length() == 0) {
            processingNote = false;
          }
        }


        if (cells.size() == 4 &&
            getText(cells.get(2)).trim().length() > 0 &&
            getText(cells.get(3)).trim().length() > 0) {
          String name2 = getText(cells.get(2));
          String val2 = getText(cells.get(3));
          if (name2.contains("Airline Ref:")) {
            vo.setReservationNumber(val2);
          }
          else if (name2.trim().length() > 0 && val2.trim().length() > 0) {
            notes.append(name2);
            notes.append(" ");
            notes.append(val2);
            notes.append("\n");
          }
        }
      }
    }

    if (flight != null && flight.contains("Flight")) {
      String s = flight.substring(flight.indexOf("Flight") + 7).trim();
      String[] tokens = s.split(" ");
      if (tokens.length == 2) {
        vo.setCode(tokens[0]);
        vo.setNumber(tokens[1]);
      }
    }

    if (departInfo != null) {
      //process depart Info
      String[] tokens = departInfo.toString().split("\n");
      for (int i = 0; i < tokens.length; i++) {
        String s = tokens[i];
        if (i == 0) {
          AirportVO aiportVO = new AirportVO();
          aiportVO.setName(s);
          vo.setDepartureAirport(aiportVO);
        }

        if (s.contains(" AM") || s.contains(" PM")) {
          String departureDate = date + " " + s;
          try {
            java.sql.Timestamp t = new java.sql.Timestamp(du.parseDate(departureDate, dateTimeFormat).getTime());
            vo.setDepatureTime(t);
          }
          catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    }

    if (arriveInfo != null) {
      //process depart Info
      String[] tokens = arriveInfo.toString().split("\n");
      for (int i = 0; i < tokens.length; i++) {
        String s = tokens[i];
        if (i == 0) {
          AirportVO aiportVO = new AirportVO();
          aiportVO.setName(s);
          vo.setArrivalAirport(aiportVO);
        }

        if (s.contains(" AM") || s.contains(" PM")) {
          String arrivalDate = date + " " + s;
          try {
            java.sql.Timestamp t = new java.sql.Timestamp(du.parseDate(arrivalDate, dateTimeFormat).getTime());
            vo.setArrivalTime(t);
          }
          catch (Exception e) {
            e.printStackTrace();
          }
        }
        else {
          try {
            Date d = du.parseDate(s, dateTimeFormat);
            if (d != null) {
              date = s;
            }
          }
          catch (Exception e) {

          }
        }
      }
    }

    vo.setNotes(notes.toString());


    if (vo.isValid()) {
      return vo;
    }
    return null;
  }

  public HotelVO processHotel(Elements rows, String date) {
    HotelVO vo = new HotelVO();


    StringBuilder hotelInfo = new StringBuilder();
    StringBuilder arriveInfo = null;
    StringBuilder notes = new StringBuilder();
    if (recordLocator != null && recordLocator.trim().length() > 0) {
      notes.append(recordLocator);
      notes.append("\n");
    }
    String checkout = null;
    String checkin = date;


    String hotelName = null;

    boolean pHotel = true;

    rows.remove(0);
    for (int i = 0; i < rows.size(); i++) {
      Element row = rows.get(i);
      Elements cells = row.select("td");

      if (i == 0) {
        hotelName = cells.get(0).text();
      }
      else {
        if (cells.size() == 1) {
          if (pHotel) {
            hotelName = getText(cells.get(0));
            pHotel = false;
          }
          notes.append(getText(cells.get(0)));
          notes.append("\n");

        }
        if (cells.size() == 4) {
          String name1 = cells.get(0).text();
          String val1 = getText(cells.get(1));
          if (name1.contains("Check Out:")) {
            checkout = val1;
          }
          else if (name1.trim().length() > 0 && val1.trim().length() > 0) {
            notes.append(name1);
            notes.append(" ");
            notes.append(val1);
            notes.append("\n");
          }

          String name2 = cells.get(2).text();
          String val2 = getText(cells.get(3));
          if (name2.contains("Confirmation:")) {
            vo.setConfirmation(val2);
          }
          else if (name2.trim().length() > 0 && val2.trim().length() > 0) {
            notes.append(name2);
            notes.append(" ");
            notes.append(val2);
            notes.append("\n");
          }
        }
      }
    }

    vo.setHotelName(hotelName);
    String checkinDate = date + " 3:00 PM";
    try {
      java.sql.Timestamp t = new java.sql.Timestamp(du.parseDate(checkinDate, dateTimeFormat).getTime());
      vo.setCheckin(t);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    if (checkout != null) {
      checkout = checkout + " 12:00 PM";
      try {
        java.sql.Timestamp t = new java.sql.Timestamp(du.parseDate(checkout, dateTimeFormat).getTime());
        vo.setCheckout(t);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }

    vo.setNote(notes.toString());
    if (vo.isValid()) {
      return vo;
    }
    return null;
  }

  public TransportVO processCar(Elements rows, String date) {
    TransportVO transportVO = new TransportVO();

    StringBuilder notes = new StringBuilder();
    if (recordLocator != null && recordLocator.trim().length() > 0) {
      notes.append(recordLocator);
      notes.append("\n");
    }
    StringBuilder pickup = null;
    StringBuilder dropoff = null;
    boolean pPickup = false;
    boolean pDropoff = false;

    String name = null;


    rows.remove(0);
    for (int i = 0; i < rows.size(); i++) {
      Element row = rows.get(i);
      Elements cells = row.select("td");

      if (i == 0) {
        name = cells.get(0).text();
      }
      else {

        if (cells.size() == 4) {

          String name1 = cells.get(0).text();
          String val1 = getText(cells.get(1));
          if (name1.trim().length() > 0) {
            pDropoff = false;
          }

          if (name1.contains("Pick Up:")) {
            pPickup = true;
            pDropoff = false;
            pickup = new StringBuilder();
          }
          if (name1.contains("Drop Off:")) {
            pPickup = false;
            pDropoff = true;
            dropoff = new StringBuilder();
          }

          if (!pPickup && !pDropoff &&
              name1 != null &&
              name1.trim().length() > 0 &&
              val1 != null && val1.trim().length() > 0) {
            notes.append(name1);
            notes.append(" ");
            notes.append(val1);
            notes.append("\n");
          }

          String name2 = cells.get(2).text();
          String val2 = getText(cells.get(3));
          if (name2.contains("Confirmation:")) {
            transportVO.setConfirmationNumber(val2);
          }
          else if (name2.trim().length() > 0 && val2.trim().length() > 0) {
            notes.append(name2);
            notes.append(" ");
            notes.append(val2);
            notes.append("\n");
          }
        }
        if (cells.size() > 1) {
          if (pPickup && cells.get(0).text().trim().length() == 0) {
            pickup.append(getText(cells.get(1)));
            pickup.append("\n");
          }
          if (pDropoff && cells.get(0).text().trim().length() == 0) {
            dropoff.append(getText(cells.get(1)));
            dropoff.append("\n");
          }
        }
      }
    }

    transportVO.setName(name);
    transportVO.setCmpyName(name);
    transportVO.setBookingType(ReservationType.CAR_RENTAL);
    transportVO.setNote(notes.toString());
    if (pickup != null && pickup.toString().trim().length() > 0) {
      String[] tokens = pickup.toString().split("\n");

      for (int i = 0; i < tokens.length; i++) {
        String s = tokens[i];
        if (i == 0) {
          transportVO.setPickupLocation(s);
        }
        if (s.contains(" AM") || s.contains(" PM")) {
          String departureDate = date + " " + s;
          try {
            java.sql.Timestamp t = new java.sql.Timestamp(du.parseDate(departureDate, dateTimeFormat).getTime());
            transportVO.setPickupDate(t);
          }
          catch (Exception e) {
            e.printStackTrace();
          }
        }

      }
    }

    if (dropoff != null) {
      String[] tokens = dropoff.toString().split("\n");
      String dropoffTimestamp = "";

      for (int i = 0; i < tokens.length; i++) {
        String s = tokens[i];
        if (i == 0) {
          transportVO.setDropoffLocation(s);
        }
        Matcher dateM = datePattern.matcher(s);
        if (dateM != null && dateM.find()) {
          dropoffTimestamp = dateM.group().trim();
        }

        if ((s.contains(" AM") || s.contains(" PM")) && dropoffTimestamp.trim().length() > 0) {
          dropoffTimestamp = dropoffTimestamp + " " + s;
          try {
            java.sql.Timestamp t = new java.sql.Timestamp(du.parseDate(dropoffTimestamp, dateTimeFormat).getTime());
            transportVO.setDropoffDate(t);
          }
          catch (Exception e) {
            e.printStackTrace();
          }
        }

      }
    }

    if (transportVO.isValid()) {
      return transportVO;
    }
    return null;
  }

  String getText(Element parentElement) {
    String working = "";
    for (Node child : parentElement.childNodes()) {
      if (child instanceof TextNode) {
        working += ((TextNode) child).text();
      }
      if (child instanceof Element) {
        Element childElement = (Element) child;
        // do more of these for p or other tags you want a new line for
        if (childElement.tag().getName().equalsIgnoreCase("br")) {
          working += "\n";
        }

        working += getText(childElement);
      }
    }

    return working.trim();
  }
}
