package com.mapped.publisher.parse.extractor.booking.email;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.EmailBodyExtractor;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.*;
import org.jsoup.select.Elements;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by twong on 15-05-01.
 */
public class WorldspanRecLocatorExtractor
    extends EmailBodyExtractor {

  private static String[] dateTimeFormat = {"yyyyMMdd'T'HHmm","yyyy-MM-dd kk:mm", "M/dd/yyyy h:mm:ss a", "dd MMM yyyy", "dd MMM yyyy ",
                                            "E dd MMM yyyy h:mma", "E dd MMM yyyy", "E dd MMM yyyy ",
                                            "E dd MMMMM yyyy HH:mm", "dd MMM yyyy HH:mm a", "E dd MMM yyyy HH:mm",
                                            "dd MMMMM yyyy HH:mm", "E, MMMMM dd, yyyy HH:mm", "E, MMMMM dd, yyyy",
                                            "E, MMMMM dd, yyyy hh:mm a", "EEE dd MMM yyyy h:mm a",
                                            "EEE dd MMM yyyy 0h:mm a", "EEEE, MMMM dd, yyyy h:mm a", "EEEE, MMMM d, yyyy "};

  private static DateUtils du = new DateUtils();

  public static TripVO getInfo(String info, String recLocator) {

    TripVO trip = new TripVO();
    info = info.replaceAll("&amp;", "&");


    List<String> passengers = new ArrayList<>();

    //try to enhance the bookings
    Document doc = Jsoup.parse(info);

    processBookings(info, trip, doc);

    //add manual segments and take care for duplicates
    processManualSegment (trip, doc);

    //get passengers
    Elements allSelects = doc.select("select option");
    if (allSelects != null) {
      for (Element e: allSelects) {
        String name =  e.attr("value");
        if (name != null && name.toUpperCase().equals(name) && name.contains("/")) {
          passengers.add(name);
        }
      }
    }

    //if no passengers - try to find the single one
    if (passengers.size() == 0) {
      Element emailLink = doc.getElementById("ContentPlaceHolder1_hypEMailItinerary");
      if (emailLink != null) {
        Attributes attributes = emailLink.attributes();
        if (attributes != null) {
          for (Attribute attribute : attributes) {
            if (attribute.getKey().equals("href")) {
              String s = attribute.getValue();
              if (s.contains("Name=")) {
                s = s.substring(s.indexOf("Name=") + 5);
                if (s.contains("&")) {
                  s = s.substring(0, s.indexOf("&"));
                  if (s.contains("%20")) {
                    String fName = s.substring(0, s.indexOf("%20"));
                    String lName = s.substring(s.indexOf("%20") + 3);
                    passengers.add(lName.toUpperCase() + "/" + fName.toUpperCase());
                  } else if (s.contains(" ")) {
                    String fName = s.substring(0, s.indexOf(" "));
                    String lName = s.substring(s.indexOf(" ") + 1);
                    passengers.add(lName.toUpperCase() + "/" + fName.toUpperCase());
                  }
                }
              }
            }
          }
        }

      }
    }

    //try to enhance the hotels
    if (trip.getHotels() != null && trip.getHotels().size() > 0) {
      Elements allTables = doc.select("table");
      for (Element e : allTables) {
        if (e.attr("width").equals("96%") && e.attr("border").equals("0") && e.attr("cellpadding").equals("2") && e.attr("cellspacing").equals("0") ) {
          String s = getText(e);
          for (HotelVO hotelVO : trip.getHotels()) {
            if (s.toLowerCase().contains(hotelVO.getHotelName().toLowerCase()) && (hotelVO.getConfirmation() == null || hotelVO.getConfirmation().trim().length() == 0 || s.contains(hotelVO.getConfirmation())) ) {
              enhanceHotel(hotelVO, e);
              break;
            }
          }
        }
      }

    }

    if (info.contains("sojern_data")) {
      if (trip.getFlights() == null || trip.getFlights().size() == 0) {
        List<FlightVO> flights = getFlights(info);
        for (FlightVO flight : flights) {
          trip.addFlight(flight);
        }
      }
      if (trip.getHotels() == null || trip.getHotels().size() == 0) {
        List<HotelVO> hotels = getHotels(info);
        for (HotelVO hotel : hotels) {
          trip.addHotel(hotel);
        }
      }
      if (trip.getActivities() == null || trip.getActivities().size() == 0) {
        List<ActivityVO> activities = getActivities(info);
        for (ActivityVO activity : activities) {
          trip.addActivity(activity);
        }
      }
      if (trip.getTransport() == null || trip.getTransport().size() == 0) {
        List<TransportVO> cars = getCarRentals(info);
        for (TransportVO car : cars) {
          trip.addTransport(car);
        }
      }
    }


    Elements elements = doc.select("table[border=0][cellpadding=0][cellspacing=0][width=100%]");
    //                                                    "style=\"BORDER-RIGHT:#333 2px solid; BORDER-TOP:#333 2px
    // solid; BORDER-LEFT:#333 2px solid; \"");

    for (Element table : elements) {
      String style = table.attr("style");

      if (style != null && style.contains("BORDER-RIGHT:#333 2px solid") && style.contains("BORDER-TOP:#333 2px " +
                                                                                           "solid") && style
          .contains("BORDER-LEFT:#333 2px solid")) {
        if (table.toString().contains("Flight Number")) {
          Elements e = table.getElementsByAttributeValueContaining("id", "lblFlightNumberData");
          Elements e1 = table.getElementsByAttributeValueContaining("id", "lblDepartDateData");
          Elements e2 = table.getElementsByAttributeValueContaining("id", "lblDepartTimeData");

          String flightNum = e.text();
          String departDate = e1.text();
          String departTime = e2.text();
          Timestamp ts = null;


          for (FlightVO flightVO : trip.getFlights()) {
            String fvo = flightVO.getCode().trim() + flightVO.getNumber().trim();
            String fvoDepart = departDate + ", " + Utils.formatTimestamp(flightVO.getDepatureTime().getTime(), "YYYY");
            if (departDate != null && departTime != null) {
              ts = getTimestamp(fvoDepart, departTime);
            }

            if (fvo.toLowerCase()
                   .equals(flightNum.toLowerCase()) && ts != null && ts.equals(flightVO.getDepatureTime())) {
              enhanceFlight(flightVO, table, passengers);
              break;
            }
          }
        }
        else {
          Elements e1 = table.getElementsByAttributeValueContaining("id", "lblCarTypeData");
          if (e1 != null && e1.text().trim().length() > 0) { //this is a car
            Elements e = table.getElementsByAttributeValueContaining("id", "lblVendorNameData");
            Elements e3 = table.getElementsByAttributeValueContaining("id", "lblPickupTimeData");
            String pickupDate = e3.text();
            String name = e.text().toLowerCase();
            String type = e1.text();

            if (e != null && e.hasText()) {
              for (TransportVO carVO : trip.getTransport()) {
                String s = Utils.formatTimestamp(carVO.getPickupDate().getTime(), "EEEE, MMMM dd");
                if (name.contains(carVO.getName().toLowerCase()) && Utils.formatTimestamp(carVO.getPickupDate()
                                                                                               .getTime(),
                                                                                          "EEEE, MMMM d")
                                                                         .contains(pickupDate)) {
                  enhanceCar(carVO, table);
                }
              }
            }
          }

        }
      }

    }
    trip.setImportSrc(BookingSrc.ImportSrc.WORLDSPAN_MYTRIPS);
    trip.setImportTs(System.currentTimeMillis());

    if (trip.getFlights() != null) {
      for (FlightVO f : trip.getFlights()) {
        if (f.getReservationNumber() == null || f.getReservationNumber().trim().length() == 0) {
          f.setReservationNumber(recLocator);
        }
      }
    }

    return trip;
  }

  public static void processBookings (String origInfo, TripVO tripVO, Document doc) {
    int i = 1;
    while (true) {
      if (origInfo.contains("START"+i)) {
        String startDate = null;
        String endDate = null;
        String name = null;
        String details = null;
        Elements elements = doc.getElementsByAttributeValue("name", "START"+i);
        if (elements != null && elements.size() == 1) {
          startDate = elements.get(0).attr("value");
        }
        elements = doc.getElementsByAttributeValue("name", "END"+i);
        if (elements != null && elements.size() == 1) {
          endDate = elements.get(0).attr("value");
        }
        elements = doc.getElementsByAttributeValue("name", "SUBJECT"+i);
        if (elements != null && elements.size() == 1) {
          name = elements.get(0).attr("value");
        }
        elements = doc.getElementsByAttributeValue("name", "BODY"+i);
        if (elements != null && elements.size() == 1) {
          details = elements.get(0).attr("value");
        }

        if (startDate != null && endDate != null && name != null) {
          if (name.startsWith("Car")) {
            processCar(name, startDate, endDate, details, tripVO);
          } else if (name.startsWith("Flight")) {
            processFlight(name, startDate, endDate, details, tripVO);
          } else if (name.startsWith("Property")) {
            processHotel(name, startDate, endDate, details, tripVO);
          } else if (name.startsWith("Rail")) {
            System.out.println("----- " + name);
          } else if (name.startsWith("Tour")) {
            System.out.println("-----++++++ " + name);
          }

        }

      } else {
        break;
      }

      i++;
    }
  }

  public static void processManualSegment (TripVO tripVO, Document doc) {

    List<String> trainIds = new ArrayList<> ();
    List<String> busIds = new ArrayList<>();
    List<String> tourIds = new ArrayList<>();
    List<String> carIds = new ArrayList<>();
    List<String> flightIds = new ArrayList<>();

    for( Element e : doc.select("span"))
    {
      String text = e.text();
      String id  = e.attributes().get("id");
      if (id != null && id.startsWith("ContentPlaceHolder") && id.contains("_lbl")) {
        id = id.substring(0, id.indexOf("_lbl"));
      }
      if (text.equals("Train")) {
        trainIds.add(id);
      } else if (text.equals("Tour")) {
        tourIds.add(id);
      } else if (text.equals("Bus")) {
        busIds.add(id);
      }  else if (text.equals("Car")) {
        carIds.add(id);
      }  else if (text.equals("Carrier")) {
        flightIds.add(id);
      }
    }


    Elements allEments = new Elements();
    for (Element e: doc.select("table")){
      Attributes attributes = e.attributes();
      if (attributes.size() == 5 && attributes.hasKey("border") && attributes.hasKey("cellpadding") && attributes.hasKey("cellspacing") && attributes.hasKey("width") && attributes.hasKey("style")) {
        if (attributes.get("style").equals("BORDER-RIGHT:#333 2px solid; BORDER-TOP:#333 2px solid; BORDER-LEFT:#333 2px solid; BORDER-BOTTOM:#333 2px solid")) {
          boolean found = false;
          for (String id: trainIds) {
            if (e.getElementById(id + "_lblBegin") != null) {
              //process rail
              processManualTransport(e, tripVO, id, doc, ReservationType.RAIL);
              found = true;
              break;
            }
          }

          if (!found) {
            for (String id: tourIds) {
              if (e.getElementById(id + "_lblBegin") != null) {
                //process tour
                processManualActivity(e, tripVO, id, doc);
                found = true;
                break;
              }
            }
          }

          if (!found) {
            for (String id: busIds) {
              if (e.getElementById(id + "_lblBegin") != null) {
                processManualTransport(e, tripVO, id, doc, ReservationType.BUS);
                found = true;
                break;
              }
            }
          }

          if (!found) {
            for (String id: carIds) {
              if (e.getElementById(id + "_lblBegin") != null) {
                processManualTransport(e, tripVO, id, doc, ReservationType.CAR_RENTAL);
                found = true;
                break;
              }
            }
          }

          if (!found) {
            for (String id: flightIds) {
              if (e.getElementById(id + "_lblBegin") != null) {
                processManualFlight(e, tripVO, id, doc, ReservationType.CAR_RENTAL);
                found = true;
                break;
              }
            }
          }
        }
      }
    }
  }

  public static void processManualTransport (Element table, TripVO tripVO, String id, Document document, ReservationType bookingType) {

    String year = Utils.formatTimestamp(System.currentTimeMillis(), "YYYY");

    TransportVO transportVO = new TransportVO();
    transportVO.setBookingType(bookingType);
    if (table.getElementById(id+"_lblVendorData") != null && table.getElementById(id+"_lblVendorData").hasText()) {
      transportVO.setCmpyName(table.getElementById(id + "_lblVendorData").text());
      transportVO.setName(table.getElementById(id + "_lblVendorData").text());

    }
    if (document.getElementById(id+"_lblSegmentDateData") != null && document.getElementById(id+"_lblSegmentDateData").hasText()) {
      transportVO.setPickupDate(getTimestamp(document.getElementById(id + "_lblSegmentDateData").text(),""));
      if (transportVO.getPickupDate() != null) {
        year = Utils.formatTimestamp(transportVO.getPickupDate().getTime(), "YYYY");
      }

    }
    if (table.getElementById(id+"_lblConfirmationData") != null && table.getElementById(id+"_lblConfirmationData").hasText()) {
      transportVO.setConfirmationNumber(table.getElementById(id + "_lblConfirmationData").text());
    }
    if (table.getElementById(id+"_lblEndData") != null && table.getElementById(id+"_lblEndData").hasText()) {
      transportVO.setDropoffDate(getTimestamp(table.getElementById(id + "_lblEndData").text() + ", " + year, ""));
    }


    StringBuilder note = new StringBuilder();
    Elements rows = table.select("tr");
    for (Element row: rows) {
      Elements table1 = row.select("table");
      if (table1 != null && table1.size() == 1) {
        Elements rows1 = table1.get(0).select("tr");
        if (rows1.size()  == 1) {
          //remarks
          note.append("\nRemarks:\n");
          note.append(rows1.text());
        } else {
          for (Element row1 : rows1) {
            Elements cols1 = row1.select("td");
            if (cols1.size() == 3 && cols1.get(1).hasText() && cols1.get(1).text().contains("Confirmation Number")) {
              transportVO.setConfirmationNumber(cols1.get(2).text().trim());
            }
            else {
              note.append(row1.text());
              note.append("\n");
            }
          }
        }
      }
    }
    transportVO.setNote(note.toString());

    if (transportVO.isValid()) {
      tripVO.addTransport(transportVO);
    }
  }

  public static void processManualActivity (Element table, TripVO tripVO, String id, Document document) {
    String year = Utils.formatTimestamp(System.currentTimeMillis(), "YYYY");

    ActivityVO activityVO = new ActivityVO();
    activityVO.setBookingType(ReservationType.TOUR);
    if (table.getElementById(id+"_lblVendorData") != null && table.getElementById(id+"_lblVendorData").hasText()) {
      activityVO.setName(table.getElementById(id + "_lblVendorData").text());

    }
    if (document.getElementById(id+"_lblSegmentDateData") != null && document.getElementById(id+"_lblSegmentDateData").hasText()) {
      activityVO.setStartDate(getTimestamp(document.getElementById(id + "_lblSegmentDateData").text(), ""));
      if (activityVO.getStartDate() != null) {
        year = Utils.formatTimestamp(activityVO.getStartDate().getTime(), "YYYY");
      }
    }
    if (table.getElementById(id+"_lblConfirmationData") != null && table.getElementById(id+"_lblConfirmationData").hasText()) {
      activityVO.setConfirmation(table.getElementById(id + "_lblConfirmationData").text());
    }
    if (table.getElementById(id+"_lblEndData") != null && table.getElementById(id+"_lblEndData").hasText()) {
      activityVO.setEndDate(getTimestamp(table.getElementById(id + "_lblEndData").text()+ ", " + year, ""));
    }


    StringBuilder note = new StringBuilder();
    Elements rows = table.select("tr");
    for (Element row: rows) {
      Elements table1 = row.select("table");
      if (table1 != null && table1.size() == 1) {
        Elements rows1 = table1.get(0).select("tr");
        if (rows1.size()  == 1) {
          //remarks
          note.append("\nRemarks:\n");
          note.append(rows1.text());
        } else {
          for (Element row1 : rows1) {
            Elements cols1 = row1.select("td");
            if (cols1.size() == 3 && cols1.get(1).hasText() && cols1.get(1).text().contains("Confirmation Number")) {
              activityVO.setConfirmation(cols1.get(2).text().trim());
            }
            else {
              note.append(row1.text());
              note.append("\n");
            }
          }
        }
      }
    }
    activityVO.setNote(note.toString());

    if (activityVO.isValid()) {
      tripVO.addActivity(activityVO);
    }
  }


  public static void processManualFlight (Element table, TripVO tripVO, String id, Document document, ReservationType bookingType) {

    String year = Utils.formatTimestamp(System.currentTimeMillis(), "YYYY");

    FlightVO flightVO = new FlightVO();
    String airlineName = null;
    String date = null;
    String departTime = null;
    String arriveTime = null;
    String departAirportName = null;
    String arriveAirportName = null;
    String code = null;


    if (table.getElementById(id+"_lblVendorData") != null && table.getElementById(id+"_lblVendorData").hasText()) {
      airlineName = table.getElementById(id + "_lblVendorData").text();
    }
    if (document.getElementById(id+"_lblSegmentDateData") != null && document.getElementById(id+"_lblSegmentDateData").hasText()) {
      date = document.getElementById(id + "_lblSegmentDateData").text();
      if (date != null) {
        year = Utils.formatTimestamp(getTimestamp(date, "").getTime(), "YYYY");
      }

    }
    if (table.getElementById(id+"_lblConfirmationData") != null && table.getElementById(id+"_lblConfirmationData").hasText()) {
      flightVO.setReservationNumber(table.getElementById(id + "_lblConfirmationData").text());
    }

    StringBuilder note = new StringBuilder();
    Elements rows = table.select("tr");
    for (Element row: rows) {
      Elements table1 = row.select("table");
      if (table1 != null && table1.size() == 1) {
        Elements rows1 = table1.get(0).select("tr");
        if (rows1.size()  == 1) {
          //remarks
          note.append("\nRemarks:\n");
          note.append(rows1.text());
        } else {
          for (Element row1 : rows1) {
            Elements cols1 = row1.select("td");
            if (cols1.size() == 3 && cols1.get(1).hasText() && cols1.get(1).text().contains("Confirmation Number")) {
              flightVO.setReservationNumber(cols1.get(2).text().trim());
            } else if (cols1.size() == 3 && cols1.get(1).hasText() && cols1.get(1).text().contains("Departure City")) {
              departAirportName = cols1.get(2).text().trim();
            } else if (cols1.size() == 3 && cols1.get(1).hasText() && cols1.get(1).text().contains("Time of Departure")) {
              departTime = cols1.get(2).text().trim();
            } else if (cols1.size() == 3 && cols1.get(1).hasText() && cols1.get(1).text().contains("Arrival City")) {
              arriveAirportName = cols1.get(2).text().trim();
            } else if (cols1.size() == 3 && cols1.get(1).hasText() && cols1.get(1).text().contains("Time of Arrival")) {
              arriveTime = cols1.get(2).text().trim();
            } else if (cols1.size() == 3 && cols1.get(1).hasText() && cols1.get(1).text().contains("Flight Number")) {
              code = cols1.get(2).text().trim();
            }
            else {
              note.append(row1.text());
              note.append("\n");
            }
          }
        }
      }
    }

    flightVO.setCode(airlineName);
    flightVO.setNumber(code);
    flightVO.setDepatureTime(getTimestamp(date, departTime));
    flightVO.setArrivalTime(getTimestamp(date, arriveTime));

    if (flightVO.getArrivalTime() != null && flightVO.getArrivalTime().before(flightVO.getDepatureTime())) {
      long arriveTimestamp = flightVO.getArrivalTime().getTime() + TimeUnit.DAYS.toMillis(1);
      flightVO.setArrivalTime(new Timestamp(arriveTimestamp));
    }

    AirportVO departAirport = new AirportVO();
    departAirport.setName(departAirportName);
    flightVO.setDepartureAirport(departAirport);
    AirportVO arriveAirport = new AirportVO();
    arriveAirport.setName(arriveAirportName);
    flightVO.setArrivalAirport(arriveAirport);

    flightVO.setNote(note.toString());

    if (flightVO.isValid()) {
      tripVO.addFlight(flightVO);
    }
  }

  public static void processFlight (String name, String startDate, String endDate, String info, TripVO tripVO) {
    Timestamp startMs = getTimestamp(startDate);
    Timestamp endMs = getTimestamp(endDate);
    String[] nameTokens = name.split(" ");



    String[] tokens = info.split("\n");

    FlightVO vo = new FlightVO();
    if (nameTokens != null && nameTokens.length == 5) {
      vo.setCode(nameTokens[1]);
      vo.setNumber(nameTokens[2]);
      AirportVO departAiport = new AirportVO();
      departAiport.setCode(nameTokens[3]);
      vo.setDepartureAirport(departAiport);

      AirportVO arriveAirport = new AirportVO();
      arriveAirport.setCode(nameTokens[4]);
      vo.setArrivalAirport(arriveAirport);
    }
    vo.setArrivalTime(endMs);
    vo.setDepatureTime(startMs);

    if (vo.isValid()) {
      tripVO.addFlight(vo);
    }
  }

  public static void processCar (String name, String startDate, String endDate, String info, TripVO tripVO) {
    Timestamp startMs = getTimestamp(startDate);
    Timestamp endMs = getTimestamp(endDate);
    name = name.replace("Car: ","").trim();
    int i = name.indexOf(",");
    if (i > 0) {
      name = name.substring(0, i);
    }
    String[] tokens = info.split("\n");

    TransportVO vo = new TransportVO();
    vo.setName(name);
    vo.setPickupDate(startMs);
    vo.setDropoffDate(endMs);

    if (tokens != null) {
      for (String s: tokens) {
        if (s.contains("Pick Up:")) {
          vo.setPickupLocation(s.replace("Pick Up:","").trim());
        } else if (s.contains("Drop Off:")) {
          vo.setDropoffLocation(s.replace("Drop Off:","").trim());
        }
       }
    }

    if (vo.isValid()) {
      tripVO.addTransport(vo);
    }

  }

  public static void processHotel (String name, String startDate, String endDate, String info, TripVO tripVO) {
    if (startDate.contains("T0000"))
      startDate = startDate.replace("T0000", "T1500");

    Timestamp startMs = getTimestamp(startDate);
    if (endDate.contains("T0000"))
      endDate = endDate.replace("T0000", "T1200");

    Timestamp endMs = getTimestamp(endDate);
    name = name.replace("Property Name: ","").trim();
    int i = name.indexOf(",");
    if (i > 0) {
      name = name.substring(0, i);
    }
    String[] tokens = info.split("\n");

    HotelVO vo = new HotelVO();
    vo.setName(name);
    vo.setCheckin(startMs);
    vo.setCheckout(endMs);

    StringBuilder sb = new StringBuilder();
    if (tokens != null) {
      for (String s: tokens) {
        if (s.contains("Confirmation:")) {
          vo.setConfirmation(s.replace("Confirmation:", "").trim());
        } else if (!s.contains("Check In") && !s.contains("Check Out") && !s.contains("Confirmation") && !s.contains(name)) {
          sb.append(s);sb.append("\n");
        }
      }
    }
    vo.setNote(sb.toString());

    if (vo.isValid()) {
      tripVO.addHotel(vo);
    }

  }

  public static List<FlightVO> getFlights(String origInfo) {

    //narrow the search string to only the flight segment to prevent duplicate issues
    String info = "";
    if (origInfo.contains("\"air_segments\"")) {
      info = origInfo.substring(origInfo.indexOf("air_segments"));
      if (info.contains("]")) {
        info = info.substring(0, info.indexOf("]"));
      }
    }

    List<FlightVO> flights = new ArrayList<FlightVO>();

    Matcher destinations = Pattern.compile("\"destination.*.[A-Z]{3}\"").matcher(info);
    Matcher originations = Pattern.compile("\"origination.*.[A-Z]{3}\"").matcher(info);
    Matcher departureDates = Pattern.compile("\"departure_date.*.[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}")
                                    .matcher(info);
    Matcher arrivalDates = Pattern.compile("\"arrival_date.*.[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}")
                                  .matcher(info);
    Matcher serviceClasses = Pattern.compile("\"service_class.*.[A-Z]\"").matcher(info);
    Matcher partySizes = Pattern.compile("\"party_size.*.[0-9]+").matcher(info);
    Matcher flightNumbers = Pattern.compile("\"flight_number.*.[0-9]+\"").matcher(info);
    Matcher carrierCodes = Pattern.compile("\"carrier_code.*.[a-zA-Z0-9]+").matcher(info);
    Matcher flightDurations = Pattern.compile("\"flight_duration.*.[0-9]+").matcher(info);

    String departureDate = "";
    String departureTime = "";
    String arrivalDate = "";
    String arrivalTime = "";

    //if(departureDates.find())
    //    System.out.println(departureDates.group().substring(departureDates.group().indexOf(":") + 1));

    while (true) {

      FlightVO flight = new FlightVO();

      flight.setDepartureAirport(new AirportVO());
      flight.setArrivalAirport(new AirportVO());

      if (destinations.find()) {
        flight.getArrivalAirport()
              .setCode(destinations.group()
                                   .substring(destinations.group().length() - 4, destinations.group().length() - 1));
      }

      if (originations.find()) {
        flight.getDepartureAirport()
              .setCode(originations.group()
                                   .substring(originations.group().length() - 4, originations.group().length() - 1));
      }

      if (departureDates.find()) {
        String departureDateTime = departureDates.group().substring(departureDates.group().indexOf(":") + 1);
        departureDateTime = departureDateTime.replace("\"", "");
        departureDateTime = departureDateTime.replace("T", " ");
        departureDate = departureDateTime.substring(0, departureDateTime.indexOf(" "));
        departureTime = departureDateTime.substring(departureDateTime.indexOf(" ") + 1);

        flight.setDepatureTime(getTimestamp(departureDate, departureTime));
      }

      if (arrivalDates.find()) {
        String arrivalDateTime = arrivalDates.group().substring(arrivalDates.group().indexOf(":") + 1);
        arrivalDateTime = arrivalDateTime.replace("\"", "");
        arrivalDateTime = arrivalDateTime.replace("T", " ");
        arrivalDate = arrivalDateTime.substring(0, arrivalDateTime.indexOf(" "));
        arrivalTime = arrivalDateTime.substring(arrivalDateTime.indexOf(" ") + 1);

        flight.setArrivalTime(getTimestamp(arrivalDate, arrivalTime));
      }

      if (serviceClasses.find()) {
        ;
      }
      if (partySizes.find()) {
        ;
      }
      if (flightDurations.find()) {
        ;
      }

      if (flightNumbers.find()) {
        String s = flightNumbers.group();
        if (s != null && s.contains(":")) {
          s = s.replaceAll("\"", "");
          flight.setNumber(s.substring(s.indexOf(":") + 1));
        }
      }


      if (carrierCodes.find()) {
        String s = carrierCodes.group();
        if (s != null && s.contains(":")) {
          s = s.replaceAll("\"", "");
          flight.setCode(s.substring(s.indexOf(":") + 1));
        }
      }

      if (destinations.hitEnd() && originations.hitEnd() && departureDates.hitEnd() && arrivalDates.hitEnd() &&
          serviceClasses.hitEnd() && partySizes.hitEnd() && flightNumbers.hitEnd() && carrierCodes.hitEnd() &&
          flightDurations.hitEnd()) {
        break;
      }

      if (flight.isValid()) {
        flights.add(flight);
      }
    }

    return flights;
  }

  public static List<HotelVO> getHotels(String origInfo) {

    //narrow the search string to only the flight segment to prevent duplicate issues
    String info = "";
    if (origInfo.contains("\"hotel_segments\"")) {
      info = origInfo.substring(origInfo.indexOf("hotel_segments"));
      if (info.contains("]")) {
        info = info.substring(0, info.indexOf("]"));
      }
    }

    List<HotelVO> hotels = new ArrayList<HotelVO>();

    Matcher hotelNames = Pattern.compile("\"name.*.[A-Za-z0-9\\s,-]+\"").matcher(info);
    Matcher checkinDates = Pattern.compile("\"check_in_date.*.[0-9]{4}-[0-9]{2}-[0-9]{2}").matcher(info);
    Matcher checkoutDates = Pattern.compile("\"check_out_date.*.[0-9]{4}-[0-9]{2}-[0-9]{2}").matcher(info);

    while (true) {

      HotelVO hotel = new HotelVO();

      if (hotelNames.find()) {
        hotel.setHotelName(hotelNames.group().substring(8, hotelNames.group().length() - 1).trim());
      }

      if (checkinDates.find()) {
        String checkinDate = checkinDates.group().substring(checkinDates.group().indexOf(":") + 1);
        checkinDate = checkinDate.replace("\"", "");
        hotel.setCheckin(getTimestamp(checkinDate, "15:00"));
      }

      if (checkoutDates.find()) {
        String checkoutDate = checkoutDates.group().substring(checkoutDates.group().indexOf(":") + 1);
        checkoutDate = checkoutDate.replace("\"", "");
        hotel.setCheckout(getTimestamp(checkoutDate, "12:00"));
      }

      if (hotelNames.hitEnd() && checkinDates.hitEnd() && checkoutDates.hitEnd()) {
        break;
      }

      if (hotel.isValid()) {
        hotels.add(hotel);
      }
    }

    return hotels;
  }

  public static List<ActivityVO> getActivities(String origInfo) {
    //narrow the search string to only the flight segment to prevent duplicate issues
    String info = "";
    if (origInfo.contains("\"tour_segments\"")) {
      info = origInfo.substring(origInfo.indexOf("tour_segments"));
      if (info.contains("]")) {
        info = info.substring(0, info.indexOf("]"));
      }
    }

    List<ActivityVO> activities = new ArrayList<ActivityVO>();

    Matcher vendorCode = Pattern.compile("\"vendor_code.*.\"").matcher(info);
    Matcher rawData = Pattern.compile("\"raw_data.*\"").matcher(info);
    Matcher rate = Pattern.compile("\"rate.*\"").matcher(info);
    Matcher partySize = Pattern.compile("\"party_size.*\"").matcher(info);


    Matcher startDate = Pattern.compile("\"start_date.*.[0-9]{4}-[0-9]{2}-[0-9]{2}").matcher(info);
    Matcher endDate = Pattern.compile("\"end_date.*.[0-9]{4}-[0-9]{2}-[0-9]{2}").matcher(info);
    Matcher startLocation = Pattern.compile("\"airport.*\"").matcher(info);

    StringBuffer note = new StringBuffer();
    while (true) {
      ActivityVO activity = new ActivityVO();

      if (startDate.find()) {
        String date = startDate.group().substring(startDate.group().indexOf(":") + 1);
        date = date.replace("\"", "");
        activity.setStartDate(getTimestamp(date, "00:00"));
      }

      if (endDate.find()) {
        String date = endDate.group().substring(endDate.group().indexOf(":") + 1);
        date = date.replace("\"", "");
        activity.setEndDate(getTimestamp(date, "00:00"));
      }

      if (startLocation.find()) {
        String s = startLocation.group().substring(startLocation.group().indexOf(":") + 1).replaceAll("\"", "").trim();
        if (s.length() > 0) {
          note.append("\n Start Location: ");
          note.append(s);
          note.append("\n");
        }
      }

      if (vendorCode.find()) {
        String s = vendorCode.group().substring(vendorCode.group().indexOf(":") + 1).replaceAll("\"", "").trim();
        activity.setName(s);
      }

      if (rawData.find()) {
        String s = rawData.group().substring(rawData.group().indexOf(":") + 1).replaceAll("\"", "").trim();
        if (s.length() > 0) {
          note.append("\n Note:");
          note.append(s);
          note.append("\n");
        }
      }

      if (rate.find()) {
        String s = rate.group().substring(rate.group().indexOf(":") + 1).replaceAll("\"", "").trim();
        if (s.length() > 0) {
          note.append("\n Rate:");
          note.append(s);
          note.append("\n");
        }
      }

      if (partySize.find()) {
        String s = partySize.group().substring(partySize.group().indexOf(":") + 1).replaceAll("\"", "").trim();
        if (s.length() > 0) {
          note.append("\n Party Size:");
          note.append(s);
          note.append("\n");
        }
      }

      if (startDate.hitEnd() && endDate.hitEnd() && startLocation.hitEnd()) {
        break;
      }

      if (activity.isValid()) {
        activity.setNote(note.toString());
        activities.add(activity);
      }
    }

    return activities;
  }

  public static List<TransportVO> getCarRentals(String origInfo) {
    //narrow the search string to only the flight segment to prevent duplicate issues
    String info = "";
    if (origInfo.contains("\"car_segments\"")) {
      info = origInfo.substring(origInfo.indexOf("car_segments"));
      if (info.contains("]")) {
        info = info.substring(0, info.indexOf("]"));
      }
    }

    List<TransportVO> cars = new ArrayList<TransportVO>();

    Matcher name = Pattern.compile("\"name.*\"").matcher(info);
    Matcher rawData = Pattern.compile("\"raw_data.*\"").matcher(info);
    Matcher rate = Pattern.compile("\"rate.*\"").matcher(info);
    Matcher partySize = Pattern.compile("\"party_size.*\"").matcher(info);


    Matcher startDate = Pattern.compile("\"start_date.*.[0-9]{4}-[0-9]{2}-[0-9]{2}").matcher(info);
    Matcher endDate = Pattern.compile("\"return_date.*.[0-9]{4}-[0-9]{2}-[0-9]{2}").matcher(info);
    Matcher startLocation = Pattern.compile("\"start_location.*\"").matcher(info);
    Matcher returnLocation = Pattern.compile("\"return_location.*\"").matcher(info);


    StringBuffer note = new StringBuffer();
    while (true) {
      TransportVO car = new TransportVO();

      if (startDate.find()) {
        String date = startDate.group().substring(startDate.group().indexOf(":") + 1);
        date = date.replace("\"", "");
        car.setPickupDate(getTimestamp(date, "00:00"));
      }

      if (endDate.find()) {
        String date = endDate.group().substring(endDate.group().indexOf(":") + 1);
        date = date.replace("\"", "");
        car.setDropoffDate(getTimestamp(date, "00:00"));
      }

      if (startLocation.find()) {
        String s = startLocation.group().substring(startLocation.group().indexOf(":") + 1).replaceAll("\"", "").trim();
        if (s.length() > 0) {
          if (s.startsWith(",")) {
            s = s.replace(",", "");
          }
          car.setPickupLocation(s.trim());
        }
      }

      if (name.find()) {
        String s = name.group().substring(name.group().indexOf(":") + 1).replaceAll("\"", "").trim();
        if (s.length() > 0) {
          car.setName(s);
        }
      }

      if (returnLocation.find()) {
        String s = returnLocation.group()
                                 .substring(returnLocation.group().indexOf(":") + 1)
                                 .replaceAll("\"", "")
                                 .trim();
        if (s.length() > 0) {
          car.setDropoffLocation(s);
        }
      }


      if (name.hitEnd() && startDate.hitEnd() && endDate.hitEnd() && startLocation.hitEnd()) {
        break;
      }

      if (car.isValid()) {
        car.setBookingType(ReservationType.CAR_RENTAL);
        cars.add(car);
      }
    }

    return cars;

  }

  private static Timestamp getTimestamp(String date, String time) {
    try {
      Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime());
      return t;
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  private static Timestamp getTimestamp(String timestamp) {
    try {
      Timestamp t = new Timestamp(du.parseDate(timestamp, dateTimeFormat).getTime());
      return t;
    }
    catch (Exception e) {
    }
    return null;
  }

  public static TripVO getItinerary(String name, String locator) {
    try {
      Connection.Response homeResp = Jsoup.connect("https://mytripandmore.com/")
                                          .timeout(100 * 1000)
                                          .method(Connection.Method.GET)
                                          .userAgent(
                                              ":Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 " +
                                              "(KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36")
                                          .execute();
      String aspSessionId = homeResp.cookie("ASP.NET_SessionId"); // you will need
      String sessionId = homeResp.cookie("SessionId");
      Document homeHtml = homeResp.parse();

      String __VIEWSTATEGENERATOR = homeHtml.select("input[name=__VIEWSTATEGENERATOR]").first().attr("value");
      String __EVENTTARGET = "btnItenerarySignIn";//homeHtml.select("input[name=__EVENTTARGET]").first().attr("value");
      String __EVENTARGUMENT = "";// homeHtml.select("input[name=__EVENTARGUMENT]").first().attr("value");
      String __LASTFOCUS = "";//homeHtml.select("input[name=__LASTFOCUS]").first().attr("value");
      String __VIEWSTATE = homeHtml.select("input[name=__VIEWSTATE]").first().attr("value");
      String __EVENTVALIDATION = homeHtml.select("input[name=__EVENTVALIDATION]").first().attr("value");

      Connection.Response loginResp = Jsoup.connect("https://mytripandmore.com/")
                                           .followRedirects(false)
                                           .timeout(100 * 1000)
                                           .cookie("ASP.NET_SessionId", aspSessionId)
                                           .cookie("sessionId", sessionId)
                                           .cookie("__utma", "1.672708320.1426000658.1427224774.1427892503.5")
                                           .cookie("__utmc", "1")
                                           .cookie("__utmz",
                                                   "1.1427892503.5.2.utmcsr=google|utmccn=(organic)" +
                                                   "|utmcmd=organic|utmctr=(not%20provided)")
                                           .cookie("lang", "en")
                                           .cookie("TimeFormat", "12")
                                           .cookie("CultureId", "en")
                                           .userAgent(
                                               ":Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 " +
                                               "(KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36")
                                           .data("__EVENTTARGET", __EVENTTARGET)
                                           .data("__EVENTARGUMENT", __EVENTARGUMENT)
                                           .data("__LASTFOCUS", __LASTFOCUS)
                                           .data("__VIEWSTATE", __VIEWSTATE)
                                           .data("__EVENTVALIDATION", __EVENTVALIDATION)
                                           .data("__VIEWSTATEGENERATOR", __VIEWSTATEGENERATOR)
                                           .data("__PREVIOUSPAGE",
                                                 "DvymzSGon_02Yk18-IhJRL9pkNTxEUzsf6e0kCPM2zA5pg6DyGFwLe5jA781")
                                           .data("ddlLanguage", "en")
                                           .data("txtLoginEmail", "Email address")
                                           .data("pwPWord", "Password")
                                           .data("txtPassword", "password")
                                           .data("remember", "Remember me")
                                           .data("txtEmailAddress", "Email Address")
                                           .data("txtChangePasswordSW", "Password")
                                           .data("txtChangePasswordP", "")
                                           .data("txtConfirmPasswordSW", "Confirm Password")
                                           .data("txtConfirmPassword", "")
                                           .data("txtLocator", locator)
                                           .data("txtLName", name)
                                           .data("hdnIsEUUser", "False")
                                           .data("ddlCountry", "")
                                           .data("ddlSecurityQuestions", "")
                                           .data("txtSecurityAnswer", "Answer")
                                           .data("txtOfficeAdmUsername", "Username")
                                           .data("pwConfirmPWord", "Password")
                                           .data("txtOfficeAdmPassword", "")
                                           .data("txtUsername", "Username")
                                           .data("txtAdminForgotEmail", "Email address")

                                           .data("hdnPanelField", "")

                                           .method(Connection.Method.POST)
                                           .execute();

      Document printDoc1 = loginResp.parse();

      if (loginResp.statusCode() == 302) {
        try {
          Thread.sleep(100);//1 sec delay
        }
        catch (InterruptedException ie) {

        }

        int count = 0;
        while (count < 3) {
          count++;
          try {
            Connection.Response itinResp = Jsoup.connect("https://mytripandmore.com/Itinerary.aspx")
                                                .followRedirects(false)
                                                .cookie("ASP.NET_SessionId", aspSessionId)
                                                .cookie("sessionId", sessionId)
                                                .cookie("__utma", "1.672708320.1426000658.1427224774.1427892503.5")
                                                .cookie("__utmc", "1")
                                                .cookie("__utmz",
                                                        "1.1427892503.5.2.utmcsr=google|utmccn=(organic)" +
                                                        "|utmcmd=organic|utmctr=(not%20provided)")
                                                .cookie("lang", "en")
                                                .cookie("TimeFormat", "12")
                                                .cookie("CultureId", "en")
                                                .userAgent(
                                                    ":Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537" +
                                                    ".36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36")
                                                .timeout(60 * 1000)
                                                .data("PNR", locator)
                                                .data("lastName", name)
                                                .data("clockFormat", "12")
                                                .method(Connection.Method.GET)
                                                .execute();
            if (itinResp.statusCode() == 200) {
              Document printDoc = itinResp.parse();

              if (printDoc != null) {
                TripVO tripVO = getInfo(printDoc.toString(), locator) ;
                if (tripVO != null && tripVO.getFlights() != null && tripVO.getFlights().size() > 0) {
                  try {
                    Connection.Response eTicketResp = Jsoup.connect(
                        "https://mytripandmore.com/ElectronicTicketRecord.aspx?PNR=" + locator.toUpperCase())
                                                           .followRedirects(false)
                                                           .cookie("ASP.NET_SessionId", aspSessionId)
                                                           .cookie("sessionId", sessionId)
                                                           .cookie("__utma",
                                                                   "1.672708320.1426000658.1427224774.1427892503.5")
                                                           .cookie("__utmc", "1")
                                                           .cookie("__utmz",
                                                                   "1.1427892503.5.2.utmcsr=google|utmccn=(organic)"
                                                                   + "|utmcmd=organic|utmctr=(not%20provided)")
                                                           .cookie("lang", "en")
                                                           .cookie("TimeFormat", "12")
                                                           .cookie("CultureId", "en")
                                                           .userAgent(
                                                               ":Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) " +
                                                               "AppleWebKit/537" + ".36 (KHTML, " +
                                                               "like Gecko) Chrome/42.0.2311.90 Safari/537.36")
                                                           .timeout(60 * 1000)
                                                           .data("PNR", locator)
                                                           .data("lastName", name)
                                                           .data("clockFormat", "12")
                                                           .method(Connection.Method.GET)
                                                           .execute();
                    if (eTicketResp.statusCode() == 200) {
                      Document eTicketDoc = eTicketResp.parse();
                      getEticketInfo(eTicketDoc, tripVO);
                    }
                  } catch (Exception e) {

                  }
                }
                return tripVO;
              }
            }
            else if (itinResp.statusCode() == 302) { //might not be ready yet - so sleep for a sec
              try {
                Thread.sleep(100);//100 millsec delay
              }
              catch (InterruptedException ie) {

              }
            }
          }
          catch (Exception e) {
            e.printStackTrace();
            try {
              Thread.sleep(1000);//1 sec delay
            }
            catch (InterruptedException ie) {

            }
          }
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    return null;

  }

  public static void main(String[] args) {

    try {
/*
      Document doc = getItinerary("HUSTON.HOLM", "3GPZF2");
      if (doc != null) {
        System.out.println(doc.toString());
        TripVO tripVO = getInfo(doc.toString());
        System.out.println(tripVO);
      }
*/


      String s = FileUtils.readFileToString(new File("/Volumes/data2/temp/worldspan12.html"));
      TripVO tripVO = getInfo(s,"WWWW");
      System.out.println(tripVO);

      if (tripVO.getFlights() != null) {
        for (FlightVO flightVO:tripVO.getFlights()) {
          if (flightVO.getPassengers() != null) {
            for (PassengerVO passengerVO : flightVO.getPassengers()) {
              System.out.println(passengerVO);
            }
          }
        }
      }

/*
      TripVO tripVO = getItinerary("Santangelo", "275BN6");
      System.out.println(tripVO);
*/
/*
      //System.out.println(tripVO);
      String s1 = FileUtils.readFileToString(new File("/Volumes/data2/temp/worldspanetickets1.html"));
      Document doc = Jsoup.parse(s1);

      getEticketInfo(doc, tripVO);
      for (FlightVO vo : tripVO.getFlights()) {
        System.out.println(vo.getCode() + " " + vo.getNumber() + " - " + vo.getReservationNumber());
        if (vo.getPassengers() != null) {
          for (PassengerVO passengerVO: vo.getPassengers()) {
            System.out.println(passengerVO);
          }
        }
      }
      */

    }

    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void enhanceFlight(FlightVO flightVO, Element e, List<String> passengers) {
    String arriveDate = null;
    String arriveTime = null;
    String stopover = null;
    if (passengers != null && passengers.size() > 0) {
      for (String name: passengers) {
        PassengerVO passengerVO = new PassengerVO();
        passengerVO.setFirstName(name.substring(name.indexOf("/") + 1));
        passengerVO.setLastName(name.substring(0, name.indexOf("/")));
        flightVO.addPassenger(passengerVO);
      }
    }

    StringBuilder sb = new StringBuilder();
    Elements tables = e.select("table");
    if (tables.size() > 1) {
      Element table = tables.get(1);
      Elements rows = table.select("tr");
      if (rows != null) {
        for (Element row : rows) {
          Elements cols = row.select("td");
          for (Element col : cols) {
            if (col.toString().contains("lblConfirmationData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblConfirmationData");
              if (element != null && element.hasText()) {
                flightVO.setReservationNumber(element.text());
              }
            }
            else if (col.toString().contains("lblClassData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblClassData");
              if (element != null && element.hasText()) {
                if (flightVO.getPassengers() != null) {
                  for (PassengerVO passengerVO : flightVO.getPassengers()){
                    passengerVO.setSeatClass(element.text());
                  }
                } else {
                  sb.append("Class: ");
                  sb.append(element.text());
                  sb.append("\n");
                }
              }

            }
            else if (col.toString().contains("divOperatedBy")) {
              if (col.hasText()) {
                sb.append(col.text().trim());
              }
              sb.append("\n");
            }
            else if (col.toString().contains("lblDepartTerminalData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblDepartTerminalData");
              if (element != null && element.hasText()) {
                flightVO.getDepartureAirport().setTerminal(element.text());
              }
            }
            else if (col.toString().contains("lblSeatData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblSeatData");
              if (element != null && element.hasText()) {
                if (flightVO.getPassengers() != null) {
                  String seat = element.text();
                  String[] seats = seat.split(",");
                  int i = 0;
                  for (PassengerVO passengerVO : flightVO.getPassengers()){
                    if (seats.length > i) {
                      passengerVO.setSeat(seats[i]);
                    }
                    i++;
                  }
                } else {
                  sb.append("Assigned Seat(s): ");
                  sb.append(element.text());
                  sb.append("\n");
                }
              }

            }
            else if (col.toString().contains("lblMealData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblMealData");
              if (element != null && element.hasText()) {
                if (flightVO.getPassengers() != null) {
                  for (PassengerVO passengerVO : flightVO.getPassengers()){
                    passengerVO.setMeal(element.text());
                  }
                } else {
                  sb.append("Meal: ");
                  sb.append(element.text());
                  sb.append("\n");
                }
              }
            }
            else if (col.toString().contains("lblMileageData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblMileageData");
              if (element != null && element.hasText()) {
                sb.append("Mileage: ");
              }
              sb.append(element.text());
              sb.append("\n");
            }
            else if (col.toString().contains("lblClassTravelTimeData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblClassTravelTimeData");
              if (element != null && element.hasText()) {
                sb.append("Travel Time: ");
              }
              sb.append(element.text());
              sb.append("\n");
            }
            else if (col.toString().contains("lblArriveDateData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblArriveDateData");
              if (element != null && element.hasText()) {
                arriveDate = element.text().trim();
              }
            }
            else if (col.toString().contains("lblArriveLine1Data")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblArriveLine1Data");
              if (element != null && element.hasText()) {
                arriveTime = element.text().trim();
              }
            }
            else if (col.toString().contains("trdisplayStopOver")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "trdisplayStopOver");
              if (element.first() != null) {
                stopover = getText(element.first());
              }
            }
            else if (col.toString().contains("lblFQTVAir") && flightVO.getPassengers() != null) {
              try {
                String tableStr = table.toString();
                for (PassengerVO passengerVO : flightVO.getPassengers()) {
                  String fName = passengerVO.getFirstName()
                                            .substring(0, 1)
                                            .toUpperCase()
                                            .concat(passengerVO.getFirstName().substring(1).toLowerCase());
                  String lName = passengerVO.getLastName()
                                            .substring(0, 1)
                                            .toUpperCase()
                                            .concat(passengerVO.getLastName().substring(1).toLowerCase());
                  String nToken = fName + " " + lName;
                  if (tableStr.contains(nToken)) {
                    String fToken = tableStr.substring(tableStr.indexOf(nToken) + nToken.length());
                    if (fToken.indexOf("</td>") > 0) {
                      String fflyer = fToken.substring(0, fToken.indexOf("</td>"));
                      if (fflyer != null) {
                        passengerVO.setFrequentFlyer(fflyer);
                      }
                    }
                  }
                }
              }catch (Exception e1) {
                e1.printStackTrace();
              }


            }

          }

        }
      }
    }

    if (arriveDate != null && arriveTime != null) {
      String year = Utils.formatTimestamp(flightVO.getDepatureTime().getTime(), "YYYY");
      arriveDate = arriveDate + ", " + year;
      Timestamp ts = getTimestamp(arriveDate, arriveTime);
      if (ts != null) {
        flightVO.setArrivalTime(ts);
      }
    }

    if (stopover != null) {
      sb.append("\n\n Stopover:\n");
      sb.append(stopover);
    }

    flightVO.setNotes(sb.toString());

    /*
    Elements rows = e.select("tr");
    if (rows != null && rows.size() > 0) {
          Elements cols = row.select("td");

    }
    */

/*
    Elements tables = e.select("table");
    if (tables != null && tables.size() > 0) {
      //process first table
      Elements rows = tables.get(0).select("tr");
      if (rows != null && rows.size() > 0) {
        for (Element row : rows) {
          Elements cols = row.select("td");
          if (cols.size() == 2) {
            String col1 = cols.get(0).text();
            String col2 = cols.get(1).text();
            if (col1.contains("Confirmation Number")) {
              flightVO.setReservationNumber(col2);
            } else if (col1.contains("Class of Service:")) {
              sb.append(col1);sb.append(" ");sb.append(col2);sb.append("\n");
            }

          } else if (cols.size() == 3) {
            String col1 = cols.get(0).text();
            String col2 = cols.get(1).text();
            String col3 = cols.get(2).text();

            if (col1.contains("Depart") && col3.contains("Terminal")) {
              Elements elements = cols.get(2).select("nobr");
              if (elements != null && elements.size() > 0)
                flightVO.getDepartureAirport().setTerminal(elements.get(0).text());
            }
          }
        }
      }
      if (tables.size() >1) {
        //see if the second table is for seats
        Element secondTable = tables.get(1);
        String temp = getTextNoBr(secondTable);
        String[] tokens = temp.split("\n");
        boolean pSpecialReq = false;
        for (String s : tokens) {
          if (!s.contains("Click here for Flight Service Information")) {
            if (s.contains("Click here for Special Services Requested")) {
              pSpecialReq = true;
            } else if (s.contains("Remarks")){
              pSpecialReq = false;
            }
            if (pSpecialReq) {
              if (s.contains("Ticket Number"))
                sb.append(s);sb.append("\n");
            } else {
              if (!s.contains("In-Flight Services") && s.trim().length() > 0) {
                sb.append(s);sb.append("\n");
              }
            }
          }

        }
      }
    }

    flightVO.setNotes(sb.toString());
    */
  }

  public static void enhanceHotel(HotelVO hotelVO, Element e) {
    StringBuilder sb = new StringBuilder();
    StringBuilder sb1 = new StringBuilder();

    Elements rows = e.select("tr");
    if (rows.size() > 0) {
      for (Element row : rows) {
        Elements cols = row.select("td");
        if (cols.size() > 1) {
          Element col1 = cols.get(0);
          Element col2 = cols.get(1);
          String col1Text = getText(col1).trim();
          if (!col1Text.contains("Hotel") && !col1Text.contains("Check In") && !col1Text.contains("Check Out")) {
            if (col1Text.contains("Room Description") || col1Text.contains("Information") || col1Text.contains("Remarks")  || col1Text.contains("Rate")) {
              sb.append("\n");sb.append(col1Text); sb.append("\n");
            }

            sb.append(getText(col2));
            sb.append("\n");
          }
        }
        if (cols.size() == 5) {
          Element col3 = cols.get(3);
          Element col4 = cols.get(4);
          String col3Txt = getText(col3);
          if (!col3Txt.contains("Confirmation Number")) {
            sb1.append(col3Txt);sb1.append(" "); sb1.append(getText(col4));
            sb1.append("\n");

          }
        }
      }
    }

    sb.append("\n");
    sb.append(sb1);
    if (sb.toString().length() > 0) {
      hotelVO.setNote(sb.toString());
    }
  }

  public static void enhanceCar(TransportVO carVO, Element e) {

    StringBuilder sb = new StringBuilder();
    StringBuilder pickupLocation = new StringBuilder();
    StringBuilder dropOffLocation = new StringBuilder();

    String pickupDate = null;
    String pickupTime = null;
    String dropoffDate = null;
    String dropoffTime = null;

    Elements tables = e.select("table");
    if (tables.size() > 1) {
      Element table = tables.get(1);
      Elements rows = table.select("tr");
      if (rows != null) {
        for (Element row : rows) {
          Elements cols = row.select("td");
          for (Element col : cols) {
            if (col.toString().contains("lblVendorNameData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblVendorNameData");
              if (element != null && element.hasText()) {
                carVO.setName(element.text());
              }
            }
            else if (col.toString().contains("lblConfirmationData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblConfirmationData");
              if (element != null && element.hasText()) {
                carVO.setConfirmationNumber(element.text());
              }
            }
            else if (col.toString().contains("lblCarTypeData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblCarTypeData");
              if (element != null && element.hasText()) {
                sb.append("Car Type:");
              }
              sb.append("\n");
              sb.append(element.text());
            }
            else if (col.toString().contains("lblPickupLocationData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblPickupLocationData");
              if (element != null && element.hasText()) {
                pickupLocation.append(element.text());
              }
            }
            else if (col.toString().contains("lblPickupTimeData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblPickupTimeData");
              if (element != null && element.hasText()) {
                pickupDate = element.text();
              }
            }
            else if (col.toString().contains("lblPickupCityData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblPickupCityData");
              if (element != null && element.hasText()) {
                if (pickupLocation.toString().trim().length() > 0) {
                  pickupLocation.append(", ");
                }
                pickupLocation.append(element.text());
              }
            }
            else if (col.toString().contains("lblPickupData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblPickupData");
              if (element != null && element.hasText()) {
                pickupTime = element.text();
              }
            }
            else if (col.toString().contains("lblDropoffLocationData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblDropoffLocationData");
              if (element != null && element.hasText()) {
                dropOffLocation.append(element.text());
              }
            }
            else if (col.toString().contains("lblDropoffTimeData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblDropoffTimeData");
              if (element != null && element.hasText()) {
                dropoffDate = element.text();
              }
            }
            else if (col.toString().contains("lblDropoffCityData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblDropoffCityData");
              if (element != null && element.hasText()) {
                if (dropOffLocation.toString().trim().length() > 0) {
                  dropOffLocation.append(", ");
                }
                dropOffLocation.append(element.text());
              }
            }
            else if (col.toString().contains("lblDropoffData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblDropoffData");
              if (element != null && element.hasText()) {
                dropoffTime = element.text();
              }
            }
            else if (col.toString().contains("lblRateData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblRateData");
              if (element != null && element.hasText()) {
                sb.append("Rate:");
              }
              sb.append("\n");
              sb.append(element.text());
            }
            else if (col.toString().contains("lblRatePlanData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblRatePlanData");
              if (element != null && element.hasText()) {
                sb.append("Rate Plan:");
              }
              sb.append("\n");
              sb.append(element.text());
            }
            else if (col.toString().contains("lblExtraDayData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblExtraDayData");
              if (element != null && element.hasText()) {
                sb.append("Extra Day:");
              }
              sb.append("\n");
              sb.append(element.text());
            }
            else if (col.toString().contains("lblMileageData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblMileageData");
              if (element != null && element.hasText()) {
                sb.append("Mileage:");
              }
              sb.append("\n");
              sb.append(element.text());
            }
            else if (col.toString().contains("lblExtraHourData")) {
              Elements element = table.getElementsByAttributeValueContaining("id", "lblExtraHourData");
              if (element != null && element.hasText()) {
                sb.append("Extra Hour:");
              }
              sb.append("\n");
              sb.append(element.text());
            }
            else if (col.toString().contains("lblRemarks")) {
              sb.append("\n");
              sb.append(getText(row));
            }

          }
        }
      }
    }
    String pickupYear = Utils.formatTimestamp(carVO.getPickupDate().getTime(), "YYYY");
    String dropoffYear = pickupYear;
    if (carVO.getDropoffDate() != null) {
      dropoffYear = Utils.formatTimestamp(carVO.getDropoffDate().getTime(), "YYYY");
    }


    carVO.setPickupLocation(pickupLocation.toString());
    carVO.setDropoffLocation(dropOffLocation.toString());
    if (pickupDate != null) {
      Timestamp ts = null;
      if (pickupTime != null && (pickupTime.contains("AM") || pickupTime.contains("PM"))) {
        ts = getTimestamp(pickupDate + ", " + pickupYear, pickupTime);
      }
      else {
        ts = getTimestamp(pickupDate + ", " + pickupYear, "0:00");
      }

      if (ts != null) {
        carVO.setPickupDate(ts);
      }
    }

    if (dropoffDate != null) {
      Timestamp ts = null;
      if (dropoffTime != null && (dropoffTime.contains("AM") || dropoffTime.contains("PM"))) {
        ts = getTimestamp(dropoffDate + ", " + dropoffYear, dropoffTime);
      }
      else {
        ts = getTimestamp(dropoffDate + ", " + dropoffYear, "0:00");
      }

      if (ts != null) {
        carVO.setDropoffDate(ts);
      }
    }

    carVO.setNote(sb.toString());
  }

  public static void getEticketInfo( Document doc, TripVO tripVO) {
    try {
      if (doc != null && doc.toString().contains("lblETicketTravelerNameData")) {
        getEticketInfoSingle(doc, tripVO);
      } else if (doc != null && doc.toString().contains("lbxMultipleETicketsData")) {
        getEticketInfoMultiple(doc, tripVO);
      }

    } catch (Exception e) {

    }

  }

  public static void getEticketInfoMultiple( Document doc, TripVO tripVO) {
    try {

      Elements lists = doc.getElementsByAttributeValueContaining("name", "lbxMultipleETicketsData");
      if (lists != null && lists.size() > 0) {
        Element selectList = lists.get(0);
        Elements options = selectList.select("option");
        for (Element o: options) {
          String s = o.text();
          String[] tokens = s.split(" ");
          String eTicket = tokens[tokens.length - 1];
          String airlineCode = tokens[tokens.length - 3];
          String lastName = s.substring(0, s.indexOf("/")).trim();
          if (lastName.contains(" ")) {
            lastName = lastName.replace(" ", ".");
          }
          String firstName = s.substring(s.indexOf("/") + 1, s.indexOf(airlineCode)).trim();
          if (firstName.contains(" ")) {
            firstName = firstName.replace(" ", ".");
          }
          if (airlineCode != null && airlineCode.length() == 2) {
            for (FlightVO vo : tripVO.getFlights()) {
              if (vo.getCode() != null && vo.getCode().equals(airlineCode)) {
                boolean found = false;
                if (vo.getPassengers() != null && vo.getPassengers().size() > 0) {
                  for (PassengerVO passengerVO: vo.getPassengers()) {
                    String fName = passengerVO.getFirstName().replace(" ", ".").toUpperCase();
                    String lName = passengerVO.getLastName().replace(" ", ".").toUpperCase();

                    if (fName.equals(lastName) && lName.equals(firstName)) {
                      passengerVO.seteTicketNumber(eTicket);
                      found = true;
                      break;
                    }
                  }
                }
                if (!found) {
                  PassengerVO passengerVO = new PassengerVO();
                  passengerVO.setFirstName(firstName);
                  passengerVO.setLastName(lastName);
                  passengerVO.seteTicketNumber(eTicket);
                  vo.addPassenger(passengerVO);
                }
              }
            }
          }

        }

      }




    } catch (Exception e) {

    }
  }


  public static void getEticketInfoSingle( Document doc, TripVO tripVO) {
    try {
      List <String> confirms = new ArrayList<>();


      Elements passengers = doc.getElementsByAttributeValueContaining("id","lblETicketTravelerNameData");
      PassengerVO passengerVO = new PassengerVO();

      if (passengers != null && passengers.size() > 0) {
        for (Element e: passengers) {
          passengerVO.setFullName(e.text());
        }
      }
      Elements etickets = doc.getElementsByAttributeValueContaining("id","lblETicketNumberData");
      if (etickets != null && etickets.size() > 0) {
        for (Element e: etickets) {
          passengerVO.seteTicketNumber(e.text());
        }
      }
      Elements confirmNums = doc.getElementsByAttributeValueContaining("id","lblAirlineRecLocData");
      if (confirmNums != null && confirmNums.size() > 0) {
        for (Element e: confirmNums) {
          String confirmNum = e.text();
          if (confirmNum.length() >6) {
            confirmNum= confirmNum.substring(0,6);
          }
          if (confirmNum.length() == 6 &&  !confirms.contains(confirmNum)) {

            for (FlightVO vo : tripVO.getFlights()) {
              if (vo.getReservationNumber() != null && vo.getReservationNumber().equals(confirmNum)) {
                boolean found = false;
                if (vo.getPassengers() != null && vo.getPassengers().size() > 0) {
                  for (PassengerVO pVO:vo.getPassengers()) {
                    String firstName = pVO.getFirstName().replace(".", " ").toUpperCase();
                    String lastName = pVO.getLastName().replace(".", " ").toUpperCase();
                    String fullName = passengerVO.getFullName().replace(".", " ").toUpperCase();
                    if (fullName.contains(firstName) && fullName.contains(lastName)) {
                      found = true;
                      pVO.seteTicketNumber(passengerVO.geteTicketNumber());
                      break;
                    }
                  }
                }
                if (!found) {
                  vo.addPassenger(passengerVO);
                }
              }
            }
            confirms.add(confirmNum);
          }

        }
      }

    } catch (Exception e) {

    }
  }

    public static String getText(Element parentElement) {
    StringBuilder working = new StringBuilder();
    for (Node child : parentElement.childNodes()) {
      if (child instanceof TextNode) {
        working.append(((TextNode) child).text());
      }
      if (child instanceof Element) {
        Element childElement = (Element) child;
        // do more of these for p or other tags you want a new line for
        if (childElement.tag().getName().equalsIgnoreCase("br") || childElement.tag()
                                                                               .getName()
                                                                               .equalsIgnoreCase("tr")) {
          working.append("\n");
        }

        working.append(getText(childElement));
      }
    }

    return working.toString().trim();
  }

  public static String getTextNoBr(Element parentElement) {
    StringBuilder working = new StringBuilder();
    for (Node child : parentElement.childNodes()) {
      if (child instanceof TextNode) {
        working.append(((TextNode) child).text());
      }
      if (child instanceof Element) {
        Element childElement = (Element) child;
        // do more of these for p or other tags you want a new line for
        if (childElement.tag().getName().equalsIgnoreCase("tr") || childElement.tag()
                                                                               .getName()
                                                                               .equalsIgnoreCase("table") || childElement
                .tag()
                .getName()
                .equalsIgnoreCase("div")) {
          working.append("\n");
        }

        working.append(getTextNoBr(childElement));
      }
    }

    return working.toString().trim();
  }

  protected void initImpl(Map<String, String> params) {
  }

  protected TripVO extractDataImpl(String input)
      throws Exception {

    return null;
  }

  @Override
  public ArrayList<ParseError> getParseErrors() {
    return null;
  }

}
