package com.mapped.publisher.parse.extractor.booking;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.valueObject.ActivityVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.HotelVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.TransportVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import java.io.InputStream;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Serguei Moutovkin on 2014-05-05.
 * <p/>
 * Parsers below are based on the following assumptions
 * <p/>
 * 1. At least one section (either tasks or items is present)
 * 2. Tasks/Items are separated by a sequence of characters that start at the beginning of the line
 * 3. Sequence of characters (in #2) is from a fixed set of possibilities
 * 4. Each tasks/item contains anywhere in the string must contain 3 numbers 1 floating point followed by an integer
 * and then followed by another floating point number
 * 5. Each valid task must contain a date written as an integer followed by either abbreviated or full name of the
 * month, if not date will be assumed to be the same as the element just preceding it
 */
public class QueenOfClubsExtractor
    extends BookingExtractor {
  private static final boolean DEBUG = false; //Compiler will remove all if(DEBUG) statements while it is set to false

  private static String footerHeader = "Subtotal\\s+[0-9,\\.]+";
  private static String tasksHeader = "Task\\s*Time\\s*Entry\\s*Notes\\s*Rate\\s*Hours\\s*Line\\s*Total";
  private static String itemsHeader = "Item\\s*Description\\s*Unit\\s*Cost\\s*Quantity\\s*Line\\s*Total";
  private static Pattern taskAndItemStarter;

  /**
   * Matching dates in various order DD MMM, MMM DD, DD AND DD MMM, MMM DD AND DD.
   * Pattern will accept short or full month names
   */
  private static Pattern datePattern = Pattern.compile("(?:(?:([0-9]{1,2})\\s*(?:TO|AND|\\-)\\s*)?([0-9]{1,2}))?\\s*" +
                                                       "(JAN(?:UARY)?|FEB(?:RUARY)?|MAR(?:CH)?|APR(?:IL)?|MAY|JUN" +
                                                       "(?:E)?|JUL(?:Y)?|AUG(?:UST)?|SEP(?:TEMBER)?|OCT(?:OBER)?|NOV" +
                                                       "(?:EMBER)?|DEC(?:DECEMBER)?)\\s*" +
                                                       "(?:(?:([0-9]{1,2})\\s*(?:TO|AND|\\-)\\s*)?(([0-9]{1,2})))?\\s*",
                                                       Pattern.CASE_INSENSITIVE);

  /** matching dd MMM to dd MMM pattern
   *
   */
  private static Pattern dateRangePattern = Pattern.compile("([0-9]{1,2})|(JAN(?:UARY)?|FEB(?:RUARY)?|MAR(?:CH)" +
                                                            "?|APR(?:IL)?|MAY|JUN(?:E)?|JUL(?:Y)?|AUG(?:UST)?|SEP" +
                                                            "(?:TEMBER)?|OCT(?:OBER)?|NOV(?:EMBER)?|DEC(?:DECEMBER))|TO",Pattern.CASE_INSENSITIVE);

  private static Pattern dateRangeExactPattern = Pattern.compile("([0-9]{1,2})\\s*(JAN(?:UARY)?|FEB(?:RUARY)?|MAR" +
                                                                 "(?:CH)?|APR(?:IL)?|MAY|JUN(?:E)?|JUL(?:Y)?|AUG" +
                                                                 "(?:UST)?|SEP(?:TEMBER)?|OCT(?:OBER)?|NOV(?:EMBER)" +
                                                                 "?|DEC(?:DECEMBER))\\s*to\\s*([0-9]{1," +
                                                                 "2})\\s*(JAN(?:UARY)?|FEB(?:RUARY)?|MAR(?:CH)?|APR" +
                                                                 "(?:IL)?|MAY|JUN(?:E)?|JUL(?:Y)?|AUG(?:UST)?|SEP" +
                                                                 "(?:TEMBER)?|OCT(?:OBER)?|NOV(?:EMBER)?|DEC" +
                                                                 "(?:DECEMBER))", Pattern.CASE_INSENSITIVE);

  private static Pattern timePattern = Pattern.compile("([0-2]?[0-9])[:\\.]([0-5][0-9])",
                                                       Pattern.CASE_INSENSITIVE); //Time can not be in the format
                                                       // HH-MM otherwise it will be confused with date range
  private static Pattern rateQuantityTotalPattern = Pattern.compile("[0-9\\.\\,]\\s+[0-9\\,]\\s+[0-9\\.\\,]");

  public enum QofCSections {
    SECTION_HEADER,
    SECTION_TASKS,
    SECTION_ITEMS,
    SECTION_FOOTER
  }

  private Map<QofCSections, String> sectionStrings;

  public String rawPDFText;
  private ArrayList<ParseError> parseErrors = new ArrayList<ParseError>();

  private static final Map<String, TripVO.ValObjects> typesMap = new HashMap<String, TripVO.ValObjects>();

  /**
   * Static Element Initialization
   */
  static {
    typesMap.put("as directed", TripVO.ValObjects.VO_TRANSPORT);
    typesMap.put("tailor made tour", TripVO.ValObjects.VO_ACTIVITY);
    typesMap.put("tailor made london", TripVO.ValObjects.VO_ACTIVITY);
    typesMap.put("transfer", TripVO.ValObjects.VO_TRANSPORT);
    typesMap.put("guide", TripVO.ValObjects.VO_ACTIVITY);
    typesMap.put("attraction", TripVO.ValObjects.VO_ACTIVITY);
    typesMap.put("assistance", TripVO.ValObjects.VO_TRANSPORT); //Does this get parsed?
    typesMap.put("assistant", TripVO.ValObjects.VO_TRANSPORT); //Does this get parsed?
    typesMap.put("coach 57", TripVO.ValObjects.VO_TRANSPORT);
    typesMap.put("coach 49", TripVO.ValObjects.VO_TRANSPORT);
    typesMap.put("coach", TripVO.ValObjects.VO_TRANSPORT);
    typesMap.put("train", TripVO.ValObjects.VO_TRANSPORT);
    typesMap.put("flight", TripVO.ValObjects.VO_TRANSPORT);

    // typesMap.put("tailor made", TripVO.ValObjects.VO_ACTIVITY);

    typesMap.put("vip porters", TripVO.ValObjects.VO_TRANSPORT);
    typesMap.put("vip meet and greet", TripVO.ValObjects.VO_TRANSPORT);

    typesMap.put("restaurant", TripVO.ValObjects.VO_ACTIVITY);
    typesMap.put("champagne", TripVO.ValObjects.VO_ACTIVITY);
    typesMap.put("meal", TripVO.ValObjects.VO_ACTIVITY);
    typesMap.put("micellaneous", TripVO.ValObjects.VO_ACTIVITY);
    typesMap.put("miscellaneous", TripVO.ValObjects.VO_ACTIVITY);
    typesMap.put("hotel", TripVO.ValObjects.VO_HOTEL);
    typesMap.put("ticket", TripVO.ValObjects.VO_ACTIVITY);
    typesMap.put("minicoach Lux35", TripVO.ValObjects.VO_TRANSPORT);
    typesMap.put("lunch", TripVO.ValObjects.VO_ACTIVITY);
    typesMap.put("dinner", TripVO.ValObjects.VO_ACTIVITY);
    typesMap.put("theatre", TripVO.ValObjects.VO_ACTIVITY);
    typesMap.put("private guide", TripVO.ValObjects.VO_ACTIVITY);

    taskAndItemStarter = QueenOfClubsExtractor.buildBasicTasksRE();
  }


  protected TripVO extractDataImpl(InputStream input)
      throws Exception {
    TripVO tvo = new TripVO();


    PDDocument pdfDoc = PDDocument.load(input);
    PDFTextStripper stripper = new PDFTextStripper();
    stripper.setSortByPosition(true);
    rawPDFText = stripper.getText(pdfDoc);

    sectionStrings = getSections(rawPDFText);

    //Parsing both sections
    if (sectionStrings.containsKey(QofCSections.SECTION_TASKS)) {
      parseElements(tvo, QofCSections.SECTION_TASKS);
    }
    if (sectionStrings.containsKey(QofCSections.SECTION_ITEMS)) {
      parseElements(tvo, QofCSections.SECTION_ITEMS);
    }

    tvo.setImportSrc(BookingSrc.ImportSrc.QUEEN_OF_CLUB);
    tvo.setImportTs(System.currentTimeMillis());
    return tvo;
  }

  protected void initImpl(Map<String, String> params) {
    return;
  }

  /**
   * @param rawText text string extracted from the PDF Document
   * @return Map of various sections of the document
   * @note Function assumes that tasks section goes above items section
   */
  private Map<QofCSections, String> getSections(String rawText) {
    Map<QofCSections, String> sectionStrings = new HashMap<QofCSections, String>();
    boolean tasksPresent = false;
    boolean itemsPresent = false;
    boolean footerPresent = false;

    String topAndBottom[] = rawText.split(footerHeader);
    if (topAndBottom.length != 1) {
      footerPresent = true;
      sectionStrings.put(QofCSections.SECTION_FOOTER, topAndBottom[1]);
    }

    String itemStrings[] = topAndBottom[0].split(itemsHeader);
    if (itemStrings.length != 1) //No items header found => either wrong document or just tasks present
    {
      itemsPresent = true;
    }

    String taskStrings[] = itemStrings[0].split(tasksHeader);
    if (taskStrings.length != 1) {
      tasksPresent = true;
    }

    sectionStrings.put(QofCSections.SECTION_HEADER, taskStrings[0]);


    if (itemsPresent && tasksPresent) {
      String tasks = StringUtils.join(taskStrings, "", 1, taskStrings.length);
      String items = StringUtils.join(itemStrings, "", 1, itemStrings.length);
      sectionStrings.put(QofCSections.SECTION_TASKS, tasks);
      sectionStrings.put(QofCSections.SECTION_ITEMS, items);
    }
    else if (itemsPresent && !tasksPresent) {
      String items = StringUtils.join(itemStrings, "", 1, itemStrings.length);
      sectionStrings.put(QofCSections.SECTION_ITEMS, items);
    }
    else if (!itemsPresent && tasksPresent) {
      String tasks = StringUtils.join(taskStrings, "", 1, taskStrings.length);
      sectionStrings.put(QofCSections.SECTION_TASKS, tasks);
    }
    return sectionStrings;
  }


  /**
   * Regular expression used to split list of items in the QofC invoice.
   * <p/>
   * This also removes prices from the resulting string.
   *
   * @return Pattern that can be used to initialize static variable used to match
   * @note This does not match all different elements of the task
   */
  private static Pattern buildBasicTasksRE() {
    StringBuilder strBuilder = new StringBuilder("^((("); //I want group(1) to be full string, sans prices

    Set<String> keys = typesMap.keySet();
    int counter = 0;
    for (String key : keys) {
      if (++counter == keys.size()) {
        strBuilder.append(key);
      }
      else {
        strBuilder.append(key).append("|");
      }
    }
    strBuilder.append(")).*?)");                            //Reluctant matching for the rest of the string
    strBuilder.append("(?:(?:\\s*?[0-9\\.\\,]+\\s*){3})$"); //This is non-matching group for prices
    return Pattern.compile(strBuilder.toString(), Pattern.CASE_INSENSITIVE);
  }

  private ArrayList<Pair<String, String>> getElementStrings(QofCSections section) {
    ArrayList<Pair<String, StringBuilder>> tasks = new ArrayList<Pair<String, StringBuilder>>();

    //1. Splitting text dump into separate lines
    String lines[] = sectionStrings.get(section).split("\\r?\\n");

    int elementCount = 0;

    //2. For each line check if it is a starter of a new Task or Item element
    for (String line : lines) {
      Matcher pm = taskAndItemStarter.matcher(line);
      if (pm.matches()) {
        elementCount++;
        //group(1) is a group without prices, group(2) is task/item identifier
        tasks.add(Pair.of(pm.group(2), new StringBuilder(pm.group(1))));

        if (DEBUG) {
          for (int g = 0; g < pm.groupCount(); g++) {
            Log.log(LogLevel.DEBUG, "SB Group #" + g + " >>>" + pm.group(g) + "<<<");
          }
        }
      }
      else {
        if (elementCount > 0) {
          tasks.get(elementCount - 1).getRight().append(" ").append(line);
        }
      }
    }

    //3. Creating array of pairs
    ArrayList<Pair<String, String>> taskStrings = new ArrayList<Pair<String, String>>();
    for (Pair<String, StringBuilder> pr : tasks) {
      String elemString = pr.getRight().toString().replaceAll("\\s+", " ");
      taskStrings.add(Pair.of(pr.getLeft(), elemString)); //We are also removing duplicate spaces
    }
    return taskStrings;
  }


  private void parseElements(TripVO tripvo, QofCSections section) {
    ArrayList<Pair<String, String>> elementPairs = getElementStrings(section);
    int prevElemDayBeg = 0;
    int prevElemDayEnd = 0;
    String prevMonth = "";
    String currMonth = "";
    String startMonth = "";



    for (Pair<String, String> ePair : elementPairs) {
      String taskString = ePair.getRight();
      if(ePair.getLeft() != null && !ePair.getLeft().trim().toLowerCase().contains("hotel")) {
        taskString = taskString.replace(ePair.getLeft(), "");
      }
      int fromHH = 0;
      int fromMM = 0;
      int toHH = 0;
      int toMM = 0;
      int elemDayBeg = 0;
      int elemDayEnd = 0;
      prevMonth = currMonth;
      startMonth = "";
      boolean wrongDate = true;

      boolean dateRangeFound = false;

      Matcher em = dateRangeExactPattern.matcher(taskString);
      if (em.find() ) {
        String s = em.group(0);
        if (s != null) {
          String [] tokens = s.split(" ");
          if (tokens != null && tokens.length == 5) {
            elemDayBeg = Integer.parseInt(tokens[0]);
            startMonth = tokens[1];
            elemDayEnd = Integer.parseInt(tokens[3]);
            currMonth = tokens[4];
            prevElemDayBeg = elemDayBeg;
            prevElemDayEnd = elemDayEnd;
            dateRangeFound =true;
            wrongDate = false;
          }
        }


      }

      if (!dateRangeFound) {

        //1. Get Date
        Matcher dm = datePattern.matcher(taskString);
        if (dm.find()) //Currently taking the first date in the range and using it for the event
        {
          try {

            if (DEBUG) {
              Log.log(LogLevel.DEBUG, "Group1: (" + dm.group(1) + "," + dm.group(2) + ")" +
                                      "Group2: (" + dm.group(4) + "," + dm.group(5) + ") MMM:" + dm.group(3) +
                                      " Str:" + taskString);
            }


            if (dm.group(1) != null || dm.group(2) != null) { //If date appears before Month
              elemDayBeg = (dm.group(1) != null) ? Integer.parseInt(dm.group(1)) : 0;
              elemDayEnd = (dm.group(2) != null) ? Integer.parseInt(dm.group(2)) : 0;
            }
            else if (dm.group(4) != null || dm.group(5) != null) { //If date appears after Month
              elemDayBeg = (dm.group(4) != null) ? Integer.parseInt(dm.group(4)) : 0;
              elemDayEnd = (dm.group(5) != null) ? Integer.parseInt(dm.group(5)) : 0;
            }

            if (elemDayBeg != 0 || elemDayEnd != 0) { //Set month only if dates are available
              currMonth = (dm.group(3) != null) ? dm.group(3) : prevMonth;
            }

            //If there is either a valid start or end date but the other one is not,
            // set them to the same value
            if (elemDayBeg != 0 && elemDayEnd != 0) {
              wrongDate = false;
            }
            else if (elemDayEnd == 0 && elemDayBeg > 0 && elemDayBeg < 32) {
              elemDayEnd = elemDayBeg;
              wrongDate = false;
            }
            else if (elemDayBeg == 0 && elemDayEnd > 0 && elemDayEnd < 32) {
              elemDayBeg = elemDayEnd;
              wrongDate = false;
            }
            else if (elemDayBeg == 0 && elemDayEnd == 0)//No valid date found, but month is present
            {
              if (section == QofCSections.SECTION_TASKS) {
                elemDayBeg = prevElemDayBeg;
                elemDayEnd = prevElemDayEnd;
                wrongDate = false;
              }
              addParseError(new ParseError(ParseError.ParseErrorSeverity.PARSE_ERROR_SEVERITY_NORMAL,
                                           ParseError.ParseErrorType.PARSE_ERROR_TYPE_MISSING_DATE,
                                           taskString));
            }

            prevElemDayBeg = elemDayBeg;
            prevElemDayEnd = elemDayEnd;


          }
          catch (Exception e) {
            Log.log(LogLevel.INFO, "Exception parsing date/month in PDF");
          } //Do nothing
        }
        else {
          //For tasks Assume previous valid dates if dates are missing
          if (section == QofCSections.SECTION_TASKS) {
            elemDayBeg = prevElemDayBeg;
            elemDayEnd = prevElemDayEnd;
            currMonth = prevMonth;
            wrongDate = false;
          }

          Log.log(LogLevel.DEBUG, "ERROR: No Date Found in :" + taskString);
          addParseError(new ParseError(ParseError.ParseErrorSeverity.PARSE_ERROR_SEVERITY_NORMAL,
                                       ParseError.ParseErrorType.PARSE_ERROR_TYPE_MISSING_DATE,
                                       taskString));
        }
      }

      //All items without a valid date are ignored TODO: Thierry, please confirm
      if (!wrongDate || (section == QofCSections.SECTION_TASKS)) {
        //2. Get Time
        Matcher tm = timePattern.matcher(taskString);
        int i = 0;

        while (tm.find()) {
          try {
            int HH = Integer.parseInt(tm.group(1));
            int MM = Integer.parseInt(tm.group(2));
            ++i;
            if (i == 1) {
              fromHH = HH;
              fromMM = MM;
            }
            else if (i == 2) {
              toHH = HH;
              toMM = MM;
              break;
            }
          }
          catch (Exception e) {
            //Do nothing
          }
        }

        if (i == 0) {
          toHH = 0;
          toMM = 0;
          fromHH = 0;
          fromMM = 0;
          if (section == QofCSections.SECTION_TASKS) {
            addParseError(new ParseError(ParseError.ParseErrorSeverity.PARSE_ERROR_SEVERITY_NORMAL,
                                         ParseError.ParseErrorType.PARSE_ERROR_TYPE_MISSING_TIME,
                                         taskString));
          }
        }
        else if (i == 1) {
          toHH = fromHH;
          toMM = fromMM;
          if (section == QofCSections.SECTION_TASKS) {
            addParseError(new ParseError(ParseError.ParseErrorSeverity.PARSE_ERROR_SEVERITY_MINOR,
                                         ParseError.ParseErrorType.PARSE_ERROR_TYPE_MISSING_STOP_TIME,
                                         taskString));
          }
        }

        //3. Try to guess if this element might contain another element that was not parsed
        Matcher sp = rateQuantityTotalPattern.matcher(taskString);
        if (sp.find()) {
          addParseError(new ParseError(ParseError.ParseErrorSeverity.PARSE_ERROR_SEVERITY_CRITICAL,
                                       ParseError.ParseErrorType.PARSE_ERROR_TYPE_UNEXPECTED_DATA,
                                       taskString));
        }


        //4. Insert data into correct class
        if(startMonth == null || startMonth.trim().length() == 0) {
          startMonth =currMonth;
        }
        Timestamp tsFrom = getTimestamp(startMonth, elemDayBeg, fromHH, fromMM);
        Timestamp tsTo = getTimestamp(currMonth, elemDayEnd, toHH, toMM);

        if (tsFrom.getTime() == tsTo.getTime()) {
          tsTo = null;
        }
        if (wrongDate) //For items we don't want to assume previous date
        {
          tsTo = null;
          tsFrom = null;
        }

        TripVO.ValObjects className = typesMap.get(ePair.getLeft().toLowerCase().trim());
        if (className == null) {
          className = TripVO.ValObjects.VO_ACTIVITY;
        }
        String name = ePair.getLeft();
        switch (className) {
          case VO_ACTIVITY:
            boolean insertVO = true;
            if (section.equals(QofCSections.SECTION_ITEMS)) {
              //it this is a ticket or attaction, try to match it to a tour and don't create a separate activity
              if (ePair.getLeft().toLowerCase().equals("ticket") || ePair.getLeft()
                                                                         .toLowerCase()
                                                                         .equals("attraction")) {
                //try to find a matching VO
                for (ActivityVO vo : tripvo.getActivities()) {
                  if (vo.getStartDate() != null && tsFrom != null) {
                    String voDate = Utils.formatDateControl(vo.getStartDate().getTime());
                    String newDate = Utils.formatDateControl(tsFrom.getTime());

                    if (vo.getTag() != null && vo.getTag()
                                                  .toLowerCase()
                                                  .contains("tailor made") && voDate != null && newDate != null &&
                        voDate
                            .equals(newDate)) {
                      StringBuilder sb = new StringBuilder(vo.getNote());
                      sb.append("\n\n");
                      sb.append(taskString);
                      vo.setNote(sb.toString());
                      insertVO = false;
                      break;
                    }
                  }
                }
              }
            }

            if (insertVO) {
              ActivityVO avo = new ActivityVO();
              avo.setStartDate(tsFrom);
              avo.setEndDate(tsTo);
              avo.setNote(taskString);
              avo.setTag(new String(name));
              if (taskString != null) {
                String[] tokens = taskString.split(" - ");
                if (tokens != null && tokens.length > 2) {
                  name = tokens[1];
                  if(name != null) {
                    name = name.trim();
                  }
                }
              }
              avo.setName(name);
              tripvo.addActivity(avo);
            }
            break;
          case VO_HOTEL:
            HotelVO hvo = new HotelVO();

            if (taskString != null) {
              String[] tokens = taskString.split(" - ");
              if (tokens != null && tokens.length > 2) {
                name = tokens[1];
                if(name != null) {
                  name = name.trim();
                }
              }
            }
            hvo.setCheckin(tsFrom);
            if (tsTo == null && tsFrom != null) {

              long day = 24 * 60 * 60 * 1000;
              long checkout = tsFrom.getTime() + day;
              tsTo = new Timestamp(checkout);
            }
            hvo.setCheckout(tsTo);

            hvo.setNote(taskString);
            hvo.setHotelName(name);
            if (tsFrom != null) {
              tripvo.addHotel(hvo);
            }
            break;
          case VO_TRANSPORT:
            if (taskString != null) {
              String[] tokens = taskString.split(" - ");
              if (tokens != null && tokens.length > 2) {
                name = tokens[1];
                if(name != null) {
                  name = name.trim();
                }
              }
            }
            if (tsFrom != null) {
              TransportVO tvo = new TransportVO();
              tvo.setPickupDate(tsFrom);
              tvo.setDropoffDate(tsTo);
              tvo.setNote(taskString);
              tvo.setCmpyName(ePair.getLeft());
              tvo.setName(name);
              tripvo.addTransport(tvo);
            }

            break;
          default:
            Log.log(LogLevel.ERROR, "Unsupported Value Object Type");
        }

      }
      else {
        Log.log(LogLevel.INFO, "UNPROCESSED TASK: " + taskString);
        addParseError(new ParseError(ParseError.ParseErrorSeverity.PARSE_ERROR_SEVERITY_CRITICAL,
                                     ParseError.ParseErrorType.PARSE_ERROR_TYPE_UNPROCESSED,
                                     taskString));

      }
    }

  }

  private void addParseError(ParseError pe) {
    if (parseErrors.size() == 0) {
      parseErrors.add(pe);
      return;
    }
    //Only adding more severe parse errors for the same element
    ParseError prevError = parseErrors.get(parseErrors.size() - 1);
    if (prevError.text.compareTo(pe.text) == 0) {
      if (prevError.severity.getLevel() < pe.severity.getLevel()) {
        parseErrors.set(parseErrors.size() - 1, pe);
      }
    }
    else {
      parseErrors.add(pe);
    }
  }

  private Timestamp getTimestamp(String month, int day, int hh, int mm) {
    //Getting current time
    Calendar cal = Calendar.getInstance();
    int currYY = cal.get(Calendar.YEAR);
    int currMM = cal.get(Calendar.MONTH);

    if (DEBUG) {
      Log.log(LogLevel.DEBUG, "getTimestamp(" + month + ", " + day + ", " + hh + ", " + mm + ")");
    }
    //Convert month to computer understandable value
    try {
      Date date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(month);
      cal.setTime(date);
    }
    catch (ParseException e) {
      Log.log(LogLevel.ERROR, "Failed to parse month :" + month);
    }

    int itemMonth = cal.get(Calendar.MONTH);

    if (itemMonth - currMM < -2) //If up to three months old, still the same year, otherwise next
    {
      ++currYY;
    }

    cal.setTimeInMillis(0); //Eliminate seconds and milliseconds from getInstance() call
    cal.set(currYY, itemMonth, day, hh, mm);

    return new Timestamp(cal.getTimeInMillis());
  }

  /**
   * Access to the list of parse errors.
   *
   * @return list of parse errors,
   * @note must be called only after calling @see extractData() method
   */
  @Override
  public ArrayList<ParseError> getParseErrors() {
    return parseErrors;
  }


  @Override
  public void postProcessingImpl(String tripId, String userId) {
  }
  public String toString() {
    StringBuilder strBuilder = new StringBuilder("Split Document\n");
    strBuilder.append("\nHEADER\n\n" + sectionStrings.get(QueenOfClubsExtractor.QofCSections.SECTION_HEADER));
    strBuilder.append("\nTASKS \n\n" + sectionStrings.get(QueenOfClubsExtractor.QofCSections.SECTION_TASKS));
    strBuilder.append("\nITEMS \n\n" + sectionStrings.get(QueenOfClubsExtractor.QofCSections.SECTION_ITEMS));
    strBuilder.append("\nFOOTER\n\n" + sectionStrings.get(QueenOfClubsExtractor.QofCSections.SECTION_FOOTER));

    strBuilder.append("\nRAW\n" + rawPDFText);
    return strBuilder.toString();
  }
}
