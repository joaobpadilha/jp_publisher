package com.mapped.publisher.parse.extractor.booking;

import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-10-25
 * Time: 9:32 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class BookingExtractor {

  private String cmpyId = null;
  private String userId = null;

  /* Parameter configurable by the administrator */

  /** Slogan that might appear on each page to be removed */
  public final static String CONF_SLOGAN = "SLOGAN";

  public static enum Parsers {
    SABRE(com.mapped.publisher.parse.extractor.booking.SabreExtractor.class, "Sabre TripCase", true),
    CLIENTBASE(com.mapped.publisher.parse.extractor.booking.ClientbaseExtractor.class, "Clientbase Invoice", true),
    WORLDSPAN(com.mapped.publisher.parse.extractor.booking.WorldspanTramsParser.class, "Worldspan TRAMS Invoice (Beta)", true),
    AMADEUS(com.mapped.publisher.parse.extractor.booking.AmadeusExtractor2.class, "Amadeus", true),
    AMADEUS_OLD(com.mapped.publisher.parse.extractor.booking.AmadeusCheckMyTrip.class, "Amadeus - CheckMyTrip (Beta)", true),
    QOFC(com.mapped.publisher.parse.extractor.booking.QueenOfClubsExtractor.class, "Queen of Clubs (Freshbooks)", false),
    ATLANTICO(com.mapped.publisher.parse.extractor.booking.TopAtlanticoExtractor.class, "Top Atlântico (Itinerary)", false),
    GOLDMAN(com.mapped.publisher.parse.extractor.booking.GoldmanParser.class, "Tramada Invoice (Itinerary)", false),
    ALPINE_VACATIONS(com.mapped.publisher.parse.extractor.booking.AlpineExtractor.class, "Alpine Adventures Invoice (Beta)", true),
    TRAVEL2(com.mapped.publisher.parse.extractor.booking.Travel2Extractor.class, "Travel2 PDF", true),
    ISLANDSINTHESUN(com.mapped.publisher.parse.extractor.booking.Travel2Extractor.class, "Islands in the Sun PDF", true),
    QUANTASVACATION(com.mapped.publisher.parse.extractor.booking.Travel2Extractor.class, "Qantas Vacations PDF", true),
    AEROPLAN_PDF(com.mapped.publisher.parse.extractor.booking.AeroplanExtractor.class, "Aeroplan Invoice", true),
    CALYPSO(com.mapped.publisher.parse.extractor.booking.CalypsoExtractor.class, "Calypso Invoice", true),
    AUTOEUROPE(com.mapped.publisher.parse.extractor.booking.AutoEuropeExtractor.class, "Auto Europe Invoice", true),
    BIG_FIVE(com.mapped.publisher.parse.extractor.booking.BigFiveTourExtractor.class, "Big Five Custom Itinerary", false),
    CLIENT_BASE_DDMMYYY(com.mapped.publisher.parse.extractor.booking.ClientbaseExtractorCdnDate.class, "Clientbase DDMMYYY format", false),
    OPENSKIES(com.mapped.publisher.parse.extractor.booking.OpenSkiesExtractor.class, "Open Skies Invoice", false);

    ;


    /**
     * Class to instantiate the extractor
     */
    private Class<? extends BookingExtractor> extractor;

    /**
     * Means access to this extractor is open to all companies
     */
    private boolean open;

    private String description;

    Parsers(Class<? extends BookingExtractor> extractor, String desc, boolean open) {
      this.extractor = extractor;
      this.open = open;
      this.description = desc;
    }

    public BookingExtractor makeExtractor() {
      try {
        return extractor.newInstance();
      }
      catch (Exception e) {
        return null;
      }
    }

    public static Parsers fromClass(String classPath) {
      for (Parsers p : values()) {
        if (p.extractor.getCanonicalName().equals(classPath)) {
          return p;
        }
      }
      return null;
    }

    public boolean isOpen() {
      return this.open;
    }

    public String getDescription() {
      return this.description;
    }

    public String getExtractorName() {
      return this.extractor.getCanonicalName();
    }
  }

  public void init(Map<String, String> params) {
    this.initImpl(params);
  }

  protected abstract void initImpl(Map<String, String> params);

  public TripVO extractData(InputStream input)
      throws Exception {
    try {
      return this.extractDataImpl(input);
    }
    finally {
      if (input != null) {
        input.close();
      }
    }
  }

  protected abstract TripVO extractDataImpl(InputStream input)
      throws Exception;

  public void postProcessing(String tripId, String userId) {
    try {
      if (tripId != null) {
        postProcessingImpl(tripId, userId);
      }
    } catch (Exception e) {

    }
  }

  protected abstract void postProcessingImpl(String tripId, String userId);


  /**
   * Access to the list of parse errors.
   *
   * @return list of parse errors, null if class does not support error reporting
   * @note must be called only after calling @see extractData() method
   */
  abstract public ArrayList<ParseError> getParseErrors();

  public String getCmpyId() {
    return cmpyId;
  }

  public void setCmpyId(String cmpyId) {
    this.cmpyId = cmpyId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }
}
