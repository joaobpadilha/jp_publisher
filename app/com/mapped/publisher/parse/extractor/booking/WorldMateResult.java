package com.mapped.publisher.parse.extractor.booking;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.extractor.booking.email.ReservationsHolder;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.mapped.publisher.parse.schemaorg.*;
import com.mapped.publisher.parse.schemaorg.Reservation;
import com.mapped.publisher.parse.worldmate.*;
import com.mapped.publisher.parse.worldmate.Event;
import com.mapped.publisher.parse.worldmate.Flight;
import com.mapped.publisher.persistence.bookingapi.BookingRS;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.enums.ReservationType;
import controllers.ApiBookingController;
import models.publisher.BkApiSrc;

import javax.xml.datatype.XMLGregorianCalendar;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

/**
 * Helper class to provide convenience functionality to WorldMate Parsed Data
 * Created by surge on 2014-08-04.
 */
public class WorldMateResult
    extends WorldmateParsingResult {

  long startTimestamp =0;
  long endTimestamp =0;

  public enum ReturnCode {
    SUCCESS(true),
    PARTIAL_SUCCESS(true),
    UNSUPPORTED(false),
    NO_DATA(false),
    INSUFFICIENT_DATA(false),
    NO_ITEMS(false),
    EXTERNAL_ERROR(false),
    INTERNAL_ERROR(false),
    UNRECOGNIZED_FORMAT(false),
    REPEATED_FAILURE(false),
    ACCOUNT_SUSPENDED(false),
    QUOTA_EXCEDEED(false);

    private boolean success;

    ReturnCode(boolean success) {
      this.success = success;
    }

    public boolean isSuccess() {
      return this.success;
    }

  }

  public ReturnCode getStatusCode() {
    return statusCode;
  }

  private ReturnCode statusCode = ReturnCode.NO_DATA;

  public WorldMateResult(WorldmateParsingResult result) {
    if (result != null) {
      status = result.getStatus();
      statusCode = ReturnCode.valueOf(status);
      errorMsg = result.getErrorMsg();
      headers = result.getHeaders();
      endUserEmails = result.getEndUserEmails();
      reservationDetails = result.getReservationDetails();
      items = result.getItems();
      received = result.getReceived();
      requestId = result.getRequestId();
    }
  }

  public TripVO toTripVO() {
    TripVO tvo = new TripVO();

    ReturnCode rc = ReturnCode.valueOf(status);

    //If not success just return empty object
    if (!rc.isSuccess()) {
      return null;
    }

    for (ItineraryItem item : items.getFlightOrCarRentalOrHotelReservation()) {

      Class<?> reservationClass = item.getClass();
      String reservationTypeName = reservationClass.getSimpleName();

      if (reservationTypeName.equals("HotelReservation")) {
        addHotel(tvo, (HotelReservation) item);
      }
      else if (reservationTypeName.equals("Flight")) {
        addFlight(tvo, (Flight) item);
      }
      else if (reservationTypeName.equals("CarRental")) {
        addCar(tvo, (CarRental) item);
      }
      else if (reservationTypeName.equals("Event")) {
        addEvent(tvo, (Event) item);
      }
      else if (reservationTypeName.equals("Meeting")) {
        addMeeting(tvo, (Meeting) item);
      }
      else if (reservationTypeName.equals("PublicTransportation")) {
        addTransport(tvo, (PublicTransportation) item);
      }
    }
    tvo.setImportSrc(BookingSrc.ImportSrc.WORLDMATE);
    tvo.setImportTs(System.currentTimeMillis());
    return tvo;
  }

  private void addHotel(TripVO tvo, HotelReservation hr) {
    HotelVO hvo = new HotelVO();

    hvo.setHotelName(hr.getHotelName());
    hvo.setCheckin(new Timestamp(hr.getCheckIn().toGregorianCalendar().getTimeInMillis()));
    hvo.setCheckout(new Timestamp(hr.getCheckOut().toGregorianCalendar().getTimeInMillis()));
    hvo.setConfirmation(hr.getBookingDetails().getConfirmationNumber());
    hvo.setNote(hr.getNotes());

    tvo.addHotel(hvo);
  }

  private void addFlight(TripVO tvo, Flight fr) {
    FlightVO fvo = new FlightVO();

    fvo.setName(fr.getProviderDetails().getName());

    fvo.setReservationNumber(fr.getProviderDetails().getConfirmationNumber());

    fvo.setCode(fr.getDetails().getAirlineCode());
    fvo.setNumber(Integer.toString(fr.getDetails().getNumber()));

    fvo.setNotes(fr.getNotes());

    /* Departure airport */
    AirportVO davo = new AirportVO();
    davo.setCity(fr.getDeparture().getName());
    davo.setCode(fr.getDeparture().getAirportCode());
    davo.setTerminal(fr.getDeparture().getTerminal());

    /* Arrival airport */
    AirportVO aavo = new AirportVO();
    aavo.setCity(fr.getArrival().getName());
    aavo.setCode(fr.getArrival().getAirportCode());
    aavo.setTerminal(fr.getArrival().getTerminal());

    fvo.setDepartureAirport(davo);
    fvo.setArrivalAirport(aavo);

    fvo.setDepatureTime(new Timestamp(fr.getDeparture().getLocalDateTime().toGregorianCalendar().getTimeInMillis()));
    fvo.setArrivalTime(new Timestamp(fr.getArrival().getLocalDateTime().toGregorianCalendar().getTimeInMillis()));

    for (Traveler t : fr.getTraveler()) {
      PassengerVO pvo = new PassengerVO();
      pvo.setFirstName(t.getFirstName());
      pvo.setLastName(t.getLastName());
      pvo.seteTicketNumber(t.getETicket());

      StringBuilder sb = new StringBuilder();
      if (t.getSeat() != null && t.getSeat().length() > 0) {
        pvo.setSeat(t.getSeat());
      }
      if (t.getMeal() != null && t.getMeal().length() > 0) {
        pvo.setMeal(t.getMeal());
      }
      if (t.getCabin() != null && t.getCabin().length() > 0) {
        pvo.setSeatClass(t.getCabin());
      }

      pvo.setNote(sb.toString());

      fvo.addPassenger(pvo);
    }

    tvo.addFlight(fvo);
  }

  private void addCar(TripVO tvo, CarRental cr) {
    TransportVO tro = new TransportVO();

    tro.setCmpyName(cr.getBookingDetails().getName());

    if (tro.getCmpyName() == null) {
      tro.setCmpyName(reservationDetails.getBookingDetails().getName());
    }

    tro.setConfirmationNumber(cr.getBookingDetails().getConfirmationNumber());


    tro.setPickupLocation(((cr.getPickup().getAirportCode() != null)?cr.getPickup().getAirportCode() + " ":"") +
                          ((cr.getPickup().getAddress().getCity() != null)?cr.getPickup().getAddress().getCity() + " ":"") +
                          ((cr.getPickup().getAddress().getStreet() != null)?cr.getPickup().getAddress().getStreet():""));

    tro.setPickupDate(new Timestamp(cr.getPickup().getLocalDateTime().toGregorianCalendar().getTimeInMillis()));

    tro.setDropoffLocation(((cr.getDropoff().getAirportCode() != null)?cr.getDropoff().getAirportCode() + " ":"") +
                           ((cr.getDropoff().getAddress().getCity() != null)?cr.getDropoff().getAddress().getCity() + " ":"") +
                           ((cr.getDropoff().getAddress().getStreet() != null)?cr.getDropoff().getAddress().getStreet():""));

    tro.setDropoffDate(new Timestamp(cr.getDropoff().getLocalDateTime().toGregorianCalendar().getTimeInMillis()));
    tro.setNote(cr.getNotes());

    tvo.addTransport(tro);
  }

  private void addEvent(TripVO tvo, Event er) {
    ActivityVO avo = new ActivityVO();

    avo.setName(er.getEventName());
    if (avo.getName() == null) {
      avo.setName(reservationDetails.getBookingDetails().getName());
    }

    avo.setConfirmation(er.getBookingDetails().getConfirmationNumber());
    avo.setContact(er.getLocation());
    avo.setStartDate(new Timestamp(er.getLocalStartTime().toGregorianCalendar().getTimeInMillis()));
    avo.setEndDate(new Timestamp(er.getLocalEndTime().toGregorianCalendar().getTimeInMillis()));
    avo.setNote(er.getNotes());

    tvo.addActivity(avo);
  }

  private void addMeeting(TripVO tvo, Meeting mr) {
    ActivityVO avo = new ActivityVO();

    avo.setName(mr.getName());

    if (avo.getName() == null) {
      avo.setName(reservationDetails.getBookingDetails().getName());
    }

    avo.setContact(mr.getLocation());
    avo.setStartDate(new Timestamp(mr.getLocalStartTime().toGregorianCalendar().getTimeInMillis()));
    avo.setEndDate(new Timestamp(mr.getLocalEndTime().toGregorianCalendar().getTimeInMillis()));
    avo.setNote(mr.getNotes());

    tvo.addActivity(avo);
  }

  private void addTransport(TripVO tvo, PublicTransportation pr) {
    TransportVO tro = new TransportVO();

    if (pr.getBookingDetails().getName() != null) {
      tro.setCmpyName(pr.getBookingDetails().getName());
    }
    else if (reservationDetails.getBookingDetails().getName() != null) {
      tro.setCmpyName(reservationDetails.getBookingDetails().getName());
    }

    tro.setConfirmationNumber(pr.getBookingDetails().getConfirmationNumber());
    tro.setPickupLocation(pr.getDeparture().getStationName());
    tro.setPickupDate(new Timestamp(pr.getDeparture().getLocalDateTime().toGregorianCalendar().getTimeInMillis()));
    tro.setDropoffLocation(pr.getArrival().getStationName());
    tro.setDropoffDate(new Timestamp(pr.getArrival().getLocalDateTime().toGregorianCalendar().getTimeInMillis()));
    tro.setNote(pr.getNotes());

    tvo.addTransport(tro);
  }


  public ReservationPackage toReservationPackage(String userId, String cmpyId, String tripName) {
    ReservationPackage rp = new ReservationPackage();

    ReturnCode rc = ReturnCode.valueOf(status);

    //If not success just return empty object
    if (!rc.isSuccess()) {
      return null;
    }

    rp.reservationNumber = Utils.shortenNumber(DBConnectionMgr.getUniqueKey());
    rp.bookingAgent = new Organization();
    rp.bookingAgent.umId = userId;
    rp.umId = "EMAIL";
    rp.name = tripName;

    for (ItineraryItem item : items.getFlightOrCarRentalOrHotelReservation()) {

      Class<?> reservationClass = item.getClass();
      String reservationTypeName = reservationClass.getSimpleName();

      if (reservationTypeName.equals("HotelReservation")) {
        rp.reservation.add(getHotel((HotelReservation) item));
      }
      else if (reservationTypeName.equals("Flight")) {
        rp.reservation.addAll(getFlight((Flight) item));
      }
      else if (reservationTypeName.equals("CarRental")) {
        rp.reservation.add(getCar((CarRental) item));
      }
      else if (reservationTypeName.equals("Event")) {
        rp.reservation.add(getEvent((Event) item));
      }
      else if (reservationTypeName.equals("Meeting")) {
        rp.reservation.add(getMeeting((Meeting) item));
      }
      else if (reservationTypeName.equals("PublicTransportation")) {
        PublicTransportation p = (PublicTransportation) item;
        if (p.getTrainNumber() != null && !p.getTrainNumber().isEmpty()) {
          rp.reservation.add(getRail((PublicTransportation) item));
        } else {
          rp.reservation.add(getTransport((PublicTransportation) item));
        }
      }
    }
    return rp;
  }

  private LodgingReservation getHotel(HotelReservation hr) {
    LodgingReservation h = new LodgingReservation();
    h.reservationId = DBConnectionMgr.getUniqueId();
    h.reservationStatus = "Confirmed";
    h.reservationFor = new LodgingBusiness();
    h.reservationFor.name = hr.getHotelName();
    if (hr.getAddress() != null) {
      h.reservationFor.address = new PostalAddress();
      h.reservationFor.address.streetAddress = hr.getAddress().getStreet();
      h.reservationFor.address.addressLocality = hr.getAddress().getCity();
      h.reservationFor.address.addressRegion = hr.getAddress().getStateCode();
      h.reservationFor.address.postalCode = hr.getAddress().getPostalCode();
      h.reservationFor.address.addressCountry = hr.getAddress().getCountryName();
    }
    h.reservationFor.telephone = hr.getPhone();
    h.reservationFor.faxNumber = hr.getFax();



    h.checkinDate = getDateTime(hr.getCheckIn());
    h.checkoutDate = getDateTime(hr.getCheckOut());

    h.reservationNumber = hr.getBookingDetails().getConfirmationNumber();

    h.lodgingUnitDescription = hr.getRoom();

    StringBuilder sb = new StringBuilder();
    if (hr.getNotes() != null && !hr.getNotes().isEmpty()) {
      sb.append(hr.getNotes());
      sb.append("\n");

    }

    if (hr.getRoom() != null && !hr.getRoom().isEmpty()) {
      h.lodgingUnitDescription= hr.getRoom();
      sb.append(hr.getRoom());
      sb.append("\n");

    }
    if (hr.getTotalPrice() != null) {
      String cur = "";
      if (hr.getTotalPrice().getCurrencyCode() != null) {
        cur = hr.getTotalPrice().getCurrencyCode();
        h.priceCurrency =hr.getTotalPrice().getCurrencyCode();
      }
      if (hr.getTotalPrice().getDailyCost() != null) {
        sb.append("\n\nDaily Rate: ");
        sb.append(hr.getTotalPrice().getDailyCost().toString());
        sb.append(" ");
        sb.append(cur);
        sb.append("\n");
        h.price = hr.getTotalPrice().getDailyCost().toString();
      }
      if (hr.getTotalPrice().getTax() != null) {
        sb.append("Tax: ");
        sb.append(hr.getTotalPrice().getTax().toString());
        sb.append("\n");
      }

      if (hr.getTotalPrice().getTotalCost() != null) {
        sb.append("Total: ");
        sb.append(hr.getTotalPrice().getTotalCost().toString());
        sb.append(" ");
        sb.append(cur);
        sb.append("\n");
      }

    }
    h.description = sb.toString();

    return h;
  }

  private FlightReservation getFlightReservation( Flight fr) {
    FlightReservation f = new FlightReservation();
    f.reservationId = DBConnectionMgr.getUniqueId();
    f.reservationNumber = fr.getProviderDetails().getConfirmationNumber();
    f.reservationStatus = "Confirmed";
    f.reservationFor = new com.mapped.publisher.parse.schemaorg.Flight();
    f.reservationFor.airline = new Airline();
    f.reservationFor.airline.name = fr.getProviderDetails().getName();
    f.reservationFor.airline.iataCode = fr.getDetails().getAirlineCode();
    f.reservationFor.flightNumber = String.valueOf(fr.getDetails().getNumber());
    f.description = fr.getNotes();

    f.reservationFor.arrivalAirport = new Airport();
    f.reservationFor.arrivalAirport.iataCode = fr.getArrival().getAirportCode();
    f.reservationFor.arrivalAirport.name =fr.getArrival().getName();
    f.reservationFor.arrivalTerminal = fr.getArrival().getTerminal();

    f.reservationFor.departureAirport = new Airport();
    f.reservationFor.departureAirport.iataCode = fr.getDeparture().getAirportCode();
    f.reservationFor.departureAirport.name =fr.getDeparture().getName();
    f.reservationFor.departureTerminal = fr.getDeparture().getTerminal();

    f.reservationFor.arrivalTime = getDateTime(fr.getArrival().getLocalDateTime());
    f.reservationFor.departureTime = getDateTime(fr.getDeparture().getLocalDateTime());

    if (fr.getAircraft() != null) {
      f.reservationFor.aircraft = new Thing();
      f.reservationFor.aircraft.name = fr.getAircraft().getName();
    }

    return f;
  }

  private List<FlightReservation> getFlight( Flight fr) {
    List<FlightReservation> flights = new ArrayList<>();
    if (fr.getTraveler() != null && fr.getTraveler().isEmpty()) {
      flights.add(getFlightReservation(fr));
    } else {
      for (Traveler t: fr.getTraveler()) {
        FlightReservation f = getFlightReservation(fr);
        f.underName = new Person();
        f.underName.givenName = t.getFirstName();
        f.underName.familyName = t.getLastName();
        f.airplaneSeat = t.getSeat();
        f.ticketNumber = t.getETicket();
        f.airplaneSeatClass = new AirplaneSeatClass();
        f.airplaneSeatClass.name = t.getCabin();
        flights.add(f);
      }
    }
    return flights;
  }

  private RentalCarReservation getCar(CarRental cr) {
    RentalCarReservation c = new RentalCarReservation();
    c.reservationId = DBConnectionMgr.getUniqueId();
    c.reservationNumber = cr.getBookingDetails().getConfirmationNumber();
    c.reservationStatus="Confirmed";

    c.pickupTime = getDateTime(cr.getPickup().getLocalDateTime());
    c.pickupLocation = new Place();
    c.pickupLocation.name = cr.getPickup().getAirportCode();
    if (cr.getPickup().getAddress() != null ) {
      c.pickupLocation.address = new PostalAddress();
      c.pickupLocation.address.streetAddress = cr.getPickup().getAddress().getStreet();
      c.pickupLocation.address.addressLocality = cr.getPickup().getAddress().getCity();
      c.pickupLocation.address.addressRegion = cr.getPickup().getAddress().getStateCode();
      c.pickupLocation.address.postalCode = cr.getPickup().getAddress().getPostalCode();
      c.pickupLocation.address.addressCountry = cr.getPickup().getAddress().getCountryName();
    }
    c.pickupLocation.telephone = cr.getPickup().getPhone();

    c.dropoffTime = getDateTime(cr.getDropoff().getLocalDateTime());
    c.dropoffLocation = new Place();
    c.dropoffLocation.name = cr.getDropoff().getAirportCode();
    if (cr.getDropoff().getAddress() != null ) {
      c.dropoffLocation.address = new PostalAddress();
      c.dropoffLocation.address.streetAddress = cr.getDropoff().getAddress().getStreet();
      c.dropoffLocation.address.addressLocality = cr.getDropoff().getAddress().getCity();
      c.dropoffLocation.address.addressRegion = cr.getDropoff().getAddress().getStateCode();
      c.dropoffLocation.address.postalCode = cr.getDropoff().getAddress().getPostalCode();
      c.dropoffLocation.address.addressCountry = cr.getDropoff().getAddress().getCountryName();
    }
    c.dropoffLocation.telephone = cr.getDropoff().getPhone();

    c.reservationFor = new RentalCar();
    if (cr.getCarType() != null) {
      c.reservationFor.model = cr.getCarType().getName();
    }
    c.reservationFor.rentalCompany = new Organization();
    c.reservationFor.rentalCompany.name = cr.getBookingDetails().getName();

    StringBuilder sb = new StringBuilder();
    if (cr.getNotes() != null && !cr.getNotes().isEmpty()) {
      sb.append(cr.getNotes());
      sb.append("\n");
    }
    if (cr.getTotalPrice() != null) {
      String cur = "";
      if (cr.getTotalPrice().getCurrencyCode() != null) {
        cur = cr.getTotalPrice().getCurrencyCode();
        c.priceCurrency =cr.getTotalPrice().getCurrencyCode();
      }
      if (cr.getTotalPrice().getDailyCost() != null) {
        sb.append("\n\nDaily Rate: ");
        sb.append(cr.getTotalPrice().getDailyCost().toString());
        sb.append(" ");
        sb.append(cur);
        sb.append("\n");
        c.price = cr.getTotalPrice().getDailyCost().toString();
      }
      if (cr.getTotalPrice().getTax() != null) {
        sb.append("Tax: ");
        sb.append(cr.getTotalPrice().getTax().toString());
        sb.append("\n");
      }

      if (cr.getTotalPrice().getTotalCost() != null) {
        sb.append("Total: ");
        sb.append(cr.getTotalPrice().getTotalCost().toString());
        sb.append(" ");
        sb.append(cur);
        sb.append("\n");
      }
    }

    return c ;
  }

  private ActivityReservation getEvent(Event er) {
    ActivityReservation a = new ActivityReservation();
    a.reservationId = DBConnectionMgr.getUniqueId();
    a.reservationStatus = "Confirmed";
    a.reservationNumber = er.getBookingDetails().getConfirmationNumber();
    a.name = er.getEventName();

    a.startLocation = new Place();
    a.startLocation.name = er.getLocation();
    a.startLocation.telephone = er.getPhone();
    a.startLocation.faxNumber = er.getFax();

    if (er.getAddress() != null) {
      a.startLocation.address = new PostalAddress();
      a.startLocation.address.streetAddress = er.getAddress().getStreet();
      a.startLocation.address.addressLocality = er.getAddress().getCity();
      a.startLocation.address.addressRegion = er.getAddress().getStateCode();
      a.startLocation.address.postalCode = er.getAddress().getPostalCode();
      a.startLocation.address.addressCountry = er.getAddress().getCountryName();
    }

    a.startTime = getDateTime(er.getLocalStartTime());
    a.finishTime = getDateTime(er.getLocalEndTime());

    a.description = er.getNotes();
    return a;
  }

  private ActivityReservation getMeeting(Meeting mr) {
    ActivityReservation a = new ActivityReservation();
    a.reservationId = DBConnectionMgr.getUniqueId();
    a.reservationStatus = "Confirmed";
    a.name = mr.getName();

    a.startLocation = new Place();
    a.startLocation.name = mr.getLocation();
    a.startLocation.telephone = mr.getPhone();
    a.startLocation.faxNumber = mr.getFax();

    if (mr.getAddress() != null) {
      a.startLocation.address = new PostalAddress();
      a.startLocation.address.streetAddress = mr.getAddress().getStreet();
      a.startLocation.address.addressLocality = mr.getAddress().getCity();
      a.startLocation.address.addressRegion = mr.getAddress().getStateCode();
      a.startLocation.address.postalCode = mr.getAddress().getPostalCode();
      a.startLocation.address.addressCountry = mr.getAddress().getCountryName();
    }

    a.startTime = getDateTime(mr.getLocalStartTime());
    a.finishTime = getDateTime(mr.getLocalEndTime());

    a.description = mr.getNotes();
    return a;
  }

  private TransferReservation getTransport(PublicTransportation pr) {
    TransferReservation t = new TransferReservation();
    t.reservationId = DBConnectionMgr.getUniqueId();
    t.reservationStatus = "Confirmed";
    t.name = pr.getBookingDetails().getName();
    t.reservationNumber =pr.getBookingDetails().getConfirmationNumber();

    t.reservationFor = new Transfer();
    if (pr.getDeparture() != null) {
      t.reservationFor.pickupTime = getDateTime(pr.getDeparture().getLocalDateTime());
      t.reservationFor.pickupLocation = new Place();
      t.reservationFor.pickupLocation.name = pr.getDeparture().getStationName();
      if (pr.getDeparture().getAddress() != null) {
        PublicTransportationLocaion mr = pr.getDeparture();
        t.reservationFor.pickupLocation.address = new PostalAddress();
        t.reservationFor.pickupLocation.address.streetAddress = mr.getAddress().getStreet();
        t.reservationFor.pickupLocation.address.addressLocality = mr.getAddress().getCity();
        t.reservationFor.pickupLocation.address.addressRegion = mr.getAddress().getStateCode();
        t.reservationFor.pickupLocation.address.postalCode = mr.getAddress().getPostalCode();
        t.reservationFor.pickupLocation.address.addressCountry = mr.getAddress().getCountryName();
      }
    }

    if (pr.getArrival() != null) {
      t.reservationFor.dropoffTime = getDateTime(pr.getArrival().getLocalDateTime());
      t.reservationFor.dropoffLocation = new Place();
      t.reservationFor.dropoffLocation.name = pr.getArrival().getStationName();
      if (pr.getArrival().getAddress() != null) {
        PublicTransportationLocaion mr = pr.getArrival();
        t.reservationFor.dropoffLocation.address = new PostalAddress();
        t.reservationFor.dropoffLocation.address.streetAddress = mr.getAddress().getStreet();
        t.reservationFor.dropoffLocation.address.addressLocality = mr.getAddress().getCity();
        t.reservationFor.dropoffLocation.address.addressRegion = mr.getAddress().getStateCode();
        t.reservationFor.dropoffLocation.address.postalCode = mr.getAddress().getPostalCode();
        t.reservationFor.dropoffLocation.address.addressCountry = mr.getAddress().getCountryName();
      }
    }

    if (pr.getArrival() != null) {
      t.reservationFor.dropoffTime = getDateTime(pr.getArrival().getLocalDateTime());
    }

    t.description = pr.getNotes();

    return t;
  }

  private TrainReservation getRail(PublicTransportation pr) {
    TrainReservation t = new TrainReservation();
    t.reservationId = DBConnectionMgr.getUniqueId();
    t.reservationStatus = "Confirmed";
    t.name = pr.getBookingDetails().getName();
    t.reservationNumber =pr.getBookingDetails().getConfirmationNumber();

    t.reservationFor = new TrainTrip();
    t.reservationFor.trainName = pr.getBookingDetails().getName();
    t.reservationFor.trainNumber = pr.getTrainNumber();
    if (pr.getDeparture() != null) {
      t.reservationFor.departTime = getDateTime(pr.getDeparture().getLocalDateTime());
      t.reservationFor.departStation = new Place();
      t.reservationFor.departStation.name = pr.getDeparture().getStationName();
      t.reservationFor.departPlatform = pr.getDeparture().getPlatform();
      if (pr.getDeparture().getAddress() != null) {
        PublicTransportationLocaion mr = pr.getDeparture();
        t.reservationFor.departStation.address = new PostalAddress();
        t.reservationFor.departStation.address.streetAddress = mr.getAddress().getStreet();
        t.reservationFor.departStation.address.addressLocality = mr.getAddress().getCity();
        t.reservationFor.departStation.address.addressRegion = mr.getAddress().getStateCode();
        t.reservationFor.departStation.address.postalCode = mr.getAddress().getPostalCode();
        t.reservationFor.departStation.address.addressCountry = mr.getAddress().getCountryName();
      }
    }

    if (pr.getArrival() != null) {
      t.reservationFor.arrivalTime = getDateTime(pr.getArrival().getLocalDateTime());
      t.reservationFor.arrivalStation = new Place();
      t.reservationFor.arrivalStation.name = pr.getArrival().getStationName();
      t.reservationFor.arrivalPlatform = pr.getArrival().getPlatform();
      if (pr.getArrival().getAddress() != null) {
        PublicTransportationLocaion mr = pr.getArrival();
        t.reservationFor.arrivalStation.address = new PostalAddress();
        t.reservationFor.arrivalStation.address.streetAddress = mr.getAddress().getStreet();
        t.reservationFor.arrivalStation.address.addressLocality = mr.getAddress().getCity();
        t.reservationFor.arrivalStation.address.addressRegion = mr.getAddress().getStateCode();
        t.reservationFor.arrivalStation.address.postalCode = mr.getAddress().getPostalCode();
        t.reservationFor.arrivalStation.address.addressCountry = mr.getAddress().getCountryName();
      }
    }

    t.description = pr.getNotes();

    return t;
  }


  public DateTime getDateTime (XMLGregorianCalendar xml) {
    if (xml != null) {
      long t = xml.toGregorianCalendar().getTimeInMillis();
      if (startTimestamp == 0 || t < startTimestamp) {
        startTimestamp = t;
      }

      if (endTimestamp == 0 || t > endTimestamp) {
        endTimestamp = t;
      }

      String s = xml.toString();
      s = s .replace(".000","");
      if (s.contains("T") && s.contains("Z")) {
        return new DateTime(s);
      } else if (s.contains("Z")) {
        return new DateTime(s.replace("Z","T00:00:00Z"));
      }
    }
    return null;
  }



  public List<BookingRS> getBookingRS(String userId, String cmpyId, String tripName) {
    ReservationPackage reservationPackage = toReservationPackage(userId, cmpyId, tripName);
    ReservationsHolder rh = new ReservationsHolder();
    List<BookingRS> bookings = new ArrayList<>();
    ObjectMapper om = new ObjectMapper();
    int srcId = BkApiSrc.getId("EMAIL");
    BookingRS rp = ApiBookingController.getPackageBookingRS(srcId,
            cmpyId,
            userId,
            cmpyId,
            userId,
            reservationPackage,
            startTimestamp,
            endTimestamp);
    try {
      rp.setData(om.writeValueAsString(reservationPackage));
      rp.setHash(Utils.hash(rp.getData()));
      bookings.add(rp);
    } catch (Exception e) {
      e.printStackTrace();
    }

    for (Reservation r: reservationPackage.reservation) {
      try {
        BookingRS rs = ApiBookingController.getBaseBookingRS(BkApiSrc.getId("EMAIL"),
                cmpyId,
                userId,
                cmpyId,
                userId,
                reservationPackage.reservationNumber,
                r.reservationStatus);
        rs.setSrcPk(r.reservationId);
        rs.setHash(Utils.hash(rs.getData()));


        if (r instanceof com.mapped.publisher.parse.schemaorg.FlightReservation) {
          com.mapped.publisher.parse.schemaorg.FlightReservation f = (com.mapped.publisher.parse.schemaorg.FlightReservation) r;
          if (f.reservationFor != null && f.reservationFor.arrivalTime != null && f.reservationFor.arrivalTime.getTimestamp() != null) {
            rs.setEndTS(f.reservationFor.arrivalTime.getTimestamp().getTime());
          }
          if (f.reservationFor != null && f.reservationFor.departureTime != null && f.reservationFor.departureTime.getTimestamp() != null) {
            rs.setStartTS(f.reservationFor.departureTime.getTimestamp().getTime());
          }
          rs.setSearchWords(rh.getKeywords(f));
          rs.setBookingType(ReservationType.FLIGHT.ordinal());
          rs.setData(om.writeValueAsString(f));
          rs.setMainTraveler(ApiBookingController.getMainTraveler(f));
        } else if (r instanceof com.mapped.publisher.parse.schemaorg.LodgingReservation) {
          LodgingReservation f = (LodgingReservation) r;
          if (f.reservationFor != null && f.checkoutDate != null &&  f.checkoutDate.getTimestamp() != null) {
            rs.setEndTS(f.checkoutDate.getTimestamp().getTime());
          }
          if (f.reservationFor != null && f.checkinDate != null && f.checkinDate.getTimestamp() != null) {
            rs.setStartTS(f.checkinDate.getTimestamp().getTime());
          }
          rs.setSearchWords(rh.getKeywords(f));
          rs.setBookingType(ReservationType.HOTEL.ordinal());
          rs.setData(om.writeValueAsString(f));
          rs.setMainTraveler(ApiBookingController.getMainTraveler(f));
        } else if (r instanceof com.mapped.publisher.parse.schemaorg.RentalCarReservation) {
          RentalCarReservation f = (RentalCarReservation) r;
          if (f.dropoffTime != null && f.dropoffTime.getTimestamp() != null) {
            rs.setEndTS(f.dropoffTime.getTimestamp().getTime());
          }
          if (f.pickupTime != null && f.pickupTime.getTimestamp() != null) {
            rs.setStartTS(f.pickupTime.getTimestamp().getTime());
          }
          rs.setSearchWords(rh.getKeywords(f));
          rs.setBookingType(ReservationType.CAR_RENTAL.ordinal());
          rs.setData(om.writeValueAsString(f));
          rs.setMainTraveler(ApiBookingController.getMainTraveler(f));
        } else  if (r instanceof com.mapped.publisher.parse.schemaorg.TrainReservation) {
          TrainReservation f = (TrainReservation) r;
          if (f.reservationFor != null && f.reservationFor.arrivalTime != null && f.reservationFor.arrivalTime.getTimestamp() != null) {
            rs.setEndTS(f.reservationFor.arrivalTime.getTimestamp().getTime());
          }
          if (f.reservationFor != null && f.reservationFor.departTime != null && f.reservationFor.departTime.getTimestamp() != null) {
            rs.setStartTS(f.reservationFor.departTime.getTimestamp().getTime());
          }
          rs.setSearchWords(rh.getKeywords(f));
          rs.setBookingType(ReservationType.RAIL.ordinal());
          rs.setData(om.writeValueAsString(f));
          rs.setMainTraveler(ApiBookingController.getMainTraveler(f));
        } else if (r instanceof com.mapped.publisher.parse.schemaorg.TransferReservation) {
          TransferReservation f = (TransferReservation) r;
          if (f.reservationFor != null && f.reservationFor.dropoffTime != null) {
            rs.setEndTS(f.reservationFor.dropoffTime.getTimestamp().getTime());
          }
          if (f.reservationFor != null && f.reservationFor.pickupTime != null) {
            rs.setStartTS(f.reservationFor.pickupTime.getTimestamp().getTime());
          }
          rs.setSearchWords(rh.getKeywords(f));
          rs.setBookingType(ReservationType.TRANSPORT.ordinal());
          rs.setData(om.writeValueAsString(f));
          rs.setMainTraveler(ApiBookingController.getMainTraveler(f));
        } else if (r instanceof com.mapped.publisher.parse.schemaorg.ActivityReservation) {
          ActivityReservation f = (ActivityReservation) r;
          if (f.finishTime != null) {
            rs.setEndTS(f.finishTime.getTimestamp().getTime());
          }
          if (f.startTime != null) {
            rs.setStartTS(f.startTime.getTimestamp().getTime());
          }
          if (f.finishTime != null) {
            rs.setEndTS(f.finishTime.getTimestamp().getTime());
          }
          if (f.startTime != null) {
            rs.setStartTS(f.startTime.getTimestamp().getTime());
          }
          rs.setSearchWords(rh.getKeywords(f));
          rs.setBookingType(ReservationType.ACTIVITY.ordinal());
          rs.setSrcPk(f.reservationId);
          rs.setData(om.writeValueAsString(f));
          rs.setMainTraveler(ApiBookingController.getMainTraveler(f));
        }
        if (rs.searchWords != null) {
          rs.setSearchWords(rs.getSearchWords() + " " + tripName);
        }



        bookings.add(rs);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    return bookings;
  }

}
