
package com.mapped.publisher.parse.iceportal;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mapped.publisher.parse.iceportal package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SearchBrochureInfo_QNAME = new QName("http://services.iceportal.com/service", "SearchBrochureInfo");
    private final static QName _ICEAuthHeader_QNAME = new QName("http://services.iceportal.com/service", "ICEAuthHeader");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mapped.publisher.parse.iceportal
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetStatesResponse }
     * 
     */
    public GetStatesResponse createGetStatesResponse() {
        return new GetStatesResponse();
    }

    /**
     * Create an instance of {@link BrochureListItem }
     * 
     */
    public BrochureListItem createBrochureListItem() {
        return new BrochureListItem();
    }

    /**
     * Create an instance of {@link GetBrochureStatusResponse }
     * 
     */
    public GetBrochureStatusResponse createGetBrochureStatusResponse() {
        return new GetBrochureStatusResponse();
    }

    /**
     * Create an instance of {@link GetRoomTypesForImageResponse }
     * 
     */
    public GetRoomTypesForImageResponse createGetRoomTypesForImageResponse() {
        return new GetRoomTypesForImageResponse();
    }

    /**
     * Create an instance of {@link RoomTypeCodes }
     * 
     */
    public RoomTypeCodes createRoomTypeCodes() {
        return new RoomTypeCodes();
    }

    /**
     * Create an instance of {@link GetMappingInfo }
     * 
     */
    public GetMappingInfo createGetMappingInfo() {
        return new GetMappingInfo();
    }

    /**
     * Create an instance of {@link GetNewBrochures }
     * 
     */
    public GetNewBrochures createGetNewBrochures() {
        return new GetNewBrochures();
    }

    /**
     * Create an instance of {@link GetBrochure }
     * 
     */
    public GetBrochure createGetBrochure() {
        return new GetBrochure();
    }

    /**
     * Create an instance of {@link GetBrochureV1 }
     * 
     */
    public GetBrochureV1 createGetBrochureV1() {
        return new GetBrochureV1();
    }

    /**
     * Create an instance of {@link CreateProperty }
     * 
     */
    public CreateProperty createCreateProperty() {
        return new CreateProperty();
    }

    /**
     * Create an instance of {@link Property }
     * 
     */
    public Property createProperty() {
        return new Property();
    }

    /**
     * Create an instance of {@link GetNewBrochureIDsSinceResponse }
     * 
     */
    public GetNewBrochureIDsSinceResponse createGetNewBrochureIDsSinceResponse() {
        return new GetNewBrochureIDsSinceResponse();
    }

    /**
     * Create an instance of {@link ArrayOfBrochureInfos }
     * 
     */
    public ArrayOfBrochureInfos createArrayOfBrochureInfos() {
        return new ArrayOfBrochureInfos();
    }

    /**
     * Create an instance of {@link CreateSupplierAccountResponse }
     * 
     */
    public CreateSupplierAccountResponse createCreateSupplierAccountResponse() {
        return new CreateSupplierAccountResponse();
    }

    /**
     * Create an instance of {@link SearchBrochuresResponse }
     * 
     */
    public SearchBrochuresResponse createSearchBrochuresResponse() {
        return new SearchBrochuresResponse();
    }

    /**
     * Create an instance of {@link SearchBrochureInfo }
     * 
     */
    public SearchBrochureInfo createSearchBrochureInfo() {
        return new SearchBrochureInfo();
    }

    /**
     * Create an instance of {@link CreateBrochureResponse }
     * 
     */
    public CreateBrochureResponse createCreateBrochureResponse() {
        return new CreateBrochureResponse();
    }

    /**
     * Create an instance of {@link SearchBrochures }
     * 
     */
    public SearchBrochures createSearchBrochures() {
        return new SearchBrochures();
    }

    /**
     * Create an instance of {@link GetNewBrochuresResponse }
     * 
     */
    public GetNewBrochuresResponse createGetNewBrochuresResponse() {
        return new GetNewBrochuresResponse();
    }

    /**
     * Create an instance of {@link ArrayOfBrochureInfo }
     * 
     */
    public ArrayOfBrochureInfo createArrayOfBrochureInfo() {
        return new ArrayOfBrochureInfo();
    }

    /**
     * Create an instance of {@link GetRegionsResponse }
     * 
     */
    public GetRegionsResponse createGetRegionsResponse() {
        return new GetRegionsResponse();
    }

    /**
     * Create an instance of {@link GetPropertyTypes }
     * 
     */
    public GetPropertyTypes createGetPropertyTypes() {
        return new GetPropertyTypes();
    }

    /**
     * Create an instance of {@link GetCountriesResponse }
     * 
     */
    public GetCountriesResponse createGetCountriesResponse() {
        return new GetCountriesResponse();
    }

    /**
     * Create an instance of {@link GetBrochureV1Response }
     * 
     */
    public GetBrochureV1Response createGetBrochureV1Response() {
        return new GetBrochureV1Response();
    }

    /**
     * Create an instance of {@link BrochureV1 }
     * 
     */
    public BrochureV1 createBrochureV1() {
        return new BrochureV1();
    }

    /**
     * Create an instance of {@link GetBrochureStatus }
     * 
     */
    public GetBrochureStatus createGetBrochureStatus() {
        return new GetBrochureStatus();
    }

    /**
     * Create an instance of {@link GetAllBrochureIDs }
     * 
     */
    public GetAllBrochureIDs createGetAllBrochureIDs() {
        return new GetAllBrochureIDs();
    }

    /**
     * Create an instance of {@link GetUpdatedBrochuresSinceResponse }
     * 
     */
    public GetUpdatedBrochuresSinceResponse createGetUpdatedBrochuresSinceResponse() {
        return new GetUpdatedBrochuresSinceResponse();
    }

    /**
     * Create an instance of {@link ArrayOfUpdatedBrochureInfo }
     * 
     */
    public ArrayOfUpdatedBrochureInfo createArrayOfUpdatedBrochureInfo() {
        return new ArrayOfUpdatedBrochureInfo();
    }

    /**
     * Create an instance of {@link GetLanguagesResponse }
     * 
     */
    public GetLanguagesResponse createGetLanguagesResponse() {
        return new GetLanguagesResponse();
    }

    /**
     * Create an instance of {@link ArrayOfItem }
     * 
     */
    public ArrayOfItem createArrayOfItem() {
        return new ArrayOfItem();
    }

    /**
     * Create an instance of {@link CreateSupplierAccount }
     * 
     */
    public CreateSupplierAccount createCreateSupplierAccount() {
        return new CreateSupplierAccount();
    }

    /**
     * Create an instance of {@link Supplier }
     * 
     */
    public Supplier createSupplier() {
        return new Supplier();
    }

    /**
     * Create an instance of {@link GetAllMappingsResponse }
     * 
     */
    public GetAllMappingsResponse createGetAllMappingsResponse() {
        return new GetAllMappingsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfMappingInfo }
     * 
     */
    public ArrayOfMappingInfo createArrayOfMappingInfo() {
        return new ArrayOfMappingInfo();
    }

    /**
     * Create an instance of {@link GetDirectVideoLinksResponse }
     * 
     */
    public GetDirectVideoLinksResponse createGetDirectVideoLinksResponse() {
        return new GetDirectVideoLinksResponse();
    }

    /**
     * Create an instance of {@link Brochure }
     * 
     */
    public Brochure createBrochure() {
        return new Brochure();
    }

    /**
     * Create an instance of {@link GetDirectVideoLinks }
     * 
     */
    public GetDirectVideoLinks createGetDirectVideoLinks() {
        return new GetDirectVideoLinks();
    }

    /**
     * Create an instance of {@link GetMappingInfoResponse }
     * 
     */
    public GetMappingInfoResponse createGetMappingInfoResponse() {
        return new GetMappingInfoResponse();
    }

    /**
     * Create an instance of {@link MappingInfo }
     * 
     */
    public MappingInfo createMappingInfo() {
        return new MappingInfo();
    }

    /**
     * Create an instance of {@link GetNewBrochuresSinceResponse }
     * 
     */
    public GetNewBrochuresSinceResponse createGetNewBrochuresSinceResponse() {
        return new GetNewBrochuresSinceResponse();
    }

    /**
     * Create an instance of {@link GetRoomTypesForProperty }
     * 
     */
    public GetRoomTypesForProperty createGetRoomTypesForProperty() {
        return new GetRoomTypesForProperty();
    }

    /**
     * Create an instance of {@link GetBrochureResponse }
     * 
     */
    public GetBrochureResponse createGetBrochureResponse() {
        return new GetBrochureResponse();
    }

    /**
     * Create an instance of {@link CreateBrochure }
     * 
     */
    public CreateBrochure createCreateBrochure() {
        return new CreateBrochure();
    }

    /**
     * Create an instance of {@link PropertyBrochure }
     * 
     */
    public PropertyBrochure createPropertyBrochure() {
        return new PropertyBrochure();
    }

    /**
     * Create an instance of {@link GetRoomTypesForImage }
     * 
     */
    public GetRoomTypesForImage createGetRoomTypesForImage() {
        return new GetRoomTypesForImage();
    }

    /**
     * Create an instance of {@link ICEAuthHeader }
     * 
     */
    public ICEAuthHeader createICEAuthHeader() {
        return new ICEAuthHeader();
    }

    /**
     * Create an instance of {@link GetUpdatedBrochuresSince }
     * 
     */
    public GetUpdatedBrochuresSince createGetUpdatedBrochuresSince() {
        return new GetUpdatedBrochuresSince();
    }

    /**
     * Create an instance of {@link GetNewBrochuresSince }
     * 
     */
    public GetNewBrochuresSince createGetNewBrochuresSince() {
        return new GetNewBrochuresSince();
    }

    /**
     * Create an instance of {@link GetPropertyTypesResponse }
     * 
     */
    public GetPropertyTypesResponse createGetPropertyTypesResponse() {
        return new GetPropertyTypesResponse();
    }

    /**
     * Create an instance of {@link GetRoomTypesForPropertyResponse }
     * 
     */
    public GetRoomTypesForPropertyResponse createGetRoomTypesForPropertyResponse() {
        return new GetRoomTypesForPropertyResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRoomType }
     * 
     */
    public ArrayOfRoomType createArrayOfRoomType() {
        return new ArrayOfRoomType();
    }

    /**
     * Create an instance of {@link GetImagesOfRoomTypeResponse }
     * 
     */
    public GetImagesOfRoomTypeResponse createGetImagesOfRoomTypeResponse() {
        return new GetImagesOfRoomTypeResponse();
    }

    /**
     * Create an instance of {@link ArrayOfImage }
     * 
     */
    public ArrayOfImage createArrayOfImage() {
        return new ArrayOfImage();
    }

    /**
     * Create an instance of {@link GetNewBrochureIDsSince }
     * 
     */
    public GetNewBrochureIDsSince createGetNewBrochureIDsSince() {
        return new GetNewBrochureIDsSince();
    }

    /**
     * Create an instance of {@link SetBrochureStatusResponse }
     * 
     */
    public SetBrochureStatusResponse createSetBrochureStatusResponse() {
        return new SetBrochureStatusResponse();
    }

    /**
     * Create an instance of {@link CreatePropertyResponse }
     * 
     */
    public CreatePropertyResponse createCreatePropertyResponse() {
        return new CreatePropertyResponse();
    }

    /**
     * Create an instance of {@link CreatedProperty }
     * 
     */
    public CreatedProperty createCreatedProperty() {
        return new CreatedProperty();
    }

    /**
     * Create an instance of {@link GetUpdatedBrochureIDsSince }
     * 
     */
    public GetUpdatedBrochureIDsSince createGetUpdatedBrochureIDsSince() {
        return new GetUpdatedBrochureIDsSince();
    }

    /**
     * Create an instance of {@link GetLanguages }
     * 
     */
    public GetLanguages createGetLanguages() {
        return new GetLanguages();
    }

    /**
     * Create an instance of {@link GetUpdatedBrochureIDsSinceResponse }
     * 
     */
    public GetUpdatedBrochureIDsSinceResponse createGetUpdatedBrochureIDsSinceResponse() {
        return new GetUpdatedBrochureIDsSinceResponse();
    }

    /**
     * Create an instance of {@link ArrayOfUpdatedBrochureIDInfo }
     * 
     */
    public ArrayOfUpdatedBrochureIDInfo createArrayOfUpdatedBrochureIDInfo() {
        return new ArrayOfUpdatedBrochureIDInfo();
    }

    /**
     * Create an instance of {@link GetCountries }
     * 
     */
    public GetCountries createGetCountries() {
        return new GetCountries();
    }

    /**
     * Create an instance of {@link GetImagesOfRoomType }
     * 
     */
    public GetImagesOfRoomType createGetImagesOfRoomType() {
        return new GetImagesOfRoomType();
    }

    /**
     * Create an instance of {@link SetBrochureStatus }
     * 
     */
    public SetBrochureStatus createSetBrochureStatus() {
        return new SetBrochureStatus();
    }

    /**
     * Create an instance of {@link GetRegions }
     * 
     */
    public GetRegions createGetRegions() {
        return new GetRegions();
    }

    /**
     * Create an instance of {@link GetAllBrochureIDsResponse }
     * 
     */
    public GetAllBrochureIDsResponse createGetAllBrochureIDsResponse() {
        return new GetAllBrochureIDsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link GetAllMappings }
     * 
     */
    public GetAllMappings createGetAllMappings() {
        return new GetAllMappings();
    }

    /**
     * Create an instance of {@link GetStates }
     * 
     */
    public GetStates createGetStates() {
        return new GetStates();
    }

    /**
     * Create an instance of {@link VideosListV1 }
     * 
     */
    public VideosListV1 createVideosListV1() {
        return new VideosListV1();
    }

    /**
     * Create an instance of {@link UpdatedBrochureIDInfo }
     * 
     */
    public UpdatedBrochureIDInfo createUpdatedBrochureIDInfo() {
        return new UpdatedBrochureIDInfo();
    }

    /**
     * Create an instance of {@link VideoInfo }
     * 
     */
    public VideoInfo createVideoInfo() {
        return new VideoInfo();
    }

    /**
     * Create an instance of {@link SupplierInformation }
     * 
     */
    public SupplierInformation createSupplierInformation() {
        return new SupplierInformation();
    }

    /**
     * Create an instance of {@link BrochureLocationV1 }
     * 
     */
    public BrochureLocationV1 createBrochureLocationV1() {
        return new BrochureLocationV1();
    }

    /**
     * Create an instance of {@link BrochureImageV1 }
     * 
     */
    public BrochureImageV1 createBrochureImageV1() {
        return new BrochureImageV1();
    }

    /**
     * Create an instance of {@link ArrayOfSearchBrochure }
     * 
     */
    public ArrayOfSearchBrochure createArrayOfSearchBrochure() {
        return new ArrayOfSearchBrochure();
    }

    /**
     * Create an instance of {@link ArrayOfDescriptionItem }
     * 
     */
    public ArrayOfDescriptionItem createArrayOfDescriptionItem() {
        return new ArrayOfDescriptionItem();
    }

    /**
     * Create an instance of {@link Item }
     * 
     */
    public Item createItem() {
        return new Item();
    }

    /**
     * Create an instance of {@link BrochureHotel }
     * 
     */
    public BrochureHotel createBrochureHotel() {
        return new BrochureHotel();
    }

    /**
     * Create an instance of {@link BrochureInfo }
     * 
     */
    public BrochureInfo createBrochureInfo() {
        return new BrochureInfo();
    }

    /**
     * Create an instance of {@link ArrayOfVideoInfoV1 }
     * 
     */
    public ArrayOfVideoInfoV1 createArrayOfVideoInfoV1() {
        return new ArrayOfVideoInfoV1();
    }

    /**
     * Create an instance of {@link MediaLink }
     * 
     */
    public MediaLink createMediaLink() {
        return new MediaLink();
    }

    /**
     * Create an instance of {@link ThumbUrl }
     * 
     */
    public ThumbUrl createThumbUrl() {
        return new ThumbUrl();
    }

    /**
     * Create an instance of {@link VideoURLCaption }
     * 
     */
    public VideoURLCaption createVideoURLCaption() {
        return new VideoURLCaption();
    }

    /**
     * Create an instance of {@link PDFFile }
     * 
     */
    public PDFFile createPDFFile() {
        return new PDFFile();
    }

    /**
     * Create an instance of {@link VideosList }
     * 
     */
    public VideosList createVideosList() {
        return new VideosList();
    }

    /**
     * Create an instance of {@link BrochureContentV1 }
     * 
     */
    public BrochureContentV1 createBrochureContentV1() {
        return new BrochureContentV1();
    }

    /**
     * Create an instance of {@link BrochureInfoV1 }
     * 
     */
    public BrochureInfoV1 createBrochureInfoV1() {
        return new BrochureInfoV1();
    }

    /**
     * Create an instance of {@link BrochureInformation }
     * 
     */
    public BrochureInformation createBrochureInformation() {
        return new BrochureInformation();
    }

    /**
     * Create an instance of {@link ArrayOfBrochureImageV1 }
     * 
     */
    public ArrayOfBrochureImageV1 createArrayOfBrochureImageV1() {
        return new ArrayOfBrochureImageV1();
    }

    /**
     * Create an instance of {@link BrochureConstraint }
     * 
     */
    public BrochureConstraint createBrochureConstraint() {
        return new BrochureConstraint();
    }

    /**
     * Create an instance of {@link VideoInfoV1 }
     * 
     */
    public VideoInfoV1 createVideoInfoV1() {
        return new VideoInfoV1();
    }

    /**
     * Create an instance of {@link BrochureInformationV1 }
     * 
     */
    public BrochureInformationV1 createBrochureInformationV1() {
        return new BrochureInformationV1();
    }

    /**
     * Create an instance of {@link Dimensions }
     * 
     */
    public Dimensions createDimensions() {
        return new Dimensions();
    }

    /**
     * Create an instance of {@link SearchBrochure }
     * 
     */
    public SearchBrochure createSearchBrochure() {
        return new SearchBrochure();
    }

    /**
     * Create an instance of {@link Video }
     * 
     */
    public Video createVideo() {
        return new Video();
    }

    /**
     * Create an instance of {@link ArrayOfVideoInfo }
     * 
     */
    public ArrayOfVideoInfo createArrayOfVideoInfo() {
        return new ArrayOfVideoInfo();
    }

    /**
     * Create an instance of {@link ArrayOfCategory }
     * 
     */
    public ArrayOfCategory createArrayOfCategory() {
        return new ArrayOfCategory();
    }

    /**
     * Create an instance of {@link Caption }
     * 
     */
    public Caption createCaption() {
        return new Caption();
    }

    /**
     * Create an instance of {@link ArrayOfPDFFileV1 }
     * 
     */
    public ArrayOfPDFFileV1 createArrayOfPDFFileV1() {
        return new ArrayOfPDFFileV1();
    }

    /**
     * Create an instance of {@link ArrayOfBrochureImage }
     * 
     */
    public ArrayOfBrochureImage createArrayOfBrochureImage() {
        return new ArrayOfBrochureImage();
    }

    /**
     * Create an instance of {@link BrochureImage }
     * 
     */
    public BrochureImage createBrochureImage() {
        return new BrochureImage();
    }

    /**
     * Create an instance of {@link IFrameInfo }
     * 
     */
    public IFrameInfo createIFrameInfo() {
        return new IFrameInfo();
    }

    /**
     * Create an instance of {@link MediaContentV1 }
     * 
     */
    public MediaContentV1 createMediaContentV1() {
        return new MediaContentV1();
    }

    /**
     * Create an instance of {@link Credentials }
     * 
     */
    public Credentials createCredentials() {
        return new Credentials();
    }

    /**
     * Create an instance of {@link ArrayOfPDFFile }
     * 
     */
    public ArrayOfPDFFile createArrayOfPDFFile() {
        return new ArrayOfPDFFile();
    }

    /**
     * Create an instance of {@link BrochureInfos }
     * 
     */
    public BrochureInfos createBrochureInfos() {
        return new BrochureInfos();
    }

    /**
     * Create an instance of {@link Image }
     * 
     */
    public Image createImage() {
        return new Image();
    }

    /**
     * Create an instance of {@link BrochureResources }
     * 
     */
    public BrochureResources createBrochureResources() {
        return new BrochureResources();
    }

    /**
     * Create an instance of {@link PDFFileV1 }
     * 
     */
    public PDFFileV1 createPDFFileV1() {
        return new PDFFileV1();
    }

    /**
     * Create an instance of {@link MediaContent }
     * 
     */
    public MediaContent createMediaContent() {
        return new MediaContent();
    }

    /**
     * Create an instance of {@link DescriptionItem }
     * 
     */
    public DescriptionItem createDescriptionItem() {
        return new DescriptionItem();
    }

    /**
     * Create an instance of {@link Keyword }
     * 
     */
    public Keyword createKeyword() {
        return new Keyword();
    }

    /**
     * Create an instance of {@link UpdatedBrochureInfo }
     * 
     */
    public UpdatedBrochureInfo createUpdatedBrochureInfo() {
        return new UpdatedBrochureInfo();
    }

    /**
     * Create an instance of {@link BrochureLocation }
     * 
     */
    public BrochureLocation createBrochureLocation() {
        return new BrochureLocation();
    }

    /**
     * Create an instance of {@link BrochureContent }
     * 
     */
    public BrochureContent createBrochureContent() {
        return new BrochureContent();
    }

    /**
     * Create an instance of {@link ArrayOfVideo }
     * 
     */
    public ArrayOfVideo createArrayOfVideo() {
        return new ArrayOfVideo();
    }

    /**
     * Create an instance of {@link ArrayOfVideoURLCaption }
     * 
     */
    public ArrayOfVideoURLCaption createArrayOfVideoURLCaption() {
        return new ArrayOfVideoURLCaption();
    }

    /**
     * Create an instance of {@link Category }
     * 
     */
    public Category createCategory() {
        return new Category();
    }

    /**
     * Create an instance of {@link BrochureRequest }
     * 
     */
    public BrochureRequest createBrochureRequest() {
        return new BrochureRequest();
    }

    /**
     * Create an instance of {@link BrochureResourcesV1 }
     * 
     */
    public BrochureResourcesV1 createBrochureResourcesV1() {
        return new BrochureResourcesV1();
    }

    /**
     * Create an instance of {@link RoomType }
     * 
     */
    public RoomType createRoomType() {
        return new RoomType();
    }

    /**
     * Create an instance of {@link ArrayOfCaption }
     * 
     */
    public ArrayOfCaption createArrayOfCaption() {
        return new ArrayOfCaption();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchBrochureInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.iceportal.com/service", name = "SearchBrochureInfo")
    public JAXBElement<SearchBrochureInfo> createSearchBrochureInfo(SearchBrochureInfo value) {
        return new JAXBElement<SearchBrochureInfo>(_SearchBrochureInfo_QNAME, SearchBrochureInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ICEAuthHeader }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.iceportal.com/service", name = "ICEAuthHeader")
    public JAXBElement<ICEAuthHeader> createICEAuthHeader(ICEAuthHeader value) {
        return new JAXBElement<ICEAuthHeader>(_ICEAuthHeader_QNAME, ICEAuthHeader.class, null, value);
    }

}
