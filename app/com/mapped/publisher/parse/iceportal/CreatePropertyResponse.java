
package com.mapped.publisher.parse.iceportal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreatePropertyResult" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ErrorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GeneratedBrochure" type="{http://services.iceportal.com/service}CreatedProperty" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createPropertyResult",
    "errorMessage",
    "generatedBrochure"
})
@XmlRootElement(name = "CreatePropertyResponse")
public class CreatePropertyResponse {

    @XmlElement(name = "CreatePropertyResult")
    protected int createPropertyResult;
    @XmlElement(name = "ErrorMessage")
    protected String errorMessage;
    @XmlElement(name = "GeneratedBrochure")
    protected CreatedProperty generatedBrochure;

    /**
     * Gets the value of the createPropertyResult property.
     * 
     */
    public int getCreatePropertyResult() {
        return createPropertyResult;
    }

    /**
     * Sets the value of the createPropertyResult property.
     * 
     */
    public void setCreatePropertyResult(int value) {
        this.createPropertyResult = value;
    }

    /**
     * Gets the value of the errorMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the value of the errorMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorMessage(String value) {
        this.errorMessage = value;
    }

    /**
     * Gets the value of the generatedBrochure property.
     * 
     * @return
     *     possible object is
     *     {@link CreatedProperty }
     *     
     */
    public CreatedProperty getGeneratedBrochure() {
        return generatedBrochure;
    }

    /**
     * Sets the value of the generatedBrochure property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreatedProperty }
     *     
     */
    public void setGeneratedBrochure(CreatedProperty value) {
        this.generatedBrochure = value;
    }

}
