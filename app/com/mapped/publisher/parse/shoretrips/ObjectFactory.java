//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.06.09 at 08:26:38 AM EDT 
//


package com.mapped.publisher.parse.shoretrips;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mapped.publisher.parse.shoretrips package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mapped.publisher.parse.shoretrips
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IntegrationResponse }
     * 
     */
    public IntegrationResponse createIntegrationResponse() {
        return new IntegrationResponse();
    }

    /**
     * Create an instance of {@link IntegrationResponse.Excursions }
     * 
     */
    public IntegrationResponse.Excursions createIntegrationResponseExcursions() {
        return new IntegrationResponse.Excursions();
    }

    /**
     * Create an instance of {@link IntegrationResponse.Excursions.Regions }
     * 
     */
    public IntegrationResponse.Excursions.Regions createIntegrationResponseExcursionsRegions() {
        return new IntegrationResponse.Excursions.Regions();
    }

    /**
     * Create an instance of {@link IntegrationResponse.Excursions.Regions.Region }
     * 
     */
    public IntegrationResponse.Excursions.Regions.Region createIntegrationResponseExcursionsRegionsRegion() {
        return new IntegrationResponse.Excursions.Regions.Region();
    }

    /**
     * Create an instance of {@link IntegrationResponse.Excursions.Regions.Region.Ports }
     * 
     */
    public IntegrationResponse.Excursions.Regions.Region.Ports createIntegrationResponseExcursionsRegionsRegionPorts() {
        return new IntegrationResponse.Excursions.Regions.Region.Ports();
    }

    /**
     * Create an instance of {@link IntegrationResponse.Excursions.Regions.Region.Ports.Location }
     * 
     */
    public IntegrationResponse.Excursions.Regions.Region.Ports.Location createIntegrationResponseExcursionsRegionsRegionPortsLocation() {
        return new IntegrationResponse.Excursions.Regions.Region.Ports.Location();
    }

    /**
     * Create an instance of {@link IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups }
     * 
     */
    public IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups createIntegrationResponseExcursionsRegionsRegionPortsLocationTripGroups() {
        return new IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups();
    }

    /**
     * Create an instance of {@link IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup }
     * 
     */
    public IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup createIntegrationResponseExcursionsRegionsRegionPortsLocationTripGroupsTripGroup() {
        return new IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup();
    }

    /**
     * Create an instance of {@link IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips }
     * 
     */
    public IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips createIntegrationResponseExcursionsRegionsRegionPortsLocationTripGroupsTripGroupTrips() {
        return new IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips();
    }

    /**
     * Create an instance of {@link IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip }
     * 
     */
    public IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip createIntegrationResponseExcursionsRegionsRegionPortsLocationTripGroupsTripGroupTripsTrip() {
        return new IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip();
    }

    /**
     * Create an instance of {@link IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip.Times }
     * 
     */
    public IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip.Times createIntegrationResponseExcursionsRegionsRegionPortsLocationTripGroupsTripGroupTripsTripTimes() {
        return new IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip.Times();
    }

    /**
     * Create an instance of {@link IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip.Prices }
     * 
     */
    public IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip.Prices createIntegrationResponseExcursionsRegionsRegionPortsLocationTripGroupsTripGroupTripsTripPrices() {
        return new IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip.Prices();
    }

    /**
     * Create an instance of {@link IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip.Times.TripTime }
     * 
     */
    public IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip.Times.TripTime createIntegrationResponseExcursionsRegionsRegionPortsLocationTripGroupsTripGroupTripsTripTimesTripTime() {
        return new IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip.Times.TripTime();
    }

    /**
     * Create an instance of {@link IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip.Prices.TripPrice }
     * 
     */
    public IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip.Prices.TripPrice createIntegrationResponseExcursionsRegionsRegionPortsLocationTripGroupsTripGroupTripsTripPricesTripPrice() {
        return new IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip.Prices.TripPrice();
    }

}
