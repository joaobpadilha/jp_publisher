package com.mapped.publisher.parse.valueObject;

import java.awt.image.BufferedImage;
import java.util.Date;
import java.util.Map;

public class TravelDetails {

  private Map<Date, DateDetails> dateWiseDetails;
  private String additionalInfo;
  private String introInfo;
  private BufferedImage coverImg;

  public Map<Date, DateDetails> getDateWiseDetails() {
    return dateWiseDetails;
  }

  public void setDateWiseDetails(Map<Date, DateDetails> dateWiseDetails) {
    this.dateWiseDetails = dateWiseDetails;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }

  public String getIntroInfo() {
    return introInfo;
  }

  public void setIntroInfo(String introInfo) {
    this.introInfo = introInfo;
  }

  public BufferedImage getCoverImg() {
    return coverImg;
  }

  public void setCoverImg(BufferedImage coverImg) {
    this.coverImg = coverImg;
  }
}
