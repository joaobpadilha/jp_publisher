package com.mapped.publisher.parse.valueObject;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-22
 * Time: 9:36 AM
 * To change this template use File | Settings | File Templates.
 */
public class DateDetails {

    private Date date;
    private Date endDate;

    private String fulText;
    private Map<Date, DayDetails> activities;
    private List<Date> dates;
    private String title;


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getFulText() {
        return fulText;
    }

    public void setFulText(String fulText) {
        this.fulText = fulText;
    }

    public Map<Date, DayDetails> getActivities() {
        return activities;
    }

    public void setActivities(Map<Date, DayDetails> activities) {
        this.activities = activities;
    }

    public List<Date> getDates() {
        return dates;
    }

    public void setDates(List<Date> dates) {
        this.dates = dates;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
