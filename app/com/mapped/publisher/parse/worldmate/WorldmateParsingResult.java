
package com.mapped.publisher.parse.worldmate;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="error-msg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="headers" type="{http://www.worldmate.com/schemas/worldmate-api-v1.xsd}headers"/>
 *         &lt;element name="end-user-emails" type="{http://www.worldmate.com/schemas/worldmate-api-v1.xsd}end-user-emails"/>
 *         &lt;element name="reservation-details" type="{http://www.worldmate.com/schemas/worldmate-api-v1.xsd}reservation" minOccurs="0"/>
 *         &lt;element name="items" type="{http://www.worldmate.com/schemas/worldmate-api-v1.xsd}items" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="received" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *       &lt;attribute name="request-id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "status",
    "errorMsg",
    "headers",
    "endUserEmails",
    "reservationDetails",
    "items"
})
@XmlRootElement(name = "worldmate-parsing-result", namespace = "http://www.worldmate.com/schemas/worldmate-api-v1.xsd")
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
public class WorldmateParsingResult {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected String status;
    @XmlElement(name = "error-msg")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected String errorMsg;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected Headers headers;
    @XmlElement(name = "end-user-emails", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected EndUserEmails endUserEmails;
    @XmlElement(name = "reservation-details")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected Reservation reservationDetails;
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected Items items;
    @XmlAttribute(name = "received", required = true)
    @XmlSchemaType(name = "dateTime")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected XMLGregorianCalendar received;
    @XmlAttribute(name = "request-id", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected String requestId;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the errorMsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public String getErrorMsg() {
        return errorMsg;
    }

    /**
     * Sets the value of the errorMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setErrorMsg(String value) {
        this.errorMsg = value;
    }

    /**
     * Gets the value of the headers property.
     * 
     * @return
     *     possible object is
     *     {@link Headers }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public Headers getHeaders() {
        return headers;
    }

    /**
     * Sets the value of the headers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Headers }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setHeaders(Headers value) {
        this.headers = value;
    }

    /**
     * Gets the value of the endUserEmails property.
     * 
     * @return
     *     possible object is
     *     {@link EndUserEmails }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public EndUserEmails getEndUserEmails() {
        return endUserEmails;
    }

    /**
     * Sets the value of the endUserEmails property.
     * 
     * @param value
     *     allowed object is
     *     {@link EndUserEmails }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setEndUserEmails(EndUserEmails value) {
        this.endUserEmails = value;
    }

    /**
     * Gets the value of the reservationDetails property.
     * 
     * @return
     *     possible object is
     *     {@link Reservation }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public Reservation getReservationDetails() {
        return reservationDetails;
    }

    /**
     * Sets the value of the reservationDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link Reservation }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setReservationDetails(Reservation value) {
        this.reservationDetails = value;
    }

    /**
     * Gets the value of the items property.
     * 
     * @return
     *     possible object is
     *     {@link Items }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public Items getItems() {
        return items;
    }

    /**
     * Sets the value of the items property.
     * 
     * @param value
     *     allowed object is
     *     {@link Items }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setItems(Items value) {
        this.items = value;
    }

    /**
     * Gets the value of the received property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public XMLGregorianCalendar getReceived() {
        return received;
    }

    /**
     * Sets the value of the received property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setReceived(XMLGregorianCalendar value) {
        this.received = value;
    }

    /**
     * Gets the value of the requestId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public String getRequestId() {
        return requestId;
    }

    /**
     * Sets the value of the requestId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setRequestId(String value) {
        this.requestId = value;
    }

}
