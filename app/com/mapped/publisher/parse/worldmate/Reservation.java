
package com.mapped.publisher.parse.worldmate;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for reservation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="reservation">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.worldmate.com/schemas/worldmate-api-v1.xsd}itinerary-item">
 *       &lt;sequence>
 *         &lt;element name="record-locator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="booking-details" type="{http://www.worldmate.com/schemas/worldmate-api-v1.xsd}booking-details" minOccurs="0"/>
 *         &lt;element name="provider-details" type="{http://www.worldmate.com/schemas/worldmate-api-v1.xsd}provider-details" minOccurs="0"/>
 *         &lt;element name="total-price" type="{http://www.worldmate.com/schemas/worldmate-api-v1.xsd}price-details" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reservation", namespace = "http://www.worldmate.com/schemas/worldmate-api-v1.xsd", propOrder = {
    "recordLocator",
    "bookingDetails",
    "providerDetails",
    "totalPrice"
})
@XmlSeeAlso({
    Event.class,
    PublicTransportation.class,
    CarRental.class,
    Flight.class,
    HotelReservation.class
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
public class Reservation
    extends ItineraryItem
{

    @XmlElement(name = "record-locator")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected String recordLocator;
    @XmlElement(name = "booking-details")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected BookingDetails bookingDetails;
    @XmlElement(name = "provider-details")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected ProviderDetails providerDetails;
    @XmlElement(name = "total-price")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected PriceDetails totalPrice;

    /**
     * Gets the value of the recordLocator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public String getRecordLocator() {
        return recordLocator;
    }

    /**
     * Sets the value of the recordLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setRecordLocator(String value) {
        this.recordLocator = value;
    }

    /**
     * Gets the value of the bookingDetails property.
     * 
     * @return
     *     possible object is
     *     {@link BookingDetails }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public BookingDetails getBookingDetails() {
        return bookingDetails;
    }

    /**
     * Sets the value of the bookingDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingDetails }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setBookingDetails(BookingDetails value) {
        this.bookingDetails = value;
    }

    /**
     * Gets the value of the providerDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ProviderDetails }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public ProviderDetails getProviderDetails() {
        return providerDetails;
    }

    /**
     * Sets the value of the providerDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProviderDetails }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setProviderDetails(ProviderDetails value) {
        this.providerDetails = value;
    }

    /**
     * Gets the value of the totalPrice property.
     * 
     * @return
     *     possible object is
     *     {@link PriceDetails }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public PriceDetails getTotalPrice() {
        return totalPrice;
    }

    /**
     * Sets the value of the totalPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriceDetails }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setTotalPrice(PriceDetails value) {
        this.totalPrice = value;
    }

}
