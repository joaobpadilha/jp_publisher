
package com.mapped.publisher.parse.worldmate;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for flight-details complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="flight-details">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="airline-code" use="required" type="{http://www.worldmate.com/schemas/worldmate-api-v1.xsd}airline-code" />
 *       &lt;attribute name="number" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "flight-details", namespace = "http://www.worldmate.com/schemas/worldmate-api-v1.xsd")
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
public class FlightDetails {

    @XmlAttribute(name = "airline-code", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected String airlineCode;
    @XmlAttribute(name = "number", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected int number;

    /**
     * Gets the value of the airlineCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public String getAirlineCode() {
        return airlineCode;
    }

    /**
     * Sets the value of the airlineCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setAirlineCode(String value) {
        this.airlineCode = value;
    }

    /**
     * Gets the value of the number property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public int getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setNumber(int value) {
        this.number = value;
    }

}
