
package com.mapped.publisher.parse.worldmate;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for car-rental complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="car-rental">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.worldmate.com/schemas/worldmate-api-v1.xsd}reservation">
 *       &lt;sequence>
 *         &lt;element name="car-type" type="{http://www.worldmate.com/schemas/worldmate-api-v1.xsd}coded-name" minOccurs="0"/>
 *         &lt;element name="pickup" type="{http://www.worldmate.com/schemas/worldmate-api-v1.xsd}car-rental-locaion"/>
 *         &lt;element name="dropoff" type="{http://www.worldmate.com/schemas/worldmate-api-v1.xsd}car-rental-locaion"/>
 *         &lt;element name="driver" type="{http://www.worldmate.com/schemas/worldmate-api-v1.xsd}traveler" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "car-rental", namespace = "http://www.worldmate.com/schemas/worldmate-api-v1.xsd", propOrder = {
    "carType",
    "pickup",
    "dropoff",
    "driver"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
public class CarRental
    extends Reservation
{

    @XmlElement(name = "car-type")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected CodedName carType;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected CarRentalLocaion pickup;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected CarRentalLocaion dropoff;
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected List<Traveler> driver;

    /**
     * Gets the value of the carType property.
     * 
     * @return
     *     possible object is
     *     {@link CodedName }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public CodedName getCarType() {
        return carType;
    }

    /**
     * Sets the value of the carType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedName }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setCarType(CodedName value) {
        this.carType = value;
    }

    /**
     * Gets the value of the pickup property.
     * 
     * @return
     *     possible object is
     *     {@link CarRentalLocaion }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public CarRentalLocaion getPickup() {
        return pickup;
    }

    /**
     * Sets the value of the pickup property.
     * 
     * @param value
     *     allowed object is
     *     {@link CarRentalLocaion }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setPickup(CarRentalLocaion value) {
        this.pickup = value;
    }

    /**
     * Gets the value of the dropoff property.
     * 
     * @return
     *     possible object is
     *     {@link CarRentalLocaion }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public CarRentalLocaion getDropoff() {
        return dropoff;
    }

    /**
     * Sets the value of the dropoff property.
     * 
     * @param value
     *     allowed object is
     *     {@link CarRentalLocaion }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setDropoff(CarRentalLocaion value) {
        this.dropoff = value;
    }

    /**
     * Gets the value of the driver property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the driver property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDriver().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Traveler }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public List<Traveler> getDriver() {
        if (driver == null) {
            driver = new ArrayList<Traveler>();
        }
        return this.driver;
    }

}
