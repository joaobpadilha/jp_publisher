
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DayMedia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DayMedia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StartDayNum" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FormatType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Content" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UsePreviousDay" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DayMedia", namespace = "http://webapi.globusandcosmos.com/TourInformation.xsd", propOrder = {
    "contentType",
    "startDayNum",
    "formatType",
    "content",
    "usePreviousDay"
})
public class DayMedia {

    @XmlElement(name = "ContentType")
    protected String contentType;
    @XmlElement(name = "StartDayNum")
    protected int startDayNum;
    @XmlElement(name = "FormatType")
    protected String formatType;
    @XmlElement(name = "Content")
    protected String content;
    @XmlElement(name = "UsePreviousDay")
    protected boolean usePreviousDay;

    /**
     * Gets the value of the contentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * Sets the value of the contentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentType(String value) {
        this.contentType = value;
    }

    /**
     * Gets the value of the startDayNum property.
     * 
     */
    public int getStartDayNum() {
        return startDayNum;
    }

    /**
     * Sets the value of the startDayNum property.
     * 
     */
    public void setStartDayNum(int value) {
        this.startDayNum = value;
    }

    /**
     * Gets the value of the formatType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormatType() {
        return formatType;
    }

    /**
     * Sets the value of the formatType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormatType(String value) {
        this.formatType = value;
    }

    /**
     * Gets the value of the content property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets the value of the content property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContent(String value) {
        this.content = value;
    }

    /**
     * Gets the value of the usePreviousDay property.
     * 
     */
    public boolean isUsePreviousDay() {
        return usePreviousDay;
    }

    /**
     * Sets the value of the usePreviousDay property.
     * 
     */
    public void setUsePreviousDay(boolean value) {
        this.usePreviousDay = value;
    }

}
