
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCurrentPromotionsXMLResult" type="{http://webapi.globusandcosmos.com/}ArrayOfCurrentPromotion" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCurrentPromotionsXMLResult"
})
@XmlRootElement(name = "GetCurrentPromotionsXMLResponse")
public class GetCurrentPromotionsXMLResponse {

    @XmlElement(name = "GetCurrentPromotionsXMLResult")
    protected ArrayOfCurrentPromotion getCurrentPromotionsXMLResult;

    /**
     * Gets the value of the getCurrentPromotionsXMLResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCurrentPromotion }
     *     
     */
    public ArrayOfCurrentPromotion getGetCurrentPromotionsXMLResult() {
        return getCurrentPromotionsXMLResult;
    }

    /**
     * Sets the value of the getCurrentPromotionsXMLResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCurrentPromotion }
     *     
     */
    public void setGetCurrentPromotionsXMLResult(ArrayOfCurrentPromotion value) {
        this.getCurrentPromotionsXMLResult = value;
    }

}
