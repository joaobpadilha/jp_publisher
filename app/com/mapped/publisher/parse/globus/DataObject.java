
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for DataObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DataObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreatedByUserId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="UpdatedByUserId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DateCreated" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DateUpdated" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="IsOpenForModification" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataObject", propOrder = {
    "createdByUserId",
    "updatedByUserId",
    "dateCreated",
    "dateUpdated",
    "isOpenForModification"
})
@XmlSeeAlso({
    ExcursionPrice.class
})
public abstract class DataObject {

    @XmlElement(name = "CreatedByUserId")
    protected int createdByUserId;
    @XmlElement(name = "UpdatedByUserId")
    protected int updatedByUserId;
    @XmlElement(name = "DateCreated", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateCreated;
    @XmlElement(name = "DateUpdated", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateUpdated;
    @XmlElement(name = "IsOpenForModification")
    protected boolean isOpenForModification;

    /**
     * Gets the value of the createdByUserId property.
     * 
     */
    public int getCreatedByUserId() {
        return createdByUserId;
    }

    /**
     * Sets the value of the createdByUserId property.
     * 
     */
    public void setCreatedByUserId(int value) {
        this.createdByUserId = value;
    }

    /**
     * Gets the value of the updatedByUserId property.
     * 
     */
    public int getUpdatedByUserId() {
        return updatedByUserId;
    }

    /**
     * Sets the value of the updatedByUserId property.
     * 
     */
    public void setUpdatedByUserId(int value) {
        this.updatedByUserId = value;
    }

    /**
     * Gets the value of the dateCreated property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateCreated() {
        return dateCreated;
    }

    /**
     * Sets the value of the dateCreated property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateCreated(XMLGregorianCalendar value) {
        this.dateCreated = value;
    }

    /**
     * Gets the value of the dateUpdated property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateUpdated() {
        return dateUpdated;
    }

    /**
     * Sets the value of the dateUpdated property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateUpdated(XMLGregorianCalendar value) {
        this.dateUpdated = value;
    }

    /**
     * Gets the value of the isOpenForModification property.
     * 
     */
    public boolean isIsOpenForModification() {
        return isOpenForModification;
    }

    /**
     * Sets the value of the isOpenForModification property.
     * 
     */
    public void setIsOpenForModification(boolean value) {
        this.isOpenForModification = value;
    }

}
