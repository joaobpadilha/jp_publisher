
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExcursionMedia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExcursionMedia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExcursionID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="OptionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ImageName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BriefDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Advisement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InstructionsForUse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Comments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StartTime" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DurationHours" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MinimumAge" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MobilityLimitations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsAnytime" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsGroup" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsInventoryControlled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsVoucherRequiredForPax" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ExcursionIncluded" type="{http://webapi.globusandcosmos.com/}ArrayOfExcursionFeature" minOccurs="0"/>
 *         &lt;element name="ExcursionStyles" type="{http://webapi.globusandcosmos.com/}ArrayOfExcursionFeature" minOccurs="0"/>
 *         &lt;element name="ExcursionPrices" type="{http://webapi.globusandcosmos.com/}ArrayOfExcursionPrice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExcursionMedia", propOrder = {
    "name",
    "excursionID",
    "optionCode",
    "status",
    "imageName",
    "briefDescription",
    "description",
    "advisement",
    "instructionsForUse",
    "comments",
    "locationCity",
    "locationCountry",
    "startTime",
    "durationHours",
    "minimumAge",
    "mobilityLimitations",
    "isAnytime",
    "isGroup",
    "isInventoryControlled",
    "isVoucherRequiredForPax",
    "excursionIncluded",
    "excursionStyles",
    "excursionPrices"
})
public class ExcursionMedia {

    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "ExcursionID")
    protected int excursionID;
    @XmlElement(name = "OptionCode")
    protected String optionCode;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "ImageName")
    protected String imageName;
    @XmlElement(name = "BriefDescription")
    protected String briefDescription;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "Advisement")
    protected String advisement;
    @XmlElement(name = "InstructionsForUse")
    protected String instructionsForUse;
    @XmlElement(name = "Comments")
    protected String comments;
    @XmlElement(name = "LocationCity")
    protected String locationCity;
    @XmlElement(name = "LocationCountry")
    protected String locationCountry;
    @XmlElement(name = "StartTime")
    protected int startTime;
    @XmlElement(name = "DurationHours")
    protected int durationHours;
    @XmlElement(name = "MinimumAge")
    protected int minimumAge;
    @XmlElement(name = "MobilityLimitations")
    protected boolean mobilityLimitations;
    @XmlElement(name = "IsAnytime")
    protected boolean isAnytime;
    @XmlElement(name = "IsGroup")
    protected boolean isGroup;
    @XmlElement(name = "IsInventoryControlled")
    protected boolean isInventoryControlled;
    @XmlElement(name = "IsVoucherRequiredForPax")
    protected boolean isVoucherRequiredForPax;
    @XmlElement(name = "ExcursionIncluded")
    protected ArrayOfExcursionFeature excursionIncluded;
    @XmlElement(name = "ExcursionStyles")
    protected ArrayOfExcursionFeature excursionStyles;
    @XmlElement(name = "ExcursionPrices")
    protected ArrayOfExcursionPrice excursionPrices;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the excursionID property.
     * 
     */
    public int getExcursionID() {
        return excursionID;
    }

    /**
     * Sets the value of the excursionID property.
     * 
     */
    public void setExcursionID(int value) {
        this.excursionID = value;
    }

    /**
     * Gets the value of the optionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOptionCode() {
        return optionCode;
    }

    /**
     * Sets the value of the optionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOptionCode(String value) {
        this.optionCode = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the imageName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImageName() {
        return imageName;
    }

    /**
     * Sets the value of the imageName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImageName(String value) {
        this.imageName = value;
    }

    /**
     * Gets the value of the briefDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBriefDescription() {
        return briefDescription;
    }

    /**
     * Sets the value of the briefDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBriefDescription(String value) {
        this.briefDescription = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the advisement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvisement() {
        return advisement;
    }

    /**
     * Sets the value of the advisement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvisement(String value) {
        this.advisement = value;
    }

    /**
     * Gets the value of the instructionsForUse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstructionsForUse() {
        return instructionsForUse;
    }

    /**
     * Sets the value of the instructionsForUse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstructionsForUse(String value) {
        this.instructionsForUse = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Gets the value of the locationCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationCity() {
        return locationCity;
    }

    /**
     * Sets the value of the locationCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationCity(String value) {
        this.locationCity = value;
    }

    /**
     * Gets the value of the locationCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationCountry() {
        return locationCountry;
    }

    /**
     * Sets the value of the locationCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationCountry(String value) {
        this.locationCountry = value;
    }

    /**
     * Gets the value of the startTime property.
     * 
     */
    public int getStartTime() {
        return startTime;
    }

    /**
     * Sets the value of the startTime property.
     * 
     */
    public void setStartTime(int value) {
        this.startTime = value;
    }

    /**
     * Gets the value of the durationHours property.
     * 
     */
    public int getDurationHours() {
        return durationHours;
    }

    /**
     * Sets the value of the durationHours property.
     * 
     */
    public void setDurationHours(int value) {
        this.durationHours = value;
    }

    /**
     * Gets the value of the minimumAge property.
     * 
     */
    public int getMinimumAge() {
        return minimumAge;
    }

    /**
     * Sets the value of the minimumAge property.
     * 
     */
    public void setMinimumAge(int value) {
        this.minimumAge = value;
    }

    /**
     * Gets the value of the mobilityLimitations property.
     * 
     */
    public boolean isMobilityLimitations() {
        return mobilityLimitations;
    }

    /**
     * Sets the value of the mobilityLimitations property.
     * 
     */
    public void setMobilityLimitations(boolean value) {
        this.mobilityLimitations = value;
    }

    /**
     * Gets the value of the isAnytime property.
     * 
     */
    public boolean isIsAnytime() {
        return isAnytime;
    }

    /**
     * Sets the value of the isAnytime property.
     * 
     */
    public void setIsAnytime(boolean value) {
        this.isAnytime = value;
    }

    /**
     * Gets the value of the isGroup property.
     * 
     */
    public boolean isIsGroup() {
        return isGroup;
    }

    /**
     * Sets the value of the isGroup property.
     * 
     */
    public void setIsGroup(boolean value) {
        this.isGroup = value;
    }

    /**
     * Gets the value of the isInventoryControlled property.
     * 
     */
    public boolean isIsInventoryControlled() {
        return isInventoryControlled;
    }

    /**
     * Sets the value of the isInventoryControlled property.
     * 
     */
    public void setIsInventoryControlled(boolean value) {
        this.isInventoryControlled = value;
    }

    /**
     * Gets the value of the isVoucherRequiredForPax property.
     * 
     */
    public boolean isIsVoucherRequiredForPax() {
        return isVoucherRequiredForPax;
    }

    /**
     * Sets the value of the isVoucherRequiredForPax property.
     * 
     */
    public void setIsVoucherRequiredForPax(boolean value) {
        this.isVoucherRequiredForPax = value;
    }

    /**
     * Gets the value of the excursionIncluded property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfExcursionFeature }
     *     
     */
    public ArrayOfExcursionFeature getExcursionIncluded() {
        return excursionIncluded;
    }

    /**
     * Sets the value of the excursionIncluded property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfExcursionFeature }
     *     
     */
    public void setExcursionIncluded(ArrayOfExcursionFeature value) {
        this.excursionIncluded = value;
    }

    /**
     * Gets the value of the excursionStyles property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfExcursionFeature }
     *     
     */
    public ArrayOfExcursionFeature getExcursionStyles() {
        return excursionStyles;
    }

    /**
     * Sets the value of the excursionStyles property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfExcursionFeature }
     *     
     */
    public void setExcursionStyles(ArrayOfExcursionFeature value) {
        this.excursionStyles = value;
    }

    /**
     * Gets the value of the excursionPrices property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfExcursionPrice }
     *     
     */
    public ArrayOfExcursionPrice getExcursionPrices() {
        return excursionPrices;
    }

    /**
     * Sets the value of the excursionPrices property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfExcursionPrice }
     *     
     */
    public void setExcursionPrices(ArrayOfExcursionPrice value) {
        this.excursionPrices = value;
    }

}
