
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetBasicHotelMediaResult" type="{http://webapi.globusandcosmos.com/}ArrayOfBasicHotelMedia" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getBasicHotelMediaResult"
})
@XmlRootElement(name = "GetBasicHotelMediaResponse")
public class GetBasicHotelMediaResponse {

    @XmlElement(name = "GetBasicHotelMediaResult")
    protected ArrayOfBasicHotelMedia getBasicHotelMediaResult;

    /**
     * Gets the value of the getBasicHotelMediaResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBasicHotelMedia }
     *     
     */
    public ArrayOfBasicHotelMedia getGetBasicHotelMediaResult() {
        return getBasicHotelMediaResult;
    }

    /**
     * Sets the value of the getBasicHotelMediaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBasicHotelMedia }
     *     
     */
    public void setGetBasicHotelMediaResult(ArrayOfBasicHotelMedia value) {
        this.getBasicHotelMediaResult = value;
    }

}
