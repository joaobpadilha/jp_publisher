
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetBasicHotelMediaByLocationResult" type="{http://webapi.globusandcosmos.com/}ArrayOfBasicHotelMedia" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getBasicHotelMediaByLocationResult"
})
@XmlRootElement(name = "GetBasicHotelMediaByLocationResponse")
public class GetBasicHotelMediaByLocationResponse {

    @XmlElement(name = "GetBasicHotelMediaByLocationResult")
    protected ArrayOfBasicHotelMedia getBasicHotelMediaByLocationResult;

    /**
     * Gets the value of the getBasicHotelMediaByLocationResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBasicHotelMedia }
     *     
     */
    public ArrayOfBasicHotelMedia getGetBasicHotelMediaByLocationResult() {
        return getBasicHotelMediaByLocationResult;
    }

    /**
     * Sets the value of the getBasicHotelMediaByLocationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBasicHotelMedia }
     *     
     */
    public void setGetBasicHotelMediaByLocationResult(ArrayOfBasicHotelMedia value) {
        this.getBasicHotelMediaByLocationResult = value;
    }

}
