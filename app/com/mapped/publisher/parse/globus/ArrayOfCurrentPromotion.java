
package com.mapped.publisher.parse.globus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfCurrentPromotion complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ArrayOfCurrentPromotion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CurrentPromotion" type="{http://webapi.globusandcosmos.com/}CurrentPromotion" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCurrentPromotion", propOrder = {
    "currentPromotions"
})
public class ArrayOfCurrentPromotion {

    @XmlElement(name = "CurrentPromotion", nillable = true)
    protected ArrayList<CurrentPromotion> currentPromotions;

    /**
     * Gets the value of the currentPromotions property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the currentPromotions property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCurrentPromotions().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CurrentPromotion }
     *
     *
     */
    public ArrayList<CurrentPromotion> getCurrentPromotions() {
        if (currentPromotions == null) {
            currentPromotions = new ArrayList<CurrentPromotion>();
        }
        return this.currentPromotions;
    }

}
