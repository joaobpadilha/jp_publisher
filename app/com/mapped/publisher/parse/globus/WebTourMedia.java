
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WebTourMedia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WebTourMedia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TourMediaCollection" type="{http://webapi.globusandcosmos.com/TourInformation.xsd}ArrayOfTourMedia" minOccurs="0"/>
 *         &lt;element name="DayMediaCollection" type="{http://webapi.globusandcosmos.com/TourInformation.xsd}ArrayOfDayMedia" minOccurs="0"/>
 *         &lt;element name="TourKeywordCollection" type="{http://webapi.globusandcosmos.com/TourInformation.xsd}ArrayOfTourKeyword" minOccurs="0"/>
 *         &lt;element name="TourInfo" type="{http://webapi.globusandcosmos.com/TourInformation.xsd}TourInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WebTourMedia", namespace = "http://webapi.globusandcosmos.com/TourInformation.xsd", propOrder = {
    "tourMediaCollection",
    "dayMediaCollection",
    "tourKeywordCollection",
    "tourInfo"
})
public class WebTourMedia {

    @XmlElement(name = "TourMediaCollection")
    protected ArrayOfTourMedia tourMediaCollection;
    @XmlElement(name = "DayMediaCollection")
    protected ArrayOfDayMedia dayMediaCollection;
    @XmlElement(name = "TourKeywordCollection")
    protected ArrayOfTourKeyword tourKeywordCollection;
    @XmlElement(name = "TourInfo")
    protected TourInfo tourInfo;

    /**
     * Gets the value of the tourMediaCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTourMedia }
     *     
     */
    public ArrayOfTourMedia getTourMediaCollection() {
        return tourMediaCollection;
    }

    /**
     * Sets the value of the tourMediaCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTourMedia }
     *     
     */
    public void setTourMediaCollection(ArrayOfTourMedia value) {
        this.tourMediaCollection = value;
    }

    /**
     * Gets the value of the dayMediaCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDayMedia }
     *     
     */
    public ArrayOfDayMedia getDayMediaCollection() {
        return dayMediaCollection;
    }

    /**
     * Sets the value of the dayMediaCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDayMedia }
     *     
     */
    public void setDayMediaCollection(ArrayOfDayMedia value) {
        this.dayMediaCollection = value;
    }

    /**
     * Gets the value of the tourKeywordCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTourKeyword }
     *     
     */
    public ArrayOfTourKeyword getTourKeywordCollection() {
        return tourKeywordCollection;
    }

    /**
     * Sets the value of the tourKeywordCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTourKeyword }
     *     
     */
    public void setTourKeywordCollection(ArrayOfTourKeyword value) {
        this.tourKeywordCollection = value;
    }

    /**
     * Gets the value of the tourInfo property.
     * 
     * @return
     *     possible object is
     *     {@link TourInfo }
     *     
     */
    public TourInfo getTourInfo() {
        return tourInfo;
    }

    /**
     * Sets the value of the tourInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TourInfo }
     *     
     */
    public void setTourInfo(TourInfo value) {
        this.tourInfo = value;
    }

}
