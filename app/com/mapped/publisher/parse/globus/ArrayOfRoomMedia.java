
package com.mapped.publisher.parse.globus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfRoomMedia complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ArrayOfRoomMedia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RoomMedia" type="{http://webapi.globusandcosmos.com/}RoomMedia" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRoomMedia", propOrder = {
    "roomMedias"
})
public class ArrayOfRoomMedia {

    @XmlElement(name = "RoomMedia", nillable = true)
    protected ArrayList<RoomMedia> roomMedias;

    /**
     * Gets the value of the roomMedias property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the roomMedias property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRoomMedias().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoomMedia }
     *
     *
     */
    public ArrayList<RoomMedia> getRoomMedias() {
        if (roomMedias == null) {
            roomMedias = new ArrayList<RoomMedia>();
        }
        return this.roomMedias;
    }

}
