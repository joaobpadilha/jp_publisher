
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetHotelMediaByCityAndTourCodeResult" type="{http://webapi.globusandcosmos.com/}ArrayOfHotelMedia" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getHotelMediaByCityAndTourCodeResult"
})
@XmlRootElement(name = "GetHotelMediaByCityAndTourCodeResponse")
public class GetHotelMediaByCityAndTourCodeResponse {

    @XmlElement(name = "GetHotelMediaByCityAndTourCodeResult")
    protected ArrayOfHotelMedia getHotelMediaByCityAndTourCodeResult;

    /**
     * Gets the value of the getHotelMediaByCityAndTourCodeResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfHotelMedia }
     *     
     */
    public ArrayOfHotelMedia getGetHotelMediaByCityAndTourCodeResult() {
        return getHotelMediaByCityAndTourCodeResult;
    }

    /**
     * Sets the value of the getHotelMediaByCityAndTourCodeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfHotelMedia }
     *     
     */
    public void setGetHotelMediaByCityAndTourCodeResult(ArrayOfHotelMedia value) {
        this.getHotelMediaByCityAndTourCodeResult = value;
    }

}
