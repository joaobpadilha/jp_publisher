
package com.mapped.publisher.parse.globus;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HotelMedia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HotelMedia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HotelCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressStateProvince" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StreetAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CheckInTime" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CheckOutTime" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DistanceToAirport" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DistanceToCityCenter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DistanceToMetroStation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NearestMajorAirportCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumberOfFloors" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NumberOfRooms" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RoomDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StarRating" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="WebsiteUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="YearBuilt" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="YearLastRenovated" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IsAShip" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Latitude" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Longitude" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="HotelImages" type="{http://webapi.globusandcosmos.com/}ArrayOfHotelImage" minOccurs="0"/>
 *         &lt;element name="HotelAmenities" type="{http://webapi.globusandcosmos.com/}ArrayOfHotelAmenity" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HotelMedia", propOrder = {
    "hotelCode",
    "name",
    "description",
    "locationDescription",
    "addressCity",
    "addressStateProvince",
    "country",
    "streetAddress",
    "checkInTime",
    "checkOutTime",
    "distanceToAirport",
    "distanceToCityCenter",
    "distanceToMetroStation",
    "nearestMajorAirportCode",
    "numberOfFloors",
    "numberOfRooms",
    "roomDescription",
    "starRating",
    "websiteUrl",
    "yearBuilt",
    "yearLastRenovated",
    "isAShip",
    "latitude",
    "longitude",
    "hotelImages",
    "hotelAmenities"
})
public class HotelMedia {

    @XmlElement(name = "HotelCode")
    protected String hotelCode;
    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "LocationDescription")
    protected String locationDescription;
    @XmlElement(name = "AddressCity")
    protected String addressCity;
    @XmlElement(name = "AddressStateProvince")
    protected String addressStateProvince;
    @XmlElement(name = "Country")
    protected String country;
    @XmlElement(name = "StreetAddress")
    protected String streetAddress;
    @XmlElement(name = "CheckInTime")
    protected int checkInTime;
    @XmlElement(name = "CheckOutTime")
    protected int checkOutTime;
    @XmlElement(name = "DistanceToAirport")
    protected String distanceToAirport;
    @XmlElement(name = "DistanceToCityCenter")
    protected String distanceToCityCenter;
    @XmlElement(name = "DistanceToMetroStation")
    protected String distanceToMetroStation;
    @XmlElement(name = "NearestMajorAirportCode")
    protected String nearestMajorAirportCode;
    @XmlElement(name = "NumberOfFloors")
    protected int numberOfFloors;
    @XmlElement(name = "NumberOfRooms")
    protected int numberOfRooms;
    @XmlElement(name = "RoomDescription")
    protected String roomDescription;
    @XmlElement(name = "StarRating", required = true)
    protected BigDecimal starRating;
    @XmlElement(name = "WebsiteUrl")
    protected String websiteUrl;
    @XmlElement(name = "YearBuilt")
    protected int yearBuilt;
    @XmlElement(name = "YearLastRenovated")
    protected int yearLastRenovated;
    @XmlElement(name = "IsAShip")
    protected boolean isAShip;
    @XmlElement(name = "Latitude", required = true)
    protected BigDecimal latitude;
    @XmlElement(name = "Longitude", required = true)
    protected BigDecimal longitude;
    @XmlElement(name = "HotelImages")
    protected ArrayOfHotelImage hotelImages;
    @XmlElement(name = "HotelAmenities")
    protected ArrayOfHotelAmenity hotelAmenities;

    /**
     * Gets the value of the hotelCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelCode() {
        return hotelCode;
    }

    /**
     * Sets the value of the hotelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelCode(String value) {
        this.hotelCode = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the locationDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationDescription() {
        return locationDescription;
    }

    /**
     * Sets the value of the locationDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationDescription(String value) {
        this.locationDescription = value;
    }

    /**
     * Gets the value of the addressCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressCity() {
        return addressCity;
    }

    /**
     * Sets the value of the addressCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressCity(String value) {
        this.addressCity = value;
    }

    /**
     * Gets the value of the addressStateProvince property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressStateProvince() {
        return addressStateProvince;
    }

    /**
     * Sets the value of the addressStateProvince property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressStateProvince(String value) {
        this.addressStateProvince = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the streetAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreetAddress() {
        return streetAddress;
    }

    /**
     * Sets the value of the streetAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreetAddress(String value) {
        this.streetAddress = value;
    }

    /**
     * Gets the value of the checkInTime property.
     * 
     */
    public int getCheckInTime() {
        return checkInTime;
    }

    /**
     * Sets the value of the checkInTime property.
     * 
     */
    public void setCheckInTime(int value) {
        this.checkInTime = value;
    }

    /**
     * Gets the value of the checkOutTime property.
     * 
     */
    public int getCheckOutTime() {
        return checkOutTime;
    }

    /**
     * Sets the value of the checkOutTime property.
     * 
     */
    public void setCheckOutTime(int value) {
        this.checkOutTime = value;
    }

    /**
     * Gets the value of the distanceToAirport property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistanceToAirport() {
        return distanceToAirport;
    }

    /**
     * Sets the value of the distanceToAirport property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistanceToAirport(String value) {
        this.distanceToAirport = value;
    }

    /**
     * Gets the value of the distanceToCityCenter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistanceToCityCenter() {
        return distanceToCityCenter;
    }

    /**
     * Sets the value of the distanceToCityCenter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistanceToCityCenter(String value) {
        this.distanceToCityCenter = value;
    }

    /**
     * Gets the value of the distanceToMetroStation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistanceToMetroStation() {
        return distanceToMetroStation;
    }

    /**
     * Sets the value of the distanceToMetroStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistanceToMetroStation(String value) {
        this.distanceToMetroStation = value;
    }

    /**
     * Gets the value of the nearestMajorAirportCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNearestMajorAirportCode() {
        return nearestMajorAirportCode;
    }

    /**
     * Sets the value of the nearestMajorAirportCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNearestMajorAirportCode(String value) {
        this.nearestMajorAirportCode = value;
    }

    /**
     * Gets the value of the numberOfFloors property.
     * 
     */
    public int getNumberOfFloors() {
        return numberOfFloors;
    }

    /**
     * Sets the value of the numberOfFloors property.
     * 
     */
    public void setNumberOfFloors(int value) {
        this.numberOfFloors = value;
    }

    /**
     * Gets the value of the numberOfRooms property.
     * 
     */
    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    /**
     * Sets the value of the numberOfRooms property.
     * 
     */
    public void setNumberOfRooms(int value) {
        this.numberOfRooms = value;
    }

    /**
     * Gets the value of the roomDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoomDescription() {
        return roomDescription;
    }

    /**
     * Sets the value of the roomDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoomDescription(String value) {
        this.roomDescription = value;
    }

    /**
     * Gets the value of the starRating property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getStarRating() {
        return starRating;
    }

    /**
     * Sets the value of the starRating property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setStarRating(BigDecimal value) {
        this.starRating = value;
    }

    /**
     * Gets the value of the websiteUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebsiteUrl() {
        return websiteUrl;
    }

    /**
     * Sets the value of the websiteUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebsiteUrl(String value) {
        this.websiteUrl = value;
    }

    /**
     * Gets the value of the yearBuilt property.
     * 
     */
    public int getYearBuilt() {
        return yearBuilt;
    }

    /**
     * Sets the value of the yearBuilt property.
     * 
     */
    public void setYearBuilt(int value) {
        this.yearBuilt = value;
    }

    /**
     * Gets the value of the yearLastRenovated property.
     * 
     */
    public int getYearLastRenovated() {
        return yearLastRenovated;
    }

    /**
     * Sets the value of the yearLastRenovated property.
     * 
     */
    public void setYearLastRenovated(int value) {
        this.yearLastRenovated = value;
    }

    /**
     * Gets the value of the isAShip property.
     * 
     */
    public boolean isIsAShip() {
        return isAShip;
    }

    /**
     * Sets the value of the isAShip property.
     * 
     */
    public void setIsAShip(boolean value) {
        this.isAShip = value;
    }

    /**
     * Gets the value of the latitude property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLatitude() {
        return latitude;
    }

    /**
     * Sets the value of the latitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLatitude(BigDecimal value) {
        this.latitude = value;
    }

    /**
     * Gets the value of the longitude property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLongitude() {
        return longitude;
    }

    /**
     * Sets the value of the longitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLongitude(BigDecimal value) {
        this.longitude = value;
    }

    /**
     * Gets the value of the hotelImages property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfHotelImage }
     *     
     */
    public ArrayOfHotelImage getHotelImages() {
        return hotelImages;
    }

    /**
     * Sets the value of the hotelImages property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfHotelImage }
     *     
     */
    public void setHotelImages(ArrayOfHotelImage value) {
        this.hotelImages = value;
    }

    /**
     * Gets the value of the hotelAmenities property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfHotelAmenity }
     *     
     */
    public ArrayOfHotelAmenity getHotelAmenities() {
        return hotelAmenities;
    }

    /**
     * Sets the value of the hotelAmenities property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfHotelAmenity }
     *     
     */
    public void setHotelAmenities(ArrayOfHotelAmenity value) {
        this.hotelAmenities = value;
    }

}
