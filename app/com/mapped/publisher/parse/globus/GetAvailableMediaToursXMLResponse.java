
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://webapi.globusandcosmos.com/AvailableTours.xsd}GetAvailableMediaToursXMLResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAvailableMediaToursXMLResult"
})
@XmlRootElement(name = "GetAvailableMediaToursXMLResponse")
public class GetAvailableMediaToursXMLResponse {

    @XmlElement(name = "GetAvailableMediaToursXMLResult", namespace = "http://webapi.globusandcosmos.com/AvailableTours.xsd", required = true, nillable = true)
    protected AvailableMediaTours getAvailableMediaToursXMLResult;

    /**
     * Gets the value of the getAvailableMediaToursXMLResult property.
     * 
     * @return
     *     possible object is
     *     {@link AvailableMediaTours }
     *     
     */
    public AvailableMediaTours getGetAvailableMediaToursXMLResult() {
        return getAvailableMediaToursXMLResult;
    }

    /**
     * Sets the value of the getAvailableMediaToursXMLResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AvailableMediaTours }
     *     
     */
    public void setGetAvailableMediaToursXMLResult(AvailableMediaTours value) {
        this.getAvailableMediaToursXMLResult = value;
    }

}
