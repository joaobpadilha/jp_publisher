
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Brand.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Brand">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Unknown"/>
 *     &lt;enumeration value="All"/>
 *     &lt;enumeration value="Globus"/>
 *     &lt;enumeration value="Cosmos"/>
 *     &lt;enumeration value="Avalon"/>
 *     &lt;enumeration value="Monograms"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Brand", namespace = "http://enterprise.globusandcosmos.com/ebl/")
@XmlEnum
public enum Brand {

    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown"),
    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("Globus")
    GLOBUS("Globus"),
    @XmlEnumValue("Cosmos")
    COSMOS("Cosmos"),
    @XmlEnumValue("Avalon")
    AVALON("Avalon"),
    @XmlEnumValue("Monograms")
    MONOGRAMS("Monograms");
    private final String value;

    Brand(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Brand fromValue(String v) {
        for (Brand c: Brand.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
