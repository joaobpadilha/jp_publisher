
package com.mapped.publisher.parse.globus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfDeparturePricing complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ArrayOfDeparturePricing">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DeparturePricing" type="{http://webapi.globusandcosmos.com/}DeparturePricing" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDeparturePricing", propOrder = {
    "departurePricings"
})
public class ArrayOfDeparturePricing {

    @XmlElement(name = "DeparturePricing", nillable = true)
    protected ArrayList<DeparturePricing> departurePricings;

    /**
     * Gets the value of the departurePricings property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the departurePricings property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDeparturePricings().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DeparturePricing }
     *
     *
     */
    public ArrayList<DeparturePricing> getDeparturePricings() {
        if (departurePricings == null) {
            departurePricings = new ArrayList<DeparturePricing>();
        }
        return this.departurePricings;
    }

}
