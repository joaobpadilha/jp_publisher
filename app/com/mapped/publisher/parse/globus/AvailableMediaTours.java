
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AvailableMediaTours complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AvailableMediaTours">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MediaTours" type="{http://webapi.globusandcosmos.com/AvailableTours.xsd}ArrayOfAvailableMediaTour" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AvailableMediaTours", namespace = "http://webapi.globusandcosmos.com/AvailableTours.xsd", propOrder = {
    "mediaTours"
})
public class AvailableMediaTours {

    @XmlElement(name = "MediaTours")
    protected ArrayOfAvailableMediaTour mediaTours;

    /**
     * Gets the value of the mediaTours property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAvailableMediaTour }
     *     
     */
    public ArrayOfAvailableMediaTour getMediaTours() {
        return mediaTours;
    }

    /**
     * Sets the value of the mediaTours property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAvailableMediaTour }
     *     
     */
    public void setMediaTours(ArrayOfAvailableMediaTour value) {
        this.mediaTours = value;
    }

}
