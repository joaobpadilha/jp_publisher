
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://webapi.globusandcosmos.com/AvailableTours.xsd}GetAvailableGSAToursXMLResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAvailableGSAToursXMLResult"
})
@XmlRootElement(name = "GetAvailableGSAToursXMLResponse")
public class GetAvailableGSAToursXMLResponse {

    @XmlElement(name = "GetAvailableGSAToursXMLResult", namespace = "http://webapi.globusandcosmos.com/AvailableTours.xsd", required = true, nillable = true)
    protected AvailableTours getAvailableGSAToursXMLResult;

    /**
     * Gets the value of the getAvailableGSAToursXMLResult property.
     * 
     * @return
     *     possible object is
     *     {@link AvailableTours }
     *     
     */
    public AvailableTours getGetAvailableGSAToursXMLResult() {
        return getAvailableGSAToursXMLResult;
    }

    /**
     * Sets the value of the getAvailableGSAToursXMLResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AvailableTours }
     *     
     */
    public void setGetAvailableGSAToursXMLResult(AvailableTours value) {
        this.getAvailableGSAToursXMLResult = value;
    }

}
