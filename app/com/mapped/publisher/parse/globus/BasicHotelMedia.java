
package com.mapped.publisher.parse.globus;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BasicHotelMedia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BasicHotelMedia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BasicName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BasicDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BasicAddressCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BasicAddressStateProvince" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BasicCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BasicStreetAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BasicLatitude" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="BasicLongitude" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="BasicHotelRating" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BasicHotelCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HasWifi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BasicSellingLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HotelCommments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BasicHotelMedia", propOrder = {
    "basicName",
    "basicDescription",
    "basicAddressCity",
    "basicAddressStateProvince",
    "basicCountry",
    "basicStreetAddress",
    "basicLatitude",
    "basicLongitude",
    "basicHotelRating",
    "basicHotelCode",
    "hasWifi",
    "basicSellingLocation",
    "hotelCommments"
})
public class BasicHotelMedia {

    @XmlElement(name = "BasicName")
    protected String basicName;
    @XmlElement(name = "BasicDescription")
    protected String basicDescription;
    @XmlElement(name = "BasicAddressCity")
    protected String basicAddressCity;
    @XmlElement(name = "BasicAddressStateProvince")
    protected String basicAddressStateProvince;
    @XmlElement(name = "BasicCountry")
    protected String basicCountry;
    @XmlElement(name = "BasicStreetAddress")
    protected String basicStreetAddress;
    @XmlElement(name = "BasicLatitude", required = true)
    protected BigDecimal basicLatitude;
    @XmlElement(name = "BasicLongitude", required = true)
    protected BigDecimal basicLongitude;
    @XmlElement(name = "BasicHotelRating")
    protected String basicHotelRating;
    @XmlElement(name = "BasicHotelCode")
    protected String basicHotelCode;
    @XmlElement(name = "HasWifi")
    protected String hasWifi;
    @XmlElement(name = "BasicSellingLocation")
    protected String basicSellingLocation;
    @XmlElement(name = "HotelCommments")
    protected String hotelCommments;

    /**
     * Gets the value of the basicName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasicName() {
        return basicName;
    }

    /**
     * Sets the value of the basicName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasicName(String value) {
        this.basicName = value;
    }

    /**
     * Gets the value of the basicDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasicDescription() {
        return basicDescription;
    }

    /**
     * Sets the value of the basicDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasicDescription(String value) {
        this.basicDescription = value;
    }

    /**
     * Gets the value of the basicAddressCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasicAddressCity() {
        return basicAddressCity;
    }

    /**
     * Sets the value of the basicAddressCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasicAddressCity(String value) {
        this.basicAddressCity = value;
    }

    /**
     * Gets the value of the basicAddressStateProvince property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasicAddressStateProvince() {
        return basicAddressStateProvince;
    }

    /**
     * Sets the value of the basicAddressStateProvince property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasicAddressStateProvince(String value) {
        this.basicAddressStateProvince = value;
    }

    /**
     * Gets the value of the basicCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasicCountry() {
        return basicCountry;
    }

    /**
     * Sets the value of the basicCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasicCountry(String value) {
        this.basicCountry = value;
    }

    /**
     * Gets the value of the basicStreetAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasicStreetAddress() {
        return basicStreetAddress;
    }

    /**
     * Sets the value of the basicStreetAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasicStreetAddress(String value) {
        this.basicStreetAddress = value;
    }

    /**
     * Gets the value of the basicLatitude property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBasicLatitude() {
        return basicLatitude;
    }

    /**
     * Sets the value of the basicLatitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBasicLatitude(BigDecimal value) {
        this.basicLatitude = value;
    }

    /**
     * Gets the value of the basicLongitude property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBasicLongitude() {
        return basicLongitude;
    }

    /**
     * Sets the value of the basicLongitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBasicLongitude(BigDecimal value) {
        this.basicLongitude = value;
    }

    /**
     * Gets the value of the basicHotelRating property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasicHotelRating() {
        return basicHotelRating;
    }

    /**
     * Sets the value of the basicHotelRating property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasicHotelRating(String value) {
        this.basicHotelRating = value;
    }

    /**
     * Gets the value of the basicHotelCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasicHotelCode() {
        return basicHotelCode;
    }

    /**
     * Sets the value of the basicHotelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasicHotelCode(String value) {
        this.basicHotelCode = value;
    }

    /**
     * Gets the value of the hasWifi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHasWifi() {
        return hasWifi;
    }

    /**
     * Sets the value of the hasWifi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHasWifi(String value) {
        this.hasWifi = value;
    }

    /**
     * Gets the value of the basicSellingLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasicSellingLocation() {
        return basicSellingLocation;
    }

    /**
     * Sets the value of the basicSellingLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasicSellingLocation(String value) {
        this.basicSellingLocation = value;
    }

    /**
     * Gets the value of the hotelCommments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelCommments() {
        return hotelCommments;
    }

    /**
     * Sets the value of the hotelCommments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelCommments(String value) {
        this.hotelCommments = value;
    }

}
