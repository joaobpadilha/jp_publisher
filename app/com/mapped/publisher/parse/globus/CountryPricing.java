
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CountryPricing.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CountryPricing">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="USA"/>
 *     &lt;enumeration value="Canada"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CountryPricing", namespace = "http://enterprise.globusandcosmos.com/ebl/")
@XmlEnum
public enum CountryPricing {

    USA("USA"),
    @XmlEnumValue("Canada")
    CANADA("Canada");
    private final String value;

    CountryPricing(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CountryPricing fromValue(String v) {
        for (CountryPricing c: CountryPricing.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
