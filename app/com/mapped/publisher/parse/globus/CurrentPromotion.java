
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CurrentPromotion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CurrentPromotion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TourCodes" type="{http://webapi.globusandcosmos.com/}ArrayOfCurrentPromotionTour" minOccurs="0"/>
 *         &lt;element name="PromotionId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Headline" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Category" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Disclaimer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BookStartDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BookEndDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TravelStartDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TravelEndDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AskReceiveCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RequiresAir" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CurrentPromotion", propOrder = {
    "tourCodes",
    "promotionId",
    "headline",
    "category",
    "disclaimer",
    "bookStartDate",
    "bookEndDate",
    "travelStartDate",
    "travelEndDate",
    "askReceiveCode",
    "requiresAir"
})
public class CurrentPromotion {

    @XmlElement(name = "TourCodes")
    protected ArrayOfCurrentPromotionTour tourCodes;
    @XmlElement(name = "PromotionId")
    protected int promotionId;
    @XmlElement(name = "Headline")
    protected String headline;
    @XmlElement(name = "Category")
    protected String category;
    @XmlElement(name = "Disclaimer")
    protected String disclaimer;
    @XmlElement(name = "BookStartDate")
    protected String bookStartDate;
    @XmlElement(name = "BookEndDate")
    protected String bookEndDate;
    @XmlElement(name = "TravelStartDate")
    protected String travelStartDate;
    @XmlElement(name = "TravelEndDate")
    protected String travelEndDate;
    @XmlElement(name = "AskReceiveCode")
    protected String askReceiveCode;
    @XmlElement(name = "RequiresAir")
    protected boolean requiresAir;

    /**
     * Gets the value of the tourCodes property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCurrentPromotionTour }
     *     
     */
    public ArrayOfCurrentPromotionTour getTourCodes() {
        return tourCodes;
    }

    /**
     * Sets the value of the tourCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCurrentPromotionTour }
     *     
     */
    public void setTourCodes(ArrayOfCurrentPromotionTour value) {
        this.tourCodes = value;
    }

    /**
     * Gets the value of the promotionId property.
     * 
     */
    public int getPromotionId() {
        return promotionId;
    }

    /**
     * Sets the value of the promotionId property.
     * 
     */
    public void setPromotionId(int value) {
        this.promotionId = value;
    }

    /**
     * Gets the value of the headline property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeadline() {
        return headline;
    }

    /**
     * Sets the value of the headline property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeadline(String value) {
        this.headline = value;
    }

    /**
     * Gets the value of the category property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets the value of the category property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategory(String value) {
        this.category = value;
    }

    /**
     * Gets the value of the disclaimer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisclaimer() {
        return disclaimer;
    }

    /**
     * Sets the value of the disclaimer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisclaimer(String value) {
        this.disclaimer = value;
    }

    /**
     * Gets the value of the bookStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookStartDate() {
        return bookStartDate;
    }

    /**
     * Sets the value of the bookStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookStartDate(String value) {
        this.bookStartDate = value;
    }

    /**
     * Gets the value of the bookEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookEndDate() {
        return bookEndDate;
    }

    /**
     * Sets the value of the bookEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookEndDate(String value) {
        this.bookEndDate = value;
    }

    /**
     * Gets the value of the travelStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTravelStartDate() {
        return travelStartDate;
    }

    /**
     * Sets the value of the travelStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTravelStartDate(String value) {
        this.travelStartDate = value;
    }

    /**
     * Gets the value of the travelEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTravelEndDate() {
        return travelEndDate;
    }

    /**
     * Sets the value of the travelEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTravelEndDate(String value) {
        this.travelEndDate = value;
    }

    /**
     * Gets the value of the askReceiveCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAskReceiveCode() {
        return askReceiveCode;
    }

    /**
     * Sets the value of the askReceiveCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAskReceiveCode(String value) {
        this.askReceiveCode = value;
    }

    /**
     * Gets the value of the requiresAir property.
     * 
     */
    public boolean isRequiresAir() {
        return requiresAir;
    }

    /**
     * Sets the value of the requiresAir property.
     * 
     */
    public void setRequiresAir(boolean value) {
        this.requiresAir = value;
    }

}
