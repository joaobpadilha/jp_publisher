
package com.mapped.publisher.parse.globus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfExtendedDeparture complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ArrayOfExtendedDeparture">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExtendedDeparture" type="{http://webapi.globusandcosmos.com/ExtendedDepartureList.xsd}ExtendedDeparture" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfExtendedDeparture", namespace = "http://webapi.globusandcosmos.com/ExtendedDepartureList.xsd", propOrder = {
    "extendedDepartures"
})
public class ArrayOfExtendedDeparture {

    @XmlElement(name = "ExtendedDeparture", nillable = true)
    protected ArrayList<ExtendedDeparture> extendedDepartures;

    /**
     * Gets the value of the extendedDepartures property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the extendedDepartures property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExtendedDepartures().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExtendedDeparture }
     *
     *
     */
    public ArrayList<ExtendedDeparture> getExtendedDepartures() {
        if (extendedDepartures == null) {
            extendedDepartures = new ArrayList<ExtendedDeparture>();
        }
        return this.extendedDepartures;
    }

}
