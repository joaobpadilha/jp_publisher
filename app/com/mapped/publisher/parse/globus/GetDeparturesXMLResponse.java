
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://webapi.globusandcosmos.com/DepartureList.xsd}GetDeparturesXMLResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getDeparturesXMLResult"
})
@XmlRootElement(name = "GetDeparturesXMLResponse")
public class GetDeparturesXMLResponse {

    @XmlElement(name = "GetDeparturesXMLResult", namespace = "http://webapi.globusandcosmos.com/DepartureList.xsd", required = true, nillable = true)
    protected DepartureList getDeparturesXMLResult;

    /**
     * Gets the value of the getDeparturesXMLResult property.
     * 
     * @return
     *     possible object is
     *     {@link DepartureList }
     *     
     */
    public DepartureList getGetDeparturesXMLResult() {
        return getDeparturesXMLResult;
    }

    /**
     * Sets the value of the getDeparturesXMLResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link DepartureList }
     *     
     */
    public void setGetDeparturesXMLResult(DepartureList value) {
        this.getDeparturesXMLResult = value;
    }

}
