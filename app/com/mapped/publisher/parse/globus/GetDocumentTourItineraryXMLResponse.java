
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetDocumentTourItineraryXMLResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getDocumentTourItineraryXMLResult"
})
@XmlRootElement(name = "GetDocumentTourItineraryXMLResponse")
public class GetDocumentTourItineraryXMLResponse {

    @XmlElement(name = "GetDocumentTourItineraryXMLResult")
    protected String getDocumentTourItineraryXMLResult;

    /**
     * Gets the value of the getDocumentTourItineraryXMLResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetDocumentTourItineraryXMLResult() {
        return getDocumentTourItineraryXMLResult;
    }

    /**
     * Sets the value of the getDocumentTourItineraryXMLResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetDocumentTourItineraryXMLResult(String value) {
        this.getDocumentTourItineraryXMLResult = value;
    }

}
