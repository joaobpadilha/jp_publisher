
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://webapi.globusandcosmos.com/DepartureList.xsd}GetGSADeparturesXMLResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getGSADeparturesXMLResult"
})
@XmlRootElement(name = "GetGSADeparturesXMLResponse")
public class GetGSADeparturesXMLResponse {

    @XmlElement(name = "GetGSADeparturesXMLResult", namespace = "http://webapi.globusandcosmos.com/DepartureList.xsd", required = true, nillable = true)
    protected DepartureList getGSADeparturesXMLResult;

    /**
     * Gets the value of the getGSADeparturesXMLResult property.
     * 
     * @return
     *     possible object is
     *     {@link DepartureList }
     *     
     */
    public DepartureList getGetGSADeparturesXMLResult() {
        return getGSADeparturesXMLResult;
    }

    /**
     * Sets the value of the getGSADeparturesXMLResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link DepartureList }
     *     
     */
    public void setGetGSADeparturesXMLResult(DepartureList value) {
        this.getGSADeparturesXMLResult = value;
    }

}
