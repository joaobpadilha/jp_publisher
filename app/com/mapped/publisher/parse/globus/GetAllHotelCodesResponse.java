
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAllHotelCodesResult" type="{http://webapi.globusandcosmos.com/}ArrayOfString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllHotelCodesResult"
})
@XmlRootElement(name = "GetAllHotelCodesResponse")
public class GetAllHotelCodesResponse {

    @XmlElement(name = "GetAllHotelCodesResult")
    protected ArrayOfString getAllHotelCodesResult;

    /**
     * Gets the value of the getAllHotelCodesResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getGetAllHotelCodesResult() {
        return getAllHotelCodesResult;
    }

    /**
     * Sets the value of the getAllHotelCodesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setGetAllHotelCodesResult(ArrayOfString value) {
        this.getAllHotelCodesResult = value;
    }

}
