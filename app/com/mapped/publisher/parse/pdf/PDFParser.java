package com.mapped.publisher.parse.pdf;

import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.InputStream;
import java.util.List;

public interface PDFParser {
	public String parse(InputStream input) throws Exception;
    public PDDocument getPDDocument ();
    public List<?> getAllPages ();

    }
 
