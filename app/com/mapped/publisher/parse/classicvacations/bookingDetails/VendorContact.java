package com.mapped.publisher.parse.classicvacations.bookingDetails;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;

/**
 * Created by twong on 15-06-30.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
                       "Address",
                       "ContactInfo",
                       "WebSiteAddress"
                   })
public class VendorContact {
  @JsonProperty("Address")
  private String Address;
  @JsonProperty("ContactInfo")
  private String ContactInfo;
  @JsonProperty("WebSiteAddress")
  private String WebSiteAddress;

  @JsonProperty("Address")
  public String getAddress() {
    return Address;
  }
  @JsonProperty("Address")
  public void setAddress(String address) {
    Address = address;
  }

  @JsonProperty("ContactInfo")
  public String getContactInfo() {
    return ContactInfo;
  }
  @JsonProperty("ContactInfo")
  public void setContactInfo(String contactInfo) {
    ContactInfo = contactInfo;
  }

  @JsonProperty("WebSiteAddress")
  public String getWebSiteAddress() {
    return WebSiteAddress;
  }
  @JsonProperty("WebSiteAddress")
  public void setWebSiteAddress(String webSiteAddress) {
    WebSiteAddress = webSiteAddress;
  }
}
