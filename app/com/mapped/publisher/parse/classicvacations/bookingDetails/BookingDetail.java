
package com.mapped.publisher.parse.classicvacations.bookingDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "Name",
    "Detail",
    "Payment",
    "Exception",
    "Destination",
    "PartyName",
    "Adults",
    "Children",
    "TimeStamp"
})
public class BookingDetail {

    @JsonProperty("Name")
    private List<com.mapped.publisher.parse.classicvacations.bookingDetails.Name> Name = new ArrayList<com.mapped.publisher.parse.classicvacations.bookingDetails.Name>();
    @JsonProperty("Detail")
    private List<com.mapped.publisher.parse.classicvacations.bookingDetails.Detail> Detail = new ArrayList<com.mapped.publisher.parse.classicvacations.bookingDetails.Detail>();
    @JsonProperty("Payment")
    private com.mapped.publisher.parse.classicvacations.bookingDetails.Payment Payment;
    @JsonProperty("Exception")
    private Object Exception;
    @JsonProperty("Destination")
    private String Destination;
    @JsonProperty("PartyName")
    private String PartyName;
    @JsonProperty("Adults")
    private Integer Adults;
    @JsonProperty("Children")
    private Integer Children;
    @JsonProperty("TimeStamp")
    private String TimeStamp;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The Name
     */
    @JsonProperty("Name")
    public List<com.mapped.publisher.parse.classicvacations.bookingDetails.Name> getName() {
        return Name;
    }

    /**
     * 
     * @param Name
     *     The Name
     */
    @JsonProperty("Name")
    public void setName(List<com.mapped.publisher.parse.classicvacations.bookingDetails.Name> Name) {
        this.Name = Name;
    }

    /**
     * 
     * @return
     *     The Detail
     */
    @JsonProperty("Detail")
    public List<com.mapped.publisher.parse.classicvacations.bookingDetails.Detail> getDetail() {
        return Detail;
    }

    /**
     * 
     * @param Detail
     *     The Detail
     */
    @JsonProperty("Detail")
    public void setDetail(List<com.mapped.publisher.parse.classicvacations.bookingDetails.Detail> Detail) {
        this.Detail = Detail;
    }

    /**
     * 
     * @return
     *     The Payment
     */
    @JsonProperty("Payment")
    public com.mapped.publisher.parse.classicvacations.bookingDetails.Payment getPayment() {
        return Payment;
    }

    /**
     * 
     * @param Payment
     *     The Payment
     */
    @JsonProperty("Payment")
    public void setPayment(com.mapped.publisher.parse.classicvacations.bookingDetails.Payment Payment) {
        this.Payment = Payment;
    }

    /**
     * 
     * @return
     *     The Exception
     */
    @JsonProperty("Exception")
    public Object getException() {
        return Exception;
    }

    /**
     * 
     * @param Exception
     *     The Exception
     */
    @JsonProperty("Exception")
    public void setException(Object Exception) {
        this.Exception = Exception;
    }

    /**
     * 
     * @return
     *     The Destination
     */
    @JsonProperty("Destination")
    public String getDestination() {
        return Destination;
    }

    /**
     * 
     * @param Destination
     *     The Destination
     */
    @JsonProperty("Destination")
    public void setDestination(String Destination) {
        this.Destination = Destination;
    }

    /**
     * 
     * @return
     *     The PartyName
     */
    @JsonProperty("PartyName")
    public String getPartyName() {
        return PartyName;
    }

    /**
     * 
     * @param PartyName
     *     The PartyName
     */
    @JsonProperty("PartyName")
    public void setPartyName(String PartyName) {
        this.PartyName = PartyName;
    }

    /**
     * 
     * @return
     *     The Adults
     */
    @JsonProperty("Adults")
    public Integer getAdults() {
        return Adults;
    }

    /**
     * 
     * @param Adults
     *     The Adults
     */
    @JsonProperty("Adults")
    public void setAdults(Integer Adults) {
        this.Adults = Adults;
    }

    /**
     * 
     * @return
     *     The Children
     */
    @JsonProperty("Children")
    public Integer getChildren() {
        return Children;
    }

    /**
     * 
     * @param Children
     *     The Children
     */
    @JsonProperty("Children")
    public void setChildren(Integer Children) {
        this.Children = Children;
    }

    /**
     * 
     * @return
     *     The TimeStamp
     */
    @JsonProperty("TimeStamp")
    public String getTimeStamp() {
        return TimeStamp;
    }

    /**
     * 
     * @param TimeStamp
     *     The TimeStamp
     */
    @JsonProperty("TimeStamp")
    public void setTimeStamp(String TimeStamp) {
        this.TimeStamp = TimeStamp;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
