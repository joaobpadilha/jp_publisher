
package com.mapped.publisher.parse.classicvacations.bookingDetails;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "SellAmount",
    "CommAmount",
    "TaxAmount",
    "AmountBeforeTax",
    "DepAmountDue",
    "AmountPosted",
    "CurrAmountDue",
    "DueDate"
})
public class Payment {

    @JsonProperty("SellAmount")
    private Double SellAmount;
    @JsonProperty("CommAmount")
    private Double CommAmount;
    @JsonProperty("TaxAmount")
    private Double TaxAmount;
    @JsonProperty("AmountBeforeTax")
    private Double AmountBeforeTax;
    @JsonProperty("DepAmountDue")
    private Double DepAmountDue;
    @JsonProperty("AmountPosted")
    private Double AmountPosted;
    @JsonProperty("CurrAmountDue")
    private Double CurrAmountDue;
    @JsonProperty("DueDate")
    private String DueDate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The SellAmount
     */
    @JsonProperty("SellAmount")
    public Double getSellAmount() {
        return SellAmount;
    }

    /**
     * 
     * @param SellAmount
     *     The SellAmount
     */
    @JsonProperty("SellAmount")
    public void setSellAmount(Double SellAmount) {
        this.SellAmount = SellAmount;
    }

    /**
     * 
     * @return
     *     The CommAmount
     */
    @JsonProperty("CommAmount")
    public Double getCommAmount() {
        return CommAmount;
    }

    /**
     * 
     * @param CommAmount
     *     The CommAmount
     */
    @JsonProperty("CommAmount")
    public void setCommAmount(Double CommAmount) {
        this.CommAmount = CommAmount;
    }

    /**
     * 
     * @return
     *     The TaxAmount
     */
    @JsonProperty("TaxAmount")
    public Double getTaxAmount() {
        return TaxAmount;
    }

    /**
     * 
     * @param TaxAmount
     *     The TaxAmount
     */
    @JsonProperty("TaxAmount")
    public void setTaxAmount(Double TaxAmount) {
        this.TaxAmount = TaxAmount;
    }

    /**
     * 
     * @return
     *     The AmountBeforeTax
     */
    @JsonProperty("AmountBeforeTax")
    public Double getAmountBeforeTax() {
        return AmountBeforeTax;
    }

    /**
     * 
     * @param AmountBeforeTax
     *     The AmountBeforeTax
     */
    @JsonProperty("AmountBeforeTax")
    public void setAmountBeforeTax(Double AmountBeforeTax) {
        this.AmountBeforeTax = AmountBeforeTax;
    }

    /**
     * 
     * @return
     *     The DepAmountDue
     */
    @JsonProperty("DepAmountDue")
    public Double getDepAmountDue() {
        return DepAmountDue;
    }

    /**
     * 
     * @param DepAmountDue
     *     The DepAmountDue
     */
    @JsonProperty("DepAmountDue")
    public void setDepAmountDue(Double DepAmountDue) {
        this.DepAmountDue = DepAmountDue;
    }

    /**
     * 
     * @return
     *     The AmountPosted
     */
    @JsonProperty("AmountPosted")
    public Double getAmountPosted() {
        return AmountPosted;
    }

    /**
     * 
     * @param AmountPosted
     *     The AmountPosted
     */
    @JsonProperty("AmountPosted")
    public void setAmountPosted(Double AmountPosted) {
        this.AmountPosted = AmountPosted;
    }

    /**
     * 
     * @return
     *     The CurrAmountDue
     */
    @JsonProperty("CurrAmountDue")
    public Double getCurrAmountDue() {
        return CurrAmountDue;
    }

    /**
     * 
     * @param CurrAmountDue
     *     The CurrAmountDue
     */
    @JsonProperty("CurrAmountDue")
    public void setCurrAmountDue(Double CurrAmountDue) {
        this.CurrAmountDue = CurrAmountDue;
    }

    /**
     * 
     * @return
     *     The DueDate
     */
    @JsonProperty("DueDate")
    public String getDueDate() {
        return DueDate;
    }

    /**
     * 
     * @param DueDate
     *     The DueDate
     */
    @JsonProperty("DueDate")
    public void setDueDate(String DueDate) {
        this.DueDate = DueDate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
