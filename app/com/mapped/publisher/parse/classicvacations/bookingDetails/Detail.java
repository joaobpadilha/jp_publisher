
package com.mapped.publisher.parse.classicvacations.bookingDetails;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "BeginDate",
    "BeginTime",
    "VendorID",
    "ProductID",
    "PartyName",
    "ArrivalLoc",
    "UseLoc",
    "EndDate",
    "EndTime",
    "VendorConf",
    "VendorName",
    "ProductName",
    "ItemKey",
    "InternalComments",
    "FlightPassengers",
    "FlightSeatMapping",
    "FlightStartAirportCity",
    "FlightEndAirportCity",
    "FlightOperatingAirlines",
    "FlightDuration",
    "FlightClass",
    "HotelPassengers",
    "HotelConditions",
    "HotelAmenities",
    "HotelNoOfGuests",
    "HotelBedding",
    "DurationHours",
    "TourTime",
    "Conditions",
    "DriverPickUpLoc",
    "DetailType",
    "Payment",
    "CancellationPolicy",
    "VendorInfo"
})
public class Detail {

    @JsonProperty("BeginDate")
    private String BeginDate;
    @JsonProperty("BeginTime")
    private String BeginTime;
    @JsonProperty("VendorID")
    private String VendorID;
    @JsonProperty("ProductID")
    private String ProductID;
    @JsonProperty("PartyName")
    private String PartyName;
    @JsonProperty("ArrivalLoc")
    private String ArrivalLoc;
    @JsonProperty("UseLoc")
    private String UseLoc;
    @JsonProperty("EndDate")
    private String EndDate;
    @JsonProperty("EndTime")
    private String EndTime;
    @JsonProperty("VendorConf")
    private String VendorConf;
    @JsonProperty("VendorName")
    private String VendorName;
    @JsonProperty("ProductName")
    private String ProductName;
    @JsonProperty("ItemKey")
    private String ItemKey;
    @JsonProperty("InternalComments")
    private String InternalComments;
    @JsonProperty("FlightPassengers")
    private String FlightPassengers;
    @JsonProperty("FlightSeatMapping")
    private String FlightSeatMapping;
    @JsonProperty("FlightStartAirportCity")
    private String FlightStartAirportCity;
    @JsonProperty("FlightEndAirportCity")
    private String FlightEndAirportCity;
    @JsonProperty("FlightOperatingAirlines")
    private String FlightOperatingAirlines;
    @JsonProperty("FlightDuration")
    private String FlightDuration;
    @JsonProperty("FlightClass")
    private String FlightClass;
    @JsonProperty("HotelPassengers")
    private String HotelPassengers;
    @JsonProperty("HotelConditions")
    private String HotelConditions;
    @JsonProperty("HotelAmenities")
    private String HotelAmenities;
    @JsonProperty("HotelNoOfGuests")
    private String HotelNoOfGuests;
    @JsonProperty("HotelBedding")
    private String HotelBedding;
    @JsonProperty("DurationHours")
    private String DurationHours;
    @JsonProperty("TourTime")
    private String TourTime;
    @JsonProperty("Conditions")
    private String Conditions;
    @JsonProperty("DriverPickUpLoc")
    private String DriverPickUpLoc;
    @JsonProperty("DetailType")
    private String DetailType;
    @JsonProperty("Payment")
    private String Payment;
    @JsonProperty("CancellationPolicy")
    private String CancellationPolicy;
    @JsonProperty("VendorInfo")
    private VendorContact VendorInfo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The BeginDate
     */
    @JsonProperty("BeginDate")
    public String getBeginDate() {
        return BeginDate;
    }

    /**
     * 
     * @param BeginDate
     *     The BeginDate
     */
    @JsonProperty("BeginDate")
    public void setBeginDate(String BeginDate) {
        this.BeginDate = BeginDate;
    }

    /**
     * 
     * @return
     *     The BeginTime
     */
    @JsonProperty("BeginTime")
    public String getBeginTime() {
        return BeginTime;
    }

    /**
     * 
     * @param BeginTime
     *     The BeginTime
     */
    @JsonProperty("BeginTime")
    public void setBeginTime(String BeginTime) {
        this.BeginTime = BeginTime;
    }

    /**
     * 
     * @return
     *     The VendorID
     */
    @JsonProperty("VendorID")
    public String getVendorID() {
        return VendorID;
    }

    /**
     * 
     * @param VendorID
     *     The VendorID
     */
    @JsonProperty("VendorID")
    public void setVendorID(String VendorID) {
        this.VendorID = VendorID;
    }

    /**
     * 
     * @return
     *     The ProductID
     */
    @JsonProperty("ProductID")
    public String getProductID() {
        return ProductID;
    }

    /**
     * 
     * @param ProductID
     *     The ProductID
     */
    @JsonProperty("ProductID")
    public void setProductID(String ProductID) {
        this.ProductID = ProductID;
    }

    /**
     * 
     * @return
     *     The PartyName
     */
    @JsonProperty("PartyName")
    public String getPartyName() {
        return PartyName;
    }

    /**
     * 
     * @param PartyName
     *     The PartyName
     */
    @JsonProperty("PartyName")
    public void setPartyName(String PartyName) {
        this.PartyName = PartyName;
    }

    /**
     * 
     * @return
     *     The ArrivalLoc
     */
    @JsonProperty("ArrivalLoc")
    public String getArrivalLoc() {
        return ArrivalLoc;
    }

    /**
     * 
     * @param ArrivalLoc
     *     The ArrivalLoc
     */
    @JsonProperty("ArrivalLoc")
    public void setArrivalLoc(String ArrivalLoc) {
        this.ArrivalLoc = ArrivalLoc;
    }

    /**
     * 
     * @return
     *     The UseLoc
     */
    @JsonProperty("UseLoc")
    public String getUseLoc() {
        return UseLoc;
    }

    /**
     * 
     * @param UseLoc
     *     The UseLoc
     */
    @JsonProperty("UseLoc")
    public void setUseLoc(String UseLoc) {
        this.UseLoc = UseLoc;
    }

    /**
     * 
     * @return
     *     The EndDate
     */
    @JsonProperty("EndDate")
    public String getEndDate() {
        return EndDate;
    }

    /**
     * 
     * @param EndDate
     *     The EndDate
     */
    @JsonProperty("EndDate")
    public void setEndDate(String EndDate) {
        this.EndDate = EndDate;
    }

    /**
     * 
     * @return
     *     The EndTime
     */
    @JsonProperty("EndTime")
    public String getEndTime() {
        return EndTime;
    }

    /**
     * 
     * @param EndTime
     *     The EndTime
     */
    @JsonProperty("EndTime")
    public void setEndTime(String EndTime) {
        this.EndTime = EndTime;
    }

    /**
     * 
     * @return
     *     The VendorConf
     */
    @JsonProperty("VendorConf")
    public String getVendorConf() {
        return VendorConf;
    }

    /**
     * 
     * @param VendorConf
     *     The VendorConf
     */
    @JsonProperty("VendorConf")
    public void setVendorConf(String VendorConf) {
        this.VendorConf = VendorConf;
    }

    /**
     * 
     * @return
     *     The VendorName
     */
    @JsonProperty("VendorName")
    public String getVendorName() {
        return VendorName;
    }

    /**
     * 
     * @param VendorName
     *     The VendorName
     */
    @JsonProperty("VendorName")
    public void setVendorName(String VendorName) {
        this.VendorName = VendorName;
    }

    /**
     * 
     * @return
     *     The ProductName
     */
    @JsonProperty("ProductName")
    public String getProductName() {
        return ProductName;
    }

    /**
     * 
     * @param ProductName
     *     The ProductName
     */
    @JsonProperty("ProductName")
    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    /**
     * 
     * @return
     *     The ItemKey
     */
    @JsonProperty("ItemKey")
    public String getItemKey() {
        return ItemKey;
    }

    /**
     * 
     * @param ItemKey
     *     The ItemKey
     */
    @JsonProperty("ItemKey")
    public void setItemKey(String ItemKey) {
        this.ItemKey = ItemKey;
    }

    /**
     * 
     * @return
     *     The InternalComments
     */
    @JsonProperty("InternalComments")
    public String getInternalComments() {
        return InternalComments;
    }

    /**
     * 
     * @param InternalComments
     *     The InternalComments
     */
    @JsonProperty("InternalComments")
    public void setInternalComments(String InternalComments) {
        this.InternalComments = InternalComments;
    }

    /**
     * 
     * @return
     *     The FlightPassengers
     */
    @JsonProperty("FlightPassengers")
    public String getFlightPassengers() {
        return FlightPassengers;
    }

    /**
     * 
     * @param FlightPassengers
     *     The FlightPassengers
     */
    @JsonProperty("FlightPassengers")
    public void setFlightPassengers(String FlightPassengers) {
        this.FlightPassengers = FlightPassengers;
    }

    /**
     * 
     * @return
     *     The FlightSeatMapping
     */
    @JsonProperty("FlightSeatMapping")
    public String getFlightSeatMapping() {
        return FlightSeatMapping;
    }

    /**
     * 
     * @param FlightSeatMapping
     *     The FlightSeatMapping
     */
    @JsonProperty("FlightSeatMapping")
    public void setFlightSeatMapping(String FlightSeatMapping) {
        this.FlightSeatMapping = FlightSeatMapping;
    }

    /**
     * 
     * @return
     *     The FlightStartAirportCity
     */
    @JsonProperty("FlightStartAirportCity")
    public String getFlightStartAirportCity() {
        return FlightStartAirportCity;
    }

    /**
     * 
     * @param FlightStartAirportCity
     *     The FlightStartAirportCity
     */
    @JsonProperty("FlightStartAirportCity")
    public void setFlightStartAirportCity(String FlightStartAirportCity) {
        this.FlightStartAirportCity = FlightStartAirportCity;
    }

    /**
     * 
     * @return
     *     The FlightEndAirportCity
     */
    @JsonProperty("FlightEndAirportCity")
    public String getFlightEndAirportCity() {
        return FlightEndAirportCity;
    }

    /**
     * 
     * @param FlightEndAirportCity
     *     The FlightEndAirportCity
     */
    @JsonProperty("FlightEndAirportCity")
    public void setFlightEndAirportCity(String FlightEndAirportCity) {
        this.FlightEndAirportCity = FlightEndAirportCity;
    }

    /**
     * 
     * @return
     *     The FlightOperatingAirlines
     */
    @JsonProperty("FlightOperatingAirlines")
    public Object getFlightOperatingAirlines() {
        return FlightOperatingAirlines;
    }

    /**
     * 
     * @param FlightOperatingAirlines
     *     The FlightOperatingAirlines
     */
    @JsonProperty("FlightOperatingAirlines")
    public void setFlightOperatingAirlines(String FlightOperatingAirlines) {
        this.FlightOperatingAirlines = FlightOperatingAirlines;
    }

    /**
     * 
     * @return
     *     The FlightDuration
     */
    @JsonProperty("FlightDuration")
    public String getFlightDuration() {
        return FlightDuration;
    }

    /**
     * 
     * @param FlightDuration
     *     The FlightDuration
     */
    @JsonProperty("FlightDuration")
    public void setFlightDuration(String FlightDuration) {
        this.FlightDuration = FlightDuration;
    }

    /**
     * 
     * @return
     *     The FlightClass
     */
    @JsonProperty("FlightClass")
    public String getFlightClass() {
        return FlightClass;
    }

    /**
     * 
     * @param FlightClass
     *     The FlightClass
     */
    @JsonProperty("FlightClass")
    public void setFlightClass(String FlightClass) {
        this.FlightClass = FlightClass;
    }

    /**
     * 
     * @return
     *     The HotelPassengers
     */
    @JsonProperty("HotelPassengers")
    public String getHotelPassengers() {
        return HotelPassengers;
    }

    /**
     * 
     * @param HotelPassengers
     *     The HotelPassengers
     */
    @JsonProperty("HotelPassengers")
    public void setHotelPassengers(String HotelPassengers) {
        this.HotelPassengers = HotelPassengers;
    }

    /**
     * 
     * @return
     *     The HotelConditions
     */
    @JsonProperty("HotelConditions")
    public String getHotelConditions() {
        return HotelConditions;
    }

    /**
     * 
     * @param HotelConditions
     *     The HotelConditions
     */
    @JsonProperty("HotelConditions")
    public void setHotelConditions(String HotelConditions) {
        this.HotelConditions = HotelConditions;
    }

    /**
     * 
     * @return
     *     The HotelAmenities
     */
    @JsonProperty("HotelAmenities")
    public String getHotelAmenities() {
        return HotelAmenities;
    }

    /**
     * 
     * @param HotelAmenities
     *     The HotelAmenities
     */
    @JsonProperty("HotelAmenities")
    public void setHotelAmenities(String HotelAmenities) {
        this.HotelAmenities = HotelAmenities;
    }

    /**
     * 
     * @return
     *     The HotelNoOfGuests
     */
    @JsonProperty("HotelNoOfGuests")
    public String getHotelNoOfGuests() {
        return HotelNoOfGuests;
    }

    /**
     * 
     * @param HotelNoOfGuests
     *     The HotelNoOfGuests
     */
    @JsonProperty("HotelNoOfGuests")
    public void setHotelNoOfGuests(String HotelNoOfGuests) {
        this.HotelNoOfGuests = HotelNoOfGuests;
    }

    /**
     * 
     * @return
     *     The HotelBedding
     */
    @JsonProperty("HotelBedding")
    public String getHotelBedding() {
        return HotelBedding;
    }

    /**
     * 
     * @param HotelBedding
     *     The HotelBedding
     */
    @JsonProperty("HotelBedding")
    public void setHotelBedding(String HotelBedding) {
        this.HotelBedding = HotelBedding;
    }

    /**
     * 
     * @return
     *     The DurationHours
     */
    @JsonProperty("DurationHours")
    public String getDurationHours() {
        return DurationHours;
    }

    /**
     * 
     * @param DurationHours
     *     The DurationHours
     */
    @JsonProperty("DurationHours")
    public void setDurationHours(String DurationHours) {
        this.DurationHours = DurationHours;
    }

    /**
     * 
     * @return
     *     The TourTime
     */
    @JsonProperty("TourTime")
    public String getTourTime() {
        return TourTime;
    }

    /**
     * 
     * @param TourTime
     *     The TourTime
     */
    @JsonProperty("TourTime")
    public void setTourTime(String TourTime) {
        this.TourTime = TourTime;
    }

    /**
     * 
     * @return
     *     The Conditions
     */
    @JsonProperty("Conditions")
    public String getConditions() {
        return Conditions;
    }

    /**
     * 
     * @param Conditions
     *     The Conditions
     */
    @JsonProperty("Conditions")
    public void setConditions(String Conditions) {
        this.Conditions = Conditions;
    }

    /**
     * 
     * @return
     *     The DriverPickUpLoc
     */
    @JsonProperty("DriverPickUpLoc")
    public String getDriverPickUpLoc() {
        return DriverPickUpLoc;
    }

    /**
     * 
     * @param DriverPickUpLoc
     *     The DriverPickUpLoc
     */
    @JsonProperty("DriverPickUpLoc")
    public void setDriverPickUpLoc(String DriverPickUpLoc) {
        this.DriverPickUpLoc = DriverPickUpLoc;
    }

    /**
     * 
     * @return
     *     The DetailType
     */
    @JsonProperty("DetailType")
    public String getDetailType() {
        return DetailType;
    }

    /**
     * 
     * @param DetailType
     *     The DetailType
     */
    @JsonProperty("DetailType")
    public void setDetailType(String DetailType) {
        this.DetailType = DetailType;
    }

    /**
     * 
     * @return
     *     The Payment
     */
    @JsonProperty("Payment")
    public String getPayment() {
        return Payment;
    }

    /**
     * 
     * @param Payment
     *     The Payment
     */
    @JsonProperty("Payment")
    public void setPayment(String Payment) {
        this.Payment = Payment;
    }

    /**
     * 
     * @return
     *     The CancellationPolicy
     */
    @JsonProperty("CancellationPolicy")
    public String getCancellationPolicy() {
        return CancellationPolicy;
    }

    /**
     * 
     * @param CancellationPolicy
     *     The CancellationPolicy
     */
    @JsonProperty("CancellationPolicy")
    public void setCancellationPolicy(String CancellationPolicy) {
        this.CancellationPolicy = CancellationPolicy;
    }

    /**
     * 
     * @return
     *     The VendorInfo
     */
    @JsonProperty("VendorInfo")
    public VendorContact getVendorInfo() {
        return VendorInfo;
    }

    /**
     * 
     * @param VendorInfo
     *     The VendorInfo
     */
    @JsonProperty("VendorInfo")
    public void setVendorInfo(VendorContact VendorInfo) {
        this.VendorInfo = VendorInfo;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
