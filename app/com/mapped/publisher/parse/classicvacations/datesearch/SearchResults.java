
package com.mapped.publisher.parse.classicvacations.datesearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "Booking",
    "Exception",
    "TimeStamp"
})
public class SearchResults {

    @JsonProperty("Booking")
    private List<com.mapped.publisher.parse.classicvacations.datesearch.Booking> Booking = new ArrayList<com.mapped.publisher.parse.classicvacations.datesearch.Booking>();
    @JsonProperty("Exception")
    private Object Exception;
    @JsonProperty("TimeStamp")
    private String TimeStamp;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The Booking
     */
    @JsonProperty("Booking")
    public List<com.mapped.publisher.parse.classicvacations.datesearch.Booking> getBooking() {
        return Booking;
    }

    /**
     * 
     * @param Booking
     *     The Booking
     */
    @JsonProperty("Booking")
    public void setBooking(List<com.mapped.publisher.parse.classicvacations.datesearch.Booking> Booking) {
        this.Booking = Booking;
    }

    /**
     * 
     * @return
     *     The Exception
     */
    @JsonProperty("Exception")
    public Object getException() {
        return Exception;
    }

    /**
     * 
     * @param Exception
     *     The Exception
     */
    @JsonProperty("Exception")
    public void setException(Object Exception) {
        this.Exception = Exception;
    }

    /**
     * 
     * @return
     *     The TimeStamp
     */
    @JsonProperty("TimeStamp")
    public String getTimeStamp() {
        return TimeStamp;
    }

    /**
     * 
     * @param TimeStamp
     *     The TimeStamp
     */
    @JsonProperty("TimeStamp")
    public void setTimeStamp(String TimeStamp) {
        this.TimeStamp = TimeStamp;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
