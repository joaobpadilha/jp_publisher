package com.mapped.publisher.parse.virtuoso.cruise;

import com.mapped.publisher.parse.virtuoso.cruise.v5_0.Benefit;
import org.apache.commons.lang3.StringEscapeUtils;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by twong on 15-05-19.
 */
public class CruiseStopVO {
  private String cruiseId;
  private int day;
  private Timestamp startTimestamp;
  private Timestamp endTimestamp;
  private String name; //Will require escaping HTML characters
  private float latitude;
  private float longitude;
  private String note;
  private Boolean cruiseStop = true;
  private List<Benefit> benefits;

  public String getCruiseId() {
    return cruiseId;
  }

  public void setCruiseId(String cruiseId) {
    this.cruiseId = cruiseId;
  }

  public int getDay() {
    return day;
  }

  public void setDay(int day) {
    this.day = day;
  }

  public Timestamp getStartTimestamp() {
    return startTimestamp;
  }

  public void setStartTimestamp(Timestamp startTimestamp) {
    this.startTimestamp = startTimestamp;
  }

  public Timestamp getEndTimestamp() {
    return endTimestamp;
  }

  public void setEndTimestamp(Timestamp endTimestamp) {
    this.endTimestamp = endTimestamp;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    if (name != null) {
      this.name =  StringEscapeUtils.unescapeHtml4(name);
    }
  }

  public float getLatitude() {
    return latitude;
  }

  public void setLatitude(float latitude) {
    this.latitude = latitude;
  }

  public float getLongitude() {
    return longitude;
  }

  public void setLongitude(float longitude) {
    this.longitude = longitude;
  }

  public String getNote() {

    return note;
  }

  public void setNote(String note) {
    if (note != null) {
      this.note =  StringEscapeUtils.unescapeHtml4(name);
    }
  }

  public Boolean getCruiseStop() {
    return cruiseStop;
  }

  public void setCruiseStop(Boolean cruiseStop) {
    this.cruiseStop = cruiseStop;
  }

  public List<Benefit> getBenefits() {
    return benefits;
  }

  public void setBenefits(List<Benefit> benefits) {
    this.benefits = benefits;
  }
}
