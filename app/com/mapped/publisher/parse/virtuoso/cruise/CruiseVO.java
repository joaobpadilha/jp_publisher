package com.mapped.publisher.parse.virtuoso.cruise;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.parse.virtuoso.cruise.v5_0.Benefit;
import com.mapped.publisher.parse.virtuoso.cruise.v5_0.Root.Cruise.CruiseInfo.Promotions.Promotion;
import com.mapped.publisher.utils.Utils;
import models.publisher.TmpltImport;
import models.publisher.TmpltType;
import org.apache.commons.lang3.StringEscapeUtils;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

/**
 * Created by twong on 15-05-19.
 */
public class CruiseVO {
  private String cruiseId;
  private String virtuosoUrl;
  private CruiseShipVO cruiseShip;
  private Timestamp startDate;
  private Timestamp endDate;
  private String cruiseName;
  private String cruiseDescription;
  private String startLocation;
  private String endLocation;
  private String duration;
  private String includedAmenities;
  private String excludedAmenities;
  private String description;
  private String termsAndCondition;
  private List<Promotion> promotions;
  private List<Benefit> benefits;
  private String bookingInstructions;
  private List<CruiseStopVO> cruiseStops;
  private String note;

  public String getCruiseId() {
    return cruiseId;
  }

  public void setCruiseId(String cruiseId) {
    this.cruiseId = cruiseId;
  }

  public String getVirtuosoUrl() {
    return virtuosoUrl;
  }

  public void setVirtuosoUrl(String virtuosoUrl) {
    this.virtuosoUrl = virtuosoUrl;
  }

  public CruiseShipVO getCruiseShip() {
    return cruiseShip;
  }

  public void setCruiseShip(CruiseShipVO cruiseShip) {
    this.cruiseShip = cruiseShip;
  }

  public Timestamp getStartDate() {
    return startDate;
  }

  public void setStartDate(Timestamp startDate) {
    this.startDate = startDate;
  }

  public Timestamp getEndDate() {
    return endDate;
  }

  public void setEndDate(Timestamp endDate) {
    this.endDate = endDate;
  }

  public String getCruiseName() {
    return cruiseName;
  }

  public void setCruiseName(String cruiseName) {
    if (cruiseName != null) {
      this.cruiseName =  StringEscapeUtils.unescapeHtml4(cruiseName);
    }
  }

  public String getCruiseDescription() {
    return cruiseDescription;
  }

  public void setCruiseDescription(String cruiseDescription) {
    if (cruiseDescription != null) {
      this.cruiseDescription =  StringEscapeUtils.unescapeHtml4(cruiseDescription);
    }
  }

  public String getStartLocation() {
    return startLocation;
  }

  public void setStartLocation(String startLocation) {
    this.startLocation = startLocation;
  }

  public String getEndLocation() {
    return endLocation;
  }

  public void setEndLocation(String endLocation) {
    this.endLocation = endLocation;
  }

  public String getDuration() {
    return duration;
  }

  public void setDuration(String duration) {
    this.duration = duration;
  }

  public String getIncludedAmenities() {
    return includedAmenities;
  }

  public void setIncludedAmenities(String includedAmenities) {
    this.includedAmenities = includedAmenities;
  }

  public String getExcludedAmenities() {
    return excludedAmenities;
  }

  public void setExcludedAmenities(String excludedAmenities) {
    this.excludedAmenities = excludedAmenities;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    if (description != null) {
      this.description =  StringEscapeUtils.unescapeHtml4(description);
    }
  }

  public String getTermsAndCondition() {
    return termsAndCondition;
  }

  public void setTermsAndCondition(String termsAndCondition) {
    this.termsAndCondition = termsAndCondition;
  }

  public List<Promotion> getPromotions() {
    return promotions;
  }

  public void setPromotions(List<Promotion> promotions) {
    this.promotions = promotions;
  }

  public List<Benefit> getBenefits() {
    return benefits;
  }

  public void setBenefits(List<Benefit> benefits) {
    this.benefits = benefits;
  }

  public String getBookingInstructions() {
    return bookingInstructions;
  }

  public void setBookingInstructions(String bookingInstructions) {
    this.bookingInstructions = bookingInstructions;
  }

  public List<CruiseStopVO> getCruiseStops() {
    return cruiseStops;
  }

  public void setCruiseStops(List<CruiseStopVO> cruiseStops) {
    this.cruiseStops = cruiseStops;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    if (note != null) {
      this.note =  StringEscapeUtils.unescapeHtml4(note);
    }
  }

  public String toJson () {
    try {
      ObjectMapper mapper = new ObjectMapper();
      return mapper.writeValueAsString(this);

    } catch (Exception e) {

    }

    return null;
  }

  public long save () {

    TmpltImport tmpltImport = null;

    try {
      List<TmpltImport> ti = TmpltImport.findBySrcReferenceId(cruiseId, TmpltType.Type.CRUISE_VIRTUOSO);
      String json = this.toJson();
      byte[] hash = Utils.hash(json).getBytes();

      if (ti != null && ti.size() > 0) {
        tmpltImport = ti.get(0);
        //if it has not changed, don;t bother saving
        if (Arrays.equals(hash, tmpltImport.getHash())) {
          return 0;
        }

      }
      else {
        tmpltImport = new TmpltImport();
        tmpltImport.setImportId(DBConnectionMgr.getUniqueLongId());
      }

      tmpltImport.setName(cruiseName);
      tmpltImport.setRaw(json);
      tmpltImport.setSrcReferenceId(cruiseId);
      tmpltImport.setSrcUrl(virtuosoUrl);
      tmpltImport.setStartTs(startDate.getTime());
      tmpltImport.setImportTs(System.currentTimeMillis());
      tmpltImport.setHash(hash);
      tmpltImport.setFinishTs(endDate.getTime());
      tmpltImport.setTmplt_type(TmpltType.Type.CRUISE_VIRTUOSO);
      tmpltImport.setTmpltId(null);
      tmpltImport.setSyncTs(null);
      tmpltImport.save();
    } catch (Exception e) {
      System.out.println(cruiseId + " - " + cruiseName + " - " + virtuosoUrl);
    }

    return tmpltImport.getImportId();
  }

}
