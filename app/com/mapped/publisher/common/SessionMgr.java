package com.mapped.publisher.common;

import models.publisher.Account;
import play.mvc.Http;
import play.mvc.Http.Session;

import java.util.Optional;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-14
 * Time: 8:57 AM
 */
public class SessionMgr {
  public final static String CONTEXT_KEY = "smgr";
  private Session session;
  private boolean sessionIdMismatch = false;
  private boolean sessionExpired    = false;
  private Credentials credentials;

  public SessionMgr(Http.Session pSession) {
    this.session = pSession;
    credentials = null;
  }

  public static SessionMgr fromContext(Http.Context ctx) {
    return (SessionMgr) ctx.args.get(CONTEXT_KEY);
  }

  public String getParam(String name) {
    if (name != null && name.startsWith("p_")) {
      return session.get(name);
    }
    else {
      return null;
    }
  }

  public boolean setParam(String name, String value) {
    if (name != null && value != null && name.startsWith("p_")) {
      session.put(name, value);
      return true;
    }
    else {
      return false;
    }
  }

  public Boolean isSessionValidLite() {
    long expiry = getSessionExpiry();
    if (expiry < System.currentTimeMillis()) {
      logout();
      return false;
    }
    setSessionExpiry();
    return true;
  }

  public long getSessionExpiry() {
    String s = session.get(SessionConstants.SESSION_CORE_EXPIRY);
    if (s == null) {
      return 0;
    }
    try {
      return Long.parseLong(s);
    }
    catch (NumberFormatException ne) {
      ne.printStackTrace();
    }
    return 0;
  }

  public void logout() {
    try {
      //clean up cached parsers for the users
      CacheMgr.set(APPConstants.CACHE_USER_API_PARSERS + getAccountId(), null, APPConstants.CACHE_EXPIRY_SECS);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    session.put(SessionConstants.SESSION_CORE_ACCOUNT_ID, null);
    session.put(SessionConstants.SESSION_CORE_USER_ID, null);
    session.put(SessionConstants.SESSION_CORE_EXPIRY, null);
    session.put(SessionConstants.SESSION_CORE_ID, null);
    session.clear();
  }

  /**
   * @return new expiry timestamp
   */
  public long setSessionExpiry() {
    long expiry = System.currentTimeMillis() + APPConstants.SESSION_EXPIRY;
    session.put(SessionConstants.SESSION_CORE_EXPIRY, String.valueOf(expiry));
    return expiry;
  }

  public Long getAccountId() {
    try {
      return Long.parseLong(session.get(SessionConstants.SESSION_CORE_ACCOUNT_ID));
    } catch (Exception e) {}
    return Account.AID_UNDEFINED;
  }

  public Optional<Account> getAccount() {
    return Optional.ofNullable(Account.find.byId(getAccountId()));
  }

  public void setAccountId(Long aid) {
    session.put(SessionConstants.SESSION_CORE_ACCOUNT_ID, aid.toString());
  }

  public Boolean isSessionValid() {
    long   expiry    = getSessionExpiry();
    String sessionId = session.get(SessionConstants.SESSION_CORE_ID);

    if (expiry == 0 || sessionId == null) {
      return false;
    }

    if (expiry < System.currentTimeMillis()) {
      logout();
      return false;
    }

    Credentials cred = getCredentials();
    //If cred is null this means it is either expired or this is the first acess
    if(cred != null) {
      if (cred.getSessionId() != null && !cred.getSessionId().equals(sessionId)) {
        sessionIdMismatch = true;
      }
      else if (cred.getExpiry() < System.currentTimeMillis()) {
        sessionExpired = true;
      }

      if (!sessionExpired && !sessionIdMismatch) {

        //If we are logging in within 1 hour of expiry - update expiry for another term
        if (cred.getExpiry() - System.currentTimeMillis() < 3_600_000) {
          expiry = setSessionExpiry(); //new expiry value
          cred.setExpiry(expiry);
          cred.updateCache();
        }

        return true;
      }
    }

    logout();
    return false;
  }

  public Credentials getCredentials() {
    if (credentials == null) {
      credentials = Credentials.getFromCache(getAccountId());
    }
    return credentials;
  }

  public Optional<Credentials> getOptionalCredentials() {
    return Optional.ofNullable(getCredentials());
  }

  public String getUserId() {
    return session.get(SessionConstants.SESSION_CORE_USER_ID);
  }

  public String setUserId(String userId) {
    return session.put(SessionConstants.SESSION_CORE_USER_ID, userId);
  }

  public void setAccountId(String uid) {
    session.put(SessionConstants.SESSION_CORE_USER_ID, uid);
  }

  public Boolean isSessionIdMismatch() {
    return sessionIdMismatch;
  }

  public Boolean isSessionExpired() {
    return sessionExpired;
  }

  public void setSessionId(String sessionId) {
    session.put(SessionConstants.SESSION_CORE_ID, sessionId);
  }

  public int getTimezoneOffsetMins() {
    String s = session.get(SessionConstants.SESSION_CORE_TIMEZONE_OFFSET_MINS);
    if (s != null) {
      try {
        return Integer.parseInt(s);
      }
      catch (NumberFormatException ne) {
        ne.printStackTrace();
      }
    }
    return 0;
  }

  public void setTimezoneOffsetMins(int i) {
    i = i * -1;
    session.put(SessionConstants.SESSION_CORE_TIMEZONE_OFFSET_MINS, String.valueOf(i));
  }
}
