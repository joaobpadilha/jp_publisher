package com.mapped.publisher.common;

/**
 * Created by twong on 15-06-30.
 */
public class BookingSrc {
  public static enum ImportSrc {
    UMAPPED_TEMPLATE,
    EMAIL_PDF, //generic fallback type
    EMAIL_TEXT, //generic fallback type
    REC_LOCATOR, //generic fallback type
    UPLOAD_PDF,
    SABRE_PDF,
    SABRE_TRIPCASE,
    VIRTUOSO_AIR,
    AMADEUS_PDF,
    AMADEUS_CHECKMYTRIPS,
    TRAVELPORT__VIEWTRIPS,
    WORLDSPAN_EMAIL,
    WORLDSPAN_TRAMS,
    WORLDSPAN_MYTRIPS,
    CLIENT_BASE_PDF,
    VIRTUOSO_CRUISE,
    SHORE_TRIPS,
    CLASSIC_VACATIONS,
    TA_SOLFERIAS_PDF,
    TA_SOLTOURS_PDF,
    TA_NONIUS_PDF,
    TA_NORTRAVEL_PDF,
    CENTRAV,
    APOLLO,
    QUEEN_OF_CLUB,
    WORLDMATE,
    SCHEMA_ORG,
    BIG_FIVE,
    GLOBUS_TOUR,
    TRAVEL42_DESTINATION,
    TRAVEL42_GUIDE,
    TRAVEL42_POI,
    TRAVEL42_HOTEL,
    API,
    AEROPLAN_PDF,
    TRAVEL2,
    TRAVEL2_QANTAS,
    TRAVEL2_ISLANDS,
    TRAMADA,
    ALPINE,
    TRAVELBOUND,
    GOGO,
    EXTERNAL_SITE,
    OPEN_SKIES_PDF
  }
}
