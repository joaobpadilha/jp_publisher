package com.mapped.publisher.common;

import com.avaje.ebean.annotation.EnumValue;
import com.firebase.security.token.TokenGenerator;
import com.firebase.security.token.TokenOptions;
import models.publisher.Account;
import models.publisher.AccountCmpyLink;
import models.publisher.Company;

import java.io.Serializable;
import java.util.*;

import static models.publisher.AccountCmpyLink.LinkType.SUSPENDED;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-23
 * Time: 12:11 PM
 */
public class Credentials
    implements Serializable {

  public enum BillingStatus {
    /**
     * UNKNOWN
     */
    @EnumValue(value = "UNK")
    UNKNOWN,
    /**
     * INVOICE BILLING
     */
    @EnumValue(value = "INV")
    INVOICE_BILLING,
    /**
     * AGENT BILLING
     */
    @EnumValue(value = "AGB")
    AGENT_BILLING,
    /**
     * CREDIT CARD OK
     */
    @EnumValue(value = "COK")
    CREDIT_CARD_OK,
    /**
     * AGENT SUBSCRIPTION NEEDED
     */
    @EnumValue(value = "ASN")
    AGENT_SUBSCRIPTION_NOT_SETUP,
    /**
     * CREDIT CARD EXPIRING
     */
    @EnumValue(value = "CEX")
    CREDIT_CARD_EXPIRING,
    /**
     * CREDIT CARD INFO needs to be updated
     */
    @EnumValue(value = "CCU")
    CREDIT_CARD_UPDATE_NEEDED,
    /**
     * Credit card info needs to be setup
     */
    @EnumValue(value = "CCN")
    CREDIT_CARD_SETUP_NEEDED
  }
  /**
   * Timestamp when the object was created
   */
  private long                                 createdTs;
  /**
   * Play Session ID
   */
  private String                               sessionId;
  /**
   * Main account information
   */
  private Account                              account;
  /**
   * When credentials expire
   */
  private Long                                 expiry;
  /**
   * All capabilities that user can work with
   */
  private EnumSet<Capability>                  capabilities;
  /**
   * Company associated with the account
   *
   * @return
   */
  private String                               cmpyId;
  /**
   * Company identifier used by POI
   */
  private int                                  cmpyIdInt;
  /**
   * Company name is frequently used in the views
   */
  private String                               cmpyName;
  /**
   * Company association level
   *
   * @return
   */
  private AccountCmpyLink.LinkType             cmpyLinkType;
  /**
   * Map of grous that user belongs to users ids in each of the groups
   * TODO: Serguei: Replace with Account IDs for users in the future
   */
  private Map<String, Set<String>>             userGroups;
  /**
   * Shared trips: TripID to Access Level
   */
  private Map<String, SecurityMgr.AccessLevel> sharedTrips;

  /*
   Prevent access if this flag is set
   */
  private BillingStatus billingStatus;

  /*
   Prevent access if this flag is set - redirect to subscription page
   */
  private boolean subscribedToBilling = true;

  /*
    Flag to indicate that this session needs to charge for every published trip
  */
  private boolean perTripBilling = false;

  //unsure variable to force session invalidation
  //change the name and the class will not longer be the same on next deployment
  // all cached sessions will be removed
  private String invalidateCache_1_44;


  public static Credentials buildCredentials(Account a) {
    Credentials c = new Credentials();
    c.account = a;
    c.createdTs = System.currentTimeMillis();
    c.sharedTrips = new HashMap<>();
    c.userGroups = new HashMap<>();
    c.cmpyIdInt = Company.NO_COMPANY_ID;
    c.cmpyId = "";
    c.cmpyName = "";
    c.cmpyLinkType = SUSPENDED;
    return c;
  }

  public static Credentials getFromCache(Long accountId) {
    try {
      Credentials cred = (Credentials) CacheMgr.get(APPConstants.CACHE_SESSION_PREFIX + accountId);
      if (cred != null && cred.getExpiry() > System.currentTimeMillis()) {
        return cred;
      }
    } catch (Exception e) {} //Don't want to bother anyone with messages that Credentials changed.
    return null;
  }

  public Long getExpiry() {
    return expiry;
  }

  public Credentials setExpiry(Long expiry) {
    this.expiry = expiry;
    return this;
  }

  public String getCmpyName() {
    return cmpyName;
  }

  public boolean hasCompany() {
    return cmpyIdInt != Company.NO_COMPANY_ID;
  }

  public boolean hasGroupies() {
    for (Set<String> us : userGroups.values()) {
      if (us.size() > 0) {
        return true;
      }
    }
    return false;
  }

  public boolean isInGroupWith(String userId) {
    for (Set<String> gs : userGroups.values()) {
      if (gs.contains(userId)) {
        return true;
      }
    }
    return false;
  }

  public Map<String, Set<String>> getUserGroups() {
    return userGroups;
  }

  public Credentials addGroupId(String groupId) {
    if (!userGroups.containsKey(groupId)) {
      userGroups.put(groupId, new HashSet<>());
    }
    return this;
  }

  public Credentials addGroupUser(String groupId, String userId) {
    Set<String> gus = userGroups.get(groupId);
    if (gus == null) {
      gus = new HashSet<>();
      userGroups.put(groupId, gus);
    }
    gus.add(userId);
    return this;
  }

  public boolean isCmpyMember(int cmpyId) {
    return cmpyIdInt == cmpyId;
  }

  public boolean isCmpyMember(String cmpyId) {
    return this.cmpyId.equals(cmpyId);
  }

  public boolean isCmpyAdmin(int cmpyId) {
    return cmpyIdInt == cmpyId && cmpyLinkType == AccountCmpyLink.LinkType.ADMIN;
  }

  public boolean isCmpyAdmin(String cmpyId) {
    return cmpyLinkType == AccountCmpyLink.LinkType.ADMIN && this.cmpyId.equals(cmpyId);
  }

  public boolean isCmpyAdmin() {
    return cmpyLinkType == AccountCmpyLink.LinkType.ADMIN && this.cmpyIdInt != Company.NO_COMPANY_ID;
  }

  public boolean isCmpyPowerMember(int cmpyId) {
    return cmpyLinkType == AccountCmpyLink.LinkType.POWER && this.cmpyIdInt == cmpyId;
  }

  public boolean isCmpyPowerMember(String cmpyId) {
    return cmpyLinkType == AccountCmpyLink.LinkType.POWER && this.cmpyId.equals(cmpyId);
  }

  public boolean isCmpyPowerMember() {
    return cmpyLinkType == AccountCmpyLink.LinkType.POWER &&
           this.cmpyIdInt != Company.NO_COMPANY_ID;
  }

  public boolean isInSharedTrip(String tripId) {
    return sharedTrips.containsKey(tripId);
  }

  public SecurityMgr.AccessLevel getSharedTripAccessLevel(String tripId) {
    SecurityMgr.AccessLevel al = sharedTrips.get(tripId);
    if (al == null) {
      return SecurityMgr.AccessLevel.NONE;
    }
    return al;
  }

  public Credentials setCompany(Company cmpy, AccountCmpyLink.LinkType linkType) {
    this.cmpyId = cmpy.getCmpyid();
    this.cmpyIdInt = cmpy.getCmpyId();
    this.cmpyLinkType = linkType;
    this.cmpyName = cmpy.getName();
    return this;
  }

  public Company getCompany() {
    if(cmpyIdInt == Company.NO_COMPANY_ID) {
      return null;
    }
    return Company.findByCmpyID(cmpyIdInt);
  }

  public Credentials becomeAnAgent() {
    this.account.setAccountType(Account.AccountType.PUBLISHER);
    return this;
  }

  public int getCmpyIdInt() {
    return cmpyIdInt;
  }

  public Credentials setCmpyIdInt(int cmpyIdInt) {
    this.cmpyIdInt = cmpyIdInt;
    return this;
  }

  public Long getAccountId() {
    return account.getUid();
  }

  public Account getAccount() {
    return account;
  }

  public Credentials setAccount(Account account) {
    this.account = account;
    return this;
  }

  public String getCmpyId() {
    return cmpyId;
  }

  public Credentials setCmpyId(String cmpyId) {
    this.cmpyId = cmpyId;
    return this;
  }

  public AccountCmpyLink.LinkType getCmpyLinkType() {
    return cmpyLinkType;
  }

  public Credentials setCmpyLinkType(AccountCmpyLink.LinkType cmpyLinkType) {
    this.cmpyLinkType = cmpyLinkType;
    return this;
  }

  public String getUserId() {
    return account.getLegacyId();
  }

  public String getFirstName() {
    return account.getFirstName();
  }

  public String getLastName() {
    return account.getLastName();
  }

  public boolean isUmappedAdmin() {
    return account.getAccountType() == Account.AccountType.UMAPPED_ADMIN;
  }

  public long getCreatedTs() {
    return createdTs;
  }

  public boolean hasCapability(Capability cap) {
    return capabilities.contains(cap);
  }

  public EnumSet<Capability> getCapabilities() {
    if (capabilities == null) {
      return EnumSet.noneOf(Capability.class);
    }
    return capabilities;
  }

  public Credentials setCapabilities(EnumSet<Capability> capabilities) {
    this.capabilities = capabilities;
    return this;
  }

  public void fullyCapable() {
    capabilities = EnumSet.allOf(Capability.class);
  }

  public void incapable() {
    capabilities = EnumSet.noneOf(Capability.class);
  }

  public Map<String, SecurityMgr.AccessLevel> getSharedTrips() {
    return sharedTrips;
  }

  public Credentials setSharedTrips(Map<String, SecurityMgr.AccessLevel> sharedTrips) {
    this.sharedTrips = sharedTrips;
    return this;
  }

  public void addSharedTrip(String tripId, SecurityMgr.AccessLevel accessLevel) {

    if (tripId != null && accessLevel != null && accessLevel != SecurityMgr.AccessLevel.NONE) {
      sharedTrips.put(tripId, accessLevel);
    }
  }

  public void removeSharedTrip(String tripId) {
    if (sharedTrips != null) {
      sharedTrips.remove(tripId);
    }
  }

  public void changeSharedTripAccessLevel(String tripId, SecurityMgr.AccessLevel accessLevel) {
    if (sharedTrips != null) {
      sharedTrips.put(tripId, accessLevel);
    }
  }

  public String getSessionId() {
    return sessionId;
  }

  public Credentials setSessionId(String sessionId) {
    this.sessionId = sessionId;
    return this;
  }

  public String getFirebaseToken() {
    return getFirebaseToken(expiry);
  }

  public String getFirebaseToken(long expiryTs) {
    Map<String, Object> authPayload = new HashMap<>();
    authPayload.put("uid", account.getLegacyId().replace('.', '_')); //TODO: To be replaced
    authPayload.put("cmpyid", getMainCmpy());
    authPayload.put("expiry", expiryTs);
    authPayload.put("displayName", account.getFullName());

    TokenOptions tokenOptions = new TokenOptions();
    tokenOptions.setExpires(new Date(expiryTs));
    //tokenOptions.setExpires(new Date(Long.MAX_VALUE)); //Debug way to get a long running token

    TokenGenerator tokenGenerator = new TokenGenerator(ConfigMgr.getAppParameter(com.mapped.common.CoreConstants
                                                                                     .FIREBASE_SECRET));
    return tokenGenerator.createToken(authPayload, tokenOptions);
  }

  public String getMainCmpy() {
    return cmpyId;
  }

  public Credentials updateCache() {
    CacheMgr.set(APPConstants.CACHE_SESSION_PREFIX + account.getUid(), this, APPConstants.SESSION_EXPIRY_SECS);
    return this;
  }

  /**
   * Synchronous cache write
   * @return
   */
  public Credentials saveCache() {
    CacheMgr.setBlocking(APPConstants.CACHE_SESSION_PREFIX + account.getUid(), this, APPConstants.SESSION_EXPIRY_SECS);
    return this;
  }

  public BillingStatus getBillingStatus() {
    return billingStatus;
  }

  public Credentials setBillingStatus(BillingStatus billingStatus) {
    this.billingStatus = billingStatus;
    return this;
  }

  public boolean isSubscribedToBilling() {
    return subscribedToBilling;
  }

  public void setSubscribedToBilling(boolean subscribedToBilling) {
    this.subscribedToBilling = subscribedToBilling;
  }

  public boolean isPerTripBilling() {
    return perTripBilling;
  }

  public void setPerTripBilling(boolean perTripBilling) {
    this.perTripBilling = perTripBilling;
  }
}
