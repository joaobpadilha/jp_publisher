package com.mapped.publisher.common;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-14
 * Time: 9:01 AM
 * To change this template use File | Settings | File Templates.
 */
public class SessionConstants {
  public final static String SESSION_CORE_ID = "s_session_id";
  public final static String SESSION_CORE_EXPIRY               = "s_expiry";
  public final static String SESSION_CORE_ACCOUNT_ID           = "s_aid";
  public final static String SESSION_CORE_USER_ID              = "s_userid";
  public final static String SESSION_CORE_TIMEZONE_OFFSET_MINS = "s_timezoneoffset";
  public final static String SESSION_PARAM_TRIPID              = "p_tripid";
  public final static String SESSION_PARAM_USERID              = "p_userid";
  public final static String SESSION_PARAM_KEYWORD             = "p_keyword";
  public final static String SESSION_PARAM_GROUPID             = "p_groupId";
  public final static String SESSION_PARAM_SEARCH_TITLE_ONLY   = "p_titleOnly";
  public final static String SESSION_PARAM_NOTES               = "p_notes";
  public final static String SESSION_PARAM_ACCESS_LEVEL        = "p_access_level";
  public final static String SESSION_PARAM_SCROLL_TO_DIV       = "p_scrollToDiv";
  public final static String SESSION_PARAM_MSG                 = "p_msg";
  public final static String SESSION_PARAM_ACTIVE_TAB          = "p_tab";
  public final static String SESSION_DEST_ID                   = "p_destId";
  public final static String SESSION_MODAL_URL                 = "p_modal_url";

/*
  public final static String SESSION_PARAM_ITINERARY_TRIPID  = "p_i_tripId";
  public final static String SESSION_PARAM_TRIPID = "p_tripid";
  public final static String SESSION_PARAM_USERID = "p_userid";
  public final static String SESSION_PARAM_KEYWORD = "p_keyword";
  public final static String SESSION_PARAM_GROUPID = "p_groupId";
  public final static String SESSION_PARAM_SEARCH_TITLE_ONLY = "p_titleOnly";
  public final static String SESSION_PARAM_NOTES = "p_notes";
  public final static String SESSION_PARAM_ACCESS_LEVEL = "p_access_level";
  public final static String SESSION_PARAM_SCROLL_TO_DIV = "p_scrollToDiv";
  public final static String SESSION_PARAM_ITINERARY_TRIPID = "p_i_tripId";
  public final static String SESSION_PARAM_ITINERARY_GROUPID = "p__i_groupId";
  public final static String SESSION_PROVIDER_ID             = "p_providerId";
  public final static String SESSION_TRIP_NOTE_ID            = "p_noteId";
*/
}
