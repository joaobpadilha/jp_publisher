package com.mapped.publisher.common;

import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.persistence.PoiRS;
import com.mapped.publisher.persistence.RecordStatus;
import com.mapped.publisher.persistence.SecurityDBMgr;
import com.mapped.publisher.persistence.SecurityRS;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import controllers.AccountController;
import models.publisher.*;
import org.apache.commons.lang3.time.StopWatch;

import java.sql.Connection;
import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-15
 * Time: 8:36 AM
 * To change this template use File | Settings | File Templates.
 */
public class SecurityMgr {

  public static final boolean TRIP_ACCESS_SHARED_INCLUDE = true;
  public static final boolean TRIP_ACCESS_SHARED_EXCLUDE = false;

  public enum AccessLevel {
    /**
     * No access to the element
     *
     * @note Ordinal value 0
     */
    NONE,
    /**
     * Read only access to the element
     *
     * @note Ordinal value 1
     */
    READ,

    /**
     * Read and new write access level to own bookings
     */
    TRAVELER,

    /**
     * Append only access to the module and sub modules
     *
     * @note Ordinal value 2
     */
    APPEND,
    /**
     * Contribute, Publish and Invite permissions
     *
     * @note Ordinal value 3
     */
    APPEND_N_PUBLISH,
    /**
     * Module owner
     * Full access and ability to publish, invite and perform other trip operations
     *
     * @note Ordinal value 4
     */
    OWNER,
    /**
     * Additional access to the element usually not available to the regular user.
     * Examples of this access is a company administrator
     *
     * @note Ordinal value 5
     */
    ADMIN,
    /**
     * Full access level not available to anyone in the company
     * Examples of this are administrators of the platform
     *
     * @note Ordinal value 6
     */
    SUPER_ADMIN;

    /**
     * Greater than otherLevel
     *
     * @param otherLevel - another level being compared to
     * @return true if greater or equal, false otehrwise
     */
    public boolean gt(AccessLevel otherLevel) {
      return this.ordinal() > otherLevel.ordinal();
    }

    /**
     * Greater or equal than otherLevel
     *
     * @param otherLevel - another level being compared to
     * @return true if greater or equal, false otherwise
     */
    public boolean ge(AccessLevel otherLevel) {
      return this.ordinal() >= otherLevel.ordinal();
    }

    /**
     * Less or equal than other level
     *
     * @param otherLevel - another level being compared to
     * @return true if less then or equal otherLevel
     */
    public boolean le(AccessLevel otherLevel) {
      return this.ordinal() <= otherLevel.ordinal();
    }

    /**
     * Less then the otherLevel
     *
     * @param otherLevel - another level being compared to
     * @return true if less than otherLevel
     */
    public boolean lt(AccessLevel otherLevel) {
      return this.ordinal() < otherLevel.ordinal();
    }

    public AccessLevel fromOrdinal(int value) {
      return AccessLevel.values()[value];
    }
  }

  public static void godMode(Company cmpy, AccountCmpyLink.LinkType mode, Credentials creds) {
    if (!creds.isUmappedAdmin()) {
      return;
    }

    if (mode == AccountCmpyLink.LinkType.ADMIN) { //In admin mode enabling all capabilities
      creds.fullyCapable();
    }
    else {
      creds.incapable();
    }

    creds.setCompany(cmpy, mode).becomeAnAgent() //Giving up all the rights
        .updateCache();

    //Code to support legacy modes
    Account                         a  = Account.findByLegacyId(creds.getUserId());
    UserProfile                     u  = UserProfile.findByPK(a.getLegacyId());
    AccountController.AccountHelper ah = AccountController.AccountHelper.build(creds.getUserId());
    ah.setAccount(a);
    ah.setUserProfile(u);
    ah.unlinkFromAll();
    ah.setAccountCmpy(cmpy);
    ah.linkToCompany(cmpy, mode);
  }

  /**
   * Figures out if user specified by userProfile can invite others
   *
   * @return
   */
  public static boolean canInviteOthers(String tripId, String userId, AccessLevel accessLevel) {
    if (tripId == null || userId == null || accessLevel == null) {
      return false;
    }
    return (TripShare.getParentShare(tripId, userId) == null && accessLevel.ge(AccessLevel.APPEND_N_PUBLISH));
  }

  public static AccessLevel getTripAccessLevel(Trip trip, SessionMgr sessionMgr) {
    if (trip == null || sessionMgr == null || trip.status == APPConstants.STATUS_DELETED) {
      return AccessLevel.NONE;
    }
    return getTripAccessLevel(trip.getTripid(),
                              trip.getCreatedby(),
                              trip.getCmpyid(),
                              sessionMgr.getCredentials(),
                              TRIP_ACCESS_SHARED_INCLUDE);
  }

  public static AccessLevel getTripAccessLevel(Trip trip, Account a) {
    if (trip == null || a == null || trip.status == APPConstants.STATUS_DELETED) {
      return AccessLevel.NONE;
    }
    switch (a.getAccountType()) {
      case TRAVELER:
        AccountTripLink atl = AccountTripLink.findByUid(a.getUid(), trip.getTripid());
        if(atl == null ||  atl.getLinkType() != AccountDetailLink.DetailLinkType.TRAVELER) {
          return AccessLevel.NONE;
        }
        return AccessLevel.TRAVELER;

      case UMAPPED_ADMIN:
        return AccessLevel.SUPER_ADMIN;

      case PUBLISHER:
        //Agent
        Credentials cred = Credentials.getFromCache(a.getUid());
        if(cred == null) {
          cred = SecurityMgr.getCredentials(a);
          if (cred != null) {
            cred.saveCache();
          }
        }

        //Still no credentials
        if(cred == null) {
          return AccessLevel.NONE;
        }
        AccessLevel al =  SecurityMgr.getTripAccessLevel(trip, cred);
        if(al.lt(AccessLevel.READ)) {
          AccountTripLink atlp = AccountTripLink.findByUid(a.getUid(), trip.getTripid());
          if(atlp == null ||  atlp.getLinkType() != AccountDetailLink.DetailLinkType.TRAVELER) {
            return AccessLevel.NONE;
          }
          return AccessLevel.TRAVELER;
        }
        return al;
      default:
        return AccessLevel.NONE;
    }
  }

  /**
   * Verifies access Level for the trip for a user specified by credentials
   *
   * @param tripId        tripId in question
   * @param tripCreatorId trip owner
   * @param tripCmpyId    trip owner company
   * @param cred          user credentials
   * @return Access level to the trip specified by tripid
   */
  public static AccessLevel getTripAccessLevel(String tripId,
                                               String tripCreatorId,
                                               String tripCmpyId,
                                               Credentials cred,
                                               boolean includeShared) {
    if (cred == null || tripCmpyId == null || tripCreatorId == null || tripId == null) {
      return AccessLevel.NONE;
    }

    String userId = cred.getUserId();

    //Super admin here
    if (cred.isUmappedAdmin()) {
      return AccessLevel.SUPER_ADMIN;
    }

    //Check if user belong to trip company
    if (tripCmpyId.equals(cred.getCmpyId())) {

      //Company administrator here
      if (cred.getCmpyLinkType().equals(AccountCmpyLink.LinkType.ADMIN)) {
        return AccessLevel.ADMIN;
      }

      //Trip creator him/herself is here OR
      //If user belongs to the group treat it as if owner
      if (tripCreatorId.equals(userId) || cred.isInGroupWith(tripCreatorId)) {
        return AccessLevel.OWNER;
      }
    }

    //Check if trip is in user's shared trips
    if (includeShared && cred.isInSharedTrip(tripId)) {
      return cred.getSharedTripAccessLevel(tripId);
    }

    //Out of luck, no access
    return AccessLevel.NONE;
  }

  public static AccessLevel getTripAccessLevel(Trip trip, Credentials cred) {
    if (cred == null || trip == null || trip.status == APPConstants.STATUS_DELETED) {
      return AccessLevel.NONE;
    }

    String tripCreatorId = trip.getCreatedby();
    String tripCmpyId    = trip.getCmpyid();
    String tripId        = trip.getTripid();

    return getTripAccessLevel(tripId, tripCreatorId, tripCmpyId, cred, TRIP_ACCESS_SHARED_INCLUDE);
  }

  public static boolean canCreateTrip(Company cmpy, SessionMgr sessionMgr) {
    Credentials cred = sessionMgr.getCredentials();
    if (cred == null || cred.isUmappedAdmin() || cmpy == null) {
      return false;
    }

    return cred.getCmpyIdInt() == cmpy.getCmpyId();
  }

  public static boolean canEditPoi(PoiRS prs, SessionMgr sessionMgr) {
    Credentials cred = sessionMgr.getCredentials();
    if (cred == null || prs == null) {
      return false;
    }

    return cred.isUmappedAdmin() || cred.isCmpyAdmin(prs.getCmpyId()) || cred.isCmpyPowerMember(prs.getCmpyId());
  }

  public static boolean canAccessGuide(String guideId, SessionMgr sessionMgr) {
    Destination dest = Destination.find.byId(guideId);
    Credentials cred = sessionMgr.getCredentials();
    if (dest == null || dest.status == APPConstants.STATUS_DELETED || cred == null) {
      return false;
    }

    return cred.isUmappedAdmin() || dest.cmpyid.equals(cred.getCmpyId());
  }

  public static boolean canEditGuide(Destination dest, DestinationGuide guide, SessionMgr sessionMgr) {
    Credentials cred = sessionMgr.getCredentials();
    if (cred == null || guide == null) {
      return false;
    }

    //If Document Page (Destination Guide) was created by the user - s/he can access it
    if (sessionMgr.getUserId().equals(guide.getCreatedby())) {
      return true;
    }

    //If UMapped Admin
    if (cred.isUmappedAdmin()) {
      return true;
    }

    //If user is admin of the destination's owner company or powet user
    if (dest.getStatus() != APPConstants.STATUS_DELETED && (cred.isCmpyAdmin(dest.cmpyid) || cred.isCmpyPowerMember
        (dest.cmpyid))) {
      return true;
    }

    //If user is a member of the group within the company
    if (cred.isInGroupWith(dest.createdby)) {
      return true;
    }

    //if this is a trip document, check for shared access
    if (dest.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE || dest.destinationtypeid == APPConstants
        .TRIP_DOC_DESTINATION_TYPE) {
      //check for shared trip access
      // If current trip belongs to a user's shared trip then this guide can be accessed
      String tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);
      if (cred.getSharedTripAccessLevel(tripId).ge(AccessLevel.APPEND)) {
        return true;
      }
    }

    return false;
  }

  public static boolean canAccessDestination(Destination dest, SessionMgr sessionMgr) {
    Credentials cred = sessionMgr.getCredentials();
    if (cred == null || dest == null) {
      return false;
    }

    //If Document Page (Destination Guide) was created by the user - s/he can access it or
    //If UMapped Admin or member of the destination company and it is not deleted
    if (cred.isUmappedAdmin() ||
        dest.getCreatedby().equals(cred.getUserId()) ||
        (dest.getStatus() != APPConstants.STATUS_DELETED && dest.cmpyid.equals(cred.getCmpyId()))) {
      return true;
    }

    // If current trip belongs to a user's shared trip then this guide can be accessed
    String tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);
    if (tripId != null) {
      AccessLevel accessLevel = cred.getSharedTrips().get(tripId);
      if (accessLevel != AccessLevel.NONE) {
        return true;
      }
    }

    return false;
  }

  public static boolean canEditDestination(Destination dest, SessionMgr sessionMgr) {
    Credentials cred = sessionMgr.getCredentials();
    if (cred == null || dest == null) {
      return false;
    }

    if (cred.isUmappedAdmin()) {
      return true;
    }

    if (dest.getStatus() == APPConstants.STATUS_DELETED) {
      return false;
    }

    if (cred.isCmpyAdmin(dest.cmpyid) || (cred.isCmpyPowerMember(dest.cmpyid) && dest.createdby != null && dest
        .createdby
        .equals(cred.getUserId()))) {
      return true;
    }

    if (dest.destinationtypeid == APPConstants.TRIP_DOC_DESTINATION_TYPE || dest.destinationtypeid == APPConstants.TOUR_TYPE || dest.destinationtypeid == APPConstants
        .TOUR_DOC_DESTINATION_TYPE) {
      //check to make sure doc belongs to user or group
      if (dest.createdby.equals(cred.getUserId()) || cred.isInGroupWith(dest.createdby)) {
        return true;
      }

      //check for shared trip access
      // If current trip belongs to a user's shared trip then this guide can be accessed
      String tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);
      if (cred.getSharedTripAccessLevel(tripId).ge(AccessLevel.APPEND)) {
        return true;
      }
    }

    return false;
  }

  public static boolean isUmappedAdmin(SessionMgr sessionMgr) {
    Credentials cred = sessionMgr.getCredentials();
    return cred != null && cred.isUmappedAdmin();
  }

  public static boolean isCmpyAdmin(Company cmpy, SessionMgr sessionMgr) {
    Credentials cred = sessionMgr.getCredentials();
    return cred != null && cred.isCmpyAdmin(cmpy.getCmpyId());
  }

  public static boolean isPowerMember(String cmpyId, SessionMgr sessionMgr) {
    Credentials cred = sessionMgr.getCredentials();
    return cred != null && cred.isCmpyPowerMember(cmpyId);
  }

  public static boolean isPowerMember(Company cmpy, SessionMgr sessionMgr) {
    Credentials cred = sessionMgr.getCredentials();
    return cred != null && cred.isCmpyPowerMember(cmpy.getCmpyId());
  }

  public static boolean isCmpyAdmin(String cmpyId, SessionMgr sessionMgr) {
    Credentials cred = sessionMgr.getCredentials();
    return cred != null && cred.isCmpyAdmin(cmpyId);
  }

  public static boolean canEditCmpy(Company cmpy, SessionMgr sessionMgr) {
    Credentials cred = sessionMgr.getCredentials();
    return cred != null && (cred.isUmappedAdmin() || cred.isCmpyAdmin(cmpy.getCmpyId()));
  }

  /**
   * Adds trip to the cached session object of the user
   *
   * @param a
   * @param tripId
   * @param accessLevel
   * @return
   */
  public static void addSharedTrip(Account a, String tripId, AccessLevel accessLevel) {
    if (a == null || tripId == null || accessLevel == null || tripId.length() <= 0) {
      return;
    }

    Credentials cred = Credentials.getFromCache(a.getUid());
    if (cred != null) {
      cred.addSharedTrip(tripId, accessLevel);
      cred.updateCache();
    }
  }

  public static void chanceAccessLevel(Account a, String tripId, AccessLevel accessLevel) {
    if (a == null || tripId == null || tripId.length() <= 0 || accessLevel == null) {
      return;
    }

    Credentials cred = Credentials.getFromCache(a.getUid());
    if (cred != null) {
      cred.changeSharedTripAccessLevel(tripId, accessLevel);
      cred.updateCache();
    }
  }

  /**
   * Removes shared trip from the user's cached credentials
   *
   * @param a      account of the user to remove
   * @param tripId
   * @return
   */
  public static void removeSharedTrip(Account a, String tripId) {
    if (a == null || tripId == null || tripId.length() <= 0) {
      return;
    }

    Credentials cred = Credentials.getFromCache(a.getUid());
    if (cred != null) {
      cred.removeSharedTrip(tripId);
      cred.updateCache();
    }
  }

  public static boolean hasCapability(Trip trip, Capability cap) {
    EnumSet<Capability> userCaps = getTripCapabilities(trip);
    if (cap.equals(Capability.FLIGHT_NOTIFICATIONS) && trip != null && trip.tag != null && trip.tag.contains(APPConstants.TRIP_TAG_DISABLE_FLIGHT_NOTIFICATION)) {
      //check for trip tag to see if there is an overwrite
      return  false;
    }
    return userCaps.contains(cap);
  }

  public static EnumSet<Capability> getTripCapabilities(Trip trip) {
    //remove cache for now
    EnumSet<Capability> userCaps = null;//disable cache for now //(EnumSet<Capability>) CacheMgr.get(APPConstants
    // .CACHE_TRIP_CAP_PREFIX + trip.getTripid());

    if (userCaps == null) {
      //if this is a published trip - use the set up from the last user that published it
      TripPublishHistory tp = TripPublishHistory.getLastPublished(trip.tripid, null);
      if (tp != null) {
        userCaps = getCapabilities(tp.createdby);
      }
      else {
        userCaps = getCapabilities(trip.createdby);
      }
      CacheMgr.set(APPConstants.CACHE_TRIP_CAP_PREFIX + trip.getTripid(),
                   userCaps,
                   APPConstants.CACHE_TRANSIENT_EXPIRY_SECS);
    }

    return userCaps;
  }

  public static EnumSet<Capability> getCapabilities(String userId) {
    StopWatch sw = new StopWatch();
    sw.start();
    EnumSet<Capability> capsToRemove = EnumSet.noneOf(Capability.class);
    EnumSet<Capability> capsToEnable = EnumSet.noneOf(Capability.class);

    //TODO: Temporary exception. MUST BE FIXED
    capsToEnable.add(Capability.COMMUNICATOR_TRAVELER_TALKBACK);
    capsToEnable.add(Capability.COMMUNICATOR_MSG_DIGEST);
    capsToEnable.add(Capability.BETA_WEB_ITINERARY);



    // Permissions Cake
    //0. Plan Capabilities - Adding all capabilities from features in the plan
    List<SysCapability> capsFromPlan = SysCapability.getUserPlanCapabilities(userId);
    capsToEnable.addAll(capsFromModel(capsFromPlan));

    //1. Capabilities based on enabled/disabled, active/expired addons
    List<BillingAddonsForUser> addons = BillingAddonsForUser.getAddonsForUser(userId);
    for (BillingAddonsForUser a : addons) {
      List<SysCapability> on  = SysCapability.getPlanCapabilities(a.getPk().planId, true);
      List<SysCapability> off = SysCapability.getPlanCapabilities(a.getPk().planId, false);

      if (a.getEnabled() || (a.getCutoffTs() != null && a.getCutoffTs() >= System.currentTimeMillis())) {
        capsToEnable.addAll(capsFromModel(on));
      }
      else {
        capsToRemove.addAll(capsFromModel(on));
      }
      capsToRemove.addAll(capsFromModel(off));
    }

    capsToRemove.removeAll(capsToEnable); //These are capabilities that are actually enabled

    //2. User sets
    List<SysCapability> on      = SysCapability.getUserEnabledCapabilities(userId);
    List<SysCapability> off     = SysCapability.getUserDisabledCapabilities(userId);
    EnumSet<Capability> uOnSet  = capsFromModel(on);
    EnumSet<Capability> uOffSet = capsFromModel(off);

    //3. Set manipulation to get to the final solution
    uOnSet.removeAll(capsToRemove);   //Removing what was banned on plan/feature level
    capsToRemove.addAll(uOffSet);     //Disabling features that user explicitly disabled
    capsToEnable.addAll(uOnSet);      //Enabling all features that are enabled by plan and user enabled
    capsToEnable.removeAll(capsToRemove);//Removing all features that user or plan disabled

    //if the user is basic and does not have the communicator, then we also remove the talkback communicator
    if (!capsToEnable.contains(Capability.COMMUNICATOR) && capsToEnable.contains(Capability.COMMUNICATOR_TRAVELER_TALKBACK)) {
      capsToEnable.remove(Capability.COMMUNICATOR_TRAVELER_TALKBACK);
    }

    Log.debug("User " + userId + " capabilities built in " + sw.getTime() + "ms");

    return capsToEnable;
  }

  private static EnumSet<Capability> capsFromModel(List<SysCapability> ml) {
    EnumSet<Capability> result = EnumSet.noneOf(Capability.class);
    if (ml == null) {
      return result;
    }
    for (SysCapability sc : ml) {
      result.add(sc.getCapability());
    }
    return result;
  }

  public static Credentials getCredentials(long accountId) {
    Account a = Account.find.byId(accountId);
    return getCredentials(a);
  }

  public static Credentials getCredentials(Account a) {
    StopWatch sw = new StopWatch();
    sw.start();

    if (a == null || a.getState() != RecordStatus.ACTIVE) {
      return null;
    }

    Credentials cred = Credentials.buildCredentials(a)
                                  .setSessionId(Utils.getUniqueId())
                                  .setExpiry(System.currentTimeMillis() + APPConstants.SESSION_EXPIRY);

    if (a.getAccountType() == Account.AccountType.UMAPPED_ADMIN) {
      cred.updateCache();
      return cred;
    }

    cred.setCapabilities(getCapabilities(a.getLegacyId())); //TODO: Capabilities need to be converted to accounts

    AccountCmpyLink acl  = AccountCmpyLink.find.byId(a.getUid());
    Company         cmpy = Company.find.byId(acl.getCmpyid());

    boolean processCompany = true;

    if (cmpy.getStatus() == APPConstants.STATUS_ACTIVE &&
        cmpy.getType() != null &&
        cmpy.getType().equals(APPConstants.CMPY_TYPE_TRIAL) && //TODO: This is legacy should get from Company Billing
        cmpy.getExpirytimestamp() != null &&
        cmpy.getExpirytimestamp() < System.currentTimeMillis()) {
      processCompany = false;
    }

    if (processCompany) {
      /*
       * Here we check if company has new agreement to sign, if it does then user is assigned to the company, otherwise
       * there is no such assignment
       */

      if (acl.getLinkType() == AccountCmpyLink.LinkType.ADMIN) {
        if (cmpy.getTargetagreementver() == null ||
            (cmpy.getAuthorizeduserid() != null && cmpy.getAuthorizeduserid().equals(a.getLegacyId())) ||
            (cmpy.getTargetagreementver() != null && cmpy.getAcceptedagreementver() != null &&
             cmpy.getTargetagreementver().equals(cmpy.getAcceptedagreementver()))) {
          cred.setCompany(cmpy, acl.getLinkType());
        }
      }
      else {
        if (cmpy.getTargetagreementver() == null || (cmpy.getTargetagreementver() != null &&
                                                     cmpy.getAcceptedagreementver() != null &&
                                                     cmpy.getTargetagreementver()
                                                         .equals(cmpy.getAcceptedagreementver()))) {
          cred.setCompany(cmpy, acl.getLinkType());
        }
      }
    }

    //All shared trips accessible by the user
    //TODO:  Can be a long list, so need to create filters
    List<TripShare> sharedTrips = TripShare.getSharedTrips(a.getLegacyId());
    for (TripShare t : sharedTrips) {
      cred.addSharedTrip(t.tripid, t.accesslevel);
    }

    Connection conn = null;
    try {
      conn = DBConnectionMgr.getConnection4Publisher();

      List<SecurityRS> groupRecs = SecurityDBMgr.findByUserId(a.getLegacyId(), conn);
      if (groupRecs != null && groupRecs.size() > 0) {
        for (SecurityRS rec : groupRecs) {
          if (rec.getCmpyType() != null &&
              rec.getCmpyType().equals(APPConstants.CMPY_TYPE_TRIAL) &&
              rec.getCmpyExpiryTimestamp() != null &&
              rec.getCmpyExpiryTimestamp() < System.currentTimeMillis()) {
            continue; //Skipping
          }

          if (rec.getUserId().equals(a.getLegacyId())) {
            //get group memberships
            cred.addGroupId(rec.getGroupId());
          }
          else {
            //add other members
            cred.addGroupUser(rec.getGroupId(), rec.getUserId());
          }
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      return null;
    }
    finally {
      try {
        if (conn != null) {
          conn.close();
        }
      }
      catch (Exception s) {
        return null;
      }
    }


    sw.stop();
    Log.debug("SecurityMgr: Credentials built for aid: " + a.getEncodedId() + " legacy: " + a.getLegacyId() +
              " in " + sw.getTime() + "ms");
    return cred;
  }

  public static boolean canAccessCmpy(Company company, SessionMgr sessionMgr) {
    Credentials cred = sessionMgr.getCredentials();
    //TODO: Serguei: Why not Umapped Admin?
    return cred != null && company != null && !cred.isUmappedAdmin() && cred.getCmpyIdInt() == company.getCmpyId();
  }

  public static boolean canAccessCmpy(String cmpyId, SessionMgr sessionMgr) {
    Credentials cred = sessionMgr.getCredentials();
    return cred != null && cmpyId != null && !cred.isUmappedAdmin() && cred.getCmpyId().equals(cmpyId);
  }

  public static boolean canAccessTemplate(Template template, SessionMgr sessionMgr) {
    Credentials cred = sessionMgr.getCredentials();
    if (cred == null || cred.isUmappedAdmin() || template == null ||
        template.getStatus() == APPConstants.STATUS_DELETED) {
      return false;
    }

    //TODO: What is this case? Public templates?
    if (template.getCmpyid() != null && template.getCmpyid().equals("0")) {
      return true;
    }

    //If belongs to template company and template is either public or user is cmpy admin or template was created by user
    return cred.getCmpyId().equals(template.cmpyid) && (template.getVisibility() == Template.Visibility.PUBLIC ||
                                                        template.createdby.equals(sessionMgr.getUserId()) ||
                                                        cred.getCmpyLinkType() == AccountCmpyLink.LinkType.ADMIN);

  }

  public static boolean canEditTemplate(Template template, SessionMgr sessionMgr) {
    Credentials cred = sessionMgr.getCredentials();
    if (cred == null || cred.isUmappedAdmin() || template == null ||
        template.getStatus() == APPConstants.STATUS_DELETED) {
      return false;
    }

    return cred.getCmpyId()
               .equals(template.cmpyid) && (template.createdby.equals(cred.getUserId()) || cred.getCmpyLinkType() == AccountCmpyLink.LinkType.ADMIN);
  }

  //get the trip expiry
  public static long getTripExpirationTs(Trip trip) {
    if (trip != null && trip.endtimestamp != null && trip.endtimestamp > 0L)
      return trip.endtimestamp + TimeUnit.DAYS.toMillis(7);
    else
      return System.currentTimeMillis() + TimeUnit.DAYS.toMillis(1);
  }
}
