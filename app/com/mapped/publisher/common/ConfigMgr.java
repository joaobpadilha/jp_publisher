package com.mapped.publisher.common;

import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2012-10-11
 * Time: 8:59 AM
 * To change this template use File | Settings | File Templates.
 */
public class ConfigMgr {
  private static ConfigMgr instance;
  private Map<String, String> params;
  private List<Character> charList;
  private List<Character> lowerCharList;

  private ConfigMgr() {
    params = new HashMap<>();
    params.put(CoreConstants.LOG_LEVEL, "31");

    params.put(CoreConstants.DB_DRIVER, "org.postgresql.Driver");
    params.put(CoreConstants.DB_URL, "jdbc:postgresql://ec2-23-21-209-178.compute-1.amazonaws.com:5432/dbjvp9l6m18ata");
    params.put(CoreConstants.DB_USERID, "vygovvrywjyvgc");
    params.put(CoreConstants.DB_PWD, "nJR58aAIyvTQnCuL3JS0vMJtx7");


    params.put(CoreConstants.SESSION_TIMEOUT, "30");

    params.put(com.mapped.common.CoreConstants.APPLE_CERT,
               "/Users/twong/code/location/iphone/config/dev/Mapped_Dev.p12");
    params.put(com.mapped.common.CoreConstants.APPLE_CERT_PWD, "Th13rryw!@#");
    params.put(com.mapped.common.CoreConstants.APPLE_NOTIFICATION_MSG, "You have new stories waiting.");


    params.put(com.mapped.common.CoreConstants.EMAIL_DIGEST_FROM, "no-reply@email.umapped.com");
    params.put(com.mapped.common.CoreConstants.EMAIL_DIGEST_SUBJECT, "Updates are waiting for you...");
    params.put(com.mapped.common.CoreConstants.EMAIL_PWD_RESET_FROM, "no-reply@email.umapped.com");
    params.put(com.mapped.common.CoreConstants.EMAIL_PWD_RESET_SUBJECT, "Your Password Has Been Reset");
    params.put(com.mapped.common.CoreConstants.EMAIL_MOBILE_PWD_RESET_SUBJECT, "Your Mobile Password Has Been Reset");

    params.put(com.mapped.common.CoreConstants.EMAIL_TRIP_NOTIFICATION_FROM, "no-reply@email.umapped.com");
    params.put(com.mapped.common.CoreConstants.EMAIL_NEW_ACCOUNT_SUBJECT, "Activate your Umapped account");
    params.put(com.mapped.common.CoreConstants.EMAIL_NEW_UMAPPED_ACCOUNT_SUBJECT,
               "Your Umapped Administrator account is almost ready");
    params.put(com.mapped.common.CoreConstants.EMAIL_NEW_CMPY_LINK_SUBJECT, "New Link to ");
    params.put(com.mapped.common.CoreConstants.EMAIL_DELETE_CMPY_LINK_SUBJECT, "Link removed to ");
    params.put(com.mapped.common.CoreConstants.EMAIL_ENABLED, "Y");


    //environmental variable
    String hostUrl = System.getenv(com.mapped.common.CoreConstants.HOST_URL);
    String s3Bucket = System.getenv(com.mapped.common.CoreConstants.S3_BUCKET);
    String s3Access = System.getenv(com.mapped.common.CoreConstants.S3_ACCESS_KEY);
    String s3Secret = System.getenv(com.mapped.common.CoreConstants.S3_SECRET);
    String smtpHost = System.getenv(com.mapped.common.CoreConstants.EMAIL_SMTP_HOST);
    String smtpUser = System.getenv(com.mapped.common.CoreConstants.EMAIL_SMTP_USER_ID);
    String smtpPwd = System.getenv(com.mapped.common.CoreConstants.EMAIL_SMTP_PASSWORD);
    String sqs_Url = System.getenv(com.mapped.common.CoreConstants.SQS_Q_URL);

    String memcachedPwd = System.getenv(com.mapped.common.CoreConstants.MEMCACHED_PASSWORD);
    String memcachedUser = System.getenv(com.mapped.common.CoreConstants.MEMCACHED_USER);
    String memcachedServer = System.getenv(com.mapped.common.CoreConstants.MEMCACHED_SERVERS);
    String fiveFiltersServer = System.getenv(com.mapped.common.CoreConstants.FIVE_FILTERS);

    String freshbooksUrl = System.getenv(com.mapped.common.CoreConstants.FRESHBOOKS_URL);
    String freshbooksToken = System.getenv(com.mapped.common.CoreConstants.FRESHBOOKS_TOKEN);


    params.put(com.mapped.common.CoreConstants.FRESHBOOKS_URL, freshbooksUrl);
    params.put(com.mapped.common.CoreConstants.FRESHBOOKS_TOKEN, freshbooksToken);

    String worldmateAddr = System.getenv(com.mapped.common.CoreConstants.WORLDMATE_ADDR);
    params.put(com.mapped.common.CoreConstants.WORLDMATE_ADDR, worldmateAddr);

    String firebaseSecret = System.getenv(com.mapped.common.CoreConstants.FIREBASE_SECRET);
    params.put(com.mapped.common.CoreConstants.FIREBASE_SECRET, firebaseSecret);

    String firebaseURI = System.getenv(com.mapped.common.CoreConstants.FIREBASE_URI);
    params.put(com.mapped.common.CoreConstants.FIREBASE_URI, firebaseURI);

    String redisUrl = System.getenv(com.mapped.common.CoreConstants.REDIS_URL);
    params.put(com.mapped.common.CoreConstants.REDIS_URL, redisUrl);


    addEnvironmentVariable(com.mapped.common.CoreConstants.EMAIL_COMMUNICATOR_HOST, true, null);
    addEnvironmentVariable(com.mapped.common.CoreConstants.EMAIL_SMTP_PORT, false, "587");

    addEnvironmentVariable(com.mapped.common.CoreConstants.SMS_CLICKATELL_APIID, false, null);
    addEnvironmentVariable(com.mapped.common.CoreConstants.SMS_CLICKATELL_TOKEN, false, null);
    addEnvironmentVariable(com.mapped.common.CoreConstants.SMS_CLICKATELL_MSISDN, false, "12067354737");
    addEnvironmentVariable(com.mapped.common.CoreConstants.SMS_CALLBACK_USER, false, null);
    addEnvironmentVariable(com.mapped.common.CoreConstants.SMS_CALLBACK_PASS, false, null);
    addEnvironmentVariable(com.mapped.common.CoreConstants.FIREBASE_PING_SEC, false, "60");
    addEnvironmentVariable(com.mapped.common.CoreConstants.ENVIRONMENT, false, CoreConstants.DEV);
    addEnvironmentVariable(com.mapped.common.CoreConstants.ENV_SUFFIX, false, "");

    addEnvironmentVariable(com.mapped.common.CoreConstants.BRAINTREE_MERCHANT, true, "hmdhxbkpsd4dhz4z");
    addEnvironmentVariable(com.mapped.common.CoreConstants.BRAINTREE_PUBLIC, true, "nk55rqczr3zvyhmd");
    addEnvironmentVariable(com.mapped.common.CoreConstants.BRAINTREE_PRIVATE, true, "48c78194e24953b1ed33776f2e5f1139");
    addEnvironmentVariable(com.mapped.common.CoreConstants.BRAINTREE_USD, true, "umappedinc");
    addEnvironmentVariable(com.mapped.common.CoreConstants.BRAINTREE_CAD, false, null);
    addEnvironmentVariable(com.mapped.common.CoreConstants.BRAINTREE_AUD, false, null);
    addEnvironmentVariable(com.mapped.common.CoreConstants.BRAINTREE_EUR, false, null);
    addEnvironmentVariable(com.mapped.common.CoreConstants.S3_PDF_BUCKET, false, "umapped-pdf");


    addEnvironmentVariable(com.mapped.common.CoreConstants.TRAVELBOUND_URL_USA, true, "https://interface.demo.gta-travel.com/rbstbapi/RequestListenerServlet");
    addEnvironmentVariable(com.mapped.common.CoreConstants.TRAVELBOUND_URL_CAN, true, "https://interface.demo.gta-travel.com/rbstbapi/RequestListenerServlet");
    addEnvironmentVariable(com.mapped.common.CoreConstants.TRAVELBOUND_UMAPPED_CREDENTIAL, true, "044:46607:API@UMAPPED.COM:PASS");

    addEnvironmentVariable(com.mapped.common.CoreConstants.GOGO_URL, true, "https://gogoliveconnectqa.gogowwv.com/api/GetUmapped");




    if (hostUrl == null || hostUrl.length() == 0) {
      Log.log(LogLevel.ERROR, "Fatal - missing host url");
      System.exit(1);
    }

    if (s3Bucket == null || s3Bucket.length() == 0) {
      Log.log(LogLevel.ERROR, "Fatal - missing S3 Bucket");
      System.exit(1);
    }

    if (s3Access == null || s3Access.length() == 0) {
      Log.log(LogLevel.ERROR, "Fatal - missing S3 Access");
      System.exit(1);
    }
    if (s3Secret == null || s3Secret.length() == 0) {
      Log.log(LogLevel.ERROR, "Fatal - missing S3 Secret");
      System.exit(1);
    }
    if (smtpHost == null || smtpHost.length() == 0) {
      Log.log(LogLevel.ERROR, "Fatal - missing SMTP host");
      System.exit(1);
    }
    if (smtpUser == null || smtpUser.length() == 0) {
      Log.log(LogLevel.ERROR, "Fatal - missing SMTP User Id");
      System.exit(1);
    }
    if (smtpPwd == null || smtpPwd.length() == 0) {
      Log.log(LogLevel.ERROR, "Fatal - missing SMTP password");
      System.exit(1);
    }
    if (sqs_Url == null || sqs_Url.length() == 0) {
      Log.log(LogLevel.ERROR, "Fatal - missing sqs url");
      System.exit(1);
    }
    if (fiveFiltersServer == null || fiveFiltersServer.length() == 0) {
      Log.err("Fatal - missing Five Filters url");
      System.exit(1);
    }

    if(redisUrl == null || redisUrl.length() == 0) {
      Log.err("Fatal - missing Redis URL");
      System.exit(1);
    }

    params.put(com.mapped.common.CoreConstants.HOST_URL, hostUrl);
    params.put(com.mapped.common.CoreConstants.S3_BUCKET, s3Bucket);
    params.put(com.mapped.common.CoreConstants.S3_ACCESS_KEY, s3Access);
    params.put(com.mapped.common.CoreConstants.S3_SECRET, s3Secret);
    params.put(com.mapped.common.CoreConstants.EMAIL_SMTP_HOST, smtpHost);
    params.put(com.mapped.common.CoreConstants.EMAIL_SMTP_USER_ID, smtpUser);
    params.put(com.mapped.common.CoreConstants.EMAIL_SMTP_PASSWORD, smtpPwd);
    params.put(com.mapped.common.CoreConstants.SQS_Q_URL, sqs_Url);
    params.put(com.mapped.common.CoreConstants.FIVE_FILTERS, fiveFiltersServer);
    params.put(com.mapped.common.CoreConstants.MEMCACHED_SERVERS, memcachedServer);

    if (memcachedUser != null) {
      params.put(com.mapped.common.CoreConstants.MEMCACHED_USER, memcachedUser);
    }
    if (memcachedPwd != null) {
      params.put(com.mapped.common.CoreConstants.MEMCACHED_PASSWORD, memcachedPwd);
    }

    if (params.get(com.mapped.common.CoreConstants.ENVIRONMENT).equals(com.mapped.common.CoreConstants.PROD)) {
      params.put(com.mapped.common.CoreConstants.JS_URL, "https://s3.amazonaws.com/static-umapped-com/public");
      params.put(com.mapped.common.CoreConstants.CSS_URL, "https://s3.amazonaws.com/static-umapped-com/public");
      params.put(com.mapped.common.CoreConstants.IMG_URL, "https://s3.amazonaws.com/static-umapped-com/public");
      params.put(com.mapped.common.CoreConstants.LIB_URL, "https://s3.amazonaws.com/static-umapped-com/public");

      params.put(com.mapped.common.CoreConstants.S3_IMG_CROP_BUCKET, com.mapped.common.CoreConstants.S3_IMG_CROP_BUCKET_URL);
    }
    else {
      params.put(com.mapped.common.CoreConstants.JS_URL, "/assets");
      params.put(com.mapped.common.CoreConstants.CSS_URL, "/assets");
      params.put(com.mapped.common.CoreConstants.IMG_URL, "/assets");
      params.put(com.mapped.common.CoreConstants.LIB_URL, "/assets");
      params.put(com.mapped.common.CoreConstants.S3_IMG_CROP_BUCKET, com.mapped.common.CoreConstants.S3_IMG_CROP_BUCKET_URL + "-test");

    }

    //initialize character list
    charList = new ArrayList<Character>();
    for (int i = 0; i < 10; i++) {
      charList.add(String.valueOf(i).charAt(0));
    }
    charList.add('a');
    charList.add('b');
    charList.add('c');
    charList.add('d');
    charList.add('e');
    charList.add('f');
    charList.add('g');
    charList.add('h');
    charList.add('i');
    charList.add('j');
    charList.add('k');
    charList.add('l');
    charList.add('m');
    charList.add('n');
    charList.add('o');
    charList.add('p');
    charList.add('q');
    charList.add('r');
    charList.add('s');
    charList.add('t');
    charList.add('u');
    charList.add('v');
    charList.add('w');
    charList.add('x');
    charList.add('y');
    charList.add('z');
    charList.add('A');
    charList.add('B');
    charList.add('C');
    charList.add('D');
    charList.add('E');
    charList.add('F');
    charList.add('G');
    charList.add('H');
    charList.add('I');
    charList.add('J');
    charList.add('K');
    charList.add('L');
    charList.add('M');
    charList.add('N');
    charList.add('O');
    charList.add('P');
    charList.add('Q');
    charList.add('R');
    charList.add('S');
    charList.add('T');
    charList.add('U');
    charList.add('V');
    charList.add('W');
    charList.add('X');
    charList.add('Y');
    charList.add('Z');


    //initialize character list
    lowerCharList = new ArrayList<Character>();
    for (int i = 0; i < 10; i++) {
      lowerCharList.add(String.valueOf(i).charAt(0));
    }
    lowerCharList.add('a');
    lowerCharList.add('b');
    lowerCharList.add('c');
    lowerCharList.add('d');
    lowerCharList.add('e');
    lowerCharList.add('f');
    lowerCharList.add('g');
    lowerCharList.add('h');
    lowerCharList.add('i');
    lowerCharList.add('j');
    lowerCharList.add('k');
    lowerCharList.add('l');
    lowerCharList.add('m');
    lowerCharList.add('n');
    lowerCharList.add('o');
    lowerCharList.add('p');
    lowerCharList.add('q');
    lowerCharList.add('r');
    lowerCharList.add('s');
    lowerCharList.add('t');
    lowerCharList.add('u');
    lowerCharList.add('v');
    lowerCharList.add('w');
    lowerCharList.add('x');
    lowerCharList.add('y');
    lowerCharList.add('z');
    lowerCharList.add('a');  //repeat for base 62
    lowerCharList.add('b');
    lowerCharList.add('c');
    lowerCharList.add('d');
    lowerCharList.add('e');
    lowerCharList.add('f');
    lowerCharList.add('g');
    lowerCharList.add('h');
    lowerCharList.add('i');
    lowerCharList.add('j');
    lowerCharList.add('k');
    lowerCharList.add('l');
    lowerCharList.add('m');
    lowerCharList.add('n');
    lowerCharList.add('o');
    lowerCharList.add('p');
    lowerCharList.add('q');
    lowerCharList.add('r');
    lowerCharList.add('s');
    lowerCharList.add('t');
    lowerCharList.add('u');
    lowerCharList.add('v');
    lowerCharList.add('w');
    lowerCharList.add('x');
    lowerCharList.add('y');
    lowerCharList.add('z');
  }

  public static ConfigMgr getInstance() {
    if (instance == null) {
      synchronized (ConfigMgr.class) {
        if (instance == null) {
          instance = new ConfigMgr();
        }
      }
    }
    return instance;
  }


  private void addEnvironmentVariable(String varName, boolean required, String defaultValue) {
    String val = System.getenv(varName);

    if(val == null || val.length() == 0) {
      val = defaultValue;
    }

    if(required && (val == null || val.length() == 0)) {
      Log.err("Fatal - Environment Variable Not Set: " + varName);
      System.exit(1);
    }

    params.put(varName, val);
  }

  /**
   * Helper utility function to get access to data faster.
   * @param param
   * @return
   */
  public static String getAppParameter(String param) {
    return getInstance().params.get(param);
  }

  public Boolean isProd() {
    return params.get(com.mapped.common.CoreConstants.ENVIRONMENT).equals(com.mapped.common.CoreConstants.PROD);
  }

  /**
   * Static helper without calling for instance
   * @return
   */
  public static boolean isProduction() {
    return ConfigMgr.getInstance().isProd();
  }

  public static boolean isLocalDev() {

    if (ConfigMgr.getInstance().params.get(com.mapped.common.CoreConstants.ENVIRONMENT)
                                  .equals(com.mapped.common.CoreConstants.DEV)) {
      return ConfigMgr.getInstance().params.get(com.mapped.common.CoreConstants.HOST_URL).contains("umdev");
    }
    return false;
  }

  /**
   * For all next gen AWS resources that might be customized per developer
   * @return
   */
  public static String awsSuffix(){
    ConfigMgr cm = ConfigMgr.getInstance();
    //Development machine
    switch (cm.params.get(com.mapped.common.CoreConstants.ENVIRONMENT)) {
      case com.mapped.common.CoreConstants.DEV:
        if(cm.params.get(com.mapped.common.CoreConstants.ENV_SUFFIX).length() > 0) {
          return "-dev-" + cm.params.get(com.mapped.common.CoreConstants.ENV_SUFFIX).toLowerCase();
        }
        return "-dev";
      case com.mapped.common.CoreConstants.PROD:
        return "-prd";
      //At this time test and staging share resources
      default:
        return "-tst";
    }
  }

  public List<Character> getCharList() {
    return charList;
  }

  public List<Character> getLowerCharList() {
    return lowerCharList;
  }

}
