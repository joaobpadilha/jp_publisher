package com.mapped.publisher.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-10
 * Time: 8:28 AM
 * To change this template use File | Settings | File Templates.
 */
public interface BaseJson {

    public ObjectNode toJson () ;
    public boolean parseJson (JsonNode node) ;
}
