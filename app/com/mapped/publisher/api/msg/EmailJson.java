package com.mapped.publisher.api.msg;

import com.mapped.publisher.form.SendGridForm;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-10
 * Time: 8:28 AM
 * To change this template use File | Settings | File Templates.
 */
public class EmailJson {
  private String from;        //From SendGrid API
  private String to;
  private String subject;
  private String bodyText;
  private String cc;
  private String header;
  private List<Attachment> attachments;
  private int numAttachments;
  private String dkim;
  private String spam;
  private String charsets;
  private String html;
  private String envelope;
  private String spamScore;
  private String spf;
  private String cmpyId;
  private String userId;
  private String raw;

  /**
   * Attachments class either for file or internal
   */
  public static class Attachment {
    public String fileName;
    public String fileType;
    public String fileSize;
    public String internalFileName;
    public String contents;
  }

  public EmailJson() {
    attachments = new ArrayList<>();
  }

  public static EmailJson fromSendGrid(SendGridForm form) {
    EmailJson emailJson = new EmailJson();
    emailJson.setCc(form.getCc());
    emailJson.setBodyText(form.getText());
    emailJson.setHeader(form.getHeaders());
    emailJson.setSubject(form.getSubject());
    emailJson.setNumAttachments(form.getAttachments());
    emailJson.setFrom(form.getFrom());
    emailJson.setCharsets(form.getCharsets());
    emailJson.setDkim(form.getDkim());
    emailJson.setEnvelope(form.getEnvelope());
    emailJson.setHtml(form.getHtml());
    emailJson.setSpam(form.getSpam_report());
    emailJson.setSpamScore(form.getSpam_score());
    emailJson.setSpf(form.getSPF());
    emailJson.setRaw(form.getEmail());
    return emailJson;
  }

  public String getRaw() {
    return raw;
  }

  public void setRaw(String raw) {
    this.raw = raw;
  }

  public boolean hasTextAttachments() {
    for (Attachment a : attachments) {
      if (a.contents != null) {
        return true;
      }
    }
    return false;
  }

  public boolean hasS3Attachments() {
    for (Attachment a : attachments) {
      if (a.internalFileName != null) {
        return true;
      }
    }
    return false;
  }

  public void addAttachment(Attachment a) {
    attachments.add(a);
  }

  public void addAttachment(String fileName, String fileType, String fileSize, String internalName, String contents) {
    Attachment a = new Attachment();

    a.fileName = fileName;
    a.fileType = fileType;
    a.fileSize = fileSize;
    a.internalFileName = internalName;
    a.contents = contents;

    attachments.add(a);
  }

  public List<Attachment> getAttachments() {
    return attachments;
  }

  public void setAttachments(List<Attachment> attachments) {
    this.attachments = attachments;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getTo() {
    return to;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getBodyText() {
    return bodyText;
  }

  public void setBodyText(String bodyText) {
    this.bodyText = bodyText;
  }

  public String getCc() {
    return cc;
  }

  public void setCc(String cc) {
    this.cc = cc;
  }

  public String getHeader() {
    return header;
  }

  public void setHeader(String header) {
    this.header = header;
  }


  public int getNumAttachments() {
    return numAttachments;
  }

  public void setNumAttachments(int numAttachments) {
    this.numAttachments = numAttachments;
  }

  public String getDkim() {
    return dkim;
  }

  public void setDkim(String dkim) {
    this.dkim = dkim;
  }

  public String getSpam() {
    return spam;
  }

  public void setSpam(String spam) {
    this.spam = spam;
  }

  public String getCharsets() {
    return charsets;
  }

  public void setCharsets(String charsets) {
    this.charsets = charsets;
  }

  public String getHtml() {
    return html;
  }

  public void setHtml(String html) {
    this.html = html;
  }

  public String getEnvelope() {
    return envelope;
  }

  public void setEnvelope(String envelope) {
    this.envelope = envelope;
  }

  public String getSpamScore() {
    return spamScore;
  }

  public void setSpamScore(String spamScore) {
    this.spamScore = spamScore;
  }

  public String getSpf() {
    return spf;
  }

  public void setSpf(String spf) {
    this.spf = spf;
  }

  public String getCmpyId() {
    return cmpyId;
  }

  public void setCmpyId(String cmpyId) {
    this.cmpyId = cmpyId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }
}
