package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-18
 * Time: 3:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class LogoUploadForm {
    private String inCmpyId;
    private String inTripId;

    private String inCmpyPhotoName;
    private String inOriginPage;

    public String getInCmpyId() {
        return inCmpyId;
    }

    public void setInCmpyId(String inCmpyId) {
        this.inCmpyId = inCmpyId;
    }

    public String getInCmpyPhotoName() {
        return inCmpyPhotoName;
    }

    public void setInCmpyPhotoName(String inCmpyPhotoName) {
        this.inCmpyPhotoName = inCmpyPhotoName;
    }

    public String getInTripId() {
        return inTripId;
    }

    public void setInTripId(String inTripId) {
        this.inTripId = inTripId;
    }

    public String getInOriginPage() {
        return inOriginPage;
    }

    public void setInOriginPage(String inOriginPage) {
        this.inOriginPage = inOriginPage;
    }
}
