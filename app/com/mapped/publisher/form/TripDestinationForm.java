package com.mapped.publisher.form;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-28
 * Time: 1:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class TripDestinationForm {
    private String inTripId;
    private List<String> inDestId;

    public String getInTripId() {
        return inTripId;
    }

    public void setInTripId(String inTripId) {
        this.inTripId = inTripId;
    }

    public List<String> getInDestId() {
        return inDestId;
    }

    public void setInDestId(List<String> inDestId) {
        this.inDestId = inDestId;
    }
}
