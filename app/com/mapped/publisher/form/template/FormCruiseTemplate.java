package com.mapped.publisher.form.template;

import play.data.validation.Constraints;

/**
 * Form used to
 */
public  class FormCruiseTemplate {
  public String inTemplateId;
  public String inTmpltDetailId;
  @Constraints.Required
  public String inCruiseName;

  public int inDayOffset;
  public int inDuration;
  public String inStartTime;
  public String inEndTime;

  public Long inCruiseProviderId;
  public String inCruiseCmpyName;

  @Constraints.Required
  public String inCruisePortDepart;
  public Long inCruisePortDepartProviderId;
  @Constraints.Required
  public String inCruisePortArrive;
  public Long inCruisePortArriveProviderId;

  public String inCruiseNote;

}
