package com.mapped.publisher.form.template;

import play.data.validation.Constraints;

/**
 * Created by surge on 2016-01-04.
 */
public class FormActivityTemplate {
  @Constraints.Required
  public String inTemplateId;
  @Constraints.Required
  public String inBookingType;
  public int    inDayOffset;
  public int    inDuration;
  public String inStartTime;
  public String inEndTime;
  public String inTemplateDetailId;
  public String inLocStartName;
  public Long   inLocStartProvider;
  public String inLocFinishName;
  public Long   inLocFinishProvider;
  public String inActivityProviderName;
  public Long   inActivityProviderId;
  public String inActivityContact;
  public String inActivityNote;
  public String inActivityDestination;
  public String inActivityDestinationId;
}
