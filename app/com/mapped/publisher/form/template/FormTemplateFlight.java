package com.mapped.publisher.form.template;

import play.data.validation.Constraints;

import javax.validation.constraints.NotNull;

/**
 * Created by surge on 2016-01-04.
 */
public class FormTemplateFlight {
  @Constraints.Required
  @NotNull
  public String inTemplateId;
  public String inTemplateDetailId;
  public String inFlightNumber;

  public int    inDayOffset;
  public String inStartTime;
  public String inEndTime;

  @Constraints.Required
  public String inFlightDepartAirport;
  @Constraints.Required
  public String inFlightArrivalAirport;

  public Long   inFlightDepartAirportId;
  public Long   inFlightArrivalAirportId;
  
  public String inDepartureTerminal;
  public String inArrivalTerminal;
  
  public String inFlightNote;
}
