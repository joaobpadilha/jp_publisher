package com.mapped.publisher.form;

/**
 * Sendgrid Email Webhook API message
 * https://sendgrid.com/docs/API_Reference/Webhooks/parse.html
 */
public class SendGridForm {

  /**
   * The raw headers of the email.
   */
  private String headers;

  /**
   * Text body of email. If not set, email did not have a text body.
   */
  private String text;

  /**
   * HTML body of email. If not set, email did not have an HTML body.
   */
  private String html;

  /**
   * Email sender, as taken from the message headers.
   */
  private String from;

  /**
   * Email recipient field, as taken from the message headers.
   */
  private String to;

  /**
   * Email cc field, as taken from the message headers.
   */
  private String cc;

  /**
   * Email Subject.
   */
  private String subject;

  /**
   * A JSON string containing the verification results of any dkim and domain keys signatures in the message.
   */
  private String dkim;

  /**
   * The results of the Sender Policy Framework verification of the message sender and receiving IP address.
   */
  private String SPF;

  /**
   * A JSON string containing the SMTP envelope. This will have two variables: *to*, which is a single-element array
   * containing the address that we recieved the email to, and *from*, which is the return path for the message.
   */
  private String envelope;

  /**
   * A JSON string containing the character sets of the fields extracted from the message.
   */
  private String charsets;

  /**
   * Spam Assassin’s rating for whether or not this is spam.
   */
  private String spam_score;

  /**
   * Spam Assassin’s spam report.
   */
  private String spam_report;

  /**
   * Number of attachments included in email.
   */
  private int attachments;

  /**
   * A JSON string containing the attachmentX (see below) keys with another JSON string as the value.
   * This string will contain the keys *filename*, which is the name of the file (if it was provided) and *type*,
   * which is the [media type](http://en.wikipedia.org/wiki/Internet_media_type) of the file.
   */
  private String attachment_info;

  /**
   * These are file upload names, where N is the total number of attachments.
   * For example, if the number of attachments is 0, there will be no attachment files.
   * If the number of attachments is 3, parameters attachment1, attachment2, and attachment3 will have file uploads.
   * Attachments provided with this parameter, are provided in the form of file uploads. TNEF files (winmail.dat)
   * will be extracted and have any attachments posted.
   */
  private String attachment1;
  private String attachment2;

  /**
   * Undocumented Feature - RAW Email Body
   */
  private String email;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAttachment_info() {
    return attachment_info;
  }

  public void setAttachment_info(String attachment_info) {
    this.attachment_info = attachment_info;
  }

  public String getHeaders() {
    return headers;
  }

  public void setHeaders(String headers) {
    this.headers = headers;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getHtml() {
    return html;
  }

  public void setHtml(String html) {
    this.html = html;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getTo() {
    return to;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public String getCc() {
    return cc;
  }

  public void setCc(String cc) {
    this.cc = cc;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getDkim() {
    return dkim;
  }

  public void setDkim(String dkim) {
    this.dkim = dkim;
  }

  public String getSPF() {
    return SPF;
  }

  public void setSPF(String SPF) {
    this.SPF = SPF;
  }

  public String getEnvelope() {
    return envelope;
  }

  public void setEnvelope(String envelope) {
    this.envelope = envelope;
  }

  public String getCharsets() {
    return charsets;
  }

  public void setCharsets(String charsets) {
    this.charsets = charsets;
  }

  public String getSpam_score() {
    return spam_score;
  }

  public void setSpam_score(String spam_score) {
    this.spam_score = spam_score;
  }

  public String getSpam_report() {
    return spam_report;
  }

  public void setSpam_report(String spam_report) {
    this.spam_report = spam_report;
  }

  public int getAttachments() {
    return attachments;
  }

  public void setAttachments(int attachments) {
    this.attachments = attachments;
  }
}
