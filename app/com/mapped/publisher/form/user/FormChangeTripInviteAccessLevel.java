package com.mapped.publisher.form.user;

import com.mapped.publisher.common.SecurityMgr;

/**
 * Created by surge on 2016-01-04.
 */
public class FormChangeTripInviteAccessLevel {
  public String                  inInviteId;
  public SecurityMgr.AccessLevel inAccessLevel;

  public boolean isValid() {
    return inAccessLevel != null && inInviteId != null && inInviteId.length() > 0;
  }
}
