package com.mapped.publisher.form.user;

import com.mapped.publisher.common.SecurityMgr;

/**
 * Created by surge on 2016-01-04.
 */
public class FormInviteSharedTripUser {
  public String                  inFirstName;
  public String                  inLastName;
  public String                  inEmail;
  public String                  inNote;
  public SecurityMgr.AccessLevel inAccessLevel;

  public boolean isValid() {
    return inFirstName != null && inLastName != null && inEmail != null &&
           inFirstName.length() > 0 && inLastName.length() > 0 && inEmail.length() > 0;
  }
}
