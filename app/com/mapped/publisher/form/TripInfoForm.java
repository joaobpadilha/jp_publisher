package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-13
 * Time: 12:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class TripInfoForm {

  private String  inTripId;
  private String  inTripName;
  private String  inTripAgency;
  private String  inStartDate;
  private String  inEndDate;
  private String  inTripNote;
  private String  inTripTag;
  private Boolean inMessengerSubscription;


  public Boolean getInMessengerSubscription() {
    return inMessengerSubscription;
  }

  public TripInfoForm setInMessengerSubscription(Boolean inMessengerSubscription) {
    this.inMessengerSubscription = inMessengerSubscription;
    return this;
  }

  public String getInTripId() {
    return inTripId;
  }

  public void setInTripId(String inTripId) {
    this.inTripId = inTripId;
  }

  public String getInTripName() {
    return inTripName;
  }

  public void setInTripName(String inTripName) {
    this.inTripName = inTripName;
  }

  public String getInTripAgency() {
    return inTripAgency;
  }

  public void setInTripAgency(String inTripAgency) {
    this.inTripAgency = inTripAgency;
  }

  public String getInStartDate() {
    return inStartDate;
  }

  public void setInStartDate(String inStartDate) {
    this.inStartDate = inStartDate;
  }

  public String getInEndDate() {
    return inEndDate;
  }

  public void setInEndDate(String inEndDate) {
    this.inEndDate = inEndDate;
  }

  public String getInTripNote() {
    return inTripNote;
  }

  public void setInTripNote(String inTripNote) {
    this.inTripNote = inTripNote;
  }

  public String getInTripTag() {
    return inTripTag;
  }

  public void setInTripTag(String inTripTag) {
    this.inTripTag = inTripTag;
  }
}
