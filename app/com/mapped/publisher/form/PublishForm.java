package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-03
 * Time: 11:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class PublishForm {
  private String inTripId;
  private String inComments;
  private String inPublish;
  private String inAllowSelfRegistration;
  private String inGroupId;
  private boolean inSkipTravellerEmail = false;
  private String inViewType;

  public String getInTripId() {
    return inTripId;
  }

  public void setInTripId(String inTripId) {
    this.inTripId = inTripId;
  }

  public String getInComments() {
    return inComments;
  }

  public void setInComments(String inComments) {
    this.inComments = inComments;
  }

  public String getInPublish() {
    return inPublish;
  }

  public void setInPublish(String publish) {
    this.inPublish = publish;
  }

  public String getInAllowSelfRegistration() {
    return inAllowSelfRegistration;
  }

  public void setInAllowSelfRegistration(String inAllowSelfRegistration) {
    this.inAllowSelfRegistration = inAllowSelfRegistration;
  }

  public String getInGroupId() {
    return inGroupId;
  }

  public void setInGroupId(String inGroupId) {
    this.inGroupId = inGroupId;
  }

  public boolean getInSkipTravellerEmail() {
    return inSkipTravellerEmail;
  }

  public void setInSkipTravellerEmail(boolean inSkipTravellerEmail) {
    this.inSkipTravellerEmail = inSkipTravellerEmail;
  }

  public String getInViewType() {
    return inViewType;
  }

  public void setInViewType(String inViewType) {
    this.inViewType = inViewType;
  }
}
