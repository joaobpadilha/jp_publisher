package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-11-18
 * Time: 11:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class TripGroupForm {
    private  String inTripId;
    private  String inGroupId;
    private  String inName;
    private String inComments;

    public String getInTripId() {
        return inTripId;
    }

    public void setInTripId(String inTripId) {
        this.inTripId = inTripId;
    }

    public String getInGroupId() {
        return inGroupId;
    }

    public void setInGroupId(String inGroupId) {
        this.inGroupId = inGroupId;
    }

    public String getInName() {
        return inName;
    }

    public void setInName(String inName) {
        this.inName = inName;
    }

    public String getInComments() {
        return inComments;
    }

    public void setInComments(String inComments) {
        this.inComments = inComments;
    }
}
