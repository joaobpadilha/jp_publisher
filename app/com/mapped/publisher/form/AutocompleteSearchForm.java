package com.mapped.publisher.form;

import play.data.validation.Constraints;

import javax.validation.constraints.NotNull;

/**
 * Created by surge on 2015-03-31.
 */
public class AutocompleteSearchForm {
  @Constraints.Required @NotNull
  public String term;
}
