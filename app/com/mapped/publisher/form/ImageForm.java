package com.mapped.publisher.form;

import models.publisher.ImageLibrary;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-04-09
 * Time: 10:25 AM
 * To change this template use File | Settings | File Templates.
 */
public class ImageForm {
  private String              inCmpyId;
  private String              inTripId;
  private String              inTemplateId;
  private Long                inNoteId;
  private String              inDocId;
  private String              inPageId;
  private String              inVendorId;
  private String              inKeyword;
  private int                 inImgSrc;
  private String              inImgId;
  private String              inImgUrl;
  private ImageLibrary.ImgSrc inImgSearchType;
  private String              inWebUrl;
  private String              inFileName;
  private int                 inMultipleFiles;
  private String              inPhotoCaption;


  public String getInCmpyId() {
    return inCmpyId;
  }

  public void setInCmpyId(String inCmpyId) {
    this.inCmpyId = inCmpyId;
  }

  public String getInTripId() {
    return inTripId;
  }

  public void setInTripId(String inTripId) {
    this.inTripId = inTripId;
  }

  public String getInDocId() {
    return inDocId;
  }

  public void setInDocId(String inDocId) {
    this.inDocId = inDocId;
  }

  public String getInPageId() {
    return inPageId;
  }

  public void setInPageId(String inPageId) {
    this.inPageId = inPageId;
  }

  public int getInImgSrc() {
    return inImgSrc;
  }

  public void setInImgSrc(int inImgSrc) {
    this.inImgSrc = inImgSrc;
  }

  public String getInImgId() {
    return inImgId;
  }

  public void setInImgId(String inImgId) {
    this.inImgId = inImgId;
  }

  public String getInImgUrl() {
    return inImgUrl;
  }

  public void setInImgUrl(String inImgUrl) {
    this.inImgUrl = inImgUrl;
  }

  public String getInKeyword() {
    return inKeyword;
  }

  public void setInKeyword(String inKeyword) {
    this.inKeyword = inKeyword;
  }

  public ImageLibrary.ImgSrc getInImgSearchType() {
    return inImgSearchType;
  }

  public void setInImgSearchType(ImageLibrary.ImgSrc inImgSearchType) {
    this.inImgSearchType = inImgSearchType;
  }

  public String getInWebUrl() {
    return inWebUrl;
  }

  public void setInWebUrl(String inWebUrl) {
    this.inWebUrl = inWebUrl;
  }

  public String getInVendorId() {
    return inVendorId;
  }

  public void setInVendorId(String inVendorId) {
    this.inVendorId = inVendorId;
  }

  public String getInFileName() {
    return inFileName;
  }

  public void setInFileName(String inFileName) {
    this.inFileName = inFileName;
  }

  public int getInMultipleFiles() {
    return inMultipleFiles;
  }

  public void setInMultipleFiles(int inMultipleFiles) {
    this.inMultipleFiles = inMultipleFiles;
  }

  public String getInPhotoCaption() {
    return inPhotoCaption;
  }

  public void setInPhotoCaption(String inPhotoCaption) {
    this.inPhotoCaption = inPhotoCaption;
  }

  public String getInTemplateId() {
    return inTemplateId;
  }

  public void setInTemplateId(String inTemplateId) {
    this.inTemplateId = inTemplateId;
  }

  public Long getInNoteId() {
    return inNoteId;
  }

  public void setInNoteId(Long inNoteId) {
    this.inNoteId = inNoteId;
  }
}
