package com.mapped.publisher.form;

import com.mapped.publisher.common.SessionMgr;
import play.data.validation.Constraints;

import java.io.Serializable;

/**
 * Created by twong on 2015-02-23.
 */
public class RecordLocatorForm implements Serializable {

  public enum Provider  {SABRE,TRAVELPORT,WORLDSPAN, AMADEUS};

  private String inFirstName;
  private String inTripId;
  private String userId;

  @Constraints.Required
  private String inRecordLocator;
  private String inLastName;
  private Provider inProvider;

  public String getInRecordLocator() {
    return inRecordLocator;
  }

  public void setInRecordLocator(String inRecordLocator) {
    this.inRecordLocator = inRecordLocator;
  }

  public String getInLastName() {
    return inLastName;
  }

  public void setInLastName(String inLastName) {
    this.inLastName = inLastName;
  }

  public String getInFirstName() {
    return inFirstName;
  }

  public void setInFirstName(String inFirstName) {
    this.inFirstName = inFirstName;
  }

  public Provider getInProvider() {
    return inProvider;
  }

  public void setInProvider(Provider inProvider) {
    this.inProvider = inProvider;
  }

  public String getInTripId() {
    return inTripId;
  }

  public void setInTripId(String inTripId) {
    this.inTripId = inTripId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }
}
