package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-15
 * Time: 9:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class TourPassengerForm {
  private String inTripId;
  private String inStartDate;
  private String inEndDate;
  private String inEmail;
  private String inName;
  private String inFirstName;
  private String inLastName;

  private String inGroupId;
  private String inMSISDN;

  private String inViewType;

  public String getInMSISDN() {
    return inMSISDN;
  }

  public void setInMSISDN(String inMSISDN) {
    this.inMSISDN = inMSISDN;
  }

  public String getInTripId() {
    return inTripId;
  }

  public void setInTripId(String inTripId) {
    this.inTripId = inTripId;
  }

  public String getInStartDate() {
    return inStartDate;
  }

  public void setInStartDate(String inStartDate) {
    this.inStartDate = inStartDate;
  }

  public String getInEndDate() {
    return inEndDate;
  }

  public void setInEndDate(String inEndDate) {
    this.inEndDate = inEndDate;
  }

  public String getInEmail() {
    return inEmail;
  }

  public void setInEmail(String inEmail) {
    this.inEmail = inEmail;
  }

  public String getInName() {
    return inName;
  }

  public void setInName(String inName) {
    this.inName = inName;
  }

  public String getInGroupId() {
    return inGroupId;
  }

  public void setInGroupId(String inGroupId) {
    this.inGroupId = inGroupId;
  }

  public String getInFirstName() {
    return inFirstName;
  }

  public void setInFirstName(String inFirstName) {
    this.inFirstName = inFirstName;
  }

  public String getInLastName() {
    return inLastName;
  }

  public void setInLastName(String inLastName) {
    this.inLastName = inLastName;
  }

  public String getInViewType() {
    return inViewType;
  }

  public void setInViewType(String inViewType) {
    this.inViewType = inViewType;
  }
}
