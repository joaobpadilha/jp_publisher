package com.mapped.publisher.form.trip;

/**
 * Created by surge on 2016-01-04.
 */
public class FormToggleBranding {
  public String inTripId;
  public String inCmpyId;

  public boolean inEnabled;

  public boolean isValid() {
    return inTripId != null && inCmpyId != null;
  }
}
