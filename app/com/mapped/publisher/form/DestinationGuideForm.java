package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-07
 * Time: 2:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class DestinationGuideForm {
  private String inDestId;
  private String inDestGuideId;
  private String inDestGuideName;
  private String inDestGuideIntro;
  private String inDestGuideDesc;
  private String inDestGuideLandmark;
  private String inDestGuideAddr;
  private String inDestGuideCity;
  private String inDestGuideState;
  private String inDestGuideCountry;
  private String inDestGuideLocLat;
  private String inDestGuideLocLong;
  private String inDestGuideTag;
  private String inDestGuideDate;
  private String inDestGuideTime;
  private String inDestGuideZip;

  private int inDestGuideRecommendationType;

  public String getInDestId() {
    return inDestId;
  }

  public void setInDestId(String inDestId) {
    this.inDestId = inDestId;
  }

  public String getInDestGuideId() {
    return inDestGuideId;
  }

  public void setInDestGuideId(String inDestGuideId) {
    this.inDestGuideId = inDestGuideId;
  }

  public String getInDestGuideName() {
    return inDestGuideName;
  }

  public void setInDestGuideName(String inDestGuideName) {
    this.inDestGuideName = inDestGuideName;
  }

  public String getInDestGuideIntro() {
    return inDestGuideIntro;
  }

  public void setInDestGuideIntro(String inDestGuideIntro) {
    this.inDestGuideIntro = inDestGuideIntro;
  }

  public String getInDestGuideDesc() {
    return inDestGuideDesc;
  }

  public void setInDestGuideDesc(String inDestGuideDesc) {
    this.inDestGuideDesc = inDestGuideDesc;
  }

  public String getInDestGuideLandmark() {
    return inDestGuideLandmark;
  }

  public void setInDestGuideLandmark(String inDestGuideLandmark) {
    this.inDestGuideLandmark = inDestGuideLandmark;
  }

  public String getInDestGuideAddr() {
    return inDestGuideAddr;
  }

  public void setInDestGuideAddr(String inDestGuideAddr) {
    this.inDestGuideAddr = inDestGuideAddr;
  }

  public String getInDestGuideCity() {
    return inDestGuideCity;
  }

  public void setInDestGuideCity(String inDestGuideCity) {
    this.inDestGuideCity = inDestGuideCity;
  }

  public String getInDestGuideState() {
    return inDestGuideState;
  }

  public void setInDestGuideState(String inDestGuideState) {
    this.inDestGuideState = inDestGuideState;
  }

  public String getInDestGuideCountry() {
    return inDestGuideCountry;
  }

  public void setInDestGuideCountry(String inDestGuideCountry) {
    this.inDestGuideCountry = inDestGuideCountry;
  }

  public String getInDestGuideLocLat() {
    return inDestGuideLocLat;
  }

  public void setInDestGuideLocLat(String inDestGuideLocLat) {
    this.inDestGuideLocLat = inDestGuideLocLat;
  }

  public String getInDestGuideLocLong() {
    return inDestGuideLocLong;
  }

  public void setInDestGuideLocLong(String inDestGuideLocLong) {
    this.inDestGuideLocLong = inDestGuideLocLong;
  }

  public String getInDestGuideTag() {
    return inDestGuideTag;
  }

  public void setInDestGuideTag(String inDestGuideTag) {
    this.inDestGuideTag = inDestGuideTag;
  }

  public int getInDestGuideRecommendationType() {
    return inDestGuideRecommendationType;
  }

  public void setInDestGuideRecommendationType(int inDestGuideRecommendationType) {
    this.inDestGuideRecommendationType = inDestGuideRecommendationType;
  }

  public String getInDestGuideDate() {
    return inDestGuideDate;
  }

  public void setInDestGuideDate(String inDestGuideDate) {
    this.inDestGuideDate = inDestGuideDate;
  }

  public String getInDestGuideTime() {
    return inDestGuideTime;
  }

  public void setInDestGuideTime(String inDestGuideTime) {
    this.inDestGuideTime = inDestGuideTime;
  }

  public String getInDestGuideZip() {
    return inDestGuideZip;
  }

  public void setInDestGuideZip(String inDestGuideZip) {
    this.inDestGuideZip = inDestGuideZip;
  }
}


