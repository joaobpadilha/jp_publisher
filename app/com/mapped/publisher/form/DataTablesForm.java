package com.mapped.publisher.form;

import java.util.List;

/**
 * All parameters that can be sent by DataTables.net table component.
 * Corresponds to Protocol version 1.10.5.
 *
 * <p/>
 * More info see: https://datatables.net/manual/server-side
 * <p/>
 * <p/>
 * Data has to be marshalled into JSON prior to sending it to the server endpoint. Like the following:
 * <pre>
 *  $('#tableIdentifier').DataTable({
 *    "processing": true,
 *    "serverSide": true,
 *    "ajax": {
 *      url: '@controllers.routes.PoiController.poiFeedDT(fv.currSrc, fv.currFeedState.ordinal())',
 *      type: 'POST',
 *      contentType: "application/json",
 *      data: function (d) {
 *        return  JSON.stringify(d);
 *      }
 *    }
 *  });
 * </pre>
 * <p/>
 * Created by surge on 2015-02-24.
 */
public class DataTablesForm {

  /**
   * Draw counter. This is used by DataTables to ensure that the Ajax returns from server-side processing requests
   * are drawn in sequence by DataTables (Ajax requests are asynchronous and thus can return out of sequence). This
   * is used as part of the draw return parameter (see below).
   */
  public Integer draw;
  ;
  /**
   * Paging first record indicator. This is the start point in the current data set (0 index based - i.e. 0 is the
   * first record).
   */
  public Integer start;
  /**
   * Number of records that the table can display in the current draw. It is expected that the number of records
   * returned will be equal to this number, unless the server has fewer records to return. Note that this can be -1
   * to indicate that all records should be returned (although that negates any benefits of server-side processing!)
   */
  public Integer length;
  public List<Column> columns;
  public List<Order> order;
  /**
   * - Global search value. To be applied to all columns which have searchable as true.
   * - true if the global filter should be treated as a regular expression for advanced searching,
   *   false otherwise. Note that normally server-side processing scripts will not perform regular expression searching
   *   for performance reasons on large data sets, but it is technically possible and at the discretion of your script.
   */
  public Search search;

  public static class Search {
    /**
     * Search value to apply to this specific column.
     */
    public String value;
    /**
     * Flag to indicate if the search term for this column should be treated as regular expression (true) or not
     * (false). As with global search, normally server-side processing scripts will not perform regular expression
     * searching for performance reasons on large data sets, but it is technically possible and at the discretion
     * of your script.
     */
    public Boolean regex;
  }

  public static class Order {
    /**
     * Column to which ordering should be applied. This is an index reference to the columns array of information
     * that is also submitted to the server.
     */
    public Integer column;
    /**
     * Ordering direction for this column. It will be asc or desc to indicate ascending ordering or descending
     * ordering, respectively.
     */
    public String dir;
  }

  public static class Column {
    /**
     * Column's data source, as defined by columns.dataDT.
     */
    public String data;
    /**
     * Column's name, as defined by columns.nameDT.
     */
    public String name;
    /**
     * Flag to indicate if this column is searchable (true) or not (false). This is controlled by columns.searchableDT.
     */
    public Boolean searchable;
    /**
     * Flag to indicate if this column is orderable (true) or not (false). This is controlled by columns.orderableDT.
     */
    public Boolean orderable;
    public Search search;
  }
}
