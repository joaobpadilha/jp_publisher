package com.mapped.publisher.form.api;

/**
 * Created by surge on 2016-01-04.
 */
public class AddUserLinkForm {
  public long   inPk;
  public String inUserId;
  public String inSrcUserId;
  public long   inCmpyTokenPk;
  public String inCmpyId;
}
