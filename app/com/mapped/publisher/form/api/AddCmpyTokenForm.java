package com.mapped.publisher.form.api;

/**
 * Created by surge on 2016-01-04.
 */
public class AddCmpyTokenForm {
  public Long   inPk;
  public String inCmpyId;
  public String inSrcCmpyId;
  public int    inSrcTypeId;
  public String token;
  public String tokenType;
  public long   expiryTs;
  public String tag;
}
