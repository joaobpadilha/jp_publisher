package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-07-25
 * Time: 11:31 AM
 * To change this template use File | Settings | File Templates.
 */
public class TemplateSearchForm {
    private String inCmpyId;
    private String inSearchTerm;
    private String inTripId;


    public String getInCmpyId() {
        return inCmpyId;
    }

    public void setInCmpyId(String inCmpyId) {
        this.inCmpyId = inCmpyId;
    }

    public String getInSearchTerm() {
        return inSearchTerm;
    }

    public void setInSearchTerm(String inSearchTerm) {
        this.inSearchTerm = inSearchTerm;
    }

    public String getInTripId() {
        return inTripId;
    }

    public void setInTripId(String inTripId) {
        this.inTripId = inTripId;
    }
}
