package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-22
 * Time: 9:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class ResetPwdForm {
  Long   inAccountId;
  String inUserId;
  String inEmail;
  String inLinkId;
  String inNewPwd;
  String inConfirmPwd;
  String inCmpyId;

  public Long getInAccountId() {
    return inAccountId;
  }

  public void setInAccountId(Long inAccountId) {
    this.inAccountId = inAccountId;
  }

  public String getInUserId() {
    return inUserId;
  }

  public void setInUserId(String inUserId) {
    this.inUserId = inUserId;
  }

  public String getInEmail() {
    return inEmail;
  }

  public void setInEmail(String inEmail) {
    this.inEmail = inEmail;
  }

  public String getInLinkId() {
    return inLinkId;
  }

  public void setInLinkId(String inLinkId) {
    this.inLinkId = inLinkId;
  }

  public String getInNewPwd() {
    return inNewPwd;
  }

  public void setInNewPwd(String inNewPwd) {
    this.inNewPwd = inNewPwd;
  }

  public String getInConfirmPwd() {
    return inConfirmPwd;
  }

  public void setInConfirmPwd(String inConfirmPwd) {
    this.inConfirmPwd = inConfirmPwd;
  }

  public String getInCmpyId() {
    return inCmpyId;
  }

  public void setInCmpyId(String inCmpyId) {
    this.inCmpyId = inCmpyId;
  }
}
