package com.mapped.publisher.form;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-22
 * Time: 9:41 AM
 * To change this template use File | Settings | File Templates.
 */
public class GroupForm {
    private String inCmpyId;
    private String inGroupId;
    private String inGroupName;
    private String inGroupComments;
    private String inUserId;

    private List<String> inGroupUsers;

    public String getInCmpyId() {
        return inCmpyId;
    }

    public void setInCmpyId(String inCmpyId) {
        this.inCmpyId = inCmpyId;
    }

    public String getInGroupId() {
        return inGroupId;
    }

    public void setInGroupId(String inGroupId) {
        this.inGroupId = inGroupId;
    }

    public String getInGroupName() {
        return inGroupName;
    }

    public void setInGroupName(String inGroupName) {
        this.inGroupName = inGroupName;
    }

    public String getInGroupComments() {
        return inGroupComments;
    }

    public void setInGroupComments(String inGroupComments) {
        this.inGroupComments = inGroupComments;
    }

    public String getInUserId() {
        return inUserId;
    }

    public void setInUserId(String inUserId) {
        this.inUserId = inUserId;
    }

    public List<String> getInGroupUsers() {
        return inGroupUsers;
    }

    public void setInGroupUsers(List<String> inGroupUsers) {
        this.inGroupUsers = inGroupUsers;
    }
}
