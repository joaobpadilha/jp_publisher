package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-07
 * Time: 11:58 AM
 * To change this template use File | Settings | File Templates.
 */
public class DestinationForm {
  private String inTripId;
  private String inDestId;
  private String inDestLocLat;
  private String inDestLocLong;
  private String inDestName;
  private String inCmpyId;
  private String inDestinationTypeId;
  private String inDestCity;
  private String inDestCountry;
  private String inDestIntro;
  private String inDestTag;
  private boolean inMergeBookings;
  private boolean inPrivateDest;

  public String getInDestId() {
    return inDestId;
  }

  public void setInDestId(String inDestId) {
    this.inDestId = inDestId;
  }

  public String getInDestLocLat() {
    return inDestLocLat;
  }

  public void setInDestLocLat(String inDestLocLat) {
    this.inDestLocLat = inDestLocLat;
  }

  public String getInDestLocLong() {
    return inDestLocLong;
  }

  public void setInDestLocLong(String inDestLocLong) {
    this.inDestLocLong = inDestLocLong;
  }

  public String getInDestName() {
    return inDestName;
  }

  public void setInDestName(String inDestName) {
    this.inDestName = inDestName;
  }

  public String getInCmpyId() {
    return inCmpyId;
  }

  public void setInCmpyId(String inCmpyId) {
    this.inCmpyId = inCmpyId;
  }

  public String getInDestinationTypeId() {
    return inDestinationTypeId;
  }

  public void setInDestinationTypeId(String inDestinationTypeId) {
    this.inDestinationTypeId = inDestinationTypeId;
  }

  public String getInDestCity() {
    return inDestCity;
  }

  public void setInDestCity(String inDestCity) {
    this.inDestCity = inDestCity;
  }

  public String getInDestCountry() {
    return inDestCountry;
  }

  public void setInDestCountry(String inDestCountry) {
    this.inDestCountry = inDestCountry;
  }

  public String getInDestIntro() {
    return inDestIntro;
  }

  public void setInDestIntro(String inDestIntro) {
    this.inDestIntro = inDestIntro;
  }

  public String getInDestTag() {
    return inDestTag;
  }

  public void setInDestTag(String inDestTag) {
    this.inDestTag = inDestTag;
  }

  public String getInTripId() {
    return inTripId;
  }

  public void setInTripId(String inTripId) {
    this.inTripId = inTripId;
  }

  public boolean isInMergeBookings() {
    return inMergeBookings;
  }

  public void setInMergeBookings(boolean inMergeBookings) {
    this.inMergeBookings = inMergeBookings;
  }

  public boolean isInPrivateDest() {
    return inPrivateDest;
  }

  public void setInPrivateDest(boolean inPrivateDest) {
    this.inPrivateDest = inPrivateDest;
  }
}
