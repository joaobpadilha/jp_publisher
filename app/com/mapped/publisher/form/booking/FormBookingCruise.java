package com.mapped.publisher.form.booking;

import org.apache.commons.lang3.StringUtils;
import play.data.validation.Constraints;

import java.util.List;

/**
 * Form used to
 */
public  class FormBookingCruise
    extends FormBookingBase {
  @Constraints.Required
  public String inCruiseName;
  public Long inCruiseProviderId;

  public String inCruiseCmpyName;

  @Constraints.Required
  public String inCruisePortDepart;
  public Long inCruisePortDepartId;
  @Constraints.Required
  public String inCruisePortArrive;
  public Long inCruisePortArriveId;

  public String inCruiseCategory;
  public String inCruiseCabinNumber;
  public String inCruiseDeck;
  public String inCruiseBedding;
  public String inCruiseMeal;


  public String inCruiseImportant;
  public String inCruiseCancellation;
  public String inCruiseStartTimezone;
  public String inCruiseFinishTimezone;

  /**
   * Cruise booking number
   */
  public String inCruiseBooking;
  /**
   * Cruise record locator
   */
  public String inCruiseLocator;

  public String inCruiseNote;


  @Constraints.Required
  public String inCruiseDepartDate;
  @Constraints.Required
  public String inCruiseArriveDate;
  @Constraints.Required
  public String inCruiseBoardTime;
  @Constraints.Required
  public String inCruiseDisembarkTime;


}
