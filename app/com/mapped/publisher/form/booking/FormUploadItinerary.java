package com.mapped.publisher.form.booking;

import com.mapped.publisher.parse.extractor.booking.BookingExtractor;

/**
 * Created by surge on 2016-01-04.
 */
public class FormUploadItinerary {
  public String                   inPDFFileName;
  public BookingExtractor.Parsers inFileProvider;
  public boolean uploaded = false;
}
