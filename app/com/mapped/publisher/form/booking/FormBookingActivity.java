package com.mapped.publisher.form.booking;

import org.apache.commons.lang3.StringUtils;
import play.data.validation.Constraints;

import java.util.List;

/**
 * Created by surge on 2016-01-04.
 */
public class FormBookingActivity
    extends FormBookingBase {
  @Constraints.Required
  public String inBookingType;
  public String inLocStartName;
  public Long   inLocStartProvider;
  public String inLocFinishName;
  public Long   inLocFinishProvider;
  public String inActivityProviderName;
  public Long   inActivityProviderId;
  public String inActivityContact;
  @Constraints.Required
  public String inActivityStartDate;
  public String inActivityStartTime;
  public String inActivityEndDate;
  public String inActivityEndTime;
  public String inActivityBooking;
  public String inActivityNote;
  public String inActivityDestination;
  public String inActivityDestinationId;


  public String inActivityImportant;
  public String inActivityCancellation;
  public String inActivityStartTimezone;
  public String inActivityFinishTimezone;

  public String inActivityServiceType;


}
