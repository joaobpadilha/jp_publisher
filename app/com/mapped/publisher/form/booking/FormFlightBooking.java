package com.mapped.publisher.form.booking;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by surge on 2016-01-04.
 */
public class FormFlightBooking
    extends FormBookingBase {
  public String inFlightNumber;
  public String inFlightDepartAirport;
  public String inFlightArrivalAirport;
  public Long   inFlightAirlineId;
  public Long   inFlightDepartAirportId;
  public Long   inFlightArrivalAirportId;
  public String inFlightDate;
  public String inFlightTime;
  public String inFlightArrivalDate;
  public String inFlightArrivalTime;
  public String inFlightBooking;
  public String inFlightNote;
  public String inFlightDepartTerminal;
  public String inFlightArrivalTerminal;


  public String inFlightImportant;
  public String inFlightCancellation;
  public String inFlightStartTimezone;
  public String inFlightFinishTimezone;
  
  public List<Passenger> passengers;


  public static class Passenger {
    public String firstName;
    public String lastName;
    public String eticket;
    public String seat;
    public String fClass;
    public String frequentFlyer;
    public String meal;
    
    public boolean isEmpty() {
      return StringUtils.isEmpty(firstName) && StringUtils.isEmpty(lastName) &&
      StringUtils.isEmpty(eticket) && StringUtils.isEmpty(seat) && StringUtils.isEmpty(fClass)
      && StringUtils.isEmpty(frequentFlyer) && StringUtils.isEmpty(meal);
    }
  }
}
