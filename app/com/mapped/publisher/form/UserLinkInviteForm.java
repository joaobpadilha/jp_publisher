package com.mapped.publisher.form;

/**
 * Created by twong on 2014-05-25.
 */
public class UserLinkInviteForm {
  private String inInviteId;
  private String inUserId;
  private String inTerm;

  public String getInInviteId() {
    return inInviteId;
  }

  public void setInInviteId(String inInviteId) {
    this.inInviteId = inInviteId;
  }

  public String getInUserId() {
    return inUserId;
  }

  public void setInUserId(String inUserId) {
    this.inUserId = inUserId;
  }

  public String getInTerm() {
    return inTerm;
  }

  public void setInTerm(String inTerm) {
    this.inTerm = inTerm;
  }
}
