package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-22
 * Time: 9:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class ChangePwdForm {
    private String inUserId;
    private String inOrigPwd;
    private String inNewPwd;
    private String inConfirmPwd;

    public String getInUserId() {
        return inUserId;
    }

    public void setInUserId(String inUserId) {
        this.inUserId = inUserId;
    }

    public String getInOrigPwd() {
        return inOrigPwd;
    }

    public void setInOrigPwd(String inOrigPwd) {
        this.inOrigPwd = inOrigPwd;
    }

    public String getInNewPwd() {
        return inNewPwd;
    }

    public void setInNewPwd(String inNewPwd) {
        this.inNewPwd = inNewPwd;
    }

    public String getInConfirmPwd() {
        return inConfirmPwd;
    }

    public void setInConfirmPwd(String inConfirmPwd) {
        this.inConfirmPwd = inConfirmPwd;
    }
}
