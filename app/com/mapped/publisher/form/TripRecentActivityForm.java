package com.mapped.publisher.form;

/**
 * Created by surge on 2014-05-26.
 */
public class TripRecentActivityForm {
  private final static int DEFAULT_IN_START_POS = 0;
  private final static int DEFAULT_IN_MAX_RESULTS = 15;
  private final static int QUERY_LOW_LIMIT = 0;
  private final static int QUERY_HIGH_LIMIT = 50;

  private int inStartPos = DEFAULT_IN_START_POS;
  private int inMaxResults = DEFAULT_IN_MAX_RESULTS;

  public int getInStartPos() {
    return inStartPos;
  }

  public void setInStartPos(int inStartPos) {
    if (inStartPos < 0 || inStartPos > 1000){
      this.inStartPos = 0;
    }
    else {
      this.inStartPos = inStartPos;
    }
  }

  public int getInMaxResults() {
    return inMaxResults;
  }

  public void setInMaxResults(int inMaxResults) {
    if (inMaxResults < QUERY_LOW_LIMIT || inMaxResults > QUERY_HIGH_LIMIT)
    {
      this.inMaxResults = DEFAULT_IN_MAX_RESULTS;
    }
    else {
      this.inMaxResults = inMaxResults;
    }
  }

}
