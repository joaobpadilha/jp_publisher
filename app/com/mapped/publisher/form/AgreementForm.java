package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-16
 * Time: 10:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class AgreementForm {
    private String inCmpyId;
    private String inUserId;
    private String inAgreementId;
    private String inAccept;
    private String inAuthorized;


    public String getInCmpyId() {
        return inCmpyId;
    }

    public void setInCmpyId(String inCmpyId) {
        this.inCmpyId = inCmpyId;
    }

    public String getInUserId() {
        return inUserId;
    }

    public void setInUserId(String inUserId) {
        this.inUserId = inUserId;
    }

    public String getInAgreementId() {
        return inAgreementId;
    }

    public void setInAgreementId(String inAgreementId) {
        this.inAgreementId = inAgreementId;
    }

    public String getInAccept() {
        return inAccept;
    }

    public void setInAccept(String inAccept) {
        this.inAccept = inAccept;
    }

    public String getInAuthorized() {
        return inAuthorized;
    }

    public void setInAuthorized(String inAuthorized) {
        this.inAuthorized = inAuthorized;
    }

    public boolean hasErrors () {
        if (inCmpyId == null || inCmpyId.length() == 0)
            return  true;

        if (inUserId == null || inUserId.length() == 0)
            return  true;

        if (inAgreementId == null || inAgreementId.length() == 0)
            return  true;

        if (inAccept == null || inAccept.length() == 0)
            return  true;

        if (inAuthorized == null || inAuthorized.length() == 0)
            return  true;

        return false;
    }
}
