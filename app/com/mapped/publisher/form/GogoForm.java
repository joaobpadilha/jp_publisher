package com.mapped.publisher.form;

/**
 * Created by twong on 25/06/17.
 */
public class GogoForm {
    private String inGogoUserId;
    private String inGogoPwd;
    private String inGogoBookingId;

    public String getInGogoUserId() {
        return inGogoUserId;
    }

    public void setInGogoUserId(String inGogoUserId) {
        this.inGogoUserId = inGogoUserId;
    }

    public String getInGogoPwd() {
        return inGogoPwd;
    }

    public void setInGogoPwd(String inGogoPwd) {
        this.inGogoPwd = inGogoPwd;
    }

    public String getInGogoBookingId() {
        return inGogoBookingId;
    }

    public void setInGogoBookingId(String inGogoBookingId) {
        this.inGogoBookingId = inGogoBookingId;
    }


}
