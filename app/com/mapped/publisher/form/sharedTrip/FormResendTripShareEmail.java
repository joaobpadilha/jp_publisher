package com.mapped.publisher.form.sharedTrip;

/**
 * Created by surge on 2016-01-04.
 */
public class FormResendTripShareEmail {
  public String inShareUserId;
  public String inShareNote;

  public boolean isValid() {
    return inShareUserId != null && inShareUserId.length() > 0;
  }
}
