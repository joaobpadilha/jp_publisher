package com.mapped.publisher.form.inbox;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2016-01-04.
 */
public class AddBookingsForm {
  public String  inTripId;
  public String  inTripName;
  public String  inBookings;
  public String  inKeyword;
  public String  inSource;
  public boolean inIncludeNewBookings;

  public static List<Long> getBookingPks(String inBookings) {
    List<Long> list = new ArrayList<>();
    if (inBookings != null && inBookings.length() > 0) {
      String[] tokens = inBookings.split(",");
      for (String token : tokens) {
        try {
          Long l = Long.parseLong(token);
          list.add(l);
        }
        catch (Exception e) {

        }
      }
    }

    return list;

  }
}
