package com.mapped.publisher.form.inbox;

/**
 * Created by surge on 2016-01-04.
 */
public class SearchBookingsForm {
  public String inKeyword;
  public String inSource;
  public boolean inNewBookingsOnly = false;
  public int inStartPos;
}
