package com.mapped.publisher.form.admin;

/**
 * Created by surge on 2016-01-04.
 */
public class FormCapability {
  public Integer capPk;
  public String  capSummary;
  public String  capDescription;
  public Boolean capEnabled;
  public Boolean capUserControlled;
}
