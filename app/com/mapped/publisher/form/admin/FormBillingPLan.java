package com.mapped.publisher.form.admin;

import models.publisher.BillingPlan;
import models.publisher.BillingSchedule;

import java.util.List;

/**
 * Created by surge on 2016-01-04.
 */
public class FormBillingPLan {
  public BillingPlan.PlanType planType;
  public Long                 planId;
  public Long                 parentId;
  public String               planName;
  public String               planDescription;
  public BillingSchedule.Type planSchedule;
  public BillingPlan.Currency planCurrency;
  public Float                planCost;
  public Boolean              planEnabled;
  public Boolean              planVisible;
  public Boolean              planNewUserState;
  public Integer              planConsortium;
  public String               planExpire;
  public Integer              planTrialDays;
  public List<Integer>        planFeatures;
  public BillingPlan.BillType planBillingType;
}
