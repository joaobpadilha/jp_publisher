package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-06-17
 * Time: 12:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class WebContactForm {
    private String inName;
    private String inEmail;
    private String inPhone;
    private String inNote;
    private String inCmpy;
    private String inWeb;


  public String getInName() {
        return inName;
    }

    public void setInName(String inName) {
        this.inName = inName;
    }

    public String getInEmail() {
        return inEmail;
    }

    public void setInEmail(String inEmail) {
        this.inEmail = inEmail;
    }

    public String getInPhone() {
        return inPhone;
    }

    public void setInPhone(String inPhone) {
        this.inPhone = inPhone;
    }

    public String getInNote() {
        return inNote;
    }

    public void setInNote(String inNote) {
        this.inNote = inNote;
    }

  public String getInCmpy() {
    return inCmpy;
  }

  public void setInCmpy(String inCmpy) {
    this.inCmpy = inCmpy;
  }

  public String getInWeb() {
    return inWeb;
  }

  public void setInWeb(String inWeb) {
    this.inWeb = inWeb;
  }
}
