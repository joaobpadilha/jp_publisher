package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-08
 * Time: 2:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class FileUploadForm {
  private String inDestId;
  private String inDestGuideId;
  private String inDestPhotoCaption;
  private String inDestPhotoName;
  private String inDestPhotoId;
  private String inParentId;
  private String inTripId;
  private String inFileName;
  private String inFileId;
  private String inFileCaption;

  private String inDestFileCaption;
  private String inDestFileName;
  private String inDestFileId;
  private String inType;

  public String getInDestId() {
    return inDestId;
  }

  public void setInDestId(String inDestId) {
    this.inDestId = inDestId;
  }

  public String getInDestGuideId() {
    return inDestGuideId;
  }

  public void setInDestGuideId(String inDestGuideId) {
    this.inDestGuideId = inDestGuideId;
  }

  public String getInDestPhotoCaption() {
    return inDestPhotoCaption;
  }

  public void setInDestPhotoCaption(String inDestPhotoCaption) {
    this.inDestPhotoCaption = inDestPhotoCaption;
  }

  public String getInDestPhotoName() {
    return inDestPhotoName;
  }

  public void setInDestPhotoName(String inDestPhotoName) {
    this.inDestPhotoName = inDestPhotoName;
  }

  public String getInDestPhotoId() {
    return inDestPhotoId;
  }

  public void setInDestPhotoId(String inDestPhotoId) {
    this.inDestPhotoId = inDestPhotoId;
  }

  public String getInParentId() {
    return inParentId;
  }

  public void setInParentId(String inParentId) {
    this.inParentId = inParentId;
  }

  public String getInTripId() {
    return inTripId;
  }

  public void setInTripId(String inTripId) {
    this.inTripId = inTripId;
  }

  public String getInFileName() {
    return inFileName;
  }

  public void setInFileName(String inFileName) {
    this.inFileName = inFileName;
  }

  public String getInFileId() {
    return inFileId;
  }

  public void setInFileId(String inFileId) {
    this.inFileId = inFileId;
  }

  public String getInFileCaption() {
    return inFileCaption;
  }

  public void setInFileCaption(String inFileCaption) {
    this.inFileCaption = inFileCaption;
  }

  public String getInDestFileCaption() {
    return inDestFileCaption;
  }

  public void setInDestFileCaption(String inDestFileCaption) {
    this.inDestFileCaption = inDestFileCaption;
  }

  public String getInDestFileName() {
    return inDestFileName;
  }

  public void setInDestFileName(String inDestFileName) {
    this.inDestFileName = inDestFileName;
  }

  public String getInDestFileId() {
    return inDestFileId;
  }

  public void setInDestFileId(String inDestFileId) {
    this.inDestFileId = inDestFileId;
  }

  public String getInType() {
    return inType;
  }

  public void setInType(String inType) {
    this.inType = inType;
  }
}
