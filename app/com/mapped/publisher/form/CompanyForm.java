package com.mapped.publisher.form;

import models.publisher.BillingPmntMethod;
import models.publisher.BillingSchedule;
import models.publisher.Company;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-03
 * Time: 2:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class CompanyForm {
  private String inCmpyId;
  private String inName;
  private String inTag;
  private String inType;
  private int inStatus;
  private String inComment;
  private String inLogoUrl;
  private String inLogoName;
  private String inContact;
  private int inNumAccounts;
  private int inNumTrips;

  private boolean inFlightTracking;
  private Company.BillingType inBillingType;

  private String inCmpyAddrId;
  private String inCmpyAddress1;
  private String inCmpyAddress2;
  private String inCmpyCity;
  private String inCmpyZip;
  private String inCmpyState;
  private String inCmpyCountry;
  private String inCmpyPhone;
  private String inCmpyEmail;
  private String inCmpyWeb;
  private String inCmpyFax;
  private String inCmpyFacebook;
  private String inCmpyTwitter;
  private String inCmpyDesc;
  private String inCmpyLocLat;
  private String inCmpyLocLong;
  private String inCmpyExpiryDate;
  private String inTargetAgreement;
  private String inAuthorizedUser;

  private String inAllowApiAccess;

  private boolean inItineraryCheck;
  public boolean inEnableCollaboration;

  /*
   * BILLING
   */
  public Long billingPlan;
  public BillingPmntMethod.Type billingPmntMethod;
  public String billingStartDate;
  public String serviceCutoffDate;
  public Boolean billingRecurrent;
  public BillingSchedule.Type billingSchedule;

  private Long apiCmpyTokenPK;


  public String getInCmpyId() {
    return inCmpyId;
  }

  public void setInCmpyId(String inCmpyId) {
    this.inCmpyId = inCmpyId;
  }

  public String getInName() {
    return inName;
  }

  public void setInName(String inName) {
    this.inName = inName;
  }

  public String getInTag() {
    return inTag;
  }

  public void setInTag(String inTag) {
    this.inTag = inTag;
  }

  public String getInType() {
    return inType;
  }

  public void setInType(String inType) {
    this.inType = inType;
  }

  public int getInStatus() {
    return inStatus;
  }

  public void setInStatus(int inStatus) {
    this.inStatus = inStatus;
  }

  public String getInComment() {
    return inComment;
  }

  public void setInComment(String inComment) {
    this.inComment = inComment;
  }

  public String getInLogoUrl() {
    return inLogoUrl;
  }

  public void setInLogoUrl(String inLogoUrl) {
    this.inLogoUrl = inLogoUrl;
  }

  public String getInLogoName() {
    return inLogoName;
  }

  public void setInLogoName(String inLogoName) {
    this.inLogoName = inLogoName;
  }

  public String getInCmpyAddress1() {
    return inCmpyAddress1;
  }

  public void setInCmpyAddress1(String inCmpyAddress1) {
    this.inCmpyAddress1 = inCmpyAddress1;
  }

  public String getInCmpyAddress2() {
    return inCmpyAddress2;
  }

  public void setInCmpyAddress2(String inCmpyAddress2) {
    this.inCmpyAddress2 = inCmpyAddress2;
  }

  public String getInCmpyCity() {
    return inCmpyCity;
  }

  public void setInCmpyCity(String inCmpyCity) {
    this.inCmpyCity = inCmpyCity;
  }

  public String getInCmpyZip() {
    return inCmpyZip;
  }

  public void setInCmpyZip(String inCmpyZip) {
    this.inCmpyZip = inCmpyZip;
  }

  public String getInCmpyState() {
    return inCmpyState;
  }

  public void setInCmpyState(String inCmpyState) {
    this.inCmpyState = inCmpyState;
  }

  public String getInCmpyCountry() {
    return inCmpyCountry;
  }

  public void setInCmpyCountry(String inCmpyCountry) {
    this.inCmpyCountry = inCmpyCountry;
  }

  public String getInCmpyPhone() {
    return inCmpyPhone;
  }

  public void setInCmpyPhone(String inCmpyPhone) {
    this.inCmpyPhone = inCmpyPhone;
  }

  public String getInCmpyEmail() {
    return inCmpyEmail;
  }

  public void setInCmpyEmail(String inCmpyEmail) {
    this.inCmpyEmail = inCmpyEmail;
  }

  public String getInCmpyWeb() {
    return inCmpyWeb;
  }

  public void setInCmpyWeb(String inCmpyWeb) {
    this.inCmpyWeb = inCmpyWeb;
  }

  public String getInCmpyFax() {
    return inCmpyFax;
  }

  public void setInCmpyFax(String inCmpyFax) {
    this.inCmpyFax = inCmpyFax;
  }

  public String getInCmpyFacebook() {
    return inCmpyFacebook;
  }

  public void setInCmpyFacebook(String inCmpyFacebook) {
    this.inCmpyFacebook = inCmpyFacebook;
  }

  public String getInCmpyTwitter() {
    return inCmpyTwitter;
  }

  public void setInCmpyTwitter(String inCmpyTwitter) {
    this.inCmpyTwitter = inCmpyTwitter;
  }

  public String getInCmpyDesc() {
    return inCmpyDesc;
  }

  public void setInCmpyDesc(String inCmpyDesc) {
    this.inCmpyDesc = inCmpyDesc;
  }

  public String getInCmpyLocLat() {
    return inCmpyLocLat;
  }

  public void setInCmpyLocLat(String inCmpyLocLat) {
    this.inCmpyLocLat = inCmpyLocLat;
  }

  public String getInCmpyLocLong() {
    return inCmpyLocLong;
  }

  public void setInCmpyLocLong(String inCmpyLocLong) {
    this.inCmpyLocLong = inCmpyLocLong;
  }

  public String getInContact() {
    return inContact;
  }

  public void setInContact(String inContact) {
    this.inContact = inContact;
  }

  public String getInCmpyAddrId() {
    return inCmpyAddrId;
  }

  public void setInCmpyAddrId(String inCmpyAddrId) {
    this.inCmpyAddrId = inCmpyAddrId;
  }

  public int getInNumAccounts() {
    return inNumAccounts;
  }

  public void setInNumAccounts(int inNumAccounts) {
    this.inNumAccounts = inNumAccounts;
  }

  public int getInNumTrips() {
    return inNumTrips;
  }

  public void setInNumTrips(int inNumTrips) {
    this.inNumTrips = inNumTrips;
  }

  public String getInCmpyExpiryDate() {
    return inCmpyExpiryDate;
  }

  public void setInCmpyExpiryDate(String inCmpyExpiryDate) {
    this.inCmpyExpiryDate = inCmpyExpiryDate;
  }

  public String getInTargetAgreement() {
    return inTargetAgreement;
  }

  public void setInTargetAgreement(String inTargetAgreement) {
    this.inTargetAgreement = inTargetAgreement;
  }

  public String getInAuthorizedUser() {
    return inAuthorizedUser;
  }

  public void setInAuthorizedUser(String inAuthorizedUser) {
    this.inAuthorizedUser = inAuthorizedUser;
  }

  public String getInAllowApiAccess() {
    return inAllowApiAccess;
  }

  public void setInAllowApiAccess(String inAllowApiAccess) {
    this.inAllowApiAccess = inAllowApiAccess;
  }

  public boolean getInItineraryCheck() {
    return inItineraryCheck;
  }

  public void setInItineraryCheck(boolean inItineraryCheck) {
    this.inItineraryCheck = inItineraryCheck;
  }

  public boolean isInFlightTracking() {
    return inFlightTracking;
  }

  public void setInFlightTracking(boolean inFlightTracking) {
    this.inFlightTracking = inFlightTracking;
  }

  public Company.BillingType getInBillingType() {
    return inBillingType;
  }

  public void setInBillingType(Company.BillingType inBillingType) {
    this.inBillingType = inBillingType;
  }

  public boolean isInItineraryCheck() {
    return inItineraryCheck;
  }

  public boolean isInEnableCollaboration() {
    return inEnableCollaboration;
  }

  public void setInEnableCollaboration(boolean inEnableCollaboration) {
    this.inEnableCollaboration = inEnableCollaboration;
  }

  public BillingSchedule.Type getBillingSchedule() {
    return billingSchedule;
  }

  public void setBillingSchedule(BillingSchedule.Type billingSchedule) {
    this.billingSchedule = billingSchedule;
  }

  public Long getApiCmpyTokenPK() {
    return apiCmpyTokenPK;
  }

  public void setApiCmpyTokenPK(Long apiCmpyTokenPK) {
    this.apiCmpyTokenPK = apiCmpyTokenPK;
  }
}

