package com.mapped.publisher.persistence.redis;

import com.mapped.publisher.utils.Log;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

/**
 * Created by surge on 2016-02-04.
 */
public class RedisPubSubThread
    extends Thread {

  JedisPubSub pubSub;
  Jedis       jedis;
  String[]    channels;

  public RedisPubSubThread(Jedis connection, JedisPubSub pubSub, String... channels){
    this.pubSub = pubSub;
    this.jedis = connection;
    this.channels = channels;
  }

  @Override
  public void run() {
    Log.debug("RedisPubSubThread: Subscribing channels");
    jedis.subscribe(pubSub, channels); //This is a blocking forever call
    Log.debug("RedisPubSubThread: Subscription is over");
  }

  public void unsubscribe() {
    pubSub.unsubscribe();
    jedis.close();
  }
}
