package com.mapped.publisher.persistence.redis;

/**
 * Created by surge on 2016-02-02.
 */
public class RedisAkkaProtocol {
  public String content;
  public String channel;

  public RedisAkkaProtocol(String channel, String content) {
    this.channel = channel;
    this.content = content;
  }
}
