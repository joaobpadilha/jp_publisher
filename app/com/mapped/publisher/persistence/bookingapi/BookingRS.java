package com.mapped.publisher.persistence.bookingapi;

import com.mapped.publisher.parse.schemaorg.Thing;

import java.io.Serializable;

/**
 * Created by twong on 15-08-18.
 */
public class BookingRS implements Cloneable, Serializable {
  public long pk;
  public String srcPk = null;
  public int srcId;
  public int bookingType;
  public String srcRecLocator = null;
  public boolean addedToTrip = false;
  public String mainTraveler = null;
  public long startTS;
  public long endTS;
  public String srcCmpyId = null;
  public String srcUserId = null;
  public String uCmpyId;
  public String uUserId = null;
  public String data = null;
  public Thing reservationJson;
  public String state = "ACTIVE";
  public String searchWords = null;
  public String hash = null;
  public long createdTS;
  public long modifiedTS;
  public String createdBy = null;
  public String modifiedBy = null;
  public int version = 0;

  public String getHash() {
    return hash;
  }

  public void setHash(String hash) {
    this.hash = hash;
  }

  public long getPk() {
    return pk;
  }

  public void setPk(long pk) {
    this.pk = pk;
  }

  public String getSrcPk() {
    return srcPk;
  }

  public void setSrcPk(String srcPk) {
    this.srcPk = srcPk;
  }

  public int getSrcId() {
    return srcId;
  }

  public void setSrcId(int srcId) {
    this.srcId = srcId;
  }

  public int getBookingType() {
    return bookingType;
  }

  public void setBookingType(int bookingType) {
    this.bookingType = bookingType;
  }

  public String getSrcRecLocator() {
    return srcRecLocator;
  }

  public void setSrcRecLocator(String srcRecLocator) {
    this.srcRecLocator = srcRecLocator;
  }

  public boolean isAddedToTrip() {
    return addedToTrip;
  }

  public void setAddedToTrip(boolean addedToTrip) {
    this.addedToTrip = addedToTrip;
  }

  public long getStartTS() {
    return startTS;
  }

  public void setStartTS(long startTS) {
    this.startTS = startTS;
  }

  public long getEndTS() {
    return endTS;
  }

  public void setEndTS(long endTS) {
    this.endTS = endTS;
  }

  public String getSrcCmpyId() {
    return srcCmpyId;
  }

  public void setSrcCmpyId(String srcCmpyId) {
    this.srcCmpyId = srcCmpyId;
  }

  public String getSrcUserId() {
    return srcUserId;
  }

  public void setSrcUserId(String srcUserId) {
    this.srcUserId = srcUserId;
  }

  public String getuCmpyId() {
    return uCmpyId;
  }

  public void setuCmpyId(String uCmpyId) {
    this.uCmpyId = uCmpyId;
  }

  public String getuUserId() {
    return uUserId;
  }

  public void setuUserId(String uUserId) {
    this.uUserId = uUserId;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  public Thing getReservationJson() {
    return reservationJson;
  }

  public void setReservationJson(Thing reservationJson) {
    this.reservationJson = reservationJson;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getSearchWords() {
    return searchWords;
  }

  public void setSearchWords(String searchWords) {
    this.searchWords = searchWords;
  }

  public long getCreatedTS() {
    return createdTS;
  }

  public void setCreatedTS(long createdTS) {
    this.createdTS = createdTS;
  }

  public long getModifiedTS() {
    return modifiedTS;
  }

  public void setModifiedTS(long modifiedTS) {
    this.modifiedTS = modifiedTS;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public String getMainTraveler() {
    return mainTraveler;
  }

  public void setMainTraveler(String mainTraveler) {
    this.mainTraveler = mainTraveler;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
