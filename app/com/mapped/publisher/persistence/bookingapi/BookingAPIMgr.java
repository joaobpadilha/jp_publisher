package com.mapped.publisher.persistence.bookingapi;

import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import models.publisher.BkApiSrc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by twong on 15-08-18.
 */
public class BookingAPIMgr {
  final static String SQL_COLS;
  final static String SQL_INSERT_REC;
  final static String SQL_UPDATE_REC;
  final static String SQL_UPDATE_USER_MAPPING;


  final static String SQL_UPDATE_ADD_TRIP;

  final static String SQL_DELETE_REC;

  final static String SQL_SELECT_PK;
  final static String SQL_SELECT_SRC_PK;
  final static String SQL_SELECT_PKS;
  final static String SQL_SELECT_DATE_RANGE_USER_ID_TERM;
  final static String SQL_SELECT_ALL;
  final static String SQL_SELECT_SUB_TERM;
  final static String SQL_SELECT_TERM;
  final static String SQL_SELECT_SUB_TERM_NO_USER;
  final static String SQL_SELECT_TERM_NO_USER;
  final static String SQL_SELECT_BY_PNR;
  final static String SQL_TRIPS_IN_USE;
  final static String SQL_TRIPS_TAG_IN_USE;

  final static String SQL_TRIPS_NOTES_IN_USE;

  final static String SQL_SELECT_BY_BOOKING_TYPE;


  static {
    SQL_COLS = " pk, src_pk, src_id, booking_type, src_rec_locator, added_to_trip, start_ts, end_ts," +
               " src_cmpy_id, src_user_id, u_cmpy_id, u_user_id, data,main_traveler, state, search_words, hash, " +
               " created_ts, modified_ts, createdby, modifiedby ";

    SQL_INSERT_REC = "INSERT INTO bk_api_rec ( " + SQL_COLS + ") " +
                     "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,cast(? as jsonb),? , cast(? as bk_api_state),?,?,?,?,?,?);";

    SQL_UPDATE_REC = " UPDATE bk_api_rec" +
                     " SET u_user_id = ?, start_ts = ?, end_ts = ?, data = cast(? as jsonb), main_traveler = ?, search_words = ?, hash = ?, modified_ts = ?, modifiedby = ?" +
                     " WHERE pk = ?;";

    SQL_UPDATE_USER_MAPPING = " UPDATE bk_api_rec" +
                     " SET u_user_id = ?, modified_ts = ?, modifiedby = ?" +
                     " WHERE src_id = ? and src_cmpy_id = ? and src_user_id = ?;";

    SQL_UPDATE_ADD_TRIP = " UPDATE bk_api_rec" +
                     " SET added_to_trip = ?, modified_ts = ?, modifiedby = ?" +
                     " WHERE pk = ANY(?);";

    SQL_DELETE_REC = " UPDATE bk_api_rec" +
                     " SET state = 'DELETED', modified_ts = ?, modifiedby = ?" +
                     " WHERE pk = ?;";


    SQL_SELECT_DATE_RANGE_USER_ID_TERM = "SELECT " + SQL_COLS + " FROM bk_api_rec WHERE " +
                            "start_ts > ? and end_ts < ? and u_user_id like ? and state = 'ACTIVE' and search_words like ? order by start_ts;";

    SQL_SELECT_SRC_PK = "SELECT " + SQL_COLS + " FROM bk_api_rec WHERE " +
                            "src_pk like ? and src_id = ?; ";

    SQL_SELECT_PK = "SELECT " + SQL_COLS + " FROM bk_api_rec WHERE " +
                        "pk = ?;";

    SQL_SELECT_PKS = "SELECT " + SQL_COLS + " FROM bk_api_rec WHERE " +
                    "pk = ANY(?);";

    SQL_SELECT_ALL = "SELECT " + SQL_COLS + " FROM bk_api_rec ;";

    SQL_SELECT_SUB_TERM = "SELECT " + SQL_COLS + " FROM bk_api_rec where state = 'ACTIVE' and src_rec_locator  = ANY(?) and u_cmpy_id = ANY(?) and u_user_id like ? " +
                      " order by start_ts;";

    SQL_SELECT_TERM = "select distinct(src_rec_locator) from bk_api_rec where state = 'ACTIVE' and added_to_trip = ANY(?) and  src_id = ANY(?)  and  (search_words ILIKE ? OR (to_tsvector('english', search_words) @@ plainto_tsquery('english', ?)))" +
                      "  and (start_ts > ? or end_ts > ?) and u_cmpy_id = ANY(?) and u_user_id like ? and booking_type != 17 order by src_rec_locator offset ?;";


    SQL_SELECT_SUB_TERM_NO_USER = "SELECT " + SQL_COLS + " FROM bk_api_rec where state = 'ACTIVE' and src_rec_locator  = ANY(?) and u_cmpy_id = ANY(?) and (u_user_id like ? or u_user_id is null) " +
                          " order by start_ts;";

    SQL_SELECT_TERM_NO_USER = "select distinct(src_rec_locator) from bk_api_rec where state = 'ACTIVE' and added_to_trip = ANY(?) and  src_id = ANY(?)  and  (search_words ILIKE ? OR (to_tsvector('english', search_words) @@ plainto_tsquery('english', ?)))" +
                      "  and (start_ts > ? or end_ts > ?) and u_cmpy_id = ANY(?) and (u_user_id like ? or u_user_id is null) and booking_type != 17 order by src_rec_locator offset ?;";


    SQL_SELECT_BY_PNR = "SELECT " + SQL_COLS + "FROM bk_api_rec where state = 'ACTIVE' and src_rec_locator = ? and src_id = ? and u_cmpy_id = ? and src_cmpy_id = ?";

    SQL_TRIPS_IN_USE = "SELECT distinct(t.tripid) FROM trip_detail td, trip t " +
                       "WHERE t.tripid = td.tripid and td.bk_api_src_id " +
                       "in (select pk from bk_api_rec where state = 'ACTIVE' and src_rec_locator = ? and " +
                       "src_id = ? and  u_cmpy_id = ?) and " +
                       "t.status != -1 and td.status = 0;";

    SQL_TRIPS_TAG_IN_USE = "SELECT distinct(t.tripid) FROM trip t " +
            "WHERE t.tag like ? and " +
            "t.cmpyid = ? and " +
            "t.status != -1;";

    SQL_TRIPS_NOTES_IN_USE = "SELECT distinct(t.tripid) FROM trip_note td, trip t " +
                             "WHERE t.tripid = td.trip_id and td.import_src_id " +
                             "in (select cast(pk as varchar) from bk_api_rec where state = 'ACTIVE' and src_rec_locator = ? and " +
                             "src_id = ? and  u_cmpy_id = ?) and " +
                             "t.status != -1 and td.status = 0;";
    
    SQL_SELECT_BY_BOOKING_TYPE = "SELECT " + SQL_COLS + " FROM bk_api_rec WHERE " +
        "booking_type = ?;";

  }

  public static List<BookingRS> findAll() {
    List<BookingRS> points = new ArrayList<>();
    Connection conn = getConnection();
    if (conn == null) {
      return points;
    }
    try {
      int paramCounter = 0;

      PreparedStatement query = conn.prepareStatement(SQL_SELECT_ALL);
      ResultSet rs = query.executeQuery();
      while (rs.next()) {
        BookingRS rec = getPoiFromResultSet(rs);
        points.add(rec);
      }
    }
    catch (SQLException e) {
      Log.log(LogLevel.ERROR, "SQL Exception while reading booking api rec:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return points;
  }

  public static BookingRS findByPk(long pk) {
    List<BookingRS> points = new ArrayList<>();
    Connection conn = getConnection();
    if (conn == null) {
      return null;
    }
    try {
      int paramCounter = 0;

      PreparedStatement query = conn.prepareStatement(SQL_SELECT_PK);
      query.setLong(++paramCounter, pk);
      ResultSet rs = query.executeQuery();
      while (rs.next()) {
        BookingRS rec = getPoiFromResultSet(rs);
        points.add(rec);
      }
      if (points.size() > 0) {
        return points.get(0);
      }
    }
    catch (SQLException e) {
      Log.log(LogLevel.ERROR, "SQL Exception while reading booking api rec:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return null;
  }

  public static List<BookingRS> findByTerm(String term, List<Integer> srcids, int maxRows, int startPos, List<Boolean> flags, String userId, List<String> cmpyIds) {
    List<BookingRS> points = new ArrayList<>();
    Connection conn = getConnection();
    if (conn == null) {
      return null;
    }

    if (term == null || term.trim().length() == 0) {
      term = "%";
    } else if (!term.contains("%")) {
      term = "%" + term + "%";
    }

    if (term.replaceAll("%","").trim().length() > 0) {
      //if there is a search term, we then search for all users within that company
      userId = "%";
    }

    String recLocator = term.replaceAll("%", "").toUpperCase();

    try {
      List<String> srcLocators = new ArrayList<>();

      int paramCounter = 0;

      //first we limit the search to the subquery so we can control position and count
      PreparedStatement query = null;
      if (userId.equals("%"))
        query = conn.prepareStatement(SQL_SELECT_TERM_NO_USER);
      else
        query =conn.prepareStatement(SQL_SELECT_TERM);

      query.setArray(++paramCounter, conn.createArrayOf("boolean", flags.toArray()));
      query.setArray(++paramCounter,  conn.createArrayOf("int", srcids.toArray()));
      query.setString(++paramCounter, term);
      query.setString(++paramCounter, term);
      query.setLong(++paramCounter, System.currentTimeMillis());
      query.setLong(++paramCounter, System.currentTimeMillis());
      query.setArray(++paramCounter, conn.createArrayOf("text", cmpyIds.toArray()));
      query.setString(++paramCounter, userId);
      query.setInt(++paramCounter, startPos);
      query.setMaxRows(maxRows);

      ResultSet rs = query.executeQuery();
      while (rs.next()) {
        srcLocators.add(rs.getString(1));
      }
      query.close();
      rs.close();

      paramCounter = 0;

      if (userId.equals("%"))
        query =conn.prepareStatement(SQL_SELECT_SUB_TERM_NO_USER);
      else
        query =conn.prepareStatement(SQL_SELECT_SUB_TERM);

      query.setArray(++paramCounter, conn.createArrayOf("varchar", srcLocators.toArray()));
      query.setArray(++paramCounter, conn.createArrayOf("text", cmpyIds.toArray()));
      query.setString(++paramCounter, userId);
      rs = query.executeQuery();
      if (srcLocators.size() > 0) {
        while (rs.next()) {
          BookingRS rec = getPoiFromResultSet(rs);
          //add filter for sabre so that it is an exact match to the PNR
          if (rec.getSrcId() != BkApiSrc.getId("SABRE") || (rec.getSrcId() == BkApiSrc.getId("SABRE") && rec.getSrcRecLocator() != null && rec.getSrcRecLocator().toUpperCase().equals(recLocator) ) || (rec.getuUserId() != null && userId != null && rec.getuUserId().equals(userId))) {
            points.add(rec);
          }
        }
      }
      query.close();
      rs.close();

    }
    catch (SQLException e) {
      Log.log(LogLevel.ERROR, "SQL Exception while reading booking api rec:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }


    return points;
  }

  public static List<BookingRS> findByPks(List<Long> pks) {
    List<BookingRS> points = new ArrayList<>();
    Connection conn = getConnection();
    if (conn == null) {
      return points;
    }
    try {
      int paramCounter = 0;

      PreparedStatement query = conn.prepareStatement(SQL_SELECT_PKS);
      query.setArray(++paramCounter,  conn.createArrayOf("bigint", pks.toArray()));
      ResultSet rs = query.executeQuery();
      while (rs.next()) {
        BookingRS rec = getPoiFromResultSet(rs);
        points.add(rec);
      }
    }
    catch (SQLException e) {
      Log.log(LogLevel.ERROR, "SQL Exception while reading booking api rec:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return points;
  }

  public static List<BookingRS> findByPnrSrcId(String pnr, int srcId, String uCmpyId, String sCmpyId) {
    List<BookingRS> points = new ArrayList<>();
    Connection conn = getConnection();
    if (conn == null) {
      return points;
    }
    try {
      int paramCounter = 0;

      PreparedStatement query = conn.prepareStatement(SQL_SELECT_BY_PNR);
      query.setString(++paramCounter, pnr);
      query.setInt(++paramCounter, srcId);
      query.setString(++paramCounter, uCmpyId);
      query.setString(++paramCounter, sCmpyId);

      ResultSet rs = query.executeQuery();
      while (rs.next()) {
        BookingRS rec = getPoiFromResultSet(rs);
        points.add(rec);
      }
    }
    catch (SQLException e) {
      Log.log(LogLevel.ERROR, "SQL Exception while reading booking api rec:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return points;
  }

  public static List<BookingRS> findBySrcPk(String srcPk, int srcId) {
    List<BookingRS> points = new ArrayList<>();
    Connection conn = getConnection();
    if (conn == null) {
      return points;
    }
    try {
      int paramCounter = 0;

      PreparedStatement query = conn.prepareStatement(SQL_SELECT_SRC_PK);
      query.setString(++paramCounter, srcPk);
      query.setInt(++paramCounter, srcId);

      ResultSet rs = query.executeQuery();
      while (rs.next()) {
        BookingRS rec = getPoiFromResultSet(rs);
        points.add(rec);
      }
    }
    catch (SQLException e) {
      Log.log(LogLevel.ERROR, "SQL Exception while reading booking api rec:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return points;
  }

  public static List<BookingRS> findByDateAndUserAndTerm(long startTs, long endTs, String userid, String term) {
    List<BookingRS> points = new ArrayList<>();
    Connection conn = getConnection();
    if (conn == null) {
      return points;
    }
    try {
      int paramCounter = 0;

      PreparedStatement query = conn.prepareStatement(SQL_SELECT_DATE_RANGE_USER_ID_TERM);
      query.setLong(++paramCounter, startTs);
      query.setLong(++paramCounter, endTs);
      query.setString(++paramCounter, userid);
      query.setString(++paramCounter, term);


      ResultSet rs = query.executeQuery();
      while (rs.next()) {
        BookingRS rec = getPoiFromResultSet(rs);
        points.add(rec);
      }
    }
    catch (SQLException e) {
      Log.log(LogLevel.ERROR, "SQL Exception while reading booking api rec:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return points;
  }

  public static List<String> findTrips(String recLocator, int srcId, String uCmpyId) {
    List<String> tripIds = new ArrayList<>();
    Connection conn = getConnection();
    if (conn == null) {
      return tripIds;
    }
    try {
      int paramCounter = 0;

      PreparedStatement query = conn.prepareStatement(SQL_TRIPS_IN_USE);
      query.setString(++paramCounter, recLocator);
      query.setInt(++paramCounter, srcId);
      query.setString(++paramCounter, uCmpyId);

      ResultSet rs = query.executeQuery();
      while (rs.next()) {
        tripIds.add(rs.getString(1));
      }
      rs.close();
      query.close();

      query = conn.prepareStatement(SQL_TRIPS_NOTES_IN_USE);
      paramCounter = 0;
      query.setString(++paramCounter, recLocator);
      query.setInt(++paramCounter, srcId);
      query.setString(++paramCounter, uCmpyId);

      rs = query.executeQuery();
      while (rs.next()) {
        if (!tripIds.contains(rs.getString(1))) {
          tripIds.add(rs.getString(1));
        }
      }
      rs.close();
      query.close();

      query = conn.prepareStatement(SQL_TRIPS_TAG_IN_USE);
      String tag = recLocator;
      if (!recLocator.contains("%")) {
        tag = '%' + tag + '%';
      }
      paramCounter = 0;
      query.setString(++paramCounter, tag);
      query.setString(++paramCounter, uCmpyId);

      rs = query.executeQuery();
      while (rs.next()) {
        if (!tripIds.contains(rs.getString(1))) {
          tripIds.add(rs.getString(1));
        }
      }
      rs.close();
      query.close();



    }
    catch (SQLException e) {
      Log.log(LogLevel.ERROR, "SQL Exception while reading booking api rec:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return tripIds;
  }

  public static boolean insert(BookingRS rec) {
    boolean result = false;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_INSERT_REC);
      int paramCounter = 0;
      query.setLong(++paramCounter, rec.getPk());
      query.setString(++paramCounter, rec.getSrcPk());
      query.setInt(++paramCounter, rec.getSrcId());
      query.setLong(++paramCounter, rec.getBookingType());
      query.setString(++paramCounter, rec.getSrcRecLocator());
      query.setBoolean(++paramCounter, rec.isAddedToTrip());
      query.setLong(++paramCounter, rec.getStartTS());
      query.setLong(++paramCounter, rec.getEndTS());
      query.setString(++paramCounter, rec.getSrcCmpyId());
      query.setString(++paramCounter, rec.getSrcUserId());
      query.setString(++paramCounter, rec.getuCmpyId());
      query.setString(++paramCounter, rec.getuUserId());
      query.setString(++paramCounter, rec.getData());
      query.setString(++paramCounter, rec.getMainTraveler());
      query.setString(++paramCounter, rec.getState());
      query.setString(++paramCounter, rec.getSearchWords());
      query.setString(++paramCounter, rec.getHash());
      query.setLong(++paramCounter, rec.getCreatedTS());
      query.setLong(++paramCounter, rec.getModifiedTS());
      query.setString(++paramCounter, rec.getCreatedBy());
      query.setString(++paramCounter, rec.getModifiedBy());

      if (query.executeUpdate() == 0) {
        Log.log(LogLevel.INFO, "Unexpected result from insert query :" + query.toString());
      } else {
        result = true;
      }
    } catch (Exception e) {
      Log.log(LogLevel.ERROR, "SQL Exception while insert booking api rec:" + e.getMessage());
      e.printStackTrace();
    } finally {
      releaseConnection(conn);
    }
    return result;
  }

  public static boolean delete(BookingRS rec) {
    boolean result = false;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_DELETE_REC);
      int paramCounter = 0;
      query.setLong(++paramCounter, rec.getModifiedTS());
      query.setString(++paramCounter, rec.getModifiedBy());
      query.setLong(++paramCounter, rec.getPk());

      if (query.executeUpdate() == 0) {
        Log.log(LogLevel.INFO, "Unexpected result from update query :" + query.toString());
      } else {
        result = true;
      }
    } catch (Exception e) {
      Log.log(LogLevel.ERROR, "SQL Exception while update booking api rec:" + e.getMessage());
      e.printStackTrace();
    } finally {
      releaseConnection(conn);
    }
    return result;
  }


  public static boolean update(BookingRS rec) {
    boolean result = false;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_UPDATE_REC);
      int paramCounter = 0;
      query.setString(++paramCounter, rec.getuUserId());
      query.setLong(++paramCounter, rec.getStartTS());
      query.setLong(++paramCounter, rec.getEndTS());
      query.setString(++paramCounter, rec.getData());
      query.setString(++paramCounter, rec.getMainTraveler());
      query.setString(++paramCounter, rec.getSearchWords());
      query.setString(++paramCounter, rec.getHash());
      query.setLong(++paramCounter, rec.getModifiedTS());
      query.setString(++paramCounter, "thierry"+rec.getModifiedBy());
      query.setLong(++paramCounter, rec.getPk());

      if (query.executeUpdate() == 0) {
        Log.log(LogLevel.INFO, "Unexpected result from update query :" + query.toString());
      } else {
        result = true;
      }
    } catch (Exception e) {
      Log.log(LogLevel.ERROR, "SQL Exception while update booking api rec:" + e.getMessage());
      e.printStackTrace();
    } finally {
      releaseConnection(conn);
    }
    return result;
  }

  public static boolean updateUserMapping(String um_userid, String src_cmpyid, String src_userid, int src_type, String modifiedBy) {
    boolean result = false;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_UPDATE_USER_MAPPING);
      int paramCounter = 0;
      if (um_userid == null || um_userid.trim().length() == 0) {
        query.setNull(++paramCounter, Types.VARCHAR);
      } else {
        query.setString(++paramCounter, um_userid);
      }
      query.setLong(++paramCounter, System.currentTimeMillis());
      query.setString(++paramCounter, modifiedBy);
      query.setInt(++paramCounter, src_type);
      query.setString(++paramCounter, src_cmpyid);
      query.setString(++paramCounter, src_userid);

      if (query.executeUpdate() == 0) {
        Log.log(LogLevel.INFO, "Unexpected result from update query :" + query.toString());
      } else {
        result = true;
      }
    } catch (Exception e) {
      Log.log(LogLevel.ERROR, "SQL Exception while update booking api rec:" + e.getMessage());
      e.printStackTrace();
    } finally {
      releaseConnection(conn);
    }
    return result;
  }



  public static boolean updateAddToTrip(String userid, List<Long> pks) {
    boolean result = false;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_UPDATE_ADD_TRIP);
      int paramCounter = 0;
      query.setBoolean(++paramCounter, true);
      query.setLong(++paramCounter, System.currentTimeMillis());
      query.setString(++paramCounter, userid);
      query.setArray(++paramCounter,  conn.createArrayOf("bigint", pks.toArray()));

      if (query.executeUpdate() == 0) {
        Log.log(LogLevel.INFO, "Unexpected result from update query :" + query.toString());
      } else {
        result = true;
      }
    } catch (Exception e) {
      Log.log(LogLevel.ERROR, "SQL Exception while update booking api rec:" + e.getMessage());
      e.printStackTrace();
    } finally {
      releaseConnection(conn);
    }
    return result;
  }


  private static BookingRS getPoiFromResultSet(ResultSet rs) {

    int paramCounter = 0;

    try {
      BookingRS rec = new BookingRS();
      rec.setPk(rs.getLong(++paramCounter));
      rec.setSrcPk(rs.getString(++paramCounter));
      rec.setSrcId(rs.getInt(++paramCounter));
      rec.setBookingType(rs.getInt(++paramCounter));
      rec.setSrcRecLocator(rs.getString(++paramCounter));
      rec.setAddedToTrip(rs.getBoolean(++paramCounter));
      rec.setStartTS(rs.getLong(++paramCounter));
      rec.setEndTS(rs.getLong(++paramCounter));
      rec.setSrcCmpyId(rs.getString(++paramCounter));
      rec.setSrcUserId(rs.getString(++paramCounter));
      rec.setuCmpyId(rs.getString(++paramCounter));
      rec.setuUserId(rs.getString(++paramCounter));
      rec.setData(rs.getString(++paramCounter));
      rec.setMainTraveler(rs.getString(++paramCounter));
      rec.setState(rs.getString(++paramCounter));
      rec.setSearchWords(rs.getString(++paramCounter));
      rec.setHash(rs.getString(++paramCounter));
      rec.setCreatedTS(rs.getLong(++paramCounter));
      rec.setModifiedTS(rs.getLong(++paramCounter));
      rec.setCreatedBy(rs.getString(++paramCounter));
      rec.setModifiedBy(rs.getString(++paramCounter));
      return rec;



    }
    catch (SQLException e) {
      Log.err("SQL Exception while converting booking api rec: " + e.getMessage());
      e.printStackTrace();
    }

    return null;
  }

  public static List<BookingRS> findByBookingType(int bookingType) {
    List<BookingRS> points = new ArrayList<>();
    Connection conn = getConnection();
    if (conn == null) {
      return points;
    }
    try {
      int paramCounter = 0;

      PreparedStatement query = conn.prepareStatement(SQL_SELECT_BY_BOOKING_TYPE);
      query.setInt(++paramCounter,  bookingType);
      ResultSet rs = query.executeQuery();
      while (rs.next()) {
        BookingRS rec = getPoiFromResultSet(rs);
        points.add(rec);
      }
    }
    catch (SQLException e) {
      Log.log(LogLevel.ERROR, "SQL Exception while reading booking api rec:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return points;
  }

  /**
   * Using this specific routine to get connection.
   *
   * Future optimization can be to implement a naive connection pool for vendors.
   * @return Database connection
   */
  private static Connection getConnection() {

    Connection conn = null;

    try {
      conn = DBConnectionMgr.getConnection4Publisher();
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "Failed to obtain database connection for booking api rec operation:" + e.getMessage());
      e.printStackTrace();
    }

    return conn;
  }

  /**
   * Returning connection
   */
  private static void releaseConnection(Connection conn) {
    try {
      if (conn != null) {
        conn.close();
      }
    } catch (SQLException e) {
      Log.log(LogLevel.ERROR, "Failed to release database connection:" + e.getMessage());
      e.printStackTrace();
    }
  }
}
