package com.mapped.publisher.persistence;

import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-20
 * Time: 9:13 AM
 * To change this template use File | Settings | File Templates.
 */
public class DashboardMgr {

  private final static String DECLINED_CMPY_STATUS = "select c.cmpyid as cmpyid,c.targetAgreementVer as targetVer, " +
                                                     "c.name as cmpyname, ca.createdby as createdby, " +
                                                     "ca.createdtimestamp as createdtimestamp  from company c, " +
                                                     "cmpy_agreement ca where c.status = 0 and c.cmpyid = ca.cmpyid " +
                                                     "and c.targetAgreementVer is not null and (c.targetAgreementVer " +
                                                     "<> c.acceptedagreementver or c.acceptedagreementver is null ) " +
                                                     "and c.targetAgreementVer = ca.agreementVersion and ca.status  =" +
                                                     " 1 and ca.createdtimestamp >= ? and ca.createdtimestamp <= " +
                                                     "?order by c.name; ";
  private final static String NO_ACCEPTED_CMPY_STATUS = "select c.cmpyid as cmpyid,c.targetAgreementVer as targetVer," +
                                                        " c.name as cmpyname from company c where c.status = 0 and  c" +
                                                        ".targetAgreementVer is not null and (c.targetAgreementVer <>" +
                                                        " c.acceptedagreementver or c.acceptedagreementver is null ) " +
                                                        "order by c.name; ";

  private final static String CMPY_ACTIVE_COUNT = "select count(cmpyid) as cmpy from company where status = 0;";

  private final static String CMPY_NEW_COUNT = "select count(cmpyid) as cmpy from company where createdtimestamp >= ?" +
                                               " and createdtimestamp < ?;";
  private final static String CMPY_DELETE_COUNT = "select count(cmpyid) as cmpy from company where status = -1 and " +
                                                  "lastupdatedtimestamp >= ? and lastupdatedtimestamp < ?;";

  private final static String USER_ACTIVE_COUNT = "select usertype, count(userid) as user from user_profile where " +
                                                  "status = 0 group by usertype;";
  private final static String USER_INACTIVE_COUNT = "select u.userid, u.firstname, u.lastname, u.email, c.cmpyid, " +
                                                    "c.name as cmpyname from user_profile u, user_cmpy_link uc, " +
                                                    "company c where u.status <> 0 and u.status <> -1 and uc.userid =" +
                                                    " u.userid and c.cmpyid = uc.cmpyid and u.createdtimestamp <> u" +
                                                    ".lastlogintimestamp order by u.userid;";
  private final static String USER_NOT_LOGGEDIN_COUNT = "select u.userid, u.firstname, u.lastname, u.email, c.cmpyid," +
                                                        " c.name as cmpyname from user_profile u, user_cmpy_link uc, " +
                                                        "company c where u.status <> -1 and u.createdtimestamp = u" +
                                                        ".lastlogintimestamp and uc.userid = u.userid and c.cmpyid = " +
                                                        "uc.cmpyid order by u.userid;";

  private final static String USER_NEW_COUNT = "select usertype, count(userid) as user from user_profile where status" +
                                               " = 0 and lastupdatedtimestamp >= ? and lastupdatedtimestamp < ? group" +
                                               " by usertype;";
  private final static String USER_DELETE_COUNT = "select usertype, count(userid) as user from user_profile where " +
                                                  "status = -1 and lastupdatedtimestamp >= ? and lastupdatedtimestamp" +
                                                  " < ? group by usertype;";


  private final static String USER_STATUS = "select u.userid as userid, u.firstname as firstname, " +
                                            "u.lastname as lastname, u.lastlogintimestamp, u.status as userstatus, " +
                                            "u.createdtimestamp, uc.cmpyid as cmpyid, " +
                                            "uc.linktype as linktype from user_profile u, " +
                                            "user_cmpy_link uc where u.status = 0 and u.userid = uc.cmpyid  order by " +
                                            "u.userid; ";

  private final static String CMPY_TRIPS_STATS_DATE = "select distinct (t.cmpyid) as cmpyid, c.name as cmpyname, " +
                                                      "c.type as cmpytype,  count(distinct tp.tripid) as trips from " +
                                                      "trip t, trip_publish_history tp, " +
                                                      "company c  where t.cmpyid = c.cmpyid and c.status = 0 and t" +
                                                      ".tripid = tp.tripid  and tp.createdtimestamp >= ? and tp" +
                                                      ".createdtimestamp <= ? group by t.cmpyid, c.name, " +
                                                      "c.type order by c.name;";
  private final static String CMPY_TRIPS_ACTIVITY_DATE = "select distinct (t.cmpyid) as cmpyid, c.name as cmpyname, " +
                                                         "c.type as cmpytype,  count(distinct tp.pk) as trips from " +
                                                         "trip t, trip_publish_history tp, " +
                                                         "company c  where t.cmpyid = c.cmpyid and c.status = 0 and t" +
                                                         ".tripid = tp.tripid  and tp.createdtimestamp >= ? and tp" +
                                                         ".createdtimestamp <= ? group by t.cmpyid, c.name, " +
                                                         "c.type  order by c.name;";

  private final static String USER_TRIP_STATS_DATE = "select distinct (t.createdby) as user, " +
                                                     "u.firstname as firstname, u.lastname as lastname, " +
                                                     "c.name as cmpyname, count(distinct tp.tripid) as trips from " +
                                                     "trip t, trip_publish_history tp, company c, " +
                                                     "user_profile u  where u.usertype <> 0 and u.userid= t.createdby" +
                                                     " and t.cmpyid = c.cmpyid and t.tripid = tp.tripid and tp" +
                                                     ".createdtimestamp >= ? and tp.createdtimestamp <= ? group by t" +
                                                     ".createdby, u.firstname, u.lastname, c.name  order by c.name;";
  private final static String USER_TRIP_ACTIVITY_DATE = "select distinct (t.createdby) as user, " +
                                                        "u.firstname as firstname, u.lastname as lastname,  " +
                                                        "c.name as cmpyname, count(distinct tp.pk) as trips from trip" +
                                                        " t, trip_publish_history tp, company c, " +
                                                        "user_profile u  where u.usertype <> 0 and  u.userid= t" +
                                                        ".createdby and  t.cmpyid = c.cmpyid and t.tripid = tp.tripid" +
                                                        " and tp.createdtimestamp >= ? and tp.createdtimestamp <= ? " +
                                                        "group by t.createdby, u.firstname, u.lastname,  " +
                                                        "c.name  order by c.name;";

  private final static String USER_ACTIVE_TRIP_STATS_DATE = "select distinct (t.createdby) as user, " +
                                                            "u.firstname as firstname, u.lastname as lastname,  " +
                                                            "c.name as cmpyname, count(t.tripid) as trips, " +
                                                            "t.triptype as triptype from trip t, company c, " +
                                                            "user_profile u where u.usertype <> 0 and  u.userid= t" +
                                                            ".createdby and  t.cmpyid = c.cmpyid and t.status= 0 and " +
                                                            "t.createdtimestamp >= ? and t.createdtimestamp <= ? " +
                                                            "group by t.createdby, u.firstname, u.lastname,  c.name," +
                                                            "t.triptype  order by c.name;";
  private final static String USER_DELETE_TRIP_STATS_DATE = "select distinct (t.createdby) as user, " +
                                                            "u.firstname as firstname, u.lastname as lastname,  " +
                                                            "c.name as cmpyname, count(t.tripid) as trips, " +
                                                            "t.triptype as triptype  from trip t, company c, " +
                                                            "user_profile u where u.usertype <> 0 and  u.userid= t" +
                                                            ".createdby and  t.cmpyid = c.cmpyid and  t.status= -1 " +
                                                            "and t.createdtimestamp >= ? and t.createdtimestamp <= ? " +
                                                            "group by t.createdby, u.firstname, u.lastname,  c.name," +
                                                            "t.triptype  order by c.name;";
  private final static String USER_PENDING_TRIP_STATS_DATE = "select distinct (t.createdby) as user, " +
                                                             "u.firstname as firstname, u.lastname as lastname,  " +
                                                             "c.name as cmpyname, count(t.tripid) as trips, " +
                                                             "t.triptype as triptype  from trip t, company c, " +
                                                             "user_profile u where u.usertype <> 0 and  u.userid= t" +
                                                             ".createdby and  t.cmpyid = c.cmpyid and  t.status= 1 " +
                                                             "and t.createdtimestamp >= ? and t.createdtimestamp <= ?" +
                                                             " group by t.createdby, u.firstname, u.lastname , " +
                                                             "c.name,t.triptype  order by c.name;";

  private final static String ACTIVE_TRIP_STATS = "select count(t.tripid) as trips, " +
                                                  "t.triptype as triptype from trip t where t.status= 0 group by t" +
                                                  ".triptype;";
  private final static String DELETE_TRIP_STATS = "select count(t.tripid) as trips, " +
                                                  "t.triptype as triptype  from trip t where t.status= -1 group by t" +
                                                  ".triptype;";
  private final static String PENDING_TRIP_STATS = "select count(t.tripid) as trips, " +
                                                   "t.triptype as triptype  from trip t where t.status= 1  group by t" +
                                                   ".triptype;";


  private final static String ACTIVE_TRIP_STATS_DATE = "select distinct (t.cmpyid) as cmpyid, c.name as cmpyname, " +
                                                       "count(t.tripid) as trips, t.triptype as triptype from trip t," +
                                                       " company c where t.cmpyid = c.cmpyid and c.status = 0 and t" +
                                                       ".status= 0 and t.createdtimestamp >= ? and t.createdtimestamp" +
                                                       " <= ? group by t.cmpyid, c.name,t.triptype  order by c.name;";
  private final static String DELETE_TRIP_STATS_DATE = "select distinct (t.cmpyid) as cmpyid, c.name as cmpyname, " +
                                                       "count(t.tripid) as trips, t.triptype as triptype  from trip " +
                                                       "t, company c where t.cmpyid = c.cmpyid and c.status = 0 and  " +
                                                       "t.status= -1 and t.createdtimestamp >= ? and t" +
                                                       ".createdtimestamp <= ? group by t.cmpyid, c.name," +
                                                       "t.triptype  order by c.name;";
  private final static String PENDING_TRIP_STATS_DATE = "select distinct (t.cmpyid) as cmpyid, c.name as cmpyname, " +
                                                        "count(t.tripid) as trips, t.triptype as triptype  from trip " +
                                                        "t, company c where t.cmpyid = c.cmpyid and c.status = 0 and " +
                                                        " t.status= 1 and t.createdtimestamp >= ? and t" +
                                                        ".createdtimestamp <= ? group by t.cmpyid, c.name," +
                                                        "t.triptype  order by c.name;";


  private final static String POI_STATS_CREATED = "SELECT distinct (c.cmpyid) AS cmpyid, c.name AS cmpyname, " +
                                                  "       count(p.poi_id) AS pois\n" +
                                                  "FROM poi p, company c\n" +
                                                  "WHERE p.state = 'ACTIVE' AND p.cmpy_id = c.cmpy_id AND \n" +
                                                  "      c.status = 0 AND \n" +
                                                  "      p.createdtimestamp >= ? and p.createdtimestamp <= ? \n" +
                                                  "GROUP BY c.cmpyid, c.name;";

  private final static String POI_STATS_MODIFIED = "SELECT distinct (c.cmpyid) AS cmpyid, c.name AS cmpyname, " +
                                                   "count(p.poi_id) AS pois\n" +
                                                   "FROM poi p, company c\n" +
                                                   "WHERE p.state = 'ACTIVE' AND p.cmpy_id = c.cmpy_id AND \n" +
                                                   "      c.status = 0 AND\n" +
                                                   "      p.createdtimestamp < ? AND\n" +
                                                   "      p.lastupdatedtimestamp >= ? AND\n" +
                                                   "      p.lastupdatedtimestamp <= ? \n" +
                                                   "GROUP BY c.cmpyid, c.name;";
  private final static String POI_STATS_DELETED = "SELECT distinct (c.cmpyid) AS cmpyid, c.name AS cmpyname, " +
                                                  "count(p.poi_id) AS pois\n" +
                                                  "FROM poi p, company c\n" +
                                                  "WHERE p.state = 'DELETED' AND p.cmpy_id = c.cmpy_id AND \n" +
                                                  "      c.status = 0 AND\n" +
                                                  "      p.createdtimestamp < ? AND\n" +
                                                  "      p.lastupdatedtimestamp >= ? AND\n" +
                                                  "      p.lastupdatedtimestamp <= ? \n" +
                                                  "GROUP BY c.cmpyid, c.name;\n";

  private final static String GUIDE_STATS_CREATE_DATE = "select distinct (p.cmpyid) as cmpyid, c.name as cmpyname, " +
                                                        "count(p.destinationid) as guides from destination p, " +
                                                        "company c where p.status = 0 and p.cmpyid = c.cmpyid and c" +
                                                        ".status = 0 and   p.createdtimestamp >= ? and p" +
                                                        ".createdtimestamp <= ?group by p.cmpyid, " +
                                                        "c.name  order by c.name;";
  private final static String GUIDE_STATS_DELETE_DATE = "select distinct (p.cmpyid) as cmpyid, c.name as cmpyname, " +
                                                        "count(p.destinationid) as guides from destination p, " +
                                                        "company c where p.status = -1 and p.cmpyid = c.cmpyid and c" +
                                                        ".status = 0 and   p.createdtimestamp <> p" +
                                                        ".lastupdatedtimestamp and p.lastupdatedtimestamp >= ? and  p" +
                                                        ".lastupdatedtimestamp <= ? group by p.cmpyid, " +
                                                        "c.name  order by c.name;";
  private final static String GUIDE_PAGES_STATS_MODIFIED_DATE = "select distinct (p.cmpyid) as cmpyid, " +
                                                                "c.name as cmpyname, " +
                                                                "count(g.destinationguideid) as pages from " +
                                                                "destination p, destination_guide g, " +
                                                                "company c where p.status = 0 and  p.cmpyid = c" +
                                                                ".cmpyid and c.status = 0 and   p.destinationid = g" +
                                                                ".destinationid and g.createdtimestamp <> g" +
                                                                ".lastupdatedtimestamp and g.lastupdatedtimestamp >= " +
                                                                "? and  g.lastupdatedtimestamp <= ? group by p" +
                                                                ".cmpyid, c.name  order by c.name;";
  private final static String GUIDE_PAGES_STATS_CREATED_DATE = "select distinct (p.cmpyid) as cmpyid, " +
                                                               "c.name as cmpyname, " +
                                                               "count(g.destinationguideid) as pages from destination" +
                                                               " p, destination_guide g, " +
                                                               "company c where p.status = 0 and p.cmpyid = c.cmpyid " +
                                                               "and c.status = 0 and   p.destinationid = g" +
                                                               ".destinationid and g.createdtimestamp >= ? and  g" +
                                                               ".createdtimestamp <= ? group by p.cmpyid, " +
                                                               "c.name  order by c.name;";
  private final static String GUIDE_PAGES_STATS_DELETED_DATE = "select distinct (p.cmpyid) as cmpyid, " +
                                                               "c.name as cmpyname, " +
                                                               "count(g.destinationguideid) as pages from destination" +
                                                               " p, destination_guide g, " +
                                                               "company c where p.status = -1 and  p.cmpyid = c" +
                                                               ".cmpyid and c.status = 0 and   p.destinationid = g" +
                                                               ".destinationid and g.createdtimestamp <> g" +
                                                               ".lastupdatedtimestamp and g.lastupdatedtimestamp >= ?" +
                                                               " and  g.lastupdatedtimestamp <= ? group by p.cmpyid, " +
                                                               "c.name  order by c.name;";


  private final static String MOBILE_USER_COUNT = "select count(pk) as userscount from users where status = 0;";
  private final static String MOBILE_NEW_USER_COUNT = "select count(pk) as userscount from users where status = 0 and" +
                                                      " createtimestamp >= ? and createtimestamp < ?;";
  private final static String MOBILE_SESSION_COUNT = "select osver, dev, devver, " +
                                                     "count(pk) as sessions from usersession  where " +
                                                     "lastupdatetimestamp >= ? and lastupdatetimestamp < ? group by " +
                                                     "osver, dev, devver order by devver;";
  private final static String MOBILE_ACTIVE_SHARE_COUNT = "select sharetype, count(pk) as shares from sharedmaps  " +
                                                          "where status = 0 group by sharetype;";
  private final static String MOBILE_NEW_SHARE_COUNT = "select sharetype, count(pk) as shares from sharedmaps  where " +
                                                       "status = 0 and lastupdatetimestamp >= ? and " +
                                                       "lastupdatetimestamp < ?group by sharetype;";
  private final static String MOBILE_DELETE_SHARE_COUNT = "select sharetype, count(pk) as shares from sharedmaps  " +
                                                          "where status = -1 and lastupdatetimestamp >= ? and " +
                                                          "lastupdatetimestamp < ?group by sharetype;";

  private final static String MOBILE_ACTIVE_EVENT_COUNT = "select maptype, count(pk) as events from maps  where " +
                                                          "status = 0 group by maptype;";
  private final static String MOBILE_NEW_EVENT_COUNT = "select maptype, count(pk) as events from maps  where status =" +
                                                       " 0 and createtimestamp >= ? and createtimestamp < ? group by " +
                                                       "maptype;";
  private final static String MOBILE_DELETE_EVENT_COUNT = "select maptype, count(pk) as events from maps  where  " +
                                                          "status = -1  and lastupdatetimestamp >= ? and " +
                                                          "lastupdatetimestamp < ? group by maptype;";
  private final static String MOBILE_CHANGE_EVENT_COUNT = "select count(pk) as events from mapcomments where " +
                                                          "type='SYSTEM' and createtimestamp >= ? and createtimestamp" +
                                                          " < ?;";


  private final static String USER_AUDIT_LOGON = "select count(pk) as user from user_audit where activitytype = 0 and" +
                                                 " createdtimestamp >= ? and createdtimestamp < ?;";
  private final static String USER_AUDIT_FAILED_LOGON = "select count(pk) as user from user_audit where (activitytype" +
                                                        " = 3 or activitytype = 7 )  and createdtimestamp >= ? and " +
                                                        "createdtimestamp < ?;";
  private final static String USER_AUDIT_RESET_COUNT = "select count(pk) as user from user_audit where (activitytype " +
                                                       "= 1 or activitytype = 8 )  and createdtimestamp >= ? and " +
                                                       "createdtimestamp < ?;";

  private final static String USER_AUDIT_FAILED = "select createdby, count(pk) as user from user_audit where " +
                                                  "(activitytype = 3 or activitytype = 7 ) and createdtimestamp >= ? " +
                                                  "and createdtimestamp < ? group by createdby order by user desc";
  private final static String USER_AUDIT_RESET = "select createdby, count(pk) as user from user_audit where " +
                                                 "(activitytype = 1 or activitytype = 8 ) and createdtimestamp >= ? " +
                                                 "and createdtimestamp < ? group by createdby order by user desc";

  public static Map<String, CmpyStatsView> getCmpyStats(long startTimestamp, long endTimestamp, Connection conn)
      throws SQLException {
    Map<String, CmpyStatsView> cmpyStats = new HashMap<String, CmpyStatsView>();

    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;
    try {
      //get a seqence id from the database
      //sequence is between 1 and 999 and rolls over - combination of current start time in sec and sequence is
      // unique for up to 999 nodes
      prep = conn.prepareStatement(CMPY_TRIPS_STATS_DATE);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        CmpyStatsView view = cmpyStats.get(rs.getString("cmpyid"));
        if (view == null) {
          view = new CmpyStatsView();
          view.cmpyid = rs.getString("cmpyid");
          view.name = rs.getString("cmpyname");
          view.cmpyType = rs.getString("cmpytype");
          if (view.cmpyType != null) {
            if (view.cmpyType.equals(APPConstants.CMPY_TYPE_DESTINATION)) {
              view.cmpyType = "Destination";
            }
            else if (view.cmpyType.equals(APPConstants.CMPY_TYPE_TRAVEL)) {
              view.cmpyType = "Travel";
            }
            else if (view.cmpyType.equals(APPConstants.CMPY_TYPE_TRIAL)) {
              view.cmpyType = "Trial";
            }
          }
        }
        view.numTripsPublished = rs.getInt("trips");
        cmpyStats.put(view.cmpyid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(CMPY_TRIPS_ACTIVITY_DATE);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        CmpyStatsView view = cmpyStats.get(rs.getString("cmpyid"));
        if (view == null) {
          view = new CmpyStatsView();
          view.cmpyid = rs.getString("cmpyid");
          view.name = rs.getString("cmpyname");
          view.cmpyType = rs.getString("cmpytype");
          if (view.cmpyType != null) {
            if (view.cmpyType.equals(APPConstants.CMPY_TYPE_DESTINATION)) {
              view.cmpyType = "Destination";
            }
            else if (view.cmpyType.equals(APPConstants.CMPY_TYPE_TRAVEL)) {
              view.cmpyType = "Travel";
            }
            else if (view.cmpyType.equals(APPConstants.CMPY_TYPE_TRIAL)) {
              view.cmpyType = "Trial";
            }
          }
        }
        view.numTripsPublishedActivity = rs.getInt("trips");
        cmpyStats.put(view.cmpyid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(ACTIVE_TRIP_STATS_DATE);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        CmpyStatsView view = cmpyStats.get(rs.getString("cmpyid"));
        if (view == null) {
          view = new CmpyStatsView();
          view.cmpyid = rs.getString("cmpyid");
          view.name = rs.getString("cmpyname");

        }
        int triptype = rs.getInt("triptype");
        view.numTripsCreated = rs.getInt("trips");


        cmpyStats.put(view.cmpyid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(DELETE_TRIP_STATS_DATE);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        CmpyStatsView view = cmpyStats.get(rs.getString("cmpyid"));
        if (view == null) {
          view = new CmpyStatsView();
          view.cmpyid = rs.getString("cmpyid");
          view.name = rs.getString("cmpyname");

        }
        int triptype = rs.getInt("triptype");

        view.numTripsDeleted = rs.getInt("trips");
        cmpyStats.put(view.cmpyid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(PENDING_TRIP_STATS_DATE);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        CmpyStatsView view = cmpyStats.get(rs.getString("cmpyid"));
        if (view == null) {
          view = new CmpyStatsView();
          view.cmpyid = rs.getString("cmpyid");
          view.name = rs.getString("cmpyname");

        }
        int triptype = rs.getInt("triptype");
        view.numTripsPending = rs.getInt("trips");
        cmpyStats.put(view.cmpyid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(POI_STATS_CREATED);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        CmpyStatsView view = cmpyStats.get(rs.getString("cmpyid"));
        if (view == null) {
          view = new CmpyStatsView();
          view.cmpyid = rs.getString("cmpyid");
          view.name = rs.getString("cmpyname");

        }
        view.numPoiCreated = rs.getInt("pois");
        cmpyStats.put(view.cmpyid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(POI_STATS_MODIFIED);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, startTimestamp);
      prep.setLong(3, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        CmpyStatsView view = cmpyStats.get(rs.getString("cmpyid"));
        if (view == null) {
          view = new CmpyStatsView();
          view.cmpyid = rs.getString("cmpyid");
          view.name = rs.getString("cmpyname");

        }
        view.numPoiModified = rs.getInt("pois");
        cmpyStats.put(view.cmpyid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(POI_STATS_DELETED);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, startTimestamp);
      prep.setLong(3, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        CmpyStatsView view = cmpyStats.get(rs.getString("cmpyid"));
        if (view == null) {
          view = new CmpyStatsView();
          view.cmpyid = rs.getString("cmpyid");
          view.name = rs.getString("cmpyname");
        }
        view.numPoiDeleted = rs.getInt("pois");
        cmpyStats.put(view.cmpyid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }



      prep = conn.prepareStatement(GUIDE_STATS_CREATE_DATE);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        CmpyStatsView view = cmpyStats.get(rs.getString("cmpyid"));
        if (view == null) {
          view = new CmpyStatsView();
          view.cmpyid = rs.getString("cmpyid");
          view.name = rs.getString("cmpyname");

        }
        view.numGuidesCreated = rs.getInt("guides");
        cmpyStats.put(view.cmpyid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(GUIDE_STATS_DELETE_DATE);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        CmpyStatsView view = cmpyStats.get(rs.getString("cmpyid"));
        if (view == null) {
          view = new CmpyStatsView();
          view.cmpyid = rs.getString("cmpyid");
          view.name = rs.getString("cmpyname");

        }
        view.numGuidesDeleted = rs.getInt("guides");
        cmpyStats.put(view.cmpyid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(GUIDE_PAGES_STATS_CREATED_DATE);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        CmpyStatsView view = cmpyStats.get(rs.getString("cmpyid"));
        if (view == null) {
          view = new CmpyStatsView();
          view.cmpyid = rs.getString("cmpyid");
          view.name = rs.getString("cmpyname");

        }
        view.numGuidesPagesCreated = rs.getInt("pages");
        cmpyStats.put(view.cmpyid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(GUIDE_PAGES_STATS_MODIFIED_DATE);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        CmpyStatsView view = cmpyStats.get(rs.getString("cmpyid"));
        if (view == null) {
          view = new CmpyStatsView();
          view.cmpyid = rs.getString("cmpyid");
          view.name = rs.getString("cmpyname");

        }
        view.numGuidesPagesModified = rs.getInt("pages");
        cmpyStats.put(view.cmpyid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(GUIDE_PAGES_STATS_DELETED_DATE);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        CmpyStatsView view = cmpyStats.get(rs.getString("cmpyid"));
        if (view == null) {
          view = new CmpyStatsView();
          view.cmpyid = rs.getString("cmpyid");
          view.name = rs.getString("cmpyname");

        }
        view.numGuidesPagesDeleted = rs.getInt("pages");
        cmpyStats.put(view.cmpyid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }


      return cmpyStats;
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }


  public static Map<String, UserStatsView> getUserStats(long startTimestamp, long endTimestamp, Connection conn)
      throws SQLException {
    Map<String, UserStatsView> userStats = new HashMap<String, UserStatsView>();

    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;
    try {

      prep = conn.prepareStatement(USER_TRIP_STATS_DATE);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        UserStatsView view = userStats.get(rs.getString("user"));
        if (view == null) {
          view = new UserStatsView();
          view.userid = rs.getString("user");
          view.firstName = rs.getString("firstname");
          view.lastName = rs.getString("lastname");
          view.cmpyName = rs.getString("cmpyname");

        }
        view.numTripsPublished = rs.getInt("trips");
        userStats.put(view.userid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(USER_TRIP_ACTIVITY_DATE);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        UserStatsView view = userStats.get(rs.getString("user"));
        if (view == null) {
          view = new UserStatsView();
          view.userid = rs.getString("user");
          view.firstName = rs.getString("firstname");
          view.lastName = rs.getString("lastname");
          view.cmpyName = rs.getString("cmpyname");


        }
        view.numTripsPublishedActivity = rs.getInt("trips");
        userStats.put(view.userid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(USER_ACTIVE_TRIP_STATS_DATE);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        UserStatsView view = userStats.get(rs.getString("user"));
        if (view == null) {
          view = new UserStatsView();
          view.userid = rs.getString("user");
          view.firstName = rs.getString("firstname");
          view.lastName = rs.getString("lastname");
          view.cmpyName = rs.getString("cmpyname");


        }
        int triptype = rs.getInt("triptype");
        view.numTripsCreated = rs.getInt("trips");
        userStats.put(view.userid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(USER_DELETE_TRIP_STATS_DATE);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        UserStatsView view = userStats.get(rs.getString("user"));
        if (view == null) {
          view = new UserStatsView();
          view.userid = rs.getString("user");
          view.firstName = rs.getString("firstname");
          view.lastName = rs.getString("lastname");
          view.cmpyName = rs.getString("cmpyname");


        }
        int triptype = rs.getInt("triptype");
        view.numTripsDeleted = rs.getInt("trips");
        userStats.put(view.userid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(USER_PENDING_TRIP_STATS_DATE);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        UserStatsView view = userStats.get(rs.getString("user"));
        if (view == null) {
          view = new UserStatsView();
          view.userid = rs.getString("user");
          view.firstName = rs.getString("firstname");
          view.lastName = rs.getString("lastname");
          view.cmpyName = rs.getString("cmpyname");

        }
        int triptype = rs.getInt("triptype");
        view.numTripsPending = rs.getInt("trips");
        userStats.put(view.userid, view);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      return userStats;
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static ActivityStatsView getCurrentStats(long startTimestamp,
                                                  long endTimestamp,
                                                  boolean currentMonth,
                                                  Connection conn)
      throws SQLException {
    ActivityStatsView statsView = new ActivityStatsView();

    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;
    try {

      prep = conn.prepareStatement(CMPY_NEW_COUNT);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        statsView.numNewCmpy = rs.getInt("cmpy");
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(CMPY_DELETE_COUNT);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        statsView.numDeletedCmpy = rs.getInt("cmpy");
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(USER_NEW_COUNT);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        statsView.numNewUsers = rs.getInt("user");
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(USER_DELETE_COUNT);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        statsView.numDeletedUsers = rs.getInt("user");
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }


      prep = conn.prepareStatement(USER_AUDIT_LOGON);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        statsView.numLogons = rs.getInt("user");
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(USER_AUDIT_FAILED_LOGON);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        statsView.numFailedLogons = rs.getInt("user");
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(USER_AUDIT_RESET_COUNT);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        statsView.numPasswordReset = rs.getInt("user");
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      statsView.topFailedLogons = new HashMap<String, String>();
      prep = conn.prepareStatement(USER_AUDIT_FAILED);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      prep.setMaxRows(5);
      rs = prep.executeQuery();
      while (rs.next()) {
        statsView.topFailedLogons.put(rs.getString("createdby"), rs.getString("user"));
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      statsView.topPwdReset = new HashMap<String, String>();
      prep = conn.prepareStatement(USER_AUDIT_RESET);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      prep.setMaxRows(5);
      rs = prep.executeQuery();
      while (rs.next()) {
        statsView.topPwdReset.put(rs.getString("createdby"), rs.getString("user"));
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }


      if (currentMonth) {
        prep = conn.prepareStatement(ACTIVE_TRIP_STATS);
        rs = prep.executeQuery();
        while (rs.next()) {
          int triptype = rs.getInt("triptype");
          statsView.numTripsCreated = rs.getInt("trips");


        }
        if (prep != null) {
          prep.close();
        }
        if (rs != null) {
          rs.close();
        }

        prep = conn.prepareStatement(DELETE_TRIP_STATS);
        rs = prep.executeQuery();
        while (rs.next()) {
          int triptype = rs.getInt("triptype");
          statsView.numTripsDeleted = rs.getInt("trips");

        }
        if (prep != null) {
          prep.close();
        }
        if (rs != null) {
          rs.close();
        }

        prep = conn.prepareStatement(PENDING_TRIP_STATS);
        rs = prep.executeQuery();
        while (rs.next()) {
          int triptype = rs.getInt("triptype");

          statsView.numTripsPending = rs.getInt("trips");

        }
        if (prep != null) {
          prep.close();
        }
        if (rs != null) {
          rs.close();
        }

        prep = conn.prepareStatement(CMPY_ACTIVE_COUNT);
        rs = prep.executeQuery();
        while (rs.next()) {
          statsView.numActiveCmpy = rs.getInt("cmpy");
        }
        if (prep != null) {
          prep.close();
        }
        if (rs != null) {
          rs.close();
        }

        prep = conn.prepareStatement(USER_ACTIVE_COUNT);
        rs = prep.executeQuery();
        while (rs.next()) {
          statsView.numActiveUsers = rs.getInt("user");
        }
        if (prep != null) {
          prep.close();
        }
        if (rs != null) {
          rs.close();
        }


        prep = conn.prepareStatement(USER_INACTIVE_COUNT);
        rs = prep.executeQuery();
        statsView.inactiveUsers = new ArrayList<UserStatsView>();
        while (rs.next()) {
          UserStatsView v = new UserStatsView();
          v.cmpyName = rs.getString("cmpyname");
          v.email = rs.getString("email");
          v.firstName = rs.getString("firstname");
          v.lastName = rs.getString("lastname");
          v.userid = rs.getString("userid");
          v.cmpyId = rs.getString("cmpyid");
          statsView.inactiveUsers.add(v);

        }
        if (prep != null) {
          prep.close();
        }
        if (rs != null) {
          rs.close();
        }


        prep = conn.prepareStatement(USER_NOT_LOGGEDIN_COUNT);
        rs = prep.executeQuery();
        statsView.neverLoggedInUsers = new ArrayList<UserStatsView>();
        while (rs.next()) {
          UserStatsView v = new UserStatsView();
          v.cmpyName = rs.getString("cmpyname");
          v.email = rs.getString("email");
          v.firstName = rs.getString("firstname");
          v.lastName = rs.getString("lastname");
          v.userid = rs.getString("userid");
          v.cmpyId = rs.getString("cmpyid");
          statsView.neverLoggedInUsers.add(v);

        }
        if (prep != null) {
          prep.close();
        }
        if (rs != null) {
          rs.close();
        }


        prep = conn.prepareStatement(DECLINED_CMPY_STATUS);
        prep.setLong(1, startTimestamp);
        prep.setLong(2, endTimestamp);
        rs = prep.executeQuery();
        statsView.declinedUserAgreement = new ArrayList<CmpyStatsView>();
        while (rs.next()) {
          CmpyStatsView v = new CmpyStatsView();
          v.name = rs.getString("cmpyname");
          v.cmpyid = rs.getString("cmpyid");
          v.targetAgreement = rs.getString("targetVer");
          v.userid = rs.getString("createdby");
          v.timestamp = Utils.formatTimestamp(rs.getString("createdtimestamp"));
          statsView.declinedUserAgreement.add(v);

        }
        if (prep != null) {
          prep.close();
        }
        if (rs != null) {
          rs.close();
        }

        prep = conn.prepareStatement(NO_ACCEPTED_CMPY_STATUS);
        rs = prep.executeQuery();
        statsView.noAcceptedUserAgreement = new ArrayList<CmpyStatsView>();
        while (rs.next()) {
          CmpyStatsView v = new CmpyStatsView();
          v.name = rs.getString("cmpyname");
          v.cmpyid = rs.getString("cmpyid");
          v.targetAgreement = rs.getString("targetVer");
          statsView.noAcceptedUserAgreement.add(v);

        }
        if (prep != null) {
          prep.close();
        }
        if (rs != null) {
          rs.close();
        }


      }

      return statsView;
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }


  public static MobileStatsView getCurrentMobileStats(long startTimestamp,
                                                      long endTimestamp,
                                                      boolean currentMonth,
                                                      Connection conn)
      throws SQLException {
    MobileStatsView statsView = new MobileStatsView();

    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;
    try {

      prep = conn.prepareStatement(MOBILE_NEW_USER_COUNT);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        statsView.numNewUsers = rs.getInt("userscount");
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      statsView.sessions = new ArrayList<MobileSessionsView>();

      prep = conn.prepareStatement(MOBILE_SESSION_COUNT);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        MobileSessionsView sView = new MobileSessionsView();
        sView.count = rs.getInt("sessions");
        sView.device = rs.getString("devver");
        sView.os = rs.getString("dev");
        sView.osVer = rs.getString("osver");
        statsView.sessions.add(sView);
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(MOBILE_NEW_EVENT_COUNT);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        String triptype = rs.getString("maptype");
        if (triptype != null && triptype.equals(APPConstants.MAP_TRIP_TYPE)) {
          statsView.numNewTrips = rs.getInt("events");
        }
        else {
          statsView.numNewEvents = rs.getInt("events");

        }
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(MOBILE_NEW_SHARE_COUNT);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        String shareType = rs.getString("sharetype");
        if (shareType != null) {
          if (shareType.equals("1")) {
            statsView.numNewFullShares = rs.getInt("shares");
          }
          else if (shareType.equals("4")) {
            statsView.numNewViewShares = rs.getInt("shares");
          }
          else if (shareType.equals("5")) {
            statsView.numNewRecommendShares = rs.getInt("shares");
          }
        }

        statsView.numNewShares += rs.getInt("shares");

      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(MOBILE_DELETE_SHARE_COUNT);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        statsView.numDeletedShares = rs.getInt("shares");
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(MOBILE_DELETE_EVENT_COUNT);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        statsView.numDeletedEvents = rs.getInt("events");
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      prep = conn.prepareStatement(MOBILE_CHANGE_EVENT_COUNT);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      rs = prep.executeQuery();
      while (rs.next()) {
        statsView.numModifiedEvents = rs.getInt("events");
      }
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      if (currentMonth) {
        prep = conn.prepareStatement(MOBILE_ACTIVE_EVENT_COUNT);
        rs = prep.executeQuery();
        while (rs.next()) {
          String triptype = rs.getString("maptype");
          if (triptype != null && triptype.equals(APPConstants.MAP_TRIP_TYPE)) {
            statsView.numActiveTrips = rs.getInt("events");
          }
          else {
            statsView.numActiveEvents = rs.getInt("events");

          }
        }
        if (prep != null) {
          prep.close();
        }
        if (rs != null) {
          rs.close();
        }


        prep = conn.prepareStatement(MOBILE_ACTIVE_SHARE_COUNT);
        rs = prep.executeQuery();
        while (rs.next()) {
          String shareType = rs.getString("sharetype");
          if (shareType != null) {
            if (shareType.equals("1")) {
              statsView.numActiveFullShares = rs.getInt("shares");
            }
            else if (shareType.equals("4")) {
              statsView.numActiveViewShares = rs.getInt("shares");
            }
            else if (shareType.equals("5")) {
              statsView.numActiveRecommendShares = rs.getInt("shares");
            }
          }

          statsView.numActiveShares += rs.getInt("shares");
        }
        if (prep != null) {
          prep.close();
        }
        if (rs != null) {
          rs.close();
        }

        prep = conn.prepareStatement(MOBILE_USER_COUNT);
        rs = prep.executeQuery();
        while (rs.next()) {
          statsView.numActiveUsers = rs.getInt("userscount");
        }
        if (prep != null) {
          prep.close();
        }
        if (rs != null) {
          rs.close();
        }
      }

      return statsView;

    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }
}
