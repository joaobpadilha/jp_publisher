package com.mapped.publisher.persistence.communicator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.firebase.client.Firebase;
import com.mapped.publisher.utils.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by surge on 2015-08-04.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationConf
    extends FirebaseObject {

  /* Conference Name */
  public String name;
  public Long lastActivity;
  public Map<String, NotificationRoom> rooms;

  public Firebase update(Firebase ref) {
    Map<String, Object> uVals = new HashMap<>();
    uVals.put("name",name);
    ref.updateChildren(uVals);
    Log.debug("Firebase requested record update: " + ref.getPath().toString());
    return ref;
  }
}
