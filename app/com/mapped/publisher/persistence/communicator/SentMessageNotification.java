package com.mapped.publisher.persistence.communicator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Sent message notification is used for unmarshalling messages from JS Communicator
 * Created by surge on 2016-02-03.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SentMessageNotification {
  public String       confId;
  public String       roomId;
  public String       from;
  public String       message;
  public List<String> roomUsers;
  public List<String> roomNotify;
}
