package com.mapped.publisher.persistence.communicator;

/**
 * Created by surge on 2015-07-16.
 */
public enum NotificationType {
  NEW,
  MESSAGE,
  ALERT,
  CHANGE
}
