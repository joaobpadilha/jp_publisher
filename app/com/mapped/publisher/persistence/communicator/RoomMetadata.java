package com.mapped.publisher.persistence.communicator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.mapped.publisher.utils.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by surge on 2015-06-24.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RoomMetadata
    extends FirebaseObject {
  /*
    YAML Definition:
    ~$roomId:
      type: object
      constraint: next.hasChildren(['name', 'type'])
      properties:
        id:
          type: string
          constraint: next === $roomId
        createdByUserId:
          type: string
          constraint: auth.uid === next
        numUsers:
          type: number
        type:
          type: string
          constraint: next === 'public' || next === 'private' || ( next === 'official' && isModerator() )
        authorizedUsers:  {}
        #List of users to be notified
        notifyUsers: {}
        #External Identifier: (UMAPPED: Associated booking ID)
        exId: {type: string}
      required: [id, createdByUserId, type]
   */

  public String   id;
  public String   name;
  public RoomType type;

  /**
   * Who created
   */
  public String   by;
  /**
   * Time of creation
   */
  public Long     at;
  public UserList authorizedUsers;
  public UserList notifyUsers;

  public static class UserList
      extends HashMap<String, Boolean> {

  }

  public RoomMetadata(String name) {
    this();
    this.name = name;
  }

  public RoomMetadata() {
    authorizedUsers = new UserList();
    notifyUsers = new UserList();
  }

  public void addAuthorizedUser(String username) {
    authorizedUsers.put(username, true);
  }

  public void addNotifiedUser(String username) {
    notifyUsers.put(username, true);
  }


  public Firebase update(final Firebase ref) {
    Map<String, Object> uVals = new HashMap<>();
    uVals.put("id", id);
    uVals.put("name", name);
    uVals.put("type", type.name());
    uVals.put("by", by);
    uVals.put("at", at);
    uVals.put("authorizedUsers", authorizedUsers);
    ref.updateChildren(uVals);
    Log.debug("Firebase requested record update: " + ref.getPath().toString());


    ref.child("notifyUsers").addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot snapshot) {
        UserList currNotifiedStatus = snapshot.getValue(UserList.class);
        if (currNotifiedStatus != null) {
          for (String userId : currNotifiedStatus.keySet()) {
            if (notifyUsers.containsKey(userId)) {
              notifyUsers.put(userId, currNotifiedStatus.get(userId));
            }
          }
        }
        ref.child("notifyUsers").setValue(notifyUsers);
      }

      @Override
      public void onCancelled(FirebaseError firebaseError) {
        Log.err("Failed to update notified users list");
      }
    });

    return ref;
  }

}
