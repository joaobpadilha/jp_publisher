package com.mapped.publisher.persistence.communicator;

/**
 * Created by surge on 2015-06-24.
 */
public enum RoomType {
  PUBLIC,
  PRIVATE;
}
