package com.mapped.publisher.persistence.communicator;

import actors.ActorsHelper;
import actors.FirebaseActor;
import actors.SupervisorActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.dispatch.Futures;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.firebase.client.*;
import com.firebase.security.token.TokenGenerator;
import com.firebase.security.token.TokenOptions;
import com.mapped.common.CoreConstants;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import models.publisher.*;
import play.libs.Akka;
import scala.concurrent.Promise;
import scala.concurrent.duration.Duration;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;

/**
 * Server-sde component of the Umapped Communicator.
 * <p/>
 * Created by surge on 2015-06-23.
 */
public class Communicator {
  public final static  long    RELEASE_TS_1_15                = 1441411200000l; //1441411200000 is September 5th,
  // 2015 00:00
  /**
   * What offset into the future to use to determine whether or not trip needs to be cleaned up from the
   * messenger.
   */
  public final static  long    TRIP_EXPIRATION_OFFSET         = TimeUnit.DAYS.toMillis(30);
  public final static  int     FB_EVICTION_CYCLES             = 3; //Number of cycles before current leader is evicted
  private final static boolean DEBUG_LEADER_ELECTION          = false;
  private final static boolean CHAOS_MONKEY_MODE              = false;
  /**
   * If in Chaos Monkey mode will connect from 0 to this value of seconds after disconnect
   */
  private final static int     CHAOS_MAX_CONNECT_DELAY_SEC    = 300; //5 minutes
  /**
   * If in Chaos Monkey mode will disconnect from 0 to this value of seconds after connect
   */
  private final static int     CHAOS_MAX_DISCONNECT_DELAY_SEC = 300; //5 minutes
  /**
   * URI path where the chat application is saved, eventually should be "/chat"
   */
  private final static String  FB_COMMUNICATOR_PATH           = "";
  /**
   * By default all instances of publisher are known as chat moderators to gain access
   */
  private final static String  COMMUNICATOR_SERVER_USER       = "publisher";
  private final static String  COMMUNICATOR_SERVER_NAME       = "Umapped Platform";
  private final static long    ONE_DAY                        = (3600 * 24) * 1000;
  private final static long    ONE_WEEK                       = 7 * ONE_DAY;

  private enum SSEventType {
    CHANGE,
    REMOVE,
    ADD,
    MOVE
  }

  private final int                        FB_LEADER_EVICTION_MS;
  private final int                        FB_ORPHAN_KILL_MS;
  private       Firebase                   fb;
  private       boolean                    connected;
  private       Firebase                   refConferences;
  private       Firebase                   refMessages;
  private       Firebase                   refUsers;
  private       Firebase                   refRoomUsers;
  private       Firebase                   refRoomModerators;
  private       Integer                    clusterId;
  private       ServerStatus               myStatus;
  private       Map<Integer, ServerStatus> otherServers;
  private       ChildEventListener         clusterStatusListener;
  private       Firebase                   clusterStatusRef;
  private       OnDisconnect               clusterMyStatusDisconnect;

  //Bill Pugh's Java Singleton Approach, most efficient Java singleton
  private static class CommunicatorHelper {
    private static final Communicator INSTANCE = new Communicator();
  }

  private Communicator() {
    int pingCycleSec = 60;
    try {
      pingCycleSec = Integer.parseInt(ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.FIREBASE_PING_SEC));
    }
    catch (Exception e) {
      Log.err("FirebaseActor: misconfigured ENV variable FIREBASE_PING_SEC. Must be integer seconds value");
    }

    FB_LEADER_EVICTION_MS = FB_EVICTION_CYCLES * pingCycleSec * 1000;
    FB_ORPHAN_KILL_MS = FB_LEADER_EVICTION_MS;

    connected = false;
    fb = new Firebase("https://" + ConfigMgr.getAppParameter(CoreConstants.FIREBASE_URI) + FB_COMMUNICATOR_PATH);
    String authToken = getModeratorFirebaseToken();
    clusterId = Integer.valueOf(DBConnectionMgr.getClusterId());
    otherServers = new HashMap<>();

    fb.authWithCustomToken(authToken, new Firebase.AuthResultHandler() {
      @Override
      public void onAuthenticated(AuthData authData) {
        Log.debug("Firebase: Initial Authentication Success");

        //Network Connection monitoring
        fbConnectionStateMonitorSetup();

        //Connection Authentication monitoring
        fb.addAuthStateListener(auth -> {
          if (auth != null) {
            Log.debug("Firebase: Authentication Established");
          }
          else {
            Log.err("Firebase: Authentication Lost");
            //TODO: serverTransitionToFollower(true); //If I'm a leader going back to be a follower on connection loss
            //fb.removeAuthStateListener(this);
          }
        });
      }

      @Override
      public void onAuthenticationError(FirebaseError firebaseError) {
        Log.err("Firebase: Initial Authentication Failure");
      }
    });


    refConferences = fb.child("conferences");
    refMessages = fb.child("messages");
    refRoomModerators = fb.child("moderators");
    refUsers = fb.child("users");
    refRoomUsers = fb.child("room-users");

    if (CHAOS_MONKEY_MODE) {
      int delayDisconnect = clusterChaosScheduleDisconnect();
      Log.info("CHAOS MODE: WILL DISCONNECT IN " + delayDisconnect + " seconds...");
    }
  }

  public static Communicator Instance() {
    return CommunicatorHelper.INSTANCE;
  }

  public static boolean isAuthenticated() {
    return CommunicatorHelper.INSTANCE.fb.getAuth() != null;
  }

  /**
   * Fixes user id
   */
  public static String makeUserIdSafe(String userid) {
    return userid.replace('.', '_');
  }

  /**
   * Traveller Identifier based on numeric ID
   *
   * @param tripLink - Account trip link
   * @return
   */
  public static String getTravellerFirebaseToken(Trip trip, AccountTripLink tripLink) {
    long expiry = SecurityMgr.getTripExpirationTs(trip);

    Map<String, Object> authPayload = new HashMap<>();
    if (tripLink.getLegacyId() != null) {
      authPayload.put("uid", tripLink.getLegacyId());
    }
    else {
      authPayload.put("uid", tripLink.getPk().getUid());
    }
    authPayload.put("expiry", expiry);

    Account traveler = Account.find.byId(tripLink.getPk().getUid());
    if (traveler.getFullName() == null || traveler.getFullName().trim().length() == 0) {
      String name[] = traveler.getEmail().split("@");
      authPayload.put("displayName", name[0]);
    }
    else {
      authPayload.put("displayName", traveler.getFullName());
    }

    TokenOptions tokenOptions = new TokenOptions();
    tokenOptions.setExpires(new Date(expiry));
    TokenGenerator tokenGenerator = new TokenGenerator(ConfigMgr.getAppParameter(com.mapped.common.CoreConstants
                                                                                     .FIREBASE_SECRET));
    return tokenGenerator.createToken(authPayload, tokenOptions);
  }

  /**
   * The userId is either Publisher's legacyId, or the AccountTripLink's legacyId for a Traveler
   *
   * @param trip
   * @param userId
   * @param account
   * @return
   */
  public static String getAccountTripFirebaseToken(Trip trip, String userId, Account account) {
    long expiry = SecurityMgr.getTripExpirationTs(trip);

    Map<String, Object> authPayload = new HashMap<>();
    authPayload.put("uid", userId);
    authPayload.put("expiry", expiry);

    if (account.getFullName() == null || account.getFullName().trim().length() == 0) {
      String name[] = account.getEmail().split("@");
      authPayload.put("displayName", name[0]);
    }
    else {
      authPayload.put("displayName", account.getFullName());
    }

    TokenOptions tokenOptions = new TokenOptions();
    tokenOptions.setExpires(new Date(expiry));
    TokenGenerator tokenGenerator = new TokenGenerator(ConfigMgr.getAppParameter(com.mapped.common.CoreConstants
                                                                                     .FIREBASE_SECRET));
    return tokenGenerator.createToken(authPayload, tokenOptions);
  }

  public static String getAccountId(Account a) {
    //TODO: Now this is just a placeholder to get proper Account ID for the messenger
    if (a.getLegacyId() != null) {
      return a.getLegacyId().replace('.', '_');
    }
    else {
      return a.getEncodedId();
    }
  }

  public static String getTravelerId(AccountTripLink tripLink) {
    if (tripLink.getLegacyId() != null && tripLink.getLegacyId().trim().length() > 0) {
      return tripLink.getLegacyId(); //added for notification id reference
    }
    else {
      return Account.encodeId(tripLink.getPk().getUid());
    }
  }

  public Firebase getRefConferences() {
    return refConferences;
  }

  public Firebase getRefMessages() {
    return refMessages;
  }

  public Firebase getRefUsers() {
    return refUsers;
  }

  public Firebase getRefRoomUsers() {
    return refRoomUsers;
  }

  public Firebase getRefRoomModerators() {
    return refRoomModerators;
  }

  public int clusterChaosScheduleConnect() {
    ActorRef fba = Akka.system().actorOf(Props.create(FirebaseActor.class));
    FirebaseActor.CommunicatorCommand cmd = new FirebaseActor.CommunicatorCommand(FirebaseActor.CommunicatorCommand
                                                                                      .Type.CHAOS_CONNECT);
    Random rand      = new Random();
    int    randDelay = rand.nextInt(CHAOS_MAX_CONNECT_DELAY_SEC);
    Akka.system()
        .scheduler()
        .scheduleOnce(Duration.create(randDelay, TimeUnit.SECONDS), fba, cmd, Akka.system().dispatcher(), null);
    return randDelay;
  }

  public int clusterChaosScheduleDisconnect() {
    ActorRef fba = Akka.system().actorOf(Props.create(FirebaseActor.class));
    FirebaseActor.CommunicatorCommand cmd = new FirebaseActor.CommunicatorCommand(FirebaseActor.CommunicatorCommand
                                                                                      .Type.CHAOS_DISCONNECT);
    Random rand      = new Random();
    int    randDelay = rand.nextInt(CHAOS_MAX_DISCONNECT_DELAY_SEC);
    Akka.system()
        .scheduler()
        .scheduleOnce(Duration.create(randDelay, TimeUnit.SECONDS), fba, cmd, Akka.system().dispatcher(), null);
    return randDelay;
  }

  private void fbConnectionStateMonitorSetup() {
    Firebase fbState = new Firebase("https://" + ConfigMgr.getAppParameter(CoreConstants.FIREBASE_URI) + "/" + "" +
                                    "" + ".info/connected");
    fbState.addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot snapshot) {
        connected = snapshot.getValue(Boolean.class);
        if (connected) {
          Log.info("Firebase: Node " + clusterId + " CONNECTED");
          fbOnConnect();
        }
        else {
          Log.err("Firebase: Node " + clusterId + " CONNECTION LOST");
          fbOnDisconnect();
        }
      }

      @Override
      public void onCancelled(FirebaseError error) {
        Log.err("Communicator: " + clusterId + " FIREBASE CONNECTION MONITORING CANCELED");
      }
    });
  }

  private boolean clusterIsThereALeader() {
    for (Integer id : otherServers.keySet()) {
      ServerStatus ss = otherServers.get(id);
      if ((ss.mode == ServerStatus.ServerMode.LEADER) && (ss.update_ts > (System.currentTimeMillis() -
                                                                          FB_LEADER_EVICTION_MS))) {
        return true;
      }
    }
    return false;
  }

  private Integer clusterGetActiveLeader() {
    for (Integer id : otherServers.keySet()) {
      ServerStatus ss = otherServers.get(id);
      if (ss.mode == ServerStatus.ServerMode.LEADER) {
        if (ss.update_ts > System.currentTimeMillis() - FB_LEADER_EVICTION_MS) {
          return id;
        }
      }
    }
    return null;
  }

  private boolean clusterAmIYoungest() {
    for (Integer id : otherServers.keySet()) {
      if (id > clusterId) {
        return false;
      }
    }
    return true;
  }

  private void fbHandleServerStatusUpdate(Integer id, ServerStatus currSS, SSEventType event) {
    ServerStatus prevSS   = null;
    int          compared = clusterId.compareTo(id);
    if (compared != 0) {
      prevSS = otherServers.get(id);
      otherServers.put(id, currSS);
    }
    else {
      //Someone deleted my status node, need to recover since I'm apparently alive
      if (currSS.launch_ts == null) {
        serverTransitionToFollower(true);
      }
      else if (currSS.mode_ts.compareTo(currSS.update_ts) == 0) {
        return;//Ignoring notifications about my own mode updates
      }
    }

    switch (event) {
      case ADD:
        if (DEBUG_LEADER_ELECTION && currSS.launch_ts != null) {
          Log.debug("Communicator: " + clusterId + " ++++++++++++++++++++++++");
          Log.debug("Communicator: " + clusterId + " Publisher Server added: " + id + " uts:" + currSS.update_ts + " " +
                    "" + "" + "" + "" + "type:" + currSS.mode
              .name());
          Log.debug("Communicator: " + clusterId + " ++++++++++++++++++++++++");
        }
        break;
      case CHANGE:
        if (DEBUG_LEADER_ELECTION && currSS.launch_ts != null) {
          Log.debug("Communicator: " + clusterId + " ==========================");
          Log.debug("Communicator: " + clusterId + " Publisher Server changed: " + id + " uts:" + currSS.update_ts +
                    " " + "type:" + currSS.mode
              .name());
          Log.debug("Communicator: " + clusterId + " ==========================");
        }
        break;
      case REMOVE:
        if (compared == 0) {
          Log.err("Communicator: --------- Received own removal notice -----------");
          Log.err("Communicator: ----------- AM I MALFUNCTIONING ??? -------------");
          //Handling exceptional case where I was a leader and someone removed me
          serverTransitionToFollower(true); //I'm no longer a true LEADER, let others rule
          return;
        }
        else {
          otherServers.remove(id);
          if (DEBUG_LEADER_ELECTION && currSS.launch_ts != null) {
            Log.debug("Communicator: " + clusterId + " --------------------------");
            Log.debug("Communicator: " + clusterId + " Publisher Server removed: " + id + " uts:" + currSS.update_ts
                      + " type:" + currSS.mode
                .name());
            Log.debug("Communicator: " + clusterId + " --------------------------");
          }
        }
        break;
      case MOVE:
        Log.err("Communicator: " + clusterId + " <<<<<<<<<<<<<<<<<<<<<<<<");
        Log.err("Communicator: " + clusterId + " Publisher Server moved: " + id + " uts:" + currSS.update_ts + " " +
                "type:" + currSS.mode
            .name());
        Log.err("Communicator: " + clusterId + " >>>>>>>>>>>>>>>>>>>>>>>>");
        break;
    }

    switch (myStatus.mode) {
      case LEADER:
        Integer leaderId = clusterGetActiveLeader();
        if (leaderId != null) {
          ServerStatus ss = otherServers.get(leaderId);
          if (ss.mode_ts < myStatus.mode_ts) { //I'm older since someone younger got to be leader there was a problem
            serverTransitionToFollower(false);
          }
        }
        break;
      case CANDIDATE:
        if (clusterIsThereALeader()) {
          serverTransitionToFollower(false);
        }
        else {
          if (clusterAmIYoungest()) {
            serverTransitionToLeader();
          }
        }
        break;
      case FOLLOWER:
        if (!clusterIsThereALeader()) {
          serverTransitionToCandidate();
        }
        break;
    }
  }

  private void serverTransitionToFollower(boolean force) {
    Log.info("Communicator: " + clusterId + " transitioning to be FOLLOWER");
    Firebase ref = getMyServerStatusReference();
    if (myStatus.mode == ServerStatus.ServerMode.LEADER) {
      final FirebaseActor.CommunicatorCommand cmdLeaderStop = new FirebaseActor.CommunicatorCommand(FirebaseActor
                                                                                                        .CommunicatorCommand.Type.LEADER_STOP);
      ActorsHelper.tell(SupervisorActor.UmappedActor.FIREBASE, cmdLeaderStop);
      //Since previous data was probably wiped try to rewrite it
      force = true;
    }

    if (force) {
      myStatus.mode_ts = System.currentTimeMillis();
      myStatus.mode = ServerStatus.ServerMode.FOLLOWER;
      myStatus.overwrite(ref);
    }
    else {
      myStatus.setMode(ref, ServerStatus.ServerMode.FOLLOWER);
    }
  }

  private void serverTransitionToCandidate() {
    Log.info("Communicator: " + clusterId + " transitioning to be CANDIDATE");
    Firebase ref = getMyServerStatusReference();
    myStatus.setMode(ref, ServerStatus.ServerMode.CANDIDATE);
  }

  private void serverTransitionToLeader() {
    Log.info("Communicator: " + clusterId + " transitioning to be LEADER");
    Firebase ref = getMyServerStatusReference();
    myStatus.setMode(ref, ServerStatus.ServerMode.LEADER);

    final FirebaseActor.CommunicatorCommand cmdLeaderStart = new FirebaseActor.CommunicatorCommand(FirebaseActor
                                                                                                       .CommunicatorCommand.Type.LEADER_START);
    ActorsHelper.tell(SupervisorActor.UmappedActor.FIREBASE, cmdLeaderStart);
  }

  public void clusterKillOrphans() {
    List<Integer> otherServerKeys = new ArrayList<>();
    otherServerKeys.addAll(otherServers.keySet());
    for (Integer id : otherServerKeys) {
      ServerStatus ss = otherServers.get(id);
      if (ss.update_ts < System.currentTimeMillis() - FB_ORPHAN_KILL_MS) {
        refRoomModerators.child(COMMUNICATOR_SERVER_USER).child(id.toString()).removeValue();
      }
    }
  }

  private void fbOnDisconnect() {
    clusterMyStatusDisconnect.cancel();
    clusterStatusRef.removeEventListener(clusterStatusListener);

    final FirebaseActor.CommunicatorCommand cmdPingStop = new FirebaseActor.CommunicatorCommand(FirebaseActor
                                                                                                    .CommunicatorCommand.Type.PING_STOP);
    ActorsHelper.tell(SupervisorActor.UmappedActor.FIREBASE, cmdPingStop);

    otherServers.clear();
    serverTransitionToFollower(true);
  }

  private void fbOnConnect() {
    Firebase myStatusRef = fbServerStatusInitialize();
    clusterMyStatusDisconnect = myStatusRef.onDisconnect();
    clusterMyStatusDisconnect.removeValue();

    clusterStatusListener = new ChildEventListener() {
      @Override
      public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        Integer      id = Integer.valueOf(dataSnapshot.getKey());
        ServerStatus ss = dataSnapshot.getValue(ServerStatus.class);
        fbHandleServerStatusUpdate(id, ss, SSEventType.ADD);
      }

      @Override
      public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        Integer      id = Integer.valueOf(dataSnapshot.getKey());
        ServerStatus ss = dataSnapshot.getValue(ServerStatus.class);
        fbHandleServerStatusUpdate(id, ss, SSEventType.CHANGE);
      }

      @Override
      public void onChildRemoved(DataSnapshot dataSnapshot) {
        Integer      id = Integer.valueOf(dataSnapshot.getKey());
        ServerStatus ss = dataSnapshot.getValue(ServerStatus.class);
        fbHandleServerStatusUpdate(id, ss, SSEventType.REMOVE);
      }

      @Override
      public void onChildMoved(DataSnapshot dataSnapshot, String s) {
        Integer      id = Integer.valueOf(dataSnapshot.getKey());
        ServerStatus ss = dataSnapshot.getValue(ServerStatus.class);
        fbHandleServerStatusUpdate(id, ss, SSEventType.MOVE);
      }

      @Override
      public void onCancelled(FirebaseError firebaseError) {
        Log.err("Publisher Server error monitoring");
      }
    };

    refRoomModerators.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {

        /**
         * Initial load of server status records in Firebase
         */
        DataSnapshot publishers = dataSnapshot.child("publisher");
        for (DataSnapshot p : publishers.getChildren()) {
          Integer id = Integer.valueOf(p.getKey());
          if (!id.equals(clusterId)) { //Add everyone but myself
            ServerStatus ss = p.getValue(ServerStatus.class);
            otherServers.put(id, ss);
          }
        }
        Log.info("Firebase: Node " + clusterId + " joined cluster. There are " + otherServers.size() + " other nodes " +
                 "" + "" + "" + "" + "" + "in cluster");

        /**
         * Subscribing to pings
         */
        clusterStatusRef = refRoomModerators.child(COMMUNICATOR_SERVER_USER);
        clusterStatusRef.addChildEventListener(clusterStatusListener);

        final FirebaseActor.CommunicatorCommand cmdPingStart = new FirebaseActor.CommunicatorCommand(FirebaseActor
                                                                                                         .CommunicatorCommand.Type.PING_START);
        ActorsHelper.tell(SupervisorActor.UmappedActor.FIREBASE, cmdPingStart);
      }

      @Override
      public void onCancelled(FirebaseError firebaseError) {
        Log.err("Fatal: Firebase: failed to access moderators");
        System.exit(1);
      }
    });
  }

  private Firebase fbServerStatusInitialize() {
    if (myStatus == null) {
      myStatus = ServerStatus.buildServerStatus();
    }
    else {
      myStatus.connect_ts = System.currentTimeMillis();
      myStatus.update_ts = myStatus.connect_ts;
    }
    Firebase path = getMyServerStatusReference();
    myStatus.overwrite(path);
    return path;
  }

  public void fbServerStatusUpdate() {
    Firebase path = getMyServerStatusReference();
    myStatus.updateTs(path);
  }

  private Firebase getMyServerStatusReference() {
    return refRoomModerators.child(COMMUNICATOR_SERVER_USER).child(clusterId.toString());
  }

  private String getModeratorFirebaseToken() {
    Map<String, Object> authPayload = new HashMap<>();
    authPayload.put("uid", "publisher");
    authPayload.put("expiry", Long.MAX_VALUE);
    authPayload.put("displayName", COMMUNICATOR_SERVER_NAME);
    TokenOptions tokenOptions = new TokenOptions();
    tokenOptions.setExpires(new Date(Long.MAX_VALUE));
    TokenGenerator tokenGenerator = new TokenGenerator(ConfigMgr.getAppParameter(com.mapped.common.CoreConstants
                                                                                     .FIREBASE_SECRET));
    return tokenGenerator.createToken(authPayload, tokenOptions);
  }

  public void connect() {
    fb.goOnline();
  }

  public void disconnect() {
    fb.goOffline();
  }

  public void sendMsg(final String tripId, final String roomId, final RoomMessage msg) {
    final Notification noti = new Notification();
    noti.fromUserId = msg.userId;
    noti.fromUserName = msg.name;
    noti.notificationType = NotificationType.MESSAGE;
    noti.status = RoomMessageStatus.UNREAD;
    noti.timestamp = System.currentTimeMillis();

    final Firebase refRoomInfo = getRoomReference(tripId, roomId);
    refRoomInfo.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot snapshot) {

        if (snapshot.getValue() == null) {
          Log.err("Communicator: sendMsg failed. No room to post message to... trip ID:" + tripId +
                  " booking ID:" + roomId);
          return;
        }

        RoomMetadata roomInfo = snapshot.getValue(RoomMetadata.class);
        if (roomInfo == null) {
          Log.err("Messenger: sendMsg(): Aborting as trip is not valid for sending messages. Trip ID:" + tripId);
          return;
        }

        //1. Post message since we know room exists
        Firebase msgPath = msg.push(refMessages.child(tripId).child(roomId));

        //2. Post notifications
        Notification currNoti = new Notification(noti);
        currNoti.setMsgId(msgPath.getKey());
        for (String userId : roomInfo.notifyUsers.keySet()) {
          Firebase refUserRoomNoti = getUserNotificationRoomRef(userId, tripId, roomId);
          currNoti.push(refUserRoomNoti.child("events"));
        }
      }

      @Override
      public void onCancelled(FirebaseError firebaseError) {
        Log.err("Communicator: Request cancelled to get:" + firebaseError.getMessage());
      }
    });
  }

  protected Firebase getRoomReference(String confId, String roomId) {
    return refConferences.child(confId).child("rooms").child(roomId);
  }

  protected Firebase getUserNotificationRoomRef(String userId, String confId, String roomId) {
    return getUserNotificationConfRef(userId, confId).child("rooms").child(roomId);
  }

  protected Firebase getUserNotificationConfRef(String userId, String confId) {
    return refUsers.child(makeUserIdSafe(userId)).child("notifications").child(confId);
  }

  public void testFlightMessages() {
    List<FlightAlertEvent> faes = FlightAlertEvent.find.all();
    for (FlightAlertEvent fae : faes) {
      FlightAlert fa = fae.getFlightAlert();
      processFlightAlert(fa, fae);
    }
  }

  public void processFlightAlert(FlightAlert alert, FlightAlertEvent alertEvent) {
    List<FlightAlertBooking> bookings = FlightAlertBooking.findByFlightAlert(alert);
    final RoomMessage        msg      = new RoomMessage();
    msg.userId = "publisher";
    msg.name = "Flight Notificator";
    msg.timestamp = System.currentTimeMillis();
    StringBuilder msgText = new StringBuilder();

    final Notification noti = new Notification();
    noti.fromUserId = msg.userId;
    noti.fromUserName = msg.name;
    noti.notificationType = NotificationType.ALERT;
    noti.status = RoomMessageStatus.UNREAD;
    noti.timestamp = msg.timestamp;

    switch (alertEvent.alertType) {
      case CANCELLED:
        msgText.append("**Flight Cancelled**\n");
        msgText.append("\n**Please contact your agent**");
        noti.priority = RoomMessageType.CRITICAL;
        break;
      case GATE_ADJUSTMENT:
        msgText.append("**Departure Gate Change**");
        msgText.append("\nDeparting on ");
        msgText.append("**");
        msgText.append(Utils.formatDateTimePrint(alertEvent.departTimestamp));
        msgText.append("** ");

        if (alertEvent.departGate != null && alertEvent.departGate.trim().length() > 0) {
          msgText.append("\n**New Gate: ");
          msgText.append(alertEvent.departGate);
          msgText.append("**");
        }
        msgText.append("\n-- --");
        msgText.append("\nArriving on ");
        msgText.append("**");
        msgText.append(Utils.formatDateTimePrint(alertEvent.arriveTimestamp));
        if (alert.arrivalAirportName != null) {
          msgText.append("\n");
          msgText.append(alert.arrivalAirportName);
        }
        else if (alert.arrivalAirport != null) {
          msgText.append("\n");
          msgText.append(alert.arrivalAirport);
        }
        msgText.append("**");
        noti.priority = RoomMessageType.INFO;
        break;

      case DEPARTURE_DELAY:
        msgText.append("**Flight Delayed**");
        msgText.append("\nDeparting on ");
        msgText.append("**");
        msgText.append(Utils.formatDateTimePrint(alertEvent.departTimestamp));
        msgText.append("** ");

        if (alertEvent.departGate != null && alertEvent.departGate.trim().length() > 0) {
          msgText.append("\n **Gate: ");
          msgText.append(alertEvent.departGate);
          msgText.append("** ");
        }
        msgText.append("\n-- --");
        msgText.append("\nArriving on ");
        msgText.append("**");
        msgText.append(Utils.formatDateTimePrint(alertEvent.arriveTimestamp));
        if (alert.arrivalAirportName != null) {
          msgText.append("\n");
          msgText.append(alert.arrivalAirportName);
        }
        else if (alert.arrivalAirport != null) {
          msgText.append("\n");
          msgText.append(alert.arrivalAirport);
        }
        msgText.append("**");
        noti.priority = RoomMessageType.INFO;
        break;
      case PRE_DEPARTURE_STATUS:
        msgText.append("**Flight Reminder**");
        msgText.append("\nDeparting on ");
        msgText.append("**");
        msgText.append(Utils.formatDateTimePrint(alertEvent.departTimestamp));
        msgText.append("**");
        if (alert.departAirportName != null) {
          msgText.append("\n");
          msgText.append(alert.departAirportName);
        }
        else if (alert.departAirport != null) {
          msgText.append("\n");
          msgText.append(alert.departAirport);
        }
        if (alertEvent.departTerminal != null) {
          msgText.append(" **Terminal: ");
          msgText.append(alertEvent.departTerminal);
          msgText.append("** ");
        }
        if (alertEvent.departGate != null && alertEvent.departGate.trim().length() > 0) {
          msgText.append(" **(Gate: ");
          msgText.append(alertEvent.departGate);
          msgText.append(")** ");
        }
        msgText.append("\n-- --");
        msgText.append("\nArriving on ");
        msgText.append("**");
        msgText.append(Utils.formatDateTimePrint(alertEvent.arriveTimestamp));
        msgText.append("**");
        if (alert.arrivalAirportName != null) {
          msgText.append("\n");
          msgText.append(alert.arrivalAirportName);
        }
        else if (alert.arrivalAirport != null) {
          msgText.append("\n");
          msgText.append(alert.arrivalAirport);
        }
        noti.priority = RoomMessageType.SUCCESS;
        break;
      default:
        msgText.append("**Flight Update**");
        msgText.append("\nDeparting on ");
        msgText.append("**");
        msgText.append(Utils.formatDateTimePrint(alertEvent.departTimestamp));
        msgText.append("**");
        if (alert.departAirportName != null) {
          msgText.append("\n");
          msgText.append(alert.departAirportName);
        }
        else if (alert.departAirport != null) {
          msgText.append("\n");
          msgText.append(alert.departAirport);
        }
        if (alertEvent.departTerminal != null) {
          msgText.append(" **Terminal: ");
          msgText.append(alertEvent.departTerminal);
          msgText.append("** ");

        }
        if (alertEvent.departGate != null && alertEvent.departGate.trim().length() > 0) {
          msgText.append(" **Gate: ");
          msgText.append(alertEvent.departGate);
          msgText.append("** ");

        }
        msgText.append("\n-- --");
        msgText.append("\nArriving on ");
        msgText.append("**");
        msgText.append(Utils.formatDateTimePrint(alertEvent.arriveTimestamp));
        msgText.append("**");
        if (alert.arrivalAirportName != null) {
          msgText.append("\n");
          msgText.append(alert.arrivalAirportName);
        }
        else if (alert.arrivalAirport != null) {
          msgText.append("\n");
          msgText.append(alert.arrivalAirport);
        }
        noti.priority = RoomMessageType.SUCCESS;
        break;
    }
    msg.message = msgText.toString();
    msg.type = noti.priority;

    for (final FlightAlertBooking fab : bookings) {
      if (!MessengerUpdateHelper.isValidMessengerTrip(fab.getTrip())) {
        continue;
      }

      final Firebase refRoomInfo = getRoomReference(fab.getTrip().getTripid(), fab.getTripDetail().getDetailsid());
      refRoomInfo.addListenerForSingleValueEvent(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot snapshot) {

          if (snapshot.getValue() == null) {
            Log.err("Communicator: processFlightAlert: Failed to find room to post notification. Trip ID: " + fab
                .getTrip().getTripid() + " Booking ID:" + fab.getTripDetail().getDetailsid());
            return;
          }

          RoomMetadata roomInfo = snapshot.getValue(RoomMetadata.class);
          if (roomInfo == null) {
            Log.err("Messenger: processFlightAlert(): Failed to send flight notificaiton to invalid trip. Trip ID:" +
                    fab
                .getTrip()
                .getTripid());
            return;
          }

          //1. Post message since we know room exists
          Firebase msgPath = msg.push(refMessages.child(fab.getTrip().getTripid())
                                                 .child(fab.getTripDetail().getDetailsid()));

          //2. Post notifications
          Notification currNoti = new Notification(noti);
          currNoti.setMsgId(msgPath.getKey());
          for (String userId : roomInfo.notifyUsers.keySet()) {
            Firebase refUserRoomNoti = getUserNotificationRoomRef(userId,
                                                                  fab.getTrip().getTripid(),
                                                                  fab.getTripDetail().getDetailsid());
            currNoti.push(refUserRoomNoti.child("events"));
          }
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {
          Log.err("Communicator: Request cancelled to get:" + firebaseError.getMessage());
        }
      });
    }
  }

  public Firebase getUserRef(String userId) {
    return refUsers.child(userId);
  }

  /**
   * Requests room metadata and then requests room users metadata and passes both values to lambda
   *
   * @param confId
   * @param roomId
   * @param roomProcessor lambda with two arguments, first room metadata, second - room users
   */
  public void onConfInfo(String confId, String roomId, BiConsumer<DataSnapshot, DataSnapshot> roomProcessor) {
    Firebase confRef = getRoomReference(confId, roomId);

    confRef.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(final DataSnapshot roomMeta) {
        Firebase roomUsersRef = getRoomUsersReference(confId, roomId);
        roomUsersRef.addListenerForSingleValueEvent(new ValueEventListener() {
          @Override
          public void onDataChange(DataSnapshot roomUsers) {
            roomProcessor.accept(roomMeta, roomUsers);
          }

          @Override
          public void onCancelled(FirebaseError firebaseError) {
            Log.err("Can't access room users. ConfId:" + confId + " RoomId:" + roomId);
          }
        });
      }

      @Override
      public void onCancelled(FirebaseError firebaseError) {
        Log.err("Can't access room. ConfId:" + confId + " RoomId:" + roomId);
      }
    });
  }

  private Firebase getRoomUsersReference(String confId, String roomId) {
    return refRoomUsers.child(confId).child(roomId);
  }

  public Firebase getConferenceReference(String confId) {
    return refConferences.child(confId);
  }

  public void fbDeleteAllData() {
    refConferences.removeValue();
    refMessages.removeValue();
    refUsers.removeValue();
  }

  public Promise promiseUnreadCount(final String userId, final String confId) {
    final Promise<ObjectNode> scalaPromise = Futures.promise();
    Firebase                  userConfRef  = getUserNotificationConfRef(userId, confId);
    userConfRef.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot snapshot) {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode on = mapper.createObjectNode();
        int msgCount = 0;
        int notiCount = 0;

        if(snapshot.exists()) {
          try {
            NotificationConf conf = snapshot.getValue(NotificationConf.class);
            if (conf != null && conf.rooms != null) {
              for (NotificationRoom room : conf.rooms.values()) {
                if (room.events == null) {
                  continue;
                }
                for (Notification n : room.events.values()) {
                  if (n.status != RoomMessageStatus.UNREAD) {
                    continue;
                  }
                  switch (n.notificationType) {
                    case MESSAGE:
                      msgCount++;
                      break;
                    case NEW: //Probably will be gone in the next updates
                      break;
                    default:
                      notiCount++;
                      break;
                  }
                }
              }
            }
          }
          catch (Exception e) {
            Log.err("Error while retrieving list of notifications for user:" + userId + " conf: " + confId, e);
            e.printStackTrace();
          }
        } else {
          Log.err("Communicator: Firebase path does not exist even though requested: " + userConfRef.getPath());
        }
        on.put("msgCount", msgCount);
        on.put("notifyCount", notiCount);
        scalaPromise.success(on);
      }

      @Override
      public void onCancelled(FirebaseError firebaseError) {
        Log.err("No notifications for user: " + userId + " id: " + confId);
        scalaPromise.success(null);
      }
    });
    return scalaPromise;
  }

  public boolean isLeader () {
    if (myStatus != null && myStatus.mode == ServerStatus.ServerMode.LEADER) {
      return true;
    }
    return false;
  }
}
