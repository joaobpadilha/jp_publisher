package com.mapped.publisher.persistence.communicator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.firebase.client.Firebase;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by surge on 2015-07-16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Invite
    extends FirebaseObject {
  /* YAML Spec:
    invites:
    type: object
    properties:
      $inviteId:
        type: object
        constraint: next.id === $inviteId
        properties:
          fromUserId:   {type: string}
          fromUserName: {type: string}
          confId:       {type: string}
          roomId:       {type: string}
        required: [fromUserId, fromUserName, confId, roomId]
   */

  public String fromUserId;
  public String fromUserName;
  public String confId;
  public String roomId;

  public Firebase update(Firebase ref) {
    Map<String, Object> uVals = new HashMap<>();
    uVals.put("fromUserId", fromUserId);
    uVals.put("fromUserName", fromUserName);
    uVals.put("confId", confId);
    uVals.put("roomId", roomId);
    ref.updateChildren(uVals);
    return ref;
  }
}
