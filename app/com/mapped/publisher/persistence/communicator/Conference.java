package com.mapped.publisher.persistence.communicator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.firebase.client.Firebase;
import com.mapped.publisher.utils.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Conference is a collection of chat rooms
 * <p/>
 * Created by surge on 2015-07-14.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Conference
    extends FirebaseObject {
  /* YAML Spec

      $confId:
      type: object
      properties:
        id:       {type: string}
        name:     {type: string}
        #External Identifier: (UMAPPED: Associated Trip ID)
        extId:    {type: string}
        rooms:
          type: object
          ...
   */


  public String id;
  public String name;

  public Firebase update(Firebase ref) {
    Map<String, Object> uVals = new HashMap<>();
    uVals.put("id", id);
    uVals.put("name",name);
    ref.updateChildren(uVals);
    Log.debug("Firebase requested record update: " + ref.getPath().toString());
    return ref;
  }
}
