package com.mapped.publisher.persistence;

import com.mapped.publisher.utils.Utils;

import java.util.*;

/**
 * Information about countries. Once initialized during framework startup,
 * contents are no longer updated.
 * <p/>
 * Created by surge on 2014-10-20.
 */
public class CountriesInfo {

  private boolean locked;
  /** Search tree used to find countries by english name */
  private Map<String, Country> byName;
  /** Search tree used to find countries by Alpha-2 code */
  private Map<String, Country> byA2Code;
  /** Search tree used to find a country by Alpha-3 code */
  private Map<String, Country> byA3Code;
  /** Sorted countries */
  private List<Country> sortedCountries;

  public static enum LangCode {
    CS,
    DE,
    EN,
    ES,
    FR,
    IT,
    NL,
    RU
  }

  //Bill Pugh's Java Singleton Approach, most efficient Java singleton
  private static class CountriesInfoHelper {
    private static final CountriesInfo INSTANCE = new CountriesInfo();
  }

  public static class Country {
    /**
     * Map of  language code (EN, DE, etc) to Country name in the language
     */
    private List<String> names;
    private int code;
    private String alpha2;
    private String alpha3;

    public Country(String alpha2, String alpha3, List<String> names) {
      this.alpha2 = alpha2;
      this.alpha3 = alpha3;
      this.names = names;
    }

    public int getCode() {
      return code;
    }

    public String getAlpha2() {
      return alpha2;
    }

    public String getAlpha3() {
      return alpha3;
    }

    /**
     * Canonical name for the country
     * @param langCode
     * @return
     */
    public String getName(LangCode langCode) {

      return names.get(0);
    }
  }

  private CountriesInfo() {
    byName = new TreeMap<>(); //Tree map is the most efficient for text search
    byA2Code = new TreeMap<>();
    byA3Code = new TreeMap<>();
    locked = false;
  }

  public static CountriesInfo Instance() {
    return CountriesInfoHelper.INSTANCE;
  }


  public void addCountry(String alpha2, String alpha3, List<String> names) {
    if (locked) {
      throw new UnsupportedOperationException();
    }

    Country newCountry = new Country(alpha2, alpha3, names);
    for (String c : names) {
      byName.put(c, newCountry);
    }
    if (alpha2 != null) {
      byA2Code.put(alpha2.toLowerCase(), newCountry);
    }
    byA3Code.put(alpha3.toLowerCase(), newCountry);
  }

  /**
   * One time lock - once locked no more countries can be added (to protect internal data structures and make
   * operations completely thread safe)
   */
  public void lock(){
    this.locked = true;
  }

  /**
   * Any system supported country name
   * @param name
   * @return
   */
  public Country searchByName(String name) {
    if (name == null) {
      return null;
    }

    String cleanName = Utils.curlyQuotesToAscii(name).toLowerCase();

    Country result = byName.get(cleanName);

    //May be someone already using Alpha 2 code?
    if (result == null) {
      result = byAlpha2(cleanName);
    }

    //May be still someone is using Alpha 3 code?
    if (result == null) {
      result = byAlpha3(cleanName);
    }

    //If result is null, let's try very very slow method of searching substring
    if (result == null) {
      for(String n : byName.keySet()){
        if (n.toLowerCase().contains(cleanName)) {
          return byName.get(n);
        }
      }
    }

    return result;
  }

  public Country byAlpha2(String code) {
    if (code == null)
      return null;

    return byA2Code.get(code.toLowerCase());
  }

  public Country byAlpha3(String code) {
    if (code == null)
      return null;

    return byA3Code.get(code.toLowerCase());
  }

  /**
   * Helper (may be a better idea is to provide iterator over countries... )
   * @return
   */
  public Map<String, Country> getAlpha3CountryMap(){
    return byA3Code;
  }

  /**
   * Helper (may be a better idea is to provide iterator over countries... )
   * @return
   */
  public List<Country> getNameSortedCountryList(){
    if (sortedCountries == null) {
      sortedCountries = new ArrayList<>();
      sortedCountries.addAll(byA3Code.values());
      Collections.sort(sortedCountries, new Comparator<Country>() {
        @Override public int compare(Country o1, Country o2) {
          return o1.getName(LangCode.EN).compareTo(o2.getName(LangCode.EN));
        }
      });
    }
    return sortedCountries;
  }

}
