package com.mapped.publisher.persistence;


import com.avaje.ebean.Ebean;
import com.avaje.ebeaninternal.server.lib.util.Str;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.audit.AuditActionType;
import com.mapped.publisher.audit.AuditActorType;
import com.mapped.publisher.audit.AuditModuleType;
import com.mapped.publisher.audit.event.*;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.mapped.publisher.parse.schemaorg.ImageObject;
import com.mapped.publisher.parse.schemaorg.Place;
import com.mapped.publisher.parse.valueObject.DayDetails;
import com.mapped.publisher.persistence.communicator.MessengerUpdateHelper;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.UmPostalAddress;
import com.umapped.persistence.reservation.UmRate;
import com.umapped.persistence.reservation.UmReservation;
import com.umapped.persistence.reservation.UmTraveler;
import com.umapped.persistence.reservation.accommodation.UmAccommodationReservation;
import com.umapped.persistence.reservation.accommodation.UmAccommodationTraveler;
import com.umapped.persistence.reservation.accommodation.UmLodgingBusiness;
import com.umapped.persistence.reservation.activity.UmActivityReservation;
import com.umapped.persistence.reservation.cruise.UmCruiseReservation;
import com.umapped.persistence.reservation.cruise.UmCruiseTraveler;
import com.umapped.persistence.reservation.enums.UmRateType;
import com.umapped.persistence.reservation.flight.UmFlight;
import com.umapped.persistence.reservation.flight.UmFlightReservation;
import com.umapped.persistence.reservation.transfer.UmTransferReservation;

import controllers.BookingController;
import controllers.BookingNoteController;
import controllers.ImageController;
import controllers.PoiController;
import models.publisher.*;
import models.publisher.utils.reservation.migration.UmAccommodationReservationBuilder;
import models.publisher.utils.reservation.migration.UmFlightReservationBuilder;
import org.apache.commons.lang3.StringUtils;
import scala.concurrent.java8.FuturesConvertersImpl;
import views.html.trip.recentActiveTrips;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Helper class to work with Value Objects extracted from the document related Models in the database.
 * @note Represents a single trip
 * Created by surge on 2014-06-20.
 */
public class VOModeller {
  private List<TripVO> tripVObjects;
  private Trip trip = null;
  private Company tripCompany = null;
  private UmAccommodationReservationBuilder accommodationBuilder = new UmAccommodationReservationBuilder(); 
  /**
   * Flag to indicate if we are creating a new trip
   */
  private boolean isNewTrip;
  /**
   * Account of the entity (system, agent, traveller) submitting the document
   */
  private Account account;

  private HashMap<TripNote, NoteVO> tripNoteImages;
  private List<TripNote> notes;

  private List<TripDetail>  details;
  private List<TripDetail>  flights;
  private List<TripDetail>  hotels;
  private List<TripDetail>  cruises;
  private List<TripDetail>  transports;
  private List<TripDetail>  activities;

  Map<String, TripDetail> detailsByOrigSrcPK;

  private long startTimestamp = 0;
  private long endTimestamp = 0;

  private List<String> untrackFlights;

  private boolean generateNotes = true;

  /**
   * Source File that was used to generate TVO object
   */
  private List<TripAttachment> attachments = null;

  long currTimestamp = 0;

  AuditActorType actorType = AuditActorType.WEB_USER;

  private BookingSrc.ImportSrc importSrc = null;
  private String importSrcId = null;
  private long importTS = 0;
  private boolean noPOIMatch = false;

  /**
   * @param account   Account record  of the user who is requesting file parsing
   * @param isNewTrip Whether or not trip is new or already present in the system
   */
  public VOModeller(Trip trip, Account account, boolean isNewTrip) {
    initModeller(trip, account, isNewTrip);
  }

  private void initModeller(Trip trip, Account account, boolean isNewTrip) {
    this.trip = trip;
    this.isNewTrip = isNewTrip;
    this.account = account;

    tripCompany = Company.find.byId(trip.getCmpyid());
    tripVObjects = new ArrayList<>();
    details = new ArrayList<>();
    flights = new ArrayList<>();
    hotels = new ArrayList<>();
    cruises = new ArrayList<>();
    transports = new ArrayList<>();
    activities = new ArrayList<>();
    notes = new ArrayList<>();
    attachments = new ArrayList<>();
    currTimestamp = System.currentTimeMillis();
    startTimestamp = currTimestamp;
    endTimestamp = currTimestamp;
    untrackFlights = new ArrayList<>();
    detailsByOrigSrcPK = new HashMap<>();
    tripNoteImages = new HashMap<>();
  }


  /**
   * @param userProfile Profile of the user who is requesting file parsing
   * @param isNewTrip   Whether or not trip is new or already present in the system
   */
  public VOModeller(Trip trip, UserProfile userProfile, boolean isNewTrip) {
    Account a = Account.findActiveByLegacyId(userProfile.getUserid());
    initModeller(trip, a, isNewTrip);
  }

  /**
   * @return number of booking models
   */
  public int getBookingsCount() {
    return flights.size() + cruises.size() + hotels.size() + transports.size() + activities.size() + notes.size();
  }

  public List<TripDetail> getFlights() {
    return flights;
  }

  public List<TripNote> getNotes() {
    return notes;
  }

  public List<TripDetail> getHotels() {
    return hotels;
  }

  public List<TripDetail> getCruises() {
    return cruises;
  }
  
  public List<TripDetail> getTransports() {
    return transports;
  }

  public List<TripDetail> getActivities() {
    return activities;
  }

  public void addAttachment(String fileName, String sysFileName, String fileType, String url) {
    TripAttachment attachment = null;

    if (!isNewTrip) {
      //check to see if a file with already exists
      List<TripAttachment> tripAttachments = TripAttachment.findByTripIdFileName(trip.tripid,
                                                                                 fileName.trim());
      if (tripAttachments != null && tripAttachments.size() > 0) {
        attachment = tripAttachments.get(0);
      }
    }

    if (attachment == null) {
      attachment = new TripAttachment();
      attachment.setCreatedby(account.getLoggingId());
      attachment.setCreatedtimestamp(currTimestamp);
      attachment.setPk(DBConnectionMgr.getUniqueId());
      attachment.setStatus(APPConstants.STATUS_ACTIVE);
      attachment.setTripid(trip.tripid);
    }

    attachment.setFilename(sysFileName);
    attachment.setFiletype(fileType);
    attachment.setFileurl(url);
    attachment.setLastupdatedtimestamp(currTimestamp);
    attachment.setModifiedby(account.getLoggingId());
    attachment.setOrigfilename(fileName);

    attachments.add(attachment);
  }

  public void addAttachment(TripAttachment tripAttachment) {
    tripAttachment.setStatus(APPConstants.STATUS_ACTIVE);
    tripAttachment.setModifiedby(account.getLoggingId());
    tripAttachment.setLastupdatedtimestamp(System.currentTimeMillis());
    attachments.add(tripAttachment);
  }

  public void buildTripVOModels(TripVO tripVO) {
    tripVObjects.add(tripVO);
    this.noPOIMatch = tripVO.isNoPOIMatch();

    if (tripVO.getImportSrc() != null) {
      importSrc = tripVO.getImportSrc();
      importSrcId = tripVO.getImportSrcId();
      importTS = tripVO.getImportTs();
    }

    //try to find the earliest and latest timestamp
    //only initialize once

    if (startTimestamp == currTimestamp) {
      startTimestamp = trip.getStarttimestamp().longValue();
      if(isNewTrip || trip.getStarttimestamp().longValue() == trip.getCreatedtimestamp().longValue()) {
        startTimestamp = 0;
      }
    }

    if (endTimestamp == currTimestamp) {
      endTimestamp = trip.getEndtimestamp().longValue();
      if (isNewTrip || trip.getEndtimestamp().longValue() == trip.getCreatedtimestamp().longValue()) {
        endTimestamp = 0;
      }
    }

    for (FlightVO fvo : tripVO.getFlights()) {
      if (fvo.getDepatureTime() != null &&
          (fvo.getDepatureTime().getTime() < startTimestamp || startTimestamp == 0)) {
        startTimestamp = fvo.getDepatureTime().getTime();
      }

      if (fvo.getArrivalTime() != null && fvo.getArrivalTime().getTime() > endTimestamp) {
        endTimestamp = fvo.getArrivalTime().getTime();
      } else if (fvo.getDepatureTime() != null && fvo.getDepatureTime().getTime() > endTimestamp) {
        endTimestamp = fvo.getDepatureTime().getTime();
      }
      addFlight(fvo);
    }

    for (HotelVO hvo : tripVO.getHotels()) {
      if (hvo.getCheckin() != null && (hvo.getCheckin().getTime() < startTimestamp  || startTimestamp == 0)) {
        startTimestamp = hvo.getCheckin().getTime();
      }

      if (hvo.getCheckout() != null && hvo.getCheckout().getTime() > endTimestamp) {
        endTimestamp = hvo.getCheckout().getTime();
      } else if (hvo.getCheckin() != null && hvo.getCheckin().getTime() > endTimestamp) {
        endTimestamp = hvo.getCheckin().getTime();
      }
      addHotel(hvo);
    }

    for (CruiseVO cvo: tripVO.getCruises()) {
      if (cvo.getTimestampDepart() != null &&
          (cvo.getTimestampDepart().getTime() < startTimestamp || startTimestamp == 0)) {
        startTimestamp = cvo.getTimestampDepart().getTime();
      }

      if (cvo.getTimestampArrive() != null && cvo.getTimestampArrive().getTime() > endTimestamp) {
        endTimestamp = cvo.getTimestampArrive().getTime();
      } else if (cvo.getTimestampDepart() != null && cvo.getTimestampDepart().getTime() > endTimestamp) {
        endTimestamp = cvo.getTimestampDepart().getTime();
      }
      addCruise(cvo);
    }

    for (TransportVO tvo : tripVO.getTransport()) {
      if (tvo.getPickupDate() != null && (tvo.getPickupDate().getTime() < startTimestamp || startTimestamp == 0)) {
        startTimestamp = tvo.getPickupDate().getTime();
      }

      if (tvo.getDropoffDate() != null && tvo.getDropoffDate().getTime() > endTimestamp) {
        endTimestamp = tvo.getDropoffDate().getTime();
      } else if (tvo.getPickupDate() != null && tvo.getPickupDate().getTime() > endTimestamp) {
        endTimestamp = tvo.getPickupDate().getTime();
      }

      addTransport(tvo);
    }

    for (ActivityVO avo : tripVO.getActivities()) {
      if (avo.getStartDate() != null &&
          (avo.getStartDate().getTime() < startTimestamp || startTimestamp == 0)) {
        startTimestamp = avo.getStartDate().getTime();
      }

      if (avo.getEndDate() != null && avo.getEndDate().getTime() > endTimestamp) {
        endTimestamp = avo.getEndDate().getTime();
      } else if (avo.getStartDate() != null && avo.getStartDate().getTime() > endTimestamp) {
        endTimestamp = avo.getStartDate().getTime();
      }

      addActivity(avo);
    }

    for (NoteVO note: tripVO.getNotes()) {
      addNote(note);

    }

    startTimestamp = truncateTimeToDay(startTimestamp);
    endTimestamp = truncateTimeToDay(endTimestamp);
  }

  /**
   * Truncate the timestamp to Day
   * @param ts
   * @return
   */
  private long truncateTimeToDay(long ts) {
    Instant instant = Instant.ofEpochMilli(ts);
    Instant day = instant.truncatedTo(ChronoUnit.DAYS);
    return day.toEpochMilli();
  }

  /**
   * Persists all models to the database
   */
  public void saveAllModels() throws Exception{
    try {
      Ebean.beginTransaction();
      if (startTimestamp > 0 && endTimestamp > 0) {
        try {
          if (isNewTrip) {
            trip.setStarttimestamp(startTimestamp);
            trip.setEndtimestamp(endTimestamp);
            trip.setModifiedby(account.getLoggingId());
            trip.setLastupdatedtimestamp(currTimestamp);
            trip.save();
            //update the trip dates if needed
          }
          else if ((startTimestamp < trip.getStarttimestamp().longValue() ||
                  endTimestamp > trip.getEndtimestamp().longValue()) && startTimestamp != endTimestamp) {
            Trip t = Trip.findByPK(trip.tripid);

            if (startTimestamp < trip.getStarttimestamp().longValue()) {
              t.setStarttimestamp(startTimestamp);
            }
            if (endTimestamp > trip.getEndtimestamp().longValue()) {
              t.setEndtimestamp(endTimestamp);
            }
            t.setModifiedby(account.getLoggingId());
            t.setLastupdatedtimestamp(currTimestamp);
            t.update();
          }
        }
        catch (Exception e) {
          e.printStackTrace();
        }
      }

      for (TripDetail entry : flights) {
        try {
          entry.save();
          if (entry.createdtimestamp != null &&
                  !entry.createdtimestamp.equals(entry.lastupdatedtimestamp)) {
            BookingController.invalidateCache(trip.getTripid(), entry.getDetailsid());
          }
          auditFlight(entry);
        } catch (Exception e) {
          Log.err("VOModeler - failed to save flight Id: " + entry.getDetailsid(), e);

        }
      }

      for (TripDetail entry : hotels) {
        try {
          entry.save();
          if (entry.createdtimestamp != null &&
                  !entry.createdtimestamp.equals(entry.lastupdatedtimestamp)) {
            BookingController.invalidateCache(trip.getTripid(), entry.getDetailsid());
          }
          auditHotel(entry);
        } catch (Exception e) {
          Log.err("VOModeler - failed to save hotel Id: " + entry.getDetailsid(), e);

        }
      }

      for (TripDetail entry : cruises) {
        try {
          entry.save();
          if (entry.getCreatedtimestamp() != null &&
                  !entry.getCreatedtimestamp().equals(entry.getLastupdatedtimestamp())) {
            BookingController.invalidateCache(trip.getTripid(), entry.getDetailsid());
          }
          auditCruise(entry);
        } catch (Exception e) {
          Log.err("VOModeler - failed to save cruise Id: " + entry.getDetailsid(), e);

        }
      }

      for (TripDetail entry : transports) {
        try {
          entry.save();
          if (entry.createdtimestamp != null &&
                  !entry.createdtimestamp.equals(entry.lastupdatedtimestamp)) {
            BookingController.invalidateCache(trip.getTripid(), entry.getDetailsid());
          }
          auditTransport(entry);
        } catch (Exception e) {
          Log.err("VOModeler - failed to save transport Id: " + entry.getDetailsid(), e);

        }
      }

      for (TripDetail entry : activities) {
        try {
          entry.save();
          if (entry.createdtimestamp != null &&
                  !entry.createdtimestamp.equals(entry.lastupdatedtimestamp)) {
            BookingController.invalidateCache(trip.getTripid(), entry.getDetailsid());
          }
          auditActivity(entry);
        } catch (Exception e) {
          Log.err("VOModeler - failed to save activity Id: " + entry.getDetailsid(), e);

        }
      }

      for (TripAttachment attachment : attachments) {
        try {
          attachment.save();
        } catch (Exception e) {
          Log.err("VOModeler - failed to save attachment Id: " + attachment.getPk(), e);
        }
      }

      for (TripNote note : notes) {
        try {
          //see if we need to link notes to a trip detail... when note origsrcpk matches the tripdetail origsrcpk
          if (note.getImportSrcId() != null && !note.getImportSrcId().isEmpty() && note.getTripDetailId() == null && !detailsByOrigSrcPK.isEmpty()) {
            for (String origPk: detailsByOrigSrcPK.keySet()) {
              if (origPk.equals(note.getImportSrcId())) {
                note.setTripDetailId(detailsByOrigSrcPK.get(origPk).getDetailsid());
                break;
              }
            }

          }
          note.save();
          auditNote(note);
        } catch (Exception e) {
          Log.err("VOModeler - failed to save note - NoteId: " + note.getNoteId() + " TripId: " + trip.getTripid(), e);
        }
      }
      if (!notes.isEmpty()) {
        BookingNoteController.invalidateCache(trip.tripid);
      }

      Ebean.commitTransaction();
      //save any attachments for notes
      if (notes != null) {
        for (TripNote note: notes) {
          if (tripNoteImages.containsKey(note)) {
            NoteVO noteVO = tripNoteImages.get(note);
            if (noteVO.getImages() != null) {
              List <TripNoteAttach> attach = TripNoteAttach.findByNoteId(note.getNoteId());
              List<String> imgUrls = new ArrayList<>();
              if (attach != null) {
                for (TripNoteAttach a : attach) {
                  if (a.getAttachType() == PageAttachType.PHOTO_LINK && a.getStatus() == 0 && a.getImage() != null && a.getImage().getSrcUrl() != null) {
                    imgUrls.add(a.getImage().getSrcUrl());
                  }
                }
              }
              for (ImageObject io: noteVO.getImages()) {
                if (io.contentUrl != null && !io.contentUrl.isEmpty() && !imgUrls.contains(io.contentUrl)) {
                  String imgUrl = io.contentUrl.trim();
                  try {
                    FileImage image = FileImage.bySourceUrl(imgUrl);
                    if (image == null) {
                      String fileName = io.name;
                      if (fileName == null || fileName.isEmpty()) {
                        fileName = imgUrl.substring(imgUrl.lastIndexOf("/") + 1);
                        if (fileName.indexOf("?") > 1) {
                          fileName = fileName.substring(0, fileName.indexOf("?"));
                        }
                      }
                      image = ImageController.downloadAndSaveImage(imgUrl,
                              fileName,
                              FileSrc.FileSrcType.IMG_USER_DOWNLOAD,
                              null,
                              account,
                              false);

                    }
                    if (image != null) {
                      TripNoteAttach ta = TripNoteAttach.build(note, account.getLoggingId());
                      ta.setName(image.getFilename());
                      ta.setImage(image);
                      ta.setAttachName(image.getFilename());
                      ta.setAttachType(PageAttachType.PHOTO_LINK);
                      if (io.caption != null) {
                        ta.setComments(io.caption);
                      }
                      ta.save();
                    }
                  } catch (Exception e) {
                    Log.err("TripPublisherHelper:addCover - cannot download image for trip: " + trip.getTripid() + " url: " + imgUrl);
                  }

                }
              }
            }

          }
        }
      }

      List<TripDetail> tds = new ArrayList<>();
      tds.addAll(hotels);
      tds.addAll(flights);
      tds.addAll(cruises);
      tds.addAll(transports);
      tds.addAll(activities);
      MessengerUpdateHelper.build(trip, account).addRoomsToUpdate(tds).update();

      //let's untrack any flights that have changes
      if (untrackFlights != null && untrackFlights.size() > 0) {
       for (String detailsId: untrackFlights) {
         FlightAlertBooking.deleteByBookingId(detailsId);
       }
      }
    } catch (Exception e) {
      e.printStackTrace();
      Ebean.rollbackTransaction();
      Log.err("VOModeler - failed to save models", e);
      throw e;
    }
  }

  public void setActorType(AuditActorType actorType) {
    this.actorType = actorType;
  }

  private void auditFlight(TripDetail tripDetail){
    if(!account.isTraveler()) {
      TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_BOOKING_FLIGHT,
                                           (tripDetail.version == 0) ? AuditActionType.ADD : AuditActionType.MODIFY,
                                           actorType)
                              .withTrip(trip).withCmpyid(trip.cmpyid)
                              .withUserid(account.getLegacyId());
      UmFlightReservation reservation = cast(tripDetail.getReservation(), UmFlightReservation.class);
      String flightId = reservation != null ? reservation.getFlight().flightNumber : "";
      ((AuditTripBookingFlight) ta.getDetails()).withFlightNumber(flightId)
                                                .withBookingId(tripDetail.getDetailsid())
                                                .withDeparture(tripDetail.getStarttimestamp());
      ta.save();
    }
  }

  private void auditHotel(TripDetail tripDetail) {
    if(!account.isTraveler()) {
      TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_BOOKING_HOTEL,
                                           (tripDetail.version == 0) ? AuditActionType.ADD : AuditActionType.MODIFY,
                                           actorType).withTrip(trip)
                              .withCmpyid(trip.cmpyid)
                              .withUserid(account.getLegacyId());

      UmAccommodationReservation reservation = cast(tripDetail.getReservation(), UmAccommodationReservation.class);
      String hotelName = reservation.getAccommodation().name;
      ((AuditTripBookingHotel) ta.getDetails()).withBookingId(tripDetail.getDetailsid())
                                               .withName(hotelName)
                                               .withCheckIn(tripDetail.getStarttimestamp())
                                               .withCheckOut(tripDetail.getEndtimestamp());
      ta.save();
    }
  }

  private void auditCruise(TripDetail tripDetail) {
    if(!account.isTraveler()) {
      TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_BOOKING_CRUISE,
                                           (tripDetail.version == 0) ? AuditActionType.ADD : AuditActionType.MODIFY,
                                           actorType)
                              .withTrip(trip)
                              .withCmpyid(trip.cmpyid)
                              .withUserid(account.getLegacyId());

      UmCruiseReservation reservation = cast(tripDetail.getReservation(), UmCruiseReservation.class);
      String name = reservation != null ? reservation.getCruise().name : "";
      
      ((AuditTripBookingCruise) ta.getDetails()).withBookingId(tripDetail.getDetailsid())
                                                .withName(name)
                                                .withDepartDT(tripDetail.getStarttimestamp())
                                                .withArriveDT(tripDetail.getEndtimestamp());
      ta.save();
    }
  }

  private void auditTransport(TripDetail tripDetail) {
    if(!account.isTraveler()) {
      TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_BOOKING_TRANSPORT,
                                           (tripDetail.version == 0) ? AuditActionType.ADD : AuditActionType.MODIFY,
                                           actorType)
                              .withTrip(trip).withCmpyid(trip.cmpyid)
                              .withUserid(account.getLegacyId());

      UmTransferReservation reservation = cast(tripDetail.getReservation(), UmTransferReservation.class);
      String name = reservation != null ? reservation.getTransfer().getProvider().name : "";
      ((AuditTripBookingTransport) ta.getDetails()).withBookingId(tripDetail.getDetailsid())
                                                   .withName(name)
                                                   .withStartTime(tripDetail.getStarttimestamp());
      ta.updateAuditRecord();
      ta.save();
    }
  }

  private void auditActivity(TripDetail tripDetail) {
    if(!account.isTraveler()) {
      TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_BOOKING_ACTIVITY,
                                           (tripDetail.version == 0) ? AuditActionType.ADD : AuditActionType.MODIFY,
                                           actorType)
                              .withTrip(trip).withCmpyid(trip.cmpyid).withUserid(account.getLegacyId());

      UmActivityReservation reservation = cast(tripDetail.getReservation(), UmActivityReservation.class);
      String name = reservation != null ? reservation.getActivity().name : "";
      ((AuditTripBookingActivity) ta.getDetails()).withBookingId(tripDetail.getDetailsid())
                                                  .withName(name)
                                                  .withStartTime(tripDetail.getStarttimestamp());
      ta.updateAuditRecord();
      ta.save();
    }
  }

  private void auditNote (TripNote tripNote) {
    try {
      audit:
      {

        if (trip != null) {
          TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_BOOKING_NOTE,
                                               (tripNote.getVersion() == 0) ? AuditActionType.ADD : AuditActionType.MODIFY,
                                               AuditActorType.WEB_USER)
                                  .withTrip(trip)
                                  .withCmpyid(trip.getCmpyid())
                                  .withUserid(account.getLegacyId());

          ((AuditTripDocCustom) ta.getDetails()).withGuideId(String.valueOf(tripNote.getNoteId()))
                                                .withName(tripNote.getName())
                                                .withCoverImage("")
                                                .withText("Note: " + tripNote.getName());
          ta.save();
        } else {
          Log.log(LogLevel.ERROR, "Trip ID can not be determined while adding new noteaudit:" + tripNote.getNoteId());
        }
      }
    } catch (Exception e) {

    }
  }

  private void addNote(NoteVO bvo) {
    TripNote tn = null;

    if (bvo.getId() != null) {
      try {
        Long nid = Long.parseLong(bvo.getId());
        tn = TripNote.find.byId(nid);
      }catch (Exception e){} //Ignoring on purpose
    }

    if (tn == null && !isNewTrip) {
      long ts = 0;
      if (bvo.getTimestamp() != null) {
        ts = bvo.getTimestamp().getTime();
      }
      if (bvo.getOrigSrcPK() != null &&
               !bvo.getOrigSrcPK().isEmpty() && bvo.getSrc() != null) {
      /*
        //check to see if this was deleted on purpose by someone else
        List<TripNote> deleted = TripNote.getDeleteNotesByTripIdImportSrc(trip.tripid, bvo.getOrigSrcPK(), account.getLoggingId());
        if (deleted != null && deleted.size() > 0) {
          //this was explicitly deleted by someone else - let's not add it again
          return;
        }
      */
        List<String> ids = new ArrayList<>();
        ids.add(bvo.getOrigSrcPK());
        List<TripNote> notes = TripNote.getNotesByTripIdImportSrc(trip.tripid, ids);

        if (notes != null && notes.size() > 0) {
          if (notes.size() == 1) {
            tn = notes.get(0);
          } else { //try to match name
            for (TripNote note: notes) {
              if (note.getName() != null && bvo.getName() != null && note.getName().compareToIgnoreCase(bvo.getName()) == 0) {
                tn = note;
                break;
              }
            }
            if (tn == null) {
              tn = notes.get(0);
            }
          }
        }

      } else {
        List<TripNote> noteList = TripNote.getNotesByTripIdName(trip.tripid, bvo.getName(), ts);
        if (noteList != null && noteList.size() == 1 &&
                (noteList.get(0).getImportSrcId() == null || noteList.get(0).getImportSrcId().isEmpty()) &&
                (bvo.getOrigSrcPK() == null || bvo.getOrigSrcPK().isEmpty())) {
          tn = noteList.get(0);
        } else if (noteList != null && noteList.size() == 1) {
          tn = noteList.get(0);
        }
      }
    }

    if (tn == null) {
      tn = new TripNote();
      tn.setNoteId(DBConnectionMgr.getUniqueLongId());
      tn.setTrip(trip);
      tn.setCreatedBy(account.getLoggingId());
      tn.setCreatedTimestamp(System.currentTimeMillis());
    }

    //Trying to get place information if we can
    Place place = bvo.getPlace();
    if(place != null) {
      if(place.address != null) {
        tn.setStreetAddr(place.address.streetAddress);
        tn.setCity(place.address.addressLocality);
        tn.setCountry(place.address.addressCountry);
        tn.setZipcode(place.address.postalCode);
        tn.setState(place.address.addressRegion);
        tn.setLandmark(place.name);
      }

      if(place.geo != null && place.geo.latitude != null && place.geo.longitude != null) {
        try {
          float lat = Float.parseFloat(place.geo.latitude);
          float lng = Float.parseFloat(place.geo.longitude);
          tn.setLocLong(lng);
          tn.setLocLat(lat);
        } catch (Exception e) {} //Ignored because we don't care if object was empty
      }
    }


    tn.setName(bvo.getName());
    tn.setIntro(bvo.getNote());
    if (bvo.getTripDetailId() != null){
      tn.setTripDetailId(bvo.getTripDetailId());
    }
    if (bvo.getRank() > -1) {
      tn.setRank(bvo.getRank());
    }
    if (bvo.getTimestamp() != null) {
      tn.setNoteTimestamp(bvo.getTimestamp().getTime());
    }

    if (importSrc != null || importSrcId != null) {
      tn.setImportSrc(importSrc);
      tn.setImportSrcId(importSrcId);
      tn.setImportTs(importTS);
    }
    if (bvo.getOrigSrcPK() != null) {
      tn.setImportSrcId(bvo.getOrigSrcPK());
    }
    if (bvo.getCity() != null) {
      tn.setCity(bvo.getCity());
    }
    if (bvo.getState() != null) {
      tn.setState(bvo.getState());
    }
    if (bvo.getCountry() != null) {
      tn.setCountry(bvo.getCountry());
    }
    if (bvo.getZipcode() != null) {
      tn.setZipcode(bvo.getZipcode());
    }
    if (bvo.getLandmark() != null) {
      tn.setLandmark(bvo.getLandmark());
    }
    if (bvo.getTag() != null) {
      tn.setTag(bvo.getTag());
    }
    tn.setLocLat(bvo.getLocLat());
    tn.setLocLong(bvo.getLocLong());

    tn.setModifiedBy(account.getLoggingId());
    tn.setLastUpdatedTimestamp(System.currentTimeMillis());
    notes.add(tn);

    if (bvo.getImages() != null && !bvo.getImages().isEmpty()) {
      tripNoteImages.put(tn, bvo); //save this reference so we can add the images when we save the trip note
    }
  }

  private void addCruise(CruiseVO cvo) {
    TripDetail tripDetail = null;

    if (cvo.getTimestampDepart() == null) {
      return;
    }

    if (cvo.getId() != null) {
      try {
        tripDetail = TripDetail.find.byId(cvo.getId());
      }catch (Exception e){} //Ignoring on purpose
    }

    //If existing trip, trying to find similar record based on tripId and departure time
    if (tripDetail == null && !isNewTrip) {

      //try to find an existing match
      List<TripDetail> tripDetails = null;
      if (cvo.getSrcId() > 0 ) {
        try {
          tripDetails = TripDetail.findActiveByTripIdandSrcID(trip.getTripid(), cvo.getSrcId(), ReservationType
              .CRUISE);
        } catch (Exception e) {

        }
      }
      if (tripDetails == null || tripDetails.size() == 0) {
        if (cvo.getConfirmationNo() != null && !cvo.getConfirmationNo().isEmpty()) {
          tripDetails = TripDetail.findActiveByTripIdAndConfirmation(trip.tripid,
                                                                     cvo.getConfirmationNo(),
                                                                     ReservationType.CRUISE);
        }
        else {
          tripDetails = TripDetail.findActiveByStartTimestampAndType(trip.tripid,
                                                                     cvo.getTimestampDepart().getTime(),
                                                                     ReservationType.CRUISE);
        }
      }

      if (tripDetails != null && tripDetails.size() == 1) {
        tripDetail = tripDetails.get(0);
      }
    }

    //If no previous records were found, creating new ones
    if (tripDetail == null) {
      tripDetail = TripDetail.build(account.getLoggingId(), trip.getTripid(), ReservationType.CRUISE);
      if (cvo.getRank() > -1) {
        tripDetail.setRank(cvo.getRank());
      }
    }

    UmCruiseReservation reservation = cast(tripDetail.getReservation(), UmCruiseReservation.class);
    if (reservation == null) {
      reservation = new UmCruiseReservation();
      tripDetail.setReservation(reservation);
    }

    populateRate(reservation, cvo);

    reservation.setCancellationPolicy(cvo.getCancellationPolicy());

    if (importSrc != null) {
      tripDetail.setImportSrc(importSrc);
      tripDetail.setImportSrcId(importSrcId);
      tripDetail.setImportTs(importTS);
    }

    tripDetail.setLastupdatedtimestamp(currTimestamp);
    tripDetail.setModifiedby(account.getLoggingId());

    tripDetail.setStartTimestamp(cvo.getTimestampDepart());
    tripDetail.setEndTimestamp(cvo.getTimestampArrive());

    tripDetail.setBookingnumber(cvo.getConfirmationNo());
   // tripDetail.setRecordLocator(cvo.getRecordLocator());
    //TODO: Serguei: Currently not implemented tripDetail.setPassengerCount();
    if (generateNotes) {
      reservation.setNotesPlainText(cvo.getFullNotes());
    } else {
      reservation.setNotesPlainText(cvo.getNote());
    }

    if (cvo.getSrc() != null && cvo.getSrcId() > 0) {
      tripDetail.setBkApiSrcId(cvo.getSrcId());
      tripDetail.setImportSrcId(cvo.getSrc());
    }
    if (cvo.getRecordLocator() != null) {
      tripDetail.setRecordLocator(cvo.getRecordLocator());
    }

    UmCruiseTraveler traveler = reservation.getCruiseTraveler();
    
    traveler.setBedding(cvo.getBedding());
    traveler.setCabinNumber(cvo.getCabinNumber());
    traveler.setCategory(cvo.getCategory());
    traveler.setDeckNumber(cvo.getDeck());
    traveler.setDiningPlan(cvo.getMeal());

    reservation.getCruise().getProvider().name = cvo.getCompanyName();

    //Dealing with Cruise POIs
    PoiRS poiMain = findPoiByName(cvo.getName(), null,null, "Cruise");
    if (poiMain != null) {
      tripDetail.setPoiCmpyId(poiMain.getCmpyId());
      tripDetail.setPoiId(poiMain.getId());
      tripDetail.setLocStartLat(poiMain.locLat);
      tripDetail.setLocStartLong(poiMain.locLong);
      tripDetail.setName(poiMain.getName());
      reservation.getCruise().name = poiMain.getName();
    } else {
      tripDetail.setName(cvo.getName());
      reservation.getCruise().name = cvo.getName();
    }

    PoiRS poiStart = findPoiByName(cvo.getPortDepart(), null,null, "Port");
    if (poiStart != null) {
      tripDetail.setLocStartPoiCmpyId(poiStart.getCmpyId());
      tripDetail.setLocStartPoiId(poiStart.getId());
      tripDetail.setLocStartName(poiStart.getName());
      tripDetail.setLocStartLat(poiStart.locLat);
      tripDetail.setLocStartLong(poiStart.locLong);
    }
    else {
      tripDetail.setLocStartName(cvo.getPortDepart());
    }

    PoiRS poiFinish = findPoiByName(cvo.getPortArrive(), null,null, "Port");
    if (poiFinish != null) {
      tripDetail.setLocFinishPoiCmpyId(poiFinish.getCmpyId());
      tripDetail.setLocFinishPoiId(poiFinish.getId());
      tripDetail.setLocFinishName(poiFinish.getName());
      tripDetail.setLocFinishLat(poiFinish.locLat);
      tripDetail.setLocFinishLong(poiFinish.locLong);
    }
    else {
      tripDetail.setLocFinishName(cvo.getPortArrive());
    }

    cruises.add(tripDetail);
    details.add(tripDetail);

    if (cvo.getOrigSrcPK() != null && !cvo.getOrigSrcPK().isEmpty()) {
      detailsByOrigSrcPK.put(cvo.getOrigSrcPK(), tripDetail);
    }
  }

  private void addFlight(FlightVO fvo) {
    TripDetail tripDetail = null;

    String flightNum = null;
    if (fvo.getCode() != null) {
      flightNum = fvo.getCode() + " " + fvo.getNumber();
    } else if (fvo.getName() != null) {
      flightNum = fvo.getName() + " " + fvo.getNumber();
    } else {
      flightNum = fvo.getNumber();
    }
    if (flightNum == null) {
      flightNum = "";
    }

    if (fvo.getDepatureTime() == null) {
      return;
    }

    if (fvo.getId() != null) {
      try {
        tripDetail = TripDetail.find.byId(fvo.getId());
      }catch (Exception e){} //Ignoring on purpose
    }

    if (tripDetail == null && !isNewTrip) {
      if(fvo.getId() != null) {
        tripDetail = TripDetail.findByPK(fvo.getId());
      }

      if(tripDetail == null) {
        List<TripDetail> tripDetails = TripDetail.findActiveByStartTimestampAndType(trip.tripid,
                                                                                    fvo.getDepatureTime().getTime(),
                                                                                    ReservationType.FLIGHT);
        if (tripDetails != null) {
          for (TripDetail td : tripDetails) {
            if ((td.bookingnumber == null && fvo.getReservationNumber() == null) ||
                Utils.compareTrimmedIgnoreCase(td.bookingnumber, fvo.getReservationNumber())) {
              tripDetail = td;
              break;
            }
          }
        }
      }
    }

    if(tripDetail != null) {
      //found an existing trip - looking for passengers to see whether this is an existing passenger or a new
      // one
      //if there are existing passengers, transfer them so we do not overwrite them
      //List<PassengerVO> existingPassengerVOs = PassengerVO.parsePassengerVO(tripDetail.comments);
      // (Wei) The passenger info is now inside the reservationDetail
      List<PassengerVO> existingPassengerVOs = PassengerVO.getPassengerVO(tripDetail.getReservation());
      
      if (existingPassengerVOs != null && fvo.getPassengers() != null && fvo.getPassengers().size() > 0) {
        List<String> newPassengerNames = new ArrayList<>();
        for (PassengerVO nVO : fvo.getPassengers()) {
          newPassengerNames.add(nVO.getFullName());
        }
        for (PassengerVO passengerVO : existingPassengerVOs) {
          if (!newPassengerNames.contains(passengerVO.getFullName())) {
            fvo.addPassenger(passengerVO);
          }
        }
      }
    }

    if(tripDetail != null) {
      //flightBooking = FlightBooking.find.byId(tripDetail.getDetailsid());
      UmFlightReservation reservation = cast(tripDetail.getReservation(), UmFlightReservation.class);
      String flightId = reservation != null ? reservation.getFlight().flightNumber : null;
      //untrack flights if the flight id has changed
      if (trip != null && trip.status == APPConstants.STATUS_PUBLISHED &&
          tripCompany != null && tripCompany.isFlighttracking() &&
          flightId != null && flightNum != null &&
          !flightId.equals(flightNum)) {
        //add this booking to be untracked
        untrackFlights.add(tripDetail.getDetailsid());
      }
    }

    if (tripDetail == null) {
      tripDetail = TripDetail.build(account.getLoggingId(), trip.getTripid(), ReservationType.FLIGHT);
      if (fvo.getRank() > -1) {
        tripDetail.setRank(fvo.getRank());
      }
    }
    if (importSrc != null) {
      tripDetail.setImportSrc(importSrc);
      tripDetail.setImportSrcId(importSrcId);
      tripDetail.setImportTs(importTS);
    }
    if (fvo.getSrc() != null && fvo.getSrcId() > 0) {
      tripDetail.setBkApiSrcId(fvo.getSrcId());
      tripDetail.setImportSrcId(fvo.getSrc());
    }
    if (fvo.getRecordLocator() != null) {
      tripDetail.setRecordLocator(fvo.getRecordLocator());
    }
    tripDetail.setLastupdatedtimestamp(currTimestamp);
    tripDetail.setModifiedby(account.getLoggingId());

    tripDetail.setStartTimestamp(fvo.getDepatureTime());
    tripDetail.setEndTimestamp(fvo.getArrivalTime());

    int pt = PoiTypeInfo.Instance().byName("Airline").getId();

    UmFlightReservation reservation = new UmFlightReservation();
    tripDetail.setReservation(reservation);
    populateRate(reservation, fvo);
    reservation.setCancellationPolicy(fvo.getCancellationPolicy());

    reservation.getFlight().flightNumber = flightNum;
   
    if (fvo.getDepartureAirport() != null) {
      reservation.getFlight().departureTerminal = fvo.getDepartureAirport().getTerminal();
    }

    if (fvo.getArrivalAirport() != null) {
      reservation.getFlight().arrivalTerminal = fvo.getArrivalAirport().getTerminal();
    }

    UmFlight flight = reservation.getFlight();
    if (StringUtils.isNotEmpty(fvo.getAirline())) {
      flight.getAirline().name = fvo.getAirline();
    }

    if (StringUtils.isNotEmpty(fvo.getOperatedBy())) {
      flight.getOperatedBy().name = fvo.getOperatedBy();
    }

    if (StringUtils.isNotEmpty(fvo.getAirCraft())) {
      flight.aircraft = fvo.getAirCraft();
    }

    if (StringUtils.isNotEmpty(fvo.getDepartureGate())) {
      flight.departureGate = fvo.getDepartureGate();
    }

    if (StringUtils.isNotEmpty(fvo.getArrivalGate())) {
      flight.arrivalGate = fvo.getArrivalGate();
    }
    if (fvo.getNotes() != null && fvo.getNotes().contains("Passengers:\n")  && fvo.getNotes().contains("Name:")) {
     //some of this might still come in the comments - specifically the Sabre gateway
      UmFlightReservationBuilder builder = new UmFlightReservationBuilder();
      builder.parseComment(reservation, fvo.getNotes());
    } else {
      reservation.setTravelers(PassengerVO.getTravelers(fvo.getPassengers()));
      StringBuilder notes = new StringBuilder();
      if(generateNotes) {
        if (fvo.getPassengers() != null) {
          for (PassengerVO passengerVO : fvo.getPassengers()) {
            if (passengerVO.getFullNotes().trim().length() > 0) {
              notes.append(passengerVO.getFullNotes());
              notes.append("\n");
            }
          }
        }

        if (fvo.getFullNotes() != null) {
          notes.append(fvo.getFullNotes());
        }
      }
      else {
        notes.append(fvo.getNote());
      }

      reservation.setNotesPlainText(notes.toString());
    }
    
    PoiRS poiMain = null;
    if(fvo.getAirlinePoiId() != null) {
      try {
        Long mpid = Long.parseLong(fvo.getAirlinePoiId());
        poiMain = PoiController.getMergedPoi (mpid, tripCompany.getCmpyId());
      } catch (Exception e){}
    }

    if (poiMain == null &&fvo.getCode() != null) {
      poiMain = PoiController.findAndMergeByCode(fvo.getCode(), pt, tripCompany.getCmpyId());
    }
    if (poiMain != null) {
      tripDetail.setPoiCmpyId(tripCompany.getCmpyId());
      tripDetail.setPoiId(poiMain.getId());
      tripDetail.setLocStartLat(poiMain.locLat);
      tripDetail.setLocStartLong(poiMain.locLong);
      tripDetail.setName(poiMain.getCode() + " " + fvo.getNumber());
    }

    /* Deal with Arrival Airport */
    airportDetailHelper(tripDetail, fvo.getArrivalAirport(), AirportType.ARRIVAL);

    /* Deal with departure airport */
    airportDetailHelper(tripDetail, fvo.getDepartureAirport(), AirportType.DEPARTURE);

    /* Build flight booking comments */
    StringBuffer notes = new StringBuffer();

    if (fvo.getReservationNumber() != null) {
      tripDetail.setBookingnumber(fvo.getReservationNumber());
    }


    flights.add(tripDetail);
    details.add(tripDetail);
    if (fvo.getOrigSrcPK() != null && !fvo.getOrigSrcPK().isEmpty()) {
      detailsByOrigSrcPK.put(fvo.getOrigSrcPK(), tripDetail);
    }
  }

  public static enum AirportType {
    ARRIVAL,
    DEPARTURE
  }

  /**
   * Helps to get and match airport information with data already in the system
   *
   * @param avo        Extracted Airport Information
   * @param tripDetail Trip detail that this airport relates to
   * @param aType Airport type (Arrival vs Departure)
   * @return String representing the best possible airport information
   */
  private void airportDetailHelper(TripDetail tripDetail, AirportVO avo, AirportType aType) {
    if (avo == null) {
      return;
    }

    PoiRS prs = null;

    if(avo.getId() != null) {
      try {
        Long pid = Long.parseLong(avo.getId());
        prs = PoiController.getMergedPoi(pid, tripCompany.getCmpyId());
      } catch (Exception e) {
        Log.err("VOModeller: failed to parse Airport ID: " + avo.getId());
      }
    }

    if (avo.getCode() != null && prs == null) {
      int pt = PoiTypeInfo.Instance().byName("Airport").getId();
      prs = PoiController.findAndMergeByCode(avo.getCode(), pt, tripCompany.getCmpyId());
    }

    if (prs == null && avo.getCity() != null && avo.getName() != null) {
      prs = PoiController.findAirportByCityAndName(avo.getCity(), avo.getName(), tripCompany.getCmpyId());
    }

    if (prs != null) {
      avo.setCode(prs.getCode());
      switch (aType) {
        case DEPARTURE:
          tripDetail.setLocStartLat(prs.locLat);
          tripDetail.setLocStartLong(prs.locLong);
          tripDetail.setLocStartPoiId(prs.getId());
          tripDetail.setLocStartPoiCmpyId(tripCompany.getCmpyId());
          tripDetail.setLocStartName(prs.getName());
          break;
        case ARRIVAL:
          tripDetail.setLocFinishLat(prs.locLat);
          tripDetail.setLocFinishLong(prs.locLong);
          tripDetail.setLocFinishPoiId(prs.getId());
          tripDetail.setLocFinishPoiCmpyId(tripCompany.getCmpyId());
          tripDetail.setLocFinishName(prs.getName());
          break;
      }
    } else {
      String name  = null;
      if (avo.getName() != null) {
        name = avo.getName();
      }
      else if (avo.getCity() != null &&
               avo.getCountry() != null) {
        name = avo.getCity() + ", " + avo.getCountry();
      } else if (avo.getCode() != null) {
        name = avo.getCode();
      } else {
        name = "N/A";
      }

      switch (aType) {
        case DEPARTURE:
          tripDetail.setLocStartName(name);
          break;
        case ARRIVAL:
          tripDetail.setLocFinishName(name);
          break;
      }
    }
  }

  private void addHotel(HotelVO hvo) {
    TripDetail tripDetail = null;

    if (hvo.getCheckin() == null) {
      return;
    }

    if (hvo.getId() != null) {
      try {
        tripDetail = TripDetail.find.byId(hvo.getId());
      }catch (Exception e){} //Ignoring on purpose
    }

    if (tripDetail == null && !isNewTrip) {
      //try to find an existing match
      List<TripDetail> tripDetails = null;
      if (hvo.getSrcId() > 0 ) {
        try {
          tripDetails = TripDetail.findActiveByTripIdandSrcID(trip.getTripid(), hvo.getSrcId(), ReservationType.HOTEL);
        } catch (Exception e) {

        }
      }
      if ((tripDetails == null || tripDetails.size() == 0 )  && hvo.getConfirmation() != null && !hvo.getConfirmation().isEmpty()) {
        tripDetails = TripDetail.findActiveByTripIdAndConfirmation(trip.tripid, hvo.getConfirmation(), ReservationType.HOTEL);
      }
      if (tripDetails == null || tripDetails.size() == 0) {
        tripDetails = TripDetail.findActiveByStartTimestampAndType(trip.tripid,
                                                                   hvo.getCheckin().getTime(),
                                                                   ReservationType.HOTEL);
      }


      if (tripDetails != null) {
        for (TripDetail existingTripDetail : tripDetails) {
          if ((existingTripDetail.bookingnumber == null && hvo.getConfirmation() == null  &&
               (existingTripDetail.bkApiSrcId == null || existingTripDetail.bkApiSrcId == 0) && hvo.getSrcId() == 0 ) ||
               (existingTripDetail.bookingnumber !=  null && hvo.getConfirmation() != null &&
                existingTripDetail.bookingnumber.trim().equalsIgnoreCase(hvo.getConfirmation().trim()) &&
                existingTripDetail.getName() != null && hvo.getName() != null && existingTripDetail.getName().equalsIgnoreCase(hvo.getName())) ||
                (existingTripDetail.bkApiSrcId != null && existingTripDetail.bkApiSrcId > 0 && hvo.getSrcId() > 0 && existingTripDetail.bkApiSrcId == hvo.getSrcId())) {
            tripDetail = existingTripDetail;
            break;
          }
        }
        if (tripDetail == null && tripDetails.size() == 1 && tripDetails.get(0).bookingnumber !=  null && hvo.getConfirmation() != null &&
                tripDetails.get(0).bookingnumber.trim().equalsIgnoreCase(hvo.getConfirmation().trim())) {
          tripDetail = tripDetails.get(0);
        }
      }
    }

    if (tripDetail == null) {
      tripDetail = new TripDetail();
      tripDetail.setCreatedby(account.getLoggingId());
      tripDetail.setCreatedtimestamp(currTimestamp);
      tripDetail.setDetailsid(DBConnectionMgr.getUniqueId());
      tripDetail.setDetailtypeid(ReservationType.HOTEL);
      tripDetail.setLocStartLat(0.0f);
      tripDetail.setLocStartLong(0.0f);
      tripDetail.setLocFinishLat(0.0f);
      tripDetail.setLocFinishLong(0.0f);
      tripDetail.setStatus(APPConstants.STATUS_ACTIVE);
      tripDetail.setTripid(trip.tripid);
      tripDetail.setTriptype(0);
      tripDetail.setReservation(new UmAccommodationReservation());
      if (hvo.getRank() > -1) {
        tripDetail.setRank(hvo.getRank());
      }
    }
    if (importSrc != null) {
      tripDetail.setImportSrc(importSrc);
      tripDetail.setImportSrcId(importSrcId);
      tripDetail.setImportTs(importTS);
    }
    if (hvo.getSrc() != null && hvo.getSrcId() > 0) {
      tripDetail.setBkApiSrcId(hvo.getSrcId());
      tripDetail.setImportSrcId(hvo.getSrc());
    }
    if (hvo.getRecordLocator() != null) {
      tripDetail.setRecordLocator(hvo.getRecordLocator());
    }
    tripDetail.setStarttimestamp(hvo.getCheckin().getTime());
    if (hvo.getCheckout() != null) {
      tripDetail.setEndtimestamp(hvo.getCheckout().getTime());
    }
    else {
      tripDetail.setEndtimestamp(tripDetail.getStarttimestamp());
    }

    tripDetail.setLastupdatedtimestamp(currTimestamp);
    tripDetail.setModifiedby(account.getLoggingId());

    UmAccommodationReservation reservation = cast(tripDetail.getReservation(), UmAccommodationReservation.class);
    if (reservation == null) {
      reservation = new UmAccommodationReservation();
      tripDetail.setReservation(reservation);
    }
    populateRate(reservation, hvo);
    reservation.setCancellationPolicy(hvo.getCancellationPolicy());
    PoiRS poiMain = findPoiByName(hvo.getHotelName(), hvo.getTag(), hvo.getAddress(), "Accommodations");
    if (poiMain != null) {
      tripDetail.setPoiCmpyId(poiMain.getCmpyId());
      tripDetail.setPoiId(poiMain.getId());
      tripDetail.setLocStartLat(poiMain.locLat);
      tripDetail.setLocStartLong(poiMain.locLong);
      tripDetail.setName(poiMain.getName());
      reservation.getAccommodation().name = poiMain.getName();
    } else {
      tripDetail.setName(hvo.getHotelName());
      reservation.getAccommodation().name = hvo.getHotelName();
    }

    tripDetail.setBookingnumber(hvo.getConfirmation());

    List<HotelVO.HotelTravelerVO> travlers = hvo.getTravelers();

    reservation.setTravelers(mapAccommodationTravelers(travlers));

    String comments;
    
    if (generateNotes) {
      comments = hvo.getFullNotes();
    } else {
      comments = hvo.getNote();
    }

    if (hvo.isCommentAsContainer()) {
      // parse the comments and extract phone/address
      // The information parsed from external source
      accommodationBuilder.parseComments(reservation, comments);
    } else {
      reservation.setNotesPlainText(comments);

      UmPostalAddress hotelAddress = hvo.getAddress();
      if (hotelAddress == null) {
        hotelAddress = new UmPostalAddress();
      }
      UmLodgingBusiness hotel = reservation.getAccommodation();
      hotel.setAddress(hotelAddress);
      hotel.faxNumber = hvo.getFax();
      hotel.telephone = hvo.getPhone();
      hotel.url = hvo.getUrl();
    }
    hotels.add(tripDetail);
    details.add(tripDetail);
    if (hvo.getOrigSrcPK() != null && !hvo.getOrigSrcPK().isEmpty()) {
      detailsByOrigSrcPK.put(hvo.getOrigSrcPK(), tripDetail);
    }
  }

  private List<UmTraveler> mapAccommodationTravelers(List<HotelVO.HotelTravelerVO> voList) {
    if (voList == null) {
      return null;
    }
    List<UmTraveler> travelers = new ArrayList<>();
    for (HotelVO.HotelTravelerVO vo : voList) {
      travelers.add(mapAccommodationTraveler(vo));
    }
    return travelers;
  }

  private UmAccommodationTraveler mapAccommodationTraveler(HotelVO.HotelTravelerVO vo) {
    UmAccommodationTraveler traveler = new UmAccommodationTraveler();
    traveler.setFamilyName(vo.lastName);
    traveler.setGivenName(vo.firstName);
    traveler.setRoomType(vo.roomType);
    traveler.setBedding(vo.bedding);
    traveler.setConfirmStatus(vo.status);
    return traveler;
  }

  private PoiRS findPoiByName(String name, String tag, UmPostalAddress address, String typeName) {
    try {
      if (tripCompany != null && tripCompany.getTag() != null && tripCompany.getTag().contains(APPConstants.CMPY_TAG_API_NO_POI_MATCH)) {
        //do not try to match if this comes from the API
        //temp to prevent false matching
        return null;
      } else if (noPOIMatch) {
        return null;
      }

      PoiTypeInfo.PoiType pt = PoiTypeInfo.Instance().byName(typeName);
      if (pt == null) {
        Log.err("Wrong POI type given in VOModeller findByPoiName helper");
      }

      if (name == null) {
        return null;
      }

      name = name.trim();
      name = name.replaceAll("’", "'");
      if (address != null && address.addressLocality != null && address.addressLocality.trim().length() > 0) {
        name += ", " + address.addressLocality;
      }

      List<PoiRS> pois;
      //if there is a tag - let's try to match the exact tag
      if (tag != null && tag.trim().length() > 0) {
        if (this.importSrc != null && this.importSrc == BookingSrc.ImportSrc.TRAVELBOUND) {
          //for travelbound
          pois = PoiController.findMatchBySrcReference(tag.trim(), pt.getId(), 0, 10);
          if (pois.size() > 0) {
            return pois.get(0);
          }

        } else {
          pois = PoiController.findMatchByTagSrcReference(tag.trim(), pt.getId(), tripCompany.getCmpyId(), 10);
          if (pois.size() > 0) {
            return pois.get(0);
          }
        }
      }

      //change to make the matching more conservative and restrict matching to withing the company only
      boolean includePublic = false;

      if (typeName != null && typeName.equals("Accommodations") && name.length() > 10) {
        //we try to find an exact match by tag first if the name is more than 10 chars to avoid too many generic matches
        pois = PoiController.findMergedByTag(name.replace(" ", "%"), pt.getId(), tripCompany.getCmpyId(), 1, includePublic);
        if (pois.size() > 0) {
          PoiRS poi = pois.get(0);
          poi.setCmpyId(tripCompany.getCmpyId());
          return poi;
        }
      }


      PoiSearchHelper psh =  PoiSearchHelper.build()
                                            .setTermSmart(name)
                                            .setType(pt.getId())
                                            .setWithPublic(includePublic)//tripCompany.getCmpyId() != 883) // Only false for ski.com
                                            .setCompanyId(tripCompany.getCmpyId())
                                            .setLimit(30)
                                            .search()
                                            .rank();

      if (typeName != null && typeName.equals("Accommodations")) {
        PoiRS poi = psh.getBestFit();
        if (poi != null) {
          poi.setCmpyId(tripCompany.getCmpyId());
          return poi;
        }
      } else if (psh.getResultSet() != null) {
        //for non hotels, go for an exact match to eliminate false duplicates
        for (PoiRS poi: psh.getResultSet()) {
          if (poi.getName().trim().equalsIgnoreCase(name.trim())) {
            poi.setCmpyId(tripCompany.getCmpyId());
            return poi;
          }
        }
      }
    }
    catch (Exception e) {
      Log.err("findPoiByName: exception", e.getMessage());
      e.printStackTrace();
    }

    return null;
  }

  private void addTransport(TransportVO tvo) {
    TripDetail tripDetail = null;

    if (tvo.getPickupDate() == null) {
      return;
    }

    if (tvo.getId() != null) {
      try {
        tripDetail = TripDetail.find.byId(tvo.getId());
      }catch (Exception e){} //Ignoring on purpose
    }

    if (tripDetail == null && !isNewTrip) {
      List<TripDetail> tripDetails = null;
      //try to find an existing match
      if (tvo.getSrcId() > 0 ) {
        try {
          tripDetails = TripDetail.findActiveByTripIdandSrcID(trip.getTripid(), tvo.getSrcId(), ReservationType
              .TRANSPORT);
        } catch (Exception e) {

        }
      }

      if ((tripDetails == null || tripDetails.size() == 0 ) && tvo.getConfirmationNumber() != null && !tvo.getConfirmationNumber().isEmpty()) {
        tripDetails = TripDetail.findActiveByTripIdAndConfirmation(trip.tripid, tvo.getConfirmationNumber(), ReservationType.TRANSPORT);
      }

      if ((tripDetails == null || tripDetails.size() == 0 ) && tvo.getPickupDate() != null && (tripDetails == null || tripDetails.size() == 0)) {
        //try to find an existing match
        tripDetails = TripDetail.findActiveByStartTimestampAndType(trip.tripid,
                                                                   tvo.getPickupDate().getTime(),
                                                                   ReservationType.TRANSPORT);
      }

      if (tripDetails != null && tripDetails.size() > 0) {
        for (TripDetail existingTransportDetail : tripDetails) {
          if (((existingTransportDetail.getName() != null && tvo.getName() != null &&
                existingTransportDetail.getName().trim().equalsIgnoreCase(tvo.getName().trim()) &&
                existingTransportDetail.getBookingnumber() == null && tvo.getConfirmationNumber() == null &&
                (existingTransportDetail.bkApiSrcId == null || existingTransportDetail.bkApiSrcId == 0) && tvo.getSrcId() == 0 )
               ||(existingTransportDetail.getBookingnumber() != null && tvo.getConfirmationNumber() != null && existingTransportDetail.getBookingnumber().equals(tvo.getConfirmationNumber()))) ||
          (existingTransportDetail.bkApiSrcId != null && existingTransportDetail.bkApiSrcId > 0 && tvo.getSrcId() > 0 && existingTransportDetail.bkApiSrcId == tvo.getSrcId())) {

            tripDetail = existingTransportDetail;
            break;
          }
        }
      }
    }

    if (tripDetail == null) {
      tripDetail = new TripDetail();
      tripDetail.setCreatedby(account.getLoggingId());
      tripDetail.setCreatedtimestamp(currTimestamp);
      tripDetail.setDetailsid(DBConnectionMgr.getUniqueId());
      tripDetail.setLocStartLat(0.0f);
      tripDetail.setLocStartLong(0.0f);
      tripDetail.setLocFinishLat(0.0f);
      tripDetail.setLocFinishLong(0.0f);
      tripDetail.setStatus(APPConstants.STATUS_ACTIVE);
      tripDetail.setTripid(trip.tripid);
      tripDetail.setTriptype(0);
      tripDetail.setReservation(new UmTransferReservation());
      if (tvo.getRank() > -1) {
        tripDetail.setRank(tvo.getRank());
      }
    }

    if (importSrc != null) {
      tripDetail.setImportSrc(importSrc);
      tripDetail.setImportSrcId(importSrcId);
      tripDetail.setImportTs(importTS);
    }
    if (tvo.getSrc() != null && tvo.getSrcId() > 0) {
      tripDetail.setBkApiSrcId(tvo.getSrcId());
      tripDetail.setImportSrcId(tvo.getSrc());
    }
    if (tvo.getRecordLocator() != null) {
      tripDetail.setRecordLocator(tvo.getRecordLocator());
    }
    if (tvo.getBookingType() != null) {
      tripDetail.setDetailtypeid(tvo.getBookingType());
    } else {
      tripDetail.setDetailtypeid(ReservationType.TRANSPORT);
    }


    if (tvo.getPickupDate() != null) {
      tripDetail.setStarttimestamp(tvo.getPickupDate().getTime());
    } else if (tvo.getDropoffDate() != null) {
      tripDetail.setStarttimestamp(tvo.getDropoffDate().getTime());
    }

    if (tvo.getDropoffDate() != null) {
      tripDetail.setEndtimestamp(tvo.getDropoffDate().getTime());
    }
    else if (tvo.getPickupDate() != null) {
      tripDetail.setEndtimestamp(tripDetail.getStarttimestamp());
    }

    tripDetail.setLastupdatedtimestamp(currTimestamp);
    tripDetail.setModifiedby(account.getLoggingId());

    UmTransferReservation reservation = cast(tripDetail.getReservation(), UmTransferReservation.class);
    if (reservation == null) {
      reservation = new UmTransferReservation();
      tripDetail.setReservation(reservation);
    }

    populateRate(reservation, tvo);
    reservation.setCancellationPolicy(tvo.getCancellationPolicy());
    if (generateNotes) {
      reservation.setNotesPlainText(tvo.getFullNotes());
    } else {
      reservation.setNotesPlainText(tvo.getNote());
    }

    tripDetail.setBookingnumber(tvo.getConfirmationNumber());

    PoiRS poiMain = findPoiByName(tvo.getName(), tvo.getTag(),null, "Transportation");
    tripDetail.setName(tvo.getName());
    
    reservation.getTransfer().getProvider().name = tvo.getName();

    if (poiMain != null) {
      tripDetail.setPoiCmpyId(poiMain.getCmpyId());
      tripDetail.setPoiId(poiMain.getId());
      tripDetail.setLocStartLat(poiMain.locLat);
      tripDetail.setLocStartLong(poiMain.locLong);
      reservation.getTransfer().getProvider().contactPoint = poiMain.getName();
      if(tvo.getName() == null) {
        tripDetail.setName(poiMain.getName());
        reservation.getTransfer().getProvider().name = poiMain.getName();
      }
    } else {
      if(tvo.getCmpyName() != null) {
        reservation.getTransfer().getProvider().contactPoint = tvo.getCmpyName();
        if(tvo.getName() == null) {
          tripDetail.setName(tvo.getCmpyName());
          reservation.getTransfer().getProvider().name = tvo.getCmpyName();
        }
      }
    }

    PoiRS poiStart = checkLocationIsAirport(tvo.getPickupLocation());
    PoiRS poiFinish = checkLocationIsAirport(tvo.getDropoffLocation());

    if (poiStart != null) {
      tripDetail.setLocStartPoiCmpyId(poiStart.getCmpyId());
      tripDetail.setLocStartPoiId(poiStart.getId());
      tripDetail.setLocStartName(poiStart.getName() + " (" +
                                 PoiTypeInfo.Instance().byId(poiStart.getTypeId()).getName()
                                 + ")");
      tripDetail.setLocStartLat(poiStart.locLat);
      tripDetail.setLocStartLong(poiStart.locLong);
    }
    else {
      tripDetail.setLocStartName(tvo.getPickupLocation());
    }

    if (poiFinish != null) {
      tripDetail.setLocFinishPoiCmpyId(poiFinish.getCmpyId());
      tripDetail.setLocFinishPoiId(poiFinish.getId());
      tripDetail.setLocFinishName(poiFinish.getName() + " (" +
                                 PoiTypeInfo.Instance().byId(poiFinish.getTypeId()).getName()
                                 + ")");
      tripDetail.setLocFinishLat(poiFinish.locLat);
      tripDetail.setLocFinishLong(poiFinish.locLong);
    }
    else {
      tripDetail.setLocFinishName(tvo.getDropoffLocation());
    }

    transports.add(tripDetail);
    details.add(tripDetail);
    if (tvo.getOrigSrcPK() != null && !tvo.getOrigSrcPK().isEmpty()) {
      detailsByOrigSrcPK.put(tvo.getOrigSrcPK(), tripDetail);
    }
  }

  /**
   * Checks if specified location is an airport and returns appropriate pickup location description
   * @param location
   * @return
   */
  private PoiRS checkLocationIsAirport(String location) {
    if (location == null) {
      return null;
    }

    if (location.length() == 3) {
      int pt = PoiTypeInfo.Instance().byName("Airport").getId();
      return PoiController.findAndMergeByCode(location, pt, tripCompany.getCmpyId());
    }

    return null;
  }

  private void addActivity(ActivityVO avo) {
    TripDetail tripDetail = null;

    if (avo.getStartDate() == null) {
      return;
    }

    if (avo.getId() != null) {
      try {
        tripDetail = TripDetail.find.byId(avo.getId());
      }catch (Exception e){} //Ignoring on purpose
    }

    if (!isNewTrip) {
      //try to find an existing match
      List<TripDetail> tripDetails = null;
      if (avo.getSrcId() > 0 ) {
        try {
          tripDetails = TripDetail.findActiveByTripIdandSrcID(trip.getTripid(), avo.getSrcId(), ReservationType
              .ACTIVITY);
        } catch (Exception e) {

        }
      }
      if ((tripDetails == null || tripDetails.size() == 0 )&& avo.getConfirmation() != null && !avo.getConfirmation().isEmpty()) {
        tripDetails = TripDetail.findActiveByTripIdAndConfirmation(trip.tripid, avo.getConfirmation(), ReservationType.ACTIVITY);
      }

      if ((tripDetails == null || tripDetails.size() == 0 ) && avo.getStartDate() != null && (tripDetails == null || tripDetails.size() == 0)) {
        //try to find an existing match
        tripDetails = TripDetail.findActiveByStartTimestampAndType(trip.tripid,
                                                                   avo.getStartDate().getTime(),
                                                                   ReservationType.ACTIVITY);
      }

      if (tripDetails != null && tripDetails.size() > 0) {
        for (TripDetail a : tripDetails) {
          UmActivityReservation activity = cast(a.getReservation(), UmActivityReservation.class);
          if (activity != null && ((activity.getActivity().name != null && avo.getName() != null &&
              activity.getActivity().name.toLowerCase().trim().equals(avo.getName().toLowerCase().trim()) &&
              a.getBookingnumber() == null && avo.getConfirmation() == null &&
              (a.bkApiSrcId == null || a.bkApiSrcId == 0) && avo.getSrcId() == 0 )
               || (a.getBookingnumber() != null && avo.getConfirmation() != null && a.getBookingnumber().equals(avo.getConfirmation()))
               || (a.bkApiSrcId != null && a.bkApiSrcId > 0 && avo.getSrcId() > 0 && a.bkApiSrcId == avo.getSrcId()))
             )
          {
            tripDetail = a;
            break;
          }
        }
      }
    }

    if (tripDetail == null) {
      tripDetail = new TripDetail();
      tripDetail.setCreatedby(account.getLoggingId());
      tripDetail.setCreatedtimestamp(currTimestamp);
      tripDetail.setDetailsid(DBConnectionMgr.getUniqueId());
      tripDetail.setLocStartLat(0.0f);
      tripDetail.setLocStartLong(0.0f);
      tripDetail.setLocFinishLat(0.0f);
      tripDetail.setLocFinishLong(0.0f);
      tripDetail.setStatus(APPConstants.STATUS_ACTIVE);
      tripDetail.setTripid(trip.tripid);
      tripDetail.setTriptype(0);
      tripDetail.setReservation(new UmActivityReservation());
      if (avo.getRank() > -1) {
        tripDetail.setRank(avo.getRank());
      }
    }

    tripDetail.setLocStartName(avo.getStartLocation());
    tripDetail.setLocFinishName(avo.getEndLocation());

    if (importSrc != null) {
      tripDetail.setImportSrc(importSrc);
      tripDetail.setImportSrcId(importSrcId);
      tripDetail.setImportTs(importTS);
    }
    if (avo.getSrc() != null && avo.getSrcId() > 0) {
      tripDetail.setBkApiSrcId(avo.getSrcId());
      tripDetail.setImportSrcId(avo.getSrc());
    }
    if (avo.getRecordLocator() != null) {
      tripDetail.setRecordLocator(avo.getRecordLocator());
    }
    if (avo.getBookingType() != null) {
       tripDetail.setDetailtypeid(avo.getBookingType());
    } else {
      tripDetail.setDetailtypeid(ReservationType.ACTIVITY);
    }

    tripDetail.setStarttimestamp(avo.getStartDate().getTime());
    if (avo.getEndDate() != null) {
      tripDetail.setEndtimestamp(avo.getEndDate().getTime());
    }
    else {
      tripDetail.setEndtimestamp(tripDetail.getStarttimestamp());
    }

    tripDetail.setLastupdatedtimestamp(currTimestamp);
    tripDetail.setModifiedby(account.getLoggingId());
    
    UmActivityReservation reservation = cast(tripDetail.getReservation(), UmActivityReservation.class);
    if (reservation == null) {
      reservation = new UmActivityReservation();
      tripDetail.setReservation(reservation);
    }

    populateRate(reservation, avo);
    reservation.setCancellationPolicy(avo.getCancellationPolicy());

    if (generateNotes) {
      reservation.setNotesPlainText(avo.getFullNotes());
    } else {
      reservation.setNotesPlainText(avo.getNote());
    }
    tripDetail.setBookingnumber(avo.getConfirmation());

    reservation.getActivity().getOrganizedBy().contactPoint = avo.getContact();

    PoiRS poiMain = findPoiByName(avo.getName(), avo.getTag(), null, "Activity");
    if (poiMain != null) {
      tripDetail.setPoiCmpyId(poiMain.getCmpyId());
      tripDetail.setPoiId(poiMain.getId());
      tripDetail.setLocStartLat(poiMain.locLat);
      tripDetail.setLocStartLong(poiMain.locLong);
      tripDetail.setName(poiMain.getName());
      reservation.getActivity().name = poiMain.getName();
    } else {
      tripDetail.setName(avo.getName());
      reservation.getActivity().name = avo.getName();
    }

    activities.add(tripDetail);
    details.add(tripDetail);
    if (avo.getOrigSrcPK() != null && !avo.getOrigSrcPK().isEmpty()) {
      detailsByOrigSrcPK.put(avo.getOrigSrcPK(), tripDetail);
    }
  }

  public void addActivity(DayDetails dayDetails, Date timestamp) {

    TripDetail tripDetail = null;

    if (!isNewTrip) {
      List<TripDetail> tripDetails = TripDetail.findActiveByTripIdandStartTimestamp(trip.tripid,
                                                                                    timestamp.getTime());
      if (tripDetails != null && tripDetails.size() > 0) {
        tripDetail = tripDetails.get(0);
      }
    }

    if (tripDetail == null) {
      tripDetail = new TripDetail();
      tripDetail.setCreatedby(account.getLoggingId());
      tripDetail.setCreatedtimestamp(currTimestamp);
      tripDetail.setDetailsid(DBConnectionMgr.getUniqueId());
      tripDetail.setDetailtypeid(ReservationType.ACTIVITY);
      tripDetail.setReservation(new UmReservation());
      tripDetail.setLocStartLat(0.0f);
      tripDetail.setLocStartLong(0.0f);
      tripDetail.setLocFinishLat(0.0f);
      tripDetail.setLocFinishLong(0.0f);
      tripDetail.setStatus(APPConstants.STATUS_ACTIVE);
      tripDetail.setTripid(trip.tripid);
      tripDetail.setTriptype(0);
    }

    tripDetail.setStarttimestamp(timestamp.getTime());
    if (tripDetail.getEndtimestamp() == null ||
        tripDetail.getEndtimestamp() < timestamp.getTime()) {
      tripDetail.setEndtimestamp(timestamp.getTime());
    }

    tripDetail.setLastupdatedtimestamp(currTimestamp);
    tripDetail.setModifiedby(account.getLoggingId());
    if (importSrc != null) {
      tripDetail.setImportSrc(importSrc);
      tripDetail.setImportSrcId(importSrcId);
      tripDetail.setImportTs(importTS);
    }

    UmActivityReservation reservation = cast(tripDetail.getReservation(), UmActivityReservation.class);
    if (reservation == null) {
      reservation = new UmActivityReservation();
      tripDetail.setReservation(reservation);
    }
    PoiRS poiMain = findPoiByName(dayDetails.getIntro(), null,null, "Activity");
    if (poiMain != null) {
      tripDetail.setPoiCmpyId(poiMain.getCmpyId());
      tripDetail.setPoiId(poiMain.getId());
      tripDetail.setLocStartLat(poiMain.locLat);
      tripDetail.setLocStartLong(poiMain.locLong);
      tripDetail.setName(poiMain.getName());
      reservation.getActivity().name = poiMain.getName();
    } else {
      tripDetail.setName(dayDetails.getIntro());
      reservation.getActivity().name = dayDetails.getIntro();
    }

    activities.add(tripDetail);
    details.add(tripDetail);
  }

  protected void populateRate(UmReservation reservation, ReservationVO reservationVO) {
    //is there an alternate service type
    if (reservationVO.getServiceType() != null && reservationVO.getServiceType().trim().length() > 0) {
      reservation.setServiceType(reservationVO.getServiceType());
    }

    reservation.setCurrency(reservationVO.getCurrency());
    reservation.setSubtotal(reservationVO.getSubtotal());
    reservation.setTotal(reservationVO.getTotal());
    reservation.setImportant(reservationVO.getImportant());
    reservation.setFees(reservationVO.getFees());
    reservation.setTaxes(reservationVO.getTaxes());

    if (reservationVO.getRates()!=null) {
      List<UmRate> reservationRates = new ArrayList<>();
      for (ReservationVO.Rate r : reservationVO.getRates()) {
        UmRate rate = new UmRate();
        rate.setName(r.getName());
        rate.setPrice(r.getPrice());
        rate.setDescription(r.getDescription());
        rate.setDisplayName(StringUtils.trim(UmRateType.getTypeByName(r.getDisplayName()).getValue()));
        reservationRates.add(rate);
      }
      reservation.setRates(reservationRates);
    }
  }

  public Trip getTrip () {
    return trip;
  }

  public Account getAccount() {
    return account;
  }

  public boolean isGenerateNotes() {
    return generateNotes;
  }

  public VOModeller setGenerateNotes(boolean generateNotes) {
    this.generateNotes = generateNotes;
    return this;
  }

  public List<TripDetail> getDetails() {
    return details;
  }
}
