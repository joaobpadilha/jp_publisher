package com.mapped.publisher.persistence;

import java.util.*;

/**
 * Information about poi types. Once initialized during framework startup,
 * contents are no longer updated.
 * <p/>
 * Created by surge on 2014-10-23.
 */
public class PoiTypeInfo {
  private boolean locked;
  private Map<Integer, PoiType> byIdMap;
  private Map<String, PoiType> byNameMap;
  public final static int TYPE_ROOT_ID = 0;

  /**
   * Logic representation of the POI Type
   */
  public static class PoiType {
    private int id;
    private String name;
    private int priority;
    private PoiType parent = null;
    private RecordState state;
    private List<PoiType> children = null;

    public PoiType(int id, String name, int priority, RecordState rs) {
      this.id = id;
      this.name = name;
      this.priority = priority;
      this.state = rs;
      children = new ArrayList<>(5);
    }

    public RecordState getState() {
      return state;
    }

    public int getId() {
      return id;
    }

    public String getName() {
      return name;
    }

    public int getPriority() {
      return priority;
    }

    public PoiType getParent() {
      return parent;
    }

    public void setParent(PoiType parent) {
      this.parent = parent;
    }

    public List<PoiType> getChildren() {
      return children;
    }

    public void addChild(PoiType child) {
      children.add(child);
    }
  }

  private static class PoiTypeInfoHelper {
    private static final PoiTypeInfo INSTANCE = new PoiTypeInfo();
  }

  private PoiTypeInfo() {
    byIdMap = new TreeMap<>();
    byNameMap = new TreeMap<>();
    locked = false;
  }

  public static PoiTypeInfo Instance() {
    return PoiTypeInfoHelper.INSTANCE;
  }

  public void addTypeInfo(int id, String name, int priority, RecordState rs) {
    if (locked) {
      return;
    }
    PoiType pt = new PoiType(id, name, priority, rs);
    byIdMap.put(id, pt);
    byNameMap.put(name.toLowerCase(), pt);
  }

  public PoiType byId(int id) {
    return byIdMap.get(id);
  }

  public PoiType byName(String name) {
    if (name.equalsIgnoreCase("Hotel")) {
      name = "Accommodations";
    }
    return byNameMap.get(name.toLowerCase());
  }

  public Map<Integer, String> getTypeMap() {
    Map<Integer, String> result = new TreeMap<>();
    for (Integer key : byIdMap.keySet()) {
      if (TYPE_ROOT_ID != key) {
        PoiType pt = byIdMap.get(key);
        result.put(pt.getId(), pt.getName());
      }
    }
    return result;
  }

  public List<Integer> getTypes() {
    return new ArrayList<>(byIdMap.keySet());
  }


  public void lock() {
    locked = true;
  }
}
