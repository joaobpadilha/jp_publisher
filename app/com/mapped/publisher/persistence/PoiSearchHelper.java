package com.mapped.publisher.persistence;

import controllers.PoiController;
import models.publisher.Company;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;

/**
 * Assistant to allow finding POI Information easier.
 * <p>
 * Created by surge on 2016-09-23.
 */
public class PoiSearchHelper {

  String      term;
  List<PoiRS> resultSet;
  String      country;
  String      code;
  String      city;
  String      maxRecords;
  Integer     consortiumId;
  Integer     companyId;
  Integer     type;
  int                        limit      = 1;
  boolean                    isRanked   = false;
  boolean                    withPublic = false;
  List<Pair<Integer, PoiRS>> rankedList = new ArrayList<>();

  public static PoiSearchHelper build() {
    PoiSearchHelper psh = new PoiSearchHelper();
    psh.resultSet = new ArrayList<>();
    return psh;
  }

  public List<PoiRS> getResultSet() {
    return resultSet;
  }

  public PoiSearchHelper setLimit(int limit) {
    this.limit = limit;
    return this;
  }

  public PoiSearchHelper search() {
    if (term == null || type == null || companyId == null || limit <= 0) {
      return this;
    }

    Set<Integer> allCmpies = new HashSet<>();
    allCmpies.add(companyId);

    if (withPublic) {
      allCmpies.add(Company.PUBLIC_COMPANY_ID);
    }

    List<Integer> types = new ArrayList<>();
    types.add(type);

    Map<Long, List<PoiRS>> poiSet = PoiMgr.smartSearch(term, types, allCmpies, limit);
    for (Long poiId : poiSet.keySet()) {
      PoiRS prs = PoiController.mergePoi(poiSet.get(poiId), allCmpies);
      resultSet.add(prs);
    }

    isRanked = false;

    return this;
  }

  public PoiSearchHelper setType(Integer type) {
    this.type = type;
    return this;
  }

  public PoiSearchHelper setWithPublic(boolean withPublic) {
    this.withPublic = withPublic;
    return this;
  }

  public String getCity() {
    return city;
  }

  public PoiSearchHelper setCity(String city) {
    if (city != null) {
      this.city = city.trim();
      isRanked = false;
    }
    return this;
  }

  public PoiSearchHelper rank() {
    if (isRanked) {
      return this;
    }
    rankedList.clear();

    for (PoiRS prs : resultSet) {
      int                  rank   = rankPoi(prs);
      Pair<Integer, PoiRS> ranked = new ImmutablePair<>(rank, prs);
      rankedList.add(ranked);
    }

    rankedList.sort((o1, o2) -> o2.getLeft() - o1.getLeft());
    isRanked = true;
    return this;
  }

  public int rankPoi(PoiRS prs) {
    String pName = normalizeTerm(prs.getName());


    int rank = 0;

    if (city != null && prs.getMainAddress() != null && prs.getMainAddress().getLocality() != null) {
      rank += FeedHelperPOI.rankLocality(city, prs.getMainAddress().getLocality());
    }

    if (companyId != null) {
      if (companyId.equals(prs.getCmpyId())) {
        rank += 10;
      }
    }

    List<Integer> ranks = new ArrayList<>();
    ranks.add(rankName(pName, term));
    ranks.add(rankName(pName, city + " " + term));
    ranks.add(rankName(pName, term + " " + city));
    if (prs.getCode() != null) {
      String pCode = normalizeTerm(prs.getCode());
      ranks.add(rankName(pCode, term));
      ranks.add(rankName(pCode, city + " " + term));
      ranks.add(rankName(pCode, term + " " + city));
    }
    ranks.sort((c1, c2) -> c2 - c1);

    return ranks.get(0) + rank;
  }

  /**
   * Takes search term and makes sure that it does not contain any unsupported
   *
   * @param term
   * @return
   */
  public static String normalizeTerm(final String term) {
    return term.toLowerCase()
        //1. Fixing multiple spaces and non-breakable spaces (normal, narrow, figure and word joiner)
        .replaceAll("[\\s\u00A0\u202F\u2007\u2060]+", " ")
            //2. Removing all unsupported characters
        .replaceAll("[-,.;:?'\"`~!]", "");
  }

  /**
   * Ranks feed name as compared to POI name (from 0 to 100)
   *
   * @param a
   * @param b
   * @return 0, if no match
   * 25, if POI name contains feed name
   * 30, if Feed name contains POI name
   * 100, if names are equal
   */
  public static int rankName(String a, String b) {
    a = normalizeTerm(a);
    b = normalizeTerm(b);

    if (a.length() == 0 || b.length() == 0) {
      return 0;
    }

    if (a.equals(b)) {
      return 100;
    }

    if (a.length() > b.length()) {
      if (a.contains(b)) {
        return 30;
      }
    }
    else {
      if (b.contains(a)) {
        return 25;
      }
    }
    return 0;
  }

  public PoiRS getBestFit() {
    if (!isRanked || rankedList == null || rankedList.size() == 0) {
      return null;
    }


    return rankedList.get(0).getRight();
  }

  public PoiSearchHelper setConsortiumId(Integer consortiumId) {
    this.consortiumId = consortiumId;
    return this;
  }

  public PoiSearchHelper setCompanyId(Integer companyId) {
    this.companyId = companyId;
    return this;
  }

  public PoiSearchHelper setMaxRecords(String maxRecords) {
    this.maxRecords = maxRecords;
    return this;
  }

  public PoiSearchHelper setTerm(String term) {
    this.term = term;
    isRanked = false;
    return this;
  }

  /**
   * Idenfies if there is a city name in the search term and splits it out
   *
   * @param term
   * @return
   */
  public PoiSearchHelper setTermSmart(String term) {
    String[] parts = term.split(",");
    switch (parts.length) {
      case 0:
        this.term = null;
        break;
      case 1:
        this.term = term.trim();
        break;
      case 2:
      default:
        this.term = parts[0].trim();
        setCity(parts[1]);
        break;
    }
    isRanked = false;
    return this;
  }

  public int getFoundCount() {
    return resultSet.size();
  }

  public PoiSearchHelper addPoiResults(Collection<PoiRS> moreData) {
    if (resultSet == null) { //Superstition, should never happen
      resultSet = new ArrayList<>();
    }
    if (moreData != null) {
      resultSet.addAll(moreData);
      isRanked = false;
    }
    return this;
  }

  public PoiSearchHelper setCountry(String country) {
    this.country = country;
    isRanked = false;
    return this;
  }

  public PoiSearchHelper setCode(String code) {
    this.code = code;
    isRanked = false;
    return this;
  }
}