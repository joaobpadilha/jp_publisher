package com.mapped.publisher.persistence.clickatell;

/**
 * Mobile Originated callback payload
 * Created by surge on 2016-02-11.
 */
public class MOCallbackData {
  public Integer apiId;
  public String  moMessageId;
  /**
   * Originating ISDN
   */
  public String  from;
  public String  to;
  /**
   * Date and Time [MySQL format, GMT + 0200]
   */
  public String  timestamp;
  public String  charset;
  public String  udh;
  public String  text;

  public String toString() {
    StringBuilder sb = new StringBuilder("Clickatell MO Callback");
    sb.append(System.lineSeparator());
    if (apiId != null) {
      sb.append("apiId: ").append(apiId).append(System.lineSeparator());
    }
    if (moMessageId != null) {
      sb.append("moMessageId: ").append(moMessageId).append(System.lineSeparator());
    }
    if (from != null) {
      sb.append("from: ").append(from).append(System.lineSeparator());
    }
    if (to != null) {
      sb.append("to: ").append(to).append(System.lineSeparator());
    }
    if (timestamp != null) {
      sb.append("timestamp: ").append(timestamp).append(System.lineSeparator());
    }
    if (charset != null) {
      sb.append("charset: ").append(charset).append(System.lineSeparator());
    }
    if (udh != null) {
      sb.append("udh: ").append(udh).append(System.lineSeparator());
    }
    if (text != null) {
      sb.append("text: ").append(text).append(System.lineSeparator());
    }
    return sb.toString();
  }
}
