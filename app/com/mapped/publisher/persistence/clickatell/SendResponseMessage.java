package com.mapped.publisher.persistence.clickatell;

/**
 * Created by surge on 2016-02-10.
 */
public class SendResponseMessage {
  public Boolean accepted;
  public String to;
  public String apiMessageId;
}
