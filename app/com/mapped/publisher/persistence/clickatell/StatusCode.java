package com.mapped.publisher.persistence.clickatell;

/**
 * Clickatell
 * https://www.clickatell.com/developers/api-docs/message-status-codes/
 * Created by surge on 2016-02-09.
 */
public enum StatusCode {
  MESSAGE_UNKNOWN(1,
                  "Message unknown",
                  "The message ID is incorrect, not found or reporting is delayed. Note that a message can only be " +
                  "queried up to six " + "days after it has been submitted to our gateway."),
  MESSAGE_QUEUED(2,
                 "Message queued",
                 "The message could not be delivered and has been queued for attempted redelivery."),
  DELIVERED_TO_GATEWAY(3,
                       "Delivered to gateway",
                       "Delivered to the upstream gateway or network (delivered to the recipient)."),
  RECEIVED_BY_RECEPIENT(4, "Received by recipient", "Confirmation of receipt on the handset of the recipient."),
  ERROR_W_MESSAGE(5,
                  "Error with message",
                  "There was an error with the message, probably caused by the content of the message itself."),
  CANCELLED_USER(6, "User cancelled message delivery", "The message was terminated by a user (stop message command)."),
  ERROR_TO_DELIVER(7, "Error delivering message", "An error occurred delivering the message to the handset."),
  ERROR_ROUTING(9, "Routing error", "An error occurred while attempting to route the message."),
  MESSAGE_EXPIRED(10,
                  "Message expired",
                  "Message expired before we were able to deliver it to the upstream gateway. No charge applies."),
  MESSAGE_SCHEDULED(11,
                    "Message scheduled for later delivery",
                    "Message has been scheduled for delivery at a later time (delayed delivery feature)."),
  OUT_OF_CREDIT(12, "Out of credit", "The message cannot be delivered due to insufficient credits."),

  CANCELLED_CLICKATELL(13, "Clickatell cancelled message delivery",

                       "The message was terminated by our staff."),
  MT_EXCEEDED(14, "Maximum MT limit exceeded", "The allowable amount for MT messaging has been exceeded."),
  UNDEFINED(0, "Error not specified", "");


  int    code;
  String desc;
  String detail;

  StatusCode(int code, String desc, String detail) {
    this.code = code;
    this.desc = desc;
    this.detail = detail;
  }

  public static StatusCode fromCode(String code){
    try {
      int intVal = Integer.parseInt(code);
      return fromCode(intVal);
    } catch (Exception e) {
      return UNDEFINED;
    }
  }

  public static StatusCode fromCode(int code) {
    for (StatusCode c : StatusCode.values()) {
      if (code == c.code) {
        return c;
      }
    }
    return UNDEFINED;
  }
}
