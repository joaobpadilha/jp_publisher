package com.mapped.publisher.persistence;

import com.google.inject.Inject;
import com.mapped.common.CoreConstants;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.S3Util;
import com.umapped.api.schema.local.PoiJson;
import com.umapped.api.schema.types.Address;
import com.umapped.api.schema.types.Coordinates;
import com.umapped.api.schema.types.Feature;
import com.umapped.api.schema.types.PhoneNumber;
import com.umapped.external.travelbound.TravelboundAPICredential;
import com.umapped.external.travelbound.TravelboundAPIException;
import com.umapped.external.travelbound.TravelboundPoiImportLoader;
import com.umapped.external.travelbound.TravelboundWebService;
import com.umapped.external.travelbound.api.*;
import models.publisher.FeedSrc;
import models.publisher.PoiSrcFeed;
import org.apache.commons.codec.language.Metaphone;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.util.CollectionUtils;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.util.*;

/**
 * Created by wei on 2017-05-18.
 */
public class FeedHelperTravelboundHotel
    extends FeedHelperPOI {

  public final static String TRAVELBOUND_S3_PREFIX = "feeds/Travelbound/";

  public static String FEATURE_LOCATION = "Location";
  public static String FEATURE_AREA = "Area";
  public static String FEATURE_ROOM = "Rooms";
  public static String FEATURE_HOTEL = "Hotels";
  public static String FEATURE_RATING = "Rating";

  private static String[] NonEssentialWordInName = {"hotel"};

  private static String[] NonEssentialWordInAddress = {"no ",};

  @Inject
  static TravelboundWebService travelboundWebService;

  private JAXBContext jaxbContext;

  public FeedHelperTravelboundHotel() {
    try {
      jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
    }
    catch (JAXBException e) {
      Log.err("Fail to create JAXBContext", e);
      throw new RuntimeException(e);
    }
  }

  @Override public List<FeedResource> getResources() {
    List<FeedResource> result = new ArrayList<>();
    List<String> subFolders = S3Util.getCommonPrefixes(S3Util.getPDFBucketName(), TRAVELBOUND_S3_PREFIX);
    for (String f : subFolders) {
      FeedResource resource = new FeedResource();
      resource.setDate(new Date());
      resource.setName(f.substring(TRAVELBOUND_S3_PREFIX.length()));
      resource.setOrigin(FeedResource.Origin.AWS_S3);
      long size = S3Util.getObjecCount(S3Util.getPDFBucketName(), f);
      resource.setSize(size-1);
      result.add(resource);
    }
    return result;
  }

  @Override public Map<PoiFeedUtil.FeedResult, Integer> load(String parameter) {
    StopWatch sw = new StopWatch();
    sw.start();
    int errors = 0;
    int success = 0;
    String path = TRAVELBOUND_S3_PREFIX + parameter;

    Map<PoiFeedUtil.FeedResult, Integer> results = new HashMap<>();

    TravelboundPoiImportLoader loader = new TravelboundPoiImportLoader();

    try {
      Map<String, List<String>> countryCityMap = getCityMap();

      for (String country : countryCityMap.keySet()) {
          Pair<Integer, Integer> countryResult = loader.load(country, countryCityMap.get(country), path);
          success += countryResult.getLeft();
          errors += countryResult.getRight();
      }
    }
    catch (Exception e) {
      Log.err("Fail to load travelbound feed", e);
    }
    results.put(PoiFeedUtil.FeedResult.SUCCESS, success);
    results.put(PoiFeedUtil.FeedResult.ERROR, errors);

    long t2 = System.currentTimeMillis();

    Log.debug("Travelbound Import Total load time: " + sw.getTime());
    return results;
  }

  private TravelboundAPICredential getUmappedCredential() {
    String credential = ConfigMgr.getAppParameter(CoreConstants.TRAVELBOUND_UMAPPED_CREDENTIAL);
    if (!StringUtils.isEmpty(credential)) {
      String[] umappedCredential = StringUtils.split(credential, ':');
      if (umappedCredential != null && umappedCredential.length == 4) {
        return new TravelboundAPICredential(Integer.parseInt(umappedCredential[0]), umappedCredential[1], umappedCredential[2], umappedCredential[3]);
      }
    }
    return null;
  }

  private Map<String, List<String>> getCityMap()
      throws TravelboundAPIException {
    StopWatch sw = new StopWatch();
    sw.start();
    Map<String, List<String>> countryCityMap = new HashMap<>();
    TravelboundAPICredential credential = getUmappedCredential();
    if (credential == null) {
      throw new TravelboundAPIException("NO credential is configured to access travelbound API");
    }
    int cityCount = 0;
    List<String> countries = travelboundWebService.getCountries(credential);

    for (String c : countries) {
      List<String> cities = travelboundWebService.getCities(credential, c);
      countryCityMap.put(c, cities);
      cityCount += cities.size();
    }
    Log.debug("Get Total city:" + cityCount + ", took " + sw.getTime());
    return countryCityMap;
  }

  @Override public PoiImportRS.ImportState associate(PoiImportRS importRS) {
    List<Pair<PoiRS, Integer>> scores = getRankedPois(importRS, false);

    if (scores.size() == 0 || scores.get(0).getRight() <= 0) {
      Log.debug("Found no matches for:" + importRS.name + " from: " + importRS.countryCode);
      importRS.state = PoiImportRS.ImportState.MISMATCH;
      importRS.poiId = null;
      importRS.score = 0;
    }
    else {
      // there are many hotels share the same name
      // we need to match it more strictly than just name, also locality and address
      int score = scores.get(0).getRight();
      importRS.poiId = scores.get(0).getLeft().getId();
      if (score >= 50) {
        importRS.state = PoiImportRS.ImportState.MATCH;
      }
      else {
        if (score < 30) {
          importRS.state = PoiImportRS.ImportState.MISMATCH;
        }
        else {
          importRS.state = PoiImportRS.ImportState.UNSURE;
        }
      }
      importRS.score = score;
    }

    PoiImportMgr.update(importRS);
    return importRS.state;
  }

  @Override public void processDuplicateRecord(int srcId) {
    PoiImportMgr.setToUnprocessedForLowScoreDuplicate(srcId);
  }

  @Override public PoiSrcFeed.FeedSyncResult synchronize(PoiSrcFeed psf) {
    PoiSrcFeed.FeedSyncResult result = PoiSrcFeed.FeedSyncResult.FAIL;
    try {
      Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
      StreamSource source = new StreamSource();
      source.setReader(new StringReader(psf.getRaw()));
      JAXBElement<TItemDetail> element = unmarshaller.unmarshal(source, TItemDetail.class);
      TItemDetail itemDetail = element.getValue();
      THotelInformation hotelInformation = itemDetail.getHotelInformation();

      FeedSrc feedSrc = FeedSrc.find.byId(psf.getFeedId().srcId);
      List<PoiRS> poiRecs = PoiMgr.findAllById(psf.getFeedId().poiId);

      PoiRS rsToUpdate = null;
      for (PoiRS p : poiRecs) {
        if (p.getCmpyId() == feedSrc.getCmpyId() && p.consortiumId == feedSrc.getConsortiumId() && p.getSrcId() ==
                                                                                                   feedSrc
            .getSrcId()) {
          rsToUpdate = p;
        }
      }

      if (rsToUpdate == null) { //Creating a new POI record
        rsToUpdate = PoiRS.buildRecord(feedSrc.getConsortiumId(),
                                       feedSrc.getCmpyId(),
                                       feedSrc.getTypeId(),
                                       feedSrc.getSrcId(),
                                       "system");
        updatePoi(rsToUpdate, psf, itemDetail, feedSrc);
        rsToUpdate.setId(psf.getFeedId().poiId);
        PoiMgr.save(rsToUpdate);
        result = PoiSrcFeed.FeedSyncResult.NEW;
      }
      else { //updating existing one
        updatePoi(rsToUpdate, psf, itemDetail, feedSrc);
        PoiMgr.update(rsToUpdate);
        result = PoiSrcFeed.FeedSyncResult.UPDATED;
      }
      psf.setSyncTs(System.currentTimeMillis());
      psf.setSynced(true);
      psf.update();
    }
    catch (Exception e) {
      Log.err("Failure to load travelbound feed", e);
    }
    return result;
  }

  private PoiRS updatePoi(PoiRS poi, PoiSrcFeed feedRec, TItemDetail hotelInfo, FeedSrc src) {
    poi.setSrcReference(feedRec.getSrcReference());

    poi.setName(hotelInfo.getItem().getValue());
    Address address = new Address();

    THotelInformation hotelInformation = hotelInfo.getHotelInformation();

    if (hotelInformation != null) {

      if (hotelInformation.getGeoCodes() != null) {
        Coordinates coordinates = new Coordinates();
        coordinates.setLatitude((float) hotelInformation.getGeoCodes().getLatitude());
        coordinates.setLongitude((float) hotelInformation.getGeoCodes().getLongitude());
        address.setCoordinates(coordinates);
      }

      TAddressLines addressLines = hotelInformation.getAddressLines();

      if (addressLines != null) {

        address.setRegion(addressLines.getAddressLine3());
        address.setLocality(hotelInfo.getCity().getValue());
        if (StringUtils.isEmpty(addressLines.getAddressLine2())) {
          address.setStreetAddress(addressLines.getAddressLine1());
        }
        else {
          address.setStreetAddress(StringUtils.join(addressLines.getAddressLine1(), ", ", addressLines
              .getAddressLine2()));
        }
        if (!StringUtils.isEmpty(addressLines.getTelephone())) {
          PhoneNumber mainPhone = new PhoneNumber();
          mainPhone.setNumber(addressLines.getTelephone());
          poi.setMainPhoneNumber(mainPhone);
        }
        if (!StringUtils.isEmpty(addressLines.getWebSite())) {
          Set<String> urls = new HashSet<>();
          urls.add(addressLines.getWebSite());
          poi.data.withUrls(urls);
        }
      }
    }
    poi.setMainAddress(address);


    updateFeatures(hotelInfo, poi.data);

    return poi;
  }

  private Feature buildFeature(String name, String desc) {
    Feature feature = new Feature();
    feature.setName(name);
    feature.setDesc(desc);
    return feature;
  }

  private void updateFeatures(TItemDetail hotel, PoiJson data) {
    Set<Feature> features = new HashSet<>();

    List<String> locationFeature = getLocationFeatures(hotel.getLocationDetails());
    if (!CollectionUtils.isEmpty(locationFeature)) {
      features.add(buildFeature(FEATURE_LOCATION, StringUtils.join(locationFeature, "\n")));
    }

    THotelInformation hotelInformation = hotel.getHotelInformation();

    List<String> areaFeatures = getAreaFeatures(hotelInformation.getAreaDetails());
    if (!CollectionUtils.isEmpty(areaFeatures)) {
      features.add(buildFeature(FEATURE_AREA, StringUtils.join(areaFeatures, "\n")));
    }

    List<String> roomFeatures = getFacilityFeature(hotelInformation.getRoomFacilities());
    if (!CollectionUtils.isEmpty(roomFeatures)) {
      features.add(buildFeature(FEATURE_ROOM, StringUtils.join(roomFeatures, "\n")));
    }

    List<String> hotelFeatures = getFacilityFeature(hotelInformation.getFacilities());
    if (!CollectionUtils.isEmpty(hotelFeatures)) {
      features.add(buildFeature(FEATURE_HOTEL, StringUtils.join(hotelFeatures, "\n")));
    }

    TStarRating starRating = hotelInformation.getStarRating();
    if (starRating != null) {
      features.add(buildFeature(FEATURE_RATING, starRating.getValue()));
    }

    data.setFeatures(features);
  }

  private List<String> getLocationFeatures(TLocationDetails locationDetails) {
    List<String> features = new ArrayList<>();
    if (locationDetails != null) {
      for (TLocation loc: locationDetails.getLocations()) {
        features.add(loc.getValue());
      }
    }
    return features;
  }

  private List<String> getAreaFeatures(TAreaDetails areaDetails) {
    List<String> features = new ArrayList<>();
    if (areaDetails != null) {
      for (String area: areaDetails.getAreaDetails()) {
        features.add(area);
      }
    }
    return features;
  }

  private List<String> getFacilityFeature(TFacilities facilities) {
    List<String> features = new ArrayList<>();
    if (facilities != null) {
      for (TFacility f : facilities.getFacilities()) {
        features.add(f.getValue());
      }
    }
    return features;
  }

  @Override public List<Pair<PoiRS, Integer>> getRankedPois(PoiImportRS importRS, boolean ignoreCountry) {
    String cleanName = importRS.name;
    List<PoiRS> pois;
    if (ignoreCountry) {
      pois = PoiMgr.findForMerge(cleanName, null, importRS.typeId);
    }
    else {
      pois = PoiMgr.findForMerge(cleanName, importRS.countryCode, importRS.typeId);
    }

    List<Pair<PoiRS, Integer>> scores = new ArrayList<>();
    for (PoiRS prs : pois) {
      // only trust public company information
      if (prs.getCmpyId() == 0) {
        int currScore = rankPoi(cleanName, importRS, prs);
        scores.add(new ImmutablePair<>(prs, currScore));
      }
    }

    Collections.sort(scores, (o1, o2) -> o2.getRight() - o1.getRight());

    return scores;
  }

  protected int rankPoi(String cleanName, PoiImportRS importRS, PoiRS prs) {
    int score = 0;
    score += scoreName(cleanName, prs.getName());

    if (prs.getMainPhone() != null) {
      score += rankPhone(prs.getMainPhone().getNumber(), importRS.phone);
    }

    Address address = prs.getMainAddress();
    if (address != null) {
      if (address.getStreetAddress() != null && importRS.address != null) {
        score += scoreAddress(address.getStreetAddress(), importRS.address);
      }

      score += rankCity(address.getLocality(), importRS.city);

      if (!StringUtils.equals(prs.countryCode, importRS.countryCode)) {
        score -= 50;
      }
    }

    //If record has some resemblance, give it a notch if it is a public record.
    if (score > 0 && prs.getCmpyId() == 0) {
      score += 5;
    }

    return score;
  }

  private int scoreAddress(String feedAddress, String poiAddress) {
    String feedNormalizedAddress = feedAddress.toLowerCase().replaceAll("\\W+", " ").trim();
    String poiNormalizedAddress = poiAddress.toLowerCase().replaceAll("\\W+", " ").trim();

    if (StringUtils.isEmpty(feedNormalizedAddress) || StringUtils.isEmpty(poiNormalizedAddress)) {
      return 0;
    }

    if (feedNormalizedAddress.equals(poiNormalizedAddress)) {
      return 50;
    } else if (feedNormalizedAddress.length() > poiNormalizedAddress.length() && feedNormalizedAddress.contains(poiNormalizedAddress)) {
      return 40;
    } else if (poiNormalizedAddress.length() > feedNormalizedAddress.length() && poiNormalizedAddress.contains(feedNormalizedAddress)) {
      return 40;
    }

    feedNormalizedAddress = removeNonEssentialWords(feedNormalizedAddress, NonEssentialWordInAddress);
    poiNormalizedAddress = removeNonEssentialWords(poiNormalizedAddress, NonEssentialWordInAddress);


    String feedAddressComps[] = StringUtils.split(feedNormalizedAddress);
    String poiAddressComps[] = StringUtils.split(poiNormalizedAddress);

    int score = 0;
    int length = Math.min(feedAddressComps.length, poiAddressComps.length);
    if (length > 0) {
      int nthNumber = 0;
      for (int i = 0; i < length; i++) {
        if (StringUtils.equals(feedAddressComps[i], poiAddressComps[i])) {
          if (StringUtils.isNumeric(feedAddressComps[i]) && feedAddressComps[i].length() > 1) {
            nthNumber++;
            // give first number higher weight
            if (nthNumber == 1) {
              score += 30;
            } else {
              score +=10;
            }
          } else {
            score += 5;
          }
        }
      }
    }

    if (score < 30)  {
      //Relaxing restrictions
      Metaphone metaphone = new Metaphone();

      metaphone.setMaxCodeLen(METAPHONE_LEN);

      String feedAddressLetters = feedNormalizedAddress.replaceAll("[^a-zA-Z]", "");
      String poiAddressLetters = poiNormalizedAddress.replaceAll("[^a-zA-Z]", "");
      boolean metaPhoneCompare = metaphone.isMetaphoneEqual(feedAddressLetters, poiAddressLetters);
      if (metaPhoneCompare) {
        score = 30;
      }
    }
    return score > 50 ? 50 : score;
  }


  private String removeNonEssentialWords(String value, String[] nonEssentialWords) {
    String newValue = value;
    for (String w : nonEssentialWords) {
      newValue = newValue.replaceAll(w, "");
    }
    return StringUtils.trimToEmpty(newValue);
  }

  private int scoreName(String feedName, String poiName) {
    feedName = feedName.toLowerCase().replaceAll("\\W+", " ");
    poiName = poiName.toLowerCase().replaceAll("\\W+", " ");

    if (feedName.length() == 0 || poiName.length() == 0) {
      return 0;
    }

    if (feedName.equals(poiName)) {
      return 45;
    }

    String[] feedNameComp = StringUtils.split(feedName);
    String[] poiNameComp = StringUtils.split(poiName);

    int count = Math.min(feedNameComp.length, poiNameComp.length);
    int longer = Math.max(feedNameComp.length, poiNameComp.length);
    int increment = Math.min(5, 45 / longer);
    int score = 0;

    for (int i = 0; i < feedNameComp.length; i++) {
      if (wordInList(feedNameComp[i], poiNameComp)) {
        score += increment;
      }
    }
    return score > 45 ? 45 : score;
  }

  private boolean wordInList(String word, String[] list) {
    for (String w :  list) {
      if (StringUtils.equals(word, w)) {
        return true;
      }
    }
    return false;
  }

  // travelbound hotel always has city, should be important
  private int rankCity(String locA, String locB) {
    // Without locality from POI, lower the score
    if (locA == null || locB == null) {
      return -10;
    }

    locA = locA.toLowerCase().replaceAll("\\W+", "");
    locB = locB.toLowerCase().replaceAll("\\W+", "");

    // POI without city, lower the score
    if (locA.length() == 0 || locB.length() == 0) {
      return -10;
    }

    if (locA.equals(locB)) {
      return 15;
    }

    if  (locA.length() > locB.length() ? locA.contains(locB) : locB.contains(locA)) {
      return 10;
    }

    // not match at all
    return -10;
  }

  @Override public PoiSrcFeed.FeedSyncResult clearAmenities(PoiSrcFeed psf) {
    return null;
  }
}
