package com.mapped.publisher.persistence;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.umapped.api.schema.local.PoiJson;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.postgresql.util.PGobject;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Helper abstractions for POI table
 * Created by surge on 2014-10-14.
 */
public class PoiMgr {
  final static boolean DEBUG = false;
  final static String SQL_INSERT_POI;
  final static String SQL_FIND_POI_BY_ID;
  final static String SQL_FIND_ALL_POI_FOR_ID;
  final static String SQL_FIND_POI_PORT_BY_GPS;
  final static String SQL_FIND_POI_BY_PK;
  final static String SQL_FIND_POI_BY_NAME_CMPY_CONS_TYPE_SRC;

  final static String SQL_UPDATE_POI;
  final static String SQL_DELETE_POI;
  final static String SQL_FIND_RECENT;
  final static String SQL_FIND_BY_TERM;
  final static String SQL_FIND_FOR_MERGE;
  final static String SQL_FIND_FOR_MERGE_EVERYWHERE;
  final static String SQL_FIND_BY_TERM_AND_KEYWORDS;
  final static String SQL_FIND_BY_CODE_ONLY;
  final static String SQL_FIND_POI_FOR_CMPY;
  final static String SQL_POI_USE_COUNT;
  final static String SQL_FIND_SHIPS_NO_FILES;
  final static String SQL_FIND_POI_PORT_WITH_CODE;
  final static String SQL_DELETE_DANGLING_POI_HARD;
  final static String SQL_DELETE_DANGLING_POI_SOFT;

  final static String SQL_FIND_BY_TAG;
  final static String SQL_FIND_MATCH_BY_TAG_SRC_REFERENCE;
  final static String SQL_FIND_MATCH_BY_SRC_REFERENCE;


  static {
    SQL_DELETE_DANGLING_POI_HARD =  "DELETE FROM poi " +
                                    "WHERE src_id = ? AND poi_id IN (SELECT poi_id" +
                                    "                                  FROM poi" +
                                    "                                  WHERE poi_id NOT IN (SELECT poi_id" +
                                    "                                                       FROM poi_src_feed" +
                                    "                                                       WHERE src_id = ?) AND" +
                                    "                                        src_id = ?)";


    SQL_DELETE_DANGLING_POI_SOFT = "UPDATE poi SET  state = 'DELETED', lastupdatedtimestamp = ?, modifiedby = ? " +
                                   "WHERE src_id = ? AND poi_id IN  (SELECT poi_id " +
                                   "                                    FROM poi " +
                                   "                                    WHERE poi_id NOT IN (SELECT poi_id " +
                                   "                                                         FROM poi_src_feed " +
                                   "                                                         WHERE src_id = ?) AND " +
                                   "                                          src_id = ?)";


    SQL_FIND_POI_PORT_BY_GPS = "SELECT poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, src_reference, " +
                         "country_code, loc_lat, loc_long, file_count, data, state, createdtimestamp, " +
                         "lastupdatedtimestamp, createdby, modifiedby, version " +
                         "FROM poi p " +
                         "WHERE p.cmpy_id = 0 and p.consortium_id = 0 and p.state = 'ACTIVE' and p.type_id = 7 " +
                         " and ( p.name = ? or  " +
                         "      ((p.loc_lat between ? and ?) " +
                         "      and (p.loc_long between ? and ?)) " +
                         " )";

    SQL_FIND_POI_PORT_WITH_CODE = "SELECT poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, src_reference, " +
                               "country_code, loc_lat, loc_long, file_count, data, state, createdtimestamp, " +
                               "lastupdatedtimestamp, createdby, modifiedby, version " +
                               "FROM poi p " +
                               "WHERE p.cmpy_id = 0 and p.consortium_id = 0 and p.state = 'ACTIVE' and p.type_id = 7 " +
                               " and p.code is not null and length(code) = 3 " +
                               " order by p.name asc";

    SQL_FIND_SHIPS_NO_FILES = "SELECT poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, src_reference, " +
                               "country_code, loc_lat, loc_long, file_count, data, state, createdtimestamp, " +
                               "lastupdatedtimestamp, createdby, modifiedby, version " +
                               "FROM poi p " +
                               "WHERE p.cmpy_id = 0 and p.consortium_id = 0 and p.state = 'ACTIVE' and p.type_id = 6 " +
                               " and p.file_count = 0;";

    SQL_FIND_POI_BY_ID = "SELECT poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, src_reference, " +
                         "country_code, loc_lat, loc_long, file_count, data, state, createdtimestamp, " +
                         "lastupdatedtimestamp, createdby, modifiedby, version " +
                         "FROM poi p " +
                         "WHERE p.poi_id = ? AND ((p.cmpy_id = ANY(?) AND consortium_id = 0) OR " +
                         "      p.consortium_id IN (SELECT cons_id " +
                         "                          FROM consortium_company " +
                         "                          WHERE cmpy_id = ANY(?)))";
    SQL_FIND_ALL_POI_FOR_ID = "SELECT poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, src_reference, " +
                              "country_code, loc_lat, loc_long, file_count, data, state, createdtimestamp, " +
                              "lastupdatedtimestamp, createdby, modifiedby, version " +
                              "FROM poi p " +
                              "WHERE p.poi_id = ?";

    SQL_FIND_POI_FOR_CMPY =  "SELECT poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, src_reference, " +
                             "country_code, loc_lat, loc_long, file_count, data, state, createdtimestamp, " +
                             "lastupdatedtimestamp, createdby, modifiedby, version " +
                             "FROM poi " +
                             "WHERE poi_id = ? AND cmpy_id = ?";

    SQL_FIND_POI_BY_PK = "SELECT poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, src_reference, " +
        "country_code, loc_lat, loc_long, file_count, data, state, createdtimestamp, " +
        "lastupdatedtimestamp, createdby, modifiedby, version " +
        "FROM poi p " +
        "WHERE p.poi_id = ? AND p.cmpy_id = ? AND p.consortium_id = ? AND p.src_id = ?";

    SQL_FIND_POI_BY_NAME_CMPY_CONS_TYPE_SRC = "SELECT poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, src_reference, " +
        "country_code, loc_lat, loc_long, file_count, data, state, createdtimestamp, " +
        "lastupdatedtimestamp, createdby, modifiedby, version " +
        "FROM poi p " +
        "WHERE p.name like ?  AND p.cmpy_id = ? AND p.consortium_id = ? AND p.type_id = ? AND p.src_id = ?";

    SQL_INSERT_POI = "INSERT INTO poi (poi_id, consortium_id, cmpy_id, type_id, name, code, tags, src_id, src_reference, " +
                     "country_code, loc_lat, loc_long, file_count, data, state, search_words, createdtimestamp," +
                     "lastupdatedtimestamp, createdby, modifiedby, version) " +
                     "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, cast(? as um_type_state), ?, ?, ?, ?, ?, ?)";

    SQL_UPDATE_POI = "UPDATE poi " +
                     "SET poi_id = ?, consortium_id = ?, cmpy_id = ?, type_id = ?, name = ?, code = ?, tags = ?, " +
                     "src_id = ?, src_reference = ?, country_code = ?, loc_lat = ?, loc_long = ?, file_count = ?, " +
                     "data = ?, state = cast(? as um_type_state), search_words = ?, lastupdatedtimestamp = ?, " +
                     "modifiedby = ?, version = ? " +
                     "WHERE poi_id = ? and consortium_id = ? and cmpy_id = ? and src_id = ?";

    SQL_DELETE_POI = "DELETE FROM poi " + "WHERE poi_id = ? and consortium_id = ? and cmpy_id = ? and src_id = ? ";

    SQL_FIND_RECENT = "SELECT p.poi_id as poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, src_reference, " +
                      "country_code, loc_lat, loc_long, file_count, data, state, createdtimestamp, " +
                      "lastupdatedtimestamp, createdby, modifiedby, version " +
                      "FROM poi p INNER JOIN (SELECT poi_id " +
                      "                       FROM poi " +
                      "                       WHERE state = 'ACTIVE' AND " +
                      "                             ((cmpy_id = ANY(?) AND consortium_id = 0) OR " +
                      "                              consortium_id IN (SELECT cons_id " +
                      "                                                  FROM consortium_company " +
                      "                                                  WHERE cmpy_id = ANY(?))) " +
                      "                       ORDER BY lastupdatedtimestamp DESC, cmpy_id DESC " +
                      "                       LIMIT ?) r " +
                      "            ON p.poi_id = r.poi_id" +
                      "WHERE (p.cmpy_id = ANY(?) AND consortium_id = 0) OR " +
                      "      p.consortium_id IN (SELECT cons_id " +
                      "                          FROM consortium_company " +
                      "                          WHERE cmpy_id = ANY(?)) ";


    SQL_FIND_BY_TERM = "SELECT p.poi_id as poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, " +
                       "        src_reference, country_code, loc_lat, loc_long, file_count, data, state, " +
                       "        createdtimestamp, lastupdatedtimestamp, createdby, modifiedby, version  " +
                       "FROM poi p INNER JOIN (SELECT poi_id " +
                       "                       FROM poi " +
                       "                       WHERE state = 'ACTIVE' AND " +
                       "                             type_id = ANY(?) AND  " +
                       "                             (name ILIKE unaccent(?) OR " +
                       "                              (to_tsvector('english',name) @@ plainto_tsquery('english', ?)) OR " +
                       "                               code = ? OR tags ilike ?) AND " +
                       "                             ((cmpy_id = ANY(?) AND consortium_id = 0) OR " +
                       "                               consortium_id IN (SELECT cons_id " +
                       "                                                  FROM consortium_company " +
                       "                                                  WHERE cmpy_id = ANY(?))) " +
                       "                       ORDER BY cmpy_id DESC " +
                       "                       LIMIT ?) r  " +
                       "           ON p.poi_id = r.poi_id " +
                       "WHERE (p.cmpy_id = ANY(?) AND consortium_id = 0) OR " +
                       "      p.consortium_id IN (SELECT cons_id " +
                       "                          FROM consortium_company " +
                       "                          WHERE cmpy_id = ANY(?)) " +
                       "ORDER BY name ASC";

    SQL_FIND_BY_TAG = "SELECT p.poi_id as poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, " +
                       "        src_reference, country_code, loc_lat, loc_long, file_count, data, state, " +
                       "        createdtimestamp, lastupdatedtimestamp, createdby, modifiedby, version  " +
                       "FROM poi p INNER JOIN (SELECT poi_id " +
                       "                       FROM poi " +
                       "                       WHERE state = 'ACTIVE' AND " +
                       "                             type_id = ANY(?) AND  " +
                       "                             upper(tags) LIKE ?  AND " +
                       "                             ((cmpy_id = ANY(?) AND consortium_id = 0) OR " +
                       "                               consortium_id IN (SELECT cons_id " +
                       "                                                  FROM consortium_company " +
                       "                                                  WHERE cmpy_id = ANY(?))) " +
                       "                       ORDER BY cmpy_id DESC " +
                       "                       LIMIT ?) r  " +
                       "           ON p.poi_id = r.poi_id " +
                       "WHERE (p.cmpy_id = ANY(?) AND consortium_id = 0) OR " +
                       "      p.consortium_id IN (SELECT cons_id " +
                       "                          FROM consortium_company " +
                       "                          WHERE cmpy_id = ANY(?)) " +
                       "ORDER BY name ASC";

    SQL_FIND_MATCH_BY_TAG_SRC_REFERENCE = "SELECT p.poi_id as poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, " +
            "        src_reference, country_code, loc_lat, loc_long, file_count, data, state, " +
            "        createdtimestamp, lastupdatedtimestamp, createdby, modifiedby, version  " +
            "FROM poi p INNER JOIN (SELECT poi_id " +
            "                       FROM poi " +
            "                       WHERE state = 'ACTIVE' AND " +
            "                             type_id = ANY(?) AND  " +
            "                             (upper(cast(data->'tags' as text)) LIKE ? OR upper(src_reference) LIKE ?) AND " +
            "                             ((cmpy_id = ANY(?) AND consortium_id = 0) OR " +
            "                               consortium_id IN (SELECT cons_id " +
            "                                                  FROM consortium_company " +
            "                                                  WHERE cmpy_id = ANY(?))) " +
            "                       ORDER BY cmpy_id DESC " +
            "                       LIMIT ?) r  " +
            "           ON p.poi_id = r.poi_id " +
            "WHERE (p.cmpy_id = ANY(?) AND consortium_id = 0) OR " +
            "      p.consortium_id IN (SELECT cons_id " +
            "                          FROM consortium_company " +
            "                          WHERE cmpy_id = ANY(?)) " +
            "ORDER BY name ASC";

    SQL_FIND_MATCH_BY_SRC_REFERENCE = "SELECT p.poi_id as poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, " +
                                      "        src_reference, country_code, loc_lat, loc_long, file_count, data, state, " +
                                      "        createdtimestamp, lastupdatedtimestamp, createdby, modifiedby, version  " +
                                      "FROM poi p INNER JOIN (SELECT poi_id " +
                                      "                       FROM poi " +
                                      "                       WHERE state = 'ACTIVE' AND " +
                                      "                             type_id = ANY(?) AND  " +
                                      "                             upper(src_reference) LIKE ?  AND " +
                                      "                             ((cmpy_id = ANY(?) AND consortium_id = 0) OR " +
                                      "                               consortium_id IN (SELECT cons_id " +
                                      "                                                  FROM consortium_company " +
                                      "                                                  WHERE cmpy_id = ANY(?))) " +
                                      "                       ORDER BY cmpy_id DESC " +
                                      "                       LIMIT ?) r  " +
                                      "           ON p.poi_id = r.poi_id " +
                                      "WHERE (p.cmpy_id = ANY(?) AND consortium_id = 0) OR " +
                                      "      p.consortium_id IN (SELECT cons_id " +
                                      "                          FROM consortium_company " +
                                      "                          WHERE cmpy_id = ANY(?)) " +
                                      "ORDER BY name ASC";

    SQL_FIND_BY_CODE_ONLY = "SELECT p.poi_id as poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, " +
                            "        src_reference, country_code, loc_lat, loc_long, file_count, data, state, " +
                            "        createdtimestamp, lastupdatedtimestamp, createdby, modifiedby, version " +
                            "FROM poi p INNER JOIN (SELECT DISTINCT poi_id " +
                            "                       FROM poi " +
                            "                       WHERE state = 'ACTIVE' AND " +
                            "                             cmpy_id = ANY(?) AND " +
                            "                             type_id = ANY(?) AND " +
                            "                             code ILIKE ?" +
                            "                       LIMIT ?) r " +
                            "           ON p.poi_id = r.poi_id " +
                            "WHERE (p.cmpy_id = ANY(?) AND consortium_id = 0) OR " +
                            "      p.consortium_id IN (SELECT cons_id " +
                            "                          FROM consortium_company " +
                            "                          WHERE cmpy_id = ANY(?)) " +
                            "ORDER BY name ASC";

    SQL_FIND_BY_TERM_AND_KEYWORDS =
        "SELECT p.poi_id as poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, " +
        "        src_reference, country_code, loc_lat, loc_long, file_count, data, state, " +
        "        createdtimestamp, lastupdatedtimestamp, createdby, modifiedby, version " +
        "FROM poi p INNER JOIN (SELECT DISTINCT poi_id " +
        "                       FROM poi " +
        "                       WHERE state = 'ACTIVE' AND " +
        "                             type_id = ANY(?) AND " +
        "                             (name ILIKE ? OR " +
        "                              to_tsvector('english',name) @@ plainto_tsquery('english', ?) OR " +
        "                              code = ?) AND" +
        "                              (search_words ILIKE ? OR " +
        "                               (to_tsvector('english', search_words) @@ plainto_tsquery('english', ?))) AND " +
        "                             ((cmpy_id = ANY(?) AND consortium_id = 0) OR " +
        "                               consortium_id IN (SELECT cons_id " +
        "                                                  FROM consortium_company " +
        "                                                  WHERE cmpy_id = ANY(?))) " +
        "                       LIMIT ?) r " +
        "           ON p.poi_id = r.poi_id " +
        "WHERE (p.cmpy_id = ANY(?) AND consortium_id = 0) OR " +
        "      p.consortium_id IN (SELECT cons_id " +
        "                          FROM consortium_company " +
        "                          WHERE cmpy_id = ANY(?)) " +
        "ORDER BY name ASC";

    SQL_POI_USE_COUNT = "SELECT ( " +
                        "SELECT COUNT(*) as bkngs " +
                        "FROM trip_detail td " +
                        "WHERE (poi_id = ? AND poi_cmpy_id = ?) OR " +
                        "      (loc_start_poi_id = ? AND loc_start_poi_cmpy_id = ?) OR " +
                        "      (loc_finish_poi_id = ? AND loc_finish_poi_cmpy_id = ?)) AS bookings," +
                        "(SELECT COUNT(*) as tmplts " +
                        "FROM tmplt_details " +
                        "WHERE (poi_id = ? AND poi_cmpy_id = ?) OR " +
                        "      (loc_start_poi_id = ? AND loc_start_poi_cmpy_id = ?) OR " +
                        "      (loc_finish_poi_id = ? AND loc_finish_poi_cmpy_id = ?)) as templates";
  }

  /**
   * Moved to eBean TripDetail.getPoiUseCount and TmpltDetails.getPoiUseCount
   *
   * Checks if POI is used in bookings or templates
   * @param poiId
   * @param cmpyId
   * @return Pair of values - Left number of bookings, right number of templates
   */
  public static Pair<Integer, Integer> countPoiInBookingsAndTemplates(Long poiId, Integer cmpyId) {
    Pair<Integer, Integer> result = null;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_POI_USE_COUNT);
      int paramCounter = 0;
      query.setLong(++paramCounter, poiId);
      query.setInt(++paramCounter, cmpyId);
      query.setLong(++paramCounter, poiId);
      query.setInt(++paramCounter, cmpyId);
      query.setLong(++paramCounter, poiId);
      query.setInt(++paramCounter, cmpyId);
      query.setLong(++paramCounter, poiId);
      query.setInt(++paramCounter, cmpyId);
      query.setLong(++paramCounter, poiId);
      query.setInt(++paramCounter, cmpyId);
      query.setLong(++paramCounter, poiId);
      query.setInt(++paramCounter, cmpyId);

      ResultSet rs = query.executeQuery();
      while(rs.next()) {
        result = new ImmutablePair<>(rs.getInt(1),rs.getInt(2));
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception looking poi use count reference:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }

    return result;
  }

  /**
   * Using this specific routine to get connection.
   *
   * Future optimization can be to implement a naive connection pool for vendors.
   * @return Database connection
   */
  private static Connection getConnection() {

    Connection conn = null;

    try {
      conn = DBConnectionMgr.getConnection4Publisher();
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "Failed to obtain database connection for POI operation:" + e.getMessage());
      e.printStackTrace();
    }

    return conn;
  }

  /**
   * Returning connection
   */
  private static void releaseConnection(Connection conn) {
    try {
      conn.close();
    } catch (SQLException e) {
      Log.log(LogLevel.ERROR, "Failed to release database connection:" + e.getMessage());
      e.printStackTrace();
    }
  }

  /*
   * eBeanPorted
   */
  public static Map<Long, List<PoiRS>> smartSearch(String term, List<Integer> poiTypes, Set<Integer> cmpyIds, int limit) {
    if (term != null) {
      String[] searchTerms = StringUtils.split(term, ',');
      if (searchTerms.length == 1)  {
        String cleanTerm = searchTerms[0].trim();
        if (poiTypes.size() == 1 &&
            cleanTerm.length() == 3 &&
            poiTypes.get(0) == PoiTypeInfo.Instance().byName("Airport").getId()) {
          return findByCode(cleanTerm, poiTypes, cmpyIds, limit);
        }

        return findByTerm(cleanTerm, poiTypes, cmpyIds, limit);
      }

      if (searchTerms.length > 1){
        String keywords = "";
        for (int idx = 1; idx < searchTerms.length; idx++) {
          keywords += " " + searchTerms[idx];
        }
        keywords = keywords.trim();
        if (keywords.length() == 0) { //Just some junk in keywords
          return findByTerm(searchTerms[0], poiTypes, cmpyIds, limit);
        } else {
          return findByTermAndKeywords(searchTerms[0], keywords, poiTypes, cmpyIds, limit);
        }
      } else {
        return findByTerm(null, poiTypes, cmpyIds, limit);
      }

    } else {
      return findByTerm(null, poiTypes, cmpyIds, limit);
    }
  }


  /**
   * eBeanPorted
   *
   * Searching for term, if term is null or empty returning recent records
   *
   * @param term - word to look for in the name of the point of interest
   * @param cmpyIds Company Integer identifiers
   * @param limit maximum number of records to return (it is possible that not all tuples of the same vendor will be returned)
   * @return Returns a Map of poi records. Never returns null value!
   */
  public static Map<Long, List<PoiRS>> findByTerm(String term, List<Integer> poiTypes, Set<Integer> cmpyIds, int limit) {
    Map<Long, List<PoiRS>> result = new TreeMap<>(); //Should be more efficient than hashmap for numbers as keys

    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }
    try {
      ObjectMapper om = new ObjectMapper();

      PreparedStatement query;
      boolean isTermSearch = false;

      if (term == null || term.trim().length() == 0) {
        query = conn.prepareStatement(SQL_FIND_RECENT);

        Log.debug("POI: Searching recent;");
      } else {
        query = conn.prepareStatement(SQL_FIND_BY_TERM);
        Log.debug("POI: Searching term :" + term);
        isTermSearch = true;
      }

      int paramCounter = 0;

      if (isTermSearch) {
        //  "                       WHERE state = 'ACTIVE' AND " +
        //  "                             type_id = ANY(?) AND  " +
        //1 "                             (name ILIKE ? OR " +
        //  "                              (to_tsvector('english',name) @@ plainto_tsquery('english', ?)) OR " +
        //  "                               code = ?)  " +
        //  "                       LIMIT ?) r  " +
        //  "           ON p.poi_id = r.poi_id " +
        //  "WHERE (p.cmpy_id = ANY(?) AND consortium_id = 0) OR " +
        //  "      p.consortium_id IN (SELECT cons_id " +
        //  "                          FROM consortium_company " +
        //  "                          WHERE cmpy_id = ANY(?)) "
        query.setArray(++paramCounter, conn.createArrayOf("integer", poiTypes.toArray()));
        String[] parts = StringUtils.split(term);
        String likeTerm = StringUtils.join(parts,'%');
        query.setString(++paramCounter, '%' + likeTerm + '%');
        query.setString(++paramCounter, term);
        query.setString(++paramCounter, term.toUpperCase());
        query.setString(++paramCounter, '%' + likeTerm + '%');

      }

      //Common parameters
      Integer[] cmpyIdsArray = cmpyIds.toArray(new Integer[cmpyIds.size()]);
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setInt(++paramCounter, limit);
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));

      ResultSet rs = query.executeQuery();
      processResultSet(rs, result);
    }
    catch(SQLException e){
      Log.err("SQL Exception while searching poi:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }

    return result;
  }

  /**
   * eBeanPorted
   *
   * Searching for term, if term is null or empty returning recent records
   *
   * @param term - word to look for in the name of the point of interest
   * @param cmpyIds Company Integer identifiers
   * @param limit maximum number of records to return (it is possible that not all tuples of the same vendor will be returned)
   * @return Returns a Map of poi records. Never returns null value!
   */
  public static Map<Long, List<PoiRS>> findByName(String term, List<Integer> poiTypes, Set<Integer> cmpyIds, int limit) {
    Map<Long, List<PoiRS>> result = new TreeMap<>(); //Should be more efficient than hashmap for numbers as keys

    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }
    try {
      ObjectMapper om = new ObjectMapper();

      PreparedStatement query;
      boolean isTermSearch = false;

      if (term == null || term.trim().length() == 0) {
        query = conn.prepareStatement(SQL_FIND_RECENT);

        Log.debug("POI: Searching recent;");
      } else {
        query = conn.prepareStatement(SQL_FIND_BY_TERM);
        Log.debug("POI: Searching term :" + term);
        isTermSearch = true;
      }

      int paramCounter = 0;

      if (isTermSearch) {
        //  "                       WHERE state = 'ACTIVE' AND " +
        //  "                             type_id = ANY(?) AND  " +
        //1 "                             (name ILIKE ? OR " +
        //  "                              (to_tsvector('english',name) @@ plainto_tsquery('english', ?)) OR " +
        //  "                               code = ?)  " +
        //  "                       LIMIT ?) r  " +
        //  "           ON p.poi_id = r.poi_id " +
        //  "WHERE (p.cmpy_id = ANY(?) AND consortium_id = 0) OR " +
        //  "      p.consortium_id IN (SELECT cons_id " +
        //  "                          FROM consortium_company " +
        //  "                          WHERE cmpy_id = ANY(?)) "
        query.setArray(++paramCounter, conn.createArrayOf("integer", poiTypes.toArray()));
        query.setString(++paramCounter, term);
        query.setString(++paramCounter, term);
        query.setString(++paramCounter, term.toUpperCase());
        query.setString(++paramCounter, term);

      }

      //Common parameters
      Integer[] cmpyIdsArray = cmpyIds.toArray(new Integer[cmpyIds.size()]);
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setInt(++paramCounter, limit);
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));

      ResultSet rs = query.executeQuery();
      processResultSet(rs, result);
    }
    catch(SQLException e){
      Log.err("SQL Exception while searching poi:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }

    return result;
  }

  /*
   Search by tag
   */
  public static Map<Long, List<PoiRS>> findByTag(String term, List<Integer> poiTypes, Set<Integer> cmpyIds, int limit) {
    Map<Long, List<PoiRS>> result = new TreeMap<>(); //Should be more efficient than hashmap for numbers as keys

    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }
    try {
      ObjectMapper om = new ObjectMapper();

      PreparedStatement query;
      boolean isTermSearch = false;

      if (term == null || term.trim().length() == 0) {
        return result;
      } else {
        query = conn.prepareStatement(SQL_FIND_BY_TAG);
        Log.debug("POI: Searching tag :" + term);
        isTermSearch = true;
      }

      int paramCounter = 0;

      if (isTermSearch) {
        query.setArray(++paramCounter, conn.createArrayOf("integer", poiTypes.toArray()));
        query.setString(++paramCounter, '%' + term.toUpperCase() + '%');

      }

      //Common parameters
      Integer[] cmpyIdsArray = cmpyIds.toArray(new Integer[cmpyIds.size()]);
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setInt(++paramCounter, limit);
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));

      ResultSet rs = query.executeQuery();
      processResultSet(rs, result);
    }
    catch(SQLException e){
      Log.err("SQL Exception while searching poi:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }

    return result;
  }

  /*
   Search by tag
   */
  public static Map<Long, List<PoiRS>> findMatchByTagSrcReference(String term, List<Integer> poiTypes, Set<Integer> cmpyIds, int limit) {
    Map<Long, List<PoiRS>> result = new TreeMap<>(); //Should be more efficient than hashmap for numbers as keys

    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }
    try {
      ObjectMapper om = new ObjectMapper();

      PreparedStatement query;
      boolean isTermSearch = false;

      if (term == null || term.trim().length() == 0) {
        return result;
      } else {
        query = conn.prepareStatement(SQL_FIND_MATCH_BY_TAG_SRC_REFERENCE);
        Log.debug("POI: Searching tag and src reference:" + term);
        isTermSearch = true;
      }

      int paramCounter = 0;

      query.setArray(++paramCounter, conn.createArrayOf("integer", poiTypes.toArray()));
      query.setString(++paramCounter, "%\"" + term.toUpperCase() + "\"%");
      query.setString(++paramCounter, term.toUpperCase());

      //Common parameters
      Integer[] cmpyIdsArray = cmpyIds.toArray(new Integer[cmpyIds.size()]);
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setInt(++paramCounter, limit);
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));

      ResultSet rs = query.executeQuery();
      processResultSet(rs, result);
    }
    catch(SQLException e){
      Log.err("SQL Exception while searching poi:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }

    return result;
  }

  /*
  Search by src reference
  */
  public static Map<Long, List<PoiRS>> findMatchBySrcReference(String term, List<Integer> poiTypes, Set<Integer> cmpyIds, int limit) {
    Map<Long, List<PoiRS>> result = new TreeMap<>(); //Should be more efficient than hashmap for numbers as keys

    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }
    try {
      ObjectMapper om = new ObjectMapper();

      PreparedStatement query;
      boolean isTermSearch = false;

      if (term == null || term.trim().length() == 0) {
        return result;
      } else {
        query = conn.prepareStatement(SQL_FIND_MATCH_BY_SRC_REFERENCE);
        Log.debug("POI: Searching src reference :" + term);
        isTermSearch = true;
      }

      int paramCounter = 0;

      query.setArray(++paramCounter, conn.createArrayOf("integer", poiTypes.toArray()));
      query.setString(++paramCounter, term.toUpperCase());

      //Common parameters
      Integer[] cmpyIdsArray = cmpyIds.toArray(new Integer[cmpyIds.size()]);
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setInt(++paramCounter, limit);
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));

      ResultSet rs = query.executeQuery();
      processResultSet(rs, result);
    }
    catch(SQLException e){
      Log.err("SQL Exception while searching poi:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }

    return result;
  }


  /**
   * eBeanPorted
   * Searches pois based on matching term for specific types and either city
   *
   * @param term - word(s) to look for in the name of the point of interest
   * @param keywords - keywords reducing the search query must not be ""
   * @param cmpyIds Company Integer identifiers
   * @param limit maximum number of records to return (it is possible that not all tuples of the same vendor will be returned)
   * @return Returns a Map of poi records. Never returns null value!
   */
  public static Map<Long, List<PoiRS>> findByTermAndKeywords(String term,
                                                             String keywords,
                                                             List<Integer> poiTypes,
                                                             Set<Integer> cmpyIds,
                                                             int limit) {
    Map<Long, List<PoiRS>> result = new TreeMap<>(); //Should be more efficient than hashmap for numbers as keys

    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }
    try {
      PreparedStatement query;
      query = conn.prepareStatement(SQL_FIND_BY_TERM_AND_KEYWORDS);
      //cmpy_id = ANY(?) AND " + //SET
      //type_id = ANY(?) AND " + //SET
      //(name ILIKE ? OR " +
      //to_tsvector('english',name) @@ plainto_tsquery('english', ?) OR " +
      //code = ?) AND" +
      //(search_words ILIKE ? OR " +
      //(to_tsvector('english', search_words) @@ plainto_tsquery('english', ?))) " +
      //LIMIT ?) r " +
      int paramCounter = 0;

      String[] termParts = StringUtils.split(term);
      String likeTerm = StringUtils.join(termParts,'%');
      String[] kwParts = StringUtils.split(keywords);
      String likeKW = StringUtils.join(kwParts,'%');


      query.setArray(++paramCounter, conn.createArrayOf("integer", poiTypes.toArray()));
      query.setString(++paramCounter, "%"+likeTerm+"%");
      query.setString(++paramCounter, term);
      query.setString(++paramCounter, term);
      query.setString(++paramCounter, "%"+likeKW+"%");
      query.setString(++paramCounter, keywords);

      Integer[] cmpyIdsArray = cmpyIds.toArray(new Integer[cmpyIds.size()]);
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setInt(++paramCounter, limit);
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIdsArray));

      ResultSet rs = query.executeQuery();
      processResultSet(rs, result);
    }
    catch(SQLException e){
      Log.err("SQL Exception while searching poi:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }

    return result;
  }


  private static void processResultSet(ResultSet rs, Map<Long, List<PoiRS>> result){
    ObjectMapper om = new ObjectMapper();

    try {
      while (rs.next()) {
        PoiRS poi = getPoiFromResultSet(rs, om);
        if (result.get(poi.getId()) == null) {
          List<PoiRS> pois = new ArrayList<>(1); //Majority of records will have only 1 entry, can optimize later
          pois.add(poi);
          result.put(poi.getId(), pois);
        }
        else { //Already there
          List<PoiRS> pois = result.get(poi.getId());
          pois.add(poi);
        }
      }
    } catch (SQLException se) {
      Log.err("SQL Exception while processing poi results: ", se);
    }
  }


  /**
   * eBeanPorted
   *
   * @param code
   * @param poiTypes
   * @param cmpyIds
   * @param limit
   * @return
   */
  public static Map<Long, List<PoiRS>> findByCode(String code, List<Integer> poiTypes, Set<Integer> cmpyIds, int limit) {
    Map<Long, List<PoiRS>> result = new TreeMap<>(); //Should be more efficient than hashmap for numbers as keys

    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }
    try {
      PreparedStatement query = conn.prepareStatement(SQL_FIND_BY_CODE_ONLY);
      ObjectMapper om = new ObjectMapper();

      int paramCounter = 0;
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIds.toArray()));
      query.setArray(++paramCounter, conn.createArrayOf("integer", poiTypes.toArray()));
      query.setString(++paramCounter, "%"+code+"%");
      query.setInt(++paramCounter, limit);
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIds.toArray()));
      query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIds.toArray()));
      ResultSet rs = query.executeQuery();
      while(rs.next()) {
        PoiRS poi = getPoiFromResultSet(rs,om);
        if (result.get(poi.getId()) == null) {
          List<PoiRS> pois = new ArrayList<>(1); //Majority of records will have only 1 entry, can optimize later
          pois.add(poi);
          result.put(poi.getId(), pois);
        } else { //Already there
          List<PoiRS> pois = result.get(poi.getId());
          pois.add(poi);
        }
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while searching for code:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }

    return result;
  }

  /**
   * eBeanPorted
   * @param poiId
   * @return Returns a list of poi records. Never returns null value!
   */
  public static List<PoiRS> findById(long poiId, Set<Integer> cmpyIds) {
    List<PoiRS> points = new ArrayList<>();
    Connection conn = getConnection();
    if (conn == null) {
      return points;
    }
    try {
      int paramCounter = 0;
      PreparedStatement query;
      if (cmpyIds == null) {
        query = conn.prepareStatement(SQL_FIND_ALL_POI_FOR_ID);
        query.setLong(++paramCounter, poiId);
      } else {
        query = conn.prepareStatement(SQL_FIND_POI_BY_ID);
        query.setLong(++paramCounter, poiId);
        query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIds.toArray()));
        query.setArray(++paramCounter, conn.createArrayOf("integer", cmpyIds.toArray()));
      }

      ResultSet rs = query.executeQuery();

      ObjectMapper om = new ObjectMapper();

      while(rs.next()) {
        PoiRS poi = getPoiFromResultSet(rs,om);
        points.add(poi);
      }
    }
    catch(SQLException e){
      Log.log(LogLevel.ERROR, "SQL Exception while reading poi:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return points;
  }

  /**
   * eBeanPorted
   * @param poiId
   * @return Returns a list of poi records. Never returns null value!
   */
  public static List<PoiRS> findByPk(long poiId, int cmpyId, int consortiumId, int srcId) {
    List<PoiRS> points = new ArrayList<>();
    Connection conn = getConnection();
    if (conn == null) {
      return points;
    }
    try {
      int paramCounter = 0;
      PreparedStatement query;
      query = conn.prepareStatement(SQL_FIND_POI_BY_PK);
      query.setLong(++paramCounter, poiId);
      query.setInt(++paramCounter, cmpyId);
      query.setInt(++paramCounter, consortiumId);
      query.setInt(++paramCounter, srcId);

      ResultSet rs = query.executeQuery();

      ObjectMapper om = new ObjectMapper();

      while(rs.next()) {
        PoiRS poi = getPoiFromResultSet(rs,om);
        points.add(poi);
      }
    }
    catch(SQLException e){
      Log.log(LogLevel.ERROR, "SQL Exception while reading poi:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return points;
  }

  /**
   * eBeanPorted
   * @param poiId
   * @return Returns a list of poi records. Never returns null value!
   */
  public static List<PoiRS> findBy(String name, int cmpyId, int consortiumId, int typeId, int srcId) {
    List<PoiRS> points = new ArrayList<>();
    Connection conn = getConnection();
    if (conn == null) {
      return points;
    }
    try {
      int paramCounter = 0;
      PreparedStatement query;
      query = conn.prepareStatement(SQL_FIND_POI_BY_NAME_CMPY_CONS_TYPE_SRC);
      query.setString(++paramCounter, name);
      query.setInt(++paramCounter, cmpyId);
      query.setInt(++paramCounter, consortiumId);
      query.setInt(++paramCounter, typeId);
      query.setInt(++paramCounter, srcId);

      ResultSet rs = query.executeQuery();

      ObjectMapper om = new ObjectMapper();

      while(rs.next()) {
        PoiRS poi = getPoiFromResultSet(rs,om);
        points.add(poi);
      }
    }
    catch(SQLException e){
      Log.log(LogLevel.ERROR, "SQL Exception while reading poi:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return points;
  }

  /*
   * eBeanPorted
   */
  public static List<PoiRS> findAllById(long poiId) {
    return findById(poiId, null);
  }

  /**
   * eBeanPorted
   * @param poiId
   * @return Returns unique poi record, null otherwise
   */
  public static PoiRS findForCmpy(Long poiId, Integer cmpyId) {
    Connection conn = getConnection();
    if (conn == null) {
      return null;
    }
    try {
      PreparedStatement query = conn.prepareStatement(SQL_FIND_POI_FOR_CMPY);
      //poi_id = ? AND cmpy_id = ? AND consortium_id = ? AND src_id = ?
      int paramCounter = 0;
      query.setLong(++paramCounter, poiId);
      query.setInt(++paramCounter, cmpyId);
      ResultSet rs = query.executeQuery();
      ObjectMapper om = new ObjectMapper();

      while(rs.next()) {
        PoiRS poi = getPoiFromResultSet(rs,om);
        return poi;
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while reading poi:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return null;
  }

  /**
   * eBeanPorted
   *
   * @return Returns a list of poi records. Never returns null value!
   */
  public static List<PoiRS> findPortWithCode() {
    List<PoiRS> points = new ArrayList<>();
    Connection conn = getConnection();
    if (conn == null) {
      return points;
    }
    try {
      int paramCounter = 0;

      PreparedStatement query = conn.prepareStatement(SQL_FIND_POI_PORT_WITH_CODE);

      ResultSet rs = query.executeQuery();

      ObjectMapper om = new ObjectMapper();

      while(rs.next()) {
        PoiRS poi = getPoiFromResultSet(rs,om);
        points.add(poi);
      }
    }
    catch(SQLException e){
      Log.log(LogLevel.ERROR, "SQL Exception while reading poi:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return points;
  }

  /**
   * eBeanPorted
   * @param name
   * @param locLat
   * @param locLong
   * @param delta
   * @return Returns a list of poi records. Never returns null value!
   */
  public static List<PoiRS> findPortByNameorGPS(String name, float locLat, float locLong, float delta) {
    List<PoiRS> points = new ArrayList<>();
    Connection conn = getConnection();
    if (conn == null) {
      return points;
    }
    try {
      int paramCounter = 0;

      float minLocLat = locLat - delta;
      float maxLocLat = locLat + delta;
      float minLocLong = locLong - delta;
      float maxLocLong = locLong + delta;

      PreparedStatement query = conn.prepareStatement(SQL_FIND_POI_PORT_BY_GPS);
      query.setString(++paramCounter, name);
      query.setFloat(++paramCounter,minLocLat);
      query.setFloat(++paramCounter,maxLocLat);
      query.setFloat(++paramCounter,minLocLong);
      query.setFloat(++paramCounter,maxLocLong);


      ResultSet rs = query.executeQuery();

      ObjectMapper om = new ObjectMapper();

      while(rs.next()) {
        PoiRS poi = getPoiFromResultSet(rs,om);
        points.add(poi);
      }
    }
    catch(SQLException e){
      Log.log(LogLevel.ERROR, "SQL Exception while reading poi:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return points;
  }

  private static PoiRS getPoiFromResultSet(ResultSet rs, ObjectMapper om) {
    PoiRS poi = new PoiRS();
    int paramCounter = 0;

    try {
      //      "poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, src_reference, " +
      //      "country_code, loc_lat, loc_long, data, state, createdtimestamp, lastupdatedtimestamp, " +
      //      "createdby, modifiedby, version " +

      poi.setId(rs.getLong(++paramCounter));
      poi.consortiumId = rs.getInt(++paramCounter);
      poi.setCmpyId(rs.getInt(++paramCounter));
      poi.setTypeId(rs.getInt(++paramCounter));
      poi.setName(rs.getString(++paramCounter));
      poi.setCode(rs.getString(++paramCounter));
      poi.setSrcId(rs.getInt(++paramCounter));
      poi.setSrcReference(rs.getString(++paramCounter));
      poi.countryCode = rs.getString(++paramCounter);
      poi.locLat = rs.getFloat(++paramCounter);
      poi.locLong = rs.getFloat(++paramCounter);
      poi.setFileCount(rs.getInt(++paramCounter));
      if (DEBUG) {
        Log.info("db json:" + rs.getString(paramCounter + 1));
      }
      poi.data = om.readValue(rs.getString(++paramCounter), PoiJson.class);
      poi.state = RecordState.valueOf(rs.getString(++paramCounter));
      poi.createdtimestamp = rs.getLong(++paramCounter);
      poi.lastupdatedtimestamp = rs.getLong(++paramCounter);
      poi.createdby = rs.getString(++paramCounter);
      poi.modifiedby = rs.getString(++paramCounter);
      poi.version = rs.getInt(++paramCounter);
      poi.setTagsFromData(); //Settings tags from JSON
    }
    catch(SQLException e){
      Log.err("SQL Exception while converting poi: " + e.getMessage());
      e.printStackTrace();
    }
    catch (IOException e) {
      Log.err("Failed to generate POI from JSON: " + e.getMessage());
      e.printStackTrace();
    }
    return poi;
  }

  /**
   *
   * @param poi
   * @return
   */
  public static boolean save(PoiRS poi) {
    boolean result = false;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_INSERT_POI);
      int paramCounter = 0;
      //SQL Order:
      //poi_id, consortium_id, cmpy_id, type_id, name, code, tags, src_id, src_reference, " +
      //country_code, loc_lat, loc_long, file_count, data, state, search_words, createdtimestamp," +
      //lastupdatedtimestamp, createdby, modifiedby, version)

      query.setLong(++paramCounter, poi.getId());
      query.setInt(++paramCounter, poi.consortiumId);
      query.setInt(++paramCounter, poi.getCmpyId());
      query.setInt(++paramCounter, poi.getTypeId());
      query.setString(++paramCounter, poi.getName());
      query.setString(++paramCounter, poi.getCode());
      query.setString(++paramCounter, poi.getTags());
      query.setInt(++paramCounter, poi.getSrcId());
      query.setString(++paramCounter, poi.getSrcReference());
      query.setString(++paramCounter, poi.countryCode);
      query.setDouble(++paramCounter, poi.locLat);
      query.setDouble(++paramCounter, poi.locLong);
      query.setInt(++paramCounter, poi.getFileCount());

      ObjectMapper jsonMapper = new ObjectMapper();
      poi.data.prepareForDb();
      String json = jsonMapper.writeValueAsString(poi.data);
      PGobject jsonPGSQLObject = new PGobject();
      jsonPGSQLObject.setType("json");
      jsonPGSQLObject.setValue(json);
      query.setObject(++paramCounter, jsonPGSQLObject);
      query.setString(++paramCounter, poi.state.name());
      query.setString(++paramCounter, poi.getSearchWords());
      query.setLong(++paramCounter, poi.createdtimestamp);
      query.setLong(++paramCounter, poi.lastupdatedtimestamp);
      query.setString(++paramCounter, poi.createdby);
      query.setString(++paramCounter, poi.modifiedby);
      query.setInt(++paramCounter, poi.version);


      if (query.executeUpdate() == 0){
        Log.log(LogLevel.INFO, "Unexpected result from insert query :" + query.toString());
      } else  {
        result = true;
      }
    }
    catch(SQLException e){
      Log.log(LogLevel.ERROR, "SQL Exception while inserting poi:" + e.getMessage());
      e.printStackTrace();
    }
    catch (JsonProcessingException jpe) {
      Log.log(LogLevel.ERROR, "Failed to generate POI JSON:" + jpe.getMessage());
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

  /**
   *
   * @param poi
   * @return
   */
  public static boolean update(PoiRS poi) {
    boolean result = false;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_UPDATE_POI);
      int paramCounter = 0;
      //poi_id = ?, consortium_id = ?, cmpy_id = ?, type_id = ?, name = ?, code = ?, tags = ?,
      //src_id = ?, src_reference = ?, country_code = ?, loc_lat = ?, loc_long = ?, file_count = ?,
      //data = ?, state = cast(? as um_type_state), search_words = ?, lastupdatedtimestamp = ?,
      //modifiedby = ?, version = ?
      //WHERE poi_id = ? and consortium_id = ? and cmpy_id = ?
      query.setLong(++paramCounter, poi.getId());
      query.setInt(++paramCounter, poi.consortiumId);
      query.setInt(++paramCounter, poi.getCmpyId());
      query.setInt(++paramCounter, poi.getTypeId());
      query.setString(++paramCounter, poi.getName());
      query.setString(++paramCounter, poi.getCode());
      query.setString(++paramCounter, poi.getTags());
      query.setInt(++paramCounter, poi.getSrcId());
      query.setString(++paramCounter, poi.getSrcReference());

      query.setString(++paramCounter, poi.countryCode);
      query.setDouble(++paramCounter, poi.locLat);
      query.setDouble(++paramCounter, poi.locLong);
      query.setInt(++paramCounter, poi.getFileCount());
      poi.data.prepareForDb();
      ObjectMapper jsonMapper = new ObjectMapper();
      String json = jsonMapper.writeValueAsString(poi.data);
      PGobject jsonPGSQLObject = new PGobject();
      jsonPGSQLObject.setType("json");
      jsonPGSQLObject.setValue(json);
      query.setObject(++paramCounter, jsonPGSQLObject);
      query.setString(++paramCounter, poi.state.name());
      query.setString(++paramCounter, poi.getSearchWords());
      query.setLong(++paramCounter, poi.lastupdatedtimestamp);
      query.setString(++paramCounter, poi.modifiedby);
      query.setInt(++paramCounter, poi.version);

      query.setLong(++paramCounter, poi.getId());
      query.setInt(++paramCounter, poi.consortiumId);
      query.setInt(++paramCounter, poi.getCmpyId());
      query.setInt(++paramCounter, poi.getSrcId());


      if (query.executeUpdate() == 0){
        Log.log(LogLevel.INFO, "Unexpected result from update query :" + query.toString());
      } else  {
        result = true;
      }
    }
    catch(SQLException e){
      Log.log(LogLevel.ERROR, "SQL Exception while update poi:" + e.getMessage());
      e.printStackTrace();
    }
    catch (JsonProcessingException jpe) {
      Log.log(LogLevel.ERROR, "Failed to generate POI JSON:" + jpe.getMessage());
      jpe.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

  /**
   * eBeanPorted
   * Update database record to mark data as deleted.
   *
   * @param poi
   * @return
   */
  public static boolean markDeleted(PoiRS poi) {
    poi.data.prepareForDb();
    poi.state = RecordState.DELETED;
    return update(poi);
  }

  /**
   * Completely wipes record from the database.
   * @note Make sure poi id is not referenced from any other table.
   *
   * @param poi
   * @return
   */
  public static boolean delete(PoiRS poi) {
    boolean result = false;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_DELETE_POI);
      int paramCounter = 0;
      query.setLong(++paramCounter, poi.getId());
      query.setInt(++paramCounter, poi.consortiumId);
      query.setInt(++paramCounter, poi.getCmpyId());
      query.setInt(++paramCounter, poi.getSrcId());

      if (query.executeUpdate() == 0){
        Log.log(LogLevel.INFO, "Unexpected result from delete query:" + query.toString());
      } else  {
        result = true;
      }
    }
    catch(SQLException e){
      Log.log(LogLevel.ERROR, "SQL Exception while delebpting POI:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

  /**
   * "Merges" company's POIs
   * @param srcCmpyId - company that will be no more
   * @param targetCmpyId - company that will persist
   * @param userId - who are you, the merger?
   * @return number of merged POI records
   */
  public static int mergeCmpy(Integer srcCmpyId, Integer targetCmpyId, String userId) {

    String s = "UPDATE poi SET cmpy_id = ?, lastupdatedtimestamp = ?, modifiedby = ?, " +
               "version = version + 1 " +
               "WHERE cmpy_id = ? AND poi_id IN (SELECT poi_id " +
               "     FROM poi " +
               "     WHERE cmpy_id = ? AND poi_id NOT IN (SELECT poi_id " +
               "     FROM poi " +
               "     WHERE cmpy_id = ?))";


    int result = 0;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(s);
      int paramCounter = 0;
      long currTime = System.currentTimeMillis();
      query.setInt(++paramCounter, targetCmpyId);
      query.setLong(++paramCounter, currTime);
      query.setString(++paramCounter, userId);
      query.setInt(++paramCounter, srcCmpyId);
      query.setInt(++paramCounter, srcCmpyId);
      query.setInt(++paramCounter, targetCmpyId);

      result = query.executeUpdate();
      if (result == 0){
        Log.info("Unexpected result from merge query :" + query.toString());
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while update poi:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }


  //      "poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, src_reference, " +
  //      "country_code, loc_lat, loc_long, data, state, createdtimestamp, lastupdatedtimestamp, " +
  //      "createdby, modifiedby, version " +

  static {
    SQL_FIND_FOR_MERGE = "SELECT p.poi_id as poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, src_reference," +
                         "       country_code, loc_lat, loc_long, file_count, data, state, " +
                         "       createdtimestamp, lastupdatedtimestamp, createdby, modifiedby, version " +
                         "FROM poi p INNER JOIN (SELECT DISTINCT poi_id " +
                         "                       FROM poi " +
                         "                       WHERE type_id = ? AND country_code = ? AND length(name)<=255 AND " +
                         "                             (name ILIKE ? OR" +
                         "                              metaphone(name, 5) = metaphone(?, 5)) " +
                         "                      ) r " +
                         "           ON p.poi_id = r.poi_id";

    SQL_FIND_FOR_MERGE_EVERYWHERE = "SELECT p.poi_id as poi_id, consortium_id, cmpy_id, type_id, name, code, src_id," +
                                    "       src_reference, country_code, loc_lat, loc_long, file_count, data, state, " +
                                    "       createdtimestamp, lastupdatedtimestamp, createdby, modifiedby, version " +
                                    "FROM poi p INNER JOIN (SELECT DISTINCT poi_id " +
                                    "                       FROM poi " +
                                    "                       WHERE type_id = ? AND length(name)<=255 AND " +
                                    "                             (name ILIKE ? OR" +
                                    "                              metaphone(name, 5) = metaphone(?, 5)) " +
                                    "                      ) r " +
                                    "           ON p.poi_id = r.poi_id";
  }

  /**
   * eBeanPorted
   * @param name
   * @param countryA3
   * @param poiType
   * @return
   */
  public static List<PoiRS> findForMerge(String name, String countryA3, int poiType) {
    List<PoiRS> matches = new ArrayList<>();

    Connection conn = getConnection();
    if (conn == null) {
      return matches;
    }

    try {
      ObjectMapper om = new ObjectMapper();
      PreparedStatement query;
      if (countryA3 != null) {
        query = conn.prepareStatement(SQL_FIND_FOR_MERGE);
      } else {
        query = conn.prepareStatement(SQL_FIND_FOR_MERGE_EVERYWHERE);
      }

      int paramCounter = 0;

      query.setInt(++paramCounter, poiType);
      if (countryA3 != null) {
        query.setString(++paramCounter, countryA3);
      }
      query.setString(++paramCounter, name);
      query.setString(++paramCounter, name);

      ResultSet rs = query.executeQuery();
      while(rs.next()) {
        PoiRS poi = getPoiFromResultSet(rs, om);
        matches.add(poi);
      }
    }
    catch(SQLException e){
      Log.err("findForMerge(): Exception for name: " + name + " country:" + countryA3 + " poi type: " + poiType);
      Log.err("findForMerge(): SQL Exception while searching poi:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return matches;
  }


  /* eBeanPorted */
  public static enum DeleteMode {
    HARD,
    SOFT,
    AMENITIES
  }

  /**
   * eBeanPorted
   * Deletes all POI records which are no longer present in the feed.
   *
   * @param srcId - source id to clean
   * @param mode  - mode to delete orphans (SOFT or HARD supported)
   * @param userId - when soft deleting this will populate modifiedBy field
   * @return number of removed records
   */
  public static int deleteOrphanPoi(int srcId, DeleteMode mode, String userId) {
    int result = 0;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }
    try {
      int paramCounter = 0;
      PreparedStatement query;
      switch (mode) {
        case HARD:
          query = conn.prepareStatement(SQL_DELETE_DANGLING_POI_HARD);
          break;
        case SOFT:
          query = conn.prepareStatement(SQL_DELETE_DANGLING_POI_SOFT);
          query.setLong(++paramCounter, System.currentTimeMillis());
          query.setString(++paramCounter, userId);
          break;
        default:
          throw new NotImplementedException("Deleting orphan POI records in " + mode + " is not supported.");
      }

      query.setInt(++paramCounter, srcId);
      query.setInt(++paramCounter, srcId);
      query.setInt(++paramCounter, srcId);

      result = query.executeUpdate();
      if (result == 0) {
        Log.info("No dangling POI found for source id: " + srcId);
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while deleting dangling POI records for source id:" + srcId, e);
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

  /**
   * eBeanPorted
   * Find cruise ships with no photos
   * @return
   */
  public static List<PoiRS> findCruiseShipsNoFiles(){
    List<PoiRS> result = new ArrayList<>(200);
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query;
      query = conn.prepareStatement(SQL_FIND_SHIPS_NO_FILES);

      ResultSet rs = query.executeQuery();
      ObjectMapper om = new ObjectMapper();

      while (rs.next()) {
        PoiRS poi = getPoiFromResultSet(rs, om);
        if (poi != null) {
          result.add(poi);
        }
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception looking up flight to convert:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }

    return result;
  }
}
