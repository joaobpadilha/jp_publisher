package com.mapped.publisher.persistence;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;
import com.mapped.publisher.common.APPConstants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2014-05-28.
 */
public class ActiveTrips {

  public enum TripSearchType {
    USER_PUBLISHED,
    USER_PENDING,
    SHARED_PUBLISHED,
    SHARED_PENDING,
    DELETED
  }

  private final static String TIMED_ACTIVE_USER_TRIPS =
      "SELECT\n" +
      "    trip.tripid as tripid,\n" +
      "    trip.name as tripname,\n" +
      "    trip.coverUrl as coverUrl,\n" +
      "    trip.status as status,\n" +
      "    count(trip_audit.eventtime) as changecount,\n" +
      "    avg(trip_audit.eventtime) as midtime,\n" +
      "    max(trip_audit.eventtime) as lasttime,\n" +
      "    count(distinct trip_audit.userid) as changeusers\n" +
      "FROM  trip inner join trip_audit on trip.tripid = trip_audit.tripid\n" +
      "WHERE trip.status = :tripstatus\n" +
      "  AND trip.createdby = :username\n" +
      "  AND trip_audit.eventtime BETWEEN :millisfrom AND :millisto\n" +
      "GROUP BY trip.tripid\n" +
      "ORDER BY lasttime DESC";

  private final static String TIMED_ACTIVE_SHARED_TRIPS =
      "select     sharedtrips.tripid as tripid, \n" +
      "           sharedtrips.name as tripname,\n" +
      "           sharedtrips.status as status,\n" +
      "           sharedtrips.coverurl as coverUrl,\n" +
      "           count(trip_audit.eventtime) as changecount,\n" +
      "           avg(trip_audit.eventtime) as midtime,\n" +
      "           max(trip_audit.eventtime) as lasttime,\n"+
      "           count(distinct trip_audit.userid) as changeusers\n" +
      "from\n" +
      "    (select * from\n" +
      "        ( \n" +
      "            select trip.tripid, trip.name, trip.status, trip.createdby, trip.coverurl\n" +
      "            from user_cmpy_link inner join trip on trip.cmpyid = user_cmpy_link.cmpyid \n" +
      "            where \n" +
      "                trip.createdby != :username and\n" +
      "                user_cmpy_link.userid = :username and\n" +
      "                user_cmpy_link.linktype = 1 \n" +
      "        ) as sharedadmintrips\n" +
      "    union  \n" +
      "        ( \n" +
      "            select tripid, name, trip.status, trip.createdby, trip.coverurl\n" +
      "            from \n" +
      "                (select userid\n" +
      "                from \n" +
      "                (\n" +
      "                    (select distinct cgm.groupid  \n" +
      "                     from user_cmpy_link as ucl, cmpy_group_members as cgm\n" +
      "                     where ucl.userid = :username and ucl.pk = cgm.usercmpylinkpk) as mygroups\n" +
      "                    inner join cmpy_group_members as cgm on cgm.groupid = mygroups.groupid) as mygroupmembers, \n" +
      "                    user_cmpy_link as cl\n" +
      "                    where \n" +
      "                        cl.pk = mygroupmembers.usercmpylinkpk \n" +
      "                        and userid != :username) as mymembers \n" +
      "                inner join trip on mymembers.userid = trip.createdby,  \n" +
      "                user_cmpy_link as myucl\n" +
      "            where \n" +
      "                myucl.userid = :username and myucl.cmpyid = trip.cmpyid   \n" +
      "        )\n" +
      "    ) as sharedtrips inner join trip_audit on sharedtrips.tripid = trip_audit.tripid\n" +
      "where\n" +
      "    trip_audit.eventtime BETWEEN :millisfrom AND :millisto AND\n" +
      "    sharedtrips.status = :tripstatus\n" +
      "group by sharedtrips.tripid, sharedtrips.name, sharedtrips.coverurl, sharedtrips.status\n" +
      "ORDER BY lasttime DESC";

  private final static String TIMED_ACTIVE_SHARED_TRIPS____BACKUP =
      "select     sharedtrips.tripid as tripid, \n" +
      "           sharedtrips.name as tripname,\n" +
      "           sharedtrips.coverurl as coverUrl,\n" +
      "           count(trip_audit.eventtime) as changecount,\n" +
      "           avg(trip_audit.eventtime) as midtime,\n" +
      "           max(trip_audit.eventtime) as lasttime,\n"+
      "           count(distinct trip_audit.userid) as changeusers\n" +
      "from\n" +
      "    (select * from\n" +
      "        (-- All trips from companies where user is an admin\n" +
      "            select trip.tripid, trip.name, trip.status, trip.createdby, trip.coverurl\n" +
      "            from user_cmpy_link inner join trip on trip.cmpyid = user_cmpy_link.cmpyid \n" +
      "            where \n" +
      "                trip.createdby != :username and\n" +
      "                user_cmpy_link.userid = :username and\n" +
      "                user_cmpy_link.linktype = 1 -- TODO: Verify that 1 is actually an admin link\n" +
      "        ) as sharedadmintrips\n" +
      "    union -- Need both Admin access trips and group shared trips\n" +
      "        (-- Query to get all trips from the group members that belong to companies user is in\n" +
      "            select tripid, name, trip.status, trip.createdby, trip.coverurl\n" +
      "            from \n" +
      "                (select userid\n" +
      "                from \n" +
      "                (\n" +
      "                    (select distinct cgm.groupid -- Getting the list of my groups\n" +
      "                     from user_cmpy_link as ucl, cmpy_group_members as cgm\n" +
      "                     where ucl.userid = :username and ucl.pk = cgm.usercmpylinkpk) as mygroups\n" +
      "                    inner join cmpy_group_members as cgm on cgm.groupid = mygroups.groupid) as mygroupmembers, --Getting a list of my group members\n" +
      "                    user_cmpy_link as cl\n" +
      "                    where \n" +
      "                        cl.pk = mygroupmembers.usercmpylinkpk \n" +
      "                        and userid != :username) as mymembers --UserIds of the members of the groups the user " +
      "is in \n" +
      "                inner join trip on mymembers.userid = trip.createdby, -- Trips belonging to the members of my " +
      "grous \n" +
      "                user_cmpy_link as myucl\n" +
      "            where \n" +
      "                myucl.userid = :username and myucl.cmpyid = trip.cmpyid -- Such that trips only belong to " +
      "companies that user is in as well \n" +
      "        )\n" +
      "    ) as sharedtrips inner join trip_audit on sharedtrips.tripid = trip_audit.tripid\n" +
      "where\n" +
      "    trip_audit.eventtime BETWEEN :millisfrom AND :millisto AND\n" +
      "    sharedtrips.status = :tripstatus\n" +
      "group by sharedtrips.tripid, sharedtrips.name, sharedtrips.coverurl\n" +
      "ORDER BY lasttime DESC";

  /**
   * Trip change summary
   */
  public class RecentActiveTrip {
    /**
     * Trip ID
     */
    public String tripid;
    /**
     * Trip Name
     */
    public String name;
    /**
     * Number of events to the trip
     */
    public int numEvents;
    /**
     * Average date of the change set to figure out date
     */
    public Timestamp avgtime;
    /**
     * Number of users that changed the trip
     */
    public int numUsers;
    /*
     *
     */
    public String tripCoverUrl;

    public int status;
  }

  /**
   * Creates list of recent activity summary records based on passed parameters.
   *
   * @param type        defines audit search type
   * @param userId      username that is viewing results
   * @param millisFrom  beginning of the time range where to perform search
   * @param millisTo    end of the time range where to perfom search
   * @param firstRow    first row where to start output of the data
   * @param maxResults  maxium number of results to return
   * @return Empty list if passed parameters are wrong or no activity for the specified parameters, full list otherwise
   */
  public static List<RecentActiveTrip> findActiveTrips(TripSearchType type,
                                                       String userId,
                                                       long millisFrom,
                                                       long millisTo,
                                                       int firstRow,
                                                       int maxResults) {
    List<RecentActiveTrip> activeTrips = new ArrayList<RecentActiveTrip>(maxResults);

    //Nothing to see here, wrong parameters to call this perform this query
    if (userId == null || millisFrom == millisTo || firstRow < 0 || maxResults == 0) {
      return activeTrips;
    }

    SqlQuery sqlQuery;

    switch (type) {
      case SHARED_PUBLISHED:
        sqlQuery = Ebean.createSqlQuery(TIMED_ACTIVE_SHARED_TRIPS);
        sqlQuery.setParameter("tripstatus", APPConstants.STATUS_PUBLISHED);
        break;
      case SHARED_PENDING:
        sqlQuery = Ebean.createSqlQuery(TIMED_ACTIVE_SHARED_TRIPS);
        sqlQuery.setParameter("tripstatus", APPConstants.STATUS_PENDING);
        break;
      case USER_PUBLISHED:
        sqlQuery = Ebean.createSqlQuery(TIMED_ACTIVE_USER_TRIPS);
        sqlQuery.setParameter("tripstatus", APPConstants.STATUS_PUBLISHED);
        break;
      case DELETED:
        sqlQuery = Ebean.createSqlQuery(TIMED_ACTIVE_USER_TRIPS);
        sqlQuery.setParameter("tripstatus", APPConstants.STATUS_DELETED);
        break;
      case USER_PENDING:
      default:
        sqlQuery = Ebean.createSqlQuery(TIMED_ACTIVE_USER_TRIPS);
        sqlQuery.setParameter("tripstatus", APPConstants.STATUS_PENDING);
        break;
    }

    sqlQuery.setParameter("username", userId);
    sqlQuery.setParameter("millisfrom", millisFrom);
    sqlQuery.setParameter("millisto", millisTo);


    List<SqlRow> rs = sqlQuery.setFirstRow(firstRow).setMaxRows(maxResults).findList();

    for (SqlRow sr : rs) {
      RecentActiveTrip rat = new ActiveTrips().new RecentActiveTrip();
      rat.tripid = sr.getString("tripid");
      rat.name = sr.getString("tripname");
      rat.numEvents = sr.getInteger("changecount");
      rat.avgtime = new Timestamp(sr.getLong("midtime"));
      rat.numUsers = sr.getInteger("changeusers");
      rat.tripCoverUrl = sr.getString("coverUrl");
      rat.status = sr.getInteger("status");

      activeTrips.add(rat);
    }

    return activeTrips;
  }

}
