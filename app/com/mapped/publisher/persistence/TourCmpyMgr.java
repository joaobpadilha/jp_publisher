package com.mapped.publisher.persistence;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-02
 * Time: 10:12 AM
 * To change this template use File | Settings | File Templates.
 */
public class TourCmpyMgr {
    private final static String SEARCH_NAME = "select t.tripId, t.name as tripname, t.starttimestamp, t.endtimestamp, t.comments, t.createdby, c.cmpyId, c.name as cmpyname, c.contact as cmpyContact, u.firstname, u.lastname from trip t, company c,  user_profile u where upper(t.name) like ? and t.status = 0 and t.starttimestamp > ? and t.tripid in (select tripid from trip_cmpy where status = 0 and cmpyid = ?) and t.cmpyid = c.cmpyid and u.userid = t.createdby;";
    private final static String SEARCH_DATE = "select t.tripId, t.name as tripname, t.starttimestamp, t.endtimestamp, t.comments, t.createdby, c.cmpyId, c.name as cmpyname, c.contact as cmpyContact, u.firstname, u.lastname " +
                                              "from trip t, company c,  user_profile u " +
                                              "where t.status = 0 and t.starttimestamp >= ? and t.starttimestamp < ? and t.tripid in (select tripid from trip_cmpy where status = 0 and cmpyid = ?) and t.cmpyid = c.cmpyid and u.userid = t.createdby;";


    public static List<TourCmpyRS> findByTermCmpyId (String term, String cmpyId, int maxRows, Connection conn) throws SQLException {
        ArrayList<TourCmpyRS> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Statement stat= null;
        try {

            prep = conn.prepareStatement(SEARCH_NAME);
            prep.setString(1, term);
            prep.setLong(2, System.currentTimeMillis());
            prep.setString(3, cmpyId);
            prep.setMaxRows(maxRows);

            rs = prep.executeQuery();
            if (rs == null)
                return results;

            while (rs.next()) {
                TourCmpyRS c = new TourCmpyRS();
                c.setTourId(rs.getString("tripId"));
                c.setTourName(rs.getString("tripname"));
                c.setStartTimestamp(rs.getLong("starttimestamp"));
                c.setEndTimestamp(rs.getLong("endtimestamp"));
                c.setCmpyName(rs.getString("cmpyname"));
                c.setCmpyContact(rs.getString("cmpyContact"));
                c.setCmpyId(rs.getString("cmpyId"));
                c.setfName(rs.getString("firstName"));
                c.setlName(rs.getString("lastName"));
                c.setTourComments(rs.getString("comments"));
                c.setCreatedBy(rs.getString("createdby"));
                results.add(c);
            }
            return results;
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
        }
    }

    public static List<TourCmpyRS> findByStartDate (long startTime, long endTime, String cmpyId,Connection conn) throws SQLException {
        ArrayList<TourCmpyRS> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Statement stat= null;
        try {

            prep = conn.prepareStatement(SEARCH_DATE);
            prep.setLong(1, startTime);
            prep.setLong(2, endTime);
            prep.setString(3, cmpyId);
            rs = prep.executeQuery();
            if (rs == null)
                return results;

            while (rs.next()) {
                TourCmpyRS c = new TourCmpyRS();
                c.setTourId(rs.getString("tripId"));
                c.setTourName(rs.getString("tripname"));
                c.setStartTimestamp(rs.getLong("starttimestamp"));
                c.setEndTimestamp(rs.getLong("endtimestamp"));
                c.setCmpyName(rs.getString("cmpyname"));
                c.setCmpyContact(rs.getString("cmpyContact"));
                c.setCmpyId(rs.getString("cmpyId"));
                c.setfName(rs.getString("firstName"));
                c.setlName(rs.getString("lastName"));
                c.setTourComments(rs.getString("comments"));
                c.setCreatedBy(rs.getString("createdby"));

                results.add(c);
            }
            return results;
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
        }
    }


    public static int deleteByTourId (String tourId, Connection conn) throws SQLException {
        ArrayList<TourCmpyRS> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Statement stat= null;
        try {
                prep = conn.prepareStatement("delete from trip_cmpy where tripid like ?");

                prep.setString(1, tourId);
                return prep.executeUpdate();

        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
        }
    }
}
