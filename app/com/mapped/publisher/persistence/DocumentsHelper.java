package com.mapped.publisher.persistence;

import com.mapped.publisher.common.Credentials;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.common.SessionMgr;
import models.publisher.*;

import java.util.ArrayList;
import java.util.List;

import static models.publisher.FileSrc.FileSrcType.IMG_USER_UPLOAD;

/**
 * Helper for Documents (destination) attached pages (destination guides)
 * Created by surge on 2016-10-27.
 */
public class DocumentsHelper {

  /**
   * Umapped Document consists of a destination and 0 or more pages
   */
  public static class UmDocument {
    public Destination            destination;
    public List<DestinationGuide> pages;
  }

  /**
   * Umapped Page
   * <p>
   * Usually needs parent record of the guide page is attached to
   */
  public static class UmPage {
    public Destination      destination;
    public DestinationGuide page;
  }


  public static List<UmPage> findPagesByName(String term, String cmpyId, List<Integer> types, SessionMgr sessionMgr) {
    /* OLD Search Logic

    "where d.status = 0 and d.cmpyid = ANY(?) and d.destinationtypeid = ANY(?) and " +
    "      d.destinationid = dg.destinationid and dg.status = 0 and (dg.tag ilike ? or dg.name ilike ?) " +
    "group by d.destinationid,dg.destinationguideid " +
    "order by d.name, dg.name"

     */

    Credentials cred = sessionMgr.getCredentials();

    term = prepareSearchTerm(term);
    List<DestinationGuide>  sr = DestinationGuide.findByNameForCompany(term, cmpyId, types, sessionMgr);
    List<UmPage> result = new ArrayList<>();
    if(sr == null || sr.size() == 0) {
      return result;
    }

    for(DestinationGuide page: sr) {
      Destination d = Destination.find.byId(page.getDestinationid());
      UmPage p = new UmPage();
      p.destination = d;
      p.page = page;
      result.add(p);
    }
    return result;
  }


  public static List<UmPage> findPagesByTerm(String term, String cmpyId, List<Integer> types, SessionMgr sessionMgr) {
    /* OLD Logic
      "where d.status = 0 and d.cmpyid = ANY(?) and d.destinationtypeid = ANY(?) and d.destinationid = dg" +
      ".destinationid and dg.status = 0 and (dg.tag ilike ? or dg.name ilike ? or dg.intro ilike ? or dg.description " +
      "ilike ?) " +
      "group by d.destinationid,dg.destinationguideid " +
      "order by d.name, dg.name"
     */

    Credentials cred = sessionMgr.getCredentials();

    term = prepareSearchTerm(term);
    List<DestinationGuide>  sr = DestinationGuide.findByTermForCompany(term, cmpyId, types, sessionMgr);
    List<UmPage> result = new ArrayList<>();
    if(sr == null || sr.size() == 0) {
      return result;
    }

    for(DestinationGuide page: sr) {
      Destination d = Destination.find.byId(page.getDestinationid());
      UmPage p = new UmPage();
        p.destination = d;
        p.page = page;
        result.add(p);
    }
    return result;
  }

  //Search Logic for Datatables
  public static List<UmPage> findPagesByNameDT(String term, String cmpyId, List<Integer> types, SessionMgr sessionMgr, int startPos, int maxRows, String orderBy) {
    Credentials cred = sessionMgr.getCredentials();

    term = prepareSearchTerm(term);
    List<DestinationGuide>  sr = DestinationGuide.findByNameForCompanyDT(term, cmpyId, types, startPos, maxRows, orderBy, sessionMgr);
    List<UmPage> result = new ArrayList<>();
    if(sr == null || sr.size() == 0) {
      return result;
    }

    for(DestinationGuide page: sr) {
      Destination d = Destination.find.byId(page.getDestinationid());
      UmPage p = new UmPage();
      p.destination = d;
      p.page = page;
      result.add(p);
    }
    return result;
  }


  //Search Logic for Datatables
  public static List<UmPage> findPagesByTermDT(String term, String cmpyId, List<Integer> types, SessionMgr sessionMgr, int startPos, int maxRows, String orderBy) {
    Credentials cred = sessionMgr.getCredentials();

    term = prepareSearchTerm(term);
    List<DestinationGuide>  sr = DestinationGuide.findByTermForCompanyDT(term, cmpyId, types, startPos, maxRows, orderBy, sessionMgr);
    List<UmPage> result = new ArrayList<>();
    if(sr == null || sr.size() == 0) {
      return result;
    }

    for(DestinationGuide page: sr) {
      Destination d = Destination.find.byId(page.getDestinationid());
      UmPage p = new UmPage();
      p.destination = d;
      p.page = page;
      result.add(p);
    }
    return result;
  }


  public static List<UmPage> findRecentActivePagesForCmpy(List<Integer> types, String cmpyId, int startPos, int maxRows, SessionMgr sessionMgr) {
    /* OLD Logic
      "where d.status = 0 and d.cmpyid = ANY(?) and d.destinationtypeid = ANY(?) and d.destinationid = dg" +
      ".destinationid and dg.status = 0 and (dg.tag ilike ? or dg.name ilike ? or dg.intro ilike ? or dg.description " +
      "ilike ?) " +
      "group by d.destinationid,dg.destinationguideid " +
      "order by d.name, dg.name"
     */

    Credentials cred = sessionMgr.getCredentials();

    List<DestinationGuide>  sr = DestinationGuide.findMostRecentActiveForCmpy(types, cmpyId, startPos, sessionMgr);
    List<UmPage> result = new ArrayList<>();
    if(sr == null || sr.size() == 0) {
      return result;
    }

    for(DestinationGuide page: sr) {
      if (result.size() < maxRows) {
        Destination d = Destination.find.byId(page.getDestinationid());
        if (SecurityMgr.isCmpyAdmin(d.cmpyid, sessionMgr) || d.accessLevel == Destination.AccessLevel.PUBLIC || d.accessLevel == null
            || (d.accessLevel == Destination.AccessLevel.PRIVATE && d.createdby.equals(cred.getUserId()))) {
          UmPage p = new UmPage();
          p.destination = d;
          p.page = page;
          result.add(p);
        }
      } else {
        break;
      }
    }
    return result;
  }


  private static String prepareSearchTerm(String term) {
    if (term != null || !term.isEmpty()) {
      term = term.replace(" ", "%");
      term = "%" + term + "%";
    }
    else {
      term = "%";
    }
    return term;
  }

  /**
   * Built from ImageController's getImageKey() to handle file uploads
   */
  public static String getFileKey (FileInfo file, final String filenameNormalized, Account a){
    FileSrc src = file.getSource();
    StringBuilder key = new StringBuilder(src.getPrefix()).append('/');

    switch (src.getName()) {
      case FILE_USER_UPLOAD:
        key.append(Account.encodeId(a.getModifiedBy())).append('/');
        break;
    }

    key.append(file.getEncodedPk()).append('/');
    key.append(filenameNormalized);

    return key.toString();
  }

}
