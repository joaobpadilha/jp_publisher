package com.mapped.publisher.persistence;

import au.com.bytecode.opencsv.CSVReader;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.S3Util;
import com.mapped.publisher.utils.Utils;
import com.umapped.api.schema.types.Address;
import com.umapped.api.schema.types.Feature;
import com.umapped.api.schema.types.PhoneNumber;
import com.umapped.api.schema.types.Privacy;
import models.publisher.FeedSrc;
import models.publisher.PoiSrcFeed;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.language.Metaphone;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import javax.xml.bind.DatatypeConverter;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by twong on 2017-03-15.
 */
public class FeedHelperTravelLeadersHotel extends FeedHelperPOI {
  public final static String TRAVELLEADERS_HOTEL_FEED_NAME = "TravelLeaders Hotel";
  public final static String TRAVELLEADERS_S3_PREFIX = "feeds/TravelLeaders/";
  private final static int METAPHONE_LEN = 4;

  private static Pattern feedFile = Pattern.compile("TravelLeaders_(2[0-9][0-9][0-9])([0-1][0-9])([0-3][0-9])_hotel.*.csv",
                                                    Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

  @Override public List<FeedResource> getResources() {
    List<FeedResource> result = new ArrayList<>();
    List<S3ObjectSummary> objs = S3Util.getObjectList(S3Util.getPDFBucketName(), TRAVELLEADERS_S3_PREFIX);
    Map<Date, S3ObjectSummary> amenities = new HashMap<>();
    Map<Date, S3ObjectSummary> hotels = new HashMap<>();

    for(S3ObjectSummary o : objs) {
      Matcher m = feedFile.matcher(o.getKey());
      if(m.find()) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.set(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)) - 1, Integer.parseInt(m.group(3)), 0, 0, 0);
        Date d = cal.getTime();
        hotels.put(d, o);
      }
    }

    for(Date d: hotels.keySet()) {
      S3ObjectSummary hotl = hotels.get(d);
      if(hotl != null) {
        FeedResource fr = new FeedResource();
        fr.setDate(d);
        fr.setName(hotl.getKey());
        fr.setOrigin(FeedResource.Origin.AWS_S3);
        fr.setSize(hotl.getSize());
        result.add(fr);
      }
    }
    return result;
  }

  public static class TravelLeadersHotelData {
    public String propertyName;
    public String city;
    public String state;
    public String country;
    public String chain;
    public String amadeusCode;
    public String sabreCode;
    public String worldspanCode;
    public String galileoCode;
    public String commission;
    public String numRooms;
    public String breakfastType;
    public String amenity;
    public String wifi;
    public String upgrade;
    public String checkin;
    public String phone;
    public String contactName;
    public String contactTitle;
    public String contactEmail;
  }


  private enum HotelsColumns {
    Property_Name,
    City,
    State,
    Country,
    Chain,
    Amadeus,
    Sabre,
    Worldspan,
    Galileo,
    Commission,
    NumRooms,
    Breakfast,
    Amenity,
    Wifi,
    Upgrade,
    Checkin,
    Phone,
    ContactName,
    ContactTitle,
    ContactEmail,
    COLUMN_COUNT;
  }

  public String getFeedRef(FeedHelperTravelLeadersHotel.TravelLeadersHotelData hd) {
    return DigestUtils.md5Hex(simplifyName(hd.propertyName));
  }

  @Override public Map<PoiFeedUtil.FeedResult, Integer> load(String filePath) {
    Map<PoiFeedUtil.FeedResult, Integer> results = new HashMap<>();
    int errors  = 0;
    int success = 0;
    try {
      String hotelsFilepath = "";
      Matcher m = feedFile.matcher(filePath);
      if(m.find()) {
        hotelsFilepath = TRAVELLEADERS_S3_PREFIX +  "TravelLeaders_"+m.group(1) + m.group(2) + m.group(3) + "_hotel.csv";
      }
      else {
        return null;
      }

      S3Object amenities = S3Util.getS3File(S3Util.getPDFBucketName(), filePath);
      S3Object hotels    = S3Util.getS3File(S3Util.getPDFBucketName(), hotelsFilepath);
      if(hotels == null || amenities == null) {
        return null;
      }

      Map<String, FeedHelperTravelLeadersHotel.TravelLeadersHotelData> data = new HashMap<>();
      Map<String, List<FeedHelperTravelLeadersHotel.TravelLeadersHotelData>> byCity = new HashMap<>();
      Reader hotelsReader = new InputStreamReader(hotels.getObjectContent());
      CSVReader hotelsCSV = new CSVReader(hotelsReader);

      String[] line;
      line = hotelsCSV.readNext(); //Skipping labels line
      Metaphone metaphone = new Metaphone();
      metaphone.setMaxCodeLen(METAPHONE_LEN);

      while ((line = hotelsCSV.readNext()) != null) {
        if(line.length < FeedHelperTravelLeadersHotel.HotelsColumns.COLUMN_COUNT.ordinal()) {
          Log.err("TravelLeaders hotels CSV line has fewer columns than expected");
          continue;
        }
        FeedHelperTravelLeadersHotel.TravelLeadersHotelData hd = new FeedHelperTravelLeadersHotel.TravelLeadersHotelData();
        hd.propertyName =  Utils.trim(line[HotelsColumns.Property_Name.ordinal()]);

        hd.city =  Utils.trim(line[HotelsColumns.City.ordinal()]);
        hd.state =  Utils.trim(line[HotelsColumns.State.ordinal()]);
        hd.country =  Utils.trim(line[HotelsColumns.Country.ordinal()]);
        hd.chain = Utils.trim(line[HotelsColumns.Chain.ordinal()]);
        hd.amadeusCode = Utils.trim(line[HotelsColumns.Amadeus.ordinal()]);
        hd.sabreCode = Utils.trim(line[HotelsColumns.Sabre.ordinal()]);
        hd.worldspanCode = Utils.trim(line[HotelsColumns.Worldspan.ordinal()]);
        hd.galileoCode = Utils.trim(line[HotelsColumns.Galileo.ordinal()]);
        hd.commission = Utils.trim(line[HotelsColumns.Commission.ordinal()]);
        hd.numRooms = Utils.trim(line[HotelsColumns.NumRooms.ordinal()]);

        hd.amenity =  Utils.trim(line[HotelsColumns.Amenity.ordinal()]);
        hd.breakfastType =  Utils.trim(line[HotelsColumns.Breakfast.ordinal()]);
        hd.wifi = Utils.trim(line[HotelsColumns.Wifi.ordinal()]);
        hd.upgrade = Utils.trim(line[HotelsColumns.Upgrade.ordinal()]);
        hd.checkin = Utils.trim(line[HotelsColumns.Checkin.ordinal()]);
        hd.phone = Utils.trim(line[HotelsColumns.Phone.ordinal()]);

        hd.contactName = Utils.trim(line[HotelsColumns.ContactName.ordinal()]);
        hd.contactTitle = Utils.trim(line[HotelsColumns.ContactTitle.ordinal()]);
        hd.contactEmail = Utils.trim(line[HotelsColumns.ContactEmail.ordinal()]);


        hd.propertyName = hd.propertyName.replaceAll(" +", " ");

        if(data.get(hd.propertyName) != null) {
          Log.err("{.}|{.} WTF these TravelLeaders are doing???? === " + hd.propertyName);
        }
        data.put(simplifyName(hd.propertyName), hd);

        List<FeedHelperTravelLeadersHotel.TravelLeadersHotelData> cityHotels = byCity.get(hd.city);
        if(cityHotels == null) {
          cityHotels = new ArrayList<>();
          cityHotels.add(hd);
          byCity.put(hd.city, cityHotels);
        } else {
          cityHotels.add(hd);
        }
      }



      ObjectMapper jsonMapper = new ObjectMapper();
      for(String name: data.keySet()) {
        FeedHelperTravelLeadersHotel.TravelLeadersHotelData hd = data.get(name);
        if(hd.amenity == null) {
          continue;
        }

        String json = jsonMapper.writeValueAsString(hd);
        PoiImportRS pirs = new PoiImportRS();
        pirs.state = PoiImportRS.ImportState.UNPROCESSED;
        pirs.srcId = FeedSourcesInfo.byName(TRAVELLEADERS_HOTEL_FEED_NAME);
        pirs.poiId = null;
        pirs.typeId = PoiTypeInfo.Instance().byName("Accommodations").getId();
        pirs.timestamp = System.currentTimeMillis();
        pirs.srcReference = getFeedRef(hd);
        pirs.srcFilename = hotelsFilepath;
        pirs.name = hd.propertyName;
        pirs.countryName = hd.country;

        CountriesInfo.Country c = CountriesInfo.Instance().searchByName(hd.country);
        if (c == null) {
          Log.err("Failed to find matching country: >>" + hd.country + "<< !!!");
          pirs.countryCode = "UNK";
          errors++;
        }
        else {
          pirs.countryCode = c.getAlpha3();
        }
        if (hd.phone != null && !hd.phone.isEmpty()) {
          pirs.phone = hd.phone;
        }
        pirs.region = hd.state;
        pirs.city = hd.city;
        pirs.raw = json;
        pirs.hash = DigestUtils.md5Hex(pirs.raw);
        PoiImportMgr.save(pirs);
        success++;
      }
    }
    catch (Exception e) {
      Log.err("Failure during TravelLeaders hotels feed processing");
      e.printStackTrace();
    }
    results.put(PoiFeedUtil.FeedResult.SUCCESS, success);
    results.put(PoiFeedUtil.FeedResult.ERROR, errors);
    return results;
  }

  @Override public PoiImportRS.ImportState associate(PoiImportRS importRS) {
    List<Pair<PoiRS, Integer>> scores = getRankedPois(importRS, false);

    if (scores.size() == 0 || scores.get(0).getRight() <= 0) {
      Log.debug("Found no matches for:" + importRS.name + " from: " + importRS.countryCode);
      importRS.state = PoiImportRS.ImportState.MISMATCH;
      importRS.poiId = null;
    }
    else {
      importRS.state = (scores.get(0).getRight() >= 40) ?
                       PoiImportRS.ImportState.MATCH :
                       PoiImportRS.ImportState.UNSURE;
      importRS.poiId = scores.get(0).getLeft().getId();
    }

    PoiImportMgr.update(importRS);
    return importRS.state;
  }

  @Override public PoiSrcFeed.FeedSyncResult synchronize(PoiSrcFeed psf) {
    PoiSrcFeed.FeedSyncResult result = PoiSrcFeed.FeedSyncResult.FAIL;
    try {
      ObjectMapper jsonMapper = new ObjectMapper();
      com.fasterxml.jackson.databind.ObjectReader r = jsonMapper.reader(FeedHelperTravelLeadersHotel.TravelLeadersHotelData.class);
      FeedHelperTravelLeadersHotel.TravelLeadersHotelData data = r.readValue(psf.getRaw());
      FeedSrc feedSrc = FeedSrc.find.byId(psf.getFeedId().srcId);
      List<PoiRS> poiRecs = PoiMgr.findAllById(psf.getFeedId().poiId);

      PoiRS rsToUpdate = null;
      for (PoiRS p : poiRecs) {
        if (p.getCmpyId() == feedSrc.getCmpyId() &&
            p.consortiumId == feedSrc.getConsortiumId() &&
            p.getSrcId() == feedSrc.getSrcId()) {
          rsToUpdate = p;
        }
      }

      if (rsToUpdate == null) { //Creating a new POI record
        rsToUpdate = PoiRS.buildRecord(feedSrc.getConsortiumId(),
                                       feedSrc.getCmpyId(),
                                       feedSrc.getTypeId(),
                                       feedSrc.getSrcId(),
                                       "system");
        updatePoi(rsToUpdate, psf, data, feedSrc);
        rsToUpdate.setId(psf.getFeedId().poiId);
        PoiMgr.save(rsToUpdate);
        result = PoiSrcFeed.FeedSyncResult.NEW;
      }
      else { //updating existing one
        updatePoi(rsToUpdate, psf, data, feedSrc);
        PoiMgr.update(rsToUpdate);
        result = PoiSrcFeed.FeedSyncResult.UPDATED;
      }
      psf.setSyncTs(System.currentTimeMillis());
      psf.setSynced(true);
      psf.update();

    }
    catch (Exception e) {
      Log.err("Unexpected Error - not handling gracefully:", e.getMessage());
      e.printStackTrace();
    }
    return result;
  }

  private PoiRS updatePoi(PoiRS poi, PoiSrcFeed feedRec, FeedHelperTravelLeadersHotel.TravelLeadersHotelData hotelInfo, FeedSrc src) {
    poi.setSrcReference(feedRec.getSrcReference());
    poi.setName(hotelInfo.propertyName);
    Address address = new Address();
    address.setLocality(hotelInfo.city);
    address.setRegion(hotelInfo.state);
    address.setCountryCode(CountriesInfo.Instance().searchByName(hotelInfo.country).getAlpha3());
    poi.setMainAddress(address);

    if (hotelInfo.phone != null && hotelInfo.phone.isEmpty()) {
      PhoneNumber phoneNumber = new PhoneNumber();
      phoneNumber.setPhoneType(PhoneNumber.PhoneType.MAIN);
      phoneNumber.setPrivacy(Privacy.PUBLIC);
      if (!hotelInfo.phone.trim().startsWith("+")) {
        phoneNumber.setNumber("+" + hotelInfo.phone.trim());
      } else {
        phoneNumber.setNumber(hotelInfo.phone.trim());
      }
      poi.setMainPhoneNumber(phoneNumber);
    }

    poi.clearFeatures(); //Clearing all previously listed features for this feed
    if(hotelInfo.amenity != null) {
      Feature f = new Feature();
      f.setName("Special Amenities");
      StringBuilder sb = new StringBuilder();
      sb.append(hotelInfo.amenity.trim());
      if (hotelInfo.breakfastType != null && !hotelInfo.breakfastType.isEmpty()) {
        sb.append("\n");
        sb.append(hotelInfo.breakfastType.trim());
      }
      if (hotelInfo.wifi != null && !hotelInfo.wifi.isEmpty()) {
        sb.append("\n");
        sb.append(hotelInfo.wifi.trim());
      }
      if (hotelInfo.upgrade != null && !hotelInfo.upgrade.isEmpty()) {
        sb.append("\n");
        sb.append(hotelInfo.upgrade.trim());
      }
      if (hotelInfo.checkin != null && !hotelInfo.checkin.isEmpty()) {
        sb.append("\n");
        sb.append(hotelInfo.checkin.trim());
      }
      f.setDesc(sb.toString());
      f.addTag(src.getName());
      poi.addFeature(f);
    }
    return poi;
  }

  @Override public List<Pair<PoiRS, Integer>> getRankedPois(PoiImportRS importRS, boolean ignoreCountry) {
    String cleanName = importRS.name;
    List<PoiRS> pois;
    if (ignoreCountry) {
      pois = PoiMgr.findForMerge(cleanName, null, importRS.typeId);
    }
    else {
      pois = PoiMgr.findForMerge(cleanName, importRS.countryCode, importRS.typeId);
    }

    List<Pair<PoiRS, Integer>> scores = new ArrayList<>();
    for (PoiRS prs : pois) {
      int currScore = rankPoi(cleanName, importRS, prs);
      scores.add(new ImmutablePair<>(prs, currScore));
    }

    Collections.sort(scores, (o1, o2) -> o2.getRight() - o1.getRight());

    return scores;
  }

  @Override public PoiSrcFeed.FeedSyncResult clearAmenities(PoiSrcFeed psf) {
    PoiSrcFeed.FeedSyncResult result = PoiSrcFeed.FeedSyncResult.FAIL;

    ObjectMapper jsonMapper = new ObjectMapper();
    com.fasterxml.jackson.databind.ObjectReader r = jsonMapper.reader(FeedHelperTravelLeadersHotel.TravelLeadersHotelData.class);
    try {
      FeedHelperTravelLeadersHotel.TravelLeadersHotelData data = r.readValue(psf.getRaw());
      if (data.amenity != null) {
        data.amenity = null;
        psf.setSynced(false);
        psf.setRaw(jsonMapper.writeValueAsString(data));
        psf.setHash(DatatypeConverter.parseHexBinary(DigestUtils.md5Hex(psf.getRaw())));
        psf.update();
        result = PoiSrcFeed.FeedSyncResult.UPDATED;
      } else {
        result = PoiSrcFeed.FeedSyncResult.UNCHANGED;
      }
    } catch(Exception e) {
      Log.err("Failure to remove amenities for TravelLeaders feed", e);
      e.printStackTrace();
    }
    return result;
  }
}