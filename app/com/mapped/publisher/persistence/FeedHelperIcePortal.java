package com.mapped.publisher.persistence;

import com.mapped.publisher.parse.iceportal.*;
import com.mapped.publisher.utils.Log;
import com.umapped.api.schema.types.Address;
import com.umapped.api.schema.types.PhoneNumber;
import com.umapped.api.schema.types.Privacy;
import controllers.PoiController;
import models.publisher.FeedSrc;
import models.publisher.PoiSrcFeed;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.Jsoup;
import org.jsoup.examples.HtmlToPlainText;
import org.jsoup.nodes.Document;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;

/**
 * Created by surge on 2015-02-20.
 */
public class FeedHelperIcePortal
    extends FeedHelperPOI {

  @Override public List<FeedResource> getResources() {
    return null;
  }

  @Override public Map<PoiFeedUtil.FeedResult, Integer> load(String parameter) {
    return null;
  }

  @Override public PoiImportRS.ImportState associate(PoiImportRS importRS) {
    List<Pair<PoiRS, Integer>> scores = getRankedPois(importRS, false);

    if (scores.size() == 0 || scores.get(0).getRight() <= 0) {
      Log.debug("Found no matches for:" + importRS.name + " from: " + importRS.countryCode);
      importRS.state = PoiImportRS.ImportState.MISMATCH;
      importRS.poiId = null;
    }
    else {
      importRS.state = (scores.get(0).getRight() >= 40) ?
                       PoiImportRS.ImportState.MATCH :
                       PoiImportRS.ImportState.UNSURE;
      importRS.poiId = scores.get(0).getLeft().getId();
    }

    PoiImportMgr.update(importRS);
    return importRS.state;
  }

  @Override public PoiSrcFeed.FeedSyncResult synchronize(PoiSrcFeed psf) {
    PoiSrcFeed.FeedSyncResult result = PoiSrcFeed.FeedSyncResult.FAIL;
    try {
      Brochure brochure = getBrochureResponse(psf.getRaw());
      FeedSrc feedSrc = FeedSrc.find.byId(psf.getFeedId().srcId);
      List<PoiRS> poiRecs = PoiMgr.findAllById(psf.getFeedId().poiId);

      PoiRS rsToUpdate = null;
      for (PoiRS p : poiRecs) {
        if (p.getCmpyId() == feedSrc.getCmpyId() &&
            p.consortiumId == feedSrc.getConsortiumId() &&
            p.getSrcId() == feedSrc.getSrcId()) {
          rsToUpdate = p;
        }
      }

      if (rsToUpdate == null) { //Creating a new POI record
        rsToUpdate = PoiRS.buildRecord(feedSrc.getConsortiumId(),
                                       feedSrc.getCmpyId(),
                                       feedSrc.getTypeId(),
                                       feedSrc.getSrcId(),
                                       "system");
        updatePoi(rsToUpdate, brochure, feedSrc);
        rsToUpdate.setId(psf.getFeedId().poiId);
        PoiMgr.save(rsToUpdate);
        result = PoiSrcFeed.FeedSyncResult.NEW;
      }
      else { //updating existing one
        updatePoi(rsToUpdate, brochure, feedSrc);
        PoiMgr.update(rsToUpdate);
        result = PoiSrcFeed.FeedSyncResult.UPDATED;
      }
      psf.setSyncTs(System.currentTimeMillis());
      psf.setSynced(true);
      psf.update();

    }
    catch (Exception e) {
      e.printStackTrace();
      Log.err("Unexpected Error - not handling gracefully:", e.getMessage());
    }
    return result;
  }

  /**
   * @param xmlText
   * @return Brochure unmarshalled from xmlText, null if unmarshalling failed
   */
  public static Brochure getBrochureResponse(String xmlText) {
    Brochure brochure = null;
    try {
      InputStream source = new ByteArrayInputStream(xmlText.getBytes());
      XMLInputFactory xif = XMLInputFactory.newFactory();
      XMLStreamReader xsr = xif.createXMLStreamReader(source);

      while (xsr.hasNext()) {
        xsr.next(); // Advance to Envelope tag
        if (xsr.getLocalName().equals("GetBrochureResponse")) {
          break;
        }
      }

      JAXBContext jaxbContext = JAXBContext.newInstance(GetBrochureResponse.class);
      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      JAXBElement<GetBrochureResponse> je = jaxbUnmarshaller.unmarshal(xsr, GetBrochureResponse.class);
      brochure = je.getValue().getBrochure();
    }
    catch (XMLStreamException e) {
      Log.err("Ice Portal XML Error while streaming brochure XML :", e);
      e.printStackTrace();
    }
    catch (JAXBException e) {
      Log.err("Ice Portal XML Error while Unmarshalling brochure XML :", e);
      e.printStackTrace();
    }

    return brochure;
  }

  private PoiRS updatePoi(PoiRS poi, Brochure brochure, FeedSrc src) {
    BrochureInfo brochureInfo = brochure.getBrochureInfo();

    if (brochureInfo != null) {
      poi.setSrcReference(Integer.toString(brochureInfo.getIceID()));
      poi.setName(brochureInfo.getHotelName());
      Address address = new Address();

      StringBuilder sb = new StringBuilder(brochureInfo.getAddress1());
      if (brochureInfo.getAddress2().length() > 0) {
        sb.append(" ");
        sb.append(brochureInfo.getAddress2());
      }

      if (brochureInfo.getAddress3().length() > 0) {
        sb.append(" ");
        sb.append(brochureInfo.getAddress3());
      }

      address.setStreetAddress(sb.toString());

      address.setLocality(brochureInfo.getCity());
      address.setRegion(brochureInfo.getState());
      address.setCountryCode(CountriesInfo.Instance().searchByName(brochureInfo.getCountry()).getAlpha3());
      address.setPostalCode(brochureInfo.getZip());
      poi.setMainAddress(address);

      if (brochureInfo.getPhoneNumber() != null && brochureInfo.getPhoneNumber().length() > 0) {
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber(brochureInfo.getPhoneNumber());
        phoneNumber.setPhoneType(PhoneNumber.PhoneType.MAIN);
        phoneNumber.setPrivacy(Privacy.PUBLIC);
        poi.setMainPhoneNumber(phoneNumber);
      }
    }

    BrochureInformation bi = brochure.getInformation();

    if (bi != null &&
        bi.getLongDescription() != null &&
        bi.getLongDescription().getItem() != null) {
      StringBuilder descBuilder = new StringBuilder();
      HtmlToPlainText converter = new HtmlToPlainText();

      for (Item itm : bi.getLongDescription().getItem()) {
        if (itm != null && itm.getText() != null && itm.getText().length() > 0) {
          Document doc = Jsoup.parse(itm.getText());
          descBuilder.append(converter.getPlainText(doc));
        }
      }

      poi.data.setDesc(descBuilder.toString());
    }

    return poi;
  }

  @Override public List<Pair<PoiRS, Integer>> getRankedPois(PoiImportRS importRS, boolean ignoreCountry) {
    List<PoiRS> pois;
    if (ignoreCountry) {
      pois = PoiMgr.findForMerge(importRS.name, null, importRS.typeId);
    }
    else {
      pois = PoiMgr.findForMerge(importRS.name, importRS.countryCode, importRS.typeId);
    }

    List<Pair<PoiRS, Integer>> scores = new ArrayList<>();

    for (PoiRS prs : pois) {
      int currScore = rankPoi(importRS.name, importRS, prs);
      scores.add(new ImmutablePair<>(prs, currScore));
    }

    Collections.sort(scores, new Comparator<Pair<PoiRS, Integer>>() {
      @Override public int compare(Pair<PoiRS, Integer> o1, Pair<PoiRS, Integer> o2) {
        return o2.getRight() - o1.getRight();
      }
    });

    return scores;
  }

  @Override public PoiSrcFeed.FeedSyncResult clearAmenities(PoiSrcFeed psf) {
    throw new NotImplementedException("Ice Portal has no amenities");
  }

  public List<Pair<PoiImportRS, Integer>> getFeedSuggestions(long poiId, int cmpyId) {
    List<Pair<PoiImportRS, Integer>> scores = new ArrayList<>(10);
    PoiRS prs = PoiController.getMergedPoi(poiId, cmpyId);
    if (prs == null) { //Happened when merged records and old ID came back
      Log.err("Failed to find poi record for ID "+poiId+ " when looking for Ice Portal Suggestions");
      return scores;
    }
    List<PoiImportRS> records = PoiImportMgr.getSimilarRecords(prs.getName(),
                                                               prs.getTypeId(),
                                                               FeedSourcesInfo.byName("Ice Portal"),
                                                               prs.countryCode,
                                                               10);

    for (PoiImportRS importRS : records) {
      int currRank = rankPoi(prs.getName(), importRS, prs);
      scores.add(new ImmutablePair<>(importRS, currRank));
    }

    Collections.sort(scores, new Comparator<Pair<PoiImportRS, Integer>>() {
      @Override public int compare(Pair<PoiImportRS, Integer> o1, Pair<PoiImportRS, Integer> o2) {
        return o2.getRight() - o1.getRight();
      }
    });

    return scores;
  }
}
