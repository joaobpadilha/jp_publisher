package com.mapped.publisher.persistence;

import com.avaje.ebean.annotation.EnumValue;

/**
 * Backwards compatible attachment type enumeration
 */
public enum PageAttachType {
  @EnumValue("0")
  FILE_LINK(0, "0"),
  @EnumValue("1")
  VIDEO_LINK(1, "1"),
  @EnumValue("2")
  WEB_LINK(2, "2"),
  @EnumValue("3")
  PHOTO_LINK(3, "3");

  int    ordVal;
  String strVal;

  PageAttachType(int ord, final String str) {
    ordVal = ord;
    strVal = str;
  }

  public int getOrdVal() {
    return ordVal;
  }

  public String getStrVal() {
    return strVal;
  }

  public static PageAttachType fromIntString(final String str) {
    switch (str){
      case "0":
        return FILE_LINK;
      case "1":
        return VIDEO_LINK;
      case "2":
        return WEB_LINK;
      case "3":
      default:
        return PHOTO_LINK;
    }
  }
}
