package com.mapped.publisher.persistence;

import models.publisher.UserProfile;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-23
 * Time: 7:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class UserProfileRS extends UserProfile {

    public String cmpyId;
    public int isCmpyIdOwner;
    public int cmpyLinkType;
    public String linkId;


    public String getCmpyId() {
        return cmpyId;
    }

    public void setCmpyId(String cmpyId) {
        this.cmpyId = cmpyId;
    }

    public int getCmpyIdOwner() {
        return isCmpyIdOwner;
    }

    public void setCmpyIdOwner(int cmpyIdOwner) {
        isCmpyIdOwner = cmpyIdOwner;
    }

    public int getCmpyLinkType() {
        return cmpyLinkType;
    }

    public void setCmpyLinkType(int cmpyLinkType) {
        this.cmpyLinkType = cmpyLinkType;
    }

    public String getLinkId() {
        return linkId;
    }

    public void setLinkId(String linkId) {
        this.linkId = linkId;
    }
}
