package com.mapped.publisher.persistence;

import java.beans.Transient;
import java.sql.Timestamp;

/**
 * Foundation for common Umapped models that can be handled in the same
 * collection
 * Created by surge on 2016-11-03.
 */
public interface IUmappedBaseModel {

  /**
   * This function allows to have consistent function to access PKs which either Long, Integer or String
   * for use in non-strictly enforced foreign keys
   * @return
   */
  @Transient
  String getPkAsString();

  @Transient
  Long getCreatedByPk();

  @Transient
  Long getModifiedByPk();

  @Transient
  String getCreatedByLegacy();

  @Transient
  String getModifiedByLegacy();

  @Transient
  Long  getCreatedEpoch();

  @Transient
  Timestamp getCreatedTs();

  @Transient
  Long  getModifiedEpoch();

  @Transient
  Timestamp getModifiedTs();

}
