package com.mapped.publisher.persistence;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;
import com.avaje.ebean.SqlUpdate;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.TripBrandingView;
import com.umapped.api.schema.types.PhoneNumber;
import models.publisher.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by twong on 2014-07-16.
 */
public class TripBrandingMgr {

  public static enum COBRANDING_TAG {
    SKI_COM_TRAVELPLAN ("ski_com_travelplan", "Travelplan", "https://s3.amazonaws.com/umapped_prd/1025492311240001542_ski_com_travelplan.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2090285139&Signature=8Ng%2FOF2LHSSqTXXIK0u73vMphh0%3D", "1025492311240001542_ski_com_travelplan.png", "", "www.travelplanski.com", ""),
    SKI_COM_SUNVALLEY ("ski_com_sunvalley", "Visit Sun Valley", "https://s3.amazonaws.com/umapped_prd/1015656900740005313_ski_com_sunvalley.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2089307304&Signature=Yr9bGW3wW%2B1WBx2KPm5eFiPzsoM%3D", "1015656900740005313_ski_com_sunvalley.png", "", "www.visitsunvalley.com", "866-265-4197"),
    SKI_COM_ROCKYMOUNTAINTOURS ("ski_com_rockymountaintours", "Rocky Mountain Tours", "https://s3.amazonaws.com/umapped_prd/1015136690730019475_ski_com_rockymountaintours.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2089307350&Signature=CiR7u34%2BIy1ZnpxcVle6N5RnYys%3D", "1015136690730019475_ski_com_rockymountaintours.png", "", "www.rockymountaintours.com", "800-525-7547"),
    SKI_COM_DELTAVACATIONS ("ski_com_deltavacations", "Delta Vacations", "https://s3.amazonaws.com/umapped_prd/1015132040720019514_ski_com_deltavacations.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2089307400&Signature=vXsMwwkF3tV0E3ogTitLuybiv8I%3D", "1015132040720019514_ski_com_deltavacations.png", "", "www.deltavacationsski.com", "800-754-8599"),
    SKI_COM_HOLAM ("ski_com_holam", "Holam", "https://s3.amazonaws.com/umapped_prd/1015136690730019477_ski_com_holam.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2089307435&Signature=MgdwLGYWY0F2sF6a6KXkpDWSQMM%3D", "1015136690730019477_ski_com_holam.png", "", "www.skiholam.mx", "55-1084-1799"),
    SKI_COM_MOUNTAILTRAVEL ("ski_com_mountaintravel", "Mountain.travel", "https://s3.amazonaws.com/umapped_prd/1015136690730019488_ski_com_mountaintravel.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2089307484&Signature=fsHktk4xcHnzDhGRLzJ%2F52Ba9Og%3D", "1015136690730019488_ski_com_mountaintravel.png", "", "www.mountain.travel", "877-284-7548"),
    SKI_COM_UNITEDVACATIONS ("ski_com_unitedvacations", "United Vacations", "https://s3.amazonaws.com/umapped_prd/1015136690730019489_ski_com_unitedvacations.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2089307525&Signature=PpM8bQmnHG2QYcOQGzewxMiyqeI%3D", "1015136690730019489_ski_com_unitedvacations.png", "", "www.unitedvacationsski.com", "800-309-4543"),
    SKI_COM_ASCR ("ski_com_ascr", "Aspen Snowmass Cen Res", "https://s3.amazonaws.com/umapped_prd/1015132040720019520_ski_com_ascr.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2089307570&Signature=joUyUsBNaJMVHTwN2M3x0XW3Y8E%3D", "1015132040720019520_ski_com_ascr.png", "", "www.snowmass.com", "800-923-8920"),
    SKI_COM_SKITRAVELAGENTS ("ski_com_skitravelagents", "Ski Travel Agents", "https://s3.amazonaws.com/umapped_prd/1015132040720019527_ski_com_skitravelagents.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2089307609&Signature=eXGZ2D89ImpUdWRHbKLJAunrKWI%3D", "1015132040720019527_ski_com_skitravelagents.png", "", "www.skitravelagents.com", ""),
    SKI_COM_SSV ("ski_com_ssv", "Sell Mountain Vacations", "https://s3.amazonaws.com/umapped_prd/920269968500066855_logo_skicom.png?Expires=2079802012&AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Signature=WECVP8DCTA%2BXZF2bFvaCVjKVQ9c%3D", "920269968500066855_logo_skicom.png", "", "www.sellmountainvacations.com","800-469-8795"),
    SKI_COM_COLLEGESKITRIPS ("ski_com_collegeskitrips", "College Ski Trips", "https://s3.amazonaws.com/umapped_prd/1015656900740005350_ski_com_collegeskitrips.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2089307777&Signature=rfIsL4OIPjeHUGCg1Wdpea9vfoo%3D", "1015656900740005350_ski_com_collegeskitrips.png", "", "www.collegeskitrips.com", "800-313-0300"),
    SKI_COM_RMVR ("ski_com_rmvr", "Rocky Mountain Vacation Rentals", "https://s3.amazonaws.com/umapped_prd/1015136690730019520_ski_com_rmvr.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2089307826&Signature=%2FaGP8eUt6j36iM7lCBo7ryGGKBI%3D", "1015136690730019520_ski_com_rmvr.png", "", " www.rockymountainvacationrentals.com", "877-825-4094"),
    SKI_COM_ROCKIES ("ski_com_ski_rockies", "Ski the Rockies", "https://s3.amazonaws.com/umapped_prd/1215253074520004077_ski_ski_the_rockies.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2109248231&Signature=M7s7BSAHAqROwSZhh1k8f9%2BUK0g%3D", "1215253074520004077_ski_ski_the_rockies.png", "", " www.skitherockies.com", "800-291-2588"),
    SKI_COM_BRECK ("ski_com_breck", "Go Breck", "https://s3.amazonaws.com/umapped_prd/1217139994650025276_ski_gobreck.jpg?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2109460966&Signature=JDKJL1712kshBmjQSRmTToJ3bNE%3D", "1217139994650025276_ski_gobreck.jpg", "", " www.gobreck.com", "888-251-2417"),
    SKI_COM_APPLE ("ski_com_apple", "Apple Vacations", "https://s3.amazonaws.com/umapped_prd/1215253074520008199_ski_apple_logo.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2109248488&Signature=tPAPY8p0nKc0QXBakDCZvdNfV18%3D", "1215253074520008199_ski_apple_logo.png", "", " http://ski.applevacations.com/", "800-517-2000"),
    SKI_COM_WARREN_MILLER ("ski_com_warren_miller", "Warren Miller", "https://s3.amazonaws.com/umapped_prd/920269968500066855_logo_skicom.png?Expires=2079802012&AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Signature=WECVP8DCTA%2BXZF2bFvaCVjKVQ9c%3D", "920269968500066855_logo_skicom.png", "", " http://www.skinet.com/warrenmiller/travel", "844-711-4600"),
    SKI_COM_ROOST ("ski_com_roost", "Vacation Roost", "https://s3.amazonaws.com/umapped_prd/1215255084530003775_ski_roost.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2109248205&Signature=pxtYngDxRB%2BIooJz6A1wlJYGt6w%3D", "1215255084530003775_ski_roost.png", "", " www.vacationroost.com", "888-337-6678"),
    SKI_COM_POWDERHOUND ("ski_com_powderhounds", "Powderhounds.com", "https://s3.amazonaws.com/umapped_prd/1215264494540007204_ski_powderhounds.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2109248423&Signature=V%2FU2WjoBJTc5Atzm2FyigSl%2BJY0%3D", "1215264494540007204_ski_powderhounds.png", "", " www.powderhounds.com", "866-265-4192"),
    SKI_COM_SUMMER ("ski_com_summer", "summermountaintravel.com", "https://s3.amazonaws.com/umapped_prd/1215264494540005561_ski_summer_mountain_travel.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2109248318&Signature=RcWjqhYGbp6hFKsQcizPX9LeEM4%3D", "1215264494540005561_ski_summer_mountain_travel.png", "", " www.summermountaintravel.com", "800-450-0943"),
    SKI_COM_MTN_RESERVATIONS ("ski_com_mtn_reservations", "Mountain Reservations", "https://s3.amazonaws.com/umapped_prd/1215264494540008018_ski_mtn_reservation.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2109248462&Signature=1yIE2qZV5w71KwqpKhFNFCm2Zuc%3D", "1215264494540008018_ski_mtn_reservation.png", "", " www.mountainreservations.com", "800-754-9378"),
    SKI_COM_FIVE_STAR ("ski_com_five_star_colorado", "Mountain Reservations", "https://s3.amazonaws.com/umapped_prd/1215253074520009266_fivestarcolorado.png?AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Expires=2109250818&Signature=SB%2FRe71GZwWuci3TILYlsmTHRmU%3D", "1215253074520009266_fivestarcolorado.png", "", " www.fivestarcolorado.com", "888-267-5475"),
    ;



    public String tag;
    public String url;
    public String logoName;
    public String name;
    public String email;
    public String web;
    public String phone;

    private COBRANDING_TAG (String tag, String name, String url, String logoName, String email, String web, String phone) {
      this.tag = tag;
      this.url = url;
      this.logoName = logoName;
      this.name = name;
      this.email = email;
      this.web = web;
      this.phone = phone;
    }

  };

  private final static String SHARED_CMPIES = "select c.cmpyid  from trip_branding tb, company c where tb.tripid = :tripid and tb.cmpyid = c.cmpyid and " +
                                              "(  tb.cmpyid " +
                                              " in \n" +
                                              "(select distinct(c.parent_cmpy_id) as parentCmpyId from trip_share ts, " +
                                              "user_cmpy_link ucl, company c where \n" +
                                              "ts.tripid = :tripid and ts.userid = ucl.userid and ucl.status = 0 and c.parent_cmpy_id is not null " +
                                              "and ts.status = 0 " +
                                              "and ucl.cmpyid = c.cmpyid and c.status = 0) or " +
                                              "tb.cmpyid  in (select c.parent_cmpy_id from trip t, company c where t.tripid = :tripid and t.cmpyid = c.cmpyid and c.parent_cmpy_id is not null))";

  private final static String UNSHARED_CMPIES = "select c.cmpyid, tb.ismainbrand  from trip_branding tb, company c where tb.tripid = :tripid and tb.cmpyid = c.cmpyid and tb.cmpyid " +
                                                "not in \n" +
                                                "(select distinct(c.cmpyid) as cmpyid from trip_share ts, " +
                                                "user_cmpy_link ucl, company c where \n" +
                                                "ts.tripid = :tripid and ts.userid = ucl.userid and ucl.status = 0 " +
                                                "and ts.status = 0 " +
                                                "and ucl.cmpyid = c.cmpyid and c.status = 0) and " +
                                                "tb.cmpyid not in (select cmpyid from trip where tripid = :tripid) " +
                                                "and tb.cmpyid not in ( " +  SHARED_CMPIES + " )";




  private final static String NEWLY_SHARED_CMPIES = "select distinct(c.cmpyid)as cmpyid , c.parent_cmpy_id as parentCmpyId  from trip_share ts, " +
                                                    "user_cmpy_link ucl, company c where \n" +
                                                    "ts.tripid = :tripid and ts.userid = ucl.userid and ucl.status = " +
                                                    "0 " +
                                                    "and ucl.cmpyid = c.cmpyid and c.status = 0 and ts.status = 0 and" +
                                                    " " +
                                                    "((c.cmpyid not in (select cmpyid from trip_branding where tripid =" +
                                                    " :tripid) and " +
                                                    "c.cmpyid not in (select cmpyid from trip where tripid = :tripid)) or" +
                                                    "(c.parent_cmpy_id is not null and c.parent_cmpy_id not in " +
                                                    "(select cmpyid from trip_branding where tripid =:tripid)))" +
                                                    " ;";

  private final static String BRANDED_CMPIES = "select c.cmpyid as cmpyid, c.name as name, c.logourl as logo, c.logoname as logoname, " +
                                               " tb.isenabled as enabled,   tb.isMainBrand as mainbrand, " +
                                               " tb.position as position " +
                                               " , ca.email as email, ca.phone as phone, ca.web as web, " +
                                               " ca.facebook as facebook, ca.twitter as twitter " +
                                               " from trip_branding tb, company c " +
                                               " left join cmpy_addr ca on c.cmpyid = ca.cmpyid where " +
                                               " tb.tripid = :tripid and tb.cmpyid = c.cmpyid and c.status = 0 " +
                                               " order by tb.position;";


  private final static String ACTIVE_BRANDED_CMPIES = "select c.cmpyid as cmpyid, c.name as name, c.logourl as logo,c.logoname as logoname, " +
                                                      " tb.isenabled as enabled,   tb.isMainBrand as mainbrand, " +
                                                      " tb.position as position, ca.email as email, ca.fax as fax, ca.phone as phone," +
                                                      " ca.web as web, ca.facebook as facebook, " +
                                                      " ca.twitter as twitter " +
                                                      " from trip_branding tb, company c " +
                                                      " left join cmpy_addr ca on c.cmpyid = ca.cmpyid where " +
                                                      " tb.tripid = :tripid and tb.isEnabled = :isEnabled and " +
                                                      " tb.cmpyid = c.cmpyid and c.status = 0 " +
                                                      " order by tb.position;";


  /**
   * Creates list of cmpies based on shared trips
   *
   * @param tripId tripid
   * @return Empty list if passed parameters are wrong or no activity for the specified parameters, full list otherwise
   */
  public static List<String> findUnSharedCmpies(String tripId) {
    List<String> cmpies = new ArrayList<String>();

    //Nothing to see here, wrong parameters to call this perform this query
    if (tripId == null) {
      return cmpies;
    }

    SqlQuery sqlQuery;
    sqlQuery = Ebean.createSqlQuery(UNSHARED_CMPIES);
    sqlQuery.setParameter("tripid", tripId);


    List<SqlRow> rs = sqlQuery.findList();

    for (SqlRow sr : rs) {
      String cmpyId = sr.getString("cmpyid");
      boolean isMainBrand = sr.getBoolean("ismainbrand");
      if (cmpyId != null ) {
        cmpies.add(cmpyId);
        if (isMainBrand) {
          //reset the main brand to the default cmpy
          Trip trip = Trip.find.byId(tripId);
          if (trip != null) {
            TripBranding tb =  TripBranding.findActiveByPK(tripId, trip.getCmpyid());
            if (tb != null) {
              tb.setIsmainbrand(true);
              tb.setIsenabled(true);
              tb.save();
            } else {
              tb = new TripBranding();
              TripBrandingPK pk = new TripBrandingPK(trip.tripid, trip.getCmpyid());
              tb.setPk(pk);
              tb.setCreatedby("system");
              tb.setCreatedtimestamp(System.currentTimeMillis());
              tb.setIsenabled(true);
              tb.setIsmainbrand(true);
              tb.setLastupdatedtimestamp(tb.getCreatedtimestamp());
              tb.setModifiedby("system");
              tb.setPosition(0);
              tb.save();
            }
          }
        }
      }
    }

    return cmpies;
  }

  public static List<String> findNewSharedCmpies(String tripId) {
    List<String> cmpies = new ArrayList<String>();

    //Nothing to see here, wrong parameters to call this perform this query
    if (tripId == null) {
      return cmpies;
    }

    SqlQuery sqlQuery;
    sqlQuery = Ebean.createSqlQuery(NEWLY_SHARED_CMPIES);
    sqlQuery.setParameter("tripid", tripId);


    List<SqlRow> rs = sqlQuery.findList();

    for (SqlRow sr : rs) {
      String cmpyId = sr.getString("cmpyid");
      if (cmpyId != null) {
        cmpies.add(cmpyId);
        //new feature to automaticallly include linked companies
        String parentCmpyId = sr.getString("parentCmpyId");
        if (parentCmpyId != null && parentCmpyId.trim().length() > 0 && !cmpies.contains(parentCmpyId)) {
          cmpies.add(parentCmpyId);
        }

      }
    }

    return cmpies;
  }


  public static int deleteUnsharedCmpies(String tripId) {
    List<String> cmpies = findUnSharedCmpies(tripId);

    if (cmpies != null && cmpies.size() > 0) {
      for (String cmpyId : cmpies) {
        TripBrandingMgr.deleteTripBranding(tripId, cmpyId);
      }

      return cmpies.size();
    }

    return 0;
  }

  public static int addNewSharedCmpies(String tripId, String cmpyId) {
    TripBranding tb = TripBranding.findActiveByPK(tripId, cmpyId);
    if (tb == null) {
      //set default
      tb = new TripBranding();
      tb.pk = new TripBrandingPK(tripId, cmpyId);
      tb.isenabled = true;
      tb.ismainbrand = true;
      tb.createdby = "system";
      tb.createdtimestamp = System.currentTimeMillis();
      tb.modifiedby = "system";
      tb.lastupdatedtimestamp = System.currentTimeMillis();
      tb.position = 0;
      tb.save();
    }

    List<String> cmpies = findNewSharedCmpies(tripId);
    Company c = Company.find.byId(cmpyId);
    if (c != null && c.getParentCmpyId() != null && c.getParentCmpyId().trim().length() > 0 && !cmpies.contains(c.getParentCmpyId())) {
      cmpies.add(c.getParentCmpyId());
    }

    if (cmpies != null && cmpies.size() > 0) {
      for (String brandedCmpyId : cmpies) {
        try {
          tb = new TripBranding();
          tb.pk = new TripBrandingPK(tripId, brandedCmpyId);
          tb.isenabled = false;
          tb.ismainbrand = false;
          tb.createdby = "system";
          tb.createdtimestamp = System.currentTimeMillis();
          tb.modifiedby = "system";
          tb.lastupdatedtimestamp = System.currentTimeMillis();
          tb.position = 1;
          tb.save();
        } catch (Exception e) {

        }
      }

      return cmpies.size();
    }

    return 0;

  }

  public static int deleteTripBranding(String tripId, String cmpyId) {
    String s = "DELETE from TRIP_BRANDING where tripid = :tripid and cmpyid = :cmpyid";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("tripid", tripId);
    update.setParameter("cmpyid", cmpyId);
    return Ebean.execute(update);
  }

  public static List<TripBrandingView> findBrandedCmpies(Trip trip) {
    List<TripBrandingView> cmpies = new ArrayList<TripBrandingView>();

    //Nothing to see here, wrong parameters to call this perform this query
    if (trip == null) {
      return cmpies;
    }


    SqlQuery sqlQuery;
    sqlQuery = Ebean.createSqlQuery(BRANDED_CMPIES);
    sqlQuery.setParameter("tripid", trip.getTripid());


    List<SqlRow> rs = sqlQuery.findList();

    for (SqlRow sr : rs) {
      TripBrandingView view = buildBrandingView(sr);
      cmpies.add(view);
    }

    //allow tag based co-branding ie cmpy uses a tag in the trip to change the branding
    TripBrandingView tagBranding = buildTripCobrandingByTag(trip, cmpies);
    if (tagBranding != null) {
      cmpies.add(tagBranding);
    }

    return cmpies;
  }

  public static List<TripBrandingView> findActiveBrandedCmpies(Trip trip) {
    List<TripBrandingView> cmpies = new ArrayList<TripBrandingView>();

    //Nothing to see here, wrong parameters to call this perform this query
    if (trip == null) {
      return cmpies;
    }


    SqlQuery sqlQuery;
    sqlQuery = Ebean.createSqlQuery(ACTIVE_BRANDED_CMPIES);
    sqlQuery.setParameter("tripid", trip.getTripid());
    sqlQuery.setParameter("isEnabled", true);


    List<SqlRow> rs = sqlQuery.findList();

    for (SqlRow sr : rs) {
      TripBrandingView view = buildBrandingView(sr);
      cmpies.add(view);
    }

    //allow tag based co-branding ie cmpy uses a tag in the trip to change the branding
    TripBrandingView tagBranding = buildTripCobrandingByTag(trip, cmpies);
    if (tagBranding != null) {
      cmpies.add(tagBranding);
    }

    return cmpies;
  }

  public static Company getMainContact(List<TripBrandingView> cmpies) {
    if (cmpies != null) {
      for (TripBrandingView tb : cmpies) {
        if (tb.isMainBrand) {
          return Company.find.byId(tb.cmpyId);
        }
      }
    }

    return null;
  }

  public static TripBrandingView buildBrandingView(SqlRow sr) {
    TripBrandingView view = new TripBrandingView();
    view.cmpyId = sr.getString("cmpyid");
    view.cmpyLogoUrl = Utils.escapeS3Umapped_Prd(sr.getString("logo"));
    view.cmpyLogoName = sr.getString("logoname");

    view.cmpyName = sr.getString("name");
    view.isEnabled = sr.getBoolean("enabled");
    view.isMainBrand = sr.getBoolean("mainBrand");
    view.position = sr.getInteger("position");
    view.cmpyEmail = sr.getString("email");
    view.cmpyFacebook = sr.getString("facebook");
    view.cmpyPhone = sr.getString("phone");
    view.cmpyTwitter = sr.getString("twitter");
    view.cmpyWebsite = sr.getString("web");
    view.cmpyFax = sr.getString("fax");
    return view;
  }

  public static TripBrandingView buildTripCobrandingByTag (Trip trip, List<TripBrandingView> views) {

    if (trip != null && trip.tag != null && !trip.tag.isEmpty()) {
      String cmpyName = null;
      String cmpyLogo = null;
      String cmpyEmail = null;
      String cmpyWeb = null;
      String cmpyPhone = null;
      String cmpyLogoName = null;

      String tripTag = trip.tag.toLowerCase();
      boolean widelogo = false;

      if (tripTag.contains(COBRANDING_TAG.SKI_COM_ASCR.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_ASCR.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_ASCR.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_ASCR.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_ASCR.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_ASCR.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_ASCR.logoName;
      } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_COLLEGESKITRIPS.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_COLLEGESKITRIPS.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_COLLEGESKITRIPS.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_COLLEGESKITRIPS.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_COLLEGESKITRIPS.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_COLLEGESKITRIPS.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_COLLEGESKITRIPS.logoName;
      } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_DELTAVACATIONS.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_DELTAVACATIONS.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_DELTAVACATIONS.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_DELTAVACATIONS.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_DELTAVACATIONS.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_DELTAVACATIONS.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_DELTAVACATIONS.logoName;
      } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_HOLAM.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_HOLAM.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_HOLAM.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_HOLAM.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_HOLAM.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_HOLAM.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_HOLAM.logoName;
      } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_MOUNTAILTRAVEL.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_MOUNTAILTRAVEL.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_MOUNTAILTRAVEL.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_MOUNTAILTRAVEL.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_MOUNTAILTRAVEL.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_MOUNTAILTRAVEL.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_MOUNTAILTRAVEL.logoName;
      } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_RMVR.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_RMVR.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_RMVR.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_RMVR.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_RMVR.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_RMVR.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_RMVR.logoName;
      } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_ROCKYMOUNTAINTOURS.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_ROCKYMOUNTAINTOURS.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_ROCKYMOUNTAINTOURS.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_ROCKYMOUNTAINTOURS.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_ROCKYMOUNTAINTOURS.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_ROCKYMOUNTAINTOURS.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_ROCKYMOUNTAINTOURS.logoName;
      } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_SKITRAVELAGENTS.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_SKITRAVELAGENTS.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_SKITRAVELAGENTS.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_SKITRAVELAGENTS.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_SKITRAVELAGENTS.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_SKITRAVELAGENTS.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_SKITRAVELAGENTS.logoName;
      } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_SSV.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_SSV.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_SSV.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_SSV.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_SSV.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_SSV.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_SSV.logoName;
      } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_SUNVALLEY.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_SUNVALLEY.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_SUNVALLEY.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_SUNVALLEY.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_SUNVALLEY.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_SUNVALLEY.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_SUNVALLEY.logoName;
      } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_UNITEDVACATIONS.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_UNITEDVACATIONS.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_UNITEDVACATIONS.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_UNITEDVACATIONS.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_UNITEDVACATIONS.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_UNITEDVACATIONS.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_UNITEDVACATIONS.logoName;
      } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_TRAVELPLAN.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_TRAVELPLAN.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_TRAVELPLAN.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_TRAVELPLAN.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_TRAVELPLAN.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_TRAVELPLAN.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_TRAVELPLAN.logoName;
      }  else if (tripTag.contains(COBRANDING_TAG.SKI_COM_ROCKIES.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_ROCKIES.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_ROCKIES.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_ROCKIES.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_ROCKIES.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_ROCKIES.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_ROCKIES.logoName;
      }  else if (tripTag.contains(COBRANDING_TAG.SKI_COM_BRECK.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_BRECK.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_BRECK.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_BRECK.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_BRECK.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_BRECK.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_BRECK.logoName;
      }  else if (tripTag.contains(COBRANDING_TAG.SKI_COM_APPLE.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_APPLE.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_APPLE.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_APPLE.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_APPLE.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_APPLE.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_APPLE.logoName;
      }  else if (tripTag.contains(COBRANDING_TAG.SKI_COM_WARREN_MILLER.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_WARREN_MILLER.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_WARREN_MILLER.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_WARREN_MILLER.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_WARREN_MILLER.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_WARREN_MILLER.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_WARREN_MILLER.logoName;
      }  else if (tripTag.contains(COBRANDING_TAG.SKI_COM_ROOST.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_ROOST.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_ROOST.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_ROOST.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_ROOST.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_ROOST.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_ROOST.logoName;
      }   else if (tripTag.contains(COBRANDING_TAG.SKI_COM_POWDERHOUND.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_POWDERHOUND.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_POWDERHOUND.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_POWDERHOUND.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_POWDERHOUND.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_POWDERHOUND.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_POWDERHOUND.logoName;
        widelogo = true;
      }   else if (tripTag.contains(COBRANDING_TAG.SKI_COM_SUMMER.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_SUMMER.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_SUMMER.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_SUMMER.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_SUMMER.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_SUMMER.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_SUMMER.logoName;
      }   else if (tripTag.contains(COBRANDING_TAG.SKI_COM_MTN_RESERVATIONS.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_MTN_RESERVATIONS.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_MTN_RESERVATIONS.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_MTN_RESERVATIONS.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_MTN_RESERVATIONS.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_MTN_RESERVATIONS.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_MTN_RESERVATIONS.logoName;
      }   else if (tripTag.contains(COBRANDING_TAG.SKI_COM_FIVE_STAR.tag)) {
        cmpyName = COBRANDING_TAG.SKI_COM_FIVE_STAR.name;
        cmpyLogo = COBRANDING_TAG.SKI_COM_FIVE_STAR.url;
        cmpyEmail = COBRANDING_TAG.SKI_COM_FIVE_STAR.email;
        cmpyPhone = COBRANDING_TAG.SKI_COM_FIVE_STAR.phone;
        cmpyWeb = COBRANDING_TAG.SKI_COM_FIVE_STAR.web;
        cmpyLogoName = COBRANDING_TAG.SKI_COM_FIVE_STAR.logoName;
        widelogo = true;
      }


      if (cmpyLogo != null && cmpyName != null) {
        TripBrandingView view = null;
        if (views != null) {
          for (TripBrandingView tb : views) {
            if (tb.cmpyId.equals(trip.cmpyid)) {
              view = tb;
              break;
            }
          }
        }

        if (view == null) {
          view = new TripBrandingView();
          view.cmpyId = trip.getCmpyid();
          view.isEnabled = true;
          view.isMainBrand = true;
          view.position = 0;
        } else {
          views.remove(view);
        }
        view.isOverwritenByTag = true;
        view.cmpyLogoUrl = cmpyLogo;
        view.cmpyLogoName = cmpyLogoName;
        view.cmpyName = cmpyName;
        view.cmpyEmail = cmpyEmail;
        view.cmpyFacebook = null;
        view.cmpyPhone = cmpyPhone;
        view.cmpyTwitter = null;
        view.cmpyWebsite = cmpyWeb;
        if (widelogo) {
          view.cmpyTag = "widelogo";
        }
        return view;
      }
    }

    return null;

  }

  public static String getWhieLabelEmail(Trip trip, String email) {
    if (email == null || !email.toLowerCase().contains("@ski.com")) {
      return email;
    }
    if (trip.tag != null && !trip.tag.isEmpty()) {
      String tripTag = trip.tag.toLowerCase();
      if (email != null && email.indexOf("@") > 0) {
        String email1 = email.substring(0, email.indexOf("@"));
        if (tripTag.contains(COBRANDING_TAG.SKI_COM_ASCR.tag)) {
          email = email1 + "@snowmass.com";
        } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_COLLEGESKITRIPS.tag)) {
          email = email1 +  "@collegeskitrips.com";

        } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_DELTAVACATIONS.tag)) {
          email = email1 +  "@deltavacationsski.com";

        } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_HOLAM.tag)) {
          email = email1 +  "@skiholam.com";

        } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_MOUNTAILTRAVEL.tag)) {
          email = email1 +  "@mountain.travel";

        } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_RMVR.tag)) {
          email = email1 +  "@gormvr.com";

        } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_ROCKYMOUNTAINTOURS.tag)) {
          email = email1 +  "@ski.com";

        } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_SKITRAVELAGENTS.tag)) {
          email = email1 +  "@skitravelagents.com";

        } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_SSV.tag)) {
          email = email1 +  "@ski.com";

        } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_SUNVALLEY.tag)) {
          email = email1 +  "@book.skivacation.com";

        }  else if (tripTag.contains(COBRANDING_TAG.SKI_COM_FIVE_STAR.tag) ||
                    tripTag.contains(COBRANDING_TAG.SKI_COM_POWDERHOUND.tag) ||
                    tripTag.contains(COBRANDING_TAG.SKI_COM_ROCKIES.tag) ||
                    tripTag.contains(COBRANDING_TAG.SKI_COM_BRECK.tag) ||
                    tripTag.contains(COBRANDING_TAG.SKI_COM_APPLE.tag) ||
                    tripTag.contains(COBRANDING_TAG.SKI_COM_WARREN_MILLER.tag)) {
          email = email1 +  "@book.vacationski.com";

        } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_UNITEDVACATIONS.tag)) {
          email = email1 +  "@unitedvacationsski.com";

        } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_TRAVELPLAN.tag)) {
          email = email1 +  "@travelplanski.com";

        } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_SUMMER.tag)) {
          email = email1 +  "@summermountaintravel.com";

        } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_ROOST.tag)) {
          email = email1 +  "@vacationroost.com";

        } else if (tripTag.contains(COBRANDING_TAG.SKI_COM_MTN_RESERVATIONS.tag)) {
          email = email1 +  "@mountainreservations.com";

        }
      }
    }


    return  email;
  }

  public static String getWhiteLabelTag(String agent) {
    if (agent != null) {
      agent = agent.toLowerCase();
      if (agent.contains("snowmass")) {
        return COBRANDING_TAG.SKI_COM_ASCR.tag;
      } else if (agent.contains("collegeskitrips")) {
        return COBRANDING_TAG.SKI_COM_COLLEGESKITRIPS.tag;
      } else if (agent.contains("deltavacationsski")) {
        return COBRANDING_TAG.SKI_COM_DELTAVACATIONS.tag;
      } else if (agent.contains("skiholam")) {
        return COBRANDING_TAG.SKI_COM_HOLAM.tag;
      } else if (agent.contains("@mountain.travel")) {
        return COBRANDING_TAG.SKI_COM_MOUNTAILTRAVEL.tag;
      } else if (agent.contains("gormvr")) {
        return COBRANDING_TAG.SKI_COM_RMVR.tag ;
      } else if (agent.contains("rockymountaintours")) {
        return COBRANDING_TAG.SKI_COM_ROCKYMOUNTAINTOURS.tag;
      } else if (agent.contains("skitravelagents")) {
        return COBRANDING_TAG.SKI_COM_SKITRAVELAGENTS.tag;
      } else if (agent.contains("sellmountainvacations") || agent.contains("skicom")) {
        return COBRANDING_TAG.SKI_COM_SSV.tag;
      } else if (agent.contains("book.skivacation")) {
        return COBRANDING_TAG.SKI_COM_SUNVALLEY.tag;
      } else if (agent.contains("unitedvacationsski")) {
        return COBRANDING_TAG.SKI_COM_UNITEDVACATIONS.tag;
      }else if (agent.contains("travelplan")) {
        return COBRANDING_TAG.SKI_COM_TRAVELPLAN.tag;
      }else if (agent.contains("skirockies")) {
        return COBRANDING_TAG.SKI_COM_ROCKIES.tag;
      } else if (agent.contains("gobreck")) {
        return COBRANDING_TAG.SKI_COM_BRECK.tag;
      } else if (agent.contains("apple")) {
        return COBRANDING_TAG.SKI_COM_APPLE.tag;
      } else if (agent.contains("warrenmiller")) {
        return COBRANDING_TAG.SKI_COM_WARREN_MILLER.tag;
      } else if (agent.contains("vacroost")) {
        return COBRANDING_TAG.SKI_COM_ROOST.tag;
      } else if (agent.contains("powderhounds")) {
        return COBRANDING_TAG.SKI_COM_POWDERHOUND.tag;
      } else if (agent.contains("summermtntravel")) {
        return COBRANDING_TAG.SKI_COM_SUMMER.tag;
      } else if (agent.contains("mountres")) {
        return COBRANDING_TAG.SKI_COM_MTN_RESERVATIONS.tag;
      } else if (agent.contains("fivestars")) {
        return COBRANDING_TAG.SKI_COM_FIVE_STAR.tag;
      }
    }
    return null;
  }

  public static TripBrandingView getOverwrittenByTag (List<TripBrandingView> views, Trip trip) {
    if (views != null) {
      for (TripBrandingView tb: views) {
        if (tb.isMainBrand && tb.isEnabled && tb.isOverwritenByTag && tb.cmpyId.equals(trip.getCmpyid())) {
          return tb;
        }
      }
    }
    return null;
  }

  public static boolean hasOverwriteBrandingTag (String tripTag) {
    if (tripTag != null && !tripTag.isEmpty()) {
      if (tripTag.contains(COBRANDING_TAG.SKI_COM_ASCR.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_COLLEGESKITRIPS.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_DELTAVACATIONS.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_HOLAM.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_MOUNTAILTRAVEL.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_RMVR.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_ROCKYMOUNTAINTOURS.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_SKITRAVELAGENTS.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_SSV.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_SUNVALLEY.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_UNITEDVACATIONS.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_TRAVELPLAN.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_ROCKIES.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_BRECK.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_APPLE.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_WARREN_MILLER.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_ROOST.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_POWDERHOUND.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_SUMMER.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_MTN_RESERVATIONS.tag)
              || tripTag.contains(COBRANDING_TAG.SKI_COM_FIVE_STAR.tag)) {
        return true;
      }
    }
    return false;

  }

  public static String getCompanyExtension (Account agent, String cmpyPhone) {
    if (agent != null && cmpyPhone != null && !cmpyPhone.isEmpty()) {
      //check to see if we need to add an extension to the phone number
      //extension is stored in the ext field of the account contact
      AccountProp prop = AccountProp.findByPk(agent.getUid(), AccountProp.PropertyType.CONTACT);
      if (prop != null && prop.accountContact != null && prop.accountContact.phones != null) {
        PhoneNumber ext = prop.accountContact.phones.get(PhoneNumber.PhoneType.EXT);
        if (ext != null && ext.getNumber() != null && !ext.getNumber().isEmpty()) {
          cmpyPhone += " " + ext.getNumber();
        }
      }
    }
    return cmpyPhone;
  }
}
