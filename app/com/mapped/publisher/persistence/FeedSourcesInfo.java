package com.mapped.publisher.persistence;

import com.mapped.publisher.utils.Log;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by surge on 2015-02-11.
 */
public class FeedSourcesInfo {
  TreeMap<Integer, String> byId;
  Map<String, Integer> byName;

  private FeedSourcesInfo() {
    byId = new TreeMap();
    byName = new HashMap<>(5);
  }

  public void addSource(Integer id, String name) {
    byId.put(id, name);
    byName.put(name, id);
  }

  public static int byName(String name) {
    Integer result = SourceInfoHelper.INSTANCE.byName.get(name);
    if (result == null) {
      Log.err("POI Source Info requested unknown source name:" + name);
      return 0;
    }
    return result;
  }

  public static String byId(Integer id) {
    return SourceInfoHelper.INSTANCE.byId.get(id);
  }

  //Bill Pugh's Java Singleton Approach, most efficient Java singleton
  private static class SourceInfoHelper {
    private static final FeedSourcesInfo INSTANCE = new FeedSourcesInfo();
  }

  public static FeedSourcesInfo Instance() {
    return SourceInfoHelper.INSTANCE;
  }

  public static Map<Integer, String> getIdMap() {
    return (Map<Integer, String>) SourceInfoHelper.INSTANCE.byId.clone();
  }
}
