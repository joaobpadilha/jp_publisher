package com.mapped.publisher.persistence.template;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.utils.Log;
import org.postgresql.util.PGobject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by surge on 2015-10-04.
 */
public class TmpltMgr {
  final static String SQL_META_INSERT;
  final static String SQL_META_UPDATE;
  final static String SQL_META_DELETE;

  static {
    SQL_META_INSERT = "INSERT INTO tmplt_meta (tmplt_id, data, created_ts, created_by, modified_ts, modified_by) " +
                      "VALUES (?, ?, ?, ?, ?, ?)";

    SQL_META_UPDATE = "UPDATE tmplt_meta " +
                      "SET   data = ?, modified_ts = ?, modified_by = ? " +
                      "WHERE tmplt_id = ?";

    SQL_META_DELETE = "DELETE FROM tmplt_meta " +
                      "WHERE tmplt_id = ?";
  }

  /**
   * Using this specific routine to get connection.
   *
   * Future optimization can be to implement a naive connection pool for vendors.
   * @return Database connection
   */
  private static Connection getConnection() {
    Connection conn = null;

    try {
      conn = DBConnectionMgr.getConnection4Publisher();
    }
    catch (Exception e) {
      Log.err("Failed to obtain database connection for POI operation:" + e.getMessage());
      e.printStackTrace();
    }
    return conn;
  }

  /**
   * Returning connection
   */
  private static void releaseConnection(Connection conn) {
    try {
      conn.close();
    } catch (SQLException e) {
      Log.err("Failed to release database connection:" + e.getMessage());
      e.printStackTrace();
    }
  }

  public static boolean save(TmpltMetaRs rec) {
    boolean result = false;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_META_INSERT);
      int paramCounter = 0;
      //SQL Order:
      //tmplt_id, data, created_ts, created_by, modified_ts, modified_by
      query.setString(++paramCounter, rec.getTemplateId());
      ObjectMapper jsonMapper = new ObjectMapper();
      String json = jsonMapper.writeValueAsString(rec.data);
      PGobject jsonPGSQLObject = new PGobject();
      jsonPGSQLObject.setType("jsonb");
      jsonPGSQLObject.setValue(json);
      query.setObject(++paramCounter, jsonPGSQLObject);
      query.setLong(++paramCounter,   rec.createdTs);
      query.setString(++paramCounter, rec.createdBy);
      query.setLong(++paramCounter,   rec.modifiedTs);
      query.setString(++paramCounter, rec.modifiedBy);

      if (query.executeUpdate() == 0){
        Log.info("Unexpected result from insert query :" + query.toString());
      } else  {
        result = true;
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while inserting template metadata",e );
      e.printStackTrace();
    }
    catch (JsonProcessingException e) {
      Log.err("Failed to marshal POJO to Json when saving template metadata", e);
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

  public static boolean update(TmpltMetaRs rec) {
    boolean result = false;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_META_UPDATE);
      int paramCounter = 0;
      //SQL Order:
      //data = ?::jsonb, modified_ts = ?, modified_by = ?
      ObjectMapper jsonMapper = new ObjectMapper();
      String json = jsonMapper.writeValueAsString(rec.data);
      PGobject jsonPGSQLObject = new PGobject();
      jsonPGSQLObject.setType("jsonb");
      jsonPGSQLObject.setValue(json);
      query.setObject(++paramCounter, jsonPGSQLObject);
      query.setLong(++paramCounter,   rec.modifiedTs);
      query.setString(++paramCounter, rec.modifiedBy);
      query.setString(++paramCounter, rec.getTemplateId());

      if (query.executeUpdate() == 0){
        Log.info("Unexpected result from update query :" + query.toString());
      } else  {
        result = true;
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while updating template metadata", e);
      e.printStackTrace();
    }
    catch (JsonProcessingException e) {
      Log.err("Failed to marshal POJO to Json when saving template metadata", e);
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

  public static boolean delete(String templateId) {
    boolean result = false;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }
    try {
      PreparedStatement query = conn.prepareStatement(SQL_META_DELETE);
      int paramCounter = 0;
      //SQL Order:
      //data = ?::jsonb, modified_ts = ?, modified_by = ?
      query.setString(++paramCounter, templateId);

      if (query.executeUpdate() == 0){
        Log.info("Unexpected result from delete query :" + query.toString());
      } else  {
        result = true;
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while deleting template metadata", e);
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

}
