package com.mapped.publisher.persistence.template;

import java.util.ArrayList;
import java.util.List;

/**
 * JSON version of this class will be stored in tmplt_meta table, data column.
 * Created by surge on 2015-10-04.
 */
public class TmpltMetaJson {
  String name;
  /**
   * Big Five calls this series, but really it is categories
   */
  List<String> categories;
  String description;
  String highlights;
  Integer length;
  Float price;
  Currency currency;
  String priceInfo;
  String itineraryDesc;
  List<String> images;
  List<Attachment> attachments;
  List<String> countries;
  List<String> links;

  /**
   * Season/year in which template is available
   */
  String season;

  public TmpltMetaJson(String name) {
    this.name = name;
    categories = new ArrayList<>(1);
    images = new ArrayList<>(1);
    attachments = new ArrayList<>(1);
    countries = new ArrayList<>(1);
    links = new ArrayList<>(1);
  }

  public String getSeason() {
    return season;
  }

  public void setSeason(String season) {
    this.season = season;
  }

  public List<String> getLinks() {
    return links;
  }

  public void setLinks(List<String> links) {
    this.links = links;
  }

  public List<String> getCategories() {
    return categories;
  }

  public void setCategories(List<String> categories) {
    this.categories = categories;
  }

  public List<String> getImages() {
    return images;
  }

  public void setImages(List<String> images) {
    this.images = images;
  }

  public List<Attachment> getAttachments() {
    return attachments;
  }

  public void setAttachments(List<Attachment> attachments) {
    this.attachments = attachments;
  }

  public List<String> getCountries() {
    return countries;
  }

  public void setCountries(List<String> countries) {
    this.countries = countries;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getHighlights() {
    return highlights;
  }

  public void setHighlights(String highlights) {
    this.highlights = highlights;
  }

  public Integer getLength() {
    return length;
  }

  public void setLength(Integer length) {
    this.length = length;
  }

  public Float getPrice() {
    return price;
  }

  public void setPrice(Float price) {
    this.price = price;
  }

  public Currency getCurrency() {
    return currency;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }

  public String getPriceInfo() {
    return priceInfo;
  }

  public void setPriceInfo(String priceInfo) {
    this.priceInfo = priceInfo;
  }

  public String getItineraryDesc() {
    return itineraryDesc;
  }

  public void setItineraryDesc(String itineraryDesc) {
    this.itineraryDesc = itineraryDesc;
  }

}
