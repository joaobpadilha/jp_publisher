package com.mapped.publisher.persistence.template;

/**
 * Created by surge on 2015-10-04.
 */
public class TmpltMetaRs {
  String templateId;
  TmpltMetaJson data;
  String createdBy;
  String modifiedBy;
  Long createdTs;
  Long modifiedTs;

  public static TmpltMetaRs buildMeta(String userId, String templateId, String name) {
    TmpltMetaRs metaRS = new TmpltMetaRs();

    metaRS.templateId = templateId;
    metaRS.createdBy = userId;
    metaRS.modifiedBy = userId;
    metaRS.createdTs = System.currentTimeMillis();
    metaRS.modifiedTs = metaRS.createdTs;
    metaRS.data = new TmpltMetaJson(name);

    return metaRS;
  }

  public String getTemplateId() {
    return templateId;
  }

  public void setTemplateId(String templateId) {
    this.templateId = templateId;
  }

  public TmpltMetaJson getData() {
    return data;
  }

  public void setData(TmpltMetaJson data) {
    this.data = data;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public Long getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Long createdTs) {
    this.createdTs = createdTs;
  }

  public Long getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Long modifiedTs) {
    this.modifiedTs = modifiedTs;
  }
}
