package com.mapped.publisher.persistence;

import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.persistence.cache.PoiFeedStateCO;
import com.mapped.publisher.utils.Log;
import models.publisher.FeedSrc;
import models.publisher.PoiSrcFeed;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by surge on 2015-01-30.
 */
public class PoiImportMgr {
  final static boolean DEBUG = false;

  final static String SQL_INSERT;
  final static String SQL_UPDATE;
  final static String SQL_GET_IMPORT;
  final static String SQL_GET_FILTERED;
  final static String SQL_GET_ONE;
  final static String SQL_PURGE;
  final static String SQL_MOVE_INSERT;
  final static String SQL_MOVE_UPDATE;
  final static String SQL_DELETE_ONE;
  final static String SQL_DELETE_ALL;
  final static String SQL_RECORDS_PRESENT;
  final static String SQL_DELETE_DUPLICATES;
  final static String SQL_WIPE_ALL;
  final static String SQL_UPDATE_DUP_MATCH;
  final static String SQL_FIND_SIMILAR;
  final static String SQL_FIND_UNSYNCED; //Works with 'poi_src_feed' not poi_import
  final static String SQL_GET_LAST_IMPORT_TS;
  final static String SQL_SET_UNPROCESSED_FOR_LOW_SCORE_DUPLICATES;
  final static String SQL_UPDATE_SRC_REF_MATCHED;
  static {

    SQL_GET_LAST_IMPORT_TS = "SELECT import_ts " +
                             "FROM poi_src_feed " +
                             "WHERE src_id = ? " +
                             "ORDER BY import_ts DESC " +
                             "LIMIT 1";

    SQL_FIND_UNSYNCED = "SELECT poi_id, src_id, src_reference, raw, hash, import_ts, is_synced, sync_ts, version " +
                        "FROM poi_src_feed " +
                        "WHERE src_id = ? AND is_synced = FALSE";

    SQL_FIND_SIMILAR = "SELECT import_id, state, src_id, poi_id, hash, type_id, import_ts, src_reference, " +
                       "        src_filename, name, country_name, country_code, phone, region, city, address, " +
                       "        postal_code, loc_lat, loc_long, raw, version " +
                       "FROM poi_import  " +
                       "WHERE type_id = ? AND src_id = ? AND country_code = ? AND  " +
                       "       (name ILIKE ? OR  " +
                       "        to_tsvector('english',name) @@ plainto_tsquery(?) OR  " +
                       "        metaphone(name, 6) = metaphone(?, 6)) " +
                       "LIMIT ?";

    SQL_UPDATE_DUP_MATCH = "UPDATE poi_import  " +
                           "SET state = 'UNPROCESSED'  " +
                           "WHERE poi_id IN (SELECT poi_id " +
                           "                 FROM poi_import " +
                           "                 WHERE poi_id IS NOT NULL AND src_id = ? " +
                           "                 GROUP BY  poi_id,src_id " +
                           "                 HAVING COUNT(*) > 1)";

    SQL_DELETE_DUPLICATES = "DELETE FROM poi_import " +
                            "WHERE import_id IN (SELECT import_id " +
                            "              FROM (SELECT import_id, row_number() over (partition BY src_id, " +
                            "                           src_reference, name ORDER BY import_id DESC) AS rnum " +
                            "                    FROM poi_import) t " +
                            "              WHERE t.rnum > 1) AND " +
                            "      src_id = ?";

    SQL_WIPE_ALL = "DELETE FROM poi_import " +
                   "WHERE src_id = ?";


    SQL_RECORDS_PRESENT = "SELECT COUNT(*) FROM poi_import WHERE src_id = ? AND src_reference = ?";

    SQL_INSERT = "INSERT INTO poi_import (state, src_id, poi_id, hash, type_id, import_ts, " +
                 "src_reference, src_filename, name, country_name, country_code, phone, region, city, address, " +
                 "postal_code, loc_lat, loc_long, raw) " +
                 "VALUES (cast(? as um_poi_import_state), ?, ?, decode(?, 'hex'), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
                 "?, ?, ?, ?)";

    SQL_UPDATE = "UPDATE poi_import SET state = cast(? as um_poi_import_state), src_id = ?, " +
                 "                      poi_id = ?, hash = decode(?, 'hex'), type_id = ?, import_ts = ?, src_reference = ?, " +
                 "                      src_filename = ?, name = ?, country_name = ?, country_code = ?, " +
                 "                      phone = ?, region = ?, city = ?, address = ?, postal_code = ?, " +
                 "                      loc_lat = ?, loc_long = ?, raw = ?, score=?, version = ? " +
                 "WHERE import_id = ?";

    SQL_GET_FILTERED = "SELECT import_id, state, src_id, poi_id, encode(hash,'hex'), type_id, " +
                       "       import_ts, src_reference, src_filename, name, country_name, country_code, " +
                       "       phone, region, city, address, postal_code, loc_lat, loc_long, raw, version  " +
                       "FROM poi_import " +
                       "WHERE state = cast(? as um_poi_import_state) AND src_id = ? " +
                       "LIMIT ?";

    SQL_GET_IMPORT = "SELECT import_id, state, src_id, poi_id, encode(hash,'hex'), type_id, " +
                     "       import_ts, src_reference, src_filename, name, country_name, country_code," +
                     "       phone, region, city, address, postal_code, loc_lat, loc_long, raw, version " +
                     "FROM poi_import " +
                     "WHERE state='UNPROCESSED' AND src_id=? AND type_id = ?";

    SQL_GET_ONE = "SELECT import_id, state, src_id, poi_id, encode(hash,'hex'), type_id, " +
                     "       import_ts, src_reference, src_filename, name, country_name, country_code," +
                     "       phone, region, city, address, postal_code, loc_lat, loc_long, raw, version " +
                     "FROM poi_import " +
                     "WHERE import_id = ?";

    SQL_PURGE = "DELETE FROM poi_import  " +
                "WHERE import_id IN (SELECT import_id " +
                "                    FROM poi_import pi, poi_src_feed sf " +
                "                    WHERE pi.hash = sf.hash AND pi.src_id = ?)";

    SQL_MOVE_INSERT = "INSERT INTO poi_src_feed (poi_id, src_id, src_reference, raw, hash, import_ts)  " +
                      "SELECT pi.poi_id, pi.src_id, pi.src_reference, pi.raw, pi.hash, pi.import_ts " +
                      "FROM poi_import pi " +
                      "WHERE state = 'MATCH' AND  " +
                      "      src_id = ? AND " +
                      "      NOT EXISTS (SELECT poi_id, src_id FROM poi_src_feed WHERE poi_id = pi.poi_id AND  " +
                      "                                                                src_id = pi.src_id)";

    SQL_MOVE_UPDATE = "UPDATE poi_src_feed AS sf " +
                      "SET src_reference = pi.src_reference, " +
                      "    raw = pi.raw,  " +
                      "    hash = pi.hash, " +
                      "    import_ts = pi.import_ts, " +
                      "    is_synced = FALSE " +
                      "FROM  poi_import pi " +
                      "WHERE pi.src_reference = sf.src_reference AND sf.src_id = pi.src_id AND pi.hash != sf.hash AND" +
                      "      pi.src_id = ?";

    SQL_DELETE_ONE = "DELETE FROM poi_import WHERE import_id = ?;";
    SQL_DELETE_ALL = "DELETE FROM poi_import WHERE src_id = ? AND state = cast(? as um_poi_import_state)";

    SQL_SET_UNPROCESSED_FOR_LOW_SCORE_DUPLICATES = "WITH poi_score  AS (SELECT poi_id, Max(score) max_score "
                                                   + "FROM poi_import WHERE src_id = ? AND poi_id IS NOT NULL "
                                                   + "GROUP BY poi_id HAVING Count(*) > 1) "
                                                   + "UPDATE poi_import SET state = 'UNSURE', poi_id = null WHERE import_id IN "
                                                   + "(SELECT import_id FROM poi_import, poi_score WHERE  poi_import.poi_id = poi_score.poi_id "
                                                   + "AND poi_import.score != poi_score.max_score)";

    SQL_UPDATE_SRC_REF_MATCHED = "UPDATE poi_import SET poi_id=poi.poi_id, state='MATCH' " +
                                 " FROM poi WHERE poi.src_id=poi_import.src_id and poi.src_reference = poi_import.src_reference" +
                                 " and poi_import.state='UNPROCESSED' and poi_import.src_id=?";
  }

  /**
   * Using this specific routine to get connection.
   *
   * Future optimization can be to implement a naive connection pool for vendors.
   * @return Database connection
   */
  private static Connection getConnection() {

    Connection conn = null;

    try {
      conn = DBConnectionMgr.getConnection4Publisher();
    }
    catch (Exception e) {
      Log.err("Failed to obtain database connection for POI operation:" + e.getMessage());
      e.printStackTrace();
    }

    return conn;
  }

  /**
   * Returning connection
   */
  private static void releaseConnection(Connection conn) {
    try {
      conn.close();
    } catch (SQLException e) {
      Log.err("Failed to release database connection:" + e.getMessage());
      e.printStackTrace();
    }
  }

  /**
   *
   * @param importRS
   * @return
   */
  public static boolean save(PoiImportRS importRS) {
    boolean result = false;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_INSERT);
      int paramCounter = 0;
      //SQL Order:
      //state, src_id, consortium_id, cmpy_id, poi_id, hash, type_id, import_ts,
      //src_reference, name, country_name, country_code, phone, region,
      //city, address, postal_code, loc_lat, loc_long, raw

      query.setString(++paramCounter, importRS.state.name()); //Import state
      query.setInt(++paramCounter, importRS.srcId);
      if (importRS.poiId == null) {
        query.setNull(++paramCounter, Types.BIGINT);
      } else {
        query.setLong(++paramCounter, importRS.poiId);
      }
      query.setString(++paramCounter, importRS.hash);
      query.setInt(++paramCounter, importRS.typeId);
      query.setLong(++paramCounter, importRS.timestamp);
      query.setString(++paramCounter, importRS.srcReference);
      query.setString(++paramCounter, importRS.srcFilename);
      query.setString(++paramCounter, importRS.name);
      query.setString(++paramCounter, importRS.countryName);
      query.setString(++paramCounter, importRS.countryCode);
      query.setString(++paramCounter, importRS.phone);
      query.setString(++paramCounter, importRS.region);
      query.setString(++paramCounter, importRS.city);
      query.setString(++paramCounter, importRS.address);
      query.setString(++paramCounter, importRS.postalCode);
      if (importRS.latitude != null && importRS.longitude != null) {
        query.setFloat(++paramCounter, importRS.latitude);
        query.setFloat(++paramCounter, importRS.longitude);
      } else {
        query.setNull(++paramCounter, Types.FLOAT);
        query.setNull(++paramCounter, Types.FLOAT);
      }
      query.setString(++paramCounter, importRS.raw);

      if (query.executeUpdate() == 0){
        Log.info("Unexpected result from insert query :" + query.toString());
      } else  {
        result = true;
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while inserting poi import:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

  /**
   * Updates existing record in  new connection.
   * @param importRS
   * @return
   */
  public static boolean update(PoiImportRS importRS) {
    boolean result = false;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_UPDATE);
      int paramCounter = 0;
      //SQL Order:
      // state = ?, src_id = ?, consortium_id = ?, cmpy_id = ?,
      // poi_id = ?, hash = ?, type_id = ?, import_ts = ?, src_reference = ?,
      // src_filename = ?, name = ?, country_name = ?, country_code = ?,
      // phone = ?, region = ?, city = ?, address = ?, postal_code = ?,
      // raw = encode(?, 'hex'), version = ?
      // WHERE import_id = ?;

      query.setString(++paramCounter, importRS.state.name()); //Import state
      query.setInt(++paramCounter, importRS.srcId);
      if (importRS.poiId == null) {
        query.setNull(++paramCounter, Types.BIGINT);
      } else {
        query.setLong(++paramCounter, importRS.poiId);
      }

      query.setString(++paramCounter, importRS.hash);
      query.setInt(++paramCounter, importRS.typeId);
      query.setLong(++paramCounter, importRS.timestamp);
      query.setString(++paramCounter, importRS.srcReference);
      query.setString(++paramCounter, importRS.srcFilename);
      query.setString(++paramCounter, importRS.name);
      query.setString(++paramCounter, importRS.countryName);
      query.setString(++paramCounter, importRS.countryCode);
      query.setString(++paramCounter, importRS.phone);
      query.setString(++paramCounter, importRS.region);
      query.setString(++paramCounter, importRS.city);
      query.setString(++paramCounter, importRS.address);
      query.setString(++paramCounter, importRS.postalCode);
      if (importRS.latitude != null && importRS.longitude != null) {
        query.setFloat(++paramCounter, importRS.latitude);
        query.setFloat(++paramCounter, importRS.longitude);
      } else {
        query.setNull(++paramCounter, Types.FLOAT);
        query.setNull(++paramCounter, Types.FLOAT);
      }
      query.setString(++paramCounter, importRS.raw);
      if (importRS.score !=null) {
        query.setInt(++paramCounter, importRS.score);
      } else {
        query.setNull(++paramCounter, Types.INTEGER);
      }
      query.setInt(++paramCounter,    ++importRS.version);
      query.setLong(++paramCounter, importRS.importId);

      if (query.executeUpdate() == 0){
        Log.info("Unexpected result from update query :" + query.toString());
      } else  {
        result = true;
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while updating poi import:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

  /**
   * Deleting all records for the specified source
   * @return
   */
  public static int wipeAll(int srcId) {
    int result = 0;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }
    try {
      PreparedStatement query = conn.prepareStatement(SQL_WIPE_ALL);
      int paramCounter = 0;
      query.setInt(++paramCounter, srcId);
      result = query.executeUpdate();
      if (result == 0) {
        Log.info("No records to delete for poi source id: " + srcId);
      } else {
        Log.debug("Deleted " + result + " records from poi_import for source id: " + srcId);
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while deleting feed import:", e);
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

  /**
   * For duplicate poi_import that match the same poi, set the lower scores record
   * back to unprocessed
   */
  public static void setToUnprocessedForLowScoreDuplicate(int srcId) {
    Connection conn = getConnection();
    if (conn == null) {
      return;
    }
    try {
      PreparedStatement query = conn.prepareStatement(SQL_SET_UNPROCESSED_FOR_LOW_SCORE_DUPLICATES);
      int paramCounter = 0;
      query.setInt(++paramCounter, srcId);

      query.executeUpdate();
    }
    catch(SQLException e){
      Log.err("SQL Exception while deleting feed import duplicates:", e );
    }
    finally {
      releaseConnection(conn);
    }
    return;
  }

  /**
   * Deletes duplicates preserving only the most recently imported version
   * @return
   */
  public static int deleteDuplicates(int srcId) {
    int result = 0;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }
    try {
      PreparedStatement query = conn.prepareStatement(SQL_DELETE_DUPLICATES);
      int paramCounter = 0;
      query.setInt(++paramCounter, srcId);

      result = query.executeUpdate();
      if (result == 0) {
        Log.info("No duplicates found while deleting duplicates");
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while deleting feed import duplicates:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

  /**
   * Deletes duplicates preserving only the most recently imported version
   * @return
   */
  public static int unprocessDuplicates(int srcId) {
    int result = 0;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }
    try {
      PreparedStatement query = conn.prepareStatement(SQL_UPDATE_DUP_MATCH);
      int paramCounter = 0;
      query.setInt(++paramCounter, srcId);

      result = query.executeUpdate();
      if (result == 0) {
        Log.info("No duplicates found while deleting duplicates");
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while deleting feed import duplicates:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

  public static boolean deleteOne(long importId) {
    boolean result = false;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_DELETE_ONE);
      int paramCounter = 0;
      query.setLong(++paramCounter, importId);

      if (query.executeUpdate() == 0){
        Log.info("Unexpected result from delete query:" + query.toString());
      } else  {
        result = true;
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while deleting imported record:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

  /**
   * Deletes all from specified source in specific state
   * @param srcId
   * @param state
   * @return
   */
  public static boolean deleteAll(int srcId, PoiImportRS.ImportState state) {
    boolean result = false;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_DELETE_ALL);
      int paramCounter = 0;
      query.setLong(++paramCounter, srcId);
      query.setString(++paramCounter, state.name());

      if (query.executeUpdate() == 0){
        Log.info("Unexpected result from delete all query:" + query.toString());
      } else  {
        result = true;
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while deleting all imported records:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

  public static int clearAmenities(int srcId) {
    int result = 0;
    try {
      FeedSrc feedSrc = FeedSrc.find.byId(srcId);
      FeedHelperPOI helper = (FeedHelperPOI) Class.forName(feedSrc.getLoaderClass()).newInstance();
      PoiFeedStateCO feedState = (PoiFeedStateCO) CacheMgr.get(APPConstants.CACHE_POI_FEED_STATE +
                                                               Integer.toString(srcId));
      if (feedState == null) {
        feedState = new PoiFeedStateCO();
      }

      List<PoiSrcFeed> gone = PoiSrcFeed.getSourceRemovedRecords(srcId);
      feedState.start(PoiFeedStateCO.State.AMENITIES, gone.size());

      for(PoiSrcFeed rec : gone) {
        if (++feedState.currItem % 5 == 0) {
          feedState.update();
          CacheMgr.set(APPConstants.CACHE_POI_FEED_STATE + Integer.toString(srcId), feedState);
        }
        PoiSrcFeed.FeedSyncResult fsr = helper.clearAmenities(rec);
        if(fsr == PoiSrcFeed.FeedSyncResult.UPDATED) {
          result++;
        }
      }

      feedState.stop();
      CacheMgr.set(APPConstants.CACHE_POI_FEED_STATE +
                   Integer.toString(srcId), feedState);
    } catch (Exception e) {
      Log.err("Error while clearing amenities for src: " + srcId, e);
      e.printStackTrace();
    }
    return result;
  }

  /**
   * Deletes all imported feed records that have matching hash in poi_src_feed table
   * @param srcId
   * @return number of deleted records
   */
  public static int purge(int srcId) {
    int result = 0;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_PURGE);
      int paramCounter = 0;
      query.setInt(++paramCounter, srcId);

      result = query.executeUpdate();
      if (result == 0){
        Log.debug("No records were purged :" + query.toString());
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while purging poi import:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

  /**
   * Moves all matched data feed records (inserts new ones and updates existing).
   * @param srcId
   * @return number of changed records records
   */
  public static int insertMatched(int srcId) {
    int result = 0;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }
    try {
      PreparedStatement query = conn.prepareStatement(SQL_MOVE_INSERT);
      int paramCounter = 0;
      query.setInt(++paramCounter, srcId);

      result = query.executeUpdate();
      if (result == 0){
        Log.debug("No records were inserted :" + query.toString());
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while purging poi import:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

  /**
   * Moves all matched data feed records (inserts new ones and updates existing).
   * @param srcId
   * @return number of changed records records
   */
  public static int updateModified(int srcId) {
    int result = 0;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }
    try {
      PreparedStatement query = conn.prepareStatement(SQL_MOVE_UPDATE);
      int paramCounter = 0;
      query.setInt(++paramCounter, srcId);

      result = query.executeUpdate();
      if (result == 0){
        Log.debug("No records were updated in poi feed:" + query.toString());
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while updating modified poi feed:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

  public static Map<PoiSrcFeed.FeedSyncResult, Integer> syncMatchedWithPoi(int srcId, final FeedHelperPOI helper) {
    final Map<PoiSrcFeed.FeedSyncResult, Integer> result = new HashMap<>();
    result.put(PoiSrcFeed.FeedSyncResult.NEW, 0);
    result.put(PoiSrcFeed.FeedSyncResult.UPDATED, 0);
    result.put(PoiSrcFeed.FeedSyncResult.DELETED, 0);
    result.put(PoiSrcFeed.FeedSyncResult.FAIL, 0);

    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PoiFeedStateCO feedState = (PoiFeedStateCO) CacheMgr.get(APPConstants.CACHE_POI_FEED_STATE +
                                                               Integer.toString(srcId));
      if (feedState == null) {
        feedState = new PoiFeedStateCO();
      }

      // make sure autocommit is off - required for streaming
      conn.setAutoCommit(false);
      PreparedStatement query = conn.prepareStatement(SQL_FIND_UNSYNCED);
      // Turn use of the cursor on with cache of 50 RS
      query.setFetchSize(50);

      int paramCounter = 0;
      query.setInt(++paramCounter, srcId);
      ResultSet rs = query.executeQuery();

      feedState.start(PoiFeedStateCO.State.POI_SYNC, 0);

      while(rs.next()) {
        paramCounter = 0;
        if (++feedState.currItem % 50 == 0) {
          feedState.update();
          CacheMgr.set(APPConstants.CACHE_POI_FEED_STATE + Integer.toString(srcId), feedState);
        }

        long poi_id = rs.getLong(++paramCounter);
        int src_id = rs.getInt(++paramCounter);

        PoiSrcFeed psf = new PoiSrcFeed();
        psf.setFeedId(poi_id, src_id);
        psf.setSrcReference(rs.getString(++paramCounter));
        psf.setRaw(rs.getString(++paramCounter));
        psf.setHash(rs.getBytes(++paramCounter));
        psf.setImportTs(rs.getLong(++paramCounter));
        psf.setSynced(rs.getBoolean(++paramCounter));
        psf.setSyncTs(rs.getLong(++paramCounter));
        psf.setVersion(rs.getInt(++paramCounter));

        StopWatch sw = new StopWatch();
        sw.start();

        PoiSrcFeed.FeedSyncResult currRes = helper.synchronize(psf);
        result.put(currRes, result.get(currRes) + 1);
        sw.stop();
        Log.debug("Synchronized " + psf.getFeedId().poiId + " in " + sw.getTime() + "ms");
      }

      feedState.stop();
      CacheMgr.set(APPConstants.CACHE_POI_FEED_STATE +
                   Integer.toString(srcId), feedState);
    }
    catch(SQLException e){
      Log.err("SQL Exception while searching poi_src_feed unsynced:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }

  public static int associateMatchedSrcReference(int srcId) {
    Connection conn = getConnection();
    int results = 0;
    if (conn == null) {
      return results;
    }
    try {
      PreparedStatement query = conn.prepareStatement(SQL_UPDATE_SRC_REF_MATCHED);
      int paramCounter = 0;
      query.setInt(++paramCounter, srcId);

      results = query.executeUpdate();
    }
    catch(SQLException e){
      Log.err("SQL Exception while deleting feed import duplicates:", e );
    }
    finally {
      releaseConnection(conn);
    }
    return results;
  }

  /**
   * Performs data merge.
   * For each of the import records looks for matching POI records and performs association
   * @note Assumes that data is already imported.
   * @note Here we are not inserting new poi records.
   * @param srcId - Import source
   * @param poiType - Import record type to process
   * @return
   */
  public static Map<String, Integer> associateImportFeed(int srcId, int poiType, FeedHelperPOI processor) {
    Map<String, Integer> results = new HashMap<>();
    results.put("UNPROCESSED", 0);
    results.put("MATCH", 0);
    results.put("MISMATCH", 0);
    results.put("UNSURE", 0);
    results.put("DELETED", 0);

    Connection conn = getConnection();
    if (conn == null) {
      return results;
    }

    try {
      PoiFeedStateCO feedState = (PoiFeedStateCO) CacheMgr.get(APPConstants.CACHE_POI_FEED_STATE +
                                                               Integer.toString(srcId));
      if (feedState == null) {
        feedState = new PoiFeedStateCO();
      }

      // make sure autocommit is off - required for streaming
      conn.setAutoCommit(false);
      PreparedStatement query = conn.prepareStatement(SQL_GET_IMPORT);
      // Turn use of the cursor on with cache of 50 RS
      query.setFetchSize(50);

      int paramCounter = 0;
      query.setInt(++paramCounter, srcId);
      query.setInt(++paramCounter, poiType);
      ResultSet rs = query.executeQuery();

      feedState.start(PoiFeedStateCO.State.ASSOCIATE, 0);

      while(rs.next()) {
        if (++feedState.currItem % 50 == 0) {
          feedState.update();
          CacheMgr.set(APPConstants.CACHE_POI_FEED_STATE +
                       Integer.toString(srcId), feedState);
        }

        PoiImportRS pirs = getPirsFromResultSet(rs);
        PoiImportRS.ImportState newState = processor.associate(pirs);

        switch (newState) {
          case UNPROCESSED:
            results.put("UNPROCESSED", results.get("UNPROCESSED") + 1);
            break;
          case MATCH:
            results.put("MATCH", results.get("MATCH") + 1);
            break;
          case MISMATCH:
            results.put("MISMATCH", results.get("MISMATCH") + 1);
            break;
          case UNSURE:
            results.put("UNSURE", results.get("UNSURE") + 1);
            break;
        }
      }

      // Let processor first deal with duplicate records
      processor.processDuplicateRecord(srcId);

      //During matching there might be duplicate POI association to handle them
      //separately from Mismatch or Unsure setting state to UNPROCESSED

      int dupMatches = unprocessDuplicates(srcId);
      results.put("UNPROCESSED", results.get("UNPROCESSED") + dupMatches);

      feedState.stop();
      CacheMgr.set(APPConstants.CACHE_POI_FEED_STATE +
                   Integer.toString(srcId), feedState);
    }
    catch(SQLException e){
      Log.err("SQL Exception while searching poi import:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }

    return results;
  }

  /**
   * Number of records satisfying the required parameters.
   *
   * @param srcId
   * @param referenceId
   * @return number of records with srcId and referenceId as specified
   */
  public static int isImported(int srcId, String referenceId) {

    int result = 0;

    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }
    try {
      PreparedStatement query = conn.prepareStatement(SQL_RECORDS_PRESENT);
      int paramCounter = 0;
      query.setInt(++paramCounter, srcId);
      query.setString(++paramCounter, referenceId);
      ResultSet rs = query.executeQuery();

      while(rs.next()) {
        paramCounter = 0;
        result = rs.getInt(++paramCounter);
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while counting poi import:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }


  public static List<PoiImportRS> getImportedPois(int feedSrcId, PoiImportRS.ImportState state, int limit) {
    List<PoiImportRS> results = new ArrayList<>();

    Connection conn = getConnection();
    if (conn == null) {
      return results;
    }
    try {
      PreparedStatement query = conn.prepareStatement(SQL_GET_FILTERED);
      int paramCounter = 0;
      query.setString(++paramCounter, state.name());
      query.setInt(++paramCounter, feedSrcId);
      query.setInt(++paramCounter, limit);

      ResultSet rs = query.executeQuery();
      while(rs.next()) {
        PoiImportRS pirs = getPirsFromResultSet(rs);
        results.add(pirs);
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while searching poi import:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }

    return results;
  }


  public static PoiImportRS getOne(long importId) {
    PoiImportRS result = null;

    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }
    try {
      PreparedStatement query = conn.prepareStatement(SQL_GET_ONE);
      int paramCounter = 0;
      query.setLong(++paramCounter, importId);

      ResultSet rs = query.executeQuery();
      while(rs.next()) {
        result = getPirsFromResultSet(rs);
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while searching poi import:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }

    return result;
  }

  private static PoiImportRS getPirsFromResultSet(ResultSet rs) {
    PoiImportRS pirs = new PoiImportRS();

    int paramCounter = 0;

    try {
      //  import_id, state, src_id, poi_id, encode(hash,'hex'), type_id,
      //  import_ts, src_reference, src_filename, name, country_name, country_code,
      //  phone, region, city, address, postal_code, loc_lat, loc_long, raw, version

      pirs.importId = rs.getLong(++paramCounter);
      pirs.state = PoiImportRS.ImportState.valueOf(rs.getString(++paramCounter));
      pirs.srcId = rs.getInt(++paramCounter);
      pirs.poiId = rs.getLong(++paramCounter);
      pirs.hash = rs.getString(++paramCounter);
      pirs.typeId = rs.getInt(++paramCounter);
      pirs.timestamp = rs.getLong(++paramCounter);
      pirs.srcReference = rs.getString(++paramCounter);
      pirs.srcFilename = rs.getString(++paramCounter);
      pirs.name = rs.getString(++paramCounter);
      pirs.countryName = rs.getString(++paramCounter);
      pirs.countryCode = rs.getString(++paramCounter);
      pirs.phone = rs.getString(++paramCounter);
      pirs.region = rs.getString(++paramCounter);
      pirs.city = rs.getString(++paramCounter);
      pirs.address = rs.getString(++paramCounter);
      pirs.postalCode = rs.getString(++paramCounter);
      pirs.latitude = rs.getFloat(++paramCounter);
      pirs.longitude = rs.getFloat(++paramCounter);
      pirs.raw = rs.getString(++paramCounter);

    }
    catch(SQLException e){
      Log.err("SQL Exception while converting poi: " + e.getMessage());
      e.printStackTrace();
    }

    return pirs;
  }

  public static PoiSrcFeed moveOneToFeed(long importId, boolean ignoreMatch, String userId){
    PoiSrcFeed feedRecord = buildPoiSourceFeed(importId, ignoreMatch ? 0 : -1, userId);
    feedRecord.save();
    deleteOne(importId);
    return feedRecord;
  }

  public static PoiSrcFeed moveOneToFeed(long importId, long poiId, String userId){
    PoiSrcFeed feedRecord = buildPoiSourceFeed(importId, poiId, userId);
    feedRecord.save();
    deleteOne(importId);
    return feedRecord;
  }

  /**
   * Building new PoiSourceFeed Record
   * @param importId  feed record id
   * @param poiId     if -1, will ignore importId match, if 0 will preserve import match, if > 0 will use this poiId
   * @param userId    who is performing action
   * @return
   */
  public static PoiSrcFeed buildPoiSourceFeed(long importId, long poiId, String userId) {
    PoiImportRS importRS = getOne(importId);
    PoiSrcFeed feedRecord;
    if (poiId > 0) {
      importRS.poiId = poiId;
      feedRecord = PoiSrcFeed.buildNewRecord(importRS, false, userId);
    } else if (poiId == 0) {
      feedRecord = PoiSrcFeed.buildNewRecord(importRS, true, userId);
    } else {
      feedRecord = PoiSrcFeed.buildNewRecord(importRS, false, userId);
    }
    feedRecord.setSynced(false);
    return feedRecord;
  }

  /**
   * Creates new PoiSrcFeed records for each of the mismatched
   * @param srcId
   * @return count of converted records
   */
  public static int moveAllToFeed(int srcId,PoiImportRS.ImportState state, boolean ignoreMatch, String userId){
    int result = 0;

    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      // make sure autocommit is off - required for streaming
      conn.setAutoCommit(false);
      PreparedStatement query = conn.prepareStatement(SQL_GET_FILTERED);
      // Turn use of the cursor on with cache of 50 RS
      query.setFetchSize(50);
      int paramCounter = 0;
      query.setString(++paramCounter, state.name());
      query.setInt(++paramCounter, srcId);
      query.setInt(++paramCounter, Integer.MAX_VALUE);

      ResultSet rs = query.executeQuery();
      while(rs.next()) {
        PoiImportRS importRS = getPirsFromResultSet(rs);
        PoiSrcFeed feedRecord = PoiSrcFeed.buildNewRecord(importRS, ignoreMatch, userId);
        feedRecord.save();
        deleteOne(importRS.importId);
        result++;
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while moving poi import to feed store:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }

    return result;
  }


  public static List<PoiImportRS> getSimilarRecords(String name, int poiType, int srcId, String countryCode , int limit) {
    List<PoiImportRS> results = new ArrayList<>();

    Connection conn = getConnection();
    if (conn == null) {
      return results;
    }
    try {
      PreparedStatement query = conn.prepareStatement(SQL_FIND_SIMILAR);

      //       type_id = ? AND src_id = ? AND country_code = ? AND  " +
      //       (name ILIKE ? OR  " +
      //        to_tsvector('english',name) @@ plainto_tsquery(?) OR  " +
      //        metaphone(name, 6) = metaphone(?, 6))";


      int paramCounter = 0;
      query.setInt(++paramCounter, poiType);
      query.setInt(++paramCounter, srcId);
      query.setString(++paramCounter, countryCode);

      String[] parts = StringUtils.split(name);
      String likeTerm = StringUtils.join(parts,'%');
      query.setString(++paramCounter, '%' + likeTerm + '%');
      query.setString(++paramCounter, name);
      query.setString(++paramCounter, name);
      query.setInt(++paramCounter, limit);

      ResultSet rs = query.executeQuery();
      while(rs.next()) {
        PoiImportRS pirs = getPirsFromResultSet(rs);
        results.add(pirs);
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while looking for similar poi import records:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return results;
  }


  public static long getLastImportTs(int srcId) {
    long result = 0;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_GET_LAST_IMPORT_TS);
      int paramCounter = 0;
      query.setInt(++paramCounter, srcId);

      ResultSet rs = query.executeQuery();
      while(rs.next()) {
        paramCounter = 0;
        result = rs.getLong(++paramCounter);
      }
    }
    catch(SQLException e){
      Log.err("SQL Exception while looking for last imported timestamp:" + e.getMessage());
      e.printStackTrace();
    }
    finally {
      releaseConnection(conn);
    }
    return result;
  }



}
