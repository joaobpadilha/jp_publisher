package com.mapped.publisher.persistence;

import com.amazonaws.services.s3.model.CopyObjectResult;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.mapped.common.CoreConstants;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.S3Util;
import com.mapped.publisher.utils.Utils;
import models.publisher.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.time.StopWatch;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.mapped.publisher.utils.S3Util.S3_DOMAIN;
import static models.publisher.FileSrc.FileSrcType.IMG_USER_UPLOAD;
import static models.publisher.FileSrc.FileSrcType.IMG_VENDOR_ICE_PORTAL;

/**
 * Methods that allow easier operations on image files
 * <p>
 * Created by surge on 2016-10-26.
 */
public class ImageHelper {

  static Map<FileSrc.FileSrcType, FileSrc> fileSrcCache;
  private static Pattern umappedFile = Pattern.compile("/([0-9]{5,})_(.*)");

  static {
    Log.debug("");
    fileSrcCache = new HashMap<>();
    FileSrc.find.all().forEach((FileSrc src) -> fileSrcCache.put(src.getName(), src));
  }

  Account             account;
  FileInfo            file;
  FileImage           fileImage;
  FileImageMigrate    fim;
  FileSrc.FileSrcType srcType;
  LibraryPageImage    pageImage;
  LstFileType         lstFileType;
  /**
   * Url to migrate
   */
  String              migrateUrl;
  /**
   * Bucket of the url to migrate
   */
  String              migrateBucket;
  /**
   * Key of the url to migrate
   */
  String              migrateKey;
  /**
   * MD5 hash/etag of the object where migrate url points
   */
  String              migrateHash;
  /**
   * Filename part extracted from the url (without any prefixes we used in the past)
   */
  String              migrateFilename;
  /**
   * Normalized filename to be migrated
   */
  String              migrateFilenameNormalized;
  /**
   * If file already has an entry in the 'file' table this is the record
   */
  FileInfo            migrateDuplicateFile;
  FileImage           migrateDuplicateFileImage;
  /**
   * Something is wrong with URL and or object
   */
  boolean             migrateFileError;
  /**
   * Stores legacy uncropped URL (if the current file represents cropped version)
   * Use during migration only
   */
  String              migrateUncroppedUrl;
  String              migrateUncroppedBucket;
  String              migrateUncroppedKey;
  /**
   * Suffix used at the end of the bucket name
   */
  String              envSuffix;
  /**
   * For images that were detected to be cropped version
   */
  ImageHelper         croppedSourceHelper;

  /**
   * Whether or not to check for crop
   */
  boolean       checkForCrop  = false;
  long          migrationTime = 0;
  StringBuilder errors        = new StringBuilder();
  StopWatch     stopWatch     = new StopWatch();

  /**
   * All-in-one migration
   *
   * @param url
   * @param usedBy
   * @param id
   * @param checkForCrop
   * @return
   */
  public static ImageHelper migrateUrl(final String url,
                                       String modifiedBy,
                                       long modifiedTsMs,
                                       FileImageMigrate.ImageUser usedBy,
                                       String id,
                                       boolean checkForCrop) {

    Account     account = Account.findByLegacyId(modifiedBy);
    ImageHelper ih      = ImageHelper.build(IMG_USER_UPLOAD, account);

    try {
      Instant modifiedTs = Instant.ofEpochMilli(modifiedTsMs);

      //By default assuming we have User Upload
      ih.stopWatch.start();

      ih.fim.setSourceUse(usedBy);
      ih.fim.setSourcePk(id);

      ih.setCheckForCrop(checkForCrop);

      ih.setMigrateUrl(url);

      //Already have an error
      if (ih.isMigrateFileError()) {
        ih.fim.setMigrated(false);
        ih.fim.save();
        return ih;
      }

      //. Checking if image was uploaded or downloaded into image library
      ImageLibrary il = ImageLibrary.findByUrlOrName(ih.migrateUrl, ih.migrateKey);
      if (il != null) { //Downloaded from the web
        ih.setSourceType(ImageHelper.fileTypeFromImgLib(il.getImgsrc()));

        if (url.contains("iceportal.com")) {
          ih.setSourceType(IMG_VENDOR_ICE_PORTAL);
        }

        modifiedTs = Instant.ofEpochMilli(il.getCreatedtimestamp());
        ih.file.setSearchWords(il.getSearchTerm());
        ih.fileImage.setSrcUrl(il.getOrigurl());

        if (ih.lstFileType.getExtension().equals("uki") && il.getImgsrc() != null) {
          ih.lstFileType = LstFileType.findFromFileName(il.getOrigurl());
        }
      }

      ih.setFileCreatedTs(Timestamp.from(modifiedTs));

      //Handle if this is a cropped version
      if (checkForCrop && ih.isMigrateCropped()) {
        ih.buildParentForCropped(ih.fim.getSourceUse(), ih.fim.getSourcePk());
        ih.croppedSourceHelper.setCheckForCrop(false);
        ih.croppedSourceHelper.setMigrateUrl(ih.migrateUncroppedUrl);
        if (ih.croppedSourceHelper.isMigrateDuplicate()) {
          ih.croppedSourceHelper.file = ih.croppedSourceHelper.migrateDuplicateFile;
          ih.croppedSourceHelper.fileImage = ih.croppedSourceHelper.migrateDuplicateFileImage;
          ih.croppedSourceHelper.fim.setFilePk(ih.croppedSourceHelper.migrateDuplicateFile.getPk());
        }
        else {
          ih.croppedSourceHelper.migrateFile();
        }
        ih.croppedSourceHelper.save();
        ih.fileImage.setParent(ih.croppedSourceHelper.file);
        //When this is a cropped file filename needs to contain actual file ID
        ih.migrateFilenameNormalized = Utils.base64encode(ih.file.getPk()) + "_" + ih.migrateFilenameNormalized;
        ih.file.setFilename(ih.migrateFilename);

        //Now that cropped version is saved, we need to check that primary is not a duplicate
        if(Arrays.equals(ih.file.getHash(), ih.croppedSourceHelper.file.getHash()) && !ih.isMigrateDuplicate()) {
          ih.migrateDuplicateFile = ih.croppedSourceHelper.file;
          ih.migrateDuplicateFileImage = ih.croppedSourceHelper.fileImage;
          ih.fim.setDuplicate(true);
        }
      }

      //If duplicate just put existing file here
      if (ih.isMigrateDuplicate()) {
        ih.file = ih.migrateDuplicateFile;
        ih.fileImage = ih.migrateDuplicateFileImage;
        ih.fim.setFilePk(ih.file.getPk());
        ih.fim.save();
        return ih;
      }

      //Regular case
      ih.migrateFile();

      if (ih.isMigrateFileError()) {
        ih.fim.setMigrated(false);
        ih.fim.save();
        return ih;
      }

      ih.fim.setMigrated(true);
      ih.save();

      //After image has been migrated checking if it is attached to the mobilized page
      if (il != null && il.getPageId() != null) {
        ih.pageImage = LibraryPageImage.build(il.getPageId(), ih.fileImage);
        ih.pageImage.save();
      }
      ih.stopWatch.stop();
    }
    catch (Exception e) {
      Log.err("ImageHelper::migrateUrl() : Migration Failed for URL: " + url);
      Log.err("ImageHelper::migrateUrl(): Full report:" + ih.toString());
      Log.err("ImageHelper::migrateUrl(): Exception: ", e);
    }
    return ih;
  }

  /**
   * Sets url and determines all needed parameters for other actions to be performed
   *
   * @param migrateUrl
   * @return
   * @throws Exception if one of many errors occurs
   */
  public ImageHelper setMigrateUrl(String migrateUrl)
      throws Exception {
    migrateFileError = true;

    this.migrateUrl = migrateUrl;
    fim.setPublicUrl(migrateUrl);

    S3Util.S3FileInfo sfi = S3Util.parseS3Url(migrateUrl);
    if(sfi == null) {
      throw new Exception("Not AWS S3 Url: " + migrateUrl);
    }

    migrateBucket = sfi.bucket;
    migrateKey = sfi.key;
    migrateFilename = sfi.filename;
    migrateFilenameNormalized = S3Util.normalizeFilename(migrateFilename);


    lstFileType = LstFileType.findFromFileName(migrateFilenameNormalized);

    file.setFilename(migrateFilename);
    fim.setBucket(migrateBucket);
    fim.setKey(migrateKey);
    fim.setFilename(migrateFilename);
    fim.setFilenameNormalized(migrateFilenameNormalized);

    //Get MD5 hash of the S3 object
    ObjectMetadata om = S3Util.getMetadata(migrateBucket, migrateKey);
    if (om != null) {
      file.setHash(om.getETag());
      migrateDuplicateFile = FileInfo.byHash(om.getETag());
      if (isMigrateDuplicate()) {
        fim.setDuplicate(true);
        migrateDuplicateFileImage = FileImage.find.byId(migrateDuplicateFile.getPk());
      }
    }
    else {
      throw new Exception("ImageHelper: setMigrateUrl(): No corresponding object. Bucket: " + migrateBucket + " Key: " +
                          "" + "" + "" + "" + migrateKey);
    }

    if (checkForCrop) {
      //Checking if uncropped image exists
      String imgBucket = ConfigMgr.getAppParameter(CoreConstants.S3_IMG_CROP_BUCKET);
      if (S3Util.doesFileexist(imgBucket, "orig/" + migrateKey)) {
        migrateUncroppedBucket = imgBucket;
        migrateUncroppedKey = "orig/" + migrateKey;
        migrateUncroppedUrl = "https://" + imgBucket + ".s3.amazonaws.com/orig/" + migrateKey;
        fim.setCropped(true);
      }
    }

    migrateFileError = false;
    return this;
  }


  public static ImageHelper build(FileSrc.FileSrcType src, Account modifiedBy) {
    ImageHelper ih   = new ImageHelper();
    FileSrc     fSrc = fileSrcCache.get(src);
    ih.file = FileInfo.buildFileInfo(modifiedBy, fSrc, 0);
    ih.fileImage = FileImage.build(ih.file, modifiedBy);
    ih.fim = FileImageMigrate.build();
    ih.account = modifiedBy;
    ih.srcType = src;
    ih.pageImage = null;
    return ih;
  }

  public ImageHelper setCheckForCrop(boolean checkForCrop) {
    this.checkForCrop = checkForCrop;
    return this;
  }

  public boolean isMigrateFileError() {
    return migrateFileError;
  }

  /**
   * Constructs parent records
   *
   * @return
   */
  ImageHelper buildParentForCropped(FileImageMigrate.ImageUser usedby, String id) {
    croppedSourceHelper = ImageHelper.build(file.getSource().getName(), account);
    croppedSourceHelper.setFileCreatedTs(file.getCreatedTs());
    croppedSourceHelper.setSourceType(file.getSource().getName());
    croppedSourceHelper.fim.setBucket(migrateUncroppedBucket);
    croppedSourceHelper.fim.setKey(migrateUncroppedKey);
    croppedSourceHelper.fim.setPublicUrl(migrateUncroppedUrl);
    croppedSourceHelper.fim.setSourceUse(usedby);
    croppedSourceHelper.fim.setSourcePk(id);
    return this;
  }


  public ImageHelper setSourceType(FileSrc.FileSrcType src) {
    FileSrc fSrc = fileSrcCache.get(src);
    file.setSource(fSrc);
    if (croppedSourceHelper != null) {
      croppedSourceHelper.setSourceType(src);
    }
    return this;
  }

  public static FileSrc.FileSrcType fileTypeFromImgLib(ImageLibrary.ImgSrc src) {
    switch (src) {
      case UMAPPED:
        return FileSrc.FileSrcType.IMG_VENDOR_UMAPPED;
      case WIKIPEDIA:
        return FileSrc.FileSrcType.IMG_VENDOR_WIKIPEDIA;
      case MOBILIZER:
        return FileSrc.FileSrcType.IMG_MOBILIZER;
      case VIRTUOSO:
        return FileSrc.FileSrcType.IMG_VENDOR_VIRTUOSO;
      case BIG_FIVE:
        return FileSrc.FileSrcType.IMG_VENDOR_BIGFIVE;
      case WETU:
        return FileSrc.FileSrcType.IMG_VENDOR_WETU;
      case WEB:
      default:
        return FileSrc.FileSrcType.IMG_USER_DOWNLOAD;
    }
  }

  public ImageHelper setFileCreatedTs(Timestamp ts) {
    file.setCreatedTs(ts);
    if (croppedSourceHelper != null) {
      croppedSourceHelper.setFileCreatedTs(ts);
    }
    return this;
  }

  public boolean isMigrateDuplicate() {
    return migrateDuplicateFile != null;
  }

  public static String switchExtention(final String filename, final String newExt){
    String fn = FilenameUtils.removeExtension(filename);
    return fn + "." + newExt;
  }

  public static String getImageKey(FileImage image, final String filenameNormalized, Account a){
    FileSrc src = image.getFile().getSource();
    StringBuilder key = new StringBuilder(src.getPrefix()).append('/');

    switch (src.getName()) {
      case IMG_USER_UPLOAD:
        key.append(Account.encodeId(a.getModifiedBy())).append('/');
        break;
    }

    switch (image.getType()) {
      case ORIGINAL: //Original images should never have parents!
        key.append(image.getFile().getEncodedPk()).append('/');
        break;
      default:
        String filenamePrefix = "";
        if(image.getParent() != null) {
          key.append(image.getParent().getEncodedPk());
          filenamePrefix = image.getFile().getEncodedPk() + "_";
        } else {
          key.append(image.getFile().getEncodedPk());
        }
        key.append('/').append(image.getType().getShort().toLowerCase()).append('/'); //Appended img type sub-folder

        if(src.getName() != IMG_USER_UPLOAD) {
          key.append(Account.encodeId(a.getModifiedBy())).append('/');                 //Appended user sub-folder
        }

        //For all modified images we encode
        key.append(filenamePrefix);
        break;
    }

    key.append(filenameNormalized);

    return key.toString();
  }

  public boolean migrateFile()
      throws Exception {

    if (migrateUrl == null) {
      throw new Exception("Can't migrate URL. URL is not set");
    }

    String        bucket = file.getSource().getRealBucketName();

    if (isMigrateCropped()) {
      fileImage.setProcessed(true);
      fileImage.setType(FileImage.ImageType.WIDE_CROP_16x9);
      //Previous cropping logic always
      migrateFilename = switchExtention(migrateFilename, "png");
      migrateFilenameNormalized = switchExtention(migrateFilenameNormalized, "png");
    }
    else {
      fileImage.setProcessed(false);
      fileImage.setType(FileImage.ImageType.ORIGINAL);
    }

    fileImage.setLegacyUrl(migrateUrl);

    if (file.getSource().getName() == IMG_USER_UPLOAD) {
      fileImage.setPublic(false);
    }
    else {
      fileImage.setPublic(true);
    }

    String s3Key = getImageKey(fileImage, migrateFilenameNormalized, account);

    //Copied object should have long shelf (cache) life as it just an image
    ObjectMetadata om = new ObjectMetadata();
    S3Util.setCacheControl(om, TimeUnit.DAYS.toSeconds(365), false, true);
    om.setContentType(lstFileType.getProperMimeType());
    CopyObjectResult res = S3Util.s3CopyObject(migrateBucket, migrateKey, bucket, s3Key, om);
    if (res != null) {
      if (S3Util.setObjectPublic(bucket, s3Key)) {
        file.setFilesize(FileInfo.UNKNOWN_FILESIZE); //Unknown for now
        ObjectMetadata meta = S3Util.getMetadata(bucket, s3Key);
        if (file.updateFromMetadata(meta)) {
          Log.debug("ImageHelper: Updated file information successfully: " + s3Key);
        }
        file.setFilename(migrateFilename)
            .setFilepath(s3Key)
            .setFiletype(lstFileType)
            .setBucket(bucket)
            .setAsPublicUrl();

        Log.info("Moved photo from: " + migrateUrl + " as " + file.getUrl());
      }
      else {
        Log.err("AccountController: Skipping object: " + file.getFilename() + " Can't set public access");
        migrateFileError = true;
        return false;
      }
    }
    else {
      Log.err("ImageHelper: Failed to copy image");
      migrateFileError = true;
      return false;
    }

    migrateFileError = false;
    return true;
  }

  public boolean isMigrateCropped() {
    return migrateUncroppedUrl != null;
  }

  /**
   * Saves all generated objects
   */
  public void save() {
    file.save();
    fileImage.save();
    fim.save();
  }

  public String getErrors() {
    return errors.toString();
  }

  public FileInfo getFile() {
    return file;
  }

  public FileImage getFileImage() {
    return fileImage;
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();

    sb.append("Migration for:\t" + fim.getSourceUse().name())
      .append(System.lineSeparator())
      .append("Image source:\t" + file.getSource().getName())
      .append(System.lineSeparator())
      .append("URL:\t" + migrateUrl)
      .append(System.lineSeparator())
      .append("Bucket:\t")
      .append(migrateBucket)
      .append(System.lineSeparator())
      .append("Key:\t")
      .append(migrateKey)
      .append(System.lineSeparator())
      .append("Filename\t")
      .append(migrateFilename)
      .append(System.lineSeparator())
      .append("Normalized\t")
      .append(migrateFilenameNormalized)
      .append(System.lineSeparator());

    if (isMigrateCropped()) {
      sb.append("IsCropped:\t").append(migrateUncroppedUrl).append(System.lineSeparator());
    }
    else {
      sb.append("IsCropped:\tFALSE\n");
    }

    if (isMigrateDuplicate()) {
      sb.append("IsDuplicate:\t").append(migrateDuplicateFile.getUrl()).append(System.lineSeparator());
    }
    else {
      sb.append("IsDuplicate:\tFALSE\n");
    }

    if (isMigrateFileError()) {
      sb.append("!!!! ERROR WHILE MIGRATING !!!!").append(System.lineSeparator());
    }

    if (errors.length() > 0) {
      sb.append("Error Log:").append(errors).append(System.lineSeparator());
    }

    sb.append("File Processing Time: ").append(stopWatch.getTime()).append("ms").append(System.lineSeparator());

    return sb.toString();
  }

  /********************************************************************************************************************
   * FUNCTIONS BELOW ARE NOT CURRENTLY BEING USED ANYWHERE                                                            *
   ********************************************************************************************************************/

  /**
   * Extracts filename from legacy URLs
   *
   * @param url
   * @param normalize
   * @return
   */
  public static String extractFilenameFromUrl(final String url, boolean normalize) {
    String result = url;

    int domain = result.lastIndexOf(S3_DOMAIN);
    if (domain == -1) {
      return null;
    }

    //Removing all parameters
    int params = url.lastIndexOf('?');
    if (params != -1) {
      result = url.substring(0, params);
    }

    result = result.substring(domain + S3_DOMAIN.length());

    Matcher m = umappedFile.matcher(result);
    if (m.find()) {
      result = m.group(2);
    }
    else {
      Log.err("ImageHelper: extractFilenameFromUrl(): Pattern didn't match for string: " + result + " from url:" + url);

      int lastSlash = result.lastIndexOf('/');
      if (lastSlash != -1) {
        result = result.substring(lastSlash + 1);
      }
      int lastUnderscore = result.lastIndexOf('_');
      if (lastUnderscore != -1) {
        result = result.substring(lastUnderscore + 1);
      }

      Log.err("ImageHelper: extractFilenameFromUrl(): alternative logic result:" + result);
    }

    return normalize ? S3Util.normalizeFilename(result) : result;
  }

  public long getMigrationTime() {
    return migrationTime;
  }

  public FileInfo getMigrateDuplicateFile() {
    return migrateDuplicateFile;
  }

  public FileImage getMigrateDuplicateFileImage() {
    return migrateDuplicateFileImage;
  }

  public String getMigrateUncroppedBucket() {
    return migrateUncroppedBucket;
  }

  public String getMigrateUncroppedKey() {
    return migrateUncroppedKey;
  }

  public String getMigrateUrl() {
    return migrateUrl;
  }

  public String getMigrateBucket() {
    return migrateBucket;
  }

  public String getMigrateKey() {
    return migrateKey;
  }

  public String getMigrateHash() {
    return migrateHash;
  }

  public String getMigrateFilename() {
    return migrateFilename;
  }

  public String getMigrateFilenameNormalized() {
    return migrateFilenameNormalized;
  }

  public String getEnvSuffix() {
    return envSuffix;
  }

  public ImageHelper setEnvSuffix(String envSuffix) {
    this.envSuffix = envSuffix;
    return this;
  }

  private FileSrc srcFromType(FileSrc.FileSrcType type) {
    return fileSrcCache.get(type);
  }

  public String getMigrateUncroppedUrl() {
    return migrateUncroppedUrl;
  }

  public FileImageMigrate getFim() {
    return fim;
  }
}
