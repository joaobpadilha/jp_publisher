package com.mapped.publisher.persistence;

import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.drew.lang.StringUtil;
import com.umapped.external.virtuoso.hotel.SpecialAmenities;
import com.umapped.external.virtuoso.hotel.Amenity;
import com.umapped.external.virtuoso.hotel.HotelInfo;
import com.umapped.external.virtuoso.hotel.Root;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.S3Util;
import com.umapped.api.schema.types.Address;
import com.umapped.api.schema.types.Feature;
import models.publisher.FeedSrc;
import models.publisher.PoiSrcFeed;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;

import javax.xml.bind.*;
import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.*;

/**
 * Created by surge on 2015-02-02.
 */
public class FeedHelperVirtuosoHotels
    extends FeedHelperPOI {

  /**
   * 5 character long metaphone - a magic number that seems to work ok.
   * If this passes through too many false positives, increase number to 6 and worst case 7
   */
  private final static int METAPHONE_LEN = 5;
  private final static PolicyFactory policy = new HtmlPolicyBuilder().toFactory();


  @Override public List<FeedResource> getResources() {
    List<FeedResource> result = new ArrayList<>();

    String prefix = "virtuoso/Hotel";
    List<S3ObjectSummary> files = S3Util.getObjectList(S3Util.getPDFBucketName(), prefix);
    for(S3ObjectSummary o : files) {
      FeedResource fr = new FeedResource();
      fr.setDate(o.getLastModified());
      fr.setName(o.getKey());
      fr.setOrigin(FeedResource.Origin.AWS_S3);
      fr.setSize(o.getSize());
      result.add(fr);
    }

    return result;
  }

  @Override public Map<PoiFeedUtil.FeedResult, Integer> load(String filePath) {
    Map<PoiFeedUtil.FeedResult, Integer> results = new HashMap<>();
    try {
      S3Object s3Obj = S3Util.getS3File(S3Util.getPDFBucketName(), filePath);
      JAXBContext jaxbContext = JAXBContext.newInstance(Root.class);
      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      Root pois = (Root) jaxbUnmarshaller.unmarshal(s3Obj.getObjectContent());
      results.put(PoiFeedUtil.FeedResult.SUCCESS, 0);
      results.put(PoiFeedUtil.FeedResult.ERROR, 0);

      String folder = FilenameUtils.getBaseName(FilenameUtils.getPathNoEndSeparator(filePath));
      String fileName = FilenameUtils.getName(filePath);

      for (HotelInfo hi : pois.getHotel().getHotelInfo()) {
        PoiImportRS pirs = new PoiImportRS();
        pirs.state = PoiImportRS.ImportState.UNPROCESSED;
        pirs.srcId = FeedSourcesInfo.byName("Virtuoso Hotels");
        pirs.poiId = null;
        pirs.typeId = PoiTypeInfo.Instance().byName("Accommodations").getId();
        pirs.timestamp = System.currentTimeMillis();
        pirs.srcReference = hi.getMasterEntityId();
        pirs.srcFilename = fileName;
        if (hi.getCompanyName() != null) {
          pirs.name = hi.getCompanyName().replace("\r", "").trim();
        }
        if (hi.getCountry() != null) {
          pirs.countryName = hi.getCountry().replace("\r", "");
        }
        if (hi.getCountry() != null) {
          CountriesInfo.Country c = CountriesInfo.Instance().searchByName(hi.getCountry().replace("\r", ""));
          if (c == null) {
            Log.err("Failed to find matching country: " + hi.getCountry() + " !!!");
            pirs.countryCode = "UNK";
            results.put(PoiFeedUtil.FeedResult.ERROR, results.get(PoiFeedUtil.FeedResult.ERROR) + 1);
          }
          else {
            pirs.countryCode = c.getAlpha3();
            results.put(PoiFeedUtil.FeedResult.SUCCESS, results.get(PoiFeedUtil.FeedResult.SUCCESS) + 1);
          }
        } else {
          pirs.countryCode = "UNK";
          results.put(PoiFeedUtil.FeedResult.ERROR, results.get(PoiFeedUtil.FeedResult.ERROR) + 1);
        }
        pirs.phone = null;
        if (hi.getState() != null) {
          pirs.region = hi.getState().replace("\r", "");
        }
        if (hi.getPostalCode() != null) {
          pirs.postalCode = hi.getPostalCode().replace("\r", "");
        }
        if (hi.getCity() != null) {
          pirs.city = hi.getCity().replace("\r", "");
        }
        if (hi.getAddress() != null) {
          pirs.address = hi.getAddress().replace("\r", "");
        }


        JAXBContext jaxbHotelInfoContext = JAXBContext.newInstance(HotelInfo.class);
        Marshaller jaxbMarshaller = jaxbHotelInfoContext.createMarshaller();

        // output pretty printed
        //jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        StringWriter raw = new StringWriter();
        jaxbMarshaller.marshal(hi, raw);
        pirs.raw = raw.toString();
        pirs.hash = DigestUtils.md5Hex(pirs.raw);
        PoiImportMgr.save(pirs);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return results;
  }

  @Override public PoiImportRS.ImportState associate(PoiImportRS importRS) {
    List<Pair<PoiRS, Integer>> scores = getRankedPois(importRS, false);

    if (scores.size() == 0 || scores.get(0).getRight() <= 0) {
      Log.debug("Found no matches for:" + importRS.name + " from: " + importRS.countryCode);
      importRS.state = PoiImportRS.ImportState.MISMATCH;
      importRS.poiId = null;
    }
    else {
      importRS.state = (scores.get(0).getRight() >= 40) ?
                       PoiImportRS.ImportState.MATCH :
                       PoiImportRS.ImportState.UNSURE;
      importRS.poiId = scores.get(0).getLeft().getId();
    }

    PoiImportMgr.update(importRS);
    return importRS.state;
  }

  @Override public PoiSrcFeed.FeedSyncResult synchronize(PoiSrcFeed psf) {
    JAXBContext jaxbContext = null;
    PoiSrcFeed.FeedSyncResult result = PoiSrcFeed.FeedSyncResult.FAIL;
    try {
      jaxbContext = JAXBContext.newInstance(HotelInfo.class);
      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      StringReader sr = new StringReader(psf.getRaw());
      HotelInfo hotelInfo = (HotelInfo) jaxbUnmarshaller.unmarshal(sr);

      FeedSrc feedSrc = FeedSrc.find.byId(psf.getFeedId().srcId);

      List<PoiRS> poiRecs = PoiMgr.findAllById(psf.getFeedId().poiId);

      PoiRS rsToUpdate = null;
      for (PoiRS p : poiRecs) {
        if (p.getCmpyId() == feedSrc.getCmpyId() &&
            p.consortiumId == feedSrc.getConsortiumId() &&
            p.getSrcId() == feedSrc.getSrcId()) {
          rsToUpdate = p;
        }
      }

      if (rsToUpdate == null) { //Creating a new POI record
        rsToUpdate = PoiRS.buildRecord(feedSrc.getConsortiumId(),
                                       feedSrc.getCmpyId(),
                                       feedSrc.getTypeId(),
                                       feedSrc.getSrcId(),
                                       "system");
        updatePoi(rsToUpdate, hotelInfo, feedSrc);
        rsToUpdate.setId(psf.getFeedId().poiId);
        PoiMgr.save(rsToUpdate);
        result = PoiSrcFeed.FeedSyncResult.NEW;
      }
      else { //updating existing one
        updatePoi(rsToUpdate, hotelInfo, feedSrc);
        PoiMgr.update(rsToUpdate);
        result = PoiSrcFeed.FeedSyncResult.UPDATED;
      }
      psf.setSyncTs(System.currentTimeMillis());
      psf.setSynced(true);
      psf.update();

    }
    catch (JAXBException e) {
      e.printStackTrace();
    }
    catch (Exception e) {
      e.printStackTrace();
      Log.err("Unexpected Error - not handling gracefully:", e.getMessage());
    }
    return result;
  }

  @Override public PoiSrcFeed.FeedSyncResult clearAmenities(PoiSrcFeed psf) {
    PoiSrcFeed.FeedSyncResult result = PoiSrcFeed.FeedSyncResult.FAIL;
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(HotelInfo.class);
      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      StringReader sr = new StringReader(psf.getRaw());
      HotelInfo hotelInfo = (HotelInfo) jaxbUnmarshaller.unmarshal(sr);

      if(hotelInfo.getSpecialAmenities() != null) {
        hotelInfo.setSpecialAmenities(null);

        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        // output pretty printed
        //jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        StringWriter raw = new StringWriter();
        jaxbMarshaller.marshal(hotelInfo, raw);
        psf.setSynced(false);
        psf.setRaw(raw.toString());
        psf.setHash(DatatypeConverter.parseHexBinary(DigestUtils.md5Hex(raw.toString())));
        psf.update();
        result = PoiSrcFeed.FeedSyncResult.UPDATED;
      } else {
        result = PoiSrcFeed.FeedSyncResult.UNCHANGED;
      }
    }
    catch (JAXBException e) {
      Log.err("JAXB failed to (un)marshal Virtuoso Hotel Feed XML data in poi_src_feed table id:" + psf.getFeedId());
      e.printStackTrace();
    }
    catch (Exception e) {
      Log.err("Unexpected Error - not handling gracefully:", e);
      e.printStackTrace();
    }
    return result;
  }


  @Override public List<Pair<PoiRS, Integer>> getRankedPois(PoiImportRS importRS, boolean ignoreCountry) {

    String cleanName = importRS.name.replaceAll("[\\*]*\\s*(?i)non-renewal\\s*-\\s*", "");

    List<PoiRS> pois;

    if (ignoreCountry) {
      pois = PoiMgr.findForMerge(cleanName, null, importRS.typeId);
    }
    else {
      pois = PoiMgr.findForMerge(cleanName, importRS.countryCode, importRS.typeId);
    }

    List<Pair<PoiRS, Integer>> scores = new ArrayList<>();

    for (PoiRS prs : pois) {
      int currScore = rankPoi(cleanName, importRS, prs);
      scores.add(new ImmutablePair<>(prs, currScore));
    }

    Collections.sort(scores, new Comparator<Pair<PoiRS, Integer>>() {
      @Override public int compare(Pair<PoiRS, Integer> o1, Pair<PoiRS, Integer> o2) {
        return o2.getRight() - o1.getRight();
      }
    });
    return scores;
  }

  private PoiRS updatePoi(PoiRS poi, HotelInfo hotelInfo, FeedSrc src) {
    poi.setSrcReference(hotelInfo.getMasterEntityId());
    if (hotelInfo.getCompanyName() != null) {
      poi.setName(hotelInfo.getCompanyName().replace("\r", "").trim());
    }
    Address address = new Address();
    if (hotelInfo.getAddress() != null) {
      address.setStreetAddress(hotelInfo.getAddress().replace("\r", ""));
    }
    if (hotelInfo.getCity() != null) {
      address.setLocality(hotelInfo.getCity().replace("\r", ""));
    }
    if (hotelInfo.getState() != null) {
      address.setRegion(hotelInfo.getState().replace("\r", ""));
    }
    if (hotelInfo.getCountry() != null) {
      address.setCountryCode(CountriesInfo.Instance()
                                          .searchByName(hotelInfo.getCountry().replace("\r", ""))
                                          .getAlpha3());
    } else {
      address.setCountryCode("UNK");
    }
    if (hotelInfo.getPostalCode() != null) {
      address.setPostalCode(hotelInfo.getPostalCode().replace("\r", ""));
    }
    poi.setMainAddress(address);
    poi.clearFeatures(); //Clearing all previously listed features for this feed
    if (hotelInfo.getSpecialAmenities() != null && hotelInfo.getSpecialAmenities().getAmenity() != null && hotelInfo.getSpecialAmenities().getAmenity().getAmenityDescription() != null ) {
      Amenity amenity = hotelInfo.getSpecialAmenities().getAmenity();
      if (amenity instanceof Amenity) {
        Feature f = new Feature();
        f.setName("");
        //amenities feed contain HTML tags - we are going to strip them out but keep carriage returns
        String desc = hotelInfo.getSpecialAmenities()
                               .getAmenity()
                               .getAmenityDescription()
                               .replace("\r", "")
                               .replace("</p", "\n</p")
                               .replace("</li", "\n</li")
                               .replace("<br>", " ");
        f.setDesc(StringUtils.strip(policy.sanitize(desc)));
        f.addTag(src.getName());
        poi.addFeature(f);
      }
    }
    return poi;
  }


  public static void main (String[] args) {
    try {
      File f = new File ("/volumes/data2/downloads/HotelExport_20160212_084405_V4_0.xml");
      JAXBContext jaxbContext = JAXBContext.newInstance(Root.class);
      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      Root pois = (Root) jaxbUnmarshaller.unmarshal(f);
      PolicyFactory policy = new HtmlPolicyBuilder().toFactory();

      for (HotelInfo hotelInfo: pois.getHotel().getHotelInfo()) {
        String s = hotelInfo.getSpecialAmenities().getAmenity().getAmenityDescription().replace("\r", "").replace("</p","\n</p").replace("</li","\n</li");
        System.out.println("\n------------\n" + s);
        System.out.println("\n!!!!!!!!!!!!\n" + policy.sanitize(s));



      }

    } catch (Exception e) {

    }

  }
}
