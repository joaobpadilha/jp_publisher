package com.mapped.publisher.view;

import com.mapped.publisher.view.billing.BillingInfoView;
import com.mapped.publisher.view.billing.CreditCardInfoView;
import com.mapped.publisher.view.billing.SubscriptionPlanView;
import com.umapped.persistence.accountprop.EligiblePlans;
import models.publisher.BillingPlan;
import models.publisher.Consortium;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-18
 * Time: 6:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserView
    extends BaseView
    implements Serializable {
  public Long accountId;
  public String userId;
  public String firstName;
  public String lastName;
  public String name;
  public String email;
  public String facebook;
  public String twitter;
  public String phone;
  public String mobile;
  public String web;
  public String fax;
  public String profilePhoto;
  public String profilePhotoUrl;

  public String lastLoginTimestamp;
  public String lastLoginPrint;
  public int status;

  public int numPendingTrips;
  public int numPublishedTrips;

  public List<UserCmpyLinkView> cmpyLinks;

  public String term;
  public String cmpyName;
  public String cmpyId;

  public boolean pageMode;
  public boolean isExistingUser;
  public boolean isOwner;
  public String  cmpyIdOwner;


  public boolean isCmpyAdmin;
  public int userType;
  public int intUserType;
  public int cmpyLinkType;

  public boolean canSendWelcomeEmail;

  public List<String> groups;

  public String createdby;
  public String createdon;

  public boolean isViaCollab;

  public BillingInfoView billing;

  public HashMap<String, String> cmpyApis;

  public List<ApiUserLinkView> apiUserLinks;
  public List<ApiCmpyTokenView> apiCmpyTokens;

  public CreditCardInfoView creditCardInfo;

  public boolean stillOnTrial;
  public List<BillingPlan> monthlyBasicPlanList;
  public List<BillingPlan> monthlyProPlanList;
  public List<BillingPlan> yearlyBasicPlanList;
  public List<BillingPlan> yearlyProPlanList;

  public EligiblePlans eligiblePlans;

  public SubscriptionPlanView monthlyBasicPlan;
  public SubscriptionPlanView monthlyProPlan;
  public SubscriptionPlanView yearlyBasicPlan;
  public SubscriptionPlanView yearlyProPlan;

  public String umConsortiumPricingUrl;


  public Consortium getEligiblePlansCons() {
    if (monthlyBasicPlan != null && monthlyBasicPlan.consortium != null) {
      return monthlyBasicPlan.consortium;
    } else if (yearlyBasicPlan != null && yearlyBasicPlan.consortium != null) {
      return monthlyBasicPlan.consortium;
    } else if (monthlyProPlan != null && monthlyProPlan.consortium != null) {
      return monthlyProPlan.consortium;
    } else if (yearlyProPlan != null && yearlyProPlan.consortium != null) {
      return yearlyProPlan.consortium;
    } else {
      return null;
    }
  }

  public void setUmConsortiumPricingUrl () {
    Consortium cons = getEligiblePlansCons();
    if (cons != null) {
      switch (cons.getConsId()) {
        case 0:
          umConsortiumPricingUrl = "https://umapped.com/pricing/";
          break;
        case 2:
          umConsortiumPricingUrl = "https://umapped.com/virtuoso/pricing/";
          break;
        case 3:
          umConsortiumPricingUrl = "https://umapped.com/ensemble/pricing/";
          break;
        case 5:
          umConsortiumPricingUrl = "https://umapped.com/travelleaders/pricing/";
          break;
        default:
          umConsortiumPricingUrl = null;
          break;
      }
    } else {
      umConsortiumPricingUrl = "https://umapped.com/pricing/";
    }
  }



  public String extension;
}
