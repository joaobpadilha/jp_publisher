package com.mapped.publisher.view;

import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.cruise.UmCruiseReservation;
import com.umapped.persistence.reservation.cruise.UmCruiseStop;
import com.umapped.persistence.reservation.cruise.UmCruiseTraveler;
import com.umapped.persistence.reservation.utils.UmReservationUtils;
import models.publisher.TripDetail;

import java.io.Serializable;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

/**
 * Created by ryan on 17/03/17.
 */
public class UmCruiseReservationView extends TripBookingDetailView
        implements Serializable{

    public UmCruiseReservationView() {
        this.detailsTypeId = ReservationType.CRUISE;
    }

    public List<CruiseStop> cruiseStops;
    public String numNights;

    public static class CruiseStop
            implements Serializable {
        public UmCruiseStop stop;
        public String portCode;
        public String rank;
        public String tripDetailId;

        //temp fields
        public String date;
        public String itinerary;
        public String arriveTime;
        public Long arriveTimeMs;
        public String arriveTimePrint;
        public String departTime;
        public Long departTimeMs;
        public String departTimePrint;
        public String datePrint;

        public String getTripDetailId() {
            return this.tripDetailId;
        }
        public String getDate() {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            return this.date;
            //return stop.arrivalTime.getTime().format(formatter);
        }
        public Long getArriveDateMs() {
            ZoneId zoneId = ZoneId.systemDefault();
            return this.arriveTimeMs;
            //return stop.arrivalTime.getTime().atZone(zoneId).toInstant().toEpochMilli();
        }
        public String getArriveDatePrint() {
            ZoneId zoneId = ZoneId.systemDefault();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm a");
            //String arriveTime = stop.arrivalTime.getTime().format(formatter);
            return this.arriveTimePrint;
            //return Utils.formatDatePrint(stop.arrivalTime.getTime().atZone(zoneId).toInstant().toEpochMilli()) + " - " + arriveTime;
        }
        public String getArriveTime() {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm a");
            return this.arriveTime;
            //return stop.arrivalTime.getTime().format(formatter);
        }
        public String getItineraryName() {
            return this.itinerary;
        }
        public String getPortCode() {
            return this.portCode;
        }
        public void setPortCode(String portCode) {
            this.portCode = portCode;
        }
        public Long getDepartDateMs() {
            ZoneId zoneId = ZoneId.systemDefault();
            return this.departTimeMs;
            //return stop.departTime.getTime().atZone(zoneId).toInstant().toEpochMilli();
        }
        public String getDepartDatePrint() {
            ZoneId zoneId = ZoneId.systemDefault();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm a");
            //String departTime = stop.departTime.getTime().format(formatter);
            return this.departTimePrint;
            //return Utils.formatDatePrint(stop.departTime.getTime().atZone(zoneId).toInstant().toEpochMilli()) + " - " + departTime;
        }
        public String getDepartTime() {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm a");
            return this.departTime;
            //return stop.departTime.getTime().format(formatter);
        }
        public String getRank() {
            return this.rank;
        }
    }

    public void buildViewInfo(String tripId, TripDetail tripDetail, boolean printDateFormat, SecurityMgr.AccessLevel tripAccessLevel) {
        super.buildViewInfo(tripId, tripDetail, printDateFormat, tripAccessLevel);
        UmCruiseReservation cruise = cast(tripDetail.getReservation(), UmCruiseReservation.class);
        if (cruise != null) {
            this.cruise = new TripBookingDetailView.Cruise();
            UmCruiseTraveler traveler = cruise.getCruiseTraveler();
            if (traveler != null) {
                this.cruise.bedding       = traveler.getBedding();
                this.cruise.cabinNumber   = traveler.getCabinNumber();
                this.cruise.category      = traveler.getCategory();
                this.cruise.deck          = traveler.getDeckNumber();
                this.cruise.meal          = traveler.getDiningPlan();
            }

            this.cruise.cmpyName      = cruise.getCruise().getProvider().name;

            if (poiMain != null) {
                this.cruise.name        = poiMain.getName();

            } else {
                this.cruise.name        = (tripDetail.getName() != null)?tripDetail.getName():cruise.getCruise().name;
            }
            this.name                 = this.cruise.name;
            this.providerName         = this.cruise.name;

            if (this.poiStart != null) {
                this.cruise.portDepart = this.poiStart.getName();
            } else {
                this.cruise.portDepart = tripDetail.getLocStartName();
            }

            if (this.poiFinish != null) {
                this.cruise.portArrive = this.poiFinish.getName();
            } else {
                this.cruise.portArrive = tripDetail.getLocFinishName();
            }
        }
        else {
            Log.err("Database records missing no cruise details for trip " + tripId);
            //return null;
        }
    }

    public void buildViewInfo(TripDetail tripDetail, boolean printDateFormat, SecurityMgr.AccessLevel tripAccessLevel) {
        super.buildViewInfo(tripDetail, printDateFormat, tripAccessLevel);
    }

    public static UmCruiseReservationView parseCruise (TripBookingView bookingView,  TripBookingDetailView view) {
        return parseCruise(bookingView, view, bookingView);


    }

    public static UmCruiseReservationView parseCruise (TripBookingView bookingView,  TripBookingDetailView view, TripBookingView allBookings) {
        UmCruiseReservationView cruiseView = new UmCruiseReservationView();
        cruiseView.cruiseStops = new ArrayList<>();

        if (bookingView != null && bookingView.activities != null && view != null) {
            //extract all cruise stops that belong to this cruise
            for (TripBookingDetailView activity : allBookings.activities) {
                if (activity.detailsTypeId == ReservationType.CRUISE_STOP && activity.startDateMs >= view.startDateMs && activity.startDateMs <= view.endDateMs) {
                    CruiseStop stop = new CruiseStop();
                    stop.tripDetailId = activity.detailsId;
                    stop.arriveTime = activity.startDatePrint;
                    if (activity.startTime != null && activity.startTime.trim().length() > 0) {
                        stop.arriveTime += " - " + activity.startTime;
                        stop.arriveTimePrint = activity.startTime;
                    }
                    stop.date = activity.startDate;
                    stop.arriveTimeMs = activity.startDateMs;
                    stop.datePrint = activity.startDatePrint;
                    if (activity.rank > 0) {
                        stop.rank = String.valueOf(activity.rank);
                    } else {
                        stop.rank = " -- ";
                    }
                    stop.itinerary = activity.providerName;
                    stop.departTime = activity.endDatePrint;
                    stop.departTimeMs = activity.endDateMs;
                    if (activity.endTime != null && activity.endTime.trim().length() > 0) {
                        stop.departTime += " - " + activity.endTime;
                        stop.departTimePrint = activity.endTime;

                    }
                    stop.portCode = activity.arrivalAirportCode;
                    cruiseView.cruiseStops.add(stop);
                }
            }
        }

        if (view != null) {
            if(view.startDateMs > 1 && view.endDateMs > view.startDateMs) {
                double d = view.endDateMs - view.startDateMs;
                d = d / (24 * 60 * 60 * 1000);
                int days = (int)Math.ceil(d);
                if (days > 1 )
                    cruiseView.numNights = String.valueOf(days) + " Days";
                else
                    cruiseView.numNights = "1 Day";
            }
        }
        return cruiseView;
    }

    public static UmCruiseReservationView parseCruiseCB (TripBookingView bookingView,  TripBookingDetailView view, TripBookingView allBookings) {
        UmCruiseReservationView cruiseView = new UmCruiseReservationView();
        cruiseView.cruiseStops = new ArrayList<>();

        if (bookingView != null && bookingView.activities != null && view != null) {
            //extract all cruise stops that belong to this cruise
            String departCity = "";
            String departCode = "";
            if (view.poiStart != null) {
                departCity = view.poiStart.getName();
                departCode = view.poiStart.getCode();
            }
            long departTimestamp = view.startDateMs;
            long arriveTimestamp = view.endDateMs;
            CruiseStop finalStop = null;

            for (TripBookingDetailView activity : allBookings.activities) {
                if (activity.detailsTypeId == ReservationType.CRUISE_STOP  && activity.startDateMs >= view.startDateMs && activity.startDateMs <= view.endDateMs && !activity.name.toLowerCase().contains("day") && !activity.name.toLowerCase().contains("sea")) {
                    CruiseStop stop = new CruiseStop();
                    stop.tripDetailId = activity.detailsId;
                    if (activity.rank > 0) {
                        stop.rank = String.valueOf(activity.rank);
                    } else {
                        stop.rank = " -- ";
                    }
                    stop.departTimeMs = departTimestamp;
                    stop.arriveTimeMs = activity.startDateMs;
                    departTimestamp = activity.endDateMs;
                    stop.itinerary = activity.providerName;
                    stop.portCode = activity.arrivalAirportCode;
                    //stop.arrivePort = activity.arrivalAirportCode;
                    //stop.arriveCity = activity.name;

                    //stop.departCity = departCity;
                    //stop.departPort = departCode;

                    departCity = activity.name;
                    departCode = activity.arrivalAirportCode;

                    cruiseView.cruiseStops.add(stop);
                    finalStop = stop;

                }
            }

            if (finalStop != null) {

            }
        }

        if (view != null) {
            if(view.startDateMs > 1 && view.endDateMs > view.startDateMs) {
                double d = view.endDateMs - view.startDateMs;
                d = d / (24 * 60 * 60 * 1000);
                int days = (int)Math.ceil(d);
                if (days > 1 )
                    cruiseView.numNights = String.valueOf(days) + " Days";
                else
                    cruiseView.numNights = "1 Day";
            }
        }

        return cruiseView;


    }

    public String getRecordLocator() {
        return this.recordLocator;
    }

    public String getConsortiumAmenities() {
        return this.consortiumAmenities;
    }

    public String getConsortiumName() {
        return this.consortiumName;
    }

    public String getCruiseName() {
        if(this.reservation != null && UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruise() != null) {
            return UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruise().name;
        } else {
            return "";
        }
    }

    public String getCruiseCmpyName() {
        if(this.reservation != null && UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruise().getProvider() != null) {
            return UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruise().getProvider().name;
        } else {
            return "";
        }
    }

    public String getCategory() {
        if(this.reservation != null && UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruiseTraveler() != null) {
            return UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruiseTraveler().getCategory();
        } else {
            return "";
        }
    }

    public String getCabinNumber() {
        if(this.reservation != null && UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruiseTraveler() != null) {
            return UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruiseTraveler().getCabinNumber();
        } else {
            return "";
        }
    }

    public String getDeck() {
        if(this.reservation != null && UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruiseTraveler() != null) {
            return UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruiseTraveler().getDeckNumber();
        } else {
            return "";
        }
    }

    public String getBedding() {
        if(this.reservation != null && UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruiseTraveler() != null) {
            return UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruiseTraveler().getBedding();
        } else {
            return "";
        }
    }

    public String getDining() {
        if(this.reservation != null && UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruiseTraveler() != null) {
            return UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruiseTraveler().getDiningPlan();
        } else {
            return "";
        }
    }

    public String getDepartPort() {
        if(this.reservation != null && UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruise() != null && UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruise().departPort != null) {
            return UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruise().departPort.name;
        } else {
            if (this.getPoiStart() != null) {
                return this.getPoiStart().getName();
            } else if (this.getLocStartName() != null && !this.getLocStartName().isEmpty()) {
                return this.getLocStartName();
            }
        }
        return "";
    }

    public String getArrivePort() {
        if(this.reservation != null && UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruise() != null && UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruise().arrivalPort != null) {
            return UmReservationUtils.cast(this.reservation, UmCruiseReservation.class).getCruise().arrivalPort.name;
        } else {
            if (this.getPoiFinish() != null) {
                return this.getPoiFinish().getName();
            } else if (this.getLocFinishName() != null && !this.getLocFinishName().isEmpty()) {
                return this.getLocFinishName();
            }
        }
        return "";
    }

    public String getNumNights() {
        if(this.getStartDateMilli() > 1 && this.getEndDateMilli() > this.getStartDateMilli()) {
            double d = this.getEndDateMilli() - this.getStartDateMilli();
            d = d / (24 * 60 * 60 * 1000);
            int days = (int)Math.ceil(d);
            if (days > 1 )
                return String.valueOf(days) + " Days";
            else
                return "1 Day";
        } else {
            return "";
        }
    }

    public List<CruiseStop> getCruiseStops(TripBookingView tbv) {
        cruiseStops = new ArrayList<>();

        if (tbv != null && tbv.activities != null) {
            //extract all cruise stops that belong to this cruise
            for (TripBookingDetailView activity : tbv.activities) {
                if (activity.detailsTypeId == ReservationType.CRUISE_STOP && activity.startDateMs >= this.getStartDateMilli() && activity.startDateMs <= this.getEndDateMilli()) {
                    CruiseStop stop = new CruiseStop();
                    stop.tripDetailId = activity.detailsId;
                    stop.arriveTime = activity.startDatePrint;
                    if (activity.startTime != null && activity.startTime.trim().length() > 0) {
                        stop.arriveTime += " - " + activity.startTime;
                        stop.arriveTimePrint = activity.startTime;
                    }
                    stop.date = activity.startDate;
                    stop.arriveTimeMs = activity.startDateMs;
                    stop.datePrint = activity.startDatePrint;
                    if (activity.rank > 0) {
                        stop.rank = String.valueOf(activity.rank);
                    } else {
                        stop.rank = " -- ";
                    }
                    stop.itinerary = activity.providerName;
                    stop.departTime = activity.endDatePrint;
                    stop.departTimeMs = activity.endDateMs;
                    if (activity.endTime != null && activity.endTime.trim().length() > 0) {
                        stop.departTime += " - " + activity.endTime;
                        stop.departTimePrint = activity.endTime;

                    }
                    stop.portCode = activity.arrivalAirportCode;
                    cruiseStops.add(stop);
                }
            }
        }
        return cruiseStops;
    }
}
