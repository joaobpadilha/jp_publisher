package com.mapped.publisher.view;

import models.publisher.Company;
import models.publisher.UserCmpyLink;
import models.publisher.UserProfile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2015-12-23.
 */
public class UserProfileView
    extends BaseView {

  public UserProfile            up;
  public String                 fullName;
  public boolean                isOwner;
  public Company                cmpy;
  public UserCmpyLink.LinkType  cmpyLinkType;
  public String                 cmpyParseEmailAddr;
  public String                 userParseEmailAddr; //Personal Email email address for future use
  public String                 memberSince;
  public String                 lastLoginOn;
  public List<ApiCmpyTokenView> apiCmpyTokens;
  public List<ApiUserLinkView>  apiUserLinks;
  public String                 extension;

  public boolean                isLoggedinClassicVacations = false;

  public UserProfileView() {
    apiCmpyTokens = new ArrayList<>();
    apiUserLinks = new ArrayList<>();
  }


}
