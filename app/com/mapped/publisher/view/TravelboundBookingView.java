package com.mapped.publisher.view;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wei on 2017-05-13.
 */
public class TravelboundBookingView
    extends BaseView
    implements Serializable {

  public String tripId;
  public String tripName;
  public String tripStartDatePrint;
  public String tripEndDatePrint;
  public List<Integer> clientIds;

  private boolean hasBookings = false;

  public TravelboundBookingView(boolean hasBookings) {
    this.hasBookings = hasBookings;
  }

  public boolean getHasBookings() {
    return hasBookings;
  }
}
