package com.mapped.publisher.view;

import com.mapped.common.CoreConstants;
import com.mapped.publisher.common.Capability;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.parse.extractor.booking.BookingExtractor;
import com.umapped.persistence.notes.StructuredNote;
import models.publisher.TripNote;

import java.io.Serializable;
import java.util.EnumSet;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-14
 * Time: 9:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class BaseView
    implements Serializable {
  public String                  message;
  public SecurityMgr.AccessLevel accessLevel;
  public boolean isUMappedAdmin;
  public boolean isPowerUser;
  public String  loggedInUserId;
  public String  linkId;
  public String  firstName;
  public String  lastName;
  public Boolean isCmpyAdmin;
  public String  title;
  public String  jsUrl;
  public String  cssUrl;
  public String  imgUrl;
  public String  libUrl;
  public String  url;
  public String  scrollToId;
  public String modalUrl;
  public int nextPos;
  public int prevPos;
  public boolean display12Hr = true;
  public List<BookingExtractor.Parsers> parsers;
  EnumSet<Capability> capabilities;




  public String cmpyId;


  //public Lang lang = new Lang(new play.api.i18n.Lang("en", "US"));//new Lang(new play.api.i18n.Lang("pt", "PT"));

  public BaseView(String message) {
    this(); //Default constructor
    this.message = message;
  }

  public BaseView() {
    jsUrl = ConfigMgr.getAppParameter(CoreConstants.JS_URL);
    cssUrl = ConfigMgr.getAppParameter(CoreConstants.CSS_URL);
    imgUrl = ConfigMgr.getAppParameter(CoreConstants.IMG_URL);
    libUrl = ConfigMgr.getAppParameter(CoreConstants.LIB_URL);
    url = ConfigMgr.getAppParameter(CoreConstants.HOST_URL);
    capabilities = EnumSet.noneOf(Capability.class);
  }

  public boolean hasCapability(Capability cap) {
    return capabilities.contains(cap);
  }

  public void addCapabilities(EnumSet<Capability> caps) {
    capabilities.addAll(caps);
  }

  public String getInitials() {
    if (firstName != null && lastName != null) {
      return firstName.substring(0, 1) + lastName.substring(0, 1);
    }
    return "";
  }

  public boolean isProd() {
    return ConfigMgr.isProduction();
  }

  public BaseView buildView(TripNote note, BaseView inView) {
    return null;
  }

}
