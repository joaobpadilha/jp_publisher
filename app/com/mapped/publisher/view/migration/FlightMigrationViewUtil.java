package com.mapped.publisher.view.migration;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.umapped.persistence.reservation.UmTraveler;
import com.umapped.persistence.reservation.flight.UmFlight;
import com.umapped.persistence.reservation.flight.UmFlightReservation;
import com.umapped.persistence.reservation.flight.UmFlightTraveler;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

/**
 * Helper class to facilitate the reservation migration
 * 
 * Before the structure data editor UI is built, the reservation data needs to
 * be converted back
 * to comments field in the view to display
 * 
 * @author wei
 *
 */
public class FlightMigrationViewUtil {
  public String reservationToComment(UmFlightReservation reservation) {
    StringBuilder builder = new StringBuilder();

    if (reservation != null) {
      if (reservation.getNotesPlainText() != null) {
        builder.append(reservation.getNotesPlainText());
      }

      UmFlight flight = reservation.getFlight();
      
      if (!StringUtils.isEmpty(flight.departureTerminal)) {
        builder.append("Departure Terminal:");
        builder.append(flight.departureTerminal);
        builder.append("\n");
      }
      
      if (!StringUtils.isEmpty(flight.arrivalTerminal)) {
        builder.append("Arrival Terminal:");
        builder.append(flight.arrivalTerminal);
        builder.append("\n");
      }
      
      List<UmTraveler> travelers = reservation.getTravelers();

      if (travelers != null) {
        builder.append("Passengers:\n");
        for (UmTraveler t : travelers) {
          UmFlightTraveler ft = cast(t, UmFlightTraveler.class);

          if (ft != null) {
            buildTraveler(ft, builder);
          }
        }
      }
    }
    return builder.toString();
  }

  private void buildTraveler(UmFlightTraveler t, StringBuilder builder) {
    addField("Name", t.getName(), builder);
    addField("Seat", t.getSeat(), builder);
    addField("Class", t.getSeatClass().seatCategory, builder);
    addField("eTicket", t.getTicketNumber(), builder);
    addField("Frequent Flyer", t.getProgram().membershipNumber, builder);
    addField("Meal", t.getMeal(), builder);
  }

  private void addField(String name, String value, StringBuilder builder) {
    if (!StringUtils.isEmpty(value)) {
      builder.append(name);
      builder.append(": ");
      builder.append(value);
      builder.append("\n");
    }
  }
}
