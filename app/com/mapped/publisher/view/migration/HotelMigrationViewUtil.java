package com.mapped.publisher.view.migration;

import org.apache.commons.lang3.StringUtils;

import com.umapped.persistence.reservation.accommodation.UmAccommodationReservation;
import com.umapped.persistence.reservation.accommodation.UmLodgingBusiness;

public class HotelMigrationViewUtil {
  public String reservationToComment(UmAccommodationReservation reservation) {
    StringBuilder builder = new StringBuilder();

    if (reservation != null) {
      
      UmLodgingBusiness hotel = reservation.getAccommodation();
      String addressLines[] = hotel.getAddress().toStringTwoLines();
      if (StringUtils.isNotEmpty(addressLines[0]) || StringUtils.isNotEmpty(addressLines[1])) {
        builder.append("Address:\n");
        builder.append(addressLines[0]);
        if (StringUtils.isNotEmpty(addressLines[0])) {
          builder.append("\n");
        }
        builder.append(addressLines[1]);
        builder.append("\n");
      }
      
      if (StringUtils.isNotEmpty(hotel.telephone)) {
        builder.append("Phone: ");
        builder.append(hotel.telephone);
        builder.append("\n");
      }
      
      if (StringUtils.isNotEmpty(hotel.faxNumber)) {
        builder.append("Fax:  ");
        builder.append(hotel.faxNumber);
        builder.append("\n");
      }
      
      if (StringUtils.isNotEmpty(hotel.url)) {
        builder.append("Web:  ");
        builder.append(hotel.url);
        builder.append("\n");
      }

      if (builder.length() > 0) {
        // add another newline to separate the address info with regular comments
        builder.append("\n");
      }
      
      if (reservation.getNotesPlainText() != null) {
        builder.append(reservation.getNotesPlainText());
      }
    }
    return builder.toString();
  }
}
