package com.mapped.publisher.view;

import java.io.Serializable;

/**
 * Created by george on 2017-02-17.
 */
public class FileView implements Serializable {
  public String attachId;
  public String attachUrl;
  public String accessLevel;
  public String pageId;
  public String createdBy;
  public String note;
  public String title;
  public Long origFileId;
  public Integer width;
  public String  fileName;
  public String fileType;
}
