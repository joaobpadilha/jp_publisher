package com.mapped.publisher.view;

import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.persistence.CountriesInfo;
import com.mapped.publisher.persistence.template.Attachment;
import com.mapped.publisher.view.BaseView;
import com.umapped.api.schema.types.Address;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.UmReservation;
import com.umapped.persistence.reservation.UmTraveler;
import com.umapped.persistence.reservation.enums.UmRateType;
import com.umapped.persistence.reservation.flight.UmFlight;
import com.umapped.persistence.reservation.flight.UmFlightReservation;
import com.umapped.persistence.reservation.flight.UmFlightTraveler;
import com.umapped.persistence.reservation.utils.UmReservationUtils;
import models.publisher.TripDetail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

  /**
   * Created by ryan on 16/03/17.
   */
  public class UmFlightReservationView extends TripBookingDetailView
    implements Serializable{

    private List<PassengerInfo> passengers;

    public static class PassengerInfo
            implements Serializable {
        public UmFlightTraveler traveler;

        public String getName() {
            return traveler.getName();
        }

        public String getGivenName() {
            return traveler.getGivenName();
        }

        public String getFamilyName() {
            return traveler.getFamilyName();
        }

        public String getSeat() {
            return traveler.getSeat();
        }

        public String getSeatCategory() {
            return traveler.getSeatClass().seatCategory;
        }

        public String getTicketNumber() {
            return traveler.getTicketNumber();
        }

        public String getMembershipNumber() {
            return traveler.getProgram().membershipNumber;
        }

        public String getMeal() {
            return traveler.getMeal();
        }
    }

    public UmFlightReservationView() {
        this.detailsTypeId = ReservationType.FLIGHT;
    }

    public void buildViewInfo(String tripId, TripDetail tripDetail, boolean printDateFormat, SecurityMgr.AccessLevel tripAccessLevel) {
        super.buildViewInfo(tripId, tripDetail, printDateFormat, tripAccessLevel);

        if (poiMain != null && poiMain.data != null) {
            if (poiMain.data.getAdditionalProperties().containsKey(APPConstants.BAGGAGE_URL) ) {
                this.baggageUrl = poiMain.data.getAdditionalProperties().get(APPConstants.BAGGAGE_URL);
            }
            if (poiMain.data.getAdditionalProperties().containsKey(APPConstants.CHECKIN_URL) ) {
                this.checkinUrl = poiMain.data.getAdditionalProperties().get(APPConstants.CHECKIN_URL);
            }
        }

        UmFlightReservation fligtReservation = cast(reservation, UmFlightReservation.class);

        // For the new reservation structure
        if (fligtReservation != null) {
            if (fligtReservation.getFlight() != null) {
                this.flightId = fligtReservation.getFlight().flightNumber;
                this.name = fligtReservation.getFlight().flightNumber; //In case of flights flight ID is the name and poi name is airline
            }
        }

        if (this.poiStart != null) {
            this.departureAirport     = this.poiStart.getName();
            this.departureAirportId   = this.poiStart.getId();
            this.departureAirportCode = this.poiStart.getCode();
            Address address = this.poiStart.getMainAddress();
            if (address != null) {
                this.departCity = address.getLocality();
                String cc = (address.getCountryCode() != null) ? address.getCountryCode() : poiStart.countryCode;
                if (cc != null && CountriesInfo.Instance()
                        .byAlpha3(cc) != null) {
                    this.departCountry = CountriesInfo.Instance()
                            .byAlpha3(cc)
                            .getName(CountriesInfo.LangCode.EN);
                }
            }
        } else {
            this.departureAirport = tripDetail.getLocStartName();
        }

        if (this.poiFinish != null) {
            this.arrivalAirport     = this.poiFinish.getName();
            this.arrivalAirportId   = this.poiFinish.getId();
            this.arrivalAirportCode = this.poiFinish.getCode();
            Address address = this.poiFinish.getMainAddress();
            if (address != null) {
                this.arriveCity = address.getLocality();
                String cc = (address.getCountryCode() != null) ? address.getCountryCode() : poiFinish.countryCode;
                if (cc != null && CountriesInfo.Instance()
                        .byAlpha3(cc) != null) {
                    this.arriveCountry = CountriesInfo.Instance()
                            .byAlpha3(cc)
                            .getName(CountriesInfo.LangCode.EN);
                }
            }
        } else {
            this.arrivalAirport = tripDetail.getLocFinishName();
        }
        if (poiMain != null && poiMain.getName() != null) {
            this.providerName = poiMain.getName();
        }
    }

    public void buildViewInfo(TripDetail tripDetail, boolean printDateFormat, SecurityMgr.AccessLevel tripAccessLevel) {
        super.buildViewInfo(tripDetail, printDateFormat, tripAccessLevel);
    }

    public static List<UmFlightReservationView.PassengerInfo> getPassengerInfo(TripBookingDetailView view) {
        List<UmFlightReservationView.PassengerInfo> passengers = new ArrayList<>();
        UmFlightReservation flight = cast(view.reservation, UmFlightReservation.class);

        if (view != null && flight != null && flight.getTravelers() != null) {

            for (UmTraveler t : flight.getTravelers()) {
                UmFlightTraveler ft = cast(t, UmFlightTraveler.class);
                if (ft != null) {
                    PassengerInfo pi = new PassengerInfo();
                    pi.traveler = ft;
                    passengers.add(pi);
                }
            }
        }
        return passengers;
    }

    public String getDepartTerminal() {
        if(this.reservation != null) {
            return UmReservationUtils.cast(this.reservation, UmFlightReservation.class).getFlight().departureTerminal;
        } else {
            return "";
        }
    }

    public String getArriveTerminal() {
        if(this.reservation != null) {
            return UmReservationUtils.cast(this.reservation, UmFlightReservation.class).getFlight().arrivalTerminal;
        } else {
            return "";
        }
    }

    public String getFlightId() {
        if(this.reservation != null) {
            return UmReservationUtils.cast(this.reservation, UmFlightReservation.class).getFlight().flightNumber;
        } else {
            return "";
        }
    }

    public String getDepartAirport() {
        //return UmReservationUtils.cast(this.reservation, UmFlightReservation.class).getFlight().departureAirport;
        return this.departureAirport;
    }

    // TODO ryan TW_17W14_refactor: fields to be changed
    public Long getDepartAirportId() {
        return this.departureAirportId;
    }

    public String getDepartAirportCode() {
        return this.departureAirportCode;
    }

    public String getArriveAirport() {
        return this.arrivalAirport;
    }

    public Long getArriveAirportId() {
        return this.arrivalAirportId;
    }

    public String getArriveAirportCode() {
        return this.arrivalAirportCode;
    }

    public String getDepartCity() {
        return this.departCity;
    }

    public String getArriveCity() {
        return this.arriveCity;
    }

    public String getDepartCountry() {
        return this.departCountry;
    }

    public String getArriveCountry() {
        return this.arriveCountry;
    }

    public String getCheckinUrl() {
        if(this.reservation != null) {
            String s = UmReservationUtils.cast(this.reservation, UmFlightReservation.class).getCheckinUrl();
            if (s !=null && !s.trim().isEmpty())
             return UmReservationUtils.cast(this.reservation, UmFlightReservation.class).getCheckinUrl();
        }

        if (this.checkinUrl != null && !this.checkinUrl.isEmpty()){
            return this.checkinUrl;
        }
        return null;
    }

    public String getBaggageUrl() {
        if(this.reservation != null) {
            String s = UmReservationUtils.cast(this.reservation, UmFlightReservation.class).getBaggageUrl();
            if (s != null && !s.trim().isEmpty())
                return UmReservationUtils.cast(this.reservation, UmFlightReservation.class).getBaggageUrl();
        }

        if (this.baggageUrl != null && !this.baggageUrl.isEmpty()) {
            return this.baggageUrl;
        }
        return null;
    }

    public List<PassengerInfo> getPassengers() {
        this.passengers = getPassengerInfo(this);
        return passengers;
    }
}