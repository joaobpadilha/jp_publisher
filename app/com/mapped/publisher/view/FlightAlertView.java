package com.mapped.publisher.view;

import models.publisher.FlightAlertEvent;

import java.util.List;

/**
 * Created by twong on 2015-01-22.
 */
public class FlightAlertView extends BaseView {
  public String title;
  public String flightId;
  public String airlineCode;
  public String airlineName;
  public String departAirportCode;
  public String departAirportName;
  public String arriveAirportCode;
  public String arriveAirportName;
  public String origDepartTime;
  public String departGate;
  public String departTerminal;
  public String arriveGate;
  public String arriveTerminal;
  public String checkinUrl;
  public String tripName;
  public String tripUrl;
  public boolean isAgent;

  public FlightAlertEvent.FlightAlertType alertType;

  public String getTitle() {
    if (alertType == FlightAlertEvent.FlightAlertType.CANCELLED) {
      return "FLIGHT CANCELLED";
    }
    else if (alertType == FlightAlertEvent.FlightAlertType.DEPARTURE_DELAY) {
      return "FLIGHT DELAYED";
    }else if (alertType == FlightAlertEvent.FlightAlertType.GATE_ADJUSTMENT) {
      return "GATE CHANGE";
    } else if (alertType == FlightAlertEvent.FlightAlertType.PRE_DEPARTURE_STATUS) {
      return "FLIGHT REMINDER";
    }
    else
      return "FLIGHT UPDATE";
  }


  public List<FlightAlertEventView> events;
  public class FlightAlertEventView {
    public FlightAlertEvent.FlightAlertType alertType;
    public String timestamp;
    public String departTime;
    public String arriveTime;


    public String getAlertDesc() {
      if (alertType == FlightAlertEvent.FlightAlertType.PRE_DEPARTURE_STATUS) {
        return "Flight Status";
      }
      else if (alertType == FlightAlertEvent.FlightAlertType.CANCELLED) {
        return "FLIGHT CANCELLED";
      }
      else if (alertType == FlightAlertEvent.FlightAlertType.DEPARTURE_DELAY) {
        return "FLIGHT DELAYED";
      }else if (alertType == FlightAlertEvent.FlightAlertType.GATE_ADJUSTMENT) {
        return "Flight Update - Gate change";
      }
      else
        return "Flight Update";
    }

  }

}


