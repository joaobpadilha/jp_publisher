package com.mapped.publisher.view.admin;

import com.mapped.publisher.view.BaseView;
import models.publisher.SysCapability;

import java.util.List;

/**
 * Created by surge on 2015-12-15.
 */
public class CapabilityView
    extends BaseView {
  public SysCapability editable;

  public List<SysCapability> capabilities;
}
