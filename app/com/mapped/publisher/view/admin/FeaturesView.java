package com.mapped.publisher.view.admin;

import com.mapped.publisher.view.BaseView;
import models.publisher.SysCapability;
import models.publisher.SysFeature;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2015-12-15.
 */
public class FeaturesView
    extends BaseView {

  public FeatureInfo editable;
  public List<SysCapability> availableCaps;

  public List<FeatureInfo> features;
  public static class FeatureInfo {
    public SysFeature feature;
    public List<SysCapability> capabilities;
  }

  public FeaturesView() {
    features = new ArrayList<>();
  }
}
