package com.mapped.publisher.view;

import java.util.List;

/**
 * Created by george on 2017-11-13.
 */
public class AfarPoiView extends BaseView {
  public String title;
  public Integer poiId;
  public String placeName;
  public String cityName;
  public Integer cityId;
  public String countryName;
  public Integer countryId;
  public Integer destId;
  public List<String> categories;
  public String description;
  public String address;
  public String locationPhone;
  public Double longitude;
  public Double latitude;
  public List<ImageView> images;
}
