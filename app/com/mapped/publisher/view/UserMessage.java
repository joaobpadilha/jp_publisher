package com.mapped.publisher.view;

import com.umapped.api.schema.types.ReturnCode;
import play.data.Form;
import play.mvc.Result;

import static play.mvc.Results.ok;

/**
 * Created by surge on 2016-01-11.
 */
public class UserMessage
    extends BaseView {

  public UserMessage(){
    super();
  }

  public UserMessage(String msg){
    super(msg);
  }

  /**
   * Use this message when form contains errors
   * @param  form - form that caused the problem
   * @return
   */
  public static Result formErrors(Form form){
    if(form == null) {
      return UserMessage.message(ReturnCode.HTTP_REQ_WRONG_DATA);
    } else {
      //TODO: Go over errors and make error more intelligent
      return UserMessage.message(ReturnCode.HTTP_REQ_WRONG_DATA);
    }
  }

  public static Result message(ReturnCode rc, String... customMessages) {
    StringBuilder sb = new StringBuilder(rc.msg());

    if(customMessages.length > 0) {
      for(String cm : customMessages) {
        sb.append(" - ");
        sb.append(cm);
      }
    }

    switch (rc.level()) {
      case FATAL:
      case CRITICAL:
      case SEVERE:
      case ERROR:
        return error(sb.toString());
      case WARNING:
        return warning(sb.toString());
      case INFO:
      case DEBUG:
      default:
        return message(sb.toString());
    }
  }

  public static Result message(String msg) {
    UserMessage um = new UserMessage(msg);
    return ok(views.html.common.message.render(um));
  }

  public static Result error(String msg) {
    return message(msg);
  }

  public static Result warning(String msg) {
    return message(msg);
  }

  public static Result success(String msg) {
    return message(msg);
  }

}
