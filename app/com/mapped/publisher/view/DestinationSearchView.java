package com.mapped.publisher.view;

import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-07
 * Time: 1:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class DestinationSearchView
    extends BaseView {

  /**
   * OLD VALUES
   *   public final static int TAB_ADDITIONAL_DOC = 1;
   *   public final static int TAB_UPLOADED_FILE = 2;
   *   public final static int TAB_TRIP_DOCUMENT = 3;
   */
  public enum Tab {
    ATTACHMENTS,
    CUSTOM_DOC;

    /**
     * Class to allow specifying parameter from enum in Play Framework.
     */
    public static class Bound
        implements QueryStringBindable<Bound>, PathBindable<Bound> {
      private Tab value;

      /**
       * This empty constructor must be there, implicit one is not recognized
       * when other constructors are present.
       */
      public Bound() {
        this.value = null;
      }

      public Bound(Tab tab) {
        this.value = tab;
      }

      @Override
      public Bound bind(String key, String txt) {
        this.value = Tab.valueOf(txt);
        return this;
      }

      @Override
      public Optional<Bound> bind(String key, Map<String, String[]> params) {
        String[] arr = params.get(key);
        if (arr != null && arr.length > 0) {
          this.value = Tab.valueOf(arr[0]);
          return Optional.of(this);
        }
        else {
          return Optional.empty();
        }
      }

      @Override
      public String unbind(String key) {
        return this.value.name();
      }

      @Override
      public String javascriptUnbind() {
        return this.value.name();
      }

      public Tab value() {
        return this.value;
      }
    }

  }

  public String cmpyId;
  public List<String> destinationTypes;

  public HashMap<String, String> tripAgencyList;
  public List<GenericTypeView> destinationTypeList;
  public String tripId;
  public boolean   tripNote;
  public String tripNoteDate;
  public boolean displayMap;
  public String keyword;

  public List<DestinationView> destinationList;
  public List<DestinationGuideView> destinationGuideList;
  public String destGuideId;
  public String destId;
  public DestinationView tripDestinationView;
  public TripInfoView tripInfo;
  public List<AttachmentView> tripAttachments;

  public Tab selectedTab;
  public boolean isAdminOfACompany;
  public boolean searchTitleOnly = true;

  public String tripDetailId;
}
