package com.mapped.publisher.view;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-14
 * Time: 1:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class TripAgentView {
    public String pk;

    public String tripId;
    public String groupId;
    public String tripName;
    public String agentName;
    public String agencyName;
    public String agentEmail;
    public String agentComments;
    public WebItineraryStatsView webItineraryStats;


}
