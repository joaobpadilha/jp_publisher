package com.mapped.publisher.view;

import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.notes.insurance.InsuranceNote;
import com.umapped.persistence.notes.insurance.InsuranceTraveler;
import com.umapped.persistence.notes.tableNote.TableNote;
import com.umapped.persistence.reservation.UmTraveler;
import models.publisher.TripNote;

import java.io.Serializable;
import java.util.ArrayList;

import java.io.Serializable;

import static com.umapped.persistence.notes.utils.StructuredNoteUtils.cast;

/**
 * Created by george on 2017-05-09.
 */
public class TableNoteView extends NoteBaseView
    implements Serializable {

  public String noteName;
  public String intro;
  public String startDate;
  public String startTime;
  public String tag;
  public String linkedTripDetailsId;

  //Overriden method from SuperClass BaseView.
  //Used to create view object to be rendered by template, for respective note type
  @Override
  public TableNoteView buildView(TripNote note, BaseView inView) {
    TableNote tableNote = cast(note.getNote(), TableNote.class);
    TableNoteView view = (TableNoteView) inView;

    if (view.scrollToId == null) {
      view.scrollToId = String.valueOf(note.getNoteId());
    }
    view.noteId = note.getNoteId();
    if (tableNote != null) {
      view.noteName = tableNote.name;
      if (note.getNoteTimestamp() != null && note.getNoteTimestamp() > 0L) {
        view.startTime = Utils.getTimeString(note.getNoteTimestamp());
        view.startDate = Utils.formatDateControlYYYY(note.getNoteTimestamp());
      }
      view.intro = note.getIntro();
      view.tag = note.getTag();
      view.linkedTripDetailsId = note.getTripDetailId();

    }
    return view;
  }
}
