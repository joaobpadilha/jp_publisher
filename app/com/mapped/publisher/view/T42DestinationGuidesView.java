package com.mapped.publisher.view;

import com.mapped.publisher.utils.Log;
import models.publisher.T42Destination;
import models.publisher.T42Guide;
import models.publisher.T42GuideIndex;
import models.publisher.T42Hotel;

import java.util.*;

/**
 * Created by surge on 2015-12-01.
 */
public class T42DestinationGuidesView
    extends BaseView {
  public T42Destination       dest;
  public Map<String, TabData> tabs;
  /**
   * Data for hotels for this destination
   */
  public TabData              tabHotels;

  public T42DestinationGuidesView() {
    tabs = new LinkedHashMap<>();
    tabHotels = null;
  }

  public void addGuides(List<T42Guide> guides) {
    for (T42Guide g: guides) {
      addGuide(g);
    }

    for(String tid: tabs.keySet()) {
      TabData t = tabs.get(tid);
      flatten(t);
    }
    Log.debug("Tabs processed");
  }

  public void addHotels(List<T42Hotel> hotels) {
    if(tabHotels == null) {
      tabHotels = new TabData("Hotels");
    }

    for(T42Hotel h: hotels) {
      addHotel(h);
    }

    //Tabs go in order of insert sort
    for(String tn: tabHotels.tabs.keySet()) {
      TabData td = tabHotels.tabs.get(tn);
      td.isActive = "active";
      break;
    }

    Log.debug(hotels.size() + " Hotels added");
  }

  public void addHotel(T42Hotel hotel) {
    Integer rating = hotel.getStarRating();
    if(rating == null) {
      rating = 0;
    }

    TabData parentTab = tabHotels.tabs.get(rating.toString());
    if(parentTab == null) {
      parentTab = new TabData(rating.toString());
      parentTab.rating = rating;
      tabHotels.tabs.put(rating.toString(), parentTab);
    }

    if (parentTab.hotels == null) {
      parentTab.hotels = new ArrayList<>();
    }
    parentTab.hotels.add(hotel);
  }

  public void flatten(TabData td){
    Iterator it = td.tabs.entrySet().iterator();
    boolean isActive = true;
    while(it.hasNext()) {
      Map.Entry pair = (Map.Entry)it.next();
      TabData currTd = (TabData) pair.getValue();
      currTd.isActive = isActive?"active":"";
      isActive = false;
      for(TabData t: currTd.tabs.values()) {
        collapseChildren(t);
      }
    }
  }

  public List<T42Guide> collapseChildren(TabData td) {
    for(TabData t: td.tabs.values()) {
      td.guides.addAll(collapseChildren(t));
    }
    return td.guides;
  }

  public void preRenderPrepare(TabData td){
    Iterator it = td.tabs.entrySet().iterator();
    while(it.hasNext()) {
      Map.Entry pair = (Map.Entry)it.next();
      String tid = (String)pair.getKey();
      TabData t = (TabData) pair.getValue();
      if(t.tabs.size() == 0) {
        td.guides.addAll(t.guides);
        it.remove();
      } else {
        preRenderPrepare(t);
      }
    }
  }

  public void addGuide(T42Guide guide) {
    TabData td;

    /*
     * Pre-processing special case of hotel descriptions
     */
    if(guide.getClassId().getClassId().equals("#DG-HOTEL-HOTELOVW#")) {
      T42GuideIndex newClassRef = T42GuideIndex.findByKey("#DG-OVER#");
      guide.setRefClassId(newClassRef);
    }

    switch (guide.getLevelNbr().intValue()) {
      case 1:
        td = new TabData(guide);
        if(tabs.size() == 0) {
          td.isActive = "active";
        }
        tabs.put(guide.getClassId().getIdName(), td);
        break;
      case 2:
        td = tabs.get(guide.getRefClassId().getIdName());
        if(td == null) {
          Log.err("Out of order tab :" + guide.getId().getGeoPlaceKey() + " sequence: " + guide.getId().getSequenceNbr());
        } else {
          td.addGuide(guide);
        }
        break;
      case 3:
        td = tabs.get(guide.getTopClassId().getIdName());
        if(td == null) {
          Log.err("Out of order tab :" + guide.getId().getGeoPlaceKey() + " sequence: " + guide.getId().getSequenceNbr());
        } else {
          td.addGuide(guide);
        }
        break;
    }

  }

  public static class TabData {
    public String               id;
    public String               name;
    /**
     * How many "stars" (alternative to the name used for Hotels)
     */
    public Integer              rating;
    public T42Guide             root;
    public Integer              tabLevel;
    public Map<String, TabData> tabs;
    public List<T42Guide>       guides;
    public List<T42Hotel>       hotels;
    public String               isActive;

    /**
     * Used for Hotels tabs
     */
    public TabData(String name) {
      this.name = name;
      isActive = "";
      tabs = new LinkedHashMap<>();
    }

    /**
     * Used for Guide Tabs
     * @param g
     */
    public TabData(T42Guide g) {
      guides = new ArrayList<>();
      isActive = "";
      tabs = new LinkedHashMap<>();
      guides = new ArrayList<>();
      root = g;
      name = g.getClassId().getClassName();
      id = g.getClassId().getIdName();
      tabLevel = root.getLevelNbr();

      if(g.getContent().length() > 0) {
        guides.add(g);
      }
      //Log.debug("New TabData created " + name + " ID: " + id );
    }

    public void addGuide(T42Guide guide) {
      //Log.debug("Adding Guide: L#" + guide.getLevelNbr() + " Seq:" + guide.getId().getSequenceNbr());
      //1. if on the same level
      if(guide.getLevelNbr().equals(tabLevel)) {
        guides.add(guide);
        return;
      }

      //2. One level below - create a tab
      if(guide.getLevelNbr().intValue()  == tabLevel + 1) {
        TabData td = tabs.get(guide.getClassId().getIdName());
        if(td == null) {
          td = new TabData(guide);
          tabs.put(guide.getClassId().getIdName(), td);
        } else {
          td.addGuide(guide);
        }
      }
      //3. If below 2nd level then just pass to next level tab
      else {
        TabData td = tabs.get(guide.getRefClassId().getIdName());
        if(td == null) {
          guides.add(guide); //For now let's see if this happens
        } else {
          td.addGuide(guide);
        }
      }
    }
  }
}

