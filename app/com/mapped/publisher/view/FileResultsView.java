package com.mapped.publisher.view;

import models.publisher.ImageLibrary;

import java.util.HashMap;
import java.util.List;

/**
 * Created by george on 2017-02-17.
 */
public class FileResultsView extends BaseView {
  public String cmpyId;
  public String keyword;

  public String tripId;
  public String templateId;
  public Long tripNoteId;

  public String docId;
  public String pageId;
  public String vendorId;

  public int multipleFiles;

  public String inSearchOrigin;

  public ImageLibrary.ImgSrc searchType;
  public List<FileView> umappedResults;

  public HashMap<String, List<FileView>> searchResults;
}
