package com.mapped.publisher.view;

import java.util.List;
import java.util.Map;

/**
 * Created by george on 2017-11-12.
 */
public class AfarInfoView extends BaseView {
    public String cityName;
    public Integer cityId;
    public String countryName;
    public String regionName;
    public Integer countryId;
    public Integer destId;
    public String overview;
    public String heroImageUrl;
    public String photoCredit;
    public String tripId;
    public boolean isCountry;
    public List<AfarListView> afarLists;
    public Map<Integer, AfarListView> selectedAfarLists;
}
