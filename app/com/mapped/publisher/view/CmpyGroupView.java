package com.mapped.publisher.view;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-18
 * Time: 6:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class CmpyGroupView {
    public String id;
    public String cmpyId;
    public String name;
    public String comments;
    public String tag;

    public List<UserView> members;

}
