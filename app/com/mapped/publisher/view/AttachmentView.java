package com.mapped.publisher.view;

import com.fasterxml.jackson.annotation.JsonInclude;
import models.publisher.FileImage;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-01
 * Time: 11:31 AM
 * To change this template use File | Settings | File Templates.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AttachmentView
    implements Serializable {
  public String    id;
  public Long      attachId;
  public String    name;
  public String    attachName;
  public String    attachType;
  public String    attachUrl;
  public FileImage image; //For use where url is not enough and additional data is needed
  public String    tag;
  public int       rank;
  public int       height;
  public int       width;
  public String    comments;
  public String    youTubeCover;
  public String    lastUpdateTimestampPrint;
  public String    createdById;
}
