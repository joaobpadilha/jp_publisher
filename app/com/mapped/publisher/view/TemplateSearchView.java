package com.mapped.publisher.view;

import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-07-25
 * Time: 11:27 AM
 * To change this template use File | Settings | File Templates.
 */
public class TemplateSearchView extends BaseView{
    public String searchTerm ;
    public HashMap<String, String > tripAgencyList;
    public String cmpyId;

    public String tripId;


    public List<TemplateView> myTemplates;
    public List<TemplateView> publicTemplates;
}
