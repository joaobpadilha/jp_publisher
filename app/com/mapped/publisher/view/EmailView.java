package com.mapped.publisher.view;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-01-23
 * Time: 10:36 AM
 * To change this template use File | Settings | File Templates.
 */
public class EmailView extends BaseView{
  public TripInfoView tripInfo;
  public String tripUrl;
  public String note;
  public List<TripPassengerView> passengers;
  public boolean updated;
  public BusinessCardView businessCard;
  public boolean published = true;
  public boolean displayMobileApps = true;
  public Long travelerId;
}
