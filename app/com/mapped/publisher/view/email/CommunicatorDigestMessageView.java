package com.mapped.publisher.view.email;

import com.mapped.publisher.persistence.communicator.Communicator;
import com.mapped.publisher.view.BaseView;
import com.umapped.persistence.digest.CommunicatorMsg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by twong on 08/04/16.
 */
public class CommunicatorDigestMessageView extends BaseView {
    public Map<String, TripDigestMessage> allMessages = new HashMap<>(); //key per trip
    public List<String> sortedTripId = new ArrayList<>();
    public String toEmail;
    public String firstName;
    public String lastName;
    public String fullName;
    public String accountId;
    public Long uid;

    public class TripDigestMessage {
        public Map<String, List<CommunicatorMsg>> bookingMsgs = new HashMap<>();
        public List<String> sortedBookingIds = new ArrayList<>();
        public String tripName;
        public String groupId;
    }

}
