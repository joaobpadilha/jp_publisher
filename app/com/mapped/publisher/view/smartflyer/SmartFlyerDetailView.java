package com.mapped.publisher.view.smartflyer;

import java.util.List;

/**
 * Created by twong on 2014-09-18.
 */
public class SmartFlyerDetailView {
  public static class PassengerInfo  {
    public String name;
    public String seat;
    public String fClass;
    public String status;
    public String frequentflyer = "N/A";
    public String eticket;
    public String meal = "";
  };

  public static class CruiseStop  {
    public String day;
    public String date;
    public String itinerary;
    public String arriveTime;
    public String departTime;

  };

  public List<PassengerInfo> passengers;
  public List<CruiseStop> cruiseStops;

  public String departTerminal ="";
  public String arriveTerminal ="";

  public String amenities ="";
  public String comments = "";


  public String providerAddr1;
  public String providerAddr2;
  public String providerPhone;
  public String city;
  public String country;
  public String zip;
  public String state;
  public String numNights;
  public String providerFax;
  public String providerWeb;

  public String duration = "";
  public String aircraft = "";
  public String distance = "";
  public String stops = "";



}
