package com.mapped.publisher.view;


import com.umapped.persistence.enums.ReservationType;

/**
 * Created by twong on 15-08-19.
 */
public class BookingDetailView extends BaseView {
  public String          pk;
  public ReservationType bookingType;
  public String          details;

}
