package com.mapped.publisher.view;

/**
 * Created by Serguei Moutovkin on 2014-05-02.
 */
public class ExpPDFParserView
    extends BaseView {
  public String userId;

  public String fileName;
  public boolean isUploaded;
  public String pdfText;
}
