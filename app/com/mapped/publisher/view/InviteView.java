package com.mapped.publisher.view;

import java.util.List;

/**
 * Created by twong on 2014-05-25.
 */
public class InviteView
    extends BaseView {
  public String               term;
  public List<InviteInfoView> invites;
  public List<UserView>       users;

  public InviteInfoView selectedInvite;
}
