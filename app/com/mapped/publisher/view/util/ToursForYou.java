package com.mapped.publisher.view.util;

import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.DestinationGuideView;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.view.DestinationView;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.util.StringUtil;

import java.util.*;


/**
 * Created by twong on 2014-08-21.
 */
public class ToursForYou {
  public static enum LABELS  {TO, NUM_PASSENGERS, INTRO, DATE, FOOTER, TAG, ADDRESS, MAIN_BRAND, COLLABORATOR, EMERGENCY};
  public static enum LANG {EN, PT, ES};

  public static final Map<LABELS, String> translationsEN;
  public static final Map<LABELS, String> translationsPT;
  public static final Map<LABELS, String> translationsES;

  static  {
    Map <LABELS, String> mapEN =new HashMap<LABELS, String>();
    mapEN.put(LABELS.TO, "To:");
    mapEN.put(LABELS.NUM_PASSENGERS, "Number of Passengers:");
    mapEN.put(LABELS.INTRO, "Tours For You is very pleased to present you this <span style='font-weight:700'>tailor-made itinerary </span> considering your special interest in visiting Portugal." +
                            " This personalized brochure that we have designed exclusively" +
                            " for your holidays in Portugal is our best way of telling you that we honor our promise of" +
                            " taking care of each proposal as a truly individual request, " +
                            "according to the specific needs that you have." +
                            " This itinerary and all the included charming services " +
                            "represent whet we feel and believe to be the best way of showing you <span " +
                            "style='font-weight:700'> \"A Never Ending Portugal to Discover\". </span>" +
                            " We are certain that our success in achieving it will be just" +
                            " the beginning of the beautiful memories that you'll take with you when returning back " +
                            "home." +
                            " Please remember that your feedback at the conclusion of the " +
                            "services is highly appreciated... Thank you for visiting portugal with Tours For You! We " +
                            "hope that you may enjoy our country as much as we do!");
    mapEN.put(LABELS.DATE, "Date:");
    mapEN.put(LABELS.FOOTER, "- Living is an art, Travelling the inspiration -");
    mapEN.put(LABELS.TAG, "A Never Ending Portugal to Discover");
    mapEN.put(LABELS.ADDRESS, "Address");
    mapEN.put(LABELS.MAIN_BRAND, "This program was designed and operated by");
    mapEN.put(LABELS.COLLABORATOR, "In association with:");
    mapEN.put(LABELS.EMERGENCY, "                   P.S Meanwhile, please keep note of our Office and Emergency Phone " +
                              "Number:<br>" +
                              "                    Office: (+351) 21 390 42 08<br>" +
                              "                    Emergency: (+351) 91 631 29 19");
    translationsEN = Collections.unmodifiableMap(mapEN);

    Map <LABELS, String> mapPT =new HashMap<LABELS, String>();
    mapPT.put(LABELS.TO, "Para:");
    mapPT.put(LABELS.NUM_PASSENGERS, "Número de Passageiros:");
    mapPT.put(LABELS.INTRO, "A Tours For You tem o prazer de lhe apresentar este itinerário personalizado considerando o seu interesse especial em visitar Portugal. Esta brochura que desenhámos exclusivamente para as suas férias em Portugal, são a melhor maneira de lhe dizer que honramos o nosso compromisso de tratar cada proposta como um pedido individual único, totalmente de acordo com as suas necessidades. Este itinerário, e todos os serviços de charme nele incluidos, representam o que nós acreditamos ser a melhor maneira de lhe trazer o charme de Portugal. Estamos certos que, o nosso sucesso em atingir este patamar, será apenas o principio das inesqueciveis recordações que irá levar consigo no seu regresso a casa. E lembre-se que gostariamos muito de ter a sua opinião após a conclusão de todos os serviços... Obrigado por visitar Portugal com a Tours For You! Esperamos que possa apreciar tudo, tanto quanto nós apreciamos!");
    mapPT.put(LABELS.DATE, "Data:");
    mapPT.put(LABELS.FOOTER, "- Living is an art, Travelling the inspiration -");
    mapPT.put(LABELS.TAG, "Um Portugal sem fim para descobrir");
    mapPT.put(LABELS.ADDRESS, "Address");
    mapPT.put(LABELS.MAIN_BRAND, "Este programa foi desenhado e é operado por:");
    mapPT.put(LABELS.COLLABORATOR, "Em associação com a:");
    mapPT.put(LABELS.EMERGENCY, "                  P.S. Entretanto, p.f. tome nota do nosso telefone do escritório e de emergência: " +
                              "<br>" +
                              "                    Escritório: (+351) 21 390 42 08<br>" +
                              "                    Emergência: (+351) 91 631 29 19");
    translationsPT = Collections.unmodifiableMap(mapPT);

    Map <LABELS, String> mapES =new HashMap<LABELS, String>();
    mapES.put(LABELS.TO, "para:");
    mapES.put(LABELS.NUM_PASSENGERS, "Número de Pasajeros:");
    mapES.put(LABELS.INTRO, "Tours For You se complace en presentarle este itinerario personalizado teniendo en cuenta su especial interés en conocer Portugal, Este programa detallado que hemos diseñado  en exclusivo para sus vacaciones en Portugal, es la mejor manera de  decirle que honoramos nuestro compromiso de tratar cada propuesta como una petición singular única, totalmente de acuerdo con sus necesidades. Este itinerario y toda la elegancia de los servicios incluidos, representan lo que creemos ser la mejor forma de mostrarle el encanto de Portugal. ¡Estamos seguros que nuestro éxito en alcanzar este nivel, será sólo el comienzo de recuerdos inolvidables que  llevará con Usted en su regreso a casa! Y ¡se acuerde que nos gustaría mucho de conocer su opinión sobre los servicios al final de su viaje! ¡Muchas Gracias por visitar Portugal con Tours For You! ¡Esperamos que pueda disfrutar de todo tanto como nosotros disfrutamos!\n" + "\n");
    mapES.put(LABELS.DATE, "FECHA:");
    mapES.put(LABELS.FOOTER, "- Living is an art, Travelling the inspiration -");
    mapES.put(LABELS.TAG, "Un Portugal sin fin para descubrir");
    mapES.put(LABELS.ADDRESS, "Address");
    mapES.put(LABELS.MAIN_BRAND, "Este programa foi desenhado e é operado por:");
    mapES.put(LABELS.COLLABORATOR, "Em associação com a:");
    mapES.put(LABELS.EMERGENCY, "                  P.S. Sin embargo por favor tome nota del teléfono de nuestra oficina y de emergencia: " +
                              "<br>" +
                              "                    Oficina: (+351) 21 390 42 08<br>" +
                              "                    Emergencia: (+351) 91 631 29 19");
    translationsES = Collections.unmodifiableMap(mapES);

  }



  /*
    Special conventions for the Tours For You PDF
    use a lin in rtf to figure out where things get separated
   */

  public final static String baseUrl = "https://s3.amazonaws.com/static-umapped-com/public/external/toursforyou/img/";

  public static String getFirstPartDesc(DestinationGuideView v) {
    String s = v.description;

    if (s != null && s.indexOf("<hr />") > 0) {
      return s.substring(0, s.indexOf("<hr />"));
    }

    return s;
  }


  public static String getSecondPartDesc(DestinationGuideView v) {
    String s = v.description;

    if (s != null && s.indexOf("<hr />") > 0) {
      s = s.substring(s.indexOf("<hr />") + 6).replaceAll("<hr />", "");
      if (s.trim().length() > 0) {
        return s;
      }
    }

    return null;
  }

  public static boolean isExperience(DestinationGuideView v) {

    if (v != null && v.description != null) {
      if (v.description.toLowerCase().contains("sobre esta experiência") || v.description.toLowerCase()
                                                                                         .contains(
                                                                                             "about this experience")
          || v.description
              .toLowerCase()
              .contains("sobre esta experiencia") || (v.tag != null && v.tag.toLowerCase().contains("layout1"))) {
        return true;
      }
    }
    else if (v != null && v.recommendationType == APPConstants.RECOMMENDATION_TYPE_FOOD) {
      return true;
    }

    return isHotel(v);

  }

  public static boolean isHotel(DestinationGuideView v) {

    if (v != null && v.description != null) {
      if (v.description.toLowerCase().contains("acerca del hotel") || v.description.toLowerCase()
                                                                                   .contains("about the hotel") || v
              .description
              .toLowerCase()
              .contains("sobre o hotel") || v.description.toLowerCase().contains("sobre este espaço")) {
        return true;
      }
    }

    return false;
  }

  public static String getIconImgTag(DestinationGuideView v) {
    if (v != null && v.tag != null) {
      String[] tokens = v.tag.split(",");
      for (String s : tokens) {
        String url = imgUrl(s);
        if (url.length() > 0) {
          return url;
        }
      }
    }
    return "";
  }

  public static String getPageTitleFromTag(DestinationGuideView v) {
    if (v != null && v.tag != null) {
      String[] tokens = v.tag.split(",");

      if (tokens.length > 0) {
        if (imgUrl(tokens[0]).trim().length() == 0) { //not an icon token
          return tokens[0];
        }
      }
    }
    return "";
  }

  public static String getFamilyNameFromTag(DestinationView v) {
    if (v != null && v.tag != null) {
      String[] tokens = v.tag.split(",");

      if (tokens.length > 0) {
        return tokens[0];
      }
    }
    return "";
  }

  public static String imgUrl(String s) {
    String iconName = null;
    if (s != null) {
      s = s.trim();

      if (s.equalsIgnoreCase("balloon")) {
        iconName = "balloon.png";
      }
      else if (s.equalsIgnoreCase("beach")) {
        iconName = "beach.png";
      } else if (s.equalsIgnoreCase("bike")) {
        iconName = "bike.png";
      } else if (s.equalsIgnoreCase("car")) {
        iconName = "car.png";
      } else if (s.equalsIgnoreCase("castle")) {
        iconName = "castle.png";
      } else if (s.equalsIgnoreCase("castle2")) {
        iconName = "castle2.png";
      } else if (s.equalsIgnoreCase("church")) {
        iconName = "church.png";
      } else if (s.equalsIgnoreCase("cow")) {
        iconName = "cow.png";
      } else if (s.equalsIgnoreCase("cruise")) {
        iconName = "cruise.png";
      } else if (s.equalsIgnoreCase("cruise2")) {
        iconName = "cruise2.png";
      } else if (s.equalsIgnoreCase("dolphin")) {
        iconName = "dolphin.png";
      } else if (s.equalsIgnoreCase("eagle")) {
        iconName = "eagle.png";
      } else if (s.equalsIgnoreCase("farm")) {
        iconName = "farm.png";
      } else if (s.equalsIgnoreCase("fish")) {
        iconName = "fish.png";
      } else if (s.equalsIgnoreCase("flowers")) {
        iconName = "flowers.png";
      } else if (s.equalsIgnoreCase("gourmet")) {
        iconName = "gourmet.png";
      } else if (s.equalsIgnoreCase("hatfemale")) {
        iconName = "hatfemale.png";
      } else if (s.equalsIgnoreCase("hatmale")) {
        iconName = "hatmale.png";
      }  else if (s.equalsIgnoreCase("hawk")) {
        iconName = "hawk.png";
      } else if (s.equalsIgnoreCase("house")) {
        iconName = "house.png";
      } else if (s.equalsIgnoreCase("lighthouse")) {
        iconName = "lighthouse.png";
      } else if (s.equalsIgnoreCase("monument")) {
        iconName = "monument.png";
      } else if (s.equalsIgnoreCase("mountain")) {
        iconName = "mountain.png";
      }  else if (s.equalsIgnoreCase("pattern")) {
        iconName = "pattern.png";
      } else if (s.equalsIgnoreCase("suitcase")) {
        iconName = "suitcase.png";
      } else if (s.equalsIgnoreCase("train")) {
        iconName = "train.png";
      }

      if (iconName != null) {
        return "<img src='"+ baseUrl + iconName + "' class='introIcon'>";
      }

    }

    return "";
  }

  public static String numberOfPassengers (DestinationView v) {
    if (v != null && v.tag != null && v.tag.trim().length() > 0) {

      String[] tokens = v.tag.split(",");
      for (String s : tokens) {
        s = s.trim();
        if (StringUtils.isNumeric(s)) {
          return s;
        }
      }
    }
    return "";
  }

  public static int getLanguage (DestinationView v) {
    int language = LANG.EN.ordinal();
    if (v != null && v.tag != null && v.tag.trim().length() > 0) {
      String[] tokens = v.tag.split(",");
      for (String s : tokens) {
        if (s.trim().toUpperCase().equals("ES")) {
          language = LANG.ES.ordinal();
          break;
        }
        else if (s.trim().toUpperCase().equals("PT")) {
          language = LANG.PT.ordinal();
          break;
        }
      }
    }

    return language;
  }

  public static String translate (LABELS label, DestinationView v) {
    int language = getLanguage(v);
    String translation = null;


      if (language == LANG.PT.ordinal()) {
        translation = translationsPT.get(label);
      }
      else if (language == LANG.ES.ordinal()) {
        translation = translationsES.get(label);
      }
      else {
        translation = translationsEN.get(label);
      }



    if (translation == null)
      return "";
    else
      return translation;
  }

  public static String formatDate (long timestamp, DestinationView v) {
    int language = getLanguage(v);

    if (language == LANG.PT.ordinal()) {
      Locale locale = new Locale("pt", "PT");
      return Utils.formatDatePrint(timestamp, locale);

    } else if (language == LANG.ES.ordinal()) {
      Locale locale = new Locale("es", "ES");
      return Utils.formatDatePrint(timestamp, locale);

    } else {
      return Utils.formatDatePrint(timestamp);
    }


  }

  public static ArrayList<String> getCollaborators (DestinationView v) {
    ArrayList<String> collaborators = new ArrayList<String>();

    if (v != null && v.tag != null && v.tag.trim().length() > 0) {
      String [] tokens = v.tag.split(",");
      for (String t : tokens) {
        t = t.trim();
        if (t.startsWith("**") && t.length() > (t.lastIndexOf("*") + 1)) {
          String s = t.substring(t.lastIndexOf("*") + 1).trim();
          collaborators.add(s);
        }
      }

    }

    return collaborators;


  }
}

