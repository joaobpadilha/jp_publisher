package com.mapped.publisher.view.util;


import com.mapped.publisher.view.DestinationGuideView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by twong on 2015-01-09.
 */
public class MadeForSpain {

  public static List<String> getTags(DestinationGuideView m) {
    List<String> tags = new ArrayList<String>();
    if (m != null && m.tag != null) {
      String[] tokens = m.tag.split(",");
      if (tokens != null) {
        tags.addAll(Arrays.asList(tokens));
      }
    }

    return tags;
  }

  public static String cleanString (String s) {
    return s.replaceAll(" <p> </p> ","");
  }
}