package com.mapped.publisher.view.util;

import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;

import java.util.*;

/**
 * Created by twong on 2014-10-24.
 */
public class Pique {
  public String seats = null;
  public String eticket = null;
  public String fClass = null;
  public EnhancedTripBookingDetailView details;


  public static Pique parseFlight(TripBookingDetailView view) {
    EnhancedTripBookingDetailView v = EnhancedTripBookingDetailView.parseFlight(view);
    Pique p = new Pique();
    if (v != null && v.passengers != null) {
      p.details = v;
      for (EnhancedTripBookingDetailView.PassengerInfo pi : v.passengers) {
        if (pi.seat != null && pi.seat.trim().length() > 0) {
          if (p.seats == null) {
            p.seats = pi.seat;
          }
          else {
            p.seats = p.seats + "/" + pi.seat;
          }
        }

        if (pi.eticket != null && pi.eticket.trim().length() > 0) {
          if (p.eticket == null) {
            p.eticket = pi.name + " " + pi.eticket;
          }
          else {
            p.eticket = p.eticket + " | " + pi.name + " " + pi.eticket;
          }
        }

        if (pi.fClass != null && pi.fClass.trim().length() > 0) {
          if (p.fClass == null) {
            p.fClass = pi.fClass;
          }
          else if (!p.fClass.contains(pi.fClass)) {
            p.fClass = p.fClass + " | " + pi.fClass;
          }
        }
      }
    }


    return p;

  }

  public static void updateChronologicalBookings(DestinationView m) {
    //handle car drop offs
    if (m.chronologicalBookingsPages != null && m.chronologicalPagesBookings != null && m.tripBookingView != null && m.tripBookingView.transfers != null) {
      ArrayList<TripBookingDetailView> cars = new ArrayList<TripBookingDetailView>();

      for (TripBookingDetailView v : m.tripBookingView.transfers) {
        if (v.detailsTypeId == ReservationType.CAR_RENTAL && v.startDate != null && v.endDate != null && !v.startDate
            .equals(v.endDate)) {
          UmTransferReservationView n = new UmTransferReservationView();
          n.name = v.name;
          n.startDate = v.endDate;
          n.startDateMs = v.endDateMs;
          n.startDatePrint = v.endDatePrint;
          n.startDay = v.startDay;
          n.startHour = v.startHour;
          n.startMin = v.startMin;
          n.startMonth = v.startMonth;
          n.startTime = v.startTime;
          n.startYear = v.startYear;
          n.bookingNumber = v.bookingNumber;
          n.locStartName = v.locStartName;
          n.locFinishName = v.locFinishName;
          n.contact = v.contact;
          n.endDate = v.endDate;
          n.endDateMs = v.endDateMs;
          n.endDatePrint = v.endDatePrint;
          n.endDay = v.endDay;
          n.endHour = v.endHour;
          n.endMin = v.endHour;
          n.endMonth = v.endMonth;
          n.endTime = v.endTime;
          n.endYear = v.endYear;
          n.detailsTypeId = v.detailsTypeId;
          n.detailsId = v.detailsId;
          n.providerName = v.providerName;
          cars.add(n);
        }
      }

      boolean sortNeeded = false;

      for (TripBookingDetailView v : cars) {
        String date = Utils.formatDatePrint(v.endDateMs);
        if (m.chronologicalPagesBookings.contains(date)) {
          BookingAndPageView checkoutHotel = new BookingAndPageView();
          checkoutHotel.tripBookingDetailView = v;

          m.chronologicalBookingsPages.get(date).add(0,checkoutHotel);
          //resort the chronological bookings
          // Collections.sort(m.chronologicalBookingsPages.get(date), PiqueComparatorPage);

        }
        else {
          //insert new date
          m.chronologicalPagesBookings.add(date);
          ArrayList<BookingAndPageView> newRentals = new ArrayList<BookingAndPageView>();
          BookingAndPageView checkoutHotel = new BookingAndPageView();
          checkoutHotel.tripBookingDetailView = v;
          newRentals.add(checkoutHotel);
          m.chronologicalBookingsPages.put(date, newRentals);
          sortNeeded = true;
        }
      }

      if (sortNeeded) {
        Collections.sort(m.chronologicalPagesBookings, new Comparator<String>() {
          @Override public int compare(String o1, String o2) {
            long sDate = getMillis(o1);
            long eDate = getMillis(o2);
            if (sDate > eDate) {
              return 1;
            }
            else if (sDate < eDate) {
              return -1;
            }
            else {
              return 0;
            }
          }
        });
      }
    }

    if (m.chronologicalBookingsPages != null && m.chronologicalPagesBookings != null && m.tripBookingView != null && m.tripBookingView.hotels != null) {
      ArrayList<TripBookingDetailView> hotels = new ArrayList<TripBookingDetailView>();

      for (TripBookingDetailView v : m.tripBookingView.hotels) {
        if (v.detailsTypeId == ReservationType.HOTEL && v.startDate != null && v.endDate != null && !v.startDate.equals(
            v.endDate)) {
          UmAccommodationReservationView n = new UmAccommodationReservationView();
          n.name = v.name;
          n.startDate = v.endDate;
          n.startDateMs = v.endDateMs;
          n.startDatePrint = v.endDatePrint;
          n.endDate = v.endDate;
          n.endDateMs = v.endDateMs;
          n.endDatePrint = v.endDatePrint;
          n.detailsTypeId = v.detailsTypeId;
          n.detailsId = v.detailsId;
          n.providerName = v.providerName;
          hotels.add(n);
        }
        else if (v.detailsTypeId == ReservationType.HOTEL && v.startDate != null && v.endDate != null && v.startDate
            .equals(v.endDate)) {
          //same day checkin - we need to insert another record but this time set the start time to the chekckin time so it sorts properly
          UmAccommodationReservationView n = new UmAccommodationReservationView();
          n.name = v.name;
          n.startDate = v.endDate;
          n.startDateMs = v.endDateMs;
          n.startDatePrint = v.endDatePrint;
          n.endDate = v.endDate;
          n.endDateMs = v.endDateMs;
          n.endDatePrint = v.endDatePrint;
          n.detailsTypeId = v.detailsTypeId;
          n.detailsId = v.detailsId;
          n.providerName = v.providerName;
          hotels.add(n);
        }
      }

      boolean sortNeeded = false;

      for (TripBookingDetailView v : hotels) {
        String date = Utils.formatDatePrint(v.endDateMs);
        if (m.chronologicalPagesBookings.contains(date)) {
          BookingAndPageView checkoutHotel = new BookingAndPageView();
          checkoutHotel.tripBookingDetailView = v;

          m.chronologicalBookingsPages.get(date).add(0,checkoutHotel);
          //resort the chronological bookings
         // Collections.sort(m.chronologicalBookingsPages.get(date), PiqueComparatorPage);

        }
        else {
          //insert new date
          m.chronologicalPagesBookings.add(date);
          ArrayList<BookingAndPageView> newRentals = new ArrayList<BookingAndPageView>();
          BookingAndPageView checkoutHotel = new BookingAndPageView();
          checkoutHotel.tripBookingDetailView = v;
          newRentals.add(checkoutHotel);
          m.chronologicalBookingsPages.put(date, newRentals);
          sortNeeded = true;
        }
      }

      if (sortNeeded) {
        Collections.sort(m.chronologicalPagesBookings, new Comparator<String>() {
          @Override public int compare(String o1, String o2) {
            long sDate = getMillis(o1);
            long eDate = getMillis(o2);
            if (sDate > eDate) {
              return 1;
            }
            else if (sDate < eDate) {
              return -1;
            }
            else {
              return 0;
            }
          }
        });
      }
    }


  }

  public static void updateChronologicalBookings(TripBookingView m) {
    //insert new card dropoff
    if (m.chronologicalBookings != null && m.chronologicalDates != null && m.transfers != null) {
      ArrayList<TripBookingDetailView> car = new ArrayList<TripBookingDetailView>();

      for (TripBookingDetailView v : m.transfers) {
        if (v.detailsTypeId == ReservationType.CAR_RENTAL && v.startDate != null && v.endDate != null && !v.startDate
            .equals(v.endDate)) {
          UmTransferReservationView n = new UmTransferReservationView();
          n.name = v.name;
          n.startDate = v.endDate;
          n.startDateMs = v.endDateMs;
          n.startDatePrint = v.endDatePrint;
          n.startDay = v.startDay;
          n.startHour = v.startHour;
          n.startMin = v.startMin;
          n.startMonth = v.startMonth;
          n.startTime = v.startTime;
          n.startYear = v.startYear;
          n.bookingNumber = v.bookingNumber;
          n.locStartName = v.locStartName;
          n.locFinishName = v.locFinishName;
          n.contact = v.contact;
          n.endDate = v.endDate;
          n.endDateMs = v.endDateMs;
          n.endDatePrint = v.endDatePrint;
          n.endDay = v.endDay;
          n.endHour = v.endHour;
          n.endMin = v.endHour;
          n.endMonth = v.endMonth;
          n.endTime = v.endTime;
          n.endYear = v.endYear;
          n.detailsTypeId = v.detailsTypeId;
          n.detailsId = v.detailsId;
          n.providerName = v.providerName;
          car.add(n);
        }
      }

      boolean sortNeeded = false;

      for (TripBookingDetailView v : car) {
        String date = Utils.formatDatePrint(v.endDateMs);
        if (m.chronologicalDates.contains(date)) {
          m.chronologicalBookings.get(date).add(0,v);
          //resort the chronological bookings
          //Collections.sort(m.chronologicalBookings.get(date), PiqueComparator);

        }
        else {
          //insert new date
          m.chronologicalDates.add(date);
          ArrayList<TripBookingDetailView> newRentals = new ArrayList<TripBookingDetailView>();
          newRentals.add(v);
          m.chronologicalBookings.put(date, newRentals);
          sortNeeded = true;
        }
      }

      if (sortNeeded) {
        Collections.sort(m.chronologicalDates, new Comparator<String>() {
          @Override public int compare(String o1, String o2) {
            long sDate = getMillis(o1);
            long eDate = getMillis(o2);
            if (sDate > eDate) {
              return 1;
            }
            else if (sDate < eDate) {
              return -1;
            }
            else {
              return 0;
            }
          }
        });
      }
    }


    if (m.chronologicalBookings != null && m.chronologicalDates != null && m.hotels != null) {
      ArrayList<TripBookingDetailView> hotels = new ArrayList<TripBookingDetailView>();

      for (TripBookingDetailView v : m.hotels) {
        if (v.detailsTypeId == ReservationType.HOTEL && v.startDate != null && v.endDate != null && !v.startDate.equals(
            v.endDate)) {
          UmAccommodationReservationView n = new UmAccommodationReservationView();
          n.name = v.name;
          n.startDate = v.endDate;
          n.startDateMs = v.endDateMs;
          n.startDatePrint = v.endDatePrint;
          n.endDate = v.endDate;
          n.endDateMs = v.endDateMs;
          n.endDatePrint = v.endDatePrint;
          n.detailsTypeId = v.detailsTypeId;
          n.detailsId = v.detailsId;
          n.providerName = v.providerName;
          hotels.add(n);
        }
        else if (v.detailsTypeId == ReservationType.HOTEL && v.startDate != null && v.endDate != null && v.startDate
            .equals(v.endDate)) {
          //same day checkin - we need to insert another record but this time set the start time to the chekckin time so it sorts properly

          UmAccommodationReservationView n = new UmAccommodationReservationView();
          n.name = v.name;
          n.startDate = v.endDate;
          n.startDateMs = v.endDateMs;
          n.startDatePrint = v.endDatePrint;
          n.endDate = v.endDate;
          n.endDateMs = v.endDateMs;
          n.endDatePrint = v.endDatePrint;
          n.detailsTypeId = v.detailsTypeId;
          n.detailsId = v.detailsId;
          n.providerName = v.providerName;
          hotels.add(n);
        }
      }

      boolean sortNeeded = false;

      for (TripBookingDetailView v : hotels) {
        String date = Utils.formatDatePrint(v.endDateMs);
        if (m.chronologicalDates.contains(date)) {
          m.chronologicalBookings.get(date).add(0,v);
          //resort the chronological bookings
          //Collections.sort(m.chronologicalBookings.get(date), PiqueComparator);

        }
        else {
          //insert new date
          m.chronologicalDates.add(date);
          ArrayList<TripBookingDetailView> newRentals = new ArrayList<TripBookingDetailView>();
          newRentals.add(v);
          m.chronologicalBookings.put(date, newRentals);
          sortNeeded = true;
        }
      }

      if (sortNeeded) {
        Collections.sort(m.chronologicalDates, new Comparator<String>() {
          @Override public int compare(String o1, String o2) {
            long sDate = getMillis(o1);
            long eDate = getMillis(o2);
            if (sDate > eDate) {
              return 1;
            }
            else if (sDate < eDate) {
              return -1;
            }
            else {
              return 0;
            }
          }
        });
      }
    }


  }

  public static long getMillis(String s) {
    if (s == null || s.trim().length() == 0)
      return 0;
    try {
      String[] formatters = new String[2];
      formatters[0] = "EEE MMM dd, yyyy";

      Date date = DateUtils.parseDate(s, formatters);
      if (date != null) {
        return date.getTime();
      }
    }
    catch (Exception e) {
      //e.printStackTrace();
    }
    return 0;

  }

  public static String cleanName(String s) {
    if (s != null) {
      return s.replaceAll("_", " ");
    }

    return "";
  }

  public static String getDocCoverForTrip(DestinationView v) {
    if (v.tag != null && v.tag.trim().length() > 0 && StringUtils.isAlphanumeric(v.tag.replaceAll(" ", ""))) {
      StringBuilder sb = new StringBuilder("https://s3.amazonaws.com/static-umapped-com/public/external/pique/img/cover/");
      sb.append(v.tag.toLowerCase().replaceAll(" ", ""));
      sb.append(".png");
      return sb.toString();
    }

    return v.coverUrl;
  }

  public static boolean hasTripCover(DestinationView v) {
    if (v.tag != null && v.tag.trim().length() > 0 && StringUtils.isAlphanumeric(v.tag.replaceAll(" ", ""))) {
      return true;
    }

    return false;
  }

  public static List<TripBookingDetailView> getCarsByDropoff(String date, List<TripBookingDetailView> carRentals) {
    DateTime dateTime = null;
    String[] dateTimeFormat = {"EEE MMM dd, yyyy"};
    List<TripBookingDetailView> cars = new ArrayList<>();

    if (date != null) {
      try {
        DateUtils du = new DateUtils();
        Date d = du.parseDate(date, dateTimeFormat);
        dateTime = new DateTime(d).withTimeAtStartOfDay();

        if (carRentals != null) {
          for (TripBookingDetailView carRental : carRentals) {
            if (carRental.detailsTypeId.equals(ReservationType.CAR_RENTAL) && carRental.endDateMs > 0) {
              DateTime endDateTime = new DateTime(carRental.endDateMs).withTimeAtStartOfDay();
              if (dateTime.isEqual(endDateTime.toInstant())) {
                cars.add(carRental);
              }
            }
          }
        }

      }
      catch (Exception e) {

      }
    }
    return cars;
  }


  public static Comparator<TripBookingDetailView> PiqueComparator = new Comparator<TripBookingDetailView>() {

    @Override public int compare(TripBookingDetailView o1, TripBookingDetailView o2) {
      long date1 = o1.startDateMs;
      long date2 = o2.startDateMs;
      //figure out if we should use start or end date for comparison
      long diff = date1 - date2;
      if (diff < 0) {
        diff *= -1;
        if (diff > (24 * 60 * 60 * 1000)) {
          //greater than a day - try again with end date
          date1 = o1.endDateMs;
          if (date1 > date2) {
            return 1;
          }
          else if (date1 < date2) {
            return -1;
          }
          else {
            return 0;
          }
        }
        if (o1.rank >= 0 && o2.rank >= 0 && o1.startDateMs != o1.endDateMs && o2.startDateMs != o2.endDateMs && o1.rank != o2.rank) {
          if (o1.rank > o2.rank)
            return 1;
          else if (o1.rank < o2.rank)
            return -1;
          else
            return 0;
        }

        return -1;
      }
      else if (diff > 0) {
        if (diff > (24 * 60 * 60 * 1000)) {
          //greater than a day - try again with end date
          date2 = o2.endDateMs;
          if (date1 > date2) {
            return 1;
          }
          else if (date1 < date2) {
            return -1;
          }
          else {
            return 0;
          }
        }
        if (o1.rank >= 0 && o2.rank >= 0 && o1.startDateMs != o1.endDateMs && o2.startDateMs != o2.endDateMs && o1.rank != o2.rank) {
          if (o1.rank > o2.rank)
            return 1;
          else if (o1.rank < o2.rank)
            return -1;
          else
            return 0;
        }

        return 1;
      }
      else {
        if (o1.rank >= 0 && o2.rank >= 0 && o1.startDateMs != o1.endDateMs && o2.startDateMs != o2.endDateMs && o1.rank != o2.rank) {
          if (o1.rank > o2.rank)
            return 1;
          else if (o1.rank < o2.rank)
            return -1;
          else
            return 0;
        }

        return 0;
      }

    }

  };


  public static Comparator<BookingAndPageView> PiqueComparatorPage = new Comparator<BookingAndPageView>() {

    @Override public int compare(BookingAndPageView o1, BookingAndPageView o2) {
      long date1 = 0;
      long date2 = 0;
      if (o1.destinationGuideView != null) {
        date1 = o1.destinationGuideView.timestamp;
      }
      else if (o1.tripBookingDetailView != null) {
        date1 = o1.tripBookingDetailView.startDateMs;
      }
      if (o2.destinationGuideView != null) {
        date2 = o2.destinationGuideView.timestamp;
      }
      else if (o2.tripBookingDetailView != null) {
        date2 = o2.tripBookingDetailView.startDateMs;
      }


      //figure out if we should use start or end date for comparison
      long diff = date1 - date2;
      if (diff < 0) {
        diff *= -1;
        if (diff > (24 * 60 * 60 * 1000)) {
          //greater than a day - try again with end date
          if (o1.tripBookingDetailView != null) {
            date1 = o1.tripBookingDetailView.endDateMs;
            if (date1 > date2) {
              return 1;
            }
            else if (date1 < date2) {
              return -1;
            }
            else {
              return 0;
            }
          }
          else
            return 0;
        }
        if (o1.tripBookingDetailView != null && o2.tripBookingDetailView != null && o1.tripBookingDetailView.rank >= 0 && o2.tripBookingDetailView.rank >= 0
            && o1.tripBookingDetailView.rank != o2.tripBookingDetailView.rank
            && o1.tripBookingDetailView.startDateMs != o1.tripBookingDetailView.endDateMs && o2.tripBookingDetailView.startDateMs != o2.tripBookingDetailView.endDateMs) {
          if (o1.tripBookingDetailView.rank > o2.tripBookingDetailView.rank)
            return 1;
          else if (o1.tripBookingDetailView.rank < o2.tripBookingDetailView.rank)
            return -1;
          else
            return 0;
        }

        return -1;
      }
      else if (diff > 0) {
        if (diff > (24 * 60 * 60 * 1000)) {
          //greater than a day - try again with end date
          if (o1.tripBookingDetailView != null) {
            date2 = o2.tripBookingDetailView.endDateMs;
            if (date1 > date2) {
              return 1;
            }
            else if (date1 < date2) {
              return -1;
            }
            else {
              return 0;
            }
          }
          else
            return 0;
        }
        if (o1.tripBookingDetailView != null && o2.tripBookingDetailView != null && o1.tripBookingDetailView.rank >= 0 && o2.tripBookingDetailView.rank >= 0
            && o1.tripBookingDetailView.rank != o2.tripBookingDetailView.rank
            && o1.tripBookingDetailView.startDateMs != o1.tripBookingDetailView.endDateMs && o2.tripBookingDetailView.startDateMs != o2.tripBookingDetailView.endDateMs) {
          if (o1.tripBookingDetailView.rank > o2.tripBookingDetailView.rank)
            return 1;
          else if (o1.tripBookingDetailView.rank < o2.tripBookingDetailView.rank)
            return -1;
          else
            return 0;
        }
        return 1;
      }
      else {
        if (o1.tripBookingDetailView != null && o2.tripBookingDetailView != null && o1.tripBookingDetailView.rank >= 0 && o2.tripBookingDetailView.rank >= 0
            && o1.tripBookingDetailView.rank != o2.tripBookingDetailView.rank
            && o1.tripBookingDetailView.startDateMs != o1.tripBookingDetailView.endDateMs && o2.tripBookingDetailView.startDateMs != o2.tripBookingDetailView.endDateMs) {
          if (o1.tripBookingDetailView.rank > o2.tripBookingDetailView.rank)
            return 1;
          else if (o1.tripBookingDetailView.rank < o2.tripBookingDetailView.rank)
            return -1;
          else
            return 0;
        }
        return 0;
      }

    }

  };
}
