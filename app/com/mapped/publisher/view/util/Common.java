package com.mapped.publisher.view.util;

import com.mapped.publisher.view.TripBookingDetailView;
import com.mapped.publisher.view.UmAccommodationReservationView;

/**
 * Created by twong on 2014-10-30.
 */
public class Common {
  public static String getTitle (String tripName) {
    int startIndex = 0;
    int endIndex = tripName.length();

    if (tripName.indexOf(" - ") > 0)
      startIndex = tripName.indexOf(" - ") + 3;

    if (tripName.toLowerCase().indexOf(" for ") > 0)
      endIndex = tripName.toLowerCase().indexOf(" for ");

    if (endIndex > startIndex)
      return tripName.substring(startIndex, endIndex).trim();
    else
      return tripName;

  }

  public static String getPassenger (String tripName) {
    if (tripName != null && tripName.toLowerCase().indexOf(" for ") > 0) {
      tripName = tripName.replaceAll(" the ", " ");
      return tripName.substring(tripName.toLowerCase().indexOf(" for ") + 5).trim();

    }
    return "";
  }

  public static String getPageBreak (TripBookingDetailView n) {
    if (n != null && n.tag != null) {
      if (n.tag.toLowerCase().contains("page break before")) {
        return "page-break-before: always;";
      } else if (n.tag.toLowerCase().contains("page break after")) {
        return "page-break-after: always;";

      }
    }
    return "";
  }

  public static String getTitleBeforeDDMMMYYYY (String tripName) {
    if (tripName.matches(".*.[0-9]{1,2}\\s[A-Z,a-z]{3}\\s[0-9]{4}.*")) {
       String[] s = tripName.split("[0-9]{1,2}\\s[a-zA-Z]{3}\\s[0-9]{4}");
       if (s[0] != null && s[0].trim().length() > 0) {
         return s[0].trim();
       }
    }
    return null;
  }

  public static String getTitleAfterDDMMMYYYY (String tripName) {
    if (tripName.matches(".*.[0-9]{1,2}\\s[a-zA-Z]{3}\\s[0-9]{4}.*")) {
      String[] s = tripName.split("[0-9]{1,2}\\s[a-zA-Z]{3}\\s[0-9]{4}");
      if (s[1] != null && s[1].trim().length() > 0) {
        return tripName.replace(s[0],"").trim();
      }
    }
    return null;
  }

  public static boolean hasHotelPassengerContent (com.mapped.publisher.view.UmAccommodationReservationView arv) {
    if (arv != null && arv.getPassengers() != null && arv.getPassengers().size() > 0) {
      for (UmAccommodationReservationView.PassengerInfo v : arv.getPassengers()) {
        if ((v.getRoomType() != null && !v.getRoomType().isEmpty())
            || (v.getBedding() != null && !v.getBedding().isEmpty())
            || (v.getMembershipId() != null && !v.getMembershipId().isEmpty())
            || (v.getStatus() != null && !v.getStatus().isEmpty())) {
          return true;
        }
      }
    }

    return false;

  }
}
