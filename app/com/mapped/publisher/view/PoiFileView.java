package com.mapped.publisher.view;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-15
 * Time: 12:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class PoiFileView
    extends BaseView {
  public String id;
  public String name;
  public String comments;
  public String rank;
  public String status;
  public String fileName;
}
