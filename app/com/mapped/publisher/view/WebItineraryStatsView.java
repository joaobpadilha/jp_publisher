package com.mapped.publisher.view;

import java.sql.Timestamp;

/**
 * Created by george on 2016-11-25.
 */
public class WebItineraryStatsView extends BaseView {
  public String tripId;
  public Long uId;
  public int clicks;
  public String firstClick;
  public String lastClick;
}
