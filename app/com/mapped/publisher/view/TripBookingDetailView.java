package com.mapped.publisher.view;

import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.parse.schemaorg.PostalAddress;
import com.mapped.publisher.parse.schemaorg.Reservation;
import com.mapped.publisher.persistence.CountriesInfo;
import com.mapped.publisher.persistence.PoiRS;
import com.mapped.publisher.persistence.template.Attachment;
import com.mapped.publisher.utils.Utils;
import com.umapped.api.schema.local.PoiJson;
import com.umapped.api.schema.types.Address;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.UmPostalAddress;
import com.umapped.persistence.reservation.UmRate;
import com.umapped.persistence.reservation.UmReservation;

import com.umapped.persistence.reservation.UmTraveler;
import com.umapped.persistence.reservation.accommodation.UmAccommodationReservation;
import com.umapped.persistence.reservation.activity.UmActivityReservation;
import com.umapped.persistence.reservation.transfer.UmTransfer;
import com.umapped.persistence.reservation.transfer.UmTransferReservation;
import controllers.BookingController;
import models.publisher.Account;
import models.publisher.TripDetail;
import org.joda.time.DateTime;

import java.io.Serializable;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;
import static controllers.PoiController.getMergedPoi;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-19
 * Time: 4:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class TripBookingDetailView extends NoteBaseView
    implements Serializable {

    /* Trip Note fields */

  public Long noteId;
  public String intro;
  public String description;
  public int rank = 0;
  public String loggedInUserId;
  public String landmark;
  public String cmpyId;
  public String tag;
  public long tripStartDate;
  public String tripStartDatePrint;
  public String linkedTripDetailsId;
  public boolean isMobilized;
  /* Common fields */

  public SecurityMgr.AccessLevel accessLevel;
  public boolean isCmpyAdmin;
  public TripBookingDetailView tripNote;

  public String          detailsId;
  public String          tripId;
  public String          tripName;
  public String          createdById;
  public String          createdByName;
  public ReservationType detailsTypeId;
  public int             tripType;
  public String          bookingNumber;
  public String          startDate;
  public String          startDatePrint;

  public String startTime;
  public long startDateMs;
  public String endDate;
  public String endDatePrint;

  public String endTime;
  public long endDateMs;
  public long createdTimestampMs;
  public String createdDate;

  public BookingSrc.ImportSrc importSrc;
  public String importSrcId;


  public String recordLocator;
  public int passengerCount;

  public int status;
  public String contact;

  /**
   * These should replace
   */
  public PoiRS poiMain;
  public PoiRS poiStart;
  public PoiRS poiFinish;

  public String name;
  public Long poiId;

  /**
   * To be replaced with POI
   */
  public String providerName;
  public String providerComments;
  public String providerWeb;
  public String providerEmail;

  /* Tobe be replaced by POI: Location Information (2014-09-09) */
  public Long locStartPoi;
  public String locStartName;
  public Float locStartLat;
  public Float locStartLong;
  public Long locFinishPoi;
  public String locFinishName;
  public Float locFinishLat;
  public Float locFinishLong;
  public int dayOffset;
  public int duration;


  public String providerAddr1;
  public String providerAddr2;
  public String providerPhone;
  public String city;
  public String country;
  public String zip;
  public String state;
  public String departCity;
  public String departCountry;
  public String arriveCity;
  public String arriveCountry;
  public String formattedAddr;

  public String destination;
  public String destinationId;
  public String destinationUrl;

  public String consortiumName;
  public String consortiumAmenities;

  public String baggageUrl;
  public String checkinUrl;

  public EnhancedTripBookingDetailView enhancedView = null;

  public boolean afterCrossDateFlight = false;

  // The reservation data is here
  public UmReservation reservation;

  public TripDetailTags tagChecks = new TripDetailTags();

  public static class TripDetailTags
      implements Serializable {
    public boolean pageBreakBefore;
    public boolean pageBreakAfter;
    public boolean lastItem;

    public TripDetailTags() {
      this.pageBreakBefore = false;
      this.pageBreakAfter = false;
      this.lastItem = false;
    }

    public boolean isPageBreakBefore() {
      return pageBreakBefore;
    }

    public void setPageBreakBefore(boolean pageBreakBefore) {
      this.pageBreakBefore = pageBreakBefore;
    }

    public boolean isPageBreakAfter() {
      return pageBreakAfter;
    }

    public void setPageBreakAfter(boolean pageBreakAfter) {
      this.pageBreakAfter = pageBreakAfter;
    }

    public boolean isLastItem() {
      return lastItem;
    }

    public void setLastItem(boolean lastItem) {
      this.lastItem = lastItem;
    }
  }
  
  public String getMonthName (String mon) {
    try {
      String[] str = {"JAN",
                      "FEB",
                      "MAR",
                      "APR",
                      "MAY",
                      "JUN",
                      "JUL",
                      "AUG",
                      "SEP",
                      "OCT",
                      "NOV",
                      "DEC"};
      int i = Integer.parseInt(mon);
      if (i >= 0 && i <12) {
        return str[i];
      }

    } catch (Exception e) {

    }
    return "";
  }

  public static class Cruise
      implements Serializable {
    public String name;
    public String cmpyName;
    /**
     * Not applicable when used for templates
     */
    public String category;
    /**
     * Not applicable when used for templates
     */
    public String cabinNumber;
    /**
     * Not applicable when used for templates
     */
    public String deck;
    /**
     * Not applicable when used for templates
     */
    public String bedding;
    /**
     * Not applicable when used for templates
     */
    public String meal;
    public String portDepart;
    public String portArrive;
  }

  public Cruise cruise;
  public String flightId;

  public String departureAirport;
  public String departureAirportCode;
  public String arrivalAirportCode;

  public String arrivalAirport;
  public Long departureAirportId;
  public Long arrivalAirportId;
  public String comments;
  public String origComments;

  public String startYear;
  public String startMonth;
  public String startDay;
  public String startHour;
  public String startMin;
  public String endYear;
  public String endMonth;
  public String endDay;
  public String endHour;
  public String endMin;

  public boolean showPhotosBtn;
  public List<AttachmentView> files;
  public List<AttachmentView> photos;
  public List<AttachmentView> videos;
  public List<AttachmentView> links;


  //Fields from EnhancedTripBookingDetailView.java
  //public List<CruiseStop>    cruiseStops;
  public String departTerminal = "";
  public String arriveTerminal = "";
  public String amenities      = "";
  public String providerFax;
  public String aircraft = "";
  public String distance = "";
  public String stops    = "";

  public static Comparator<TripBookingDetailView> TripBookingDetailViewComparator = new Comparator<TripBookingDetailView>() {

    public int compare(TripBookingDetailView t1, TripBookingDetailView t2) {
      return timeCompare(t1, t2);
    }

  };

  public static  int timeCompare (TripBookingDetailView t1, TripBookingDetailView t2) {
    long origT1 = t1.startDateMs;
    long origT2 = t2.startDateMs;

    //if this is a flight that lands before it takes off, then use the landing time so we keep the time chronological
    if (t1.detailsTypeId == ReservationType.FLIGHT && t1.endDateMs > 0 && t1.endDateMs < t1.startDateMs) {
      //set only if the endDateMS is the same day as startStateMs
      String startDate = Utils.formatDateControl(t1.startDateMs);
      String endDate = Utils.formatDateControl(t1.endDateMs);
      if (startDate != null && endDate != null && startDate.equals(endDate)) {
        origT1 = t1.endDateMs;
      }
    }
    if (t2.detailsTypeId == ReservationType.FLIGHT && t2.endDateMs > 0 && t2.endDateMs < t2.startDateMs) {
      //set only if the endDateMS is the same day as startStateMs
      String startDate = Utils.formatDateControl(t1.startDateMs);
      String endDate = Utils.formatDateControl(t1.endDateMs);
      if (startDate != null && endDate != null && startDate.equals(endDate)) {
        origT2 = t2.endDateMs;
      }
    }

    if (origT1 > 0 && (t1.noteId == null || t1.noteId == 0)) {
      Calendar cal = Calendar.getInstance();
      cal.setTimeInMillis(origT1);
      if (cal.get(Calendar.HOUR_OF_DAY) == 0 && cal.get(Calendar.MINUTE) == 0) {
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        origT1 = cal.getTimeInMillis();
      }
    }

    if (origT2 > 0 && (t2.noteId == null || t2.noteId == 0)) {
      Calendar cal = Calendar.getInstance();
      cal.setTimeInMillis(origT2);
      if (cal.get(Calendar.HOUR_OF_DAY) == 0 && cal.get(Calendar.MINUTE) == 0) {
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        origT2 = cal.getTimeInMillis();
      }
    }

    DateTime t1DateTime = new DateTime(origT1);
    DateTime t2DateTime = new DateTime(origT2);
    if (t1DateTime.withTimeAtStartOfDay().getMillis() == t2DateTime.withTimeAtStartOfDay().getMillis()) {
      //we try a sort by rank only for bookings within the same day
      //we exclude the notes since the notes sorting have their own complicated logic
      if((t1.rank > 0 || t2.rank > 0) && (t1.noteId == null || t1.noteId == 0) && (t2.noteId == null || t2.noteId == 0)) {
        if (t1.rank > t2.rank) {
          return  1;
        } else if (t1.rank < t2.rank) {
          return -1;
        }
        return 0;
      } else {

        if (origT1 > origT2) {
          return 1;
        }
        else if (origT1 < origT2) {
          return -1;
        }
        else {
          if (t1.rank > t2.rank) {
            return  1;
          } else if (t1.rank < t2.rank) {
            return -1;
          }
          return 0;
        }
      }
    } else {
      if (origT1 > origT2) {
        return 1;
      }
      else if (origT1 < origT2) {
        return -1;
      }
      else {
        if (t1.rank > t2.rank) {
          return  1;
        } else if (t1.rank < t2.rank) {
          return -1;
        }
        return 0;
      }
    }


  }

  public String getTimePrint(String hr, String min){
    try {
      int h = Integer.parseInt(hr);
      int m = Integer.parseInt(min);
      if (!(h == 0 && m == 0)) {
        String hs = hr;
        String ms = min;
        if (h  == 0) {
          hs  = "00";
        }
        if (m  < 10) {
          ms  = "0" + ms;
        }
        return hs+":"+ms;
      }
    } catch (Exception e) {

    }
    return "";
  }

  public boolean hasStartTimestamp () {
    if (startTime != null && !startTime.isEmpty() && !startTime.contains("23:59") && !startTime.contains("11:59 PM") && !startTime.contains("12:00 AM")) {
      return true;
    }
    return false;
  }

  public void buildViewInfo(String tripId, TripDetail tripDetail, boolean printDateFormat, SecurityMgr.AccessLevel tripAccessLevel) {
    PoiRS poiMain   = getMergedPoi(tripDetail.getPoiId(), tripDetail.getPoiCmpyId());
    PoiRS poiStart  = getMergedPoi(tripDetail.getLocStartPoiId(), tripDetail.getLocStartPoiCmpyId());
    PoiRS poiFinish = getMergedPoi(tripDetail.getLocFinishPoiId(), tripDetail.getLocFinishPoiCmpyId());

    UmReservation reservation = tripDetail.getReservation();

    this.poiMain        = poiMain;
    this.poiStart       = poiStart;
    this.poiFinish      = poiFinish;
    this.tripId         = tripId;
    this.reservation    = reservation;
    this.createdById = tripDetail.createdby;

    if (Utils.isInteger(this.createdById)) {
      try {
        Long acid = Long.parseLong(this.createdById);
        Account a = Account.find.byId(acid);
        this.createdByName = (a != null)?a.getFullName():this.createdById;
        this.accessLevel = SecurityMgr.AccessLevel.TRAVELER;
      } catch (Exception e) {} //Ignoring
    }
    else {
      Account a = Account.findByLegacyId(this.createdById);
      this.createdByName = (a != null)?a.getFullName():this.createdById;
      this.accessLevel = tripAccessLevel;
    }

    this.status         = tripDetail.status;
    this.detailsId      = tripDetail.detailsid;
    this.detailsTypeId  = tripDetail.detailtypeid;
    this.tripType       = tripDetail.triptype;
    this.bookingNumber  = tripDetail.bookingnumber;
    this.recordLocator  = tripDetail.getRecordLocator();
    // TODO: (Wei)   Use reservation.notePlainTet when everything is migrated
    // Currently it is done by each type below

    if (tripDetail.getReservation() == null) {
      this.comments       = Utils.escapeHtml(tripDetail.comments);
      this.origComments   = tripDetail.comments;
    } else {
      this.comments       = Utils.escapeHtml(tripDetail.getReservation().getNotesPlainText());
      this.origComments   = tripDetail.getReservation().getNotesPlainText();
    }

    this.passengerCount = tripDetail.getPassengerCount();
    this.name           = tripDetail.getName();
    this.providerName   = tripDetail.getName(); //TODO: Serguei: Cleanup eventually
    this.rank           = tripDetail.getRank();
    this.createdTimestampMs = tripDetail.getCreatedtimestamp();
    this.importSrc      = tripDetail.getImportSrc();
    this.importSrcId    = tripDetail.getImportSrcId();

    if (tripDetail.starttimestamp != null && tripDetail.starttimestamp > 0) {
      this.startDatePrint = Utils.formatDatePrint(tripDetail.starttimestamp);
      this.startDate = Utils.getDateString(tripDetail.starttimestamp);
      this.startTime = Utils.getTimeString(tripDetail.starttimestamp);
      this.startDateMs = tripDetail.starttimestamp;
      Calendar cal = Calendar.getInstance();
      cal.setTimeInMillis(tripDetail.starttimestamp);
      this.startYear = String.valueOf(cal.get(Calendar.YEAR));
      this.startMonth = String.valueOf(cal.get(Calendar.MONTH));
      this.startDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
      this.startHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
      this.startMin = String.valueOf(cal.get(Calendar.MINUTE));
    }
    if (tripDetail.endtimestamp != null &&
            tripDetail.starttimestamp > 0 && tripDetail.endtimestamp > 0) {
      if ((tripDetail.detailtypeid != null && tripDetail.detailtypeid == ReservationType.FLIGHT) || !tripDetail.endtimestamp.equals(tripDetail.starttimestamp)) {
        this.endDate = Utils.getDateString(tripDetail.endtimestamp);
        this.endDatePrint = Utils.formatDatePrint(tripDetail.endtimestamp);
        this.endTime = Utils.getTimeString(tripDetail.endtimestamp);
        this.endDateMs = tripDetail.endtimestamp;
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(tripDetail.endtimestamp);
        this.endYear = String.valueOf(cal.get(Calendar.YEAR));
        this.endMonth = String.valueOf(cal.get(Calendar.MONTH));
        this.endDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
        this.endHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
        this.endMin = String.valueOf(cal.get(Calendar.MINUTE));
      }
    }

    //Updating poi information that might have changed. Not updating flights as they have custom naming consisting
    //of poi name and flight number.
    if (this.poiMain != null &&
        this.poiMain.getIdLong() != null) {
      this.poiId              = this.poiMain.getId();
      if (this.poiMain.getName() != null) {
        this.providerName = this.poiMain.getName();
      }
      if (printDateFormat) {
        this.providerComments = Utils.escapeHtml(this.poiMain.data.getDesc());
      }
      else {
        this.providerComments = this.poiMain.data.getDesc();
      }
    } else if (this.poiMain == null && tripDetail.detailtypeid == ReservationType.FLIGHT) {
      this.providerName = null;
    }
    else if (tripDetail.getName() != null){
      this.providerName       = tripDetail.getName();
    }


    //This is a kludge to support a historical feature for flight booking- will be fixed by PUB-249
    /*
    historically, there was a depart loc and start loc in the booking view
    for flights, depart loc was the departure airport and start loc was the arrival airport
    to limit the changes for all the maps and plots, when building the view for flights, we need to flip the departure and arrival airport i.e. depart airport becomes finish location and arrive airport becomes start location
    if not, we need to change all the maps and the mobile since the start loc used to be the arrival airport and is now the departure airport
     */
    if (tripDetail.detailtypeid == ReservationType.FLIGHT) {
      if (this.poiStart != null) { //Updating data from probably up to date poi object
        this.locFinishName     = this.poiStart.getName();
        this.locFinishPoi      = this.poiStart.getId();
        this.locFinishLat     = this.poiStart.locLat;
        this.locFinishLong     = this.poiStart.locLong;
      } else {
        this.locFinishName    = tripDetail.getLocStartName();
        this.locFinishLat    = tripDetail.getLocStartLat();
        this.locFinishLong     = tripDetail.getLocStartLong();
      }


      if (this.poiFinish != null) {
        this.locStartName     = this.poiFinish.getName();
        this.locStartPoi      = this.poiFinish.getId();
        this.locStartLat      = this.poiFinish.locLat;
        this.locStartLong     = this.poiFinish.locLong;
      } else {
        this.locStartName     = tripDetail.getLocFinishName();
        this.locStartLat      = tripDetail.getLocFinishLat();
        this.locStartLong     = tripDetail.getLocFinishLong();
      }

    } else {
      if (this.poiStart != null) { //Updating data from probably up to date poi object
        //hack for cruise stops imported - we would have a poiid for gps coordinates but the name might not be 100 match for accuracy reason
        if (this.detailsTypeId == ReservationType.CRUISE_STOP || this.detailsTypeId == ReservationType.TOUR) {
          this.locStartName = tripDetail.getLocStartName();
        } else {
          this.locStartName = this.poiStart.getName();
        }
        this.locStartPoi      = this.poiStart.getId();
        this.locStartLat      = this.poiStart.locLat;
        this.locStartLong     = this.poiStart.locLong;
      } else {
        this.locStartName     = tripDetail.getLocStartName();
        this.locStartLat      = tripDetail.getLocStartLat();
        this.locStartLong     = tripDetail.getLocStartLong();
      }


      if (this.poiFinish != null) {
        this.locFinishName     = this.poiFinish.getName();
        this.locFinishPoi      = this.poiFinish.getId();
        this.locFinishLat      = this.poiFinish.locLat;
        this.locFinishLong     = this.poiFinish.locLong;
      } else {
        this.locFinishName     = tripDetail.getLocFinishName();
        this.locFinishLat      = tripDetail.getLocFinishLat();
        this.locFinishLong     = tripDetail.getLocFinishLong();
      }
    }

    //Add Files to Flight booking
    this.files = BookingController.getFilesForBooking(tripDetail.detailsid);


    //Extracting other misc data from whatever poi information is currently available (mainly for older records)
    PoiRS prs = null;
    this.showPhotosBtn = false;
    if (this.poiMain != null  && this.poiMain.getIdLong() != null) {
      prs = this.poiMain;

      TripDetail.ExtraDetails ed = tripDetail.getExtraDetails();

      this.photos = BookingController.getPhotosForBooking(tripDetail, prs.getId(), prs.getCmpyId(), true);
      if (this.photos != null && this.photos.size() > 0) {
        this.showPhotosBtn = true;
      }
      if (ed != null && ed.showPhotos != null && !ed.showPhotos && this.photos != null) {
        this.photos.clear();
      }
    } else if (this.poiStart != null) {
      prs = this.poiStart;
    } else if (this.poiFinish != null) {
      prs = this.poiFinish;
    }

    if (prs != null) {
      Address a = prs.getMainAddress();
      if (a != null) {
        this.formattedAddr = prs.getMainAddressString();
        this.providerAddr1 = a.getStreetAddress();
        if (this.providerAddr1 != null && this.formattedAddr.contains(this.providerAddr1) && this.formattedAddr.length() > this.providerAddr1.length()) {
           this.providerAddr2 = this.formattedAddr.substring(this.providerAddr1.length()).trim();
          if (this.providerAddr2.startsWith(",")) {
            this.providerAddr2 = this.providerAddr2.replaceFirst(",", "").trim();
          }

        }
        this.city = a.getLocality();
        this.state = a.getRegion();
        this.zip = a.getPostalCode();

        String cc = (a.getCountryCode() != null) ? a.getCountryCode() : prs.countryCode;

        if (cc != null && !cc.equals("UNK")) {
          this.country = CountriesInfo.Instance().byAlpha3(cc).getName(CountriesInfo.LangCode.EN);
        }
      }

      if (prs.data.getPhoneNumbers().size() > 0) {
        this.providerPhone = prs.getMainPhone().getNumber();
      }

      if (prs.data.getUrls().size() > 0) {
        this.providerWeb = prs.getMainUrl();
      }

      if (prs.data.getEmails().size() > 0) {
        this.providerEmail = prs.getMainEmail().getAddress();
      }
    } else {
      setAddress(tripDetail);
    }

    this.tag = tripDetail.getTag();

    //Set boolean for Page breaks tags
    if (tripDetail.getTag() != null && !tripDetail.getTag().isEmpty()) {
      if (tripDetail.getTag().toLowerCase().contains(APPConstants.TAG_PAGE_BREAK_BEFORE)) {
        this.tagChecks.pageBreakBefore = true;
      }
      if (tripDetail.getTag().toLowerCase().contains(APPConstants.TAG_PAGE_BREAK_AFTER)) {
        this.tagChecks.pageBreakAfter = true;
      }
    }

  }

  public void buildViewInfo(TripDetail tripDetail, boolean printDateFormat, SecurityMgr.AccessLevel tripAccessLevel) {
    PoiRS poiMain   = getMergedPoi(tripDetail.getPoiId(), tripDetail.getPoiCmpyId());
    PoiRS poiStart  = getMergedPoi(tripDetail.getLocStartPoiId(), tripDetail.getLocStartPoiCmpyId());
    PoiRS poiFinish = getMergedPoi(tripDetail.getLocFinishPoiId(), tripDetail.getLocFinishPoiCmpyId());
    this.tag = tripDetail.getTag();
    
    UmReservation reservation = tripDetail.getReservation();

    this.poiMain        = poiMain;
    this.poiStart       = poiStart;
    this.poiFinish      = poiFinish;
    this.reservation    = reservation;
    this.providerAddr2  = null;
    this.country        = null;

    //Updating poi information that might have changed. Not updating flights as they have custom naming consisting
    //of poi name and flight number.
    if (this.poiMain != null &&
            this.poiMain.getIdLong() != null) {
      this.poiId              = this.poiMain.getId();
      if (this.poiMain.getName() != null) {
        this.providerName = this.poiMain.getName();
      }
      if (printDateFormat) {
        this.providerComments = Utils.escapeHtml(this.poiMain.data.getDesc());
      }
      else {
        this.providerComments = this.poiMain.data.getDesc();
      }
    } else if (this.poiMain == null && tripDetail.detailtypeid == ReservationType.FLIGHT) {
      this.providerName = null;
    }
    else if (tripDetail.getName() != null){
      this.providerName       = tripDetail.getName();
    }


    //This is a kludge to support a historical feature for flight booking- will be fixed by PUB-249
    /*
    historically, there was a depart loc and start loc in the booking view
    for flights, depart loc was the departure airport and start loc was the arrival airport
    to limit the changes for all the maps and plots, when building the view for flights, we need to flip the departure and arrival airport i.e. depart airport becomes finish location and arrive airport becomes start location
    if not, we need to change all the maps and the mobile since the start loc used to be the arrival airport and is now the departure airport
     */
    if (tripDetail.detailtypeid == ReservationType.FLIGHT) {
      if (this.poiStart != null) { //Updating data from probably up to date poi object
        this.locFinishName     = this.poiStart.getName();
        this.locFinishPoi      = this.poiStart.getId();
        this.locFinishLat     = this.poiStart.locLat;
        this.locFinishLong     = this.poiStart.locLong;
      } else {
        this.locFinishName    = tripDetail.getLocStartName();
        this.locFinishLat    = tripDetail.getLocStartLat();
        this.locFinishLong     = tripDetail.getLocStartLong();
      }


      if (this.poiFinish != null) {
        this.locStartName     = this.poiFinish.getName();
        this.locStartPoi      = this.poiFinish.getId();
        this.locStartLat      = this.poiFinish.locLat;
        this.locStartLong     = this.poiFinish.locLong;
      } else {
        this.locStartName     = tripDetail.getLocFinishName();
        this.locStartLat      = tripDetail.getLocFinishLat();
        this.locStartLong     = tripDetail.getLocFinishLong();
      }
    } else {
      if (this.poiStart != null) { //Updating data from probably up to date poi object
        //hack for cruise stops imported - we would have a poiid for gps coordinates but the name might not be 100 match for accuracy reason
        if (this.detailsTypeId == ReservationType.CRUISE_STOP || this.detailsTypeId == ReservationType.TOUR) {
          this.locStartName = tripDetail.getLocStartName();
        } else {
          this.locStartName = this.poiStart.getName();
        }
        this.locStartPoi      = this.poiStart.getId();
        this.locStartLat      = this.poiStart.locLat;
        this.locStartLong     = this.poiStart.locLong;
      } else {
        this.locStartName     = tripDetail.getLocStartName();
        this.locStartLat      = tripDetail.getLocStartLat();
        this.locStartLong     = tripDetail.getLocStartLong();
      }


      if (this.poiFinish != null) {
        this.locFinishName     = this.poiFinish.getName();
        this.locFinishPoi      = this.poiFinish.getId();
        this.locFinishLat      = this.poiFinish.locLat;
        this.locFinishLong     = this.poiFinish.locLong;
      } else {
        this.locFinishName     = tripDetail.getLocFinishName();
        this.locFinishLat      = tripDetail.getLocFinishLat();
        this.locFinishLong     = tripDetail.getLocFinishLong();
      }
    }

    //Add Files to Flight booking
    this.files = BookingController.getFilesForBooking(tripDetail.detailsid);


    //Extracting other misc data from whatever poi information is currently available (mainly for older records)
    PoiRS prs = null;
    this.showPhotosBtn = false;
    if (this.poiMain != null  && this.poiMain.getIdLong() != null) {
      prs = this.poiMain;

      TripDetail.ExtraDetails ed = tripDetail.getExtraDetails();

      this.photos = BookingController.getPhotosForBooking(tripDetail, prs.getId(), prs.getCmpyId(), true);
      if (this.photos != null && this.photos.size() > 0) {
        this.showPhotosBtn = true;
      }
      if (ed != null && ed.showPhotos != null && !ed.showPhotos && this.photos != null) {
        this.photos.clear();
      }
    } else if (this.poiStart != null) {
      prs = this.poiStart;
    } else if (this.poiFinish != null) {
      prs = this.poiFinish;
    }
    if(this.recordLocator == null) {
      this.recordLocator = "9";
    }
    if (prs != null) {
      Address a = prs.getMainAddress();
      if (a != null) {
        this.formattedAddr = prs.getMainAddressString();
        this.providerAddr1 = a.getStreetAddress();

        if (this.providerAddr1 != null && this.formattedAddr.contains(this.providerAddr1) && this.formattedAddr.length() > this.providerAddr1.length()) {
          this.providerAddr2 = this.formattedAddr.substring(this.providerAddr1.length()).trim();
          if (this.providerAddr2.startsWith(",")) {
            this.providerAddr2 = this.providerAddr2.replaceFirst(",", "").trim();
          }

        }

        this.city = a.getLocality();
        this.state = a.getRegion();
        this.zip = a.getPostalCode();

        String cc = (a.getCountryCode() != null) ? a.getCountryCode() : prs.countryCode;

        if (cc != null && !cc.equals("UNK")) {
          this.country = CountriesInfo.Instance().byAlpha3(cc).getName(CountriesInfo.LangCode.EN);
        }
      }

      if (prs.data.getPhoneNumbers().size() > 0) {
        this.providerPhone = prs.getMainPhone().getNumber();
      }

      if (prs.data.getUrls().size() > 0) {
        this.providerWeb = prs.getMainUrl();
      }

      if (prs.data.getEmails().size() > 0) {
        this.providerEmail = prs.getMainEmail().getAddress();
      }
    } else {
      setAddress(tripDetail);
    }

    this.tag = tripDetail.getTag();

    //Set boolean for Page breaks tags
    if (tripDetail.getTag() != null && !tripDetail.getTag().isEmpty()) {
      if (tripDetail.getTag().toLowerCase().contains(APPConstants.TAG_PAGE_BREAK_BEFORE)) {
        this.tagChecks.pageBreakBefore = true;
      }
      if (tripDetail.getTag().toLowerCase().contains(APPConstants.TAG_PAGE_BREAK_AFTER)) {
        this.tagChecks.pageBreakAfter = true;
      }
    }
  }

  public void setAddress (TripDetail tripDetail) {
    UmReservation reservation = tripDetail.getReservation();

    switch (tripDetail.getDetailtypeid()) {
      case HOTEL:
        UmAccommodationReservation h = cast(tripDetail.getReservation(), UmAccommodationReservation.class);
        if (h != null && h.getAccommodation() != null && h.getAccommodation().getAddress() != null) {
          UmPostalAddress addr = h.getAccommodation().getAddress();
          this.providerPhone = h.getAccommodation().telephone;
          this.providerFax = h.getAccommodation().faxNumber;
          this.providerWeb = h.getAccommodation().url;
          setAddr(addr);
        }
        break;
      case ACTIVITY:
        UmActivityReservation a = cast(tripDetail.getReservation(), UmActivityReservation.class);
        if (a != null && a.getActivity() != null && a.getActivity().getOrganizedBy() != null) {
          this.providerPhone = a.getActivity().getOrganizedBy().telephone;
          this.providerFax = a.getActivity().getOrganizedBy().faxNumber;
          this.providerWeb = a.getActivity().getOrganizedBy().url;
          UmPostalAddress addr = a.getActivity().getOrganizedBy().getAddress();
          setAddr(addr);
        }
        break;
      case TRANSPORT:
        UmTransferReservation t = cast(tripDetail.getReservation(), UmTransferReservation.class);
        if (t != null && t.getServiceProvider() != null ) {
          this.providerPhone = t.getServiceProvider().telephone;
          this.providerFax = t.getServiceProvider().faxNumber;
          this.providerWeb = t.getServiceProvider().url;
          UmPostalAddress addr = t.getServiceProvider().getAddress();
          setAddr(addr);
        }

        break;

    }
  }

  public void setAddr (UmPostalAddress addr) {
    if (addr != null) {
      this.providerAddr1 = addr.streetAddress;

      StringBuilder sb = new StringBuilder();
      if (addr.addressLocality != null && addr.addressLocality.trim().length() > 0) {
        if (sb.toString().trim().length() > 0) {
          sb.append(", ");
        }
        sb.append(addr.addressLocality.trim());
      }

      if (addr.addressRegion != null && addr.addressRegion.trim().length() > 0) {
        if (sb.toString().trim().length() > 0) {
          sb.append(", ");
        }
        sb.append(addr.addressRegion.trim());
        sb.append(' ');
      }
      if (addr.postalCode != null && addr.postalCode.trim().length() > 0) {
        sb.append(addr.postalCode.trim());
      }
      if (addr.addressCountry != null && addr.addressCountry.trim().length() > 0) {
        sb.append(addr.addressCountry.trim());
      }

      this.providerAddr2 = sb.toString();

      this.city = addr.addressLocality;
      this.state = addr.addressRegion;
      this.zip = addr.postalCode;
      this.country = addr.addressCountry;
    }
  }

  public ReservationType getDetailsTypeId() {
    return this.detailsTypeId;
  }

  public UmReservation getReservation() {
    return this.reservation;
  }

  public String getCreatedById() {
    return this.createdById;
  }

  public List<AttachmentView> getFiles() {
    return this.files;
  }

  public List<AttachmentView> getPhotos() {
    return this.photos;
  }

  public List<AttachmentView> getVideos() {
    return this.videos;
  }

  public List<AttachmentView> getLinks() {
    return this.links;
  }

  public String getTripId() {
    return this.tripId;
  }

  public String getTripName() {
    return this.tripName;
  }

  public String getStartDate() {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
    if(this.reservation != null && this.reservation.getStartDateTime() != null && this.reservation.getStartDateTime().getTime() != null) {
      return this.reservation.getStartDateTime().getTime().format(formatter);
    } else {
      return this.startDate;
    }
  }

  public String getStartDateNote() {
    return this.startDate;
  }

  public String getStartDateItin() {
    ZoneId zoneId = ZoneId.systemDefault();
    if(this.reservation != null && this.reservation.getStartDateTime() != null && this.reservation.getStartDateTime().getTime() != null) {
      return Utils.formatDatePrint(this.reservation.getStartDateTime().getTime().atZone(zoneId).toInstant().toEpochMilli());
    } else {
      return this.startDatePrint;
    }
  }

  public String getStartDatePrintNote() {
    return this.startDatePrint;
  }

  public Long getStartDateMilli() {
    ZoneId zoneId = ZoneId.systemDefault();
    if(this.reservation != null && this.reservation.getStartDateTime() != null && this.reservation.getStartDateTime().getTime() != null) {
      return this.reservation.getStartDateTime().getTime().atZone(zoneId).toInstant().toEpochMilli();
    } else {
      return this.startDateMs;
    }
  }

  public Long getStartDateMsNote() {
    return this.startDateMs;
  }

  public String getStartHour() {
    return this.startHour;
  }

  public String getStartDay() {
    return this.startDay;
  }

  public String getStartMonth() {
    return this.startMonth;
  }

  public String getEndHour() {
    if (this.reservation != null && this.reservation.getEndDateTime() != null && this.reservation.getEndDateTime().getTime() != null && this.reservation.getStartDateTime() != null && this.reservation.getStartDateTime().getTime() != null && this.reservation.getStartDateTime().getTime().equals(this.reservation.getEndDateTime().getTime())) {
      return "";
    }
    return endHour;
  }

  public String getEndDay() {
    return this.endDay;
  }

  public String getEndMonth() {
    return this.endMonth;
  }

  public String getStartTimeNote() {
    return this.startTime;
  }

  public String getEndDate() {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
    if(this.reservation != null && this.reservation.getEndDateTime() != null && this.reservation.getEndDateTime().getTime() != null) {
      if (this.reservation.getStartDateTime() != null && this.reservation.getStartDateTime().getTime() != null && this.reservation.getStartDateTime().getTime().equals(this.reservation.getEndDateTime().getTime())) {
        return "";
      }
      return this.reservation.getEndDateTime().getTime().format(formatter);
    } else {
      return this.endDate;
    }
  }

  public String getEndDateItin() {
    ZoneId zoneId = ZoneId.systemDefault();
    if(this.reservation != null && this.reservation.getEndDateTime() != null && this.reservation.getEndDateTime().getTime() != null) {
      if (this.reservation.getStartDateTime() != null && this.reservation.getStartDateTime().getTime() != null && this.reservation.getStartDateTime().getTime().equals(this.reservation.getEndDateTime().getTime())) {
        return "";
      }
      return Utils.formatDatePrint(this.reservation.getEndDateTime().getTime().atZone(zoneId).toInstant().toEpochMilli());
    } else {
      return this.endDatePrint;
    }
  }

  public Long getEndDateMilli() {
    ZoneId zoneId = ZoneId.systemDefault();
    if(this.reservation != null && this.reservation.getEndDateTime() != null && this.reservation.getEndDateTime().getTime() != null) {
      return this.reservation.getEndDateTime().getTime().atZone(zoneId).toInstant().toEpochMilli();
    } else {
      return this.endDateMs;
    }
  }

  public String getStartTime() {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm a");
    if(this.reservation != null && this.reservation.getStartDateTime() != null && this.reservation.getStartDateTime().getTime() != null) {
      return this.reservation.getStartDateTime().getTime().format(formatter);
    } else {
      return this.startTime;
    }
  }

  public String getEndTime() {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm a");
    if(this.reservation != null && this.reservation.getEndDateTime() != null && this.reservation.getEndDateTime().getTime() != null) {
      if (this.reservation.getStartDateTime() != null && this.reservation.getStartDateTime().getTime() != null && this.reservation.getStartDateTime().getTime().equals(this.reservation.getEndDateTime().getTime())) {
        return "";
      }
      return this.reservation.getEndDateTime().getTime().format(formatter);
    } else {
      return "";
    }
  }

  public String getNotesPlain() {
    if(this.reservation != null) {
      return this.reservation.getNotesPlainText();
    } else {
      return "";
    }
  }

  public String getBookingNumber() {
    return this.bookingNumber;
  }

  public String getDetailsId() {
    return this.detailsId;
  }

  public String getProviderName() {
    return this.providerName;
  }

  public String getProviderAddr1() {
    return this.providerAddr1;
  }

  public String getProviderAddr2() {
    return this.providerAddr2;
  }

  public String getProviderPhone() {
    return this.providerPhone;
    //return this.reservation.getServiceProvider().telephone;
  }

  public String getProviderWeb() {
    return this.providerWeb;
    //return this.reservation.getServiceProvider().url;
  }

  public String getProviderFax() {
    return this.providerFax;
  }


  public String getProviderEmail() {
    return this.providerEmail;
  }

  public String getProviderComments() {
    return this.providerComments;
  }

  public String getLocStartName() {
    return this.locStartName;
  }

  public String getLocFinishName() {
    return this.locFinishName;
  }

  public String getDestination() {
    return this.destination;
  }

  public String getName() {
    return this.name;
  }

  public PoiRS getPoiStart() {
    return this.poiStart;
  }

  public PoiRS getPoiFinish() {
    return this.poiFinish;
  }

  public String getLandmark() {
    return this.landmark;
  }

  public String getCity() {
    return this.city;
  }

  public String getZip() {
    return this.zip;
  }

  public String getState() {
    return this.state;
  }

  public String getCountry() {
    return this.country;
  }

  public Long getNoteId() {
    return this.noteId;
  }

  public String getIntro() {
    return this.intro;
  }

  public String getLinkedTripDetailsId() {
    return this.linkedTripDetailsId;
  }

  public String getTag() {
    return this.tag;
  }

  public Float getLocStartLat() {
    return this.locStartLat;
  }

  public Float getLocStartLong() {
    return this.locStartLong;
  }

  public String getDescription() {
    return this.description;
  }

  public PoiRS getPoiMain() { return this.poiMain; }

  //Rates
  public List<UmRate> getRates() {
    if(this.reservation != null) {
      return this.reservation.getRates();
    } else {
      return null;
    }
  }

  public String getTaxes() {
    if(this.reservation != null) {
      return this.reservation.getTaxes();
    } else {
      return "";
    }
  }

  public String getFees() {
    if(this.reservation != null) {
      return this.reservation.getFees();
    } else {
      return "";
    }
  }

  public String getSubtotal() {
    if(this.reservation != null) {
      return this.reservation.getSubtotal();
    } else {
      return "";
    }
  }

  public String getTotal() {
    if(this.reservation != null) {
      return this.reservation.getTotal();
    } else {
      return "";
    }
  }

  public String getCurrency() {
    if(this.reservation != null) {
      return this.reservation.getCurrency();
    } else {
      return "";
    }
  }

  public String getImportant() {
    if(this.reservation != null) {
      return this.reservation.getImportant();
    } else {
      return "";
    }
  }

  public String getCancellation() {
    if(this.reservation != null) {
      return this.reservation.getCancellationPolicy();
    } else {
      return "";
    }
  }

  public String getReservationStatus() {
    if(this.reservation != null) {
      return this.reservation.getReservationStatus();
    } else {
      return "";
    }
  }

  public String getServiceType() {
    if(this.reservation != null) {
      return this.reservation.getServiceType();
    } else {
      return "";
    }
  }
}


