package com.mapped.publisher.view;

import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-21
 * Time: 2:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class TripsView extends BaseView{

    public String searchTerm;
    public String myTrips;

    public List<TripInfoView> pendingTrips;

    public int pendingTripCount;

    public List<TripInfoView> publishedTrips;
    public int publishedTripCount;

    public List<TripInfoView> groupPublishedTrips;
    public int groupPublishedTripsCount;

    public List<TripInfoView> groupPendingTrips;
    public int groupPendingTripsCount;

    public HashMap<String, String > tripAgencyList;


    public List<TripInfoView> searchResults;
    public int searchResultsCount;
    public int searchResultsAdminCount;


    public String cmpyId;

    public String nextDate;
    public String prevDate;
    public String startYear;
    public String startMonth;
    public String startDay;

    public String tableName;
    public boolean isTour;
    public boolean isSharedTrip;

    public int searchType;
    public boolean noResults;

    public int nextAdminPos;
    public int prevAdminPos;
    public int searchStatus;

    public boolean displayCmpyTabs;

    public boolean allTrips;
    public boolean searchArchives;




}
