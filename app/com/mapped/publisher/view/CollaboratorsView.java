package com.mapped.publisher.view;

import com.mapped.publisher.common.SecurityMgr;
import models.publisher.TripInvite;
import models.publisher.TripShare;
import models.publisher.UserInvite;

import java.util.List;
import java.util.Map;

/**
 * Created by surge on 2014-06-05.
 */
public class CollaboratorsView
    extends BaseView {
  /**
   * Collaborators in the system
   */
  public List<TripShare> collaborators;
  /**
   * Invited future users
   */
  public List<TripInvite> invites;

  public boolean groupsEnabled;

  public String tripId;

  public boolean canInvite;

  public String currUserId;

  public Map<String, List<EmailLogView>> collabEmailLogs;

  public String getHumanAccessName(SecurityMgr.AccessLevel aLevel) {
    switch (aLevel) {
      case READ:
        return "Read";
      case APPEND:
        return "Contribute";
      case APPEND_N_PUBLISH:
        return "Contribute & Publish";
      case OWNER:
        return "Full Access";
      default:
        return "";
    }
  }

  public String getInviteState(UserInvite.InviteState state) {
    switch(state) {
      case EXPIRED:
        return "Invitation Expired";
      case EXPIRED_CHECKED:
        return "Visited, Expired";
      case CHECKED:
        return "Visited, Not Registered";
      case INVITE_FRESH:
        return "New Invite";
      case INVITE_OLD:
      default:
        return "Not Registered";
    }
  }
}
