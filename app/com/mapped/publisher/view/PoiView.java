package com.mapped.publisher.view;

import com.umapped.persistence.enums.ReservationType;

import java.util.HashMap;
import java.util.List;

/**
 * Class from the old day of Provider Code
 */
public class PoiView
    extends BaseView {
  public String id;
  public String cmpyId;
  public String cmpyName;
  public String tripId;
  public String detailsId;

  public String name;
  public String comments;
  public String status;
  public PoiAddressView address;
  public String locLat;
  public String locLong;

  public List<PoiAddressView> addresses;
  public List<PoiFileView> files;
  public HashMap<String, String> tripAgencyList;
  public boolean canEdit;

  public List<AttachmentView> photos;
  public String tag;

  public TabType tabType;
  public boolean canManage = false;
}
