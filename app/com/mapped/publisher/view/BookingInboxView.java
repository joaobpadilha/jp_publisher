package com.mapped.publisher.view;

import java.util.List;

/**
 * Created by twong on 15-08-19.
 */
public class BookingInboxView extends BaseView {
  public int startPos;
  public String term;
  public boolean newBookingsOnly = true;
  public String selectedSrc;

  public List<String> sourceList;
  public List<BookingReservationView> reservations;
}
