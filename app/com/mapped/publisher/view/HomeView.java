package com.mapped.publisher.view;

import com.mapped.publisher.common.Credentials;

/**
 * Created by surge on 2015-08-24.
 */
public class HomeView
    extends BaseView {
  public String firebaseURI;
  public String firebaseToken;
  public String cmpyName;
  public Credentials.BillingStatus billingStatus;

  public HomeView withFirebaseURI(String uri) {
    firebaseURI = uri;
    return this;
  }

  public HomeView withFirebaseToken(String token) {
    firebaseToken = token;
    return this;
  }
}
