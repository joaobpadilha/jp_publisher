package com.mapped.publisher.view;

import com.mapped.common.CoreConstants;
import com.mapped.publisher.common.Capability;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.parse.extractor.booking.BookingExtractor;
import models.publisher.TripNote;

import java.io.Serializable;
import java.util.EnumSet;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-14
 * Time: 9:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class NoteBaseView
    extends BaseView {


  //New fields for File Attachements in Structured Notes
  public List<AttachmentView> tripAttachments;
  public List<AttachmentView> files;
  public List<AttachmentView> photos;
  public List<AttachmentView> videos;
  public List<AttachmentView> links;
  public String createdById;
  public Long noteId;
  public String tripStartDatePrint;
  public String linkedTripDetailsId;
  public String startDate;
  public TripNote.NoteType noteType;
  public String detailsId;
  public String tripId;
  public String tripName;

  public List<PassengerInfo> passengers;



  public static class PassengerInfo
          implements Serializable {
    public String name;
    public String firstName;
    public String lastName;
    public String startDate;
    public String endDate;

    public String getFirstName() {
      if (firstName != null) {
        return firstName;
      }
      if (name != null) {
        if (name.contains("/")) {
          return name.substring(name.indexOf("/") + 1).trim();
        } else if (name != null && name.contains(" ")) {
          return name.substring(0, name.indexOf(" ")).trim();
        }
      }

      return "";
    }
    public String getLastName() {
      if (lastName != null) {
        return lastName;
      }
      if (name != null) {
        if (name.contains("/")) {
          return name.substring(0, name.indexOf("/")).trim();
        } else if (name != null && name.contains(" ")) {
          return name.substring(name.indexOf(" ")).trim();
        }
      }
      return "";
    }
  }

}
