package com.mapped.publisher.view;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-04-09
 * Time: 10:16 AM
 * To change this template use File | Settings | File Templates.
 */
public class ImageView
    implements Serializable {
  public Long   id;
  public String url;
  public String licenceUrl;
  public String thumbUrl;
  public String imgSrc;
  public String note;
  public String title;
  public boolean cropped = false;
  public Integer height;
  public Integer width;
  public String  fileName;
}
