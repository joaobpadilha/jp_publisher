package com.mapped.publisher.view;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-06-18
 * Time: 10:04 AM
 * To change this template use File | Settings | File Templates.
 */
public class AgentView extends BaseView {
  public String agentName;
  public String agentCmpy;
  public String agentPhone;
  public String agentMobile;
  public String agentFax;
  public String agentFacebook;
  public String agentTwitter;

  public String agentEmail;
  public String cmpyName;
  public String cmpyPhone;
  public String cmpyEmail;
  public String cmpyWeb;
  public String cmpyLogo;
  public String cmpyFax;

  public String cmpyTag;
  public String cmpyFacebook;
  public String cmpyTwitter;

}
