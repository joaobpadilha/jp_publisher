package com.mapped.publisher.view;

import com.mapped.publisher.common.SecurityMgr;
import models.publisher.UserProfile;

/**
 * Created by surge on 2014-06-06.
 */
public class CollaboratorInviteView
    extends BaseView {
  public UserProfile userProfile;
  public String targetDivId;
  public SecurityMgr.AccessLevel currUserAccessLevel;

}
