package com.mapped.publisher.view;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 *  Class is from the old days of Providers.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PoiAddressView
    implements Serializable {
  public String id;
  public String streetAddr1;
  public String streetAddr2;
  public String city;
  public String zip;
  public String state;

  public String country;
  public String phone;
  public String fax;
  public String web;
  public String email;
  public String facebook;
  public String twitter;
  public String type;
  public String locLong;
  public String locLat;
  public String formatAddr1;
  public String formatAddr2;
}
