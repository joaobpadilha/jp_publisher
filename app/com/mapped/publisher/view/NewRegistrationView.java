package com.mapped.publisher.view;

/**
 * Created by twong on 2014-05-20.
 */
public class NewRegistrationView extends BaseView {
  public String cmpyId;
  public String cmpyName;
  public String firstName;
  public String lastName;
  public String phoneNumber;
  public String email;
  public String authCode;

  public String userId;
  public String pwdResetLink;

  public int tripCount;

}
