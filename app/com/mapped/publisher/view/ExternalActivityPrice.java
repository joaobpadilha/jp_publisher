package com.mapped.publisher.view;

/**
 * Created by twong on 15-06-09.
 */
public class ExternalActivityPrice extends BaseView{
  public String code;
  public float amount;
  public int qty;
  public String name;
  public String description;
}
