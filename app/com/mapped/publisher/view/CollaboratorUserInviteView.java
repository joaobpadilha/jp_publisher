package com.mapped.publisher.view;

import com.mapped.publisher.common.SecurityMgr;

/**
 * Created by surge on 2014-06-06.
 */
public class CollaboratorUserInviteView {
  public String targetDivId;
  public String email;
  public SecurityMgr.AccessLevel currUserAccessLevel;
}
