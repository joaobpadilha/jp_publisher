package com.mapped.publisher.view;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-14
 * Time: 2:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class DashboardView
    extends BaseView {

  public List<TripInfoView> groupPublishedTours;
  public int groupPublishedToursCount;


  public boolean displayCmpyTabs = false;

  public TripsView defaultTrips;
  public TripsView sharedTrips;

  public RecentActiveTripsView recentActiveTrips;

  public String searchTerm;

  public boolean allTrips;
  public boolean searchArchives;


}
