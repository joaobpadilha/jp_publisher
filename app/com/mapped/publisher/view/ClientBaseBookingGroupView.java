package com.mapped.publisher.view;

import com.mapped.publisher.utils.Log;
import com.umapped.persistence.enums.ReservationType;
import controllers.ClientbaseController;
import models.publisher.TripDetail;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import play.libs.F;

/**
 * Created by george on 2018-03-05.
 */
public class ClientBaseBookingGroupView extends TripBookingDetailView {
  public String vendorName;
  public String vendorCode;
  public String resType;
  public String totalPrice;
  public String totalTaxes;
  public String totalCommission;
  public String currency;
  public String comments;
  public String confirmationNo;
  public Long startMS;
  public Long endMS;
  public List<TripBookingDetailView> bookingDetailViews;


  public void populateGroupInfo() {

    try {
      TripDetail tripDetail = null;
      if (bookingDetailViews != null && bookingDetailViews.size() > 0) {
        tripDetail = TripDetail.findByPK(bookingDetailViews.get(0).detailsId);
      }

      this.resType = ClientbaseController.getResType(bookingDetailViews);

      F.Tuple<String, String> resInfo = null;
      if (tripDetail != null) {
        resInfo = ClientbaseController.getReservationProvider(bookingDetailViews.get(0), tripDetail);
      }

      if (this.resType != null && Integer.parseInt(this.resType) == 0){
        TripBookingDetailView tbdv = bookingDetailViews.get(0);
        if (tbdv.detailsTypeId.getRootLevelType() == ReservationType.FLIGHT) {
          if (tbdv.poiMain != null && tbdv.poiMain.getName() != null && !tbdv.poiMain.getName().isEmpty()
              && tbdv.poiMain.getCode() != null && !tbdv.poiMain.getCode().isEmpty()) {
            resInfo = new F.Tuple(tbdv.poiMain.getName(), tbdv.poiMain.getCode());
          }
        }
      }

      this.vendorName = resInfo._1;

      if ((vendorName != null && vendorName.equalsIgnoreCase("UMAPPED")) || (Integer.parseInt(this.resType) == 0)) {
        this.vendorCode = resInfo._2;
      } else {
        this.vendorCode = "";
      }

      this.currency = ClientbaseController.getGroupCurrency(bookingDetailViews);
      this.confirmationNo = ClientbaseController.getGroupConfirmationNo(bookingDetailViews);


      Double total = 0.0;
      Double taxes = 0.0;

      long startMS = 0;
      long endMs = 0;

      DecimalFormat decimalFormat = new DecimalFormat("#.00");

      for (TripBookingDetailView tbdv : bookingDetailViews) {
        try {
          String totalStr = tbdv.getSubtotal();
          if (totalStr != null && !totalStr.isEmpty()) {
            Double dblAmt = Double.parseDouble(ClientbaseController.formatUMPrices(totalStr));
            if (dblAmt != null) {
              total += dblAmt;
            }
          }

          String taxStr = tbdv.getTaxes();
          if (taxStr != null && !taxStr.isEmpty()) {
            Double dblAmt = Double.parseDouble(ClientbaseController.formatUMPrices(taxStr));
            if (dblAmt != null) {
              taxes += dblAmt;
            }
          }
        } catch (Exception e) {
          Log.err("Number format Error while parsing pricing", e);
        }

        //Get Start and End dates
        if (tbdv.startDateMs < startMS || startMS == 0) {
          startMS = tbdv.startDateMs;
        }
        if (tbdv.endDateMs > endMs) {
          endMs = tbdv.endDateMs;
        }
        if (endMs == 0) {
          endMs = startMS;
        }
      }
      if (total > 0.0) {
        this.totalPrice = String.valueOf(decimalFormat.format(total));
      }

      if (taxes > 0.0) {
        this.totalTaxes = String.valueOf(decimalFormat.format(taxes));
      }

      this.startMS = startMS;
      this.endMS = endMs;

    } catch (Exception e) {
      Log.err("PopulateGroupInfo: Error while populating Group fields - ", e);
    }

  }

  public String getVendorName() {
    return vendorName;
  }

  public void setVendorName(String vendorName) {
    this.vendorName = vendorName;
  }

  public String getVendorCode() {
    return vendorCode;
  }

  public void setVendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
  }

  public String getResType() {
    return resType;
  }

  public void setResType(String resType) {
    this.resType = resType;
  }

  public String getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(String totalPrice) {
    this.totalPrice = totalPrice;
  }

  public String getTotalTaxes() {
    return totalTaxes;
  }

  public void setTotalTaxes(String totalTaxes) {
    this.totalTaxes = totalTaxes;
  }

  public String getTotalCommission() {
    return totalCommission;
  }

  public void setTotalCommission(String totalCommission) {
    this.totalCommission = totalCommission;
  }

  @Override
  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public List<TripBookingDetailView> getBookingDetailViews() {
    return bookingDetailViews;
  }

  public void setBookingDetailViews(List<TripBookingDetailView> bookingDetailViews) {
    this.bookingDetailViews = bookingDetailViews;
  }
}
