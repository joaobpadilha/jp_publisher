package com.mapped.publisher.view;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.persistence.PoiRS;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.UmReservation;

import models.publisher.TripDetail;
import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

/**
 * Created by ryan on 15/03/17.
 */
public class UmReservationView extends BaseView
    implements Serializable{

    public UmReservation reservation;
    public ReservationType reservationType;
    public int rank = 0;
    public String linkedTripDetailsId;
    public boolean isMobilized;
    public String loggedInUserId;
    public String landmark;
    public String cmpyId;
    public String tag;
    public Long noteId;
    public String intro;
    public String description;

    public SecurityMgr.AccessLevel accessLevel;
    public boolean isCmpyAdmin;

    public String          detailsId;
    public String          tripId;
    public String          tripName;
    public String          createdById;
    public String          createdByName;
    public ReservationType detailsTypeId;
    public int             tripType;
    public String          bookingNumber;
    public String          startDate;
    public String          startDatePrint;

    public long startDateMs;
    public long endDateMs;
    public long createdTimestampMs;

    public BookingSrc.ImportSrc importSrc;
    public String importSrcId;

    public String recordLocator;
    public int passengerCount;

    public int status;
    public String contact;

    public String providerAddr1;
    public String providerAddr2;
    public String providerPhone;
    public String city;
    public String country;
    public String zip;
    public String state;
    public String departCity;
    public String departCountry;
    public String arriveCity;
    public String arriveCountry;
    public String formattedAddr;

    public static Comparator<UmReservationView> TripBookingDetailViewComparator = new Comparator<UmReservationView>() {
        public int compare(UmReservationView r1, UmReservationView r2) {
            return timeCompare(r1, r2);
        }
    };

    public static  int timeCompare (UmReservationView t1, UmReservationView t2) {
        /*long origT1 = t1.startDateMs;
        long origT2 = t2.startDateMs;

        //if this is a flight that lands before it takes off, then use the landing time so we keep the time chronological
        if (t1.detailsTypeId == ReservationType.FLIGHT && t1.endDateMs > 0 && t1.endDateMs < t1.startDateMs) {
            //set only if the endDateMS is the same day as startStateMs
            String startDate = Utils.formatDateControl(t1.startDateMs);
            String endDate = Utils.formatDateControl(t1.endDateMs);
            if (startDate != null && endDate != null && startDate.equals(endDate)) {
                origT1 = t1.endDateMs;
            }
        }
        if (t2.detailsTypeId == ReservationType.FLIGHT && t2.endDateMs > 0 && t2.endDateMs < t2.startDateMs) {
            //set only if the endDateMS is the same day as startStateMs
            String startDate = Utils.formatDateControl(t1.startDateMs);
            String endDate = Utils.formatDateControl(t1.endDateMs);
            if (startDate != null && endDate != null && startDate.equals(endDate)) {
                origT2 = t2.endDateMs;
            }
        }

        if (origT1 > 0 && (t1.noteId == null || t1.noteId == 0)) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(origT1);
            if (cal.get(Calendar.HOUR_OF_DAY) == 0 && cal.get(Calendar.MINUTE) == 0) {
                cal.set(Calendar.HOUR_OF_DAY, 23);
                cal.set(Calendar.MINUTE, 59);
                origT1 = cal.getTimeInMillis();
            }
        }

        if (origT2 > 0 && (t2.noteId == null || t2.noteId == 0)) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(origT2);
            if (cal.get(Calendar.HOUR_OF_DAY) == 0 && cal.get(Calendar.MINUTE) == 0) {
                cal.set(Calendar.HOUR_OF_DAY, 23);
                cal.set(Calendar.MINUTE, 59);
                origT2 = cal.getTimeInMillis();
            }
        }

        DateTime t1DateTime = new DateTime(origT1);
        DateTime t2DateTime = new DateTime(origT2);
        if (t1DateTime.withTimeAtStartOfDay().getMillis() == t2DateTime.withTimeAtStartOfDay().getMillis()) {
            //we try a sort by rank only for bookings within the same day
            //we exclude the notes since the notes sorting have their own complicated logic
            if((t1.rank > 0 || t2.rank > 0) && (t1.noteId == null || t1.noteId == 0) && (t2.noteId == null || t2.noteId == 0)) {
                if (t1.rank > t2.rank) {
                    return  1;
                } else if (t1.rank < t2.rank) {
                    return -1;
                }
                return 0;
            } else {

                if (origT1 > origT2) {
                    return 1;
                }
                else if (origT1 < origT2) {
                    return -1;
                }
                else {
                    if (t1.rank > t2.rank) {
                        return  1;
                    } else if (t1.rank < t2.rank) {
                        return -1;
                    }
                    return 0;
                }
            }
        } else {
            if (origT1 > origT2) {
                return 1;
            }
            else if (origT1 < origT2) {
                return -1;
            }
            else {
                if (t1.rank > t2.rank) {
                    return  1;
                } else if (t1.rank < t2.rank) {
                    return -1;
                }
                return 0;
            }
        }*/
        return 0;
    }

    public String getTimePrint(String hr, String min){
        try {
            int h = Integer.parseInt(hr);
            int m = Integer.parseInt(min);
            if (!(h == 0 && m == 0)) {
                String hs = hr;
                String ms = min;
                if (h  == 0) {
                    hs  = "00";
                }
                if (m  < 10) {
                    ms  = "0" + ms;
                }
                return hs+":"+ms;
            }
        } catch (Exception e) {

        }
        return "";
    }

    public String getMonthName (String mon) {
        try {
            String[] str = {"JAN",
                    "FEB",
                    "MAR",
                    "APR",
                    "MAY",
                    "JUN",
                    "JUL",
                    "AUG",
                    "SEP",
                    "OCT",
                    "NOV",
                    "DEC"};
            int i = Integer.parseInt(mon);
            if (i >= 0 && i <12) {
                return str[i];
            }

        } catch (Exception e) {

        }
        return "";
    }

    public boolean hasStartTimestamp () {
        if (reservation.getStartDateTime().toString() != null && !reservation.getStartDateTime().toString().isEmpty() && !reservation.getStartDateTime().toString().contains("23:59") && !reservation.getStartDateTime().toString().contains("11:59 PM") && !reservation.getStartDateTime().toString().contains("12:00 AM")) {
            return true;
        }
        return false;
    }

    public static void buildViewInfo(String tripId, TripDetail tripDetail, boolean printDateFormat, SecurityMgr.AccessLevel tripAccessLevel) {

    }
}
