package com.mapped.publisher.view;

import java.util.Calendar;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-14
 * Time: 11:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class TripPreviewView
    extends BaseView {
  public boolean wasPublished;
  public String startYear;
  public String startMonth;
  public String startDay;
  public String startHour;
  public String startMin;
  public String endYear;
  public String endMonth;
  public String endDay;
  public String endHour;
  public String endMin;

  public TripBookingView bookings;
  public DestinationSearchView destinations;
  public List<TripBookingDetailView> bookingsLocations;
  public List<DestinationGuideView> documentLocations;
  public List<GenericTypeView> destinationTypeList;

  public String activeTab;
  public String tripId;
  public String tripStatus;
  public String tripName;
  public String tripStartDatePrint;
  public String tripEndDatePrint;
  public String tripStartDate;
  public String tripEndDate;
  public String tripStartDateMs;
  public String tripEndDateMs;
  public String tripNote;
  public String tripStartDatePrintFull;
  public String tripEndDatePrintFull;
  public boolean isPrinterFriendly;
  public boolean showThumbnails;

  public boolean allowSelfRegistration = false;

  /**
   * Agent and Agency information (with all other brands inside)
   */
  public BusinessCardView businessCard;

  /**
   * Used in PDF views, not in the web itinerary
   */
  public AgentView agent;

  /**
   * Used in PDF views, not in the web itinerary
   */
  public List<TripBrandingView> tripBranding;

  public List<TripPassengerView> passengers;
  public int tripType;
  public boolean onUMapped;
  public List<AttachmentView> tripAttachments;
  public boolean hasEmbeddedTables;


  public String tripCoverUrl;

  public String groupId;
  public boolean displayCover = true;
  public String tripTag = null;
  public String communicatorToken;
  public String communicatorURI;
  public String travellerId;
  public Boolean isModernBrowser = false;


  public void setStartTimestamp(Long ts) {
    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(ts);
    startYear = String.valueOf(cal.get(Calendar.YEAR));
    startMonth = String.valueOf(cal.get(Calendar.MONTH));
    startDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
    startHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
    startMin = String.valueOf(cal.get(Calendar.MINUTE));
  }
}
