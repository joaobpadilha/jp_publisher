package com.mapped.publisher.view;

import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.utils.Log;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.UmReservation;
import com.umapped.persistence.reservation.transfer.UmTransferReservation;
import com.umapped.persistence.reservation.utils.UmReservationUtils;
import models.publisher.TripDetail;

import java.io.Serializable;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

/**
 * Created by ryan on 17/03/17.
 */
public class UmTransferReservationView extends TripBookingDetailView
    implements Serializable{

    public UmTransferReservationView() {
        this.detailsTypeId = ReservationType.TRANSPORT;
    }

    public void buildViewInfo(String tripId, TripDetail tripDetail, boolean printDateFormat, SecurityMgr.AccessLevel tripAccessLevel) {
        super.buildViewInfo(tripId, tripDetail, printDateFormat, tripAccessLevel);

        UmTransferReservation transport = cast(tripDetail.getReservation(), UmTransferReservation.class);
        if (transport != null) {
            if (this.poiMain == null || this.providerName == null) {
                this.providerName = transport.getTransfer().getProvider().name;
                this.name = transport.getTransfer().getProvider().name;
            }
            this.contact = transport.getTransfer().getProvider().contactPoint;
        }
        else {
            Log.err("Database records missing no transport details for trip " + tripId);
            //return null;
        }
    }

    public void buildViewInfo(TripDetail tripDetail, boolean printDateFormat, SecurityMgr.AccessLevel tripAccessLevel) {
        super.buildViewInfo(tripDetail, printDateFormat, tripAccessLevel);
    }

    public String getContact() {
        if(this.reservation != null && UmReservationUtils.cast(this.reservation, UmTransferReservation.class).getTransfer() != null && UmReservationUtils.cast(this.reservation, UmTransferReservation.class).getTransfer().getProvider() != null) {
            return UmReservationUtils.cast(this.reservation, UmTransferReservation.class).getTransfer().getProvider().contactPoint;
        } else {
            return this.contact;
        }
    }
}
