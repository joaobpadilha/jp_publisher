package com.mapped.publisher.view;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-28
 * Time: 12:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class EventPageView extends BaseView implements Serializable {
    public String id;
    public String name;
    public String note;
    public String tag;
    public String pageTag;

    public String startDatePrint;
    public String endDatePrint;
    public String startTimePrint;
    public String endTimePrint;


    public String publisherNote;
    public String locLong;
    public String locLat;
    public List<AttachmentView> photos;
    public List<AttachmentView> audio;
    public List<AttachmentView> videos;
    public  String cover;
    public String address;

    public String userName;
    public String userProfileUrl;

    public Long lastUpdatedTimestamp;
    public Long startTimestamp;

    public boolean moreBtn;


    public static Comparator<EventPageView> EventPageViewComparator = new Comparator<EventPageView>() {

        public int compare(EventPageView t1, EventPageView t2) {
            if ((t1.pageTag == null || t1.pageTag.trim().length() == 0) && (t2.pageTag == null || t2.pageTag.trim().length() == 0))  {
                //no tag - sort by date else sort by lastUpdatedTimestamp
                if (t1.startTimestamp == null && t1.startTimestamp == null) {
                    if (t1.name != null && t2.name != null)
                        return t1.name.compareTo(t2.name);
                    else
                        return t1.lastUpdatedTimestamp.compareTo(t2.lastUpdatedTimestamp);
                }  else if (t1.startTimestamp != null && t2.startTimestamp == null) {
                    return 1;
                }  else if (t1.startTimestamp == null && t2.startTimestamp != null) {
                    return -1;
                }  else
                    return t1.startTimestamp.compareTo(t2.startTimestamp);

            } else if (t1.pageTag != null && t1.pageTag.trim().length() > 0 && (t2.pageTag == null || t2.pageTag.trim().length() == 0))  {
                  return 1;
            } else if ((t1.pageTag == null || t1.pageTag.trim().length() == 0) && t2.pageTag != null && t2.pageTag.trim().length() > 0)  {
                  return -1;
            } else if (t1.pageTag != null && t2.pageTag != null && t1.pageTag.equals(t2.pageTag)) {
                //no tag - sort by date else sort by lastUpdatedTimestamp
                if (t1.startTimestamp == null && t1.startTimestamp == null) {
                    if (t1.name != null && t2.name != null)
                        return t1.name.compareTo(t2.name);
                    else
                        return t1.lastUpdatedTimestamp.compareTo(t2.lastUpdatedTimestamp);
                }  else if (t1.startTimestamp != null && t2.startTimestamp == null) {
                    return 1;
                }  else if (t1.startTimestamp == null && t2.startTimestamp != null) {
                    return -1;
                }  else
                    return t1.startTimestamp.compareTo(t2.startTimestamp);

            } else {
                return t1.pageTag.compareTo(t2.pageTag);
            }
        }

    };

    public static Comparator<EventPageView> EventPageViewComparatorByDate = new Comparator<EventPageView>() {

        public int compare(EventPageView t1, EventPageView t2) {
            if (t1.startTimestamp == null && t1.startTimestamp == null) {
                if (t1.name != null && t2.name != null)
                    return t1.name.compareTo(t2.name);
                else
                    return -1;
            }  else if (t1.startTimestamp != null && t2.startTimestamp == null) {
                return -1;
            }  else if (t1.startTimestamp == null && t2.startTimestamp != null) {
                return 1;
            }  else
                return t1.startTimestamp.compareTo(t2.startTimestamp);
        }

    };

    public static Comparator<EventPageView> EventPageViewDateComparator = new Comparator<EventPageView>() {

        public int compare(EventPageView t1, EventPageView t2) {
            if (t1.startTimestamp == null && t2.startTimestamp == null) {
                if (t1.name != null && t2.name != null)
                    return t1.name.compareTo(t2.name);
                else
                    return t1.lastUpdatedTimestamp.compareTo(t2.lastUpdatedTimestamp);
            } else if (t1.startTimestamp != null && t2.startTimestamp == null) {
                 return 1;
            } else if (t1.startTimestamp == null && t2.startTimestamp != null) {
                 return -1;
            } else {
                return t1.startTimestamp.compareTo(t2.startTimestamp);
            }
        }

    };

}
