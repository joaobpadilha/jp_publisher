package com.mapped.publisher.view;

import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.UmPostalAddress;
import com.umapped.persistence.reservation.UmTraveler;
import com.umapped.persistence.reservation.accommodation.UmAccommodationReservation;
import com.umapped.persistence.reservation.accommodation.UmLodgingBusiness;
import com.umapped.persistence.reservation.flight.UmFlight;
import com.umapped.persistence.reservation.flight.UmFlightReservation;
import com.umapped.persistence.reservation.flight.UmFlightTraveler;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.io.Serializable;
import java.util.*;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;
/**
 * Created by twong on 2014-10-16.
 */
public class EnhancedTripBookingDetailView
    implements Serializable {
  public List<PassengerInfo> passengers;
  public List<CruiseStop>    cruiseStops;
  public String departTerminal = "";
  public String arriveTerminal = "";
  public String amenities      = "";
  public String comments       = "";
  public String providerAddr1;
  public String providerAddr2;
  public String providerPhone;
  public String city;
  public String country;
  public String zip;
  public String state;
  public String numNights;
  public String providerFax;
  public String providerWeb;
  public String duration = "";
  public String aircraft = "";
  public String distance = "";
  public String stops    = "";

  public static class PassengerInfo
      implements Serializable {
    public String name;
    public String firstName;
    public String lastName;
    public String seat;
    public String fClass;
    public String status;
    public String frequentflyer = "N/A";
    public String eticket;
    public String meal = "";

    public String getFirstName() {
      if (firstName != null) {
        return firstName;
      }
      if (name != null) {
        if (name.contains("/")) {
          return name.substring(name.indexOf("/") + 1).trim();
        } else if (name != null && name.contains(" ")) {
          return name.substring(0, name.indexOf(" ")).trim();
        }
      }

      return "";
    }
    public String getLastName() {
      if (lastName != null) {
        return lastName;
      }
      if (name != null) {
        if (name.contains("/")) {
          return name.substring(0, name.indexOf("/")).trim();
        } else if (name != null && name.contains(" ")) {
          return name.substring(name.indexOf(" ")).trim();
        }
      }
      return "";
    }
  }

  public static class CruiseStop
      implements Serializable {
    public String tripDetailId;
    public String day;
    public String date;
    public String datePrint;
    public String arriveTimePrint;
    public String departTimePrint;
    public String itinerary;
    public String arriveTime;
    public String departTime;
    public long arriveTimeMs;
    public long departTimeMs;
    public float  locLat;
    public float  locLong;
    public String portCode;
    public String departCity;
    public String departPort;
    public String arriveCity;
    public String arrivePort;
  }


  public static EnhancedTripBookingDetailView parseFlight (TripBookingDetailView view) {
    EnhancedTripBookingDetailView flightView = new EnhancedTripBookingDetailView();
    
    if (view != null ) {
      UmFlightReservation flight = cast(view.reservation, UmFlightReservation.class);
      if (flight != null) {
        flightView.passengers = getPassengerInfo(view);
  
        UmFlight flt = flight.getFlight();
        flightView.departTerminal = flt.departureTerminal;
        flightView.arriveTerminal = flt.arrivalTerminal;
   
        if (view.comments != null) {
          String s = view.comments.replaceAll("\n", "<br>");
          s = s.replaceAll("<br/>", "<br>");
          String[] tokens = s.split("<br>");
          StringBuilder sb = new StringBuilder();
              
          for (String token: tokens) {
            boolean addToken = true;
            if (token.trim().length() == 0) {
              if (sb.toString().length() > 0) {
                addToken = true;
              } else
                addToken = false;
            } else if (view.comments.contains("Passengers:") && (token.toLowerCase().startsWith("passengers") || token.toLowerCase().startsWith("seat:") || token.toLowerCase().startsWith("name:")
                                                                 || token.toLowerCase().startsWith("class") || token.toLowerCase().startsWith("eticket") || token.toLowerCase().startsWith("frequent") || token.toLowerCase().startsWith("meal"))) {
              addToken = false;
            } else if (token.toLowerCase().startsWith("departure terminal") || token.toLowerCase().startsWith("arrival terminal")) {
              addToken = false;
            }
    
            if (addToken) {
              sb.append(token);sb.append("<br>");
            }
          }
          flightView.comments = sb.toString();
        }
      }
    }
    return flightView;
  }


  public static EnhancedTripBookingDetailView parseHotel (TripBookingDetailView view) {
    EnhancedTripBookingDetailView hotelView = new EnhancedTripBookingDetailView();
    hotelView.city = view.city;
    hotelView.country = view.country;
    hotelView.providerPhone = view.providerPhone;
    hotelView.state = view.state;
    hotelView.providerAddr1 = view.providerAddr1;
    hotelView.providerAddr2 = view.providerAddr2;
    hotelView.zip = view.zip;
    hotelView.providerWeb = view.providerWeb;
    hotelView.comments = view.comments;

    UmAccommodationReservation reservation = cast(view.reservation, UmAccommodationReservation.class);
    
    if (reservation != null) {
      UmLodgingBusiness hotel = reservation.getAccommodation();

      String[] addressLines = hotel.getAddress().toStringTwoLines();
      hotelView.providerAddr1 = StringUtils.trimToEmpty(addressLines[0]);

      hotelView.providerAddr2 = StringUtils.trimToEmpty(addressLines[1]);

      if (StringUtils.isNotEmpty(hotel.url)) {
        hotelView.providerWeb = hotel.url;
      }
      if (StringUtils.isNotEmpty(hotel.telephone)) {
        hotelView.providerPhone = hotel.telephone;
      }
      if (StringUtils.isNotEmpty(hotel.faxNumber)) {
        hotelView.providerFax = hotel.faxNumber;
      }
      if (hotelView.comments != null) {
        String s = hotelView.comments.replaceAll("\n", "<br>");
        s = s.replaceAll("<br/>", "<br>");
        hotelView.comments = s;
      }
    }

    if(view.startDateMs > 1 && view.endDateMs > view.startDateMs) {
      double d = view.endDateMs - view.startDateMs;
      d = d / (24 * 60 * 60 * 1000);
      int days = (int)Math.ceil(d);
      if (days > 1 )
        hotelView.numNights = String.valueOf(days) + " NIGHTS";
      else
        hotelView.numNights = "1 NIGHT";
    }

    return hotelView;
  }

  public static EnhancedTripBookingDetailView parseCruise (TripBookingView bookingView,  TripBookingDetailView view) {
    return parseCruise(bookingView, view, bookingView);


  }

  public static EnhancedTripBookingDetailView parseCruise (TripBookingView bookingView,  TripBookingDetailView view, TripBookingView allBookings) {
    EnhancedTripBookingDetailView cruiseView = new EnhancedTripBookingDetailView();
    cruiseView.cruiseStops = new ArrayList<>();

    if (bookingView != null && bookingView.activities != null && view != null) {
      //extract all cruise stops that belong to this cruise
      for (TripBookingDetailView activity : allBookings.activities) {
        if (activity.detailsTypeId == ReservationType.CRUISE_STOP && activity.startDateMs >= view.startDateMs && activity.startDateMs <= view.endDateMs) {
          EnhancedTripBookingDetailView.CruiseStop stop = new EnhancedTripBookingDetailView.CruiseStop();
          stop.tripDetailId = activity.detailsId;
          stop.arriveTime = activity.startDatePrint;
          if (activity.startTime != null && activity.startTime.trim().length() > 0 && !activity.startTime.trim().equals("12:00 AM")) {
            stop.arriveTime += " - " + activity.startTime;
            stop.arriveTimePrint = activity.startTime;

          }
          stop.date = activity.startDate;
          stop.arriveTimeMs = activity.startDateMs;
          stop.datePrint = activity.startDatePrint;
          if (activity.rank > 0) {
            stop.day = String.valueOf(activity.rank);
          } else {
            stop.day = " -- ";
          }
          stop.itinerary = activity.providerName;
          stop.departTime = activity.endDatePrint;
          stop.departTimeMs = activity.endDateMs;
          if (activity.endTime != null && activity.endTime.trim().length() > 0 && !activity.endTime.trim().equals("12:00 AM")) {
            stop.departTime += " - " + activity.endTime;
            stop.departTimePrint = activity.endTime;

          }
          stop.portCode = activity.arrivalAirportCode;
          cruiseView.cruiseStops.add(stop);

        }
      }
    }

    if (view != null) {
      if(view.startDateMs > 1 && view.endDateMs > view.startDateMs) {
        double d = view.endDateMs - view.startDateMs;
        d = d / (24 * 60 * 60 * 1000);
        int days = (int)Math.ceil(d);
        if (days > 1 )
          cruiseView.numNights = String.valueOf(days + 1) + " Days";
        else
          cruiseView.numNights = "1 Day";
      }
    }

    return cruiseView;


  }

  public static EnhancedTripBookingDetailView parseCruiseCB (TripBookingView bookingView,  TripBookingDetailView view, TripBookingView allBookings) {
    EnhancedTripBookingDetailView cruiseView = new EnhancedTripBookingDetailView();
    cruiseView.cruiseStops = new ArrayList<>();

    if (bookingView != null && bookingView.activities != null && view != null) {
      //extract all cruise stops that belong to this cruise
      String departCity = "";
      String departCode = "";
      if (view.poiStart != null) {
        departCity = view.poiStart.getName();
        departCode = view.poiStart.getCode();
      }
      long departTimestamp = view.startDateMs;
      long arriveTimestamp = view.endDateMs;
      EnhancedTripBookingDetailView.CruiseStop finalStop = null;

      for (TripBookingDetailView activity : allBookings.activities) {
        if (activity.detailsTypeId == ReservationType.CRUISE_STOP  && activity.startDateMs >= view.startDateMs && activity.startDateMs <= view.endDateMs && !activity.name.toLowerCase().contains("day") && !activity.name.toLowerCase().contains("sea")) {
          EnhancedTripBookingDetailView.CruiseStop stop = new EnhancedTripBookingDetailView.CruiseStop();
          stop.tripDetailId = activity.detailsId;
          if (activity.rank > 0) {
            stop.day = String.valueOf(activity.rank);
          } else {
            stop.day = " -- ";
          }
          stop.departTimeMs = departTimestamp;
          stop.arriveTimeMs = activity.startDateMs;
          departTimestamp = activity.endDateMs;
          stop.itinerary = activity.providerName;
          stop.portCode = activity.arrivalAirportCode;
          stop.arrivePort = activity.arrivalAirportCode;
          stop.arriveCity = activity.name;

          stop.departCity = departCity;
          stop.departPort = departCode;

          departCity = activity.name;
          departCode = activity.arrivalAirportCode;

          cruiseView.cruiseStops.add(stop);
          finalStop = stop;

        }
      }

      if (finalStop != null) {

      }
    }

    if (view != null) {
      if(view.startDateMs > 1 && view.endDateMs > view.startDateMs) {
        double d = view.endDateMs - view.startDateMs;
        d = d / (24 * 60 * 60 * 1000);
        int days = (int)Math.ceil(d);
        if (days > 1 )
          cruiseView.numNights = String.valueOf(days) + " Days";
        else
          cruiseView.numNights = "1 Day";
      }
    }

    return cruiseView;


  }


  public static boolean displayDate (TripPreviewView v, String date) {
    int count = 0;

    if (v.bookings != null) {
      for (TripBookingDetailView detail :  v.bookings.chronologicalBookings.get(date)) {
        if (detail.detailsTypeId != ReservationType.CRUISE_STOP || (detail.detailsTypeId == ReservationType.CRUISE_STOP && detail.comments != null && detail.comments.trim().length() > 0)) {
          count++;
        }
      }
    }
    if (count > 0)
      return true;
    else
      return false;
  }


  public static List<EnhancedTripBookingDetailView.PassengerInfo> getPassengerInfo(TripBookingDetailView view) {
    List<EnhancedTripBookingDetailView.PassengerInfo> passengers = new ArrayList<EnhancedTripBookingDetailView.PassengerInfo>();

    UmFlightReservation flight = cast(view.reservation, UmFlightReservation.class);

    if (view != null && flight != null && flight.getTravelers() != null) {

      for (UmTraveler t : flight.getTravelers()) {
        UmFlightTraveler ft = cast(t, UmFlightTraveler.class);
        if (ft != null) {
          EnhancedTripBookingDetailView.PassengerInfo passenger = new PassengerInfo();
          passengers.add(passenger);
          passenger.firstName = ft.getGivenName();
          passenger.lastName = ft.getFamilyName();
          passenger.name = ft.getName();
          passenger.seat = ft.getSeat();
          passenger.fClass = ft.getSeatClass().seatCategory;

          passenger.eticket = ft.getTicketNumber();
          passenger.frequentflyer = ft.getProgram().membershipNumber;
          passenger.meal = ft.getMeal();
        }
      }
    }
    return passengers;
  }

  public static String getValue (String s) {
    if (s != null && s.contains(":")) {
      s = Utils.cleanSpecials(s);
      return s.substring(s.indexOf(":") + 1).trim();
    }
    return "";
  }

  public static void updateChronologicalBookings (TripBookingView m) {
    if (m.chronologicalBookings != null && m.chronologicalDates != null && m.transfers != null) {
      ArrayList<TripBookingDetailView> rentals = new ArrayList<TripBookingDetailView>();

      for (TripBookingDetailView v : m.transfers) {
        if (v.detailsTypeId == ReservationType.CAR_RENTAL && v.startDate != null && v.endDate != null && !v.startDate.equals(v.endDate)) {
          rentals.add(v);
        }
      }

      boolean sortNeeded = false;

      for (TripBookingDetailView v : rentals) {
        String date = Utils.formatDatePrint(v.endDateMs);
        if (m.chronologicalDates.contains(date)) {
          m.chronologicalBookings.get(date).add(v);
          //resort the chronological bookings
          Collections.sort(m.chronologicalBookings.get(date), new Comparator<TripBookingDetailView>() {
            @Override public int compare(TripBookingDetailView o1, TripBookingDetailView o2) {
              long date1 = o1.startDateMs;
              long date2 = o2.startDateMs;
              //figure out if we should use start or end date for comparison
              long diff = date1 - date2;
              if (diff < 0) {
                diff *= -1;
                if (diff > (24 * 60 * 60 * 1000)) {
                  //greater than a day - try again with end date
                  date1 = o1.endDateMs;
                  if (date1 > date2)
                    return 1;
                  else if (date1 < date2)
                    return -1;
                  else
                    return 0;
                }

                return -1;
              }
              else if (diff > 0) {
                if (diff > (24 * 60 * 60 * 1000)) {
                  //greater than a day - try again with end date
                  date2 = o2.endDateMs;
                  if (date1 > date2)
                    return 1;
                  else if (date1 < date2)
                    return -1;
                  else
                    return 0;
                }
                return 1;
              }
              else
                return 0;

            }
          });

        } else {
          //insert new date
          m.chronologicalDates.add(date);
          ArrayList<TripBookingDetailView> newRentals = new ArrayList<TripBookingDetailView>();
          newRentals.add(v);
          m.chronologicalBookings.put(date, newRentals);
          sortNeeded = true;
        }
      }

      if (sortNeeded) {
        Collections.sort(m.chronologicalDates, new Comparator<String>() {
                           @Override public int compare(String o1, String o2) {
                             long sDate = getMillis(o1);
                             long eDate = getMillis(o2);
                             if (sDate > eDate)
                               return 1;
                             else if (sDate < eDate)
                               return -1;
                             else
                               return 0;
                           }
                         }
        );
      }
    }
  }



  public static long getMillis (String s) {
    try {
      String[] formatters = new String[2];
      formatters[0] = "EEE MMM dd, yyyy";

      Date date = DateUtils.parseDate(s, formatters);
      return date.getTime();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return 0;

  }
}
