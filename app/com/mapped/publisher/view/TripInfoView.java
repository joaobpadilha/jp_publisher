package com.mapped.publisher.view;

import java.util.Comparator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-14
 * Time: 9:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class TripInfoView
    extends BaseView {
  public static Comparator<TripInfoView> TripInfoViewNameComparator = (t1, t2) -> {
    if (t1.tripName != null && t2.tripName != null) {
      return t1.tripName.toLowerCase().compareTo(t2.tripName.toLowerCase());
    }
    else if (t1.tripName != null) {
      return 1;
    }
    else if (t2.tripName != null) {
      return -1;
    }
    else {
      return 0;
    }
  };
  public String  tripId;
  public String  tripName;
  public String  tripAgency;
  public String  tripAgencyId;
  public ImageView cover;
  public String  tripStartDate;
  public String  tripStartDatePrint;
  public String  tripEndDate;
  public String  tripEndDatePrint;
  public String  tripNote;
  public Integer tripStatus;
  public int     numberOfBookings;
  public boolean isComplete;
  public String  userId;
  public String  userName;
  public String  userCmpyId;
  public String  userCmpyName;
  public String  userCmpyLogo;
  public String  startYear;
  public String  startMonth;
  public String  startDay;
  public String  endYear;
  public String  endMonth;
  public String  endDay;
  public Long    starttimestamp;
  public String  tripTag;
  public String  travellerId;
  public boolean canEdit;
  public boolean isOwner;
  public String  groupId;
  public String  accessLevelVal;
  public boolean collaborationEnabled;
  public boolean hasCollaboratorEntries = false;
  public boolean hasPublishedGroups;

  public String compositeId;


  //TODO: Replace all fields below with BusinessCardView class
  public String agentName;
  public String agentPhone;
  public String agentEmail;
  public String agentProfilePhoto;

  public String agencyName;
  public String agencyAddress;
  public String agencyPhone;
  public String agencyEmail;
  public String agencyWebsite;
  public String agencyLogo;
  public String agencyTwitter;
  public String agencyFacebook;

  public boolean messengerSubscription;
  /**
   * Flags whether or not there is a control for subscribing/unsubscribing
   */
  public boolean messengerSubscriptionControl = false;

  /**
   *
   */
  public List<TripBrandingView> tripBranding;

  /**
   * Trip recent activity information.
   *
   * @note Remove this field if trip info page no longer features recent activity
   */
  public RecentActivityView rav;

  /**
   * All relevant collaborators information.
   *
   * @note Remove this field if trip info page no longer features collaboration
   */
  public CollaboratorsView collabView;
  public List<TripPassengerInfoView> passengers;

  public boolean autoPickPhoto = false;

}
