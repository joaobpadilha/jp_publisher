package com.mapped.publisher.view;

import models.publisher.ImageLibrary;

import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-04-09
 * Time: 11:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class ImageResultsView
    extends BaseView {
  public String cmpyId;
  public String keyword;

  public String tripId;
  public String templateId;
  public Long tripNoteId;

  public String docId;
  public String pageId;
  public String vendorId;

  public int multipleFiles;

  public ImageLibrary.ImgSrc searchType;
  public List<ImageView> umappedResults;

  public HashMap<String, List<ImageView>> searchResults;
}
