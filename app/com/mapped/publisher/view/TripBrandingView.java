package com.mapped.publisher.view;

import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;

import java.io.Serializable;

/**
 * Created by twong on 2014-07-15.
 */
public class TripBrandingView
    implements Serializable {
  public String cmpyId;
  public String cmpyLogoUrl;
  public String cmpyLogoName;

  public String cmpyName;
  public boolean isEnabled;
  public boolean isMainBrand;
  public boolean isOverwritenByTag;
  public int position;
  public String cmpyEmail;
  public String cmpyPhone;
  public String cmpyWebsite;
  public String cmpyFacebook;
  public String cmpyTwitter;
  public String cmpyTag;
  public String cmpyFax;


  public ObjectNode toJson() {
    ObjectNode classNode = Json.newObject();
    if (cmpyId != null) {
      classNode.put("id", cmpyId);

      if (cmpyLogoName != null) {
        classNode.put("logo", cmpyLogoName);
      }
      if (cmpyLogoUrl != null) {
        classNode.put("logoUrl", cmpyLogoUrl);
      }
      if (cmpyName != null) {
        classNode.put("name", cmpyName);
      }
      classNode.put("position", position);

      if (cmpyEmail != null) {
        classNode.put("email", cmpyEmail);
      }
      if (cmpyPhone != null) {
        classNode.put("phone", cmpyPhone);
      }
      if (cmpyWebsite != null) {
        classNode.put("web", cmpyWebsite);
      }
      if (cmpyFacebook != null) {
        classNode.put("facebook", cmpyFacebook);
      }
      if (cmpyTwitter != null) {
        classNode.put("twitter", cmpyTwitter);
      }
    }
    return classNode;
  }
}
