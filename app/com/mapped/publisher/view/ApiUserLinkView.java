package com.mapped.publisher.view;

/**
 * Created by twong on 15-10-15.
 */
public class ApiUserLinkView {
  public long pk;
  public String srcUserId;
  public String srcName;
  public String cmpyId;
  public String cmpyName;
  public long apiCmpyTokenPK;
  public String externalCmpyName;

}
