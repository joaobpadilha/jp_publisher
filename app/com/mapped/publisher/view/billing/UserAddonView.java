package com.mapped.publisher.view.billing;

import models.publisher.BillingAddonsForUser;
import models.publisher.BillingPlan;

/**
 * Created by surge on 2015-12-24.
 */
public class UserAddonView {
  public BillingPlan          addon;
  public BillingAddonsForUser addonParams;
  public String               startDate;
  public String               cutoffDate;
}
