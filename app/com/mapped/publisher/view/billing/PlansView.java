package com.mapped.publisher.view.billing;

import com.mapped.publisher.view.BaseView;
import models.publisher.BillingPlan;
import models.publisher.BillingSchedule;
import models.publisher.Consortium;
import models.publisher.SysFeature;

import java.util.ArrayList;
import java.util.List;

/**
 * POJO To work with Billing Plans
 * Created by surge on 2015-03-27.
 */
public class PlansView
    extends BaseView {

  public BillingPlan.PlanType  planType;
  public PlanDetails           editable;
  public PlanDetails           parent;
  public List<BillingSchedule> schedules;
  public List<Consortium>      consortiums;
  public List<SysFeature>      availableFeatures;

  public List<PlanDetails> plans;

  public static class PlanDetails {
    public BillingPlan       plan;
    public String            expireDate;
    public List<SysFeature>  features;
    public List<PlanDetails> addons;
    /**
     * Number of users who have this plan assigned
     */
    public int               userCount;
    /**
     * Number of companies who have this plan assigned
     */
    public int               cmpyCount;
    /**
     * In case of addon number of users who enabled it
     */
    public int               usersEnabled;
    /**
     * In case of addon number of user who disabled it
     */
    public int               usersDisabled;

    public PlanDetails() {
      userCount = 0;
      cmpyCount = 0;
      addons = new ArrayList<>();
    }
  }
  public PlansView() {
    plans = new ArrayList<>();
  }
}
