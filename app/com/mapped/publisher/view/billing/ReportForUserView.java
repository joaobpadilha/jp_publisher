package com.mapped.publisher.view.billing;

import models.publisher.BillingSettingsForUser;
import models.publisher.UserProfile;

/**
 * Created by surge on 2015-04-03.
 */
public class ReportForUserView
    extends ReportBaseView {

  public UserProfile            up;
  public BillingSettingsForUser bs;
}
