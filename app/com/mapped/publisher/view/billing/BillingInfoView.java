package com.mapped.publisher.view.billing;

import com.mapped.publisher.utils.Utils;
import models.publisher.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2015-03-30.
 */
public class BillingInfoView {
  public BillingSettingsForCmpy bsCmpy;
  public BillingSettingsForUser bsUser;
  /**
   * Printable billing start date
   */
  public String                 startTs;
  /**
   * Printable billing plan expire date
   */
  public String                 cutoffTs;

  public String billableAgentName;
  public String billableCmpyName;

  public List<BillingPlan>     availablePlans;
  public List<BillingEntity>   availableEntities;
  public List<BillingSchedule> availableSchedules;
  public List<UserAddonView>   addons;

  public BillingPlan defaultPlan;
  public BillingEntity defaultEntity;
  public BillingSchedule defaultSchedule;

  public String defaultStartTs;
  public String defaultCutoffTs;

  public BillingInfoView(BillingSettingsForCmpy bsCmpy) {
    availablePlans = new ArrayList<>(5);
    this.bsCmpy = bsCmpy;
    bsUser = null;
    if (bsCmpy != null) {
      if (bsCmpy.getStartTs() != null) {
        startTs = Utils.getISO8601Date(bsCmpy.getStartTs());
      }
      if (bsCmpy.getCutoffTs() != null) {
        cutoffTs = Utils.getISO8601Date(bsCmpy.getCutoffTs());
      }
    }
    else {
      startTs = "";
      cutoffTs = "";
    }
  }

  public BillingInfoView(BillingSettingsForUser bsUser) {
    this.bsCmpy = null;
    this.bsUser = bsUser;
    availablePlans = new ArrayList<>(5);
    if (bsUser != null) {
      if (bsUser.getStartTs() != null) {
        startTs = Utils.getISO8601Date(bsUser.getStartTs());
      }
      if (bsUser.getCutoffTs() != null) {
        cutoffTs = Utils.getISO8601Date(bsUser.getCutoffTs());
      }
    }
    else {
      startTs = "";
      cutoffTs = "";
    }
  }
}
