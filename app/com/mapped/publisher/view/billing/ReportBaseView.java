package com.mapped.publisher.view.billing;

import com.mapped.publisher.view.BaseView;
import models.publisher.Company;
import org.joda.time.YearMonth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by surge on 2015-12-16.
 */
public class ReportBaseView
    extends BaseView {

  public final static List<String>  MONTHS = Arrays.asList           ("January",
                                                           "February",
                                                           "March",
                                                           "April",
                                                           "May",
                                                           "June",
                                                           "July",
                                                           "August",
                                                           "September",
                                                           "October",
                                                           "November",
                                                           "December");
  public final static List<Integer> YEARS  = Arrays.asList(2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020);

  public Company   cmpy;
  public YearMonth period;
  public YearMonth nextPeriod;
  public long      startTs;
  public long      finishTs;
  public float     addonCost;
  public float     tripsCost;
  public float     usersCost;
  public float     totalCost;
  public String    invGenUrl;

  /**
   * All trips published by users of this company. Both billed
   * and those that are part of the unlimited plan
   */
  public List<PublishedTripView> trips;
  public List<BilledUsersView>   users;

  public ReportBaseView() {
    trips = new ArrayList<>();
    users = new ArrayList<>();
    addonCost = 0;
    tripsCost = 0;
    usersCost = 0;
  }
}
