package com.mapped.publisher.view.billing;

import models.publisher.BillingSettingsForUser;
import models.publisher.Trip;
import models.publisher.TripPublishHistory;
import models.publisher.UserProfile;

/**
 * Trips during the period.
 * Each trip is a Pair of the trip and user profile who created the trip
 *
 * Created by surge on 2015-04-07.
 */
public class PublishedTripView {
  public Trip                   trip;
  public TripPublishHistory     tph;
  public String                 createdDate;
  public String                 publishDate;
  public UserProfile            createdBy;
  public UserProfile            publishBy;
  public BillingSettingsForUser userSettings;
  public float                  cost;
}
