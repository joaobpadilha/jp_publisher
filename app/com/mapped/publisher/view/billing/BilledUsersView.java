package com.mapped.publisher.view.billing;

import models.publisher.BillingSettingsForUser;
import models.publisher.UserProfile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2015-04-07.
 */
public class BilledUsersView {
  public UserProfile            user;
  public BillingSettingsForUser bs;
  public List<BilledAddonView>  addons;
  public float   cost         = 0.0f;
  public float   proRatedCost = 0.0f;
  public boolean isProRated   = false;
  public BilledUsersView() {
    addons = new ArrayList<>(2);
  }
}
