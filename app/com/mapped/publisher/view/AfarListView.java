package com.mapped.publisher.view;

import java.util.List;

/**
 * Created by george on 2017-11-13.
 */
public class AfarListView extends BaseView {
  public String title;
  public Integer listId;
  public String cityName;
  public Integer cityId;
  public String countryName;
  public Integer countryId;
  public String regionName;
  public Integer destId;
  public String categoryType;
  public boolean isCountry;
  public List<AfarPoiView> afarPois;
}
