package com.mapped.publisher.view;

/**
 * Created by ryan on 19/01/17.
 */
public enum FilterType {
    ACTIVE,
    INPROGRESS,
    FUTURE,
    PAST,
    ALL;
}
