package com.mapped.publisher.view;

import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.billing.UserAddonView;
import models.publisher.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2015-12-23.
 */
public class UserBillingView
    extends BaseView {

  public UserProfile            up;
  public String                 fullName;
  public BillingSettingsForUser userBilling;
  public BillingSettingsForCmpy cmpyBilling; //In case company is being charged
  public Company                billedCmpy;
  public BillingSettingsForUser agentBilling; //In case agent is being charged
  public UserProfile            billedAgent;
  public BillingPlan            plan;
  public List<UserAddonView>    addons; //One or many addons

  public UserBillingView() {
    addons = new ArrayList<>();
  }

  public void addAddon(BillingPlan aPlan, BillingAddonsForUser aSettings) {
    UserAddonView uav = new UserAddonView();
    uav.addon = aPlan;
    uav.addonParams = aSettings;
    uav.startDate = Utils.getISO8601Date(aSettings.getStartTs());
    uav.cutoffDate = Utils.getISO8601Date(aSettings.getCutoffTs());
    addons.add(uav);
  }
}
