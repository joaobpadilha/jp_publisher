package com.mapped.publisher.view;

import java.util.List;

/**
 * Created by twong on 15-10-11.
 */
public class TourSearchView
    extends BaseView {
  public List<TemplateBookingView> tours;
  public String tripId;
  public String brand;
  public String brandUrl;
  public int srcId;
}
