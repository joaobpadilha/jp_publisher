package com.mapped.publisher.view;

import java.util.Map;

/**
 * Base view for POI screens
 * Information in this class should eventually be delivered over API
 */
public class PoiBaseView
    extends BaseView {
  public Map<Integer, String> userCompanies;
  public String editorId;
}
