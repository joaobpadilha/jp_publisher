package com.mapped.publisher.view;

import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.utils.Log;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.activity.UmActivityReservation;
import com.umapped.persistence.reservation.utils.UmReservationUtils;
import models.publisher.Destination;
import models.publisher.TripDetail;
import scala.concurrent.java8.FuturesConvertersImpl;

import java.io.Serializable;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

/**
 * Created by ryan on 17/03/17.
 */
public class UmActivityReservationView extends TripBookingDetailView
        implements Serializable{

    public UmActivityReservationView() {
        this.detailsTypeId = ReservationType.ACTIVITY;
    }

    public void buildViewInfo(String tripId, TripDetail tripDetail, boolean printDateFormat, SecurityMgr.AccessLevel tripAccessLevel) {
        super.buildViewInfo(tripId, tripDetail, printDateFormat, tripAccessLevel);

        String hostUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);
        UmActivityReservation activity = cast(tripDetail.getReservation(), UmActivityReservation.class);
        if (activity != null) {
            if (this.poiMain == null || this.providerName == null) {
                this.providerName = activity.getActivity().name;
                this.name = activity.getActivity().name;
            }


            this.contact = activity.getActivity().getOrganizedBy().contactPoint;
            if (activity.getActivity().umDocumentId != null) {
                Destination destination = Destination.find.byId(activity.getActivity().umDocumentId);
                this.destination = destination.getName();
                this.destinationId = destination.destinationid;
                this.destinationUrl = hostUrl;
                //this.destinationUrl = hostUrl + routes.DestinationController.loadPdf() + "?inDestId=" + destination.destinationid + "&inTripId=" + tripId ;
            }
            else {
                this.destination = "";
                this.destinationId = "";
                this.destinationUrl = "";
            }
            if (poiStart != null && poiStart.getCode() != null) {
                this.arrivalAirportCode = poiStart.getCode();
            }
        }
        else {
            Log.err("Database records missing no activity details for trip " + tripId);
            //return null;
        }
    }

    public void buildViewInfo(TripDetail tripDetail, boolean printDateFormat, SecurityMgr.AccessLevel tripAccessLevel) {
        super.buildViewInfo(tripDetail, printDateFormat, tripAccessLevel);
    }

    public String getContact() {
        if(this.reservation != null && UmReservationUtils.cast(this.reservation, UmActivityReservation.class).getActivity() != null && UmReservationUtils.cast(this.reservation, UmActivityReservation.class).getActivity().getOrganizedBy() != null) {
            return UmReservationUtils.cast(this.reservation, UmActivityReservation.class).getActivity().getOrganizedBy().contactPoint;
        } else {
            return this.contact;
        }
    }

    public String getDestinationId() {
        if (this.reservation != null && UmReservationUtils.cast(this.reservation, UmActivityReservation.class).getActivity() != null) {
            return UmReservationUtils.cast(this.reservation, UmActivityReservation.class).getActivity().umDocumentId;
        } else {
            return this.destinationId;
        }
    }

    public String getDestinationUrl() {
        if(this.reservation != null && UmReservationUtils.cast(this.reservation, UmActivityReservation.class).getActivity() != null && UmReservationUtils.cast(this.reservation, UmActivityReservation.class).getActivity().getOrganizedBy() != null) {
            return UmReservationUtils.cast(this.reservation, UmActivityReservation.class).getActivity().getOrganizedBy().url;
        } else {
            return this.destinationUrl;
        }
    }
}
