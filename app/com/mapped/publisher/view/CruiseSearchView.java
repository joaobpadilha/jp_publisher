package com.mapped.publisher.view;

import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.TripBookingView;

import java.util.List;

/**
 * Created by twong on 15-05-29.
 */
public class CruiseSearchView extends BaseView {
  public List<TripBookingView> cruises;
  public String tripId;
}
