package com.mapped.publisher.view;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-20
 * Time: 12:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class ActivityStatsView {
    public int numTripsPublished;
    public int numTripsPublishedActivity;

    public int numTripsCreated;
    public int numTripsPending;
    public int numTripsDeleted;

    public int numToursCreated;
    public int numToursPending;
    public int numToursDeleted;

    public int numPoiCreated;
    public int numPoiModified;
    public int numPoiDeleted;

    public int numGuidesCreated;
    public int numGuidesDeleted;
    public int numGuidesPagesCreated;
    public int numGuidesPagesModified;
    public int numGuidesPagesDeleted;

    public int numActiveCmpy;

    public int numNewCmpy;
    public int numDeletedCmpy;
    public int numNewUsers;
    public int numActiveUsers;

    public int numDeletedUsers;
    public int numInactiveUsers;

    public List<UserStatsView> inactiveUsers;
    public List<UserStatsView> neverLoggedInUsers;
    public List<CmpyStatsView> declinedUserAgreement;
    public List<CmpyStatsView> noAcceptedUserAgreement;

    public int numLogons;
    public int numFailedLogons;
    public int numPasswordReset;

    public Map<String, String> topFailedLogons;
    public Map<String, String> topPwdReset;

  public List<CmpyInfoView> newSelfSignupCmpies;
  public  List<CmpyInfoView> newCmpies;

  public int numCollabInvites;
  public int numCollabAccepted;

}
