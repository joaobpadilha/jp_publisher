/****************
@author  $Author:   mminhas  $, Toronto Stock Exchange. 
@version $Id: Log.java,v 1.11 2001/10/30 15:02:52 fhirsh Exp $
<PRE>
MAINTENANCE LOG
===============
$Log:   Y:/New Archives/TSE Applications/Web Systems/Web Framework/source/com/tse/common/logging/Log.v_va  $
   
      Rev 1.0   Mar 09 2002 10:18:10   mminhas
   Initial revision.
   
      Rev 1.1   Jan 17 2002 17:43:24   mminhas
   Second revision
Revision 1.11  2001/10/30 15:02:52  fhirsh
Adding CVS Headers to all files from Perl script

</PRE>
**/

package com.mapped.publisher.utils;

/**
 *
 * @author  mminhas
 * @version
 */
public interface LogInterface {

  /** Fatal error log level = 1 */
  public static final int FATAL = 1 << 0;
  
  /** Error log level = 2 */
  public static final int ERROR = 1 << 1;
  
  /** Warning log level = 4 */
  public static final int WARN = 1 << 2;
  
  /** Debug log level = 8 */
  public static final int INFO = 1 << 3;
  
  /** Trace log level = 16 */
  public static final int DEBUG = 1 << 4;
  

  public void debug(String methodName, String msg);


  public void debug(String methodName, String msg, Throwable t);


  public void info(String methodName, String msg);


  public void info(String methodName, String msg, Throwable t);


  public void warn(String methodName, String msg);


  public void warn(String methodName, String msg, Throwable t);


  public void error(String methodName, String msg);


  public void error(String methodName, String msg, Throwable t);


  public void fatal(String methodName, String msg);


  public void fatal(String methodName, String msg, Throwable t);

}
