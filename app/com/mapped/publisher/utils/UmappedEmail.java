package com.mapped.publisher.utils;

import com.mapped.common.CoreConstants;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.ConfigMgr;
import controllers.EmailController;
import models.publisher.EmailLog;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailConstants;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import com.avaje.ebean.Ebean;

import javax.mail.internet.InternetAddress;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Extending excellent Email API from Apache Commons
 * <p>
 * https://commons.apache.org/proper/commons-email/javadocs/api-release/index.html
 * Created by surge on 2016-02-04.
 */
public class UmappedEmail
    extends HtmlEmail {

  public enum SubjectType {
    DIRECT,
    FORWARD,
    REPLY
  }

  SubjectType type;
  boolean     enabled;
  String      hostUrl;
  String      fromName;
  String      fromEmail;
  String      toEmail;
  List<String> toList;
  String      subject;
  String      sentEmailId;
  String      via = " via Umapped";
  EmailLog.EmailTypes emailType;
  long        accountUId;
  String      tripId;
  long      messageId;
  String      uniqueArgs;

  String htmlMsg = null;



  public UmappedEmail() {
    enabled = true;
    fromName = null;
    fromEmail = null;
    type = SubjectType.DIRECT;
  }

  /**
   * Preconfigured Umapped Email with most common parameters
   *
   * @return
   */
  public static UmappedEmail buildDefault() {
    String       smtp = ConfigMgr.getAppParameter(CoreConstants.EMAIL_SMTP_HOST);
    String       portStr = ConfigMgr.getAppParameter(CoreConstants.EMAIL_SMTP_PORT);
    int port = Integer.parseInt(portStr);

    String user = ConfigMgr.getAppParameter(CoreConstants.EMAIL_SMTP_USER_ID);
    String pwd  = ConfigMgr.getAppParameter(CoreConstants.EMAIL_SMTP_PASSWORD);

    String fromEmail = ConfigMgr.getAppParameter(CoreConstants.EMAIL_TRIP_NOTIFICATION_FROM);


    return UmappedEmail.build(smtp, user, pwd, port, fromEmail);
  }

  public static UmappedEmail build(String smtp, String user, String pwd, int port, String fromEmail ) {
    UmappedEmail ue   = new UmappedEmail();

    ue.setCharset(EmailConstants.UTF_8);

    ue.setHostName(smtp);

    if (port < 1) {
      ue.setSmtpPort(587);
    } else {
      ue.setSmtpPort(port);
    }
    ue.setAuthentication(user, pwd);

    String emailEnabled = ConfigMgr.getAppParameter(CoreConstants.EMAIL_ENABLED);
    ue.enabled = emailEnabled.equalsIgnoreCase("y");

    ue.hostUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);

    ue.fromEmail = fromEmail;
    try {
      ue.withFromEmail(fromEmail);
    } catch (Exception e) {

    }
    ue.fromName = "Umapped";

    return ue;
  }

  public String getHostUrl() {
    return hostUrl;
  }

  public void setHostUrl(String hostUrl) {
    this.hostUrl = hostUrl;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public SubjectType getType() {
    return type;
  }

  public void setType(SubjectType type) {
    this.type = type;
  }

  public EmailLog.EmailTypes getEmailType() {
    return emailType;
  }

  public void setEmailType(EmailLog.EmailTypes emailType) {
    this.emailType = emailType;
  }

  public long getAccountUId() {
    return accountUId;
  }

  public void setAccountUId(long accountUId) {
    this.accountUId = accountUId;
  }

  public String getToEmail() {
    return toEmail;
  }

  public void setToEmail(String toEmail) {
    this.toEmail = toEmail;
  }

  public List<String> getToList() {
    return toList;
  }

  public void setToEmail(List<String> toList) {
    this.toList = toList;
  }

  /**
   * Enables or disables sending functionality
   *
   * @return
   */
  public UmappedEmail enable(boolean enabled) {
    this.enabled = enabled;
    return this;
  }

  public UmappedEmail withType(SubjectType type) {
    this.type = type;
    return this;
  }

  public UmappedEmail withSubject(String subject) {
    setSubject(subject);
    return this;
  }

  public UmappedEmail withEmailType(EmailLog.EmailTypes emailType) {
    this.emailType = emailType;
    return this;
  }

  public UmappedEmail withAccountUid(long uid) {
    this.accountUId = uid;
    return this;
  }

  public UmappedEmail withTripId (String tripId) {
    this.tripId = tripId;
    return this;
  }

  public UmappedEmail withToEmail (String email) {
    this.toEmail = email;
    return this;
  }

  public UmappedEmail withToList (List<String> emails) {
    this.toList = emails;
    return this;
  }


  public String buildAndSend()
      throws EmailException {
    if(!enabled) {
      throw new EmailException("Email Functionality Disabled");
    }

    switch (type) {
      case FORWARD:
        setSubject("Fw: " + getSubject());
        break;
      case REPLY:
        setSubject("Re:" + getSubject());
        break;
    }

    messageId = DBConnectionMgr.getUniqueLongId();

    uniqueArgs = "{\"unique_args\": {\"msg_id\": "+messageId+"}}";
    addHeader("X-SMTPAPI", uniqueArgs);

    if (this.toList != null) {
      for (String email : this.toList) {
        logEmail(email);
      }
    } else if (this.toEmail != null) {
      logEmail(this.toEmail);
    }

    try {
      sentEmailId = super.send();
    }
    catch (IllegalStateException e) {
      sentEmailId = super.sendMimeMessage(); //Someone already built a message let them send it anyway
    }


    return sentEmailId;
  }

  private void logEmail (String email) {
    try {
      //Add emailLog to table
      EmailLog emailLog = new EmailLog();
      emailLog.setPk(messageId);
      if (this.accountUId > -1) {
        emailLog.setAccount_uid(this.accountUId);
      }
      emailLog.setModified_ts(new Timestamp(System.currentTimeMillis()));
      emailLog.setSent_ts(new Timestamp(System.currentTimeMillis()));
      emailLog.setStatus(EmailLog.EmailStatus.SENT);
      emailLog.setSubject(getSubject());
      emailLog.setTo_email(email);
      emailLog.setType(this.emailType);
      if (this.tripId != null) {
        emailLog.setMeta(this.tripId);
      }
      if (emailLog.getType() == EmailLog.EmailTypes.TRAVELLER_ITINERARY && htmlMsg != null && !htmlMsg.isEmpty() && this.tripId != null) {
        try {
          //save the email as an archive in the PDF directory for this trip using the msgid as the key for later retrieval
          //save both the html body and raw email
          S3Util.saveHtml(htmlMsg.getBytes(), EmailController.getTripEmailFilename(emailLog));
          emailLog.setArchive(true);
        } catch (Exception e1) {

        }
      }


      emailLog.save();

    } catch (Exception e) {
      Log.err("Cannot Log Email: " + email + " for " + this.getSubject() + " - " + this.tripId);

    }
  }
  public UmappedEmail withViaFromName(String name)
      throws EmailException {
    return withFromName(name + via);
  }

  public UmappedEmail withFromName(String name)
      throws EmailException {
    this.fromName = name;

    try {
      if (fromEmail != null) {
        setFrom(fromEmail, fromName);
      }
    }
    catch (EmailException ee) {
      Log.err("UmappedEmail: Attempt to send message to corrupted email: " + name);
      throw ee;
    }
    return this;
  }

  public UmappedEmail withFromEmail(String email)
      throws EmailException {
    this.fromEmail = email;
    try {
      setFrom(fromEmail, fromName);
    }
    catch (EmailException ee) {
      Log.err("UmappedEmail: Attempt to send message to corrupted email: " + email);
      throw ee;
    }

    return this;
  }

  public UmappedEmail withHtml(String html)
      throws EmailException {
    try {
      htmlMsg = html;
      setHtmlMsg(html);
    }
    catch (EmailException ee) {
      Log.err("UmappedEmail: failed to set HTML body", ee);
      throw ee;
    }
    return this;
  }

  public UmappedEmail withText(String text)
      throws EmailException {
    try {
      setTextMsg(text);
    }
    catch (EmailException ee) {
      Log.err("UmappedEmai: failed to set Text body", ee);
      throw ee;
    }
    return this;
  }

  private void buildAddressString(Collection<InternetAddress> addresses, StringBuilder sb) {
    Iterator<InternetAddress> it = addresses.iterator();
    while(it.hasNext()) {
      InternetAddress currAddr = it.next();
      sb.append(currAddr.getAddress());
      if(it.hasNext()) {
        sb.append(",");
      }
    }
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();

    sb.append("To: ");
    buildAddressString(getToAddresses(), sb);
    sb.append(System.lineSeparator());
    sb.append("Cc: ");
    buildAddressString(getCcAddresses(), sb);
    sb.append(System.lineSeparator());
    sb.append("Bcc: ");
    buildAddressString(getBccAddresses(), sb);
    sb.append(System.lineSeparator());
    sb.append("Type: ");
    sb.append(getType().name());
    sb.append(System.lineSeparator());
    sb.append("Subject: ");
    sb.append(getSubject());
    sb.append(System.lineSeparator());

    sb.append("From: ");
    sb.append(getFromAddress().getAddress());
    sb.append(System.lineSeparator());

    if(getReplyToAddresses() != null) {
      sb.append("ReplyTo: ");
      buildAddressString(getReplyToAddresses(), sb);
      sb.append(System.lineSeparator());
    }

    if(text != null) {
      sb.append("Text: ");
      sb.append(text.trim().substring(0, 100));
      sb.append(System.lineSeparator());
    }


    if(html != null) {
      sb.append("Html: ");
      sb.append(html.trim().substring(0, 100));
      sb.append(System.lineSeparator());
    }

    return sb.toString();
  }



  public UmappedEmail setVia(String via) {
    this.via = via;
    return this;
  }

  public UmappedEmail addAttachment (String name, String description, URL url) throws EmailException {
    if (name != null && !name.isEmpty() && url != null) {
      EmailAttachment attachment = new EmailAttachment();
      attachment.setURL(url);
      attachment.setName(name);
      attachment.setDescription(description);
      attach(attachment);
    }
    return this;
  }
}
