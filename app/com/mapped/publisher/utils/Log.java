package com.mapped.publisher.utils;

import play.Logger;

import java.text.SimpleDateFormat;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 12-07-22
 * Time: 11:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class Log {
  public static void err(String loc, String msg) {
    log(LogLevel.ERROR, loc, msg);
  }

  public static void err(String desc, Throwable th) {
    StringBuilder sb = new StringBuilder(desc);
    sb.append('\n');
    sb.append(th.getMessage());
    sb.append('\n');
    for(StackTraceElement el : th.getStackTrace()) {
      sb.append(el.toString());
      sb.append('\n');
    }
    log(LogLevel.ERROR, sb.toString());
  }

  //wrapper around default play logger for future implementation of distributed async logging
  public static void log(LogLevel level, String loc, String msg) {
    String timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").format(System.currentTimeMillis());
    StringBuilder sb = new StringBuilder();
    sb.append("[");
    sb.append(timestamp);
    sb.append("] ");
    if (loc != null) {
      sb.append(loc);
    }
    if (msg != null) {
      sb.append(" " + msg);
    }

    switch (level) {
      case ERROR:
        Logger.error(sb.toString());
        break;
      case INFO:
        Logger.info(sb.toString());
        break;
      case DEBUG:
        Logger.debug(sb.toString());
        break;
    }
  }

  public static void info(String loc, String msg) {
    log(LogLevel.INFO, loc, msg);
  }

  public static void debug(String loc, String msg) {
    log(LogLevel.DEBUG, loc, msg);
  }

  public static void err(String msg) {
    log(LogLevel.ERROR, null, msg);
  }

  public static void info(String msg) {
    log(LogLevel.INFO, null, msg);
  }

  public static void debug(String msg) {
    log(LogLevel.DEBUG, null, msg);
  }

  public static void log(LogLevel level, String msg) {
    Log.log(level, null, msg);
  }

  public static void log(LogLevel level, String method, Throwable e) {
    Log.log(level, method, e.getMessage());
  }
}
