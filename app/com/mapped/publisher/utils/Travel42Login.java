package com.mapped.publisher.utils;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by twong on 2015-01-16.
 */
public class Travel42Login {
  public static Map<String, String> cookies = null;

  public static void loginJsoup() {
    try {
      if (cookies == null) {
        System.out.println("---- Doing Login");
        Connection.Response res = Jsoup
            .connect("http://www.travel-42.com")
            .data("__LASTFOCUS", "","__VIEWSTATE", "/wEPDwUJMTYxMTkzMTY3D2QWAmYPZBYEAgEPZBYCAg0PFgIeBFRleHQFOzxsaW5rIHJlbD0naWNvbicgaHJlZj0nL2Zhdmljb24uaWNvJyB0eXBlPSdpbWFnZS94LWljb24nIC8+ZAIDD2QWAgIDD2QWBgIFDw8WAh4HVmlzaWJsZWdkZAINDxYCHgVjbGFzcwUIY29udGVudDIWAgIBD2QWAgIBD2QWBGYPFgIfAWhkAgIPZBYCAgEPZBYCAgEPZBYCZg9kFgwCAQ9kFgRmDw8WAh8BaGRkAgIPDxYCHwFoZGQCAw9kFgRmDw8WAh8BaGRkAgIPD2QWAh4Fc3R5bGUFE21hcmdpbi1ib3R0b206MTRweDsWBgIBDw8WAh8BaGRkAgMPDxYCHwFoZGQCBQ9kFgICAQ8PFgIfAWhkZAIFD2QWAgICD2QWAgIBD2QWAgIBD2QWAmYPZBYCAgEPZBYEZg8PFgIfAWhkZAICD2QWBAIBDw8WAh8BaGRkAgMPDxYCHwFoZGQCCA9kFgRmDw8WAh8BaGRkAgIPD2QWAh8DBRNtYXJnaW4tYm90dG9tOjE0cHg7FgYCAQ8PFgIfAWhkZAIDDw8WAh8BaGRkAgUPZBYCAgEPDxYCHwFoZGQCCg9kFgRmDw8WAh8BaGRkAgIPD2QWAh8DBRNtYXJnaW4tYm90dG9tOjE0cHg7FgYCAQ8PFgIfAWhkZAIDDw8WAh8BaGRkAgUPZBYCAgEPDxYCHwFoZGQCDA9kFgRmDw8WAh8BaGRkAgIPZBYGAgEPDxYCHwFoZGQCAw8PFgIfAWhkZAIFD2QWAgIBDw8WAh8BaGRkAhEPDxYCHwFoZGQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgIFFWN0bDAwJExvZ2luMSRidG5Mb2dpbgUYY3RsMDAkTG9naW4xJGNoa1JlbWVtYmVyMTkgJLu2o393VRayJ0vdGwiHGEw=", "__EVENTTARGET", "","__EVENTARGUMENT", "","__VIEWSTATEGENERATOR", "CA0B0334","ctl00$Login1$txtUserName", "lisa@umapped.com","ctl00$Login1$txtPassword", "umap2connect","ctl00$Login1$btnLogin.x", "31","ctl00$Login1$btnLogin.y", "13")
            .method(Connection.Method.POST)
            .execute();

        if (res != null) {
          cookies = res.cookies();
        }
      }

      //get JSON
      String json = "{\"Req\":{\"ReqType\":\"ALL\",\"FolderID\":\"\",\"ListType\":\"T\",\"Value\":\"\"," +
                    "\"Sort\":\"CREATE\",\"GetShared\":false,\"SortOrder\":\"D\",\"Page\":\"1\"}}";


      Connection.Response res = Jsoup
          .connect("http://www.travel-42.com")
          .header("Content-type", "application/json")
          .data("data",json)
          .cookies(cookies)
          .method(Connection.Method.POST)
          .execute();

      if (res != null) {
        System.out.println("--- " + res.toString());
      }


    }catch (Exception e) {
e.printStackTrace();
    }

  }


    public static void login() {

    try {

      HttpClient c = HttpClientBuilder.create().build();

      //Login
      HttpPost p = new HttpPost("http://www.travel-42.com");     //use service id in the future

      List<NameValuePair> nvps = new ArrayList<NameValuePair>();
      nvps.add(new BasicNameValuePair("__LASTFOCUS", ""));
      nvps.add(new BasicNameValuePair("__VIEWSTATE", "/wEPDwUJMTYxMTkzMTY3D2QWAmYPZBYEAgEPZBYCAg0PFgIeBFRleHQFOzxsaW5rIHJlbD0naWNvbicgaHJlZj0nL2Zhdmljb24uaWNvJyB0eXBlPSdpbWFnZS94LWljb24nIC8+ZAIDD2QWAgIDD2QWBgIFDw8WAh4HVmlzaWJsZWdkZAINDxYCHgVjbGFzcwUIY29udGVudDIWAgIBD2QWAgIBD2QWBGYPFgIfAWhkAgIPZBYCAgEPZBYCAgEPZBYCZg9kFgwCAQ9kFgRmDw8WAh8BaGRkAgIPDxYCHwFoZGQCAw9kFgRmDw8WAh8BaGRkAgIPD2QWAh4Fc3R5bGUFE21hcmdpbi1ib3R0b206MTRweDsWBgIBDw8WAh8BaGRkAgMPDxYCHwFoZGQCBQ9kFgICAQ8PFgIfAWhkZAIFD2QWAgICD2QWAgIBD2QWAgIBD2QWAmYPZBYCAgEPZBYEZg8PFgIfAWhkZAICD2QWBAIBDw8WAh8BaGRkAgMPDxYCHwFoZGQCCA9kFgRmDw8WAh8BaGRkAgIPD2QWAh8DBRNtYXJnaW4tYm90dG9tOjE0cHg7FgYCAQ8PFgIfAWhkZAIDDw8WAh8BaGRkAgUPZBYCAgEPDxYCHwFoZGQCCg9kFgRmDw8WAh8BaGRkAgIPD2QWAh8DBRNtYXJnaW4tYm90dG9tOjE0cHg7FgYCAQ8PFgIfAWhkZAIDDw8WAh8BaGRkAgUPZBYCAgEPDxYCHwFoZGQCDA9kFgRmDw8WAh8BaGRkAgIPZBYGAgEPDxYCHwFoZGQCAw8PFgIfAWhkZAIFD2QWAgIBDw8WAh8BaGRkAhEPDxYCHwFoZGQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgIFFWN0bDAwJExvZ2luMSRidG5Mb2dpbgUYY3RsMDAkTG9naW4xJGNoa1JlbWVtYmVyMTkgJLu2o393VRayJ0vdGwiHGEw="));
      nvps.add(new BasicNameValuePair("__EVENTTARGET", ""));
      nvps.add(new BasicNameValuePair("__EVENTARGUMENT", ""));
      nvps.add(new BasicNameValuePair("__VIEWSTATEGENERATOR", "CA0B0334"));
      nvps.add(new BasicNameValuePair("ctl00$Login1$txtUserName", "lisa@umapped.com"));
      nvps.add(new BasicNameValuePair("ctl00$Login1$txtPassword", "umap2connect"));
      nvps.add(new BasicNameValuePair("ctl00$Login1$btnLogin.x", "31"));
      nvps.add(new BasicNameValuePair("ctl00$Login1$btnLogin.y", "13"));


      p.setEntity(new UrlEncodedFormEntity(nvps));



      HttpResponse r = c.execute(p);

      if (r != null && r.getStatusLine() != null && r.getStatusLine().getStatusCode() == 200) {
        String resp = EntityUtils.toString(r.getEntity());

        System.out.println(resp);
      }
      if (r != null) {
        String resp = EntityUtils.toString(r.getEntity());

        System.out.println(resp);

        Header[] h = r.getAllHeaders();
        if (h != null && h.length > 0) {
          for (Header h1: h) {
            System.out.println(h1.getName() + " :: " + h1.getValue());
          }
        }
      }



      HttpPost g = new HttpPost("http://www.travel-42.com/Dyna.asmx/GetReports");


      //Json to get list of all trips
      String json = "{\"Req\":{\"ReqType\":\"ALL\",\"FolderID\":\"\",\"ListType\":\"T\",\"Value\":\"\"," +
                    "\"Sort\":\"CREATE\",\"GetShared\":false,\"SortOrder\":\"D\",\"Page\":\"1\"}}";


      g.setEntity(new ByteArrayEntity(json.getBytes("UTF-8")));
      g.setHeader("Content-type", "application/json");

       r = c.execute(g);

      System.out.println("\n\n\n\n JSON Response: \n\n");

      if (r != null && r.getStatusLine() != null && r.getStatusLine().getStatusCode() == 200) {
        String resp = EntityUtils.toString(r.getEntity());

        System.out.println(resp);
      }
      if (r != null) {
        String resp = null;
        try {
          resp = EntityUtils.toString(r.getEntity());

          System.out.println(resp);
        } catch (Exception e) {
          e.printStackTrace();
        }

      }

    } catch (Exception e) {
      e.printStackTrace();
    }

  }
}
