/****************
@author  $Author:   mminhas  $, Toronto Stock Exchange.
@version $Id: LogFactory.java,v 1.3 2001/10/30 15:02:52 fhirsh Exp $
<PRE>
MAINTENANCE LOG
===============
$Log:   Y:/New Archives/TSE Applications/Web Systems/Web Framework/source/com/tse/common/logging/LogFactory.v_va  $

      Rev 1.0   Mar 09 2002 10:18:10   mminhas
   Initial revision.

      Rev 1.3   Feb 07 2002 11:50:36   twong
   removed hardcoded log settings

      Rev 1.2   Jan 31 2002 09:56:24   oopris
   comment System.out.println()

      Rev 1.1   Jan 17 2002 17:43:24   mminhas
   Second revision
Revision 1.3  2001/10/30 15:02:52  fhirsh
Adding CVS Headers to all files from Perl script

</PRE>
**/

/*
 * Log.java
 * Created on June 22, 2001
 */

package com.mapped.publisher.utils;

import com.mapped.publisher.common.ConfigMgr;

/**
 *
 * @author  mminhas
 * @version
 */
public class LogFactory {

  private static final int LOG4J = 0;
  private int logType = LOG4J;

  /**
   * Create a default logger.
   *
   * @param   appName     Application name
   * @param   configMgr   Configuration manager EJB
   * @param   className   The caller's class name
   * @return  Log         Logger
   */
  public static LogInterface createLogger(String appName, ConfigMgr configMgr, String className) {
    return createLog4J(appName, configMgr, className);
  }


  /**
   * Create a logger based on the logType.
   *
   * @param   appName     Application name
   * @param   configMgr   Configuration manager EJB
   * @param   className   The caller's class name
   * @param   logType     The type of logger to create (eg, Log4J, JMS)
   * @return  Log         Logger requested in <code>logType</code> or the default
   *                      logger if the <code>logType</code> isn't found
   */
  public static LogInterface createLogger(String appName, ConfigMgr configMgr, String className, int logType) {
    switch(logType) {
      case LOG4J:
        return createLog4J(appName, configMgr, className);
    }
    // create the default logger
    return createLogger(appName, configMgr, className);
  }


  /**
   * Create a default logger.
   *
   * @param   logSettings Log settings as a name/value pair list delimited by ';' characters
   * @param   logLevel    Log level
   * @param   className   The caller's class name
   * @return  Log         Logger or null of the config manager is not found
   */
  public static LogInterface createLogger(String logSettings, int logLevel, String className) {
    return new Log4J(logSettings, logLevel, className);
  }

  /**
   * Create a Log4J logger.
   *
   * @param   appName       Application name
   * @param   configMgr     Configuration manager EJB
   * @param   className     The caller's class name
   * @return  Log           Log4J logger
   */
  private static LogInterface createLog4J(String appName, ConfigMgr configMgr, String className) {
    String logSettings;
    int logLevel;
    try {
      logSettings = configMgr.getAppParameter("logSettings");
      logLevel = Integer.parseInt(configMgr.getAppParameter("logLevel"));
    } catch (Exception e) {
      System.out.println("Cannot get log settings for application: " + appName);
      e.printStackTrace();
      logLevel = 31;
      logSettings = "log4j.rootCategory=DEBUG, stdout;log4j.appender.stdout=org.apache.log4j.ConsoleAppender;log4j.appender.stdout.layout=org.apache.log4j.PatternLayout;log4j.appender.stdout.layout.ConversionPattern=%d [%t] %-5p %c - %m%n";
    }

    return new Log4J(logSettings, logLevel, className);
  }

}
