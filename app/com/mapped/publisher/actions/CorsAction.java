package com.mapped.publisher.actions;

import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Http.Response;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

/**
 * Created by surge on 2016-01-22.
 */
public class CorsAction
    extends Action.Simple {

  @Override
  public CompletionStage<Result> call(Http.Context context) {
    Response response = context.response();
    response.setHeader("Access-Control-Allow-Origin", "*");

    //Handle preflight requests
    if (context.request().method().equals("OPTIONS")) {
      response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE");
      response.setHeader("Access-Control-Max-Age", "3600");
      response.setHeader("Access-Control-Allow-Headers",
                         "Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Auth-Token");
      response.setHeader("Access-Control-Allow-Credentials", "true");

      return delegate.call(context);
    }

    response.setHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, X-Auth-Token");
    return delegate.call(context);
  }

}
