package com.mapped.publisher.actions;

import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

public class Authorized extends Action.Simple {

  public CompletionStage<Result> call(Http.Context ctx) {
    //Log.debug("Authorized","Calling action for " + ctx);
    return delegate.call(ctx);
  }
}
