package com.mapped.publisher.actions;

import com.mapped.publisher.utils.Log;
import com.umapped.mobile.api.model.MobileAPIResponse;
import com.umapped.mobile.api.model.MobileAPIResponseCode;
import org.apache.commons.lang3.StringUtils;
import play.libs.Json;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by wei on 2017-08-16.
 */
public class MobileAPIVersionCheck extends Action.Simple  {
  private static Set<String> SupportedVerisons = new HashSet<>();
  static {
    SupportedVerisons.add("1.0");
  }

  @Override public CompletionStage<Result> call(Http.Context ctx) {
    String apiVersion = ctx.request().getHeader("X-UM-API-VERSION");

    if (StringUtils.isEmpty(apiVersion)) {
      return CompletableFuture.completedFuture(badRequest(Json.toJson(new MobileAPIResponse(MobileAPIResponseCode.MISSING_API_VERSION))));
    }

    if (!isVersionSupported(apiVersion)) {
      return CompletableFuture.completedFuture(badRequest(Json.toJson(new MobileAPIResponse(MobileAPIResponseCode.API_VERSION_NOT_SUPPORTED))));
    }
    return delegate.call(ctx);
  }

  private boolean isVersionSupported(String apiVersion) {
    return SupportedVerisons.contains(apiVersion);
  }
}
