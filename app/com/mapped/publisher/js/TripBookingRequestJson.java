package com.mapped.publisher.js;

import com.umapped.api.schema.types.Operation;
import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;

import java.util.List;

/**
 * Json Request for booking operation
 *
 * Created by surge on 2014-09-25.
 */
public class TripBookingRequestJson {
  /** Operation performed */
  public Operation operation;

  public List<BookingDetailsJson> bookings;

  public static TripBookingRequestJson fromJson(JsonNode jsonNode) {
    return Json.fromJson(jsonNode, com.mapped.publisher.js.TripBookingRequestJson.class);
  }
}
