package com.mapped.publisher.js;

import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.umapped.persistence.enums.ReservationType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by surge on 2014-10-10.
 */
public class TripBookingsJson
    extends BaseJsonResponse {

  public String tripId;
  public String tripName;
  public String cmpyId;
  public String cmpyName;
  public boolean canEdit;

  public Map<ReservationType, List<BookingDetailsJson>> bookings;

  public TripBookingsJson(String tripId, String tripName) {
    this.tripId = tripId;
    this.tripName = tripName;
    this.returnCode = 0;
    bookings = new HashMap<>();
  }

  public void addBookingDetails(BookingDetailsJson tbdj) {
    if (tbdj == null || tbdj.type == null) {
      Log.log(LogLevel.ERROR, "Failed to add trip booking detail view either type or view is null");
      return;
    }
    List<BookingDetailsJson> tbdjl = bookings.get(tbdj.type);
    if (tbdjl == null) {
      tbdjl = new ArrayList<>();
      bookings.put(tbdj.type, tbdjl);
    }
    tbdjl.add(tbdj);
  }
}
