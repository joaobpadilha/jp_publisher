package com.mapped.publisher.js;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.umapped.api.schema.types.ReturnCode;

/**
 * Created by surge on 2016-04-05.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JwtToken
    extends BaseJsonResponse {
  public String token;

  public MessengerConfig messenger;

  public boolean authorized = false;

  public boolean authenticated = false;

  public JwtToken(ReturnCode rc) {
    super(rc);
  }
}
