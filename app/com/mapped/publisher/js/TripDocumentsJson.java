package com.mapped.publisher.js;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mapped.publisher.view.AttachmentView;
import com.mapped.publisher.view.DestinationView;
import com.umapped.api.schema.types.ReturnCode;

import java.io.Serializable;
import java.util.List;

/**
 * Created by surge on 2016-04-19.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TripDocumentsJson
    extends BaseJsonResponse
    implements Serializable {


  public List<DestinationView> docs;
  public List<AttachmentView> attachments;

  public TripDocumentsJson(ReturnCode rc) {
    super(rc);
  }
}
