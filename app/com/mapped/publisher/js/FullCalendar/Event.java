package com.mapped.publisher.js.FullCalendar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.umapped.persistence.enums.ReservationType;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.List;

/**
 * FullCalendar.io jQuery based calendar Event object
 * <p>
 * http://fullcalendar.io/docs/event_data/Event_Object/
 * Created by surge on 2016-01-27.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Event
    implements Serializable {

  public Event(){

  }

  public Event(String id, String title, Long start, Long end) {
    this.id = id;
    this.title = title;
    this.start = start;
    this.end = end;
  }

  /**
   * String/Integer. Optional
   * Uniquely identifies the given event. Different instances of repeating events should all have the same id.
   */
  public String id;

  /**
   * String. Required.
   * The text on an event's element
   */
  @Nonnull
  public String title;


  /**
   * true or false. Optional.
   * Whether an event occurs at a specific time-of-day. This property affects whether an event's time is shown. Also,
   * in the agenda views, determines if it is displayed in the "all-day" section.
   * If this value is not explicitly specified, allDayDefault will be used if it is defined.
   * If all else fails, FullCalendar will try to guess. If either the start or end value has a "T" as part of the
   * ISO8601 date string, allDay will become false. Otherwise, it will be true.
   * Don't include quotes around your true/false. This value is a boolean, not a string!
   */
  public Boolean allDay;

  /**
   * The date/time an event begins. Required.
   * A Moment-ish input, like an ISO8601 string. Throughout the API this will become a real Moment object.
   */
  @Nonnull
  public Long start;

  /**
   * The exclusive date/time an event ends. Optional.
   * A Moment-ish input, like an ISO8601 string. Throughout the API this will become a real Moment object.
   * It is the moment immediately after the event has ended. For example, if the last full day of an event is
   * Thursday, the exclusive end of the event will be 00:00:00 on Friday!
   */
  public Long end;

  /**
   * String. Optional.
   * A URL that will be visited when this event is clicked by the user. For more information on controlling this
   * behavior, see the eventClick callback.
   */
  public String url;

  /**
   * String/Array. Optional.
   * A CSS class (or array of classes) that will be attached to this event's element.
   */
  public List<String> className;

  /**
   * true or false. Optional.
   * Overrides the master editable option for this single event.
   */
  public Boolean editable;

  /**
   * true or false. Optional.
   * Overrides the master eventStartEditable option for this single event.
   */
  public Boolean startEditable;

  /**
   * true or false. Optional.
   * Overrides the master eventDurationEditable option for this single event.
   */
  public Boolean durationEditable;

  /**
   * Allows alternate rendering of the event, like background events.
   * Can be empty, "background", or "inverse-background"
   */
  public String rendering;

  /**
   * true or false. Optional.
   * Overrides the master eventOverlap option for this single event.
   * If false, prevents this event from being dragged/resized over other events.
   * Also prevents other events from being dragged/resized over this event.
   */
  public Boolean overlap;

  /**
   * an event ID, "businessHours", object. Optional.
   * Overrides the master eventConstraint option for this single event.
   */
  public String constraint;


  /**
   * Sets an event's background and border color just like the calendar-wide eventColor option.
   */
  public String color;

  /**
   * Sets an event's background color just like the calendar-wide eventBackgroundColor option.
   */
  public String backgroundColor;

  /**
   * Sets an event's border color just like the the calendar-wide eventBorderColor option.
   */
  public String borderColor;

  /**
   * Sets an event's text color just like the calendar-wide eventTextColor option.
   */
  public String textColor;

  /**
   * Umapped Specific:
   * Type of the event
   */
  public ReservationType umBookingType;

  /**
   * Umapped Specific:
   * Parent/Master type for this event
   */
  public ReservationType umBookingTypeParent;
}
