package com.mapped.publisher.js;

import com.fasterxml.jackson.databind.JsonNode;
import com.umapped.persistence.enums.ReservationType;
import play.libs.Json;

/**
 * Created by surge on 2014-09-23.
 */
public class BookingDetailsJson {
  public ReservationType type;
  public ReservationType rootType;
  public String          typeLabel;
  public String          tmpltDetailId;
  public String          tripDetailId;
  public String          name;
  public int             dayOffset;
  public int             duration;
  public String          timeStart;
  public String          timeEnd;
  public String          cssClassName;
  public String          detailsEditUrl;

  public static BookingDetailsJson fromJson(JsonNode jsonNode) {
    return Json.fromJson(jsonNode, com.mapped.publisher.js.BookingDetailsJson.class);
  }
}
