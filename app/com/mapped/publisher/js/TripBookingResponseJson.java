package com.mapped.publisher.js;

import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.umapped.api.schema.types.Operation;
import com.umapped.persistence.enums.ReservationType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Response for the booking operation.
 * <p/>
 * Created by surge on 2014-09-25.
 */
public class TripBookingResponseJson
    extends BaseJsonResponse {
  /**
   * Trip ID
   */
  public String                                         id;
  /**
   * Trip Name
   */
  public String                                         name;
  /** Trip creator's company ID */
  public String                                         cmpyId;
  /** Trip creator's company Name */
  public String                                         cmpyName;
  /** Whether or not user can edit the trip */
  public boolean                                        canEdit;
  /** Affected bookings */
  public Map<ReservationType, List<BookingDetailsJson>> bookings;
  /** Operation performed */
  public Operation                                      operation;


  /** Locking down default constructor */
  private TripBookingResponseJson() {
  }

  public TripBookingResponseJson(String tripId, String tripName) {
    this.id = tripId;
    this.name = tripName;
    this.returnCode = 0;
    bookings = new HashMap<>();
  }

  public void addBookingDetails(BookingDetailsJson tbdj) {
    if (tbdj == null || tbdj.type == null) {
      Log.log(LogLevel.ERROR, "Failed to add template booking detail view either type or view is null");
      return;
    }
    List<BookingDetailsJson> tbdjl = bookings.get(tbdj.type);
    if (tbdjl == null) {
      tbdjl = new ArrayList<>();
      bookings.put(tbdj.type, tbdjl);
    }
    tbdjl.add(tbdj);
  }

  public TripBookingResponseJson withOperation(Operation operation) {
    this.operation = operation;
    return this;
  }
}
