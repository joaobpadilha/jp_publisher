package com.mapped.publisher.msg;

import java.io.Serializable;

/**
 * Created by twong on 2014-12-24.
 */
public class TripPdfRec
    implements Serializable {
  public String fileName;
  public Long startTimestamp;
}
