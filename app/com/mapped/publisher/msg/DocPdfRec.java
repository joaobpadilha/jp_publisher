package com.mapped.publisher.msg;

import java.io.Serializable;

/**
 * Created by twong on 2014-12-24.
 */
public class DocPdfRec implements Serializable {
  public String destId;
  public String tripId;
  public String fileName;
  public Long startTimestamp;
}
