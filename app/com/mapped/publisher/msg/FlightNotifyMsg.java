package com.mapped.publisher.msg;

import java.io.Serializable;

/**
 * Created by twong on 2015-01-15.
 */
public class FlightNotifyMsg
    implements Serializable {

  public long flightAlertId = 0;
  public long alertBookingId = 0; //default to 0 - notify all - if set,only notify this id
}
