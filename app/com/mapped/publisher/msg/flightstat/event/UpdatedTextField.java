package com.mapped.publisher.msg.flightstat.event;

/**
 * Created by twong on 2015-01-24.
 */
public class UpdatedTextField {
  private String field;
  private String newText;
  private String originalText;

  public String getField() {
    return field;
  }

  public void setField(String field) {
    this.field = field;
  }

  public String getNewText() {
    return newText;
  }

  public void setNewText(String newText) {
    this.newText = newText;
  }

  public String getOriginalText() {
    return originalText;
  }

  public void setOriginalText(String originalText) {
    this.originalText = originalText;
  }
}
