package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

/**
 * Created by twong on 2015-01-24.
 */
public class UpdatedTextFields {
  private List<UpdatedDateField> updatedTextField;

  public List<UpdatedDateField> getUpdatedTextField() {
    return updatedTextField;
  }

  public void setUpdatedTextField(List<UpdatedDateField> updatedTextField) {
    this.updatedTextField = updatedTextField;
  }
}
