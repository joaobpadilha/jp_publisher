
package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

public class Airlines{
   	private Airline airline;

 	public Airline getAirline(){
		return this.airline;
	}
	public void setAirline(Airline airline){
		this.airline = airline;
	}
}
