package com.mapped.publisher.msg.flightstat.event;

/**
 * Created by twong on 2015-01-27.
 */
public class Downline {

  private String arrivalAirport;
  private String fsCode;
  private String flightId;

  public String getArrivalAirport() {
    return arrivalAirport;
  }

  public void setArrivalAirport(String arrivalAirport) {
    this.arrivalAirport = arrivalAirport;
  }

  public String getFsCode() {
    return fsCode;
  }

  public void setFsCode(String fsCode) {
    this.fsCode = fsCode;
  }

  public String getFlightId() {
    return flightId;
  }

  public void setFlightId(String flightId) {
    this.flightId = flightId;
  }
}
