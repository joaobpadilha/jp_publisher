
package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

public class RuleEvents{
   	private List<RuleEvent> ruleEvent;

 	public List<RuleEvent> getRuleEvent(){
		return this.ruleEvent;
	}
	public void setRuleEvent(List<RuleEvent> ruleEvent){
		this.ruleEvent = ruleEvent;
	}
}
