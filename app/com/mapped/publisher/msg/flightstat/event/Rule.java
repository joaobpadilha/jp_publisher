
package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

public class Rule{
   	private String arrival;
   	private String arrivalAirportFsCode;
   	private String carrierFsCode;
   	private Delivery delivery;
   	private String departure;
   	private String departureAirportFsCode;
   	private String flightNumber;
   	private String id;
   	private String name;
   	private String nameValues;
   	private RuleEvents ruleEvents;
    private String description;

 	public String getArrival(){
		return this.arrival;
	}
	public void setArrival(String arrival){
		this.arrival = arrival;
	}
 	public String getArrivalAirportFsCode(){
		return this.arrivalAirportFsCode;
	}
	public void setArrivalAirportFsCode(String arrivalAirportFsCode){
		this.arrivalAirportFsCode = arrivalAirportFsCode;
	}
 	public String getCarrierFsCode(){
		return this.carrierFsCode;
	}
	public void setCarrierFsCode(String carrierFsCode){
		this.carrierFsCode = carrierFsCode;
	}
 	public Delivery getDelivery(){
		return this.delivery;
	}
	public void setDelivery(Delivery delivery){
		this.delivery = delivery;
	}
 	public String getDeparture(){
		return this.departure;
	}
	public void setDeparture(String departure){
		this.departure = departure;
	}
 	public String getDepartureAirportFsCode(){
		return this.departureAirportFsCode;
	}
	public void setDepartureAirportFsCode(String departureAirportFsCode){
		this.departureAirportFsCode = departureAirportFsCode;
	}
 	public String getFlightNumber(){
		return this.flightNumber;
	}
	public void setFlightNumber(String flightNumber){
		this.flightNumber = flightNumber;
	}
 	public String getId(){
		return this.id;
	}
	public void setId(String id){
		this.id = id;
	}
 	public String getName(){
		return this.name;
	}
	public void setName(String name){
		this.name = name;
	}
 	public String getNameValues(){
		return this.nameValues;
	}
	public void setNameValues(String nameValues){
		this.nameValues = nameValues;
	}
 	public RuleEvents getRuleEvents(){
		return this.ruleEvents;
	}
	public void setRuleEvents(RuleEvents ruleEvents){
		this.ruleEvents = ruleEvents;
	}

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
