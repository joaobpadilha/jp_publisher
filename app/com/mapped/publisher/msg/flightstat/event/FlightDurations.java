
package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

public class FlightDurations{
   	private String airMinutes;
   	private String blockMinutes;
   	private String scheduledAirMinutes;
   	private String scheduledBlockMinutes;
   	private String scheduledTaxiInMinutes;
   	private String scheduledTaxiOutMinutes;
   	private String taxiInMinutes;
   	private String taxiOutMinutes;

 	public String getAirMinutes(){
		return this.airMinutes;
	}
	public void setAirMinutes(String airMinutes){
		this.airMinutes = airMinutes;
	}
 	public String getBlockMinutes(){
		return this.blockMinutes;
	}
	public void setBlockMinutes(String blockMinutes){
		this.blockMinutes = blockMinutes;
	}
 	public String getScheduledAirMinutes(){
		return this.scheduledAirMinutes;
	}
	public void setScheduledAirMinutes(String scheduledAirMinutes){
		this.scheduledAirMinutes = scheduledAirMinutes;
	}
 	public String getScheduledBlockMinutes(){
		return this.scheduledBlockMinutes;
	}
	public void setScheduledBlockMinutes(String scheduledBlockMinutes){
		this.scheduledBlockMinutes = scheduledBlockMinutes;
	}
 	public String getScheduledTaxiInMinutes(){
		return this.scheduledTaxiInMinutes;
	}
	public void setScheduledTaxiInMinutes(String scheduledTaxiInMinutes){
		this.scheduledTaxiInMinutes = scheduledTaxiInMinutes;
	}
 	public String getScheduledTaxiOutMinutes(){
		return this.scheduledTaxiOutMinutes;
	}
	public void setScheduledTaxiOutMinutes(String scheduledTaxiOutMinutes){
		this.scheduledTaxiOutMinutes = scheduledTaxiOutMinutes;
	}
 	public String getTaxiInMinutes(){
		return this.taxiInMinutes;
	}
	public void setTaxiInMinutes(String taxiInMinutes){
		this.taxiInMinutes = taxiInMinutes;
	}
 	public String getTaxiOutMinutes(){
		return this.taxiOutMinutes;
	}
	public void setTaxiOutMinutes(String taxiOutMinutes){
		this.taxiOutMinutes = taxiOutMinutes;
	}
}
