
package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

public class UpdatedDateField{
   	private String field;
   	private String newDateLocal;
    private String originalDateLocal;

 	public String getField(){
		return this.field;
	}
	public void setField(String field){
		this.field = field;
	}
 	public String getNewDateLocal(){
		return this.newDateLocal;
	}
	public void setNewDateLocal(String newDateLocal){
		this.newDateLocal = newDateLocal;
	}

  public String getOriginalDateLocal() {
    return originalDateLocal;
  }

  public void setOriginalDateLocal(String originalDateLocal) {
    this.originalDateLocal = originalDateLocal;
  }
}
