
package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

public class Alert{
   	private String dataSource;
   	private String dateTimeRecorded;
   	private Event event;
   	private FlightStatus flightStatus;
   	private Rule rule;

 	public String getDataSource(){
		return this.dataSource;
	}
	public void setDataSource(String dataSource){
		this.dataSource = dataSource;
	}
 	public String getDateTimeRecorded(){
		return this.dateTimeRecorded;
	}
	public void setDateTimeRecorded(String dateTimeRecorded){
		this.dateTimeRecorded = dateTimeRecorded;
	}
 	public Event getEvent(){
		return this.event;
	}
	public void setEvent(Event event){
		this.event = event;
	}
 	public FlightStatus getFlightStatus(){
		return this.flightStatus;
	}
	public void setFlightStatus(FlightStatus flightStatus){
		this.flightStatus = flightStatus;
	}
 	public Rule getRule(){
		return this.rule;
	}
	public void setRule(Rule rule){
		this.rule = rule;
	}
}
