
package com.mapped.publisher.msg.flightstat.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties({"airlines"})

public class Appendix{
   	private Airlines airlines;
   	private Airports airports;

 	public Airlines getAirlines(){
		return this.airlines;
	}
	public void setAirlines(Airlines airlines){
		this.airlines = airlines;
	}
 	public Airports getAirports(){
		return this.airports;
	}
	public void setAirports(Airports airports){
		this.airports = airports;
	}
}
