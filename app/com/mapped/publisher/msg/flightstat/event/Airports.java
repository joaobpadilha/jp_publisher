
package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

public class Airports{
   	private List<Airport> airport;

 	public List<Airport> getAirport(){
		return this.airport;
	}
	public void setAirport(List<Airport> airport){
		this.airport = airport;
	}
}
