
package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

public class AlertEvent{
   	private Alert alert;
   	private Appendix appendix;

 	public Alert getAlert(){
		return this.alert;
	}
	public void setAlert(Alert alert){
		this.alert = alert;
	}
 	public Appendix getAppendix(){
		return this.appendix;
	}
	public void setAppendix(Appendix appendix){
		this.appendix = appendix;
	}
}
