
package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

public class Upline{
   	private String flightId;
   	private String fsCode;
    private String departureAirport;

 	public String getFlightId(){
		return this.flightId;
	}
	public void setFlightId(String flightId){
		this.flightId = flightId;
	}
 	public String getFsCode(){
		return this.fsCode;
	}
	public void setFsCode(String fsCode){
		this.fsCode = fsCode;
	}

  public String getDepartureAirport() {
    return departureAirport;
  }

  public void setDepartureAirport(String departureAirport) {
    this.departureAirport = departureAirport;
  }
}
