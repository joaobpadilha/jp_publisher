
package com.mapped.publisher.msg.flightstat.track;

import java.util.List;

public class AlertResponse{
   	private AlertCapabilities alertCapabilities;
   	private Appendix appendix;
   	private Request request;
   	private Rule rule;
    private Error error;

 	public AlertCapabilities getAlertCapabilities(){
		return this.alertCapabilities;
	}
	public void setAlertCapabilities(AlertCapabilities alertCapabilities){
		this.alertCapabilities = alertCapabilities;
	}
 	public Appendix getAppendix(){
		return this.appendix;
	}
	public void setAppendix(Appendix appendix){
		this.appendix = appendix;
	}
 	public Request getRequest(){
		return this.request;
	}
	public void setRequest(Request request){
		this.request = request;
	}
 	public Rule getRule(){
		return this.rule;
	}
	public void setRule(Rule rule){
		this.rule = rule;
	}

  public Error getError() {
    return error;
  }

  public void setError(Error error) {
    this.error = error;
  }
}
