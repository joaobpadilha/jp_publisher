
package com.mapped.publisher.msg.flightstat.track;

import java.util.List;

public class AlertCapabilities{
   	private boolean arrivalGateChange;
   	private boolean baggage;
   	private boolean departureGateChange;
   	private boolean gateArrival;
   	private boolean gateDeparture;
   	private boolean runwayArrival;
   	private boolean runwayDeparture;

 	public boolean getArrivalGateChange(){
		return this.arrivalGateChange;
	}
	public void setArrivalGateChange(boolean arrivalGateChange){
		this.arrivalGateChange = arrivalGateChange;
	}
 	public boolean getBaggage(){
		return this.baggage;
	}
	public void setBaggage(boolean baggage){
		this.baggage = baggage;
	}
 	public boolean getDepartureGateChange(){
		return this.departureGateChange;
	}
	public void setDepartureGateChange(boolean departureGateChange){
		this.departureGateChange = departureGateChange;
	}
 	public boolean getGateArrival(){
		return this.gateArrival;
	}
	public void setGateArrival(boolean gateArrival){
		this.gateArrival = gateArrival;
	}
 	public boolean getGateDeparture(){
		return this.gateDeparture;
	}
	public void setGateDeparture(boolean gateDeparture){
		this.gateDeparture = gateDeparture;
	}
 	public boolean getRunwayArrival(){
		return this.runwayArrival;
	}
	public void setRunwayArrival(boolean runwayArrival){
		this.runwayArrival = runwayArrival;
	}
 	public boolean getRunwayDeparture(){
		return this.runwayDeparture;
	}
	public void setRunwayDeparture(boolean runwayDeparture){
		this.runwayDeparture = runwayDeparture;
	}
}
