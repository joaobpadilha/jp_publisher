
package com.mapped.publisher.msg.flightstat.track;

import java.util.List;

public class Appendix{
   	private List<Airlines> airlines;
   	private List<Airports> airports;

 	public List<Airlines> getAirlines(){
		return this.airlines;
	}
	public void setAirlines(List<Airlines> airlines){
		this.airlines = airlines;
	}
 	public List<Airports> getAirports(){
		return this.airports;
	}
	public void setAirports(List<Airports> airports){
		this.airports = airports;
	}
}
