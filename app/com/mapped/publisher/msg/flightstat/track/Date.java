
package com.mapped.publisher.msg.flightstat.track;

import java.util.List;

public class Date{
   	private String day;
   	private String interpreted;
   	private String month;
   	private String year;

 	public String getDay(){
		return this.day;
	}
	public void setDay(String day){
		this.day = day;
	}
 	public String getInterpreted(){
		return this.interpreted;
	}
	public void setInterpreted(String interpreted){
		this.interpreted = interpreted;
	}
 	public String getMonth(){
		return this.month;
	}
	public void setMonth(String month){
		this.month = month;
	}
 	public String getYear(){
		return this.year;
	}
	public void setYear(String year){
		this.year = year;
	}
}
