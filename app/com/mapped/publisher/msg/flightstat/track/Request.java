
package com.mapped.publisher.msg.flightstat.track;

import java.util.List;

public class Request{
   	private AirlineCode airlineCode;
   	private Airport airport;
   	private CodeType codeType;
   	private Date date;
   	private DeliverTo deliverTo;
   	private Description description;
   	private List<Events> events;
   	private ExtendedOptions extendedOptions;
   	private FlightNumber flightNumber;
   	private Name name;
   	private List<NameValues> nameValues;
   	private Type type;
   	private String url;

 	public AirlineCode getAirlineCode(){
		return this.airlineCode;
	}
	public void setAirlineCode(AirlineCode airlineCode){
		this.airlineCode = airlineCode;
	}
 	public Airport getAirport(){
		return this.airport;
	}
	public void setAirport(Airport airport){
		this.airport = airport;
	}
 	public CodeType getCodeType(){
		return this.codeType;
	}
	public void setCodeType(CodeType codeType){
		this.codeType = codeType;
	}
 	public Date getDate(){
		return this.date;
	}
	public void setDate(Date date){
		this.date = date;
	}
 	public DeliverTo getDeliverTo(){
		return this.deliverTo;
	}
	public void setDeliverTo(DeliverTo deliverTo){
		this.deliverTo = deliverTo;
	}
 	public Description getDescription(){
		return this.description;
	}
	public void setDescription(Description description){
		this.description = description;
	}
 	public List<Events> getEvents(){
		return this.events;
	}
	public void setEvents(List<Events> events){
		this.events = events;
	}
 	public ExtendedOptions getExtendedOptions(){
		return this.extendedOptions;
	}
	public void setExtendedOptions(ExtendedOptions extendedOptions){
		this.extendedOptions = extendedOptions;
	}
 	public FlightNumber getFlightNumber(){
		return this.flightNumber;
	}
	public void setFlightNumber(FlightNumber flightNumber){
		this.flightNumber = flightNumber;
	}
 	public Name getName(){
		return this.name;
	}
	public void setName(Name name){
		this.name = name;
	}
 	public List<NameValues> getNameValues(){
		return this.nameValues;
	}
	public void setNameValues(List<NameValues> nameValues){
		this.nameValues = nameValues;
	}
 	public Type getType(){
		return this.type;
	}
	public void setType(Type type){
		this.type = type;
	}
 	public String getUrl(){
		return this.url;
	}
	public void setUrl(String url){
		this.url = url;
	}
}
