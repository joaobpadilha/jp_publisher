package com.mapped.publisher.msg.flightstat.status;

import java.util.List;

/**
 * Created by twong on 2015-02-10.
 */
public class Equipments {
  private List<Equipment> equipments;

  public List<Equipment> getEquipments() {
    return equipments;
  }

  public void setEquipments(List<Equipment> equipments) {
    this.equipments = equipments;
  }
}
