
package com.mapped.publisher.msg.flightstat.status;

import com.mapped.publisher.msg.flightstat.event.*;

public class OperationalTimes{
   	private ActualGateArrival actualGateArrival;
   	private ActualGateDeparture actualGateDeparture;
   	private ActualRunwayArrival actualRunwayArrival;
   	private ActualRunwayDeparture actualRunwayDeparture;
   	private EstimatedGateArrival estimatedGateArrival;
   	private EstimatedGateDeparture estimatedGateDeparture;
   	private EstimatedRunwayArrival estimatedRunwayArrival;
   	private EstimatedRunwayDeparture estimatedRunwayDeparture;
   	private FlightPlanPlannedArrival flightPlanPlannedArrival;
   	private FlightPlanPlannedDeparture flightPlanPlannedDeparture;
   	private PublishedArrival publishedArrival;
   	private PublishedDeparture publishedDeparture;
   	private ScheduledGateArrival scheduledGateArrival;
   	private ScheduledGateDeparture scheduledGateDeparture;

 	public ActualGateArrival getActualGateArrival(){
		return this.actualGateArrival;
	}
	public void setActualGateArrival(ActualGateArrival actualGateArrival){
		this.actualGateArrival = actualGateArrival;
	}
 	public ActualGateDeparture getActualGateDeparture(){
		return this.actualGateDeparture;
	}
	public void setActualGateDeparture(ActualGateDeparture actualGateDeparture){
		this.actualGateDeparture = actualGateDeparture;
	}
 	public ActualRunwayArrival getActualRunwayArrival(){
		return this.actualRunwayArrival;
	}
	public void setActualRunwayArrival(ActualRunwayArrival actualRunwayArrival){
		this.actualRunwayArrival = actualRunwayArrival;
	}
 	public ActualRunwayDeparture getActualRunwayDeparture(){
		return this.actualRunwayDeparture;
	}
	public void setActualRunwayDeparture(ActualRunwayDeparture actualRunwayDeparture){
		this.actualRunwayDeparture = actualRunwayDeparture;
	}
 	public EstimatedGateArrival getEstimatedGateArrival(){
		return this.estimatedGateArrival;
	}
	public void setEstimatedGateArrival(EstimatedGateArrival estimatedGateArrival){
		this.estimatedGateArrival = estimatedGateArrival;
	}
 	public EstimatedGateDeparture getEstimatedGateDeparture(){
		return this.estimatedGateDeparture;
	}
	public void setEstimatedGateDeparture(EstimatedGateDeparture estimatedGateDeparture){
		this.estimatedGateDeparture = estimatedGateDeparture;
	}
 	public EstimatedRunwayArrival getEstimatedRunwayArrival(){
		return this.estimatedRunwayArrival;
	}
	public void setEstimatedRunwayArrival(EstimatedRunwayArrival estimatedRunwayArrival){
		this.estimatedRunwayArrival = estimatedRunwayArrival;
	}
 	public EstimatedRunwayDeparture getEstimatedRunwayDeparture(){
		return this.estimatedRunwayDeparture;
	}
	public void setEstimatedRunwayDeparture(EstimatedRunwayDeparture estimatedRunwayDeparture){
		this.estimatedRunwayDeparture = estimatedRunwayDeparture;
	}
 	public FlightPlanPlannedArrival getFlightPlanPlannedArrival(){
		return this.flightPlanPlannedArrival;
	}
	public void setFlightPlanPlannedArrival(FlightPlanPlannedArrival flightPlanPlannedArrival){
		this.flightPlanPlannedArrival = flightPlanPlannedArrival;
	}
 	public FlightPlanPlannedDeparture getFlightPlanPlannedDeparture(){
		return this.flightPlanPlannedDeparture;
	}
	public void setFlightPlanPlannedDeparture(FlightPlanPlannedDeparture flightPlanPlannedDeparture){
		this.flightPlanPlannedDeparture = flightPlanPlannedDeparture;
	}
 	public PublishedArrival getPublishedArrival(){
		return this.publishedArrival;
	}
	public void setPublishedArrival(PublishedArrival publishedArrival){
		this.publishedArrival = publishedArrival;
	}
 	public PublishedDeparture getPublishedDeparture(){
		return this.publishedDeparture;
	}
	public void setPublishedDeparture(PublishedDeparture publishedDeparture){
		this.publishedDeparture = publishedDeparture;
	}
 	public ScheduledGateArrival getScheduledGateArrival(){
		return this.scheduledGateArrival;
	}
	public void setScheduledGateArrival(ScheduledGateArrival scheduledGateArrival){
		this.scheduledGateArrival = scheduledGateArrival;
	}
 	public ScheduledGateDeparture getScheduledGateDeparture(){
		return this.scheduledGateDeparture;
	}
	public void setScheduledGateDeparture(ScheduledGateDeparture scheduledGateDeparture){
		this.scheduledGateDeparture = scheduledGateDeparture;
	}
}
