package com.mapped.publisher.msg.flightstat.status;

import com.mapped.publisher.msg.flightstat.event.FlightStatus;

import java.util.List;

/**
 * Created by twong on 2015-02-10.
 */
public class FlightStatuses {
  private List<FlightStatus> flightStatuses;

  public List<FlightStatus> getFlightStatuses() {
    return flightStatuses;
  }

  public void setFlightStatuses(List<FlightStatus> flightStatuses) {
    this.flightStatuses = flightStatuses;
  }
}
