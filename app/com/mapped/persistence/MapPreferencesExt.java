package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 12-08-03
 * Time: 2:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class MapPreferencesExt extends  MapPreferences{
    private static final String SELECT_ALL_BY_OWNER_SINCE_LAST_UPDATE= "Select Pk, Name, Value, MapsId, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, Status, SyncTimestamp from MapPreferences where (MapsId in (select mapsid from sharedmaps where owner = ? and status = 0) or modifiedBy = ?) and (SyncTimestamp = 0 or SyncTimestamp > ?);";
    private static final String SELECT_SHARED_BY_USER_SINCE_LAST_UPDATE= "Select Pk, Name, Value, MapsId, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, Status, SyncTimestamp from MapPreferences where MapsId in (select mapsid from sharedmaps where shareuserid = ? and status = 0) and (SyncTimestamp = 0 or SyncTimestamp > ?);";
    private static final String SELECT_NEW_SHARED_BY_USER_SINCE_LAST_UPDATE= "Select Pk, Name, Value, MapsId, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, Status, SyncTimestamp from MapPreferences where MapsId in (select mapsid from sharedmaps where shareuserid = ? and status = 0 and (SyncTimestamp = 0 or SyncTimestamp > ?) and createtimestamp = lastupdatetimestamp);";
    private static final String INSERT = "insert into MapPreferences (Pk, Name, Value, MapsId, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, Status, SyncTimestamp) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String UPDATE = "update MapPreferences SET Name=?, Value=?, MapsId=?, LastUpdateTimestamp=?, ModifiedBy=?, Status=?, SyncTimestamp=? WHERE Pk = ?; ";




    public static ArrayList<MapPreferences> getAllByOwnerSinceLastUpdate(String userId, long lastUpdate) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_ALL_BY_OWNER_SINCE_LAST_UPDATE);
            prep.setString(1, userId);
            prep.setString(2, userId);
            prep.setLong(3,lastUpdate);
            rs = prep.executeQuery();
            return MapPreferencesMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList<MapPreferences> getSharedByUserSinceLastUpdate(String userId, long lastUpdate) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_SHARED_BY_USER_SINCE_LAST_UPDATE);
            prep.setString(1, userId);
            prep.setLong(2,lastUpdate);
            rs = prep.executeQuery();
            return MapPreferencesMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList<MapPreferences> getNewSharedByUserSinceLastUpdate(String userId, long lastUpdate) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_NEW_SHARED_BY_USER_SINCE_LAST_UPDATE);
            prep.setString(1, userId);
            prep.setLong(2,lastUpdate);
            rs = prep.executeQuery();
            return MapPreferencesMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static void insert (MapPreferences c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(INSERT);
            prep.setString(1, c.getPk());
            prep.setString(2, c.getName());
            prep.setString(3, c.getValue());
            prep.setString(4, c.getMapsId());
            prep.setLong(5, c.getLastUpdateTimestamp());
            prep.setLong(6, c.getCreateTimestamp());
            prep.setString(7, c.getModifiedBy());
            prep.setString(8, c.getCreatedBy());
            prep.setLong(9, c.getStatus());
            prep.setLong(10, c.getSyncTimestamp());
            prep.execute();
        } finally {
            if (prep != null)
                prep.close();
        }
    }

    public static int update (MapPreferences c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(UPDATE);
            prep.setString(1, c.getName());
            prep.setString(2, c.getValue());
            prep.setString(3, c.getMapsId());
            prep.setLong(4, c.getLastUpdateTimestamp());
            prep.setString(5, c.getModifiedBy());
            prep.setLong(6, c.getStatus());
            prep.setLong(7, c.getSyncTimestamp());
            prep.setString(8, c.getPk());
            return prep.executeUpdate();
        } finally {
            if (prep != null)
                prep.close();
        }
    }


}
