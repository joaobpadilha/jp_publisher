/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mapped.persistence.util;

import com.google.inject.Inject;
import com.mapped.publisher.utils.Log;
import play.db.Database;
import play.db.NamedDatabase;

import java.sql.*;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Properties;

/**
 * @author twong
 */
public class DBConnectionMgr {

  private final static boolean DEBUG       = false;
  /**
   * Publisher standard default database
   */
  @Inject
  static Database dbPub;
  /**
   * Mobile App Sync Database
   */
  @Inject
  @NamedDatabase("web")
  static Database dbMob;


  private static       int     clusterId   = -1;
  private static       long    uniqueKey   = (System.currentTimeMillis() / 1000) * 100000000;
  private static       long    startEpoch  = 0;
  private static       long    counter     = 0;
  private static       int     openCounter = 0;


  /**
   * TODO: 2016-11-21: Should explicitly declare that can throw null pointer error
   *
   * @return
   */
  public static Connection getConnection4Mobile() {
    return dbMob.getConnection();
  }

  public static Connection getConnection4Publisher() {
    return dbPub.getConnection();
  }

  public static Connection getDBConnection(String url, String userId, String password)
      throws SQLException {
    try {
      Class.forName("org.postgresql.Driver");
      Properties props = new Properties();
      props.setProperty("user", userId);
      props.setProperty("password", password);
      props.setProperty("ssl", "true");
      props.setProperty("sslfactory", "org.postgresql.ssl.NonValidatingFactory");
      if (DEBUG) {
        ++openCounter;
        Log.info(">>>> Open DB connections:" + openCounter + "<<<<");
      }
      return DriverManager.getConnection(url, props);
    }
    catch (Exception e) {
      --openCounter;
      e.printStackTrace();
      throw new SQLException();
    }
  }

  public static void closeConnection(Connection conn)
      throws SQLException {
    if (conn != null) {
      if (DEBUG) {
        --openCounter;
      }
      conn.close();
    }
  }

  //TODO - provide proper implementation
  public static String getUniqueId() {
    synchronized (DBConnectionMgr.class) {
      long id = idGenerator();
      return String.valueOf(id);
    }
  }

  private static long idGenerator() {
    ++uniqueKey;
    ++counter;
    if (counter > 9999998) {
      //increment custom epoch counter;
      startEpoch++;
      long currentEpoch = startEpoch * 1_000L * 10_000_000L;
      uniqueKey = currentEpoch + clusterId * 10_000_000L;
      counter = 0;
    }

    return uniqueKey;
  }

  public static long getUniqueLongId() {
    synchronized (DBConnectionMgr.class) {
      return idGenerator();
    }
  }

  public static int initClusterId()
      throws Exception {

    ZoneId zoneId = ZoneId.of("UTC");
    ZonedDateTime zdt = ZonedDateTime.of(2013, 01, 01, 0, 0, 0, 0, zoneId);
    zdt = zdt.toLocalDate().atStartOfDay(zoneId);
    Instant inst = zdt.toInstant();
    zdt.toEpochSecond();

    long         startEpoch    = ((System.currentTimeMillis() - inst.toEpochMilli()) / 1000L);

    Connection conn = null;
    try {
      conn = dbPub.getConnection();
      PreparedStatement ps = conn.prepareStatement("select nextval('instanceid') as instanceid;");
      ResultSet         rs = ps.executeQuery();
      if (rs != null && rs.next()) {

        int  clusterId    = rs.getInt("instanceid");
        long currentEpoch = startEpoch * 1_000L * 10_000_000L;
        long startkey     = currentEpoch + clusterId * 10_000_000L;

        if (startkey < 0) {
          return -1;
        }

        DBConnectionMgr.setStartEpoch(startEpoch);
        DBConnectionMgr.setClusterId(clusterId);
        DBConnectionMgr.setUniqueKey(startkey);

        ps.close();
        return DBConnectionMgr.getClusterId();
      }
    }
    catch (Exception e) {
      Log.err("DBConnectionMgr:initUniqueKey", e);
      throw e;
    }
    finally {
      if (conn != null) {
        conn.close();
      }
    }

    return -1;
  }

  public static int getClusterId() {
    return clusterId;
  }

  public static void setClusterId(int clusterId) {
    DBConnectionMgr.clusterId = clusterId;
  }

  public static long getUniqueKey() {
    return uniqueKey;
  }

  public static void setUniqueKey(long uniqueKey) {
    DBConnectionMgr.uniqueKey = uniqueKey;
  }

  public static long getStartEpoch() {
    return startEpoch;
  }

  public static void setStartEpoch(long startEpoch) {
    DBConnectionMgr.startEpoch = startEpoch;
  }

  public static long getCounter() {
    return counter;
  }

  public static void setCounter(long counter) {
    DBConnectionMgr.counter = counter;
  }
}
