package com.mapped.persistence;

import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import play.libs.Json;
 public class Invites { 
 
	 public final static String InvitesTypeId = "160"; 
	 protected static String pkTypeId =  "161"; 
	 private String pk; 
	 protected static String inviteeEmailTypeId =  "162"; 
	 private String inviteeEmail; 
	 protected static String usersIdTypeId =  "163"; 
	 private String usersId; 
	 protected static String statusTypeId =  "164"; 
	 private long status; 
	 protected static String lastUpdateTimestampTypeId =  "165"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "166"; 
	 private long createTimestamp; 

 	 // Contructor 
	 public Invites () {} 
	 public Invites ( String pk,  String inviteeEmail,  String usersId,  long status,  long lastUpdateTimestamp,  long createTimestamp ) { 
		 this.pk=pk; 
		 this.inviteeEmail=inviteeEmail; 
		 this.usersId=usersId; 
		 this.status=status; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getInviteeEmail () { 
	 	 return inviteeEmail; 
 	 } 
 
 	 public void setInviteeEmail(String inviteeEmail) { 
	 	 this.inviteeEmail = inviteeEmail; 
 	 } 
 
 	 public  String getUsersId () { 
	 	 return usersId; 
 	 } 
 
 	 public void setUsersId(String usersId) { 
	 	 this.usersId = usersId; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public Users getUsers () throws Exception{ 
		 return UsersMgr.getByPk(usersId); 
	}
	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: Invites"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 InviteeEmail: ").append(this.inviteeEmail );  
		 sb.append("	 UsersId: ").append(this.usersId );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", InvitesTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (inviteeEmail != null) {
			 memberNode.put(inviteeEmailTypeId, inviteeEmail);
		 }
		 if (usersId != null) {
			 memberNode.put(usersIdTypeId, usersId);
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 return classNode;
	 } 
	 public static Invites parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 Invites  c= new Invites(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.inviteeEmail = fields.findPath(inviteeEmailTypeId).textValue();
			 c.usersId = fields.findPath(usersIdTypeId).textValue();
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 return c;
		} else
			 return null;
	 } 
} 
 
