package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 12-07-26
 * Time: 9:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class UsersMgrExt extends UsersMgr {
    private static final String SELECT_ALL_BY_EMAIL = "Select Pk, Email, FName, LName, Pwd, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, SyncTimestamp, authcode from Users where lower(Email) = ?;";
    private static final String SELECT_ALL_BY_AUTH_CODE = "Select Pk, Email, FName, LName, Pwd, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, SyncTimestamp, authcode from Users where authcode = ? and status = 0;";

    private static final String SELECT_NEW_SHARED_USERS = "Select Pk, Email, FName, LName, Pwd, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, SyncTimestamp, authcode from Users where (pk in (select owner from sharedMaps where shareUserId = ? and status = 0 and synctimestamp > ?) or pk in (select shareuserid from sharedmaps where owner = ? and status = 0 and synctimestamp > ?) or pk in (select shareuserid from sharedmaps where owner in (select owner from sharedMaps where shareUserId = ? and status = 0 and synctimestamp > ?))) ;";
    private static final String SELECT_OWNER_SHARED_MAPS_BY_MAPSID = "Select Pk, Email, FName, LName, Pwd, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, SyncTimestamp, authcode from Users where pk in (Select ShareUserId from SharedMaps where status = 0 and MapsId=? and shareType='3') and status = 0;";


    public static List<Users> getOwnersByMapsId (String mapsId) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_OWNER_SHARED_MAPS_BY_MAPSID);
            prep.setString(1, mapsId);
            rs = prep.executeQuery();
            return UsersMgr.handleResults(rs);

        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }


    public static Users getByEmail (String email) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
             stat = conn.createStatement();
             prep = conn.prepareStatement(SELECT_ALL_BY_EMAIL);
            prep.setString(1, email.toLowerCase());
            rs = prep.executeQuery();
            ArrayList<Users> results = UsersMgr.handleResults(rs);
            if (results != null && results.size() == 1)
                return results.get(0);
            else
                return null;
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static Users getByAuthCode (String authCode) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_ALL_BY_AUTH_CODE);
            prep.setString(1, authCode);
            rs = prep.executeQuery();
            ArrayList<Users> results = UsersMgr.handleResults(rs);
            if (results != null && results.size() == 1)
                return results.get(0);
            else
                return null;
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList<Users> getNewSharedUsers (String userId, long lastUpdateTime) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_NEW_SHARED_USERS);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdateTime);
            prep.setString(3, userId);
            prep.setLong(4, lastUpdateTime);
            prep.setString(5, userId);
            prep.setLong(6, lastUpdateTime);

            rs = prep.executeQuery();
            ArrayList<Users> results = UsersMgr.handleResults(rs);

            return results;
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }
}
