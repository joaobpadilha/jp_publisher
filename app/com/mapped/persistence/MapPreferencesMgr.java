package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class MapPreferencesMgr {

	 private static final String SELECT_ALL = "Select Pk, Name, Value, MapsId, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, Status, SyncTimestamp from MapPreferences;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, Name, Value, MapsId, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, Status, SyncTimestamp from MapPreferences where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from MapPreferences where Pk = ?;";
	 private static final String INSERT = "insert into MapPreferences (Pk, Name, Value, MapsId, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, Status, SyncTimestamp) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update MapPreferences SET Name=?, Value=?, MapsId=?, LastUpdateTimestamp=?, ModifiedBy=?, Status=?, SyncTimestamp=? WHERE Pk = ?; ";
	 private static final String SELECT_ALL_BY_MAPS_ID = "Select Pk, Name, Value, MapsId, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, Status, SyncTimestamp from MapPreferences where MapsId=? ";
	 private static final String DELETE_BY_MAPS_ID = "delete from MapPreferences where MapsId=? ";
	 public static ArrayList <MapPreferences> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return MapPreferencesMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static MapPreferences getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<MapPreferences> results = MapPreferencesMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (MapPreferences c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getName());
				 prep.setString(3, c.getValue());
				 prep.setString(4, c.getMapsId());
				 prep.setLong(5, c.getLastUpdateTimestamp());
				 prep.setLong(6, c.getCreateTimestamp());
				 prep.setString(7, c.getModifiedBy());
				 prep.setString(8, c.getCreatedBy());
				 prep.setLong(9, c.getStatus());
				 prep.setLong(10, c.getSyncTimestamp());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (MapPreferences c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getName());
				 prep.setString(2, c.getValue());
				 prep.setString(3, c.getMapsId());
				 prep.setLong(4, c.getLastUpdateTimestamp());
				 prep.setString(5, c.getModifiedBy());
				 prep.setLong(6, c.getStatus());
				 prep.setLong(7, c.getSyncTimestamp());
				 prep.setString(8, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <MapPreferences> getAllByMapsId(String id) throws SQLException {
		 ResultSet rs = null;
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_MAPS_ID);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 return MapPreferencesMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void deleteByMapsId (String id) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_MAPS_ID);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <MapPreferences> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <MapPreferences> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 MapPreferences c = new MapPreferences();
			 c.setPk(rs.getString("Pk"));
			 c.setName(rs.getString("Name"));
			 c.setValue(rs.getString("Value"));
			 c.setMapsId(rs.getString("MapsId"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 c.setModifiedBy(rs.getString("ModifiedBy"));
			 c.setCreatedBy(rs.getString("CreatedBy"));
			 c.setStatus(rs.getLong("Status"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 results.add(c);
		 }
		 return results;
	 }
}
