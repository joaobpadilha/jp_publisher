package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class PreferencesMgr {

	 private static final String SELECT_ALL = "Select Pk, Name, Value, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, Status, SyncTimestamp from Preferences;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, Name, Value, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, Status, SyncTimestamp from Preferences where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from Preferences where Pk = ?;";
	 private static final String INSERT = "insert into Preferences (Pk, Name, Value, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, Status, SyncTimestamp) values (?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update Preferences SET Name=?, Value=?, LastUpdateTimestamp=?, ModifiedBy=?, Status=?, SyncTimestamp=? WHERE Pk = ?; ";
	 public static ArrayList <Preferences> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return PreferencesMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static Preferences getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<Preferences> results = PreferencesMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (Preferences c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getName());
				 prep.setString(3, c.getValue());
				 prep.setLong(4, c.getLastUpdateTimestamp());
				 prep.setLong(5, c.getCreateTimestamp());
				 prep.setString(6, c.getModifiedBy());
				 prep.setString(7, c.getCreatedBy());
				 prep.setLong(8, c.getStatus());
				 prep.setLong(9, c.getSyncTimestamp());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (Preferences c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getName());
				 prep.setString(2, c.getValue());
				 prep.setLong(3, c.getLastUpdateTimestamp());
				 prep.setString(4, c.getModifiedBy());
				 prep.setLong(5, c.getStatus());
				 prep.setLong(6, c.getSyncTimestamp());
				 prep.setString(7, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <Preferences> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <Preferences> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 Preferences c = new Preferences();
			 c.setPk(rs.getString("Pk"));
			 c.setName(rs.getString("Name"));
			 c.setValue(rs.getString("Value"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 c.setModifiedBy(rs.getString("ModifiedBy"));
			 c.setCreatedBy(rs.getString("CreatedBy"));
			 c.setStatus(rs.getLong("Status"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 results.add(c);
		 }
		 return results;
	 }
}
