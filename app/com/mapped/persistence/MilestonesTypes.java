package com.mapped.persistence;
 
import java.util.*;
	 import com.mapped.persistence.util.*;
 import java.sql.*; 
 import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import play.libs.Json;
 public class MilestonesTypes { 
 
	 public final static String MilestonesTypesTypeId = "3500"; 
	 protected static String pkTypeId =  "3501"; 
	 private String pk; 
	 protected static String placeTypeTypeId =  "3502"; 
	 private String placeType; 
	 protected static String placeNameTypeId =  "3503"; 
	 private String placeName; 
	 protected static String mapsIdTypeId =  "3504"; 
	 private String mapsId; 
	 protected static String rankTypeId =  "3505"; 
	 private String rank; 
	 protected static String modifiedByTypeId =  "3590"; 
	 private String modifiedBy; 
	 protected static String createdByTypeId =  "3591"; 
	 private String createdBy; 
	 protected static String statusTypeId =  "3592"; 
	 private long status; 
	 protected static String syncTimestampTypeId =  "3593"; 
	 private long syncTimestamp; 
	 protected static String lastUpdateTimestampTypeId =  "3594"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "3595"; 
	 private long createTimestamp; 

 	 // Contructor 
	 public MilestonesTypes () {} 
	 public MilestonesTypes ( String pk,  String placeType,  String placeName,  String mapsId,  String rank,  String modifiedBy,  String createdBy,  long status,  long syncTimestamp,  long lastUpdateTimestamp,  long createTimestamp ) { 
		 this.pk=pk; 
		 this.placeType=placeType; 
		 this.placeName=placeName; 
		 this.mapsId=mapsId; 
		 this.rank=rank; 
		 this.modifiedBy=modifiedBy; 
		 this.createdBy=createdBy; 
		 this.status=status; 
		 this.syncTimestamp=syncTimestamp; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getPlaceType () { 
	 	 return placeType; 
 	 } 
 
 	 public void setPlaceType(String placeType) { 
	 	 this.placeType = placeType; 
 	 } 
 
 	 public  String getPlaceName () { 
	 	 return placeName; 
 	 } 
 
 	 public void setPlaceName(String placeName) { 
	 	 this.placeName = placeName; 
 	 } 
 
 	 public  String getMapsId () { 
	 	 return mapsId; 
 	 } 
 
 	 public void setMapsId(String mapsId) { 
	 	 this.mapsId = mapsId; 
 	 } 
 
 	 public  String getRank () { 
	 	 return rank; 
 	 } 
 
 	 public void setRank(String rank) { 
	 	 this.rank = rank; 
 	 } 
 
 	 public  String getModifiedBy () { 
	 	 return modifiedBy; 
 	 } 
 
 	 public void setModifiedBy(String modifiedBy) { 
	 	 this.modifiedBy = modifiedBy; 
 	 } 
 
 	 public  String getCreatedBy () { 
	 	 return createdBy; 
 	 } 
 
 	 public void setCreatedBy(String createdBy) { 
	 	 this.createdBy = createdBy; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public Maps getMaps () throws Exception{ 
		 return MapsMgr.getByPk(mapsId); 
	}
	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: MilestonesTypes"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 PlaceType: ").append(this.placeType );  
		 sb.append("	 PlaceName: ").append(this.placeName );  
		 sb.append("	 MapsId: ").append(this.mapsId );  
		 sb.append("	 Rank: ").append(this.rank );  
		 sb.append("	 ModifiedBy: ").append(this.modifiedBy );  
		 sb.append("	 CreatedBy: ").append(this.createdBy );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", MilestonesTypesTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (placeType != null) {
			 memberNode.put(placeTypeTypeId, placeType);
		 }
		 if (placeName != null) {
			 memberNode.put(placeNameTypeId, placeName);
		 }
		 if (mapsId != null) {
			 memberNode.put(mapsIdTypeId, mapsId);
		 }
		 if (rank != null) {
			 memberNode.put(rankTypeId, rank);
		 }
		 if (modifiedBy != null) {
			 memberNode.put(modifiedByTypeId, modifiedBy);
		 }
		 if (createdBy != null) {
			 memberNode.put(createdByTypeId, createdBy);
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 return classNode;
	 } 
	 public static MilestonesTypes parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 MilestonesTypes  c= new MilestonesTypes(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.placeType = fields.findPath(placeTypeTypeId).textValue();
			 c.placeName = fields.findPath(placeNameTypeId).textValue();
			 c.mapsId = fields.findPath(mapsIdTypeId).textValue();
			 c.rank = fields.findPath(rankTypeId).textValue();
			 c.modifiedBy = fields.findPath(modifiedByTypeId).textValue();
			 c.createdBy = fields.findPath(createdByTypeId).textValue();
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 return c;
		} else
			 return null;
	 } 
} 
 
