package com.mapped.persistence;
 
import java.util.*;
	 import com.mapped.persistence.util.*;
 import java.sql.*; 
 import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import play.libs.Json;
 public class MilestonesLike { 
 
	 public final static String MilestonesLikeTypeId = "3400"; 
	 protected static String pkTypeId =  "3401"; 
	 private String pk; 
	 protected static String milestonesIdTypeId =  "3402"; 
	 private String milestonesId; 
	 protected static String usersIdTypeId =  "3403"; 
	 private String usersId; 
	 protected static String notesTypeId =  "3404"; 
	 private String notes; 
	 protected static String modifiedByTypeId =  "3490"; 
	 private String modifiedBy; 
	 protected static String createdByTypeId =  "3491"; 
	 private String createdBy; 
	 protected static String statusTypeId =  "3492"; 
	 private long status; 
	 protected static String syncTimestampTypeId =  "3493"; 
	 private long syncTimestamp; 
	 protected static String lastUpdateTimestampTypeId =  "3494"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "3495"; 
	 private long createTimestamp; 

 	 // Contructor 
	 public MilestonesLike () {} 
	 public MilestonesLike ( String pk,  String milestonesId,  String usersId,  String notes,  String modifiedBy,  String createdBy,  long status,  long syncTimestamp,  long lastUpdateTimestamp,  long createTimestamp ) { 
		 this.pk=pk; 
		 this.milestonesId=milestonesId; 
		 this.usersId=usersId; 
		 this.notes=notes; 
		 this.modifiedBy=modifiedBy; 
		 this.createdBy=createdBy; 
		 this.status=status; 
		 this.syncTimestamp=syncTimestamp; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getMilestonesId () { 
	 	 return milestonesId; 
 	 } 
 
 	 public void setMilestonesId(String milestonesId) { 
	 	 this.milestonesId = milestonesId; 
 	 } 
 
 	 public  String getUsersId () { 
	 	 return usersId; 
 	 } 
 
 	 public void setUsersId(String usersId) { 
	 	 this.usersId = usersId; 
 	 } 
 
 	 public  String getNotes () { 
	 	 return notes; 
 	 } 
 
 	 public void setNotes(String notes) { 
	 	 this.notes = notes; 
 	 } 
 
 	 public  String getModifiedBy () { 
	 	 return modifiedBy; 
 	 } 
 
 	 public void setModifiedBy(String modifiedBy) { 
	 	 this.modifiedBy = modifiedBy; 
 	 } 
 
 	 public  String getCreatedBy () { 
	 	 return createdBy; 
 	 } 
 
 	 public void setCreatedBy(String createdBy) { 
	 	 this.createdBy = createdBy; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public Milestones getMilestones () throws Exception{ 
		 return MilestonesMgr.getByPk(milestonesId); 
	}
	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: MilestonesLike"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 MilestonesId: ").append(this.milestonesId );  
		 sb.append("	 UsersId: ").append(this.usersId );  
		 sb.append("	 Notes: ").append(this.notes );  
		 sb.append("	 ModifiedBy: ").append(this.modifiedBy );  
		 sb.append("	 CreatedBy: ").append(this.createdBy );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", MilestonesLikeTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (milestonesId != null) {
			 memberNode.put(milestonesIdTypeId, milestonesId);
		 }
		 if (usersId != null) {
			 memberNode.put(usersIdTypeId, usersId);
		 }
		 if (notes != null) {
			 memberNode.put(notesTypeId, notes);
		 }
		 if (modifiedBy != null) {
			 memberNode.put(modifiedByTypeId, modifiedBy);
		 }
		 if (createdBy != null) {
			 memberNode.put(createdByTypeId, createdBy);
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 return classNode;
	 } 
	 public static MilestonesLike parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 MilestonesLike  c= new MilestonesLike(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.milestonesId = fields.findPath(milestonesIdTypeId).textValue();
			 c.usersId = fields.findPath(usersIdTypeId).textValue();
			 c.notes = fields.findPath(notesTypeId).textValue();
			 c.modifiedBy = fields.findPath(modifiedByTypeId).textValue();
			 c.createdBy = fields.findPath(createdByTypeId).textValue();
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 return c;
		} else
			 return null;
	 } 
} 
 
