package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.utils.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class ChatMsgsDeliveryMgr {

  private static final String SELECT_ALL = "Select Pk, MsgId, UsersId, Status, LastUpdateTimestamp, CreateTimestamp, " +
                                           "SyncTimestamp from ChatMsgsDelivery;";
  private static final String SELECT_ALL_BY_PK = "Select Pk, MsgId, UsersId, Status, LastUpdateTimestamp, " +
                                                 "CreateTimestamp, SyncTimestamp from ChatMsgsDelivery where Pk = ?;";
  private static final String DELETE_BY_PK = "delete from ChatMsgsDelivery where Pk = ?;";
  private static final String INSERT = "insert into ChatMsgsDelivery (Pk, MsgId, UsersId, Status, " +
                                       "LastUpdateTimestamp, CreateTimestamp, SyncTimestamp) values (?, ?, ?, ?, ?, " +
                                       "?, ?);";
  private static final String UPDATE = "update ChatMsgsDelivery SET MsgId=?, UsersId=?, Status=?, " +
                                       "LastUpdateTimestamp=?, SyncTimestamp=? WHERE Pk = ?; ";

  public static ArrayList<ChatMsgsDelivery> getAll()
      throws SQLException {
    ResultSet rs = null;
    PreparedStatement prep = null;
    Connection conn = null;
    try {
      conn = DBConnectionMgr.getConnection4Mobile();
      prep = conn.prepareStatement(SELECT_ALL);
      rs = prep.executeQuery();
      return ChatMsgsDeliveryMgr.handleResults(rs);
    }
    catch (Exception e) {
      Log.err("ChatMsgsDeliveryMgr: Exception", e);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
      DBConnectionMgr.closeConnection(conn);
    }
    return  null;
  }

  public static ArrayList<ChatMsgsDelivery> handleResults(ResultSet rs)
      throws SQLException {
    ArrayList<ChatMsgsDelivery> results = new ArrayList<>();
    if (rs == null) {
      return results;
    }
    while (rs.next()) {
      ChatMsgsDelivery c = new ChatMsgsDelivery();
      c.setPk(rs.getString("Pk"));
      c.setMsgId(rs.getString("MsgId"));
      c.setUsersId(rs.getString("UsersId"));
      c.setStatus(rs.getLong("Status"));
      c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
      c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
      c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
      results.add(c);
    }
    return results;
  }

  public static ChatMsgsDelivery getByPk(String id)
      throws SQLException {
    ResultSet rs = null;
    PreparedStatement prep = null;
    Connection conn = null;
    try {
      conn = DBConnectionMgr.getConnection4Mobile();
      prep = conn.prepareStatement(SELECT_ALL_BY_PK);
      prep.setString(1, id);
      rs = prep.executeQuery();
      ArrayList<ChatMsgsDelivery> results = ChatMsgsDeliveryMgr.handleResults(rs);
      if (results != null && results.size() == 1) {
        return results.get(0);
      }
      else {
        return null;
      }
    }
    catch (Exception e) {
      Log.err("ChatMsgsDeliveryMgr: Exception", e);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
      DBConnectionMgr.closeConnection(conn);
    }
    return  null;
  }

  public static void deleteByPK(String id)
      throws SQLException {
    PreparedStatement prep = null;
    Connection conn = null;
    try {
      conn = DBConnectionMgr.getConnection4Mobile();
      prep = conn.prepareStatement(DELETE_BY_PK);
      prep.setString(1, id);
      prep.execute();
    }
    catch (Exception e) {
      Log.err("ChatMsgsDeliveryMgr: Exception", e);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      DBConnectionMgr.closeConnection(conn);
    }
  }

  public static void insert(ChatMsgsDelivery c)
      throws SQLException {
    PreparedStatement prep = null;
    Connection conn = null;
    try {
      conn = DBConnectionMgr.getConnection4Mobile();
      prep = conn.prepareStatement(INSERT);
      prep.setString(1, c.getPk());
      prep.setString(2, c.getMsgId());
      prep.setString(3, c.getUsersId());
      prep.setLong(4, c.getStatus());
      prep.setLong(5, c.getLastUpdateTimestamp());
      prep.setLong(6, c.getCreateTimestamp());
      prep.setLong(7, c.getSyncTimestamp());
      prep.execute();
    }
    catch (Exception e) {
      Log.err("ChatMsgsDeliveryMgr: Exception", e);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      DBConnectionMgr.closeConnection(conn);
    }
  }

  public static void update(ChatMsgsDelivery c)
      throws SQLException {
    Connection conn = null;
    PreparedStatement prep = null;
    try {
      conn = DBConnectionMgr.getConnection4Mobile();
      prep = conn.prepareStatement(UPDATE);
      prep.setString(1, c.getMsgId());
      prep.setString(2, c.getUsersId());
      prep.setLong(3, c.getStatus());
      prep.setLong(4, c.getLastUpdateTimestamp());
      prep.setLong(5, c.getSyncTimestamp());
      prep.setString(6, c.getPk());
      prep.execute();
    }
    catch (Exception e) {
      Log.err("ChatMsgsDeliveryMgr: Exception", e);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      DBConnectionMgr.closeConnection(conn);
    }
  }
}
