package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class MilestonesMgr {

	 private static final String SELECT_ALL = "Select Pk, Note, Proximity, ProximityAlert, Rank, Latitude, Longitude, Bearing, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, Name, Address, StartDate, EndDate, MapsId, SyncTimestamp, Tag, PublisherNote from Milestones;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, Note, Proximity, ProximityAlert, Rank, Latitude, Longitude, Bearing, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, Name, Address, StartDate, EndDate, MapsId, SyncTimestamp, Tag, PublisherNote from Milestones where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from Milestones where Pk = ?;";
	 private static final String INSERT = "insert into Milestones (Pk, Note, Proximity, ProximityAlert, Rank, Latitude, Longitude, Bearing, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, Name, Address, StartDate, EndDate, MapsId, SyncTimestamp, Tag, PublisherNote) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update Milestones SET Note=?, Proximity=?, ProximityAlert=?, Rank=?, Latitude=?, Longitude=?, Bearing=?, Status=?, LastUpdateTimestamp=?, ModifiedBy=?, Name=?, Address=?, StartDate=?, EndDate=?, MapsId=?, SyncTimestamp=?, Tag=?, PublisherNote=? WHERE Pk = ?; ";
	 private static final String SELECT_ALL_BY_MAPS_ID = "Select Pk, Note, Proximity, ProximityAlert, Rank, Latitude, Longitude, Bearing, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, Name, Address, StartDate, EndDate, MapsId, SyncTimestamp, Tag, PublisherNote from Milestones where MapsId=? ";
	 private static final String DELETE_BY_MAPS_ID = "delete from Milestones where MapsId=? ";
	 public static ArrayList <Milestones> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return MilestonesMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static Milestones getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<Milestones> results = MilestonesMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (Milestones c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getNote());
				 prep.setString(3, c.getProximity());
				 prep.setString(4, c.getProximityAlert());
				 prep.setString(5, c.getRank());
				 prep.setString(6, c.getLatitude());
				 prep.setString(7, c.getLongitude());
				 prep.setString(8, c.getBearing());
				 prep.setLong(9, c.getStatus());
				 prep.setLong(10, c.getLastUpdateTimestamp());
				 prep.setLong(11, c.getCreateTimestamp());
				 prep.setString(12, c.getModifiedBy());
				 prep.setString(13, c.getCreatedBy());
				 prep.setString(14, c.getName());
				 prep.setString(15, c.getAddress());
				 prep.setString(16, c.getStartDate());
				 prep.setString(17, c.getEndDate());
				 prep.setString(18, c.getMapsId());
				 prep.setLong(19, c.getSyncTimestamp());
				 prep.setString(20, c.getTag());
				 prep.setString(21, c.getPublisherNote());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (Milestones c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getNote());
				 prep.setString(2, c.getProximity());
				 prep.setString(3, c.getProximityAlert());
				 prep.setString(4, c.getRank());
				 prep.setString(5, c.getLatitude());
				 prep.setString(6, c.getLongitude());
				 prep.setString(7, c.getBearing());
				 prep.setLong(8, c.getStatus());
				 prep.setLong(9, c.getLastUpdateTimestamp());
				 prep.setString(10, c.getModifiedBy());
				 prep.setString(11, c.getName());
				 prep.setString(12, c.getAddress());
				 prep.setString(13, c.getStartDate());
				 prep.setString(14, c.getEndDate());
				 prep.setString(15, c.getMapsId());
				 prep.setLong(16, c.getSyncTimestamp());
				 prep.setString(17, c.getTag());
				 prep.setString(18, c.getPublisherNote());
				 prep.setString(19, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <Milestones> getAllByMapsId(String id) throws SQLException {
		 ResultSet rs = null;
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_MAPS_ID);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 return MilestonesMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void deleteByMapsId (String id) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_MAPS_ID);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <Milestones> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <Milestones> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 Milestones c = new Milestones();
			 c.setPk(rs.getString("Pk"));
			 c.setNote(rs.getString("Note"));
			 c.setProximity(rs.getString("Proximity"));
			 c.setProximityAlert(rs.getString("ProximityAlert"));
			 c.setRank(rs.getString("Rank"));
			 c.setLatitude(rs.getString("Latitude"));
			 c.setLongitude(rs.getString("Longitude"));
			 c.setBearing(rs.getString("Bearing"));
			 c.setStatus(rs.getLong("Status"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 c.setModifiedBy(rs.getString("ModifiedBy"));
			 c.setCreatedBy(rs.getString("CreatedBy"));
			 c.setName(rs.getString("Name"));
			 c.setAddress(rs.getString("Address"));
			 c.setStartDate(rs.getString("StartDate"));
			 c.setEndDate(rs.getString("EndDate"));
			 c.setMapsId(rs.getString("MapsId"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 c.setTag(rs.getString("Tag"));
			 c.setPublisherNote(rs.getString("PublisherNote"));
			 results.add(c);
		 }
		 return results;
	 }
}
