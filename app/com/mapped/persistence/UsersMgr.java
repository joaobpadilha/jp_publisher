package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class UsersMgr {

	 private static final String SELECT_ALL = "Select Pk, Email, FName, LName, Pwd, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, SyncTimestamp, AuthCode from Users;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, Email, FName, LName, Pwd, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, SyncTimestamp, AuthCode from Users where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from Users where Pk = ?;";
	 private static final String INSERT = "insert into Users (Pk, Email, FName, LName, Pwd, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, SyncTimestamp, AuthCode) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update Users SET Email=?, FName=?, LName=?, Pwd=?, Status=?, LastUpdateTimestamp=?, ModifiedBy=?, SyncTimestamp=?, AuthCode=? WHERE Pk = ?; ";
	 public static ArrayList <Users> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return UsersMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static Users getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<Users> results = UsersMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (Users c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getEmail());
				 prep.setString(3, c.getFName());
				 prep.setString(4, c.getLName());
				 prep.setString(5, c.getPwd());
				 prep.setLong(6, c.getStatus());
				 prep.setLong(7, c.getLastUpdateTimestamp());
				 prep.setLong(8, c.getCreateTimestamp());
				 prep.setString(9, c.getModifiedBy());
				 prep.setString(10, c.getCreatedBy());
				 prep.setLong(11, c.getSyncTimestamp());
				 prep.setString(12, c.getAuthCode());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (Users c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getEmail());
				 prep.setString(2, c.getFName());
				 prep.setString(3, c.getLName());
				 prep.setString(4, c.getPwd());
				 prep.setLong(5, c.getStatus());
				 prep.setLong(6, c.getLastUpdateTimestamp());
				 prep.setString(7, c.getModifiedBy());
				 prep.setLong(8, c.getSyncTimestamp());
				 prep.setString(9, c.getAuthCode());
				 prep.setString(10, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <Users> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <Users> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 Users c = new Users();
			 c.setPk(rs.getString("Pk"));
			 c.setEmail(rs.getString("Email"));
			 c.setFName(rs.getString("FName"));
			 c.setLName(rs.getString("LName"));
			 c.setPwd(rs.getString("Pwd"));
			 c.setStatus(rs.getLong("Status"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 c.setModifiedBy(rs.getString("ModifiedBy"));
			 c.setCreatedBy(rs.getString("CreatedBy"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 c.setAuthCode(rs.getString("AuthCode"));
			 results.add(c);
		 }
		 return results;
	 }
}
