package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class SyncStatusMgr {

	 private static final String SELECT_ALL = "Select Pk, UsersId, DevId, StartTimestamp, EndTimestamp, ClientTimestamp, ServerTimestamp, Status, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp from SyncStatus;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, UsersId, DevId, StartTimestamp, EndTimestamp, ClientTimestamp, ServerTimestamp, Status, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp from SyncStatus where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from SyncStatus where Pk = ?;";
	 private static final String INSERT = "insert into SyncStatus (Pk, UsersId, DevId, StartTimestamp, EndTimestamp, ClientTimestamp, ServerTimestamp, Status, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update SyncStatus SET UsersId=?, DevId=?, StartTimestamp=?, EndTimestamp=?, ClientTimestamp=?, ServerTimestamp=?, Status=?, LastUpdateTimestamp=?, SyncTimestamp=? WHERE Pk = ?; ";
	 private static final String SELECT_ALL_BY_USERS_ID = "Select Pk, UsersId, DevId, StartTimestamp, EndTimestamp, ClientTimestamp, ServerTimestamp, Status, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp from SyncStatus where UsersId=? ";
	 private static final String DELETE_BY_USERS_ID = "delete from SyncStatus where UsersId=? ";
	 public static ArrayList <SyncStatus> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return SyncStatusMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static SyncStatus getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<SyncStatus> results = SyncStatusMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (SyncStatus c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getUsersId());
				 prep.setString(3, c.getDevId());
				 prep.setLong(4, c.getStartTimestamp());
				 prep.setLong(5, c.getEndTimestamp());
				 prep.setLong(6, c.getClientTimestamp());
				 prep.setLong(7, c.getServerTimestamp());
				 prep.setLong(8, c.getStatus());
				 prep.setLong(9, c.getLastUpdateTimestamp());
				 prep.setLong(10, c.getCreateTimestamp());
				 prep.setLong(11, c.getSyncTimestamp());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (SyncStatus c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getUsersId());
				 prep.setString(2, c.getDevId());
				 prep.setLong(3, c.getStartTimestamp());
				 prep.setLong(4, c.getEndTimestamp());
				 prep.setLong(5, c.getClientTimestamp());
				 prep.setLong(6, c.getServerTimestamp());
				 prep.setLong(7, c.getStatus());
				 prep.setLong(8, c.getLastUpdateTimestamp());
				 prep.setLong(9, c.getSyncTimestamp());
				 prep.setString(10, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <SyncStatus> getAllByUsersId(String id) throws SQLException {
		 ResultSet rs = null;
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_USERS_ID);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 return SyncStatusMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void deleteByUsersId (String id) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_USERS_ID);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <SyncStatus> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <SyncStatus> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 SyncStatus c = new SyncStatus();
			 c.setPk(rs.getString("Pk"));
			 c.setUsersId(rs.getString("UsersId"));
			 c.setDevId(rs.getString("DevId"));
			 c.setStartTimestamp(rs.getLong("StartTimestamp"));
			 c.setEndTimestamp(rs.getLong("EndTimestamp"));
			 c.setClientTimestamp(rs.getLong("ClientTimestamp"));
			 c.setServerTimestamp(rs.getLong("ServerTimestamp"));
			 c.setStatus(rs.getLong("Status"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 results.add(c);
		 }
		 return results;
	 }
}
