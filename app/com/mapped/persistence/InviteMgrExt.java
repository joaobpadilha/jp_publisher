package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 12-07-26
 * Time: 10:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class InviteMgrExt extends  InvitesMgr{
    private static final String UPDATE_STATUS_BY_EMAIL = "update Invites SET Status=?, LastUpdateTimestamp=? WHERE InviteeEmail = ?; ";
    private static final String GET_BY_STATUS = "Select distinct i.InviteeEmail as inviteeemail, u.fname as userid, u.email as useremail, i.LastUpdateTimestamp as lastmodified from Invites i, users u where i.status = ? and u.pk = i.usersid and i.lastupdatetimestamp < ?;";


    public static void updateStatusByEmail (long status, String email) throws SQLException {
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(UPDATE_STATUS_BY_EMAIL);
            prep.setLong(1, status);
            prep.setLong(2, System.currentTimeMillis());
            prep.setString(3, email);
            prep.execute();
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static void updateStatusByEmail (Connection conn, long status, String email) throws SQLException {
        PreparedStatement prep= null;
        Statement stat= null;        try {
            stat = conn.createStatement();
            prep = conn.prepareStatement(UPDATE_STATUS_BY_EMAIL);
            prep.setLong(1, status);
            prep.setLong(2, System.currentTimeMillis());
            prep.setString(3, email);
            prep.execute();
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
        }
    }


    public static List<Map<String, String>> getByStatus (Connection conn, long status, long lastmodified) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(GET_BY_STATUS);
            prep.setLong(1, status);
            prep.setLong(2, lastmodified);

            rs = prep.executeQuery();

            ArrayList< Map<String, String>> results = new ArrayList<>();
            if (rs == null)
                return results;

            while (rs.next()) {
                Map <String, String> rec;
                rec = new HashMap<String, String>();

                rec.put("INVITEE_EMAIL" ,rs.getString("inviteeemail"));
                rec.put("USER_FIRST_NAME", rs.getString("userid"));
                rec.put("USER_EMAIL", rs.getString("useremail"));
                rec.put("LAST_MODIFIED", String.valueOf(rs.getLong("lastmodified")));
                results.add(rec);
            }

            return results;
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
        }
    }

    public static List<Map<String, String>> getNewUsers (Connection conn) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(GET_BY_STATUS);

            rs = prep.executeQuery();

            ArrayList< Map<String, String>> results = new ArrayList<>();
            if (rs == null)
                return results;

            while (rs.next()) {
                Map <String, String> rec;
                rec = new HashMap<String, String>();

                rec.put("INVITEE_EMAIL" ,rs.getString("inviteeemail"));
                rec.put("USER_FIRST_NAME", rs.getString("userid"));
                rec.put("USER_EMAIL", rs.getString("useremail"));
                rec.put("LAST_MODIFIED", String.valueOf(rs.getLong("lastmodified")));
                results.add(rec);
            }

            return results;
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
        }
    }
}
