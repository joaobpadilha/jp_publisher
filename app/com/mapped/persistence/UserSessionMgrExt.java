package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;
import org.joda.time.DateTime;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2012-09-22
 * Time: 11:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserSessionMgrExt extends UserSessionMgr {
    private static final String DELETE_BY_USER_ID_DEVICE_ID = "delete from UserSession where UsersId=? and devid = ? ";
    private static final String GET_SESSION_BY_EMAIL = "select us.Pk, us.UsersId, us.DevId, us.OSVer, us.DevVer, us.Dev, us.SyncTimestamp, us.LastUpdateTimestamp, us.CreateTimestamp, us.Notify " +
                                                       "from UserSession us, users u  where us.UsersId=u.pk and lower(u.email) like ? and us.lastupdatetimestamp > ? order by us.lastupdatetimestamp desc";


    public static void deleteByUserIdandDevice (String userId, String deviceId) throws SQLException {
        PreparedStatement prep= null;
        Connection conn= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            prep = conn.prepareStatement(DELETE_BY_USER_ID_DEVICE_ID);
            prep.setString(1, userId);
            prep.setString(2, deviceId);
            prep.execute();
        } finally {
            if (prep != null)
                prep.close();
            DBConnectionMgr.closeConnection(conn);
        }
    }




  public static List<UserSession> getSessionByEmail (String email) throws SQLException {
    PreparedStatement prep= null;
    Connection conn= null;
    ResultSet rs = null;
    DateTime dateTime = new DateTime();
    List<String> sentDevices = new ArrayList<>();

    try {
      conn = DBConnectionMgr.getConnection4Mobile();
      prep = conn.prepareStatement(GET_SESSION_BY_EMAIL);
      if (email != null) {
        email = email.toLowerCase();
      }
      prep.setString(1, email);
      prep.setLong(2, dateTime.minusYears(1).getMillis());

      rs = prep.executeQuery();
      ArrayList<UserSession> sessions =  UserSessionMgr.handleResults(rs);

      Map<String, UserSession> uniqueSessions = new HashMap<>();
      if (sessions != null && sessions.size() > 0) {
        for (UserSession us: sessions) {
          if (us.getDev() != null && us.getDevVer() != null && us.getDevId() != null && us.getDevId().trim().length() > 37  && !uniqueSessions.containsKey(us.getDevId().trim())) {
              String devPK = us.getDev() + us.getDevVer();
              if (!sentDevices.contains(devPK)) {
                  sentDevices.add(devPK);
                  uniqueSessions.put(us.getDevId().trim(), us);
              }
          }
        }
      }
      return (new ArrayList<UserSession>(uniqueSessions.values()));


    } finally {
      if (prep != null)
        prep.close();
      DBConnectionMgr.closeConnection(conn);
    }

  }

}

