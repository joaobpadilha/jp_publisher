package com.mapped.persistence;
 
import java.util.*;
	 import com.mapped.persistence.util.*;
 import java.sql.*; 
 import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import play.libs.Json;
 public class NotificationMsgs { 
 
	 public final static String NotificationMsgsTypeId = "260"; 
	 protected static String pkTypeId =  "261"; 
	 private String pk; 
	 protected static String devIdTypeId =  "262"; 
	 private String devId; 
	 protected static String usersIdTypeId =  "263"; 
	 private String usersId; 
	 protected static String msgTypeId =  "264"; 
	 private String msg; 
	 protected static String lastUpdateTimestampTypeId =  "265"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "266"; 
	 private long createTimestamp; 
	 protected static String statusTypeId =  "267"; 
	 private long status; 
	 protected static String syncTimestampTypeId =  "268"; 
	 private long syncTimestamp; 

 	 // Contructor 
	 public NotificationMsgs () {} 
	 public NotificationMsgs ( String pk,  String devId,  String usersId,  String msg,  long lastUpdateTimestamp,  long createTimestamp,  long status,  long syncTimestamp ) { 
		 this.pk=pk; 
		 this.devId=devId; 
		 this.usersId=usersId; 
		 this.msg=msg; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
		 this.status=status; 
		 this.syncTimestamp=syncTimestamp; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getDevId () { 
	 	 return devId; 
 	 } 
 
 	 public void setDevId(String devId) { 
	 	 this.devId = devId; 
 	 } 
 
 	 public  String getUsersId () { 
	 	 return usersId; 
 	 } 
 
 	 public void setUsersId(String usersId) { 
	 	 this.usersId = usersId; 
 	 } 
 
 	 public  String getMsg () { 
	 	 return msg; 
 	 } 
 
 	 public void setMsg(String msg) { 
	 	 this.msg = msg; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: NotificationMsgs"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 DevId: ").append(this.devId );  
		 sb.append("	 UsersId: ").append(this.usersId );  
		 sb.append("	 Msg: ").append(this.msg );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", NotificationMsgsTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (devId != null) {
			 memberNode.put(devIdTypeId, devId);
		 }
		 if (usersId != null) {
			 memberNode.put(usersIdTypeId, usersId);
		 }
		 if (msg != null) {
			 memberNode.put(msgTypeId, msg);
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 return classNode;
	 } 
	 public static NotificationMsgs parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 NotificationMsgs  c= new NotificationMsgs(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.devId = fields.findPath(devIdTypeId).textValue();
			 c.usersId = fields.findPath(usersIdTypeId).textValue();
			 c.msg = fields.findPath(msgTypeId).textValue();
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 return c;
		} else
			 return null;
	 } 
} 
 
