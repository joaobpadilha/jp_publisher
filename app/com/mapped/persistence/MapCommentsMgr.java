package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class MapCommentsMgr {

	 private static final String SELECT_ALL = "Select Pk, MapsId, CreatedBy, Msg, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp, Type from MapComments;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, MapsId, CreatedBy, Msg, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp, Type from MapComments where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from MapComments where Pk = ?;";
	 private static final String INSERT = "insert into MapComments (Pk, MapsId, CreatedBy, Msg, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp, Type) values (?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update MapComments SET MapsId=?, Msg=?, LastUpdateTimestamp=?, SyncTimestamp=?, Type=? WHERE Pk = ?; ";
	 public static ArrayList <MapComments> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return MapCommentsMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static MapComments getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<MapComments> results = MapCommentsMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (MapComments c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getMapsId());
				 prep.setString(3, c.getCreatedBy());
				 prep.setString(4, c.getMsg());
				 prep.setLong(5, c.getLastUpdateTimestamp());
				 prep.setLong(6, c.getCreateTimestamp());
				 prep.setLong(7, c.getSyncTimestamp());
				 prep.setString(8, c.getType());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (MapComments c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getMapsId());
				 prep.setString(2, c.getMsg());
				 prep.setLong(3, c.getLastUpdateTimestamp());
				 prep.setLong(4, c.getSyncTimestamp());
				 prep.setString(5, c.getType());
				 prep.setString(6, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <MapComments> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <MapComments> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 MapComments c = new MapComments();
			 c.setPk(rs.getString("Pk"));
			 c.setMapsId(rs.getString("MapsId"));
			 c.setCreatedBy(rs.getString("CreatedBy"));
			 c.setMsg(rs.getString("Msg"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 c.setType(rs.getString("Type"));
			 results.add(c);
		 }
		 return results;
	 }
}
