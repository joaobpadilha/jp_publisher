package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class PageMgr {

	 private static final String SELECT_ALL = "Select Pk, MapsId, Name, Note, PageDate, PageRank, TemplateId, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp, MilestonesId, PageType, PageTag from Page;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, MapsId, Name, Note, PageDate, PageRank, TemplateId, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp, MilestonesId, PageType, PageTag from Page where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from Page where Pk = ?;";
	 private static final String INSERT = "insert into Page (Pk, MapsId, Name, Note, PageDate, PageRank, TemplateId, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp, MilestonesId, PageType, PageTag) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update Page SET MapsId=?, Name=?, Note=?, PageDate=?, PageRank=?, TemplateId=?, ModifiedBy=?, Status=?, SyncTimestamp=?, LastUpdateTimestamp=?, MilestonesId=?, PageType=?, PageTag=? WHERE Pk = ?; ";
	 private static final String SELECT_ALL_BY_MAPS_ID = "Select Pk, MapsId, Name, Note, PageDate, PageRank, TemplateId, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp, MilestonesId, PageType, PageTag from Page where MapsId=? ";

    private static final String DELETE_BY_MAPS_ID = "delete from Page where MapsId=? ";
	 public static ArrayList <Page> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return PageMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static Page getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<Page> results = PageMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (Page c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getMapsId());
				 prep.setString(3, c.getName());
				 prep.setString(4, c.getNote());
				 prep.setString(5, c.getPageDate());
				 prep.setString(6, c.getPageRank());
				 prep.setString(7, c.getTemplateId());
				 prep.setString(8, c.getModifiedBy());
				 prep.setString(9, c.getCreatedBy());
				 prep.setLong(10, c.getStatus());
				 prep.setLong(11, c.getSyncTimestamp());
				 prep.setLong(12, c.getLastUpdateTimestamp());
				 prep.setLong(13, c.getCreateTimestamp());
				 prep.setString(14, c.getMilestonesId());
				 prep.setString(15, c.getPageType());
				 prep.setString(16, c.getPageTag());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (Page c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getMapsId());
				 prep.setString(2, c.getName());
				 prep.setString(3, c.getNote());
				 prep.setString(4, c.getPageDate());
				 prep.setString(5, c.getPageRank());
				 prep.setString(6, c.getTemplateId());
				 prep.setString(7, c.getModifiedBy());
				 prep.setLong(8, c.getStatus());
				 prep.setLong(9, c.getSyncTimestamp());
				 prep.setLong(10, c.getLastUpdateTimestamp());
				 prep.setString(11, c.getMilestonesId());
				 prep.setString(12, c.getPageType());
				 prep.setString(13, c.getPageTag());
				 prep.setString(14, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <Page> getAllByMapsId(String id) throws SQLException {
		 ResultSet rs = null;
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_MAPS_ID);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 return PageMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }



	 public static void deleteByMapsId (String id) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_MAPS_ID);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <Page> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <Page> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 Page c = new Page();
			 c.setPk(rs.getString("Pk"));
			 c.setMapsId(rs.getString("MapsId"));
			 c.setName(rs.getString("Name"));
			 c.setNote(rs.getString("Note"));
			 c.setPageDate(rs.getString("PageDate"));
			 c.setPageRank(rs.getString("PageRank"));
			 c.setTemplateId(rs.getString("TemplateId"));
			 c.setModifiedBy(rs.getString("ModifiedBy"));
			 c.setCreatedBy(rs.getString("CreatedBy"));
			 c.setStatus(rs.getLong("Status"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 c.setMilestonesId(rs.getString("MilestonesId"));
			 c.setPageType(rs.getString("PageType"));
			 c.setPageTag(rs.getString("PageTag"));
			 results.add(c);
		 }
		 return results;
	 }
}
