package com.mapped.persistence;
 
import java.util.*;
	 import com.mapped.persistence.util.*;
 import java.sql.*; 
 import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import com.mapped.persistence.MilestonesLike;
import com.mapped.persistence.MilestonesLikeMgr;
import play.libs.Json;
 public class Milestones { 
 
	 public final static String MilestonesTypeId = "20"; 
	 protected static String pkTypeId =  "21"; 
	 private String pk; 
	 protected static String noteTypeId =  "22"; 
	 private String note; 
	 protected static String proximityTypeId =  "23"; 
	 private String proximity; 
	 protected static String proximityAlertTypeId =  "24"; 
	 private String proximityAlert; 
	 protected static String rankTypeId =  "25"; 
	 private String rank; 
	 protected static String latitudeTypeId =  "26"; 
	 private String latitude; 
	 protected static String longitudeTypeId =  "27"; 
	 private String longitude; 
	 protected static String bearingTypeId =  "28"; 
	 private String bearing; 
	 protected static String statusTypeId =  "29"; 
	 private long status; 
	 protected static String lastUpdateTimestampTypeId =  "30"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "31"; 
	 private long createTimestamp; 
	 protected static String modifiedByTypeId =  "32"; 
	 private String modifiedBy; 
	 protected static String createdByTypeId =  "33"; 
	 private String createdBy; 
	 protected static String nameTypeId =  "34"; 
	 private String name; 
	 protected static String addressTypeId =  "35"; 
	 private String address; 
	 protected static String startDateTypeId =  "36"; 
	 private String startDate; 
	 protected static String endDateTypeId =  "37"; 
	 private String endDate; 
	 protected static String mapsIdTypeId =  "38"; 
	 private String mapsId; 
	 protected static String syncTimestampTypeId =  "39"; 
	 private long syncTimestamp; 
	 protected static String tagTypeId =  "40"; 
	 private String tag; 
	 protected static String publisherNoteTypeId =  "41"; 
	 private String publisherNote; 

 	 // Contructor 
	 public Milestones () {} 
	 public Milestones ( String pk,  String note,  String proximity,  String proximityAlert,  String rank,  String latitude,  String longitude,  String bearing,  long status,  long lastUpdateTimestamp,  long createTimestamp,  String modifiedBy,  String createdBy,  String name,  String address,  String startDate,  String endDate,  String mapsId,  long syncTimestamp,  String tag,  String publisherNote ) { 
		 this.pk=pk; 
		 this.note=note; 
		 this.proximity=proximity; 
		 this.proximityAlert=proximityAlert; 
		 this.rank=rank; 
		 this.latitude=latitude; 
		 this.longitude=longitude; 
		 this.bearing=bearing; 
		 this.status=status; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
		 this.modifiedBy=modifiedBy; 
		 this.createdBy=createdBy; 
		 this.name=name; 
		 this.address=address; 
		 this.startDate=startDate; 
		 this.endDate=endDate; 
		 this.mapsId=mapsId; 
		 this.syncTimestamp=syncTimestamp; 
		 this.tag=tag; 
		 this.publisherNote=publisherNote; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getNote () { 
	 	 return note; 
 	 } 
 
 	 public void setNote(String note) { 
	 	 this.note = note; 
 	 } 
 
 	 public  String getProximity () { 
	 	 return proximity; 
 	 } 
 
 	 public void setProximity(String proximity) { 
	 	 this.proximity = proximity; 
 	 } 
 
 	 public  String getProximityAlert () { 
	 	 return proximityAlert; 
 	 } 
 
 	 public void setProximityAlert(String proximityAlert) { 
	 	 this.proximityAlert = proximityAlert; 
 	 } 
 
 	 public  String getRank () { 
	 	 return rank; 
 	 } 
 
 	 public void setRank(String rank) { 
	 	 this.rank = rank; 
 	 } 
 
 	 public  String getLatitude () { 
	 	 return latitude; 
 	 } 
 
 	 public void setLatitude(String latitude) { 
	 	 this.latitude = latitude; 
 	 } 
 
 	 public  String getLongitude () { 
	 	 return longitude; 
 	 } 
 
 	 public void setLongitude(String longitude) { 
	 	 this.longitude = longitude; 
 	 } 
 
 	 public  String getBearing () { 
	 	 return bearing; 
 	 } 
 
 	 public void setBearing(String bearing) { 
	 	 this.bearing = bearing; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public  String getModifiedBy () { 
	 	 return modifiedBy; 
 	 } 
 
 	 public void setModifiedBy(String modifiedBy) { 
	 	 this.modifiedBy = modifiedBy; 
 	 } 
 
 	 public  String getCreatedBy () { 
	 	 return createdBy; 
 	 } 
 
 	 public void setCreatedBy(String createdBy) { 
	 	 this.createdBy = createdBy; 
 	 } 
 
 	 public  String getName () { 
	 	 return name; 
 	 } 
 
 	 public void setName(String name) { 
	 	 this.name = name; 
 	 } 
 
 	 public  String getAddress () { 
	 	 return address; 
 	 } 
 
 	 public void setAddress(String address) { 
	 	 this.address = address; 
 	 } 
 
 	 public  String getStartDate () { 
	 	 return startDate; 
 	 } 
 
 	 public void setStartDate(String startDate) { 
	 	 this.startDate = startDate; 
 	 } 
 
 	 public  String getEndDate () { 
	 	 return endDate; 
 	 } 
 
 	 public void setEndDate(String endDate) { 
	 	 this.endDate = endDate; 
 	 } 
 
 	 public  String getMapsId () { 
	 	 return mapsId; 
 	 } 
 
 	 public void setMapsId(String mapsId) { 
	 	 this.mapsId = mapsId; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public  String getTag () { 
	 	 return tag; 
 	 } 
 
 	 public void setTag(String tag) { 
	 	 this.tag = tag; 
 	 } 
 
 	 public  String getPublisherNote () { 
	 	 return publisherNote; 
 	 } 
 
 	 public void setPublisherNote(String publisherNote) { 
	 	 this.publisherNote = publisherNote; 
 	 } 
 
 	 public ArrayList <MilestonesLike>  getMilestonesLike () throws Exception{ 
		 return MilestonesLikeMgr.getAllByMilestonesId(pk); 
	}
	 public Maps getMaps () throws Exception{ 
		 return MapsMgr.getByPk(mapsId); 
	}
	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: Milestones"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 Note: ").append(this.note );  
		 sb.append("	 Proximity: ").append(this.proximity );  
		 sb.append("	 ProximityAlert: ").append(this.proximityAlert );  
		 sb.append("	 Rank: ").append(this.rank );  
		 sb.append("	 Latitude: ").append(this.latitude );  
		 sb.append("	 Longitude: ").append(this.longitude );  
		 sb.append("	 Bearing: ").append(this.bearing );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 sb.append("	 ModifiedBy: ").append(this.modifiedBy );  
		 sb.append("	 CreatedBy: ").append(this.createdBy );  
		 sb.append("	 Name: ").append(this.name );  
		 sb.append("	 Address: ").append(this.address );  
		 sb.append("	 StartDate: ").append(this.startDate );  
		 sb.append("	 EndDate: ").append(this.endDate );  
		 sb.append("	 MapsId: ").append(this.mapsId );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 sb.append("	 Tag: ").append(this.tag );  
		 sb.append("	 PublisherNote: ").append(this.publisherNote );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", MilestonesTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (note != null) {
			 memberNode.put(noteTypeId, note);
		 }
		 if (proximity != null) {
			 memberNode.put(proximityTypeId, proximity);
		 }
		 if (proximityAlert != null) {
			 memberNode.put(proximityAlertTypeId, proximityAlert);
		 }
		 if (rank != null) {
			 memberNode.put(rankTypeId, rank);
		 }
		 if (latitude != null) {
			 memberNode.put(latitudeTypeId, latitude);
		 }
		 if (longitude != null) {
			 memberNode.put(longitudeTypeId, longitude);
		 }
		 if (bearing != null) {
			 memberNode.put(bearingTypeId, bearing);
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 if (modifiedBy != null) {
			 memberNode.put(modifiedByTypeId, modifiedBy);
		 }
		 if (createdBy != null) {
			 memberNode.put(createdByTypeId, createdBy);
		 }
		 if (name != null) {
			 memberNode.put(nameTypeId, name);
		 }
		 if (address != null) {
			 memberNode.put(addressTypeId, address);
		 }
		 if (startDate != null) {
			 memberNode.put(startDateTypeId, startDate);
		 }
		 if (endDate != null) {
			 memberNode.put(endDateTypeId, endDate);
		 }
		 if (mapsId != null) {
			 memberNode.put(mapsIdTypeId, mapsId);
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 if (tag != null) {
			 memberNode.put(tagTypeId, tag);
		 }
		 if (publisherNote != null) {
			 memberNode.put(publisherNoteTypeId, publisherNote);
		 }
		 return classNode;
	 } 
	 public static Milestones parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 Milestones  c= new Milestones(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.note = fields.findPath(noteTypeId).textValue();
			 c.proximity = fields.findPath(proximityTypeId).textValue();
			 c.proximityAlert = fields.findPath(proximityAlertTypeId).textValue();
			 c.rank = fields.findPath(rankTypeId).textValue();
			 c.latitude = fields.findPath(latitudeTypeId).textValue();
			 c.longitude = fields.findPath(longitudeTypeId).textValue();
			 c.bearing = fields.findPath(bearingTypeId).textValue();
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 c.modifiedBy = fields.findPath(modifiedByTypeId).textValue();
			 c.createdBy = fields.findPath(createdByTypeId).textValue();
			 c.name = fields.findPath(nameTypeId).textValue();
			 c.address = fields.findPath(addressTypeId).textValue();
			 c.startDate = fields.findPath(startDateTypeId).textValue();
			 c.endDate = fields.findPath(endDateTypeId).textValue();
			 c.mapsId = fields.findPath(mapsIdTypeId).textValue();
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 c.tag = fields.findPath(tagTypeId).textValue();
			 c.publisherNote = fields.findPath(publisherNoteTypeId).textValue();
			 return c;
		} else
			 return null;
	 } 
} 
 
