package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.utils.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class ChatSessionMgr {

  private static final String SELECT_ALL = "Select Pk, Name, Status, LastUpdateTimestamp, CreateTimestamp, " +
                                           "SyncTimestamp, MapsId from ChatSession;";
  private static final String SELECT_ALL_BY_PK = "Select Pk, Name, Status, LastUpdateTimestamp, CreateTimestamp, " +
                                                 "SyncTimestamp, MapsId from ChatSession where Pk = ?;";
  private static final String DELETE_BY_PK = "delete from ChatSession where Pk = ?;";
  private static final String INSERT = "insert into ChatSession (Pk, Name, Status, LastUpdateTimestamp, " +
                                       "CreateTimestamp, SyncTimestamp, MapsId) values (?, ?, ?, ?, ?, ?, ?);";
  private static final String UPDATE = "update ChatSession SET Name=?, Status=?, LastUpdateTimestamp=?, " +
                                       "SyncTimestamp=?, MapsId=? WHERE Pk = ?; ";

  public static ArrayList<ChatSession> getAll()
      throws SQLException {
    ResultSet rs = null;
    PreparedStatement prep = null;
    Connection conn = null;
    try {
      conn = DBConnectionMgr.getConnection4Mobile();
      prep = conn.prepareStatement(SELECT_ALL);
      rs = prep.executeQuery();
      return ChatSessionMgr.handleResults(rs);
    }
    catch (Exception e) {
      Log.err("ChatMsgsMgr: Exception", e);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
      DBConnectionMgr.closeConnection(conn);
    }
    return null;
  }

  public static ArrayList<ChatSession> handleResults(ResultSet rs)
      throws SQLException {
    ArrayList<ChatSession> results = new ArrayList<>();
    if (rs == null) {
      return results;
    }
    while (rs.next()) {
      ChatSession c = new ChatSession();
      c.setPk(rs.getString("Pk"));
      c.setName(rs.getString("Name"));
      c.setStatus(rs.getLong("Status"));
      c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
      c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
      c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
      c.setMapsId(rs.getString("MapsId"));
      results.add(c);
    }
    return results;
  }

  public static ChatSession getByPk(String id)
      throws SQLException {
    ResultSet rs = null;
    PreparedStatement prep = null;
    Connection conn = null;
    try {
      conn = DBConnectionMgr.getConnection4Mobile();
      prep = conn.prepareStatement(SELECT_ALL_BY_PK);
      prep.setString(1, id);
      rs = prep.executeQuery();
      ArrayList<ChatSession> results = ChatSessionMgr.handleResults(rs);
      if (results != null && results.size() == 1) {
        return results.get(0);
      }
      else {
        return null;
      }
    }
    catch (Exception e) {
      Log.err("ChatMsgsMgr: Exception", e);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
      DBConnectionMgr.closeConnection(conn);
    }
    return  null;
  }

  public static void deleteByPK(String id)
      throws SQLException {
    PreparedStatement prep = null;
    Connection conn = null;
    try {
      conn = DBConnectionMgr.getConnection4Mobile();
      prep = conn.prepareStatement(DELETE_BY_PK);
      prep.setString(1, id);
      prep.execute();
    }
    catch (Exception e) {
      Log.err("ChatMsgsMgr: Exception", e);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      DBConnectionMgr.closeConnection(conn);
    }
  }

  public static void insert(ChatSession c)
      throws SQLException {
    PreparedStatement prep = null;
    Connection conn = null;
    try {
      conn = DBConnectionMgr.getConnection4Mobile();
      prep = conn.prepareStatement(INSERT);
      prep.setString(1, c.getPk());
      prep.setString(2, c.getName());
      prep.setLong(3, c.getStatus());
      prep.setLong(4, c.getLastUpdateTimestamp());
      prep.setLong(5, c.getCreateTimestamp());
      prep.setLong(6, c.getSyncTimestamp());
      prep.setString(7, c.getMapsId());
      prep.execute();
    }
    catch (Exception e) {
      Log.err("ChatMsgsMgr: Exception", e);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      DBConnectionMgr.closeConnection(conn);
    }
  }

  public static void update(ChatSession c)
      throws SQLException {
    Connection conn = null;
    PreparedStatement prep = null;
    try {
      conn = DBConnectionMgr.getConnection4Mobile();
      prep = conn.prepareStatement(UPDATE);
      prep.setString(1, c.getName());
      prep.setLong(2, c.getStatus());
      prep.setLong(3, c.getLastUpdateTimestamp());
      prep.setLong(4, c.getSyncTimestamp());
      prep.setString(5, c.getMapsId());
      prep.setString(6, c.getPk());
      prep.execute();
    }
    catch (Exception e) {
      Log.err("ChatMsgsMgr: Exception", e);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      DBConnectionMgr.closeConnection(conn);
    }
  }
}
