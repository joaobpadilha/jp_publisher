package com.mapped.persistence;
 
import java.util.*;
	 import com.mapped.persistence.util.*;
 import java.sql.*; 
 import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import play.libs.Json;
 public class ChatUsers { 
 
	 public final static String ChatUsersTypeId = "190"; 
	 protected static String pkTypeId =  "191"; 
	 private String pk; 
	 protected static String chatIdTypeId =  "192"; 
	 private String chatId; 
	 protected static String usersIdTypeId =  "193"; 
	 private String usersId; 
	 protected static String lastUpdateTimestampTypeId =  "194"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "195"; 
	 private long createTimestamp; 
	 protected static String statusTypeId =  "196"; 
	 private long status; 
	 protected static String syncTimestampTypeId =  "197"; 
	 private long syncTimestamp; 

 	 // Contructor 
	 public ChatUsers () {} 
	 public ChatUsers ( String pk,  String chatId,  String usersId,  long lastUpdateTimestamp,  long createTimestamp,  long status,  long syncTimestamp ) { 
		 this.pk=pk; 
		 this.chatId=chatId; 
		 this.usersId=usersId; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
		 this.status=status; 
		 this.syncTimestamp=syncTimestamp; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getChatId () { 
	 	 return chatId; 
 	 } 
 
 	 public void setChatId(String chatId) { 
	 	 this.chatId = chatId; 
 	 } 
 
 	 public  String getUsersId () { 
	 	 return usersId; 
 	 } 
 
 	 public void setUsersId(String usersId) { 
	 	 this.usersId = usersId; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: ChatUsers"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 ChatId: ").append(this.chatId );  
		 sb.append("	 UsersId: ").append(this.usersId );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", ChatUsersTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (chatId != null) {
			 memberNode.put(chatIdTypeId, chatId);
		 }
		 if (usersId != null) {
			 memberNode.put(usersIdTypeId, usersId);
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 return classNode;
	 } 
	 public static ChatUsers parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 ChatUsers  c= new ChatUsers(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.chatId = fields.findPath(chatIdTypeId).textValue();
			 c.usersId = fields.findPath(usersIdTypeId).textValue();
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 return c;
		} else
			 return null;
	 } 
} 
 
