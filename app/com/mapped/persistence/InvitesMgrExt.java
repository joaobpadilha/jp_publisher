package com.mapped.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-27
 * Time: 2:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class InvitesMgrExt extends InvitesMgr {
    private static final String INSERT = "insert into Invites (Pk, InviteeEmail, UsersId, Status, LastUpdateTimestamp, CreateTimestamp) values (?, ?, ?, ?, ?, ?);";
    private static final String UPDATE = "update Invites SET InviteeEmail=?, UsersId=?, Status=?, LastUpdateTimestamp=? WHERE Pk = ?; ";

    public static void insert (Invites c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(INSERT);
            prep.setString(1, c.getPk());
            prep.setString(2, c.getInviteeEmail());
            prep.setString(3, c.getUsersId());
            prep.setLong(4, c.getStatus());
            prep.setLong(5, c.getLastUpdateTimestamp());
            prep.setLong(6, c.getCreateTimestamp());
            prep.execute();
        } finally {
            if (prep != null)
                prep.close();
        }
    }

    public static int update (Invites c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(UPDATE);
            prep.setString(1, c.getInviteeEmail());
            prep.setString(2, c.getUsersId());
            prep.setLong(3, c.getStatus());
            prep.setLong(4, c.getLastUpdateTimestamp());
            prep.setString(5, c.getPk());
            return prep.executeUpdate();
        } finally {
            if (prep != null)
                prep.close();
        }
    }

}
