package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;

import java.sql.*;
import java.util.ArrayList;


public class MapsMgrExt extends MapsMgr {

     private static final String SELECT_ALL_BY_OWNER_LASTUPATE = "Select Pk, Name, Note, Date, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, MapType, StartDate, EndDate, SyncTimestamp, tag from Maps where createdby = ? and (SyncTimestamp = 0 or SyncTimestamp > ?);";
    private static final String SELECT_SHARED_MAPS_BY_USER_LASTUPATE = "Select Pk, Name, Note, Date, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, MapType, StartDate, EndDate, SyncTimestamp, tag from Maps where Pk in (select mapsId from sharedmaps where shareuserid = ? and status = 0) and (SyncTimestamp = 0 or SyncTimestamp > ?);";
    private static final String SELECT_NEW_SHARED_MAPS_BY_USER_LASTUPATE = "Select Pk, Name, Note, Date, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, MapType, StartDate, EndDate, SyncTimestamp, tag from Maps where Pk in (select mapsId from sharedmaps where shareuserid = ? and status = 0 and (SyncTimestamp = 0 or SyncTimestamp > ?) and createtimestamp = lastupdatetimestamp);";
    private static final String INSERT = "insert into Maps (Pk, Name, Note, Date, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, MapType, StartDate, EndDate, SyncTimestamp, Tag) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String UPDATE = "update Maps SET Name=?, Note=?, Date=?, Status=?, LastUpdateTimestamp=?, ModifiedBy=?, MapType=?, StartDate=?, EndDate=?, SyncTimestamp=?, Tag=? WHERE Pk = ?; ";

    public static ArrayList <Maps> getAllByOwnerSinceLastUpdate(String userId, long lastUpdate) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_ALL_BY_OWNER_LASTUPATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdate);
            rs = prep.executeQuery();
            return (MapsMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <Maps> getSharedByUserSinceLastUpdate(String userId, long lastUpdate) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_SHARED_MAPS_BY_USER_LASTUPATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdate);
            rs = prep.executeQuery();


            return (MapsMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <Maps> getNewSharedMapsByUserSinceLastUpdate(String userId, long lastUpdate) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_NEW_SHARED_MAPS_BY_USER_LASTUPATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdate);
            rs = prep.executeQuery();


            return (MapsMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static void insert (Maps c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(INSERT);
            prep.setString(1, c.getPk());
            prep.setString(2, c.getName());
            prep.setString(3, c.getNote());
            prep.setString(4, c.getDate());
            prep.setLong(5, c.getStatus());
            prep.setLong(6, c.getLastUpdateTimestamp());
            prep.setLong(7, c.getCreateTimestamp());
            prep.setString(8, c.getModifiedBy());
            prep.setString(9, c.getCreatedBy());
            prep.setString(10, c.getMapType());
            prep.setString(11, c.getStartDate());
            prep.setString(12, c.getEndDate());
            prep.setLong(13, c.getSyncTimestamp());
            prep.setString(14, c.getTag());
            prep.execute();
        } finally {
            if (prep != null)
                prep.close();
        }
    }

    public static int update (Maps c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(UPDATE);
            prep.setString(1, c.getName());
            prep.setString(2, c.getNote());
            prep.setString(3, c.getDate());
            prep.setLong(4, c.getStatus());
            prep.setLong(5, c.getLastUpdateTimestamp());
            prep.setString(6, c.getModifiedBy());
            prep.setString(7, c.getMapType());
            prep.setString(8, c.getStartDate());
            prep.setString(9, c.getEndDate());
            prep.setLong(10, c.getSyncTimestamp());
            prep.setString(11, c.getTag());
            prep.setString(12, c.getPk());
            return prep.executeUpdate();

        } finally {
            if (prep != null)
                prep.close();
        }
    }


}
