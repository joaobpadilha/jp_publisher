package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class MapsMgr {

	 private static final String SELECT_ALL = "Select Pk, Name, Note, Date, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, MapType, StartDate, EndDate, SyncTimestamp, Tag from Maps;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, Name, Note, Date, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, MapType, StartDate, EndDate, SyncTimestamp, Tag from Maps where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from Maps where Pk = ?;";
	 private static final String INSERT = "insert into Maps (Pk, Name, Note, Date, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, MapType, StartDate, EndDate, SyncTimestamp, Tag) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update Maps SET Name=?, Note=?, Date=?, Status=?, LastUpdateTimestamp=?, ModifiedBy=?, MapType=?, StartDate=?, EndDate=?, SyncTimestamp=?, Tag=? WHERE Pk = ?; ";
	 public static ArrayList <Maps> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return MapsMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static Maps getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<Maps> results = MapsMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (Maps c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getName());
				 prep.setString(3, c.getNote());
				 prep.setString(4, c.getDate());
				 prep.setLong(5, c.getStatus());
				 prep.setLong(6, c.getLastUpdateTimestamp());
				 prep.setLong(7, c.getCreateTimestamp());
				 prep.setString(8, c.getModifiedBy());
				 prep.setString(9, c.getCreatedBy());
				 prep.setString(10, c.getMapType());
				 prep.setString(11, c.getStartDate());
				 prep.setString(12, c.getEndDate());
				 prep.setLong(13, c.getSyncTimestamp());
				 prep.setString(14, c.getTag());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (Maps c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getName());
				 prep.setString(2, c.getNote());
				 prep.setString(3, c.getDate());
				 prep.setLong(4, c.getStatus());
				 prep.setLong(5, c.getLastUpdateTimestamp());
				 prep.setString(6, c.getModifiedBy());
				 prep.setString(7, c.getMapType());
				 prep.setString(8, c.getStartDate());
				 prep.setString(9, c.getEndDate());
				 prep.setLong(10, c.getSyncTimestamp());
				 prep.setString(11, c.getTag());
				 prep.setString(12, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <Maps> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <Maps> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 Maps c = new Maps();
			 c.setPk(rs.getString("Pk"));
			 c.setName(rs.getString("Name"));
			 c.setNote(rs.getString("Note"));
			 c.setDate(rs.getString("Date"));
			 c.setStatus(rs.getLong("Status"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 c.setModifiedBy(rs.getString("ModifiedBy"));
			 c.setCreatedBy(rs.getString("CreatedBy"));
			 c.setMapType(rs.getString("MapType"));
			 c.setStartDate(rs.getString("StartDate"));
			 c.setEndDate(rs.getString("EndDate"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 c.setTag(rs.getString("Tag"));
			 results.add(c);
		 }
		 return results;
	 }
}
