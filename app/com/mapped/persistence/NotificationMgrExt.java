package com.mapped.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2012-10-11
 * Time: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class NotificationMgrExt extends NotificationMsgsMgr {

    public static String GET_IOS_NOTIFICATIONS = "select distinct nm.usersid as userid, us.devid as devid, u.fname as fname from notificationmsgs nm, users u, usersession us where nm.status = 6 and u.pk = nm.usersid and nm.usersid = us.usersid and length(us.devid) > 20 and us.dev like 'iPhone%' order by nm.usersid;";
    public static String DELETE_IOS_NOTIFICATIONS = "delete from notificationmsgs where usersid = ? and status = 6;";
    public static String GET_NEW_USERS = "select distinct nm.usersid as userid, u.fName as fName, u.email as email from notificationmsgs nm, users u where nm.status = 9 and nm.usersid = u.pk;";
    private static final String INSERT = "insert into NotificationMsgs (Pk, DevId, UsersId, Msg, LastUpdateTimestamp, CreateTimestamp, Status, SyncTimestamp) values (?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String UPDATE = "update NotificationMsgs SET DevId=?, UsersId=?, Msg=?, LastUpdateTimestamp=?, Status=?, SyncTimestamp=? WHERE Pk = ?; ";

    public static List <Map<String, String>> getIOSNotificationList (Connection conn) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(GET_IOS_NOTIFICATIONS);
            rs = prep.executeQuery();

            ArrayList< Map<String, String>> results = new ArrayList<>();
            if (rs == null)
                return results;

            while (rs.next()) {
                Map <String, String> rec;
                rec = new HashMap<String, String>();

                rec.put("USER_ID" ,rs.getString("userid"));
                rec.put("DEV_ID", rs.getString("devid"));
                rec.put("FIRST_NAME", rs.getString("fname"));

                results.add(rec);
            }

            return results;
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
        }
    }

    public static List <Map<String, String>> getNewUsers(Connection conn) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(GET_NEW_USERS);
            rs = prep.executeQuery();

            ArrayList< Map<String, String>> results = new ArrayList<>();
            if (rs == null)
                return results;

            while (rs.next()) {
                Map <String, String> rec;
                rec = new HashMap<String, String>();

                rec.put("USER_ID" ,rs.getString("userid"));
                rec.put("FIRST_NAME", rs.getString("fname"));
                rec.put("EMAIL", rs.getString("email"));

                results.add(rec);
            }

            return results;
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
        }
    }

    public static void insert (NotificationMsgs c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(INSERT);
            prep.setString(1, c.getPk());
            prep.setString(2, c.getDevId());
            prep.setString(3, c.getUsersId());
            prep.setString(4, c.getMsg());
            prep.setLong(5, c.getLastUpdateTimestamp());
            prep.setLong(6, c.getCreateTimestamp());
            prep.setLong(7, c.getStatus());
            prep.setLong(8, c.getSyncTimestamp());
            prep.execute();
        } finally {
            if (prep != null)
                prep.close();
        }
    }

    public static int update (NotificationMsgs c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(UPDATE);
            prep.setString(1, c.getDevId());
            prep.setString(2, c.getUsersId());
            prep.setString(3, c.getMsg());
            prep.setLong(4, c.getLastUpdateTimestamp());
            prep.setLong(5, c.getStatus());
            prep.setLong(6, c.getSyncTimestamp());
            prep.setString(7, c.getPk());
            return prep.executeUpdate();
        } finally {
            if (prep != null)
                prep.close();
        }
    }

    public static void deleteNotificationByUserId (Connection conn, String userId) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(DELETE_IOS_NOTIFICATIONS);
            prep.setString(1, userId);
            prep.execute();
        } finally {
            if (prep != null)
                prep.close();
        }
    }
}
