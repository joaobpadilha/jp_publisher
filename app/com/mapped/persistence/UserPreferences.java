package com.mapped.persistence;
 
import java.util.*;
	 import com.mapped.persistence.util.*;
 import java.sql.*; 
 import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import play.libs.Json;
 public class UserPreferences { 
 
	 public final static String UserPreferencesTypeId = "320"; 
	 protected static String pkTypeId =  "321"; 
	 private String pk; 
	 protected static String usersIdTypeId =  "322"; 
	 private String usersId; 
	 protected static String summaryEmailTypeId =  "323"; 
	 private String summaryEmail; 
	 protected static String syncTimestampTypeId =  "328"; 
	 private long syncTimestamp; 
	 protected static String lastUpdateTimestampTypeId =  "329"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "330"; 
	 private long createTimestamp; 

 	 // Contructor 
	 public UserPreferences () {} 
	 public UserPreferences ( String pk,  String usersId,  String summaryEmail,  long syncTimestamp,  long lastUpdateTimestamp,  long createTimestamp ) { 
		 this.pk=pk; 
		 this.usersId=usersId; 
		 this.summaryEmail=summaryEmail; 
		 this.syncTimestamp=syncTimestamp; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getUsersId () { 
	 	 return usersId; 
 	 } 
 
 	 public void setUsersId(String usersId) { 
	 	 this.usersId = usersId; 
 	 } 
 
 	 public  String getSummaryEmail () { 
	 	 return summaryEmail; 
 	 } 
 
 	 public void setSummaryEmail(String summaryEmail) { 
	 	 this.summaryEmail = summaryEmail; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public Users getUsers () throws Exception{ 
		 return UsersMgr.getByPk(usersId); 
	}
	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: UserPreferences"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 UsersId: ").append(this.usersId );  
		 sb.append("	 SummaryEmail: ").append(this.summaryEmail );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", UserPreferencesTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (usersId != null) {
			 memberNode.put(usersIdTypeId, usersId);
		 }
		 if (summaryEmail != null) {
			 memberNode.put(summaryEmailTypeId, summaryEmail);
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 return classNode;
	 } 
	 public static UserPreferences parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 UserPreferences  c= new UserPreferences(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.usersId = fields.findPath(usersIdTypeId).textValue();
			 c.summaryEmail = fields.findPath(summaryEmailTypeId).textValue();
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 return c;
		} else
			 return null;
	 } 
} 
 
