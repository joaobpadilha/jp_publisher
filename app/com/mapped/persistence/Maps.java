package com.mapped.persistence;
 
import java.util.*;
	 import com.mapped.persistence.util.*;
 import java.sql.*; 
 import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import com.mapped.persistence.Milestones;
import com.mapped.persistence.MilestonesMgr;
import com.mapped.persistence.MapPreferences;
import com.mapped.persistence.MapPreferencesMgr;
import play.libs.Json;
 public class Maps { 
 
	 public final static String MapsTypeId = "1"; 
	 protected static String pkTypeId =  "2"; 
	 private String pk; 
	 protected static String nameTypeId =  "3"; 
	 private String name; 
	 protected static String noteTypeId =  "4"; 
	 private String note; 
	 protected static String dateTypeId =  "5"; 
	 private String date; 
	 protected static String statusTypeId =  "7"; 
	 private long status; 
	 protected static String lastUpdateTimestampTypeId =  "8"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "9"; 
	 private long createTimestamp; 
	 protected static String modifiedByTypeId =  "10"; 
	 private String modifiedBy; 
	 protected static String createdByTypeId =  "11"; 
	 private String createdBy; 
	 protected static String mapTypeTypeId =  "12"; 
	 private String mapType; 
	 protected static String startDateTypeId =  "13"; 
	 private String startDate; 
	 protected static String endDateTypeId =  "14"; 
	 private String endDate; 
	 protected static String syncTimestampTypeId =  "15"; 
	 private long syncTimestamp; 
	 protected static String tagTypeId =  "16"; 
	 private String tag; 

 	 // Contructor 
	 public Maps () {} 
	 public Maps ( String pk,  String name,  String note,  String date,  long status,  long lastUpdateTimestamp,  long createTimestamp,  String modifiedBy,  String createdBy,  String mapType,  String startDate,  String endDate,  long syncTimestamp,  String tag ) { 
		 this.pk=pk; 
		 this.name=name; 
		 this.note=note; 
		 this.date=date; 
		 this.status=status; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
		 this.modifiedBy=modifiedBy; 
		 this.createdBy=createdBy; 
		 this.mapType=mapType; 
		 this.startDate=startDate; 
		 this.endDate=endDate; 
		 this.syncTimestamp=syncTimestamp; 
		 this.tag=tag; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getName () { 
	 	 return name; 
 	 } 
 
 	 public void setName(String name) { 
	 	 this.name = name; 
 	 } 
 
 	 public  String getNote () { 
	 	 return note; 
 	 } 
 
 	 public void setNote(String note) { 
	 	 this.note = note; 
 	 } 
 
 	 public  String getDate () { 
	 	 return date; 
 	 } 
 
 	 public void setDate(String date) { 
	 	 this.date = date; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public  String getModifiedBy () { 
	 	 return modifiedBy; 
 	 } 
 
 	 public void setModifiedBy(String modifiedBy) { 
	 	 this.modifiedBy = modifiedBy; 
 	 } 
 
 	 public  String getCreatedBy () { 
	 	 return createdBy; 
 	 } 
 
 	 public void setCreatedBy(String createdBy) { 
	 	 this.createdBy = createdBy; 
 	 } 
 
 	 public  String getMapType () { 
	 	 return mapType; 
 	 } 
 
 	 public void setMapType(String mapType) { 
	 	 this.mapType = mapType; 
 	 } 
 
 	 public  String getStartDate () { 
	 	 return startDate; 
 	 } 
 
 	 public void setStartDate(String startDate) { 
	 	 this.startDate = startDate; 
 	 } 
 
 	 public  String getEndDate () { 
	 	 return endDate; 
 	 } 
 
 	 public void setEndDate(String endDate) { 
	 	 this.endDate = endDate; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public  String getTag () { 
	 	 return tag; 
 	 } 
 
 	 public void setTag(String tag) { 
	 	 this.tag = tag; 
 	 } 
 
 	 public ArrayList <Milestones>  getMilestones () throws Exception{ 
		 return MilestonesMgr.getAllByMapsId(pk); 
	}
	 public ArrayList <MapPreferences>  getMapPreferences () throws Exception{ 
		 return MapPreferencesMgr.getAllByMapsId(pk); 
	}
	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: Maps"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 Name: ").append(this.name );  
		 sb.append("	 Note: ").append(this.note );  
		 sb.append("	 Date: ").append(this.date );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 sb.append("	 ModifiedBy: ").append(this.modifiedBy );  
		 sb.append("	 CreatedBy: ").append(this.createdBy );  
		 sb.append("	 MapType: ").append(this.mapType );  
		 sb.append("	 StartDate: ").append(this.startDate );  
		 sb.append("	 EndDate: ").append(this.endDate );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 sb.append("	 Tag: ").append(this.tag );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", MapsTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (name != null) {
			 memberNode.put(nameTypeId, name);
		 }
		 if (note != null) {
			 memberNode.put(noteTypeId, note);
		 }
		 if (date != null) {
			 memberNode.put(dateTypeId, date);
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 if (modifiedBy != null) {
			 memberNode.put(modifiedByTypeId, modifiedBy);
		 }
		 if (createdBy != null) {
			 memberNode.put(createdByTypeId, createdBy);
		 }
		 if (mapType != null) {
			 memberNode.put(mapTypeTypeId, mapType);
		 }
		 if (startDate != null) {
			 memberNode.put(startDateTypeId, startDate);
		 }
		 if (endDate != null) {
			 memberNode.put(endDateTypeId, endDate);
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 if (tag != null) {
			 memberNode.put(tagTypeId, tag);
		 }
		 return classNode;
	 } 
	 public static Maps parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 Maps  c= new Maps(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.name = fields.findPath(nameTypeId).textValue();
			 c.note = fields.findPath(noteTypeId).textValue();
			 c.date = fields.findPath(dateTypeId).textValue();
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 c.modifiedBy = fields.findPath(modifiedByTypeId).textValue();
			 c.createdBy = fields.findPath(createdByTypeId).textValue();
			 c.mapType = fields.findPath(mapTypeTypeId).textValue();
			 c.startDate = fields.findPath(startDateTypeId).textValue();
			 c.endDate = fields.findPath(endDateTypeId).textValue();
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 c.tag = fields.findPath(tagTypeId).textValue();
			 return c;
		} else
			 return null;
	 } 
} 
 
