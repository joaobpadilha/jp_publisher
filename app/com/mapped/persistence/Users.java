package com.mapped.persistence;
 
import java.util.*;
	 import com.mapped.persistence.util.*;
 import java.sql.*; 
 import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import com.mapped.persistence.UserSession;
import com.mapped.persistence.UserSessionMgr;
import com.mapped.persistence.SyncStatus;
import com.mapped.persistence.SyncStatusMgr;
import com.mapped.persistence.Invites;
import com.mapped.persistence.InvitesMgr;
import com.mapped.persistence.UserPreferences;
import com.mapped.persistence.UserPreferencesMgr;
import play.libs.Json;
 public class Users { 
 
	 public final static String UsersTypeId = "60"; 
	 protected static String pkTypeId =  "61"; 
	 private String pk; 
	 protected static String emailTypeId =  "62"; 
	 private String email; 
	 protected static String fNameTypeId =  "63"; 
	 private String fName; 
	 protected static String lNameTypeId =  "64"; 
	 private String lName; 
	 protected static String pwdTypeId =  "65"; 
	 private String pwd; 
	 protected static String statusTypeId =  "66"; 
	 private long status; 
	 protected static String lastUpdateTimestampTypeId =  "67"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "68"; 
	 private long createTimestamp; 
	 protected static String modifiedByTypeId =  "69"; 
	 private String modifiedBy; 
	 protected static String createdByTypeId =  "70"; 
	 private String createdBy; 
	 protected static String syncTimestampTypeId =  "71"; 
	 private long syncTimestamp; 
	 protected static String authCodeTypeId =  "72"; 
	 private String authCode; 

 	 // Contructor 
	 public Users () {} 
	 public Users ( String pk,  String email,  String fName,  String lName,  String pwd,  long status,  long lastUpdateTimestamp,  long createTimestamp,  String modifiedBy,  String createdBy,  long syncTimestamp,  String authCode ) { 
		 this.pk=pk; 
		 this.email=email; 
		 this.fName=fName; 
		 this.lName=lName; 
		 this.pwd=pwd; 
		 this.status=status; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
		 this.modifiedBy=modifiedBy; 
		 this.createdBy=createdBy; 
		 this.syncTimestamp=syncTimestamp; 
		 this.authCode=authCode; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getEmail () { 
	 	 return email; 
 	 } 
 
 	 public void setEmail(String email) { 
	 	 this.email = email; 
 	 } 
 
 	 public  String getFName () { 
	 	 return fName; 
 	 } 
 
 	 public void setFName(String fName) { 
	 	 this.fName = fName; 
 	 } 
 
 	 public  String getLName () { 
	 	 return lName; 
 	 } 
 
 	 public void setLName(String lName) { 
	 	 this.lName = lName; 
 	 } 
 
 	 public  String getPwd () { 
	 	 return pwd; 
 	 } 
 
 	 public void setPwd(String pwd) { 
	 	 this.pwd = pwd; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public  String getModifiedBy () { 
	 	 return modifiedBy; 
 	 } 
 
 	 public void setModifiedBy(String modifiedBy) { 
	 	 this.modifiedBy = modifiedBy; 
 	 } 
 
 	 public  String getCreatedBy () { 
	 	 return createdBy; 
 	 } 
 
 	 public void setCreatedBy(String createdBy) { 
	 	 this.createdBy = createdBy; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public  String getAuthCode () { 
	 	 return authCode; 
 	 } 
 
 	 public void setAuthCode(String authCode) { 
	 	 this.authCode = authCode; 
 	 } 
 
 	 public ArrayList <UserSession>  getUserSession () throws Exception{ 
		 return UserSessionMgr.getAllByUsersId(pk); 
	}
	 public ArrayList <SyncStatus>  getSyncStatus () throws Exception{ 
		 return SyncStatusMgr.getAllByUsersId(pk); 
	}
	 public ArrayList <Invites>  getInvites () throws Exception{ 
		 return InvitesMgr.getAllByUsersId(pk); 
	}
	 public ArrayList <UserPreferences>  getUserPreferences () throws Exception{ 
		 return UserPreferencesMgr.getAllByUsersId(pk); 
	}
	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: Users"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 Email: ").append(this.email );  
		 sb.append("	 FName: ").append(this.fName );  
		 sb.append("	 LName: ").append(this.lName );  
		 sb.append("	 Pwd: ").append(this.pwd );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 sb.append("	 ModifiedBy: ").append(this.modifiedBy );  
		 sb.append("	 CreatedBy: ").append(this.createdBy );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 sb.append("	 AuthCode: ").append(this.authCode );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", UsersTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (email != null) {
			 memberNode.put(emailTypeId, email);
		 }
		 if (fName != null) {
			 memberNode.put(fNameTypeId, fName);
		 }
		 if (lName != null) {
			 memberNode.put(lNameTypeId, lName);
		 }
		 if (pwd != null) {
			 memberNode.put(pwdTypeId, pwd);
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 if (modifiedBy != null) {
			 memberNode.put(modifiedByTypeId, modifiedBy);
		 }
		 if (createdBy != null) {
			 memberNode.put(createdByTypeId, createdBy);
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 if (authCode != null) {
			 memberNode.put(authCodeTypeId, authCode);
		 }
		 return classNode;
	 } 
	 public static Users parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 Users  c= new Users(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.email = fields.findPath(emailTypeId).textValue();
			 c.fName = fields.findPath(fNameTypeId).textValue();
			 c.lName = fields.findPath(lNameTypeId).textValue();
			 c.pwd = fields.findPath(pwdTypeId).textValue();
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 c.modifiedBy = fields.findPath(modifiedByTypeId).textValue();
			 c.createdBy = fields.findPath(createdByTypeId).textValue();
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 c.authCode = fields.findPath(authCodeTypeId).textValue();
			 return c;
		} else
			 return null;
	 } 
} 
 
