package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class FileMgr {

	 private static final String SELECT_ALL = "Select Pk, FilePath, FileTag, FileNote, Status, FileType, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, ParentId, SyncTimestamp, Rank, Uploaded, ImgWidth, ImgHeight, Latitude, Longitude, Bearing from File;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, FilePath, FileTag, FileNote, Status, FileType, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, ParentId, SyncTimestamp, Rank, Uploaded, ImgWidth, ImgHeight, Latitude, Longitude, Bearing from File where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from File where Pk = ?;";
	 private static final String INSERT = "insert into File (Pk, FilePath, FileTag, FileNote, Status, FileType, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, ParentId, SyncTimestamp, Rank, Uploaded, ImgWidth, ImgHeight, Latitude, Longitude, Bearing) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update File SET FilePath=?, FileTag=?, FileNote=?, Status=?, FileType=?, LastUpdateTimestamp=?, ModifiedBy=?, ParentId=?, SyncTimestamp=?, Rank=?, Uploaded=?, ImgWidth=?, ImgHeight=?, Latitude=?, Longitude=?, Bearing=? WHERE Pk = ?; ";
	 public static ArrayList <File> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return FileMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static File getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<File> results = FileMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (File c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getFilePath());
				 prep.setString(3, c.getFileTag());
				 prep.setString(4, c.getFileNote());
				 prep.setLong(5, c.getStatus());
				 prep.setString(6, c.getFileType());
				 prep.setLong(7, c.getLastUpdateTimestamp());
				 prep.setLong(8, c.getCreateTimestamp());
				 prep.setString(9, c.getModifiedBy());
				 prep.setString(10, c.getCreatedBy());
				 prep.setString(11, c.getParentId());
				 prep.setLong(12, c.getSyncTimestamp());
				 prep.setString(13, c.getRank());
				 prep.setString(14, c.getUploaded());
				 prep.setString(15, c.getImgWidth());
				 prep.setString(16, c.getImgHeight());
				 prep.setString(17, c.getLatitude());
				 prep.setString(18, c.getLongitude());
				 prep.setString(19, c.getBearing());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (File c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getFilePath());
				 prep.setString(2, c.getFileTag());
				 prep.setString(3, c.getFileNote());
				 prep.setLong(4, c.getStatus());
				 prep.setString(5, c.getFileType());
				 prep.setLong(6, c.getLastUpdateTimestamp());
				 prep.setString(7, c.getModifiedBy());
				 prep.setString(8, c.getParentId());
				 prep.setLong(9, c.getSyncTimestamp());
				 prep.setString(10, c.getRank());
				 prep.setString(11, c.getUploaded());
				 prep.setString(12, c.getImgWidth());
				 prep.setString(13, c.getImgHeight());
				 prep.setString(14, c.getLatitude());
				 prep.setString(15, c.getLongitude());
				 prep.setString(16, c.getBearing());
				 prep.setString(17, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <File> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <File> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 File c = new File();
			 c.setPk(rs.getString("Pk"));
			 c.setFilePath(rs.getString("FilePath"));
			 c.setFileTag(rs.getString("FileTag"));
			 c.setFileNote(rs.getString("FileNote"));
			 c.setStatus(rs.getLong("Status"));
			 c.setFileType(rs.getString("FileType"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 c.setModifiedBy(rs.getString("ModifiedBy"));
			 c.setCreatedBy(rs.getString("CreatedBy"));
			 c.setParentId(rs.getString("ParentId"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 c.setRank(rs.getString("Rank"));
			 c.setUploaded(rs.getString("Uploaded"));
			 c.setImgWidth(rs.getString("ImgWidth"));
			 c.setImgHeight(rs.getString("ImgHeight"));
			 c.setLatitude(rs.getString("Latitude"));
			 c.setLongitude(rs.getString("Longitude"));
			 c.setBearing(rs.getString("Bearing"));
			 results.add(c);
		 }
		 return results;
	 }
}
