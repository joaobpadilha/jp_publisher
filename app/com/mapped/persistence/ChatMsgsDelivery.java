package com.mapped.persistence;
 

import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 
 
import play.libs.Json;
 public class ChatMsgsDelivery { 
 
	 public final static String ChatMsgsDeliveryTypeId = "210"; 
	 protected static String pkTypeId =  "211"; 
	 private String pk; 
	 protected static String msgIdTypeId =  "212"; 
	 private String msgId; 
	 protected static String usersIdTypeId =  "213"; 
	 private String usersId; 
	 protected static String statusTypeId =  "214"; 
	 private long status; 
	 protected static String lastUpdateTimestampTypeId =  "215"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "216"; 
	 private long createTimestamp; 
	 protected static String syncTimestampTypeId =  "217"; 
	 private long syncTimestamp; 

 	 // Contructor 
	 public ChatMsgsDelivery () {} 
	 public ChatMsgsDelivery ( String pk,  String msgId,  String usersId,  long status,  long lastUpdateTimestamp,  long createTimestamp,  long syncTimestamp ) { 
		 this.pk=pk; 
		 this.msgId=msgId; 
		 this.usersId=usersId; 
		 this.status=status; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
		 this.syncTimestamp=syncTimestamp; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getMsgId () { 
	 	 return msgId; 
 	 } 
 
 	 public void setMsgId(String msgId) { 
	 	 this.msgId = msgId; 
 	 } 
 
 	 public  String getUsersId () { 
	 	 return usersId; 
 	 } 
 
 	 public void setUsersId(String usersId) { 
	 	 this.usersId = usersId; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: ChatMsgsDelivery"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 MsgId: ").append(this.msgId );  
		 sb.append("	 UsersId: ").append(this.usersId );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", ChatMsgsDeliveryTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (msgId != null) {
			 memberNode.put(msgIdTypeId, msgId);
		 }
		 if (usersId != null) {
			 memberNode.put(usersIdTypeId, usersId);
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 return classNode;
	 } 
	 public static ChatMsgsDelivery parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 ChatMsgsDelivery  c= new ChatMsgsDelivery(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.msgId = fields.findPath(msgIdTypeId).textValue();
			 c.usersId = fields.findPath(usersIdTypeId).textValue();
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 return c;
		} else
			 return null;
	 } 
} 
 
