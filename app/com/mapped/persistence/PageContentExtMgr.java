package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2012-11-22
 * Time: 9:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class PageContentExtMgr extends  PageContentMgr {

    private static final String SELECT_STR = "Select pc.Pk, pc.PageId, pc.ContentId, pc.ContentType, pc.Rank, pc.ModifiedBy, pc.CreatedBy, pc.Status, pc.SyncTimestamp, pc.LastUpdateTimestamp, pc.CreateTimestamp from PageContent pc";
    private static final String SELECT_ALL_BY_OWNER_LASTUPATE = SELECT_STR + ", Page p, Maps b where b.createdby = ? and p.MapsId = b.Pk and p.pk = pc.pageid and (pc.SyncTimestamp = 0 or pc.SyncTimestamp > ?);";
    private static final String SELECT_SHARED_MAPS_BY_USER_LASTUPATE = SELECT_STR + ", Page p where p.mapsid in (select mapsid from sharedmaps where shareuserid = ? and status = 0) and  p.pk = pc.pageid and (pc.SyncTimestamp = 0 or pc.SyncTimestamp > ?);";
    private static final String SELECT_NEW_SHARED_MAPS_BY_USER_LASTUPATE = SELECT_STR + ", Page p where p.mapsid in (select mapsId from sharedmaps where shareuserid = ? and status = 0 and (SyncTimestamp = 0 or SyncTimestamp > ?) and createtimestamp = lastupdatetimestamp) and  p.pk = pc.pageid ;";
    private static final String SELECT_ALL_BY_MAPSID = SELECT_STR + ", Page p where p.mapsid = ? and p.status = 0 and pc.status = 0 and pc.pageId = p.pk ORDER BY pc.Rank, pc.CreateTimestamp";
    private static final String INSERT = "insert into PageContent (Pk, PageId, ContentId, ContentType, Rank, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String UPDATE = "update PageContent SET PageId=?, ContentId=?, ContentType=?, Rank=?, ModifiedBy=?, Status=?, SyncTimestamp=?, LastUpdateTimestamp=? WHERE Pk = ?; ";

    public static ArrayList<PageContent> getAllByMapOwnerSinceLastUpdate(String userId, long lastUpdateTimestamp) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            prep = conn.prepareStatement(SELECT_ALL_BY_OWNER_LASTUPATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdateTimestamp);
            rs = prep.executeQuery();
            return PageContentMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <PageContent> getSharedByUserSinceLastUpdate(String userId, long lastUpdateTimestamp) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            prep = conn.prepareStatement(SELECT_SHARED_MAPS_BY_USER_LASTUPATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdateTimestamp);
            rs = prep.executeQuery();
            return PageContentMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <PageContent> getNewSharedByUserSinceLastUpdate(String userId, long lastUpdateTimestamp) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            prep = conn.prepareStatement(SELECT_NEW_SHARED_MAPS_BY_USER_LASTUPATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdateTimestamp);
            rs = prep.executeQuery();
            return PageContentMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <PageContent> getAllPageContentByMapsId(String mapsId) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            prep = conn.prepareStatement(SELECT_ALL_BY_MAPSID);
            prep.setString(1, mapsId);
            rs = prep.executeQuery();
            return PageContentMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static void insert (PageContent c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(INSERT);
            prep.setString(1, c.getPk());
            prep.setString(2, c.getPageId());
            prep.setString(3, c.getContentId());
            prep.setString(4, c.getContentType());
            prep.setString(5, c.getRank());
            prep.setString(6, c.getModifiedBy());
            prep.setString(7, c.getCreatedBy());
            prep.setLong(8, c.getStatus());
            prep.setLong(9, c.getSyncTimestamp());
            prep.setLong(10, c.getLastUpdateTimestamp());
            prep.setLong(11, c.getCreateTimestamp());
            prep.execute();
        } finally {
            if (prep != null)
                prep.close();
        }
    }

    public static int update (PageContent c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(UPDATE);
            prep.setString(1, c.getPageId());
            prep.setString(2, c.getContentId());
            prep.setString(3, c.getContentType());
            prep.setString(4, c.getRank());
            prep.setString(5, c.getModifiedBy());
            prep.setLong(6, c.getStatus());
            prep.setLong(7, c.getSyncTimestamp());
            prep.setLong(8, c.getLastUpdateTimestamp());
            prep.setString(9, c.getPk());
            return prep.executeUpdate();
        } finally {
            if (prep != null)
                prep.close();
        }
    }

}
