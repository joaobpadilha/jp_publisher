package com.mapped.persistence;
 
import java.util.*;
	 import com.mapped.persistence.util.*;
 import java.sql.*; 
 import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import com.mapped.persistence.PageContent;
import com.mapped.persistence.PageContentMgr;
import com.mapped.persistence.PageEntry;
import com.mapped.persistence.PageEntryMgr;
import play.libs.Json;
 public class Page { 
 
	 public final static String PageTypeId = "3000"; 
	 protected static String pkTypeId =  "3001"; 
	 private String pk; 
	 protected static String mapsIdTypeId =  "3002"; 
	 private String mapsId; 
	 protected static String nameTypeId =  "3003"; 
	 private String name; 
	 protected static String noteTypeId =  "3004"; 
	 private String note; 
	 protected static String pageDateTypeId =  "3005"; 
	 private String pageDate; 
	 protected static String pageRankTypeId =  "3006"; 
	 private String pageRank; 
	 protected static String templateIdTypeId =  "3007"; 
	 private String templateId; 
	 protected static String modifiedByTypeId =  "3090"; 
	 private String modifiedBy; 
	 protected static String createdByTypeId =  "3091"; 
	 private String createdBy; 
	 protected static String statusTypeId =  "3092"; 
	 private long status; 
	 protected static String syncTimestampTypeId =  "3093"; 
	 private long syncTimestamp; 
	 protected static String lastUpdateTimestampTypeId =  "3094"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "3095"; 
	 private long createTimestamp; 
	 protected static String milestonesIdTypeId =  "3096"; 
	 private String milestonesId; 
	 protected static String pageTypeTypeId =  "3097"; 
	 private String pageType; 
	 protected static String pageTagTypeId =  "3098"; 
	 private String pageTag; 

 	 // Contructor 
	 public Page () {} 
	 public Page ( String pk,  String mapsId,  String name,  String note,  String pageDate,  String pageRank,  String templateId,  String modifiedBy,  String createdBy,  long status,  long syncTimestamp,  long lastUpdateTimestamp,  long createTimestamp,  String milestonesId,  String pageType,  String pageTag ) { 
		 this.pk=pk; 
		 this.mapsId=mapsId; 
		 this.name=name; 
		 this.note=note; 
		 this.pageDate=pageDate; 
		 this.pageRank=pageRank; 
		 this.templateId=templateId; 
		 this.modifiedBy=modifiedBy; 
		 this.createdBy=createdBy; 
		 this.status=status; 
		 this.syncTimestamp=syncTimestamp; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
		 this.milestonesId=milestonesId; 
		 this.pageType=pageType; 
		 this.pageTag=pageTag; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getMapsId () { 
	 	 return mapsId; 
 	 } 
 
 	 public void setMapsId(String mapsId) { 
	 	 this.mapsId = mapsId; 
 	 } 
 
 	 public  String getName () { 
	 	 return name; 
 	 } 
 
 	 public void setName(String name) { 
	 	 this.name = name; 
 	 } 
 
 	 public  String getNote () { 
	 	 return note; 
 	 } 
 
 	 public void setNote(String note) { 
	 	 this.note = note; 
 	 } 
 
 	 public  String getPageDate () { 
	 	 return pageDate; 
 	 } 
 
 	 public void setPageDate(String pageDate) { 
	 	 this.pageDate = pageDate; 
 	 } 
 
 	 public  String getPageRank () { 
	 	 return pageRank; 
 	 } 
 
 	 public void setPageRank(String pageRank) { 
	 	 this.pageRank = pageRank; 
 	 } 
 
 	 public  String getTemplateId () { 
	 	 return templateId; 
 	 } 
 
 	 public void setTemplateId(String templateId) { 
	 	 this.templateId = templateId; 
 	 } 
 
 	 public  String getModifiedBy () { 
	 	 return modifiedBy; 
 	 } 
 
 	 public void setModifiedBy(String modifiedBy) { 
	 	 this.modifiedBy = modifiedBy; 
 	 } 
 
 	 public  String getCreatedBy () { 
	 	 return createdBy; 
 	 } 
 
 	 public void setCreatedBy(String createdBy) { 
	 	 this.createdBy = createdBy; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public  String getMilestonesId () { 
	 	 return milestonesId; 
 	 } 
 
 	 public void setMilestonesId(String milestonesId) { 
	 	 this.milestonesId = milestonesId; 
 	 } 
 
 	 public  String getPageType () { 
	 	 return pageType; 
 	 } 
 
 	 public void setPageType(String pageType) { 
	 	 this.pageType = pageType; 
 	 } 
 
 	 public  String getPageTag () { 
	 	 return pageTag; 
 	 } 
 
 	 public void setPageTag(String pageTag) { 
	 	 this.pageTag = pageTag; 
 	 } 
 
 	 public ArrayList <PageContent>  getPageContent () throws Exception{ 
		 return PageContentMgr.getAllByPageId(pk); 
	}
	 public ArrayList <PageEntry>  getPageEntry () throws Exception{ 
		 return PageEntryMgr.getAllByPageId(pk); 
	}
	 public Maps getMaps () throws Exception{ 
		 return MapsMgr.getByPk(mapsId); 
	}
	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: Page"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 MapsId: ").append(this.mapsId );  
		 sb.append("	 Name: ").append(this.name );  
		 sb.append("	 Note: ").append(this.note );  
		 sb.append("	 PageDate: ").append(this.pageDate );  
		 sb.append("	 PageRank: ").append(this.pageRank );  
		 sb.append("	 TemplateId: ").append(this.templateId );  
		 sb.append("	 ModifiedBy: ").append(this.modifiedBy );  
		 sb.append("	 CreatedBy: ").append(this.createdBy );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 sb.append("	 MilestonesId: ").append(this.milestonesId );  
		 sb.append("	 PageType: ").append(this.pageType );  
		 sb.append("	 PageTag: ").append(this.pageTag );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", PageTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (mapsId != null) {
			 memberNode.put(mapsIdTypeId, mapsId);
		 }
		 if (name != null) {
			 memberNode.put(nameTypeId, name);
		 }
		 if (note != null) {
			 memberNode.put(noteTypeId, note);
		 }
		 if (pageDate != null) {
			 memberNode.put(pageDateTypeId, pageDate);
		 }
		 if (pageRank != null) {
			 memberNode.put(pageRankTypeId, pageRank);
		 }
		 if (templateId != null) {
			 memberNode.put(templateIdTypeId, templateId);
		 }
		 if (modifiedBy != null) {
			 memberNode.put(modifiedByTypeId, modifiedBy);
		 }
		 if (createdBy != null) {
			 memberNode.put(createdByTypeId, createdBy);
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 if (milestonesId != null) {
			 memberNode.put(milestonesIdTypeId, milestonesId);
		 }
		 if (pageType != null) {
			 memberNode.put(pageTypeTypeId, pageType);
		 }
		 if (pageTag != null) {
			 memberNode.put(pageTagTypeId, pageTag);
		 }
		 return classNode;
	 } 
	 public static Page parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 Page  c= new Page(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.mapsId = fields.findPath(mapsIdTypeId).textValue();
			 c.name = fields.findPath(nameTypeId).textValue();
			 c.note = fields.findPath(noteTypeId).textValue();
			 c.pageDate = fields.findPath(pageDateTypeId).textValue();
			 c.pageRank = fields.findPath(pageRankTypeId).textValue();
			 c.templateId = fields.findPath(templateIdTypeId).textValue();
			 c.modifiedBy = fields.findPath(modifiedByTypeId).textValue();
			 c.createdBy = fields.findPath(createdByTypeId).textValue();
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 c.milestonesId = fields.findPath(milestonesIdTypeId).textValue();
			 c.pageType = fields.findPath(pageTypeTypeId).textValue();
			 c.pageTag = fields.findPath(pageTagTypeId).textValue();
			 return c;
		} else
			 return null;
	 } 
} 
 
