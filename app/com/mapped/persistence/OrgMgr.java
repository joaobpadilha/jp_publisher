package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class OrgMgr {

	 private static final String SELECT_ALL = "Select Pk, Name, Type, Note, Uri, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from Org;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, Name, Type, Note, Uri, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from Org where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from Org where Pk = ?;";
	 private static final String INSERT = "insert into Org (Pk, Name, Type, Note, Uri, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update Org SET Name=?, Type=?, Note=?, Uri=?, ModifiedBy=?, Status=?, SyncTimestamp=?, LastUpdateTimestamp=? WHERE Pk = ?; ";
	 public static ArrayList <Org> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return OrgMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static Org getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<Org> results = OrgMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (Org c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getName());
				 prep.setString(3, c.getType());
				 prep.setString(4, c.getNote());
				 prep.setString(5, c.getUri());
				 prep.setString(6, c.getModifiedBy());
				 prep.setString(7, c.getCreatedBy());
				 prep.setLong(8, c.getStatus());
				 prep.setLong(9, c.getSyncTimestamp());
				 prep.setLong(10, c.getLastUpdateTimestamp());
				 prep.setLong(11, c.getCreateTimestamp());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (Org c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getName());
				 prep.setString(2, c.getType());
				 prep.setString(3, c.getNote());
				 prep.setString(4, c.getUri());
				 prep.setString(5, c.getModifiedBy());
				 prep.setLong(6, c.getStatus());
				 prep.setLong(7, c.getSyncTimestamp());
				 prep.setLong(8, c.getLastUpdateTimestamp());
				 prep.setString(9, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <Org> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <Org> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 Org c = new Org();
			 c.setPk(rs.getString("Pk"));
			 c.setName(rs.getString("Name"));
			 c.setType(rs.getString("Type"));
			 c.setNote(rs.getString("Note"));
			 c.setUri(rs.getString("Uri"));
			 c.setModifiedBy(rs.getString("ModifiedBy"));
			 c.setCreatedBy(rs.getString("CreatedBy"));
			 c.setStatus(rs.getLong("Status"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 results.add(c);
		 }
		 return results;
	 }
}
