package com.mapped.persistence;
 
import java.util.*;
	 import com.mapped.persistence.util.*;
 import java.sql.*; 
 import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import play.libs.Json;
 public class SyncStatus { 
 
	 public final static String SyncStatusTypeId = "140"; 
	 protected static String pkTypeId =  "141"; 
	 private String pk; 
	 protected static String usersIdTypeId =  "142"; 
	 private String usersId; 
	 protected static String devIdTypeId =  "143"; 
	 private String devId; 
	 protected static String startTimestampTypeId =  "144"; 
	 private long startTimestamp; 
	 protected static String endTimestampTypeId =  "145"; 
	 private long endTimestamp; 
	 protected static String clientTimestampTypeId =  "146"; 
	 private long clientTimestamp; 
	 protected static String serverTimestampTypeId =  "147"; 
	 private long serverTimestamp; 
	 protected static String statusTypeId =  "148"; 
	 private long status; 
	 protected static String lastUpdateTimestampTypeId =  "149"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "150"; 
	 private long createTimestamp; 
	 protected static String syncTimestampTypeId =  "151"; 
	 private long syncTimestamp; 

 	 // Contructor 
	 public SyncStatus () {} 
	 public SyncStatus ( String pk,  String usersId,  String devId,  long startTimestamp,  long endTimestamp,  long clientTimestamp,  long serverTimestamp,  long status,  long lastUpdateTimestamp,  long createTimestamp,  long syncTimestamp ) { 
		 this.pk=pk; 
		 this.usersId=usersId; 
		 this.devId=devId; 
		 this.startTimestamp=startTimestamp; 
		 this.endTimestamp=endTimestamp; 
		 this.clientTimestamp=clientTimestamp; 
		 this.serverTimestamp=serverTimestamp; 
		 this.status=status; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
		 this.syncTimestamp=syncTimestamp; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getUsersId () { 
	 	 return usersId; 
 	 } 
 
 	 public void setUsersId(String usersId) { 
	 	 this.usersId = usersId; 
 	 } 
 
 	 public  String getDevId () { 
	 	 return devId; 
 	 } 
 
 	 public void setDevId(String devId) { 
	 	 this.devId = devId; 
 	 } 
 
 	 public  long getStartTimestamp () { 
	 	 return startTimestamp; 
 	 } 
 
 	 public void setStartTimestamp(long startTimestamp) { 
	 	 this.startTimestamp = startTimestamp; 
 	 } 
 
 	 public  long getEndTimestamp () { 
	 	 return endTimestamp; 
 	 } 
 
 	 public void setEndTimestamp(long endTimestamp) { 
	 	 this.endTimestamp = endTimestamp; 
 	 } 
 
 	 public  long getClientTimestamp () { 
	 	 return clientTimestamp; 
 	 } 
 
 	 public void setClientTimestamp(long clientTimestamp) { 
	 	 this.clientTimestamp = clientTimestamp; 
 	 } 
 
 	 public  long getServerTimestamp () { 
	 	 return serverTimestamp; 
 	 } 
 
 	 public void setServerTimestamp(long serverTimestamp) { 
	 	 this.serverTimestamp = serverTimestamp; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public Users getUsers () throws Exception{ 
		 return UsersMgr.getByPk(usersId); 
	}
	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: SyncStatus"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 UsersId: ").append(this.usersId );  
		 sb.append("	 DevId: ").append(this.devId );  
		 sb.append("	 StartTimestamp: ").append(this.startTimestamp );  
		 sb.append("	 EndTimestamp: ").append(this.endTimestamp );  
		 sb.append("	 ClientTimestamp: ").append(this.clientTimestamp );  
		 sb.append("	 ServerTimestamp: ").append(this.serverTimestamp );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", SyncStatusTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (usersId != null) {
			 memberNode.put(usersIdTypeId, usersId);
		 }
		 if (devId != null) {
			 memberNode.put(devIdTypeId, devId);
		 }
		 if (startTimestamp >= 0) {
			 memberNode.put(startTimestampTypeId, Long.toString(startTimestamp));
		 }
		 if (endTimestamp >= 0) {
			 memberNode.put(endTimestampTypeId, Long.toString(endTimestamp));
		 }
		 if (clientTimestamp >= 0) {
			 memberNode.put(clientTimestampTypeId, Long.toString(clientTimestamp));
		 }
		 if (serverTimestamp >= 0) {
			 memberNode.put(serverTimestampTypeId, Long.toString(serverTimestamp));
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 return classNode;
	 } 
	 public static SyncStatus parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 SyncStatus  c= new SyncStatus(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.usersId = fields.findPath(usersIdTypeId).textValue();
			 c.devId = fields.findPath(devIdTypeId).textValue();
			 if (fields.findPath(startTimestampTypeId).textValue() == null) {
				 c.startTimestamp = 0;
			 } else { 
				 c.startTimestamp = Long.parseLong(fields.findPath(startTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(endTimestampTypeId).textValue() == null) {
				 c.endTimestamp = 0;
			 } else { 
				 c.endTimestamp = Long.parseLong(fields.findPath(endTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(clientTimestampTypeId).textValue() == null) {
				 c.clientTimestamp = 0;
			 } else { 
				 c.clientTimestamp = Long.parseLong(fields.findPath(clientTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(serverTimestampTypeId).textValue() == null) {
				 c.serverTimestamp = 0;
			 } else { 
				 c.serverTimestamp = Long.parseLong(fields.findPath(serverTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 return c;
		} else
			 return null;
	 } 
} 
 
