package com.mapped.persistence;
 
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;
 public class ChatSession { 
 
	 public final static String ChatSessionTypeId = "180"; 
	 protected static String pkTypeId =  "181"; 
	 private String pk; 
	 protected static String nameTypeId =  "182"; 
	 private String name; 
	 protected static String statusTypeId =  "183"; 
	 private long status; 
	 protected static String lastUpdateTimestampTypeId =  "184"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "185"; 
	 private long createTimestamp; 
	 protected static String syncTimestampTypeId =  "186"; 
	 private long syncTimestamp; 
	 protected static String mapsIdTypeId =  "187"; 
	 private String mapsId; 

 	 // Contructor 
	 public ChatSession () {} 
	 public ChatSession ( String pk,  String name,  long status,  long lastUpdateTimestamp,  long createTimestamp,  long syncTimestamp,  String mapsId ) { 
		 this.pk=pk; 
		 this.name=name; 
		 this.status=status; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
		 this.syncTimestamp=syncTimestamp; 
		 this.mapsId=mapsId; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getName () { 
	 	 return name; 
 	 } 
 
 	 public void setName(String name) { 
	 	 this.name = name; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public  String getMapsId () { 
	 	 return mapsId; 
 	 } 
 
 	 public void setMapsId(String mapsId) { 
	 	 this.mapsId = mapsId; 
 	 } 
 
 	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: ChatSession"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 Name: ").append(this.name );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 sb.append("	 MapsId: ").append(this.mapsId );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", ChatSessionTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (name != null) {
			 memberNode.put(nameTypeId, name);
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 if (mapsId != null) {
			 memberNode.put(mapsIdTypeId, mapsId);
		 }
		 return classNode;
	 } 
	 public static ChatSession parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 ChatSession  c= new ChatSession(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.name = fields.findPath(nameTypeId).textValue();
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 c.mapsId = fields.findPath(mapsIdTypeId).textValue();
			 return c;
		} else
			 return null;
	 } 
} 
 
