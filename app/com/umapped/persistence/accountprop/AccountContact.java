package com.umapped.persistence.accountprop;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mapped.publisher.parse.schemaorg.PostalAddress;
import com.mapped.publisher.parse.schemaorg.umapped.AddressType;
import com.mapped.publisher.parse.schemaorg.umapped.SocialLink;
import com.umapped.api.schema.types.Email;
import com.umapped.api.schema.types.PhoneNumber;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Json Representation for contact information
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountContact {
  public Map<AddressType, PostalAddress> addresses;
  public Map<Email.EmailType, Email>             emails;
  public Map<PhoneNumber.PhoneType, PhoneNumber> phones;
  public Map<String, SocialLink>                 socialLinks;
  public Set<String> urls;

  public void addUrl(String url) {
    if (urls == null) {
      urls = new HashSet<>(1);
    }
    urls.add(url);
  }

  public void addAddress(AddressType type, PostalAddress address) {
    if (addresses == null) {
      addresses = new HashMap<>();
    }
    addresses.put(type, address);
  }

  public void addSocialLink(SocialLink sl) {
    if (socialLinks == null) {
      socialLinks = new HashMap<>(1);
    }
    socialLinks.put(sl.getServiceName(), sl);
  }

  public void addEmail(Email email) {
    if (emails == null) {
      emails = new HashMap<>(1);
    }
    emails.put(email.getEmailType(), email);
  }

  public void addPhone(PhoneNumber phone) {
    if (phones == null) {
      phones = new HashMap<>(1);
    }
    phones.put(phone.getPhoneType(), phone);
  }

  public boolean hasRecords() {
    return (addresses != null && addresses.size() > 0) ||
           (socialLinks != null && socialLinks.size() > 0) ||
           (emails != null && emails.size() > 0) ||
           (phones != null && phones.size() > 0) ||
           (urls != null && urls.size() > 0);
  }

  public void prePersist() {
    if (addresses != null && addresses.size() == 0) {
      addresses = null;
    }
    if (socialLinks != null && socialLinks.size() == 0) {
      socialLinks = null;
    }
    if (emails != null && emails.size() == 0) {
      emails = null;
    }
    if (phones != null && phones.size() == 0) {
      phones = null;
    }
    if (urls != null && urls.size() == 0) {
      urls = null;
    }
  }
}
