package com.umapped.persistence.accountprop;

import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.CacheMgr;
import models.publisher.Account;
import models.publisher.AccountProp;

import javax.inject.Singleton;

/**
 * Created by twong on 16-03-14.
 */
@Singleton
public class PreferenceMgr {
  public PreferenceMgr() {

  }

  /**
   * Preference get trip preference.
   */
  public TripPrefs getTripPref (Account a) {

    Boolean hasPrefs = (Boolean) CacheMgr.get(APPConstants.CACHE_PREFS_CHECKED + a.getUid());
    if (hasPrefs == null || hasPrefs) {
      TripPrefs prefs = (TripPrefs) CacheMgr.get(APPConstants.CACHE_TRIP_PREFS + a.getUid());
      if (prefs == null) {
        AccountProp prop =  AccountProp.findByPk(a.getUid(), AccountProp.PropertyType.TRIP_PREFERENCE);
        if (prop != null && prop.tripPreferences != null) {
          prefs = prop.tripPreferences;
          CacheMgr.set(APPConstants.CACHE_TRIP_PREFS + a.getUid(), prefs);
          CacheMgr.set(APPConstants.CACHE_PREFS_CHECKED + a.getUid(), new Boolean(true));
        }
      }
      return prefs;
    }
    return null;

  }

  /**
   * Preference get user preference.
   */
  public AccountPrefs getAccountPref (Account a) {

    Boolean hasPrefs = (Boolean) CacheMgr.get(APPConstants.CACHE_PREFS_CHECKED + a.getUid());
    if (hasPrefs == null || hasPrefs) {
      AccountPrefs prefs = (AccountPrefs) CacheMgr.get(APPConstants.CACHE_ACCOUNT_PREFS + a.getUid());
      if (prefs == null) {
        AccountProp prop = AccountProp.findByPk(a.getUid(), AccountProp.PropertyType.ACCOUNT_PREFERENCE);
        if (prop != null && prop.accountPreferences != null) {
          prefs = prop.accountPreferences;
          CacheMgr.set(APPConstants.CACHE_ACCOUNT_PREFS + a.getUid(), prefs);
          CacheMgr.set(APPConstants.CACHE_PREFS_CHECKED + a.getUid(), new Boolean(true));
        } else {
          CacheMgr.set(APPConstants.CACHE_PREFS_CHECKED + a.getUid(), new Boolean(false));
        }
      }

      return prefs;
    }
    return null;
  }
}
