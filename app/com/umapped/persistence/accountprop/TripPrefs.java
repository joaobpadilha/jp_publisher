package com.umapped.persistence.accountprop;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * Created by twong on 16-03-11.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TripPrefs implements Serializable {
  public enum TRIP_PDF_TMPLT {PDF1, PDF2, PRINT, DEF}

  ;

  public TRIP_PDF_TMPLT tripPdfTmplt = TRIP_PDF_TMPLT.DEF; //allow for override of custom template
  public boolean is12Hr = true; //allow for default 12/24 hr clock on the web itinerary
  public boolean isPrintPDF = false; //allow for displaying of printer friendly PDF

  public static TripPrefs build() {
    TripPrefs prefs = new TripPrefs();
    return prefs;
  }

  public TRIP_PDF_TMPLT getTripPdfTmplt() {
    if (tripPdfTmplt == null) {
      return TRIP_PDF_TMPLT.DEF;
    }
    return tripPdfTmplt;
  }

  public TripPrefs setTripPdfTmplt(TRIP_PDF_TMPLT tripPdfTmplt) {
    this.tripPdfTmplt = tripPdfTmplt;
    return this;
  }

  public boolean isIs12Hr() {
    return is12Hr;
  }

  public TripPrefs setIs12Hr(boolean is12Hr) {
    this.is12Hr = is12Hr;
    return this;
  }

  public boolean isPrintPDF() {
    return isPrintPDF;
  }

  public TripPrefs setPrintPDF(boolean isPrintFriendlyPDF) {
    this.isPrintPDF = isPrintFriendlyPDF;
    return this;
  }

  public boolean hasPreferences() {
    if (tripPdfTmplt != null || is12Hr || isPrintPDF) {
      return true;
    }

    return false;
  }

  public void prePersist() {

  }
}