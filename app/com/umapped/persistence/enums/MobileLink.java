package com.umapped.persistence.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by twong on 18/12/17.
 */
public enum  MobileLink {
    //add the links for the white label apps - note the names are lower case and use in either a contains search or an exact search based on the flag
    //parent and child companies or consortia membership is automatically verified
    SKI_COM("ski.com", "https://itunes.apple.com/app/ski-com/id1077442273", "https://play.google.com/store/apps/details?id=umapped.com.ski", false,false),
    TZELL("tzell", "https://itunes.apple.com/app/tzelltrip/id1186417139", "https://play.google.com/store/apps/details?id=umapped.com.tzell", true,false),
    PROTRAVEL("protravel", "https://itunes.apple.com/app/protrip/id1186417146", "https://play.google.com/store/apps/details?id=umapped.com.protravel", true,false),
    ENSEMBLE("ensemble", "https://itunes.apple.com/app/my-triproute/id1050278229", "https://play.google.com/store/apps/details?id=umapped.com.ensemble", true,false),
    ALTOUR("altour","https://itunes.apple.com/us/app/altour-trip-wallet/id1351341922","https://play.google.com/store/apps/details?id=com.altour.mobile.android", true, false),
    SMARTFLYER("smartflyer", "https://itunes.apple.com/app/smartflyer/id1314979854", "https://play.google.com/store/apps/details?id=com.smartflyer.mobile.android", false,false),
    LOZANO("lozano", "https://itunes.apple.com/app/lozano-travel/id1313504358", "https://play.google.com/store/apps/details?id=com.lozanotravel.mobile.android", false,false),
    TEPLIS("teplis", "https://itunes.apple.com/us/app/teplis-travel/id1378368327", "https://play.google.com/store/apps/details?id=com.teplis.mobile.android", false,false),
    UMAPPED("Umapped", "https://itunes.apple.com/app/umapped-v2/id1263115316?mt=8", "https://play.google.com/store/apps/details?id=com.umapped.mobile&hl=en", false,true);

    private String iosLink;
    private String androidLink;
    private boolean isConsortia;
    private String name;
    private boolean exactMatch;

    private MobileLink(String name, String iosLink, String androidLink, boolean isConsortia, boolean isExactMatch) {
        this.name = name;
        this.iosLink = iosLink;
        this.androidLink = androidLink;
        this.isConsortia = isConsortia;
        this.exactMatch = isExactMatch;
    }

    public static List<MobileLink> getCompanyLinks() {
        List<MobileLink> links = new ArrayList<>();
        for (MobileLink m: MobileLink.values()) {
            if (!m.isConsortia && m != MobileLink.UMAPPED) {
                links.add(m);
            }
        }
        return links;
    }

    public static List<MobileLink> getConsortiumLinks() {
        List<MobileLink> links = new ArrayList<>();
        for (MobileLink m: MobileLink.values()) {
            if (m.isConsortia && m != MobileLink.UMAPPED) {
                links.add(m);
            }
        }
        return links;
    }

    public String getIosLink() {
        return iosLink;
    }

    public void setIosLink(String iosLink) {
        this.iosLink = iosLink;
    }

    public String getAndroidLink() {
        return androidLink;
    }

    public void setAndroidLink(String androidLink) {
        this.androidLink = androidLink;
    }

    public boolean isConsortia() {
        return isConsortia;
    }

    public void setConsortia(boolean consortia) {
        isConsortia = consortia;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isExactMatch() {
        return exactMatch;
    }

    public void setExactMatch(boolean exactMatch) {
        this.exactMatch = exactMatch;
    }
}
