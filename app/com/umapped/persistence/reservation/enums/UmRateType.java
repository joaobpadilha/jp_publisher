package com.umapped.persistence.reservation.enums;

import com.umapped.api.schema.types.AddressType;

/**
 * Created by ryan on 08/05/17.
 */
public enum UmRateType {
    REFUNDABLE("Refundable"),
    NON_REFUNDABLE("Non-Refundable"),
    DAILY("Daily"),
    WEEKLY("Weekly"),
    ONE_WAY("One Way"),
    RETURN("Return"),
    PER_PERSON("Per Person"),
    ENTIRE_PARTY("Entire Party"),
    CANCELLATION("Cancellation"),
    NOT_APPLICABLE("N/A");

    private String value;

    private UmRateType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public static UmRateType getTypeByName (String name) {
        if (name != null && !name.isEmpty()) {
            try {
                UmRateType r = UmRateType.valueOf(name);
                return r;
            } catch (Exception e) {

            }
        }
        return NOT_APPLICABLE;
    }
}
