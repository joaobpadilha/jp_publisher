package com.umapped.persistence.reservation.accommodation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.umapped.persistence.reservation.UmReservation;

/**
 * Stores data previously kept in hotel
 * <p>
 * PUB-923 Requirements:
 * <p>
 * Will not be implemented
 * Room Categories with Different pricing options
 * Price per night - what if prices are different for different nights, usually
 * weekends are much cheaper
 * Prepaid or Payable there
 * Per Traveler
 * Additional Check-in Information
 * Room Number
 * Name of person on each room reservation
 * Hotel frequent stayer ID (not in this reservation)
 * Special requests [GLOBAL]
 * available upgrades [GLOBAL]
 * Included (wifi, breakfast) [GLOBAL]
 * Room Type
 * Bedding
 * Per Reservation
 * Number of Rooms
 * Amenities
 * Number of nights
 * Number of guests (adults & children)
 * Cancellation policy [COMMON]
 * Taxes & fees [COMMON]
 * Resort Fees
 * Total price with tax [COMMON]
 * Hotel Rating (part of Organization record - aggregateRating)
 * Confirmed status [COMMON]
 * Hotel Advisories
 * <p>
 * Created by surge on 2016-11-25.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmAccommodationReservation
    extends UmReservation {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private UmLodgingBusiness accommodation;
  private String amenities;
  private String propertyDescription;
  /****************************************************************************
   * Fields from ticket: PUB-922 *
   ****************************************************************************/
  private Integer roomCount;
  private Integer nightCount;
  private Integer guestCount;
  private String resortFees;
  private String advisories;

  public Integer getRoomCount() {
    return roomCount;
  }

  public UmAccommodationReservation setRoomCount(Integer roomCount) {
    this.roomCount = roomCount;
    return this;
  }

  public Integer getNightCount() {
    return nightCount;
  }

  public UmAccommodationReservation setNightCount(Integer nightCount) {
    this.nightCount = nightCount;
    return this;
  }

  public Integer getGuestCount() {
    return guestCount;
  }

  public UmAccommodationReservation setGuestCount(Integer guestCount) {
    this.guestCount = guestCount;
    return this;
  }

  public String getResortFees() {
    return resortFees;
  }

  public UmAccommodationReservation setResortFees(String resortFees) {
    this.resortFees = resortFees;
    return this;
  }

  public String getAdvisories() {
    return advisories;
  }

  public UmAccommodationReservation setAdvisories(String advisories) {
    this.advisories = advisories;
    return this;
  }

  public String getAmenities() {
    return amenities;
  }

  public UmAccommodationReservation setAmenities(String amenities) {
    this.amenities = amenities;
    return this;
  }

  public String getPropertyDescription() {
    return propertyDescription;
  }

  public UmAccommodationReservation setPropertyDescription(String propertyDescription) {
    this.propertyDescription = propertyDescription;
    return this;
  }

  public UmLodgingBusiness getAccommodation() {
    if (accommodation == null) {
      accommodation = new UmLodgingBusiness();
    }
    return accommodation;
  }

  public UmAccommodationReservation setAccommodation(UmLodgingBusiness accommodation) {
    this.accommodation = accommodation;
    return this;
  }
}
