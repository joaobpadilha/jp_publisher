package com.umapped.persistence.reservation.flight;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.umapped.persistence.reservation.UmDateTime;
import com.umapped.persistence.reservation.UmOrganization;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmFlight
    implements Serializable {
  /**
  * 
  */
  private static final long serialVersionUID = 1L;

  /**
   * The unique identifier for a flight, not including the airline IATA code.
   * For example,
   * if describing United flight 110, the flightNumber is '110'. The IATA code
   * can be set on the Airline.
   */
  public String flightNumber;

  /**
   * IATA coded airline
   */
  private UmAirline airline;

  private UmAirline operatedBy;

  /**
   * The airport where the flight originates.
   */
  public UmAirport departureAirport;

  /**
   * Identifier of the flight's departure gate.
   */
  public String departureGate;
  /**
   * Identifier of the flight's departure terminal.
   */
  public String departureTerminal;
  /**
   * The expected departure time.
   */
  public UmDateTime departureTime;

  /**
   * The airport where the flight terminates.
   */
  public UmAirport arrivalAirport;

  /**
   * Identifier of the flight's arrival gate.
   */
  public String arrivalGate;
  /**
   * Identifier of the flight's arrival terminal.
   */
  public String arrivalTerminal;
  /**
   * The expected arrival time.
   */
  public UmDateTime arrivalTime;

  /**
   * The time when a passenger can check into the flight online.
   */
  public String webCheckinTime;

  /**
   * Boarding time
   */
  public String boardingTime;

  /**
   * Description of the meals that will be provided or available for purchase.
   */
  public String mealService;

  public UmOrganization carrier;

  public UmOrganization provider;

  /**
   * The kind of aircraft (e.g., "Boeing 747").
   */
  public String aircraft;

  /**
   * The estimated time the flight will take.
   */
  public String estimatedFlightDuration;

  /**
   * The distance of the flight
   */
  public String flightDistance;

  public UmAirline getAirline() {
    if (airline == null) {
      airline = new UmAirline();
    }
    return airline;
  }

  public UmFlight setAirline(UmAirline airline) {
    this.airline = airline;
    return this;
  }

  public UmAirline getOperatedBy() {
    if (operatedBy == null) {
      operatedBy = new UmAirline();
    }
    return operatedBy;
  }

  public UmFlight setOperatedBy(UmAirline operatedBy) {
    this.operatedBy = operatedBy;
    return this;
  }
}
