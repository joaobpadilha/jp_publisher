package com.umapped.persistence.reservation.flight;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 
 * @author wei
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmAirport
    implements Serializable {
  /**
  * 
  */
  private static final long serialVersionUID = 1L;

  /**
   * Text IATA identifier for an airline or airport.
   */
  public String iataCode;

  /**
   * Text IACO identifier for an airport.
   */
  public String icaoCode;
}
