package com.umapped.persistence.reservation.flight;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.umapped.persistence.reservation.UmTraveler;

/**
 * Created by surge on 2016-11-25.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmFlightTraveler
    extends UmTraveler {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private String            seat;
  private UmAirplaneSeatClass seatClass;
  private String            boardingGroup;
  private String            priorityStatus;
  private String            sequenceNumber;
  private String            baggageAllowance; //Per person since depends on class
  private String            caryOnAllowance;  //Per person since depends on class
  private String            fareType;
  private String            fare;
  private String            expeditedSecurityType;
  private String            expeditedSecurityNumber;
  private String            meal;
  
  public String getBaggageAllowance() {
    return baggageAllowance;
  }

  public UmFlightTraveler setBaggageAllowance(String baggageAllowance) {
    this.baggageAllowance = baggageAllowance;
    return this;
  }

  public String getCaryOnAllowance() {
    return caryOnAllowance;
  }

  public UmFlightTraveler setCaryOnAllowance(String caryOnAllowance) {
    this.caryOnAllowance = caryOnAllowance;
    return this;
  }

  public String getFareType() {
    return fareType;
  }

  public UmFlightTraveler setFareType(String fareType) {
    this.fareType = fareType;
    return this;
  }

  public String getFare() {
    return fare;
  }

  public UmFlightTraveler setFare(String fare) {
    this.fare = fare;
    return this;
  }

  public String getExpeditedSecurityType() {
    return expeditedSecurityType;
  }

  public UmFlightTraveler setExpeditedSecurityType(String expeditedSecurityType) {
    this.expeditedSecurityType = expeditedSecurityType;
    return this;
  }

  public String getExpeditedSecurityNumber() {
    return expeditedSecurityNumber;
  }

  public UmFlightTraveler setExpeditedSecurityNumber(String expeditedSecurityNumber) {
    this.expeditedSecurityNumber = expeditedSecurityNumber;
    return this;
  }

  public String getSeat() {
    return seat;
  }

  public UmFlightTraveler setSeat(String seat) {
    this.seat = seat;
    return this;
  }

  public UmAirplaneSeatClass getSeatClass() {
    if (seatClass == null) {
      seatClass = new UmAirplaneSeatClass();
    }
    return seatClass;
  }

  public UmFlightTraveler setSeatClass(UmAirplaneSeatClass seatClass) {
    this.seatClass = seatClass;
    return this;
  }

  public String getBoardingGroup() {
    return boardingGroup;
  }

  public UmFlightTraveler setBoardingGroup(String boardingGroup) {
    this.boardingGroup = boardingGroup;
    return this;
  }

  public String getPriorityStatus() {
    return priorityStatus;
  }

  public UmFlightTraveler setPriorityStatus(String priorityStatus) {
    this.priorityStatus = priorityStatus;
    return this;
  }

  public String getSequenceNumber() {
    return sequenceNumber;
  }

  public UmFlightTraveler setSequenceNumber(String sequenceNumber) {
    this.sequenceNumber = sequenceNumber;
    return this;
  }

  public String getMeal() {
    return meal;
  }

  public UmFlightTraveler setMeal(String meal) {
    this.meal = meal;
    return this;
  }
  
  
}
