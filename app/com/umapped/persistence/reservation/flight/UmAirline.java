package com.umapped.persistence.reservation.flight;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.umapped.persistence.reservation.UmOrganization;

/**
 * Airline information 
 * 
 * @author wei
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmAirline extends UmOrganization
    implements Serializable {
  /**
  * 
  */
  private static final long serialVersionUID = 1L;

  public String boardingPolicy;

  /**
   * [Text] IATA identifier for an airline or airport.
   */
  public String iataCode;
}
