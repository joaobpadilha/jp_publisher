package com.umapped.persistence.reservation;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.umapped.persistence.notes.insurance.InsuranceTraveler;
import com.umapped.persistence.reservation.accommodation.UmAccommodationTraveler;
import com.umapped.persistence.reservation.activity.UmActivityTraveler;
import com.umapped.persistence.reservation.cruise.UmCruiseTraveler;
import com.umapped.persistence.reservation.flight.UmFlightTraveler;
import com.umapped.persistence.reservation.transfer.UmTransferTraveler;

/**
 * Simplified version of the person
 * Created by surge on 2016-11-25.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonSubTypes({
  @Type(value=UmFlightTraveler.class),
  @Type(value=UmAccommodationTraveler.class),
  @Type(value=UmCruiseTraveler.class),
  @Type(value=UmActivityTraveler.class),
  @Type(value=UmTransferTraveler.class),
  @Type(value=InsuranceTraveler.class)
})
public class UmTraveler
    implements Cloneable, Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  /**
   * Umapped Account ID
   */
  private Long              aid;
  /**
   * Last Name
   */
  private String            familyName;
  /**
   * First Name
   */
  private String            givenName;
  /**
   * Primary Email
   */
  private String            email;
  /**
   * Primary mobile phone
   */
  private String            mobilePhone;
  /**
   * Reservation Number
   */
  private String            reservationNumber;
  /**
   * eTicket Number
   */
  private String            ticketNumber;
  /**
   * Reward Program
   */
  private UmProgramMembership program;


  /****************************************************************************
   * Fields from ticket: PUB-1348                                             *
   ****************************************************************************/
  private String confirmStatus;
  private String confirmedBy;
  private String payment;
  /****************************************************************************
   * Fields from ticket: PUB-922                                              *
   ****************************************************************************/
  private String extras;
  private String included;
  private String upgrades;
  private String specialRequests;
  private String rate; //Duplicates per Reservation but for individual

  public String getConfirmStatus() {
    return confirmStatus;
  }

  public UmTraveler setConfirmStatus(String confirmStatus) {
    this.confirmStatus = confirmStatus;
    return this;
  }

  public String getConfirmedBy() {
    return confirmedBy;
  }

  public UmTraveler setConfirmedBy(String confirmedBy) {
    this.confirmedBy = confirmedBy;
    return this;
  }

  public String getPayment() {
    return payment;
  }

  public UmTraveler setPayment(String payment) {
    this.payment = payment;
    return this;
  }

  public String getExtras() {
    return extras;
  }

  public UmTraveler setExtras(String extras) {
    this.extras = extras;
    return this;
  }

  public String getIncluded() {
    return included;
  }

  public UmTraveler setIncluded(String included) {
    this.included = included;
    return this;
  }

  public String getUpgrades() {
    return upgrades;
  }

  public UmTraveler setUpgrades(String upgrades) {
    this.upgrades = upgrades;
    return this;
  }

  public String getSpecialRequests() {
    return specialRequests;
  }

  public UmTraveler setSpecialRequests(String specialRequests) {
    this.specialRequests = specialRequests;
    return this;
  }

  public String getRate() {
    return rate;
  }

  public UmTraveler setRate(String rate) {
    this.rate = rate;
    return this;
  }

  public String getReservationNumber() {
    return reservationNumber;
  }

  public UmTraveler setReservationNumber(String reservationNumber) {
    this.reservationNumber = reservationNumber;
    return this;
  }

  public String getTicketNumber() {
    return ticketNumber;
  }

  public UmTraveler setTicketNumber(String ticketNumber) {
    this.ticketNumber = ticketNumber;
    return this;
  }

  public UmProgramMembership getProgram() {
    if (program == null) {
      program = new UmProgramMembership();
    }
    return program;
  }

  public UmTraveler setProgram(UmProgramMembership program) {
    this.program = program;
    return this;
  }

  public Long getAid() {
    return aid;
  }

  public UmTraveler setAid(Long aid) {
    this.aid = aid;
    return this;
  }

  public String getFamilyName() {
    return familyName;
  }

  public UmTraveler setFamilyName(String familyName) {
    this.familyName = familyName;
    return this;
  }

  public String getGivenName() {
    return givenName;
  }

  public UmTraveler setGivenName(String givenName) {
    this.givenName = givenName;
    return this;
  }

  public String getEmail() {
    return email;
  }

  public UmTraveler setEmail(String email) {
    this.email = email;
    return this;
  }

  public String getMobilePhone() {
    return mobilePhone;
  }

  public UmTraveler setMobilePhone(String mobilePhone) {
    this.mobilePhone = mobilePhone;
    return this;
  }
  
  // Dervied property
  @JsonIgnore
  public String getName() {
    return (givenName != null ? givenName : "") + " " + (familyName != null ? familyName : "");
  }
}
