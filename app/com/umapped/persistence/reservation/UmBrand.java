package com.umapped.persistence.reservation;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmBrand 
  extends UmThing {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  public String logo;
}
