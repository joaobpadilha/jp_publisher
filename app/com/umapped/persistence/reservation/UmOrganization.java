package com.umapped.persistence.reservation;

public class UmOrganization
    extends UmThing {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * [PostalAddress]
   * Physical address of the item.
   */
  private UmPostalAddress address;
  
  public String url;
  
  /**
   * [ContactPoint]
   * A contact point for a person or organization. Supersedes contactPoints.
   */
  public String contactPoint;
  
  /**
   * [Text] The telephone number.
   */
  public String telephone;
  
  /**
   * [Text]
   * The fax number.
   */
  public String faxNumber;

  public UmPostalAddress getAddress() {
    if (address == null) {
      address = new UmPostalAddress();
    }
    return address;
  }

  public void setAddress(UmPostalAddress address) {
    this.address = address;
  }
  
  
}
