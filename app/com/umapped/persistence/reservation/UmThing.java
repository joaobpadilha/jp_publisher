package com.umapped.persistence.reservation;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmThing
    implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * The string shown to the user on the UI element tied to the action.
   */
  public String name;

  /**
   * An alias for the item.
   */
  public String alternateName;

  /**
   * A short description of the item.
   */
  public String description;
  
}