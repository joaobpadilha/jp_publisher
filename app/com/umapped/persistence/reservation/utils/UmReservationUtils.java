package com.umapped.persistence.reservation.utils;

/**
 * Helper class
 * 
 * @author wei
 *
 */
public class UmReservationUtils {
  
  /**
   * Safe cast, return null if the instance is not assignable from subtype
   * 
   * @param instance
   * @param subtype
   * @return
   */
  @SuppressWarnings("unchecked")
  public static <T> T cast(Object instance, Class<T> subtype) {
    if (instance == null) {
      return null;
    }
    
    if (subtype.isInstance(instance)) {
      return (T) (instance);
    }
    else {
      return null;
    }    
  }
  

}
