package com.umapped.persistence.reservation;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmProduct
    extends UmThing {

    /**
   * 
   */
  private static final long serialVersionUID = 1L;

    /**
     * [Brand] or [Organization] The brand(s) associated with a product or service, or the brand(s) maintained by an
     * organization or business person.
     */
    public UmBrand brand;

    /**
     * color  Text  The color of the product.
     */
    public String color;

    /**
     * [ReviewAction] Actions supported by Product.
     */
    public String action;

    /**
     * Text   The Stock Keeping Unit (SKU), i.e. a merchant-specific identifier for a product or service,
     * or the product to which the offer refers.
     */
    public String sku;


    /**
     * [ProductModel] or [Text]
     * The model of the product. Use with the URL of a ProductModel or a textual representation of the model identifier.
     * The URL of the ProductModel can be from an external source. It is recommended to additionally provide strong
     * product identifiers via the gtin8/gtin13/gtin14 and mpn properties.
     */
    public String model;

}
