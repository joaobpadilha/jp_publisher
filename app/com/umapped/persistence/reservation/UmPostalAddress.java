package com.umapped.persistence.reservation;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.StringUtils;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmPostalAddress
    implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * [Country] The country. For example, USA. You can also provide the two-letter ISO 3166-1 alpha-2 country code.
   */
  public String addressCountry;

  /**
   * Text   The locality. For example, Mountain View.
   */
  public String addressLocality;
  /**
   * Text   The region. For example, CA.
   */
  public String addressRegion;
  /**
   * Text   The post office box number for PO box addresses.
   */
  public String postOfficeBoxNumber;
  /**
   * Text   The postal code. For example, 94043.
   */
  public String postalCode;
  /**
   * Text   The street address. For example, 1600 Amphitheatre Pkwy.
   */
  public String streetAddress;

  /**
   * Umapped Specific: three-letter ISO 3166-1 alpha-3
   *
   * @return
   */
  public String a3Country;

  @JsonIgnore
  public boolean isEmpty() {
    return addressCountry == null || addressCountry.length() == 0 ||
           addressLocality == null || addressLocality.length() == 0;
  }

  public String[] toStringTwoLines() {
    String[] lines = {"", ""};

    if ((streetAddress != null &&
         streetAddress.trim().length() > 0) ||
        (addressLocality != null &&
         addressLocality.trim().length() > 0)) {
      StringBuilder sb = new StringBuilder();

      sb.append(StringUtils.trimToEmpty(streetAddress));
      if (sb.length()==0) {
        sb.append(StringUtils.trimToEmpty(addressLocality));
        lines[0] = sb.toString();
      } else {
        lines[0] = sb.toString();
        sb = new StringBuilder();
        sb.append(StringUtils.trimToEmpty(addressLocality));
      }

      if (addressRegion != null && addressRegion.trim().length() > 0) {
        if (sb.length() > 0) {
          sb.append(", ");
        }
        sb.append(StringUtils.trimToEmpty(addressRegion));
        sb.append(' ');
      }
      if (postalCode != null && postalCode.trim().length() > 0) {
        sb.append(postalCode.trim());
      }
      if (addressCountry != null && addressCountry.trim().length() > 0) {
        if (sb.length() > 0) {
          sb.append(" ");
        }
        sb.append(StringUtils.trimToEmpty(addressCountry));
      }
      lines[1] = sb.toString();
    }
    return lines;
  }

  public String toStringSingleLine() {
    if ((streetAddress != null &&
         streetAddress.trim().length() > 0) ||
        (addressLocality != null &&
         addressLocality.trim().length() > 0)) {
      StringBuilder sb = new StringBuilder();

      if (streetAddress != null) {
        sb.append(streetAddress.trim());
      }
      if (addressLocality != null && addressLocality.trim().length() > 0) {
        if (sb.toString().trim().length() > 0) {
          sb.append(", ");
        }
        sb.append(addressLocality.trim());
      }

      if (addressRegion != null && addressRegion.trim().length() > 0) {
        if (sb.toString().trim().length() > 0) {
          sb.append(", ");
        }
        sb.append(addressRegion.trim());
        sb.append(' ');
      }
      if (postalCode != null && postalCode.trim().length() > 0) {
        sb.append(postalCode.trim());
      }
      if (addressCountry != null && addressCountry.trim().length() > 0) {
        if (sb.length() > 0) {
          sb.append(" ");
        }
        sb.append(addressCountry.trim());
      }
      return sb.toString();
    }
    return "";

  }

  public String toString() {
    if ((streetAddress != null &&
         streetAddress.trim().length() > 0) ||
        (addressLocality != null &&
         addressLocality.trim().length() > 0)) {
      StringBuilder sb = new StringBuilder();

      if (streetAddress != null) {
        sb.append(streetAddress.trim());
      }
      if (addressLocality != null && addressLocality.trim().length() > 0) {
        sb.append(System.lineSeparator()).append(addressLocality.trim());
      }

      if (addressRegion != null && addressRegion.trim().length() > 0) {
        if (sb.toString().trim().length() > 0) {
          sb.append(", ");
        }
        sb.append(addressRegion.trim());
        sb.append(' ');
      }
      if (postalCode != null && postalCode.trim().length() > 0) {
        sb.append(postalCode.trim());
      }
      if (addressCountry != null && addressCountry.trim().length() > 0) {
        sb.append(System.lineSeparator()).append(addressCountry.trim());
      }
      return sb.toString();
    }
    return "";

  }

}
