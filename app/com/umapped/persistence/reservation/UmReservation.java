package com.umapped.persistence.reservation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.umapped.api.schema.local.PoiJson;
import com.umapped.persistence.reservation.accommodation.UmAccommodationReservation;
import com.umapped.persistence.reservation.activity.UmActivityReservation;
import com.umapped.persistence.reservation.cruise.UmCruiseReservation;
import com.umapped.persistence.reservation.flight.UmFlightReservation;
import com.umapped.persistence.reservation.transfer.UmTransferReservation;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;

/**
 * Primary home for all data related to various kinds of reservations.
 * <p>
 * As much as possible this structure will use Schema.org specified objects, but trying to make objects as common
 * as possible, but keeping objects as slim as possible to assist with de-serialization speed.
 * <p>
 * Created by surge on 2016-11-25.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonSubTypes({
  @Type(value=UmFlightReservation.class),
  @Type(value=UmAccommodationReservation.class),
  @Type(value=UmCruiseReservation.class),
  @Type(value=UmActivityReservation.class),
  @Type(value=UmTransferReservation.class)
})
public class UmReservation
    implements Cloneable, Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  /**
   * Status string for the reservation
   */
  private String           reservationStatus;
  /**
   * (required) The number or id of the reservation, if applies to all travelers.
   */
  private String           reservationNumber;
  private List<UmTraveler> travelers;
  /**
   * Local overrides for main poi
   */
  private PoiJson          mainLocation;
  /**
   * Local overrides for start location
   */
  private PoiJson          startLocation;
  /**
   * Local overrides for finish location
   */
  private PoiJson          finishLocation;

  /*****************************************************************************
   * Starting/Finishing Time information
   */
  private String      startLocationTimeZone;
  private String      finishLocationTimeZone;
  
  private UmDateTime  startDateTime;
  private UmDateTime  endDateTime;
  
  /****************************************************************************
   * Fields from ticket: PUB-1348                                             *
   ****************************************************************************/
  private String       payment;
  private String       cancellationPolicy;
  private String       notesPlainText;
  private String       notesHtml;
  private String       serviceType;
  private UmOrganization serviceProvider;

  /****************************************************************************
   * Fields from ticket: PUB-1149                                             *
   ****************************************************************************/
  //Key for rates has to be an ENUM (e.g. Daily Rate, Hourly Rate)
  //TODO: change rates field to <String, Float>
  //      have taxes and fees also be <String, Float> & be stored in same way as rates
  private List<UmRate> rates;
  private String              taxes;
  private String              fees;
  private String              total;
  private String              currency;
  private String              important;
  private String              subtotal;

  public List<UmRate> getRates() {
    return rates;
  }

  public UmReservation setRates(List<UmRate> rates) {
    this.rates = rates;
    return this;
  }

  public String getTaxes() {
    return taxes;
  }

  public UmReservation setTaxes(String taxes) {
    this.taxes = taxes;
    return this;
  }

  public String getFees() {
    return fees;
  }

  public UmReservation setFees(String fees) {
    this.fees = fees;
    return this;
  }

  public String getSubtotal() { return subtotal; }

  public UmReservation setSubtotal(String subtotal) {
    this.subtotal = subtotal;
    return this;
  }

  public String getTotal() {
    return total;
  }

  public UmReservation setTotal(String total) {
    this.total = total;
    return this;
  }

  public String getCurrency() {
    return currency;
  }

  public UmReservation setCurrency(String currency) {
    this.currency = currency;
    return this;
  }

  public String getImportant() {
    return important;
  }

  public UmReservation setImportant(String important) {
    this.important = important;
    return this;
  }

  public String getPayment() {
    return payment;
  }

  public UmReservation setPayment(String payment) {
    this.payment = payment;
    return this;
  }

  public String getCancellationPolicy() {
    return cancellationPolicy;
  }

  public UmReservation setCancellationPolicy(String cancellationPolicy) {
    this.cancellationPolicy = cancellationPolicy;
    return this;
  }

  public String getNotesPlainText() {
    return notesPlainText;
  }

  public UmReservation setNotesPlainText(String notesPlainText) {
    this.notesPlainText = notesPlainText;
    return this;
  }

  public String getNotesHtml() {
    return notesHtml;
  }

  public UmReservation setNotesHtml(String notesHtml) {
    this.notesHtml = notesHtml;
    return this;
  }

  public String getServiceType() {
    return serviceType;
  }

  public UmReservation setServiceType(String serviceType) {
    this.serviceType = serviceType;
    return this;
  }

  public UmOrganization getServiceProvider() {
    return serviceProvider;
  }

  public UmReservation setServiceProvider(UmOrganization serviceProvider) {
    this.serviceProvider = serviceProvider;
    return this;
  }

  public PoiJson getMainLocation() {
    return mainLocation;
  }

  public UmReservation setMainLocation(PoiJson mainLocation) {
    this.mainLocation = mainLocation;
    return this;
  }

  public PoiJson getStartLocation() {
    return startLocation;
  }

  public UmReservation setStartLocation(PoiJson startLocation) {
    this.startLocation = startLocation;
    return this;
  }

  public PoiJson getFinishLocation() {
    return finishLocation;
  }

  public UmReservation setFinishLocation(PoiJson finishLocation) {
    this.finishLocation = finishLocation;
    return this;
  }

  public String getReservationStatus() {
    return reservationStatus;
  }

  public UmReservation setReservationStatus(String reservationStatus) {
    this.reservationStatus = reservationStatus;
    return this;
  }

  public String getReservationNumber() {
    return reservationNumber;
  }

  public UmReservation setReservationNumber(String reservationNumber) {
    this.reservationNumber = reservationNumber;
    return this;
  }

  public List<UmTraveler> getTravelers() {
    return travelers;
  }

  public UmReservation setTravelers(List<UmTraveler> travelers) {
    this.travelers = travelers;
    return this;
  }

  public String getStartLocationTimeZone() {
    return startLocationTimeZone;
  }

  public UmReservation setStartLocationTimeZone(String startLocationTimeZone) {
    this.startLocationTimeZone = startLocationTimeZone;
    return this;
  }

  public String getFinishLocationTimeZone() {
    return finishLocationTimeZone;
  }

  public UmReservation setFinishLocationTimeZone(String finishLocationTimeZone) {
    this.finishLocationTimeZone = finishLocationTimeZone;
    return this;
  }

  public UmDateTime getStartDateTime() {
    return startDateTime;
  }

  @JsonProperty("startDateTime")
  public UmReservation setStartDateTime(UmDateTime startDateTime) {
    this.startDateTime = startDateTime;
    return this;
  }

  @JsonIgnore
  public UmReservation setStartDateTime(Long ts) {
    return setStartDateTime(timestampToDateTime(ts));
  }
 
  
  public UmDateTime getEndDateTime() {
    return endDateTime;
  }
  
  @JsonProperty("endDateTime")
  public UmReservation setEndDateTime(UmDateTime endDateTime) {
    this.endDateTime = endDateTime;
    return this;
  }
  
  @JsonIgnore
  public UmReservation setEndDateTime(Long ts) {
    return setEndDateTime(timestampToDateTime(ts));
  }
  
  private UmDateTime timestampToDateTime(Long ts) {
    if (ts == null) {
      return null;
    }
    Instant       i   = Instant.ofEpochMilli(ts);
    LocalDateTime ldt = LocalDateTime.ofInstant(i, ZoneId.of("UTC"));
    UmDateTime      dt  = new UmDateTime(ldt);
    return dt;
  }
}
