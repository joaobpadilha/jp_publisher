package com.umapped.persistence.reservation.activity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.umapped.persistence.reservation.UmReservation;

/**
 * Activity information
 * <p>
 * Created by surge on 2016-11-25.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmActivityReservation
    extends UmReservation {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  UmActivity activity;

  public UmActivity getActivity() {
    if (activity == null) {
      activity = new UmActivity();
    }
    return activity;
  }

  public UmActivityReservation setActivity(UmActivity activity) {
    this.activity = activity;
    return this;
  }
}
