package com.umapped.persistence.reservation;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import com.mapped.publisher.utils.Log;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * A date value in ISO 8601 date format.
 * YYYY-MM-DDT(see Chapter 5.4 of ISO 8601).
 * Created by surge on 2016-06-29.
 */
public class UmDate
  implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  public String value;

  public UmDate() {}

  public UmDate(String date) {
    value = date;
  }

  @JsonValue
  public String getValue() {
    return value;
  }

  public String toString() {
    return value;
  }

  /**
   * Converts ISO formatted DateTime string to java.sql.Timestamp used in Umapped VO
   * @return
   */
  @JsonIgnore
  public Timestamp getTimestamp() {
    if (value == null) {
      return null;
    }

    try {
      LocalDateTime ldt = LocalDateTime.parse(value, DateTimeFormatter.ISO_LOCAL_DATE);
      Instant  inst = ldt.toInstant(ZoneOffset.UTC);
      return Timestamp.from(inst);
    }
    catch (DateTimeParseException e) {
      Log.err("Date: encountered wrong date format:", value);
    }
    return null;
  }

}
