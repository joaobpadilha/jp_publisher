package com.umapped.persistence.reservation;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import com.mapped.publisher.utils.Log;

public class UmDateTime
    implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  public String value;
  LocalDateTime time;
  private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

  public UmDateTime() {
  }

  public UmDateTime(String dateTime) {
    try {
      value = dateTime;
      time = LocalDateTime.parse(value, formatter);
    } catch (Exception e) {

    }
  }

  public UmDateTime(LocalDateTime ldt) {
    time = ldt;
    value = ldt.toInstant(ZoneOffset.UTC).toString();
  }

  @JsonValue
  public String getValue() {
    if(value != null) {
      return value;
    }
    else {
      return time.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }
  }

  public UmDateTime setValue(String value) {
    try {
      this.value = value;
      this.time = LocalDateTime.parse(value, formatter);
    } catch (Exception e) {

    }
    return this;
  }

  public String toString() {
    return value;
  }

  @JsonIgnore
  public LocalDateTime getTime() {
    try {
      if (time != null) {
        return time;
      } else {
        return LocalDateTime.parse(value);
      }
    } catch (Exception e) {
      return null;
    }
  }

  public UmDateTime setTime(LocalDateTime time) {
    try {
      this.time = time;
      this.value = time.format(DateTimeFormatter.ISO_LOCAL_DATE);
    } catch (Exception e) {

    }
    return this;
  }

  /**
   * Converts ISO formatted DateTime string to java.sql.Timestamp used in
   * Umapped VO
   *
   * @return
   */
  @JsonIgnore
  public Timestamp getTimestamp() {
    if (value == null) {
      return null;
    }
    try {
      Instant inst = Instant.parse(value);
      return Timestamp.from(inst);
    }
    catch (DateTimeParseException e) {
      Log.err("Date: encountered wrong date format:", value);
    }
    return null;
  }

  /**
   * Converts to ISO8601 Date YYYY-MM-DD
   */
  @JsonIgnore
  public String getISODate() {
    if (time == null) {
      return "";
    }
    try {
      return time.format(DateTimeFormatter.ISO_LOCAL_DATE);
    } catch (Exception e) {

    }
    return  "";
  }
}
