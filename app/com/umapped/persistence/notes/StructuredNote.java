package com.umapped.persistence.notes;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.mapped.publisher.form.TripNoteForm;
import com.umapped.persistence.notes.insurance.InsuranceNote;
import com.umapped.persistence.notes.tableNote.TableNote;
import com.umapped.persistence.reservation.UmTraveler;
import models.publisher.TripNote;

/**
 * Created by george on 2017-03-21.
 */

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonSubTypes({
    @Type(value=InsuranceNote.class),
    @Type(value=TableNote.class)
})

public abstract class StructuredNote
    implements Cloneable, Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * The string shown to the user on the UI element tied to the action.
   */
  public String name;

  /**
   * An alias for the item.
   */
  public String alternateName;

  /**
   * A short description of the item.
   */
  public String description;

  private List<UmTraveler> travelers;

  public List<UmTraveler> getTravelers() {
    return travelers;
  }

  public void setTravelers(List<UmTraveler> travelers) {
    this.travelers = travelers;
  }

  //Used to create Note object to be saved, for respective note type
  public StructuredNote populateNoteByType(TripNote tripNote, TripNoteForm tripNoteInfo) {
    return this.populateNote(tripNote, tripNoteInfo);
  }

  protected abstract StructuredNote populateNote(TripNote tripNote, TripNoteForm tripNoteInfo);


  //Used to create intro to be displayed on itinerary view page etc
  public String renderNoteByType (StructuredNote note) {
    return this.renderIntro(note);
  }

  protected abstract String renderIntro(StructuredNote note);


}
