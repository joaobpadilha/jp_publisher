package com.umapped.persistence.digest;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by twong on 07/04/16.
 */
public class CommunicatorMsg implements Serializable {
    private Timestamp msgTimestamp;
    private String tripId;
    private String tripName;
    private String roomId;
    private String roomName;
    private String msgBody;
    private String senderLegacyId;
    private Long senderId;
    private String senderName;
    private String senderInitial;
    private String replyEmail;

    public CommunicatorMsg () {

    }
    public CommunicatorMsg (String tripId, String tripName, String msgBody, String roomId, String roomName, Long senderId, String senderLegacyId, String senderName, String senderInitial, Timestamp timestamp)  {
        this.tripId = tripId;
        this.tripName = tripName;
        this.msgBody = msgBody;
        this.senderId = senderId;
        this.roomId = roomId;
        this.roomName = roomName;
        this.senderLegacyId = senderLegacyId;
        this.senderName = senderName;
        this.senderInitial = senderInitial;
        this.msgTimestamp = timestamp;

    }


    public Timestamp getMsgTimestamp() {
        return msgTimestamp;
    }

    public CommunicatorMsg setMsgTimestamp(Timestamp msgTimestamp) {
        this.msgTimestamp = msgTimestamp;
        return  this;
    }

    public String getTripId() {
        return tripId;
    }

    public CommunicatorMsg setTripId(String tripId) {
        this.tripId = tripId;
        return this;
    }

    public String getMsgBody() {
        return msgBody;
    }

    public CommunicatorMsg setMsgBody(String msgBody) {
        this.msgBody = msgBody;
        return this;
    }


    public String getSenderName() {
        return senderName;
    }

    public CommunicatorMsg setSenderName(String senderName) {
        this.senderName = senderName;
        return this;
    }

    public String getSenderInitial() {
        return senderInitial;
    }

    public CommunicatorMsg setSenderInitial(String senderInitial) {
        this.senderInitial = senderInitial;
        return this;
    }

    public String getSenderLegacyId() {
        return senderLegacyId;
    }

    public CommunicatorMsg setSenderLegacyId(String senderLegacyId) {
        this.senderLegacyId = senderLegacyId;
        return this;
    }

    public CommunicatorMsg setSenderId(Long senderId) {
        this.senderId = senderId;
        return this;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public Long getSenderId() {
        return senderId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getReplyEmail() {
        return replyEmail;
    }

    public void setReplyEmail(String replyEmail) {
        this.replyEmail = replyEmail;
    }
}
