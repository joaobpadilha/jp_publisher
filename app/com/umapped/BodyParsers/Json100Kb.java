package com.umapped.BodyParsers;

import play.http.HttpErrorHandler;
import play.mvc.BodyParser;

import javax.inject.Inject;

/**
 * Created by surge on 2016-11-16.
 */
public class Json100Kb
    extends BodyParser.Json {
  @Inject
  public Json100Kb(HttpErrorHandler errorHandler) {
    super(1024 * 100, errorHandler);
  }
}
