package com.umapped.BodyParsers;

import play.http.HttpErrorHandler;
import play.mvc.BodyParser;

import javax.inject.Inject;

/**
 * Created by surge on 2016-11-16.
 */
public class TolerantText1Mb
    extends BodyParser.TolerantText {
  @Inject
  public TolerantText1Mb(HttpErrorHandler errorHandler) {
    super(1024 * 1024, errorHandler);
  }
}
