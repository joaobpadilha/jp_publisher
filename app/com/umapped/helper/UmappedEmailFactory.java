package com.umapped.helper;

import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.UmappedEmail;
import models.publisher.Company;
import models.publisher.CompanySmtp;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;

/**
 * Created by twong on 2016-08-25.
 */
public class UmappedEmailFactory {
  private static HashMap<CompanySmtp.CmpySmtpPk, CompanySmtp> whiteLabelEmails =  new HashMap<>();
  private static Instant lastUpdate = null;

  public UmappedEmailFactory() {
  }

  public static void load() {
    UmappedEmailFactory.whiteLabelEmails =  new HashMap<>();
    List<CompanySmtp> smtps =  CompanySmtp.find.all();
    if (smtps != null) {
      for (CompanySmtp c: smtps) {
        UmappedEmailFactory.whiteLabelEmails.put(c.getPk(), c);
      }
    }
    UmappedEmailFactory.lastUpdate = Instant.now();
  }

  //get a whitelabeled email instance if the cmpy is configured
  //if not, get the umapped default
  public static UmappedEmail getInstance(String cmpyId) {
    //reload if more than 1 hour - poor man's eventual consistency without the need to restart the cluster when something is changed.
    Instant now = Instant.now();
    synchronized (UmappedEmailFactory.lastUpdate) {
      if (lastUpdate == null || now.isAfter(lastUpdate.plus(Duration.ofHours(1)))) {
        UmappedEmailFactory.load();
        UmappedEmailFactory.lastUpdate = now;
        Log.info("WhiteLabelEmailMgrHelper - reloaded " + now );
      }
    }

    CompanySmtp.CmpySmtpPk pk = new CompanySmtp.CmpySmtpPk(0, cmpyId);

    if (UmappedEmailFactory.whiteLabelEmails.containsKey(pk)) {
      CompanySmtp smtp = UmappedEmailFactory.whiteLabelEmails.get(pk);
      return (UmappedEmail.build(smtp.getHost(),
                                 smtp.getUsername(),
                                 smtp.getPassword(),
                                 smtp.getPort(),
                                 smtp.getEmailDefault()).setVia(""));
    } else {
      //check the parent company
      Company c = Company.find.byId(cmpyId);
      if (c != null && c.getParentCmpyId() != null && !c.getParentCmpyId().isEmpty()) {
        pk = new CompanySmtp.CmpySmtpPk(0, c.getParentCmpyId());
        if (UmappedEmailFactory.whiteLabelEmails.containsKey(pk)) {
          CompanySmtp smtp = UmappedEmailFactory.whiteLabelEmails.get(pk);
          return (UmappedEmail.build(smtp.getHost(),
                  smtp.getUsername(),
                  smtp.getPassword(),
                  smtp.getPort(),
                  smtp.getEmailDefault()).setVia(""));
        }
      }
    }

    return (UmappedEmail.buildDefault());
  }

  public static UmappedEmail getInstance() {
    return (UmappedEmail.buildDefault());
  }

}
