package com.umapped.mobile.api;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Query;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mapped.publisher.actions.AccountJwtAuth;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.js.BaseJsonResponse;
import com.mapped.publisher.parse.extractor.booking.email.ReservationsHolder;
import com.mapped.publisher.parse.extractor.booking.valueObject.NoteVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.parse.schemaorg.*;
import com.mapped.publisher.persistence.VOModeller;
import com.mapped.publisher.persistence.redis.RedisKeys;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.view.DestinationSearchView;
import com.mapped.publisher.view.TripBookingDetailView;
import com.mapped.publisher.view.TripBookingView;
import com.umapped.BodyParsers.TolerantText1Mb;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.helper.TripPublisherHelper;
import com.umapped.itinerary.analyze.ItineraryAnalyzeResult;
import com.umapped.itinerary.analyze.model.ItineraryItem;
import com.umapped.itinerary.analyze.segment.SegmentNature;
import com.umapped.itinerary.analyze.segment.TransitCategory;
import com.umapped.itinerary.analyze.segment.TripSegment;
import com.umapped.itinerary.analyze.segment.TripSegmentAnalyzeResult;
import com.umapped.mobile.api.model.*;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.helpers.TripReservationsHelper;
import controllers.BookingController;
import controllers.BookingNoteController;
import controllers.TripController;
import controllers.WebItineraryController;
import models.publisher.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;
import play.mvc.With;

import java.util.*;

/**
 * Created by wei on 2017-06-30.
 */
@Singleton public class MobileItineraryService {
  @Inject RedisMgr redis;
  @Inject TripQueries queries;
  @Inject
  TripPublisherHelper tripPublisherHelper;

  private static long ONE_DAY_MINUS_MILLSECOND = 24 * 60 * 60 * 1000L - 1;

  public GetItinerariesResponse getItineraries(GetItinerariesRequest request) {
    StopWatch sw = new StopWatch();
    sw.start();
    try {
      Account account = Account.find.byId(request.getAccountId());
      GetItinerariesResponse response = new GetItinerariesResponse();
      Set<Pair<String, Integer>> tripIds = new HashSet<>();
      long currentTs = System.currentTimeMillis();
      response.setSyncTimestamp(currentTs);

      if (account != null) {
        // minus 5000 millsec to in case node system time difference
        long lastSyncTs = request.getLastSyncTimestamp() == null ? 0 : request.getLastSyncTimestamp() - 5000;
        tripIds.addAll(queries.getTravelerChangedActiveTrips(account, lastSyncTs));
        if (lastSyncTs > 0) {
          // get the trip ids that are removed from this traveler
          tripIds.addAll(queries.getDeleteTravelerTrip(account, lastSyncTs));
        }
        if (account.getAccountType() == Account.AccountType.PUBLISHER) {
          tripIds.addAll(queries.getPublisherChangedActiveTrips(account, lastSyncTs));
        }
      }
      if (tripIds != null) {
        List<Itinerary> itineraryList = new ArrayList<>();
        for (Pair<String, Integer> tripIdStatus : tripIds) {
          if (tripIdStatus.getRight() == -1) {
            Itinerary itinerary = new Itinerary();
            ItineraryInfo itineraryInfo = new ItineraryInfo();
            itineraryInfo.id = tripIdStatus.getLeft();
            itineraryInfo.status = ItineraryStatus.DELETED;
            itinerary.setItineraryInfo(itineraryInfo);
            itineraryList.add(itinerary);
          }
          else {
            itineraryList.add(getItinerary(account, Trip.findByPK(tripIdStatus.getLeft())));

          }
          response.setItineraries(itineraryList);
        }
      }
      else {
        response.setItineraries(Collections.emptyList());
      }
      return response;
    } catch (Exception e) {
      Log.err("Fail to get itineraries", e);
      GetItinerariesResponse response = new GetItinerariesResponse();
      response.setSyncTimestamp(request.getLastSyncTimestamp());
      return response;
    } finally {
      Log.debug("Total time to get Itineraries: " + sw.getTime());
    }
  }

  public InviteUserResponse inviteUser(InviteUserRequest request, Long accountId) {
    InviteUserRequest req = normalize(request);

    InviteUserStatus status = validateInviteUserRequest(req);
    if (status != InviteUserStatus.SUCCESSFUL) {
      return new InviteUserResponse(status);
    }

    Account account = Account.find.byId(accountId);

    if (account.getAccountType() != Account.AccountType.TRAVELER) {
      return new InviteUserResponse(InviteUserStatus.NOT_ALLOWED);
    }

    AccountTripLink userATL = AccountTripLink.findByUid(accountId, req.getTripId());

    try {
      List<AccountTripLink> tripLinks = AccountTripLink.findByEmailTrip(req.getEmail(), req.getTripId());
      if (tripLinks == null || tripLinks.size() == 0) {
        Trip trip = Trip.findByPK(req.getTripId());
        if (trip == null) {
          Log.err("Trip Id is not valid:" + req.getTripId());
          return new InviteUserResponse((InviteUserStatus.UNEXPECTED_ERROR));
        }
        String groupId = null;
        if (userATL != null) {
          groupId = userATL.getLegacyGroupId();
        }
        TripGroup group = TripGroup.findByPK(groupId);

        String name = null;
        AccountTripLink tripLink = null;


        TripPublisherHelper.TravelerResponse response = TripPublisherHelper.addPassenger(req.getFirstName(),
                                                                                         req.getLastName(),
                                                                                         req.getEmail(),
                                                                                         null,
                                                                                         accountId.toString(),
                                                                                         trip,
                                                                                         group,
                                                                                         true, null);

      }
    } catch (Exception e) {
      Log.err("Unexpecte error for inviteUser", e);
      return new InviteUserResponse(InviteUserStatus.UNEXPECTED_ERROR);
    }
    return new InviteUserResponse(InviteUserStatus.SUCCESSFUL);
  }

  private InviteUserRequest normalize(InviteUserRequest request) {
    InviteUserRequest formatted = new InviteUserRequest();
    formatted.setTripId(StringUtils.trimToNull(request.getTripId()));
    formatted.setEmail(StringUtils.lowerCase(StringUtils.trimToNull(request.getEmail())));
    formatted.setFirstName(StringUtils.trimToNull(request.getFirstName()));
    formatted.setLastName(StringUtils.trimToNull(request.getLastName()));
    return formatted;
  }

  private InviteUserStatus validateInviteUserRequest(InviteUserRequest request) {

    if (request.getTripId() == null) {
      Log.err("InviteUser: empty trip id");
      return InviteUserStatus.UNEXPECTED_ERROR;
    }
    if (request.getEmail() == null) {
      return InviteUserStatus.EMAIL_REQUIRED;
    }
    if (request.getFirstName() == null) {
      return InviteUserStatus.FIRST_NAME_REQUIRED;
    }
    if (request.getLastName() == null) {
      return InviteUserStatus.LAST_NAME_REQUIRED;
    }

    if (!MobileAPISerivceHelper.validateEmailAddress(request.getEmail())) {
      return InviteUserStatus.EMAIL_INVALID_FORMAT;
    }
    return InviteUserStatus.SUCCESSFUL;
  }

  public List<Itinerary> getItinerary(Long uid) {
    Account account = Account.find.byId(uid);
    List<Trip> trips = null;
    if (account != null) {
      switch (account.getAccountType()) {
        case TRAVELER:
          trips = findActiveTripsForTraveler(account);
          break;
        case PUBLISHER:
          trips = findActiveTripsForPublisher(account);
          break;
      }
    }
    if (trips != null) {
      List<Itinerary> itineraryList = new ArrayList<>();
      for (Trip trip : trips) {
        itineraryList.add(getItinerary(account, trip));
      }
      return itineraryList;
    }
    return Collections.emptyList();
  }

  protected Itinerary getItinerary(Account account, Trip trip) {
    Itinerary itinerary = new Itinerary();
    List<TripDetail> tripDetails = TripDetail.findActiveByTripId(trip.getTripid());
    itinerary.setAuthorization(getAuthorization(account, trip));
    itinerary.setItineraryInfo(getItineraryInfo(trip));
    itinerary.setReservations(getReservationPackage(trip, tripDetails));
    itinerary.setDocuments(getDocuments(trip.getTripid()));
    itinerary.setMap(getMap(trip, tripDetails));
    itinerary.setAnalysisResult(new ItinearyAnalysisResult(getTripSegments(trip.getTripid())));
    return itinerary;
  }

  protected ItineraryAuthorization getAuthorization(Account account, Trip trip) {
    ItineraryAuthorization authorization = new ItineraryAuthorization();
    authorization.setAccessLevel(SecurityMgr.getTripAccessLevel(trip, account));
    return authorization;
  }

  protected ItineraryInfo getItineraryInfo(Trip trip) {
    ItineraryInfo info = new ItineraryInfo();
    info.status = trip.getStatus() != 0 ? ItineraryStatus.PENDING : ItineraryStatus.PUBLISHED;
    info.name = trip.getName();
    info.coverUrl = trip.getImageUrl();
    info.id = trip.getTripid();
    info.startTs = trip.getStarttimestamp();
    info.finishTs = trip.getEndtimestamp() + ONE_DAY_MINUS_MILLSECOND;
    info.note = trip.getComments();
    info.collab = false;
    info.bc =  WebItineraryController.getTripBusinessCard(trip, null);
    if (info.bc != null) {
      if (info.bc.companyPhone != null && info.bc.phone != null && info.bc.companyPhone.equals(info.bc.phone)) {
        info.bc.phone = null;
      }
      if (info.bc.socialFacebookUrl != null && !info.bc.socialFacebookUrl.isEmpty()) {
        info.bc.companyFacebookUrl = info.bc.socialFacebookUrl;
      }
      if (info.bc.socialTwitterUrl != null && !info.bc.socialTwitterUrl.isEmpty()) {
        info.bc.companyTwitterUrl = info.bc.socialTwitterUrl;
      }

    }
    return info;
  }

  protected ReservationPackage getReservationPackage(Trip trip, List<TripDetail> tripDetails) {

    List<TripNote> tripNotes = TripNote.getNotesByTripId(trip.getTripid());
    TripReservationsHelper trHelper = TripReservationsHelper.build().
        setTrip(trip).setBookings(tripDetails).setNotes(tripNotes, trip);
    ReservationPackage rp = trHelper.prepareReservationPackage(true);
    return rp;
  }

  protected ItineraryDocuments getDocuments(String tripId) {
    ItineraryDocuments docs = new ItineraryDocuments();

    DestinationSearchView dsv = TripController.getGuidesImproved(tripId);
    docs.docs = dsv.destinationList;
    docs.attachments = WebItineraryController.getTripAttachments(tripId, 0);
    return docs;
  }

  /**
   * TODO: replace the method that copied from WebItineraryAPIController
   * @param rp
   * @return
   */
  protected ItineraryMap getMap(ReservationPackage rp) {
    if (rp.sorted.itemListElement != null) {
      for (ListItem it : rp.sorted.itemListElement) {
        //it.item.
      }
    }
    return null;
  }

  protected ItineraryMap getMap(Trip trip, List<TripDetail> tripDetails) {
    ItineraryMap tm = null;
    try {
      Optional<ItineraryMap> otp = redis.hashGetBin(RedisKeys.H_TRIP,
                                                    trip.getTripid(),
                                                    RedisKeys.HF_TRIP_MAP_MOBILE.name(),
                                                    ItineraryMap.class);

      if (otp.isPresent()) {
        tm = otp.get();
      }
      else {
        tm = new ItineraryMap();

        TripBookingView tbv = WebItineraryController.buildTripBookingView(trip, tripDetails, false);
        //TODO: Needs to be optimized
        //From already created Trip Bookings View creating list of locations
        for (ReservationType type : tbv.bookings.keySet()) {
          List<TripBookingDetailView> bdList = tbv.bookings.get(type);
          for (TripBookingDetailView tbdv : bdList) {
            if (tbdv.locStartLat != null && tbdv.locStartLat != 0.0 && tbdv.locStartLong != null && tbdv.locStartLong
                                                                                                    != 0.0) {

              tm.addLocation(tbdv.name, tbdv.locStartLat, tbdv.locStartLong);
            }

            if (tbdv.detailsTypeId == ReservationType.FLIGHT && tbdv.locFinishLat != null && tbdv.locFinishLat != 0.0
                && tbdv.locFinishLong != null && tbdv.locFinishLong != 0.0) {
              tm.addLocation(tbdv.name, tbdv.locFinishLat, tbdv.locFinishLong);
            }
          }
        }


        for (TripBookingDetailView tbdv : tbv.notes) {
          if (tbdv.locStartLat != null && tbdv.locStartLat != 0.0 && tbdv.locStartLong != null && tbdv.locStartLong
                                                                                                  != 0.0) {
            tm.addLocation(tbdv.name, tbdv.locStartLat, tbdv.locStartLong);
          }
        }

        redis.hashSet(RedisKeys.H_TRIP, trip.getTripid(), RedisKeys.HF_TRIP_MAP_MOBILE.name(), tm, RedisMgr.WriteMode.UNSAFE);
      }
    }
    catch (Exception e) {
      Log.err("fail to get map: ",e );
    }
    return tm;
  }

  List<Trip> findActiveTripsForTraveler(Account account) {
    Query<AccountTripLink> atlPks = Ebean.createQuery(AccountTripLink.class)
                                         .select("pk.tripid")
                                         .where()
                                         .eq("pk.uid", account.getUid())
                                         .query();
    List<AccountTripLink> pks = atlPks.findList();


    return Trip.find.where()
                    .in("tripId", atlPks)
                    .ge("endtimestamp", System.currentTimeMillis())     // end date in future
                    .eq("status", 0)           // status is active
                    .orderBy("starttimestamp")
                    .findList();
  }

  List<Trip> findActiveTripsForPublisher(Account account) {
    return Trip.find.where()
             .eq("createdby", account.getLegacyId())
             .ge("endtimestamp", System.currentTimeMillis())     // end date in future
             .ge("status", 0)           // status is active
             .orderBy("starttimestamp")
             .findList();
  }

  public List<TripSegmentData> getTripSegments(String tripId) {
    TripAnalysis analysis = TripAnalysis.findLastGoodAnalysis(tripId);
    List<TripSegmentData> segmentDataList = new ArrayList<>();
    try {
      if (analysis != null) {
        ItineraryAnalyzeResult result = Json.fromJson(analysis.getItineraryAnalyzeResult(), ItineraryAnalyzeResult
            .class);
        if (result != null) {
          List<TripSegmentAnalyzeResult> segmentAnalyzeResults = result.getSegmentResults();
          if (segmentAnalyzeResults != null) {
            for (TripSegmentAnalyzeResult segmentResult : segmentAnalyzeResults) {
              TripSegmentData data = null;
              TripSegment segment = segmentResult.getSegment();
              if (segment.hasNature(SegmentNature.Transit) && TransitCategory.ContinuousTransit.equals(segment.getCategory())) {
                data = addItem(data, segment.getMainItem());
              }
              else if (segment.hasNature(SegmentNature.Layover)) {
                if (segment.getAccommodations() != null) {
                  for (ItineraryItem item : segment.getAccommodations()) {
                    data = addItem(data, item);
                  }
                }

                if (segment.getActivities() != null) {
                  for (ItineraryItem item : segment.getActivities()) {
                    data = addItem(data, item);
                  }
                }
                if (data != null) {
                  data.addLocation(segment.getStartLocation());
                  data.addLocation(segment.getEndLocation());
                }
              }
              else if (segment.hasNature(SegmentNature.Cruise)) {

              }
              if (data != null) {
                segmentDataList.add(data);
              }
            }
          }
        }
      }
    } catch (Exception e) {
      Log.err("Fail to get trip analysis result", e);
    }
    return segmentDataList;
  }

  private TripSegmentData addItem(TripSegmentData data, ItineraryItem item) {

    TripSegmentData result = data;
    if (item !=null) {
      if (result == null) {
        result = new TripSegmentData();
      }

      result.addItem(item);
    }
    return result;
  }



  public Pair<MobileAPIResponseCode, Itinerary> editReservation(Account account, String tripId, JsonNode reservationJson) {
    StopWatch sw = new StopWatch();
    sw.start();
    ReservationsHolder rh = new ReservationsHolder();

    if (!rh.assignReservationFromJson(reservationJson)) {
      return new ImmutablePair<>(MobileAPIResponseCode.INVALID_REQUEST, null);
    }


    Trip trip = Trip.findByPK(tripId);

    if(trip == null) {
      Log.err("Trip can not be found", tripId);
      return new ImmutablePair<>(MobileAPIResponseCode.INVALID_REQUEST, null);
    }

    TripVO tvo = rh.toTripVO(null);


    SecurityMgr.AccessLevel al = SecurityMgr.getTripAccessLevel(trip, account);
    if (al.lt(SecurityMgr.AccessLevel.TRAVELER)) {
      Log.err("No trip modification access", tripId);
      return new ImmutablePair<>(MobileAPIResponseCode.INVALID_REQUEST, null);
    }

    VOModeller voModeller =  new VOModeller(trip, account, false);
    voModeller.setGenerateNotes(false); //Never generate comments in API mode
    voModeller.buildTripVOModels(tvo);

    //TODO: Figure out if the booking being submitted can be overwritten by the current account

    try {
      voModeller.saveAllModels();

      // save note attachment without using the VOModeller
      List<NoteVO> notes = tvo.getNotes();
      List<TripNote> tripNotes = voModeller.getNotes();
      List<ImageObject> photos = null;
      if (notes != null && notes.size() == 1 && tripNotes != null && tripNotes.size() == 1) {
        Log.debug("Find note");
        NoteVO note = notes.get(0);
        TripNote tripNote = tripNotes.get(0);
        photos = note.getImages();

        // delete and sorting
        List<TripNoteAttach> attachList = TripNoteAttach.findByNoteId(tripNote.getNoteId());
        for (TripNoteAttach attach : attachList) {
          int rank = -1;
          if (photos != null) {
            for (int i = 0; i < photos.size(); i++) {
              if (attach.getName().equals(photos.get(i).name)) {
                rank = i;
                break;
              }
            }
          }
          if (rank == -1) {
            Log.debug("Remove image: " + attach.getName());
            attach.delete();
          }
          else {
            Log.debug("Set Rank image: " + attach.getName() + "; rank = " + rank);
            attach.setRank(rank);
            attach.save();
          }
        }
      }

      BookingController.invalidateCache(tripId, null); //Unfortunately for now removing all caches

      //let's schedule this trip to be republished
      AccountTripLink  tripLink = AccountTripLink.findByUid(account.getUid(), trip.getTripid());
      if (tripLink != null) {
        tripPublisherHelper.autoPublishTrip(trip, tripLink.getLegacyGroupId());
      }

    } catch (Exception e) {
      Log.err("Fail to update reservation", e);
      return new ImmutablePair<>(MobileAPIResponseCode.UNEXPECTED_ERROR, null);
    }
   sw.stop();
    Log.debug("WIAPI: Reservations: Created/Edited in " + sw.getTime() + "ms");
    Itinerary updatedItinerary = getItinerary(account, trip);
    return new ImmutablePair<>(MobileAPIResponseCode.SUCCESS, updatedItinerary);
  }


  public Pair<MobileAPIResponseCode, Itinerary> deleteReservation(Account account, String tripId, String detailId) {
    StopWatch sw = new StopWatch();
    sw.start();

    Trip trip = Trip.findByPK(tripId);
    if(trip == null) {
      Log.err("Trip can not be found", tripId);
      return new ImmutablePair<>(MobileAPIResponseCode.INVALID_REQUEST, null);
    }

    SecurityMgr.AccessLevel al = SecurityMgr.getTripAccessLevel(trip, account);
    if (al.lt(SecurityMgr.AccessLevel.TRAVELER)) {
      Log.err("No trip modification access", tripId);
      return new ImmutablePair<>(MobileAPIResponseCode.INVALID_REQUEST, null);
    }

    //TODO: Verify security to access individual reservation

    TripDetail td = TripDetail.findByPK(detailId);
    if (td != null) {
      td.setStatus(APPConstants.STATUS_DELETED);
      td.update();
      BookingController.invalidateCache(td.getTripid(), td.getDetailsid());
    } else {
      try {
        TripNote tn = TripNote.find.byId(Long.parseLong(detailId));
        if (tn != null) {
          tn.setStatus(APPConstants.STATUS_DELETED);
          tn.update();
          BookingNoteController.invalidateCache(trip.tripid);
        }
      } catch (NumberFormatException nfe) {

      }
    }

    //let's schedule this trip to be republished
    AccountTripLink  tripLink = AccountTripLink.findByUid(account.getUid(), trip.getTripid());
    if (tripLink != null) {
      tripPublisherHelper.autoPublishTrip(trip, tripLink.getLegacyGroupId());
    }

    sw.stop();
    Log.debug("WIAPI: Reservations: Deleted in " + sw.getTime() + "ms");
    Itinerary updatedItinerary = getItinerary(account, trip);
    return new ImmutablePair<>(MobileAPIResponseCode.SUCCESS, updatedItinerary);
  }
}
