package com.umapped.mobile.api;


import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.utils.Log;
import com.umapped.mobile.api.model.FirebaseUserToken;
import models.publisher.Account;
import models.publisher.AccountTripLink;
import org.apache.commons.lang.StringUtils;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by wei on 2017-11-06.
 */
public class MobileFirebaseService {
  public static MobileFirebaseService instance = null;

  private boolean initialized = false;

  public static MobileFirebaseService getIntance() {
    if (instance == null) {
      instance = new MobileFirebaseService();
      instance.initialize();
    }
    else if (!instance.initialized) {
      instance.initialize();
    }
    return instance;
  }

  public void initialize() {
    FirebaseOptions option = getConnectionConfig();
    try {
      if (option != null) {
        FirebaseApp.initializeApp(option);
        this.initialized = true;
      }
    }
    catch (Exception e) {
      Log.err("Fail to initialize firebase admin");
    }
  }

  private FirebaseOptions getConnectionConfig() {
    String serviceAccountPath = null;
    String dbUrl = null;
    switch (ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.ENVIRONMENT)) {
      case "PROD":
        serviceAccountPath = "conf/certs/prd/umcom-prd-firebase-adminsdk-damwj-8242a23015.json";
        dbUrl = "https://umcom-prd.firebaseio.com";
        break;
      case "STAGING":
        serviceAccountPath = "conf/certs/dev/um-staging-firebase-admin.json";
        dbUrl = "https://um-staging.firebaseio.com/";
        break;
      case "TEST":
        serviceAccountPath = "conf/certs/dev/um-test-firebase-admin.json";
        dbUrl = "firebase-adminsdk-c75kl@umcom-test.iam.gserviceaccount.com";
        break;
      case "DEV":
        serviceAccountPath = "conf/certs/dev/um-dev-firebase-admin.json";
        dbUrl = "https://um-dev-90420.firebaseio.com/";
        break;
      default:
        serviceAccountPath = "conf/certs/dev/um-dev-firebase-admin.json";
        dbUrl = "https://um-dev-90420.firebaseio.com/";
        break;
    }
    if (serviceAccountPath != null && dbUrl != null) {
      try {
        FileInputStream serviceAccount = new FileInputStream(serviceAccountPath);
        FirebaseOptions options = new FirebaseOptions.Builder().setCredentials(GoogleCredentials.fromStream(
            serviceAccount)).setDatabaseUrl(dbUrl).build();
        return options;
      }
      catch (Exception e) {
        Log.err("Fail to initialze the firebase admin", e);
      }
    }
    return null;
  }

  public FirebaseUserToken createToken(Long accountUid) {
    Account account = Account.find.byId(accountUid);
    if (account != null) {
      Map<String, Object> claims = new HashMap<>();
      claims.put("displayName", account.getFullName());
      if (account.getAccountType() == Account.AccountType.TRAVELER) {
        List<AccountTripLink> atls = AccountTripLink.findAllByUid(accountUid);
        List<String> atlLegacyIds = atls.stream().map((atl) -> atl.getLegacyId()).collect(Collectors.toList());
        claims.put("linkedIds", StringUtils.join(atlLegacyIds, ","));
        return new FirebaseUserToken(createToken(account.getUid().toString(), claims), atlLegacyIds);
      }
      else {
        return new FirebaseUserToken(createToken(account.getLegacyId().replace('.', '_'), claims));
      }
    }
    else {
      Log.err("No account found: " + accountUid);
      return null;
    }
  }

  public String createToken(Long accountUid, String tripId) {
    AccountTripLink atl = AccountTripLink.findByUid(accountUid, tripId);
    if (atl != null) {
      return createToken(atl.getLegacyId(), null);
    }
    return null;
  }

  public String createToken(String uid, Map<String, Object> claims) {
    try {
      String customToken = FirebaseAuth.getInstance().createCustomTokenAsync(uid, claims).get();
      return customToken;
    }
    catch (Exception e) {
      Log.err("Fail to get token", e);
      return null;
    }
  }
}
