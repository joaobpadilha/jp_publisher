package com.umapped.mobile.api;

import scala.concurrent.java8.FuturesConvertersImpl;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by wei on 2017-08-16.
 */
public class BundleToAppMap {
  private static Map<String, String> mapping = new HashMap<>();

  static {
    mapping.put("com.umapped.v2.mobile", "IUM");
    mapping.put("com.umapped.mobile", "AUM");
    mapping.put("com.smartflyer.ios", "ISM");
    mapping.put("com.smartflyer.mobile.android", "ASM");
    mapping.put("com.umapped.protravel", "IPT");
    mapping.put("umapped.com.protravel", "APT");
    mapping.put("com.umapped.tzell", "ITZ");
    mapping.put("umapped.com.tzell", "ATZ");
    mapping.put("com.lozanotravel.mobile.ios", "ILO");
    mapping.put("com.lozanotravel.mobile.android", "ALO");
    mapping.put("com.umapped.skicom", "ISK");
    mapping.put("umapped.com.ski", "ASK");
    mapping.put("com.umapped.mytriproute", "IMT");
    mapping.put("umapped.com.ensemble", "AMT");
    mapping.put("com.indagare.mobile.ios", "IID");
    mapping.put("com.indagare.mobile.android", "AID");
    mapping.put("com.altour.mobile.ios", "IAL");
    mapping.put("com.altour.mobile.android", "AAL");
    mapping.put("com.teplis.mobile.ios", "ITP");
    mapping.put("com.teplis.mobile.android", "ATP");

  }

  public static String getApp(String bundleId) {
    return mapping.get(bundleId);
  }
}