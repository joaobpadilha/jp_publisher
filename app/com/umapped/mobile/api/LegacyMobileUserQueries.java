package com.umapped.mobile.api;

import com.google.inject.Singleton;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.umapped.mobile.api.model.RegistrationRequest;

import java.sql.*;

/**
 * Created by wei on 2017-08-11.
 */
@Singleton
public class LegacyMobileUserQueries {
  private static int STATUS_ACTIVE = 0;

  private static String FIND_USER_BY_EMAIL = "SELECT fname, lname, pwd, createtimestamp FROM users where status = ? and email=?";

  public RegistrationRequest findUser(String email, String password) {
    try (Connection connection = DBConnectionMgr.getConnection4Mobile()) {
      PreparedStatement stmt = connection.prepareStatement(FIND_USER_BY_EMAIL);
      stmt.setInt(1, STATUS_ACTIVE);
      stmt.setString(2, email);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        String hashedPwd = rs.getString(3);
        long createdTs = rs.getLong(4);
        String computedHashPwd = Utils.hashPwd(password, Long.toString(createdTs));
        if (hashedPwd.equals(computedHashPwd)) {
          RegistrationRequest userInfo = new RegistrationRequest();
          userInfo.setFirstName(rs.getString(1));
          userInfo.setLastName(rs.getString(2));

          userInfo.setPassword(password);
          userInfo.setEmail(email);
          return userInfo;
        } else {
          Log.err("Password mismatch:" + email);
        }
      }
      return null;
    } catch (SQLException ex) {
      Log.err("Fail to query users", ex);
      return null;
    }
  }
}
