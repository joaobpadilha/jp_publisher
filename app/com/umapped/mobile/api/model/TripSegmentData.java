package com.umapped.mobile.api.model;

import com.umapped.itinerary.analyze.model.ItineraryItem;
import com.umapped.itinerary.analyze.model.Location;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by wei on 2017-09-13.
 */
public class TripSegmentData implements Serializable {
  private Set<ItineraryItem> items = new HashSet<>();
  private Map<String, Location> locations = new HashMap<>();

  public void addItem(ItineraryItem item) {
    items.add(item);
    addLocation(item.getEnhancedStartLocation());
    addLocation(item.getEnhancedEndLocation());
  }

  public void addLocation(Location location) {
    if (location != null && location.getDescription() != null) {
      locations.put(StringUtils.trimToEmpty(location.getDescription()), location);
    }
  }

  public Set<ItineraryItem> getItems() {
    return items;
  }

  public void setItems(Set<ItineraryItem> items) {
    this.items = items;
  }

  public Map<String, Location> getLocations() {
    return locations;
  }
}
