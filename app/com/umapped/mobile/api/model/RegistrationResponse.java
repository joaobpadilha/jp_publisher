package com.umapped.mobile.api.model;

import java.io.Serializable;

/**
 * Created by wei on 2017-07-14.
 */
public class RegistrationResponse extends AuthenticatedUser implements Serializable {
  private RegistrationStatus status;

  public RegistrationResponse() {
    this(null);
  }

  public RegistrationResponse(RegistrationStatus status) {
    this.status = status;
  }

  public RegistrationStatus getStatus() {
    return status;
  }

  public void setStatus(RegistrationStatus status) {
    this.status = status;
  }
}
