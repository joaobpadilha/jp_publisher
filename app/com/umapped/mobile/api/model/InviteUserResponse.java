package com.umapped.mobile.api.model;

/**
 * Created by wei on 2017-07-26.
 */
public class InviteUserResponse {
  private InviteUserStatus status;

  public InviteUserResponse() {
    status = InviteUserStatus.SUCCESSFUL;
  }

  public InviteUserResponse(InviteUserStatus status) {
    this.status = status;
  }

  public InviteUserStatus getStatus() {
    return status;
  }

  public void setStatus(InviteUserStatus status) {
    this.status = status;
  }
}
