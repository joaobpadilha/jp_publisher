package com.umapped.mobile.api.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by wei on 2017-10-03.
 */
public class S3AuthorizationResponse implements Serializable{
  private List<S3Authorization> authList;

  public S3AuthorizationResponse() {

  }

  public List<S3Authorization> getAuthList() {
    return authList;
  }

  public void setAuthList(List<S3Authorization> authList) {
    this.authList = authList;
  }

  public static class S3Authorization {
    public String name;
    public String putUrl;
    public String getUrl;
  }
}
