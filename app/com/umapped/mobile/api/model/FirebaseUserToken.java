package com.umapped.mobile.api.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wei on 2017-11-24.
 */
public class FirebaseUserToken implements Serializable {
  private String token;
  private List<String> accountTripLinkIds;

  public FirebaseUserToken(String token) {
    this.token = token;
    this.accountTripLinkIds = null;
  }

  public FirebaseUserToken(String token, List<String> accountTripLinkIds) {
    this.token = token;
    this.accountTripLinkIds = accountTripLinkIds;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public List<String> getAccountTripLinkIds() {
    return accountTripLinkIds;
  }

  public void setAccountTripLinkIds(List<String> accountTripLinkIds) {
    this.accountTripLinkIds = accountTripLinkIds;
  }
}
