package com.umapped.mobile.api.model;

/**
 * Created by wei on 2017-08-16.
 */
public enum MobileAPIResponseCode {
  SUCCESS,
  MISSING_SESSION_ID,
  INVALID_SESSION_ID,
  MISSING_API_VERSION,
  API_VERSION_NOT_SUPPORTED,
  INVALID_REQUEST,
  REQUEST_MISSING_DATA,
  UNEXPECTED_ERROR,
}
