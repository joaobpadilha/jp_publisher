package com.umapped.mobile.api.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wei on 2017-10-03.
 */
public class S3AuthorizationRequest implements Serializable {
  private List<String> fileNames;


  public S3AuthorizationRequest() {
  }

  public List<String> getFileNames() {
    return fileNames;
  }

  public void setFileNames(List<String> fileNames) {
    this.fileNames = fileNames;
  }
}
