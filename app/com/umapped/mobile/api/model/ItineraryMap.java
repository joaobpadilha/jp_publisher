package com.umapped.mobile.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mapped.publisher.js.TripMap;
import com.umapped.api.schema.types.ReturnCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wei on 2017-06-30.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ItineraryMap implements Serializable {
  public List<TripMap.Location> locations;

  public static class Location
      implements Serializable {
    public Float  latitude;
    public Float  longitude;
    public String name;
  }

  public ItineraryMap() {
    locations = new ArrayList<>();
  }

  public void addLocation(String name, Float latitude, Float longitude) {
    TripMap.Location l = new TripMap.Location();
    l.latitude = latitude;
    l.longitude = longitude;
    l.name = name;
    locations.add(l);
  }
}
