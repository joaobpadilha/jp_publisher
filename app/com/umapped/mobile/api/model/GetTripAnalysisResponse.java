package com.umapped.mobile.api.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wei on 2017-09-13.
 */
public class GetTripAnalysisResponse
    implements Serializable {
  private List<TripSegmentData> tripSegments;

  public GetTripAnalysisResponse() {

  }

  public GetTripAnalysisResponse(List<TripSegmentData> tripSegments) {
    this.tripSegments = tripSegments;
  }

  public List<TripSegmentData> getTripSegments() {
    return tripSegments;
  }

  public void setTripSegments(List<TripSegmentData> tripSegments) {
    this.tripSegments = tripSegments;
  }
}
