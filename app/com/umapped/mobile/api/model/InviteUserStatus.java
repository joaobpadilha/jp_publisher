package com.umapped.mobile.api.model;

/**
 * Created by wei on 2017-07-26.
 */
public enum InviteUserStatus {
  SUCCESSFUL,
  EMAIL_REQUIRED,
  FIRST_NAME_REQUIRED,
  LAST_NAME_REQUIRED,
  EMAIL_INVALID_FORMAT,
  NOT_ALLOWED,
  UNEXPECTED_ERROR
}
