package com.umapped.mobile.api.model;

import java.io.Serializable;

/**
 * Created by wei on 2017-07-14.
 */
public class RegistrationRequest extends AuthenticationRequest {
  private String firstName;
  private String lastName;

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
}
