package com.umapped.mobile.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mapped.publisher.common.SecurityMgr;

import java.io.Serializable;

/**
 * Created by wei on 2017-06-30.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ItineraryAuthorization implements Serializable {
  private SecurityMgr.AccessLevel accessLevel;

  public SecurityMgr.AccessLevel getAccessLevel() {
    return accessLevel;
  }

  public void setAccessLevel(SecurityMgr.AccessLevel accessLevel) {
    this.accessLevel = accessLevel;
  }
}
