package com.umapped.mobile.api.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wei on 2017-07-17.
 */
public class GetItinerariesResponse
    implements Serializable {
  private Long syncTimestamp;
  private List<Itinerary> itineraries;

  public Long getSyncTimestamp() {
    return syncTimestamp;
  }

  public void setSyncTimestamp(Long syncTimestamp) {
    this.syncTimestamp = syncTimestamp;
  }

  public List<Itinerary> getItineraries() {
    return itineraries;
  }

  public void setItineraries(List<Itinerary> itineraries) {
    this.itineraries = itineraries;
  }
}
