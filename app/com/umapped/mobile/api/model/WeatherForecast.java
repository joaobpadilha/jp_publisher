package com.umapped.mobile.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wei on 2017-09-08.
 */
public class WeatherForecast
    implements Serializable {

  private static int DEFAULT_NUM_DAYS = 4;

  @JsonIgnore
  private LocalDate reportingDate;

  public static class Temperature implements Serializable {
    public String c;
    public String f;
  }

  public static class DayForecast implements Serializable {
    // YYYY-MM-DD format
    public String date;
    public Temperature high;
    public Temperature low;
    public String condition;
    public String conditionIcon;
    public String pop;
  }

  private List<DayForecast> daysForecast = new ArrayList<>(DEFAULT_NUM_DAYS);

  public LocalDate getReportingDate() {
    return reportingDate;
  }

  public void setReportingDate(LocalDate reportingDate) {
    this.reportingDate = reportingDate;
  }

  public void addDayForcast(DayForecast day) {
    daysForecast.add(day);
  }

  public List<DayForecast> getDaysForecast() {
    return daysForecast;
  }

  public void setDaysForecast(List<DayForecast> daysForecast) {
    this.daysForecast = daysForecast;
  }
}
