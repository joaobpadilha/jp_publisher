package com.umapped.common;

/**
 * Created by wei on 2017-05-11.
 */
public interface BkApiSrcIdConstants {
  String WETU = "WETU";
  String TRAVELBOUND = "TravelBound";
  String S3_IMG_SEARCH = "S3_IMG_Search";
  String TRAMADA = "TRAMADA";
}
