package com.umapped.external.afar;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by george on 2017-11-01.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "overview",
    "hero_image_url",
    "photo_credit",
    "lists",
    "city_id",
    "region_id",
    "country_id",
    "tag_id",
    "last_update"
})
public class Destination {
    //private String name;
    /*@JsonProperty("image_url")
    private String imageUrl;

    private String attribution;
    @JsonProperty("attribution_url")
    private String attributionUrl;

    private List<Essential> essentials;
    private String url;

    //Location
    private Location location;

    private List<String> ids;

    /*public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }*/

    /*public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAttribution() {
        return attribution;
    }

    public void setAttribution(String attribution) {
        this.attribution = attribution;
    }

    public String getAttributionUrl() {
        return attributionUrl;
    }

    public void setAttributionUrl(String attributionUrl) {
        this.attributionUrl = attributionUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Essential> getEssentials() {
        return essentials;
    }

    public void setEssentials(List<Essential> essentials) {
        this.essentials = essentials;
    }

    public List<String> getIds() {
        List<String> trimmedIds = ids.stream().map(String::trim).collect(Collectors.toList());
        return trimmedIds;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }*/

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("overview")
    private String overview;
    @JsonProperty("hero_image_url")
    private String heroImageUrl;
    @JsonProperty("photo_credit")
    private String photoCredit;
    @JsonProperty("lists")
    private List<AfarList> lists = null;
    @JsonProperty("city_id")
    private Integer cityId;
    @JsonProperty("region_id")
    private Integer regionId;
    @JsonProperty("country_id")
    private Integer countryId;
    @JsonProperty("tag_id")
    private Integer tagId;
    @JsonProperty("last_update")
    private String lastUpdate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("overview")
    public String getOverview() {
        return overview;
    }

    @JsonProperty("overview")
    public void setOverview(String overview) {
        this.overview = overview;
    }

    @JsonProperty("hero_image_url")
    public String getHeroImageUrl() {
        return heroImageUrl;
    }

    @JsonProperty("hero_image_url")
    public void setHeroImageUrl(String heroImageUrl) {
        this.heroImageUrl = heroImageUrl;
    }

    @JsonProperty("photo_credit")
    public String getPhotoCredit() {
        return photoCredit;
    }

    @JsonProperty("photo_credit")
    public void setPhotoCredit(String photoCredit) {
        this.photoCredit = photoCredit;
    }

    @JsonProperty("lists")
    public List<AfarList> getLists() {
        return lists;
    }

    @JsonProperty("lists")
    public void setLists(List<AfarList> lists) {
        this.lists = lists;
    }

    @JsonProperty("city_id")
    public Integer getCityId() {
        return cityId;
    }

    @JsonProperty("city_id")
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    @JsonProperty("region_id")
    public Integer getRegionId() {
        return regionId;
    }

    @JsonProperty("region_id")
    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    @JsonProperty("country_id")
    public Integer getCountryId() {
        return countryId;
    }

    @JsonProperty("country_id")
    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    @JsonProperty("tag_id")
    public Integer getTagId() {
        return tagId;
    }

    @JsonProperty("tag_id")
    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

    @JsonProperty("last_update")
    public String getLastUpdate() {
        return lastUpdate;
    }

    @JsonProperty("last_update")
    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
