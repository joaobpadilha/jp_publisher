
package com.umapped.external.wetu.content;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "mode",
    "start_leg_id",
    "end_leg_id",
    "start_content_entity_id",
    "end_content_entity_id",
    "sequence",
    "agency",
    "vehicle",
    "reference_codes",
    "notes",
    "start",
    "start_time",
    "end",
    "end_time",
    "points",
    "via_points",
    "directions",
    "contact_numbers",
    "start_terminal",
    "end_terminal",
    "ticket_class",
    "type",
    "variant",
    "duration",
    "distance",
    "label",
    "day_sequence"
})
public class Route {

    @JsonProperty("mode")
    private String mode;
    @JsonProperty("start_leg_id")
    private int startLegId;
    @JsonProperty("end_leg_id")
    private int endLegId;
    @JsonProperty("start_content_entity_id")
    private int startContentEntityId;
    @JsonProperty("end_content_entity_id")
    private int endContentEntityId;
    @JsonProperty("sequence")
    private int sequence;
    @JsonProperty("agency")
    private String agency;
    @JsonProperty("vehicle")
    private String vehicle;
    @JsonProperty("reference_codes")
    private String referenceCodes;
    @JsonProperty("notes")
    private String notes;
    @JsonProperty("start")
    private String start;
    @JsonProperty("start_time")
    private String startTime;
    @JsonProperty("end")
    private String end;
    @JsonProperty("end_time")
    private String endTime;
    @JsonProperty("points")
    private String points;
    @JsonProperty("via_points")
    private String viaPoints;
    @JsonProperty("directions")
    private String directions;
    @JsonProperty("contact_numbers")
    private String contactNumbers;
    @JsonProperty("start_terminal")
    private String startTerminal;
    @JsonProperty("end_terminal")
    private String endTerminal;
    @JsonProperty("ticket_class")
    private String ticketClass;
    @JsonProperty("type")
    private String type;
    @JsonProperty("variant")
    private String variant;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("distance")
    private int distance;
    @JsonProperty("label")
    private String label;
    @JsonProperty("day_sequence")
    private int daySequence;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("mode")
    public String getMode() {
        return mode;
    }

    @JsonProperty("mode")
    public void setMode(String mode) {
        this.mode = mode;
    }

    @JsonProperty("start_leg_id")
    public int getStartLegId() {
        return startLegId;
    }

    @JsonProperty("start_leg_id")
    public void setStartLegId(int startLegId) {
        this.startLegId = startLegId;
    }

    @JsonProperty("end_leg_id")
    public int getEndLegId() {
        return endLegId;
    }

    @JsonProperty("end_leg_id")
    public void setEndLegId(int endLegId) {
        this.endLegId = endLegId;
    }

    @JsonProperty("start_content_entity_id")
    public int getStartContentEntityId() {
        return startContentEntityId;
    }

    @JsonProperty("start_content_entity_id")
    public void setStartContentEntityId(int startContentEntityId) {
        this.startContentEntityId = startContentEntityId;
    }

    @JsonProperty("end_content_entity_id")
    public int getEndContentEntityId() {
        return endContentEntityId;
    }

    @JsonProperty("end_content_entity_id")
    public void setEndContentEntityId(int endContentEntityId) {
        this.endContentEntityId = endContentEntityId;
    }

    @JsonProperty("sequence")
    public int getSequence() {
        return sequence;
    }

    @JsonProperty("sequence")
    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    @JsonProperty("agency")
    public String getAgency() {
        return agency;
    }

    @JsonProperty("agency")
    public void setAgency(String agency) {
        this.agency = agency;
    }

    @JsonProperty("vehicle")
    public String getVehicle() {
        return vehicle;
    }

    @JsonProperty("vehicle")
    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    @JsonProperty("reference_codes")
    public String getReferenceCodes() {
        return referenceCodes;
    }

    @JsonProperty("reference_codes")
    public void setReferenceCodes(String referenceCodes) {
        this.referenceCodes = referenceCodes;
    }

    @JsonProperty("notes")
    public String getNotes() {
        return notes;
    }

    @JsonProperty("notes")
    public void setNotes(String notes) {
        this.notes = notes;
    }

    @JsonProperty("start")
    public String getStart() {
        return start;
    }

    @JsonProperty("start")
    public void setStart(String start) {
        this.start = start;
    }

    @JsonProperty("start_time")
    public String getStartTime() {
        return startTime;
    }

    @JsonProperty("start_time")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @JsonProperty("end")
    public String getEnd() {
        return end;
    }

    @JsonProperty("end")
    public void setEnd(String end) {
        this.end = end;
    }

    @JsonProperty("end_time")
    public String getEndTime() {
        return endTime;
    }

    @JsonProperty("end_time")
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @JsonProperty("points")
    public String getPoints() {
        return points;
    }

    @JsonProperty("points")
    public void setPoints(String points) {
        this.points = points;
    }

    @JsonProperty("via_points")
    public String getViaPoints() {
        return viaPoints;
    }

    @JsonProperty("via_points")
    public void setViaPoints(String viaPoints) {
        this.viaPoints = viaPoints;
    }

    @JsonProperty("directions")
    public String getDirections() {
        return directions;
    }

    @JsonProperty("directions")
    public void setDirections(String directions) {
        this.directions = directions;
    }

    @JsonProperty("contact_numbers")
    public String getContactNumbers() {
        return contactNumbers;
    }

    @JsonProperty("contact_numbers")
    public void setContactNumbers(String contactNumbers) {
        this.contactNumbers = contactNumbers;
    }

    @JsonProperty("start_terminal")
    public String getStartTerminal() {
        return startTerminal;
    }

    @JsonProperty("start_terminal")
    public void setStartTerminal(String startTerminal) {
        this.startTerminal = startTerminal;
    }

    @JsonProperty("end_terminal")
    public String getEndTerminal() {
        return endTerminal;
    }

    @JsonProperty("end_terminal")
    public void setEndTerminal(String endTerminal) {
        this.endTerminal = endTerminal;
    }

    @JsonProperty("ticket_class")
    public String getTicketClass() {
        return ticketClass;
    }

    @JsonProperty("ticket_class")
    public void setTicketClass(String ticketClass) {
        this.ticketClass = ticketClass;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("variant")
    public String getVariant() {
        return variant;
    }

    @JsonProperty("variant")
    public void setVariant(String variant) {
        this.variant = variant;
    }

    @JsonProperty("duration")
    public String getDuration() {
        return duration;
    }

    @JsonProperty("duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    @JsonProperty("distance")
    public int getDistance() {
        return distance;
    }

    @JsonProperty("distance")
    public void setDistance(int distance) {
        this.distance = distance;
    }

    @JsonProperty("label")
    public String getLabel() {
        return label;
    }

    @JsonProperty("label")
    public void setLabel(String label) {
        this.label = label;
    }

    @JsonProperty("day_sequence")
    public int getDaySequence() {
        return daySequence;
    }

    @JsonProperty("day_sequence")
    public void setDaySequence(int daySequence) {
        this.daySequence = daySequence;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("mode", mode).append("startLegId", startLegId).append("endLegId", endLegId).append("startContentEntityId", startContentEntityId).append("endContentEntityId", endContentEntityId).append("sequence", sequence).append("agency", agency).append("vehicle", vehicle).append("referenceCodes", referenceCodes).append("notes", notes).append("start", start).append("startTime", startTime).append("end", end).append("endTime", endTime).append("points", points).append("viaPoints", viaPoints).append("directions", directions).append("contactNumbers", contactNumbers).append("startTerminal", startTerminal).append("endTerminal", endTerminal).append("ticketClass", ticketClass).append("type", type).append("variant", variant).append("duration", duration).append("distance", distance).append("label", label).append("daySequence", daySequence).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(endLegId).append(startLegId).append(vehicle).append(type).append(endTime).append(variant).append(mode).append(directions).append(startTime).append(daySequence).append(distance).append(endContentEntityId).append(contactNumbers).append(viaPoints).append(points).append(end).append(ticketClass).append(agency).append(label).append(referenceCodes).append(startContentEntityId).append(duration).append(start).append(additionalProperties).append(startTerminal).append(sequence).append(notes).append(endTerminal).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Route) == false) {
            return false;
        }
        Route rhs = ((Route) other);
        return new EqualsBuilder().append(endLegId, rhs.endLegId).append(startLegId, rhs.startLegId).append(vehicle, rhs.vehicle).append(type, rhs.type).append(endTime, rhs.endTime).append(variant, rhs.variant).append(mode, rhs.mode).append(directions, rhs.directions).append(startTime, rhs.startTime).append(daySequence, rhs.daySequence).append(distance, rhs.distance).append(endContentEntityId, rhs.endContentEntityId).append(contactNumbers, rhs.contactNumbers).append(viaPoints, rhs.viaPoints).append(points, rhs.points).append(end, rhs.end).append(ticketClass, rhs.ticketClass).append(agency, rhs.agency).append(label, rhs.label).append(referenceCodes, rhs.referenceCodes).append(startContentEntityId, rhs.startContentEntityId).append(duration, rhs.duration).append(start, rhs.start).append(additionalProperties, rhs.additionalProperties).append(startTerminal, rhs.startTerminal).append(sequence, rhs.sequence).append(notes, rhs.notes).append(endTerminal, rhs.endTerminal).isEquals();
    }

}
