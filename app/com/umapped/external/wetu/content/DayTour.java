
package com.umapped.external.wetu.content;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "operator_id",
    "operator_user_id",
    "operator_identity_id",
    "operator_theme_id",
    "type",
    "identifier",
    "identifier_key",
    "days",
    "name",
    "language",
    "summary",
    "description",
    "categories",
    "legs",
    "hide_route_information",
    "bound_lat1",
    "bound_lat2",
    "bound_lng1",
    "bound_lng2",
    "price",
    "is_disabled",
    "travellers_adult",
    "travellers_children",
    "rooms_single",
    "rooms_double",
    "rooms_twin",
    "rooms_triple",
    "rooms_family",
    "start_travel_days",
    "end_travel_days",
    "booking_status",
    "source_operator_id",
    "contacts",
    "notification_frequency",
    "images",
    "departure_locations",
    "departure_times",
    "return_locations",
    "return_times",
    "duration",
    "additional_info",
    "spoken_languages",
    "group_size",
    "published",
    "route_handling_mode"
})
public class DayTour {

    @JsonProperty("operator_id")
    private int operatorId;
    @JsonProperty("operator_user_id")
    private int operatorUserId;
    @JsonProperty("operator_identity_id")
    private int operatorIdentityId;
    @JsonProperty("operator_theme_id")
    private int operatorThemeId;
    @JsonProperty("type")
    private String type;
    @JsonProperty("identifier")
    private String identifier;
    @JsonProperty("identifier_key")
    private String identifierKey;
    @JsonProperty("days")
    private int days;
    @JsonProperty("name")
    private String name;
    @JsonProperty("language")
    private String language;
    @JsonProperty("summary")
    private String summary;
    @JsonProperty("description")
    private String description;
    @JsonProperty("categories")
    private List<String> categories = null;
    @JsonProperty("legs")
    private List<Leg> legs = null;
    @JsonProperty("hide_route_information")
    private boolean hideRouteInformation;
    @JsonProperty("bound_lat1")
    private int boundLat1;
    @JsonProperty("bound_lat2")
    private int boundLat2;
    @JsonProperty("bound_lng1")
    private int boundLng1;
    @JsonProperty("bound_lng2")
    private int boundLng2;
    @JsonProperty("price")
    private String price;
    @JsonProperty("is_disabled")
    private boolean isDisabled;
    @JsonProperty("travellers_adult")
    private int travellersAdult;
    @JsonProperty("travellers_children")
    private int travellersChildren;
    @JsonProperty("rooms_single")
    private int roomsSingle;
    @JsonProperty("rooms_double")
    private int roomsDouble;
    @JsonProperty("rooms_twin")
    private int roomsTwin;
    @JsonProperty("rooms_triple")
    private int roomsTriple;
    @JsonProperty("rooms_family")
    private int roomsFamily;
    @JsonProperty("start_travel_days")
    private int startTravelDays;
    @JsonProperty("end_travel_days")
    private int endTravelDays;
    @JsonProperty("booking_status")
    private String bookingStatus;
    @JsonProperty("source_operator_id")
    private int sourceOperatorId;
    @JsonProperty("contacts")
    private List<Contact> contacts = null;
    @JsonProperty("notification_frequency")
    private String notificationFrequency;
    @JsonProperty("images")
    private List<Image> images = null;
    @JsonProperty("departure_locations")
    private String departureLocations;
    @JsonProperty("departure_times")
    private String departureTimes;
    @JsonProperty("return_locations")
    private String returnLocations;
    @JsonProperty("return_times")
    private String returnTimes;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("additional_info")
    private String additionalInfo;
    @JsonProperty("spoken_languages")
    private String spokenLanguages;
    @JsonProperty("group_size")
    private String groupSize;
    @JsonProperty("published")
    private boolean published;
    @JsonProperty("route_handling_mode")
    private String routeHandlingMode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("operator_id")
    public int getOperatorId() {
        return operatorId;
    }

    @JsonProperty("operator_id")
    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    @JsonProperty("operator_user_id")
    public int getOperatorUserId() {
        return operatorUserId;
    }

    @JsonProperty("operator_user_id")
    public void setOperatorUserId(int operatorUserId) {
        this.operatorUserId = operatorUserId;
    }

    @JsonProperty("operator_identity_id")
    public int getOperatorIdentityId() {
        return operatorIdentityId;
    }

    @JsonProperty("operator_identity_id")
    public void setOperatorIdentityId(int operatorIdentityId) {
        this.operatorIdentityId = operatorIdentityId;
    }

    @JsonProperty("operator_theme_id")
    public int getOperatorThemeId() {
        return operatorThemeId;
    }

    @JsonProperty("operator_theme_id")
    public void setOperatorThemeId(int operatorThemeId) {
        this.operatorThemeId = operatorThemeId;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("identifier")
    public String getIdentifier() {
        return identifier;
    }

    @JsonProperty("identifier")
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @JsonProperty("identifier_key")
    public String getIdentifierKey() {
        return identifierKey;
    }

    @JsonProperty("identifier_key")
    public void setIdentifierKey(String identifierKey) {
        this.identifierKey = identifierKey;
    }

    @JsonProperty("days")
    public int getDays() {
        return days;
    }

    @JsonProperty("days")
    public void setDays(int days) {
        this.days = days;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    @JsonProperty("summary")
    public String getSummary() {
        return summary;
    }

    @JsonProperty("summary")
    public void setSummary(String summary) {
        this.summary = summary;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("categories")
    public List<String> getCategories() {
        return categories;
    }

    @JsonProperty("categories")
    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    @JsonProperty("legs")
    public List<Leg> getLegs() {
        return legs;
    }

    @JsonProperty("legs")
    public void setLegs(List<Leg> legs) {
        this.legs = legs;
    }

    @JsonProperty("hide_route_information")
    public boolean isHideRouteInformation() {
        return hideRouteInformation;
    }

    @JsonProperty("hide_route_information")
    public void setHideRouteInformation(boolean hideRouteInformation) {
        this.hideRouteInformation = hideRouteInformation;
    }

    @JsonProperty("bound_lat1")
    public int getBoundLat1() {
        return boundLat1;
    }

    @JsonProperty("bound_lat1")
    public void setBoundLat1(int boundLat1) {
        this.boundLat1 = boundLat1;
    }

    @JsonProperty("bound_lat2")
    public int getBoundLat2() {
        return boundLat2;
    }

    @JsonProperty("bound_lat2")
    public void setBoundLat2(int boundLat2) {
        this.boundLat2 = boundLat2;
    }

    @JsonProperty("bound_lng1")
    public int getBoundLng1() {
        return boundLng1;
    }

    @JsonProperty("bound_lng1")
    public void setBoundLng1(int boundLng1) {
        this.boundLng1 = boundLng1;
    }

    @JsonProperty("bound_lng2")
    public int getBoundLng2() {
        return boundLng2;
    }

    @JsonProperty("bound_lng2")
    public void setBoundLng2(int boundLng2) {
        this.boundLng2 = boundLng2;
    }

    @JsonProperty("price")
    public String getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(String price) {
        this.price = price;
    }

    @JsonProperty("is_disabled")
    public boolean isIsDisabled() {
        return isDisabled;
    }

    @JsonProperty("is_disabled")
    public void setIsDisabled(boolean isDisabled) {
        this.isDisabled = isDisabled;
    }

    @JsonProperty("travellers_adult")
    public int getTravellersAdult() {
        return travellersAdult;
    }

    @JsonProperty("travellers_adult")
    public void setTravellersAdult(int travellersAdult) {
        this.travellersAdult = travellersAdult;
    }

    @JsonProperty("travellers_children")
    public int getTravellersChildren() {
        return travellersChildren;
    }

    @JsonProperty("travellers_children")
    public void setTravellersChildren(int travellersChildren) {
        this.travellersChildren = travellersChildren;
    }

    @JsonProperty("rooms_single")
    public int getRoomsSingle() {
        return roomsSingle;
    }

    @JsonProperty("rooms_single")
    public void setRoomsSingle(int roomsSingle) {
        this.roomsSingle = roomsSingle;
    }

    @JsonProperty("rooms_double")
    public int getRoomsDouble() {
        return roomsDouble;
    }

    @JsonProperty("rooms_double")
    public void setRoomsDouble(int roomsDouble) {
        this.roomsDouble = roomsDouble;
    }

    @JsonProperty("rooms_twin")
    public int getRoomsTwin() {
        return roomsTwin;
    }

    @JsonProperty("rooms_twin")
    public void setRoomsTwin(int roomsTwin) {
        this.roomsTwin = roomsTwin;
    }

    @JsonProperty("rooms_triple")
    public int getRoomsTriple() {
        return roomsTriple;
    }

    @JsonProperty("rooms_triple")
    public void setRoomsTriple(int roomsTriple) {
        this.roomsTriple = roomsTriple;
    }

    @JsonProperty("rooms_family")
    public int getRoomsFamily() {
        return roomsFamily;
    }

    @JsonProperty("rooms_family")
    public void setRoomsFamily(int roomsFamily) {
        this.roomsFamily = roomsFamily;
    }

    @JsonProperty("start_travel_days")
    public int getStartTravelDays() {
        return startTravelDays;
    }

    @JsonProperty("start_travel_days")
    public void setStartTravelDays(int startTravelDays) {
        this.startTravelDays = startTravelDays;
    }

    @JsonProperty("end_travel_days")
    public int getEndTravelDays() {
        return endTravelDays;
    }

    @JsonProperty("end_travel_days")
    public void setEndTravelDays(int endTravelDays) {
        this.endTravelDays = endTravelDays;
    }

    @JsonProperty("booking_status")
    public String getBookingStatus() {
        return bookingStatus;
    }

    @JsonProperty("booking_status")
    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    @JsonProperty("source_operator_id")
    public int getSourceOperatorId() {
        return sourceOperatorId;
    }

    @JsonProperty("source_operator_id")
    public void setSourceOperatorId(int sourceOperatorId) {
        this.sourceOperatorId = sourceOperatorId;
    }

    @JsonProperty("contacts")
    public List<Contact> getContacts() {
        return contacts;
    }

    @JsonProperty("contacts")
    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    @JsonProperty("notification_frequency")
    public String getNotificationFrequency() {
        return notificationFrequency;
    }

    @JsonProperty("notification_frequency")
    public void setNotificationFrequency(String notificationFrequency) {
        this.notificationFrequency = notificationFrequency;
    }

    @JsonProperty("images")
    public List<Image> getImages() {
        return images;
    }

    @JsonProperty("images")
    public void setImages(List<Image> images) {
        this.images = images;
    }

    @JsonProperty("departure_locations")
    public String getDepartureLocations() {
        return departureLocations;
    }

    @JsonProperty("departure_locations")
    public void setDepartureLocations(String departureLocations) {
        this.departureLocations = departureLocations;
    }

    @JsonProperty("departure_times")
    public String getDepartureTimes() {
        return departureTimes;
    }

    @JsonProperty("departure_times")
    public void setDepartureTimes(String departureTimes) {
        this.departureTimes = departureTimes;
    }

    @JsonProperty("return_locations")
    public String getReturnLocations() {
        return returnLocations;
    }

    @JsonProperty("return_locations")
    public void setReturnLocations(String returnLocations) {
        this.returnLocations = returnLocations;
    }

    @JsonProperty("return_times")
    public String getReturnTimes() {
        return returnTimes;
    }

    @JsonProperty("return_times")
    public void setReturnTimes(String returnTimes) {
        this.returnTimes = returnTimes;
    }

    @JsonProperty("duration")
    public String getDuration() {
        return duration;
    }

    @JsonProperty("duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    @JsonProperty("additional_info")
    public String getAdditionalInfo() {
        return additionalInfo;
    }

    @JsonProperty("additional_info")
    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    @JsonProperty("spoken_languages")
    public String getSpokenLanguages() {
        return spokenLanguages;
    }

    @JsonProperty("spoken_languages")
    public void setSpokenLanguages(String spokenLanguages) {
        this.spokenLanguages = spokenLanguages;
    }

    @JsonProperty("group_size")
    public String getGroupSize() {
        return groupSize;
    }

    @JsonProperty("group_size")
    public void setGroupSize(String groupSize) {
        this.groupSize = groupSize;
    }

    @JsonProperty("published")
    public boolean isPublished() {
        return published;
    }

    @JsonProperty("published")
    public void setPublished(boolean published) {
        this.published = published;
    }

    @JsonProperty("route_handling_mode")
    public String getRouteHandlingMode() {
        return routeHandlingMode;
    }

    @JsonProperty("route_handling_mode")
    public void setRouteHandlingMode(String routeHandlingMode) {
        this.routeHandlingMode = routeHandlingMode;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("operatorId", operatorId).append("operatorUserId", operatorUserId).append("operatorIdentityId", operatorIdentityId).append("operatorThemeId", operatorThemeId).append("type", type).append("identifier", identifier).append("identifierKey", identifierKey).append("days", days).append("name", name).append("language", language).append("summary", summary).append("description", description).append("categories", categories).append("legs", legs).append("hideRouteInformation", hideRouteInformation).append("boundLat1", boundLat1).append("boundLat2", boundLat2).append("boundLng1", boundLng1).append("boundLng2", boundLng2).append("price", price).append("isDisabled", isDisabled).append("travellersAdult", travellersAdult).append("travellersChildren", travellersChildren).append("roomsSingle", roomsSingle).append("roomsDouble", roomsDouble).append("roomsTwin", roomsTwin).append("roomsTriple", roomsTriple).append("roomsFamily", roomsFamily).append("startTravelDays", startTravelDays).append("endTravelDays", endTravelDays).append("bookingStatus", bookingStatus).append("sourceOperatorId", sourceOperatorId).append("contacts", contacts).append("notificationFrequency", notificationFrequency).append("images", images).append("departureLocations", departureLocations).append("departureTimes", departureTimes).append("returnLocations", returnLocations).append("returnTimes", returnTimes).append("duration", duration).append("additionalInfo", additionalInfo).append("spokenLanguages", spokenLanguages).append("groupSize", groupSize).append("published", published).append("routeHandlingMode", routeHandlingMode).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(summary).append(spokenLanguages).append(operatorId).append(departureLocations).append(travellersAdult).append(type).append(operatorUserId).append(roomsFamily).append(identifierKey).append(contacts).append(operatorIdentityId).append(endTravelDays).append(description).append(name).append(boundLng2).append(returnLocations).append(boundLng1).append(published).append(hideRouteInformation).append(routeHandlingMode).append(roomsTriple).append(roomsTwin).append(roomsDouble).append(travellersChildren).append(roomsSingle).append(sourceOperatorId).append(startTravelDays).append(notificationFrequency).append(additionalInfo).append(isDisabled).append(returnTimes).append(duration).append(price).append(days).append(additionalProperties).append(groupSize).append(boundLat2).append(images).append(boundLat1).append(legs).append(categories).append(language).append(identifier).append(departureTimes).append(bookingStatus).append(operatorThemeId).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof DayTour) == false) {
            return false;
        }
        DayTour rhs = ((DayTour) other);
        return new EqualsBuilder().append(summary, rhs.summary).append(spokenLanguages, rhs.spokenLanguages).append(operatorId, rhs.operatorId).append(departureLocations, rhs.departureLocations).append(travellersAdult, rhs.travellersAdult).append(type, rhs.type).append(operatorUserId, rhs.operatorUserId).append(roomsFamily, rhs.roomsFamily).append(identifierKey, rhs.identifierKey).append(contacts, rhs.contacts).append(operatorIdentityId, rhs.operatorIdentityId).append(endTravelDays, rhs.endTravelDays).append(description, rhs.description).append(name, rhs.name).append(boundLng2, rhs.boundLng2).append(returnLocations, rhs.returnLocations).append(boundLng1, rhs.boundLng1).append(published, rhs.published).append(hideRouteInformation, rhs.hideRouteInformation).append(routeHandlingMode, rhs.routeHandlingMode).append(roomsTriple, rhs.roomsTriple).append(roomsTwin, rhs.roomsTwin).append(roomsDouble, rhs.roomsDouble).append(travellersChildren, rhs.travellersChildren).append(roomsSingle, rhs.roomsSingle).append(sourceOperatorId, rhs.sourceOperatorId).append(startTravelDays, rhs.startTravelDays).append(notificationFrequency, rhs.notificationFrequency).append(additionalInfo, rhs.additionalInfo).append(isDisabled, rhs.isDisabled).append(returnTimes, rhs.returnTimes).append(duration, rhs.duration).append(price, rhs.price).append(days, rhs.days).append(additionalProperties, rhs.additionalProperties).append(groupSize, rhs.groupSize).append(boundLat2, rhs.boundLat2).append(images, rhs.images).append(boundLat1, rhs.boundLat1).append(legs, rhs.legs).append(categories, rhs.categories).append(language, rhs.language).append(identifier, rhs.identifier).append(departureTimes, rhs.departureTimes).append(bookingStatus, rhs.bookingStatus).append(operatorThemeId, rhs.operatorThemeId).isEquals();
    }

}
