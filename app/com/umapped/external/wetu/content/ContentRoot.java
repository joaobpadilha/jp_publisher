
package com.umapped.external.wetu.content;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "day_tours",
    "pins"
})
public class ContentRoot {

    @JsonProperty("day_tours")
    private List<DayTour> dayTours = null;
    @JsonProperty("pins")
    private List<Pin> pins = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("day_tours")
    public List<DayTour> getDayTours() {
        return dayTours;
    }

    @JsonProperty("day_tours")
    public void setDayTours(List<DayTour> dayTours) {
        this.dayTours = dayTours;
    }

    @JsonProperty("pins")
    public List<Pin> getPins() {
        return pins;
    }

    @JsonProperty("pins")
    public void setPins(List<Pin> pins) {
        this.pins = pins;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("dayTours", dayTours).append("pins", pins).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(pins).append(additionalProperties).append(dayTours).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ContentRoot) == false) {
            return false;
        }
        ContentRoot rhs = ((ContentRoot) other);
        return new EqualsBuilder().append(pins, rhs.pins).append(additionalProperties, rhs.additionalProperties).append(dayTours, rhs.dayTours).isEquals();
    }

}
