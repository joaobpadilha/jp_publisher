
package com.umapped.external.wetu.content;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "start",
    "end",
    "type",
    "currency",
    "rate",
    "conditions",
    "included"
})
public class RoomRate {

    @JsonProperty("start")
    private String start;
    @JsonProperty("end")
    private String end;
    @JsonProperty("type")
    private String type;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("rate")
    private int rate;
    @JsonProperty("conditions")
    private String conditions;
    @JsonProperty("included")
    private String included;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("start")
    public String getStart() {
        return start;
    }

    @JsonProperty("start")
    public void setStart(String start) {
        this.start = start;
    }

    @JsonProperty("end")
    public String getEnd() {
        return end;
    }

    @JsonProperty("end")
    public void setEnd(String end) {
        this.end = end;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("rate")
    public int getRate() {
        return rate;
    }

    @JsonProperty("rate")
    public void setRate(int rate) {
        this.rate = rate;
    }

    @JsonProperty("conditions")
    public String getConditions() {
        return conditions;
    }

    @JsonProperty("conditions")
    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    @JsonProperty("included")
    public String getIncluded() {
        return included;
    }

    @JsonProperty("included")
    public void setIncluded(String included) {
        this.included = included;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("start", start).append("end", end).append("type", type).append("currency", currency).append("rate", rate).append("conditions", conditions).append("included", included).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(conditions).append(rate).append(start).append(additionalProperties).append(included).append(type).append(end).append(currency).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RoomRate) == false) {
            return false;
        }
        RoomRate rhs = ((RoomRate) other);
        return new EqualsBuilder().append(conditions, rhs.conditions).append(rate, rhs.rate).append(start, rhs.start).append(additionalProperties, rhs.additionalProperties).append(included, rhs.included).append(type, rhs.type).append(end, rhs.end).append(currency, rhs.currency).isEquals();
    }

}
