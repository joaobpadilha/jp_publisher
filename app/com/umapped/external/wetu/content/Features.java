
package com.umapped.external.wetu.content;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "star_authority",
    "stars",
    "rating",
    "rooms",
    "check_in_time",
    "check_out_time",
    "spoken_languages",
    "special_interests",
    "property_facilities",
    "room_facilities",
    "available_services",
    "activities_on_site",
    "activities_off_site",
    "max_group_size"
})
public class Features {

    @JsonProperty("star_authority")
    private String starAuthority;
    @JsonProperty("stars")
    private int stars;
    @JsonProperty("rating")
    private String rating;
    @JsonProperty("rooms")
    private int rooms;
    @JsonProperty("check_in_time")
    private String checkInTime;
    @JsonProperty("check_out_time")
    private String checkOutTime;
    @JsonProperty("spoken_languages")
    private List<String> spokenLanguages = null;
    @JsonProperty("special_interests")
    private List<String> specialInterests = null;
    @JsonProperty("property_facilities")
    private List<String> propertyFacilities = null;
    @JsonProperty("room_facilities")
    private List<String> roomFacilities = null;
    @JsonProperty("available_services")
    private List<String> availableServices = null;
    @JsonProperty("activities_on_site")
    private List<String> activitiesOnSite = null;
    @JsonProperty("activities_off_site")
    private List<String> activitiesOffSite = null;
    @JsonProperty("max_group_size")
    private int maxGroupSize;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("star_authority")
    public String getStarAuthority() {
        return starAuthority;
    }

    @JsonProperty("star_authority")
    public void setStarAuthority(String starAuthority) {
        this.starAuthority = starAuthority;
    }

    @JsonProperty("stars")
    public int getStars() {
        return stars;
    }

    @JsonProperty("stars")
    public void setStars(int stars) {
        this.stars = stars;
    }

    @JsonProperty("rating")
    public String getRating() {
        return rating;
    }

    @JsonProperty("rating")
    public void setRating(String rating) {
        this.rating = rating;
    }

    @JsonProperty("rooms")
    public int getRooms() {
        return rooms;
    }

    @JsonProperty("rooms")
    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    @JsonProperty("check_in_time")
    public String getCheckInTime() {
        return checkInTime;
    }

    @JsonProperty("check_in_time")
    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    @JsonProperty("check_out_time")
    public String getCheckOutTime() {
        return checkOutTime;
    }

    @JsonProperty("check_out_time")
    public void setCheckOutTime(String checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    @JsonProperty("spoken_languages")
    public List<String> getSpokenLanguages() {
        return spokenLanguages;
    }

    @JsonProperty("spoken_languages")
    public void setSpokenLanguages(List<String> spokenLanguages) {
        this.spokenLanguages = spokenLanguages;
    }

    @JsonProperty("special_interests")
    public List<String> getSpecialInterests() {
        return specialInterests;
    }

    @JsonProperty("special_interests")
    public void setSpecialInterests(List<String> specialInterests) {
        this.specialInterests = specialInterests;
    }

    @JsonProperty("property_facilities")
    public List<String> getPropertyFacilities() {
        return propertyFacilities;
    }

    @JsonProperty("property_facilities")
    public void setPropertyFacilities(List<String> propertyFacilities) {
        this.propertyFacilities = propertyFacilities;
    }

    @JsonProperty("room_facilities")
    public List<String> getRoomFacilities() {
        return roomFacilities;
    }

    @JsonProperty("room_facilities")
    public void setRoomFacilities(List<String> roomFacilities) {
        this.roomFacilities = roomFacilities;
    }

    @JsonProperty("available_services")
    public List<String> getAvailableServices() {
        return availableServices;
    }

    @JsonProperty("available_services")
    public void setAvailableServices(List<String> availableServices) {
        this.availableServices = availableServices;
    }

    @JsonProperty("activities_on_site")
    public List<String> getActivitiesOnSite() {
        return activitiesOnSite;
    }

    @JsonProperty("activities_on_site")
    public void setActivitiesOnSite(List<String> activitiesOnSite) {
        this.activitiesOnSite = activitiesOnSite;
    }

    @JsonProperty("activities_off_site")
    public List<String> getActivitiesOffSite() {
        return activitiesOffSite;
    }

    @JsonProperty("activities_off_site")
    public void setActivitiesOffSite(List<String> activitiesOffSite) {
        this.activitiesOffSite = activitiesOffSite;
    }

    @JsonProperty("max_group_size")
    public int getMaxGroupSize() {
        return maxGroupSize;
    }

    @JsonProperty("max_group_size")
    public void setMaxGroupSize(int maxGroupSize) {
        this.maxGroupSize = maxGroupSize;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("starAuthority", starAuthority).append("stars", stars).append("rating", rating).append("rooms", rooms).append("checkInTime", checkInTime).append("checkOutTime", checkOutTime).append("spokenLanguages", spokenLanguages).append("specialInterests", specialInterests).append("propertyFacilities", propertyFacilities).append("roomFacilities", roomFacilities).append("availableServices", availableServices).append("activitiesOnSite", activitiesOnSite).append("activitiesOffSite", activitiesOffSite).append("maxGroupSize", maxGroupSize).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(spokenLanguages).append(starAuthority).append(checkOutTime).append(specialInterests).append(activitiesOffSite).append(propertyFacilities).append(checkInTime).append(availableServices).append(roomFacilities).append(additionalProperties).append(stars).append(maxGroupSize).append(rating).append(activitiesOnSite).append(rooms).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Features) == false) {
            return false;
        }
        Features rhs = ((Features) other);
        return new EqualsBuilder().append(spokenLanguages, rhs.spokenLanguages).append(starAuthority, rhs.starAuthority).append(checkOutTime, rhs.checkOutTime).append(specialInterests, rhs.specialInterests).append(activitiesOffSite, rhs.activitiesOffSite).append(propertyFacilities, rhs.propertyFacilities).append(checkInTime, rhs.checkInTime).append(availableServices, rhs.availableServices).append(roomFacilities, rhs.roomFacilities).append(additionalProperties, rhs.additionalProperties).append(stars, rhs.stars).append(maxGroupSize, rhs.maxGroupSize).append(rating, rhs.rating).append(activitiesOnSite, rhs.activitiesOnSite).append(rooms, rhs.rooms).isEquals();
    }

}
