
package com.umapped.external.wetu.content;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "email",
    "telephone",
    "website_url",
    "address",
    "facebook",
    "twitter",
    "front_desk_telephone"
})
public class ContactInformation {

    @JsonProperty("email")
    private String email;
    @JsonProperty("telephone")
    private String telephone;
    @JsonProperty("website_url")
    private String websiteUrl;
    @JsonProperty("address")
    private String address;
    @JsonProperty("facebook")
    private String facebook;
    @JsonProperty("twitter")
    private String twitter;
    @JsonProperty("front_desk_telephone")
    private String frontDeskTelephone;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("telephone")
    public String getTelephone() {
        return telephone;
    }

    @JsonProperty("telephone")
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @JsonProperty("website_url")
    public String getWebsiteUrl() {
        return websiteUrl;
    }

    @JsonProperty("website_url")
    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("facebook")
    public String getFacebook() {
        return facebook;
    }

    @JsonProperty("facebook")
    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    @JsonProperty("twitter")
    public String getTwitter() {
        return twitter;
    }

    @JsonProperty("twitter")
    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    @JsonProperty("front_desk_telephone")
    public String getFrontDeskTelephone() {
        return frontDeskTelephone;
    }

    @JsonProperty("front_desk_telephone")
    public void setFrontDeskTelephone(String frontDeskTelephone) {
        this.frontDeskTelephone = frontDeskTelephone;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("email", email).append("telephone", telephone).append("websiteUrl", websiteUrl).append("address", address).append("facebook", facebook).append("twitter", twitter).append("frontDeskTelephone", frontDeskTelephone).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(twitter).append(address).append(email).append(additionalProperties).append(facebook).append(websiteUrl).append(telephone).append(frontDeskTelephone).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ContactInformation) == false) {
            return false;
        }
        ContactInformation rhs = ((ContactInformation) other);
        return new EqualsBuilder().append(twitter, rhs.twitter).append(address, rhs.address).append(email, rhs.email).append(additionalProperties, rhs.additionalProperties).append(facebook, rhs.facebook).append(websiteUrl, rhs.websiteUrl).append(telephone, rhs.telephone).append(frontDeskTelephone, rhs.frontDeskTelephone).isEquals();
    }

}
