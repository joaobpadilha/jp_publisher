package com.umapped.external.wetu.itinerary;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.umapped.external.wetu.content.ContentRoot;
import com.umapped.external.wetu.itinerary.details.ItineraryDetail;
import com.umapped.external.wetu.itinerary.list.ItineraryList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

/**
 * Created by george on 2016-06-14.
 */
public class ItineraryExtractor {

  public ItineraryList extractItineraryList(String token, String param) {
    HttpClient client = HttpClientBuilder.create().build();
    String url = "";
    //If token is numeric, then use new WETU API endpoints
    if (StringUtils.isNumeric(token)) {
      if (param != null && !param.isEmpty()) {
        url = "https://wetu.com/API/Itinerary/V7/List?appKey=9923A8BA-C911-4FB5-8386-E9301697DCB5&operatorId=" + token + "&" + param;
      } else {
        url = "https://wetu.com/API/Itinerary/V7/List?appKey=9923A8BA-C911-4FB5-8386-E9301697DCB5&operatorId=" + token;
      }
    } else {//Else use old endpoints
      if (param != null) {
        url = "https://wetu.com/API/Itinerary/" + token + "/V7/List?" + param;
      } else {
        url = "https://wetu.com/API/Itinerary/" + token + "/V7/List";
      }
    }

    HttpGet get = new HttpGet(url);
    get.setHeader("Content-Type", "application/json; charset=UTF-8");
    get.addHeader("accept", "application/json");

    HttpResponse r = null;
    try {
      r = client.execute(get);
    } catch (IOException e) {
      e.printStackTrace();
    }

    HttpEntity entity = r.getEntity();
    ItineraryList details = null;

    if (r != null && r.getStatusLine() != null && r.getStatusLine().getStatusCode() == 200) {

      try {
        String resp = EntityUtils.toString(entity);
        ObjectMapper mapper = new ObjectMapper();

        com.fasterxml.jackson.databind.ObjectReader reader = mapper.reader(ItineraryList.class);
        details = reader.readValue(resp);
        //System.out.println("Itinerary List >>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n\n"+details.toString()+"\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n\n");
      }
      catch (Exception e) {
        Log.log(LogLevel.ERROR, "ItineraryList - Cannot read JSON from response: " , e);
        e.printStackTrace();
      }

    }
    return details;

  }


  public ItineraryDetail extractItineraryDetail(String param) {
    HttpClient client = HttpClientBuilder.create().build();
    String url = "";
    if (param != null) {
      url = "https://wetu.com/API/Itinerary/V7/Get?id=" + param;
    }

    HttpGet get = new HttpGet(url);
    get.setHeader("Content-Type", "application/json; charset=UTF-8");
    get.addHeader("accept", "application/json");

    HttpResponse r = null;
    try {
      r = client.execute(get);
    } catch (IOException e) {
      e.printStackTrace();
    }

    HttpEntity entity = r.getEntity();
    ItineraryDetail details = null;

    if (r != null && r.getStatusLine() != null && r.getStatusLine().getStatusCode() == 200) {

      try {
        String resp = EntityUtils.toString(entity);
        ObjectMapper mapper = new ObjectMapper();

        com.fasterxml.jackson.databind.ObjectReader reader = mapper.reader(ItineraryDetail.class);
        details = reader.readValue(resp);
        System.out.println("Itinerary Details >>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n\n"+details.toString()+"\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n\n");
      }
      catch (Exception e) {
        Log.log(LogLevel.ERROR, "ItineraryDetail - Cannot read JSON from response: " , e);
        e.printStackTrace();
      }
    }
    return details;
  }

  public ContentRoot extractItineraryContent(String identifier) {
    HttpClient client = HttpClientBuilder.create().build();
    String url = "";

    if (identifier != null && !identifier.isEmpty()) {
      url = "https://wetu.com/API/Itinerary/V7/GetContent?id=" + identifier + "&appKey=9923A8BA-C911-4FB5-8386-E9301697DCB5";
    }

    HttpGet get = new HttpGet(url);
    get.setHeader("Content-Type", "application/json; charset=UTF-8");
    get.addHeader("accept", "application/json");

    HttpResponse r = null;
    try {
      r = client.execute(get);
    } catch (IOException e) {
      e.printStackTrace();
    }

    HttpEntity entity = r.getEntity();
    ContentRoot contentRoot = null;

    if (r != null && r.getStatusLine() != null && r.getStatusLine().getStatusCode() == 200) {

      try {
        String resp = EntityUtils.toString(entity);
        ObjectMapper mapper = new ObjectMapper();

        com.fasterxml.jackson.databind.ObjectReader reader = mapper.reader(ContentRoot.class);
        contentRoot = reader.readValue(resp);
      }
      catch (Exception e) {
        Log.log(LogLevel.ERROR, "ContentRoot - Cannot read JSON from response: " , e);
        e.printStackTrace();
      }

    }
    return contentRoot;
  }
}
