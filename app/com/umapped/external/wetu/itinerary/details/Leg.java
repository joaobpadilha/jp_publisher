
package com.umapped.external.wetu.itinerary.details;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "activities",
    "room_basis",
    "drinks_basis",
    "notes",
    "consultant_notes",
    "included",
    "excluded",
    "days",
    "itinerary_leg_id",
    "sequence",
    "content_entity_id",
    "destination_content_entity_id",
    "nights",
    "booking_reference",
    "rooms",
    "day_rooms",
    "type",
    "check_in_time",
    "check_out_time",
    "check_in_sequence",
    "check_out_sequence"
})
public class Leg {

    @JsonProperty("days")
    private List<Day> days = new ArrayList<Day>();
    @JsonProperty("destination_content_entity_id")
    private int destinationContentId;
    @JsonProperty("activities")
    private List<Activity> activities = new ArrayList<>();
    @JsonProperty("room_basis")
    private String roomBasis;
    @JsonProperty("drink_basis")
    private String drinkBasis;
    @JsonProperty("notes")
    private String notes;
    @JsonProperty("consultant_notes")
    private String consultantNotes;
    @JsonProperty("included")
    private String included;
    @JsonProperty("excluded")
    private String excluded;
    @JsonProperty("itinerary_leg_id")
    private int itineraryLegId;
    @JsonProperty("sequence")
    private int sequence;
    @JsonProperty("content_entity_id")
    private int contentEntityId;
    @JsonProperty("alternate_accommodations")
    private List<AlternateAccommodation> alternateAccommodations = new ArrayList<AlternateAccommodation>();
    @JsonProperty("nights")
    private int nights;
    @JsonProperty("booking_reference")
    private String bookingReference;
    @JsonProperty("rooms")
    private List<Room> rooms = new ArrayList<Room>();
    @JsonProperty("day_rooms")
    private DayRoom dayRooms;
    @JsonProperty("type")
    private String type;
    @JsonProperty("check_in_time")
    private String checkInTime;
    @JsonProperty("check_out_time")
    private String checkOutTime;
    @JsonProperty("check_in_sequence")
    private int checkInSequence;
    @JsonProperty("check_out_sequence")
    private int checkOutSequence;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     *     The days
     */
    @JsonProperty("days")
    public List<Day> getDays() {
        return days;
    }

    /**
     *
     * @param days
     *     The days
     */
    @JsonProperty("days")
    public void setDays(List<Day> days) {
        this.days = days;
    }

    @JsonProperty("destination_content_entity_id")
    public int getDestinationContentId() {return destinationContentId;}

    @JsonProperty("destination_content_entity_id")
    public void setDestinationContentId(int destinationContentId) {this.destinationContentId =  destinationContentId;}



    /**
     *
     * @return
     *     The itineraryLegId
     */
    @JsonProperty("itinerary_leg_id")
    public int getItineraryLegId() {
        return itineraryLegId;
    }

    /**
     *
     * @param itineraryLegId
     *     The itinerary_leg_id
     */
    @JsonProperty("itinerary_leg_id")
    public void setItineraryLegId(int itineraryLegId) {
        this.itineraryLegId = itineraryLegId;
    }

    /**
     *
     * @return
     *     The sequence
     */
    @JsonProperty("sequence")
    public int getSequence() {
        return sequence;
    }

    /**
     *
     * @param sequence
     *     The sequence
     */
    @JsonProperty("sequence")
    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    /**
     *
     * @return
     *     The contentEntityId
     */
    @JsonProperty("content_entity_id")
    public int getContentEntityId() {
        return contentEntityId;
    }

    /**
     *
     * @param contentEntityId
     *     The content_entity_id
     */
    @JsonProperty("content_entity_id")
    public void setContentEntityId(int contentEntityId) {
        this.contentEntityId = contentEntityId;
    }

    /**
     *
     * @return
     *     The nights
     */
    @JsonProperty("nights")
    public int getNights() {
        return nights;
    }

    /**
     *
     * @param nights
     *     The nights
     */
    @JsonProperty("nights")
    public void setNights(int nights) {
        this.nights = nights;
    }

    /**
     *
     * @return
     *     The bookingReference
     */
    @JsonProperty("booking_reference")
    public String getBookingReference() {
        return bookingReference;
    }

    /**
     *
     * @param bookingReference
     *     The booking_reference
     */
    @JsonProperty("booking_reference")
    public void setBookingReference(String bookingReference) {
        this.bookingReference = bookingReference;
    }

    /**
     *
     * @return
     *     The rooms
     */
    @JsonProperty("rooms")
    public List<Room> getRooms() {
        return rooms;
    }

    /**
     *
     * @param rooms
     *     The rooms
     */
    @JsonProperty("rooms")
    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    /**
     *
     * @return
     *     The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     *     The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("alternate_accommodations")
    public List<AlternateAccommodation> getAlternateAccommodations() {
        return alternateAccommodations;
    }


    @JsonProperty("alternate_accommodations")
    public void setAlternateAccommodations(List<AlternateAccommodation> alternateAccommodations) {
        this.alternateAccommodations = alternateAccommodations;
    }

    @JsonProperty("day_rooms")
    public DayRoom getDayRooms() {
        return dayRooms;
    }

    @JsonProperty("day_rooms")
    public void setDayRooms(DayRoom dayRooms) {
        this.dayRooms = dayRooms;
    }

    @JsonProperty("check_in_time")
    public String getCheckInTime() {
        return checkInTime;
    }

    @JsonProperty("check_in_time")
    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    @JsonProperty("check_out_time")
    public String getCheckOutTime() {
        return checkOutTime;
    }

    @JsonProperty("check_out_time")
    public void setCheckOutTime(String checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    @JsonProperty("check_in_sequence")
    public int getCheckInSequence() {
        return checkInSequence;
    }

    @JsonProperty("check_in_sequence")
    public void setCheckInSequence(int checkInSequence) {
        this.checkInSequence = checkInSequence;
    }

    @JsonProperty("check_out_sequence")
    public int getCheckOutSequence() {
        return checkOutSequence;
    }

    @JsonProperty("check_out_sequence")
    public void setCheckOutSequence(int checkOutSequence) {
        this.checkOutSequence = checkOutSequence;
    }

    @JsonProperty("activities")
    public List<Activity> getActivities() {
        return activities;
    }

    @JsonProperty("activities")
    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    @JsonProperty("room_basis")
    public String getRoomBasis() {
        return roomBasis;
    }

    @JsonProperty("room_basis")
    public void setRoomBasis(String roomBasis) {
        this.roomBasis = roomBasis;
    }

    @JsonProperty("drink_basis")
    public String getDrinkBasis() {
        return drinkBasis;
    }

    @JsonProperty("drink_basis")
    public void setDrinkBasis(String drinkBasis) {
        this.drinkBasis = drinkBasis;
    }

    @JsonProperty("notes")
    public String getNotes() {
        return notes;
    }

    @JsonProperty("notes")
    public void setNotes(String notes) {
        this.notes = notes;
    }

    @JsonProperty("consultant_notes")
    public String getConsultantNotes() {
        return consultantNotes;
    }

    @JsonProperty("consultant_notes")
    public void setConsultantNotes(String consultantNotes) {
        this.consultantNotes = consultantNotes;
    }

    @JsonProperty("included")
    public String getIncluded() {
        return included;
    }

    @JsonProperty("included")
    public void setIncluded(String included) {
        this.included = included;
    }

    @JsonProperty("excluded")
    public String getExcluded() {
        return excluded;
    }

    @JsonProperty("excluded")
    public void setExcluded(String excluded) {
        this.excluded = excluded;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(days).append(itineraryLegId).append(sequence).append(contentEntityId).append(nights).append(bookingReference).append(rooms).append(type).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Leg) == false) {
            return false;
        }
        Leg rhs = ((Leg) other);
        return new EqualsBuilder().append(days, rhs.days).append(itineraryLegId, rhs.itineraryLegId).append(sequence, rhs.sequence).append(contentEntityId, rhs.contentEntityId).append(nights, rhs.nights).append(bookingReference, rhs.bookingReference).append(rooms, rhs.rooms).append(type, rhs.type).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
