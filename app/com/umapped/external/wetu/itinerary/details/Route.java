
package com.umapped.external.wetu.itinerary.details;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "mode",
    "start_leg_id",
    "end_leg_id",
    "start_content_entity_id",
    "end_content_entity_id",
    "sequence",
    "agency",
    "vehicle",
    "reference_codes",
    "notes",
    "start",
    "start_time",
    "end",
    "end_time",
    "points",
    "via_points",
    "directions",
    "contact_numbers",
    "start_terminal",
    "end_terminal",
    "ticket_class",
    "check_in_time",
    "type",
    "variant",
    "duration",
    "distance",
    "label",
    "day_sequence"
})
public class Route {

    @JsonProperty("mode")
    private String mode;
    @JsonProperty("start_leg_id")
    private int startLegId;
    @JsonProperty("end_leg_id")
    private int endLegId;
    @JsonProperty("start_content_entity_id")
    private int startContentEntityId;
    @JsonProperty("end_content_entity_id")
    private int endContentEntityId;
    @JsonProperty("sequence")
    private int sequence;
    @JsonProperty("agency")
    private String agency;
    @JsonProperty("vehicle")
    private String vehicle;
    @JsonProperty("reference_codes")
    private String referenceCodes;
    @JsonProperty("notes")
    private String notes;
    @JsonProperty("start")
    private String start;
    @JsonProperty("start_time")
    private String startTime;
    @JsonProperty("end")
    private String end;
    @JsonProperty("end_time")
    private String endTime;
    @JsonProperty("points")
    private String points;
    @JsonProperty("via_points")
    private String viaPoints;
    @JsonProperty("directions")
    private String directions;
    @JsonProperty("contact_numbers")
    private String contactNumbers;
    @JsonProperty("start_terminal")
    private String startTerminal;
    @JsonProperty("end_terminal")
    private String endTerminal;
    @JsonProperty("ticket_class")
    private String ticketClass;
    @JsonProperty("check_in_time")
    private String checkInTime;
    @JsonProperty("type")
    private String type;
    @JsonProperty("variant")
    private String variant;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("distance")
    private double distance;
    @JsonProperty("label")
    private String label;
    @JsonProperty("day_sequence")
    private int daySequence;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The mode
     */
    @JsonProperty("mode")
    public String getMode() {
        return mode;
    }

    /**
     * 
     * @param mode
     *     The mode
     */
    @JsonProperty("mode")
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * 
     * @return
     *     The startLegId
     */
    @JsonProperty("start_leg_id")
    public int getStartLegId() {
        return startLegId;
    }

    /**
     * 
     * @param startLegId
     *     The start_leg_id
     */
    @JsonProperty("start_leg_id")
    public void setStartLegId(int startLegId) {
        this.startLegId = startLegId;
    }

    /**
     * 
     * @return
     *     The endLegId
     */
    @JsonProperty("end_leg_id")
    public int getEndLegId() {
        return endLegId;
    }

    /**
     * 
     * @param endLegId
     *     The end_leg_id
     */
    @JsonProperty("end_leg_id")
    public void setEndLegId(int endLegId) {
        this.endLegId = endLegId;
    }

    /**
     * 
     * @return
     *     The startContentEntityId
     */
    @JsonProperty("start_content_entity_id")
    public int getStartContentEntityId() {
        return startContentEntityId;
    }

    /**
     * 
     * @param startContentEntityId
     *     The start_content_entity_id
     */
    @JsonProperty("start_content_entity_id")
    public void setStartContentEntityId(int startContentEntityId) {
        this.startContentEntityId = startContentEntityId;
    }

    /**
     * 
     * @return
     *     The endContentEntityId
     */
    @JsonProperty("end_content_entity_id")
    public int getEndContentEntityId() {
        return endContentEntityId;
    }

    /**
     * 
     * @param endContentEntityId
     *     The end_content_entity_id
     */
    @JsonProperty("end_content_entity_id")
    public void setEndContentEntityId(int endContentEntityId) {
        this.endContentEntityId = endContentEntityId;
    }

    /**
     * 
     * @return
     *     The sequence
     */
    @JsonProperty("sequence")
    public int getSequence() {
        return sequence;
    }

    /**
     * 
     * @param sequence
     *     The sequence
     */
    @JsonProperty("sequence")
    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    /**
     * 
     * @return
     *     The agency
     */
    @JsonProperty("agency")
    public String getAgency() {
        return agency;
    }

    /**
     * 
     * @param agency
     *     The agency
     */
    @JsonProperty("agency")
    public void setAgency(String agency) {
        this.agency = agency;
    }

    /**
     * 
     * @return
     *     The vehicle
     */
    @JsonProperty("vehicle")
    public String getVehicle() {
        return vehicle;
    }

    /**
     * 
     * @param vehicle
     *     The vehicle
     */
    @JsonProperty("vehicle")
    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    /**
     * 
     * @return
     *     The referenceCodes
     */
    @JsonProperty("reference_codes")
    public String getReferenceCodes() {
        return referenceCodes;
    }

    /**
     * 
     * @param referenceCodes
     *     The reference_codes
     */
    @JsonProperty("reference_codes")
    public void setReferenceCodes(String referenceCodes) {
        this.referenceCodes = referenceCodes;
    }

    /**
     * 
     * @return
     *     The notes
     */
    @JsonProperty("notes")
    public String getNotes() {
        return notes;
    }

    /**
     * 
     * @param notes
     *     The notes
     */
    @JsonProperty("notes")
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * 
     * @return
     *     The start
     */
    @JsonProperty("start")
    public String getStart() {
        return start;
    }

    /**
     * 
     * @param start
     *     The start
     */
    @JsonProperty("start")
    public void setStart(String start) {
        this.start = start;
    }

    /**
     * 
     * @return
     *     The startTime
     */
    @JsonProperty("start_time")
    public String getStartTime() {
        return startTime;
    }

    /**
     * 
     * @param startTime
     *     The start_time
     */
    @JsonProperty("start_time")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * 
     * @return
     *     The end
     */
    @JsonProperty("end")
    public String getEnd() {
        return end;
    }

    /**
     * 
     * @param end
     *     The end
     */
    @JsonProperty("end")
    public void setEnd(String end) {
        this.end = end;
    }

    /**
     * 
     * @return
     *     The endTime
     */
    @JsonProperty("end_time")
    public String getEndTime() {
        return endTime;
    }

    /**
     * 
     * @param endTime
     *     The end_time
     */
    @JsonProperty("end_time")
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * 
     * @return
     *     The points
     */
    @JsonProperty("points")
    public String getPoints() {
        return points;
    }

    /**
     * 
     * @param points
     *     The points
     */
    @JsonProperty("points")
    public void setPoints(String points) {
        this.points = points;
    }

    /**
     * 
     * @return
     *     The viaPoints
     */
    @JsonProperty("via_points")
    public String getViaPoints() {
        return viaPoints;
    }

    /**
     * 
     * @param viaPoints
     *     The via_points
     */
    @JsonProperty("via_points")
    public void setViaPoints(String viaPoints) {
        this.viaPoints = viaPoints;
    }

    /**
     * 
     * @return
     *     The directions
     */
    @JsonProperty("directions")
    public String getDirections() {
        return directions;
    }

    /**
     * 
     * @param directions
     *     The directions
     */
    @JsonProperty("directions")
    public void setDirections(String directions) {
        this.directions = directions;
    }

    /**
     * 
     * @return
     *     The contactNumbers
     */
    @JsonProperty("contact_numbers")
    public String getContactNumbers() {
        return contactNumbers;
    }

    /**
     * 
     * @param contactNumbers
     *     The contact_numbers
     */
    @JsonProperty("contact_numbers")
    public void setContactNumbers(String contactNumbers) {
        this.contactNumbers = contactNumbers;
    }

    /**
     * 
     * @return
     *     The startTerminal
     */
    @JsonProperty("start_terminal")
    public String getStartTerminal() {
        return startTerminal;
    }

    /**
     * 
     * @param startTerminal
     *     The start_terminal
     */
    @JsonProperty("start_terminal")
    public void setStartTerminal(String startTerminal) {
        this.startTerminal = startTerminal;
    }

    /**
     * 
     * @return
     *     The endTerminal
     */
    @JsonProperty("end_terminal")
    public String getEndTerminal() {
        return endTerminal;
    }

    /**
     * 
     * @param endTerminal
     *     The end_terminal
     */
    @JsonProperty("end_terminal")
    public void setEndTerminal(String endTerminal) {
        this.endTerminal = endTerminal;
    }

    /**
     * 
     * @return
     *     The ticketClass
     */
    @JsonProperty("ticket_class")
    public String getTicketClass() {
        return ticketClass;
    }

    /**
     * 
     * @param ticketClass
     *     The ticket_class
     */
    @JsonProperty("ticket_class")
    public void setTicketClass(String ticketClass) {
        this.ticketClass = ticketClass;
    }

    /**
     * 
     * @return
     *     The checkInTime
     */
    @JsonProperty("check_in_time")
    public String getCheckInTime() {
        return checkInTime;
    }

    /**
     * 
     * @param checkInTime
     *     The check_in_time
     */
    @JsonProperty("check_in_time")
    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    /**
     * 
     * @return
     *     The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The variant
     */
    @JsonProperty("variant")
    public String getVariant() {
        return variant;
    }

    /**
     * 
     * @param variant
     *     The variant
     */
    @JsonProperty("variant")
    public void setVariant(String variant) {
        this.variant = variant;
    }

    /**
     * 
     * @return
     *     The duration
     */
    @JsonProperty("duration")
    public String getDuration() {
        return duration;
    }

    /**
     * 
     * @param duration
     *     The duration
     */
    @JsonProperty("duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    /**
     * 
     * @return
     *     The distance
     */
    @JsonProperty("distance")
    public double getDistance() {
        return distance;
    }

    /**
     * 
     * @param distance
     *     The distance
     */
    @JsonProperty("distance")
    public void setDistance(double distance) {
        this.distance = distance;
    }

    @JsonProperty("label")
    public String getLabel() {
        return label;
    }

    @JsonProperty("label")
    public void setLabel(String label) {
        this.label = label;
    }

    @JsonProperty("day_sequence")
    public int getDaySequence() {
        return daySequence;
    }

    @JsonProperty("day_sequence")
    public void setDaySequence(int daySequence) {
        this.daySequence = daySequence;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(mode).append(startLegId).append(endLegId).append(startContentEntityId).append(endContentEntityId).append(sequence).append(agency).append(vehicle).append(referenceCodes).append(notes).append(start).append(startTime).append(end).append(endTime).append(points).append(viaPoints).append(directions).append(contactNumbers).append(startTerminal).append(endTerminal).append(ticketClass).append(checkInTime).append(type).append(variant).append(duration).append(distance).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Route) == false) {
            return false;
        }
        Route rhs = ((Route) other);
        return new EqualsBuilder().append(mode, rhs.mode).append(startLegId, rhs.startLegId).append(endLegId, rhs.endLegId).append(startContentEntityId, rhs.startContentEntityId).append(endContentEntityId, rhs.endContentEntityId).append(sequence, rhs.sequence).append(agency, rhs.agency).append(vehicle, rhs.vehicle).append(referenceCodes, rhs.referenceCodes).append(notes, rhs.notes).append(start, rhs.start).append(startTime, rhs.startTime).append(end, rhs.end).append(endTime, rhs.endTime).append(points, rhs.points).append(viaPoints, rhs.viaPoints).append(directions, rhs.directions).append(contactNumbers, rhs.contactNumbers).append(startTerminal, rhs.startTerminal).append(endTerminal, rhs.endTerminal).append(ticketClass, rhs.ticketClass).append(checkInTime, rhs.checkInTime).append(type, rhs.type).append(variant, rhs.variant).append(duration, rhs.duration).append(distance, rhs.distance).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
