
package com.umapped.external.wetu.itinerary.details;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "operator_id",
    "operator_user_id",
    "operator_identity_id",
    "operator_theme_id",
    "type",
    "identifier",
    "identifier_key",
    "days",
    "name",
    "language",
    "reference_number",
    "description",
    "categories",
    "client_name",
    "client_email",
    "client_phone",
    "start_date",
    "legs",
    "routes",
    "documents",
    "hide_route_information",
    "bound_lat1",
    "bound_lat2",
    "bound_lng1",
    "bound_lng2",
    "price",
    "price_includes",
    "price_excludes",
    "car_hire",
    "is_disabled",
    "travellers_adult",
    "travellers_children",
    "travellers",
    "rooms_single",
    "rooms_double",
    "rooms_twin",
    "rooms_triple",
    "rooms_family",
    "start_travel_days",
    "end_travel_days",
    "booking_status",
    "source_operator_id",
    "contacts",
    "services",
    "special_interests",
    "notification_frequency",
    "images",
    "departures",
    "departure_locations",
    "departure_times",
    "returns",
    "return_locations",
    "return_times",
    "duration",
    "additional_info",
    "spoken_languages",
    "group_size",
    "published",
    "travel_arrangements",
    "route_handling_mode"
})
public class ItineraryDetail {

    @JsonProperty("operator_id")
    private int operatorId;
    @JsonProperty("operator_user_id")
    private int operatorUserId;
    @JsonProperty("operator_identity_id")
    private int operatorIdentityId;
    @JsonProperty("operator_theme_id")
    private int operatorThemeId;
    @JsonProperty("type")
    private String type;
    @JsonProperty("identifier")
    private String identifier;
    @JsonProperty("identifier_key")
    private String identifierKey;
    @JsonProperty("days")
    private int days;
    @JsonProperty("name")
    private String name;
    @JsonProperty("language")
    private String language;
    @JsonProperty("reference_number")
    private String referenceNumber;
    @JsonProperty("description")
    private String description;
    @JsonProperty("categories")
    private List<String> categories = new ArrayList<String>();
    @JsonProperty("client_name")
    private String clientName;
    @JsonProperty("client_email")
    private String clientEmail;
    @JsonProperty("client_phone")
    private String clientPhone;
    @JsonProperty("start_date")
    private String startDate;
    @JsonProperty("legs")
    private List<Leg> legs = new ArrayList<Leg>();
    @JsonProperty("routes")
    private List<Route> routes = new ArrayList<Route>();
    @JsonProperty("documents")
    private List<Document> documents = new ArrayList<Document>();
    @JsonProperty("hide_route_information")
    private boolean hideRouteInformation;
    @JsonProperty("bound_lat1")
    private double boundLat1;
    @JsonProperty("bound_lat2")
    private double boundLat2;
    @JsonProperty("bound_lng1")
    private double boundLng1;
    @JsonProperty("bound_lng2")
    private double boundLng2;
    @JsonProperty("price")
    private String price;
    @JsonProperty("price_includes")
    private String priceIncludes;
    @JsonProperty("price_excludes")
    private String priceExcludes;
    @JsonProperty("car_hire")
    private List<CarHire> carHire;
    @JsonProperty("is_disabled")
    private boolean isDisabled;
    @JsonProperty("travellers_adult")
    private int travellersAdult;
    @JsonProperty("travellers_children")
    private int travellersChildren;
    @JsonProperty("travellers")
    private List<Traveller> travellers = new ArrayList<Traveller>();
    @JsonProperty("rooms_single")
    private int roomsSingle;
    @JsonProperty("rooms_double")
    private int roomsDouble;
    @JsonProperty("rooms_twin")
    private int roomsTwin;
    @JsonProperty("rooms_triple")
    private int roomsTriple;
    @JsonProperty("rooms_family")
    private int roomsFamily;
    @JsonProperty("start_travel_days")
    private int startTravelDays;
    @JsonProperty("end_travel_days")
    private int endTravelDays;
    @JsonProperty("booking_status")
    private String bookingStatus;
    @JsonProperty("source_operator_id")
    private int sourceOperatorId;
    @JsonProperty("contacts")
    private List<Contact> contacts = new ArrayList<Contact>();
    @JsonProperty("services")
    private String services;
    @JsonProperty("special_interests")
    private List<String> specialInterests;
    @JsonProperty("notification_frequency")
    private String notificationFrequency;
    @JsonProperty("images")
    private List<Image> images;
    @JsonProperty("departures")
    private String departures;
    @JsonProperty("departure_locations")
    private String departureLocations;
    @JsonProperty("departure_times")
    private String departureTimes;
    @JsonProperty("returns")
    private String returns;
    @JsonProperty("return_locations")
    private String returnLocations;
    @JsonProperty("return_times")
    private String returnTimes;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("additional_info")
    private String additionalInfo;
    @JsonProperty("spoken_languages")
    private String spokenLanguages;
    @JsonProperty("group_size")
    private String groupSize;
    @JsonProperty("published")
    private boolean published;
    @JsonProperty("travel_arrangements")
    private List<TravelArrangement> travelArrangements;
    @JsonProperty("route_handling_mode")
    private String routeHandlingMode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The operatorId
     */
    @JsonProperty("operator_id")
    public int getOperatorId() {
        return operatorId;
    }

    /**
     * 
     * @param operatorId
     *     The operator_id
     */
    @JsonProperty("operator_id")
    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    /**
     * 
     * @return
     *     The operatorUserId
     */
    @JsonProperty("operator_user_id")
    public int getOperatorUserId() {
        return operatorUserId;
    }

    /**
     * 
     * @param operatorUserId
     *     The operator_user_id
     */
    @JsonProperty("operator_user_id")
    public void setOperatorUserId(int operatorUserId) {
        this.operatorUserId = operatorUserId;
    }

    /**
     * 
     * @return
     *     The operatorIdentityId
     */
    @JsonProperty("operator_identity_id")
    public int getOperatorIdentityId() {
        return operatorIdentityId;
    }

    /**
     * 
     * @param operatorIdentityId
     *     The operator_identity_id
     */
    @JsonProperty("operator_identity_id")
    public void setOperatorIdentityId(int operatorIdentityId) {
        this.operatorIdentityId = operatorIdentityId;
    }

    /**
     * 
     * @return
     *     The operatorThemeId
     */
    @JsonProperty("operator_theme_id")
    public int getOperatorThemeId() {
        return operatorThemeId;
    }

    /**
     * 
     * @param operatorThemeId
     *     The operator_theme_id
     */
    @JsonProperty("operator_theme_id")
    public void setOperatorThemeId(int operatorThemeId) {
        this.operatorThemeId = operatorThemeId;
    }

    /**
     * 
     * @return
     *     The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The identifier
     */
    @JsonProperty("identifier")
    public String getIdentifier() {
        return identifier;
    }

    /**
     * 
     * @param identifier
     *     The identifier
     */
    @JsonProperty("identifier")
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * 
     * @return
     *     The identifierKey
     */
    @JsonProperty("identifier_key")
    public String getIdentifierKey() {
        return identifierKey;
    }

    /**
     * 
     * @param identifierKey
     *     The identifier_key
     */
    @JsonProperty("identifier_key")
    public void setIdentifierKey(String identifierKey) {
        this.identifierKey = identifierKey;
    }

    /**
     * 
     * @return
     *     The days
     */
    @JsonProperty("days")
    public int getDays() {
        return days;
    }

    /**
     * 
     * @param days
     *     The days
     */
    @JsonProperty("days")
    public void setDays(int days) {
        this.days = days;
    }

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The language
     */
    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    /**
     * 
     * @param language
     *     The language
     */
    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * 
     * @return
     *     The referenceNumber
     */
    @JsonProperty("reference_number")
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * 
     * @param referenceNumber
     *     The reference_number
     */
    @JsonProperty("reference_number")
    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    /**
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The categories
     */
    @JsonProperty("categories")
    public List<String> getCategories() {
        return categories;
    }

    /**
     * 
     * @param categories
     *     The categories
     */
    @JsonProperty("categories")
    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    /**
     * 
     * @return
     *     The clientName
     */
    @JsonProperty("client_name")
    public String getClientName() {
        return clientName;
    }

    /**
     * 
     * @param clientName
     *     The client_name
     */
    @JsonProperty("client_name")
    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    /**
     * 
     * @return
     *     The clientEmail
     */
    @JsonProperty("client_email")
    public String getClientEmail() {
        return clientEmail;
    }

    /**
     * 
     * @param clientEmail
     *     The client_email
     */
    @JsonProperty("client_email")
    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    /**
     * 
     * @return
     *     The startDate
     */
    @JsonProperty("start_date")
    public String getStartDate() {
        return startDate;
    }

    /**
     * 
     * @param startDate
     *     The start_date
     */
    @JsonProperty("start_date")
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * 
     * @return
     *     The legs
     */
    @JsonProperty("legs")
    public List<Leg> getLegs() {
        return legs;
    }

    /**
     * 
     * @param legs
     *     The legs
     */
    @JsonProperty("legs")
    public void setLegs(List<Leg> legs) {
        this.legs = legs;
    }

    /**
     * 
     * @return
     *     The routes
     */
    @JsonProperty("routes")
    public List<Route> getRoutes() {
        return routes;
    }

    /**
     * 
     * @param routes
     *     The routes
     */
    @JsonProperty("routes")
    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    /**
     * 
     * @return
     *     The documents
     */
    @JsonProperty("documents")
    public List<Document> getDocuments() {
        return documents;
    }

    /**
     * 
     * @param documents
     *     The documents
     */
    @JsonProperty("documents")
    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    /**
     * 
     * @return
     *     The hideRouteInformation
     */
    @JsonProperty("hide_route_information")
    public boolean isHideRouteInformation() {
        return hideRouteInformation;
    }

    /**
     * 
     * @param hideRouteInformation
     *     The hide_route_information
     */
    @JsonProperty("hide_route_information")
    public void setHideRouteInformation(boolean hideRouteInformation) {
        this.hideRouteInformation = hideRouteInformation;
    }

    /**
     * 
     * @return
     *     The boundLat1
     */
    @JsonProperty("bound_lat1")
    public double getBoundLat1() {
        return boundLat1;
    }

    /**
     * 
     * @param boundLat1
     *     The bound_lat1
     */
    @JsonProperty("bound_lat1")
    public void setBoundLat1(double boundLat1) {
        this.boundLat1 = boundLat1;
    }

    /**
     * 
     * @return
     *     The boundLat2
     */
    @JsonProperty("bound_lat2")
    public double getBoundLat2() {
        return boundLat2;
    }

    /**
     * 
     * @param boundLat2
     *     The bound_lat2
     */
    @JsonProperty("bound_lat2")
    public void setBoundLat2(double boundLat2) {
        this.boundLat2 = boundLat2;
    }

    /**
     * 
     * @return
     *     The boundLng1
     */
    @JsonProperty("bound_lng1")
    public double getBoundLng1() {
        return boundLng1;
    }

    /**
     * 
     * @param boundLng1
     *     The bound_lng1
     */
    @JsonProperty("bound_lng1")
    public void setBoundLng1(double boundLng1) {
        this.boundLng1 = boundLng1;
    }

    /**
     * 
     * @return
     *     The boundLng2
     */
    @JsonProperty("bound_lng2")
    public double getBoundLng2() {
        return boundLng2;
    }

    /**
     * 
     * @param boundLng2
     *     The bound_lng2
     */
    @JsonProperty("bound_lng2")
    public void setBoundLng2(double boundLng2) {
        this.boundLng2 = boundLng2;
    }

    /**
     * 
     * @return
     *     The price
     */
    @JsonProperty("price")
    public String getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The price
     */
    @JsonProperty("price")
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * 
     * @return
     *     The priceIncludes
     */
    @JsonProperty("price_includes")
    public String getPriceIncludes() {
        return priceIncludes;
    }

    /**
     * 
     * @param priceIncludes
     *     The price_includes
     */
    @JsonProperty("price_includes")
    public void setPriceIncludes(String priceIncludes) {
        this.priceIncludes = priceIncludes;
    }

    /**
     * 
     * @return
     *     The priceExcludes
     */
    @JsonProperty("price_excludes")
    public String getPriceExcludes() {
        return priceExcludes;
    }

    /**
     * 
     * @param priceExcludes
     *     The price_excludes
     */
    @JsonProperty("price_excludes")
    public void setPriceExcludes(String priceExcludes) {
        this.priceExcludes = priceExcludes;
    }

    /**
     * 
     * @return
     *     The isDisabled
     */
    @JsonProperty("is_disabled")
    public boolean isIsDisabled() {
        return isDisabled;
    }

    /**
     * 
     * @param isDisabled
     *     The is_disabled
     */
    @JsonProperty("is_disabled")
    public void setIsDisabled(boolean isDisabled) {
        this.isDisabled = isDisabled;
    }

    /**
     * 
     * @return
     *     The travellersAdult
     */
    @JsonProperty("travellers_adult")
    public int getTravellersAdult() {
        return travellersAdult;
    }

    /**
     * 
     * @param travellersAdult
     *     The travellers_adult
     */
    @JsonProperty("travellers_adult")
    public void setTravellersAdult(int travellersAdult) {
        this.travellersAdult = travellersAdult;
    }

    /**
     * 
     * @return
     *     The travellersChildren
     */
    @JsonProperty("travellers_children")
    public int getTravellersChildren() {
        return travellersChildren;
    }

    /**
     * 
     * @param travellersChildren
     *     The travellers_children
     */
    @JsonProperty("travellers_children")
    public void setTravellersChildren(int travellersChildren) {
        this.travellersChildren = travellersChildren;
    }

    /**
     * 
     * @return
     *     The travellers
     */
    @JsonProperty("travellers")
    public List<Traveller> getTravellers() {
        return travellers;
    }

    /**
     * 
     * @param travellers
     *     The travellers
     */
    @JsonProperty("travellers")
    public void setTravellers(List<Traveller> travellers) {
        this.travellers = travellers;
    }

    /**
     * 
     * @return
     *     The roomsSingle
     */
    @JsonProperty("rooms_single")
    public int getRoomsSingle() {
        return roomsSingle;
    }

    /**
     * 
     * @param roomsSingle
     *     The rooms_single
     */
    @JsonProperty("rooms_single")
    public void setRoomsSingle(int roomsSingle) {
        this.roomsSingle = roomsSingle;
    }

    /**
     * 
     * @return
     *     The roomsDouble
     */
    @JsonProperty("rooms_double")
    public int getRoomsDouble() {
        return roomsDouble;
    }

    /**
     * 
     * @param roomsDouble
     *     The rooms_double
     */
    @JsonProperty("rooms_double")
    public void setRoomsDouble(int roomsDouble) {
        this.roomsDouble = roomsDouble;
    }

    /**
     * 
     * @return
     *     The roomsTwin
     */
    @JsonProperty("rooms_twin")
    public int getRoomsTwin() {
        return roomsTwin;
    }

    /**
     * 
     * @param roomsTwin
     *     The rooms_twin
     */
    @JsonProperty("rooms_twin")
    public void setRoomsTwin(int roomsTwin) {
        this.roomsTwin = roomsTwin;
    }

    /**
     * 
     * @return
     *     The roomsTriple
     */
    @JsonProperty("rooms_triple")
    public int getRoomsTriple() {
        return roomsTriple;
    }

    /**
     * 
     * @param roomsTriple
     *     The rooms_triple
     */
    @JsonProperty("rooms_triple")
    public void setRoomsTriple(int roomsTriple) {
        this.roomsTriple = roomsTriple;
    }

    /**
     * 
     * @return
     *     The roomsFamily
     */
    @JsonProperty("rooms_family")
    public int getRoomsFamily() {
        return roomsFamily;
    }

    /**
     * 
     * @param roomsFamily
     *     The rooms_family
     */
    @JsonProperty("rooms_family")
    public void setRoomsFamily(int roomsFamily) {
        this.roomsFamily = roomsFamily;
    }

    /**
     * 
     * @return
     *     The startTravelDays
     */
    @JsonProperty("start_travel_days")
    public int getStartTravelDays() {
        return startTravelDays;
    }

    /**
     * 
     * @param startTravelDays
     *     The start_travel_days
     */
    @JsonProperty("start_travel_days")
    public void setStartTravelDays(int startTravelDays) {
        this.startTravelDays = startTravelDays;
    }

    /**
     * 
     * @return
     *     The endTravelDays
     */
    @JsonProperty("end_travel_days")
    public int getEndTravelDays() {
        return endTravelDays;
    }

    /**
     * 
     * @param endTravelDays
     *     The end_travel_days
     */
    @JsonProperty("end_travel_days")
    public void setEndTravelDays(int endTravelDays) {
        this.endTravelDays = endTravelDays;
    }

    /**
     * 
     * @return
     *     The bookingStatus
     */
    @JsonProperty("booking_status")
    public String getBookingStatus() {
        return bookingStatus;
    }

    /**
     * 
     * @param bookingStatus
     *     The booking_status
     */
    @JsonProperty("booking_status")
    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    /**
     * 
     * @return
     *     The sourceOperatorId
     */
    @JsonProperty("source_operator_id")
    public int getSourceOperatorId() {
        return sourceOperatorId;
    }

    /**
     * 
     * @param sourceOperatorId
     *     The source_operator_id
     */
    @JsonProperty("source_operator_id")
    public void setSourceOperatorId(int sourceOperatorId) {
        this.sourceOperatorId = sourceOperatorId;
    }

    /**
     * 
     * @return
     *     The contacts
     */
    @JsonProperty("contacts")
    public List<Contact> getContacts() {
        return contacts;
    }

    /**
     * 
     * @param contacts
     *     The contacts
     */
    @JsonProperty("contacts")
    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    /**
     * 
     * @return
     *     The notificationFrequency
     */
    @JsonProperty("notification_frequency")
    public String getNotificationFrequency() {
        return notificationFrequency;
    }

    /**
     * 
     * @param notificationFrequency
     *     The notification_frequency
     */
    @JsonProperty("notification_frequency")
    public void setNotificationFrequency(String notificationFrequency) {
        this.notificationFrequency = notificationFrequency;
    }

    /**
     * 
     * @return
     *     The departureLocations
     */
    @JsonProperty("departure_locations")
    public String getDepartureLocations() {
        return departureLocations;
    }

    /**
     * 
     * @param departureLocations
     *     The departure_locations
     */
    @JsonProperty("departure_locations")
    public void setDepartureLocations(String departureLocations) {
        this.departureLocations = departureLocations;
    }

    /**
     * 
     * @return
     *     The departureTimes
     */
    @JsonProperty("departure_times")
    public String getDepartureTimes() {
        return departureTimes;
    }

    /**
     * 
     * @param departureTimes
     *     The departure_times
     */
    @JsonProperty("departure_times")
    public void setDepartureTimes(String departureTimes) {
        this.departureTimes = departureTimes;
    }

    /**
     * 
     * @return
     *     The returnLocations
     */
    @JsonProperty("return_locations")
    public String getReturnLocations() {
        return returnLocations;
    }

    /**
     * 
     * @param returnLocations
     *     The return_locations
     */
    @JsonProperty("return_locations")
    public void setReturnLocations(String returnLocations) {
        this.returnLocations = returnLocations;
    }

    /**
     * 
     * @return
     *     The returnTimes
     */
    @JsonProperty("return_times")
    public String getReturnTimes() {
        return returnTimes;
    }

    /**
     * 
     * @param returnTimes
     *     The return_times
     */
    @JsonProperty("return_times")
    public void setReturnTimes(String returnTimes) {
        this.returnTimes = returnTimes;
    }

    /**
     * 
     * @return
     *     The duration
     */
    @JsonProperty("duration")
    public String getDuration() {
        return duration;
    }

    /**
     * 
     * @param duration
     *     The duration
     */
    @JsonProperty("duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    /**
     * 
     * @return
     *     The additionalInfo
     */
    @JsonProperty("additional_info")
    public String getAdditionalInfo() {
        return additionalInfo;
    }

    /**
     * 
     * @param additionalInfo
     *     The additional_info
     */
    @JsonProperty("additional_info")
    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    /**
     * 
     * @return
     *     The spokenLanguages
     */
    @JsonProperty("spoken_languages")
    public String getSpokenLanguages() {
        return spokenLanguages;
    }

    /**
     * 
     * @param spokenLanguages
     *     The spoken_languages
     */
    @JsonProperty("spoken_languages")
    public void setSpokenLanguages(String spokenLanguages) {
        this.spokenLanguages = spokenLanguages;
    }

    /**
     * 
     * @return
     *     The groupSize
     */
    @JsonProperty("group_size")
    public String getGroupSize() {
        return groupSize;
    }

    /**
     * 
     * @param groupSize
     *     The group_size
     */
    @JsonProperty("group_size")
    public void setGroupSize(String groupSize) {
        this.groupSize = groupSize;
    }

    /**
     * 
     * @return
     *     The published
     */
    @JsonProperty("published")
    public boolean isPublished() {
        return published;
    }

    /**
     * 
     * @param published
     *     The published
     */
    @JsonProperty("published")
    public void setPublished(boolean published) {
        this.published = published;
    }

    /**
     * 
     * @return
     *     The routeHandlingMode
     */
    @JsonProperty("route_handling_mode")
    public String getRouteHandlingMode() {
        return routeHandlingMode;
    }

    /**
     * 
     * @param routeHandlingMode
     *     The route_handling_mode
     */
    @JsonProperty("route_handling_mode")
    public void setRouteHandlingMode(String routeHandlingMode) {
        this.routeHandlingMode = routeHandlingMode;
    }

    @JsonProperty("client_phone")
    public String getClientPhone() {
        return clientPhone;
    }

    @JsonProperty("client_phone")
    public void setClientPhone(String clientPhone) {
        this.clientPhone = clientPhone;
    }

    @JsonProperty("car_hire")
    public List<CarHire> getCarHire() {
        return carHire;
    }

    @JsonProperty("car_hire")
    public void setCarHire(List<CarHire> carHire) {
        this.carHire = carHire;
    }

    @JsonProperty("services")
    public String getServices() {
        return services;
    }

    @JsonProperty("services")
    public void setServices(String services) {
        this.services = services;
    }

    @JsonProperty("special_interests")
    public List<String> getSpecialInterests() {
        return specialInterests;
    }

    @JsonProperty("special_interests")
    public void setSpecialInterests(List<String> specialInterests) {
        this.specialInterests = specialInterests;
    }

    @JsonProperty("images")
    public List<Image> getImages() {
        return images;
    }

    @JsonProperty("images")
    public void setImages(List<Image> images) {
        this.images = images;
    }

    @JsonProperty("departures")
    public String getDepartures() {
        return departures;
    }

    @JsonProperty("departures")
    public void setDepartures(String departures) {
        this.departures = departures;
    }

    @JsonProperty("returns")
    public String getReturns() {
        return returns;
    }

    @JsonProperty("returns")
    public void setReturns(String returns) {
        this.returns = returns;
    }

    @JsonProperty("travel_arrangements")
    public List<TravelArrangement> getTravelArrangements() {
        return travelArrangements;
    }

    @JsonProperty("travel_arrangements")
    public void setTravelArrangements(List<TravelArrangement> travelArrangements) {
        this.travelArrangements = travelArrangements;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(operatorId).append(operatorUserId).append(operatorIdentityId).append(operatorThemeId).append(type).append(identifier).append(identifierKey).append(days).append(name).append(language).append(referenceNumber).append(description).append(categories).append(clientName).append(clientEmail).append(startDate).append(legs).append(routes).append(documents).append(hideRouteInformation).append(boundLat1).append(boundLat2).append(boundLng1).append(boundLng2).append(price).append(priceIncludes).append(priceExcludes).append(isDisabled).append(travellersAdult).append(travellersChildren).append(travellers).append(roomsSingle).append(roomsDouble).append(roomsTwin).append(roomsTriple).append(roomsFamily).append(startTravelDays).append(endTravelDays).append(bookingStatus).append(sourceOperatorId).append(contacts).append(notificationFrequency).append(departureLocations).append(departureTimes).append(returnLocations).append(returnTimes).append(duration).append(additionalInfo).append(spokenLanguages).append(groupSize).append(published).append(routeHandlingMode).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ItineraryDetail) == false) {
            return false;
        }
        ItineraryDetail rhs = ((ItineraryDetail) other);
        return new EqualsBuilder().append(operatorId, rhs.operatorId).append(operatorUserId, rhs.operatorUserId).append(operatorIdentityId, rhs.operatorIdentityId).append(operatorThemeId, rhs.operatorThemeId).append(type, rhs.type).append(identifier, rhs.identifier).append(identifierKey, rhs.identifierKey).append(days, rhs.days).append(name, rhs.name).append(language, rhs.language).append(referenceNumber, rhs.referenceNumber).append(description, rhs.description).append(categories, rhs.categories).append(clientName, rhs.clientName).append(clientEmail, rhs.clientEmail).append(startDate, rhs.startDate).append(legs, rhs.legs).append(routes, rhs.routes).append(documents, rhs.documents).append(hideRouteInformation, rhs.hideRouteInformation).append(boundLat1, rhs.boundLat1).append(boundLat2, rhs.boundLat2).append(boundLng1, rhs.boundLng1).append(boundLng2, rhs.boundLng2).append(price, rhs.price).append(priceIncludes, rhs.priceIncludes).append(priceExcludes, rhs.priceExcludes).append(isDisabled, rhs.isDisabled).append(travellersAdult, rhs.travellersAdult).append(travellersChildren, rhs.travellersChildren).append(travellers, rhs.travellers).append(roomsSingle, rhs.roomsSingle).append(roomsDouble, rhs.roomsDouble).append(roomsTwin, rhs.roomsTwin).append(roomsTriple, rhs.roomsTriple).append(roomsFamily, rhs.roomsFamily).append(startTravelDays, rhs.startTravelDays).append(endTravelDays, rhs.endTravelDays).append(bookingStatus, rhs.bookingStatus).append(sourceOperatorId, rhs.sourceOperatorId).append(contacts, rhs.contacts).append(notificationFrequency, rhs.notificationFrequency).append(departureLocations, rhs.departureLocations).append(departureTimes, rhs.departureTimes).append(returnLocations, rhs.returnLocations).append(returnTimes, rhs.returnTimes).append(duration, rhs.duration).append(additionalInfo, rhs.additionalInfo).append(spokenLanguages, rhs.spokenLanguages).append(groupSize, rhs.groupSize).append(published, rhs.published).append(routeHandlingMode, rhs.routeHandlingMode).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
