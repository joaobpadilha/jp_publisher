package com.umapped.external.wetu.itinerary.details;

/**
 * Created by george on 2017-06-05.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "content_entity_id",
    "booking_reference",
    "rooms"
})
public class DayRoom {
  @JsonProperty("content_entity_id")
  private int contentEntityId;
  @JsonProperty("booking_reference")
  private String bookingReference;
  @JsonProperty("rooms")
  private List<Room> rooms = new ArrayList<Room>();
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<String, Object>();

  @JsonProperty("content_entity_id")
  public int getContentEntityId() {
    return contentEntityId;
  }

  @JsonProperty("content_entity_id")
  public void setContentEntityId(int contentEntityId) {
    this.contentEntityId = contentEntityId;
  }

  @JsonProperty("booking_reference")
  public String getBookingReference() {
    return bookingReference;
  }

  @JsonProperty("booking_reference")
  public void setBookingReference(String bookingReference) {
    this.bookingReference = bookingReference;
  }

  @JsonProperty("rooms")
  public List<Room> getRooms() {
    return rooms;
  }

  @JsonProperty("rooms")
  public void setRooms(List<Room> rooms) {
    this.rooms = rooms;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(contentEntityId).append(bookingReference).append(rooms).append(additionalProperties).toHashCode();
  }

  @Override
  public boolean equals(Object other) {
    if (other == this) {
      return true;
    }
    if ((other instanceof DayRoom) == false) {
      return false;
    }
    DayRoom rhs = ((DayRoom) other);
    return new EqualsBuilder().append(contentEntityId, rhs.contentEntityId).append(bookingReference, rhs.bookingReference).append(rooms, rhs.rooms).append(additionalProperties, rhs.additionalProperties).isEquals();
  }

}
