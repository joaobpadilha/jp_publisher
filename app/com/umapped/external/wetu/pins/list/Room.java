
package com.umapped.external.wetu.pins.list;

import java.util.ArrayList;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "per_person_sharing_rates",
    "single_rates",
    "room_rates",
    "id",
    "name",
    "description",
    "images"
})
public class Room {

    @JsonProperty("per_person_sharing_rates")
    private ArrayList<ItemRate> perPersonSharingRates = new ArrayList<ItemRate>();
    @JsonProperty("single_rates")
    private ArrayList<ItemRate> singleRates = new ArrayList<ItemRate>();
    @JsonProperty("room_rates")
    private ArrayList<ItemRate> roomRates = new ArrayList<ItemRate>();
    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("images")
    private ArrayList<Image> images = new ArrayList<Image>();

    private ArrayList<String> amenities;
    private String affiliateCode;
    
    

    /**
     * 
     * @return
     *     The perPersonSharingRates
     */
    @JsonProperty("per_person_sharing_rates")
    public ArrayList<ItemRate> getPerPersonSharingRates() {
        return perPersonSharingRates;
    }

    /**
     * 
     * @param perPersonSharingRates
     *     The per_person_sharing_rates
     */
    @JsonProperty("per_person_sharing_rates")
    public void setPerPersonSharingRates(ArrayList<ItemRate> perPersonSharingRates) {
        this.perPersonSharingRates = perPersonSharingRates;
    }

    /**
     * 
     * @return
     *     The singleRates
     */
    @JsonProperty("single_rates")
    public ArrayList<ItemRate> getSingleRates() {
        return singleRates;
    }

    /**
     * 
     * @param singleRates
     *     The single_rates
     */
    @JsonProperty("single_rates")
    public void setSingleRates(ArrayList<ItemRate> singleRates) {
        this.singleRates = singleRates;
    }

    /**
     * 
     * @return
     *     The roomRates
     */
    @JsonProperty("room_rates")
    public ArrayList<ItemRate> getRoomRates() {
        return roomRates;
    }

    /**
     * 
     * @param roomRates
     *     The room_rates
     */
    @JsonProperty("room_rates")
    public void setRoomRates(ArrayList<ItemRate> roomRates) {
        this.roomRates = roomRates;
    }

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The images
     */
    @JsonProperty("images")
    public ArrayList<Image> getImages() {
        return images;
    }

    /**
     * 
     * @param images
     *     The images
     */
    @JsonProperty("images")
    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

    public String getAffiliateCode() {
        return affiliateCode;
    }

    public void setAffiliateCode(String affiliateCode) {
        this.affiliateCode = affiliateCode;
    }

    public ArrayList<String> getAmenities() {
        return amenities;
    }

    public void setAmenities(ArrayList<String> amenities) {
        this.amenities = amenities;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(perPersonSharingRates).append(singleRates).append(roomRates).append(id).append(name).append(description).append(images).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Room) == false) {
            return false;
        }
        Room rhs = ((Room) other);
        return new EqualsBuilder().append(perPersonSharingRates, rhs.perPersonSharingRates).append(singleRates, rhs.singleRates).append(roomRates, rhs.roomRates).append(id, rhs.id).append(name, rhs.name).append(description, rhs.description).append(images, rhs.images).isEquals();
    }

}
