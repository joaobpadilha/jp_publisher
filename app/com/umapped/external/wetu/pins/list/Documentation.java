package com.umapped.external.wetu.pins.list;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by george on 2016-06-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class Documentation {
  public String section;
  public String label;
  public String url;

  public String getSection() {
    return section;
  }

  public void setSection(String section) {
    this.section = section;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
