
package com.umapped.external.wetu.pins.list;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "email",
    "telephone",
    "website_url",
    "bookings_url",
    "address",
    "front_desk_telephone"
})
public class ContactInformation {

    @JsonProperty("email")
    private String email;
    @JsonProperty("telephone")
    private String telephone;
    @JsonProperty("website_url")
    private String websiteUrl;
    @JsonProperty("bookings_url")
    private String bookingsUrl;
    @JsonProperty("address")
    private String address;
    @JsonProperty("front_desk_telephone")
    private String frontDeskTelephone;

    private String skype;
    private String facebook;
    private String twitter;
    private String mobileBookingsUrl;


    /**
     * 
     * @return
     *     The email
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The telephone
     */
    @JsonProperty("telephone")
    public String getTelephone() {
        return telephone;
    }

    /**
     * 
     * @param telephone
     *     The telephone
     */
    @JsonProperty("telephone")
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * 
     * @return
     *     The websiteUrl
     */
    @JsonProperty("website_url")
    public String getWebsiteUrl() {
        return websiteUrl;
    }

    /**
     * 
     * @param websiteUrl
     *     The website_url
     */
    @JsonProperty("website_url")
    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    /**
     * 
     * @return
     *     The bookingsUrl
     */
    @JsonProperty("bookings_url")
    public String getBookingsUrl() {
        return bookingsUrl;
    }

    /**
     * 
     * @param bookingsUrl
     *     The bookings_url
     */
    @JsonProperty("bookings_url")
    public void setBookingsUrl(String bookingsUrl) {
        this.bookingsUrl = bookingsUrl;
    }

    /**
     * 
     * @return
     *     The address
     */
    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    /**
     * 
     * @param address
     *     The address
     */
    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 
     * @return
     *     The frontDeskTelephone
     */
    @JsonProperty("front_desk_telephone")
    public String getFrontDeskTelephone() {
        return frontDeskTelephone;
    }

    /**
     * 
     * @param frontDeskTelephone
     *     The front_desk_telephone
     */
    @JsonProperty("front_desk_telephone")
    public void setFrontDeskTelephone(String frontDeskTelephone) {
        this.frontDeskTelephone = frontDeskTelephone;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getMobileBookingsUrl() {
        return mobileBookingsUrl;
    }

    public void setMobileBookingsUrl(String mobileBookingsUrl) {
        this.mobileBookingsUrl = mobileBookingsUrl;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(email).append(telephone).append(websiteUrl).append(bookingsUrl).append(address).append(frontDeskTelephone).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ContactInformation) == false) {
            return false;
        }
        ContactInformation rhs = ((ContactInformation) other);
        return new EqualsBuilder().append(email, rhs.email).append(telephone, rhs.telephone).append(websiteUrl, rhs.websiteUrl).append(bookingsUrl, rhs.bookingsUrl).append(address, rhs.address).append(frontDeskTelephone, rhs.frontDeskTelephone).isEquals();
    }

}
