package com.umapped.external.wetu.pins.list;

import java.util.ArrayList;

/**
 * Created by george on 2016-06-07.
 */
public class Features_ConferenceVenue {
  public String starAuthority;
  public int stars;
  public String rating;
  public int rooms;
  public int paxAccomodationOnSite;
  public int paxAccomodationOffSite;
  public ArrayList<String> spokenLanguages;
  public ArrayList<String> conferenceFacilities;
  public ArrayList<String> conferenceEquipment;
  public ArrayList<String> propertyFacilities;
  public ArrayList<String> roomFacilities;
  public ArrayList<String> availableServices;
  public ArrayList<String> activitiesOnSite;
  public ArrayList<String> activitiesOffSite;

  public String getStarAuthority() {
    return starAuthority;
  }

  public void setStarAuthority(String starAuthority) {
    this.starAuthority = starAuthority;
  }

  public int getStars() {
    return stars;
  }

  public void setStars(int stars) {
    this.stars = stars;
  }

  public String getRating() {
    return rating;
  }

  public void setRating(String rating) {
    this.rating = rating;
  }

  public int getRooms() {
    return rooms;
  }

  public void setRooms(int rooms) {
    this.rooms = rooms;
  }

  public int getPaxAccomodationOnSite() {
    return paxAccomodationOnSite;
  }

  public void setPaxAccomodationOnSite(int paxAccomodationOnSite) {
    this.paxAccomodationOnSite = paxAccomodationOnSite;
  }

  public int getPaxAccomodationOffSite() {
    return paxAccomodationOffSite;
  }

  public void setPaxAccomodationOffSite(int paxAccomodationOffSite) {
    this.paxAccomodationOffSite = paxAccomodationOffSite;
  }

  public ArrayList<String> getSpokenLanguages() {
    return spokenLanguages;
  }

  public void setSpokenLanguages(ArrayList<String> spokenLanguages) {
    this.spokenLanguages = spokenLanguages;
  }

  public ArrayList<String> getConferenceFacilities() {
    return conferenceFacilities;
  }

  public void setConferenceFacilities(ArrayList<String> conferenceFacilities) {
    this.conferenceFacilities = conferenceFacilities;
  }

  public ArrayList<String> getConferenceEquipment() {
    return conferenceEquipment;
  }

  public void setConferenceEquipment(ArrayList<String> conferenceEquipment) {
    this.conferenceEquipment = conferenceEquipment;
  }

  public ArrayList<String> getPropertyFacilities() {
    return propertyFacilities;
  }

  public void setPropertyFacilities(ArrayList<String> propertyFacilities) {
    this.propertyFacilities = propertyFacilities;
  }

  public ArrayList<String> getRoomFacilities() {
    return roomFacilities;
  }

  public void setRoomFacilities(ArrayList<String> roomFacilities) {
    this.roomFacilities = roomFacilities;
  }

  public ArrayList<String> getAvailableServices() {
    return availableServices;
  }

  public void setAvailableServices(ArrayList<String> availableServices) {
    this.availableServices = availableServices;
  }

  public ArrayList<String> getActivitiesOnSite() {
    return activitiesOnSite;
  }

  public void setActivitiesOnSite(ArrayList<String> activitiesOnSite) {
    this.activitiesOnSite = activitiesOnSite;
  }

  public ArrayList<String> getActivitiesOffSite() {
    return activitiesOffSite;
  }

  public void setActivitiesOffSite(ArrayList<String> activitiesOffSite) {
    this.activitiesOffSite = activitiesOffSite;
  }


  @Override
  public String toString() {
    return "Features_ConferenceVenue{" +
        "starAuthority='" + starAuthority + '\'' +
        ", stars=" + stars +
        ", rating='" + rating + '\'' +
        ", rooms=" + rooms +
        ", paxAccomodationOnSite=" + paxAccomodationOnSite +
        ", paxAccomodationOffSite=" + paxAccomodationOffSite +
        ", spokenLanguages=" + spokenLanguages +
        ", conferenceFacilities=" + conferenceFacilities +
        ", conferenceEquipment=" + conferenceEquipment +
        ", propertyFacilities=" + propertyFacilities +
        ", roomFacilities=" + roomFacilities +
        ", availableServices=" + availableServices +
        ", activitiesOnSite=" + activitiesOnSite +
        ", activitiesOffSite=" + activitiesOffSite +
        '}';
  }
}
