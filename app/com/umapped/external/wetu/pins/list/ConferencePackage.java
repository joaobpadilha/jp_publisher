package com.umapped.external.wetu.pins.list;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by george on 2016-06-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class ConferencePackage extends SimpleItem {
  public ArrayList<ItemRate> rates;

  public ArrayList<ItemRate> getRates() {
    return rates;
  }

  public void setRates(ArrayList<ItemRate> rates) {
    this.rates = rates;
  }
}
