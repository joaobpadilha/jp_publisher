package com.umapped.external.wetu.pins.list;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by george on 2016-06-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class ConferenceVenueRoom extends SimpleItem {
  public int capacityReception;
  public int capacityTheatre;
  public int capacityClassroom;
  public int capacityBanquet;
  public int capacityBanquetDanceFloor;
  public int capacityBoardroom;
  public int capacityUShaped;
  public double entranceHeight;
  public double entranceWidth;
  public double dimensionsLength;
  public double dimensionsWidth;
  public ArrayList<ItemRate> venueHireDates;


  public int getCapacityReception() {
    return capacityReception;
  }

  public void setCapacityReception(int capacityReception) {
    this.capacityReception = capacityReception;
  }

  public int getCapacityTheatre() {
    return capacityTheatre;
  }

  public void setCapacityTheatre(int capacityTheatre) {
    this.capacityTheatre = capacityTheatre;
  }

  public int getCapacityClassroom() {
    return capacityClassroom;
  }

  public void setCapacityClassroom(int capacityClassroom) {
    this.capacityClassroom = capacityClassroom;
  }

  public int getCapacityBanquet() {
    return capacityBanquet;
  }

  public void setCapacityBanquet(int capacityBanquet) {
    this.capacityBanquet = capacityBanquet;
  }

  public int getCapacityBanquetDanceFloor() {
    return capacityBanquetDanceFloor;
  }

  public void setCapacityBanquetDanceFloor(int capacityBanquetDanceFloor) {
    this.capacityBanquetDanceFloor = capacityBanquetDanceFloor;
  }

  public int getCapacityBoardroom() {
    return capacityBoardroom;
  }

  public void setCapacityBoardroom(int capacityBoardroom) {
    this.capacityBoardroom = capacityBoardroom;
  }

  public int getCapacityUShaped() {
    return capacityUShaped;
  }

  public void setCapacityUShaped(int capacityUShaped) {
    this.capacityUShaped = capacityUShaped;
  }

  public double getEntranceHeight() {
    return entranceHeight;
  }

  public void setEntranceHeight(double entranceHeight) {
    this.entranceHeight = entranceHeight;
  }

  public double getEntranceWidth() {
    return entranceWidth;
  }

  public void setEntranceWidth(double entranceWidth) {
    this.entranceWidth = entranceWidth;
  }

  public double getDimensionsLength() {
    return dimensionsLength;
  }

  public void setDimensionsLength(double dimensionsLength) {
    this.dimensionsLength = dimensionsLength;
  }

  public double getDimensionsWidth() {
    return dimensionsWidth;
  }

  public void setDimensionsWidth(double dimensionsWidth) {
    this.dimensionsWidth = dimensionsWidth;
  }

  public ArrayList<ItemRate> getVenueHireDates() {
    return venueHireDates;
  }

  public void setVenueHireDates(ArrayList<ItemRate> venueHireDates) {
    this.venueHireDates = venueHireDates;
  }
}