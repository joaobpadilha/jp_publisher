package com.umapped.external.wetu.pins.list;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by george on 2016-06-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class Features_Person {
  public ArrayList<String> languages;
  public ArrayList<String> skills;
  public ArrayList<String> Regions;
  public ArrayList<String> specialInterest;

  public ArrayList<String> getLanguages() {
    return languages;
  }

  public void setLanguages(ArrayList<String> languages) {
    this.languages = languages;
  }

  public ArrayList<String> getSkills() {
    return skills;
  }

  public void setSkills(ArrayList<String> skills) {
    this.skills = skills;
  }

  public ArrayList<String> getRegions() {
    return Regions;
  }

  public void setRegions(ArrayList<String> regions) {
    Regions = regions;
  }

  public ArrayList<String> getSpecialInterest() {
    return specialInterest;
  }

  public void setSpecialInterest(ArrayList<String> specialInterest) {
    this.specialInterest = specialInterest;
  }
}
