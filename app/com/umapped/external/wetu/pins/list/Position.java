
package com.umapped.external.wetu.pins.list;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "latitude",
    "longitude",
    "driving_latitude",
    "driving_longitude",
    "country",
    "region",
    "area",
    "destination",
    "destination_content_entity_id",
    "country_content_entity_id"
})
public class Position {

    @JsonProperty("latitude")
    private double latitude;
    @JsonProperty("longitude")
    private double longitude;
    @JsonProperty("driving_latitude")
    private double drivingLatitude;
    @JsonProperty("driving_longitude")
    private double drivingLongitude;
    @JsonProperty("country")
    private String country;
    @JsonProperty("region")
    private String region;
    @JsonProperty("area")
    private String area;
    @JsonProperty("destination")
    private String destination;
    @JsonProperty("destination_content_entity_id")
    private int destinationContentId;
    @JsonProperty("country_content_entity_id")
    private int countryContentId;

    private String location;

    /**
     * 
     * @return
     *     The latitude
     */
    @JsonProperty("latitude")
    public double getLatitude() {
        return latitude;
    }

    /**
     * 
     * @param latitude
     *     The latitude
     */
    @JsonProperty("latitude")
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * 
     * @return
     *     The longitude
     */
    @JsonProperty("longitude")
    public double getLongitude() {
        return longitude;
    }

    /**
     * 
     * @param longitude
     *     The longitude
     */
    @JsonProperty("longitude")
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * 
     * @return
     *     The drivingLatitude
     */
    @JsonProperty("driving_latitude")
    public double getDrivingLatitude() {
        return drivingLatitude;
    }

    /**
     * 
     * @param drivingLatitude
     *     The driving_latitude
     */
    @JsonProperty("driving_latitude")
    public void setDrivingLatitude(double drivingLatitude) {
        this.drivingLatitude = drivingLatitude;
    }

    /**
     * 
     * @return
     *     The drivingLongitude
     */
    @JsonProperty("driving_longitude")
    public double getDrivingLongitude() {
        return drivingLongitude;
    }

    /**
     * 
     * @param drivingLongitude
     *     The driving_longitude
     */
    @JsonProperty("driving_longitude")
    public void setDrivingLongitude(double drivingLongitude) {
        this.drivingLongitude = drivingLongitude;
    }

    /**
     * 
     * @return
     *     The country
     */
    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    /**
     * 
     * @param country
     *     The country
     */
    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * 
     * @return
     *     The region
     */
    @JsonProperty("region")
    public String getRegion() {
        return region;
    }

    /**
     * 
     * @param region
     *     The region
     */
    @JsonProperty("region")
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * 
     * @return
     *     The area
     */
    @JsonProperty("area")
    public String getArea() {
        return area;
    }

    /**
     * 
     * @param area
     *     The area
     */
    @JsonProperty("area")
    public void setArea(String area) {
        this.area = area;
    }

    /**
     * 
     * @return
     *     The destination
     */
    @JsonProperty("destination")
    public String getDestination() {
        return destination;
    }

    /**
     * 
     * @param destination
     *     The destination
     */
    @JsonProperty("destination")
    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


    @JsonProperty("destination_content_entity_id")
    public int getDestinationContentId () { return destinationContentId;}
    public void setDestinationContentId (int destinationContentId) { this.destinationContentId = destinationContentId; }

    @JsonProperty("country_content_entity_id")
    public int getCountryContentId () { return countryContentId;}
    public void setCountryContentId (int countryContentId) { this.countryContentId = countryContentId;}

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(latitude).append(longitude).append(drivingLatitude).append(drivingLongitude).append(country).append(region).append(area).append(destination).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Position) == false) {
            return false;
        }
        Position rhs = ((Position) other);
        return new EqualsBuilder().append(latitude, rhs.latitude).append(longitude, rhs.longitude).append(drivingLatitude, rhs.drivingLatitude).append(drivingLongitude, rhs.drivingLongitude).append(country, rhs.country).append(region, rhs.region).append(area, rhs.area).append(destination, rhs.destination).isEquals();
    }

}
