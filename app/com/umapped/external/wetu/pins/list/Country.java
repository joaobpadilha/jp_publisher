package com.umapped.external.wetu.pins.list;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by george on 2016-06-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class Country extends Destination {
  @JsonProperty("travel_information")
  private TravelInformation travelInformation;

  @JsonProperty("travel_information")
  public TravelInformation getTravelInformation() {
    return travelInformation;
  }

  @JsonProperty("travel_information")
  public void setTravelInformation(TravelInformation travelInformation) {
    this.travelInformation = travelInformation;
  }
}
