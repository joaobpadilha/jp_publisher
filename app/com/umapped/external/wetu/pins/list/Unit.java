package com.umapped.external.wetu.pins.list;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by george on 2016-06-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class Unit extends SimpleItem {
  public ArrayList<ItemRate> unitRates;
  public ArrayList<String> amenities;
  public String affiliateCode;

  public ArrayList<ItemRate> getUnitRates() {
    return unitRates;
  }

  public void setUnitRates(ArrayList<ItemRate> unitRates) {
    this.unitRates = unitRates;
  }

  public ArrayList<String> getAmenities() {
    return amenities;
  }

  public void setAmenities(ArrayList<String> amenities) {
    this.amenities = amenities;
  }

  public String getAffiliateCode() {
    return affiliateCode;
  }

  public void setAffiliateCode(String affiliateCode) {
    this.affiliateCode = affiliateCode;
  }
}
