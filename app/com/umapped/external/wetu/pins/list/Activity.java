package com.umapped.external.wetu.pins.list;

/**
 * Created by george on 2016-06-07.
 */

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "features",
    "options",
    "map_object_id",
    "name",
    "last_modified",
    "type",
    "category",
    "position",
    "content",
    "documentation",
    "specials"
})
public class Activity extends Pin {
  @JsonProperty("features")
  private Features_Activity features;
  @JsonProperty("options")
  private List<Option> options = new ArrayList<Option>();

  @JsonProperty("features")
  public Features_Activity getFeatures() {
    return features;
  }

  @JsonProperty("features")
  public void setFeatures(Features_Activity features) {
    this.features = features;
  }

  @JsonProperty("options")
  public List<Option> getOptions() {
    return options;
  }

  @JsonProperty("options")
  public void setOptions(List<Option> options) {
    this.options = options;
  }
}
