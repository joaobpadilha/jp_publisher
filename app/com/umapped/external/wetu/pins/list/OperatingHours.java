package com.umapped.external.wetu.pins.list;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by george on 2016-06-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class OperatingHours {
  public String mondayOpen;
  public String mondayClose;
  public String tuesdayOpen;
  public String tuesdayClose;
  public String wednesdayOpen;
  public String wednesdayClose;
  public String thursdayOpen;
  public String thursdayClose;
  public String fridayOpen;
  public String fridayClose;
  public String saturdayOpen;
  public String saturdayClose;
  public String sundayOpen;
  public String sundayClose;
  public boolean openPublicHolidays;
  public boolean byAppointment;

  public String getMondayClose() {
    return mondayClose;
  }

  public void setMondayClose(String mondayClose) {
    this.mondayClose = mondayClose;
  }

  public String getTuesdayOpen() {
    return tuesdayOpen;
  }

  public void setTuesdayOpen(String tuesdayOpen) {
    this.tuesdayOpen = tuesdayOpen;
  }

  public String getTuesdayClose() {
    return tuesdayClose;
  }

  public void setTuesdayClose(String tuesdayClose) {
    this.tuesdayClose = tuesdayClose;
  }

  public String getWednesdayOpen() {
    return wednesdayOpen;
  }

  public void setWednesdayOpen(String wednesdayOpen) {
    this.wednesdayOpen = wednesdayOpen;
  }

  public String getWednesdayClose() {
    return wednesdayClose;
  }

  public void setWednesdayClose(String wednesdayClose) {
    this.wednesdayClose = wednesdayClose;
  }

  public String getThursdayOpen() {
    return thursdayOpen;
  }

  public void setThursdayOpen(String thursdayOpen) {
    this.thursdayOpen = thursdayOpen;
  }

  public String getThursdayClose() {
    return thursdayClose;
  }

  public void setThursdayClose(String thursdayClose) {
    this.thursdayClose = thursdayClose;
  }

  public String getFridayOpen() {
    return fridayOpen;
  }

  public void setFridayOpen(String fridayOpen) {
    this.fridayOpen = fridayOpen;
  }

  public String getFridayClose() {
    return fridayClose;
  }

  public void setFridayClose(String fridayClose) {
    this.fridayClose = fridayClose;
  }

  public String getSaturdayOpen() {
    return saturdayOpen;
  }

  public void setSaturdayOpen(String saturdayOpen) {
    this.saturdayOpen = saturdayOpen;
  }

  public String getSaturdayClose() {
    return saturdayClose;
  }

  public void setSaturdayClose(String saturdayClose) {
    this.saturdayClose = saturdayClose;
  }

  public String getSundayOpen() {
    return sundayOpen;
  }

  public void setSundayOpen(String sundayOpen) {
    this.sundayOpen = sundayOpen;
  }

  public String getSundayClose() {
    return sundayClose;
  }

  public void setSundayClose(String sundayClose) {
    this.sundayClose = sundayClose;
  }

  public boolean isOpenPublicHolidays() {
    return openPublicHolidays;
  }

  public void setOpenPublicHolidays(boolean openPublicHolidays) {
    this.openPublicHolidays = openPublicHolidays;
  }

  public boolean isByAppointment() {
    return byAppointment;
  }

  public void setByAppointment(boolean byAppointment) {
    this.byAppointment = byAppointment;
  }

  public String getMondayOpen() {
    return mondayOpen;
  }

  public void setMondayOpen(String mondayOpen) {
    this.mondayOpen = mondayOpen;
  }
}
