package com.umapped.external.wetu.pins.list;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by george on 2016-06-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class ItemRate {
  public String start;
  public String end;
  public String type;
  public String currency;
  public String rate;
  public String conditions;
  public String included;

  public String getStart() {
    return start;
  }

  public void setStart(String start) {
    this.start = start;
  }

  public String getEnd() {
    return end;
  }

  public void setEnd(String end) {
    this.end = end;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getRate() {
    return rate;
  }

  public void setRate(String rate) {
    this.rate = rate;
  }

  public String getConditions() {
    return conditions;
  }

  public void setConditions(String conditions) {
    this.conditions = conditions;
  }

  public String getIncluded() {
    return included;
  }

  public void setIncluded(String included) {
    this.included = included;
  }
}
