package com.umapped.external.virtuoso.agents;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;

public class Agents {

  public static void main (String[] args) {
    try {
      int i = 0;
      FileWriter fw = new FileWriter("/home/twong/temp/virtuoso/virtuoso_agent" + i + ".csv");

      String line = "\"Agent\", \"Agency\", \"Phone\", \"Email\"";
      fw.write(line);
      fw.write("\n");
      fw.flush();



      String s = FileUtils.readFileToString(new File("/home/twong/temp/virtuoso/virtuoso_agent" + i + ".html"));
      Document doc = Jsoup.parse(s);
      Elements adivsors = doc.select("section");
      for (Element a: adivsors) {
        String phone = "";
        String email = "";
        String name = "";
        String company = "";
        String id = "";
        Elements card = a.select(".js-contact-btn");
        Elements cmpy = a.select(".catalog-card--details");
        if (cmpy != null && cmpy.size() > 0) {
          company = cmpy.get(0).text();
        }
        if (card != null && card.size() > 0) {
          //get data attributes
          phone = card.get(0).attr("data-phone");
          id = card.get(0).attr("data-id").trim();
          name = card.get(0).attr("data-name");

        }
        if (id != null && id.trim().length() > 0) {
          long l = 1518714473621l;
          CookieStore httpCookieStore = new BasicCookieStore();
          HttpClient hc = HttpClientBuilder.create().setDefaultCookieStore(httpCookieStore).build();
          HttpGet g1 = new HttpGet("https://www.virtuoso.com");
          hc.execute(g1);
          HttpGet g = new HttpGet("https://www.virtuoso.com/advisors");
          hc.execute(g);

          String emailUrl = "https://www.virtuoso.com/advisors/ajax/getemail?advisorMeid="+ id + "&_=" + l++;
          HttpGet p = new HttpGet(emailUrl);
          p.setHeader("accept-language", "en-US,en-GB;q=0.9,en;q=0.8");
          p.setHeader("accept-encoding", " gzip, deflate, br");
          p.setHeader("'accept","text/plain, */*; q=0.01");
          p.setHeader("Cache-Control", "no-cache");
          p.setHeader("X-Requested-With", "XMLHttpRequest");
          p.setHeader("Content-Type", "application/json; charset=utf-8");
          p.setHeader("referer", "https://www.virtuoso.com/advisors");
          p.setHeader("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/64.0.3282.140 Chrome/64.0.3282.140 Safari/537.36");
          p.setHeader("authority", "www.virtuoso.com");
          HttpResponse r = hc.execute(p);
          email = parseResponse(r);

          System.out.println(r.getStatusLine() + " = " + email);

          Thread.sleep(1000);
        }

        if (name != null && name.trim().length() > 0) {
          String out = "\"" + name + "\"," + "\"" + company + "\"," + "\"" + phone + "\"," + "\"" + email + "\"," + "\"" + id + "\"";
          fw.write(out);
          fw.write("\n");
          fw.flush();
          System.out.println(out);
        }
      }
      fw.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static String parseResponse (HttpResponse response) throws Exception{
    BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

    StringBuffer result = new StringBuffer();
    String line = "";
    while ((line = rd.readLine()) != null) {
      result.append(line);
    }
    return result.toString();
  }
}
