
package com.umapped.external.bing.web;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "answerType",
    "resultIndex",
    "value"
})
public class Item {

    @JsonProperty("answerType")
    private String answerType;
    @JsonProperty("resultIndex")
    private int resultIndex;
    @JsonProperty("value")
    private Value value;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The answerType
     */
    @JsonProperty("answerType")
    public String getAnswerType() {
        return answerType;
    }

    /**
     * 
     * @param answerType
     *     The answerType
     */
    @JsonProperty("answerType")
    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    /**
     * 
     * @return
     *     The resultIndex
     */
    @JsonProperty("resultIndex")
    public int getResultIndex() {
        return resultIndex;
    }

    /**
     * 
     * @param resultIndex
     *     The resultIndex
     */
    @JsonProperty("resultIndex")
    public void setResultIndex(int resultIndex) {
        this.resultIndex = resultIndex;
    }

    /**
     * 
     * @return
     *     The value
     */
    @JsonProperty("value")
    public Value getValue() {
        return value;
    }

    /**
     * 
     * @param value
     *     The value
     */
    @JsonProperty("value")
    public void setValue(Value value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(answerType).append(resultIndex).append(value).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Item) == false) {
            return false;
        }
        Item rhs = ((Item) other);
        return new EqualsBuilder().append(answerType, rhs.answerType).append(resultIndex, rhs.resultIndex).append(value, rhs.value).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
