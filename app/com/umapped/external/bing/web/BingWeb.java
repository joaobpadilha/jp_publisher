
package com.umapped.external.bing.web;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "_type",
    "webPages",
    "images",
    "relatedSearches",
    "rankingResponse"
})
public class BingWeb {

    @JsonProperty("_type")
    private String type;
    @JsonProperty("webPages")
    private WebPages webPages;
    @JsonProperty("images")
    private Images images;
    @JsonProperty("relatedSearches")
    private RelatedSearches relatedSearches;
    @JsonProperty("rankingResponse")
    private RankingResponse rankingResponse;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The type
     */
    @JsonProperty("_type")
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The _type
     */
    @JsonProperty("_type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The webPages
     */
    @JsonProperty("webPages")
    public WebPages getWebPages() {
        return webPages;
    }

    /**
     * 
     * @param webPages
     *     The webPages
     */
    @JsonProperty("webPages")
    public void setWebPages(WebPages webPages) {
        this.webPages = webPages;
    }

    /**
     * 
     * @return
     *     The images
     */
    @JsonProperty("images")
    public Images getImages() {
        return images;
    }

    /**
     * 
     * @param images
     *     The images
     */
    @JsonProperty("images")
    public void setImages(Images images) {
        this.images = images;
    }

    /**
     * 
     * @return
     *     The relatedSearches
     */
    @JsonProperty("relatedSearches")
    public RelatedSearches getRelatedSearches() {
        return relatedSearches;
    }

    /**
     * 
     * @param relatedSearches
     *     The relatedSearches
     */
    @JsonProperty("relatedSearches")
    public void setRelatedSearches(RelatedSearches relatedSearches) {
        this.relatedSearches = relatedSearches;
    }

    /**
     * 
     * @return
     *     The rankingResponse
     */
    @JsonProperty("rankingResponse")
    public RankingResponse getRankingResponse() {
        return rankingResponse;
    }

    /**
     * 
     * @param rankingResponse
     *     The rankingResponse
     */
    @JsonProperty("rankingResponse")
    public void setRankingResponse(RankingResponse rankingResponse) {
        this.rankingResponse = rankingResponse;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(type).append(webPages).append(images).append(relatedSearches).append(rankingResponse).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof BingWeb) == false) {
            return false;
        }
        BingWeb rhs = ((BingWeb) other);
        return new EqualsBuilder().append(type, rhs.type).append(webPages, rhs.webPages).append(images, rhs.images).append(relatedSearches, rhs.relatedSearches).append(rankingResponse, rhs.rankingResponse).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
