
package com.umapped.external.bing.web;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "mainline",
    "sidebar"
})
public class RankingResponse {

    @JsonProperty("mainline")
    private Mainline mainline;
    @JsonProperty("sidebar")
    private Sidebar sidebar;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The mainline
     */
    @JsonProperty("mainline")
    public Mainline getMainline() {
        return mainline;
    }

    /**
     * 
     * @param mainline
     *     The mainline
     */
    @JsonProperty("mainline")
    public void setMainline(Mainline mainline) {
        this.mainline = mainline;
    }

    /**
     * 
     * @return
     *     The sidebar
     */
    @JsonProperty("sidebar")
    public Sidebar getSidebar() {
        return sidebar;
    }

    /**
     * 
     * @param sidebar
     *     The sidebar
     */
    @JsonProperty("sidebar")
    public void setSidebar(Sidebar sidebar) {
        this.sidebar = sidebar;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(mainline).append(sidebar).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RankingResponse) == false) {
            return false;
        }
        RankingResponse rhs = ((RankingResponse) other);
        return new EqualsBuilder().append(mainline, rhs.mainline).append(sidebar, rhs.sidebar).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
