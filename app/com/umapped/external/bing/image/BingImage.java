
package com.umapped.external.bing.image;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "_type",
    "instrumentation",
    "webSearchUrl",
    "totalEstimatedMatches",
    "value",
    "queryExpansions",
    "nextOffsetAddCount",
    "pivotSuggestions",
    "displayShoppingSourcesBadges",
    "displayRecipeSourcesBadges",
    "similarTerms"
})
public class BingImage {

    @JsonProperty("_type")
    private String type;
    @JsonProperty("instrumentation")
    private Instrumentation instrumentation;
    @JsonProperty("webSearchUrl")
    private String webSearchUrl;
    @JsonProperty("totalEstimatedMatches")
    private int totalEstimatedMatches;
    @JsonProperty("value")
    private List<Value> value = new ArrayList<Value>();
    @JsonProperty("queryExpansions")
    private List<QueryExpansion> queryExpansions = new ArrayList<QueryExpansion>();
    @JsonProperty("nextOffsetAddCount")
    private int nextOffsetAddCount;
    @JsonProperty("pivotSuggestions")
    private List<PivotSuggestion> pivotSuggestions = new ArrayList<PivotSuggestion>();
    @JsonProperty("displayShoppingSourcesBadges")
    private boolean displayShoppingSourcesBadges;
    @JsonProperty("displayRecipeSourcesBadges")
    private boolean displayRecipeSourcesBadges;
    @JsonProperty("similarTerms")
    private List<SimilarTerm> similarTerms = new ArrayList<SimilarTerm>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The type
     */
    @JsonProperty("_type")
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The _type
     */
    @JsonProperty("_type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The instrumentation
     */
    @JsonProperty("instrumentation")
    public Instrumentation getInstrumentation() {
        return instrumentation;
    }

    /**
     * 
     * @param instrumentation
     *     The instrumentation
     */
    @JsonProperty("instrumentation")
    public void setInstrumentation(Instrumentation instrumentation) {
        this.instrumentation = instrumentation;
    }

    /**
     * 
     * @return
     *     The webSearchUrl
     */
    @JsonProperty("webSearchUrl")
    public String getWebSearchUrl() {
        return webSearchUrl;
    }

    /**
     * 
     * @param webSearchUrl
     *     The webSearchUrl
     */
    @JsonProperty("webSearchUrl")
    public void setWebSearchUrl(String webSearchUrl) {
        this.webSearchUrl = webSearchUrl;
    }

    /**
     * 
     * @return
     *     The totalEstimatedMatches
     */
    @JsonProperty("totalEstimatedMatches")
    public int getTotalEstimatedMatches() {
        return totalEstimatedMatches;
    }

    /**
     * 
     * @param totalEstimatedMatches
     *     The totalEstimatedMatches
     */
    @JsonProperty("totalEstimatedMatches")
    public void setTotalEstimatedMatches(int totalEstimatedMatches) {
        this.totalEstimatedMatches = totalEstimatedMatches;
    }

    /**
     * 
     * @return
     *     The value
     */
    @JsonProperty("value")
    public List<Value> getValue() {
        return value;
    }

    /**
     * 
     * @param value
     *     The value
     */
    @JsonProperty("value")
    public void setValue(List<Value> value) {
        this.value = value;
    }

    /**
     * 
     * @return
     *     The queryExpansions
     */
    @JsonProperty("queryExpansions")
    public List<QueryExpansion> getQueryExpansions() {
        return queryExpansions;
    }

    /**
     * 
     * @param queryExpansions
     *     The queryExpansions
     */
    @JsonProperty("queryExpansions")
    public void setQueryExpansions(List<QueryExpansion> queryExpansions) {
        this.queryExpansions = queryExpansions;
    }

    /**
     * 
     * @return
     *     The nextOffsetAddCount
     */
    @JsonProperty("nextOffsetAddCount")
    public int getNextOffsetAddCount() {
        return nextOffsetAddCount;
    }

    /**
     * 
     * @param nextOffsetAddCount
     *     The nextOffsetAddCount
     */
    @JsonProperty("nextOffsetAddCount")
    public void setNextOffsetAddCount(int nextOffsetAddCount) {
        this.nextOffsetAddCount = nextOffsetAddCount;
    }

    /**
     * 
     * @return
     *     The pivotSuggestions
     */
    @JsonProperty("pivotSuggestions")
    public List<PivotSuggestion> getPivotSuggestions() {
        return pivotSuggestions;
    }

    /**
     * 
     * @param pivotSuggestions
     *     The pivotSuggestions
     */
    @JsonProperty("pivotSuggestions")
    public void setPivotSuggestions(List<PivotSuggestion> pivotSuggestions) {
        this.pivotSuggestions = pivotSuggestions;
    }

    /**
     * 
     * @return
     *     The displayShoppingSourcesBadges
     */
    @JsonProperty("displayShoppingSourcesBadges")
    public boolean isDisplayShoppingSourcesBadges() {
        return displayShoppingSourcesBadges;
    }

    /**
     * 
     * @param displayShoppingSourcesBadges
     *     The displayShoppingSourcesBadges
     */
    @JsonProperty("displayShoppingSourcesBadges")
    public void setDisplayShoppingSourcesBadges(boolean displayShoppingSourcesBadges) {
        this.displayShoppingSourcesBadges = displayShoppingSourcesBadges;
    }

    /**
     * 
     * @return
     *     The displayRecipeSourcesBadges
     */
    @JsonProperty("displayRecipeSourcesBadges")
    public boolean isDisplayRecipeSourcesBadges() {
        return displayRecipeSourcesBadges;
    }

    /**
     * 
     * @param displayRecipeSourcesBadges
     *     The displayRecipeSourcesBadges
     */
    @JsonProperty("displayRecipeSourcesBadges")
    public void setDisplayRecipeSourcesBadges(boolean displayRecipeSourcesBadges) {
        this.displayRecipeSourcesBadges = displayRecipeSourcesBadges;
    }

    /**
     * 
     * @return
     *     The similarTerms
     */
    @JsonProperty("similarTerms")
    public List<SimilarTerm> getSimilarTerms() {
        return similarTerms;
    }

    /**
     * 
     * @param similarTerms
     *     The similarTerms
     */
    @JsonProperty("similarTerms")
    public void setSimilarTerms(List<SimilarTerm> similarTerms) {
        this.similarTerms = similarTerms;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "BingImage{" +
            "type='" + type + '\'' +
            ", instrumentation=" + instrumentation +
            ", webSearchUrl='" + webSearchUrl + '\'' +
            ", totalEstimatedMatches=" + totalEstimatedMatches +
            ", value=" + value +
            ", queryExpansions=" + queryExpansions +
            ", nextOffsetAddCount=" + nextOffsetAddCount +
            ", pivotSuggestions=" + pivotSuggestions +
            ", displayShoppingSourcesBadges=" + displayShoppingSourcesBadges +
            ", displayRecipeSourcesBadges=" + displayRecipeSourcesBadges +
            ", similarTerms=" + similarTerms +
            ", additionalProperties=" + additionalProperties +
            '}';
    }
}
