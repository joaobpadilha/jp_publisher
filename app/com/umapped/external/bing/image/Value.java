
package com.umapped.external.bing.image;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "name",
    "webSearchUrl",
    "thumbnailUrl",
    "datePublished",
    "contentUrl",
    "hostPageUrl",
    "contentSize",
    "encodingFormat",
    "hostPageDisplayUrl",
    "width",
    "height",
    "thumbnail",
    "imageInsightsToken",
    "insightsSourcesSummary",
    "imageId",
    "accentColor"
})
public class Value {

    @JsonProperty("name")
    private String name;
    @JsonProperty("webSearchUrl")
    private String webSearchUrl;
    @JsonProperty("thumbnailUrl")
    private String thumbnailUrl;
    @JsonProperty("datePublished")
    private String datePublished;
    @JsonProperty("contentUrl")
    private String contentUrl;
    @JsonProperty("hostPageUrl")
    private String hostPageUrl;
    @JsonProperty("contentSize")
    private String contentSize;
    @JsonProperty("encodingFormat")
    private String encodingFormat;
    @JsonProperty("hostPageDisplayUrl")
    private String hostPageDisplayUrl;
    @JsonProperty("width")
    private int width;
    @JsonProperty("height")
    private int height;
    @JsonProperty("thumbnail")
    private Thumbnail thumbnail;
    @JsonProperty("imageInsightsToken")
    private String imageInsightsToken;
    @JsonProperty("insightsSourcesSummary")
    private InsightsSourcesSummary insightsSourcesSummary;
    @JsonProperty("imageId")
    private String imageId;
    @JsonProperty("accentColor")
    private String accentColor;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The webSearchUrl
     */
    @JsonProperty("webSearchUrl")
    public String getWebSearchUrl() {
        return webSearchUrl;
    }

    /**
     * 
     * @param webSearchUrl
     *     The webSearchUrl
     */
    @JsonProperty("webSearchUrl")
    public void setWebSearchUrl(String webSearchUrl) {
        this.webSearchUrl = webSearchUrl;
    }

    /**
     * 
     * @return
     *     The thumbnailUrl
     */
    @JsonProperty("thumbnailUrl")
    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    /**
     * 
     * @param thumbnailUrl
     *     The thumbnailUrl
     */
    @JsonProperty("thumbnailUrl")
    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    /**
     * 
     * @return
     *     The datePublished
     */
    @JsonProperty("datePublished")
    public String getDatePublished() {
        return datePublished;
    }

    /**
     * 
     * @param datePublished
     *     The datePublished
     */
    @JsonProperty("datePublished")
    public void setDatePublished(String datePublished) {
        this.datePublished = datePublished;
    }

    /**
     * 
     * @return
     *     The contentUrl
     */
    @JsonProperty("contentUrl")
    public String getContentUrl() {
        return contentUrl;
    }

    /**
     * 
     * @param contentUrl
     *     The contentUrl
     */
    @JsonProperty("contentUrl")
    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    /**
     * 
     * @return
     *     The hostPageUrl
     */
    @JsonProperty("hostPageUrl")
    public String getHostPageUrl() {
        return hostPageUrl;
    }

    /**
     * 
     * @param hostPageUrl
     *     The hostPageUrl
     */
    @JsonProperty("hostPageUrl")
    public void setHostPageUrl(String hostPageUrl) {
        this.hostPageUrl = hostPageUrl;
    }

    /**
     * 
     * @return
     *     The contentSize
     */
    @JsonProperty("contentSize")
    public String getContentSize() {
        return contentSize;
    }

    /**
     * 
     * @param contentSize
     *     The contentSize
     */
    @JsonProperty("contentSize")
    public void setContentSize(String contentSize) {
        this.contentSize = contentSize;
    }

    /**
     * 
     * @return
     *     The encodingFormat
     */
    @JsonProperty("encodingFormat")
    public String getEncodingFormat() {
        return encodingFormat;
    }

    /**
     * 
     * @param encodingFormat
     *     The encodingFormat
     */
    @JsonProperty("encodingFormat")
    public void setEncodingFormat(String encodingFormat) {
        this.encodingFormat = encodingFormat;
    }

    /**
     * 
     * @return
     *     The hostPageDisplayUrl
     */
    @JsonProperty("hostPageDisplayUrl")
    public String getHostPageDisplayUrl() {
        return hostPageDisplayUrl;
    }

    /**
     * 
     * @param hostPageDisplayUrl
     *     The hostPageDisplayUrl
     */
    @JsonProperty("hostPageDisplayUrl")
    public void setHostPageDisplayUrl(String hostPageDisplayUrl) {
        this.hostPageDisplayUrl = hostPageDisplayUrl;
    }

    /**
     * 
     * @return
     *     The width
     */
    @JsonProperty("width")
    public int getWidth() {
        return width;
    }

    /**
     * 
     * @param width
     *     The width
     */
    @JsonProperty("width")
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * 
     * @return
     *     The height
     */
    @JsonProperty("height")
    public int getHeight() {
        return height;
    }

    /**
     * 
     * @param height
     *     The height
     */
    @JsonProperty("height")
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * 
     * @return
     *     The thumbnail
     */
    @JsonProperty("thumbnail")
    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    /**
     * 
     * @param thumbnail
     *     The thumbnail
     */
    @JsonProperty("thumbnail")
    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    /**
     * 
     * @return
     *     The imageInsightsToken
     */
    @JsonProperty("imageInsightsToken")
    public String getImageInsightsToken() {
        return imageInsightsToken;
    }

    /**
     * 
     * @param imageInsightsToken
     *     The imageInsightsToken
     */
    @JsonProperty("imageInsightsToken")
    public void setImageInsightsToken(String imageInsightsToken) {
        this.imageInsightsToken = imageInsightsToken;
    }

    /**
     * 
     * @return
     *     The insightsSourcesSummary
     */
    @JsonProperty("insightsSourcesSummary")
    public InsightsSourcesSummary getInsightsSourcesSummary() {
        return insightsSourcesSummary;
    }

    /**
     * 
     * @param insightsSourcesSummary
     *     The insightsSourcesSummary
     */
    @JsonProperty("insightsSourcesSummary")
    public void setInsightsSourcesSummary(InsightsSourcesSummary insightsSourcesSummary) {
        this.insightsSourcesSummary = insightsSourcesSummary;
    }

    /**
     * 
     * @return
     *     The imageId
     */
    @JsonProperty("imageId")
    public String getImageId() {
        return imageId;
    }

    /**
     * 
     * @param imageId
     *     The imageId
     */
    @JsonProperty("imageId")
    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    /**
     * 
     * @return
     *     The accentColor
     */
    @JsonProperty("accentColor")
    public String getAccentColor() {
        return accentColor;
    }

    /**
     * 
     * @param accentColor
     *     The accentColor
     */
    @JsonProperty("accentColor")
    public void setAccentColor(String accentColor) {
        this.accentColor = accentColor;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
