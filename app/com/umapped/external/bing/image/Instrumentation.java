
package com.umapped.external.bing.image;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "pageLoadPingUrl"
})
public class Instrumentation {

    @JsonProperty("pageLoadPingUrl")
    private String pageLoadPingUrl;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The pageLoadPingUrl
     */
    @JsonProperty("pageLoadPingUrl")
    public String getPageLoadPingUrl() {
        return pageLoadPingUrl;
    }

    /**
     * 
     * @param pageLoadPingUrl
     *     The pageLoadPingUrl
     */
    @JsonProperty("pageLoadPingUrl")
    public void setPageLoadPingUrl(String pageLoadPingUrl) {
        this.pageLoadPingUrl = pageLoadPingUrl;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "Instrumentation{" +
            "pageLoadPingUrl='" + pageLoadPingUrl + '\'' +
            ", additionalProperties=" + additionalProperties +
            '}';
    }
}
