package com.umapped.external.travelbound;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mapped.common.CoreConstants;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.utils.Log;
import com.umapped.external.travelbound.api.*;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wei on 2017-05-09.
 */
@Singleton public class TravelboundAPIInvoker {
  private Map<String, String> serverUrls;

  private JAXBContext jaxbContext;

  private static String TRAVELBOUND_CAN = "034";
  private static String TRAVELBOUND_USA = "044";

  @Inject public TravelboundAPIInvoker() {
    serverUrls = new HashMap<>();
    serverUrls.put(TRAVELBOUND_CAN, ConfigMgr.getAppParameter(CoreConstants.TRAVELBOUND_URL_CAN));
    serverUrls.put(TRAVELBOUND_USA, ConfigMgr.getAppParameter(CoreConstants.TRAVELBOUND_URL_USA));

    if (serverUrls.isEmpty()) {
      Log.err("Fatal: travelbound api URL not configured");
      throw new RuntimeException("Fatal: travelbound api URL not configured");
    }
    try {
      jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
    }
    catch (JAXBException e) {
      Log.err("Fail to create JAXBContext", e);
      throw new RuntimeException(e);
    }
  }

  public Response invoke(TravelboundAPICredential credential, TRequestDetails requestDetails) {
    String serverUrl = serverUrls.get(credential.getSite());
    Log.debug("Travelbound site:" + credential.getSite() );
    HttpPost postRequest = new HttpPost(serverUrl);
    Marshaller marsharller = null;
    try {
      marsharller = jaxbContext.createMarshaller();
      StringWriter payload = new StringWriter();
      marsharller.marshal(createRequestWrapper(credential, requestDetails), payload);
      String payloadValue = payload.toString();
      StringEntity requestEntity = new StringEntity(payloadValue);
      Log.debug("Travelbound Request: " + payloadValue);
      postRequest.setEntity(requestEntity);
    }
    catch (Exception e) {
      Log.err("fail to create request", e);
      throw new RuntimeException(e);
    }

    postRequest.setHeader("Content-Type", "text/xml");
    try (CloseableHttpClient client = HttpClients.createDefault();
         CloseableHttpResponse response = client.execute(postRequest)) {
      Header[] headers = response.getAllHeaders();

      HttpEntity entity = response.getEntity();

      Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
      Response travelboundResponse = (Response) unmarshaller.unmarshal(entity.getContent());

      try {
        StringWriter writer = new StringWriter();
        marsharller.marshal(travelboundResponse, writer);
        Log.debug("Travelbound Response:" + writer.toString());
      }
      catch (Exception ex) {
        // ignore the log
        Log.err("Fail to log response", ex);
      }

      return travelboundResponse;
    }
    catch (Exception e) {
      Log.err("Fail to post request and process response", e);
      throw new RuntimeException(e);
    }
  }

  public Request createRequestWrapper(TravelboundAPICredential credential, TRequestDetails requestDetails) {

    Request request = new Request();
    TSource source = new TSource();

    request.setSource(source);

    source.setRequestorID(getRequestorID(credential));
    source.setRequestorPreferences(getRequestorPreferences());

    request.setRequestDetails(requestDetails);
    return request;
  }

  private TRequestorID getRequestorID(TravelboundAPICredential credential) {
    TRequestorID requestorId = new TRequestorID();
    requestorId.setClient(credential.getClientId());
    requestorId.setEMailAddress(credential.getEmail());
    requestorId.setPassword(credential.getPassword());
    return requestorId;
  }

  private TRequestorPreferences getRequestorPreferences() {
    TRequestorPreferences pref = new TRequestorPreferences();
    pref.setCountry("US");
    pref.setCurrency("USD");
    pref.setLanguage("en");
    pref.setRequestMode(TRequestMode.SYNCHRONOUS);
    return pref;
  }
}
