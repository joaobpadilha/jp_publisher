package com.umapped.external.travelbound;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.persistence.VOModeller;
import com.mapped.publisher.utils.Log;
import com.umapped.external.travelbound.api.*;
import io.jsonwebtoken.lang.Collections;
import models.publisher.Account;
import models.publisher.Trip;
import models.publisher.TripDetail;
import org.apache.commons.lang3.tuple.Pair;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by wei on 2017-05-15.
 */
@Singleton public class TravelboundImportService {
  @Inject private TravelboundBookingMapper mapper;

  @Inject private TravelboundWebService webService;

  public void importBooking(Account account, TravelboundAPICredential credential, String tripId, String bookingId)
      throws TravelboundImportException {

    Trip trip = Trip.findByPK(tripId);

    if (trip == null) {
      return;
    }
    importBooking(account, credential, trip, bookingId);
  }

  public void importBooking(Account account, TravelboundAPICredential credential, Trip trip, String bookingId)
      throws TravelboundImportException {
    try {
      TSearchBookingItemResponse response = webService.getBookingItems(credential, bookingId);

      // request specific error, i.e. function not authorized, need to investigate and likely contact travelbound
      if (response.getErrors() != null &&  !Collections.isEmpty(response.getErrors().getErrors())) {
        throw new TravelboundAPIException(response.getErrors().getErrors());
      }

      Pair<String, List<Integer>> bookingItemIds = mapper.getBookingItemIds(response);

      Map<Integer, TSearchChargeConditionsResponse> chargeConditionsResponseMap = new HashMap<>();
      if (bookingItemIds.getLeft()!= null) {
        for (Integer bookingItemRef : bookingItemIds.getRight()) {
          TSearchChargeConditionsResponse searchChargeConditionsResponse =
              webService.searchChargeConditions(credential, bookingItemIds.getLeft(), bookingItemRef);
          chargeConditionsResponseMap.put(bookingItemRef, searchChargeConditionsResponse);
        }
      }

      Map<TravelBoundItemInfoKey, TSearchItemInformationResponse> itemInfoResponseMap = new HashMap<>();

      for (TBookingItem item : response.getBookingItems().getBookingItems()) {
        // retrieve only sightsee item
        if (TItemType.SIGHTSEEING.equals(item.getItemType())) {
          TravelBoundItemInfoKey key = mapper.getItemInfoKey(item);
          if (key != null) {
            itemInfoResponseMap.put(key, webService.getItemInformation(credential, TItemType.SIGHTSEEING, key));
          }
        }
      }
      VOModeller voModeller = new VOModeller(trip, account, false);


      TripVO tripvo = new TripVO();
      tripvo.setImportSrc(BookingSrc.ImportSrc.TRAVELBOUND);
      tripvo.setImportSrcId(bookingId);
      tripvo.setImportTs(Instant.now().toEpochMilli());

      mapper.map(tripvo, response, chargeConditionsResponseMap, itemInfoResponseMap);

      voModeller.buildTripVOModels(tripvo);
      voModeller.saveAllModels();
    }
    catch (TravelboundAPIException e) {
      Log.err("Fail to retrieve booking items: " + bookingId, e);
    }
    catch (Exception e) {
      Log.err("Fail to import booking ", e);
      throw new TravelboundImportException("Fail to import travelbound booking: " + bookingId, e);
    }
  }

  public void updateBookings(Account account, TravelboundAPICredential credential, Trip trip, Set<String> apiBookingIds)
      throws TravelboundImportException {
    try {
      List<TripDetail> tripDetails = TripDetail.findActiveByTripId(trip.getTripid());
      removeBooking(tripDetails, apiBookingIds);

      for (String bookingId : apiBookingIds) {
        importBooking(account, credential, trip, bookingId);
      }
    }
    catch (Exception e) {
      Log.err("Fail to import booking ", e);
      throw new TravelboundImportException("Fail to update travelbound booking for trip: " + trip.getTripid(), e);
    }
  }

  protected void removeBooking(List<TripDetail> tripDetails, Set<String> bookingIds) {
    for (TripDetail detail : tripDetails) {
      if (BookingSrc.ImportSrc.TRAVELBOUND.equals(detail.getImportSrc()) && bookingIds.contains(detail.getImportSrcId
          ())) {
        detail.delete();
      }
    }
  }
}
