package com.umapped.external.travelbound;

import com.google.inject.Singleton;
import com.mapped.publisher.utils.Log;
import com.umapped.api.schema.local.PoiPhoto;
import com.umapped.external.travelbound.api.ObjectFactory;
import com.umapped.external.travelbound.api.TImageLink;
import com.umapped.external.travelbound.api.TImageLinks;
import com.umapped.external.travelbound.api.TItemDetail;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by wei on 2017-05-22.
 */
@Singleton public class TravelboundItemMapper {

  private JAXBContext jaxbContext;

  public TravelboundItemMapper() {
    try {
      jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
    }
    catch (JAXBException e) {
      Log.err("Fail to create JAXBContext", e);
      throw new RuntimeException(e);
    }
  }

  public List<PoiPhoto> getHotelPhotos(String hotel) {
    try {
      Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
      StreamSource source = new StreamSource();
      source.setReader(new StringReader(hotel));
      JAXBElement<TItemDetail> element = unmarshaller.unmarshal(source, TItemDetail.class);
      TItemDetail itemDetail = element.getValue();
      return getHotelPhotos(itemDetail);
    }
    catch (JAXBException e) {
      Log.err("Fail to unmarshal travelbound hotel", e);
      return Collections.emptyList();
    }
  }

  public List<PoiPhoto> getHotelPhotos(TItemDetail hotelInfo) {
    List<PoiPhoto> photos = new ArrayList<>();

    if (hotelInfo.getHotelInformation().getLinks() != null && hotelInfo.getHotelInformation()
                                                                       .getLinks()
                                                                       .getImageLinks() != null) {
      TImageLinks imageLinks = hotelInfo.getHotelInformation().getLinks().getImageLinks();
      for (TImageLink link : imageLinks.getImageLinks()) {
        photos.add(map(link));
      }
    }
    return photos;
  }

  private PoiPhoto map(TImageLink link) {
    PoiPhoto photo = new PoiPhoto();
    photo.url = link.getImage();
    photo.thumb = link.getThumbNail();
    photo.caption = link.getText();
    if ((photo.url.lastIndexOf("/") + 1) < photo.url.length()) {
      photo.fileName = photo.url.substring(photo.url.lastIndexOf("/") + 1);
    }
    return photo;
  }
}
