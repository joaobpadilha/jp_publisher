package com.umapped.external.travelbound;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.umapped.external.travelbound.api.*;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.openrdf.query.algebra.Str;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created by wei on 2017-05-09.
 */
@Singleton public class TravelboundBookingMapper
    implements TravelboundConstants {
  @Inject TravelboundLocationMapper locationMapper;

  public List<TravelboundBookingSearchResult> map(Integer clientId, TSearchBookingResponse response) {

    TBookings bookings = response.getBookings();
    if (bookings == null) {
      return Collections.emptyList();
    }
    List<TravelboundBookingSearchResult> bookingResult = new ArrayList<>();

    for (TBooking b : bookings.getBookings()) {
      if (!isCancelled(b.getBookingStatus())) {
        bookingResult.add(new TravelboundBookingSearchResult(clientId, b.getBookingName(),
                                                             mapTimestamp(b.getBookingDepartureDate()),
                                                             getAPIBookingReference(b.getBookingReferences()),
                                                             b.getBookingStatus().getValue()));
      }
    }
    return bookingResult;
  }


  public Pair<String, List<Integer>> getBookingItemIds(TSearchBookingItemResponse response) {
    String bookingId = getAPIBookingReference(response.getBookingReferences());
    List<Integer> itemIds = new ArrayList<>();
    if (!isCancelled(response.getBookingStatus())) {
      for (TBookingItem item : response.getBookingItems().getBookingItems()) {
        itemIds.add(item.getItemReference());
      }
    }
    return new ImmutablePair<>(bookingId, itemIds);
  }

  public void map(TripVO trip,
                  TSearchBookingItemResponse response,
                  Map<Integer, TSearchChargeConditionsResponse> searchChargeConditionsResponseMap,
                  Map<TravelBoundItemInfoKey, TSearchItemInformationResponse> itemInfoMap) {
    mapTripVO(trip, response);
    Map<Integer, Passenger> passengers = mapPassenger(response.getPaxNames());
    List<TBookingItem> items = response.getBookingItems().getBookingItems();
    for (TBookingItem item : items) {
      TSearchChargeConditionsResponse chargeConditionsResponse = searchChargeConditionsResponseMap.get(item.getItemReference());
      switch (item.getItemType()) {
        case HOTEL:
          trip.addHotel(mapHotel(response, item, passengers, chargeConditionsResponse));
          break;
        case SIGHTSEEING:
          TravelBoundItemInfoKey infoKey = this.getItemInfoKey(item);
          trip.addActivity(mapSightSeeing(response, item, passengers, chargeConditionsResponse, itemInfoMap.get(infoKey)));
          TItemCity city = item.getItemCity();
          TItem itemInfo = item.getItem();
          if (city != null && itemInfo != null) {
            // get tour information

          }
          break;
        case TRANSFER:
          trip.addTransport(mapTransport(response, item, passengers, chargeConditionsResponse));
          break;
        case APARTMENT:
          break;
      }
    }
  }

  public String getAPIBookingReference(TBookingReferences references) {
    List<TBookingReference> rs = references.getBookingReferences();
    for (TBookingReference ref : rs) {
      if (TReferenceSource.API.equals(ref.getReferenceSource())) {
        return ref.getValue();
      }
    }
    return null;
  }

  public void mapTripVO(TripVO trip, TSearchBookingItemResponse response) {
    trip.setName(response.getBookingName());
    trip.setImportSrc(BookingSrc.ImportSrc.TRAVELBOUND);
    trip.setImportSrcId(getAPIBookingReference(response.getBookingReferences()));
    trip.setImportTs(System.currentTimeMillis());
  }

  public void mapPrice(ReservationVO reservationVO, TBookingItem item) {
    TBookingPrice price = item.getItemPrice();

    if (price != null) {
      reservationVO.setCurrency(price.getCurrency());

      reservationVO.setTotal(mapPrice(price.getGross()));
    }
  }

  private String mapPrice(Double price) {
    if (price != null) {
      return String.format("%.2f", price);
    }
    else {
      return null;
    }
  }

  private String mapEssentialInformation(TBookingItem item) {
    StringBuilder builder = new StringBuilder();
    if (item.getEssentialInformation() != null) {
      for (TInformation essInfo : item.getEssentialInformation().getInformations()) {
        builder.append("\n- ");
        builder.append(essInfo.getText());
      }
    }
    return builder.toString();
  }

  public HotelVO mapHotel(TSearchBookingItemResponse response,
                          TBookingItem item,
                          Map<Integer, Passenger> passengers,
                          TSearchChargeConditionsResponse chargeConditionsResponse) {
    HotelVO hotelVO = new HotelVO();

    mapPrice(hotelVO, item);
    hotelVO.setName(item.getItem().getValue());
    hotelVO.setConfirmation(item.getItemConfirmationReference());

    TItemCity city = item.getItemCity();
    TItem hotelInfo = item.getItem();

    if (city != null && hotelInfo != null) {
      String cityCode = city.getCode();
      String hotelCode = hotelInfo.getCode();
      hotelVO.setTag(StringUtils.trimToEmpty(cityCode) + "_" + StringUtils.trimToEmpty(hotelCode));
    }

    THotelItem hotelItem = item.getHotelItem();
    XMLGregorianCalendar checkinDate = hotelItem.getPeriodOfStay().getCheckInDate();
    XMLGregorianCalendar checkoutDate = hotelItem.getPeriodOfStay().getCheckOutDate();
    hotelVO.setCheckin(mapTimestamp(checkinDate));
    hotelVO.setCheckout(mapTimestamp(checkoutDate));
    if (hotelInfo != null) {
      hotelVO.setHotelName(hotelInfo.getValue());
    }
    mapRoomType(hotelVO, hotelItem.getHotelPaxRoom());
    mapMeals(hotelVO, hotelItem.getMeals());

    THotelRooms rooms = hotelItem.getHotelRooms();

    String essentialInformation = mapEssentialInformation(item);
    if (StringUtils.isNotEmpty(essentialInformation)) {
      hotelVO.addProperty("Important Information", essentialInformation );
    }

    hotelVO.setTravelers(mapHotelTravelers(rooms, passengers));
    hotelVO.setCancellationPolicy(mapChargeConditions(chargeConditionsResponse));
    return hotelVO;
  }


  private void mapRoomType(HotelVO hotelVO, THotelPaxRoom room) {
    if (room != null) {
      hotelVO.addProperty("Room Type", room.getDescription());
    }
  }
  private void mapMeals(HotelVO hotelVO, TMeals meals) {

    if (meals != null && meals.getBasis() != null && meals.getBasis().getCode().equals("N")) {
      hotelVO.addProperty("Meal Basis", "Room only");
    }
    if (meals != null) {
      if (meals.getBreakfast() != null) {
        hotelVO.addProperty("Meal Basis", meals.getBreakfast().getValue() + " Breakfast");
      }
      if (meals.getLunch() != null) {
        hotelVO.addProperty("Meal Basis", meals.getLunch().getValue() + " Lunch");
      }
      if (meals.getDinner() != null) {
        hotelVO.addProperty("Meal Basis", meals.getDinner().getValue() + " Dinner");
      }
    }
  }

  public List<HotelVO.HotelTravelerVO> mapHotelTravelers(THotelRooms rooms, Map<Integer, Passenger> passengers) {
    List<HotelVO.HotelTravelerVO> travelers = new ArrayList<>();
    if (rooms != null && rooms.getHotelRooms() != null) {
      for (THotelRoom room : rooms.getHotelRooms()) {
        TPaxIds ids = room.getPaxIds();
        if (ids != null) {
          for (Integer id : ids.getPaxIds()) {
            Passenger passenger = passengers.get(id);
            travelers.add(mapHotelTraveler(room, passenger));
          }
        }
      }
    }
    return travelers;
  }

  public HotelVO.HotelTravelerVO mapHotelTraveler(THotelRoom room, Passenger passenger) {
    HotelVO.HotelTravelerVO vo = new HotelVO.HotelTravelerVO();
    vo.roomType = room.getDescription();

    Pair<String, String> names = splitName(passenger.name);
    vo.firstName = names.getLeft();
    vo.lastName = names.getRight();
    return vo;
  }

  public Pair<String, String> splitName(String name) {
    int lastSpace = StringUtils.lastIndexOf(name, " ");
    String lastName = "";
    String firstName = "";
    if (lastSpace != -1) {
      lastName = StringUtils.substring(name, lastSpace + 1);
      firstName = StringUtils.trim(StringUtils.substring(name, 0, lastSpace));
    }
    else {
      lastName = name;
    }

    return new ImmutablePair<>(firstName, lastName);
  }

  public TransportVO mapTransport(TSearchBookingItemResponse response,
                                  TBookingItem item,
                                  Map<Integer, Passenger> passengers, TSearchChargeConditionsResponse chargeConditionsResponse) {
    TransportVO transportVO = new TransportVO();
    mapPrice(transportVO, item);
    transportVO.setBookingType(ReservationType.TRANSPORT);
    transportVO.setName(item.getItem().getValue());
    transportVO.setConfirmationNumber(item.getItemConfirmationReference());
    TTransferItem transferItem = item.getTransferItem();
    transportVO.setPickupDate(mapTimestamp(transferItem.getTransferDate()));

    TPickUpDetails pickUpDetails = transferItem.getPickUpDetails();
    mapTransferPickup(transportVO, transferItem.getTransferDate(), pickUpDetails);

    TDropOffDetails dropOffDetails = transferItem.getDropOffDetails();
    mapTransferDropOff(transportVO, transferItem.getTransferDate(), dropOffDetails);

    if (!StringUtils.isEmpty(transferItem.getSupplierTelephoneNumber())) {
      transportVO.addProperty("Telephone", transferItem.getSupplierTelephoneNumber());
    }
    TTransferVehicle vehicle = transferItem.getTransferVehicle();

    if (vehicle != null) {
      if (vehicle.getVehicle() != null) {
        transportVO.addProperty("Vehichle Type", vehicle.getVehicle().getValue());
        if (vehicle.getVehicle().getMaximumPassengers() != null) {
          transportVO.addProperty("Maximum number of passengers",
                                  vehicle.getVehicle().getMaximumPassengers().toString());
        }
        if (vehicle.getVehicle().getMaximumLuggage() != null) {
          transportVO.addProperty("Maximum pieces of luggage", vehicle.getVehicle().getMaximumLuggage().toString());
        }
      }
    }
    TTransferConditions transferConditions = transferItem.getTransferConditions();
    if (transferConditions != null) {
      List<String> conditions = transferConditions.getTransferConditions();
      StringBuilder builder = new StringBuilder();
      for (String c : conditions) {
        builder.append(c);
        builder.append("\n");
      }
      transportVO.addProperty("Conditions", builder.toString());
    }

    String essentialInformation = mapEssentialInformation(item);
    if (StringUtils.isNotEmpty(essentialInformation)) {
      transportVO.addProperty("Important Information", essentialInformation );
    }

    transportVO.setCancellationPolicy(mapChargeConditions(chargeConditionsResponse));
    return transportVO;
  }

  public ActivityVO mapSightSeeing(TSearchBookingItemResponse response,
                                   TBookingItem item,
                                   Map<Integer, Passenger> passengers, TSearchChargeConditionsResponse chargeConditionsResponse,
                                   TSearchItemInformationResponse itemInformationResponse) {
    ActivityVO activityVO = new ActivityVO();
    mapPrice(activityVO, item);
    activityVO.setBookingType(ReservationType.TOUR);
    activityVO.setConfirmation(item.getItemConfirmationReference());
    activityVO.setName(item.getItem().getValue());
    TSightseeingItem sightseeingItem = item.getSightseeingItem();

    Timestamp startDate = null;

    XMLGregorianCalendar tourDate = sightseeingItem.getTourDate();
    TDeparture departure = sightseeingItem.getTourDeparture();
    if (departure != null) {
      if (departure.getTimes() != null && departure.getTimes().size() > 0) {
        startDate = mapTimestamp(tourDate, departure.getTimes().get(0));
      }

      if (departure.getDeparturePoint() != null) {
        activityVO.setStartLocation(departure.getDeparturePoint().getValue());
      }

      if (departure.getAddressLines() != null || departure.getDescription() != null) {
        setDeparturePointDetails(activityVO, departure.getAddressLines(), departure.getDescription());
      }

      if (StringUtils.isNotEmpty(departure.getTelephone())) {
        activityVO.addProperty("Contact Phone #", departure.getTelephone() + "\n");
      }
    }

    if (startDate == null) {
      startDate = mapTimestamp(tourDate);
    }

    activityVO.setStartDate(startDate);

    if (sightseeingItem.getTourLanguage() != null) {
      activityVO.addProperty("Tour Language", sightseeingItem.getTourLanguage().getValue() + "\n");
    }

    if (sightseeingItem.getTourSupplier() != null) {
      activityVO.setContact(sightseeingItem.getTourSupplier().getValue());
    }

    if (itemInformationResponse!=null) {
      String duration = this.getItemDuration(itemInformationResponse);
      if (!StringUtils.isEmpty(duration)) {
        activityVO.addProperty("Tour Duration", duration + "\n");
      }
    }

    String essentialInformation = mapEssentialInformation(item);
    if (StringUtils.isNotEmpty(essentialInformation)) {
      activityVO.addProperty("Important Information", essentialInformation + "\n" );
    }

    activityVO.setCancellationPolicy(mapChargeConditions(chargeConditionsResponse));
    return activityVO;
  }

  private String getItemDuration(TSearchItemInformationResponse response) {
    if (response.getItemDetails() != null && response.getItemDetails().getItemDetails() != null && response.getItemDetails().getItemDetails().size() == 1) {
      TItemDetail itemDetail = response.getItemDetails().getItemDetails().get(0);
      if (itemDetail.getSightseeingInformation() != null) {
        return itemDetail.getSightseeingInformation().getDuration() ;
      }
    }
    return null;
  }

  private void setDeparturePointDetails(ActivityVO activityVO, TAddressLines addressLines, String descrption) {
    StringBuilder builder = new StringBuilder();
    if (addressLines != null) {
      if (!StringUtils.isEmpty(addressLines.getAddressLine1())) {
        builder.append(addressLines.getAddressLine1());
        builder.append("\n");
      }
      if (!StringUtils.isEmpty(addressLines.getAddressLine2())) {
        builder.append(addressLines.getAddressLine2());
        builder.append("\n");
      }
      if (!StringUtils.isEmpty(addressLines.getAddressLine3())) {
        builder.append(addressLines.getAddressLine3());
        builder.append("\n");
      }
      if (!StringUtils.isEmpty(addressLines.getAddressLine4())) {
        builder.append(addressLines.getAddressLine4());
        builder.append("\n");
      }
      if (!StringUtils.isEmpty(descrption)) {
        builder.append(descrption);
        builder.append("\n");
      }
    }
    if (builder.length() > 0) {
      activityVO.addProperty("Departure Point", "\n" + builder.toString());
    }
  }

  private Timestamp mapTimestamp(XMLGregorianCalendar calendar) {
    if (calendar != null) {
      Date date = calendar.toGregorianCalendar().getTime();
      Timestamp ts = new Timestamp(date.getTime());
      return ts;
    }
    else {
      return null;
    }
  }

  private Timestamp mapTimestamp(XMLGregorianCalendar calendar, String time) {
    if (calendar != null && time != null) {
      String[] hm = time.split("\\.");
      if (hm != null && hm.length == 2) {
        GregorianCalendar gcal = calendar.toGregorianCalendar();
        gcal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hm[0]));
        gcal.set(Calendar.MINUTE, Integer.parseInt(hm[1]));
        return new Timestamp(gcal.getTime().getTime());
      }
      else {
        return mapTimestamp(calendar);
      }
    }
    else {
      return null;
    }
  }

  public Map<Integer, Passenger> mapPassenger(TPaxNames names) {
    Map<Integer, Passenger> passengers = new HashMap<>();
    if (names.getPaxNames() != null) {
      for (TPaxName name : names.getPaxNames()) {
        Passenger passenger = new Passenger(name.getPaxId(), name.getValue(), name.getPaxType(), name.getChildAge());
        passengers.put(passenger.paxId, passenger);
      }
    }
    return passengers;
  }

  private void mapTransferPickup(TransportVO transportVO,
                                 XMLGregorianCalendar transferDate,
                                 TPickUpDetails pickUpDetails) {
    if (pickUpDetails.getPickUpAirport() != null) {
      mapTransferAirportPickup(transportVO, transferDate, pickUpDetails.getPickUpAirport());
    }
    if (pickUpDetails.getPickUpAccommodation() != null) {
      mapTransferAccomondationPickup(transportVO, transferDate, pickUpDetails.getPickUpAccommodation());
    }
    if (pickUpDetails.getPickUpStation() != null) {
      mapTransferStationPickup(transportVO, transferDate, pickUpDetails.getPickUpStation());
    }
    if (pickUpDetails.getPickUpPoints() != null) {
      mapTransferPointsPickup(transportVO, transferDate, pickUpDetails.getPickUpPoints());
    }
    if (pickUpDetails.getPickUpOther() != null) {
      mapTransferOtherPickup(transportVO, transferDate, pickUpDetails.getPickUpOther());
    }

  }

  private void mapTransferDropOff(TransportVO transportVO,
                                  XMLGregorianCalendar transferDate,
                                  TDropOffDetails dropOffDetails) {
    if (dropOffDetails.getDropOffAirport() != null) {
      mapTransferAirportDropOff(transportVO, transferDate, dropOffDetails.getDropOffAirport());
    }
    if (dropOffDetails.getDropOffAccommodation() != null) {
      mapTransferAccomondationDropOff(transportVO, transferDate, dropOffDetails.getDropOffAccommodation());
    }
    if (dropOffDetails.getDropOffStation() != null) {
      mapTransferStationDropOff(transportVO, transferDate, dropOffDetails.getDropOffStation());
    }
    if (dropOffDetails.getDropOffPoints() != null) {
      mapTransferPointsDropOff(transportVO, transferDate, dropOffDetails.getDropOffPoints());
    }
    if (dropOffDetails.getDropOffOther() != null) {
      mapTransferOtherDropOff(transportVO, transferDate, dropOffDetails.getDropOffOther());
    }

  }

  private void mapTransferAirportPickup(TransportVO transportVO,
                                        XMLGregorianCalendar transferDate,
                                        TPickUpAirport pickUpAirport) {
    transportVO.setPickupLocation(locationMapper.mapAirport(pickUpAirport.getAirport()));
    transportVO.setPickupDate(mapTimestamp(transferDate, pickUpAirport.getEstimatedArrival()));
  }

  private void mapTransferStationPickup(TransportVO transportVO,
                                        XMLGregorianCalendar transferDate,
                                        TPickUpStation pickUpStation) {
    transportVO.setPickupLocation(locationMapper.mapStation(pickUpStation.getStation()));
    transportVO.setPickupDate(mapTimestamp(transferDate, pickUpStation.getEstimatedArrival()));
  }

  private void mapTransferAccomondationPickup(TransportVO transportVO,
                                              XMLGregorianCalendar transferDate,
                                              TPickUpAccommodation pickUpAccommodation) {
    if (pickUpAccommodation.getPickUpFrom() != null) {
      transportVO.setPickupLocation(locationMapper.mapWhere(pickUpAccommodation.getPickUpFrom()));
    }
    else {
      transportVO.setPickupLocation(locationMapper.mapAddress(pickUpAccommodation.getPickUpAddress()));
    }
    transportVO.setPickupDate(mapTimestamp(transferDate, pickUpAccommodation.getPickUpTime()));
  }

  private void mapTransferPointsPickup(TransportVO transportVO,
                                       XMLGregorianCalendar trasferDate,
                                       TPickUpPoints pickUpPoints) {
    // use the first point
    if (pickUpPoints.getPickUpPoints().size() > 0) {
      transportVO.setPickupLocation(locationMapper.mapMeetingPoint(pickUpPoints.getPickUpPoints().get(0)));
    }

    transportVO.setPickupDate(mapTimestamp(trasferDate));
  }

  private void mapTransferOtherPickup(TransportVO transportVO, XMLGregorianCalendar transferDate, TPickUpOther other) {
    if (other.getPickUpFrom() != null) {
      transportVO.setPickupLocation(locationMapper.mapWhere(other.getPickUpFrom()));
    }
    else {
      transportVO.setPickupLocation(locationMapper.mapAddress(other.getPickUpAddress()));
    }
    transportVO.setPickupDate(mapTimestamp(transferDate, other.getPickUpTime()));
  }

  private void mapTransferAirportDropOff(TransportVO transportVO,
                                         XMLGregorianCalendar transferDate,
                                         TDropOffAirport dropOffAirport) {
    transportVO.setDropoffLocation(locationMapper.mapAirport(dropOffAirport.getAirport()));
    transportVO.setDropoffDate(mapTimestamp(transferDate));
  }

  private void mapTransferStationDropOff(TransportVO transportVO,
                                         XMLGregorianCalendar transferDate,
                                         TDropOffStation dropOffStation) {
    transportVO.setDropoffLocation(locationMapper.mapStation(dropOffStation.getStation()));
    transportVO.setDropoffDate(mapTimestamp(transferDate));
  }

  private void mapTransferAccomondationDropOff(TransportVO transportVO,
                                               XMLGregorianCalendar transferDate,
                                               TDropOffAccommodation dropOffAccommodation) {
    if (dropOffAccommodation.getDropOffTo() != null) {
      transportVO.setDropoffLocation(locationMapper.mapWhere(dropOffAccommodation.getDropOffTo()));
    }
    else {
      transportVO.setDropoffLocation(locationMapper.mapAddress(dropOffAccommodation.getDropOffAddress()));
    }
    transportVO.setDropoffDate(mapTimestamp(transferDate));
  }

  private void mapTransferPointsDropOff(TransportVO transportVO,
                                        XMLGregorianCalendar trasferDate,
                                        TDropOffPoints dropOffPoints) {
    // use the first point
    if (dropOffPoints.getDropOffPoints().size() > 0) {
      transportVO.setDropoffLocation(locationMapper.mapMeetingPoint(dropOffPoints.getDropOffPoints().get(0)));
    }

    transportVO.setDropoffDate(mapTimestamp(trasferDate));
  }

  private void mapTransferOtherDropOff(TransportVO transportVO,
                                       XMLGregorianCalendar transferDate,
                                       TDropOffOther other) {
    if (other.getDropOffTo() != null) {
      transportVO.setDropoffLocation(locationMapper.mapWhere(other.getDropOffTo()));
    }
    else {
      transportVO.setDropoffLocation(locationMapper.mapAddress(other.getDropOffAddress()));
    }
    transportVO.setDropoffDate(mapTimestamp(transferDate));
  }

  private boolean isCancelled(TStatus status) {
    if (status != null) {
      return StringUtils.equalsIgnoreCase(STATUS_CANCELLED, status.getValue());
    }
    else {
      return false;
    }
  }


  String mapChargeConditions(TSearchChargeConditionsResponse response) {
    if (response != null && response.getChargeConditions() != null) {
      List<TChargeCondition> chargeConditions = response.getChargeConditions().getChargeConditions();
      TPassengerNameChange nameChange = response.getChargeConditions().getPassengerNameChange();

      StringBuilder builder = new StringBuilder();
      builder.append("\n");
      for (TChargeCondition condition : chargeConditions) {
        List<String> chargePolicies = mapChargeCondition(condition.getConditions());
        if (!CollectionUtils.isEmpty(chargePolicies)) {
          if (StringUtils.equals("cancellation", condition.getType())) {
            builder.append(" Booking Cancellation:\n");
          }
          else if (StringUtils.equals("amendment", condition.getType())) {
            builder.append(" Booking Change:\n");
          }

          for (String pol : chargePolicies) {
            builder.append(" ");
            builder.append(pol);
            builder.append("\n");
          }
          builder.append("\n");
        }
      }

      if (nameChange != null) {
        if (!nameChange.isAllowable()) {
          builder.append(" Traveler name change: Not allowed");
        }
        else {
          builder.append(" Traveler name change: ");
          addDateRange(builder, nameChange.getFromDate(), nameChange.getToDate());
          builder.append("Allowed");
        }
      }
      return builder.toString();
    }
    return null;
  }

  List<String> mapChargeCondition(List<TCondition> conditions) {
    List<String> chargePolicies = new ArrayList<>();
    for (TCondition c : conditions) {
      StringBuilder builder = new StringBuilder();
      XMLGregorianCalendar fromDate = c.getFromDate();
      XMLGregorianCalendar toDate = c.getToDate();

      addDateRange(builder, fromDate, toDate);
      if (c.isAllowable() != null && !c.isAllowable()) {
        builder.append("Not allowed");
      }
      else if (!c.isCharge()) {
        builder.append("No charge");
      }
      else {
        builder.append(mapPrice(c.getChargeAmount()));
        builder.append(" ");
        builder.append(c.getCurrency());
      }
      chargePolicies.add(builder.toString());
    }
    return chargePolicies;
  }

  void addDateRange(StringBuilder builder, XMLGregorianCalendar fromDate, XMLGregorianCalendar toDate) {

    if (toDate == null && fromDate!=null) {
      builder.append("- On and Before ");
      builder.append(formatDate(fromDate));
      builder.append(" : ");
    }
    else if (toDate != null && fromDate != null){
      builder.append("- From ");
      builder.append(formatDate(toDate));
      builder.append(" to ");
      builder.append(formatDate(fromDate));
      builder.append(" : ");
    }
  }

  String formatDate(XMLGregorianCalendar calendar) {
    if (calendar != null) {
      SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
      return formatter.format(calendar.toGregorianCalendar().getTime());
    } else {
      return "";
    }
  }

  public TravelBoundItemInfoKey getItemInfoKey(TBookingItem item) {
    TItemCity city = item.getItemCity();
    TItem itemCode = item.getItem();
    if (city != null && itemCode != null) {
      return new TravelBoundItemInfoKey(city.getCode(), itemCode.getCode());
    } else {
      return null;
    }
  }

  static class Passenger {
    int paxId;
    String name;
    TPaxType type;
    Integer childAge;

    public Passenger(int paxId, String name, TPaxType type, Integer childAge) {
      this.paxId = paxId;
      this.name = name;
      this.type = type;
      this.childAge = childAge;
    }
  }


  public static void main(String[] args)
      throws Exception {
    TravelboundBookingMapper mapper = new TravelboundBookingMapper();
    XMLGregorianCalendar cal = DatatypeFactory.newInstance().newXMLGregorianCalendar("2017-05-11");
    String time = "07.30";
    System.out.println(mapper.mapTimestamp(cal, time));
  }
}

