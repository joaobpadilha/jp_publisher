package com.umapped.external.travelbound;

import com.google.inject.Singleton;
import com.umapped.external.travelbound.api.*;
import org.apache.commons.lang.StringUtils;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by wei on 2017-05-08.
 */
@Singleton public class TravelboundRequestBuilder {
  public TRequestDetails requestDetails(JAXBElement indivRequest) {
    TRequestDetails requestDetails = new TRequestDetails();
    requestDetails.getSearchHotelPricePaxRequestsAndAddBookingItemRequestsAndAddBookingRequests().add(indivRequest);
    return requestDetails;
  }

  public TRequestDetails requestDetails(List<JAXBElement<?>> requests) {
    TRequestDetails requestDetails = new TRequestDetails();
    requestDetails.getSearchHotelPricePaxRequestsAndAddBookingItemRequestsAndAddBookingRequests().addAll(requests);
    return requestDetails;
  }

  public JAXBElement<TSearchBookingRequest> createSearchBookingRequest(TBookingReference bookingRef) {
    TSearchBookingRequest searchBookingRequest = new TSearchBookingRequest();

    searchBookingRequest.setBookingReference(bookingRef);
    return new JAXBElement<>(new QName("", "SearchBookingRequest"), TSearchBookingRequest.class, searchBookingRequest);
  }

  public JAXBElement<TSearchBookingItemRequest> createSearchBookingItemRequest(TBookingReference bookingRef) {
    TSearchBookingItemRequest searchBookingItemRequest = new TSearchBookingItemRequest();

    searchBookingItemRequest.setBookingReference(bookingRef);
    return new JAXBElement<>(new QName("", "SearchBookingItemRequest"),
                             TSearchBookingItemRequest.class,
                             searchBookingItemRequest);
  }

  public List<JAXBElement<?>> createSearchBookingItemRequests(Set<String> apiBookingIds) {
    List<JAXBElement<?>> requests = new ArrayList<>();
    for (String id : apiBookingIds) {
      TBookingReference reference = new TBookingReference();
      reference.setReferenceSource(TReferenceSource.API);
      reference.setValue(id);
      TSearchBookingItemRequest searchBookingItemRequest = new TSearchBookingItemRequest();

      searchBookingItemRequest.setBookingReference(reference);
      requests.add(new JAXBElement<>(new QName("", "SearchBookingItemRequest"),
                                     TSearchBookingItemRequest.class,
                                     searchBookingItemRequest));
    }

    return requests;
  }

  public JAXBElement<TSearchItemInformationRequest> createSearchItemInformationRequest(TItemType type,
                                                                                       String cityCode,
                                                                                       String itemCode) {
    TSearchItemInformationRequest searchItemInformationRequest = new TSearchItemInformationRequest();

    searchItemInformationRequest.setItemType(type);
    TItemDestination destination = new TItemDestination();
    destination.setDestinationType(TDestinationType.CITY);
    destination.setDestinationCode(cityCode);
    searchItemInformationRequest.setItemDestination(destination);
    if (StringUtils.isNotEmpty(itemCode)) {
      searchItemInformationRequest.setItemCode(itemCode);
    }
    return new JAXBElement<TSearchItemInformationRequest>(new QName("", "SearchItemInformationRequest"),
                                                          TSearchItemInformationRequest.class,
                                                          searchItemInformationRequest);
  }

  public JAXBElement<TSearchCountryRequest> createSearchCountryRequest() {
    TSearchCountryRequest searchCountryRequest = new TSearchCountryRequest();
    return new JAXBElement<TSearchCountryRequest>(new QName("", "SearchCountryRequest"),
                                                  TSearchCountryRequest.class,
                                                  searchCountryRequest);
  }

  public JAXBElement<TSearchCityRequest> createSearchCityRequest(String country) {
    TSearchCityRequest searchCityRequest = new TSearchCityRequest();
    searchCityRequest.setCountryCode(country);
    return new JAXBElement<TSearchCityRequest>(new QName("", "SearchCityRequest"),
                                               TSearchCityRequest.class,
                                               searchCityRequest);
  }

  public JAXBElement<TSearchChargeConditionsRequest> createSearchChargeConditionRequest(String apiBookingId,
                                                                                        int referenceNumber) {
    TSearchChargeConditionsRequest request = new TSearchChargeConditionsRequest();
    TChargeConditionsBookingItem item = new TChargeConditionsBookingItem();
    TBookingReference bookingReference = new TBookingReference();
    bookingReference.setReferenceSource(TReferenceSource.API);
    bookingReference.setValue(apiBookingId);
    item.setBookingReference(bookingReference);
    item.setItemReference(referenceNumber);
    request.setChargeConditionsBookingItem(item);
    request.setDateFormatResponse(new TEmpty());
    return new JAXBElement<TSearchChargeConditionsRequest>(new QName("", "SearchChargeConditionsRequest"),
                                                           TSearchChargeConditionsRequest.class,
                                                           request);
  }

}
