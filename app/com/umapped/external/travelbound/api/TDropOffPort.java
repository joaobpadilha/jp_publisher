//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.05.08 at 02:53:54 PM EDT 
//


package com.umapped.external.travelbound.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_DropOffPort complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_DropOffPort">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="City" type="{}t_City" minOccurs="0"/>
 *         &lt;element name="DepartingTo" type="{}t_Input50" minOccurs="0"/>
 *         &lt;element name="ShipName" type="{}t_Input50" minOccurs="0"/>
 *         &lt;element name="ShippingCompany" type="{}t_Input50" minOccurs="0"/>
 *         &lt;element name="DepartureTime" type="{}t_DepartureTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_DropOffPort", propOrder = {
    "city",
    "departingTo",
    "shipName",
    "shippingCompany",
    "departureTime"
})
public class TDropOffPort {

    @XmlElement(name = "City")
    protected TCity city;
    @XmlElement(name = "DepartingTo")
    protected String departingTo;
    @XmlElement(name = "ShipName")
    protected String shipName;
    @XmlElement(name = "ShippingCompany")
    protected String shippingCompany;
    @XmlElement(name = "DepartureTime")
    protected TDepartureTime departureTime;

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link TCity }
     *     
     */
    public TCity getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link TCity }
     *     
     */
    public void setCity(TCity value) {
        this.city = value;
    }

    /**
     * Gets the value of the departingTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartingTo() {
        return departingTo;
    }

    /**
     * Sets the value of the departingTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartingTo(String value) {
        this.departingTo = value;
    }

    /**
     * Gets the value of the shipName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipName() {
        return shipName;
    }

    /**
     * Sets the value of the shipName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipName(String value) {
        this.shipName = value;
    }

    /**
     * Gets the value of the shippingCompany property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingCompany() {
        return shippingCompany;
    }

    /**
     * Sets the value of the shippingCompany property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingCompany(String value) {
        this.shippingCompany = value;
    }

    /**
     * Gets the value of the departureTime property.
     * 
     * @return
     *     possible object is
     *     {@link TDepartureTime }
     *     
     */
    public TDepartureTime getDepartureTime() {
        return departureTime;
    }

    /**
     * Sets the value of the departureTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link TDepartureTime }
     *     
     */
    public void setDepartureTime(TDepartureTime value) {
        this.departureTime = value;
    }

}
