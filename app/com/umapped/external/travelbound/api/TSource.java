//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.05.08 at 02:53:54 PM EDT 
//


package com.umapped.external.travelbound.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_Source complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_Source">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestorID" type="{}t_RequestorID"/>
 *         &lt;element name="RequestorPreferences" type="{}t_RequestorPreferences"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_Source", propOrder = {
    "requestorID",
    "requestorPreferences"
})
public class TSource {

    @XmlElement(name = "RequestorID", required = true)
    protected TRequestorID requestorID;
    @XmlElement(name = "RequestorPreferences", required = true)
    protected TRequestorPreferences requestorPreferences;

    /**
     * Gets the value of the requestorID property.
     * 
     * @return
     *     possible object is
     *     {@link TRequestorID }
     *     
     */
    public TRequestorID getRequestorID() {
        return requestorID;
    }

    /**
     * Sets the value of the requestorID property.
     * 
     * @param value
     *     allowed object is
     *     {@link TRequestorID }
     *     
     */
    public void setRequestorID(TRequestorID value) {
        this.requestorID = value;
    }

    /**
     * Gets the value of the requestorPreferences property.
     * 
     * @return
     *     possible object is
     *     {@link TRequestorPreferences }
     *     
     */
    public TRequestorPreferences getRequestorPreferences() {
        return requestorPreferences;
    }

    /**
     * Sets the value of the requestorPreferences property.
     * 
     * @param value
     *     allowed object is
     *     {@link TRequestorPreferences }
     *     
     */
    public void setRequestorPreferences(TRequestorPreferences value) {
        this.requestorPreferences = value;
    }

}
