package com.umapped.external.travelbound;

import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.mapped.publisher.persistence.*;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.S3Util;
import com.umapped.external.travelbound.api.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.util.CollectionUtils;

import javax.xml.bind.*;
import javax.xml.namespace.QName;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by wei on 2017-05-18.
 */

public class TravelboundPoiImportLoader {
  public static String TRAVELBOUND_HOTEL_SRC = "Travelbound Hotel";

  private JAXBContext jaxbContext;

  public TravelboundPoiImportLoader() {
    try {
      jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
    }
    catch (JAXBException e) {
      Log.err("Fail to create JAXBContext", e);
      throw new RuntimeException(e);
    }
  }

  public Pair<Integer, Integer> load(String countryCode, List<String> cities, String path) {
    CountriesInfo.Country country = CountriesInfo.Instance().byAlpha2(countryCode);
    if (country == null) {
      country = new CountriesInfo.Country(countryCode, countryCode, new ArrayList<String>());
      Log.err("Unknow Country: " + countryCode );
    }
    int successCount = 0;
    int failedCount = 0;
    for (String city : cities) {
        Pair<Integer, Integer> result = load(country, city, path);
        successCount += result.getLeft();
        failedCount += result.getRight();
    }
    Log.debug(String.format("Country: %s, Success: %s, Fail: %s", countryCode, successCount, failedCount));
    return new ImmutablePair<>(successCount, failedCount);
  }

  public Pair<Integer, Integer> load(CountriesInfo.Country country, String city, String path) {
    List<S3ObjectSummary> items = getAllItems(city, path);
    int successCount = 0;
    int failedCount = 0;
    for (S3ObjectSummary i : items) {
      try {
        load(country, i.getKey());
        successCount++;
      }
      catch (Exception e) {
        Log.err("Fail to load travelbound hotel: " + i.getKey(), e);
        failedCount++;
      }
    }
    return new ImmutablePair<>(successCount, failedCount);
  }

  public List<S3ObjectSummary> getAllItems(String city, String path) {
    String prefix = path + city + "_";

    List<S3ObjectSummary> files = S3Util.getObjectList(S3Util.getPDFBucketName(), prefix);

    return files;
  }


  public void load(CountriesInfo.Country country, String filePath)
      throws Exception {

    String bucketName = S3Util.getPDFBucketName();

    S3Object hotel = S3Util.getS3File(bucketName, filePath);

    TItemDetail itemDetail = getItemDetail(new InputStreamReader(hotel.getObjectContent()));
    PoiImportRS pirs = map(country, itemDetail);

    PoiImportMgr.save(pirs);

    S3Util.deleteS3File(bucketName, filePath);
  }

  public PoiImportRS map(CountriesInfo.Country country, TItemDetail itemDetail)
      throws JAXBException {
    Marshaller marshaller = jaxbContext.createMarshaller();
    PoiImportRS pirs = new PoiImportRS();
    pirs.state = PoiImportRS.ImportState.UNPROCESSED;
    pirs.srcId = FeedSourcesInfo.byName(TRAVELBOUND_HOTEL_SRC);
    pirs.poiId = null;
    pirs.typeId = PoiTypeInfo.Instance().byName("Accommodations").getId();
    pirs.timestamp = System.currentTimeMillis();
    String id = itemDetail.getCity().getCode() + "_" + itemDetail.getItem().getCode();

    pirs.srcReference = id;
    pirs.srcFilename = null;
    pirs.name = itemDetail.getItem().getValue();
    pirs.countryName = country.getName(CountriesInfo.LangCode.EN);
    pirs.countryCode = country.getAlpha3();
    TAddressLines addressLines = itemDetail.getHotelInformation().getAddressLines();
    if (addressLines != null) {
      pirs.phone = addressLines.getTelephone();
      pirs.region = addressLines.getAddressLine3();
      pirs.city = itemDetail.getCity().getValue();
      if (StringUtils.isEmpty(addressLines.getAddressLine2())) {
        pirs.address = addressLines.getAddressLine1();
      }
      else {
        pirs.address = StringUtils.join(addressLines.getAddressLine1(), ", ", addressLines.getAddressLine2());
      }
    }
    StringWriter writer = new StringWriter();
    marshaller.marshal(new JAXBElement<TItemDetail>(new QName("", "ItemDetail"), TItemDetail.class, itemDetail),
                       writer);
    pirs.raw = writer.toString();
    pirs.hash = DigestUtils.md5Hex(pirs.raw);

    return pirs;
  }

  public TItemDetail getItemDetail(Reader reader)
      throws JAXBException {
    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
    Response response = (Response) unmarshaller.unmarshal(reader);
    TSearchItemInformationResponse itemResponse = (TSearchItemInformationResponse) response.getResponseDetails()
                                                                                           .getSearchHotelPricePaxResponsesAndApartmentPriceBreakdownResponsesAndAvailabilityCacheResponses()
                                                                                           .get(0)
                                                                                           .getValue();
    return itemResponse.getItemDetails().getItemDetails().get(0);
  }
}
