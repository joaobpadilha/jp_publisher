package com.umapped.external.flightcenter;

import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public enum FlightCenterBrand {
    USA_GOGO("us/gogo"),
    USA_EUROBREAKS("us/eurobreaks"),
    USA_DISCOVERIES("us/discoveries"),
    USA_CANADABREAKS("us/canadabreaks"),
    USA_USABREAKS("us/usabreaks"),
    USA_ESCAPES("us/escapes"),
    USA_JOURNEYS("us/journeys"),
    USA_WORLDWIDETRAVELER("us/worldwidetraveler"),
    CAN_GETAWAYS("ca/getaways"),
    CAN_EUROBREAKS("ca/eurobreaks"),
    CAN_JOURNEYS("ca/journeys"),
    CAN_ESCAPES("ca/escapes");

    private String brandName;

    private FlightCenterBrand(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandName() {
        return brandName;
    }
}
