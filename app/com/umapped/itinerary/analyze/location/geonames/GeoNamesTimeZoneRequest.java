package com.umapped.itinerary.analyze.location.geonames;

import com.umapped.itinerary.analyze.model.Location;

/**
 * Created by wei on 2017-03-22.
 */
public class GeoNamesTimeZoneRequest extends GeoNamesAPIRequest {
  public void setGeoCoordinates(Location.GeoLocation coordinates) {
    addParam(GeoNamesWebServiceRequestHandler.PARAM_LATITUDE, Double.toString(coordinates.getLatitude()));
    addParam(GeoNamesWebServiceRequestHandler.PARAM_LONGITUDE, Double.toString(coordinates.getLongitude()));
  }
}
