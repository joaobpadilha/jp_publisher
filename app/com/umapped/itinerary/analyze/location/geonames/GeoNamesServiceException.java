package com.umapped.itinerary.analyze.location.geonames;

/**
 * Created by wei on 2017-03-22.
 */
public class GeoNamesServiceException
    extends RuntimeException{
  public GeoNamesServiceException() {
  }

  public GeoNamesServiceException(String message) {
    super(message);
  }

  public GeoNamesServiceException(String message, Throwable cause) {
    super(message, cause);
  }

  public GeoNamesServiceException(Throwable cause) {
    super(cause);
  }

  public GeoNamesServiceException(String message,
                                  Throwable cause,
                                  boolean enableSuppression,
                                  boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
