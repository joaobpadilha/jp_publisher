package com.umapped.itinerary.analyze.location.geonames;

import com.google.inject.Inject;
import com.umapped.itinerary.analyze.location.TimeZoneLookupService;
import com.umapped.itinerary.analyze.model.Location;
import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;
import java.time.ZoneId;
import java.util.Map;

/**
 * Created by wei on 2017-03-22.
 */
public class GeoNamesTimeZoneLookupService
    implements TimeZoneLookupService {

  private GeoNamesWebServiceRequestHandler requestHandler;

  @Inject public GeoNamesTimeZoneLookupService(GeoNamesWebServiceRequestHandler requestHandler) {
    this.requestHandler = requestHandler;
  }

  @Override public ZoneId lookup(Location.GeoLocation location) {
    GeoNamesTimeZoneRequest request = new GeoNamesTimeZoneRequest();
    request.setGeoCoordinates(location);
    try {
      Map<String, Object> result = requestHandler.call(GeoNamesWebServiceRequestHandler.TIME_ZONE_API_END_POINT,
                                                       request.buildQueryString());
      String zoneIdStr = (String) result.get("timezoneId");
      if (StringUtils.isEmpty(zoneIdStr)) {
        return null;
      } else {
        return ZoneId.of(zoneIdStr);
      }
    }
    catch (UnsupportedEncodingException e) {
      throw new GeoNamesServiceException(e);
    }

  }
}
