package com.umapped.itinerary.analyze.location.google;

/**
 * Created by wei on 2017-03-22.
 */
public class GoogleAPIException extends RuntimeException {
  public GoogleAPIException() {
  }

  public GoogleAPIException(String message) {
    super(message);
  }

  public GoogleAPIException(String message, Throwable cause) {
    super(message, cause);
  }

  public GoogleAPIException(Throwable cause) {
    super(cause);
  }

}
