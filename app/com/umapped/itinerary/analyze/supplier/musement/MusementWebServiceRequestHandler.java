package com.umapped.itinerary.analyze.supplier.musement;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mapped.publisher.utils.Log;
import com.umapped.itinerary.analyze.webservice.HttpResponseException;
import com.umapped.itinerary.analyze.webservice.JsonWebServiceRequestHandler;
import com.umapped.itinerary.analyze.webservice.WebAPIServiceRequest;
import org.apache.commons.lang.StringUtils;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wei on 2017-06-14.
 */
@Singleton
public class MusementWebServiceRequestHandler extends JsonWebServiceRequestHandler {
  private static final String SERVER_URL = "http://developers.musement.com/api/v3/";

  private static final String LOGIN = "login";
  //TODO: moved to configuration
  private static String ClientId = "52_1qasyczge3s0o4kso0kg800c8wggosswkwwgs0w8g4ocoogs0c";
  private static String ClientSecret = "52hwidlzz9gkcowgo0wko8wgkw0scckkwcwow8wgs4w00s8w44";
  private static String GrantType = "client_credentials";

  private static String CURRENCY_HEADER = "X-Musement-Currency";

  private String accessToken = "";
  private long   lastUpdateTs = 0;

  @Override protected String getServerUrl() {
    return SERVER_URL;
  }

  @Inject
  public MusementWebServiceRequestHandler(ObjectMapper objectMapper) {
    super(objectMapper);
  }

  @Override public <T> T get(String endPoint, WebAPIServiceRequest serviceRequest, Class<T> responseClass)
      throws HttpResponseException {
    if (StringUtils.isEmpty(accessToken)) {
      login();
    }
    try {
      serviceRequest.addHeader("Authorization", "Bearer " + accessToken);
      serviceRequest.addHeader(CURRENCY_HEADER, "USD");
      return super.get(endPoint, serviceRequest, responseClass);
    } catch (HttpResponseException e) {
      // Unauthorized
      if (e.getStatusCode() == 401) {
        Log.err("Unauthorized, login first ...");
        try {
          login();
        } catch (HttpResponseException ex) {
          throw ex;
        }
      }
      // rerun the request after login
      serviceRequest.addHeader("Authorization", "Bearer " + accessToken);
      return super.get(endPoint, serviceRequest, responseClass);
    }
  }

  private synchronized void login() throws HttpResponseException {
    WebAPIServiceRequest loginRequest = new WebAPIServiceRequest();
    loginRequest.addParam("client_id", ClientId);
    loginRequest.addParam("client_secret", ClientSecret);
    loginRequest.addParam("grant_type", "client_credentials");
    try {
      Map<String, Object> loginResult = super.get(LOGIN, loginRequest, HashMap.class);
      accessToken = (String) loginResult.get("access_token");
      return;
    } catch (HttpResponseException e) {
      Log.err("Fail to login to Musement API, status code: " + e.getStatusCode());
      throw e;
    }
  }
}
