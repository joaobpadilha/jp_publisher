package com.umapped.itinerary.analyze.supplier.musement.response;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by wei on 2017-06-14.
 */
@JsonIgnoreProperties("metadata")
public class SearchEventResponse {

  @JsonProperty("data")
  private List<Event> data;

  public List<Event> getData() {
    return data;
  }

  public void setData(List<Event> data) {
    this.data = data;
  }
}
