package com.umapped.itinerary.analyze.supplier.musement.response;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;
import com.umapped.itinerary.analyze.model.Location;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import javax.inject.Singleton;
import java.util.List;

/**
 * Created by wei on 2017-03-31.
 */
@Singleton
public class MusementCityLookup {

  private static final String City_Lookup = "SELECT m.musement_city_id as city_id, u.name as name, u.country" +
               " FROM musement_city m, um_city u WHERE m.um_city_id=:um_city_id and m.um_city_id=u.id";

  private static final String MUSEMENT_City_ID_Lookup = "SELECT musement_city_id as city_id " +
                                            " FROM musement_city WHERE um_city_id=:um_city_id";

  public MusementCityLookup() {
  }


  public String getCityId(String umappedCityId) {
    SqlRow row = Ebean.createSqlQuery(MUSEMENT_City_ID_Lookup).setParameter("um_city_id", umappedCityId).findUnique();
    if (row != null) {
      return row.getString("city_id");
    } else {
      return null;
    }
  }

  public Pair<String, String> getCity(String umappedCityId) {
    SqlRow row = Ebean.createSqlQuery(City_Lookup).setParameter("um_city_id", umappedCityId).findUnique();
    if (row != null) {
      return new ImmutablePair<>(row.getString("city_id"), row.getString("name"));
    } else {
      return null;
    }
  }
}
