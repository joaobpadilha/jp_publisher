//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.04.17 at 04:55:33 PM EDT 
//


package com.umapped.itinerary.analyze.supplier.travelbound.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_HotelRoom complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_HotelRoom">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaxIds" type="{}t_PaxIds"/>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{}a_HotelRoom"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_HotelRoom", propOrder = {
    "description",
    "paxIds"
})
public class THotelRoom {

    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "PaxIds", required = true)
    protected TPaxIds paxIds;
    @XmlAttribute(name = "Code")
    protected String code;
    @XmlAttribute(name = "Id")
    protected String id;
    @XmlAttribute(name = "ExtraBed")
    protected Boolean extraBed;
    @XmlAttribute(name = "NumberOfExtraBeds")
    protected Integer numberOfExtraBeds;
    @XmlAttribute(name = "SharingBedding")
    protected Boolean sharingBedding;
    @XmlAttribute(name = "NumberOfCots")
    protected Integer numberOfCots;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the paxIds property.
     * 
     * @return
     *     possible object is
     *     {@link TPaxIds }
     *     
     */
    public TPaxIds getPaxIds() {
        return paxIds;
    }

    /**
     * Sets the value of the paxIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link TPaxIds }
     *     
     */
    public void setPaxIds(TPaxIds value) {
        this.paxIds = value;
    }

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the extraBed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExtraBed() {
        return extraBed;
    }

    /**
     * Sets the value of the extraBed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExtraBed(Boolean value) {
        this.extraBed = value;
    }

    /**
     * Gets the value of the numberOfExtraBeds property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfExtraBeds() {
        return numberOfExtraBeds;
    }

    /**
     * Sets the value of the numberOfExtraBeds property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfExtraBeds(Integer value) {
        this.numberOfExtraBeds = value;
    }

    /**
     * Gets the value of the sharingBedding property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSharingBedding() {
        return sharingBedding;
    }

    /**
     * Sets the value of the sharingBedding property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSharingBedding(Boolean value) {
        this.sharingBedding = value;
    }

    /**
     * Gets the value of the numberOfCots property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfCots() {
        return numberOfCots;
    }

    /**
     * Sets the value of the numberOfCots property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfCots(Integer value) {
        this.numberOfCots = value;
    }

}
