package com.umapped.itinerary.analyze.supplier.shoretrip;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;
import com.umapped.itinerary.analyze.model.Location;

import java.util.List;

/**
 * Created by wei on 2017-04-05.
 */
public class PortLookup {

  private static final String City_Lookup = "SELECT port, name, ST_Distance(location, ST_MakePoint(:lng, :lat)) distance " +
                                            " FROM shoretrip_port WHERE ST_Distance(location, ST_MakePoint(:lng, :lat)) <= :radius * 1000 order by distance asc";


  public PortLookup() {
  }

  public String getPort(Location.GeoLocation location) {
    List<SqlRow> rows = Ebean.createSqlQuery(City_Lookup)
                             .setParameter("lng", location.getLongitude())
                             .setParameter("lat", location.getLatitude())
                             .setParameter("radius", 50).findList();
    if (rows.size() > 0) {
      return rows.get(0).getString("port");
    } else {
      return null;
    }
  }
}
