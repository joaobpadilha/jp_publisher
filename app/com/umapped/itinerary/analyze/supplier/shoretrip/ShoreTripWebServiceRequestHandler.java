package com.umapped.itinerary.analyze.supplier.shoretrip;

import com.umapped.itinerary.analyze.webservice.WebServiceRequestHandler;

/**
 * Created by wei on 2017-04-05.
 */
public class ShoreTripWebServiceRequestHandler extends WebServiceRequestHandler{
  private static String SERVER_URL = "https://www.shoretrips.com/integration/gateway/";

  @Override protected String getServerUrl() {
    return SERVER_URL;
  }
}
