package com.umapped.itinerary.analyze.supplier;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.umapped.itinerary.analyze.TripFeature;
import com.umapped.itinerary.analyze.segment.SegmentNature;
import com.umapped.itinerary.analyze.segment.TripSegment;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeature;
import com.umapped.itinerary.analyze.supplier.shoretrip.ShoreTripProvider;
import com.umapped.itinerary.analyze.supplier.travelbound.TraveBoundProvider;
import com.umapped.itinerary.analyze.supplier.wcities.WCitiesProivder;

import java.util.EnumMap;

/**
 * Created by wei on 2017-04-20.
 */
@Singleton
public class ProviderRepositoryImpl implements ProviderRepository {
  private WCitiesProivder wCitiesProivder;
  private ShoreTripProvider shoreTripProvider;
  private TraveBoundProvider traveBoundProvider;

  @Inject
  public ProviderRepositoryImpl(WCitiesProivder wCitiesProivder,
                                ShoreTripProvider shoreTripProvider,
                                TraveBoundProvider traveBoundProvider) {
    this.wCitiesProivder = wCitiesProivder;
    this.shoreTripProvider = shoreTripProvider;
    this.traveBoundProvider = traveBoundProvider;
  }

  @Override
  public AccommondationProvider getAccommodationProvider(DecisionResult decisionResult, TripSegment segment,
                                                         EnumMap<TripFeature, Object> tripFeatures,
                                                         EnumMap<SegmentFeature, Object> segmentFeatures) {
    return wCitiesProivder;
  }

  @Override
  public ActivityProvider getActivityProvider(DecisionResult decisionResult,  TripSegment segment,
                                              EnumMap<TripFeature, Object> tripFeatures,
                                              EnumMap<SegmentFeature, Object> segmentFeatures) {
    if (segment.hasNature(SegmentNature.Cruise)) {
      return shoreTripProvider;
    } else {
      return wCitiesProivder;
    }
  }

  @Override
  public ContentProvider getContentProvider(DecisionResult decisionResult, TripSegment segment,
                                            EnumMap<TripFeature, Object> tripFeatures,
                                            EnumMap<SegmentFeature, Object> segmentFeatures) {
    return wCitiesProivder;
  }
}
