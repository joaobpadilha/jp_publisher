package com.umapped.itinerary.analyze.supplier;

import com.google.inject.Singleton;
import com.umapped.itinerary.analyze.TripFeature;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.recommendation.MergeRankingStrategy;
import com.umapped.itinerary.analyze.recommendation.RankingStrategy;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;

import java.time.Duration;
import java.util.EnumMap;

/**
 * Created by wei on 2017-04-20.
 */
@Singleton
public class ProviderHelper {
  private RankingStrategy defaultRankingStrategy = new MergeRankingStrategy();
  
  public Location findLocation(DecisionResult dr) {
    // favor the location from the end
    for (int i = dr.getLocations().size() - 1; i >= 0; i--) {
      Location loc = dr.getLocations().get(i);
      if (loc.getGeoLocation() != null) {
        return loc;
      }
    }
    return null;
  }

  public long getDurationDays(DecisionResult dr) {
    return Duration.between(dr.getStartDate(), dr.getEndDate()).toDays();
  }

  public RankingStrategy getRankingStrategy(EnumMap<TripFeature, Object> features, DecisionResult dr) {
    return defaultRankingStrategy;
  }
}
