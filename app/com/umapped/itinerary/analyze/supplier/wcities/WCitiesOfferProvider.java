package com.umapped.itinerary.analyze.supplier.wcities;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.umapped.itinerary.analyze.recommendation.RecommendedItem;
import com.umapped.itinerary.analyze.recommendation.RecommendedItemType;
import com.umapped.service.offer.ContentFilter;
import com.umapped.service.offer.ContentOfferProvider;
import com.umapped.service.offer.OfferResult;
import com.umapped.service.offer.UMappedCity;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by wei on 2017-06-13.
 */
@Singleton
public class WCitiesOfferProvider implements ContentOfferProvider {
  @Inject
  private WCitiesWebService wCitiesWebService;

  @Inject
  WCitiesCityLookup cityLookup;

  @Override public OfferResult getContentOffers(ContentFilter filter) {
    UMappedCity city = filter.getLocation();
    if (city != null) {
      Pair<String, String> cityPair = cityLookup.getCity(city.getId());
      if (cityPair != null) {
        if(filter.getOfferType() == null || filter.getOfferType() == RecommendedItemType.Content) {
          List<RecommendedItem> items = wCitiesWebService.getGuides(cityPair);
          if (items != null && items.size() > 1) {
            Collections.sort(items, new Comparator<RecommendedItem>() {
              @Override public int compare(RecommendedItem o1, RecommendedItem o2) {
                WCitiesGuideEnum enum1 = WCitiesGuideEnum.getFromLabel(o1.name);
                WCitiesGuideEnum enum2 = WCitiesGuideEnum.getFromLabel(o2.name);
                if (enum1 != null && enum2 != null) {
                  return enum1.compareTo(enum2);
                }
                return 0;
              }
            });
          }
          return new OfferResult(filter.getStayPeriods(), items);
        } else {

          List<RecommendedItem> items = wCitiesWebService.getRecords(cityPair, filter);

          OfferResult result = new OfferResult(filter.getStayPeriods(), items);
          //set pagination
          if (items != null && items.size() == Constants.MAX_W_CITIES) {
            //there is more so let's figure out the next start
            result.setNextPos(filter.getStartPos() + Constants.MAX_W_CITIES);
          }
          if (filter.getStartPos() >= Constants.MAX_W_CITIES) {
            result.setPrevPos(filter.getStartPos() - Constants.MAX_W_CITIES);
          }
          if (filter.getKeyword() != null && !filter.getKeyword().isEmpty()) {
            result.setKeyword(filter.getKeyword());
          }
          return result;
        }
      }
    }
    return null;
  }
}
