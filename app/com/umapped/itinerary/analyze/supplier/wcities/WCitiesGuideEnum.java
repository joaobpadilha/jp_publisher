package com.umapped.itinerary.analyze.supplier.wcities;

import com.umapped.persistence.enums.ReservationType;

public enum  WCitiesGuideEnum {
  CITY_BLURB(0, "City Blurb"),
  DISTRICT_GUIDE(1, "District Guide"),
  HISTORICAL_BACKGROUND(2, "Historical Background"),
  GETTING_THERE(3, "Getting There/Around"),
  DINING_AND_DRINKING(4, "Dining and Drinking"),
  ENTERTAINMENT(5, "Entertainment"),
  FUN_FACTS(6, "Fun Facts"),
  RECOMMENDED_TOURS(7, "Recommended Tours"),
  WHERE_TO_STAY(8, "Where To Stay");


  private int id;
  private String label;

  private WCitiesGuideEnum(int dbIdx, String label) {
    this.id = dbIdx;
    this.label = label;
  }

  public boolean equals(String label) {
    if (label != null && this.label.equals(label)) {
      return true;
    }
    return false;
  }

  public static WCitiesGuideEnum getFromLabel(String name) {
    if (name != null) {
      for (WCitiesGuideEnum b : WCitiesGuideEnum.values()) {
        if (b.label != null && b.label.compareToIgnoreCase(name) == 0) {
          return b;
        }
      }
    }
    return null;

  }



}
