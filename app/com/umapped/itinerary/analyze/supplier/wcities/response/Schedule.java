
package com.umapped.itinerary.analyze.supplier.wcities.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "venue_id",
    "event_time",
    "dates",
    "bookinginfo"
})
public class Schedule {

    @JsonProperty("venue_id")
    private String venueId;
    @JsonProperty("event_time")
    private String eventTime;
    @JsonProperty("dates")
    private List<Dates> dates;
    @JsonProperty("bookinginfo")
    private Bookinginfo bookinginfo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("venue_id")
    public String getVenueId() {
        return venueId;
    }

    @JsonProperty("venue_id")
    public void setVenueId(String venueId) {
        this.venueId = venueId;
    }

    @JsonProperty("event_time")
    public String getEventTime() {
        return eventTime;
    }

    @JsonProperty("event_time")
    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    @JsonProperty("dates")
    public List<Dates> getDates() {
        return dates;
    }

    @JsonProperty("dates")
    public void setDates(List<Dates> dates) {
        this.dates = dates;
    }

    @JsonProperty("bookinginfo")
    public Bookinginfo getBookinginfo() {
        return bookinginfo;
    }

    @JsonProperty("bookinginfo")
    public void setBookinginfo(Bookinginfo bookinginfo) {
        this.bookinginfo = bookinginfo;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
