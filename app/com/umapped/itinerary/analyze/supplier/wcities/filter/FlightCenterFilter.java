package com.umapped.itinerary.analyze.supplier.wcities.filter;

import com.umapped.itinerary.analyze.supplier.wcities.WCitiesGuideEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class FlightCenterFilter implements BaseFilter {

  public static  List<WCitiesGuideEnum> excluded =  new ArrayList<>();
  public static  List<WCitiesGuideEnum> always = new ArrayList<>();
  public static  List<WCitiesGuideEnum> random = new ArrayList<>();


  static {
    excluded.add(WCitiesGuideEnum.GETTING_THERE);
    excluded.add(WCitiesGuideEnum.RECOMMENDED_TOURS);
    excluded.add(WCitiesGuideEnum.WHERE_TO_STAY);
    always.add(WCitiesGuideEnum.CITY_BLURB);
    random.add(WCitiesGuideEnum.DINING_AND_DRINKING);
    random.add(WCitiesGuideEnum.DISTRICT_GUIDE);
    random.add(WCitiesGuideEnum.ENTERTAINMENT);
    random.add(WCitiesGuideEnum.FUN_FACTS);
    random.add(WCitiesGuideEnum.HISTORICAL_BACKGROUND);
  }

  public FlightCenterFilter () {

  }

  public List<WCitiesGuideEnum> getExcluded () {
    return excluded;
  }

  public List<WCitiesGuideEnum> getIncluded () {
    List<WCitiesGuideEnum> selected = new ArrayList<>();
    //add the always selected section
    selected.addAll(always);

    //add the randomly selected section
    if (random != null && random.size() > 0) {
      //randomly choose 1 from the included list
      int randomPos = ThreadLocalRandom.current().nextInt(0, random.size());
      if (randomPos < random.size()) {
        selected.add(random.get(randomPos));
      } else {
        selected.add(random.get(0));
      }
    }
    return selected;
  }

}
