package com.umapped.itinerary.analyze.adapters;

import com.mapped.publisher.parse.schemaorg.Airport;
import com.mapped.publisher.parse.schemaorg.FlightReservation;
import com.mapped.publisher.parse.schemaorg.Reservation;
import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.model.Location;

/**
 * Created by wei on 2017-03-06.
 */
public class FlightReservationAdapter
    extends AbstractReservationItinearyItemAdapter {
  @Override protected ItineraryItemType getItemType(Reservation reservation) {
    return ItineraryItemType.Flight;
  }


  @Override protected Location getStartLocation(Reservation reservation) {
    FlightReservation r = (FlightReservation) reservation;
    return getAirportLocation(r.reservationFor.departureAirport);
  }

  @Override protected Location getEndLocation(Reservation reservation) {
    FlightReservation r = (FlightReservation) reservation;
    return getAirportLocation(r.reservationFor.arrivalAirport);
  }

  private Location getAirportLocation(Airport airport) {
    Location location = new Location();

    location.setUmPoidId(airport.umId);
    location.fromPostalAddress(airport.address);
    location.setGeoLocation(Location.GeoLocation.build(airport.geo));
    location.setDescription(airport.name);
    return location;
  }
}
