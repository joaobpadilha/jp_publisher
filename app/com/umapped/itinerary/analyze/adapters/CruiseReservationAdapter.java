package com.umapped.itinerary.analyze.adapters;

import com.mapped.publisher.parse.schemaorg.CruiseShipReservation;
import com.mapped.publisher.parse.schemaorg.Reservation;
import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.model.Location;

/**
 * Created by wei on 2017-03-07.
 */
public class CruiseReservationAdapter
    extends AbstractReservationItinearyItemAdapter {
  @Override protected ItineraryItemType getItemType(Reservation reservation) {
    return ItineraryItemType.Cruise;
  }

  @Override protected Location getStartLocation(Reservation reservation) {
    CruiseShipReservation r = (CruiseShipReservation) reservation;
    return getLocationFromPlace(r.reservationFor.departPort);
  }

  @Override protected Location getEndLocation(Reservation reservation) {
    CruiseShipReservation r = (CruiseShipReservation) reservation;
    return getLocationFromPlace(r.reservationFor.arrivalPort);
  }
}
