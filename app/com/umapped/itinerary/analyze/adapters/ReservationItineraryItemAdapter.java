package com.umapped.itinerary.analyze.adapters;

import com.mapped.publisher.parse.schemaorg.Reservation;
import com.umapped.itinerary.analyze.model.ItineraryItem;

/**
 * Created by wei on 2017-03-06.
 */
public interface ReservationItineraryItemAdapter {
  ItineraryItem adapt(Reservation reservation);
}
