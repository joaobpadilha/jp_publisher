package com.umapped.itinerary.analyze.adapters;

import com.mapped.publisher.parse.schemaorg.Reservation;
import com.mapped.publisher.parse.schemaorg.TrainReservation;
import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.model.Location;

/**
 * Created by wei on 2017-03-07.
 */
public class TrainReservationAdapter extends AbstractReservationItinearyItemAdapter {
  @Override protected Location getStartLocation(Reservation reservation) {
    TrainReservation r = (TrainReservation) reservation;

    return getLocationFromPlace(r.reservationFor.departStation);
  }

  @Override protected Location getEndLocation(Reservation reservation) {
    TrainReservation r = (TrainReservation) reservation;
    return getLocationFromPlace(r.reservationFor.arrivalStation);
  }

  @Override protected ItineraryItemType getItemType(Reservation reservation) {
    long duration = duration(reservation);
    if (duration == -1 || duration <= 2) {
      return ItineraryItemType.ShortTransfer;
    } else {
      return ItineraryItemType.LongTransfer;
    }
  }
}
