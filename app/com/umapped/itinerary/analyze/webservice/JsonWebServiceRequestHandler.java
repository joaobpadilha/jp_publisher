package com.umapped.itinerary.analyze.webservice;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;

import java.io.IOException;

/**
 * Created by wei on 2017-03-30.
 */
abstract public class JsonWebServiceRequestHandler extends WebServiceRequestHandler{

  protected ObjectMapper objectMapper;

  public JsonWebServiceRequestHandler(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

   public <T> T get(String endPoint, WebAPIServiceRequest serviceRequest, Class<T> responseClass) throws HttpResponseException {
     return super.get(endPoint, serviceRequest, (is) -> {
       try {
         return objectMapper.readValue(is, responseClass);
       } catch (IOException e) {
         throw new RuntimeException(e);
       }
     });
   }
}
