package com.umapped.itinerary.analyze.recommendation;

import java.util.*;

/**
 * Created by wei on 2017-03-31.
 */
public class MergeRankingStrategy<T extends RecommendedItem> implements RankingStrategy<T> {
  @Override public List<T> rank(Map<String, List<T>> itemMap) {

    Set<String> nameSet = new HashSet<>();
    List<T> finalList = new ArrayList<T>();

    for (String key : itemMap.keySet()) {
      List<RecommendedItem> itemList = (List<RecommendedItem>) itemMap.get(key);
      for (RecommendedItem i : itemList) {
        if (!nameSet.contains(i.name)) {
          nameSet.add(i.name);
          finalList.add((T) i);
        }
      }
    }
    return finalList;
  }
}
