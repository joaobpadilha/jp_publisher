package com.umapped.itinerary.analyze.recommendation;

import com.umapped.itinerary.analyze.model.StayPeriod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wei on 2017-04-10.
 */
public class RecommendationList {
  private StayPeriod period;
  private List<RecommendedItem> items;

  public RecommendationList(StayPeriod period, List<RecommendedItem> items) {
    this.period = period;
    this.items = new ArrayList<>(items);
  }

  public StayPeriod getPeriod() {
    return period;
  }

  public List<RecommendedItem> getItems() {
    return items;
  }
}
