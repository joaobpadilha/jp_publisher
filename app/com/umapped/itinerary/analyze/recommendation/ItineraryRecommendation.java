package com.umapped.itinerary.analyze.recommendation;


import java.util.List;

/**
 * Created by wei on 2017-03-31.
 */
public class ItineraryRecommendation {
  private List<TripSegmentRecommendation> segmentRecommendations;

  private String itineraryId;

  public ItineraryRecommendation(String itineraryId) {
    this.itineraryId = itineraryId;
  }

  public List<TripSegmentRecommendation> getSegmentRecommendations() {
    return segmentRecommendations;
  }

  public ItineraryRecommendation setSegmentRecommendations(List<TripSegmentRecommendation> segmentRecommendations) {
    this.segmentRecommendations = segmentRecommendations;
    return this;
  }

  public RecommendationResultByDate getRecommendationsByDate() {
    RecommendationResultByDate result = new RecommendationResultByDate();
    for (TripSegmentRecommendation tsr : getSegmentRecommendations()) {
      result.add(tsr);
    }

    return result;
  }

  public RecommendationResultByPeriod getRecommendationByPeriod() {
    RecommendationResultByPeriod result = new RecommendationResultByPeriod();
    for (TripSegmentRecommendation tsr : getSegmentRecommendations()) {
      result.addHotel(tsr.getHotels());
      result.addActivity(tsr.getActivities());
      result.addGuide(tsr.getGuides());
    }

    return result;
  }
}
