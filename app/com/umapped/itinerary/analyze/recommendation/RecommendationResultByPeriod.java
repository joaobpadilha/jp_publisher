package com.umapped.itinerary.analyze.recommendation;

import com.umapped.itinerary.analyze.model.StayPeriod;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by wei on 2017-04-10.
 */
public class RecommendationResultByPeriod {
  List<RecommendationList> hotelRecommendations;
  List<RecommendationList> activityRecommendations;
  List<RecommendationList> guides;

  public RecommendationResultByPeriod() {
    this.hotelRecommendations = new ArrayList<>();
    this.activityRecommendations = new ArrayList<>();
    this.guides = new ArrayList<>();
  }

  public void addHotel(Map<StayPeriod, List<RecommendedItem>> items) {
    addItems(hotelRecommendations, items);
  }

  public void addActivity(Map<StayPeriod, List<RecommendedItem>> items) {
    addItems(activityRecommendations, items);
  }

  public void addGuide(Map<StayPeriod, List<RecommendedItem>> items) {
    addItems(guides, items);
  }

  private void addItems(List<RecommendationList> recommendationList, Map<StayPeriod, List<RecommendedItem>> items) {
    if (items == null) {
      return;
    }
    for (StayPeriod p : items.keySet()) {
      List<RecommendedItem> itemList = items.get(p);
      if (itemList != null && itemList.size()>0) {
        recommendationList.add(new RecommendationList(p, itemList));
      }
    }
  }

  public List<RecommendationList> getHotelRecommendations() {
    return hotelRecommendations;
  }

  public List<RecommendationList> getActivityRecommendations() {
    return activityRecommendations;
  }

  public List<RecommendationList> getGuides() {
    return guides;
  }
}