package com.umapped.itinerary.analyze.recommendation;

/**
 * Created by wei on 2017-03-30.
 */
public class RecommendedAccommodation
    extends RecommendedItem {

  public String toString() {
    return String.format("%s: %s\n", name, bookingUrl);
  }
}
