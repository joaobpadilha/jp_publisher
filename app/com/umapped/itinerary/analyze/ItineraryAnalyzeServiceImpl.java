package com.umapped.itinerary.analyze;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.name.Named;
import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import com.mapped.publisher.utils.Log;
import com.umapped.itinerary.analyze.adapters.ReservationPackageItineraryAdapter;
import com.umapped.itinerary.analyze.log.ItineraryAnalyzeServiceLog;
import com.umapped.itinerary.analyze.model.Itinerary;
import com.umapped.itinerary.analyze.model.ItineraryItem;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.segment.SegmentDetector;
import com.umapped.itinerary.analyze.segment.TripSegment;
import models.publisher.TripAnalysis;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wei on 2017-03-29.
 */
@Singleton public class ItineraryAnalyzeServiceImpl
    implements ItineraryAnalyzeService {
  private ReservationPackageItineraryAdapter reservationPackageItineraryAdapter;
  private SegmentDetector segmentDetector;
  private ItineraryAnalyzer itineraryAnalyzer;
  private ItineraryAnalyzeServiceLog serviceLog;
  private ItineraryLocationEnhancer itineraryLocationEnhancer;
  private ObjectMapper objectMapper;

  @Inject
  public ItineraryAnalyzeServiceImpl(ReservationPackageItineraryAdapter rpAdapter,
                                     ItineraryLocationEnhancer itineraryLocationEnhancer,
                                     SegmentDetector segmentDetector,
                                     ItineraryAnalyzer itineraryAnalyzer,
                                     ItineraryAnalyzeServiceLog serviceLog,
                                     @Named("ItineraryAnalayzeObjectMapper") ObjectMapper objectMapper) {
    this.reservationPackageItineraryAdapter = rpAdapter;
    this.itineraryLocationEnhancer = itineraryLocationEnhancer;
    this.segmentDetector = segmentDetector;
    this.itineraryAnalyzer = itineraryAnalyzer;
    this.serviceLog = serviceLog;
    this.objectMapper = objectMapper;
  }

  @Override public ItineraryAnalyzeResult analyze(ReservationPackage reservationPackage) {
    StopWatch sw = new StopWatch();
    sw.start();
    long rpToItineraryTime = sw.getTime();
    long locEnhanceTime = rpToItineraryTime;
    long segmentDetectTime = rpToItineraryTime;
    long analyzeTime = rpToItineraryTime;
    ItineraryAnalyzeResult analyzeResult = null;

    Instant startTime = Instant.now();
    try {
      Itinerary itinerary = reservationPackageItineraryAdapter.adapt(reservationPackage);

      TripAnalysis prevAnalysis = TripAnalysis.findLastGoodAnalysis(itinerary.getUmItineraryId());

      ItineraryAnalyzeResult prevResult = null;
      prevResult = retrieveItineraryAnalyzeResult(prevAnalysis);

      if (prevResult != null && prevResult.getItinerary() != null) {
        if (compareAndEnhance(itinerary, prevResult.getItinerary())) {
          Log.debug("The itinerary doesn't change from prev analysis: " + itinerary.getUmItineraryId());
          return prevResult;
        }
      }

      rpToItineraryTime = sw.getTime();


      itineraryLocationEnhancer.enhanceItineraryLocation(itinerary);

      locEnhanceTime = sw.getTime();

      List<TripSegment> segments = segmentDetector.analyze(itinerary);

      segmentDetectTime = sw.getTime();


      analyzeResult = itineraryAnalyzer.analyze(itinerary, segments);

      analyzeTime = sw.getTime();
      sw.stop();
    } catch (Exception e) {
      String r = "";
      try {
        ObjectMapper om = new ObjectMapper();
        r = om.writeValueAsString(reservationPackage);
      } catch (Exception e1) {

      }
      Log.err("ItineraryAnalyzeService - error " + r , e);
    } finally {
      if (analyzeResult != null) {
        log(reservationPackage,
            analyzeResult,
            startTime,
            rpToItineraryTime,
            locEnhanceTime,
            segmentDetectTime,
            analyzeTime);
      }
    }

    return analyzeResult;
  }

  private void log(ReservationPackage rp, ItineraryAnalyzeResult result, Instant runtime, long... time) {
    Map<String, Long> performance = new HashMap<>();
    if (time != null && time.length > 0) {
      performance.put("RPToItinerary", time[0]);
      if (time.length > 1) {
        performance.put("LocationEnhance", time[1] - time[0]);
      }
      if (time.length > 2) {
        performance.put("SegmentDetect", time[2] - time[1]);
      }
      if (time.length > 3) {
        performance.put("Analyze", time[3] - time[2]);
      }
    }
    TripAnalysis tripAnalysis = TripAnalysis.build(rp, result, performance, runtime);
    tripAnalysis.save();
    //serviceLog.log(rp, result, performance);
  }

  private ItineraryAnalyzeResult retrieveItineraryAnalyzeResult(TripAnalysis analysis) {
    JsonNode itineraryAnalyzeResult = (analysis == null) ? null : analysis.getItineraryAnalyzeResult();
    if (itineraryAnalyzeResult != null) {
      try {
        return this.objectMapper.treeToValue(itineraryAnalyzeResult, ItineraryAnalyzeResult.class);
      }
      catch (JsonProcessingException e) {
        Log.err("Fail to parse Itinerary Analyze Result", e);
      }
    }
    return null;
  }

  /**
   * If the location is changed
   * @param newItinerary
   * @param prevItinerary
   * @return
   */
  private boolean compareAndEnhance(Itinerary newItinerary, Itinerary prevItinerary) {
    boolean noChange = true;

    Map<String, ItineraryItem> itemMap = new HashMap<>();
    if (prevItinerary.getItems() != null) {
      for (ItineraryItem item : prevItinerary.getItems()) {
        itemMap.put(item.getUmId(), item);
      }
    }
    for (ItineraryItem item : newItinerary.getItems()) {
      noChange = noChange && compareAndEnhance(item, itemMap.get(item.getUmId()));
    }


    return noChange && newItinerary.getItems().size() == prevItinerary.getItems().size();
  }

  private boolean compareAndEnhance(ItineraryItem item, ItineraryItem prevItem) {
    if (prevItem == null) {
      return false;
    }

    boolean noChange = true;
    Location enhancedStartLocation = compareAndEnhance(prevItem.getStartLocation(), prevItem.getEnhancedStartLocation(), item.getStartLocation() );

    Location enhancedEndLocation = compareAndEnhance(prevItem.getEndLocation(), prevItem.getEnhancedEndLocation(), item.getEndLocation());

    if (enhancedStartLocation == null && item.getStartLocation()!=null) {
      noChange = false;
    } else {
      item.setEnhancedStartLocation(enhancedStartLocation);
    }

    if (enhancedEndLocation == null && item.getEndLocation()!=null) {
      noChange = false;
    } else {
      item.setEnhancedEndLocation(enhancedEndLocation);
    }

    noChange &= compare(prevItem.getStartDateTime(), item.getStartDateTime());
    noChange &= compare(prevItem.getEndDateTime(), item.getEndDateTime());

    return noChange;
  }

  private Location compareAndEnhance(Location prevLocation, Location prevEnhancedLocation, Location location) {
    if (location == null && prevLocation == null) {
      return null;
    }

    if (location != null && prevLocation != null) {
      if (StringUtils.equals(prevLocation.getUmPoidId(), location.getUmPoidId())) {
        if (compareAddress(prevLocation.getAddress(),
                           location.getAddress()) && StringUtils.equals(StringUtils.trimToNull(location.getDescription()),
                                                                        StringUtils.trimToNull(prevLocation.getDescription()))) {

          return prevEnhancedLocation;
        }
      }
    }
    return null;
  }

  private boolean compareAddress(Location.Address address1, Location.Address address2) {
    if (address1 == null) {
     return address2 == null;
    } else {
      return address1.equals(address2);
    }
  }

  private boolean compare(ZonedDateTime time1, ZonedDateTime time2) {
    if (time1 == null) {
      return time2 == null;
    } else {
      return time1.equals(time2);
    }
  }
}
