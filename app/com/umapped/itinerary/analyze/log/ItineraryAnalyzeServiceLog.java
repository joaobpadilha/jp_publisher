package com.umapped.itinerary.analyze.log;

import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import com.umapped.itinerary.analyze.ItineraryAnalyzeResult;

import java.util.Map;

/**
 * Created by wei on 2017-03-29.
 */
public interface ItineraryAnalyzeServiceLog {
  void log(ReservationPackage reservationPackage, ItineraryAnalyzeResult result, Map<String, Long> performance);
}
