package com.umapped.itinerary.analyze.db.model;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbJsonB;
import com.fasterxml.jackson.databind.JsonNode;
import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import com.umapped.itinerary.analyze.ItineraryAnalyzeResult;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by wei on 2017-03-30.
 */
@Entity
@Table(name = "itinerary_analyze")
public class ItineraryAnalyzeResultModel extends Model {

  @Id
  @Column(name = "analyze_id")
  private long analyzeId;

  @Column(name = "itinerary_id")
  private String itineraryId;

  @DbJsonB
  @Column(name = "reservation_package")
  private JsonNode reservationPackage;

  @DbJsonB
  @Column(name = "result")
  private JsonNode itineraryAnalyzeResult;

  @DbJsonB
  @Column(name = "performance")
  private JsonNode performance;

  private Timestamp created_ts;

  public long getAnalyzeId() {
    return analyzeId;
  }

  public ItineraryAnalyzeResultModel setAnalyzeId(long analyzeId) {
    this.analyzeId = analyzeId;
    return this;
  }

  public String getItineraryId() {
    return itineraryId;
  }

  public ItineraryAnalyzeResultModel setItineraryId(String itineraryId) {
    this.itineraryId = itineraryId;
    return this;
  }

  public JsonNode getReservationPackage() {
    return reservationPackage;
  }

  public ItineraryAnalyzeResultModel setReservationPackage(JsonNode reservationPackage) {
    this.reservationPackage = reservationPackage;
    return this;
  }

  public JsonNode getItineraryAnalyzeResult() {
    return itineraryAnalyzeResult;
  }

  public ItineraryAnalyzeResultModel setItineraryAnalyzeResult(JsonNode itineraryAnalyzeResult) {
    this.itineraryAnalyzeResult = itineraryAnalyzeResult;
    return this;
  }

  public JsonNode getPerformance() {
    return performance;
  }

  public ItineraryAnalyzeResultModel setPerformance(JsonNode performance) {
    this.performance = performance;
    return this;
  }

  public Timestamp getCreated_ts() {
    return created_ts;
  }

  public void setCreated_ts(Timestamp created_ts) {
    this.created_ts = created_ts;
  }
}
