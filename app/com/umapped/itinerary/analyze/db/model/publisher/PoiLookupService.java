package com.umapped.itinerary.analyze.db.model.publisher;

import com.mapped.publisher.persistence.CountriesInfo;
import com.mapped.publisher.persistence.PoiMgr;
import com.mapped.publisher.persistence.PoiRS;
import com.umapped.api.schema.types.Address;
import com.umapped.api.schema.types.Coordinates;
import com.umapped.itinerary.analyze.location.LocationLookupService;
import com.umapped.itinerary.analyze.model.Location;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by wei on 2017-03-28.
 */
public class PoiLookupService implements LocationLookupService {
  private static final Logger LOG = LoggerFactory.getLogger(PoiLookupService.class);


  private static final String POI_SERVER = "db";

  public PoiLookupService() {
  }


  public Location lookup(Location location) {
    String umId = location.getUmPoidId();
    long poiId;
    try {
      poiId = Long.parseLong(umId);
    } catch (Exception e) {
      LOG.debug("Invalid poidId : " + umId);
      return null;
    }
    List<PoiRS> pois = PoiMgr.findAllById(poiId);

    LOG.debug(String.format("Get %s POI records for %s", pois.size(), poiId));

    return enhanceLocation(location, pois);
  }

  public Location enhanceLocation(Location loc, List<PoiRS> pois) {
    Location location = new Location();
    location.setUmPoidId(loc.getUmPoidId());
    location.setGeoLocation(loc.getGeoLocation());
    location.setAddress(loc.getAddress());
    location.setDescription(loc.getDescription());
    for (PoiRS p : pois) {
        for (Address addr : p.data.getAddresses()) {
          if (loc.getGeoLocation() == null) {
            Coordinates geoLocation = addr.getCoordinates();
            if (geoLocation != null && geoLocation.getLongitude() != null && geoLocation.getLatitude()!=null) {
              location.setGeoLocation(new Location.GeoLocation(geoLocation.getLatitude(),
                                                               geoLocation.getLongitude()
              ));
            }
          }
          Location.Address address = location.getAddress();
          if (address.getStreetAddress() == null) {
            address.setLocality(addr.getLocality());
          }
          if (address.getLocality() == null) {
            address.setLocality(addr.getLocality());
          }
          if (address.getRegion() == null) {
            address.setRegion(addr.getRegion());
          }
          if (address.getA2CountryCode() == null) {
            String countryCode =addr.getCountryCode();
            if (StringUtils.isNotEmpty(countryCode)) {
            CountriesInfo.Country country = CountriesInfo.Instance().byAlpha3(countryCode);
            if (country != null) {
              address.setCountry(country.getName(CountriesInfo.LangCode.EN));
              address.setA2CountryCode(country.getAlpha2());
            }
          }
        }
      }
    }
    return location;
  }

}
