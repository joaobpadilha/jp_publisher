package com.umapped.itinerary.analyze;

import com.umapped.itinerary.analyze.segment.feature.SegmentFeatureDetectorRegistry;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

/**
 * Created by wei on 2017-03-29.
 */
@Singleton
public class ItineraryAnalyzerProvider implements Provider<ItineraryAnalyzer> {

  private ItineraryAnalyzer instance;

  @Inject
  public ItineraryAnalyzerProvider(SegmentFeatureDetectorRegistry segmentFeatureDetectorRegistry) {
   this.instance = new ItineraryAnalyzer(segmentFeatureDetectorRegistry);

  }

  @Override public ItineraryAnalyzer get() {
    return instance;
  }
}
