package com.umapped.itinerary.analyze;

import com.mapped.publisher.parse.schemaorg.ReservationPackage;

/**
 * Created by wei on 2017-03-29.
 */
public interface ItineraryAnalyzeService {
  ItineraryAnalyzeResult analyze(ReservationPackage reservationPackage);
}
