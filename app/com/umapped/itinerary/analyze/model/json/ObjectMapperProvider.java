package com.umapped.itinerary.analyze.model.json;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.mapped.publisher.parse.schemaorg.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Created by wei on 2017-03-02.
 */
@Singleton public class ObjectMapperProvider
    implements Provider<ObjectMapper> {
  private ObjectMapper om;

  public ObjectMapperProvider() {
    om = new ObjectMapper();
    SimpleModule module = new SimpleModule();
    module.addDeserializer(ZonedDateTime.class, new ZoneDateTimeJsonMarshaller.ZoneDateTimeDeserializer())
          .addDeserializer(LocalDate.class, new LocalDateJsonMarshaller.LocalDateDeserializer())
          .addDeserializer(ZoneId.class, new ZoneIdJsonMarshaller.ZoneIdDeserializer())
          .addSerializer(ZonedDateTime.class, new ZoneDateTimeJsonMarshaller.ZoneDateTimeSerializer())
          .addSerializer(LocalDate.class, new LocalDateJsonMarshaller.LocalDateSerializer())
          .addSerializer(ZoneId.class, new ZoneIdJsonMarshaller.ZoneIdSerializer());


    om.registerModule(module);
    om.enable(SerializationFeature.INDENT_OUTPUT);
    om.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    om.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    om.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);

    om.registerSubtypes(ActivityReservation.class,
                        LodgingReservation.class,
                        FlightReservation.class,
                        TrainReservation.class,
                        EventReservation.class,
                        CruiseShipReservation.class,
                        TransferReservation.class,
                        TaxiReservation.class,
                        FoodEstablishmentReservation.class,
                        BusReservation.class,
                        RentalCarReservation.class);

  }

  @Override public ObjectMapper get() {
    return om;
  }
}
