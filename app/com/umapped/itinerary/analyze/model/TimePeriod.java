package com.umapped.itinerary.analyze.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mapped.publisher.parse.schemaorg.DateTime;
import com.umapped.itinerary.analyze.model.json.LocalDateTimeJsonMarshaller;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;


/**
 * Created by wei on 2017-02-24.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TimePeriod {
  private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

  private TimeZone startTimeZone;
  private LocalDateTime startDateTime;
  private TimeZone endTimeZone;
  private LocalDateTime endDateTime;

  public TimePeriod() {
  }

  private LocalDateTime convert(DateTime dt) {
    if (dt != null) {
      return LocalDateTime.parse(dt.getValue(), formatter);
    } else {
      return null;
    }
  }

  public TimePeriod(DateTime startDateTime, DateTime endDateTime) {
    this.startDateTime = convert(startDateTime);
    this.endDateTime = convert(endDateTime);
  }

  public TimePeriod(LocalDateTime startDateTime, LocalDateTime endDateTime) {
    this.startDateTime = startDateTime;
    this.endDateTime = endDateTime;
  }

  public TimePeriod(DateTime startDateTime,
                    TimeZone startTimeZone,
                    DateTime endDateTime,
                    TimeZone endTimeZone) {
    this.startTimeZone = startTimeZone;
    this.startDateTime = convert(startDateTime);
    this.endTimeZone = endTimeZone;
    this.endDateTime = convert(endDateTime);
  }

  public TimeZone getStartTimeZone() {
    return startTimeZone;
  }

  public TimePeriod setStartTimeZone(TimeZone startTimeZone) {
    this.startTimeZone = startTimeZone;
    return this;
  }

  @JsonSerialize(using = LocalDateTimeJsonMarshaller.LocalDateTimeSerializer.class)
  @JsonDeserialize(using = LocalDateTimeJsonMarshaller.LocalDateTimeDeserializer.class)
  public LocalDateTime getStartDateTime() {
    return startDateTime;
  }

  public TimePeriod setStartDateTime(LocalDateTime startDateTime) {
    this.startDateTime = startDateTime;
    return this;
  }

  public TimeZone getEndTimeZone() {
    return endTimeZone;
  }

  public TimePeriod setEndTimeZone(TimeZone endTimeZone) {
    this.endTimeZone = endTimeZone;
    return this;
  }

  @JsonSerialize(using = LocalDateTimeJsonMarshaller.LocalDateTimeSerializer.class)
  @JsonDeserialize(using = LocalDateTimeJsonMarshaller.LocalDateTimeDeserializer.class)
  public LocalDateTime getEndDateTime() {
    return endDateTime;
  }

  public TimePeriod setEndDateTime(LocalDateTime endDateTime) {
    this.endDateTime = endDateTime;
    return this;
  }

  public String toString() {
    return ToStringBuilder.reflectionToString(this,  ToStringStyle.MULTI_LINE_STYLE);
  }
}
