package com.umapped.itinerary.analyze.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mapped.publisher.parse.schemaorg.GeoCoordinates;
import com.mapped.publisher.parse.schemaorg.PostalAddress;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.openrdf.query.algebra.Add;

import java.io.Serializable;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wei on 2017-02-23.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Location implements Serializable {
  private String umPoidId;

  private GeoLocation geoLocation;

  private ZoneId timezone;

  private Address address;

  private String description;

  private Map<String, String> locationIds;

  public Location() {

  }

  public String getUmPoidId() {
    return umPoidId;
  }

  public Location addPlaceId(String key, String placeId) {
    if (locationIds == null) {
      locationIds = new HashMap<>();
    }
    locationIds.put(key, placeId);
    return this;
  }

  public Location setUmPoidId(String umPoidId) {
    this.umPoidId = umPoidId;
    return this;
  }

  public Address getAddress() {
    if (address == null) {
      address = new Address();
    }
    return address;
  }

  public Location setAddress(Address address) {
    this.address = address;
    return this;
  }

  public Map<String, String> getLocationIds() {
    return locationIds;
  }

  public Location setLocationIds(Map<String, String> locationIds) {
    this.locationIds = locationIds;
    return this;
  }

  public Location fromPostalAddress(PostalAddress address) {
    this.address = Address.fromPostAddress(address);
    return this;
  }

  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  public static class Address implements Serializable {
    private String streetAddress;
    private String locality;
    private String region;
    private String country;
    private String a2CountryCode;

    public static Address fromPostAddress(PostalAddress address) {
      Address self = new Address();
      if (address != null) {
        self.streetAddress = address.streetAddress;
        self.locality = address.addressLocality;
        self.region = address.addressRegion;
        self.country = address.a3Country;
      }
      return self;
    }

    public String getStreetAddress() {
      return streetAddress;
    }

    public String getLocality() {
      return locality;
    }

    public String getRegion() {
      return region;
    }

    public String getCountry() {
      return country;
    }

    public String getA2CountryCode() {
      return a2CountryCode;
    }

    public Address setStreetAddress(String streetAddress) {
      this.streetAddress = streetAddress;
      return this;
    }

    public Address setLocality(String locality) {
      this.locality = locality;
      return this;
    }

    public Address setRegion(String region) {
      this.region = region;
      return this;
    }

    public Address setCountry(String country) {
      this.country = country;
      return this;
    }


    public Address setA2CountryCode(String a2CountryCode) {
      this.a2CountryCode = a2CountryCode;
      return this;
    }

    public String toString() {
      return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    @Override public boolean equals(Object obj) {
      if (obj == null || !(obj instanceof  Address)) {
        return false;
      }
      Address other = (Address) obj;

      return StringUtils.equals(StringUtils.trimToNull(this.getStreetAddress()), StringUtils.trimToNull(other.getStreetAddress())) &&
             StringUtils.equals(StringUtils.trimToNull(this.getLocality()), StringUtils.trimToNull(other.getLocality())) &&
             StringUtils.equals(StringUtils.trimToNull(this.getRegion()), StringUtils.trimToNull(other.getRegion())) &&
             StringUtils.equals(StringUtils.trimToNull(this.getCountry()), StringUtils.trimToNull(other.getCountry())) &&
             StringUtils.equals(StringUtils.trimToNull(this.getA2CountryCode()), StringUtils.trimToNull(other.getA2CountryCode()));
    }
  }


  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  public static class GeoLocation implements Serializable {
    private double latitude;
    private double longitude;

    public static GeoLocation build(GeoCoordinates coordinates) {
      try {
        if (coordinates != null)
          return new GeoLocation(Double.parseDouble(coordinates.latitude), Double.parseDouble(coordinates.longitude));
      }
      catch (Exception e) {
        // LOG it
      }
      return null;
    }

    public GeoLocation() {

    }

    public GeoLocation(double latitude, double longitude) {
      this.latitude = latitude;
      this.longitude = longitude;
    }

    public double getLatitude() {
      return latitude;
    }

    public double getLongitude() {
      return longitude;
    }

    public String toString() {
      return "(" + latitude + "," + longitude + ")";
    }
  }


  public ZoneId getTimezone() {
    return timezone;
  }

  public Location setTimezone(ZoneId timezone) {
    this.timezone = timezone;
    return this;
  }

  public Location(GeoLocation geoLocation) {
    this.geoLocation = geoLocation;
  }

  public Location(double latitude, double longitude) {
    geoLocation = new GeoLocation(latitude, longitude);
  }


  public GeoLocation getGeoLocation() {
    return geoLocation;
  }


  public Location setGeoLocation(GeoLocation geoLocation) {
    this.geoLocation = geoLocation;
    return this;
  }


  public String getDescription() {
    return description;
  }

  public Location setDescription(String description) {
    this.description = description;
    return this;
  }

  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
  }
}
