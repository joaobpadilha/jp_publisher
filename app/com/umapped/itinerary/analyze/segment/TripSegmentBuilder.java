package com.umapped.itinerary.analyze.segment;

import com.umapped.itinerary.analyze.model.ItineraryItem;
import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.model.Location;
import org.apache.commons.lang3.tuple.MutablePair;

import java.time.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wei on 2017-03-01.
 * <p>
 * Statful builder
 */
public class TripSegmentBuilder {
  private TripSegment buffer;
  private List<TripSegment> segments;


  private Location startingLocation;
  private ZonedDateTime startingDateTime;

  private TripSegment current;

  public TripSegmentBuilder() {
    segments = new ArrayList<>();
  }

  public List<TripSegment> getTripSegments() {
    return segments;
  }

  public void startTransit() {
    current = new TripSegment(TransitCategory.NonTransit);
  }

  public void addTransitSegment(TransitCategory category, ItineraryItem item) {
    current.setEndLocation(item.getEnhancedStartLocation());
    current.setEndDateTime(item.getStartDateTime());
    current.addNature(SegmentNature.Layover);
    segments.add(current);

    TripSegment transitSegment = new TripSegment(category);
    transitSegment.setStartDateTime(item.getStartDateTime())
                   .setEndDateTime(item.getEndDateTime())
                   .setStartLocation(item.getEnhancedStartLocation())
                   .setEndLocation(item.getEnhancedEndLocation());
    if (category == TransitCategory.ContinuousTransit) {
      transitSegment.addNature(SegmentNature.Transit);
    }
    transitSegment.setMainItem(item);
    if (item.getType() == ItineraryItemType.Cruise) {
      transitSegment.addNature(SegmentNature.Cruise);
    }
    segments.add(transitSegment);

    current = new TripSegment(TransitCategory.NonTransit);
    current.setStartDateTime(item.getEndDateTime());
    current.setStartLocation(item.getEnhancedEndLocation());
  }

  public void endTransit() {
    if (current != null) {
      segments.add(current);
    }
  }

  public void addAccommodationItem(ItineraryItem item) {
    ZonedDateTime checkinDate = ZonedDateTime.of(LocalDateTime.of(item.getStartDateTime().toLocalDate(),
                                                                  LocalTime.of(23, 59, 59)),
                                                 item.getStartDateTime().getZone());
    ZonedDateTime checkoutDate = ZonedDateTime.of(LocalDateTime.of(item.getEndDateTime().toLocalDate(),
                                                                  LocalTime.of(0, 0, 0)),
                                                 item.getStartDateTime().getZone());

    for (int i = 0; i < segments.size(); i++) {
      TripSegment s = segments.get(i);
      if (s.getCategory() != TransitCategory.ContinuousTransit) {
        if (overlap(checkinDate, checkoutDate, s.getStartDateTime(), s.getEndDateTime())) {
          // Accomodation fit in this segment
          if (!s.hasNature(SegmentNature.Cruise)) {
            // split the segment into two
            // Assume all the current items in the segments is before this accomodation
            // so just end this segment and create a new one
            TripSegment newSegment = new TripSegment();
            newSegment.addNature(SegmentNature.Layover);
            newSegment.setStartDateTime(s.getStartDateTime());
            newSegment.setStartLocation(s.getStartLocation());
            newSegment.setEndDateTime(item.getEndDateTime());
            newSegment.setEndLocation(item.getEnhancedEndLocation());

            s.setStartDateTime(item.getEndDateTime());
            s.setStartLocation(item.getEnhancedEndLocation());
            newSegment.addAccommodation(item);

            segments.add(i, newSegment);
          } else {
            s.addAccommodation(item);
          }
          return;
        }
      }
    }
  }

  // check whether the two time period overlap
  private boolean overlap(ZonedDateTime p1Start, ZonedDateTime p1End, ZonedDateTime p2Start, ZonedDateTime p2End) {
    ZonedDateTime overLapStart;
    if (p1Start != null && p2Start!=null) {
      if (p1Start.isBefore(p2Start)) {
        overLapStart = p2Start;
      } else {
        overLapStart = p1Start;
      }
    } else {
      overLapStart = p1Start == null ? p2Start : p1Start;
    }

    ZonedDateTime overLapEnd;
    if (p1End != null && p2End!=null) {
      if (p1End.isBefore(p2End)) {
        overLapEnd = p1End;
      } else {
        overLapEnd = p2End;
      }
    } else {
      overLapEnd = p1End == null ? p2End : p1End;
    }

    if (overLapStart != null && overLapEnd!=null) {
      return overLapEnd.isAfter(overLapStart);
    }
    return false;
  }

  public void addActivityItem(ItineraryItem item) {
    ZonedDateTime startDateTime = item.getStartDateTime();

    for (TripSegment s : segments) {
      if (s.getCategory() != TransitCategory.ContinuousTransit) {
        if (((s.getStartDateTime() == null) || (s.getStartDateTime() != null && s.getStartDateTime()
                                                                                 .isBefore(startDateTime))) && (s.getEndDateTime() == null || s
            .getEndDateTime()
            .isAfter(startDateTime))) {
          if (item.getType() == ItineraryItemType.Activity) {
            s.addActivity(item);
          } else if (item.getType() == ItineraryItemType.CruiseStop) {
            s.addCruiseStop(item);
          } else {
            s.addTransfer(item);
          }
        }
      }
    }
  }

  public List<TripSegment> getResult() {
    List<TripSegment> result = new ArrayList<>();
    for (TripSegment s : segments) {
      if (!s.isEmpty()) {
        result.add(verifyStartEndTime(s));
      }
    }
    return result;
  }

  //For starting and end segment, the starting and ending time may not be
  // set during parsing.
  public TripSegment verifyStartEndTime(TripSegment segment) {

    if (segment.getStartDateTime() == null || segment.getEndDateTime() == null) {
      // needs to figure out start or end time
      ZonedDateTime start = null;
      ZonedDateTime end = null;
      MutablePair<ZonedDateTime, ZonedDateTime> period = new MutablePair<>(null, null);
      updatePeriod(segment.getMainItem(), period);
      updatePeriod(segment.getAccommodations(), period);
      updatePeriod(segment.getTransfers(), period);
      updatePeriod(segment.getActivities(), period);

      if (segment.getStartDateTime() == null)  {
        segment.setStartDateTime(period.getLeft());
      }
      if (segment.getEndDateTime() == null) {
        segment.setEndDateTime(period.getRight());
      }
    }
    return segment;
  }

  private void updatePeriod(List<ItineraryItem> items, MutablePair<ZonedDateTime, ZonedDateTime> period) {
    if (items == null) return;
    for (ItineraryItem item : items) {
      updatePeriod(item, period);
    }
  }

  private void updatePeriod(ItineraryItem item, MutablePair<ZonedDateTime, ZonedDateTime> period) {
    if (item == null) {
      return ;
    }
    if (period.getLeft() == null) {
      period.setLeft(item.getStartDateTime());
    } else {
      if (item.getStartDateTime() != null) {
        if (item.getStartDateTime().isBefore(period.getLeft())) {
          period.setLeft(item.getStartDateTime());
        }
      }
    }
    if (period.getRight() == null) {
      period.setRight(item.getEndDateTime());
    } else {
      if (item.getEndDateTime() != null) {
        if (item.getEndDateTime().isAfter(period.getRight())) {
          period.setRight(item.getEndDateTime());
        }
      }
    }

  }
}
