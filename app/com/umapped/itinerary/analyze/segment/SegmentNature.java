package com.umapped.itinerary.analyze.segment;

/**
 * Created by wei on 2017-03-07.
 */
public enum SegmentNature {
  Layover,
  Transit,
  StopOver,
  Cruise,
  Ski
}
