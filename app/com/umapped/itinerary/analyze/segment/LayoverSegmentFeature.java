package com.umapped.itinerary.analyze.segment;

/**
 * Created by wei on 2017-03-08.
 */
public enum LayoverSegmentFeature {
  Duration,
  AccommodationCount,
  AccommodationDuration,
  ActivityCount,
  OverNightSpan,
  MorningSpan,
  AfternoonSpan,
  EveningSpan,
  DaysWithoutActivities,
  DaysWithoutAccomodations
}
