package com.umapped.itinerary.analyze.segment.decisiontree;

import com.umapped.itinerary.analyze.segment.AnalyzerContext;

/**
 * Created by wei on 2017-03-09.
 */
public class EndDecisionNode
    implements DecisionNode {

  private Evaluator evaluator;

  public EndDecisionNode(Evaluator evaluator) {
    this.evaluator = evaluator;
  }

  @Override public Object getEvaluateResult(AnalyzerContext context) {
    return evaluator.evaluate(context);
  }

  @Override public boolean isLeaf() {
    return true;
  }

  @Override public DecisionNode next(AnalyzerContext context) {
    return null;
  }
}
