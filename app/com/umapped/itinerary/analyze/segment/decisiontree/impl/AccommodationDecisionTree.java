package com.umapped.itinerary.analyze.segment.decisiontree.impl;

import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.segment.decisiontree.AbstractDecisionTree;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionNode;
import com.umapped.itinerary.analyze.segment.decisiontree.EndDecisionNode;
import com.umapped.itinerary.analyze.segment.SegmentNature;
import com.umapped.itinerary.analyze.segment.decisiontree.SplitDecisionNode;
import com.umapped.itinerary.analyze.segment.decisiontree.evaluators.AccommodationDecisionEvaluator;

import java.util.ArrayList;
import java.util.List;

import static com.umapped.itinerary.analyze.segment.feature.SegmentFeature.*;

/**
 * Created by wei on 2017-03-09.
 */
public class AccommodationDecisionTree extends AbstractDecisionTree {

  @Override
  protected DecisionNode initTree() {

    SplitDecisionNode isTransitSegment = new SplitDecisionNode((context) -> context.getCurrent()
                                                                                   .hasNature(SegmentNature.Transit));

    SplitDecisionNode longDuration = new SplitDecisionNode((context) -> ((Integer) context.getFeature(DurationHours)) >= 24);

    SplitDecisionNode hasOvernightStay = new SplitDecisionNode((context) -> ((Integer) context.getFeature(OverNightHours)) > 6);

    SplitDecisionNode hasDatesWithoutAccomondation = new SplitDecisionNode((context) -> ((List<StayPeriod>) context
        .getFeature(DatesWithoutAccommodation)).size()>0);

    SplitDecisionNode accommodationCancellable = new SplitDecisionNode((context) -> context.getFeature(HasCancellableAccomodation));

    EndDecisionNode noRecommendationResult = new EndDecisionNode((context) -> new ArrayList<>(0));
    EndDecisionNode recommendationResult = new EndDecisionNode(new AccommodationDecisionEvaluator());

    isTransitSegment.addNode(Boolean.TRUE, noRecommendationResult).addNode(Boolean.FALSE, longDuration);

    longDuration.addNode(Boolean.TRUE, hasDatesWithoutAccomondation).addNode(Boolean.FALSE, hasOvernightStay);

    hasDatesWithoutAccomondation.addNode(Boolean.TRUE, recommendationResult)
                                .addNode(Boolean.FALSE, accommodationCancellable);

    hasOvernightStay.addNode(Boolean.TRUE, hasDatesWithoutAccomondation).addNode(Boolean.FALSE, noRecommendationResult);

    accommodationCancellable.addNode(Boolean.TRUE, recommendationResult).addNode(Boolean.FALSE, noRecommendationResult);

    return isTransitSegment;

  }
}
