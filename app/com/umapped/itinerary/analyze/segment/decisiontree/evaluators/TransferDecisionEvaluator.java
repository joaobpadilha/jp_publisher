package com.umapped.itinerary.analyze.segment.decisiontree.evaluators;

import com.umapped.itinerary.analyze.model.ItineraryItem;
import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.recommendation.RecommendedItemType;
import com.umapped.itinerary.analyze.segment.TripSegment;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import com.umapped.itinerary.analyze.segment.decisiontree.Evaluator;
import com.umapped.itinerary.analyze.segment.AnalyzerContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wei on 2017-03-10.
 * Simplified version just to get the starting/ending location and date time
 */
public class TransferDecisionEvaluator
    implements Evaluator {
  @Override public Object evaluate(AnalyzerContext context) {
    List<DecisionResult> results = new ArrayList<>();

    TripSegment segment = context.getCurrent();

    if (context.getPrevious() != null) {
      results.add(new DecisionResult(RecommendedItemType.Transfer, segment.getStartLocation(), segment.getStartDateTime().toLocalDate()));
    }

    if (context.getNext() != null) {
      results.add(new DecisionResult(RecommendedItemType.Transfer, segment.getEndLocation(), segment.getEndDateTime().toLocalDate()));
    }

    return results;
  }
}