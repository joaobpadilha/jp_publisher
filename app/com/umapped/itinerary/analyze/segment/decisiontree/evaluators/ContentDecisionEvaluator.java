package com.umapped.itinerary.analyze.segment.decisiontree.evaluators;

import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.recommendation.RecommendedItemType;
import com.umapped.itinerary.analyze.segment.AnalyzerContext;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import com.umapped.itinerary.analyze.segment.decisiontree.Evaluator;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeature;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wei on 2017-06-09.
 */
public class ContentDecisionEvaluator implements Evaluator {
  @Override public Object evaluate(AnalyzerContext context) {
    // TODO: simplify it with the starting location for now
    List<DecisionResult> results = new ArrayList<>();
    StayPeriod p = new StayPeriod(context.getCurrent().getStartDateTime().toLocalDate(), context.getCurrent().getEndDateTime().toLocalDate());
    results.add(new DecisionResult(RecommendedItemType.Content, context.getPossibleLocation(p), p.getStart(), p.getEnd()));
    return results;
  }
}