package com.umapped.itinerary.analyze.segment.decisiontree;

import com.umapped.itinerary.analyze.segment.AnalyzerContext;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by wei on 2017-03-09.
 */
public class SplitDecisionNode
    implements DecisionNode {
  protected Map<Object, DecisionNode> branchMap = new HashMap<>();

  protected Evaluator evaluator;

  public SplitDecisionNode(Evaluator evaluator) {
    this.evaluator = evaluator;
  }

  public SplitDecisionNode addNode(Object result, DecisionNode branch) {
    branchMap.put(result, branch);
    return this;
  }

  @Override public Object getEvaluateResult(AnalyzerContext context) {
    return evaluator.evaluate(context);
  }

  @Override public DecisionNode next(AnalyzerContext context) {
    if (isLeaf()) return null;

    Object result = getEvaluateResult(context);

    return branchMap.get(result);
  }

  @Override public boolean isLeaf() {
    return false;
  }
}
