package com.umapped.itinerary.analyze.segment.feature;

import com.umapped.itinerary.analyze.segment.AnalyzerContext;

/**
 * Created by wei on 2017-03-08.
 */
public interface SegmentFeatureDetector {
  Object detect(AnalyzerContext context);
}
