package com.umapped.service.offer;

/**
 * Created by wei on 2017-06-13.
 */
public interface ContentOfferProvider {
  OfferResult getContentOffers(ContentFilter filter);
}
