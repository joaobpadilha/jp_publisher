package com.umapped.service.offer;

/**
 * Created by wei on 2017-06-13.
 */
public class ActivityFilter extends OfferFilter {
  private boolean familyFriendly;

  private ActivityType actvityType;

  private Duration duration;

  /* 1 to 5, 0 means all */
  private int starRating;

  public enum ActivityType {
    CULTURE,
    ADVENTURE,
    ENTERTAINMENT,
  }

  public enum Duration {
    TWO_HOURS,
    HALF_DAY,
    FULL_DAY,
    MULTI_DAYS
  }
}
