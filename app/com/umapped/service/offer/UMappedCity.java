package com.umapped.service.offer;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.umapped.itinerary.analyze.model.Location;
import org.postgis.PGgeometry;
import org.postgis.Point;
import org.postgresql.util.PGobject;
import redis.clients.jedis.GeoCoordinate;

import java.io.Serializable;

/**
 * Created by wei on 2017-06-09.
 */
public class UMappedCity implements Serializable {
  private String id;
  private String name;
  private String region;
  private String country;
  private String extraDetail;

  private Location.GeoLocation geoLocation;

  public UMappedCity(String id, String name, String region, String country) {
    this.id = id;
    this.name = name;
    this.region = region;
    this.country = country;
  }

  public UMappedCity(String id, String name, String region, String country, String detail, Object geoLocation) {
    this.id = id;
    this.name = name;
    this.region = region;
    this.country = country;
    this.extraDetail = detail;
    if (geoLocation != null) {
      try {
        PGobject pGobject = (PGobject) geoLocation;
        Point postgisPoint = (Point) PGgeometry.geomFromString(pGobject.getValue());
        if (postgisPoint != null) {
          this.geoLocation = new Location.GeoLocation(postgisPoint.getY(), postgisPoint.getX());
        }
      } catch (Exception e) {

      }
    }
  }

  public String getId() {
    return id;
  }

  public UMappedCity setId(String id) {
    this.id = id;
    return this;
  }

  public String getRegion() {
    return region;
  }

  public UMappedCity setRegion(String region) {
    this.region = region;
    return this;
  }

  public String getCountry() {
    return country;
  }

  public UMappedCity setCountry(String country) {
    this.country = country;
    return this;
  }

  public Location.GeoLocation getGeoLocation() {
    return geoLocation;
  }

  public UMappedCity setGeoLocation(Location.GeoLocation geoLocation) {
    this.geoLocation = geoLocation;
    return this;
  }

  public String getName() {
    return name;
  }

  public UMappedCity setName(String name) {
    this.name = name;
    return this;
  }

  @Override public int hashCode() {
    return id.hashCode();
  }

  @Override public boolean equals(Object obj) {
    if (obj instanceof UMappedCity) {
      UMappedCity other = (UMappedCity) obj;
      return other.id.equals(this.id);
    }
    return false;
  }

  public String getExtraDetail() {
    return extraDetail;
  }

  public void setExtraDetail(String extraDetail) {
    this.extraDetail = extraDetail;
  }

  public UMappedCityLookup.ExtraDetail getExtraDetailJson() {
    try {
      ObjectMapper om = new ObjectMapper();
      ObjectReader or = om.readerFor(UMappedCityLookup.ExtraDetail.class);
      JsonFactory f = new JsonFactory();
      JsonParser jp = f.createParser(this.extraDetail);
      UMappedCityLookup.ExtraDetail json = or.readValue(jp, UMappedCityLookup.ExtraDetail.class);
      return json;
    } catch (Exception e) {

    }
    return null;
  }
}
