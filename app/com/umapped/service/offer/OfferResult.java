package com.umapped.service.offer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.recommendation.RecommendationList;
import com.umapped.itinerary.analyze.recommendation.RecommendedItem;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by wei on 2017-06-13.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OfferResult {

  List<StayPeriod> stayPeriods;
  List<RecommendedItem> recommendedItems;
  int nextPos=-1;
  int prevPos=-1;
  String keyword="";

  public OfferResult(List<StayPeriod> stayPeriods, List<RecommendedItem> recommendedItems) {
    this.stayPeriods = stayPeriods;
    this.recommendedItems = recommendedItems;
  }

  public List<RecommendedItem> getRecommendedItems() {
    return recommendedItems;
  }

  @JsonProperty
  public LocalDate getOfferDate() {
    if (stayPeriods != null && stayPeriods.size()>0) {
      return stayPeriods.get(0).getStart();
    } else {
      return null;
    }
  }

  @JsonProperty
  public List<StayPeriod> getStayPeriods() {
    return stayPeriods;
  }

  public int getNextPos() {
    return nextPos;
  }

  public void setNextPos(int nextPos) {
    this.nextPos = nextPos;
  }

  public int getPrevPos() {
    return prevPos;
  }

  public void setPrevPos(int prevPos) {
    this.prevPos = prevPos;
  }

  public String getKeyword() {
    return keyword;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }
}
