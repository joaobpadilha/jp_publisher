package com.umapped.service.offer;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.umapped.itinerary.analyze.recommendation.RecommendedItemType;
import com.umapped.itinerary.analyze.supplier.musement.MusementOfferProvider;
import com.umapped.itinerary.analyze.supplier.wcities.WCitiesActivityProvider;
import com.umapped.itinerary.analyze.supplier.wcities.WCitiesOfferProvider;

/**
 * Created by wei on 2017-06-13.
 */
@Singleton
public class OfferProviderRespositoryImpl implements OfferProviderRepository {
  @Inject
  private WCitiesOfferProvider wCitiesOfferProvider;

  @Inject
  private WCitiesActivityProvider wCitiesActivityProvider;

  @Inject
  private MusementOfferProvider musementProvider;

  @Override public OfferResult getContentOffer(ContentFilter filter) {
    return wCitiesOfferProvider.getContentOffers(filter);
  }

  @Override public OfferResult getActivityOffer(ActivityFilter filter) {
    if (filter.getOfferType() == RecommendedItemType.Events) {
      return wCitiesActivityProvider.getActivityOffers(filter);
    } else {
      return musementProvider.getActivityOffers(filter);
    }
  }
}
