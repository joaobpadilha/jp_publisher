package com.umapped.service.offer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import com.mapped.publisher.utils.Log;
import com.umapped.itinerary.analyze.ItineraryAnalyzeResult;
import com.umapped.itinerary.analyze.ItineraryAnalyzeService;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.model.json.ObjectMapperProvider;
import com.umapped.itinerary.analyze.recommendation.RecommendedItemType;
import com.umapped.itinerary.analyze.segment.TripSegmentAnalyzeResult;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import com.umapped.itinerary.analyze.supplier.wcities.WCitiesCityLookup;
import com.umapped.persistence.helpers.TripReservationsHelper;
import models.publisher.Trip;
import models.publisher.TripAnalysis;
import models.publisher.TripAudit;
import models.publisher.TripDetail;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wei on 2017-06-09.
 */
@Singleton
public class OfferService {
  private static ObjectMapperProvider om;
  static  {
    om = new ObjectMapperProvider();
  }
  private ItineraryAnalyzeService analyzeService;

  @Inject
  private UMappedCityLookup cityLookup;

  @Inject
  public OfferService(ItineraryAnalyzeService analyzeService, UMappedCityLookup cityLookup) {
    this.analyzeService = analyzeService;
    this.cityLookup = cityLookup;
  }


  public ItineraryOffer getOffer(String tripId) {
    ItineraryAnalyzeResult analyzeResult = analyzeTrip(tripId);
    List<LocationOffer> locationOffers = getLocationOffers(analyzeResult);
    return new ItineraryOffer(tripId, locationOffers);
  }

  public List<LocationOffer> getLocationOffers(ItineraryAnalyzeResult result) {
    List<DecisionResult> filtered = filter(result);

    List<LocationOffer> offers = new ArrayList<>();
    Map<UMappedCity, LocationOffer> offerMap = new HashMap<>();

    if (filtered != null) {
      for (DecisionResult dr : filtered) {
        UMappedCity city = mapCity(dr);

        if (city != null) {
          LocationOffer locationOffer = offerMap.get(city);
          if (locationOffer == null) {
            locationOffer = new LocationOffer(city);
            offers.add(locationOffer);
            offerMap.put(city, locationOffer);
          }
          locationOffer.add(dr);
        }
      }
    }
    return offers;
  }


  private UMappedCity mapCity(DecisionResult result) {
    if (result != null) {
      List<Location> locations = result.getLocations();
      if ((locations != null)) {
        for (Location loc : locations) {
          if (loc != null && loc.getGeoLocation() != null) {
            return cityLookup.getCity(loc.getGeoLocation());
          }
        }
      }
    }
    return null;
  }

  public List<DecisionResult> filter(ItineraryAnalyzeResult result) {
    List<DecisionResult> results = new ArrayList<>();
    if (result != null && result.getSegmentResults() != null) {
      for (TripSegmentAnalyzeResult segment : result.getSegmentResults()) {
        if (segment.getDecisionResults() != null) {
          for (DecisionResult dr : segment.getDecisionResults()) {
            if (RecommendedItemType.Content.equals(dr.getType()) || RecommendedItemType.Activity.equals(dr.getType())) {
              results.add(dr);
            }
          }
        }
      }
    }
    return results;
  }

  //trip analysis with caching
  public ItineraryAnalyzeResult analyzeTrip(String tripId) {

    //let's see if we already have a good analysis in the logs
    TripAnalysis tripAnalysis = TripAnalysis.findLastGoodAnalysis(tripId);
    if (tripAnalysis != null && tripAnalysis.getItineraryAnalyzeResult() != null) {
      //check if there were any updates since the last analysis
      List<TripAudit> tripAudits = TripAudit.getTripWithBookingsModificationEvents(tripId, tripAnalysis.getCreated_ts().getTime());
      if (tripAudits == null || tripAudits.isEmpty()) {
        try {
          ObjectReader r = om.get().readerFor(ItineraryAnalyzeResult.class);
          ItineraryAnalyzeResult result = r.readValue(tripAnalysis.getItineraryAnalyzeResult());
          return result;
        } catch (Exception e) {
          Log.err("OfferService:analyzeTrip Cannot read result from DB: " + tripId, e);
        }
      }
    }
    //no cached result - so lets' analyze the trip now
    try {
      Trip trip = Trip.findByPK(tripId);
      List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
      return  analyzeTrip(trip, tripDetails);
    } catch (Exception e) {
      Log.err("OfferService:analyzeTrip Cannot analyse trip: " + tripId, e);
    }
    return null;
  }

  //actual trip analysis with no caching
  public ItineraryAnalyzeResult analyzeTrip(Trip trip, List<TripDetail> tripDetails) {
    if (trip == null || tripDetails == null) {
      return null;
    }
    ReservationPackage rp = getReservationPackage(trip, tripDetails);
    ItineraryAnalyzeResult analyzeResult = analyzeService.analyze(rp);
    return analyzeResult;
  }

  private ReservationPackage getReservationPackage(Trip trip, List<TripDetail> tripDetails) {
    TripReservationsHelper trHelper = TripReservationsHelper.build().
        setTrip(trip).setBookings(tripDetails);
    ReservationPackage rp = trHelper.prepareReservationPackage(false);
    return rp;
  }


}
