package com.umapped.service.offer;

/**
 * Created by wei on 2017-06-13.
 */
public interface ActivityOfferProvider {
  OfferResult getActivityOffers(ActivityFilter filter);
}
