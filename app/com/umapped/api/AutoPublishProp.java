package com.umapped.api;

import com.mapped.publisher.common.APPConstants;
import models.publisher.ApiCmpyToken;
import models.publisher.Company;

/**
 * Created by twong on 15/08/17.
 */
public class AutoPublishProp {
    public boolean autoPublish = false; //CMPY_TAG_API_AUTOPUBLISH
    public boolean noPoiMatch = false; //CMPY_TAG_API_NO_POI_MATCH
    public boolean noEmail = false; //CMPY_TAG_API_AUTOPUBLISH_NO_EMAIL
    public boolean autoTripCover = false; // CMPY_TAG_API_AUTO_TRIP_COVER
    public boolean autoAddTripSummary = false;
    public String summaryTemplate = null;
    public boolean notifyAgentFirstPublish = false;
    public boolean notifyAgentErrorPublish = false;
    public boolean notifyAgentRePublish = false;
    public boolean autoAddAfarGuides = false;
    public boolean autoAddWcities = false;
    public boolean addPDFtoEmail = false;
    public boolean controlTravelers = false; //travelers will only be added and removed from API
    public boolean updateTripName = false; //

    public boolean fullReservationPacakage = false; //if true, then delete any existing booking not in the current reservation package

    public AutoPublishProp (Company company, ApiCmpyToken token) {
        String s =  null;
        if (token != null && token.getTag() != null && !token.getTag().isEmpty()) {
            s = token.getTag();
        } else {
            if (company != null && company.getTag() != null) {
               s = company.getTag();
            }
        }

        if (s != null) {
            if (s.contains(APPConstants.CMPY_TAG_API_NO_POI_MATCH)) {
                this.noPoiMatch = true;
            }
            if (s.contains(APPConstants.CMPY_TAG_API_AUTOPUBLISH)) {
                this.autoPublish = true;
            }
            if (s.contains(APPConstants.CMPY_TAG_API_AUTOPUBLISH_NO_EMAIL)) {
                this.noEmail = true;
            }
            if (s.contains(APPConstants.CMPY_TAG_API_AUTO_TRIP_COVER)) {
                this.autoTripCover = true;
            }
            if (s.contains(APPConstants.CMPY_TAG_SUMMARY_NOTE_1)) {
                this.autoAddTripSummary = true;
                this.summaryTemplate = APPConstants.CMPY_TAG_SUMMARY_NOTE_1;
            } else if (s.contains(APPConstants.CMPY_TAG_SUMMARY_NOTE)) {
                this.autoAddTripSummary = true;
                this.summaryTemplate = APPConstants.CMPY_TAG_SUMMARY_NOTE;
            }
            if (s.contains(APPConstants.CMPY_TAG_API_NOTIFY_AGENT_PUBLISH)) {
                this.notifyAgentFirstPublish = true;
            }
            if (s.contains(APPConstants.CMPY_TAG_API_NOTIFY_AGENT_REPUBLISH)) {
                this.notifyAgentRePublish = true;
            }
            if (s.contains(APPConstants.CMPY_TAG_API_NOTIFY_AGENT_PUBLISH_ERROR)) {
                this.notifyAgentErrorPublish = true;
            }

            if (s.contains(APPConstants.CMPY_TAG_API_FULL_RESERVATION_PACKAGE)) {
                this.fullReservationPacakage =true;
            }
            if (s.contains(APPConstants.CMPY_TAG_AFAR_GUIDE)) {
                this.autoAddAfarGuides = true;
            }
            if (s.contains(APPConstants.CMPY_TAG_WCITIES)) {
                this.autoAddWcities = true;
            }
            if (s.contains(APPConstants.CMPY_TAG_API_ATTACH_ITINERARY_PDF_EMAIL)) {
                this.addPDFtoEmail = true;
            }
            if (s.contains(APPConstants.CMPY_TAG_API_CONTROL_TRAVELERS)) {
                this.controlTravelers = true;
            }
            if (s.contains(APPConstants.CMPY_TAG_API_UPDATE_TRIP_NAME)) {
                this.updateTripName = true;
            }
        }
    }

    public String getTripTags () {
        StringBuilder sb = new StringBuilder();
        if (addPDFtoEmail) {
            sb.append(APPConstants.CMPY_TAG_API_ATTACH_ITINERARY_PDF_EMAIL);
        }

        return sb.toString();
    }

}
