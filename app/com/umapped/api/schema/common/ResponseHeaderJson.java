package com.umapped.api.schema.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.utils.Utils;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.api.schema.types.SeverityLevel;
import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;

import javax.annotation.Generated;

/**
 * UMapped API JSON Protocol Request Header.
 * Generated with JsonSchema2Pojo:
 * jsonschema2pojo -E -S -R -f -c3 -b -a JACKSON2 --source ... --target ...
 * Do not re-generate as additional logic was added
 * <p/>
 */
@JsonInclude(JsonInclude.Include.NON_NULL) @Generated("org.jsonschema2pojo")
@JsonPropertyOrder({"req_id", "resp_id", "timestamp", "resp_code", "resp_code_severity", "msg"})
public class ResponseHeaderJson {

  /**
   * Unique request identifier generated by the client. Should contain software session unique identifier as well
   * as sequential part
   */
  @JsonProperty("req_id")
  private String reqId;
  /**
   * Unique response identifier.
   */
  @JsonProperty("resp_id")
  private String respId;
  /**
   * ISO8601 Formatted Response Timestamp. (yyyy-MM-ddTHH:mm:ss.SSSZ)
   */
  @JsonProperty("timestamp")
  private String timestamp;
  /**
   * Numeric representation for the response code. Not to be confused with protocol HTTP codes
   */
  @JsonProperty("resp_code")
  private Integer respCode;
  /**
   * Severity levels in decreasing order
   */
  @JsonProperty("resp_code_severity")
  private SeverityLevel respCodeSeverity;
  /**
   * User facing message from the platform describing the return code. Message has not specific format
   */
  @JsonProperty("msg")
  private String msg;

  /**
   * Useful for building simple error responses
   */
  public ResponseHeaderJson(String requestId, ReturnCode rc) {
    timestamp = Utils.getISO8601Timestamp(System.currentTimeMillis());
    respId = DBConnectionMgr.getUniqueId();
    reqId = requestId;
    withCode(rc);
  }

  public ResponseHeaderJson(){}

  public ResponseHeaderJson withCode(ReturnCode rc) {
    this.respCode = rc.getIdx();
    this.msg = rc.msg();
    this.respCodeSeverity = rc.level();
    return this;
  }

  public ResponseHeaderJson withSeverityLevel(SeverityLevel level) {
    this.respCodeSeverity = level;
    return this;
  }

  public JsonNode toJson() {
    return Json.toJson(this);
  }

  /**
   * Unique request identifier generated by the client. Should contain software session unique identifier as well
   * as sequential part
   *
   * @return The reqId
   */
  @JsonProperty("req_id")
  public String getReqId() {
    return reqId;
  }

  /**
   * Unique request identifier generated by the client. Should contain software session unique identifier as well
   * as sequential part
   *
   * @param reqId The req_id
   */
  @JsonProperty("req_id")
  public void setReqId(String reqId) {
    this.reqId = reqId;
  }

  public ResponseHeaderJson withReqId(String reqId) {
    this.reqId = reqId;
    return this;
  }

  /**
   * Unique response identifier.
   *
   * @return The respId
   */
  @JsonProperty("resp_id")
  public String getRespId() {
    return respId;
  }

  /**
   * Unique response identifier.
   *
   * @param respId The resp_id
   */
  @JsonProperty("resp_id")
  public void setRespId(String respId) {
    this.respId = respId;
  }

  public ResponseHeaderJson withRespId(String respId) {
    this.respId = respId;
    return this;
  }

  /**
   * ISO8601 Formatted Response Timestamp. (yyyy-MM-ddTHH:mm:ss.SSSZ)
   *
   * @return The timestamp
   */
  @JsonProperty("timestamp")
  public String getTimestamp() {
    return timestamp;
  }

  /**
   * ISO8601 Formatted Response Timestamp. (yyyy-MM-ddTHH:mm:ss.SSSZ)
   *
   * @param timestamp The timestamp
   */
  @JsonProperty("timestamp")
  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public ResponseHeaderJson withTimestamp(String timestamp) {
    this.timestamp = timestamp;
    return this;
  }

  /**
   * Numeric representation for the response code. Not to be confused with protocol HTTP codes
   *
   * @return The respCode
   */
  @JsonProperty("resp_code")
  public Integer getRespCode() {
    return respCode;
  }

  /**
   * Numeric representation for the response code. Not to be confused with protocol HTTP codes
   *
   * @param respCode The resp_code
   */
  @JsonProperty("resp_code")
  public void setRespCode(Integer respCode) {
    this.respCode = respCode;
  }

  public void setRespCode(ReturnCode respCode) {
    this.respCode = respCode.getIdx();
  }

  public ResponseHeaderJson withRespCode(Integer respCode) {
    this.respCode = respCode;
    return this;
  }

  public ResponseHeaderJson withRespCode(ReturnCode respCode) {
    this.respCode = respCode.getIdx();
    return this;
  }


  /**
   * Severity levels in decreasing order
   *
   * @return The respCodeSeverity
   */
  @JsonProperty("resp_code_severity")
  public SeverityLevel getRespCodeSeverity() {
    return respCodeSeverity;
  }

  /**
   * Severity levels in decreasing order
   *
   * @param respCodeSeverity The resp_code_severity
   */
  @JsonProperty("resp_code_severity")
  public void setRespCodeSeverity(SeverityLevel respCodeSeverity) {
    this.respCodeSeverity = respCodeSeverity;
  }

  public ResponseHeaderJson withRespCodeSeverity(SeverityLevel respCodeSeverity) {
    this.respCodeSeverity = respCodeSeverity;
    return this;
  }

  /**
   * User facing message from the platform describing the return code. Message has not specific format
   *
   * @return The msg
   */
  @JsonProperty("msg")
  public String getMsg() {
    return msg;
  }

  /**
   * User facing message from the platform describing the return code. Message has not specific format
   *
   * @param msg The msg
   */
  @JsonProperty("msg")
  public void setMsg(String msg) {
    this.msg = msg;
  }

  public ResponseHeaderJson withMsg(String msg) {
    this.msg = msg;
    return this;
  }

}
