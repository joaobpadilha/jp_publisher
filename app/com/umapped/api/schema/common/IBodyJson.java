package com.umapped.api.schema.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Interface that must be implemented by all JSON Message Body implementations
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXTERNAL_PROPERTY)
@JsonInclude(JsonInclude.Include.NON_NULL)
public interface IBodyJson {

}
