package com.umapped.api.schema.types;

import com.fasterxml.jackson.annotation.*;


import javax.annotation.Generated;
import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL) @Generated("org.jsonschema2pojo")
@JsonPropertyOrder({"address_type", "privacy", "street_address", "locality", "region", "postal_code", "country_code",
                    "coordinates"})
public class Address
    implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  /**
   * Address type providing metadata on what the address is, main being the key address
   */
  @JsonProperty("address_type")
  private AddressType addressType;
  /**
   * Public - visible to the traveller, Private not visible to the traveller
   */
  @JsonProperty("privacy")
  private Privacy     privacy;
  /**
   * Including street number, apartment number and street name
   */
  @JsonProperty("street_address")
  private String      streetAddress;
  /**
   * Local region name like Maple, Vaughan, i.e. city, village, etc name
   */
  @JsonProperty("locality")
  private String      locality;
  /**
   * State/province in North America; County, Republic, Region elsewere
   */
  @JsonProperty("region")
  private String      region;
  @JsonProperty("postal_code")
  private String      postalCode;
  /**
   * 3-letter ISO 3166-1/GENC code (needs to be extended with appropriate pattern)
   */
  @JsonProperty("country_code")
  private String      countryCode;
  /**
   *
   */
  @JsonProperty("coordinates")
  private Coordinates coordinates;

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("[");
    if (privacy != null) {
      sb.append(privacy.name());
    }
    sb.append('@');
    if (addressType != null) {
      sb.append(addressType.name());
    }
    sb.append("] ");
    sb.append(streetAddress);
    sb.append("; ");
    sb.append(locality);
    sb.append("; ");
    sb.append(region);
    sb.append("; ");
    sb.append(postalCode);
    sb.append("; ");
    sb.append(countryCode);
    sb.append(";;");

    return sb.toString();
  }

  /**
   * Address type providing metadata on what the address is, main being the key address
   *
   * @return The addressType
   */
  @JsonProperty("address_type")
  public AddressType getAddressType() {
    return addressType;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if ((obj == null) || (obj.getClass() != this.getClass())) {
      return false;
    }

    return this.hashCode() == obj.hashCode();
  }

  /**
   * Address type providing metadata on what the address is, main being the key address
   *
   * @param addressType The address_type
   */
  @JsonProperty("address_type")
  public void setAddressType(AddressType addressType) {
    this.addressType = addressType;
  }

  @Override
  public int hashCode() {
    int hash = 1;
    if (addressType != null) {
      hash *= addressType.hashCode();
    }

    if (privacy != null) {
      hash *= privacy.hashCode();
    }

    if (streetAddress != null && streetAddress.length() > 0) {
      hash *= streetAddress.hashCode();
    }

    if (locality != null && locality.length() > 0) {
      hash *= locality.hashCode();
    }

    if (region != null && region.length() > 0) {
      hash *= region.hashCode();
    }

    if (postalCode != null && postalCode.length() > 0) {
      hash *= postalCode.hashCode();
    }

    if (countryCode != null && countryCode.length() > 0) {
      hash *= countryCode.hashCode();
    }

    if (coordinates != null) {
      hash *= coordinates.hashCode();
    }
    return hash;
  }

  public Address withAddressType(AddressType addressType) {
    this.addressType = addressType;
    return this;
  }

  /**
   * Public - visible to the traveller, Private not visible to the traveller
   *
   * @return The privacy
   */
  @JsonProperty("privacy")
  public Privacy getPrivacy() {
    return privacy;
  }

  /**
   * Public - visible to the traveller, Private not visible to the traveller
   *
   * @param privacy The privacy
   */
  @JsonProperty("privacy")
  public void setPrivacy(Privacy privacy) {
    this.privacy = privacy;
  }

  public Address withPrivacy(Privacy privacy) {
    this.privacy = privacy;
    return this;
  }

  /**
   * Including street number, apartment number and street name
   *
   * @return The streetAddress
   */
  @JsonProperty("street_address")
  public String getStreetAddress() {
    return streetAddress;
  }

  /**
   * Including street number, apartment number and street name
   *
   * @param streetAddress The street_address
   */
  @JsonProperty("street_address")
  public void setStreetAddress(String streetAddress) {
    this.streetAddress = streetAddress;
  }

  public Address withStreetAddress(String streetAddress) {
    this.streetAddress = streetAddress;
    return this;
  }

  /**
   * Local region name like Maple, Vaughan, i.e. city, village, etc name
   *
   * @return The locality
   */
  @JsonProperty("locality")
  public String getLocality() {
    return locality;
  }

  /**
   * Local region name like Maple, Vaughan, i.e. city, village, etc name
   *
   * @param locality The locality
   */
  @JsonProperty("locality")
  public void setLocality(String locality) {
    this.locality = locality;
  }

  public Address withLocality(String locality) {
    this.locality = locality;
    return this;
  }

  /**
   * State/province in North America; County, Republic, Region elsewere
   *
   * @return The region
   */
  @JsonProperty("region")
  public String getRegion() {
    return region;
  }

  /**
   * State/province in North America; County, Republic, Region elsewere
   *
   * @param region The region
   */
  @JsonProperty("region")
  public void setRegion(String region) {
    this.region = region;
  }

  public Address withRegion(String region) {
    this.region = region;
    return this;
  }

  /**
   * @return The postalCode
   */
  @JsonProperty("postal_code")
  public String getPostalCode() {
    return postalCode;
  }

  /**
   * @param postalCode The postal_code
   */
  @JsonProperty("postal_code")
  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public Address withPostalCode(String postalCode) {
    this.postalCode = postalCode;
    return this;
  }

  /**
   * 2-letter ISO 3166-1 code needs to be extended with appropriate pattern
   *
   * @return The countryCode
   */
  @JsonProperty("country_code")
  public String getCountryCode() {
    return countryCode;
  }

  /**
   * 2-letter ISO 3166-1 code needs to be extended with appropriate pattern
   *
   * @param countryCode The country_code
   */
  @JsonProperty("country_code")
  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public Address withCountryCode(String countryCode) {
    this.countryCode = countryCode;
    return this;
  }

  /**
   * @return The coordinates
   */
  @JsonProperty("coordinates")
  public Coordinates getCoordinates() {
    return coordinates;
  }

  /**
   * @param coordinates The coordinates
   */
  @JsonProperty("coordinates")
  public void setCoordinates(Coordinates coordinates) {
    this.coordinates = coordinates;
  }

  public Address withCoordinates(Coordinates coordinates) {
    this.coordinates = coordinates;
    return this;
  }


}
