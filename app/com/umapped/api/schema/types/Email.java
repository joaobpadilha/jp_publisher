package com.umapped.api.schema.types;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


/**
 * Email information
 */
@JsonInclude(JsonInclude.Include.NON_NULL) @Generated("org.jsonschema2pojo")
@JsonPropertyOrder({"privacy", "email_type", "address"})
public class Email implements Serializable {

  /**
   * Public - visible to the traveller, Private not visible to the traveller
   */
  @JsonProperty("privacy")
  private Privacy privacy;
  @JsonProperty("email_type")
  private Email.EmailType emailType;
  @JsonProperty("address")
  private String address;

  @Generated("org.jsonschema2pojo")
  public static enum EmailType {

    MAIN("main"),
    BUSINESS("business"),
    BILLING("billing"),
    HOME("home");
    private static Map<String, EmailType> constants = new HashMap<String, EmailType>();

    static {
      for (Email.EmailType c : values()) {
        constants.put(c.value, c);
      }
    }

    private final String value;

    private EmailType(String value) {
      this.value = value;
    }

    @JsonCreator
    public static Email.EmailType fromValue(String value) {
      Email.EmailType constant = constants.get(value);
      if (constant == null) {
        throw new IllegalArgumentException(value);
      }
      else {
        return constant;
      }
    }

    @JsonValue @Override
    public String toString() {
      return this.value;
    }

  }

  public Email() {}

  public Email(String address, EmailType type, Privacy privacy) {
    this.privacy = privacy;
    this.emailType = type;
    this.address = address;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if ((obj == null) || (obj.getClass() != this.getClass())) {
      return false;
    }

    return this.hashCode() == obj.hashCode();
  }

  /**
   * Public - visible to the traveller, Private not visible to the traveller
   *
   * @return The privacy
   */
  @JsonProperty("privacy")
  public Privacy getPrivacy() {
    return privacy;
  }

  @Override
  public int hashCode() {
    int hash = 1;

    if (privacy != null && privacy.hashCode() != 0) {
      hash *= privacy.hashCode();
    }

    if (emailType != null && emailType.hashCode() != 0) {
      hash *= emailType.hashCode();
    }

    if (address != null && address.hashCode() != 0) {
      hash *= address.hashCode();
    }

    return hash;
  }

  /**
   * Public - visible to the traveller, Private not visible to the traveller
   *
   * @param privacy The privacy
   */
  @JsonProperty("privacy")
  public void setPrivacy(Privacy privacy) {
    this.privacy = privacy;
  }

  public Email withPrivacy(Privacy privacy) {
    this.privacy = privacy;
    return this;
  }

  @Override
  public String toString() {
    return "[" + privacy.name() + "@" + address + "]" + emailType.name() + ";;";
  }

  /**
   * @return The emailType
   */
  @JsonProperty("email_type")
  public Email.EmailType getEmailType() {
    return emailType;
  }

  /**
   * @param emailType The email_type
   */
  @JsonProperty("email_type")
  public void setEmailType(Email.EmailType emailType) {
    this.emailType = emailType;
  }

  public Email withEmailType(Email.EmailType emailType) {
    this.emailType = emailType;
    return this;
  }

  /**
   * @return The address
   */
  @JsonProperty("address")
  public String getAddress() {
    return address;
  }

  /**
   * @param address The address
   */
  @JsonProperty("address")
  public void setAddress(String address) {
    this.address = address;
  }

  public Email withAddress(String address) {
    this.address = address;
    return this;
  }


}
