package com.umapped.api.schema.types;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@Generated("org.jsonschema2pojo")
public enum Privacy {

  PUBLIC("public"),
  PRIVATE("private");
  private final String value;
  private static Map<String, Privacy> constants = new HashMap<String, Privacy>();

  static {
    for (Privacy c: values()) {
      constants.put(c.value, c);
    }
  }

  private Privacy(String value) {
    this.value = value;
  }

  @JsonValue
  @Override
  public String toString() {
    return this.value;
  }

  @JsonCreator
  public static Privacy fromValue(String value) {
    Privacy constant = constants.get(value);
    if (constant == null) {
      throw new IllegalArgumentException(value);
    } else {
      return constant;
    }
  }

}
