package com.umapped.api.schema.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.annotation.Generated;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;


/**
 * Additional features available for POI
 */
@JsonInclude(JsonInclude.Include.NON_NULL) @Generated("org.jsonschema2pojo")
@JsonPropertyOrder({"name", "desc", "tags"})
public class Feature
    implements Serializable {

  /**
   * Feature Name
   */
  @JsonProperty("name")
  private String name;
  /**
   * Feature description
   */
  @JsonProperty("desc")
  private String desc;
  /**
   * Feature tags should allow setting various parameters specific to this tag
   */
  @JsonProperty("tags") @JsonDeserialize(as = java.util.LinkedHashSet.class)
  private Set<String> tags = new LinkedHashSet<String>();

  /**
   * Feature Name
   *
   * @return The name
   */
  @JsonProperty("name")
  public String getName() {
    return name;
  }

  /**
   * Feature Name
   *
   * @param name The name
   */
  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

  public Feature withName(String name) {
    this.name = name;
    return this;
  }

  /**
   * Feature description
   *
   * @return The desc
   */
  @JsonProperty("desc")
  public String getDesc() {
    return desc;
  }

  /**
   * Feature description
   *
   * @param desc The desc
   */
  @JsonProperty("desc")
  public void setDesc(String desc) {
    this.desc = desc;
  }

  public Feature withDesc(String desc) {
    this.desc = desc;
    return this;
  }

  /**
   * Feature tags should allow setting various parameters specific to this tag
   *
   * @return The tags
   */
  @JsonProperty("tags")
  public Set<String> getTags() {
    return tags;
  }

  /**
   * Feature tags should allow setting various parameters specific to this tag
   *
   * @param tags The tags
   */
  @JsonProperty("tags")
  public void setTags(Set<String> tags) {
    this.tags = tags;
  }

  public Feature withTags(Set<String> tags) {
    this.tags = tags;
    return this;
  }

  /**
   * Helper function to add tags one by one
   *
   * @param tag
   * @return
   */
  public Feature addTag(String tag) {
    this.tags.add(tag);
    return this;
  }

}

