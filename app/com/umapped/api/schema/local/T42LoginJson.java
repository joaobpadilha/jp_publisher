package com.umapped.api.schema.local;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.umapped.api.schema.common.IBodyJson;

import java.util.Map;

/**
 * Authentication and response in the form of available documents.
 * Created by surge on 2015-04-30.
 */
@JsonTypeName("T42LOGIN") @JsonInclude(JsonInclude.Include.NON_NULL)
public class T42LoginJson
    extends AuthJson
    implements IBodyJson {

  /** Request ID to track for state */
  @JsonProperty(required = false, value = "jobId")
  public String jobId;

  /** Login state */
  @JsonProperty(required = false, value = "state")
  public String state;


  /** Login state */
  @JsonProperty(required = false, value = "links")
  public Map<String, String> links;
}
