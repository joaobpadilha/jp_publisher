package com.umapped.api.schema.local;

import actors.MobilizerActor;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.umapped.api.schema.common.IBodyJson;
import com.umapped.api.schema.types.Operation;

import java.util.ArrayList;
import java.util.List;

/**
 * Used to allow search for articles that can be "mobilized"
 * Created by surge on 2014-12-10.
 */
@JsonTypeName("WEBSEARCH")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WebSearchJson implements IBodyJson {
  @JsonProperty(required = false, value = "operation")
  public Operation operation;

  /** ID of the parent object (i.e. Document ID/Destination ID */
  @JsonProperty(required = false, value = "id")
  public String id;

  /** ID of the parent object (i.e. Trip ID */
  @JsonProperty(required = false, value = "tripId")
  public String tripId;

  /** ID of the parent object (i.e. Trip ID */
  @JsonProperty(required = false, value = "date")
  public String date;

  @JsonProperty(required = false, value = "search_term")
  public String searchTerm;

  /** Company ID of the POI (string version) */
  @JsonProperty(required = false, value = "cmpyid")
  public String cmpyId;

  /**
   * Start location for search results
   */
  @JsonProperty(required = false, value = "cursor")
  public Integer cursor;

  /**
   * Maximum number of results
   */
  @JsonProperty(required = false, value = "count")
  public Integer count;

  /**
   * State of the background job
   */
  @JsonProperty(required = false, value = "state")
  public String state;

  /**
   * Background job id
   */
  @JsonProperty(required = false, value = "jobId")
  public String jobId;

  /**
   * Background job command name
   */
  @JsonProperty(required = false, value = "jobCmd")
  public MobilizerActor.MobilizerCommand jobCmd;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static class PageDetails {
    @JsonProperty(required = true, value = "resultId")
    public String resultId;

    /**  Page name */
    @JsonProperty(required = true, value = "title")
    public String title;

    /** Raw URL */
    @JsonProperty(required = true, value = "url")
    public String url;

    /** Friendly URL to show along with the name */
    @JsonProperty(required = false, value = "displayUrl")
    public String displayUrl;

    /** Short description (usually from search engine) */
    @JsonProperty(required = false, value = "description")
    public String description;

    /** HTML Contents that might be sent when page is mobilized */
    @JsonProperty(required = false, value = "content")
    public String content;

    /** Extracted name of the article's author */
    @JsonProperty(required = false, value = "author")
    public String author;
  }

  @JsonProperty(required = false, value = "pages")
  public List<PageDetails> pages = null;

  @JsonProperty(required = false, value = "domains")
  public List<String> domains = null;

  public void addDomain(String domain) {
    if (domains == null) {
      domains = new ArrayList<>();
    }
    domains.add(domain);
  }

  /**
   * Creates a new file based on input parameters
   * @return created file object with fields provided filled in.
   */
  public PageDetails addPage(String id, String title, String url, String displayUrl, String desc) {
    if (pages == null) {
      pages = new ArrayList<>();
    }
    PageDetails pd = new PageDetails();
    pd.resultId = id;
    pd.title = title;
    pd.url = url;
    pd.displayUrl = displayUrl;
    pd.description = desc;

    pages.add(pd);

    return pd;
  }

  /**
   * Helper
   * @return created file object with fields provided filled in.
   */
  public PageDetails addPage(PageDetails pd) {
    if (pages == null) {
      pages = new ArrayList<>();
    }

    pages.add(pd);
    return pd;
  }



}
