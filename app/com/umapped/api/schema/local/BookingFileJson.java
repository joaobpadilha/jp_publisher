package com.umapped.api.schema.local;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Created by surge on 2015-03-09.
 */
@JsonTypeName("BKFILE")
public class BookingFileJson
    extends PoiFileJson {

  /**
   * Booking "details" ID
   */
  @JsonProperty(required = false, value = "detailsId")
  public String detailsId;
  @JsonProperty(required = false, value = "showPhotos")
  public Boolean showPhotos;

}
