package com.umapped.api.schema.local;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.umapped.api.schema.common.IBodyJson;
import com.umapped.api.schema.types.Operation;

import java.util.List;

/**
 * Created by surge on 2014-11-07.
 */
@JsonTypeName("COUNTRY") @JsonInclude(JsonInclude.Include.NON_NULL)
public class CountryJson
    implements IBodyJson {

  @JsonProperty(required = false, value = "operation")
  public Operation operation;

  @JsonProperty(required = false, value = "search_term")
  public String searchTerm;

  @JsonProperty(required = false, value = "search_id")
  public Integer searchId;

  /**
   * Start location for search results
   */
  @JsonProperty(required = false, value = "cursor")
  public Integer cursor;

  /**
   * Maximum number of results
   */
  @JsonProperty(required = false, value = "count")
  public Integer count;


  @JsonProperty(required = true)
  public List<CountryDetails> countries;

  public static class CountryDetails {
    @JsonProperty(required = true)
    public String alpha2;
    @JsonProperty(required = true)
    public String alpha3;
    @JsonProperty(required = true)
    public String name;
  }

}
