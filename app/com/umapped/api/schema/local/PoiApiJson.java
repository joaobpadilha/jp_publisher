package com.umapped.api.schema.local;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.mapped.publisher.persistence.PoiRS;
import com.mapped.publisher.utils.Log;
import com.umapped.api.schema.common.IBodyJson;
import com.umapped.api.schema.types.Operation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Extended version of the POI schema for use by UMapped API
 * <p/>
 * Created by surge on 2014-11-03.
 */
@JsonTypeName("POI") @JsonInclude(JsonInclude.Include.NON_NULL)
public class PoiApiJson
    implements IBodyJson {

  @JsonProperty(required = false, value = "operation")
  public Operation operation;

  @JsonProperty(required = false, value = "search_term")
  public String searchTerm;


  public String getSearchId() {
    return searchId;
  }

  public void setSearchId(String searchId) {
    this.searchId = searchId;
  }

  //TODO: figure out how to serialize & deserialize automatically from Long
  @JsonProperty(required = false, value = "search_id")
  private String searchId;

  /**
   * Start location for search results
   */
  @JsonProperty(required = false, value = "cursor")
  public Integer cursor;

  /**
   * Maximum number of results
   */
  @JsonProperty(required = false, value = "count")
  public Integer count;

  /**
   * Company id used for searching
   */
  @JsonProperty(required = false, value = "cmpy_id")
  public Integer cmpyId;

  /**
   * Poi Types available on the platform
   */
  @JsonProperty(required = false, value = "types")
  public Map<Integer, String> types;

  /**
   * Poi Types selected during search by the user
   */
  @JsonProperty(required = false, value = "selected_types")
  public List<Integer> selectedTypes;

  public List<PoiJson> pois;

  /**
   * Adding pois to the
   */
  public void addPoi(PoiRS rs) {
    if (rs == null || rs.data == null) {
      Log.err("POI JSON: Attempting to add RS with empty data...");
      return;
    }
    if (pois == null) {
      pois = new ArrayList<>();
    }
    rs.data.prepareForBrowser();
    pois.add(rs.data);
  }
}
