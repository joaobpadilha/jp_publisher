package com.umapped.api.schema.local;

/**
 * Created by wei on 2017-05-22.
 */
public class PoiPhoto {
  public String thumb;
  public String url;
  public String caption;
  public String fileName;
  public Long importId; //Id of the import record if it was not yet matched
}
