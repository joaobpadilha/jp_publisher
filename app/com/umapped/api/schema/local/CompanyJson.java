package com.umapped.api.schema.local;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.umapped.api.schema.common.IBodyJson;
import com.umapped.api.schema.types.Operation;
import models.publisher.UserCmpyLink;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2014-10-31.
 */
@JsonTypeName("COMPANY")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompanyJson
    implements IBodyJson {

  @JsonProperty(required = false, value = "operation")
  public Operation operation;

  @JsonProperty(required = false, value = "search_term")
  public String searchTerm;

  @JsonProperty(required = false, value = "search_id")
  public Integer searchId;

  /**
   * Start location for search results
   */
  @JsonProperty(required = false, value = "cursor")
  public Integer cursor;

  /**
   * Maximum number of results
   */
  @JsonProperty(required = false, value = "count")
  public Integer count;


  //TODO: Serguei: Extend to include all company details
  public static class CompanyDetailsJson {
    @JsonProperty(value = "id")
    public Integer cmpyId;
    @JsonProperty(value = "legacy_id")
    public String legacyId;
    @JsonProperty(value = "name")
    public String name;
    @JsonProperty(value = "user_link_type")
    public UserCmpyLink.LinkType userLinkType;
  }

  /**
   * List of companies which to which operation is applied
   */
  public List<CompanyDetailsJson> companies;

  public void addCompany(CompanyDetailsJson cdj) {
    if (companies == null) {
      companies = new ArrayList<>();
    }
    companies.add(cdj);
  }

  public void addCompany(Integer id, String legacyId, String name) {
    CompanyDetailsJson cdj = new CompanyDetailsJson();
    cdj.legacyId = legacyId;
    cdj.cmpyId = id;
    cdj.name = name;
    addCompany(cdj);
  }


}
