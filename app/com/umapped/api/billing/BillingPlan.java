package com.umapped.api.billing;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.mapped.publisher.parse.schemaorg.DateTime;

import java.io.Serializable;
import java.util.List;

/**
 * Created by twong on 2016-10-18.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BillingPlan implements Serializable {
  public String id;
  public String name;
  public String description;
  public String price;
  public String cur;
  public DateTime lastBilDate;
  public DateTime nextBilDate;

  public List<BillingAddon> addons;
  public BillingAgent agent;
}
