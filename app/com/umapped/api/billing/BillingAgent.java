package com.umapped.api.billing;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * Created by twong on 2016-10-18.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BillingAgent implements Serializable {
  public String uid;
  public String fName;
  public String lName;
  public String legacyUserid;

}
