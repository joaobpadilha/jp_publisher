package com.umapped.api.billing;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * Created by twong on 2016-11-02.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BillingCCInfo implements Serializable{

  public String cardNumber;
  public String expiryYY;
  public String expiryMM;
  public String cardType;
  public String imgUrl;

}
