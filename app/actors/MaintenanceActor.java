package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.mapped.publisher.js.BackgroundJobStateView;
import com.mapped.publisher.persistence.communicator.Communicator;
import com.mapped.publisher.persistence.communicator.MessengerUpdateHelper;
import com.mapped.publisher.utils.Log;
import models.publisher.MessengerConference;
import models.publisher.Trip;

/**
 * Actor to perform various maintenance procedures in Publisher
 * Created by surge on 2016-08-08.
 */
public class MaintenanceActor
    extends UntypedActor {

  public final static boolean DEBUG = false;

  public final static Props props = Props.create(MaintenanceActor.class);

  public interface Factory {
    public Actor create();
  }

  public static class Protocol {
    public enum Command {
      /**
       * Clean all eligible conferences in the messenger
       */
      CLEAN_MESSENGER
    }

    public Command        cmd;
    public Message        msg;

    public static class Message {
      /**
       * Usually account ID of the user requesting the operation
       */
      public Long accountId;
    }

    private Protocol() {
    }

    public static Protocol build(Command cmd) {
      Protocol p = new Protocol();
      p.cmd = cmd;

      switch (cmd) {
        case CLEAN_MESSENGER:
          p.msg = new Message();
          break;
      }
      return p;
    }
  }

  @Override
  public void onReceive(Object message)
      throws Exception {
    Log.info("MaintenanceActor: Received message");
    if (message instanceof Protocol) {
      Protocol p = (Protocol) message;
      switch (p.cmd) {
        case CLEAN_MESSENGER:
          this.cleanMessenger(p.msg);
          break;
      }
    }
    else {
      unhandled(message);
    }
  }

  private void cleanMessenger(Protocol.Message msg) {
    BackgroundJobStateView bjsv = BackgroundJobStateView.getFromCache(Protocol.Command.CLEAN_MESSENGER.name());
    if(bjsv.condition == BackgroundJobStateView.Condition.RUNNING && bjsv.isUpdatedInTheLastMinutes(5)) {
      Log.err("MaintenanceActor: Attempting to run new messenger cleaner while previous copy is running...");
      return;
    }

    if(!bjsv.isUpdatedInTheLastMinutes(5)) {
      Log.err("MaintenanceActor: Last messenger clean job is still running but got stuck. Trying again. " +
              "Keep fingers crossed.");
    }

    bjsv.state = "Umapped Messenger Conference Cleaning";
    bjsv.updateRate = 2;

    if(DEBUG) {
      int maxVal = MessengerConference.find.where().eq("removed", false).findCount();
      bjsv.start(BackgroundJobStateView.Condition.RUNNING, maxVal);

      //While debugging use this approach:
      MessengerConference.find.where().eq("removed", false).orderBy("tripid").findEach((MessengerConference mc) -> {
        Trip t = Trip.findByPK(mc.getTripid());
        MessengerUpdateHelper.build(t, msg.accountId).wipeTrip();
        bjsv.next();
      });
    } else {
      long tripendlimit = System.currentTimeMillis() - Communicator.TRIP_EXPIRATION_OFFSET;

      int maxVal = Trip.messengerTripsToCleanCount(Communicator.RELEASE_TS_1_15, tripendlimit);
      bjsv.start(BackgroundJobStateView.Condition.RUNNING, maxVal);

      Trip.messengerTripsToCleanRun(Communicator.RELEASE_TS_1_15, tripendlimit,(Trip t) -> {
        MessengerUpdateHelper.build(t, msg.accountId).wipeTrip();
        bjsv.next();
      });
    }

    bjsv.stop();
  }
}
