package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.firebase.client.DataSnapshot;
import com.google.inject.Inject;
import com.mapped.publisher.persistence.communicator.Communicator;
import com.mapped.publisher.persistence.communicator.RoomMessage;
import com.mapped.publisher.persistence.communicator.SentMessageNotification;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.utils.Log;
import models.publisher.Account;
import models.publisher.AccountTripLink;
import models.publisher.UserProfile;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * Process and distribute replies from a variety of sources
 * Created by surge on 2016-02-02.
 */
public class CommunicatorReplyActor
    extends UntypedActor {

  public final static Props  props           = Props.create(CommunicatorReplyActor.class);
  public static final String REPLY_SEPARATOR = "##- Please type your reply above this line -##";
  @Inject
  RedisMgr redis;

  public static class Protocol {
    public enum Command {
      /**
       * Pre-processed reply
       */
      REPLY
    }

    public Command cmd;
    public Reply   reply;

    public static class Reply {
      public String  tripId;
      /**
       * If null, then treated as "internal" room
       */
      public String  detailsId;
      public String  userId;
      public String  travelerId;
      public String  message;
      public Boolean isHtml;
      public String fromEmail;
    }

    private Protocol() {
    }

    public static Protocol build(Command cmd) {
      Protocol p = new Protocol();
      p.cmd = cmd;
      switch (cmd) {
        case REPLY:
          p.reply = new Reply();
          break;
      }
      return p;
    }
  }

  @Override
  public void onReceive(Object message)
      throws Exception {

    Log.info("CommunicatorReplyActor: Received message");

    if (message instanceof Protocol) {
      Protocol p = (Protocol) message;

      switch (p.cmd) {
        case REPLY:
          processReply(p.reply);
          break;
      }
    }
    else {
      unhandled(message);
    }
  }

  void processReply(Protocol.Reply r) {
    try {
      RoomMessage msg = new RoomMessage();
      if (r.isHtml) {
        msg.message = extractReply(r.message);
      }
      else {
        msg.message = r.message;
      }

      String confId = r.tripId;
      String roomId = r.detailsId;

      switch (r.detailsId) {
        case "general":
        case "internal":
          roomId = confId + "-" + r.detailsId;
          break;
        default:
          break;
      }

      if (r.userId != null) {
        UserProfile up = UserProfile.findByPK(r.userId);
        if (up == null && r.userId.contains("_")) {
          up = UserProfile.findByPK(r.userId.replace('_','.'));
        }
        if(up == null || r.fromEmail == null || !r.fromEmail.toLowerCase().contains(up.getEmail().toLowerCase())) {
          Log.err("Wrong user id: " + r.userId + " from: " + r.fromEmail);
          return;
        }
        msg.userId = up.getUserid();
        msg.name = up.getFullName();
      }
      else if (r.travelerId != null) {
        AccountTripLink tripLink = AccountTripLink.findByLegacyId(r.travelerId);
        if (tripLink == null) {
          Log.err("Wrong traveller id: " + r.travelerId + " from: " + r.fromEmail);
          return;
        }
        Account account = Account.find.byId(tripLink.getPk().getUid());
        if(account == null || r.fromEmail == null || !r.fromEmail.toLowerCase().contains(account.getEmail().toLowerCase())) {
          Log.err("Wrong traveller id: " + r.travelerId + " from: " + r.fromEmail);
          return;
        }
        msg.userId = r.travelerId;
        msg.name = account.getFullName();
      }

      //Sending to the Communicator:
      msg.timestamp = System.currentTimeMillis();
      Communicator c = Communicator.Instance();
      c.sendMsg(confId, roomId, msg);

      final SentMessageNotification sentNotification = new SentMessageNotification();
      sentNotification.confId = confId;
      sentNotification.roomId = roomId;
      sentNotification.message = msg.message;
      sentNotification.from = msg.userId;

      c.onConfInfo(confId, roomId, (roomMeta, roomUsers) -> {
        try {
          List<String> online = new ArrayList<>();
          for (DataSnapshot ds : roomUsers.getChildren()) {
            online.add(ds.getKey());
          }

          List<String> notified = new ArrayList<>();
          for (DataSnapshot ds : roomMeta.child("notifyUsers").getChildren()) {
            notified.add(ds.getKey());
          }

          sentNotification.roomNotify = notified;
          sentNotification.roomUsers = online;
          sentNotification.roomUsers.add(msg.userId); //Appear user who sent the message as if he/she is online

          ObjectMapper om      = new ObjectMapper();
          String       jsonStr = om.writeValueAsString(sentNotification);
          redis.publish(CommunicatorToAllActor.COMM2ALL_CHANNEL, jsonStr);
        }
        catch (JsonProcessingException e) {
          Log.err("FromEmailActor: Failed to serialize Sent Message Notification", e);
        }
      });

    }
    catch (Exception e) {
      Log.err("Failed to process reply 2way message", e);
    }
  }

  /**
   * Two-tier attempt to remove quoted text from the email HTML body.
   * https://www.w3.org/TR/1998/NOTE-HTMLThreading-0105#Determining Metadata from Text
   *
   * @param html
   * @return
   */
  public static String extractReply(String html) {
    try {
      Document doc = Jsoup.parseBodyFragment(html);
      doc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);

      doc.select("head, title, meta, style").remove(); //First level cleanup
      doc.select(":containsOwn(\u00a0)").remove(); //Removing fake paragraphs with nbsp

      Element body = doc.body();

      Elements quote = body.select("blockquote");
      boolean hasBlockquote = !quote.isEmpty();

      if (hasBlockquote) { //Proper email client - thank you

        //Going route of removing block quoted area
        quote.remove();
      }

      String cleanedContent = body.html();
      // get pretty printed html with preserved br and p tags
      cleanedContent = Jsoup.clean(cleanedContent,
              "",
              Whitelist.none().addTags("br", "p"),
              new Document.OutputSettings().prettyPrint(true));

      // get plain text with preserved line breaks by disabled prettyPrint
      cleanedContent = Jsoup.clean(cleanedContent,
              "",
              Whitelist.none(),
              new Document.OutputSettings().prettyPrint(false));


      StringBuilder sb = new StringBuilder();
      String[] lines = cleanedContent.split(System.lineSeparator());

      for (String line : lines) {
        if (line.contains(CommunicatorToAllActor.FROM_NAME)) {
          break;
        }
        if (line.contains(REPLY_SEPARATOR)) {
          break;
        }
        sb.append(line.trim());
        sb.append(System.lineSeparator());
      }

      return sb.toString().trim();
    } catch (Exception e) {
      Log.err("FromEmailActor - extractReply: Failed to extract Message", e);
      Log.err("FromEmailActor -msg: " + html);
      return html;
    }
  }

  public interface Factory {
    Actor create();
  }
}
