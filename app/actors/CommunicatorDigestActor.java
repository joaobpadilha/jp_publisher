package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.google.inject.Inject;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.Capability;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.persistence.communicator.Communicator;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.UmappedEmail;
import com.mapped.publisher.view.email.CommunicatorDigestMessageView;
import com.umapped.persistence.digest.CommunicatorMsg;
import models.publisher.*;
import org.apache.commons.lang3.StringUtils;

import java.net.URI;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created by twong on 07/04/16.
 */
public class CommunicatorDigestActor extends UntypedActor {
    @Inject
    RedisMgr redis;

    public static final String FROM_NAME = "Umapped Notifications";

    public final static Props props = Props.create(DocPDFActor.class);

    public interface Factory {
        Actor create();
    }

    public static class Protocol {
        public enum Command {
            AGENT_DIGEST
        }

        public Command cmd;

        private Protocol() {
        }

        public static Protocol build(Command cmd) {
            Protocol p = new Protocol();
            p.cmd = cmd;
            return p;
        }
    }

    @Override
    public void onReceive(Object message)
            throws Exception {
        Log.info("Communicator Digest: Got message in... checking");
        if (message instanceof Protocol) {
            Protocol p = (Protocol) message;

            switch (p.cmd) {
                case AGENT_DIGEST:
                    System.out.println("----- G O T  M S G");
                    processDigest();
                    break;
            }
        }
    }

    private void processDigest() {
        Set<String> values = redis.getSet(APPConstants.CACHE_DIGEST_ACCOUNT_LIST_KEY);

        if (values != null && values.size() > 0) {
            for (String accountId:values) {
                Account account = null;
                AccountTripLink travelerLink = null;
                UserProfile up = null;
                CommunicatorDigestMessageView view = null;
                if ( StringUtils.isNumeric(accountId)) {
                    travelerLink = AccountTripLink.findByLegacyId(accountId);
                } else {
                    up = UserProfile.findByPK(accountId);
                    if (up == null && accountId.contains("_")) {
                        up = UserProfile.findByPK(accountId.replace('_','.'));
                    }
                    if (up != null) {
                        account = Account.findActiveByLegacyId(up.getUserid());
                    }
                }
                if (account != null) {
                    EnumSet<Capability> capabilities = SecurityMgr.getCapabilities(account.getLegacyId());
                    if (capabilities.contains(Capability.COMMUNICATOR_MSG_DIGEST)) {
                        Log.debug("CommunicatorDigestActor - PROCESSING digest for Agent " + accountId);
                        view = new CommunicatorDigestMessageView();
                        view.firstName = account.getFirstName();
                        view.lastName = account.getLastName();
                        view.fullName = account.getFullName();
                        view.toEmail = account.getEmail();
                        view.accountId = account.getLegacyId();
                        view.uid = account.getUid();
                        List<CommunicatorMsg> msgList = (List<CommunicatorMsg>) redis.getBList(APPConstants.CACHE_DIGEST_MSGS_BY_ACCOUNT_PREFIX + accountId);
                        view = buildView(msgList, view, up, null);
                        if (view != null && view.allMessages.size() > 0) {
                            sendDigest(view, true);
                        }
                    } else {
                        Log.debug("CommunicatorDigestActor - SKIPPING digest for " + accountId);
                    }
                } else if (travelerLink != null) {
                    Account traveler = Account.find.byId(travelerLink.getPk().getUid());
                  if (traveler != null) {
                    Log.debug("CommunicatorDigestActor - PROCESSING digest for Traveler " + accountId);
                    view = new CommunicatorDigestMessageView();
                    view.firstName = "";
                    view.lastName = "";
                    view.fullName = traveler.getFullName();
                    view.toEmail = traveler.getEmail();
                    view.accountId = travelerLink.getLegacyId();
                    view.uid = traveler.getUid();
                    List<CommunicatorMsg> msgList = (List<CommunicatorMsg>) redis.getBList(APPConstants.CACHE_DIGEST_MSGS_BY_ACCOUNT_PREFIX + accountId);
                    view = buildView(msgList, view, null, travelerLink);
                    if (view != null && view.allMessages.size() > 0) {
                      sendDigest(view, false);
                    }
                  }
                }



                //remove info from redis
                redis.removeFromSet(APPConstants.CACHE_DIGEST_ACCOUNT_LIST_KEY, accountId);
                redis.delKey(APPConstants.CACHE_DIGEST_MSGS_BY_ACCOUNT_PREFIX + accountId);

            }
        }
    }

    private void sendDigest (CommunicatorDigestMessageView view, boolean agent) {
        try {
            UmappedEmail email = UmappedEmail.buildDefault();
            if (agent) {
                email.withHtml(views.html.email.communicatorDigestMessage.render(view).toString())
                        .withSubject("New Umapped Messages")
                        .setFrom("no-reply@umapped.com", FROM_NAME)
                        .addReplyTo("no-reply@umapped.com");
            } else {
                email.withHtml(views.html.email.communicatorTravelerDigestMsg.render(view).toString())
                        .withSubject("New Umapped Messages")
                        .setFrom("no-reply@umapped.com", FROM_NAME)
                        .addReplyTo("no-reply@umapped.com");
            }
            List<String> emails = new ArrayList<>();
            emails.add(view.toEmail);

            email.withToList(emails)
                 .withEmailType(EmailLog.EmailTypes.DIGEST)
                 .withAccountUid(view.uid);


            email.addTo(view.toEmail, view.fullName);

            email.buildAndSend();
        } catch (Exception e) {
            Log.err("CommunicatorDigestActor - Cannot sent digest to: " + view.toEmail, e);
        }

    }

    private CommunicatorDigestMessageView buildView (List<CommunicatorMsg> msgList, CommunicatorDigestMessageView view, UserProfile up, AccountTripLink tp) {
        boolean tripEmailNotification = false; //if trip is enabled for real-time, do not send digest
        Trip trip = null;
        if (msgList != null) {
            for (CommunicatorMsg msg1 : msgList) {
                CommunicatorDigestMessageView.TripDigestMessage tripMsgs = view.allMessages.get(msg1.getTripId());
                if (tripMsgs == null) { //new trip
                    tripMsgs = view.new TripDigestMessage();
                    tripMsgs.tripName = msg1.getTripName();
                    view.sortedTripId.add(msg1.getTripId());
                    trip = Trip.find.byId(msg1.getTripId());
                    if (trip != null && SecurityMgr.hasCapability(trip, Capability.COMMUNICATOR_NOTIFICATION_EMAIL)) {
                        tripEmailNotification = true;
                    } else {
                        tripEmailNotification = false;
                    }
                    if (tp != null) {
                      tripMsgs.groupId = tp.getLegacyGroupId();
                    }
                }
                if (!tripEmailNotification) { //if this trip does not have real-time notifications, then include the msgs in the digest
                    //group messages by booking
                    List<CommunicatorMsg> bookingMsgs = tripMsgs.bookingMsgs.get(msg1.getRoomId());
                    if (bookingMsgs == null) {
                        tripMsgs.sortedBookingIds.add(msg1.getRoomId());
                        bookingMsgs = new ArrayList<>();
                    }
                    msg1.setReplyEmail(getReplyToEmail(trip.getName(), trip, msg1.getRoomId(), up, tp));
                    bookingMsgs.add(msg1);
                    tripMsgs.bookingMsgs.put(msg1.getRoomId(), bookingMsgs);
                    view.allMessages.put(msg1.getTripId(), tripMsgs);
                }
            }
        }

        return view;
    }

    private String getReplyToEmail (String emailName, Trip trip, String roomId, UserProfile up, AccountTripLink tp) {
      try {
        String email = URLEncoder.encode(emailName.replace(",","") + " Reply <", "UTF-8").replace("+", "%20");
        email += CommunicatorToAllActor.getReplyToEmail(trip, roomId, up, tp) + ">";
        return email;
      } catch (Exception e) {

      }
      return null;

    }
}
