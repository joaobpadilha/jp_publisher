package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.rjeschke.txtmark.Configuration;
import com.github.rjeschke.txtmark.Processor;
import com.google.inject.Inject;
import com.mapped.common.CoreConstants;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.*;
import com.mapped.publisher.persistence.communicator.SentMessageNotification;
import com.mapped.publisher.persistence.redis.RedisAkkaProtocol;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.persistence.redis.RedisPubSubToAkka;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.UmappedEmail;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.email.CommunicatorMessageView;
import com.umapped.persistence.digest.CommunicatorMsg;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.flight.UmFlightReservation;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

import controllers.routes;
import models.publisher.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.EmailException;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by surge on 2016-02-02.
 */
public class CommunicatorToAllActor
    extends UntypedActor {

  public static final String   COMM2ALL_CHANNEL = "c2a";
  private static final boolean DEBUG            = false;

  public final static Props props = Props.create(CommunicatorToAllActor.class);

  @Inject
  RedisMgr redisMgr;
  RedisPubSubToAkka ps2akka;

  ObjectMapper objectMapper = new ObjectMapper();

  public interface Factory {
    Actor create();
  }

  public static class Protocol {
    public enum Command {
      SUBSCRIBE,
      UNSUBSCRIBE
    }

    public Command cmd;

    private Protocol() {
    }

    public static Protocol build(Command cmd) {
      Protocol p = new Protocol();
      p.cmd = cmd;
      return p;
    }
  }

  @Override
  public void onReceive(Object message)
      throws Exception {
    Log.info("2EmailActor: Got message in... checking");
    if(message instanceof Protocol) {
      Protocol p = (Protocol) message;

      switch (p.cmd) {
        case SUBSCRIBE:
          Log.info("ToEmaialActor: SUBSCRIBE");
          ps2akka = new RedisPubSubToAkka(SupervisorActor.UmappedActor.COMMUNICATOR_TO_ALL);
          redisMgr.subscribe(ps2akka, COMM2ALL_CHANNEL);
          break;
        case UNSUBSCRIBE:
          Log.info("ToEmailActor: UNSUBSCRIBE");
          if(ps2akka != null) {
            redisMgr.unsubscribe(ps2akka);
            ps2akka = null;
          }
          break;
      }
    } else if (message instanceof RedisAkkaProtocol) {
      RedisAkkaProtocol p = (RedisAkkaProtocol) message;
      processRedisMessage(p.content);
    } else {
      unhandled(message);
    }
  }

  /**
   * Based on the incoming parameters build communicator email address
   * @param trip  Trip which is being discussed
   * @param roomId Trip Room being discussed
   * @param up Agent (if message originates from the agent)
   * @return Email address that will allow identify topic of conversation
   */
  public static String getReplyToEmail(Trip trip, String roomId, UserProfile up, AccountTripLink tripLink) {
    StringBuilder sb = new StringBuilder();

    try {
      switch (roomId) {
        case "internal":
          sb.append("i.");
          long tripId = Long.parseLong(trip.getTripid());
          sb.append(Utils.shortenNumber(tripId));
          break;
        case "general":
          sb.append("g.");
          long gTripId = Long.parseLong(trip.getTripid());
          sb.append(Utils.shortenNumber(gTripId));
          break;
        default:
          sb.append("b.");
          long dtId = Long.parseLong(roomId);
          sb.append(Utils.shortenNumber(dtId));
          break;
      }

      if(up != null) {
        sb.append(".u.");
        sb.append(up.getUserid());
      }

      if(tripLink != null && tripLink.getLegacyId() != null) {
        sb.append(".t.");
        long trId = Long.parseLong(tripLink.getLegacyId());
        sb.append(Utils.shortenNumber(trId));
      }
      sb.append('@');
      sb.append(ConfigMgr.getAppParameter(CoreConstants.EMAIL_COMMUNICATOR_HOST));
    } catch (NumberFormatException e) {
      Log.err("ToEmailActor: attempting to have conversation in old trip (non-long id)");
      return null;
    }

    return sb.toString();
  }

  String preProcessOutgoingMessage(String msg) {
    /* Lightweight markdown pre-processor: https://github.com/rjeschke/txtmark */
    Configuration.Builder conf = Configuration.builder();//.convertNewline2Br();
    return Processor.process(msg, conf.build());
  }


  public static final String FROM_NAME = "Umapped Notifications";
  void processRedisMessage(String message) {
    try {
      SentMessageNotification msg = objectMapper.readValue(message, SentMessageNotification.class);
      Trip trip = Trip.find.byId(msg.confId);
      if(trip == null) {
        return;
      }

      String roomId;
      String webItineraryUrl;
      TripDetail td = TripDetail.findByPK(msg.roomId);
      UserProfile up1 = null;
      AccountTripLink tripLink = null;
      if(StringUtils.isNumeric(msg.from)) { //Traveller
        tripLink = AccountTripLink.findByLegacyId(msg.from);
      } else {
        up1 = UserProfile.findByPK(msg.from);
        if (up1 == null && msg.from.contains("_")) {
          up1 = UserProfile.findByPK(msg.from.replace('_','.'));
        }
      }
      //queue this message for the digest... we are not doing any validation here just a simple append operation
      //we let the digest actor make the decision
      queueMessageForDigest(msg, trip, td, tripLink, up1);

      //Not sending any kind of notifications if trip's user does not have this capability enabled
      //or trip object is null which can only mean someone is faking the request
      if(!SecurityMgr.hasCapability(trip, Capability.COMMUNICATOR_NOTIFICATION_EMAIL)) {
        return;
      }

      CommunicatorMessageView view = new CommunicatorMessageView();
      StringBuilder msgSB = new StringBuilder();
      if(td != null) { //Internal
        msgSB.append("Booking:  ").append(td.getName());
        roomId = td.getDetailsid();
      } else {
        if(msg.roomId.contains("internal")) {
          roomId = "internal";
        }else  if(msg.roomId.contains("general")) {
          roomId = "general";
        } else {
          return; //Corrupted data room can't be identified
        }
      }
      msgSB.append(preProcessOutgoingMessage(msg.message));
      view.message = msgSB.toString();
      view.tripName = trip.getName();
      Log.debug("processRedisMessage - PROCESSING redis msg for trip " + trip.getName() + " from " + msg.from + " about: " + view.message);


      if(tripLink != null) {
        webItineraryUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL) + routes.WebItineraryController
            .index(trip.getTripid(), null, tripLink.getPk().getUid())
            .url();
      } else {
        webItineraryUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL) +
                               routes.WebItineraryController.index(trip.getTripid(), null, 0L).url();
      }

      StringBuilder subject = new StringBuilder();
      subject.append("New Message about your trip - ").append(trip.getName());


      view.title = subject.toString();

      if(tripLink != null && tripLink.getLegacyId() != null) { //Traveller
        Account traveler = Account.find.byId(tripLink.getPk().getUid());
        view.fromEmail = traveler.getEmail();
        view.fromName = traveler.getFullName();
        if(view.fromName == null || view.fromName.length() == 0) {
          view.fromName = traveler.getEmail();
        }
      } else {                              //Agent
        view.fromEmail = up1.getEmail();
        view.fromName = up1.getFullName();
      }

      String replyTo = null;
      Set<String> online = new HashSet<>(msg.roomUsers);
      Set<String> notify = new HashSet<>(msg.roomNotify);
      notify.removeAll(online);

      if(DEBUG) {
        notify.clear();
        notify.add("sergei");
      }

      CommunicatorSMSActor.Protocol smsCmd =  CommunicatorSMSActor.Protocol.build(CommunicatorSMSActor
                                                                                      .Protocol
                                                                                      .Command.SEND);
      smsCmd.msg.fromName = view.fromName;
      smsCmd.msg.tripId = trip.getTripid();
      smsCmd.msg.detailsId = roomId;
      smsCmd.msg.message = msg.message;
      smsCmd.msg.fromUserId = (up1 != null) ? up1.getUserid() : null;
      smsCmd.msg.fromTravelerId = (tripLink != null) ? tripLink.getPk().getUid() : null;

      for(String id : notify) {
        Log.debug("processRedisMessage - PROCESSING redis msg for trip " + trip.getName() + " from " + msg.from + " Recipient: " + id);

        String toAddr;
        String toName;
        Long uid = null;
        if(StringUtils.isNumeric(id)) { //Traveller
          tripLink = AccountTripLink.findByLegacyId(id);
          if(tripLink == null) {
            continue;
          }
          smsCmd.msg.addTraveller(id);

          replyTo = getReplyToEmail(trip, roomId, null, tripLink);
          Account traveler = Account.find.byId(tripLink.getPk().getUid());
          toAddr = traveler.getEmail();
          toName = traveler.getFullName();
          if(toName != null && toName.length() == 0) {
            toName = null;
          }
          uid = tripLink.getPk().getUid();
        } else {                              //Agent
          Account a = Account.findByLegacyId(id);
          if (a == null && id.contains("_")) {
            a = Account.findByLegacyId(id.replace('_','.'));
          }
          if(a == null) {
            Log.debug("processRedisMessage - SKIPPING redis msg - no user found - for trip " + trip.getName() + " from " + msg.from + " Recipient: " + id);
            continue;
          }
          UserProfile up2 = UserProfile.findByPK(a.getLegacyId());
          uid = a.getUid();
          Credentials cred = SecurityMgr.getCredentials(a);

          if(cred.hasCapability(Capability.COMMUNICATOR_NOTIFICATION_SMS)) {
            smsCmd.msg.addUser(up2.getUserid());
          }

          replyTo = getReplyToEmail(trip, roomId, up2, null);
          toAddr = up2.getEmail();
          toName = up2.getFullName();

          if(!cred.hasCapability(Capability.COMMUNICATOR_NOTIFICATION_EMAIL)) {
            Log.debug("processRedisMessage - SKIPPING redis msg for trip " + trip.getName() + " from " + msg.from + " Recipient: " + id);
            continue; //Not sending anything to the user who disable notifications
          }
        }

        view.id = replyTo.split("@")[0]; //Email username is ID for now since email is not permanent
        view.time = Utils.formatTime12(System.currentTimeMillis()) + " UTC";
        view.toEmail = toAddr;
        view.webItineraryUrl = webItineraryUrl + "?tid=" + id;

        List<String> emails = new ArrayList<>();
        emails.add(toAddr);

        UmappedEmail email     = UmappedEmail.buildDefault();
        email.withHtml(views.html.email.communicatorMessage.render(view).toString())
             .withSubject(subject.toString())
             .withTripId(trip.tripid)
             .withAccountUid(uid)
             .withEmailType(EmailLog.EmailTypes.MESSENGER)
             .withToList(emails)
             .setFrom(replyTo, FROM_NAME)
             .addReplyTo(replyTo);

        if(DEBUG) {
          email.addTo("msergei@gmail.com", "Sergei");
        } else {
          email.addTo(toAddr, toName);
        }

        email.buildAndSend();
        Log.debug("Processing Redis Sending email to: " + email.toString());

        //now let's try to send a mobile notification if applicable
        StringBuilder mobileMsg = new StringBuilder();
        mobileMsg.append("From: ");
        mobileMsg.append(view.fromName);
        mobileMsg.append("\n");
        mobileMsg.append(msg.message);

        final MobilePushNotificationActor.Command cmd2 = new MobilePushNotificationActor.Command(toAddr,
                                                                                                 trip.getName(),
                                                                                                 mobileMsg.toString(),
                                                                                                 DBConnectionMgr.getUniqueLongId());
        ActorsHelper.tell(SupervisorActor.UmappedActor.MOBILE_PUSH_NOTIFY, cmd2);
      }

      ActorsHelper.tell(SupervisorActor.UmappedActor.COMMUNICATOR_SMS, smsCmd);

    } catch (IOException e) {
      Log.err("CommunicatorToEmail Actor failed to unmarshall Json message: " + message, e);
    }
    catch (EmailException ee) {
      Log.err("ToEmailActor: failed to send Communicator message notification", ee);
    }
  }

  //create a communicator msg and store in redis
  private void queueMessageForDigest (SentMessageNotification msg, Trip trip, TripDetail td, AccountTripLink tripLink, UserProfile up) {
    if (msg.message != null && !msg.message.isEmpty()) {
      Log.debug("Process new communicator msg for digest - conf: " + msg.confId);

      CommunicatorMsg commMsg = new CommunicatorMsg();
      commMsg.setTripId(trip.tripid);
      commMsg.setTripName(trip.getName());
      commMsg.setMsgBody(msg.message);
      if (td != null) { //Internal
        commMsg.setRoomId(td.getDetailsid());
        commMsg.setRoomName(td.getName());
        if (td.getDetailtypeid() == ReservationType.FLIGHT) {
          UmFlightReservation reservation = cast(td.getReservation(), UmFlightReservation.class);
          
          if (reservation != null && td.getName() != reservation.getFlight().flightNumber) {
            commMsg.setRoomName(td.getName() + " " + reservation.getFlight().flightNumber);
          }
        }
      }
      else {
        if (msg.roomId.contains("internal")) {
          commMsg.setRoomId("internal");
          commMsg.setRoomName("Internal Trip Chat");
        }
        else if (msg.roomId.contains("general")) {
          commMsg.setRoomId("general");
          commMsg.setRoomName("Trip Chat");
        }
        else {
          Log.debug("Failed Process new communicator msg for digest - conf: " + msg.confId + "  room: " +  msg.roomId);
          return; //Corrupted data room can't be identified
        }
      }

      if (tripLink != null && tripLink.getLegacyId() != null) { //Traveller
        Account traveler = Account.find.byId(tripLink.getPk().getUid());
        commMsg.setSenderId(Long.parseLong(msg.from));
        commMsg.setSenderName(traveler.getFullName());

      }
      else {                              //Agent
        Account account = Account.findActiveByLegacyId(msg.from);
        if (account == null && msg.from.contains("_")) {
          account = Account.findActiveByLegacyId(msg.from.replace('_', '.'));
        }
        if (account == null) {
          return;
        }
        commMsg.setSenderLegacyId(account.getLegacyId());
        commMsg.setSenderInitial(account.getInitials());
        commMsg.setSenderName(account.getFullName());
      }
      commMsg.setMsgTimestamp(Timestamp.from(Instant.now()));
      Set<String> notify = new HashSet<>(msg.roomNotify);
      for (String id : notify) {
        if (!msg.from.equals(id)) { //only dealing with agent for now
          redisMgr.addToSet(APPConstants.CACHE_DIGEST_ACCOUNT_LIST_KEY, id);
          redisMgr.appendToList(APPConstants.CACHE_DIGEST_MSGS_BY_ACCOUNT_PREFIX + id, commMsg);
          Log.debug("Queue communicator msg for digest - " + id);
        }
      }
    } else {
      Log.debug("NOT Queued communicator msg for digest - from: " + msg.from + " TripdId: " + trip.tripid + " Room:" + msg.roomId );
    }
  }
}
