package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.parse.iceportal.*;
import com.mapped.publisher.persistence.*;
import com.mapped.publisher.persistence.cache.PoiFeedStateCO;
import com.mapped.publisher.utils.Log;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.time.StopWatch;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.Holder;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.PortInfo;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.*;

/**
 * Created by surge on 2015-02-20.
 * Designing actor using Best Practices from:
 * http://doc.akka.io/docs/akka/snapshot/java/untyped-actors.html
 * and
 * http://www.informit.com/articles/article.aspx?p=2228804
 */
public class IcePortalActor
    extends UntypedActor {
  public interface Factory {
    public Actor create();
  }


  /**
   * Akka Actor Properties
   */
  public final static Props props = Props.create(IcePortalActor.class);

  private static final String ICE_PORTAL_USERNAME = "thierry@umapped.com";
  private static final String ICE_PORTAL_PASSWORD = "Thierry51";

  public static enum IceCommand
      implements Serializable {
    GET_ALL,
    GET_UPDATED,
    STOP
  }

  /**
   * Creating properly wrapped SOAP message
   * <p/>
   * With great thanks to post at SO:
   * http://examples.javacodegeeks.com/enterprise-java/jws/jax-ws-soap-handler-example/
   * and
   * http://stackoverflow.com/questions/10654608/add-soap-header-object-using-pure-jax-ws
   * and
   * http://stackoverflow.com/questions/830691/how-do-i-add-a-soap-header-using-java-jax-ws
   *
   * @return
   */
  public static ICEWebServiceSoap getIcePortalService(final ByteArrayOutputStream rawResponseOut) {

    ICEWebService iceWebService = new ICEWebService();

    ICEAuthHeader iceAuthHeader = new ICEAuthHeader();
    iceAuthHeader.setUsername(ICE_PORTAL_USERNAME);
    iceAuthHeader.setPassword(ICE_PORTAL_PASSWORD);

    final ObjectFactory objectFactory = new ObjectFactory();
    // creating JAXBElement from iceAuthHeader
    final JAXBElement<ICEAuthHeader> requestAuth = objectFactory.createICEAuthHeader(iceAuthHeader);

    final List<Handler> handlersList = new ArrayList<>();

    handlersList.add(new SOAPHandler<SOAPMessageContext>() {
      @Override
      public Set<QName> getHeaders() {
        return new TreeSet();
      }

      @Override
      public boolean handleMessage(final SOAPMessageContext context) {
        try {
          // checking whether handled message is outbound one
          Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
          if (outboundProperty != null && outboundProperty.booleanValue()) {
            // obtaining marshaller which should marshal instance to xml
            final Marshaller marshaller = JAXBContext.newInstance(ICEAuthHeader.class).createMarshaller();
            // adding header because otherwise it's null
            final SOAPHeader soapHeader = context.getMessage().getSOAPPart().getEnvelope().addHeader();
            // marshalling instance (appending) to SOAP header's xml node
            marshaller.marshal(requestAuth, soapHeader);
          }
          else {
            SOAPMessage soapMessage = context.getMessage();
            soapMessage.writeTo(rawResponseOut);
          }

        }
        catch (final Exception e) {
          throw new RuntimeException(e);
        }
        return true;
      }

      @Override
      public boolean handleFault(SOAPMessageContext context) {
        throw new UnsupportedOperationException("Can't handle non conformant SOAP Message. Dying...");
      }

      @Override
      public void close(MessageContext context) {
      }
    });

    iceWebService.setHandlerResolver(new HandlerResolver() {
      @Override
      public List<Handler> getHandlerChain(PortInfo portInfo) {
        return handlersList;
      }
    });
    return iceWebService.getICEWebServiceSoap12();
  }

  /**
   * To be implemented by concrete UntypedActor, this defines the behavior of the
   * UntypedActor.
   *
   * @param message
   */
  @Override public void onReceive(Object message)
      throws Exception {

    if (message instanceof IceCommand) {
      PoiFeedStateCO feedState = (PoiFeedStateCO) CacheMgr.get(APPConstants.CACHE_POI_FEED_STATE +
                                                               FeedSourcesInfo.byName("Ice Portal"));
      if (feedState == null) {
        feedState = new PoiFeedStateCO();
      }

      if (feedState.currState != PoiFeedStateCO.State.STOPPED) {
        //TODO: Serguei: Communicate back saying that we can't run
        return;
      }

      switch ((IceCommand)message) {
        case GET_ALL:
          getAllIceBrochures(feedState);
          break;
        case GET_UPDATED:
          getUpdatedBrochures(feedState);
          break;
      }
    }
    else {
      unhandled(message);
    }
  }

  private static int MAX_ATTEMPTS = 5;
  private void getAllIceBrochures(PoiFeedStateCO feedState) {
    try {
      StopWatch sw = new StopWatch();

      int srcId = FeedSourcesInfo.byName("Ice Portal");

      feedState.start(PoiFeedStateCO.State.INIT, 1);
      updateCache(feedState);

      ByteArrayOutputStream lastResponseStream = new ByteArrayOutputStream();
      ICEWebServiceSoap iceSoapService = getIcePortalService(lastResponseStream);

      Holder<Integer> resultCode = new Holder<>();
      Holder<String> errorMessage = new Holder<>();
      Holder<ArrayOfString> iDs = new Holder<>();
      sw.start();
      iceSoapService.getAllBrochureIDs(resultCode, errorMessage, iDs);
      lastResponseStream.reset();
      sw.stop();

      System.out.println("Received " + iDs.value.getString().size() + " IDS to retrieve in " + sw.getTime() + " ms");

      feedState.start(PoiFeedStateCO.State.IMPORT, iDs.value.getString().size());
      updateCache(feedState);

      loadBrochures(iceSoapService, iDs.value.getString(), feedState, lastResponseStream, true);

      PoiImportMgr.deleteDuplicates(srcId);

    } catch (Exception e) {
      Log.err("Error while loading Ice Portal feed feed: " + e.getMessage());
      e.printStackTrace();
    }

    feedState.stop();
    updateCache(feedState);
  }

  private void getUpdatedBrochures(PoiFeedStateCO feedState) {
    try {
      StopWatch sw = new StopWatch();

      feedState.start(PoiFeedStateCO.State.INIT, 1);
      updateCache(feedState);

      int srcId = FeedSourcesInfo.byName("Ice Portal");

      //Getting last timestamp
      long lastImportTs = PoiImportMgr.getLastImportTs(srcId);
      //Let's unroll 24 hours just to be sure we don't miss anything
      lastImportTs -= 24 * 3600 * 1000;
      GregorianCalendar gc = new GregorianCalendar();
      gc.setTimeInMillis(lastImportTs);
      // to XML Gregorian Calendar
      XMLGregorianCalendar xmlTs = null;
      try {
        xmlTs = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
      }
      catch (DatatypeConfigurationException e) {
        e.printStackTrace();
        return;
      }

      ByteArrayOutputStream lastResponseStream = new ByteArrayOutputStream();
      ICEWebServiceSoap iceSoapService = getIcePortalService(lastResponseStream);

      Holder<Integer> resultCode = new Holder<>();
      Holder<String> errorMessage = new Holder<>();
      Holder<ArrayOfBrochureInfos> newIds = new Holder<>();
      Holder<ArrayOfUpdatedBrochureIDInfo> updatedIds = new Holder<>();

      sw.start();

      iceSoapService.getNewBrochureIDsSince(xmlTs, resultCode, errorMessage, newIds);
      lastResponseStream.reset();
      iceSoapService.getUpdatedBrochureIDsSince(xmlTs, resultCode, errorMessage, updatedIds);
      lastResponseStream.reset();

      sw.stop();

      int totalIds = newIds.value.getBrochureInfos().size() + updatedIds.value.getUpdatedBrochureIDInfo().size();
      System.out.println("Received " + totalIds + " new and updated Ice Portal IDS to retrieve in " + sw.getTime() + " ms");


      feedState.start(PoiFeedStateCO.State.IMPORT, totalIds);
      updateCache(feedState);

      List<String> extractedIds = new ArrayList<>(newIds.value.getBrochureInfos().size());
      for (BrochureInfos bi : newIds.value.getBrochureInfos()) {
        extractedIds.add(bi.getBrochureID());
      }
      loadBrochures(iceSoapService, extractedIds, feedState, lastResponseStream, false);

      extractedIds = new ArrayList<>(updatedIds.value.getUpdatedBrochureIDInfo().size());
      for (UpdatedBrochureIDInfo bi : updatedIds.value.getUpdatedBrochureIDInfo()) {
        extractedIds.add(bi.getBrochureID());
      }
      loadBrochures(iceSoapService, extractedIds, feedState, lastResponseStream, false);

      PoiImportMgr.deleteDuplicates(srcId);
    } catch (Exception e) {
      Log.err("Error while updating Ice Portal feed feed: " + e.getMessage());
      e.printStackTrace();
    }

    feedState.stop();
    updateCache(feedState);
  }

  /**
   *
   * @param iDs list of IDs to retrieve
   * @param feedState state to update
   * @param isNew true if importing new records, false if updating old ones
   */
  private void loadBrochures(ICEWebServiceSoap iceSoapService, List<String> iDs,
                             PoiFeedStateCO feedState, ByteArrayOutputStream lastResponseStream, boolean isNew) {
    StopWatch sw = new StopWatch();
    int srcId = FeedSourcesInfo.byName("Ice Portal");
    Holder<Integer> resultCode = new Holder<>();
    Holder<String> errorMessage = new Holder<>();

    int totalAttempts = 0;
    while (totalAttempts++ < MAX_ATTEMPTS) { //Will be trying to get these until tired
      Log.debug("POI FEED LOAD ATTEMPT :" + totalAttempts);
      for (Iterator<String> it = iDs.iterator(); it.hasNext(); ) {
        String id = it.next();
        sw.reset();
        sw.start();
        //Updating
        if (++feedState.currItem % 50 == 0) {
          feedState.update();
          updateCache(feedState);
        }

        if (isNew && PoiImportMgr.isImported(srcId, id.substring(3)) > 0) {
          it.remove(); //We already imported this record, skipping
          continue;
        }

        Holder<Brochure> brochure = new Holder<>();
        try {
          lastResponseStream.reset();
          iceSoapService.getBrochure(id, resultCode, errorMessage, brochure);
          if (resultCode.value < 0) {
            it.remove(); //Negative code means restricted or otherwise locked brochure
            continue;
          }
        }
        catch (Exception e) {
          Log.err("Error while requesting Ice Portal ID:" + id);
          e.printStackTrace();
          continue;
        }

        try {
          if (brochure.value == null) {
            Log.err("Problem retrieving " + id + " null brochure...");
            continue; //Something is wrong, we will try again later
          }
          BrochureInfo brochureInfo = brochure.value.getBrochureInfo();
          PoiImportRS pirs = new PoiImportRS();
          pirs.state = PoiImportRS.ImportState.UNPROCESSED;
          pirs.srcId = srcId;
          pirs.poiId = null;
          pirs.typeId = PoiTypeInfo.Instance().byName("Accommodations").getId();
          pirs.timestamp = System.currentTimeMillis();
          pirs.srcReference = Integer.toString(brochureInfo.getIceID());
          pirs.srcFilename = "";
          pirs.name = brochureInfo.getHotelName();
          pirs.countryName = brochureInfo.getCountry();
          CountriesInfo.Country c = CountriesInfo.Instance().searchByName(brochureInfo.getCountry());
          if (c == null) {
            Log.err("Failed to find matching country: " + brochureInfo.getCountry() + " !!!");
            Log.err(brochureInfo.toString());
            pirs.countryCode = "UNK";
          }
          else {
            pirs.countryCode = c.getAlpha3();
          }
          pirs.phone = brochureInfo.getPhoneNumber();
          pirs.region = brochureInfo.getState();
          pirs.postalCode = brochureInfo.getZip();
          pirs.city = brochureInfo.getCity();
          pirs.address = brochureInfo.getAddress1() + " " + brochureInfo.getAddress2() + " " + brochureInfo.getAddress3();
          pirs.address = pirs.address.trim();
          pirs.raw = lastResponseStream.toString("UTF-8");
          pirs.hash = DigestUtils.md5Hex(pirs.raw);
          PoiImportMgr.save(pirs);
          sw.stop();
          System.out.println("Processed brochure " + id + " in " + sw.getTime() + " ms - Hotel: " + brochure.value.getBrochureInfo()
                                                                                                                  .getHotelName());
        }
        catch (Exception e) {
          Log.err("Error while processing Ice Portal ID:" + id);
          e.printStackTrace();
          continue;
        }
        it.remove(); //Removing from the list if all good
      }
    }
  }

  private void updateCache(PoiFeedStateCO state) {
    CacheMgr.set(APPConstants.CACHE_POI_FEED_STATE  +
                 FeedSourcesInfo.byName("Ice Portal"), state);
  }
}
