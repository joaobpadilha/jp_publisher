package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.persistence.communicator.MessengerUpdateHelper;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import models.publisher.Trip;
import models.publisher.TripDetail;
import org.apache.commons.lang3.time.StopWatch;

import java.io.Serializable;
import java.util.List;

/**
 * Created by twong on 16-01-26.
 */
public class PublishFirebaseActor
    extends UntypedActor {

  public final static Props props = Props.create(PublishFirebaseActor.class);

  public static class Command
      implements Serializable {
    private final Trip             trip;
    private final List<TripDetail> bookings;
    private final String           cmdId;
    private final Long             accountId;

    public Command(Trip trip, List<TripDetail> bookings, String cmdId, Long accountId) {
      this.trip = trip;
      this.bookings = bookings;
      this.cmdId = cmdId;
      this.accountId = accountId;
    }

    public Long getAccountId() {
      return accountId;
    }

    public Trip getTrip() {
      return trip;
    }

    public List<TripDetail> getBookings() {
      return bookings;
    }

    public String getCmdId() {
      return cmdId;
    }
  }

  @Override
  public void onReceive(Object message)
      throws Exception {

    if (message instanceof Command) {
      Command cmd = (Command) message;

      Trip             trip      = cmd.getTrip();
      List<TripDetail> bookings  = cmd.getBookings();
      String           msgId     = cmd.getCmdId();
      String           className = "PublishFirebaseActor - Actor Id: " + msgId;

      StopWatch sw = new StopWatch();
      sw.start();

      try {
        //Create Firebase chat room:
        Log.log(LogLevel.INFO, className + " Start Processing: " + trip.tripid);
        MessengerUpdateHelper.build(trip, cmd.getAccountId())
                             .addRoomsToUpdate(bookings)
                             .setAddDefaultRooms(true)
                             .update();
      }
      catch (Exception e) {
        Log.err(className + "Trip : " + trip.tripid, e);
      }
      finally {
        CacheMgr.set(APPConstants.CACHE_PUB_ACTOR + msgId, null);
      }

      sw.stop();
      Log.info(className + " Time for: " + trip.tripid + " Generate: " + sw.getTime());
    }
  }

  public interface Factory {
    public Actor create();
  }
}
