package actors;

import org.apache.commons.lang.StringUtils;

import com.umapped.persistence.reservation.flight.UmFlightReservation;

import models.publisher.FlightAlert;
import models.publisher.FlightAlertEvent;

public class FlightAlertsHelper {
  public static String generateComment(UmFlightReservation reservation, FlightAlertEvent alertEvent, FlightAlert alert) {
    StringBuilder newComments = new StringBuilder();
    
    if (StringUtils.isNotEmpty(reservation.getNotesPlainText())) {
      String s = reservation.getNotesPlainText();
      String[] lines = s.split("\n");
      for (String line : lines) {
        if (!line.contains("Gate") && !line.contains("Terminal") && !line.contains("Flight Status -")) {
          newComments.append(line);
          newComments.append("\n");
        }
      }
    }
    newComments.insert(0, "\n");
    if (alert.newArriveGate != null && !alert.newArriveGate.isEmpty()) {
      newComments.insert(0, "Arrival Gate: " + alert.newArriveGate + "\n");
    }
    if (alert.newArriveTerminal != null && !alert.newArriveTerminal.isEmpty()) {
      if (alert.newArriveTerminal.toLowerCase().contains("erminal")) {
        newComments.insert(0, "Arrival Terminal: " + alert.newArriveTerminal + "\n");
      } else {
        newComments.insert(0, "Arrival Terminal: Terminal " + alert.newArriveTerminal + "\n");
      }
    }
    if (alert.newDepartGate != null && !alert.newDepartGate.isEmpty()) {
      newComments.insert(0, "Departure Gate: " + alert.newDepartGate + "\n");
    }
    if (alert.newDepartTerminal != null && !alert.newDepartTerminal.isEmpty()) {
      if (alert.newDepartTerminal.toLowerCase().contains("erminal")) {
        newComments.insert(0, "Departure Terminal: " + alert.newDepartTerminal + "\n");
      } else {
        newComments.insert(0, "Departure Terminal: Terminal " + alert.newDepartTerminal + "\n");
      }
    }

    if (alertEvent != null ) {
      if (alertEvent.alertType == null) {
        newComments.insert(0,"Flight Status - Updated\n");
      } else {
        switch (alertEvent.alertType) {
          case CANCELLED:
            newComments.insert(0,"Flight Status - Cancelled\n");
            break;
          case DEPARTURE_DELAY:
            newComments.insert(0,"Flight Status - Departure Delay\n");
            break;
          case ARRIVAL_DELAY:
            newComments.insert(0,"Flight Status - Arrival Delay\n");
            break;
          case DIVERTED:
            newComments.insert(0,"Flight Status - Diverted\n");
            break;
          case GATE_ADJUSTMENT:
            newComments.insert(0,"Flight Status - Gate Update\n");
            break;
          case TIME_ADJUSTMENT:
            newComments.insert(0,"Flight Status - Schedule Update\n");
            break;
          case PRE_DEPARTURE_STATUS:
            newComments.insert(0,"Flight Status - Updated\n");
            break;

        }
      }
    }
    return newComments.toString();
  }
}
