package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.form.RecordLocatorForm;
import com.mapped.publisher.parse.extractor.booking.email.*;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.persistence.VOModeller;
import com.mapped.publisher.view.BaseView;
import models.publisher.Trip;
import models.publisher.UserProfile;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * Created by twong on 15-08-06.
 */
public class RecLocatorActor
    extends UntypedActor {

  public interface Factory {
    public Actor create();
  }

  /**
   * Akka Actor Properties
   */
  public final static Props props = Props.create(RecLocatorActor.class);

  public static class Command {
    private final RecordLocatorForm form;

    public Command(RecordLocatorForm form) {
      this.form = form;
    }

    public RecordLocatorForm getForm() {
      return form;
    }
  }

  @Override
  public void onReceive(Object message)
      throws Exception {
    if (message instanceof Command) {

      RecordLocatorForm formInfo = ((Command) message).getForm();
      Trip trip = Trip.findByPK(formInfo.getInTripId());
      String userId = formInfo.getUserId();

      TripVO tripVO = null;
      if (formInfo.getInProvider() == RecordLocatorForm.Provider.SABRE) {
        StringBuilder url = new StringBuilder();
        url.append("https://services.tripcase.com/new/itineraryChron.html?lastName=");
        url.append(formInfo.getInLastName().toUpperCase().trim());
        url.append("&segment=print&action=printPreview&pnr=");
        url.append(formInfo.getInRecordLocator().toUpperCase().trim());
        try {
          Document document = Jsoup.connect(url.toString()).timeout(10 * 1000).followRedirects(true).get();
          if (document != null && document.hasText() && document.title().contains("sorry")) {
            BaseView baseView = new BaseView();
            baseView.message = "Invalid Record Locator/Name combination - please try again";
            CacheMgr.set(APPConstants.CACHE_REC_LOCATOR_WEB + formInfo.getInRecordLocator() + userId,
                         baseView);

            return;
          }
          else if (document != null && document.hasText() && document.title().contains("Print Your Itinerary")) {
            tripVO = TripCaseExtractor.getInfo(document, formInfo.getInRecordLocator().toUpperCase());
          }
        }
        catch (org.jsoup.HttpStatusException hs) {
          if (hs.getStatusCode() >= 500 && hs.getStatusCode() >= 500) {
            BaseView baseView = new BaseView();
            baseView.message = "Invalid Record Locator/Name combination - please try again";
            CacheMgr.set(APPConstants.CACHE_REC_LOCATOR_WEB + formInfo.getInRecordLocator() + userId,
                         baseView);

            return;
          }

        }
        catch (Exception e) {
          e.printStackTrace();
        }
      }
      else if (formInfo.getInProvider() == RecordLocatorForm.Provider.TRAVELPORT || formInfo.getInProvider() == RecordLocatorForm.Provider.WORLDSPAN) {
        try {
          String document = TravelportExtractorV2.getItinerary(formInfo.getInLastName()
                                                                       .toUpperCase()
                                                                       .trim()
                                                                       .replaceAll(" ", "."),
                                                               formInfo.getInRecordLocator().toUpperCase().trim());
          if (document != null && document.trim().length() > 0) {
            tripVO = TravelportExtractorV2.parseItinerary(document, formInfo.getInRecordLocator().toUpperCase().trim());

          }
        }
        catch (Exception e) {
          e.printStackTrace();
        }
      }
      /*else if (formInfo.getInProvider() == RecordLocatorForm.Provider.WORLDSPAN) {
        try {
          tripVO = WorldspanRecLocatorExtractor.getItinerary(formInfo.getInLastName()
                                                                     .toUpperCase()
                                                                     .trim()
                                                                     .replaceAll(" ", "."),
                                                             formInfo.getInRecordLocator().toUpperCase().trim());

        }
        catch (Exception e) {
          e.printStackTrace();
        }
      }*/
      else if (formInfo.getInProvider() == RecordLocatorForm.Provider.AMADEUS) {
        try {
          //tripVO = CheckMyTripExtractor.getItinerary(formInfo.getInLastName().toUpperCase().trim(),
          //                                           formInfo.getInRecordLocator().toUpperCase().trim());
          tripVO = CheckMyTripExtractorV2.getItinerary(formInfo.getInRecordLocator().toUpperCase().trim(),
                  formInfo.getInFirstName().toUpperCase().trim(),
                  formInfo.getInLastName().toUpperCase().trim());
        }
        catch (Exception e) {
          e.printStackTrace();
        }
      }

      try {

        if (tripVO != null) {
          UserProfile userProfile = UserProfile.findByPK(userId);
          VOModeller voModeller = new VOModeller(trip, userProfile, false);
          voModeller.buildTripVOModels(tripVO);
          if (voModeller.getBookingsCount() > 0) {
            if (tripVO.getImportSrc() == null) {
              tripVO.setImportSrc(BookingSrc.ImportSrc.REC_LOCATOR);
              tripVO.setImportTs(System.currentTimeMillis());
            }
            if (tripVO.getImportSrcId() == null) {
              tripVO.setImportSrcId(formInfo.getInRecordLocator().toUpperCase().trim() + " / " + formInfo.getInLastName()
                                                                                                         .toUpperCase()
                                                                                                         .trim());
            }
            voModeller.saveAllModels();
            BaseView baseView = new BaseView();
            baseView.message = voModeller.getBookingsCount() + " bookings imported";
            CacheMgr.set(APPConstants.CACHE_REC_LOCATOR_WEB + formInfo.getInRecordLocator() + userId,
                         baseView);

            //flash(SessionConstants.SESSION_PARAM_MSG, voModeller.getBookingsCount() + " bookings imported");
            //return redirect(routes.BookingController.bookings(trip.tripid, new TabType.Bound(TabType.ITINERARY), null));
            return;
          }
        }

      }
      catch (Exception e) {
        e.printStackTrace();
      }

      BaseView baseView = new BaseView();
      baseView.message = "Invalid Record Locator/Name combination - please try again";
      CacheMgr.set(APPConstants.CACHE_REC_LOCATOR_WEB + formInfo.getInRecordLocator() + userId, baseView);
    }
  }
}
