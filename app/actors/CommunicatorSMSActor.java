package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.CharMatcher;
import com.mapped.common.CoreConstants;
import com.mapped.publisher.common.Capability;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.common.Credentials;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.persistence.clickatell.*;
import com.mapped.publisher.utils.Log;
import models.publisher.Account;
import models.publisher.AccountTripLink;
import models.publisher.SmsLog;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.net.URLCodec;
import play.libs.ws.WSClient;
import play.libs.ws.WSRequest;

import javax.inject.Inject;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static models.publisher.Account.AID_PUBLISHER;

/**
 * Created by surge on 2016-02-08.
 */
public class CommunicatorSMSActor
    extends UntypedActor {

  public final static Props props = Props.create(CommunicatorSMSActor.class);
  @Inject
  WSClient ws;

  /**
   * TODO: Protocol needs cleaning to avoid legacy ids being passed all around
   */
  public static class Protocol {
    public enum Command {
      /**
       * Process SMS sending request
       */
      SEND,
      /**
       * Process SMS status from carrier
       */
      STATUS,
      /**
       * Process reply from the SMS Carrier
       */
      REPLY
    }

    public Command    cmd;
    public Message    msg;
    public StatusData status;
    public MOCallbackData reply;

    public static class Message {
      public String       fromName;
      public String       fromUserId;
      public Long         fromTravelerId;
      public String       message;
      public List<String> travellers;
      public List<String> users;
      public String       tripId;
      public String       detailsId;

      public Message() {
        travellers = new ArrayList<>();
        users = new ArrayList<>();
      }

      public void addTraveller(String traveller) {
        travellers.add(traveller);
      }

      public void addUser(String userId) {
        users.add(userId);
      }
    }

    private Protocol() {
    }

    public static Protocol build(Command cmd) {
      Protocol p = new Protocol();
      p.cmd = cmd;

      switch (cmd) {
        case SEND:
          p.msg = new Message();
          break;
      }
      return p;
    }
  }

  @Override
  public void onReceive(Object message)
      throws Exception {

    if (message instanceof Protocol) {
      Protocol p = (Protocol) message;

      switch (p.cmd) {
        case SEND:
          sendSMSCommand(p.msg);
          break;
        case STATUS:
          processStatus(p.status);
          break;
        case REPLY:
          processReply(p.reply);
          break;
      }
    }
    else {
      unhandled(message);
    }
  }

  /**
   * @param msg
   */
  private void sendSMSCommand(Protocol.Message msg) {
    Log.debug("Got message to send via SMS");

    Account originAccount = null;

    if(msg.fromTravelerId != null) {
      originAccount = Account.find.byId(msg.fromTravelerId);
    } else if (msg.fromUserId != null) {
      originAccount = Account.findByLegacyId(msg.fromUserId);
    }

    for (String tid : msg.travellers) {
      AccountTripLink tripLink = AccountTripLink.findByLegacyId(tid);
      if (tripLink != null) {
        Account traveler = Account.find.byId(tripLink.getPk().getUid());
        if (traveler != null && traveler.getMsisdn() != null && traveler.getMsisdn().trim().length() > 0) {
          SmsLog smsLog = SmsLog.build(originAccount.getUid());
          smsLog.setContents(msg.message);
          smsLog.setDestUid(traveler.getUid());
          smsLog.setDestMsisdnAndClean(traveler.getMsisdn());
          smsLog.setFromUid(originAccount.getUid());
          smsLog.setTripId(msg.tripId);
          smsLog.setTripDetailId(msg.detailsId);
          smsLog.setOutgoing(true);
          smsLog.save();
          submitSMS(smsLog);
        }
      }

    }

    for (String uid : msg.users) {
      Account a = Account.findActiveByLegacyId(uid);
      if (a != null && a.getMsisdn() != null && a.getMsisdn().trim().length() > 0) {
        Credentials cred = SecurityMgr.getCredentials(a);
        if(!cred.hasCapability(Capability.COMMUNICATOR_NOTIFICATION_SMS)) {
          continue; //User has no SMS capability, continuing
        }
        SmsLog smsLog = SmsLog.build(originAccount.getUid());
        smsLog.setContents(msg.message);
        smsLog.setDestUid(a.getUid());
        smsLog.setDestMsisdnAndClean(a.getMsisdn());
        smsLog.setFromUid(originAccount.getUid());
        smsLog.setTripId(msg.tripId);
        smsLog.setTripDetailId(msg.detailsId);
        smsLog.setOutgoing(true);
        smsLog.save();
        submitSMS(smsLog);
      }
    }
  }

  void processStatus(StatusData status) {
    SmsLog smsLog = SmsLog.findByMessageId(status.apiMessageId);
    if (smsLog == null) {
      Log.err("SMSActor: can't find message with id" + status.apiMessageId);
      return;
    }

    smsLog.setApiId(status.apiId.toString());
    smsLog.setCliMessageId(status.clientMessageId);
    smsLog.setStatusTs(status.timestamp * 1000l); //Converted into milliseconds
    smsLog.setCost(status.charge);
    StatusCode sc = StatusCode.fromCode(status.messageStatus);
    if (sc == StatusCode.UNDEFINED) {
      smsLog.setStatus(status.messageStatus); //Let's just write what we got
    }
    else {
      smsLog.setStatus(sc.name());
    }
    smsLog.markModified(AID_PUBLISHER);
    smsLog.update();
  }

  private void processReply(MOCallbackData reply) {
    SmsLog lastLog = SmsLog.findLastTo(reply.from);
    if(lastLog == null) {
      Log.err("SMSActor: Received message from unknown source: " + reply.toString());
      return;
    }

    String message = "**Umapped Received SMS message that can't be decoded**";
    try {
      message =  new URLCodec().decode(reply.text, reply.charset);
    } catch (DecoderException|UnsupportedEncodingException e) {
      Log.err("SMSActor: Can't decode a message from Clickatell" + reply.toString());
    }

    SmsLog replyLog = SmsLog.buildReply(lastLog, Account.AID_PUBLISHER);
    replyLog.setApiId(reply.apiId.toString());
    replyLog.setCliMessageId(reply.moMessageId);
    replyLog.setFromMsisdn(reply.from);
    replyLog.setDestMsisdn(reply.to);
    replyLog.setContents(message);
    replyLog.save();

    CommunicatorReplyActor.Protocol cmd = CommunicatorReplyActor.Protocol.build(CommunicatorReplyActor.Protocol
                                                                                    .Command.REPLY);
    cmd.reply.tripId = replyLog.getTripId();
    cmd.reply.detailsId = replyLog.getTripDetailId();

    Account originAccount = Account.find.byId(replyLog.getFromUid());

    if(originAccount.getAccountType() == Account.AccountType.PUBLISHER) {
      cmd.reply.userId = originAccount.getLegacyId();
    } else {
      AccountTripLink atl = AccountTripLink.findByUid(originAccount.getUid(), replyLog.getTripId());
      cmd.reply.travelerId = atl.getLegacyId();
    }
    cmd.reply.fromEmail = originAccount.getEmail();

    cmd.reply.message = replyLog.getContents();
    cmd.reply.isHtml = false;

    ActorsHelper.tell(SupervisorActor.UmappedActor.COMMUNICATOR_REPLY, cmd);
  }

  private boolean submitSMS(SmsLog smsLog) {
    try {
      String             smsToken = ConfigMgr.getAppParameter(CoreConstants.SMS_CLICKATELL_TOKEN);
      String             smsApiID = ConfigMgr.getAppParameter(CoreConstants.SMS_CLICKATELL_APIID);
      String             smsMSISDN = ConfigMgr.getAppParameter(CoreConstants.SMS_CLICKATELL_MSISDN);
      final ObjectMapper om       = new ObjectMapper();

      if (smsToken == null) {
        Log.info("SMSActor: Ignoring SMS SEND. No Auth Token set.");
        return false;
      }
      boolean isAscii =  CharMatcher.ASCII.matchesAllOf(smsLog.getContents());

      Message msg = Message.build();
      msg.addRecepient(smsLog.getDestMsisdn());
      msg.text = isAscii?smsLog.getContents():(Hex.encodeHexString(smsLog.getContents().getBytes("UTF-16BE")));
      msg.clientMessageId = smsLog.getPk().toString();
      msg.from = smsMSISDN;
      msg.mo = 1;
      msg.unicode = isAscii?0:1;
      /*
      https://www.clickatell.com/developers/api-docs/callback-(push-status-and-cost-notification)-advanced-message-send/
      */
      msg.callback = 3; //Callback for errors 003, 004, 005, 007, 009, 010, 012


      Log.debug("SMSActor: Sending SMS: " + msg.toString());
      String jsonRequest = om.writeValueAsString(msg);
      WSRequest request = ws
          .url("https://api.clickatell.com/rest/message")
          .setHeader("X-Version", "1")
          .setHeader("Content-Type", "application/json")
          .setHeader("Authorization", "bearer " + smsToken)
          .setHeader("Accept", "application/json")
          .setRequestTimeout(5000)
          .setFollowRedirects(true); //Just in case

      request.post(jsonRequest).thenAcceptAsync(response -> {
        HttpStatusCode sc = HttpStatusCode.fromCode(response.getStatus());
        if (sc.isError()) {
          Log.err("SMSActor: SMS Send fail Http Error Code. " + sc.name() + ": " + sc.getDetail());
          smsLog.setStatus(sc.name());
          smsLog.markModified(Account.AID_PUBLISHER);
          smsLog.update();
          return;
        }

        try {
          JsonNode            jsonNode = response.asJson();
          SendResponse        sr  = om.treeToValue(jsonNode, SendResponse.class);
          SendResponseMessage srm = sr.data.message.get(0); //We sent 1 so we should get at least 1
          if (srm == null) {
            smsLog.setStatus("Error Submitting");
          }
          else {
            if (srm.accepted) {
              smsLog.setStatus("Accepted");
            }
            else {
              smsLog.setStatus("Rejected");
            }
            smsLog.setApiMessageId(srm.apiMessageId);
          }
          smsLog.setDestMsisdn(srm.to);
          smsLog.setApiId(smsApiID);
          smsLog.update();
        }
        catch (JsonProcessingException e) {
          Log.err("SMSActor::submitSMS JSON Error", e);
        }

      });
    }
    catch (Exception e) {
      Log.err("SMSActor: failed sending.", e);
      return false;
    }
    return true;
  }

  public String cleanMSISDN(String num) {
    return num.replaceAll("[^0-9]", "");
  }

  public interface Factory {
    Actor create();
  }
}
