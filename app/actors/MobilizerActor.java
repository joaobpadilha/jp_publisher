package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.fasterxml.jackson.databind.JsonNode;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.persistence.cache.MobilizerStateCO;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.newrelic.agent.deps.org.apache.http.cookie.ClientCookie;
import controllers.MobilizerController;
import models.publisher.LibraryPage;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by surge on 2015-04-29.
 */
public class MobilizerActor
    extends UntypedActor {
  public interface Factory {
    public Actor create();
  }

  /**
   * Akka Actor Properties
   */
  public final static Props props = Props.create(MobilizerActor.class);

  private final static String T42_DOMAIN_URL = "travel-42.com";
  private final static String T42_DETAIL_URL = "travel-42.com/Client/View/detail.aspx";
  private final static String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36";

  private final static int FF_CONN_TIMEOUT = 60000; //Some requests take up to 60 seconds


  public static enum MobilizerCommand
      implements Serializable {
    T42_LOGIN,
    T42_MOBILIZE_TRIP_PLAN,
    T42_MOBILIZE_PAGE,
    SEARCH,
    MOBILIZE_URL
  }

  public static class MobilizerMessage
      implements Serializable {
    public MobilizerCommand cmd;

    /**
     * Identifier that browser can use to query state of the request
     */
    private String uuid;

    /**
     * Id of the user who made the request
     */
    private String userId;

    /**
     * Username to use to authenticate
     */
    private String username;

    /**
     * Password to use to authenticate
     */
    private String password;

    /**
     * Url to be mobilized
     */
    private String url;

    public MobilizerMessage(MobilizerCommand cmd) {
      this.cmd = cmd;
    }

    public String getUrl() {
      return url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public String getUsername() {
      return username;
    }

    public void setUsername(String username) {
      this.username = username;
    }

    public String getPassword() {
      return password;
    }

    public void setPassword(String password) {
      this.password = password;
    }

    public String getUserId() {
      return userId;
    }

    public void setUserId(String userId) {
      this.userId = userId;
    }

    public String getUuid() {
      return uuid;
    }

    public void setUuid(String uuid) {
      this.uuid = uuid;
    }
  }

  /**
   * To be implemented by concrete UntypedActor, this defines the behavior of the
   * UntypedActor.
   *
   * @param message
   */
  @Override public void onReceive(Object message)
      throws Exception {

    if (message instanceof MobilizerMessage) {
      MobilizerMessage cmd = (MobilizerMessage) message;
      StopWatch sw = new StopWatch();
      sw.start();
      MobilizerStateCO stateCO = MobilizerStateCO.getFromCache(cmd.getUuid());
      try {

        switch (cmd.cmd) {
          case T42_LOGIN:
            t42Login(cmd, stateCO);
            break;
          case T42_MOBILIZE_TRIP_PLAN:
            t42MobilizeTripPlan(cmd, stateCO);
            break;
          case T42_MOBILIZE_PAGE:
            break;
          case SEARCH:
            break;
          case MOBILIZE_URL:
            break;
          default:
            Log.err("Unhandled command: " + cmd.cmd.name());
            throw new UnsupportedOperationException();
        }

      } catch(Exception e) {
        stateCO.error();
        Log.err("Error: Something went terribly wrong in Mobilizer actor:", e.getMessage());
        e.printStackTrace();
      }
      sw.stop();
      Log.info("Mobilizer finished command:" + cmd.cmd.name() + " in " + sw.getTime() + "ms");
    }
    else {
      unhandled(message);
    }
  }

  private void t42MobilizeTripPlan(MobilizerMessage cmd, MobilizerStateCO state) {
    state.start(MobilizerStateCO.State.CONNECTING, 1);

    List<LibraryPage> pages = new ArrayList<>();
    try {
      Document cover = Jsoup.connect(cmd.getUrl())
                            .timeout(FF_CONN_TIMEOUT)
          .userAgent(USER_AGENT) //Let them think people are coming in
          .get();

      Elements pageLinks = cover.select("div#toc a");
      state.start(MobilizerStateCO.State.PARSING, pageLinks.size());

      /*
      Element eTitle = cover.select("div.detail h1.cb").first();
      String eTitleStr ="";
      if (eTitle != null && eTitle.text() != null) {
        eTitleStr = eTitle.text();
      }
      */

      for (Element pageUrl : pageLinks) {
        String subPageUrl = pageUrl.attr("href");
        if (!subPageUrl.contains("travel-42.com")) {
          subPageUrl = "http://www.travel-42.com" + subPageUrl;
        }

        JsonNode ffData = MobilizerController.mobilizeUrl(subPageUrl);
        LibraryPage lp = MobilizerController.saveMobilizedPage("travel42", ffData, cmd.userId);
        state.next();
        pages.add(lp);
      }
    } catch (IOException ioe) {
      ioe.printStackTrace();
      state.error();
    }

    state.pages = pages;
    state.success();
  }

  private void t42Login(MobilizerMessage msg, MobilizerStateCO stateCO) {

    StopWatch sw = new StopWatch();
    sw.start();
    stateCO.start(MobilizerStateCO.State.CONNECTING, 1);
    String viewState = "/wEPDwULLTE1MDY1Mjg1MjgPZBYCZg9kFgQCAQ9kFgICDQ8WAh4EVGV4dAU7PGxpbmsgcmVsPSdpY29uJyBocmVmPScvZmF2aWNvbi5pY28nIHR5cGU9J2ltYWdlL3gtaWNvbicgLz5kAgMPZBYCAgMPZBYGAgUPDxYCHgdWaXNpYmxlZ2RkAg0PFgIeBWNsYXNzBQhjb250ZW50MhYCAgEPZBYCAgEPZBYEZg8PFgIfAWhkZAICDw9kFgIeBXN0eWxlBRNtYXJnaW4tYm90dG9tOjE0cHg7FgYCAQ8PFgIfAWhkZAIDDw8WAh8BaGRkAgUPZBYCAgEPDxYCHwFoZGQCEQ8PFgIfAWhkZBgBBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WAgUVY3RsMDAkTG9naW4xJGJ0bkxvZ2luBRhjdGwwMCRMb2dpbjEkY2hrUmVtZW1iZXLE69ilOQZOZCpG/ttkNkgQr6Bnqg==";

    try {
      URL url = new URL("http://www.travel-42.com");
      Document doc = Jsoup.parse(url, 30000);
      Element input = doc.getElementById("__VIEWSTATE");
      if (input != null && input.attr("value") != null  && input.attr("value").length() > 0 ) {
        viewState = input.attr("value");
      }

      BasicCookieStore httpCookieStore = new BasicCookieStore();
      HttpClient c = HttpClientBuilder.create().setDefaultCookieStore(httpCookieStore).build();
      HttpPost p = new HttpPost("https://www.travel-42.com");     //use service id in the future

      List<NameValuePair> nvps = new ArrayList<>();
      nvps.add(new BasicNameValuePair("__LASTFOCUS", ""));
      nvps.add(new BasicNameValuePair("__VIEWSTATE", viewState));
      nvps.add(new BasicNameValuePair("__EVENTTARGET", ""));
      nvps.add(new BasicNameValuePair("__EVENTARGUMENT", ""));
      nvps.add(new BasicNameValuePair("__VIEWSTATEGENERATOR", "CA0B0334"));
      nvps.add(new BasicNameValuePair("ctl00$Login1$txtUserName", msg.getUsername()));
      nvps.add(new BasicNameValuePair("ctl00$Login1$txtPassword", msg.getPassword()));
      nvps.add(new BasicNameValuePair("ctl00$Login1$btnLogin.x", "38"));
      nvps.add(new BasicNameValuePair("ctl00$Login1$btnLogin.y", "8"));

      p.setEntity(new UrlEncodedFormEntity(nvps));
      HttpResponse r = c.execute(p);

      if (r.getStatusLine() != null &&
          r.getStatusLine().getStatusCode() == 302) {

        Log.debug("travel42 Login: " + sw.getTime());

        //consume the request to close the HTTP connection
        EntityUtils.consume(r.getEntity());

        //get more cookies
        HttpGet g1 = new HttpGet("http://www.travel-42.com/Home");
        HttpResponse r1 = c.execute(g1);
        EntityUtils.consume(r1.getEntity());


        stateCO.start(MobilizerStateCO.State.PARSING, 1);
        HttpPost g = new HttpPost("http://www.travel-42.com/Dyna.asmx/GetReports");

        //Json to get list of all trips
        String json = "{\"Req\":{\"ReqType\":\"ALL\",\"FolderID\":\"\",\"ListType\":\"T\",\"Value\":\"\"," +
                      "\"Sort\":\"CREATE\",\"GetShared\":false,\"SortOrder\":\"D\",\"Page\":\"1\"}}";

        g.setEntity(new ByteArrayEntity(json.getBytes("UTF-8")));
        g.setHeader("Content-type", "application/json");

        BasicClientCookie cookie = new BasicClientCookie("s_cc", "true");
        cookie.setDomain(".travel-42.com");
        cookie.setPath("/");
        cookie.setAttribute(ClientCookie.DOMAIN_ATTR, "true");
        httpCookieStore.addCookie(cookie);

        cookie = new BasicClientCookie("s_nr", "1501881360522-New1");
        cookie.setDomain(".travel-42.com");
        cookie.setPath("/");
        cookie.setAttribute(ClientCookie.DOMAIN_ATTR, "true");
        httpCookieStore.addCookie(cookie);

        cookie = new BasicClientCookie("s_sq", "%5B%5BB%5D%5D");
        cookie.setDomain(".travel-42.com");
        cookie.setPath("/");
        cookie.setAttribute(ClientCookie.DOMAIN_ATTR, "true");
        httpCookieStore.addCookie(cookie);

        r = c.execute(g);

        if (r != null &&
            r.getStatusLine() != null &&
            r.getStatusLine().getStatusCode() == 200) {
          Log.debug("travel42 All Trips: " + sw.getTime());
          String body = StringEscapeUtils.unescapeJava(EntityUtils.toString(r.getEntity())) ;
          body = new String(body.getBytes("ISO-8859-1"), "UTF-8");

          Document docsPage = Jsoup.parseBodyFragment(body);
          Elements guideEntries = docsPage.select("div.trips-table tbody tr");
          Log.debug("Found " + guideEntries.size() + " travel42 guides");

          Map<String, String> guides = new TreeMap<>();
          for (Element e: guideEntries) {
            String id = "http://www.travel-42.com/Client/View/?rid=" +
                        e.select("div.trip-table-tname > input").first().attr("_id");
            String name = e.select("div.trip-table-tname > a").first().text();
            guides.put(id, name);
          }

          //sort guides
          stateCO.links = Utils.sortMapByValue(guides);
          if (stateCO.links == null || stateCO.links.size() == 0) {
            stateCO.start(MobilizerStateCO.State.NO_DATA, 0);
          } else {
            CacheMgr.set(APPConstants.CACHE_T42_REPORT_USER + msg.getUserId(), stateCO.links,
                         APPConstants.CACHE_EXPIRY_T42_COOKIE);
            stateCO.success();
          }
        }
      } else {
        if (r.getStatusLine() != null &&
            r.getStatusLine().getStatusCode() >= 200 &&
            r.getStatusLine().getStatusCode() < 300 ) {
          stateCO.start(MobilizerStateCO.State.INVALID_LOGIN, 0);
        } else {
          stateCO.error();
          Log.err("Unknown Travel42 Error when trying to login with User:" + msg.getUsername() +
                  " Password:" + msg.getPassword());
        }
      }
    }
    catch (IOException e) {
      Log.err("Travel42 Login Failure", e);
      stateCO.error();
    }
  }
}
