package actors;

import akka.actor.*;
import com.mapped.publisher.utils.Log;
import models.publisher.TripAnalysis;
import play.libs.Akka;
import play.libs.akka.InjectedActorSupport;
import scala.concurrent.duration.Duration;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Umapped Actor supervisor.
 * <p>
 * This actor is responsible for initiating all other actors used by Umapped. Add actor initialization into constructor
 * for this class.
 * <p>
 * Created by surge on 2015-09-25.
 */
@Singleton
public class SupervisorActor
    extends UntypedActor
    implements InjectedActorSupport {

  public enum UmappedActor {
    //@formatter:off
    BILLING("user/main/billing",                          "billing",              BillingActor.props),
    FIREBASE("user/main/firebase",                        "firebase",             FirebaseActor.props),
    COMMUNICATOR_TO_ALL("user/main/comm_to_all",          "comm_to_all",          CommunicatorToAllActor.props),
    COMMUNICATOR_REPLY("user/main/comm_reply",            "comm_reply",           CommunicatorReplyActor.props),
    COMMUNICATOR_SMS("user/main/sms",                     "sms",                  CommunicatorSMSActor.props),
    COMMUNICATOR_DIGEST("user/main/comm_digest",          "comm_digest",          CommunicatorDigestActor.props),
    EMAIL_PARSER_ATTACH("user/main/email_parser_attach",  "email_parser_attach",  EmailAttachmentParserActor.props),
    EMAIL_PARSER_BODY("user/main/email_parser_body",      "email_parser_body",    EmailBodyParserActor.props),
    FLIGHT_NOTIFY("user/main/flight_notify",              "flight_notify",        FlightNotifyActor.props),
    FLIGHT_POLL("user/main/flight_poll",                  "flight_poll",          FlightPollAlertsActor.props),
    FLIGHT_TRACK("user/main/flight_track",                "flight_track",         FlightTrackActor.props),
    ICE_PORTAL("user/main/iceportal",                     "iceportal",            IcePortalActor.props),
    MOBILIZER("user/main/mobilizer",                      "mobilizer",            MobilizerActor.props),
    PDF_DOC("user/main/pdf_doc",                          "pdf_doc",              DocPDFActor.props),
    PDF_TRIP("user/main/pdf_trip",                        "pdf_trip",             TripPDFActor.props),
    RECORD_LOCATOR("user/main/record_locator",            "record_locator",       RecLocatorActor.props),
    SUPERVISOR("user/main",                               "main",                 SupervisorActor.props),
    VIRTUOSO_CRUISE("user/main/virtuoso_cruise",          "virtuoso_cruise",      VirtuosoCruiseActor.props),
    VIRTUOSO_CRUISES("user/main/virtuoso_cruises",        "virtuoso_cruises",     VirtuosoCruisesActor.props),
    MOBILE_PUSH_NOTIFY("user/main/mobile_push_notify",    "mobile_push_notify",   MobilePushNotificationActor.props),
    PUBLISH_FIRABASE("user/main/publish_firebase",        "publish_firebase",     PublishFirebaseActor.props),
    WETU_ITINERARY("user/main/wetu_itinerary",            "wetu_itinerary",       WetuItineraryActor.props),
    MAINTENANCE("user/main/maintenance",                  "maintenance",          MaintenanceActor.props),
    TRIP_AUTO_CREATE_PUBLISH("user/main/trip_autocreate_publish",        "trip_autocreate_publish",     TripAutoCreatePublishActor.props),
    TRIP_PUBLISHER("user/main/trip_publisher",        "trip_publisher",     TripPublisherActor.props),
    TRIP_ANALYZE("user/main/trip_analysis", "trip_analysis", TripAnalysisActor.props);

    //@formatter:on
    final private String path;
    final private String relativePath;
    final private Props  props;

    UmappedActor(String path, String relativePath, Props props) {
      this.path = path;
      this.relativePath = relativePath;
      this.props = props;
    }

    public String getRelativePath() {
      return relativePath;
    }

    public String getPath() {
      return path;
    }

    public Props getProps() {
      return props;
    }
  }

  public static Props props = Props.create(SupervisorActor.class);
  @Inject
  BillingActor.Factory               afBilling;
  @Inject
  DocPDFActor.Factory                afDocPdf;
  @Inject
  FirebaseActor.Factory              afFirebase;
  @Inject
  CommunicatorToAllActor.Factory     afCommToAll;
  @Inject
  CommunicatorReplyActor.Factory     afCommReply;
  @Inject
  CommunicatorSMSActor.Factory       afCommSMS;
  @Inject
  CommunicatorDigestActor.Factory    afCommDigest;
  @Inject
  EmailAttachmentParserActor.Factory afEmailAttach;
  @Inject
  EmailBodyParserActor.Factory       afEmailBody;
  @Inject
  FlightNotifyActor.Factory          afFlightNotify;
  @Inject
  FlightPollAlertsActor.Factory      afFlightPoll;
  @Inject
  FlightTrackActor.Factory           afFlightTrack;
  @Inject
  IcePortalActor.Factory             afIce;
  @Inject
  MobilizerActor.Factory             afMob;
  @Inject
  RecLocatorActor.Factory            afRecLoc;
  @Inject
  TripPDFActor.Factory               afTripPDF;
  @Inject
  VirtuosoCruiseActor.Factory        afVirtCr;
  @Inject
  VirtuosoCruisesActor.Factory       afVirtCrs;
  @Inject
  PublishFirebaseActor.Factory       afPubFirebase;
  @Inject
  MobilePushNotificationActor.Factory    afMobilePushNotify;
  @Inject
  WetuItineraryActor.Factory         afWetuItinerary;
  @Inject
  MaintenanceActor.Factory           afMaintenance;
  @Inject
  TripPublisherActor.Factory         afTripPublisher;
  @Inject
  TripAutoCreatePublishActor.Factory afTripAutoCreatePublish;
  @Inject
  TripAnalysisActor.Factory afTripAnalysis;

  private Map<UmappedActor, ActorRef> actors;

  public static class Protocol {
    public enum Command {
      INIT
    }

    public Command cmd;

    private Protocol() {
    }

    public static Protocol build(Command cmd) {
      Protocol p = new Protocol();
      p.cmd = cmd;
      return p;
    }
  }

  public SupervisorActor() {
    actors = new HashMap<>();
  }

  private void launchActor(UmappedActor actor, Props props) {
    Log.info("Launching Actor: " + actor.name());
    ActorRef ref = context().actorOf(props, actor.relativePath);
    addActor(actor, ref);
  }

  private void addActor(UmappedActor actor, ActorRef ref) {
    Log.info("Launched Actor: " + actor.name());
    actors.put(actor, ref);
  }

  /**
   * To be implemented by concrete UntypedActor, this defines the behavior of the UntypedActor.
   * @param message
   */
  @Override
  public void onReceive(Object message)
      throws Exception {

    if (message instanceof Protocol) {
      Protocol p = (Protocol) message;
      switch (p.cmd) {
        case INIT:
          addActor(UmappedActor.BILLING,
                   injectedChild(() -> afBilling.create(),      UmappedActor.BILLING.relativePath));
          addActor(UmappedActor.FIREBASE,
                   injectedChild(() -> afFirebase.create(),     UmappedActor.FIREBASE.relativePath));
          addActor(UmappedActor.COMMUNICATOR_TO_ALL,
                   injectedChild(() -> afCommToAll.create(),    UmappedActor.COMMUNICATOR_TO_ALL.relativePath));
          addActor(UmappedActor.COMMUNICATOR_REPLY,
                   injectedChild(() -> afCommReply.create(),    UmappedActor.COMMUNICATOR_REPLY.relativePath));
          addActor(UmappedActor.COMMUNICATOR_SMS,
                   injectedChild(() -> afCommSMS.create(),      UmappedActor.COMMUNICATOR_SMS.relativePath));
          addActor(UmappedActor.COMMUNICATOR_DIGEST,
                  injectedChild(() -> afCommDigest.create(),    UmappedActor.COMMUNICATOR_DIGEST.relativePath));
          addActor(UmappedActor.PDF_DOC,
                   injectedChild(() -> afDocPdf.create(),       UmappedActor.PDF_DOC.relativePath));
          addActor(UmappedActor.EMAIL_PARSER_ATTACH,
                   injectedChild(() -> afEmailAttach.create(),  UmappedActor.EMAIL_PARSER_ATTACH.relativePath));
          addActor(UmappedActor.EMAIL_PARSER_BODY,
                   injectedChild(() -> afEmailBody.create(),    UmappedActor.EMAIL_PARSER_BODY.relativePath));
          addActor(UmappedActor.FLIGHT_NOTIFY,
                   injectedChild(() -> afFlightNotify.create(), UmappedActor.FLIGHT_NOTIFY.relativePath));
          addActor(UmappedActor.FLIGHT_POLL,
                   injectedChild(() -> afFlightPoll.create(),   UmappedActor.FLIGHT_POLL.relativePath));
          addActor(UmappedActor.FLIGHT_TRACK,
                   injectedChild(() -> afFlightTrack.create(),  UmappedActor.FLIGHT_TRACK.relativePath));
          addActor(UmappedActor.ICE_PORTAL,
                   injectedChild(() -> afIce.create(),          UmappedActor.ICE_PORTAL.relativePath));
          addActor(UmappedActor.MOBILIZER,
                   injectedChild(() -> afMob.create(),          UmappedActor.MOBILIZER.relativePath));
          addActor(UmappedActor.RECORD_LOCATOR,
                   injectedChild(() -> afRecLoc.create(),       UmappedActor.RECORD_LOCATOR.relativePath));
          addActor(UmappedActor.PDF_TRIP,
                   injectedChild(() -> afTripPDF.create(),      UmappedActor.PDF_TRIP.relativePath));
          addActor(UmappedActor.VIRTUOSO_CRUISE,
                   injectedChild(() -> afVirtCr.create(),       UmappedActor.VIRTUOSO_CRUISE.relativePath));
          addActor(UmappedActor.VIRTUOSO_CRUISES,
                   injectedChild(() -> afVirtCrs.create(),      UmappedActor.VIRTUOSO_CRUISES.relativePath));
          addActor(UmappedActor.PUBLISH_FIRABASE,
                   injectedChild(() -> afPubFirebase.create(),  UmappedActor.PUBLISH_FIRABASE.relativePath));
          addActor(UmappedActor.MOBILE_PUSH_NOTIFY,
                   injectedChild(() -> afMobilePushNotify.create(), UmappedActor.MOBILE_PUSH_NOTIFY.relativePath));
          addActor(UmappedActor.WETU_ITINERARY,
                   injectedChild(() -> afWetuItinerary.create(), UmappedActor.WETU_ITINERARY.relativePath));
          addActor(UmappedActor.MAINTENANCE,
                   injectedChild(() -> afMaintenance.create(),  UmappedActor.MAINTENANCE.relativePath));
          addActor(UmappedActor.TRIP_PUBLISHER,
                   injectedChild(() -> afTripPublisher.create(), UmappedActor.TRIP_PUBLISHER.relativePath));
          addActor(UmappedActor.TRIP_AUTO_CREATE_PUBLISH,
                   injectedChild(() -> afTripAutoCreatePublish.create(), UmappedActor.TRIP_AUTO_CREATE_PUBLISH.relativePath));
          addActor(UmappedActor.TRIP_ANALYZE,
                  injectedChild(() -> afTripAnalysis.create(), UmappedActor.TRIP_ANALYZE.relativePath));
          break;
      }
    }
  }
}
