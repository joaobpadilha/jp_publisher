package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.avaje.ebean.Ebean;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.mapped.publisher.persistence.PageAttachType;
import com.mapped.publisher.persistence.VOModeller;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.umapped.external.wetu.content.*;
import com.umapped.external.wetu.content.DayTour;
import com.umapped.external.wetu.itinerary.ItineraryExtractor;
import com.umapped.external.wetu.itinerary.details.*;
import com.umapped.external.wetu.itinerary.details.Contact;
import com.umapped.external.wetu.itinerary.details.Day;
import com.umapped.external.wetu.itinerary.details.Leg;
import com.umapped.external.wetu.itinerary.details.Route;
import com.umapped.external.wetu.pins.list.*;
import com.umapped.external.wetu.pins.list.Activity;
import com.umapped.external.wetu.pins.list.Image;
import com.umapped.external.wetu.pins.list.Pin;
import com.umapped.persistence.enums.ReservationType;
import controllers.BookingNoteController;
import controllers.ImageController;
import models.publisher.*;
import org.apache.commons.lang3.time.DateUtils;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by twong on 2016-07-11.
 */
public class WetuItineraryActor extends UntypedActor {
  private final static String[] DATE_TIME_FORMATS = {"MM/dd/yyyy", "MM/dd/yyyy hh:mm:ss a", "MM/dd/yyyy h:mm:ss a", "MM/dd/yyyy h:mma", "yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyy-MM-dd h:mma"};
  private final static String[] TIME_FORMATS = {"hh:mm:ss a", "h:mma", "HH:mm:ss", "HH:mm", "hh:mm"};
  private final static DateUtils dateUtils = new DateUtils();
  private final static PolicyFactory policy = new HtmlPolicyBuilder().allowElements("br").toFactory();

  public interface Factory {
    public Actor create();
  }

  /**
   * Akka Actor Properties
   */
  public final static Props props = Props.create(WetuItineraryActor.class);

  public static class Command implements  Serializable{
    private final Trip trip;
    private final String token;
    private final String itineraryId;
    private final String userId;
    private final Timestamp startDate;
    
    public Command(Trip trip, String token, String itineraryId, String userId, Timestamp startDate) {
      this.trip = trip;
      this.token = token;
      this.itineraryId = itineraryId;
      this.userId = userId;
      this.startDate = startDate;
    }

    public Trip getTrip() {
      return trip;
    }

    public String getToken() {
      return token;
    }

    public String getItineraryId() {
      return itineraryId;
    }

    public String getUserId() {
      return userId;
    }

    public Timestamp getStartDate() {
      return startDate;
    }
  }

  @Override
  public void onReceive(Object message)
      throws Exception {
    if (message instanceof Command) {
      Command cmd = (Command) message;

      if (StringUtils.isNumeric(cmd.getToken())) {
        buildWetuTrip2(cmd);
      } else {
        buildWetuTrip(cmd);
      }

    }
  }

  public void buildWetuTrip (Command cmd) throws Exception {
    String identifier = cmd.getItineraryId();

    ItineraryExtractor extractor = new ItineraryExtractor();
    ItineraryDetail itineraryDetail = extractor.extractItineraryDetail(identifier);

    Map<String, NoteVO> noteVOMap = new HashMap<>();
    Map<String, List<Image>> noteImageMap = new HashMap<>();
    Map<String, List<NoteVO>> destinationContentNoteVOMap = new HashMap<>();
    Map<String, NoteVO> countryContentNoteVOMap = new HashMap<>();




    TripVO tripVO = new TripVO();
    tripVO.setName(itineraryDetail.getName());
    tripVO.setRecordLocator(identifier);
    tripVO.setSrc("WETU");
    tripVO.setImportSrcId(identifier);
    tripVO.setImportTs(System.currentTimeMillis());

    if (itineraryDetail.getTravellers() != null && itineraryDetail.getTravellers().size() > 0) {
      for (Traveller t : itineraryDetail.getTravellers()) {
        PassengerVO passenger = new PassengerVO();
        passenger.setFullName(t.getName());
        tripVO.addPassenger(passenger);
      }
    }

    WetuExtractor wetuExtractor = new WetuExtractor();
    Timestamp startTimestamp = null;
    Timestamp initStartTime = null;
    Timestamp lastStartTime = null;


    try {

      String startDate = itineraryDetail.getStartDate();
      if (startDate != null && !startDate.isEmpty()) {
        startTimestamp = new Timestamp(dateUtils.parseDate(startDate, DATE_TIME_FORMATS).getTime());
      } else if (cmd.getStartDate() != null) {
        startTimestamp = cmd.getStartDate();
      }

      initStartTime = startTimestamp;
      lastStartTime = startTimestamp;

    }catch (Exception e) {
      Log.log(LogLevel.ERROR, "Time stamp conversion: ", e);
      e.printStackTrace();
      CacheMgr.set(APPConstants.CACHE_WETU_ITINERARY + cmd.getItineraryId() + cmd.getUserId(), "Error importing itinerary - please contact Umapped support");
      return;
    }

    //Create General TripNot for the Trip to store all generic information
    StringBuilder genTripNotes = new StringBuilder();
   /*
    genTripNotes.append("Wetu Identifier: ");
    genTripNotes.append(identifier);
    genTripNotes.append("\n");
    genTripNotes.append("Identifier Key: ");
    genTripNotes.append(itineraryDetail.getIdentifierKey());
    genTripNotes.append("\n");

    int tNum1 = 1;
    for (String s : itineraryDetail.getCategories()){
      if (s != null || s.length() > 0) {
        genTripNotes.append("Category "+tNum1+": ");
        genTripNotes.append(s);
        genTripNotes.append("\n");
        tNum1++;
      }
    }
    genTripNotes.append("\n");
    */
    if (itineraryDetail.getReferenceNumber() != null  && itineraryDetail.getReferenceNumber().length() > 0) {
      genTripNotes.append("Reference Number: ");
      genTripNotes.append(itineraryDetail.getReferenceNumber());
      genTripNotes.append("\n\n");
    }
    if (itineraryDetail.getPrice() != null && itineraryDetail.getPrice().length() > 0) {
      genTripNotes.append("Price: ");
      genTripNotes.append(itineraryDetail.getPrice());
      genTripNotes.append("\n\n");
    }
    if (itineraryDetail.getPriceIncludes() != null && itineraryDetail.getPriceIncludes().length() > 0) {
      genTripNotes.append("Price Includes: \n");
      genTripNotes.append(itineraryDetail.getPriceIncludes());
      genTripNotes.append("\n\n");
    }
    if (itineraryDetail.getPriceExcludes() != null && itineraryDetail.getPriceExcludes().length() > 0) {
      genTripNotes.append("Excluded: \n");
      genTripNotes.append(itineraryDetail.getPriceExcludes());
      genTripNotes.append("\n\n");
    }

    for (Contact c : itineraryDetail.getContacts()){
      if (c != null) {
        genTripNotes.append("Company: ");
        genTripNotes.append(c.getCompany());
        genTripNotes.append("\n");
        genTripNotes.append("Telephone: ");
        genTripNotes.append(c.getTelephone());
        genTripNotes.append("\n");
        genTripNotes.append("Contact Person: ");
        genTripNotes.append(c.getContactPerson());
        genTripNotes.append("\n");
        genTripNotes.append("Email: ");
        genTripNotes.append(c.getEmail());
        genTripNotes.append("\n\n");
      }
    }

    NoteVO genNoteVO = new NoteVO();
    genNoteVO.setName("Additional Trip Information");
    genNoteVO.setNote(sanitizeString(genTripNotes.toString(),"<br>"));
    genNoteVO.setTimestamp(startTimestamp);
    genNoteVO.setRank(0);
    tripVO.addNoteVO(genNoteVO);



    List<Integer> countries = new ArrayList<>();
    List<Integer> destinations = new ArrayList<>();


    int i = 0;

    //Obtain all the contentIds into a List
    for (Leg leg : itineraryDetail.getLegs()) {

      i++;
      int contentId = leg.getContentEntityId();
      int destinationContentId = leg.getDestinationContentId();

      StringBuilder bookingNum = new StringBuilder();
      bookingNum.append(contentId);
      bookingNum.append("-");
      bookingNum.append(leg.getItineraryLegId());
      bookingNum.append("-");
      bookingNum.append(leg.getSequence());

      if (contentId != 0) {
        Accommodation[] accommodations = wetuExtractor.getAccommodations(cmd.getToken(), "ids=" + String.valueOf(contentId));

        if (accommodations != null && accommodations.length > 0) {
          HotelVO hotel = new HotelVO();

          hotel.setCheckin(startTimestamp);
          hotel.setHotelName(accommodations[0].getName());
          int numNights = leg.getNights();
          Timestamp checkoutDate = new Timestamp(startTimestamp.getTime() + TimeUnit.DAYS.toMillis(numNights));
          hotel.setCheckout(checkoutDate);
          hotel.setConfirmation(bookingNum.toString());
          hotel.setRank(tripVO.getHotels().size());
          //hotel.setSrc("WETU");
          //hotel.setSrcId(leg.getContentEntityId());
          hotel.setRecordLocator(itineraryDetail.getIdentifier());
          StringBuilder note = new StringBuilder();

        /*
        note.append("Content ID: ");
        note.append(leg.getContentEntityId());
        note.append("\n");
        int tNum = 1;
        for (Traveller t : itineraryDetail.getTravellers()){
          if (t.getName() != null || t.getName().length() > 0) {
            note.append("Traveller "+tNum+": ");
            note.append(t.getName());
            note.append("\n");
            tNum++;
          }
        }

        if (leg.getDays().get(0).getRoomBasis() != null && leg.getDays().get(0).getRoomBasis().length() > 0) {
          note.append("Room Basis: ");
          note.append(leg.getDays().get(0).getRoomBasis());
          note.append("\n");
        }
        if (leg.getDays().get(0).getDrinksBasis() != null && leg.getDays().get(0).getDrinksBasis().length() > 0) {
          note.append("Drink Basis: ");
          note.append(leg.getDays().get(0).getDrinksBasis());
          note.append("\n");
        }
        */
          if (leg.getDays().size() > 0 && leg.getDays().get(0).getNotes() != null && leg.getDays().get(0).getNotes().length() > 0) {
            note.append(leg.getDays().get(0).getNotes());
            note.append("\n");
          }
          if (leg.getDays().size() > 0 && leg.getDays().get(0).getIncluded() != null && leg.getDays().get(0).getIncluded().length() > 0) {
            note.append("Included: \n");
            note.append(leg.getDays().get(0).getIncluded());
            note.append("\n");
          }


          hotel.setNote(sanitizeString(note.toString(), "\n"));


          tripVO.addHotel(hotel);

          NoteVO nVo = new NoteVO();
          nVo.setName(accommodations[0].getName());
          nVo.setTimestamp(startTimestamp);
          nVo.setOrigSrcPK(bookingNum.toString());
          nVo.setRank(hotel.getRank());
          nVo.setRecordLocator(hotel.getRecordLocator());
          StringBuilder note1 = new StringBuilder();
          if (accommodations[0].getContent().getGeneralDescription() != null && accommodations[0].getContent().getGeneralDescription().length() > 0) {
            //          note1.append("General Description: \n");
            note1.append(accommodations[0].getContent().getGeneralDescription());
            note1.append("\n");
          } else if (accommodations[0].getContent().getExtendedDescription() != null && accommodations[0].getContent().getExtendedDescription().length() > 0) {
            //          note1.append("Extended Description: \n");
            note1.append(accommodations[0].getContent().getExtendedDescription());
            note1.append("\n");
          } else if (accommodations[0].getContent().getTeaserDescription() != null && accommodations[0].getContent().getTeaserDescription().length() > 0) {
            //          note1.append("Teaser Description: \n");
            note1.append(accommodations[0].getContent().getTeaserDescription());
            note1.append("\n");
          }
          if (accommodations[0].getFeatures().getSpecialInterests().size() > 0) {
            note1.append("Special Interests: ");
            note1.append(accommodations[0].getFeatures().getSpecialInterests().toString().replace("[", "").replace("]", ""));
            note1.append("\n\n");
          }
          if (accommodations[0].getFeatures().getPropertyFacilities().size() > 0) {
            note1.append("Properties Facilities: ");
            note1.append(accommodations[0].getFeatures().getPropertyFacilities().toString().replace("[", "").replace("]", ""));
            note1.append("\n\n");
          }
          if (accommodations[0].getFeatures().getSpecialInterests().size() > 0) {
            note1.append("Room Facilities: ");
            note1.append(accommodations[0].getFeatures().getRoomFacilities().toString().replace("[", "").replace("]", ""));
            note1.append("\n\n");
          }
          if (accommodations[0].getFeatures().getAvailableServices().size() > 0) {
            note1.append("Available Services: ");
            note1.append(accommodations[0].getFeatures().getAvailableServices().toString().replace("[", "").replace("]", ""));
            note1.append("\n\n");
          }
          if (accommodations[0].getFeatures().getActivitiesOnSite().size() > 0) {
            note1.append("Activities On-site: ");
            note1.append(accommodations[0].getFeatures().getActivitiesOnSite().toString().replace("[", "").replace("]", ""));
            note1.append("\n\n");
          }
          if (accommodations[0].getFeatures().getActivitiesOffSite().size() > 0) {
            note1.append("Activities Off-site: ");
            note1.append(accommodations[0].getFeatures().getActivitiesOffSite().toString().replace("[", "").replace("]", ""));
            note1.append("\n\n");
          }
          nVo.setNote(sanitizeString(note1.toString(), "<br>"));
          nVo.setStreetAddr(accommodations[0].getPosition().getDestination());
          nVo.setCity(accommodations[0].getPosition().getRegion());
          nVo.setState(accommodations[0].getPosition().getArea());
          nVo.setCountry(accommodations[0].getPosition().getCountry());
          nVo.setLocLat((float) accommodations[0].getPosition().getLatitude());
          nVo.setLocLong((float) accommodations[0].getPosition().getLongitude());
          noteVOMap.put(bookingNum.toString(), nVo);

          noteImageMap.put(bookingNum.toString(), accommodations[0].getContent().getImages());

          //Get Leg Activities
          if (leg.getActivities() != null && leg.getActivities().size() > 0) {
            for (com.umapped.external.wetu.itinerary.details.Activity a : leg.getActivities()) {
              for (SimpleItem act : accommodations[0].getActivities()) {
                if (act.getId().equals(a.getSimpleItemId())) {
                  StringBuilder activityNum = new StringBuilder(bookingNum.toString());
                  activityNum.append("-");
                  activityNum.append(act.getId());
                  activityNum.append("-");
                  activityNum.append(a.getSequence());
                  ActivityVO activityVO = new ActivityVO();
                  activityVO.setName(act.getName());
                  //activityVO.setSrcId(Long.parseLong(act.getId()));
                  activityVO.setConfirmation(activityNum.toString());
                  activityVO.setRank(tripVO.getActivities().size());

                  activityVO.setStartDate(startTimestamp);
                  activityVO.setEndDate(checkoutDate);
                  tripVO.addActivity(activityVO);

                  NoteVO nVo1 = new NoteVO();
                  nVo1.setName(activityVO.getName());
                  nVo1.setTimestamp(activityVO.getStartDate());
                  nVo1.setOrigSrcPK(activityVO.getConfirmation());
                  nVo1.setRank(activityVO.getRank());

                  nVo1.setNote(sanitizeString(act.getDescription(), "<br>"));

                  nVo1.setStreetAddr(accommodations[0].getPosition().getDestination());
                  nVo1.setCity(accommodations[0].getPosition().getRegion());
                  nVo1.setState(accommodations[0].getPosition().getArea());
                  nVo1.setCountry(accommodations[0].getPosition().getCountry());
                  nVo1.setLocLat((float) accommodations[0].getPosition().getLatitude());
                  nVo1.setLocLong((float) accommodations[0].getPosition().getLongitude());

                  noteVOMap.put(activityNum.toString(), nVo1);
                  noteImageMap.put(activityNum.toString(), act.getImages());

                }
              }
            }
          }


          //Get Leg Days Activities
          if (leg.getDays().size() > 0) {
            for (Day days : leg.getDays()) {
              if (days.getActivities() != null && days.getActivities().size() > 0) {

                StringBuilder activitiesContentIds = new StringBuilder();
                for (com.umapped.external.wetu.itinerary.details.Activity a : days.getActivities()) {
                  activitiesContentIds.append(a.getContentEntityId());
                  activitiesContentIds.append(",");
                }
                com.umapped.external.wetu.pins.list.Activity[] activities = wetuExtractor.getActivities(cmd.getToken(), "ids=" + activitiesContentIds.toString());

                if (activities != null && activities.length > 0) {
                  for (com.umapped.external.wetu.itinerary.details.Activity a : days.getActivities()) {

                    for (com.umapped.external.wetu.pins.list.Activity act : activities) {
                      if (act.getMapObjectId() == a.getContentEntityId()) {
                        StringBuilder activityNum = new StringBuilder(bookingNum.toString());
                        activityNum.append("-");
                        activityNum.append(act.getMapObjectId());
                        activityNum.append("-");
                        activityNum.append(days.getDay());
                        ActivityVO activityVO = new ActivityVO();
                        activityVO.setName(act.getName());
                        //activityVO.setSrcId(Long.parseLong(act.getId()));
                        activityVO.setConfirmation(activityNum.toString());
                        activityVO.setRank(tripVO.getActivities().size());
                        StringBuilder note2 = new StringBuilder();
                  /*
                  note2.append("Content Entity ID: ");
                  note2.append(a.getContentEntityId());
                  note2.append("\n");
                  if (a.getReference() != null && a.getReference().length() > 0) {
                    note2.append("Reference: ");
                    note2.append(a.getReference());
                    note2.append("\n");
                  }
                  if (a.getSimpleItemId() != null && a.getSimpleItemId().length() > 0) {
                    note2.append("Simple Item ID: ");
                    note2.append(a.getSimpleItemId());
                    note2.append("\n");
                  }
                  */
                        if (act.getCategory() != null && !act.getCategory().isEmpty()) {
                          note2.append("Category: ");
                          note2.append(act.getCategory());
                          note2.append("\n");
                        }
                        if (act.getSubCategory() != null && !act.getSubCategory().isEmpty()) {
                          note2.append("Sub-Category: ");
                          note2.append(act.getSubCategory());
                          note2.append("\n");
                        }
                        if (a.getStartTime() != null && a.getStartTime().length() > 0) {
                          note2.append("Start Time: ");
                          note2.append(a.getStartTime());
                          note2.append("\n");
                        }
                        if (a.getDuration() != null && a.getDuration().length() > 0) {
                          note2.append("Duration: ");
                          note2.append(a.getDuration());
                          note2.append("\n");
                        }
                        if (a.getEndTime() != null && a.getEndTime().length() > 0) {
                          note2.append("End Time: ");
                          note2.append(a.getEndTime());
                          note2.append("\n");
                        }
                  /*
                  if (a.getTimeSlot() != null && a.getTimeSlot().length() > 0) {
                    note2.append("Time Slot: ");
                    note2.append(a.getTimeSlot());
                    note2.append("\n");
                  }
                  if (a.getType() != null && a.getType().length() > 0) {
                    note2.append("Type: ");
                    note2.append(a.getType());
                    note2.append("\n");
                  }

                  note2.append("Sequence: ");
                  note2.append(a.getSequence());
                  note2.append("\n");
                  */
                        activityVO.setNote(note2.toString());
                        Timestamp activityStartDate = new Timestamp(startTimestamp.getTime() + TimeUnit.DAYS.toMillis(days.getDay()));

                        activityVO.setStartDate(activityStartDate);
                        tripVO.addActivity(activityVO);

                        NoteVO nVo1 = new NoteVO();
                        nVo1.setName(activityVO.getName());
                        nVo1.setTimestamp(activityVO.getStartDate());
                        nVo1.setOrigSrcPK(activityVO.getConfirmation());
                        nVo1.setRank(activityVO.getRank());

                        nVo1.setNote(sanitizeString(act.getContent().getGeneralDescription(), "<br>"));

                        nVo1.setStreetAddr(act.getPosition().getDestination());
                        nVo1.setCity(act.getPosition().getRegion());
                        nVo1.setState(act.getPosition().getArea());
                        nVo1.setCountry(act.getPosition().getCountry());
                        nVo1.setLocLat((float) act.getPosition().getLatitude());
                        nVo1.setLocLong((float) act.getPosition().getLongitude());

                        noteVOMap.put(activityNum.toString(), nVo1);
                        noteImageMap.put(activityNum.toString(), act.getContent().getImages());

                      }
                    }
                  }
                }
              }
            }
          }

          if (destinationContentId != 0 && !destinations.contains(destinationContentId)) {
            //get the destination content
            com.umapped.external.wetu.pins.list.Destination destination = wetuExtractor.getDestination(cmd.getToken(), "ids=" + String.valueOf(destinationContentId));
            if (destination != null) {
              String notePK = String.valueOf(destinationContentId);

              NoteVO dVo = new NoteVO();
              dVo.setName(destination.getName());
              dVo.setTimestamp(startTimestamp);
              dVo.setOrigSrcPK(notePK);
              dVo.setRank(hotel.getRank());
              dVo.setRecordLocator(hotel.getRecordLocator());
              if (destination.getContent().getExtendedDescription() != null && !destination.getContent().getExtendedDescription().isEmpty()) {

                dVo.setNote(sanitizeString(destination.getContent().getExtendedDescription(), "<br>"));
              } else if (destination.getContent().getGeneralDescription() != null && !destination.getContent().getGeneralDescription().isEmpty()) {
                dVo.setNote(sanitizeString(destination.getContent().getGeneralDescription(), "<br>"));
              }
              dVo.setCity(destination.getPosition().getRegion());
              dVo.setState(destination.getPosition().getArea());
              dVo.setCountry(destination.getPosition().getCountry());
              dVo.setLocLat((float) destination.getPosition().getLatitude());
              dVo.setLocLong((float) destination.getPosition().getLongitude());

              if (dVo.getNote() != null && !dVo.getNote().isEmpty()) {
                List<NoteVO> destinationNotes = destinationContentNoteVOMap.get(bookingNum);
                if (destinationNotes == null) {
                  destinationNotes = new ArrayList<>();
                }
                destinationNotes.add(dVo);
                destinationContentNoteVOMap.put(bookingNum.toString(), destinationNotes);
                noteImageMap.put(notePK, destination.getContent().getImages());
              }
              destinations.add(destinationContentId);
              if (destination.getPosition().getCountryContentId() != 0 && !countries.contains(destination.getPosition().getCountryContentId())) {
                countries.add(destination.getPosition().getCountryContentId());
              }
            }
          }

          lastStartTime = startTimestamp;
          startTimestamp = checkoutDate;
        }
      }
      else {
        if (leg.getDays().size() > 0) {
          for (Day days : leg.getDays()) {
            NoteVO note = new NoteVO();
            note.setName(leg.getType());
            note.setNote(sanitizeString(days.getNotes(), "<br>"));
            if (leg.getType() != null && leg.getType().contains("Departure")) {
              note.setTimestamp(startTimestamp);
            } else {
              note.setTimestamp(initStartTime);
            }
            tripVO.addNoteVO(note);
          }
        }
      }
    }



    //First Get all the start and end contentIds for all routes to be passed as Ids to Wetu API
    Set<String> routesContentIdSet = new HashSet();
    for (Route route : itineraryDetail.getRoutes()) {
      if (route.getStartContentEntityId() > -1) {
        routesContentIdSet.add(String.valueOf(route.getStartContentEntityId()));
      }
      if (route.getEndContentEntityId() > -1) {
        routesContentIdSet.add(String.valueOf(route.getEndContentEntityId()));
      }

    }
    String routesContentIds = StringUtils.join(routesContentIdSet, ',');
    Accommodation[] pins = wetuExtractor.getAccommodations(cmd.getToken(), "ids="+routesContentIds);



    //Now Get All Routes (Transfers and Flights) Info
    for (Route route : itineraryDetail.getRoutes()) {
      if (route.getMode().equals("Transfer") || route.getMode().equals("Selfdrive")
          || route.getMode().equals("Train") || route.getMode().equals("Boat")
          || route.getMode().equals("Hike") || route.getMode().equals("Helicopter") || route.getMode().equals("Other")) {
        //Make confirmation num a composite of start & end content entity Ids.
        StringBuilder transferNum = new StringBuilder();
        transferNum.append(route.getStartContentEntityId());
        transferNum.append("-");
        transferNum.append(route.getEndContentEntityId());

        TransportVO transportVO = new TransportVO();
        StringBuilder title = new StringBuilder();
        title.append(route.getMode());
        title.append(" - ");
        title.append(route.getType());
        transportVO.setName(title.toString());
        if (route.getAgency() != null || !route.getAgency().isEmpty()) {
          transportVO.setCmpyName(route.getAgency());
        } else {
          transportVO.setCmpyName("N/A");
        }
        transportVO.setConfirmationNumber(transferNum.toString());
        transportVO.setRank(tripVO.getTransport().size());

        if (route.getMode().equals("Train")) {
          transportVO.setBookingType(ReservationType.RAIL);
        } else if (route.getMode().equals("Boat")) {
          transportVO.setBookingType(ReservationType.FERRY);
        } else if (route.getMode().equals("Selfdrive")) {
          transportVO.setBookingType(ReservationType.TRANSPORT);
        } else {
          transportVO.setBookingType(ReservationType.PRIVATE_TRANSFER);
        }

        Timestamp transferStartTimestamp = null;
        Timestamp transferEndTimestamp = null;

        for (Pin pin : pins) {
          if (pin.getMapObjectId() == route.getStartContentEntityId()) {
            transportVO.setPickupLocation(pin.getName());
          }
          if (pin.getMapObjectId() == route.getEndContentEntityId()) {
            transportVO.setDropoffLocation(pin.getName());
          }
        }

        String startDate = route.getStart();
        if (startDate != null && !startDate.isEmpty()) {
          transferStartTimestamp = new Timestamp(dateUtils.parseDate(startDate, DATE_TIME_FORMATS).getTime());
          if (route.getStartTime() != null && !route.getStartTime().isEmpty()) {
            Timestamp startTime = new Timestamp(dateUtils.parseDate(route.getStartTime(), TIME_FORMATS).getTime());
            transferStartTimestamp = new Timestamp(transferStartTimestamp.getTime() + startTime.getTime());
          }
        }
        transportVO.setPickupDate(transferStartTimestamp);

        String endDate = route.getEnd();
        if (endDate != null && !endDate.isEmpty()) {
          transferEndTimestamp = new Timestamp(dateUtils.parseDate(endDate, DATE_TIME_FORMATS).getTime());
          if (route.getEndTime() != null && !route.getEndTime().isEmpty()) {
            Timestamp endTime = new Timestamp(dateUtils.parseDate(route.getEndTime(), TIME_FORMATS).getTime());
            transferEndTimestamp = new Timestamp(transferEndTimestamp.getTime() + endTime.getTime());
          }
        }
        transportVO.setDropoffDate(transferEndTimestamp);
        StringBuilder note = new StringBuilder();


        if (route.getDistance() > 0.0) {
          double distance = route.getDistance() / 1000;
          note.append("Distance: ~ ");
          note.append(Math.round(distance));
          note.append(" km");
          note.append("\n");
        }
        if (route.getDuration() != null && !route.getDuration().isEmpty()) {
          note.append("Duration: ");
          note.append(route.getDuration());
          note.append("\n");
        }

        transportVO.setNote(note.toString());
        tripVO.addTransport(transportVO);

      } else if (route.getMode().equals("ScheduledFlight") || route.getMode().equals("CharterFlight")) {

        if ((route.getAgency() != null && !route.getAgency().isEmpty())
            && (route.getVehicle() != null && !route.getVehicle().isEmpty())
            && (route.getStartContentEntityId() > -1 && route.getEndContentEntityId() > -1)
            && (route.getStart() != null && !route.getStart().isEmpty())
            && (route.getStartTime() != null && !route.getStartTime().isEmpty())
            && (route.getEnd() != null && !route.getEnd().isEmpty())
            && (route.getEndTime() != null && !route.getEndTime().isEmpty())) {

          StringBuilder flightNum = new StringBuilder();
          flightNum.append(route.getStartContentEntityId());
          flightNum.append("-");
          flightNum.append(route.getEndContentEntityId());

          FlightVO flightVO = new FlightVO();
          flightVO.setRank(tripVO.getFlights().size());
          if (route.getVehicle() != null && route.getVehicle().length() > 3) {
            flightVO.setCode(route.getVehicle().substring(0, 2));
            flightVO.setNumber(route.getVehicle().substring(2));
          } else {
            flightVO.setNumber(route.getVehicle());
          }
          flightVO.setName(route.getVehicle());

          Timestamp flightStartTimestamp = null;
          Timestamp flightEndTimestamp = null;

          String startDate = route.getStart();
          if (startDate != null && !startDate.isEmpty()) {
            flightStartTimestamp = new Timestamp(dateUtils.parseDate(startDate, DATE_TIME_FORMATS).getTime());
            if (route.getStartTime() != null && !route.getStartTime().isEmpty()) {
              Timestamp startTime = new Timestamp(dateUtils.parseDate(route.getStartTime(), TIME_FORMATS).getTime());
              flightStartTimestamp = new Timestamp(flightStartTimestamp.getTime() + startTime.getTime());
            }
          }
          flightVO.setDepatureTime(flightStartTimestamp);

          String endDate = route.getEnd();
          if (endDate != null && !endDate.isEmpty()) {
            flightEndTimestamp = new Timestamp(dateUtils.parseDate(endDate, DATE_TIME_FORMATS).getTime());
            if (route.getEndTime() != null && !route.getEndTime().isEmpty()) {
              Timestamp endTime = new Timestamp(dateUtils.parseDate(route.getEndTime(), TIME_FORMATS).getTime());
              flightEndTimestamp = new Timestamp(flightEndTimestamp.getTime() + endTime.getTime());
            }
          }
          flightVO.setArrivalTime(flightEndTimestamp);

          for (Pin pin : pins) {
            if (pin.getMapObjectId() == route.getStartContentEntityId()) {
              AirportVO airportVO = new AirportVO();
              int iataIndex = pin.getName().indexOf('[');
              if (iataIndex > -1) {
                airportVO.setName(pin.getName().substring(0, iataIndex));
                airportVO.setCode(pin.getName().substring(iataIndex + 1, iataIndex + 4));
              } else {
                airportVO.setName(pin.getName());
              }
              airportVO.setCity(pin.getPosition().getArea());
              airportVO.setCountry(pin.getPosition().getCountry());
              airportVO.setTerminal(route.getStartTerminal());
              flightVO.setDepartureAirport(airportVO);
            }
            if (pin.getMapObjectId() == route.getEndContentEntityId()) {
              AirportVO airportVO = new AirportVO();
              int iataIndex = pin.getName().indexOf('[');
              if (iataIndex > -1) {
                airportVO.setName(pin.getName().substring(0, iataIndex));
                airportVO.setCode(pin.getName().substring(iataIndex + 1, iataIndex + 4));
              } else {
                airportVO.setName(pin.getName());
              }
              airportVO.setCity(pin.getPosition().getArea());
              airportVO.setCountry(pin.getPosition().getCountry());
              airportVO.setTerminal(route.getStartTerminal());
              flightVO.setArrivalAirport(airportVO);
            }
          }
          tripVO.addFlight(flightVO);

        } else {
          //Make confirmation num a composite of start & end content entity Ids.
          StringBuilder transferNum = new StringBuilder();
          transferNum.append(route.getStartContentEntityId());
          transferNum.append("-");
          transferNum.append(route.getEndContentEntityId());

          TransportVO transportVO = new TransportVO();
          StringBuilder title = new StringBuilder();
          title.append(route.getMode());
          title.append(" - ");
          title.append(route.getType());
          transportVO.setName(title.toString());
          if (route.getAgency() != null || !route.getAgency().isEmpty()) {
            transportVO.setCmpyName(route.getAgency());
          } else {
            transportVO.setCmpyName("N/A");
          }
          transportVO.setConfirmationNumber(transferNum.toString());
          transportVO.setRank(tripVO.getTransport().size());

          transportVO.setBookingType(ReservationType.PRIVATE_TRANSFER);

          Timestamp transferStartTimestamp = null;
          Timestamp transferEndTimestamp = null;

          for (Pin pin : pins) {
            if (pin.getMapObjectId() == route.getStartContentEntityId()) {
              transportVO.setPickupLocation(pin.getName());
            }
            if (pin.getMapObjectId() == route.getEndContentEntityId()) {
              transportVO.setDropoffLocation(pin.getName());
            }
          }

          String startDate = route.getStart();
          if (startDate != null && !startDate.isEmpty()) {
            transferStartTimestamp = new Timestamp(dateUtils.parseDate(startDate, DATE_TIME_FORMATS).getTime());
            if (route.getStartTime() != null && !route.getStartTime().isEmpty()) {
              Timestamp startTime = new Timestamp(dateUtils.parseDate(route.getStartTime(), TIME_FORMATS).getTime());
              transferStartTimestamp = new Timestamp(transferStartTimestamp.getTime() + startTime.getTime());
            }
          }
          transportVO.setPickupDate(transferStartTimestamp);

          String endDate = route.getEnd();
          if (endDate != null && !endDate.isEmpty()) {
            transferEndTimestamp = new Timestamp(dateUtils.parseDate(endDate, DATE_TIME_FORMATS).getTime());
            if (route.getEndTime() != null && !route.getEndTime().isEmpty()) {
              Timestamp endTime = new Timestamp(dateUtils.parseDate(route.getEndTime(), TIME_FORMATS).getTime());
              transferEndTimestamp = new Timestamp(transferEndTimestamp.getTime() + endTime.getTime());
            }
          }
          transportVO.setDropoffDate(transferEndTimestamp);
          StringBuilder note = new StringBuilder();

          if (route.getDistance() > 0.0) {
            double distance = route.getDistance() / 1000;
            note.append("Approximate Distance: ");
            note.append(distance);
            note.append("\n");
          }
          if (route.getDuration() != null && !route.getDuration().isEmpty()) {
            note.append("Duration: ");
            note.append(route.getDuration());
            note.append("\n");
          }
          transportVO.setNote(note.toString());
          tripVO.addTransport(transportVO);
        }
      }
    }


    //get all the country info
    if (countries != null && countries.size() > 0) {
      int rank = 99;
      for (Integer countryId : countries) {
        com.umapped.external.wetu.pins.list.Country country = wetuExtractor.getCountry(cmd.getToken(), "ids=" + String.valueOf(countryId));
        if (country != null) {
          String notePK = String.valueOf(countryId);

          NoteVO dVo = new NoteVO();
          dVo.setName(country.getName());
          dVo.setTimestamp(startTimestamp);
          dVo.setOrigSrcPK(notePK);
          dVo.setRank(rank++);

          StringBuilder sb = new StringBuilder();

          if (country.getContent().getExtendedDescription() != null && !country.getContent().getExtendedDescription().isEmpty()) {
            sb.append(sanitizeString(country.getContent().getExtendedDescription(), "<br>"));
          } else if (country.getContent().getGeneralDescription() != null && !country.getContent().getGeneralDescription().isEmpty()) {
            sb.append(sanitizeString(country.getContent().getGeneralDescription(), "<br>"));
          }
          sb.append("<br>");

          if (country.getTravelInformation() != null) {
            if (country.getTravelInformation().getVisa() != null && !country.getTravelInformation().getVisa().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getVisa());
            }
            if (country.getTravelInformation().getBanking() != null && !country.getTravelInformation().getBanking().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getBanking());
            }
            if (country.getTravelInformation().getTransport() != null && !country.getTravelInformation().getTransport().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getTransport());
            }
            if (country.getTravelInformation().getHealth() != null && !country.getTravelInformation().getHealth().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getHealth());
            }
            if (country.getTravelInformation().getSafety() != null && !country.getTravelInformation().getSafety().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getSafety());
            }
            if (country.getTravelInformation().getCuisine() != null && !country.getTravelInformation().getCuisine().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getCuisine());
            }
            if (country.getTravelInformation().getClimate() != null && !country.getTravelInformation().getClimate().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getClimate());
            }
            if (country.getTravelInformation().getDress() != null && !country.getTravelInformation().getDress().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getDress());
            }
            if (country.getTravelInformation().getElectricity() != null && !country.getTravelInformation().getElectricity().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getElectricity());
            }
            if (country.getTravelInformation().getAdditionalInfo() != null && !country.getTravelInformation().getAdditionalInfo().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getAdditionalInfo());
            }
          }

          dVo.setNote(sb.toString());
          if (dVo.getNote() != null && !dVo.getNote().isEmpty()) {
            noteImageMap.put(notePK, country.getContent().getImages());
            countryContentNoteVOMap.put(notePK, dVo);
          }
        }
      }
    }


    Trip tripModel = null;
    boolean isNewTrip = false;
    UserProfile up = UserProfile.findByPK(cmd.getUserId());
    Account account = Account.findActiveByLegacyId(cmd.getUserId());

    try {
      if (cmd.getTrip() != null) {
        tripModel = cmd.getTrip();
      }
      else {
        Log.log(LogLevel.ERROR, "Something Wrong - Cannot find exisiting Trip!!");
        CacheMgr.set(APPConstants.CACHE_WETU_ITINERARY + cmd.getItineraryId() + cmd.getUserId(), "Error importing itinerary - please contact Umapped support");
        return;
      }
      VOModeller modeller = new VOModeller(tripModel, up, isNewTrip);
      modeller.buildTripVOModels(tripVO);
      modeller.saveAllModels();
    }catch (Exception e) {
      Log.log(LogLevel.ERROR, "VOModeller Error: " , e);
      e.printStackTrace();
      CacheMgr.set(APPConstants.CACHE_WETU_ITINERARY + cmd.getItineraryId() + cmd.getUserId(), "Error importing itinerary - please contact Umapped support");
      return;
    }


    //Second Pass: Adding all the contents as Notes for respective Trip Details
    TripVO tripVO2 = new TripVO();

    List<TripDetail> tripDetailList = TripDetail.findActiveByTripId(cmd.getTrip().getTripid());


    for (TripDetail tripDetail : tripDetailList) {
      NoteVO nVO = noteVOMap.get(tripDetail.getBookingnumber());
      if (nVO != null) {
        nVO.setTripDetailId(tripDetail.getDetailsid());
        tripVO2.addNoteVO(nVO);
      }
      List<NoteVO> destinationNotes = destinationContentNoteVOMap.get(tripDetail.getBookingnumber());
      if (destinationNotes != null) {
        for (NoteVO noteVO: destinationNotes) {
          noteVO.setTripDetailId(tripDetail.getDetailsid());
          tripVO2.addNoteVO(noteVO);
        }
      }
    }

    if (countryContentNoteVOMap.size() > 0) {
      //create all destination and country content notes
      for (NoteVO noteVO: countryContentNoteVOMap.values()) {
        tripVO2.addNoteVO(noteVO);
      }
    }

    isNewTrip = false;
    try {
      VOModeller modeller = new VOModeller(tripModel, up, isNewTrip);
      modeller.buildTripVOModels(tripVO2);
      modeller.saveAllModels();
    }catch (Exception e) {
      Log.log(LogLevel.ERROR, "VOModeller Error: " , e);
      e.printStackTrace();
      CacheMgr.set(APPConstants.CACHE_WETU_ITINERARY + cmd.getItineraryId() + cmd.getUserId(), "Error importing itinerary - please contact Umapped support");
      return;
    }


    //Attach Images to respective Notes

    try {

      long currentTime = System.currentTimeMillis();
      for (TripDetail tripDetail : tripDetailList) {
        try {
          if (noteVOMap != null && tripDetail.getBookingnumber() != null && noteVOMap.containsKey(tripDetail
              .getBookingnumber())) {
            String noteName = noteVOMap.get(tripDetail.getBookingnumber()).getName();
            List<TripNote> tripNotes = TripNote.getNotesByTripIdNameDetailId(cmd.getTrip().getTripid(),
                noteName,
                tripDetail.getDetailsid());
            if (tripNotes != null && tripNotes.size() > 0) {
              TripNote tn = tripNotes.get(0);
              if (noteImageMap.get(tripDetail.getBookingnumber()) != null && noteImageMap.get(tripDetail.getBookingnumber())

                  .size() > 0) {
                int max = 5;
                if (noteImageMap.get(tripDetail.getBookingnumber()) != null && noteImageMap.get(tripDetail.getBookingnumber())
                    .size() < 5) {
                  max = noteImageMap.get(tripDetail.getBookingnumber()).size();
                }

                //get existing images so we do not duplicate them when the trip is re-imported
                List<TripNoteAttach> attachments = TripNoteAttach.findByNoteId(tn.getNoteId());
                List<String> existingUrls = new ArrayList<>();
                if (attachments != null) {
                  for (TripNoteAttach attach : attachments) {
                    if (attach.getImageUrl() != null) {
                      existingUrls.add(attach.getImageUrl());
                    }
                  }
                }


                if (noteImageMap.containsKey(tripDetail.getBookingnumber())) {
                  List<Image> images = noteImageMap.get(tripDetail.getBookingnumber());
                  attachImages(images, account, existingUrls, tn);
                }
              }
            }
          }
        }
        catch (Exception e){
          e.printStackTrace();
          Log.log(LogLevel.ERROR, "Add to Image Library Error : " , e);
        }
      }

      //add destination and country images
      if (destinationContentNoteVOMap.size() > 0) {
        for (String key: destinationContentNoteVOMap.keySet()) {
          for (NoteVO noteVO: destinationContentNoteVOMap.get(key)) {
            TripNote note = TripNote.getNotesByTripIdImportSrcId(cmd.getTrip().getTripid(), noteVO.getOrigSrcPK());
            if (note != null) {
              List<TripNoteAttach> attachments = TripNoteAttach.findByNoteId(note.getNoteId());
              List<String> existingUrls = new ArrayList<>();
              if (attachments != null) {
                for (TripNoteAttach attach : attachments) {
                  if (attach.getImageUrl() != null) {
                    existingUrls.add(attach.getImageUrl());
                  }
                }
              }
              if (noteImageMap.containsKey(noteVO.getOrigSrcPK())) {
                List<Image> images = noteImageMap.get(noteVO.getOrigSrcPK());
                if (images != null && images.size() > 0) {
                  attachImages(images, account, existingUrls, note);
                }
              }
            }
          }
        }
      }


      if (countryContentNoteVOMap.size() > 0) {
        for (String key: countryContentNoteVOMap.keySet()) {
          //get existing images so we do not duplicate them when the trip is re-imported
          TripNote note = TripNote.getNotesByTripIdImportSrcId(cmd.getTrip().getTripid(), key);
          if (note != null) {
            List<TripNoteAttach> attachments = TripNoteAttach.findByNoteId(note.getNoteId());
            List<String> existingUrls = new ArrayList<>();
            if (attachments != null) {
              for (TripNoteAttach attach : attachments) {
                if (attach.getImageUrl() != null) {
                  existingUrls.add(attach.getImageUrl());
                }
              }
            }

            if (noteImageMap.containsKey(key)) {
              List<Image> images = noteImageMap.get(key);
              attachImages(images, account, existingUrls, note);
            }
          }
        }
      }

      if (tripModel != null) {
        BookingNoteController.invalidateCache(tripModel.getTripid());
      }
      CacheMgr.set(APPConstants.CACHE_WETU_ITINERARY + cmd.getItineraryId() + cmd.getUserId(), "Itinerary imported successfully");
      return;

    } catch (Exception e) {
      Log.err("WetuController - failed to save TripNoteAttachments");
      e.printStackTrace();
    }
    CacheMgr.set(APPConstants.CACHE_WETU_ITINERARY + cmd.getItineraryId() + cmd.getUserId(), "Error importing itinerary - please contact Umapped support");
    return;
  }


  //New wetu API
  public void buildWetuTrip2 (Command cmd) throws Exception {
    String identifier = cmd.getItineraryId();

    ItineraryExtractor extractor = new ItineraryExtractor();
    ItineraryDetail itineraryDetail = extractor.extractItineraryDetail(identifier);
    ContentRoot itineraryContents = extractor.extractItineraryContent(identifier);
    Map<String, DayTour> dayTourMap = null;
    Map<Integer, com.umapped.external.wetu.content.Pin> pinMap = null;

    if (itineraryContents != null) {
      if (itineraryContents.getDayTours() != null && itineraryContents.getDayTours().size() > 0) {
        dayTourMap = buildContentDayTourMap(itineraryContents.getDayTours());
      }

      if (itineraryContents.getPins() != null && itineraryContents.getPins().size() > 0) {
        pinMap = buildContentPinMap(itineraryContents.getPins());
      }
    }

    Map<String, NoteVO> noteVOMap = new HashMap<>();
    Map<String, List<com.umapped.external.wetu.content.Image>> noteImageMap = new HashMap<>();
    Map<String, List<NoteVO>> destinationContentNoteVOMap = new HashMap<>();
    Map<String, NoteVO> countryContentNoteVOMap = new HashMap<>();

    WetuExtractor wetuExtractor = new WetuExtractor();
    Timestamp startTimestamp = null;
    Timestamp initStartTime = null;
    Timestamp lastStartTime = null;

    try {
      String startDate = itineraryDetail.getStartDate();
      if (startDate != null && !startDate.isEmpty()) {
        startTimestamp = new Timestamp(dateUtils.parseDate(startDate, DATE_TIME_FORMATS).getTime());
      } else if (cmd.getStartDate() != null) {
        startTimestamp = cmd.getStartDate();
      }

      initStartTime = startTimestamp;
      lastStartTime = startTimestamp;

    }catch (Exception e) {
      Log.log(LogLevel.ERROR, "Time stamp conversion: ", e);
      e.printStackTrace();
      CacheMgr.set(APPConstants.CACHE_WETU_ITINERARY + cmd.getItineraryId() + cmd.getUserId(), "Error importing itinerary - please contact Umapped support");
      return;
    }

    TripVO tripVO = buildTripVO(cmd, itineraryDetail, startTimestamp);

    List<Integer> countries = new ArrayList<>();
    List<Integer> destinations = new ArrayList<>();


    int i = 0;

    //Obtain all the contentIds into a List
    for (Leg leg : itineraryDetail.getLegs()) {

      i++;
      int contentId = leg.getContentEntityId();
      int destinationContentId = leg.getDestinationContentId();

      StringBuilder bookingNum = new StringBuilder();
      bookingNum.append(contentId);
      bookingNum.append("-");
      bookingNum.append(leg.getItineraryLegId());
      bookingNum.append("-");
      bookingNum.append(leg.getSequence());

      if (contentId != 0) {
        com.umapped.external.wetu.content.Pin accommodationPin = pinMap.get(contentId);

        if (accommodationPin != null) {
          HotelVO hotel = buildHotelVO(accommodationPin, leg, itineraryDetail.getIdentifier(), bookingNum.toString(), startTimestamp, tripVO.getHotels().size());
          Timestamp checkoutDate = hotel.getCheckout();
          tripVO.addHotel(hotel);

          NoteVO nVo = new NoteVO();
          nVo.setName(accommodationPin.getName());
          nVo.setTimestamp(startTimestamp);
          nVo.setOrigSrcPK(bookingNum.toString());
          nVo.setRank(hotel.getRank());
          nVo.setRecordLocator(hotel.getRecordLocator());
          StringBuilder note1 = new StringBuilder();
          if (accommodationPin.getContent().getGeneralDescription() != null && accommodationPin.getContent().getGeneralDescription().length() > 0) {
            //          note1.append("General Description: \n");
            note1.append(accommodationPin.getContent().getGeneralDescription());
            note1.append("\n");
          } else if (accommodationPin.getContent().getExtendedDescription() != null && accommodationPin.getContent().getExtendedDescription().length() > 0) {
            //          note1.append("Extended Description: \n");
            note1.append(accommodationPin.getContent().getExtendedDescription());
            note1.append("\n");
          } /*else if (accommodationPin.getContent().getTeaserDescription() != null && accommodationPin.getContent().getTeaserDescription().length() > 0) {
          //          note1.append("Teaser Description: \n");
          note1.append(accommodationPin.getContent().getTeaserDescription());
          note1.append("\n");
        }*/
          if (accommodationPin.getFeatures() != null) {
            if (accommodationPin.getFeatures().getSpecialInterests() != null && accommodationPin.getFeatures().getSpecialInterests().size() > 0) {
              note1.append("Special Interests: ");
              note1.append(accommodationPin.getFeatures().getSpecialInterests().toString().replace("[", "").replace("]", ""));
              note1.append("\n\n");
            }
            if (accommodationPin.getFeatures().getPropertyFacilities() != null && accommodationPin.getFeatures().getPropertyFacilities().size() > 0) {
              note1.append("Properties Facilities: ");
              note1.append(accommodationPin.getFeatures().getPropertyFacilities().toString().replace("[", "").replace("]", ""));
              note1.append("\n\n");
            }
            if (accommodationPin.getFeatures().getSpecialInterests() != null && accommodationPin.getFeatures().getSpecialInterests().size() > 0) {
              note1.append("Room Facilities: ");
              note1.append(accommodationPin.getFeatures().getRoomFacilities().toString().replace("[", "").replace("]", ""));
              note1.append("\n\n");
            }
            if (accommodationPin.getFeatures().getAvailableServices() != null && accommodationPin.getFeatures().getAvailableServices().size() > 0) {
              note1.append("Available Services: ");
              note1.append(accommodationPin.getFeatures().getAvailableServices().toString().replace("[", "").replace("]", ""));
              note1.append("\n\n");
            }
            if (accommodationPin.getFeatures().getActivitiesOnSite() != null && accommodationPin.getFeatures().getActivitiesOnSite().size() > 0) {
              note1.append("Activities On-site: ");
              note1.append(accommodationPin.getFeatures().getActivitiesOnSite().toString().replace("[", "").replace("]", ""));
              note1.append("\n\n");
            }
            if (accommodationPin.getFeatures().getActivitiesOffSite() != null && accommodationPin.getFeatures().getActivitiesOffSite().size() > 0) {
              note1.append("Activities Off-site: ");
              note1.append(accommodationPin.getFeatures().getActivitiesOffSite().toString().replace("[", "").replace("]", ""));
              note1.append("\n\n");
            }
          }
          nVo.setNote(sanitizeString(note1.toString(), "<br>"));
          if (accommodationPin.getPosition() != null) {
            nVo.setStreetAddr(accommodationPin.getPosition().getDestination());
            nVo.setCity(accommodationPin.getPosition().getRegion());
            nVo.setState(accommodationPin.getPosition().getArea());
            nVo.setCountry(accommodationPin.getPosition().getCountry());
            nVo.setLocLat((float) accommodationPin.getPosition().getLatitude());
            nVo.setLocLong((float) accommodationPin.getPosition().getLongitude());
          }
          noteVOMap.put(bookingNum.toString(), nVo);

          noteImageMap.put(bookingNum.toString(), accommodationPin.getContent().getImages());

          //Get Leg Activities
          if (leg.getActivities() != null && leg.getActivities().size() > 0) {
            for (com.umapped.external.wetu.itinerary.details.Activity a : leg.getActivities()) {
              com.umapped.external.wetu.content.Pin legActivity = pinMap.get(a.getContentEntityId());
              if (legActivity != null) {
                StringBuilder activityNum = new StringBuilder(bookingNum.toString());
                activityNum.append("-");
                activityNum.append(a.getContentEntityId());
                activityNum.append("-");
                activityNum.append(a.getSequence());
                ActivityVO activityVO = new ActivityVO();
                activityVO.setName(legActivity.getName());
                //activityVO.setSrcId(Long.parseLong(legActivity.getId()));
                activityVO.setConfirmation(activityNum.toString());
                activityVO.setRank(tripVO.getActivities().size());

                activityVO.setStartDate(startTimestamp);
                activityVO.setEndDate(checkoutDate);
                tripVO.addActivity(activityVO);

                NoteVO nVo1 = new NoteVO();
                nVo1.setName(activityVO.getName());
                nVo1.setTimestamp(activityVO.getStartDate());
                nVo1.setOrigSrcPK(activityVO.getConfirmation());
                nVo1.setRank(activityVO.getRank());

                nVo1.setNote(sanitizeString(legActivity.getContent().getGeneralDescription(), "<br>"));

                nVo1.setStreetAddr(accommodationPin.getPosition().getDestination());
                nVo1.setCity(accommodationPin.getPosition().getRegion());
                nVo1.setState(accommodationPin.getPosition().getArea());
                nVo1.setCountry(accommodationPin.getPosition().getCountry());
                nVo1.setLocLat((float) accommodationPin.getPosition().getLatitude());
                nVo1.setLocLong((float) accommodationPin.getPosition().getLongitude());

                noteVOMap.put(activityNum.toString(), nVo1);
                noteImageMap.put(activityNum.toString(), legActivity.getContent().getImages());

              }
            }
          }


          //Get Leg Days Activities
          if (leg.getDays().size() > 0) {
            for (Day days : leg.getDays()) {
              if (days.getActivities() != null && days.getActivities().size() > 0) {

                for (com.umapped.external.wetu.itinerary.details.Activity a : days.getActivities()) {
                  com.umapped.external.wetu.content.Pin dayActivity = pinMap.get(a.getContentEntityId());

                  if (dayActivity != null) {
                    StringBuilder activityNum = new StringBuilder(bookingNum.toString());
                    activityNum.append("-");
                    activityNum.append(dayActivity.getMapObjectId());
                    activityNum.append("-");
                    activityNum.append(days.getDay());
                    ActivityVO activityVO = new ActivityVO();
                    activityVO.setName(dayActivity.getName());
                    //activityVO.setSrcId(Long.parseLong(dayActivity.getId()));
                    activityVO.setConfirmation(activityNum.toString());
                    activityVO.setRank(tripVO.getActivities().size());
                    StringBuilder note2 = new StringBuilder();

                    if (dayActivity.getCategory() != null && !dayActivity.getCategory().isEmpty()) {
                      note2.append("Category: ");
                      note2.append(dayActivity.getCategory());
                      note2.append("\n");
                    }
                  /*if (dayActivity.getSubCategory() != null && !dayActivity.getSubCategory().isEmpty()) {
                    note2.append("Sub-Category: ");
                    note2.append(dayActivity.getSubCategory());
                    note2.append("\n");
                  }*/
                    if (a.getStartTime() != null && a.getStartTime().length() > 0) {
                      note2.append("Start Time: ");
                      note2.append(a.getStartTime());
                      note2.append("\n");
                    }
                    if (a.getDuration() != null && a.getDuration().length() > 0) {
                      note2.append("Duration: ");
                      note2.append(a.getDuration());
                      note2.append("\n");
                    }
                    if (a.getEndTime() != null && a.getEndTime().length() > 0) {
                      note2.append("End Time: ");
                      note2.append(a.getEndTime());
                      note2.append("\n");
                    }

                    activityVO.setNote(note2.toString());
                    Timestamp activityStartDate = new Timestamp(startTimestamp.getTime() + TimeUnit.DAYS.toMillis(days.getDay()));

                    activityVO.setStartDate(activityStartDate);
                    tripVO.addActivity(activityVO);

                    NoteVO nVo1 = new NoteVO();
                    nVo1.setName(activityVO.getName());
                    nVo1.setTimestamp(activityVO.getStartDate());
                    nVo1.setOrigSrcPK(activityVO.getConfirmation());
                    nVo1.setRank(activityVO.getRank());

                    nVo1.setNote(sanitizeString(dayActivity.getContent().getGeneralDescription(), "<br>"));

                    nVo1.setStreetAddr(dayActivity.getPosition().getDestination());
                    nVo1.setCity(dayActivity.getPosition().getRegion());
                    nVo1.setState(dayActivity.getPosition().getArea());
                    nVo1.setCountry(dayActivity.getPosition().getCountry());
                    nVo1.setLocLat((float) dayActivity.getPosition().getLatitude());
                    nVo1.setLocLong((float) dayActivity.getPosition().getLongitude());

                    noteVOMap.put(activityNum.toString(), nVo1);
                    noteImageMap.put(activityNum.toString(), dayActivity.getContent().getImages());

                  }
                }
              }
            }
          }

          if (destinationContentId != 0 && !destinations.contains(destinationContentId)) {
            //get the destination content
            com.umapped.external.wetu.content.Pin destination = pinMap.get(String.valueOf(destinationContentId));
            if (destination != null) {
              String notePK = String.valueOf(destinationContentId);

              NoteVO dVo = new NoteVO();
              dVo.setName(destination.getName());
              dVo.setTimestamp(startTimestamp);
              dVo.setOrigSrcPK(notePK);
              dVo.setRank(hotel.getRank());
              dVo.setRecordLocator(hotel.getRecordLocator());
              if (destination.getContent().getExtendedDescription() != null && !destination.getContent().getExtendedDescription().isEmpty()) {

                dVo.setNote(sanitizeString(destination.getContent().getExtendedDescription(), "<br>"));
              } else if (destination.getContent().getGeneralDescription() != null && !destination.getContent().getGeneralDescription().isEmpty()) {
                dVo.setNote(sanitizeString(destination.getContent().getGeneralDescription(), "<br>"));
              }
              dVo.setCity(destination.getPosition().getRegion());
              dVo.setState(destination.getPosition().getArea());
              dVo.setCountry(destination.getPosition().getCountry());
              dVo.setLocLat((float) destination.getPosition().getLatitude());
              dVo.setLocLong((float) destination.getPosition().getLongitude());

              if (dVo.getNote() != null && !dVo.getNote().isEmpty()) {
                List<NoteVO> destinationNotes = destinationContentNoteVOMap.get(bookingNum);
                if (destinationNotes == null) {
                  destinationNotes = new ArrayList<>();
                }
                destinationNotes.add(dVo);
                destinationContentNoteVOMap.put(bookingNum.toString(), destinationNotes);
                noteImageMap.put(notePK, destination.getContent().getImages());
              }
              destinations.add(destinationContentId);
              if (destination.getPosition().getCountryContentEntityId() != 0 && !countries.contains(destination.getPosition().getCountryContentEntityId())) {
                countries.add(destination.getPosition().getCountryContentEntityId());
              }
            }
          }

          lastStartTime = startTimestamp;
          startTimestamp = hotel.getCheckout();
        }
      }
      else {
        if (leg.getDays().size() > 0) {
          for (Day days : leg.getDays()) {
            NoteVO note = new NoteVO();
            note.setName(leg.getType());
            note.setNote(sanitizeString(days.getNotes(), "<br>"));
            if (leg.getType() != null && leg.getType().contains("Departure")) {
              note.setTimestamp(startTimestamp);
            } else {
              note.setTimestamp(initStartTime);
            }
            tripVO.addNoteVO(note);
          }
        }
      }
    }


    //Now Get All Routes (Transfers and Flights) Info
    for (Route route : itineraryDetail.getRoutes()) {
      if (route.getMode().equals("Transfer") || route.getMode().equals("Selfdrive")
          || route.getMode().equals("Train") || route.getMode().equals("Boat")
          || route.getMode().equals("Hike") || route.getMode().equals("Helicopter") || route.getMode().equals("Other")) {
        //Make confirmation num a composite of start & end content entity Ids.
        StringBuilder transferNum = new StringBuilder();
        transferNum.append(route.getStartContentEntityId());
        transferNum.append("-");
        transferNum.append(route.getEndContentEntityId());

        TransportVO transportVO = new TransportVO();
        StringBuilder title = new StringBuilder();
        title.append(route.getMode());
        title.append(" - ");
        title.append(route.getType());
        transportVO.setName(title.toString());
        if (route.getAgency() != null || !route.getAgency().isEmpty()) {
          transportVO.setCmpyName(route.getAgency());
        } else {
          transportVO.setCmpyName("N/A");
        }
        transportVO.setConfirmationNumber(transferNum.toString());
        transportVO.setRank(tripVO.getTransport().size());

        if (route.getMode().equals("Train")) {
          transportVO.setBookingType(ReservationType.RAIL);
        } else if (route.getMode().equals("Boat")) {
          transportVO.setBookingType(ReservationType.FERRY);
        } else if (route.getMode().equals("Selfdrive")) {
          transportVO.setBookingType(ReservationType.TRANSPORT);
        } else {
          transportVO.setBookingType(ReservationType.PRIVATE_TRANSFER);
        }

        Timestamp transferStartTimestamp = null;
        Timestamp transferEndTimestamp = null;
        
        if (pinMap.get(route.getStartContentEntityId()) != null) {
          transportVO.setPickupLocation(pinMap.get(route.getStartContentEntityId()).getName());
        }
        if (pinMap.get(route.getEndContentEntityId()) != null) {
          transportVO.setDropoffLocation(pinMap.get(route.getEndContentEntityId()).getName());
        }

        String startDate = route.getStart();
        if (startDate != null && !startDate.isEmpty()) {
          transferStartTimestamp = new Timestamp(dateUtils.parseDate(startDate, DATE_TIME_FORMATS).getTime());
          if (route.getStartTime() != null && !route.getStartTime().isEmpty()) {
            Timestamp startTime = new Timestamp(dateUtils.parseDate(route.getStartTime(), TIME_FORMATS).getTime());
            transferStartTimestamp = new Timestamp(transferStartTimestamp.getTime() + startTime.getTime());
          }
        }
        transportVO.setPickupDate(transferStartTimestamp);

        String endDate = route.getEnd();
        if (endDate != null && !endDate.isEmpty()) {
          transferEndTimestamp = new Timestamp(dateUtils.parseDate(endDate, DATE_TIME_FORMATS).getTime());
          if (route.getEndTime() != null && !route.getEndTime().isEmpty()) {
            Timestamp endTime = new Timestamp(dateUtils.parseDate(route.getEndTime(), TIME_FORMATS).getTime());
            transferEndTimestamp = new Timestamp(transferEndTimestamp.getTime() + endTime.getTime());
          }
        }
        transportVO.setDropoffDate(transferEndTimestamp);
        StringBuilder note = new StringBuilder();


        if (route.getDistance() > 0.0) {
          double distance = route.getDistance() / 1000;
          note.append("Distance: ~ ");
          note.append(Math.round(distance));
          note.append(" km");
          note.append("\n");
        }
        if (route.getDuration() != null && !route.getDuration().isEmpty()) {
          note.append("Duration: ");
          note.append(route.getDuration());
          note.append("\n");
        }

        transportVO.setNote(note.toString());
        tripVO.addTransport(transportVO);

      } else if (route.getMode().equals("ScheduledFlight") || route.getMode().equals("CharterFlight")) {

        if ((route.getAgency() != null && !route.getAgency().isEmpty())
            && (route.getVehicle() != null && !route.getVehicle().isEmpty())
            && (route.getStartContentEntityId() > -1 && route.getEndContentEntityId() > -1)
            && (route.getStart() != null && !route.getStart().isEmpty())
            && (route.getStartTime() != null && !route.getStartTime().isEmpty())
            && (route.getEnd() != null && !route.getEnd().isEmpty())
            && (route.getEndTime() != null && !route.getEndTime().isEmpty())) {

          StringBuilder flightNum = new StringBuilder();
          flightNum.append(route.getStartContentEntityId());
          flightNum.append("-");
          flightNum.append(route.getEndContentEntityId());

          FlightVO flightVO = new FlightVO();
          flightVO.setRank(tripVO.getFlights().size());
          if (route.getVehicle() != null && route.getVehicle().length() > 3) {
            flightVO.setCode(route.getVehicle().substring(0, 2));
            flightVO.setNumber(route.getVehicle().substring(2));
          } else {
            flightVO.setNumber(route.getVehicle());
          }
          flightVO.setName(route.getVehicle());

          Timestamp flightStartTimestamp = null;
          Timestamp flightEndTimestamp = null;

          String startDate = route.getStart();
          if (startDate != null && !startDate.isEmpty()) {
            flightStartTimestamp = new Timestamp(dateUtils.parseDate(startDate, DATE_TIME_FORMATS).getTime());
            if (route.getStartTime() != null && !route.getStartTime().isEmpty()) {
              Timestamp startTime = new Timestamp(dateUtils.parseDate(route.getStartTime(), TIME_FORMATS).getTime());
              flightStartTimestamp = new Timestamp(flightStartTimestamp.getTime() + startTime.getTime());
            }
          }
          flightVO.setDepatureTime(flightStartTimestamp);

          String endDate = route.getEnd();
          if (endDate != null && !endDate.isEmpty()) {
            flightEndTimestamp = new Timestamp(dateUtils.parseDate(endDate, DATE_TIME_FORMATS).getTime());
            if (route.getEndTime() != null && !route.getEndTime().isEmpty()) {
              Timestamp endTime = new Timestamp(dateUtils.parseDate(route.getEndTime(), TIME_FORMATS).getTime());
              flightEndTimestamp = new Timestamp(flightEndTimestamp.getTime() + endTime.getTime());
            }
          }
          flightVO.setArrivalTime(flightEndTimestamp);

          com.umapped.external.wetu.content.Pin startRoute = pinMap.get(route.getStartContentEntityId());
          com.umapped.external.wetu.content.Pin endRoute = pinMap.get(route.getStartContentEntityId());
          if (startRoute != null) {
            AirportVO airportVO = new AirportVO();
            int iataIndex = startRoute.getName().indexOf('[');
            if (iataIndex > -1) {
              airportVO.setName(startRoute.getName().substring(0, iataIndex));
              airportVO.setCode(startRoute.getName().substring(iataIndex + 1, iataIndex + 4));
            } else {
              airportVO.setName(startRoute.getName());
            }
            airportVO.setCity(startRoute.getPosition().getArea());
            airportVO.setCountry(startRoute.getPosition().getCountry());
            airportVO.setTerminal(route.getStartTerminal());
            flightVO.setDepartureAirport(airportVO);
          }
          if (endRoute != null) {
            AirportVO airportVO = new AirportVO();
            int iataIndex = endRoute.getName().indexOf('[');
            if (iataIndex > -1) {
              airportVO.setName(endRoute.getName().substring(0, iataIndex));
              airportVO.setCode(endRoute.getName().substring(iataIndex + 1, iataIndex + 4));
            } else {
              airportVO.setName(endRoute.getName());
            }
            airportVO.setCity(endRoute.getPosition().getArea());
            airportVO.setCountry(endRoute.getPosition().getCountry());
            airportVO.setTerminal(route.getStartTerminal());
            flightVO.setArrivalAirport(airportVO);
          }

          tripVO.addFlight(flightVO);

        } else {
          //Make confirmation num a composite of start & end content entity Ids.
          StringBuilder transferNum = new StringBuilder();
          transferNum.append(route.getStartContentEntityId());
          transferNum.append("-");
          transferNum.append(route.getEndContentEntityId());

          TransportVO transportVO = new TransportVO();
          StringBuilder title = new StringBuilder();
          title.append(route.getMode());
          title.append(" - ");
          title.append(route.getType());
          transportVO.setName(title.toString());
          if (route.getAgency() != null || !route.getAgency().isEmpty()) {
            transportVO.setCmpyName(route.getAgency());
          } else {
            transportVO.setCmpyName("N/A");
          }
          transportVO.setConfirmationNumber(transferNum.toString());
          transportVO.setRank(tripVO.getTransport().size());

          transportVO.setBookingType(ReservationType.PRIVATE_TRANSFER);

          Timestamp transferStartTimestamp = null;
          Timestamp transferEndTimestamp = null;

          if (pinMap.get(route.getStartContentEntityId()) != null) {
            transportVO.setPickupLocation(pinMap.get(route.getStartContentEntityId()).getName());
          }
          if (pinMap.get(route.getEndContentEntityId()) != null) {
            transportVO.setDropoffLocation(pinMap.get(route.getEndContentEntityId()).getName());
          }

          String startDate = route.getStart();
          if (startDate != null && !startDate.isEmpty()) {
            transferStartTimestamp = new Timestamp(dateUtils.parseDate(startDate, DATE_TIME_FORMATS).getTime());
            if (route.getStartTime() != null && !route.getStartTime().isEmpty()) {
              Timestamp startTime = new Timestamp(dateUtils.parseDate(route.getStartTime(), TIME_FORMATS).getTime());
              transferStartTimestamp = new Timestamp(transferStartTimestamp.getTime() + startTime.getTime());
            }
          }
          transportVO.setPickupDate(transferStartTimestamp);

          String endDate = route.getEnd();
          if (endDate != null && !endDate.isEmpty()) {
            transferEndTimestamp = new Timestamp(dateUtils.parseDate(endDate, DATE_TIME_FORMATS).getTime());
            if (route.getEndTime() != null && !route.getEndTime().isEmpty()) {
              Timestamp endTime = new Timestamp(dateUtils.parseDate(route.getEndTime(), TIME_FORMATS).getTime());
              transferEndTimestamp = new Timestamp(transferEndTimestamp.getTime() + endTime.getTime());
            }
          }
          transportVO.setDropoffDate(transferEndTimestamp);
          StringBuilder note = new StringBuilder();

          if (route.getDistance() > 0.0) {
            double distance = route.getDistance() / 1000;
            note.append("Approximate Distance: ");
            note.append(distance);
            note.append("\n");
          }
          if (route.getDuration() != null && !route.getDuration().isEmpty()) {
            note.append("Duration: ");
            note.append(route.getDuration());
            note.append("\n");
          }
          transportVO.setNote(note.toString());
          tripVO.addTransport(transportVO);
        }
      }
    }


    //get all the country info
    if (countries != null && countries.size() > 0) {
      int rank = 99;
      for (Integer countryId : countries) {
        com.umapped.external.wetu.content.Pin country = pinMap.get(countryId);
        if (country != null) {
          String notePK = String.valueOf(countryId);
          NoteVO dVo = new NoteVO();
          dVo.setName(country.getName());
          dVo.setTimestamp(startTimestamp);
          dVo.setOrigSrcPK(notePK);
          dVo.setRank(rank++);

          StringBuilder sb = new StringBuilder();

          if (country.getContent().getExtendedDescription() != null && !country.getContent().getExtendedDescription().isEmpty()) {
            sb.append(sanitizeString(country.getContent().getExtendedDescription(), "<br>"));
          } else if (country.getContent().getGeneralDescription() != null && !country.getContent().getGeneralDescription().isEmpty()) {
            sb.append(sanitizeString(country.getContent().getGeneralDescription(), "<br>"));
          }
          sb.append("<br>");

          if (country.getTravelInformation() != null) {
            if (country.getTravelInformation().getVisa() != null && !country.getTravelInformation().getVisa().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getVisa());
            }
            if (country.getTravelInformation().getBanking() != null && !country.getTravelInformation().getBanking().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getBanking());
            }
            if (country.getTravelInformation().getTransport() != null && !country.getTravelInformation().getTransport().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getTransport());
            }
            if (country.getTravelInformation().getHealth() != null && !country.getTravelInformation().getHealth().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getHealth());
            }
            if (country.getTravelInformation().getSafety() != null && !country.getTravelInformation().getSafety().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getSafety());
            }
            if (country.getTravelInformation().getCuisine() != null && !country.getTravelInformation().getCuisine().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getCuisine());
            }
            if (country.getTravelInformation().getClimate() != null && !country.getTravelInformation().getClimate().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getClimate());
            }
            if (country.getTravelInformation().getDress() != null && !country.getTravelInformation().getDress().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getDress());
            }
            if (country.getTravelInformation().getElectricity() != null && !country.getTravelInformation().getElectricity().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getElectricity());
            }
            if (country.getTravelInformation().getAdditionalInfo() != null && !country.getTravelInformation().getAdditionalInfo().isEmpty()) {
              sb.append("<br>");
              sb.append(country.getTravelInformation().getAdditionalInfo());
            }
          }

          dVo.setNote(sb.toString());
          if (dVo.getNote() != null && !dVo.getNote().isEmpty()) {
            noteImageMap.put(notePK, country.getContent().getImages());
            countryContentNoteVOMap.put(notePK, dVo);
          }
        }
      }
    }


    Trip tripModel = null;
    boolean isNewTrip = false;
    UserProfile up = UserProfile.findByPK(cmd.getUserId());
    Account account = Account.findActiveByLegacyId(cmd.getUserId());

    try {
      if (cmd.getTrip() != null) {
        tripModel = cmd.getTrip();
      }
      else {
        Log.log(LogLevel.ERROR, "Something Wrong - Cannot find exisiting Trip!!");
        CacheMgr.set(APPConstants.CACHE_WETU_ITINERARY + cmd.getItineraryId() + cmd.getUserId(), "Error importing itinerary - please contact Umapped support");
        return;
      }
      VOModeller modeller = new VOModeller(tripModel, up, isNewTrip);
      modeller.buildTripVOModels(tripVO);
      modeller.saveAllModels();
    }catch (Exception e) {
      Log.log(LogLevel.ERROR, "VOModeller Error: " , e);
      e.printStackTrace();
      CacheMgr.set(APPConstants.CACHE_WETU_ITINERARY + cmd.getItineraryId() + cmd.getUserId(), "Error importing itinerary - please contact Umapped support");
      return;
    }


    //Second Pass: Adding all the contents as Notes for respective Trip Details
    TripVO tripVO2 = new TripVO();

    List<TripDetail> tripDetailList = TripDetail.findActiveByTripId(cmd.getTrip().getTripid());


    for (TripDetail tripDetail : tripDetailList) {
      NoteVO nVO = noteVOMap.get(tripDetail.getBookingnumber());
      if (nVO != null) {
        nVO.setTripDetailId(tripDetail.getDetailsid());
        tripVO2.addNoteVO(nVO);
      }
      List<NoteVO> destinationNotes = destinationContentNoteVOMap.get(tripDetail.getBookingnumber());
      if (destinationNotes != null) {
        for (NoteVO noteVO: destinationNotes) {
          noteVO.setTripDetailId(tripDetail.getDetailsid());
          tripVO2.addNoteVO(noteVO);
        }
      }
    }

    if (countryContentNoteVOMap.size() > 0) {
      //create all destination and country content notes
      for (NoteVO noteVO: countryContentNoteVOMap.values()) {
        tripVO2.addNoteVO(noteVO);
      }
    }

    isNewTrip = false;
    try {
      VOModeller modeller = new VOModeller(tripModel, up, isNewTrip);
      modeller.buildTripVOModels(tripVO2);
      modeller.saveAllModels();
    }catch (Exception e) {
      Log.log(LogLevel.ERROR, "VOModeller Error: " , e);
      e.printStackTrace();
      CacheMgr.set(APPConstants.CACHE_WETU_ITINERARY + cmd.getItineraryId() + cmd.getUserId(), "Error importing itinerary - please contact Umapped support");
      return;
    }


    //Attach Images to respective Notes

    try {

      long currentTime = System.currentTimeMillis();
      for (TripDetail tripDetail : tripDetailList) {
        try {
          if (noteVOMap != null && tripDetail.getBookingnumber() != null && noteVOMap.containsKey(tripDetail
              .getBookingnumber())) {
            String noteName = noteVOMap.get(tripDetail.getBookingnumber()).getName();
            List<TripNote> tripNotes = TripNote.getNotesByTripIdNameDetailId(cmd.getTrip().getTripid(),
                noteName,
                tripDetail.getDetailsid());
            if (tripNotes != null && tripNotes.size() > 0) {
              TripNote tn = tripNotes.get(0);
              if (noteImageMap.get(tripDetail.getBookingnumber()) != null && noteImageMap.get(tripDetail.getBookingnumber())

                  .size() > 0) {
                int max = 5;
                if (noteImageMap.get(tripDetail.getBookingnumber()) != null && noteImageMap.get(tripDetail.getBookingnumber())
                    .size() < 5) {
                  max = noteImageMap.get(tripDetail.getBookingnumber()).size();
                }

                //get existing images so we do not duplicate them when the trip is re-imported
                List<TripNoteAttach> attachments = TripNoteAttach.findByNoteId(tn.getNoteId());
                List<String> existingUrls = new ArrayList<>();
                if (attachments != null) {
                  for (TripNoteAttach attach : attachments) {
                    if (attach.getImageUrl() != null) {
                      existingUrls.add(attach.getImageUrl());
                    }
                  }
                }


                if (noteImageMap.containsKey(tripDetail.getBookingnumber())) {
                  List<com.umapped.external.wetu.content.Image> images = noteImageMap.get(tripDetail.getBookingnumber());
                  attachImages2(images, account, existingUrls, tn);
                }
              }
            }
          }
        }
        catch (Exception e){
          e.printStackTrace();
          Log.log(LogLevel.ERROR, "Add to Image Library Error : " , e);
        }
      }

      //add destination and country images
      if (destinationContentNoteVOMap.size() > 0) {
        for (String key: destinationContentNoteVOMap.keySet()) {
          for (NoteVO noteVO: destinationContentNoteVOMap.get(key)) {
            TripNote note = TripNote.getNotesByTripIdImportSrcId(cmd.getTrip().getTripid(), noteVO.getOrigSrcPK());
            if (note != null) {
              List<TripNoteAttach> attachments = TripNoteAttach.findByNoteId(note.getNoteId());
              List<String> existingUrls = new ArrayList<>();
              if (attachments != null) {
                for (TripNoteAttach attach : attachments) {
                  if (attach.getImageUrl() != null) {
                    existingUrls.add(attach.getImageUrl());
                  }
                }
              }
              if (noteImageMap.containsKey(noteVO.getOrigSrcPK())) {
                List<com.umapped.external.wetu.content.Image> images = noteImageMap.get(noteVO.getOrigSrcPK());
                if (images != null && images.size() > 0) {
                  attachImages2(images, account, existingUrls, note);
                }
              }
            }
          }
        }
      }


      if (countryContentNoteVOMap.size() > 0) {
        for (String key: countryContentNoteVOMap.keySet()) {
          //get existing images so we do not duplicate them when the trip is re-imported
          TripNote note = TripNote.getNotesByTripIdImportSrcId(cmd.getTrip().getTripid(), key);
          if (note != null) {
            List<TripNoteAttach> attachments = TripNoteAttach.findByNoteId(note.getNoteId());
            List<String> existingUrls = new ArrayList<>();
            if (attachments != null) {
              for (TripNoteAttach attach : attachments) {
                if (attach.getImageUrl() != null) {
                  existingUrls.add(attach.getImageUrl());
                }
              }
            }

            if (noteImageMap.containsKey(key)) {
              List<com.umapped.external.wetu.content.Image> images = noteImageMap.get(key);
              attachImages2(images, account, existingUrls, note);
            }
          }
        }
      }

      if (tripModel != null) {
        BookingNoteController.invalidateCache(tripModel.getTripid());
      }
      CacheMgr.set(APPConstants.CACHE_WETU_ITINERARY + cmd.getItineraryId() + cmd.getUserId(), "Itinerary imported successfully");
      return;

    } catch (Exception e) {
      Log.err("WetuController - failed to save TripNoteAttachments");
      e.printStackTrace();
    }
    CacheMgr.set(APPConstants.CACHE_WETU_ITINERARY + cmd.getItineraryId() + cmd.getUserId(), "Error importing itinerary - please contact Umapped support");
    return;
  }

  //Helper method to build TripVO
  public TripVO buildTripVO (Command cmd, ItineraryDetail itineraryDetail, Timestamp startTimestamp) {
    TripVO tripVO = null;
    if (cmd != null && itineraryDetail != null) {
      tripVO = new TripVO();
      tripVO.setName(itineraryDetail.getName());
      tripVO.setRecordLocator(cmd.getItineraryId());
      tripVO.setSrc("WETU");
      tripVO.setImportSrcId(cmd.getItineraryId());
      tripVO.setImportTs(System.currentTimeMillis());

      if (itineraryDetail.getTravellers() != null && itineraryDetail.getTravellers().size() > 0) {
        for (Traveller t : itineraryDetail.getTravellers()) {
          PassengerVO passenger = new PassengerVO();
          passenger.setFullName(t.getName());
          tripVO.addPassenger(passenger);
        }
      }

      //Create General TripNot for the Trip to store all generic information
      StringBuilder genTripNotes = new StringBuilder();

      if (itineraryDetail.getReferenceNumber() != null && itineraryDetail.getReferenceNumber().length() > 0) {
        genTripNotes.append("Reference Number: ");
        genTripNotes.append(itineraryDetail.getReferenceNumber());
        genTripNotes.append("\n\n");
      }
      if (itineraryDetail.getPrice() != null && itineraryDetail.getPrice().length() > 0) {
        genTripNotes.append("Price: ");
        genTripNotes.append(itineraryDetail.getPrice());
        genTripNotes.append("\n\n");
      }
      if (itineraryDetail.getPriceIncludes() != null && itineraryDetail.getPriceIncludes().length() > 0) {
        genTripNotes.append("Price Includes: \n");
        genTripNotes.append(itineraryDetail.getPriceIncludes());
        genTripNotes.append("\n\n");
      }
      if (itineraryDetail.getPriceExcludes() != null && itineraryDetail.getPriceExcludes().length() > 0) {
        genTripNotes.append("Excluded: \n");
        genTripNotes.append(itineraryDetail.getPriceExcludes());
        genTripNotes.append("\n\n");
      }

      for (Contact c : itineraryDetail.getContacts()) {
        if (c != null) {
          genTripNotes.append("Company: ");
          genTripNotes.append(c.getCompany());
          genTripNotes.append("\n");
          genTripNotes.append("Telephone: ");
          genTripNotes.append(c.getTelephone());
          genTripNotes.append("\n");
          genTripNotes.append("Contact Person: ");
          genTripNotes.append(c.getContactPerson());
          genTripNotes.append("\n");
          genTripNotes.append("Email: ");
          genTripNotes.append(c.getEmail());
          genTripNotes.append("\n\n");
        }
      }

      NoteVO genNoteVO = new NoteVO();
      genNoteVO.setName("Additional Trip Information");
      genNoteVO.setNote(sanitizeString(genTripNotes.toString(), "<br>"));
      genNoteVO.setTimestamp(startTimestamp);
      genNoteVO.setRank(0);
      tripVO.addNoteVO(genNoteVO);
    }
    return tripVO;
  }

  public HotelVO buildHotelVO (com.umapped.external.wetu.content.Pin pin, Leg leg, String identifier, String bookingNum, Timestamp startTimestamp, Integer rank) {
    HotelVO hotel = new HotelVO();

    hotel.setCheckin(startTimestamp);
    hotel.setHotelName(pin.getName());
    int numNights = leg.getNights();
    Timestamp checkoutDate = new Timestamp(startTimestamp.getTime() + TimeUnit.DAYS.toMillis(numNights));
    hotel.setCheckout(checkoutDate);
    hotel.setConfirmation(bookingNum.toString());
    hotel.setRank(rank);
    //hotel.setSrc("WETU");
    //hotel.setSrcId(leg.getContentEntityId());
    hotel.setRecordLocator(identifier);
    StringBuilder note = new StringBuilder();

    if (leg.getDays().size() > 0 && leg.getDays().get(0).getNotes() != null && leg.getDays().get(0).getNotes().length() > 0) {
      note.append(leg.getDays().get(0).getNotes());
      note.append("\n");
    }
    if (leg.getDays().size() > 0 && leg.getDays().get(0).getIncluded() != null && leg.getDays().get(0).getIncluded().length() > 0) {
      note.append("Included: \n");
      note.append(leg.getDays().get(0).getIncluded());
      note.append("\n");
    }
    hotel.setNote(sanitizeString(note.toString(), "\n"));
    
    return hotel;
  }

  public Map<Integer, com.umapped.external.wetu.content.Pin> buildContentPinMap(List<com.umapped.external.wetu.content.Pin> pins) {
    Map<Integer, com.umapped.external.wetu.content.Pin> pinsMap = new HashMap<>();
    for (com.umapped.external.wetu.content.Pin pin : pins) {
      pinsMap.put(pin.getMapObjectId(), pin);
    }
    return pinsMap;
  }

  public Map<String, DayTour> buildContentDayTourMap(List<DayTour> dayTours) {
    Map<String, DayTour> dayToursMap = new HashMap<>();
    for (DayTour dayTour : dayTours) {
      dayToursMap.put(dayTour.getIdentifier(), dayTour);
    }
    return dayToursMap;
  }

  public String sanitizeString (String s, String linebreak) {
    if (s != null && linebreak != null) {
      if (linebreak.contains("br")) {
        s = s.replace("\n","<br>");
      }
      s = policy.sanitize(s.replace("</p>","</p> " + linebreak));

      return s;
    }
    return "";
  }

  public void attachImages (List<Image> images, Account account, List<String> existingUrls, TripNote note) {
    //we will only download up to 10 imaages at this point
    int i = 0;
    int max = 10;
    for (Image img: images) {
      if (i >= max) {
        break;
      }
      if (img.getUrlFragment() != null && img.getUrl() != null) {
        String filename = img.getUrlFragment().replace('/', '-');
        int imgRank = 0;
        String urlFix = img.getUrl().replaceAll(" ", "%20");
        FileImage image = FileImage.bySourceUrl(urlFix);
        if (image == null) {
          image = ImageController.downloadAndSaveImage(urlFix,
                                                       filename,
                                                       FileSrc.FileSrcType.IMG_VENDOR_WETU,
                                                       null,
                                                       account,
                                                       false);
        }

        if (image != null && !existingUrls.contains(image.getUrl())) {
          TripNoteAttach noteAttach = TripNoteAttach.build(note, account.getLegacyId());
          noteAttach.setFileImage(image);
          noteAttach.setAttachUrl(image.getUrl());
          noteAttach.setAttachName(image.getFile().getFilename());
          noteAttach.setAttachType(PageAttachType.PHOTO_LINK);
          noteAttach.setComments(img.getLabel());
          noteAttach.setName(img.getUrlFragment().substring(img.getUrlFragment().indexOf('/') + 1));
          noteAttach.setRank(imgRank);
          noteAttach.save();
          imgRank++;
        }
        i++;
      }
    }
  }

  public void attachImages2 (List<com.umapped.external.wetu.content.Image> images, Account account, List<String> existingUrls, TripNote note) {
    //we will only download up to 10 imaages at this point
    int i = 0;
    int max = 10;
    for (com.umapped.external.wetu.content.Image img: images) {
      if (i >= max) {
        break;
      }
      if (img.getUrlFragment() != null && img.getUrl() != null) {
        String filename = img.getUrlFragment().replace('/', '-');
        int imgRank = 0;
        String urlFix = img.getUrl().replaceAll(" ", "%20");
        FileImage image = FileImage.bySourceUrl(urlFix);
        if (image == null) {
          image = ImageController.downloadAndSaveImage(urlFix,
              filename,
              FileSrc.FileSrcType.IMG_VENDOR_WETU,
              null,
              account,
              false);
        }

        if (image != null && !existingUrls.contains(image.getUrl())) {
          TripNoteAttach noteAttach = TripNoteAttach.build(note, account.getLegacyId());
          noteAttach.setFileImage(image);
          noteAttach.setAttachUrl(image.getUrl());
          noteAttach.setAttachName(image.getFile().getFilename());
          noteAttach.setAttachType(PageAttachType.PHOTO_LINK);
          noteAttach.setComments(img.getLabel());
          noteAttach.setName(img.getUrlFragment().substring(img.getUrlFragment().indexOf('/') + 1));
          noteAttach.setRank(imgRank);
          noteAttach.save();
          imgRank++;
        }
        i++;
      }
    }
  }
}
