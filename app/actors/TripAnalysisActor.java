package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.google.inject.Inject;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.utils.Log;
import com.umapped.service.offer.OfferService;
import models.publisher.Trip;
import models.publisher.TripAudit;
import models.publisher.TripDetail;
import org.apache.commons.lang.time.StopWatch;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 * Created by twong on 22/08/17.
 */
public class TripAnalysisActor extends UntypedActor {

    private static int timeIntervalSecs = 65; //changes in the past 65 secs

    @Inject
    OfferService offerService;

    @Inject
    CacheMgr cacheMgr;

    public interface Factory {
        public Actor create();
    }

    /**
     * Akka Actor Properties
     */
    public final static Props props = Props.create(TripAnalysisActor.class);

    public static class Command {
        private final Instant runInstant;

        public Command() {
            this.runInstant = Instant.now();
        }

        public Instant getRunInstant() {
            return runInstant;
        }
    }

    @Override
    public void onReceive(Object message)
            throws Exception {
        if (message instanceof TripAnalysisActor.Command) {
            Command cmd = (TripAnalysisActor.Command) message;
            StopWatch sw = new StopWatch();
            sw.start();
            long start = sw.getTime();
            Timestamp runTime = Timestamp.from(cmd.getRunInstant());
            long startTimestamp = cmd.getRunInstant().minusSeconds(timeIntervalSecs).toEpochMilli();
            Log.debug("TripAnalysisActor - start analysis for time: " + runTime);
            //get all trips that have booking modifications in the last period
            List<Trip> tripList = TripAudit.getTripWithBookingsModifiedSince(startTimestamp);
            if (tripList != null && !tripList.isEmpty()) {
                Log.debug("TripAnalysisActor - Number of trips to analyze:" + tripList.size() +
                        " for time: " + runTime);

                for (Trip trip: tripList) {

                    try {
                        List<TripDetail> tripDetails = TripDetail.findActiveByTripId(trip.getTripid());
                        long st = sw.getTime();

                        String s = (String) CacheMgr.get(APPConstants.CACHE_TRIP_ANALYSIS_TRIPID+trip.tripid);
                        if (s == null) {
                            CacheMgr.set(APPConstants.CACHE_TRIP_ANALYSIS_TRIPID+trip.tripid, trip.tripid, APPConstants.CACHE_TRIP_ANALYSIS_EXPIRY_SECS);
                            Log.debug("TripAnalysisActor - Starting to analyze trip:" + trip.getTripid() + " Bookings:" + tripDetails.size() +
                                    " for time: " + runTime);
                            offerService.analyzeTrip(trip, tripDetails);
                            Log.debug("TripAnalysisActor - Done analyzing trip:" + trip.getTripid() + " Bookings:" + tripDetails.size() +
                                    " for time: " + runTime + " Duration: " + (sw.getTime() - st));
                        } else {
                            Log.debug("TripAnalysisActor - Starting to analyze trip:" + trip.getTripid() + " Bookings:" + tripDetails.size() +
                                    " for time: " + runTime);
                        }
                    } catch (Exception e) {
                        Log.err("TripAnalysisActor:analyzeTrip Cannot analyse trip: " + trip, e);

                    } finally {
                        CacheMgr.set(APPConstants.CACHE_TRIP_ANALYSIS_TRIPID+trip.tripid, null, APPConstants.CACHE_TRIP_ANALYSIS_EXPIRY_SECS);
                    }
                }

            }

            Log.debug("TripAnalysisActor - end analysis for time: " + runTime + " Duration: " + (sw.getTime() - start));


        }
    }
}
