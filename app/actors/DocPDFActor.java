package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.google.inject.Inject;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.msg.DocPdfMsg;
import com.mapped.publisher.persistence.TripBrandingMgr;
import com.mapped.publisher.utils.*;
import com.mapped.publisher.view.*;
import com.umapped.persistence.accountprop.PreferenceMgr;
import com.umapped.persistence.accountprop.TripPrefs;
import com.umapped.persistence.enums.ReservationType;
import controllers.*;
import it.innove.play.pdf.PdfGenerator;
import models.publisher.*;
import org.apache.commons.lang3.time.StopWatch;
import play.twirl.api.Html;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by twong on 2014-12-24.
 */
public class DocPDFActor
    extends UntypedActor {

  public interface Factory {
    public Actor create();
  }

  @Inject PreferenceMgr preferenceMgr;

  @Inject PdfGenerator pdfGenerator;

  /**
   * Akka Actor Properties
   */
  public final static Props props = Props.create(DocPDFActor.class);

  private static HashMap<String, java.lang.reflect.Method> cachedCustomTemplates = new HashMap<>();
  private static ArrayList<String> cmpIdsForDefaultTemplate = new ArrayList<>();

  private boolean fontsLoaded = false;

  @Override
  public void onReceive(Object message)
      throws Exception {

    try {
      if (message instanceof DocPdfMsg) {
        StopWatch sw = new StopWatch();
        sw.start();

        if(!fontsLoaded) { //TODO: Hack should make it injectable
          PDFUtils.loadLocalFonts(pdfGenerator);
          fontsLoaded = true;
        }

        DocPdfMsg inMsg = (DocPdfMsg) message;
        String destId = inMsg.destinationId;
        String tripId = inMsg.tripId;
        try {
          List<DestinationType> destinationTypes = DestinationType.findActive();
          HashMap<Integer, String> destinationTypeDesc = new HashMap<>();
          if (destinationTypes != null) {
            for (DestinationType u : destinationTypes) {
              destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());
            }
          }

          boolean mergeBookings = false;

          if (destId != null && destId.length() > 0) {
            //retrieve dest info
            try {
              final DestinationView destView = DestinationController.getDestView(destId, destinationTypeDesc);
              if (destView != null) {
                if (destView.cmpyInfo != null) {
                  destView.display12Hr = destView.cmpyInfo.isDisplay12hrClock();
                }
                Account account = null;
                if (destView.createdById != null) {
                  //check user preferences for overrides
                  account = Account.findActiveByLegacyId(destView.createdById);
                  if (account != null) {
                    TripPrefs prefs = preferenceMgr.getTripPref(account);
                    if (prefs != null) {
                      destView.display12Hr = prefs.isIs12Hr();
                    }
                  }
                }

                //strip all img tags from imported content in the PDF
                if (destView != null) {
                  for (DestinationGuideView page : destView.guides) {
                    if (page.groupedViews != null && page.groupedViews.size() > 0) {
                      for (DestinationGuideView page1 : page.groupedViews) {
                        if (page1.description != null && page1.description.toLowerCase().contains("<img")) {
                          page1.description = page1.description.replaceAll("(?i)<img([\\w\\W]+?)>", "");
                        }
                      }

                    }
                    else {
                      if (page.description != null && page.description.toLowerCase().contains("<img")) {
                        page.description = page.description.replaceAll("(?i)<img([\\w\\W]+?)>", "");
                      }
                    }
                  }
                }
                if (tripId != null) {
                  Trip trip = Trip.findByPK(tripId);
                  if (trip != null && trip.status != APPConstants.STATUS_DELETED) {
                    TripDestination tripDestination = TripDestination.find.byId(tripId);
                    if (tripDestination != null && tripDestination.tripdestid != null &&
                        tripDestination.destinationid.equals(destView.id)) {
                      mergeBookings = tripDestination.isMergebookings();
                    }

                    destView.tripStartDate = trip.getStarttimestamp();
                    destView.tripStartDatePrint = Utils.getDateStringFullPrint(trip.getStarttimestamp());
                    destView.tripEndDate = trip.getEndtimestamp();
                    destView.tripEndDatePrint = Utils.getDateStringFullPrint(trip.getEndtimestamp());
                    destView.tripId = trip.tripid;

                    //destView.agent = UserController.getUserView(trip.createdby);

                    TripPublishHistory tp = TripPublishHistory.getLastPublished(trip.tripid, null);
                    UserProfile up = null;
                    if (tp != null) {
                      destView.agent = UserController.getUserView(tp.createdby);
                      up = UserProfile.findByPK(tp.createdby);
                      //override preferences with the published user
                      if (destView.createdById == null || !tp.createdby.equals(destView.createdById)) {
                        account = Account.findActiveByLegacyId(tp.getCreatedby());
                        TripPrefs prefs = preferenceMgr.getTripPref(account);
                        if (prefs != null) {
                          destView.display12Hr = prefs.isIs12Hr();
                        }
                      }
                    }
                    else {
                      destView.agent = UserController.getUserView(trip.createdby);
                      up = UserProfile.findByPK(trip.createdby);
                    }

                    List<TripBrandingView> brandingViews = TripBrandingMgr.findActiveBrandedCmpies(trip);
                    if (brandingViews != null && brandingViews.size() > 0) {
                      Company c = TripBrandingMgr.getMainContact(brandingViews);
                      destView.cmpyInfo = CompanyController.getCmpyView(c.cmpyid);
                      destView.tripCoBranding = brandingViews;
                      //if this is overwritten by tag for the same company e.g. ski.com
                      TripBrandingView overwritten = TripBrandingMgr.getOverwrittenByTag(brandingViews, trip);
                      if (overwritten != null) {
                        destView.cmpyInfo.cmpyName = overwritten.cmpyName;
                        destView.cmpyInfo.logoName = overwritten.cmpyLogoName;
                        destView.cmpyInfo.logoUrl = Utils.escapeS3Umapped_Prd(overwritten.cmpyLogoUrl);
                        destView.cmpyInfo.address.phone = overwritten.cmpyPhone;
                        destView.cmpyInfo.address.email = overwritten.cmpyEmail;
                        destView.cmpyInfo.address.web = overwritten.cmpyWebsite;
                        destView.agent.email = TripBrandingMgr.getWhieLabelEmail(trip,destView.agent.email); //allow overwrite of email domain for ski.com
                        destView.agent.phone = overwritten.cmpyPhone;
                        destView.agent.mobile = "";
                      }
                      if (destView.cmpyInfo != null && destView.cmpyInfo.address != null) {
                        destView.cmpyInfo.address.phone = TripBrandingMgr.getCompanyExtension(Account.findActiveByLegacyId(up.getUserid()), destView.cmpyInfo.address.phone);
                      }
                    }

                    if (destView.cmpyInfo != null) {
                      destView.display12Hr = destView.cmpyInfo.isDisplay12hrClock();
                    }
                  }

                  if (mergeBookings) {
                    destView.wasPublished = (TripPublishHistory.countPublished(trip.tripid) > 0); //George: toggle for draft B/G in PDF

                    //get all the trip bookings also
                    HashMap<String, List<TripBookingDetailView>> notesForLinkedBookings = new HashMap<>();
                    TripBookingView tripBookingView = new TripBookingView();
                    tripBookingView.lang = inMsg.lang; //TODO: MIG24 - Assuming that user requests in native language
                    List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
                    if (tripDetails != null) {
                      for (TripDetail tripDetail : tripDetails) {
                        TripBookingDetailView bookingDetails = BookingController.buildBookingView(tripId,
                                                                                                  tripDetail,
                                                                                                  true);
                        tripBookingView.addBookingDetailView(tripDetail.getDetailtypeid(), bookingDetails);
                      }
                      destView.tripBookingView = tripBookingView;
                      destView.tripBookingView.notes = BookingNoteController.getAllTripNotesView(trip, null, true);
                    }

                    // figure out all the chronological items
                    destView.chronologicalBookingsPages = new HashMap<>();
                    destView.chronologicalPagesBookings = new ArrayList<>();

                    ArrayList<BookingAndPageView> bookingsPages = new ArrayList<>();

                    //get all pages that have dates
                    for (DestinationGuideView gv : destView.guides) {
                      if (gv.timestamp != null && gv.timestamp > 0 && gv.groupedViews != null && gv.groupedViews.size
                          () > 0) {

                        for (DestinationGuideView gv1 : gv.groupedViews) {
                          BookingAndPageView v = new BookingAndPageView();
                          v.destinationGuideView = gv1;
                          v.timestamp = gv1.timestamp;
                          bookingsPages.add(v);
                        }
                      }
                    }
                    destView.chronologicalPagesBookings = new ArrayList<String>();
                    //get all the bookings that have date
                    List<TripBookingDetailView> allBookings = new ArrayList<TripBookingDetailView>();

                    for (ReservationType type : destView.tripBookingView.bookings.keySet()) {
                      allBookings.addAll(destView.tripBookingView.bookings.get(type));
                    }

                    if (destView.tripBookingView.notes != null) {
                      //add all notes that are not linked to a booking... notes linked to bookings will be added to
                      // the sorted list
                      for (TripBookingDetailView note : destView.tripBookingView.notes) {
                        if (note.linkedTripDetailsId == null || note.linkedTripDetailsId.trim().length() == 0) {
                          allBookings.add(note);
                        }
                        else {
                          List<TripBookingDetailView> linkedNotes = notesForLinkedBookings.get(note.linkedTripDetailsId);
                          if (linkedNotes == null) {
                            linkedNotes = new ArrayList<>();
                          }
                          linkedNotes.add(note);
                          notesForLinkedBookings.put(note.linkedTripDetailsId, linkedNotes);
                        }
                      }
                    }

                    for (TripBookingDetailView b : allBookings) {
                      BookingAndPageView v = new BookingAndPageView();
                      v.tripBookingDetailView = b;
                      v.timestamp = b.startDateMs;
                      bookingsPages.add(v);
                    }

                    //sort all the timestamps and group by date
                    Collections.sort(bookingsPages, BookingAndPageView.BookingAndPageViewComparator);
                    for (BookingAndPageView v : bookingsPages) {
                      String d = Utils.formatDatePrint(v.timestamp);
                      if (!destView.chronologicalPagesBookings.contains(d)) {
                        destView.chronologicalPagesBookings.add(d);
                        ArrayList<BookingAndPageView> l = new ArrayList<BookingAndPageView>();
                        l.add(v);
                        destView.chronologicalBookingsPages.put(d, l);
                      }
                      else {
                        destView.chronologicalBookingsPages.get(d).add(v);
                      }
                    }

                    //now that everything is grouped, go back and add notes after the right bookings/
                    ///after everything is sorted, we go back to add the notes bookings to the right spot
                    if (notesForLinkedBookings.size() > 0) {
                      for (String key : destView.chronologicalBookingsPages.keySet()) {
                        List<BookingAndPageView> newVersion = new ArrayList<>();

                        for (BookingAndPageView view : destView.chronologicalBookingsPages.get(key)) {
                          if (view.tripBookingDetailView != null &&
                              notesForLinkedBookings.containsKey(view.tripBookingDetailView.detailsId)) {
                            //the booking has notes... so add the notes right after the booking
                            newVersion.add(view);
                            for (TripBookingDetailView note : notesForLinkedBookings.get(view.tripBookingDetailView
                                                                                             .detailsId)) {
                              BookingAndPageView v = new BookingAndPageView();
                              v.tripBookingDetailView = note;
                              v.timestamp = view.timestamp;
                              newVersion.add(v);
                            }
                          }
                          else {
                            newVersion.add(view);
                          }

                        }
                        destView.chronologicalBookingsPages.put(key, newVersion);
                      }
                    }
                  }
                }
                CmpyCustomTemplate.TEMPLATE_TYPE customTemplateType1 = CmpyCustomTemplate.TEMPLATE_TYPE.DOC_PDF;
                if (destView.chronologicalPagesBookings != null && destView.chronologicalPagesBookings.size() > 0) {
                  customTemplateType1 = CmpyCustomTemplate.TEMPLATE_TYPE.BOOKINGS_DOC_PDF;
                }

                final CmpyCustomTemplate.TEMPLATE_TYPE customTemplateType = customTemplateType1;
                byte[] pdf = null;
                try {
                  if (!cmpIdsForDefaultTemplate.contains(destView.cmpyId)) {
                    java.lang.reflect.Method render = cachedCustomTemplates.get(destView.cmpyId);
                    if (render == null || inMsg.htmlDebug) {
                      CmpyCustomTemplate customTemplate = CmpyCustomTemplate.findActiveByCmpyType(destView.cmpyId,
                                                                                                  customTemplateType
                                                                                                      .ordinal());
                      if (customTemplate != null &&
                          customTemplate.isfiletemplate &&
                          customTemplate.getPath() != null &&
                          customTemplate.getPath().startsWith("views")) {

                        final Class<?> clazz = Class.forName(customTemplate.getPath());
                        render = clazz.getDeclaredMethod("render", DestinationView.class);
                        cachedCustomTemplates.put(destView.cmpyId, render);
                      }
                    }
                    if (render != null) {
                      Html html = (Html) render.invoke(null, destView);
                      if (inMsg.htmlDebug) {
                        pdf = html.toString().getBytes();
                      }
                      else {
                        pdf = pdfGenerator.toBytes(html, "http://umapped.com"); //TODO: MIG24 VERIFY CORRECTNESS
                      }
                    }

                  }

                  //return PDF.toBytes(views.html.external.icbellagio.document.render(destView));
                }
                catch (Exception e) {
                  e.printStackTrace();
                }
                if (!cmpIdsForDefaultTemplate.contains(destView.cmpyId) && !(cachedCustomTemplates.containsKey
                    (destView.cmpyId))) {
                  cmpIdsForDefaultTemplate.add(destView.cmpyId);
                }
                if (pdf == null) {
                  if (inMsg.htmlDebug) {
                    pdf = views.html.guide.destinationPdf.render(destView).toString().getBytes();
                  }
                  else {

                    pdf = pdfGenerator.toBytes(views.html.guide.destinationPdf.render(destView), "http://umapped.com");//TODO: MIG24 VERIFY CORRECTNESS
                  }
                }
                sw.split();
                long splitTime = sw.getSplitTime();
                sw.unsplit();


                if (pdf != null) {
                  try {
                    S3Util.savePDF(pdf, inMsg.fileName);
                    sw.stop();
                    Log.log(LogLevel.INFO,
                            "DocPDFActor: Time for: " + destId + " Generate: " + splitTime + " S3: " +
                            (sw.getTime() - splitTime) + " Total: " + sw.getTime());
                  }
                  catch (Exception e) {
                    Log.log(LogLevel.ERROR, "DocPDFActor: cannot generate pdf for doc: " + destId, e);
                  }
                }
              }
            }
            catch (Exception e) {
              e.printStackTrace();
            }
          }
        }
        finally {
          //remove cache control
          String cacheKey = destId;
          if (tripId != null) {
            cacheKey += "_" + tripId;
          }
          CacheMgr.set(APPConstants.CACHE_DOC_PDF + cacheKey, null);
        }
      }
      else {
        unhandled(message);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      Log.log(LogLevel.ERROR, "DocPDFActor: cannot generate pdf for doc", e);

      throw e;
    }
  }
}
