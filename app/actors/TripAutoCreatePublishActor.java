package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.google.inject.Inject;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.parse.extractor.booking.email.ReservationsHolder;
import com.mapped.publisher.parse.schemaorg.Organization;
import com.mapped.publisher.parse.schemaorg.Person;
import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import com.mapped.publisher.persistence.RecordStatus;
import com.mapped.publisher.persistence.TripBrandingMgr;
import com.mapped.publisher.persistence.bookingapi.BookingAPIMgr;
import com.mapped.publisher.persistence.bookingapi.BookingRS;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.UmappedEmail;
import com.mapped.publisher.view.AgentView;
import com.mapped.publisher.view.TripPreviewView;
import com.umapped.api.AutoPublishProp;
import com.umapped.helper.TripPublisherHelper;
import com.umapped.helper.UmappedEmailFactory;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.service.offer.OfferService;
import com.umapped.service.offer.UMappedCityLookup;
import controllers.*;
import models.publisher.*;
import play.data.validation.Constraints;
import play.i18n.Lang;
import play.twirl.api.Html;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by twong on 2017-02-23.
 */
public class TripAutoCreatePublishActor
    extends UntypedActor {

  @Inject
  static TripPublisherHelper tripPublisherHelper;
  private static HashMap<String, Method> cachedCustomFullEmailTemplates = new HashMap<>();
  private static ArrayList<String> cachedDefaultFullEmailTemplates = new ArrayList<>();



  public interface Factory {
    public Actor create();
  }

  public final static Props props = Props.create(TripAutoCreatePublishActor.class);


  public static class Command
      implements Serializable {
    private final Account agent;
    private final Company cmpy;
    private final String pnr;
    private final Integer srcId;
    private final String srcCmpyId;
    private final String tripName;
    private final AutoPublishProp autoPublishProp;


    public Command(String tripName, Account agent, Company cmpy, String pnr, Integer srcId, String srcCmpyId, AutoPublishProp autoPublishProp) {
      this.agent = agent;
      this.cmpy = cmpy;
      this.pnr = pnr;
      this.srcId = srcId;
      this.tripName = tripName;
      this.srcCmpyId = srcCmpyId;
      this.autoPublishProp = autoPublishProp;
    }

    public Account getAgent() {
      return agent;
    }

    public Company getCmpy() {
      return cmpy;
    }

    public String getPnr() {
      return pnr;
    }

    public Integer getSrcId() {
      return srcId;
    }

    public String getTripName() {
      return tripName;
    }

    public String getSrcCmpyId () { return srcCmpyId;}

    public AutoPublishProp getAutoPublishProp() {
      return autoPublishProp;
    }
  }

  @Override
  public void onReceive(Object message)
      throws Exception {

    if (message instanceof TripAutoCreatePublishActor.Command) {
      TripAutoCreatePublishActor.Command cmd = (TripAutoCreatePublishActor.Command) message;
      publishSyncTrip(cmd);
    }
  }

  public static boolean publishSyncTrip (TripAutoCreatePublishActor.Command cmd) {
    List<BookingRS> bookingApiRecsList = BookingAPIMgr.findByPnrSrcId(cmd.getPnr(), cmd.getSrcId(), cmd.getCmpy().getCmpyid(), cmd.getSrcCmpyId());
    BookingRS reservationPackage = null;
    ReservationsHolder rh = null;

    if (bookingApiRecsList != null) {
      List<BookingRS> bookingsToImport = new ArrayList<>();
      for (BookingRS rs : bookingApiRecsList) {
        bookingsToImport.add(rs);
        if (rs.getBookingType() == ReservationType.PACKAGE.ordinal()) {
          rh = TripPublisherHelper.parseJson(rs.getData(), BkApiSrc.getName(rs.getSrcId()), rs.pk);

        }
      }
      if (cmd.getTripName() != null) {
        Log.debug("AutoCreatePublishTripActor: Starting auto trip: " + cmd.getCmpy().getName() + " RecLocator: " + cmd.getPnr() + " SrcId: " + cmd.getSrcId());

        List<Trip> trips = Trip.findActiveNameCmpyId(5, cmd.cmpy.getCmpyid(), cmd.getTripName());
        boolean found = false;
        if (trips != null && trips.size() > 0) {
          for (Trip t : trips) {
            if (t.getTag() != null && t.getTag().contains(cmd.getPnr())) {
              //there is already a trip of this name - so no autocreation
              found = true;
              break;
            }
          }
          if (found) {
            Log.err("AutoCreatePublishTripActor: trip already exists: " + cmd.getCmpy().getName() + " RecLocator: " + cmd.getPnr() + " SrcId: " + cmd.getSrcId());
          }
        }

        if (!bookingsToImport.isEmpty() && !found) {
          Trip trip = TripPublisherHelper.createTripFromReservationPackage(cmd.getCmpy(),
                  cmd.getTripName(),
                  bookingsToImport,
                  cmd.getAgent());

          if (trip != null) {
            //now that the trip is created, let's publish it
            //create the publish the records for the async auto republish know what the agent is

            //let's add any specific trip tags to the trip for future processing
            String tripTags = cmd.autoPublishProp.getTripTags();
            if (tripTags.trim().length() > 0) {
              trip.refresh();
              if (trip.getTag() != null) {
                trip.setTag(trip.getTag() + " " + tripTags);
              } else {
                trip.setTag(tripTags);
              }
              trip.save();
            }

            //first let's see if we need to add a cover photo
            try {
              if (cmd.getAutoPublishProp().autoTripCover && trip.getCoverView() == null) {
                String cover = tripPublisherHelper.getCoverPhoto(trip);
                if (cover != null) {
                  FileImage fileImage = ImageController.downloadAndSaveImage(cover, "cover.jpg", FileSrc.FileSrcType.IMG_VENDOR_WIKIPEDIA, "", cmd.getAgent(), false);
                  if (fileImage != null) {
                    trip.refresh();
                    trip.setCoverImage(fileImage);
                    trip.setLastupdatedtimestamp(Instant.now().toEpochMilli());
                    trip.setModifiedby(cmd.getAgent().getLegacyId());
                    trip.save();
                  }
                }
              }
            } catch (Exception e) {
              e.printStackTrace();
            }
            //see if we need to add an trip summary to this
            if (cmd.getAutoPublishProp().autoAddTripSummary && cmd.getAutoPublishProp().summaryTemplate != null && !cmd.getAutoPublishProp().summaryTemplate.isEmpty()) {
              BookingNoteController.processTripSummary(trip, cmd.getCmpy(), cmd.getAgent(), null, cmd.getAutoPublishProp().summaryTemplate, tripPublisherHelper.getOfferService(), tripPublisherHelper.getUmCityLookup());
            }
            if(cmd.getAutoPublishProp().autoAddAfarGuides) {
              AfarController.autoAddAllAfarGuides(trip, cmd.getAgent(), tripPublisherHelper.getOfferService());
            }
            if(cmd.getAutoPublishProp().autoAddWcities) {
              OfferController.autoAddAllWcities(trip, cmd.getAgent(), tripPublisherHelper.getOfferService(), tripPublisherHelper.getOfferProviderRepository());
            }
            UserProfile up = UserProfile.find.byId(cmd.getAgent().getLegacyId());
            try {
              AgentView agentView = new AgentView();
              up.setEmail(TripBrandingMgr.getWhieLabelEmail(trip,
                      up.getEmail())); //allow overwrite of email domain for ski.com);
              Company cmpy = Company.find.byId(trip.cmpyid);
              agentView.agentName = up.firstname + " " + up.lastname;
              TripPublisherHelper.publishTrip(trip,
                      up,
                      trip.tripid,
                      agentView,
                      cmpy,
                      true,
                      true,
                      true,
                      true,
                      rh.reservationPackage.emailNote,
                      null,
                      Lang.forCode("en-US"),
                      true);
            } catch (Exception e) {
              e.printStackTrace();
              Log.err("AutoCreatePublishTripActor: Error publishing the trip: " + trip.tripid, e);
            }


            //now that we have thr trip published, let's see if we need to add traveler and collaborator
            if (rh != null && rh.reservationPackage != null) {
              TripPublisherHelper th = new TripPublisherHelper();
              th.addTravelerAgenttoTrip(rh.reservationPackage, trip, cmd.getCmpy(), cmd.getPnr(), cmd.getSrcId(), cmd.getAutoPublishProp());
            }

            // this is the first publish, then let's see if we need to notify the travel agent
            if (cmd.getAutoPublishProp().notifyAgentFirstPublish) {
              sendNotificationEmail(trip, cmd);
            }
            return true;
          } else {
            Log.err("AutoCreatePublishTripActor: cannot create trip for: " + cmd.getCmpy().getName() + " RecLocator: " + cmd.getPnr() + " SrcId: " + cmd.getSrcId());
          }
        }
      }
    }

    //there is an issue
    if (cmd.getAutoPublishProp().notifyAgentErrorPublish) {
      sendErrorNotificationEmail(cmd);
    }
    return false;
  }

  public static void sendNotificationEmail (Trip trip, Command cmd) {
    if (cmd != null && cmd.getCmpy() != null && cmd.getAutoPublishProp() != null && cmd.getAgent() != null
            && (cmd.getAutoPublishProp().notifyAgentFirstPublish || cmd.getAutoPublishProp().notifyAgentRePublish)) {
      try {
        String subject = "Trip Published Notification - ";

        String emailBody = null;
        if (!cachedDefaultFullEmailTemplates.contains(trip.getCmpyid())) {
          //let's check to see if this company has a custom full email template
          try {
            java.lang.reflect.Method render = cachedCustomFullEmailTemplates.get(trip.getCmpyid());
            if (render == null) {
              render = TripController.getCustomEmailTemplate(trip,
                                                             CmpyCustomTemplate.TEMPLATE_TYPE.FULL_EMAIL_PASSENGER,
                                                             cachedCustomFullEmailTemplates);
            }
            if (render != null) {
              //let's render the full email
              TripPreviewView previewView = WebItineraryController.getFullEmailView(trip);
              play.api.i18n.Lang lang = new play.api.i18n.Lang("en", "US");
              Html html = (Html) render.invoke(null, previewView, lang.code(), false);
              if (html != null) {
                emailBody = html.toString();

              }
            }
          }
          catch (Exception e) {
            Log.err(
                "TripAutoCreatePublishActor:sendNotificationEmail Cannot build custom full email template for tripid: " + trip

                    .getTripid());
          }
        }

        //send an email to the agent
        if (emailBody == null) {
          WebItineraryController webItineraryController = new WebItineraryController();
          emailBody = webItineraryController.buildEmailItineraryBody(trip);
          cachedDefaultFullEmailTemplates.add(trip.getCmpyid());
        }
        if (emailBody != null && !emailBody.isEmpty()) {
          UmappedEmail emailMgr = UmappedEmailFactory.getInstance(cmd.getCmpy().getCmpyid());
          emailMgr.addTo(cmd.getAgent().getEmail());
          emailMgr.withViaFromName("Umapped")
                  .withSubject(subject + trip.getName())
                  .withHtml(emailBody)
                  .withTripId(trip.tripid)
                  .withEmailType(EmailLog.EmailTypes.TRIP_PUBLISH_NOTIFICATION)
                  .withAccountUid(cmd.getAgent().getUid())
                  .buildAndSend();
        }
      } catch (Exception e) {
        Log.err("AutoCreatePublishTripActor: Error notifying agent " + cmd.getAgent().getEmail() + "for trip: " + trip.tripid, e);
      }
    }
  }

  public static void sendErrorNotificationEmail (Command cmd) {
    try {
      String subject = "Trip Publish Error - ";

      //send an email to the agent
      String html = views.html.email.tripPublishError.render(cmd.getTripName(), cmd.getPnr(), cmd.getCmpy().getName()).toString();
      if (html != null && !html.isEmpty()) {
        UmappedEmail emailMgr = UmappedEmailFactory.getInstance(cmd.getCmpy().getCmpyid());
        emailMgr.addTo(cmd.getAgent().getEmail());
        emailMgr.addCc("support@umapped.com");
        emailMgr.withViaFromName("Umapped")
                .withSubject(subject + cmd.getPnr() + cmd.getTripName())
                .withHtml(html)
                .withEmailType(EmailLog.EmailTypes.WARNING)
                .withAccountUid(cmd.getAgent().getUid())
                .buildAndSend();
      }
    } catch (Exception e) {
      Log.err("AutoCreatePublishTripActor: Error notifying agent " + cmd.getAgent().getEmail() + "for trip: " + cmd.getPnr(), e);
    }
  }
}
