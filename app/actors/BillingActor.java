package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.freshbooks.ApiConnection;
import com.freshbooks.ApiException;
import com.freshbooks.model.*;
import com.mapped.common.CoreConstants;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.persistence.cache.BillingStateCO;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.billing.*;
import models.publisher.*;
import org.apache.commons.lang3.time.StopWatch;
import org.joda.time.YearMonth;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Billing system Akka actor implementing variety of billing related activities in the background.
 * Created by surge on 2015-04-14.
 */
public class BillingActor
    extends UntypedActor {

  public interface Factory {
    public Actor create();
  }

  /**
   * Akka Actor Properties
   */
  public final static Props props = Props.create(BillingActor.class);

  private static final long FRESHBOOKS_DEFAULT_CLIENT_ID = 438873l;
  private static final long OVERDUE_TIME_MS = 30l * 24l * 3600l * 1000l;
  public static String INVOICE_MESSAGE = "Thank you for your business!";

  public static enum BillingCommand
      implements Serializable {
    INVOICES_GENERATE,
    INVOICES_PUSH,
    INVOICES_SEND,
    INVOICES_SYNC,
    /**
     * Generate invoice for only one specified user
     */
    INVOICE_GENERATE_USER,
    /**
     * Generate invoice for only one specified company
     */
    INVOICE_GENERATE_CMPY,
    PLANS_SYNC,
    USERS_SYNC_MODIFIED,
    USERS_SYNC_ALL,
    CMPYS_SYNC_MODIFIED,
    CMPYS_SYNC_ALL
  }

  public static class BillingMessage
      implements Serializable {
    public BillingCommand cmd;
    /**
     * Required for all invoices related commands
     */
    private YearMonth period;
    /**
     *
     */
    private String adminUserId;
    /**
     * Either user or company ID
     */
    private String id;

    public BillingMessage(BillingCommand cmd) {
      this.cmd = cmd;
    }

    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public String getAdminUserId() {
      return adminUserId;
    }

    public void setAdminUserId(String adminUserId) {
      this.adminUserId = adminUserId;
    }

    public YearMonth getPeriod() {
      return period;
    }

    public void setPeriod(YearMonth period) {
      this.period = period;
    }

    public void setPeriod(int year, int month) {
      this.period = new YearMonth(year, month);
    }
  }

  /**
   * To be implemented by concrete UntypedActor, this defines the behavior of the
   * UntypedActor.
   *
   * @param message
   */
  @Override public void onReceive(Object message)
      throws Exception {
    if (message instanceof BillingMessage) {
      StopWatch sw = new StopWatch();
      sw.start();
      BillingStateCO billingState = BillingStateCO.getFromCache();
      BillingMessage msg = (BillingMessage) message;
      try {
        switch (msg.cmd) {
          case INVOICES_GENERATE:
            invoicesGenerate(msg, billingState);
            break;
          case INVOICES_PUSH:
            invoicesPush(msg, billingState);
            break;
          case INVOICES_SEND:
            invoicesSend(msg, billingState);
            break;
          case INVOICES_SYNC:
            invoicesSync(msg, billingState);
            break;
          case PLANS_SYNC:
            plansSync(msg, billingState);
            break;
          case USERS_SYNC_ALL:
          case USERS_SYNC_MODIFIED:
            usersSync(msg, billingState);
            break;
          case CMPYS_SYNC_ALL:
          case CMPYS_SYNC_MODIFIED:
            cmpysSync(msg, billingState);
            break;
          case INVOICE_GENERATE_CMPY:
            invoiceGenerateCmpy(msg, billingState);
            break;
          case INVOICE_GENERATE_USER:
            invoiceGenerateUser(msg, billingState);
            break;
        }
      }
      catch (Exception e) {
        billingState.stop();
        Log.err("Error: Something went terribly wrong in Billing actor:", e.getMessage());
        e.printStackTrace();
      }
      sw.stop();
      Log.info("Billing finished command:" + msg.cmd.name() + " in " + sw.getTime() + "ms");
    }
    else {
      unhandled(message);
    }
  }

  private boolean invoiceGenerateCmpy(String cmpyId, long startTs, BillingMessage cmd) {
    boolean invoiceExists = BillingInvoice.invoiceExistsForCmpy(cmpyId, startTs);
    if (invoiceExists) {
      Company cmpy = Company.find.byId(cmpyId);
      Log.err("Skipping invoice generation. Invoice for " +
              cmd.period.getYear() + "-" + cmd.period.getMonthOfYear() +
              " already exists for company: " + cmpy.name);
      return false;
    }

    ReportForCmpyView cmpyReport = BillingActor.buildReportForCmpy(cmpyId, cmd.period);

    if (cmpyReport.totalCost <= 0) {
      return false; //We are skipping 0-rated invoices
    }

    BillingInvoice bi = BillingActor.reportToInvoiceForCmpy(cmpyReport, cmd.getAdminUserId());

    bi.save();
    return true;
  }

  private void invoiceGenerateCmpy(BillingMessage cmd, BillingStateCO state) {
    state.start(BillingStateCO.State.INVOICES_GENERATING, 1);
    long startTs = Utils.getFirstMsOfMonth(cmd.period.getYear(), cmd.period.getMonthOfYear());
    invoiceGenerateCmpy(cmd.getId(), startTs, cmd);
    state.stop();
  }

  private void invoiceGenerateUser(BillingMessage cmd, BillingStateCO state) {
    state.start(BillingStateCO.State.INVOICES_GENERATING, 1);
    long startTs = Utils.getFirstMsOfMonth(cmd.period.getYear(), cmd.period.getMonthOfYear());
    invoiceGenerateUser(cmd.getId(), startTs, cmd);
    state.stop();
  }

  private boolean invoiceGenerateUser(String userId, long startTs, BillingMessage cmd) {
    boolean invoiceExists = BillingInvoice.invoiceExistsForUser(userId, startTs);

    if (invoiceExists) {
      UserProfile user = UserProfile.find.byId(userId);
      Log.err("Skipping invoice generation. Invoice for " +
              cmd.period.getYear() + "-" + cmd.period.getMonthOfYear() +
              " already exists for user: " + user.firstname + " " + user.lastname + "<" + user.userid + ">");
      return false;
    }

    BillingSettingsForUser bsu = BillingSettingsForUser.find.byId(userId);
    if (bsu != null && bsu.getBraintree() != null && bsu.getBraintree().isActive()) {
      UserProfile user = UserProfile.find.byId(userId);
      Log.err("Skipping invoice generation. Invoice for " +
              cmd.period.getYear() + "-" + cmd.period.getMonthOfYear() +
              " is via Credit Card for user: " + user.firstname + " " + user.lastname + "<" + user.userid + ">");
      return false;
    }

    ReportForUserView userReport = BillingActor.buildReportForUser(userId, cmd.period);
    if (userReport.totalCost <= 0) {
      return false; //We are skipping 0-rated invoices
    }

    BillingInvoice bi = BillingActor.reportToInvoiceForUser(userReport, cmd.getAdminUserId());
    bi.save();
    return true;
  }

  private void invoicesGenerate(BillingMessage cmd, BillingStateCO state) {
    StopWatch sw = new StopWatch();
    sw.start();
    int savedCount = 0;
    List<Object> cmpyIds = BillingSettingsForCmpy.find.findIds();
    List<Object> userIds = BillingSettingsForUser.findBillableUserIds();

    state.start(BillingStateCO.State.INVOICES_GENERATING, cmpyIds.size() + userIds.size());
    long startTs = Utils.getFirstMsOfMonth(cmd.period.getYear(), cmd.period.getMonthOfYear());

    //1. Generating invoices for companies
    for (Object oCmpyId : cmpyIds) {
      state.next();
      String cmpyId = (String) oCmpyId;
      if(invoiceGenerateCmpy(cmpyId, startTs, cmd)) {
        savedCount++;
      }
    }

    //2. Generating invoices for users
    for (Object oUserId : userIds) {
      state.next();
      String userId = (String) oUserId;
      if(invoiceGenerateUser(userId, startTs, cmd)) {
        savedCount++;
      }
    }

    state.stop();
    sw.stop();
    Log.info("Generated and saved " + savedCount + " billing Invoices in " + sw.toString());
  }

  private void invoicesPush(BillingMessage cmd, BillingStateCO state) {
    long startTs = Utils.getFirstMsOfMonth(cmd.period.getYear(), cmd.period.getMonthOfYear());
    long finishTs = Utils.getLastMsOfMonth(cmd.period.getYear(), cmd.period.getMonthOfYear());

    ApiConnection conn = getFresbookConnection();
    if (conn == null) {
      Log.err("Invoice Push Failed: Can't establish connection to Freshbooks. Try later.");
      return;
    }

    List<BillingInvoice> invoices = BillingInvoice.getAllBetweenTs(startTs, finishTs);
    state.start(BillingStateCO.State.INVOICES_PUSHING, invoices.size());
    for (BillingInvoice invoice : invoices) {
      state.next();
      if (!(invoice.getState() == BillingInvoice.InvoiceState.LOCAL)) {
        continue;
      }

      Invoice inv = invoice.getFreshbookInvoiceData();
      try {
        Long invId = conn.createInvoice(inv);
        if (invId != null) {
          inv.setId(invId);
          invoice.setFreshbookInvoiceData(inv);
          invoice.setState(BillingInvoice.InvoiceState.PUSHED);
          invoice.markModified(cmd.adminUserId);
          invoice.update();
        }
      }
      catch (IOException ioe) {
        Log.err("Error: (IO) Pushing invoice to Freshbooks :" + invoice.getInvId(), ioe);
        ioe.printStackTrace();
      }
      catch (ApiException ae) {
        Log.err("Error: (API) Pushing invoice to Freshbooks : " + invoice.getInvId(), ae);
        ae.printStackTrace();
      }
    }

    state.stop();

  }

  private void invoicesSend(BillingMessage cmd, BillingStateCO state) {
    long startTs = Utils.getFirstMsOfMonth(cmd.period.getYear(), cmd.period.getMonthOfYear());
    long finishTs = Utils.getLastMsOfMonth(cmd.period.getYear(), cmd.period.getMonthOfYear());

    ApiConnection conn = getFresbookConnection();
    if (conn == null) {
      Log.err("Invoice Push Failed: Can't establish connection to Freshbooks. Try later.");
      return;
    }

    List<BillingInvoice> invoices = BillingInvoice.getSendable(startTs, finishTs);
    state.start(BillingStateCO.State.INVOICES_SENDING, invoices.size());
    for (BillingInvoice invoice : invoices) {
      state.next();
      if (invoice.getState() != BillingInvoice.InvoiceState.PUSHED) {
        continue; //Security measure
      }

      Invoice inv = invoice.getFreshbookInvoiceData();
      try {
        conn.sendInvoiceByEmail(inv.getId());
        invoice.setState(BillingInvoice.InvoiceState.SENT);
        invoice.markModified(cmd.adminUserId);
        invoice.update();
      }
      catch (IOException ioe) {
        Log.err("Error: (IO) Sending invoice via Freshbooks :" + invoice.getInvId(), ioe);
        ioe.printStackTrace();
      }
      catch (ApiException ae) {
        Log.err("Error: (API) Sending invoice via Freshbooks : " + invoice.getInvId(), ae);
        ae.printStackTrace();
      }
    }

    state.stop();
  }

  private void invoicesSync(BillingMessage cmd, BillingStateCO state) {
    long startTs = Utils.getFirstMsOfMonth(cmd.period.getYear(), cmd.period.getMonthOfYear());
    long finishTs = Utils.getLastMsOfMonth(cmd.period.getYear(), cmd.period.getMonthOfYear());

    ApiConnection conn = getFresbookConnection();
    if (conn == null) {
      Log.err("Invoice Push Failed: Can't establish connection to Freshbooks. Try later.");
      return;
    }

    List<BillingInvoice> invoices = BillingInvoice.getAllBetweenTs(startTs, finishTs);
    state.start(BillingStateCO.State.INVOICES_SYNCING, invoices.size());
    for (BillingInvoice invoice : invoices) {
      state.next();
      if (invoice.getState() == BillingInvoice.InvoiceState.LOCAL) {
        continue; //Security measure, can sync only those that were pushed
      }

      Invoice inv = invoice.getFreshbookInvoiceData();
      try {
        Invoice updated = conn.getInvoice(inv.getId());
        invoice.setFreshbookInvoiceData(updated);
        invoice.markModified(cmd.adminUserId);

        switch (updated.getStatus()) {
          case "sent":
            invoice.setState(BillingInvoice.InvoiceState.SENT);
            break;
          case "draft":
            invoice.setState(BillingInvoice.InvoiceState.PUSHED);
            break;
          case "paid":
          case "auto-paid":
            invoice.setState(BillingInvoice.InvoiceState.PAID);
            break;
        }

        long overdueTs = finishTs + OVERDUE_TIME_MS;
        long currentTs = System.currentTimeMillis();
        if (overdueTs < currentTs && invoice.getState() != BillingInvoice.InvoiceState.PAID) {
          invoice.setState(BillingInvoice.InvoiceState.OVERDUE);
        }

        invoice.markModified(cmd.adminUserId);
        invoice.update();
      }
      catch (IOException ioe) {
        Log.err("Error: (IO) Synching invoice via Freshbooks :" + invoice.getInvId(), ioe);
        ioe.printStackTrace();
      }
      catch (ApiException ae) {
        Log.err("Error: (API) Synching invoice via Freshbooks : " + invoice.getInvId(), ae);
        ae.printStackTrace();
      }
    }

    state.stop();
  }

  private void plansSync(BillingMessage cmd, BillingStateCO state) {
    List<BillingPlan> plans = BillingPlan.find.all();
    state.start(BillingStateCO.State.PLANS_SYNCING, plans.size());
    ApiConnection conn = getFresbookConnection();
    if (conn == null) {
      Log.err("Billing Plans Sync Failed: Can't establish connection to Freshbooks. Try later.");
      return;
    }

    for (BillingPlan bp : plans) {
      state.next();
      try {
        Item itm = bp.getFreshbookItemData();
        if (itm == null) { // Need to create  a new one
          itm = new Item();
          updateFreshbookItem(bp, itm);
          Long itemId = conn.createItem(itm);
          itm.setId(itemId);
          bp.setFreshbookItemData(itm);
        }
        else { //Let's sync
          Item freshItem = conn.getItem(itm.getId());
          updateFreshbookItem(bp, freshItem);
          conn.updateItem(freshItem);
          bp.setFreshbookItemData(freshItem);
        }

        bp.markModified(cmd.adminUserId);
        bp.update();
      }
      catch (IOException ioe) {
        Log.err("Error: (IO) Pushing plans to Freshbooks :" + bp.getPlanId(), ioe);
        ioe.printStackTrace();
      }
      catch (ApiException ae) {
        Log.err("Error: (API) Pushing plans to Freshbooks : " + bp.getPlanId(), ae);
        ae.printStackTrace();
      }
    }

    state.stop();
  }

  private void usersSync(BillingMessage msg, BillingStateCO state) {
    List<BillingSettingsForUser> usettings;
    if (msg.cmd == BillingCommand.USERS_SYNC_ALL) {
      usettings = BillingSettingsForUser.findBillableUsers();
    }
    else {
      usettings = BillingSettingsForUser.findBillableOutOfSyncUsers();
    }

    ApiConnection conn = getFresbookConnection();
    if (conn == null) {
      Log.err("Billing Users Sync Failed: Can't establish connection to Freshbooks. Try later.");
      return;
    }

    state.start(BillingStateCO.State.USERS_SYNCING, usettings.size());
    for (BillingSettingsForUser bs : usettings) {
      state.next();
      getFreshbookClientId(bs, conn, msg.adminUserId);
    }

    state.stop();
  }

  private void cmpysSync(BillingMessage msg, BillingStateCO state) {
    List<BillingSettingsForCmpy> csettings;
    if (msg.cmd == BillingCommand.CMPYS_SYNC_ALL) {
      csettings = BillingSettingsForCmpy.find.all();
    }
    else {
      csettings = BillingSettingsForCmpy.findBillableOutOfSyncCmpies();
    }

    ApiConnection conn = getFresbookConnection();
    if (conn == null) {
      Log.err("Billing Cmpies Sync Failed: Can't establish connection to Freshbooks. Try later.");
      return;
    }

    state.start(BillingStateCO.State.CMPYS_SYNCING, csettings.size());
    for (BillingSettingsForCmpy bs : csettings) {
      state.next();
      getFreshbookClientId(bs, conn, msg.adminUserId);
    }

    state.stop();
  }

  public static ReportForCmpyView buildReportForCmpy(String cmpyId, YearMonth period) {
    ReportForCmpyView cv = new ReportForCmpyView();
    cv.cmpy = Company.find.byId(cmpyId);
    cv.bs = BillingSettingsForCmpy.find.byId(cmpyId);
    cv.period = period;

    YearMonth nextPeriod = cv.period.plusMonths(1);
    cv.nextPeriod = nextPeriod;
    long currStartTS = Utils.getFirstMsOfMonth(cv.period.getYear(), cv.period.getMonthOfYear());
    long currFinishTS = Utils.getLastMsOfMonth(cv.period.getYear(), cv.period.getMonthOfYear());
    long nextStartTS = Utils.getFirstMsOfMonth(nextPeriod.getYear(), nextPeriod.getMonthOfYear());
    long nextFinishTS = Utils.getLastMsOfMonth(nextPeriod.getYear(), nextPeriod.getMonthOfYear());

    cv.startTs = currStartTS;
    cv.finishTs = currFinishTS;

    /* 1. Trips that were created by agents that bill the company and that were published during specified time
    interval. */
    List<TripPublishHistory> publishedTrips = TripPublishHistory.getCmpyPublishedTrips(cmpyId,
                                                                                       currStartTS,
                                                                                       currFinishTS);
    /* 2. Build trip billing structure */
    cv.tripsCost = 0.0f;
    for (TripPublishHistory tph : publishedTrips) {
      PublishedTripView ptReport = new PublishedTripView();
      ptReport.trip = Trip.findByPK(tph.tripid);
      ptReport.tph = tph;
      ptReport.createdDate = Utils.getISO8601Date(ptReport.trip.createdtimestamp);
      ptReport.publishDate = Utils.getISO8601Date(tph.createdtimestamp);
      ptReport.createdBy = UserProfile.findByPK(ptReport.trip.createdby);
      ptReport.publishBy = UserProfile.findByPK(tph.createdby);
      ptReport.userSettings = BillingSettingsForUser.find.byId(ptReport.trip.createdby);

      long realStartDate;
      if (cv.bs.getStartTs() > ptReport.userSettings.getStartTs()) {
        realStartDate = cv.bs.getStartTs();
      }
      else {
        realStartDate = ptReport.userSettings.getStartTs();
      }

      if (ptReport.tph.createdtimestamp > realStartDate) {
        switch (ptReport.userSettings.getSchedule().getName()) {
          case TRIP:
            ptReport.cost = ptReport.userSettings.getPlan().getCost();
            break;
          case COMPANY:
            switch (cv.bs.getSchedule().getName()) {
              case TRIP:
                ptReport.cost = cv.bs.getPlan().getCost();
                ptReport.userSettings.setPlan(cv.bs.getPlan()); //For presentation only
                break;
              default:
                break;
            }
            break;
          case AGENT:
            Log.err("WRONG BILLING CONFIGURATION FOR USER: AGENT IS SET AS BILLABLE"); //Can never happen
          default:
            ptReport.cost = 0.0f;
            break;
        }
      }
      else {
        ptReport.cost = 0.0f;
      }

      cv.trips.add(ptReport);
      cv.tripsCost += ptReport.cost;
    }

    /* 3. Billing for all users of the company that are on a monthly/yearly plan (either company's or their own)  */
    cv.usersCost = 0.0f;
    List<BillingSettingsForUser> userBils = BillingSettingsForUser.getCmpyAgents(cmpyId);
    for (BillingSettingsForUser bsu : userBils) {

      BilledUsersView bu = new BilledUsersView();
      bu.bs = bsu;
      bu.user = UserProfile.findByPK(bsu.getUserId());
      bu.cost = 0.0f;
      switch (bsu.getSchedule().getName()) {
        case COMPANY:
          switch (cv.bs.getSchedule().getName()) {
            case YEARLY:
              if ((bsu.getStartTs() > currStartTS && bsu.getStartTs() < currFinishTS) ||
                  (bsu.getStartTs() < currStartTS && Utils.isMonthEqual(bsu.getStartTs(), nextStartTS))) {
                bu.cost = cv.bs.getPlan().getCost();
              }
              break;
            case TRIP:
            case NONE:
              bu.cost = 0.0f; //Trip value should not count here
              break;
            default:
              if (bsu.getStartTs() < currFinishTS && bsu.getRecurrent()) {
                bu.cost = cv.bs.getPlan().getCost();
              }
              break;
          }
          bu.bs.setPlan(cv.bs.getPlan());
          break;
        case MONTHLY:
          if (bsu.getStartTs() < currFinishTS && bsu.getRecurrent()) {
            bu.cost = bsu.getPlan().getCost();
          }
          break;
        case YEARLY: //Year is charged only when months are the same
          if ((bsu.getStartTs() > currStartTS && bsu.getStartTs() < currFinishTS) ||
              (bsu.getStartTs() < currStartTS && Utils.isMonthEqual(bsu.getStartTs(), nextStartTS))) {
            bu.cost = bsu.getPlan().getCost();
          }
          break;
        default:
          break;
      }

      /* 3.a Pro-rated value calculation if the start month is the same month. Yearly not pro-rated */
      if (bsu.getStartTs() > currStartTS && bsu.getStartTs() < currFinishTS &&
          bsu.getSchedule().getName() != BillingSchedule.Type.YEARLY &&
          !(bsu.getSchedule().getName() ==  BillingSchedule.Type.COMPANY &&
           cv.bs.getSchedule().getName() == BillingSchedule.Type.YEARLY)) {
        long totalMs = currFinishTS - currStartTS;
        long chargeMs = currFinishTS - bsu.getStartTs();
        bu.proRatedCost = ((float) chargeMs / totalMs) * bu.cost;
        bu.isProRated = true;
      }

      bu.addons = addonsForUser(bu.bs, bu.user.userid, currStartTS, currFinishTS, nextStartTS, nextFinishTS);
      if(bu.bs.getRecurrent() || bu.bs.getCutoffTs() == null || bu.bs.getCutoffTs() > currFinishTS) {
        for (BilledAddonView av : bu.addons) {
          cv.addonCost += av.cost + av.proRatedCost;
        }
      }

      cv.usersCost += bu.cost + bu.proRatedCost;
      cv.users.add(bu);
    }

    cv.totalCost = cv.tripsCost + cv.usersCost + cv.addonCost;
    return cv;
  }

  private static void reportViewToInvoiceLines(ArrayList<InvoiceLine> lines,
                                               ReportBaseView view) {
    for (BilledUsersView buv : view.users) {
      StringBuilder sb = new StringBuilder();
      InvoiceLine il;


      if (buv.isProRated && buv.proRatedCost > 0) {
        InvoiceLine proratedLine = new InvoiceLine();
        proratedLine.setName(buv.bs.getPlan().getName());
        sb.append("User Plan for: ");
        sb.append(buv.user.firstname);
        sb.append(" ");
        sb.append(buv.user.lastname);
        sb.append("\nProrated for: ");
        sb.append(new DateFormatSymbols().getMonths()[view.period.getMonthOfYear() - 1]);
        proratedLine.setDescription(sb.toString());
        proratedLine.setQuantity(1);
        proratedLine.setUnitCost(Utils.roundTwoDecimals(buv.proRatedCost));
        lines.add(proratedLine);
        sb.setLength(0);
      }

      if(buv.cost > 0) {
        il = new InvoiceLine();
        il.setName(buv.bs.getPlan().getName());
        sb.append("User Plan for: ");
        sb.append(buv.user.firstname);
        sb.append(" ");
        sb.append(buv.user.lastname);
        il.setDescription(sb.toString());
        il.setQuantity(1);
        il.setUnitCost(Utils.roundTwoDecimals(buv.cost));
        lines.add(il);
        sb.setLength(0); //Clearing
      }

      for (BilledAddonView av : buv.addons) {
        if (av.cost + av.proRatedCost <= 0) {
          continue;
        }

        if (av.isProRated && av.proRatedCost > 0) {
          InvoiceLine proratedLine = new InvoiceLine();
          proratedLine.setName(av.addon.getName());
          sb.append("User Addon for: ");
          sb.append(buv.user.firstname);
          sb.append(" ");
          sb.append(buv.user.lastname);
          sb.append("\nProrated for: ");
          sb.append(new DateFormatSymbols().getMonths()[view.period.getMonthOfYear() - 1]);
          proratedLine.setDescription(sb.toString());
          proratedLine.setQuantity(1);
          proratedLine.setUnitCost(Utils.roundTwoDecimals(av.proRatedCost));
          lines.add(proratedLine);
          sb.setLength(0);
        }

        if(av.cost > 0) {
          il = new InvoiceLine();
          il.setName(av.addon.getName());
          sb.append("User Addon for: ");
          sb.append(buv.user.firstname);
          sb.append(" ");
          sb.append(buv.user.lastname);
          il.setDescription(sb.toString());
          il.setQuantity(1);
          il.setUnitCost(Utils.roundTwoDecimals(av.cost));
          lines.add(il);
          sb.setLength(0);
        }
      }
    }

    for (PublishedTripView ptv : view.trips) {
      if (ptv.cost <= 0) {
        continue; //0-rated trips are not included in the
      }
      InvoiceLine il = new InvoiceLine();

      il.setName(ptv.userSettings.getPlan().getName());
      StringBuilder sb = new StringBuilder();
      sb.append("Trip:");
      sb.append(ptv.trip.getName());
      sb.append(" published by:");
      sb.append(ptv.publishBy.firstname + " " + ptv.publishBy.lastname);
      il.setDescription(sb.toString());
      il.setQuantity(1);
      il.setUnitCost(Utils.roundTwoDecimals(ptv.cost));
      lines.add(il);
    }
  }

  public static BillingInvoice reportToInvoiceForCmpy(ReportForCmpyView cv, String adminUserId) {
    BillingInvoice bi = BillingInvoice.buildInvoice(adminUserId);
    Invoice invoice = new Invoice();

    Long clientId = null;
    if (cv.bs.getFreshbookClientData() != null) {
      clientId = cv.bs.getFreshbookClientData().getId();
    }
    if (clientId == null) {
      ApiConnection conn = BillingActor.getFresbookConnection();
      if (conn == null) {
        Log.err("Freshbook Cmpy Sync Failed: Can't establish connection to Freshbooks. Try later.");
      }

      clientId = getFreshbookClientId(cv.bs, conn, adminUserId);
    }

    if (clientId != null) {
      invoice.setClientId(clientId);
    }
    else {
      invoice.setClientId(FRESHBOOKS_DEFAULT_CLIENT_ID);
      invoice.setOrganization(cv.cmpy.getName());
    }

    invoice.setStatus("draft");
    invoice.setCurrencyCode(cv.bs.getPlan().getCurrency().name());

    invoice.setNotes(getInvoiceNotes(cv.period, cv.nextPeriod));

    java.util.Date date = new Date(cv.finishTs);
    invoice.setDate(date);

    ArrayList<InvoiceLine> lines = new ArrayList<>();

    reportViewToInvoiceLines(lines, cv);

    invoice.setLines(lines);

    bi.setStartTs(cv.startTs);
    bi.setEndTs(cv.finishTs);
    bi.setState(BillingInvoice.InvoiceState.LOCAL);
    bi.setCmpyId(cv.cmpy.getCmpyid());
    bi.setAmount(cv.totalCost);

    bi.setFreshbookInvoiceData(invoice);
    return bi;
  }

  public static ReportForUserView buildReportForUser(String userId, YearMonth period) {
    ReportForUserView uv = new ReportForUserView();
    uv.up = UserProfile.findByPK(userId);
    uv.bs = BillingSettingsForUser.find.byId(userId);
    uv.period = period;

    if (uv.bs.getCmpyid() != null && uv.bs.getCmpyid().length() > 0) {
      uv.cmpy = Company.find.byId(uv.bs.getCmpyid());
    }
    else {
      List<UserCmpyLink> ucls = UserCmpyLink.findActiveByUserId(userId);
      if (ucls != null && ucls.size() > 0) {
        uv.cmpy = ucls.get(0).cmpy;
      }
    }

    YearMonth nextPeriod = uv.period.plusMonths(1);
    uv.nextPeriod = nextPeriod;
    long currStartTS  = Utils.getFirstMsOfMonth(uv.period.getYear(), uv.period.getMonthOfYear());
    long currFinishTS = Utils.getLastMsOfMonth(uv.period.getYear(), uv.period.getMonthOfYear());
    long nextStartTS  = Utils.getFirstMsOfMonth(nextPeriod.getYear(), nextPeriod.getMonthOfYear());
    long nextFinishTS = Utils.getLastMsOfMonth(nextPeriod.getYear(), nextPeriod.getMonthOfYear());

    uv.startTs = currStartTS;
    uv.finishTs = currFinishTS;

    /* 1. Trips that were created by agent and all if his/her minions and that were published during specified time
    interval. */
    List<TripPublishHistory> publishedTrips = TripPublishHistory.getAgentAndMinionsPublishedTrips(userId,
                                                                                                  currStartTS,
                                                                                                  currFinishTS);

    /* 2. Build trip billing structure */
    uv.tripsCost = 0.0f;
    for (TripPublishHistory tph : publishedTrips) {
      PublishedTripView ptReport = new PublishedTripView();

      ptReport.trip = Trip.findByPK(tph.tripid);
      ptReport.tph = tph;
      ptReport.createdDate = Utils.getISO8601Date(ptReport.trip.createdtimestamp);
      ptReport.publishDate = Utils.getISO8601Date(tph.createdtimestamp);
      ptReport.createdBy = UserProfile.findByPK(ptReport.trip.createdby);
      ptReport.publishBy = UserProfile.findByPK(tph.createdby);
      ptReport.userSettings = BillingSettingsForUser.find.byId(ptReport.trip.createdby);

      long realStartDate;
      if (uv.bs.getStartTs() > ptReport.userSettings.getStartTs()) {
        realStartDate = uv.bs.getStartTs();
      }
      else {
        realStartDate = ptReport.userSettings.getStartTs();
      }

      if (ptReport.tph.createdtimestamp > realStartDate) {
        switch (ptReport.userSettings.getSchedule().getName()) {
          case TRIP:
            ptReport.cost = ptReport.userSettings.getPlan().getCost();
            break;
          case AGENT:
            switch (uv.bs.getSchedule().getName()) {
              case TRIP:
                ptReport.cost = uv.bs.getPlan().getCost();
                ptReport.userSettings.setPlan(uv.bs.getPlan());
                break;
              default:
                break;
            }
            break;
          case COMPANY:
            Log.err("WRONG BILLING CONFIGURATION FOR USER: COMPANY IS SET AS BILLABLE"); //Can never happen
          default:
            ptReport.cost = 0.0f;
            break;
        }
      }
      else {
        ptReport.cost = 0.0f;
      }

      uv.trips.add(ptReport);
      uv.tripsCost += ptReport.cost;
    }

    /* 3. Billing for main user and all minions who are not on per trip plan */
    uv.usersCost = 0.0f;
    List<BillingSettingsForUser> userBils = BillingSettingsForUser.getAgentAndMinions(userId);
    for (BillingSettingsForUser bsu : userBils) {
      BilledUsersView bu = new BilledUsersView();
      bu.bs = bsu;
      bu.user = UserProfile.findByPK(bsu.getUserId());

      bu.cost = 0.0f;
      switch (bsu.getSchedule().getName()) {
        case MONTHLY:
          if (bsu.getStartTs() < currFinishTS && bsu.getRecurrent()) {
            bu.cost = bsu.getPlan().getCost();
          }
          break;
        case YEARLY: //Year is charged only when *next* month after a billing cycle and start month are the same
          if ((bsu.getStartTs() > currStartTS && bsu.getStartTs() < currFinishTS) ||
              (bsu.getStartTs() < currStartTS && Utils.isMonthEqual(bsu.getStartTs(), nextStartTS))) {
            bu.cost = bsu.getPlan().getCost();
          }
          break;
        case AGENT:
          switch (uv.bs.getSchedule().getName()) {
            case TRIP:
            case NONE:
              break; //No per user charge
            case YEARLY:
              if ((bsu.getStartTs() > currStartTS && bsu.getStartTs() < currFinishTS) ||
                  (bsu.getStartTs() < currStartTS && Utils.isMonthEqual(bsu.getStartTs(), nextStartTS))) {
                bu.cost = uv.bs.getPlan().getCost();
              }
              break;
            default:
              if (bsu.getStartTs() < currFinishTS && bsu.getRecurrent()) {
                if (uv.bs.getPlan() != null) {
                  bu.cost = uv.bs.getPlan().getCost();
                }
                else {
                  Log.err("User's " + bu.user.getUserid() + " billing record is not setup correctly. Plan is missing.");
                }
              }
          }
          bu.bs.setPlan(uv.bs.getPlan()); //This is not being saved!
          break;
        default:
          break;
      }

      /* 3.a Pro-rated value calculation if the start month is the same month. */
      if (bsu.getStartTs() >= currStartTS && bsu.getStartTs() <= currFinishTS &&
          bsu.getSchedule().getName() != BillingSchedule.Type.YEARLY &&
          !(bsu.getSchedule().getName() ==  BillingSchedule.Type.AGENT &&
            uv.bs.getSchedule().getName() == BillingSchedule.Type.YEARLY)) {
        long totalMs = currFinishTS - currStartTS;
        long chargeMs = currFinishTS - bsu.getStartTs();
        //Since monthly service is pro-rated, then day before next month cost are post-paid billed and next is pre-paid
        bu.proRatedCost = ((float) chargeMs / totalMs) * bu.cost;
        bu.isProRated = true;
      }



      bu.addons = addonsForUser(bu.bs, bu.user.userid, currStartTS, currFinishTS, nextStartTS, nextFinishTS);
      if(bu.bs.getRecurrent() || bu.bs.getCutoffTs() == null || bu.bs.getCutoffTs() > currFinishTS) {
        for (BilledAddonView av : bu.addons) {
          uv.addonCost += av.cost + av.proRatedCost;
        }
      }

      uv.usersCost += bu.cost + bu.proRatedCost;
      uv.users.add(bu);
    }

    uv.totalCost = uv.tripsCost + uv.usersCost + uv.addonCost;
    return uv;
  }

  private static List<BilledAddonView> addonsForUser(BillingSettingsForUser bsu,
                                                     String userId,
                                                     Long currStartTS,
                                                     Long currFinishTS,
                                                     Long nextStartTS,
                                                     Long nextFinishTS) {
    List<BilledAddonView> addons = new ArrayList<>();
    List<BillingAddonsForUser> bss = BillingAddonsForUser.getAddonsForUser(userId);
    for(BillingAddonsForUser as : bss) {

      BilledAddonView av = new BilledAddonView();
      av.addon = BillingPlan.find.byId(as.getPk().planId);
      av.state = as;

      if(as.getEnabled()) { //Addon is enabled
        switch (av.addon.getSchedule()) {
          case YEARLY:
            //If we are in the current billing month, let's pro-rate to full year
            if(as.getStartTs() >= currStartTS && as.getStartTs() <= currFinishTS) {
              long nextYearDate = Utils.getNextDateOccurrence(bsu.getStartTs(), currStartTS);
              long totalMs  = nextYearDate - Utils.getCurrentYearDate(bsu.getStartTs(), currStartTS);
              long chargeMs = nextYearDate - as.getStartTs();
              av.proRatedCost = ((float) chargeMs / totalMs) * av.addon.getCost();
              av.isProRated = true;
            }else if(Utils.isMonthEqual(nextStartTS, bsu.getStartTs())) {
              av.cost = av.addon.getCost(); //Full renewal fee on master plan's month's renewal date
            }
            break;
          case MONTHLY:
            if(as.getStartTs() < currStartTS) { //Should have been charged in previous month, so only next month
              av.cost = av.addon.getCost();
            }
            if(as.getStartTs() >= currStartTS && as.getStartTs() <= currFinishTS) { //Needs to be pro-rated
              av.isProRated = true;
              if(as.getCutoffTs() != null && as.getCutoffTs() >= currStartTS && as.getCutoffTs() <= currFinishTS) {
                av.cost = 0;
              } else {
                av.cost = av.addon.getCost();
              }

              long totalMs = currFinishTS - currStartTS;
              long chargeMs = currFinishTS - as.getStartTs();
              av.proRatedCost = ((float) chargeMs / totalMs) * av.addon.getCost();
            }
            //Any start date in the future is ignored
            break;
          default:
            Log.debug("Unspported addon billing schedule: ", av.addon.getSchedule().getScheduleName());
        }
      } else { //Addon is disabled

        switch (av.addon.getSchedule()) {
          case YEARLY:
            //If we are in the current billing month, but then user disabled plugin,
            if(as.getStartTs() != null && as.getStartTs() >= currStartTS && as.getStartTs() <= currFinishTS) {
              long nextYearDate = Utils.getNextDateOccurrence(bsu.getStartTs(), currStartTS);
              long totalMs  = nextYearDate - Utils.getCurrentYearDate(bsu.getStartTs(), currStartTS);
              long chargeMs = nextYearDate - as.getStartTs();
              av.proRatedCost = ((float) chargeMs / totalMs) * av.addon.getCost();
              av.isProRated = true;
            }
            break;
          case MONTHLY:
            //Billing start date is in current month but service is stopped at the end of this month
            if (as.getStartTs() != null &&
                as.getStartTs() >= currStartTS && as.getStartTs() <= currFinishTS) {
              long totalMs  = currFinishTS - currStartTS;
              long chargeMs = currFinishTS - as.getStartTs();
              av.isProRated = true;
              av.proRatedCost = ((float) chargeMs / totalMs) * av.addon.getCost();

              if( as.getCutoffTs() != null && as.getCutoffTs() >= currStartTS && as.getCutoffTs() <= currFinishTS) {
                av.cost = 0; //Cost fhe next month is 0
              }
              if( as.getCutoffTs() != null && as.getCutoffTs() >= nextStartTS && as.getCutoffTs() <= nextFinishTS) {
                av.cost = av.addon.getCost();
              }
            }
            break;
          default:
            Log.debug("Unspported addon billing schedule: ", av.addon.getSchedule().getScheduleName());
        }
      }

      addons.add(av);
    }
    return addons;
  }

  public static BillingInvoice reportToInvoiceForUser(ReportForUserView uv, String adminUserId) {
    BillingInvoice bi = BillingInvoice.buildInvoice(adminUserId);
    Invoice invoice = new Invoice();

    Long clientId = null;
    if (uv.bs.getFreshbookClientData() != null) {
      clientId = uv.bs.getFreshbookClientData().getId();
    }
    if (clientId == null) {
      ApiConnection conn = BillingActor.getFresbookConnection();
      if (conn == null) {
        Log.err("Freshbook User Sync Failed: Can't establish connection to Freshbooks. Try later.");
      }

      clientId = getFreshbookClientId(uv.bs, conn, adminUserId);
    }

    if (clientId != null) {
      invoice.setClientId(clientId);
    }
    else {
      invoice.setClientId(FRESHBOOKS_DEFAULT_CLIENT_ID);
      invoice.setFirstName(uv.up.firstname);
      invoice.setLastName(uv.up.lastname);
    }

    invoice.setNotes(getInvoiceNotes(uv.period, uv.nextPeriod));

    invoice.setStatus("draft");
    invoice.setCurrencyCode(uv.bs.getPlan().getCurrency().name());

    java.util.Date date = new Date(uv.finishTs);
    invoice.setDate(date);

    ArrayList<InvoiceLine> lines = new ArrayList<>();

    reportViewToInvoiceLines(lines, uv);

    invoice.setLines(lines);

    //UMapped Invoices Data
    bi.setUserId(uv.up.getUserid());
    bi.setStartTs(uv.startTs);
    bi.setEndTs(uv.finishTs);
    bi.setState(BillingInvoice.InvoiceState.LOCAL);
    bi.setAmount(uv.totalCost);

    bi.setFreshbookInvoiceData(invoice);
    return bi;
  }

  private static ApiConnection getFresbookConnection() {
    ApiConnection con = null;
    try {
      con = new ApiConnection(new URL(ConfigMgr.getAppParameter(CoreConstants.FRESHBOOKS_URL)),
                              ConfigMgr.getAppParameter(CoreConstants.FRESHBOOKS_TOKEN),
                              "FreshBooks Java API Client");
      con.setDebug(true);

    }
    catch (MalformedURLException e) {
      Log.err(ConfigMgr.getAppParameter(CoreConstants.FRESHBOOKS_URL) +
              " is not a valid URL (" + e.getLocalizedMessage() + ")");
    }
    return con;
  }

  private void updateFreshbookItem(BillingPlan plan, Item itm) {
    itm.setName(plan.getName());
    StringBuilder sb = new StringBuilder();
    sb.append("=== GENERATED BY UMAPPED PUBLISHER\n")
      .append("=== USE PUBLISHER TO MODIFY THIS PLAN\n")
      .append("=== Type: ")
      .append(plan.getType())
      .append("\n")
      .append("=== Enabled: ")
      .append(plan.getEnabled())
      .append("\n")
      .append("=== Expires: ")
      .append(Utils.getISO8601Date(plan.getExpireTs()))
      .append("\n")
      .append("=== Currency: ")
      .append(plan.getCurrency())
      .append("\n")
      .append("=== Schedule: ")
      .append(plan.getSchedule().getScheduleName())
      .append("\n");
    if (plan.getTrialLength() == null || plan.getTrialLength().intValue() == 0) {
      sb.append("=== Trial: NOT AVAILABLE\n");
    }
    else {
      sb.append("=== Trial: ").append(plan.getTrialLength()).append(" Days\n");
    }
    if (plan.getType() == BillingPlan.PlanType.ADDON && plan.getParentPlan() != null) {
      sb.append("=== Addon Parent Plan: ").append(plan.getParentPlan().getName()).append("\n");
    }
    sb.append(plan.getDescription());
    itm.setDescription(sb.toString());
    itm.setUnitCost(plan.getCost().doubleValue());
  }

  private static Long getFreshbookClientId(BillingSettingsForUser bs, ApiConnection conn, String adminUserId) {
    Long result = null;
    UserProfile up = UserProfile.findByPK(bs.getUserId());
    try {

      Client client = bs.getFreshbookClientData();
      if (client == null) {
          /*a. Trying to find if such Client is already in the system */
        Clients searchResults = conn.listClients(1, 10, null, up.getEmail());
        if (searchResults.getClients() != null) {
          if (searchResults.getClients().size() > 1) {
            Log.info("NOTICE: Freshbooks has several clients that share e-mail:" + up.getEmail());
          }

          if (searchResults.getClients().size() > 0) {
            client = searchResults.getClients().get(0);
          }
        }

        if (client == null) { //Ok, finally creating the record
          client = new Client();
          client.setEmail(up.getEmail());
          client.setFirstName(up.getFirstname());
          client.setLastName(up.getLastname());
          List<UserCmpyLink> cmpyLinks = UserCmpyLink.findActiveByUserId(bs.getUserId());
          if (cmpyLinks.size() > 0) {
            client.setOrganization(cmpyLinks.get(0).cmpy.getName()); //Ugly hack, just taking 1st company
          }
          Long clientId = conn.createClient(client);
          client = conn.getClient(clientId);
        }

        bs.setFreshbookClientData(client);
        result = client.getId();
      }
      else {
        Client freshClient = conn.getClient(client.getId());
        freshClient.setEmail(up.getEmail());
        freshClient.setFirstName(up.getFirstname());
        freshClient.setLastName(up.getLastname());
        freshClient.setLinks(null);   //Without this - death, i.e. idiotic API
        freshClient.setCredits(null); //Without this - death, i.e. idiotic API
        conn.updateClient(freshClient);
        freshClient = conn.getClient(client.getId());
        bs.setFreshbookClientData(freshClient);
        result = freshClient.getId();
      }
      bs.markModified(adminUserId);
      bs.setSyncTsToModTs();
      bs.update();
    }
    catch (IOException ioe) {
      Log.err("Error: (IO) syncing users to Freshbooks :" + up.getUserid(), ioe);
      ioe.printStackTrace();
    }
    catch (ApiException ae) {
      Log.err("Error: (API) syncing users to Freshbooks : " + up.getUserid(), ae);
      ae.printStackTrace();
    }

    return result;
  }

  private static Long getFreshbookClientId(BillingSettingsForCmpy bs, ApiConnection conn, String adminUserId) {
    Long result = null;
    Company cmpy = Company.find.byId(bs.getCmpyId());

    if (cmpy.authorizeduserid == null) {
      Log.err("Can't sync Company with Freshbooks - no authorized user set: " + cmpy.getName() + "<" + cmpy.getCmpyid
          () + ">");
      return null;
    }

    UserProfile up = UserProfile.findByPK(cmpy.getAuthorizeduserid());
    if (up == null) {
      Log.err("Can't sync Company with Freshbooks - CORRUPTED authorized user set: " + cmpy.getName() + "<" + cmpy
          .getCmpyid() + ">");
      return null;
    }

    try {
      Client client = bs.getFreshbookClientData();
      if (client == null) {
          /*a. Trying to find if such Client is already in the system */
        Clients searchResults = conn.listClients(1, 10, null, up.getEmail());
        if (searchResults.getClients() != null) {
          if (searchResults.getClients().size() > 1) {
            Log.info("NOTICE: Freshbooks has several clients that share e-mail:" + up.getEmail());
          }

          if (searchResults.getClients().size() > 0) {
            client = searchResults.getClients().get(0);
          }
        }

        if (client == null) { //Ok, finally creating the record
          client = new Client();
          client.setEmail(up.getEmail());
          client.setFirstName(up.getFirstname());
          client.setLastName(up.getLastname());
          client.setOrganization(cmpy.getName());
          Long clientId = conn.createClient(client);
          client = conn.getClient(clientId);
        }

        bs.setFreshbookClientData(client);
        result = client.getId();
      }
      else {
        Client freshClient = conn.getClient(client.getId());
        freshClient.setEmail(up.getEmail());
        freshClient.setFirstName(up.getFirstname());
        freshClient.setLastName(up.getLastname());
        freshClient.setOrganization(cmpy.getName());
        freshClient.setLinks(null);   //Without this - death, i.e. idiotic API
        freshClient.setCredits(null); //Without this - death, i.e. idiotic API
        conn.updateClient(freshClient);
        freshClient = conn.getClient(client.getId());
        bs.setFreshbookClientData(freshClient);
        result = freshClient.getId();
      }
      bs.markModified(adminUserId);
      bs.setSyncTsToModTs();
      bs.update();
    }
    catch (IOException ioe) {
      Log.err("Error: (IO) syncing cmpies to Freshbooks :" + cmpy.getName(), ioe);
      ioe.printStackTrace();
    }
    catch (ApiException ae) {
      Log.err("Error: (API) syncing cmpies to Freshbooks : " + cmpy.getName(), ae);
      ae.printStackTrace();
    }

    return result;
  }

  private static String getInvoiceNotes(YearMonth currPeriod, YearMonth nextPeriod) {
    StringBuilder noteBuilder = new StringBuilder();
    noteBuilder.append(INVOICE_MESSAGE);
    noteBuilder.append("\n");
    noteBuilder.append("Billing Period : ");
    noteBuilder.append(new DateFormatSymbols().getMonths()[nextPeriod.getMonthOfYear() - 1]);
    noteBuilder.append(" ");
    noteBuilder.append(nextPeriod.getYear());
    noteBuilder.append("\n");
    noteBuilder.append("Trip Publishing Period : ");
    noteBuilder.append(new DateFormatSymbols().getMonths()[currPeriod.getMonthOfYear() - 1]);
    noteBuilder.append(" ");
    noteBuilder.append(currPeriod.getYear());
    return noteBuilder.toString();
  }
}
