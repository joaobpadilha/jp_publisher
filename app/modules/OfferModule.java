package modules;

import com.google.inject.AbstractModule;
import com.umapped.itinerary.analyze.recommendation.RecommendationService;
import com.umapped.itinerary.analyze.recommendation.RecommendationServiceImpl;
import com.umapped.itinerary.analyze.supplier.ProviderRepository;
import com.umapped.itinerary.analyze.supplier.ProviderRepositoryImpl;
import com.umapped.itinerary.analyze.supplier.musement.MusementWebService;
import com.umapped.itinerary.analyze.supplier.musement.response.MusementCityLookup;
import com.umapped.itinerary.analyze.supplier.shoretrip.PortLookup;
import com.umapped.itinerary.analyze.supplier.shoretrip.ShoreTripProvider;
import com.umapped.itinerary.analyze.supplier.shoretrip.ShoreTripWebService;
import com.umapped.itinerary.analyze.supplier.travelbound.TraveBoundProvider;
import com.umapped.itinerary.analyze.supplier.travelbound.TravelBoundWebService;
import com.umapped.itinerary.analyze.supplier.wcities.WCitiesCityLookup;
import com.umapped.itinerary.analyze.supplier.wcities.WCitiesProivder;
import com.umapped.itinerary.analyze.supplier.wcities.WCitiesWebService;
import com.umapped.service.offer.OfferProviderRepository;
import com.umapped.service.offer.OfferProviderRespositoryImpl;
import com.umapped.service.offer.UMappedCityLookup;

/**
 * Created by wei on 2017-06-09.
 */
public class OfferModule extends AbstractModule {
  @Override
  protected void configure() {
    bind(WCitiesWebService.class);
    bind(WCitiesCityLookup.class);
    bind(MusementCityLookup.class);
    bind(UMappedCityLookup.class);
    bind(PortLookup.class);
    bind(ShoreTripWebService.class);
    bind(TravelBoundWebService.class);
    bind(RecommendationService.class).to(RecommendationServiceImpl.class);
    bind(WCitiesProivder.class);
    bind(ShoreTripProvider.class);
    bind(TraveBoundProvider.class);
    bind(ProviderRepository.class).to(ProviderRepositoryImpl.class);
    bind(OfferProviderRepository.class).to(OfferProviderRespositoryImpl.class);
    bind(MusementWebService.class);
  }
}
