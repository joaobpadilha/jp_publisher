package modules;

import actors.ActorsHelper;
import actors.TripPDFActor;
import com.google.inject.AbstractModule;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.persistence.FeedHelperTravelboundHotel;
import com.umapped.common.OnStartRunner;
import com.umapped.external.travelbound.TravelboundItemMapper;
import com.umapped.external.travelbound.TravelboundWebService;
import com.umapped.helper.TripPublisherHelper;
import controllers.PoiController;
import actors.TripAutoCreatePublishActor;

/**
 * Created by surge on 2016-11-18.
 */
public class OnStartModule
    extends AbstractModule {
  protected void configure() {
    requestStaticInjection(ActorsHelper.class);
    requestStaticInjection(DBConnectionMgr.class);
    bind(OnStartRunner.class).asEagerSingleton();
    bind(TravelboundWebService.class);
    requestStaticInjection(FeedHelperTravelboundHotel.class);
    requestStaticInjection(PoiController.class);
    requestStaticInjection(TripAutoCreatePublishActor.class);
    requestStaticInjection(TripPDFActor.class);


  }
}
