package modules;

import akka.actor.ActorSystem;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.umapped.itinerary.analyze.*;
import com.umapped.itinerary.analyze.location.*;
import com.umapped.itinerary.analyze.location.geonames.GeoNamesTimeZoneLookupService;
import com.umapped.itinerary.analyze.location.google.GoogleLocationLookupSevice;
import com.umapped.itinerary.analyze.log.AnalyzeConsoleLog;
import com.umapped.itinerary.analyze.log.ItineraryAnalyzeServiceLog;
import com.umapped.itinerary.analyze.model.json.ObjectMapperProvider;
import com.umapped.itinerary.analyze.segment.SegmentDetecterImpl;
import com.umapped.itinerary.analyze.segment.SegmentDetector;
import com.umapped.itinerary.analyze.segment.feature.DefaultFeatureDetectorRegistory;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeatureDetectorRegistry;

/**
 * Created by wei on 2017-02-16.
 */
public class ItineraryAnalyzeModule
    extends AbstractModule {
    @Override
    protected void configure() {
        bind(TimeZoneLookupService.class).to(GeoNamesTimeZoneLookupService.class);
        bind(LocationLookupService.class).to(GoogleLocationLookupSevice.class);
        try {
            bind(LocationEnhancer.class).toConstructor(LocationEnhancer.class.getConstructor(TimeZoneLookupService.class));
        } catch (Exception e) {
            e.printStackTrace();
        }

        bind(ObjectMapper.class).annotatedWith(Names.named("ItineraryAnalayzeObjectMapper")).toProvider(ObjectMapperProvider.class);
        bind(SegmentDetector.class).to(SegmentDetecterImpl.class);
        bind(SegmentFeatureDetectorRegistry.class).to(DefaultFeatureDetectorRegistory.class);
        bind(ItineraryAnalyzer.class).toProvider(ItineraryAnalyzerProvider.class);
        bind(ItineraryAnalyzeServiceLog.class).to(AnalyzeConsoleLog.class);
        bind(LocationEnhanceService.class).to(LocationEnhanceServiceImpl.class);
        bind(TimeZoneLookupService.class).to(GeoNamesTimeZoneLookupService.class);
        bind(ItineraryLocationEnhancer.class);
        bind(ItineraryAnalyzeService.class).to(ItineraryAnalyzeServiceImpl.class);
    }
}
