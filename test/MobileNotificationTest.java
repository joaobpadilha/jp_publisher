import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Sender;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by twong on 2016-09-13.
 */
public class MobileNotificationTest extends Assert {

  @Test
  public void testAndroid() {
    try {
      Sender ANDROID_UMAPPED_SENDER = new Sender("AIzaSyCvVWoapWb-jSjWwz9vcCAkwi83CvYOYjs");
      Message androidMsg = new Message.Builder().addData("title", "TEST MSG").addData("msg", "Hello World").build();

      ANDROID_UMAPPED_SENDER.send(androidMsg,
                                  "APA91bFhaSDFenfwn6-LvIMFethqnuGr7Z-tOUQOLbpwXSYtc8ov9mozKpIOwg" +
                                  "-WM1xFYkNTz6kjVh2Ud6duiZoYca8toGPIwjtNIB464It1b3-5W713ObEP7q3wb8nldpDFpbQJYtMN",
                                  1);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
