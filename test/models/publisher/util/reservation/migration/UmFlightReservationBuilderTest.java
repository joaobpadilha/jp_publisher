package models.publisher.util.reservation.migration;

import org.junit.Test;

import com.umapped.persistence.reservation.flight.UmFlightReservation;

import models.publisher.utils.reservation.migration.UmFlightReservationBuilder;

public class UmFlightReservationBuilderTest {
  UmFlightReservationBuilder fixture = new UmFlightReservationBuilder();
  
  @Test
  public void testParse() {
    String comment = "Duration: 01hr(s) :35min(s)\n" +
        "Aircraft:  AIRBUS INDUSTRIE A320 JET\n" +
        "Distance: 0424\n" +
        "Stops: 0\n" +
        "Departure Terminal: TERMINAL 3\n" +
        "Arrival Terminal: TERMINAL 3\n" +
        "\n" +
        "Passengers:\n" +
        "Name: TALWAR/SANDEEP\n" +
        "Seat: 32D\n" +
        "Class:  United Economy\n" +
        "eTicket Receipt: 0167300443016\n" +
        "Frequent Flyer: GSN11124 / UNITED AIRLINES\n";


    UmFlightReservation reservation = new UmFlightReservation();
    fixture.parseComment(reservation, comment);
  
    System.out.println(reservation.getNotesPlainText());
  }
}
