package models.publisher;

import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.running;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.mapped.publisher.parse.extractor.booking.email.ReservationsHolder;
import com.mapped.publisher.persistence.bookingapi.BookingAPIMgr;
import com.mapped.publisher.persistence.bookingapi.BookingRS;

public class BookApiRecTest  {
  @Test
  public void loadFlightReservation() {
    running(fakeApplication(), () -> {
      List<BookingRS> flights = BookingAPIMgr.findByBookingType(1);
      System.out.println("Total Flights = " + flights.size());
      flights.stream()
        .forEach((booking) -> {
          ReservationsHolder rh = new ReservationsHolder();
            Assert.assertTrue(rh.parseJson(booking.getData()));
        });
    });
  }
}
