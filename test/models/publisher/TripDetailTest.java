package models.publisher;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.umapped.persistence.reservation.UmReservation;

import jdk.nashorn.internal.ir.annotations.Ignore;
import models.AbstractEBeanModelTest;
import models.utils.JsonObjectMapper;
import models.utils.PersistenceUtilFactory;

public class TripDetailTest extends AbstractEBeanModelTest {
  private Logger log = LoggerFactory.getLogger(TripDetailTest.class);
  
  @Test
  public void testInjectionSingleton() {
    JsonObjectMapper mapper = PersistenceUtilFactory.get(JsonObjectMapper.class);
    
    JsonObjectMapper mapper1 = PersistenceUtilFactory.get(JsonObjectMapper.class);
   
    Assert.assertEquals(mapper, mapper1);
  }
  
  // Ignore this test as it is hard coded id
  // TODO: (Wei) regfactor to use a test database
  @Ignore
  @Test
  public void testLoadTripDetailWithMigration() {
    TripDetail detail = TripDetail.find.byId("1276838035900000004");
    Assert.assertNotNull(detail);
  }
  
  // Ignore this test as it is hard coded id
  // TODO: (Wei) regfactor to use a test database
  @Ignore
  @Test
  public void testLoadTripDetail() {
    TripDetail detail = TripDetail.find.byId("1276838035900000044");
    Assert.assertNotNull(detail);
    log.info(detail.comments);
    log.info("Passenger Count = " + detail.getPassengerCount());
    UmReservation reservation = detail.getReservation();
    log.info(reservation.getClass().toString());
    reservation.setFees("700.00");
    detail.setReservation(reservation);
    detail.setPassengerCount(10);
    detail.save();
  
  }
}
