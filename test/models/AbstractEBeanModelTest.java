package models;

import org.junit.BeforeClass;

import com.avaje.ebean.EbeanServerFactory;
import com.avaje.ebean.config.ServerConfig;

public class AbstractEBeanModelTest {
  @BeforeClass
  public static void setupEBeanDefaultServer() {
    ServerConfig config = new ServerConfig();
    config.setName("db");

    config.loadTestProperties();

    // set as default and register so that Model can be
    // used if desired for save() and update() etc
    config.setDefaultServer(true);
    config.setRegister(true);
    
    EbeanServerFactory.create(config);
  }
}
