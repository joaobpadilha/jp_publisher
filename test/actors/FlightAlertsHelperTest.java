package actors;

import static org.junit.Assert.*;

import org.junit.Test;

import com.umapped.persistence.reservation.flight.UmFlightReservation;

import models.publisher.FlightAlert;
import models.publisher.FlightAlertEvent;
import models.publisher.FlightAlertEvent.FlightAlertType;

public class FlightAlertsHelperTest {

  @Test
  public void test() {
    UmFlightReservation reservation = new UmFlightReservation();
    reservation.setNotesPlainText("Arrival Gate: A11\nSome other comments");
    FlightAlert alert = new FlightAlert();
    alert.newArriveTerminal="1";
    alert.newArriveGate = "A16";
    alert.newDepartTerminal="Terminal 2";
    alert.newDepartGate = "B23";
    FlightAlertEvent alertEvent = new FlightAlertEvent();
    alertEvent.alertType = FlightAlertType.ARRIVAL_DELAY;
    
    String newComments = FlightAlertsHelper.generateComment(reservation, alertEvent, alert);
    assertTrue(newComments.contains("Arrival Delay"));
    assertTrue(newComments.contains("Terminal 1"));
    assertTrue(newComments.contains("Terminal 2"));
    assertTrue(newComments.contains("A16"));
    assertTrue(newComments.contains("B23"));
    
    System.out.println(newComments);
  }

}
