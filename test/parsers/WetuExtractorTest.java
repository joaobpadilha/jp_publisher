package parsers;

import com.umapped.external.wetu.pins.list.Accommodation;
import com.umapped.external.wetu.pins.list.WetuExtractor;
import com.umapped.external.wetu.pins.list.WetuList;

import java.util.Arrays;
import org.junit.Assert;
import org.junit.Test;

public class WetuExtractorTest extends Assert {

  @Test
  public void testExtractDataFromList() throws Exception {
    WetuExtractor data = new WetuExtractor();
    WetuList[] list = data.extractDataFromList("DIAKOANOUFR6UB6I");
    //System.out.println("This is the JSON details for Accommodation:: \n======================\n"+ Arrays.asList(list).toString()+"\n\n------------END");

    assertTrue("WetuList Ids are same", list[0].getId() == 64);

  }

  @Test
  public void testGetAccommodations() throws Exception {
    WetuExtractor data = new WetuExtractor();
    Accommodation[] list = data.getAccommodations("DIAKOANOUFR6UB6I","ids=64,65");

    //System.out.println("This is the JSON details for Accommodation:11: \n======================\n"+ Arrays.asList(list).toString()+"\n\n------------END");

    //assertTrue("Accomodation Ids both 64", list[0].getMapObjectId() == 14088);
    //assertTrue("Acommodation Ids both 65", list[1].getMapObjectId() == 65);
    assertTrue("There are two items in list!", list.length == 2);

  }
}