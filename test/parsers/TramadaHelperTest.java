package parsers;

import org.bouncycastle.jce.exception.ExtCertPathValidatorException;
import org.junit.Assert;
import org.junit.Test;

import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import com.mapped.publisher.parse.tramada.TramadaHelper;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

public class TramadaHelperTest extends Assert {


  /*String xml = "<DocumentInformation><Agency><AgencyName Label=\"TMC\">Tests " +
      "Travel Management</AgencyName><AgencyCode Label=\"TMC Code\">trunk_tramada</AgencyCode><AgencyAddress><Address>" +
      "<Address1 Label=\"Address Line 1\">trunkctm</Address1><Address2 Label=\" Address Line 2\">Address 2</Address2>" +
      "<Address3 Label=\" Address Line 3\">Address 3</Address3><PostCode Label=\" Postcode\">2000</PostCode>" +
      "<State Label=\"State\">NSW</State><Country Label=\"Country\">Australia</Country></Address></AgencyAddress><AgencyContacts>" +
      "<AgencyEmail Label=\"Email: \">ashok@tramada.com</AgencyEmail><AgencyPhone Label=\"Phone: \"/><AgencyFax Label=\"Fax No.: \"/>" +
      "</AgencyContacts></Agency><DocumentDetails/><Booking><BookingDetails><BookingNumber Label=\"Booking ID: \">576786</BookingNumber>" +
      "<PNRDetails><PNRList Label=\"PNR Reference: \">V6227Q</PNRList></PNRDetails><BookingConsultant1>test1234@test.com</BookingConsultant1>" +
      "<BookingConsultant2/><TravelApprover/><BookedBy Label=\"Booked By: \">Locomote Integration Mr</BookedBy><BookingDestinationCity/>" +
      "<BookingDepartureDate Label=\"Departure Date: \">20 Dec 17</BookingDepartureDate><BookingReturnDate Label=\"Return Date: \">27 Dec 17</BookingReturnDate><" +
      "BookingFinalTKTDate Label=\"Final TKT Date: \">08 Jun 17</BookingFinalTKTDate><BookingCancellationCondition/><BookingDeposits/>" +
      "<BookingDebtors><BookingDebtor><DebtorName Label=\"Debtor: \">Locomote Integration Test</DebtorName>" +
      "<DepartmentName Label=\"Department: \">Department 1</DepartmentName><CostCentreName Label=\"Cost Centre: \">TESTCOSTCENTER</CostCentreName>" +
      "</BookingDebtor></BookingDebtors><BookingPassengers><BookingPassenger><PassengerName>MR TWO TEST</PassengerName>" +
      "</BookingPassenger><BookingPassenger><PassengerName>MRS THREE TEST</PassengerName></BookingPassenger><BookingPassenger>" +
      "<PassengerName>TEST/LOCOMOTEINTEGRATIONMRMR</PassengerName></BookingPassenger><PassengerDetails><PassengerName>TEST/LOCOMOTEINTEGRATIONMRMR</PassengerName><PassengerAge>ADULT</PassengerAge></PassengerDetails><TicketNumbers><Ticket><PassengerName Label=\"Passenger\">TEST/LOCOMOTEINTEGRATIONMRMR</PassengerName><TicketNumber Label=\"Ticket Number\">L91CKL</TicketNumber><CarrierCode Label=\"Airline Code\">JQ</CarrierCode><ShortItin Label=\"Itinerary Summary\">SYD-AVV//MEL-SYD-HKG-AKL//MEL-BNE//PER-MEL</ShortItin><PassengerType Label=\"Passenger Type\">ADULT</PassengerType></Ticket><Ticket><PassengerName Label=\"Passenger\">TEST/LOCOMOTEINTEGRATIONMRMR</PassengerName><TicketNumber Label=\"Ticket Number\">9901969967</TicketNumber><CarrierCode Label=\"Airline Code\">CX</CarrierCode><ShortItin Label=\"Itinerary Summary\">SYD-HKG-AKL</ShortItin><PassengerType Label=\"Passenger Type\">ADULT</PassengerType></Ticket><Ticket><PassengerName Label=\"Passenger\">TEST/LOCOMOTEINTEGRATIONMRMR</PassengerName><TicketNumber Label=\"Ticket Number\">9901969968</TicketNumber><CarrierCode Label=\"Airline Code\">QF</CarrierCode><ShortItin Label=\"Itinerary Summary\">MEL-BNE</ShortItin><PassengerType Label=\"Passenger Type\">ADULT</PassengerType></Ticket><Ticket><PassengerName Label=\"Passenger\">TEST/LOCOMOTEINTEGRATIONMRMR</PassengerName><TicketNumber Label=\"Ticket Number\">9901969969</TicketNumber><CarrierCode Label=\"Airline Code\">VA</CarrierCode><ShortItin Label=\"Itinerary Summary\">PER-MEL</ShortItin><PassengerType Label=\"Passenger Type\">ADULT</PassengerType></Ticket></TicketNumbers><PassengerNameList Label=\"Itinerary For\">MR TWO TEST, MRS THREE TEST, TEST/LOCOMOTEINTEGRATIONMRMR</PassengerNameList></BookingPassengers></BookingDetails><ItineraryItems><ItineraryItem><ItemNo>3896403</ItemNo><SourceReloc Label=\"PNR Reference\">V6227Q</SourceReloc><SortingOrderNumber>1</SortingOrderNumber><SegmentTypeCode Label=\"FLIGHT\">Flight</SegmentTypeCode><PassengerDetails><PassengerNameList>TEST/LOCOMOTEINTEGRATIONMRMR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Flight Company</SupplierType><SupplierName>JETSTAR</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Aircraft Type\">Airbus A320-100/200</ServiceTypeDescription><ServiceIdentifier>JQ0605</ServiceIdentifier><ServiceClassDescription Label=\"Class\">B - Economy</ServiceClassDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Thu 20 Dec 17</ServiceDate><ServiceTime>07:55</ServiceTime><CityCode>SYD</CityCode><CityName>Kingsford Smith, Sydney</CityName><LocationDescription Label=\"Departure Terminal\">2</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Thu 20 Dec 17</ServiceDate><ServiceTime>09:30</ServiceTime><CityCode>AVV</CityCode><CityName>AVALON, Melbourne</CityName><LocationDescription Label=\"Arrival Terminal\"/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><AirlineReloc Label=\"Airline Reloc\">L91CKL</AirlineReloc><FlightSpecificMealsSeats>TEST/LOCOMOTEINTEGRATIONMRMR</FlightSpecificMealsSeats><Legs><LegsSummary>SYD (TERMINAL - 2) AVV (TERMINAL - ), Dept Time 20-12-2017 07:55, Arrival Time 20-12-2017 09:30 - Travelling time: 1 hr 35 mins</LegsSummary><Leg><StopOverLeg Label=\"Journey Info\">0</StopOverLeg><DepartureCity Label=\"Departure City\">Kingsford Smith, Sydney</DepartureCity><DepartureTerminal Label=\"Departure Terminal\">2</DepartureTerminal><DepartureDate Label=\"Departure Date\">20-12-2017</DepartureDate><DepartureTime Label=\"Departure Time\">07:55</DepartureTime><ArrivalCity Label=\"Arrival City\">AVALON, Melbourne</ArrivalCity><ArrivalDate Label=\"Arrival Date\">20-12-2017</ArrivalDate><ArrivalTime Label=\"Arrival Time\">09:30</ArrivalTime><FlightTime Label=\"Flight Time\">01:35</FlightTime><Mileage Label=\"Mileage\">469</Mileage></Leg></Legs></FlightSpecificDetails><Status>ZK</Status><IssueDate/><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/><FareRuleNotes/></ItineraryNotes><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3897215</ItemNo><SourceReloc Label=\"Calypso ID\">01183U</SourceReloc><SortingOrderNumber>2</SortingOrderNumber><SegmentTypeCode Label=\"TRANSFER\">Transfer</SegmentTypeCode><PassengerDetails><PassengerNameList>MR TWO TEST, MRS THREE TEST</PassengerNameList></PassengerDetails><Supplier><SupplierType>Transfer Company</SupplierType><SupplierCode Label=\"Supplier Code\">LORI</SupplierCode><SupplierName>LORI</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Transfer Type\">OW Seat in Coach Airport to Hotel</ServiceTypeDescription><Start><StartLabelValue>Pick Up</StartLabelValue><ServiceDate>Thu 20 Dec 17</ServiceDate><ServiceTime/><CityCode>SFO</CityCode><CityName>SAN FRANCISCO, CA</CityName><LocationTypeDescription Label=\"Start Hotel\"/><LocationDescription Label=\"Pick Up location\"/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Drop Off</EndLabelValue><ServiceDate>Thu 20 Dec 17</ServiceDate><ServiceTime/><CityCode>SFO</CityCode><CityName>SAN FRANCISCO, CA</CityName><LocationTypeDescription Label=\"Finish Hotel\"/><LocationDescription Label=\"Drop Off Location\"/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate/><OrderDate/><ConfirmationNo Label=\"Confirmation No\">01183U</ConfirmationNo><AccountDetails><PaymentNarrative>Free of Charge</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes>ARRIVAL TRANSFER: Upon arrival at San Francisco International Airport follow the signs for \"Share Ride Vans\" to the information booth located on the arrival/baggage claim level of each terminal and read the display monitor for check in instructions. If instructed to see a \"curb coordinator\" check in with our Curb Services Representative in a green GO Lorries jacket standing outside at the Share Ride Van boarding zone or call us at (415) 334-9000 to request a pick up. Free calls available from the courtesy telephones at the information booths. PASSENGER &amp; LUGGAGE RESTRICTIONS Luggage allowances are 2 standard-size suitcase and 1 small carry-on per person. No single piece of baggage or property weighing in excess of sixty (60) pounds will be accepted for transportation unless there is additional help available to aid the driver in both the loading and unloading of such baggage. If passengers have an excessive amount of checked luggage or multiple large boxes a private van will be required. Passenger will be required to pay the difference in cash between the cost of a private van and net rate of client issuing voucher. Go Lorries Airport Shuttle will not be liable for lost baggage since baggage is never removed from the passengers presence and is stored in the passenger compartment in which the passenger is riding. A passengers baggage remains, at all times, the responsibility of the passenger. Go Lorrie's does not provide child restraint seats. It is the responsibility of a child's parent or caregiver to provide the Child Restraint System when transporting children in van and/or taxi cabs. The child/restraint seat law applies to children that are less than 8 years of age, or that are less than 4 feet 9 inches in height. Reservations/Payments by Freestyle Travel PTY Limited trading as Freestyle Holidays</SegmentNotes><FareRuleNotes/></ItineraryNotes><NoOfItems Label=\"No. Of Passengers\">2</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3897216</ItemNo><SourceReloc Label=\"Calypso ID\">01183U</SourceReloc><SortingOrderNumber>3</SortingOrderNumber><SegmentTypeCode Label=\"HOTEL\">Hotel</SegmentTypeCode><PassengerDetails><PassengerNameList>MR TWO TEST, MRS THREE TEST</PassengerNameList></PassengerDetails><Supplier><SupplierType>Hotel Company</SupplierType><SupplierCode Label=\"Supplier Code\">HANL</SupplierCode><SupplierName>HANL</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Room Type\">Family Suite</ServiceTypeDescription><Start><StartLabelValue>Check In</StartLabelValue><ServiceDate>Thu 20 Dec 17</ServiceDate><ServiceTime>06:00</ServiceTime><CityCode>SFO</CityCode><CityName>SAN FRANCISCO, CA</CityName><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Check Out</EndLabelValue><ServiceDate>Wed 26 Dec 17</ServiceDate><ServiceTime>12:00</ServiceTime><CityCode/><CityName/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>OnRequest</Status><IssueDate/><OrderDate/><ConfirmationNo Label=\"Confirmation No\">01183U</ConfirmationNo><AccountDetails><PaymentNarrative>Free of Charge</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes>RATE INCLUDES: Plated Full American Breakfast for 2, based on double occupancy per day served at the Daily Grill Restaurant. Additional Room occupants may order off the menu and is payable direct to the hotel. A prepaid breakfast coupon is available. Maximum 1 Breakfast per day for single occupancy. There is no children's menu. Room service orders are not applicable for this offer. Reservations/Payments by Freestyle Travel PTY Limited trading as Freestyle Holidays</SegmentNotes><FareRuleNotes/></ItineraryNotes><NoOfItems Label=\"No. of Rooms\">1</NoOfItems><NoOfdays UnitOfCount=\"Nights\" Label=\"Duration\">6</NoOfdays><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3896405</ItemNo><SourceReloc Label=\"PNR Reference\">V6227Q</SourceReloc><SortingOrderNumber>4</SortingOrderNumber><SegmentTypeCode Label=\"FLIGHT\">Flight</SegmentTypeCode><PassengerDetails><PassengerNameList>TEST/LOCOMOTEINTEGRATIONMRMR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Flight Company</SupplierType><SupplierName>JETSTAR</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Aircraft Type\">Airbus A320-100/200</ServiceTypeDescription><ServiceIdentifier>JQ0502</ServiceIdentifier><ServiceClassDescription Label=\"Class\">Y - Starter Max</ServiceClassDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Sat 22 Dec 17</ServiceDate><ServiceTime>07:35</ServiceTime><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><LocationDescription Label=\"Departure Terminal\">4</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Sat 22 Dec 17</ServiceDate><ServiceTime>09:00</ServiceTime><CityCode>SYD</CityCode><CityName>Kingsford Smith, Sydney</CityName><LocationDescription Label=\"Arrival Terminal\">2</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><AirlineReloc Label=\"Airline Reloc\">L91CKL</AirlineReloc><FlightSpecificMealsSeats>TEST/LOCOMOTEINTEGRATIONMRMR</FlightSpecificMealsSeats><Legs><LegsSummary>MEL (TERMINAL - 4) SYD (TERMINAL - 2), Dept Time 22-12-2017 07:35, Arrival Time 22-12-2017 09:00 - Travelling time: 1 hr 25 mins</LegsSummary><Leg><StopOverLeg Label=\"Journey Info\">0</StopOverLeg><DepartureCity Label=\"Departure City\">MELBOURNE, AUSTRALIA</DepartureCity><DepartureTerminal Label=\"Departure Terminal\">4</DepartureTerminal><DepartureDate Label=\"Departure Date\">22-12-2017</DepartureDate><DepartureTime Label=\"Departure Time\">07:35</DepartureTime><ArrivalCity Label=\"Arrival City\">Kingsford Smith, Sydney</ArrivalCity><ArrivalTerminal Label=\"Arrival Terminal\">2</ArrivalTerminal><ArrivalDate Label=\"Arrival Date\">22-12-2017</ArrivalDate><ArrivalTime Label=\"Arrival Time\">09:00</ArrivalTime><FlightTime Label=\"Flight Time\">01:25</FlightTime><Mileage Label=\"Mileage\">439</Mileage></Leg></Legs></FlightSpecificDetails><Status>ZK</Status><IssueDate/><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/><FareRuleNotes/></ItineraryNotes><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3896400</ItemNo><SourceReloc Label=\"PNR Reference\">V6227Q</SourceReloc><SortingOrderNumber>5</SortingOrderNumber><SegmentTypeCode Label=\"FLIGHT\">Flight</SegmentTypeCode><PassengerDetails><PassengerNameList>TEST/LOCOMOTEINTEGRATIONMRMR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Flight Company</SupplierType><SupplierName>CATHAY PACIFIC AIRWAYS</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Aircraft Type\">Airbus A330-300</ServiceTypeDescription><ServiceIdentifier>CX0110</ServiceIdentifier><ServiceClassDescription Label=\"Class\">Y - Economy</ServiceClassDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Mon 24 Dec 17</ServiceDate><ServiceTime>07:30</ServiceTime><CityCode>SYD</CityCode><CityName>Kingsford Smith, Sydney</CityName><LocationDescription Label=\"Departure Terminal\">1</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Mon 24 Dec 17</ServiceDate><ServiceTime>15:10</ServiceTime><CityCode>HKG</CityCode><CityName>HONG KONG, HONG KONG</CityName><LocationDescription Label=\"Arrival Terminal\">1</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><BaggageAllowance Label=\"Baggage Allowance\">30 kilos</BaggageAllowance><FlightSpecificMealsSeats>TEST/LOCOMOTEINTEGRATIONMRMR</FlightSpecificMealsSeats><Legs><LegsSummary>SYD (TERMINAL - 1) HKG (TERMINAL - 1), Dept Time 24-12-2017 07:30, Arrival Time 24-12-2017 15:10 - Travelling time: 9 hrs 40 mins - Meal Service: Breakfast, Lunch</LegsSummary><Leg><StopOverLeg Label=\"Journey Info\">0</StopOverLeg><DepartureCity Label=\"Departure City\">Kingsford Smith, Sydney</DepartureCity><DepartureTerminal Label=\"Departure Terminal\">1</DepartureTerminal><DepartureDate Label=\"Departure Date\">24-12-2017</DepartureDate><DepartureTime Label=\"Departure Time\">07:30</DepartureTime><ArrivalCity Label=\"Arrival City\">HONG KONG, HONG KONG</ArrivalCity><ArrivalTerminal Label=\"Arrival Terminal\">1</ArrivalTerminal><ArrivalDate Label=\"Arrival Date\">24-12-2017</ArrivalDate><ArrivalTime Label=\"Arrival Time\">15:10</ArrivalTime><FlightTime Label=\"Flight Time\">09:40</FlightTime><Mileage Label=\"Mileage\">4586</Mileage><MealServices Label=\"Meal Services\">B,L,</MealServices></Leg></Legs></FlightSpecificDetails><Status>Confirmed</Status><IssueDate/><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/><FareRuleNotes/></ItineraryNotes><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3896404</ItemNo><SourceReloc Label=\"PNR Reference\">V6227Q</SourceReloc><SortingOrderNumber>6</SortingOrderNumber><SegmentTypeCode Label=\"FLIGHT\">Flight</SegmentTypeCode><PassengerDetails><PassengerNameList>TEST/LOCOMOTEINTEGRATIONMRMR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Flight Company</SupplierType><SupplierName>CATHAY PACIFIC AIRWAYS</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Aircraft Type\">Boeing 777-200</ServiceTypeDescription><ServiceIdentifier>CX7401</ServiceIdentifier><ServiceClassDescription Label=\"Class\">R - Premium Economy</ServiceClassDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Mon 24 Dec 17</ServiceDate><ServiceTime>19:10</ServiceTime><CityCode>HKG</CityCode><CityName>HONG KONG, HONG KONG</CityName><LocationDescription Label=\"Departure Terminal\">1</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Tue 25 Dec 17</ServiceDate><ServiceTime>09:55</ServiceTime><CityCode>AKL</CityCode><CityName>AUCKLAND, NEW ZEALAND</CityName><LocationDescription Label=\"Arrival Terminal\">I</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><CodeShare Label=\"Code Share\">AIR NEW ZEALAND</CodeShare><BaggageAllowance Label=\"Baggage Allowance\">30 kilos</BaggageAllowance><FlightSpecificMealsSeats>TEST/LOCOMOTEINTEGRATIONMRMR</FlightSpecificMealsSeats><Legs><LegsSummary>HKG (TERMINAL - 1) AKL (TERMINAL - I), Dept Time 24-12-2017 19:10, Arrival Time 25-12-2017 09:55 - Travelling time: 10 hrs 45 mins - Meal Service: Breakfast, Dinner</LegsSummary><Leg><StopOverLeg Label=\"Journey Info\">0</StopOverLeg><DepartureCity Label=\"Departure City\">HONG KONG, HONG KONG</DepartureCity><DepartureTerminal Label=\"Departure Terminal\">1</DepartureTerminal><DepartureDate Label=\"Departure Date\">24-12-2017</DepartureDate><DepartureTime Label=\"Departure Time\">19:10</DepartureTime><ArrivalCity Label=\"Arrival City\">AUCKLAND, NEW ZEALAND</ArrivalCity><ArrivalTerminal Label=\"Arrival Terminal\">I</ArrivalTerminal><ArrivalDate Label=\"Arrival Date\">25-12-2017</ArrivalDate><ArrivalTime Label=\"Arrival Time\">09:55</ArrivalTime><FlightTime Label=\"Flight Time\">10:45</FlightTime><Mileage Label=\"Mileage\">5687</Mileage><MealServices Label=\"Meal Services\">B,D,</MealServices></Leg></Legs></FlightSpecificDetails><Status>Confirmed</Status><IssueDate/><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/><FareRuleNotes/></ItineraryNotes><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3896411</ItemNo><SourceReloc Label=\"PNR Reference\">V6227Q</SourceReloc><SortingOrderNumber>7</SortingOrderNumber><SegmentTypeCode Label=\"HOTEL\">Hotel</SegmentTypeCode><PassengerDetails><PassengerNameList>TEST/LOCOMOTEINTEGRATIONMRMR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Hotel Company</SupplierType><SupplierCode Label=\"Supplier Code\">BWAKL89594</SupplierCode><SupplierName>BEST WESTERN BKS PIONEER MOTOR</SupplierName><SupplierAddress><Address><Address1>205 Kirkbride Road</Address1><Address2>Mangere 2022 NZ</Address2><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierPhone Label=\"Phone\">P-64 9-2757752</SupplierPhone><SupplierFax>F-64 9-2757753</SupplierFax></Supplier><Start><StartLabelValue>Check In</StartLabelValue><ServiceDate>Tue 25 Dec 17</ServiceDate><ServiceTime>14:00</ServiceTime><CityCode>AKL</CityCode><CityName>AUCKLAND, NEW ZEALAND</CityName><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Check Out</EndLabelValue><ServiceDate>Wed 26 Dec 17</ServiceDate><ServiceTime>10:00</ServiceTime><CityCode/><CityName/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate/><OrderDate/><ConfirmationNo Label=\"Confirmation No\">688062179</ConfirmationNo><AccountDetails><PaymentNarrative>All Charges</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions>TO AVOID CHARGE CNCL OR IGNORE WITHIN 10 MINUT</CancelConditions><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/><FareRuleNotes/></ItineraryNotes><NoOfItems Label=\"No. of Rooms\">1</NoOfItems><NoOfdays UnitOfCount=\"Nights\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD127.95</AUDRate><LocalRate Label=\"Local Rate Incl GST\">NZD133.20</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3896401</ItemNo><SourceReloc Label=\"PNR Reference\">V6227Q</SourceReloc><SortingOrderNumber>8</SortingOrderNumber><SegmentTypeCode Label=\"FLIGHT\">Flight</SegmentTypeCode><PassengerDetails><PassengerNameList>TEST/LOCOMOTEINTEGRATIONMRMR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Flight Company</SupplierType><SupplierName>QANTAS AIRWAYS</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Aircraft Type\">Boeing 737-800 (winglets)</ServiceTypeDescription><ServiceIdentifier>QF0608</ServiceIdentifier><ServiceClassDescription Label=\"Class\">M - Economy Class</ServiceClassDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Wed 26 Dec 17</ServiceDate><ServiceTime>08:00</ServiceTime><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><LocationDescription Label=\"Departure Terminal\">1</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Wed 26 Dec 17</ServiceDate><ServiceTime>10:10</ServiceTime><CityCode>BNE</CityCode><CityName>BRISBANE, AUSTRALIA</CityName><LocationDescription Label=\"Arrival Terminal\">D</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><AirlineReloc Label=\"Airline Reloc\">SVDY25</AirlineReloc><BaggageAllowance Label=\"Baggage Allowance\">1piece</BaggageAllowance><FlightSpecificMealsSeats>TEST/LOCOMOTEINTEGRATIONMRMR</FlightSpecificMealsSeats><Legs><LegsSummary>MEL (TERMINAL - 1) BNE (TERMINAL - D), Dept Time 26-12-2017 08:00, Arrival Time 26-12-2017 10:10 - Travelling time: 2 hrs 10 mins - Meal Service: Breakfast</LegsSummary><Leg><StopOverLeg Label=\"Journey Info\">0</StopOverLeg><DepartureCity Label=\"Departure City\">MELBOURNE, AUSTRALIA</DepartureCity><DepartureTerminal Label=\"Departure Terminal\">1</DepartureTerminal><DepartureDate Label=\"Departure Date\">26-12-2017</DepartureDate><DepartureTime Label=\"Departure Time\">08:00</DepartureTime><ArrivalCity Label=\"Arrival City\">BRISBANE, AUSTRALIA</ArrivalCity><ArrivalTerminal Label=\"Arrival Terminal\">D</ArrivalTerminal><ArrivalDate Label=\"Arrival Date\">26-12-2017</ArrivalDate><ArrivalTime Label=\"Arrival Time\">10:10</ArrivalTime><FlightTime Label=\"Flight Time\">02:10</FlightTime><Mileage Label=\"Mileage\">856</Mileage><MealServices Label=\"Meal Services\">B</MealServices></Leg></Legs></FlightSpecificDetails><Status>Confirmed</Status><IssueDate/><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/><FareRuleNotes/></ItineraryNotes><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3902059</ItemNo><SortingOrderNumber>9</SortingOrderNumber><SegmentTypeCode Label=\"CRUISE\">Cruise</SegmentTypeCode><PassengerDetails><PassengerNameList/></PassengerDetails><Supplier><SupplierType>Cruise Company</SupplierType><SupplierCode Label=\"Supplier Code\">CARNIVAL</SupplierCode><SupplierName>CARNIVAL CRUISE LINES</SupplierName><SupplierAddress><Address><Address1>LEVEL 1</Address1><Address2>189 KENT ST</Address2><Address3>SYDNEY</Address3><PostCode>2060</PostCode><State>NSW</State><Country>Australia</Country></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Cabin Type\">2 berth Upper with balc</ServiceTypeDescription><ServiceClassDescription Label=\"Cabin\">deck a</ServiceClassDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Wed 26 Dec 17</ServiceDate><ServiceTime>09:00</ServiceTime><CityCode>SYD CITY</CityCode><CityName>SYD CITY</CityName><LocationDescription Label=\"Port Name\"/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Mon 31 Dec 17</ServiceDate><ServiceTime>09:00</ServiceTime><CityCode>SYD CITY</CityCode><CityName>SYD CITY</CityName><LocationDescription Label=\"Port Name\"/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><FareBasis Label=\"Cruise Name\">Carnival South Pacific Cruise</FareBasis><CodeShare Label=\"Ship Name\">CARNIVAL LEGEND</CodeShare><AirlineReloc Label=\"Cabin No.\">081</AirlineReloc></FlightSpecificDetails><Status>Confirmed</Status><IssueDate/><OrderDate/><ConfirmationNo Label=\"Confirmation No\">confirmationNumber</ConfirmationNo><AccountDetails><PaymentNarrative>Free of Charge</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/><FareRuleNotes/></ItineraryNotes><NoOfItems Label=\"No. Of Passengers\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">6</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD1200.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD1200.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3902061</ItemNo><SortingOrderNumber>10</SortingOrderNumber><SegmentTypeCode Label=\"MISC\">Misc</SegmentTypeCode><PassengerDetails><PassengerNameList/></PassengerDetails><Supplier><SupplierType>Misc Company</SupplierType><SupplierCode Label=\"Supplier Code\">DRIVE</SupplierCode><SupplierName>DRIVE AWAY HOLIDAYS</SupplierName><SupplierAddress><Address><Address1>PO BOX 1321</Address1><Address2>NORTH SYDNEY</Address2><Address3/><PostCode>2059</PostCode><State>NSW</State><Country>Australia</Country></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Type\">Motorhome Hire</ServiceTypeDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Thu 27 Dec 17</ServiceDate><ServiceTime>09:00</ServiceTime><CityCode>AKL</CityCode><CityName>AUCKLAND, NEW ZEALAND</CityName><LocationDescription Label=\"Start Location\"/><Address><Address1>address 1</Address1><Address2>address 2</Address2><Address3>Auckland</Address3><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Thu 27 Dec 17</ServiceDate><ServiceTime/><CityCode>AKL</CityCode><CityName>AUCKLAND, NEW ZEALAND</CityName><LocationDescription Label=\"Drop Off Location\"/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate/><OrderDate/><AccountDetails><PaymentNarrative>Free of Charge</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription>24 hours cancellation notice required</CancellationDescription></AccountDetails><ItineraryNotes><SegmentNotes/><FareRuleNotes/></ItineraryNotes><NoOfItems Label=\"No. of Units\">1</NoOfItems><NoOfdays UnitOfCount=\"Nights\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD1200.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD1200.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3896402</ItemNo><SourceReloc Label=\"PNR Reference\">V6227Q</SourceReloc><SortingOrderNumber>11</SortingOrderNumber><SegmentTypeCode Label=\"FLIGHT\">Flight</SegmentTypeCode><PassengerDetails><PassengerNameList>TEST/LOCOMOTEINTEGRATIONMRMR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Flight Company</SupplierType><SupplierName>VIRGIN AUSTRALIA INT</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Aircraft Type\">Airbus A330-200</ServiceTypeDescription><ServiceIdentifier>VA0684</ServiceIdentifier><ServiceClassDescription Label=\"Class\">L - Economy</ServiceClassDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Thu 27 Dec 17</ServiceDate><ServiceTime>11:35</ServiceTime><CityCode>PER</CityCode><CityName>PERTH, AUSTRALIA</CityName><LocationDescription Label=\"Departure Terminal\">1</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Thu 27 Dec 17</ServiceDate><ServiceTime>17:05</ServiceTime><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><LocationDescription Label=\"Arrival Terminal\">3</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><AirlineReloc Label=\"Airline Reloc\">NRETEL</AirlineReloc><BaggageAllowance Label=\"Baggage Allowance\">1piece</BaggageAllowance><FlightSpecificMealsSeats>TEST/LOCOMOTEINTEGRATIONMRMR</FlightSpecificMealsSeats><Legs><LegsSummary>PER (TERMINAL - 1) MEL (TERMINAL - 3), Dept Time 27-12-2017 07:35, Arrival Time 27-12-2017 13:05 - Travelling time: 3 hrs 30 mins - Meal Service: Meals</LegsSummary><Leg><StopOverLeg Label=\"Journey Info\">0</StopOverLeg><DepartureCity Label=\"Departure City\">PERTH, AUSTRALIA</DepartureCity><DepartureTerminal Label=\"Departure Terminal\">1</DepartureTerminal><DepartureDate Label=\"Departure Date\">27-12-2017</DepartureDate><DepartureTime Label=\"Departure Time\">07:35</DepartureTime><ArrivalCity Label=\"Arrival City\">MELBOURNE, AUSTRALIA</ArrivalCity><ArrivalTerminal Label=\"Arrival Terminal\">3</ArrivalTerminal><ArrivalDate Label=\"Arrival Date\">27-12-2017</ArrivalDate><ArrivalTime Label=\"Arrival Time\">13:05</ArrivalTime><FlightTime Label=\"Flight Time\">03:30</FlightTime><Mileage Label=\"Mileage\">1681</Mileage><MealServices Label=\"Meal Services\">M</MealServices></Leg></Legs></FlightSpecificDetails><Status>Confirmed</Status><IssueDate/><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/><FareRuleNotes/></ItineraryNotes><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3897217</ItemNo><SourceReloc Label=\"Calypso ID\">01183U</SourceReloc><SortingOrderNumber>12</SortingOrderNumber><SegmentTypeCode Label=\"TRANSFER\">Transfer</SegmentTypeCode><PassengerDetails><PassengerNameList>MR TWO TEST, MRS THREE TEST</PassengerNameList></PassengerDetails><Supplier><SupplierType>Transfer Company</SupplierType><SupplierCode Label=\"Supplier Code\">LORI</SupplierCode><SupplierName>LORI</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Transfer Type\">OW Seat in Coach - Hotel to Airport</ServiceTypeDescription><Start><StartLabelValue>Pick Up</StartLabelValue><ServiceDate>Wed 26 Dec 17</ServiceDate><ServiceTime/><CityCode>SFO</CityCode><CityName>SAN FRANCISCO, CA</CityName><LocationTypeDescription Label=\"Start Hotel\"/><LocationDescription Label=\"Pick Up location\"/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Drop Off</EndLabelValue><ServiceDate>Wed 26 Dec 17</ServiceDate><ServiceTime/><CityCode>SFO</CityCode><CityName>SAN FRANCISCO, CA</CityName><LocationTypeDescription Label=\"Finish Hotel\"/><LocationDescription Label=\"Drop Off Location\"/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate/><OrderDate/><ConfirmationNo Label=\"Confirmation No\">01183U</ConfirmationNo><AccountDetails><PaymentNarrative>Free of Charge</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes>DEPARTURE TRANSFER: To reserve your transportation from your hotel to San Francisco International Airport, please call (415) 334-9000 at least 24 hours in advance of departure flight. Your pick up time will then be confirmed. Please advise the reservation agent that you have a pre-paid voucher to avoid double booking. 1 Standard-sized checked piece plus 1 small carry-on per person Reservations/Payments by Freestyle Travel PTY Limited trading as Freestyle Holidays</SegmentNotes><FareRuleNotes/></ItineraryNotes><NoOfItems Label=\"No. Of Passengers\">2</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3902060</ItemNo><SortingOrderNumber>13</SortingOrderNumber><SegmentTypeCode Label=\"COMMENT\">Comment</SegmentTypeCode><PassengerDetails><PassengerNameList/></PassengerDetails><Supplier><SupplierType>Comment Company</SupplierType><SupplierName/><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Type\">Comment</ServiceTypeDescription><Start><StartLabelValue>Start</StartLabelValue><ServiceDate/><ServiceTime/><CityCode/><CityName/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>End</EndLabelValue><ServiceDate/><ServiceTime/><CityCode/><CityName/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status/><IssueDate/><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes>This is a comment</SegmentNotes><FareRuleNotes/></ItineraryNotes><RateInformation/></ItineraryItem></ItineraryItems><CostingsItems><CostingItem><ItemNo>2212465</ItemNo><SourceReloc Label=\"Calypso ID\">01183U</SourceReloc><SortingOrderNumber>1</SortingOrderNumber><SegmentTypeCode Label=\"PACKAGE\">Package</SegmentTypeCode><Creditor><CreditorName>SI HOLIDAS</CreditorName></Creditor><CostingDescriptor/><IssueDate/><StartCity/><StartCityName Label=\"Departure City\"/><EndCity/><EndCityName Label=\"Arrival City\"/><CostingNotes/><CostingFinancials><BaseFare>4654.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>4654.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2211841</ItemNo><SourceReloc Label=\"PNR Reference\">V6227Q</SourceReloc><SortingOrderNumber>5</SortingOrderNumber><SegmentTypeCode Label=\"HOTEL\">Hotel</SegmentTypeCode><Creditor><CreditorName>BEST WESTERN BKS PIONEER MOTOR</CreditorName></Creditor><CostingDescriptor/><IssueDate/><ConfirmationNo Label=\"Confirmation No\">688062179</ConfirmationNo><StartServiceDate Label=\"Start Service Date\">25 Dec 17</StartServiceDate><StartCity>AKL</StartCity><StartCityName Label=\"City\">AUCKLAND, NEW ZEALAND</StartCityName><EndServiceDate Label=\"End Service Date\">26 Dec 17</EndServiceDate><EndCity/><CostingNotes/><CostingFinancials><BaseFare>127.95</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>127.95</ClientDue><PaymentType>Pay Direct</PaymentType></CostingFinancials><CancelConditions Label=\"Cancel Conditions\">TO AVOID CHARGE CNCL OR IGNORE WITHIN 10 MINUT</CancelConditions></CostingItem><CostingItem><ItemNo>2211838</ItemNo><SourceReloc Label=\"PNR Reference\">V6227Q</SourceReloc><SortingOrderNumber>6</SortingOrderNumber><SegmentTypeCode Label=\"TICKET\">Ticket</SegmentTypeCode><Creditor><CreditorName>BSP-IATA</CreditorName></Creditor><TKTSpecific><AirlineCode>CX</AirlineCode><TKTPassenger>TEST/LOCOMOTEINTEGRATIONMRMR</TKTPassenger><PassengerType>ADULT</PassengerType><ClassCode>Y</ClassCode></TKTSpecific><ShortItinerary Label=\"Short Itinerary\">SYD-HKG-AKL</ShortItinerary><CostingDescriptor/><IssueDate>08 Jun 17</IssueDate><ConfirmationNo Label=\"Ticket No\">9901969967</ConfirmationNo><StartServiceDate Label=\"Start Service Date\">24 Dec 17</StartServiceDate><StartCity/><StartCityName Label=\"Start City\"/><EndCity/><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>9284.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>235.42</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>9519.42</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2211839</ItemNo><SourceReloc Label=\"PNR Reference\">V6227Q</SourceReloc><SortingOrderNumber>7</SortingOrderNumber><SegmentTypeCode Label=\"TICKET\">Ticket</SegmentTypeCode><Creditor><CreditorName>BSP-IATA</CreditorName></Creditor><TKTSpecific><AirlineCode>QF</AirlineCode><TKTPassenger>TEST/LOCOMOTEINTEGRATIONMRMR</TKTPassenger><PassengerType>ADULT</PassengerType><ClassCode>M</ClassCode></TKTSpecific><ShortItinerary Label=\"Short Itinerary\">MEL-BNE</ShortItinerary><CostingDescriptor/><IssueDate>08 Jun 17</IssueDate><ConfirmationNo Label=\"Ticket No\">9901969968</ConfirmationNo><StartServiceDate Label=\"Start Service Date\">26 Dec 17</StartServiceDate><StartCity/><StartCityName Label=\"Start City\"/><EndCity/><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>430.49</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>16.78</TaxesAndFeesExGST><GST Label=\"GST\">44.73</GST><ClientDue>492.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2211840</ItemNo><SourceReloc Label=\"PNR Reference\">V6227Q</SourceReloc><SortingOrderNumber>8</SortingOrderNumber><SegmentTypeCode Label=\"TICKET\">Ticket</SegmentTypeCode><Creditor><CreditorName>BSP-IATA</CreditorName></Creditor><TKTSpecific><AirlineCode>VA</AirlineCode><TKTPassenger>TEST/LOCOMOTEINTEGRATIONMRMR</TKTPassenger><PassengerType>ADULT</PassengerType><ClassCode>L</ClassCode></TKTSpecific><ShortItinerary Label=\"Short Itinerary\">PER-MEL</ShortItinerary><CostingDescriptor/><IssueDate>08 Jun 17</IssueDate><ConfirmationNo Label=\"Ticket No\">9901969969</ConfirmationNo><StartServiceDate Label=\"Start Service Date\">27 Dec 17</StartServiceDate><StartCity/><StartCityName Label=\"Start City\"/><EndCity/><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>528.05</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>16.50</TaxesAndFeesExGST><GST Label=\"GST\">54.46</GST><ClientDue>599.01</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2211837</ItemNo><SourceReloc Label=\"PNR Reference\">V6227Q</SourceReloc><SortingOrderNumber>9</SortingOrderNumber><SegmentTypeCode Label=\"TICKET\">Ticket</SegmentTypeCode><Creditor><CreditorName>JETSTAR AIRWAYS</CreditorName></Creditor><TKTSpecific><AirlineCode>JQ</AirlineCode><TKTPassenger>TEST/LOCOMOTEINTEGRATIONMRMR</TKTPassenger><PassengerType>ADULT</PassengerType><ClassCode>Y</ClassCode></TKTSpecific><ShortItinerary Label=\"Short Itinerary\">SYD-AVV//MEL-SYD-HKG-AKL//MEL-BNE//PER-MEL</ShortItinerary><CostingDescriptor/><IssueDate>08 Jun 17</IssueDate><ConfirmationNo Label=\"Ticket No\">L91CKL</ConfirmationNo><StartServiceDate Label=\"Start Service Date\">20 Dec 17</StartServiceDate><StartCity/><StartCityName Label=\"Start City\"/><EndCity/><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>766.22</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">76.62</GST><ClientDue>842.84</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials><Receipts><Receipt><ReceiptNumber Label=\"Rec. No.:\">R.0001513629</ReceiptNumber><ReceiptDate Label=\"Rec. Date:\">08/06/2017</ReceiptDate><AllocatedAmount Label=\"Allocated Amount:\">842.84</AllocatedAmount><ReceiptTotalAmount Label=\"Receipt Total Amount:\">842.84</ReceiptTotalAmount></Receipt></Receipts></CostingItem><CostingItem><ItemNo>2215566</ItemNo><SortingOrderNumber>10</SortingOrderNumber><SegmentTypeCode Label=\"CRUISE\">Cruise</SegmentTypeCode><Creditor><CreditorName>CARNIVAL CRUISE LINES</CreditorName></Creditor><CostingDescriptor/><IssueDate/><ConfirmationNo Label=\"Confirmation No\">confirmationNumber</ConfirmationNo><StartServiceDate Label=\"Start Service Date\">26 Dec 17</StartServiceDate><StartCity>SYD CITY</StartCity><StartCityName Label=\"Departure Port\">SYD CITY</StartCityName><EndServiceDate Label=\"End Service Date\">31 Dec 17</EndServiceDate><EndCity>SYD CITY</EndCity><EndCityName Label=\"Arrival Port\">SYD CITY</EndCityName><CostingNotes/><CostingFinancials><BaseFare>1090.91</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">109.09</GST><ClientDue>1200.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials><ServiceTypeDescription Label=\"Service Type\">2 berth Upper with balc</ServiceTypeDescription></CostingItem><CostingItem><ItemNo>2211836</ItemNo><SourceReloc Label=\"PNR Reference\">V6227Q</SourceReloc><SortingOrderNumber>11</SortingOrderNumber><SegmentTypeCode Label=\"TICKET\">Ticket</SegmentTypeCode><Creditor><CreditorName>BSP-IATA</CreditorName></Creditor><TKTSpecific><AirlineCode>CX</AirlineCode><TKTPassenger>TEST/LOCOMOTEINTEGRATIONMRMR</TKTPassenger><PassengerType>ADULT</PassengerType><ClassCode>B</ClassCode></TKTSpecific><ShortItinerary Label=\"Short Itinerary\">SYD-AVV//MEL-SYD</ShortItinerary><CostingDescriptor/><IssueDate/><StartServiceDate Label=\"Start Service Date\">20 Dec 17</StartServiceDate><StartCity/><StartCityName Label=\"Start City\"/><EndCity/><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>348.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>69.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>417.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2211842</ItemNo><SortingOrderNumber>12</SortingOrderNumber><SegmentTypeCode Label=\"SERVICE_FEE\">Service Fee</SegmentTypeCode><Creditor><CreditorName>xCTM CREDITOR</CreditorName></Creditor><CostingDescriptor>Booking Auto Service Fee</CostingDescriptor><IssueDate/><StartCity/><StartCityName Label=\"Start City\"/><EndCity/><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>20.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>20.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2211843</ItemNo><SortingOrderNumber>13</SortingOrderNumber><SegmentTypeCode Label=\"SERVICE_FEE\">Service Fee</SegmentTypeCode><Creditor><CreditorName>xCTM CREDITOR</CreditorName></Creditor><CostingDescriptor>Auto service fee for GDS</CostingDescriptor><IssueDate/><StartCity/><StartCityName Label=\"Start City\"/><EndCity/><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>20.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">2.00</GST><ClientDue>22.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2212469</ItemNo><SourceReloc Label=\"Calypso ID\">01183U</SourceReloc><SortingOrderNumber>14</SortingOrderNumber><SegmentTypeCode Label=\"SERVICE_FEE\">Service Fee</SegmentTypeCode><Creditor><CreditorName>xCTM CREDITOR</CreditorName></Creditor><CostingDescriptor>Calypso Booking Fee</CostingDescriptor><IssueDate/><StartCity/><StartCityName Label=\"Start City\"/><EndCity/><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>10.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>10.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2215567</ItemNo><SortingOrderNumber>15</SortingOrderNumber><SegmentTypeCode Label=\"MISC\">Misc</SegmentTypeCode><Creditor><CreditorName>DRIVE AWAY HOLIDAYS</CreditorName></Creditor><CostingDescriptor/><IssueDate/><StartServiceDate Label=\"Start Service Date\">27 Dec 17</StartServiceDate><StartCity>AKL</StartCity><StartCityName Label=\"Departure City\">AUCKLAND, NEW ZEALAND</StartCityName><EndServiceDate Label=\"End Service Date\">27 Dec 17</EndServiceDate><EndCity>AKL</EndCity><EndCityName Label=\"Arrival City\">AUCKLAND, NEW ZEALAND</EndCityName><CostingNotes/><CostingFinancials><BaseFare>1200.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>1200.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials><CancellationDescription Label=\"Cancellation\">24 hours cancellation notice required</CancellationDescription><ServiceTypeDescription Label=\"Service Type\">Motorhome Hire</ServiceTypeDescription></CostingItem><DocumentTotals><TotalPayDirect><Total><BaseFare>127.95</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>127.95</ClientDue></Total></TotalPayDirect><TotalExcPayDirect><Total><BaseFare>18351.67</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>337.70</TaxesAndFeesExcGST><GST>286.90</GST><ClientDue>18976.27</ClientDue></Total></TotalExcPayDirect><TotalIncPayDirect><Total><BaseFare>18479.62</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>337.70</TaxesAndFeesExcGST><GST>286.90</GST><ClientDue>19104.22</ClientDue></Total></TotalIncPayDirect><TotalPaid>842.84</TotalPaid><TotalOutstandingExcPayDirect>18133.43</TotalOutstandingExcPayDirect><Currency>AUD</Currency></DocumentTotals></CostingsItems></Booking></DocumentInformation>\n";

  String xml2 = "<DocumentInformation><Agency><AgencyName Label=\"TMC\">Tests Travel Management</AgencyName><AgencyCode Label=\"TMC Code\">trunk_tramada</AgencyCode><AgencyAddress><Address><Address1 Label=\"Address Line 1\">trunkctm</Address1><Address2 Label=\" Address Line 2\">Address 2</Address2><Address3 Label=\" Address Line 3\">Address 3</Address3><PostCode Label=\" Postcode\">2000</PostCode><State Label=\"State\">NSW</State><Country Label=\"Country\">Australia</Country></Address></AgencyAddress><AgencyContacts><AgencyEmail Label=\"Email: \">ashok@tramada.com</AgencyEmail><AgencyPhone Label=\"Phone: \"></AgencyPhone><AgencyFax Label=\"Fax No.: \"></AgencyFax></AgencyContacts></Agency><DocumentDetails><DocumentPrintDate>Tuesday 28 November 2017 08:42</DocumentPrintDate><DocumentPrintTime>Sydney, NSW</DocumentPrintTime><DocumentTitle Label=\"Type\">Itinerary &amp; Costing - Full</DocumentTitle><DocumentAddress><Address/></DocumentAddress><DocumentContactDetails><EmergencyContact Label=\"Emergency Contact: \">020000000</EmergencyContact><AfterHours Label=\"After Hours Contact: \">020000000</AfterHours><TollFree Label=\"Toll Free Number: \">1300 000 000</TollFree></DocumentContactDetails><DocumentCommercialDetails><LicNo Label=\"Licence No.: \">lic1</LicNo><ABN Label=\"ABN: \">52 005 000 895</ABN><ACN Label=\"ACN: \">005 000 895</ACN><MerchantNo Label=\"Merchant No: \">3333863706</MerchantNo><ATAS Label=\"ATAS Accreditation Number: \">atas1</ATAS></DocumentCommercialDetails><DocumentHeaders><DocumentHeader HeaderTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually ThereÂ®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.\n" +
      "You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentHeader><DocumentHeader HeaderTitle=\"Third party hotel booking\">Please note: your hotel has been booked via a third party provider. They have provided a confirmation number and this may be required at check-in. In most instances you will also have been provided with a confirmation and payment voucher separately to the itinerary. Please ensure you have printed a copy of this voucher and are able to produce it upon request. There can be occasional communication problems between third party bookers and hotels. If you experience any difficulties at check-in please call CTM on the numbers below.</DocumentHeader><DocumentHeader HeaderTitle=\"label\">Testing Helloworld Header</DocumentHeader><DocumentHeader HeaderTitle=\"CTM EMERGENCY AFTER HOURS\">Should you require assistance with booking changes or amendments relating to imminent departures, please contact our emergency afterhours service on the following numbers:\n" +
      "\n" +
      "Within Australia - 1800 105 334\n" +
      "\n" +
      "Outside Australia -  +61 2 8268 4100.\n" +
      "\n" +
      "Please note: Service Fees will apply as per your service agreement.\n" +
      "\n" +
      "Due to privacy laws we are unable to release travel details to anyone other than the passenger.</DocumentHeader></DocumentHeaders><DocumentFooters><DocumentFooter FooterTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually ThereÂ®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.\n" +
      "You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentFooter><DocumentFooter FooterTitle=\"E-TICKET\">E ticket identification required by airlines:\n" +
      "\n" +
      "Photo identification that has been issued by the Commonwealth of Australia or an Australian State or Territory. Acceptable photo identification includes: drivers license, passport, any state, territory or federal government issued card, company issued identification, or a student card. Qantas acceptable non photo identification includes: credit card used to pay for the ticket, other credit or debit cards, social security card, QF club or frequent flyer card, certified copy/original of a birth of citizenship document. All ID?s must be current and valid.</DocumentFooter><DocumentFooter FooterTitle=\"FREQUENT FLYER\">* Please advise your airline loyalty number if applicable. Once received we will enter this into your booking and your profile for future.\n" +
      "\n" +
      "* To ensure you receive your frequent flyer points, please retain tickets and boarding passes where possible until your points have appeared on your frequent flyer statement.\n" +
      "\n" +
      "* Should your points not appear on your statement, please send copies of your boarding passes, tickets and supporting information to the relevant airline.</DocumentFooter><DocumentFooter FooterTitle=\"IATA\">Transportation and other services provided by the carrier are subject to conditions of contract and other important notices which are delivered with this\n" +
      "itinerary/receipt and form part of the nearest office of the issuing airline or travel agent to obtain a copy prior to the commencement of your trip. Notice if\n" +
      "the passenger's journey involves an ultimate destination or stop in a country other than the country of departure the Warsaw convention may be applicable\n" +
      "and the convention governs and in most cases limits the liability of carriers for death or personal injury and in respect of loss or damage to baggage.</DocumentFooter><DocumentFooter FooterTitle=\"INTERNATIONAL FLIGHTS:  IMPORTANT TRAVEL INFORMATION\">PRICES:\n" +
      "\n" +
      "Prices are inclusive of GST where applicable. Prices are subject to change at anytime until booking is paid in full and documentation issued. As much notice as possible will be given in the event of price increases.\n" +
      "\n" +
      "TRAVEL INSURANCE:\n" +
      "\n" +
      "Travel insurance is strongly recommended. CTM can assist with your travel insurance needs.\n" +
      "\n" +
      "PASSPORTS AND VISAS:\n" +
      "\n" +
      "* Please ensure you have at least 6 months validity on your passport from return date into Australia or you may be refused entry into the country you are visiting.\n" +
      "\n" +
      "* Please advise your consultant if you are not travelling on an Australian passport as you may require a re-entry visa to Australia\n" +
      "\n" +
      "* Please refer towww.dfat.gov.au\n" +
      "\n" +
      "* It is important to advise your name as it appears on your passport. Failure to do so may result in you being denied boarding of your flight.\n" +
      "\n" +
      "* Visas may be required for your trip. These are the responsibility of the traveller but please ask us about your requirements as we are able to assist.\n" +
      "\n" +
      "AIRPORT TAXES:\n" +
      "\n" +
      "Departure and security taxes are mandatory. Any taxes that are payable prior to your departure will be factored into your travel booking cost. There may be additional taxes which are payable throughout your journey. Please ask for further details.\n" +
      "\n" +
      "CANCELLATION AND AMENDMENT FEES:\n" +
      "\n" +
      "Cancellations incur charges. A $100 per person charge is the minimum cancellation fee with additional charges imposed by airlines and tour operators. In some cases cancellation can be up to 100%. A minimum $30.00 amendment fee is imposed to reissue the documentation.\n" +
      "\n" +
      "HOW CAN I PAY?\n" +
      "\n" +
      "We accept cash, bank cheque, personal cheque (only with 10 working days for clearance of funds) or direct bank deposit. Credit cards are also accepted.\n" +
      "\n" +
      "** Please note that some service providers incur an additional merchant fee**\n" +
      "\n" +
      "TRAVEL WARNINGS:\n" +
      "\n" +
      "We strongly recommend that you refer to www.smartraveller.gov.au or www.dfat.gov.au in regards to any travel warnings which may be posted for the country you are due to visit. As these constantly change it is the travellers responsibility to ensure they are fully briefed and ware before travelling.\n" +
      "\n" +
      "HEALTH:\n" +
      "\n" +
      "Vaccinations may be required for your trip. For more information please consult your local GP or go to www.thetraveldoctor.com.au.\n" +
      "\n" +
      "** It is your responsibility to ensure you have read and understood all the above conditions.</DocumentFooter><DocumentFooter FooterTitle=\"ITINERARY ACCEPTANCE\">Whilst all care has been taken to ensure your travel requirements are booked as per your request, CTM requests that you accept responsibility for double checking these details, and advise of any changes that need to be made as soon as possible. Upon receiving your itinerary please check and make sure the relevant booking is correct and as per your travel needs.</DocumentFooter><DocumentFooter FooterTitle=\"PASSPORT / VISA + DFAT Information\">PASSPORTS AND VISAS:\n" +
      "\n" +
      "* Ensure you have at least 6 months validity on your passport from return date into Australia or you may be refused entry into the country you are visiting.\n" +
      "\n" +
      "* Advise your consultant if you are not travelling on an Australian passport as you may require a re-entry visa to Australia\n" +
      "\n" +
      "* Refer to  www.dfat.gov.au\n" +
      "\n" +
      "* It is important to advise your name as it appears on your passport. Failure to do so may result in you being denied boarding of your flight.\n" +
      "\n" +
      "* Visas may be required for your trip. These are the responsibility of the traveller but please ask us about your requirements as we are able to assist.</DocumentFooter></DocumentFooters><CarbonEmissionInformation/></DocumentDetails><Booking><BookingDetails><BookingNumber Label=\"Booking ID: \">B576786</BookingNumber><PNRDetails><PNRList Label=\"PNR Reference: \">TUYVPF</PNRList></PNRDetails><BookingConsultant1>test1234@test.com</BookingConsultant1><BookingConsultant2/><TravelApprover/><BookedBy Label=\"Booked By: \">Locomote Integration Mr</BookedBy><BookingDepartureDate Label=\"Departure Date: \">27 Dec 17</BookingDepartureDate><BookingReturnDate Label=\"Return Date: \">27 Dec 17</BookingReturnDate><BookingVisaStatus Label=\"Visa\">Already acquired</BookingVisaStatus><BookingFinalTKTDate Label=\"Final TKT Date: \">21 Nov 17</BookingFinalTKTDate><BookingCancellationCondition/><BookingDeposits/><BookingDebtors><BookingDebtor><DebtorName Label=\"Debtor: \">Locomote Integration Test</DebtorName><DepartmentName Label=\"Department: \">Department 1</DepartmentName><CostCentreName Label=\"Cost Centre: \">TESTCOSTCENTER</CostCentreName></BookingDebtor></BookingDebtors><BookingPassengers><BookingPassenger><PassengerName>MR TWO TEST</PassengerName></BookingPassenger><BookingPassenger><PassengerName>MRS THREE TEST</PassengerName><Memberships><Membership><MembershipNumber>FF FREQUENT FLYER 157496654</MembershipNumber></Membership></Memberships></BookingPassenger><BookingPassenger><PassengerName>SIMPSON/OLIVIA MISS</PassengerName></BookingPassenger><BookingPassenger><PassengerName>VT/INFANT</PassengerName></BookingPassenger><TicketNumbers><Ticket><PassengerName Label=\"Passenger\">SIMPSON/OLIVIA MISS</PassengerName><TicketNumber Label=\"Ticket Number\">45796714246</TicketNumber><CarrierCode Label=\"Airline Code\">QF</CarrierCode><ShortItin Label=\"Itinerary Summary\">DFW-HOU</ShortItin><PassengerType Label=\"Passenger Type\">ADULT</PassengerType></Ticket></TicketNumbers><PassengerNameList Label=\"Itinerary For\">MR TWO TEST, MRS THREE TEST, SIMPSON/OLIVIA MISS, VT/INFANT</PassengerNameList></BookingPassengers></BookingDetails><ItineraryItems><ItineraryItem><ItemNo>3902660</ItemNo><SortingOrderNumber>2</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Car</SegmentTypeCode><PassengerDetails><PassengerNameList>MR TWO TEST</PassengerNameList></PassengerDetails><Supplier><SupplierType>Car Company</SupplierType><SupplierCode Label=\"Supplier Code\">ZI</SupplierCode><SupplierName>AVIS AUSTRALIA</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Pick Up</StartLabelValue><ServiceDate>Thu 20 Jul 17</ServiceDate><ServiceTime/><CityCode/><CityName/><LocationDescription>SFOO01</LocationDescription><Address><Address1>TAKE AIR TRAM FOR ADVANTAGE SHUTTLE</Address1><Address2/><Address3>SAN FRANCISCO</Address3><PostCode>94010</PostCode><State/><Country>United States</Country></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Drop Off</EndLabelValue><ServiceDate>Tue 25 Jul 17</ServiceDate><ServiceTime/><CityCode/><CityName>SFOO01</CityName><LocationDescription>SFO AIRPORT</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>Free of Charge</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. of Cars\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">5</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD250.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD250.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3897215</ItemNo><SourceReloc Label=\"Calypso ID\">01183U</SourceReloc><SortingOrderNumber>3</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Transfer</SegmentTypeCode><PassengerDetails><PassengerNameList>MR TWO TEST\n" +
      "MRS THREE TEST</PassengerNameList></PassengerDetails><Supplier><SupplierType>Transfer Company</SupplierType><SupplierCode Label=\"Supplier Code\">LORI</SupplierCode><SupplierName>LORI</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Transfer Type\">OW Seat in Coach Airport to Hotel</ServiceTypeDescription><Start><StartLabelValue>Pick Up</StartLabelValue><ServiceDate>Thu 20 Jul 17</ServiceDate><ServiceTime/><CityCode/><CityName>SFO</CityName><LocationDescription>SFO</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Drop Off</EndLabelValue><ServiceDate>Thu 20 Jul 17</ServiceDate><ServiceTime/><CityCode/><CityName>SFO</CityName><LocationDescription>SFO</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><ConfirmationNo Label=\"Confirmation No\">01183U</ConfirmationNo><AccountDetails><PaymentNarrative>Free of Charge</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes>ARRIVAL TRANSFER:\n" +
      "\n" +
      "\n" +
      "\n" +
      "Upon arrival at San Francisco International Airport follow the\n" +
      "\n" +
      "signs for \"Share Ride Vans\" to the information booth located on\n" +
      "\n" +
      "the arrival/baggage claim level of each terminal and read the\n" +
      "\n" +
      "display monitor for check in instructions. If instructed to see\n" +
      "\n" +
      "a \"curb coordinator\" check in with our Curb Services\n" +
      "\n" +
      "Representative in a green GO Lorries jacket standing outside at\n" +
      "\n" +
      "the Share Ride Van boarding zone or call us at (415) 334-9000\n" +
      "\n" +
      "to request a pick up. Free calls available from the courtesy\n" +
      "\n" +
      "telephones at the information booths.\n" +
      "\n" +
      "PASSENGER &amp; LUGGAGE RESTRICTIONS\n" +
      "\n" +
      "\n" +
      "\n" +
      "Luggage allowances are 2 standard-size suitcase and 1 small\n" +
      "\n" +
      "carry-on per person. No single piece of baggage or property\n" +
      "\n" +
      "weighing in excess of sixty (60) pounds will be accepted for\n" +
      "\n" +
      "transportation unless there is additional help available to aid\n" +
      "\n" +
      "the driver in both the loading and unloading of such baggage.\n" +
      "\n" +
      "\n" +
      "\n" +
      "If passengers have an excessive amount of checked luggage or\n" +
      "\n" +
      "multiple large boxes a private van will be required. Passenger\n" +
      "\n" +
      "will be required to pay the difference in cash between the cost\n" +
      "\n" +
      "of a private van and net rate of client issuing voucher.\n" +
      "\n" +
      "\n" +
      "\n" +
      "Go Lorries Airport Shuttle will not be liable for lost baggage\n" +
      "\n" +
      "since baggage is never removed from the passengers presence and\n" +
      "\n" +
      "is stored in the passenger compartment in which the passenger is\n" +
      "\n" +
      "riding. A passengers baggage remains, at all times, the\n" +
      "\n" +
      "responsibility of the passenger.\n" +
      "\n" +
      "\n" +
      "\n" +
      "Go Lorrie's does not provide child restraint seats. It is the\n" +
      "\n" +
      "responsibility of a child's parent or caregiver to provide the\n" +
      "\n" +
      "Child Restraint System when transporting children in van and/or\n" +
      "\n" +
      "taxi cabs.\n" +
      "\n" +
      "\n" +
      "\n" +
      "The child/restraint seat law applies to children that are less\n" +
      "\n" +
      "than 8 years of age, or that are less than 4 feet 9 inches in\n" +
      "\n" +
      "height.\n" +
      "\n" +
      "Reservations/Payments by Freestyle Travel PTY Limited trading as\n" +
      "\n" +
      "Freestyle Holidays</SegmentNotes></ItineraryNotes><NoOfItems Label=\"No. Of Passengers\">2</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD1550.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD1550.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3897216</ItemNo><SourceReloc Label=\"Calypso ID\">01183U</SourceReloc><SortingOrderNumber>4</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Hotel</SegmentTypeCode><PassengerDetails><PassengerNameList>MR TWO TEST\n" +
      "MRS THREE TEST</PassengerNameList></PassengerDetails><Supplier><SupplierType>Hotel Company</SupplierType><SupplierCode Label=\"Supplier Code\">HANL</SupplierCode><SupplierName>HANL</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Room Type\">Family Suite</ServiceTypeDescription><Start><StartLabelValue>Check In</StartLabelValue><ServiceDate>Thu 20 Jul 17</ServiceDate><ServiceTime>06:00</ServiceTime><CityCode/><CityName>SFO</CityName><LocationDescription>SFO</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Check Out</EndLabelValue><ServiceDate>Wed 26 Jul 17</ServiceDate><ServiceTime>12:00</ServiceTime><CityCode/><CityName>SFO</CityName><LocationDescription>SFO</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>OnRequest</Status><IssueDate></IssueDate><OrderDate/><ConfirmationNo Label=\"Confirmation No\">01183U</ConfirmationNo><AccountDetails><PaymentNarrative>Free of Charge</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes>RATE INCLUDES: Plated Full American Breakfast for 2, based on\n" +
      "\n" +
      "double occupancy per day served at the Daily Grill Restaurant.\n" +
      "\n" +
      "Additional Room occupants may order off the menu and is payable\n" +
      "\n" +
      "direct to the hotel. A prepaid breakfast coupon is available.\n" +
      "\n" +
      "\n" +
      "\n" +
      "Maximum 1 Breakfast per day for single occupancy.\n" +
      "\n" +
      "There is no children's menu. Room service orders are not\n" +
      "\n" +
      "applicable for this offer.\n" +
      "\n" +
      "Reservations/Payments by Freestyle Travel PTY Limited trading as\n" +
      "\n" +
      "Freestyle Holidays</SegmentNotes></ItineraryNotes><NoOfItems Label=\"No. of Rooms\">1</NoOfItems><NoOfdays UnitOfCount=\"Nights\" Label=\"Duration\">6</NoOfdays><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3902059</ItemNo><SortingOrderNumber>10</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Cruise</SegmentTypeCode><PassengerDetails><PassengerNameList/></PassengerDetails><Supplier><SupplierType>Cruise Company</SupplierType><SupplierCode Label=\"Supplier Code\">CARNIVAL</SupplierCode><SupplierName>CARNIVAL CRUISE LINES</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Cabin Type\">2 berth Upper with balc</ServiceTypeDescription><ServiceClassDescription Label=\"Cabin\">deck a</ServiceClassDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Wed 26 Jul 17</ServiceDate><ServiceTime>09:00</ServiceTime><CityCode/><CityName>SYD CITY</CityName><LocationDescription>SYD CITY</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Mon 31 Jul 17</ServiceDate><ServiceTime>09:00</ServiceTime><CityCode/><CityName>SYD CITY</CityName><LocationDescription>SYD CITY</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><FareBasis Label=\"Cruise Name\">Carnival South Pacific Cruise</FareBasis><CodeShare Label=\"Ship Name\">CARNIVAL LEGEND</CodeShare><AirlineReloc Label=\"Cabin No.\">081</AirlineReloc></FlightSpecificDetails><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><ConfirmationNo Label=\"Confirmation No\">confirmationNumber</ConfirmationNo><AccountDetails><PaymentNarrative>Free of Charge</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. Of Passengers\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">6</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD1200.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD1200.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3902061</ItemNo><SortingOrderNumber>11</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Misc</SegmentTypeCode><PassengerDetails><PassengerNameList/></PassengerDetails><Supplier><SupplierType>Misc Company</SupplierType><SupplierCode Label=\"Supplier Code\">DRIVE</SupplierCode><SupplierName>DRIVE AWAY HOLIDAYS</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Type\">Motorhome Hire</ServiceTypeDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Thu 27 Jul 17</ServiceDate><ServiceTime>09:00</ServiceTime><CityCode/><CityName>AKL</CityName><LocationDescription>AKL</LocationDescription><Address><Address1>address 1</Address1><Address2>address 2</Address2><Address3>Auckland</Address3><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Thu 27 Jul 17</ServiceDate><ServiceTime/><CityCode/><CityName>AKL</CityName><LocationDescription>AKL</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>Free of Charge</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription>24 hours cancellation notice required</CancellationDescription></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. of Units\">1</NoOfItems><NoOfdays UnitOfCount=\"Nights\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD1200.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD1200.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3902060</ItemNo><SortingOrderNumber>14</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Comment</SegmentTypeCode><PassengerDetails><PassengerNameList/></PassengerDetails><Supplier><SupplierType>Comment Company</SupplierType><SupplierName/><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Type\">Comment</ServiceTypeDescription><Start><StartLabelValue>Start</StartLabelValue><ServiceDate></ServiceDate><ServiceTime/><CityCode/><CityName/><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>End</EndLabelValue><ServiceDate></ServiceDate><ServiceTime/><CityCode/><CityName/><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status/><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes>This is a comment</SegmentNotes></ItineraryNotes><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3902667</ItemNo><SortingOrderNumber>15</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Train</SegmentTypeCode><PassengerDetails><PassengerNameList>MR TWO TEST\n" +
      "MRS THREE TEST</PassengerNameList></PassengerDetails><Supplier><SupplierType>Train Company</SupplierType><SupplierCode Label=\"Supplier Code\">TATRANSFE</SupplierCode><SupplierName>T/A TRANSFERS &amp; TOURS AUCKLAND</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Embark</StartLabelValue><ServiceDate>Thu 27 Jul 17</ServiceDate><ServiceTime/><CityCode/><CityName>MEL</CityName><LocationDescription>MEL</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Disembark</EndLabelValue><ServiceDate>Thu 27 Jul 17</ServiceDate><ServiceTime/><CityCode/><CityName>MEL</CityName><LocationDescription>MEL</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails/><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>Free of Charge</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription>24 hours cancellation notice required</CancellationDescription></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. of Units\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD150.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD150.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3902659</ItemNo><SortingOrderNumber>16</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Bus</SegmentTypeCode><PassengerDetails><PassengerNameList>MR TWO TEST\n" +
      "MRS THREE TEST</PassengerNameList></PassengerDetails><Supplier><SupplierType>Bus Company</SupplierType><SupplierCode Label=\"Supplier Code\">QFHOLS</SupplierCode><SupplierName>QANTAS HOLIDAYS</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Thu 27 Jul 17</ServiceDate><ServiceTime/><CityCode/><CityName>MEL</CityName><LocationDescription>MEL</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Thu 27 Jul 17</ServiceDate><ServiceTime/><CityCode/><CityName>MEL</CityName><LocationDescription>MEL</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>Pre-paid remark test cccf</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. Of Passengers\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD500.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD500.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3902666</ItemNo><SortingOrderNumber>17</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Ferry</SegmentTypeCode><PassengerDetails><PassengerNameList>MR TWO TEST\n" +
      "MRS THREE TEST\n" +
      "SIMPSON/OLIVIA MISS\n" +
      "VT/INFANT</PassengerNameList></PassengerDetails><Supplier><SupplierType>Ferry Company</SupplierType><SupplierCode Label=\"Supplier Code\">WILLF</SupplierCode><SupplierName>Williamstown Ferries</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Embark</StartLabelValue><ServiceDate>Thu 27 Jul 17</ServiceDate><ServiceTime/><CityCode/><CityName>MEL</CityName><LocationDescription>MEL</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Disembark</EndLabelValue><ServiceDate>Thu 27 Jul 17</ServiceDate><ServiceTime/><CityCode/><CityName>MEL</CityName><LocationDescription>MEL</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>Free of Charge</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. Of Passengers\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD300.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD300.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3906042</ItemNo><SourceReloc Label=\"PNR Reference\">TUYVPF</SourceReloc><SortingOrderNumber>18</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Flight</SegmentTypeCode><PassengerDetails><PassengerNameList>MR TWO TEST\n" +
      "MRS THREE TEST\n" +
      "SIMPSON/OLIVIA MISS\n" +
      "VT/INFANT\n" +
      "</PassengerNameList></PassengerDetails><Supplier><SupplierType>Flight Company</SupplierType><SupplierName>AIR NAMIBIA</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceIdentifier>SW0123</ServiceIdentifier><ServiceClassDescription Label=\"Class\">Y - Economy</ServiceClassDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Wed 27 Dec 17</ServiceDate><ServiceTime>09:00</ServiceTime><CityCode/><CityName>DFW</CityName><LocationDescription></LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Wed 27 Dec 17</ServiceDate><ServiceTime>10:20</ServiceTime><CityCode/><CityName>HOU</CityName><LocationDescription></LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><FlightSpecificMealsSeats>MR TWO TEST\n" +
      "Seat No 12A (Aisle - Forward)\n" +
      "Meal Requested ASIAN VEG.MEAL\n" +
      "\n" +
      "MRS THREE TEST\n" +
      "Seat No 22A (Window - Upper Deck)\n" +
      "Meal Requested ORIENTAL\n" +
      "\n" +
      "SIMPSON/OLIVIA MISS\n" +
      "BSCT\n" +
      "Meal Requested INF/BABY FOOD\n" +
      "\n" +
      "VT/INFANT\n" +
      "BSCT\n" +
      "Meal Requested INF/BABY FOOD\n" +
      "</FlightSpecificMealsSeats><Legs><LegsSummary>DFW (TERMINAL - ) HOU (TERMINAL - ), Dept Time 27-12-2017 09:00, Arrival Time 27-12-2017 10:20 - Meal Service: Snack\n" +
      "</LegsSummary></Legs></FlightSpecificDetails><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><RateInformation/></ItineraryItem></ItineraryItems><CostingsItems><CostingItem><ItemNo>2212465</ItemNo><SourceReloc Label=\"Calypso ID\">01183U</SourceReloc><SortingOrderNumber>1</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Package</SegmentTypeCode><Creditor><CreditorName>SI HOLIDAS</CreditorName></Creditor><CostingDescriptor/><IssueDate></IssueDate><StartCity/><StartCityName Label=\"Start City\"/><EndCity/><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>4654.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>4654.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2212466</ItemNo><SourceReloc Label=\"Calypso ID\">01183U</SourceReloc><SortingOrderNumber>2</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Transfer</SegmentTypeCode><Creditor><CreditorName>SI HOLIDAS</CreditorName></Creditor><CostingDescriptor/><IssueDate></IssueDate><ConfirmationNo Label=\"Confirmation No\">01183U</ConfirmationNo><StartServiceDate Label=\"Start Service Date\">20 Jul 17</StartServiceDate><StartCity>SFO</StartCity><StartCityName Label=\"Pick-Up City\">SFO</StartCityName><EndServiceDate Label=\"End Service Date\">20 Jul 17</EndServiceDate><EndCity>SFO</EndCity><EndCityName Label=\"Drop-off City\">SFO</EndCityName><CostingNotes/><CostingFinancials><BaseFare>1550.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>1550.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials><ServiceTypeDescription Label=\"Service Type\">OW Seat in Coach Airport to Hotel</ServiceTypeDescription></CostingItem><CostingItem><ItemNo>2212467</ItemNo><SourceReloc Label=\"Calypso ID\">01183U</SourceReloc><SortingOrderNumber>3</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Hotel</SegmentTypeCode><Creditor><CreditorName>SI HOLIDAS</CreditorName></Creditor><CostingDescriptor/><IssueDate></IssueDate><ConfirmationNo Label=\"Confirmation No\">01183U</ConfirmationNo><StartServiceDate Label=\"Start Service Date\">20 Jul 17</StartServiceDate><StartCity>SFO</StartCity><StartCityName Label=\"Address\">SFO</StartCityName><EndServiceDate Label=\"End Service Date\">26 Jul 17</EndServiceDate><EndCity>SFO</EndCity><CostingNotes/><CostingFinancials><BaseFare>1500.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>1500.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2218374</ItemNo><SortingOrderNumber>5</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode><Creditor><CreditorName>QF - Y - Economy Class</CreditorName></Creditor><TKTSpecific><AirlineCode/><TKTPassenger>SIMPSON/OLIVIA MISS</TKTPassenger><PassengerType>ADULT</PassengerType><ClassCode/></TKTSpecific><ShortItinerary Label=\"Short Itinerary\">DFW-HOU</ShortItinerary><CostingDescriptor/><IssueDate>23 Nov 17</IssueDate><ConfirmationNo Label=\"Ticket No\">45796714246</ConfirmationNo><StartServiceDate Label=\"Start Service Date\">27 Dec 17</StartServiceDate><StartCity/><StartCityName Label=\"Start City\"/><EndCity/><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>1500.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>1500.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials><CancellationDescription Label=\"Cancellation\">24 hours cancellation notice required</CancellationDescription></CostingItem><CostingItem><ItemNo>2215566</ItemNo><SortingOrderNumber>11</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Cruise</SegmentTypeCode><Creditor><CreditorName>CARNIVAL CRUISE LINES</CreditorName></Creditor><CostingDescriptor/><IssueDate></IssueDate><ConfirmationNo Label=\"Confirmation No\">confirmationNumber</ConfirmationNo><StartServiceDate Label=\"Start Service Date\">26 Jul 17</StartServiceDate><StartCity>SYD CITY</StartCity><StartCityName Label=\"Embark City\">SYD CITY</StartCityName><EndServiceDate Label=\"End Service Date\">31 Jul 17</EndServiceDate><EndCity>SYD CITY</EndCity><EndCityName Label=\"Disembark City\">SYD CITY</EndCityName><CostingNotes/><CostingFinancials><BaseFare>1090.91</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">109.09</GST><ClientDue>1200.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials><ServiceTypeDescription Label=\"Service Type\">2 berth Upper with balc</ServiceTypeDescription></CostingItem><CostingItem><ItemNo>2211842</ItemNo><SortingOrderNumber>13</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Service Fee</SegmentTypeCode><Creditor><CreditorName>xCTM CREDITOR</CreditorName></Creditor><CostingDescriptor>Booking Auto Service Fee</CostingDescriptor><IssueDate></IssueDate><StartCity/><StartCityName Label=\"Start City\"/><EndCity/><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>20.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>20.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2211843</ItemNo><SortingOrderNumber>14</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Service Fee</SegmentTypeCode><Creditor><CreditorName>xCTM CREDITOR</CreditorName></Creditor><CostingDescriptor>Auto service fee for GDS</CostingDescriptor><IssueDate></IssueDate><StartCity/><StartCityName Label=\"Start City\"/><EndCity/><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>20.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">2.00</GST><ClientDue>22.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2215567</ItemNo><SortingOrderNumber>16</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Misc</SegmentTypeCode><Creditor><CreditorName>DRIVE AWAY HOLIDAYS</CreditorName></Creditor><CostingDescriptor/><IssueDate></IssueDate><StartServiceDate Label=\"Start Service Date\">27 Jul 17</StartServiceDate><StartCity>AKL</StartCity><StartCityName Label=\"Start City\">AKL</StartCityName><EndServiceDate Label=\"End Service Date\">27 Jul 17</EndServiceDate><EndCity>AKL</EndCity><EndCityName Label=\"Finish City\">AKL</EndCityName><CostingNotes/><CostingFinancials><BaseFare>1200.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>1200.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials><CancellationDescription Label=\"Cancellation\">24 hours cancellation notice required</CancellationDescription><ServiceTypeDescription Label=\"Service Type\">Motorhome Hire</ServiceTypeDescription></CostingItem><CostingItem><ItemNo>2215967</ItemNo><SortingOrderNumber>17</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Bus</SegmentTypeCode><Creditor><CreditorName>QANTAS HOLIDAYS</CreditorName></Creditor><CostingDescriptor/><IssueDate></IssueDate><StartServiceDate Label=\"Start Service Date\">27 Jul 17</StartServiceDate><StartCity>MEL</StartCity><StartCityName Label=\"Start City\">MEL</StartCityName><EndServiceDate Label=\"End Service Date\">27 Jul 17</EndServiceDate><EndCity>MEL</EndCity><EndCityName Label=\"Finish City\">MEL</EndCityName><CostingNotes/><CostingFinancials><BaseFare>500.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>500.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2215968</ItemNo><SortingOrderNumber>18</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Car</SegmentTypeCode><Creditor><CreditorName>AVIS AUSTRALIA</CreditorName></Creditor><CostingDescriptor/><IssueDate></IssueDate><StartServiceDate Label=\"Start Service Date\">20 Jul 17</StartServiceDate><StartCity/><StartCityName Label=\"Pick-Up City\">SFOO01</StartCityName><EndServiceDate Label=\"End Service Date\">25 Jul 17</EndServiceDate><EndCity>SFOO01</EndCity><EndCityName Label=\"Drop-off City\">SFO AIRPORT</EndCityName><CostingNotes/><CostingFinancials><BaseFare>1250.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>1250.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2215973</ItemNo><SortingOrderNumber>19</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Ferry</SegmentTypeCode><Creditor><CreditorName>Williamstown Ferries</CreditorName></Creditor><CostingDescriptor/><IssueDate></IssueDate><StartServiceDate Label=\"Start Service Date\">27 Jul 17</StartServiceDate><StartCity>MEL</StartCity><StartCityName Label=\"Embark City\">MEL</StartCityName><EndServiceDate Label=\"End Service Date\">27 Jul 17</EndServiceDate><EndCity>MEL</EndCity><EndCityName Label=\"Disembark City\">MEL</EndCityName><CostingNotes/><CostingFinancials><BaseFare>300.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>300.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2215974</ItemNo><SortingOrderNumber>20</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Train</SegmentTypeCode><Creditor><CreditorName>T/A TRANSFERS &amp; TOURS AUCKLAND</CreditorName></Creditor><CostingDescriptor/><IssueDate></IssueDate><StartServiceDate Label=\"Start Service Date\">27 Jul 17</StartServiceDate><StartCity>MEL</StartCity><StartCityName Label=\"Embark City\">MEL</StartCityName><EndServiceDate Label=\"End Service Date\">27 Jul 17</EndServiceDate><EndCity>MEL</EndCity><EndCityName Label=\"Disembark City\">MEL</EndCityName><CostingNotes/><CostingFinancials><BaseFare>150.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>150.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials><CancellationDescription Label=\"Cancellation\">24 hours cancellation notice required</CancellationDescription></CostingItem><CostingItem><ItemNo>2218309</ItemNo><SortingOrderNumber>21</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Departure Tax</SegmentTypeCode><Creditor><CreditorName>QANTAS HOLIDAYS</CreditorName></Creditor><CostingDescriptor/><IssueDate></IssueDate><StartCity/><StartCityName Label=\"Start City\"/><EndCity/><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>175.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>175.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials><CancellationDescription Label=\"Cancellation\">Cancellation Terms DTX</CancellationDescription></CostingItem><CostingItem><ItemNo>2218310</ItemNo><SortingOrderNumber>22</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Foreign Exchange</SegmentTypeCode><Creditor><CreditorName>QBE TRAVEL INSURANCE</CreditorName></Creditor><CostingDescriptor>AUD 1.000000</CostingDescriptor><IssueDate></IssueDate><StartCity/><StartCityName Label=\"Start City\"/><EndCity/><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>2500.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>2500.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2218311</ItemNo><SortingOrderNumber>23</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Insurance</SegmentTypeCode><Creditor><CreditorName>COVER-MORE TRAVEL INSURANCE</CreditorName></Creditor><TicketType/><IssueDate></IssueDate><StartCity/><StartCityName Label=\"Start City\"/><EndCity/><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>1541.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.00</GST><ClientDue>1541.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials><CancellationDescription Label=\"Cancellation\">Cancellation Terms INS</CancellationDescription></CostingItem><DocumentTotals><TotalPayDirect><Total><BaseFare>0.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>0.00</ClientDue></Total></TotalPayDirect><TotalExcPayDirect><Total><BaseFare>17950.91</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>111.09</GST><ClientDue>18062.00</ClientDue></Total></TotalExcPayDirect><TotalIncPayDirect><Total><BaseFare>17950.91</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>111.09</GST><ClientDue>18062.00</ClientDue></Total></TotalIncPayDirect><TotalPaid>2000.00</TotalPaid><TotalOutstandingExcPayDirect>16062.00</TotalOutstandingExcPayDirect><Currency>AUD</Currency></DocumentTotals></CostingsItems></Booking></DocumentInformation>";

  String xml3 = "<DocumentInformation><Agency><AgencyName Label=\"TMC\">Tests Travel Management</AgencyName><AgencyCode Label=\"TMC Code\">trunk_tramada</AgencyCode><AgencyAddress><Address><Address1 Label=\"Address Line 1\">trunkctm</Address1><Address2 Label=\" Address Line 2\">Address 2</Address2><Address3 Label=\" Address Line 3\">Address 3</Address3><PostCode Label=\" Postcode\">2000</PostCode><State Label=\"State\">NSW</State><Country Label=\"Country\">Australia</Country></Address></AgencyAddress><AgencyContacts><AgencyEmail Label=\"Email: \">ashok@tramada.com</AgencyEmail><AgencyPhone Label=\"Phone: \"></AgencyPhone><AgencyFax Label=\"Fax No.: \"></AgencyFax></AgencyContacts></Agency><DocumentDetails><DocumentPrintDate>Tuesday 28 November 2017 12:14</DocumentPrintDate><DocumentPrintTime>Sydney, NSW</DocumentPrintTime><DocumentTitle Label=\"Type\">Itinerary &amp; Costing - Short</DocumentTitle><DocumentAddress><Address/></DocumentAddress><DocumentContactDetails><EmergencyContact Label=\"Emergency Contact: \">1800 663 622</EmergencyContact><AfterHours Label=\"After Hours Contact: \">1800 663 622</AfterHours><TollFree Label=\"Toll Free Number: \">1800 663 622</TollFree></DocumentContactDetails><DocumentCommercialDetails><LicNo Label=\"Licence No.: \">854</LicNo><ABN Label=\"ABN: \">52 005 000 895</ABN><ACN Label=\"ACN: \">005 000 895</ACN></DocumentCommercialDetails><DocumentHeaders><DocumentHeader HeaderTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually ThereÂ®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.\n" +
      "You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentHeader><DocumentHeader HeaderTitle=\"label\">Testing Helloworld Header</DocumentHeader></DocumentHeaders><DocumentFooters/><CarbonEmissionInformation/></DocumentDetails><Booking><BookingDetails><BookingNumber Label=\"Booking ID: \">B570373</BookingNumber><PNRDetails><PNRList/></PNRDetails><BookingConsultant1>agent</BookingConsultant1><BookingConsultant2/><TravelApprover/><BookedBy Label=\"Booked By: \">Sharyne Andrews</BookedBy><BookingDepartureDate Label=\"Departure Date: \">08 Dec 15</BookingDepartureDate><BookingReturnDate Label=\"Return Date: \">11 Dec 15</BookingReturnDate><BookingFinalTKTDate Label=\"Final TKT Date: \">06 Dec 16</BookingFinalTKTDate><BookingCancellationCondition/><BookingDeposits/><BookingDebtors><BookingDebtor><DebtorName Label=\"Debtor: \">KAL</DebtorName></BookingDebtor></BookingDebtors><BookingPassengers><BookingPassenger><PassengerName>LEONARD/GERARDMR</PassengerName><Memberships><Membership><MembershipNumber>FF QF 2045223</MembershipNumber></Membership></Memberships></BookingPassenger><TicketNumbers><Ticket><PassengerName Label=\"Passenger\">LEONARD/GERARDMR</PassengerName><TicketNumber Label=\"Ticket Number\">1213929269</TicketNumber><CarrierCode Label=\"Airline Code\">QF</CarrierCode><ShortItin Label=\"Itinerary Summary\">MEL-ADL-MEL</ShortItin><PassengerType Label=\"Passenger Type\">ADULT</PassengerType></Ticket></TicketNumbers><PassengerNameList Label=\"Itinerary For\">LEONARD/GERARDMR</PassengerNameList></BookingPassengers></BookingDetails><ItineraryItems><ItineraryItem><ItemNo>3868674</ItemNo><SortingOrderNumber>1</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Flight</SegmentTypeCode><PassengerDetails><PassengerNameList/></PassengerDetails><Supplier><SupplierType>Flight Company</SupplierType><SupplierName>QANTAS AIRWAYS</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Aircraft Type\">Boeing 737-800 (winglets)</ServiceTypeDescription><ServiceIdentifier>QF0675</ServiceIdentifier><ServiceClassDescription Label=\"Class\">S - Economy Red eDeal</ServiceClassDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Tue 08 Dec 15</ServiceDate><ServiceTime>08:15</ServiceTime><CityCode/><CityName>MEL</CityName><LocationDescription>1</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Tue 08 Dec 15</ServiceDate><ServiceTime>09:05</ServiceTime><CityCode/><CityName>ADL</CityName><LocationDescription>1</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><AirlineReloc Label=\"Airline Reloc\">67TM9D</AirlineReloc><FlightSpecificMealsSeats>LEONARD/GERARDMR\n" +
      "</FlightSpecificMealsSeats><Legs><LegsSummary>MEL (TERMINAL - 1) ADL (TERMINAL - 1), Dept Time 08-12-2015 08:15, Arrival Time 08-12-2015 09:05 - Travelling time: 1 hr 20 mins - Meal Service: Breakfast\n" +
      "</LegsSummary><Leg><StopOverLeg Label=\"Journey Info\">0</StopOverLeg><DepartureCity Label=\"Departure City\">MELBOURNE, AUSTRALIA</DepartureCity><DepartureTerminal Label=\"Departure Terminal\">1</DepartureTerminal><DepartureDate Label=\"Departure Date\">08-12-2015</DepartureDate><DepartureTime Label=\"Departure Time\">08:15</DepartureTime><ArrivalCity Label=\"Arrival City\">ADELAIDE, AUSTRALIA</ArrivalCity><ArrivalTerminal Label=\"Arrival Terminal\">1</ArrivalTerminal><ArrivalDate Label=\"Arrival Date\">08-12-2015</ArrivalDate><ArrivalTime Label=\"Arrival Time\">09:05</ArrivalTime><FlightTime Label=\"Flight Time\">01:20</FlightTime><Mileage Label=\"Mileage\">401</Mileage><MealServicesCode Label=\"Meal Services Code\">B</MealServicesCode></Leg></Legs></FlightSpecificDetails><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3868677</ItemNo><SortingOrderNumber>2</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Hotel</SegmentTypeCode><PassengerDetails><PassengerNameList/></PassengerDetails><Supplier><SupplierType>Hotel Company</SupplierType><SupplierCode Label=\"Supplier Code\">CPADL30012</SupplierCode><SupplierName>CROWNE PLAZA</SupplierName><SupplierAddress><Address><Address1>16 HINDMARSH SQUARE</Address1><Address2>ADELAIDE AU 5000 </Address2><Address3></Address3><PostCode/><State/><Country/></Address></SupplierAddress><SupplierPhone Label=\"Phone\">P-61-8-82068888</SupplierPhone><SupplierFax>F-61-8-82068800</SupplierFax></Supplier><Start><StartLabelValue>Check In</StartLabelValue><ServiceDate>Wed 09 Dec 15</ServiceDate><ServiceTime>14:00</ServiceTime><CityCode/><CityName>ADL</CityName><LocationDescription>ADL</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Check Out</EndLabelValue><ServiceDate>Fri 11 Dec 15</ServiceDate><ServiceTime>11:00</ServiceTime><CityCode/><CityName>ADL</CityName><LocationDescription>ADL</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><ConfirmationNo Label=\"Confirmation No\">66803405</ConfirmationNo><AccountDetails><PaymentNarrative>Charge to Client Account</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions>CXL AFTER 1800 09DEC FORFEIT FIRST NITE STAY</CancelConditions><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. of Rooms\">1</NoOfItems><NoOfdays UnitOfCount=\"Nights\" Label=\"Duration\">2</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD235.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD235.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3868675</ItemNo><SortingOrderNumber>3</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Flight</SegmentTypeCode><PassengerDetails><PassengerNameList/></PassengerDetails><Supplier><SupplierType>Flight Company</SupplierType><SupplierName>QANTAS AIRWAYS</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Aircraft Type\">Boeing 737-800 (winglets)</ServiceTypeDescription><ServiceIdentifier>QF0678</ServiceIdentifier><ServiceClassDescription Label=\"Class\">B - Economy Fully Flexible</ServiceClassDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Fri 11 Dec 15</ServiceDate><ServiceTime>09:45</ServiceTime><CityCode/><CityName>ADL</CityName><LocationDescription>1</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Fri 11 Dec 15</ServiceDate><ServiceTime>11:30</ServiceTime><CityCode/><CityName>MEL</CityName><LocationDescription>1</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><AirlineReloc Label=\"Airline Reloc\">67TM9D</AirlineReloc><FlightSpecificMealsSeats>LEONARD/GERARDMR\n" +
      "</FlightSpecificMealsSeats><Legs><LegsSummary>ADL (TERMINAL - 1) MEL (TERMINAL - 1), Dept Time 11-12-2015 09:45, Arrival Time 11-12-2015 11:30 - Travelling time: 1 hr 15 mins - Meal Service: Refreshment\n" +
      "</LegsSummary><Leg><StopOverLeg Label=\"Journey Info\">0</StopOverLeg><DepartureCity Label=\"Departure City\">ADELAIDE, AUSTRALIA</DepartureCity><DepartureTerminal Label=\"Departure Terminal\">1</DepartureTerminal><DepartureDate Label=\"Departure Date\">11-12-2015</DepartureDate><DepartureTime Label=\"Departure Time\">09:45</DepartureTime><ArrivalCity Label=\"Arrival City\">MELBOURNE, AUSTRALIA</ArrivalCity><ArrivalTerminal Label=\"Arrival Terminal\">1</ArrivalTerminal><ArrivalDate Label=\"Arrival Date\">11-12-2015</ArrivalDate><ArrivalTime Label=\"Arrival Time\">11:30</ArrivalTime><FlightTime Label=\"Flight Time\">01:15</FlightTime><Mileage Label=\"Mileage\">401</Mileage><MealServicesCode Label=\"Meal Services Code\">R</MealServicesCode></Leg></Legs></FlightSpecificDetails><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><RateInformation/></ItineraryItem></ItineraryItems><CostingsItems><CostingItem><ItemNo>2190194</ItemNo><SortingOrderNumber>1</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Hotel</SegmentTypeCode><Creditor><CreditorName>CROWNE PLAZA</CreditorName></Creditor><CostingDescriptor/><IssueDate></IssueDate><ConfirmationNo Label=\"Confirmation No\">66803405</ConfirmationNo><StartServiceDate Label=\"Start Service Date\">09 Dec 15</StartServiceDate><StartCity>ADL</StartCity><StartCityName Label=\"Address\">ADL</StartCityName><EndServiceDate Label=\"End Service Date\">11 Dec 15</EndServiceDate><EndCity>ADL</EndCity><CostingNotes/><CostingFinancials><BaseFare>427.27</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">42.73</GST><ClientDue>470.00</ClientDue><PaymentType>Pay Direct</PaymentType></CostingFinancials><CancelConditions Label=\"Cancel Conditions\">CXL AFTER 1800 09DEC FORFEIT FIRST NITE STAY</CancelConditions></CostingItem><CostingItem><ItemNo>2190193</ItemNo><SortingOrderNumber>2</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode><Creditor><CreditorName>QF - S - Economy Red eDeal</CreditorName></Creditor><TKTSpecific><AirlineCode/><TKTPassenger/><PassengerType>ADULT</PassengerType><ClassCode/></TKTSpecific><ShortItinerary Label=\"Short Itinerary\">MEL-ADL-MEL</ShortItinerary><CostingDescriptor/><IssueDate>07 Dec 15</IssueDate><ConfirmationNo Label=\"Ticket No\">1213929269</ConfirmationNo><StartServiceDate Label=\"Start Service Date\">08 Dec 15</StartServiceDate><StartCity/><StartCityName Label=\"Start City\"/><EndCity/><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>518.44</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>38.44</TaxesAndFeesExGST><GST Label=\"GST\">55.69</GST><ClientDue>612.57</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><DocumentTotals><TotalPayDirect><Total><BaseFare>427.27</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>42.73</GST><ClientDue>470.00</ClientDue></Total></TotalPayDirect><TotalExcPayDirect><Total><BaseFare>518.44</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>38.44</TaxesAndFeesExcGST><GST>55.69</GST><ClientDue>612.57</ClientDue></Total></TotalExcPayDirect><TotalIncPayDirect><Total><BaseFare>945.71</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>38.44</TaxesAndFeesExcGST><GST>98.42</GST><ClientDue>1082.57</ClientDue></Total></TotalIncPayDirect><TotalPaid>612.57</TotalPaid><TotalOutstandingExcPayDirect>0.00</TotalOutstandingExcPayDirect><Currency>AUD</Currency></DocumentTotals></CostingsItems></Booking></DocumentInformation>";

  String xml4 = "<DocumentInformation><Agency><AgencyName Label=\"TMC\">Tests Travel Management</AgencyName><AgencyCode Label=\"TMC Code\">publish_tramada</AgencyCode><AgencyAddress><Address><Address1 Label=\"Address Line 1\">trunkctm</Address1><Address2 Label=\" Address Line 2\">Address 2</Address2><Address3 Label=\" Address Line 3\">Address 3</Address3><PostCode Label=\" Postcode\">2000</PostCode><State Label=\"State\">NSW</State><Country Label=\"Country\">Australia</Country></Address></AgencyAddress><AgencyContacts><AgencyEmail Label=\"Email: \">ashok@tramada.com</AgencyEmail><AgencyPhone Label=\"Phone: \"></AgencyPhone><AgencyFax Label=\"Fax No.: \"></AgencyFax></AgencyContacts></Agency><DocumentDetails><DocumentPrintDate>Tuesday 28 November 2017 10:49</DocumentPrintDate><DocumentPrintTime>Sydney, NSW</DocumentPrintTime><DocumentTitle Label=\"Type\">Tramada AutoPublished Trip</DocumentTitle><DocumentAddress><Address><Address1 Label=\"Address Line 1\">21 GEORGE STREET</Address1><Address3 Label=\" Address Line 3\">SYDNEY</Address3><PostCode Label=\" Postcode\">2000</PostCode><State Label=\"State\">NSW</State><Country Label=\"Country\">AU</Country></Address></DocumentAddress><DocumentContactDetails><EmergencyContact Label=\"Emergency Contact: \">020000000</EmergencyContact><AfterHours Label=\"After Hours Contact: \">020000000</AfterHours><TollFree Label=\"Toll Free Number: \">1300 000 000</TollFree></DocumentContactDetails><DocumentCommercialDetails><LicNo Label=\"Licence No.: \">lic1</LicNo><ABN Label=\"ABN: \">52 005 000 895</ABN><ACN Label=\"ACN: \">005 000 895</ACN><MerchantNo Label=\"Merchant No: \">3333863706</MerchantNo><ATAS Label=\"ATAS Accreditation Number: \">atas1</ATAS></DocumentCommercialDetails><DocumentHeaders/><DocumentFooters/><CarbonEmissionInformation/></DocumentDetails><Booking><BookingDetails><BookingNumber Label=\"Booking ID: \">B578411auto</BookingNumber><PNRDetails><PNRList/></PNRDetails><BookingConsultant1>tconsultant</BookingConsultant1><BookingConsultant2/><TravelApprover/><BookingDepartureDate Label=\"Departure Date: \">30 Apr 18</BookingDepartureDate><BookingReturnDate Label=\"Return Date: \">30 Apr 18</BookingReturnDate><BookingFinalTKTDate Label=\"Final TKT Date: \">25 Sep 17</BookingFinalTKTDate><BookingCancellationCondition/><BookingDeposits/><BookingDebtors><BookingDebtor><DebtorName Label=\"Debtor: \">AVANT</DebtorName></BookingDebtor></BookingDebtors><BookingPassengers><BookingPassenger><PassengerName>BASTOCK/KATHY MRS</PassengerName></BookingPassenger><BookingPassenger><PassengerName>KAALZ/TEST MR</PassengerName></BookingPassenger><TicketNumbers><Ticket><PassengerName Label=\"Passenger\">KAALZ/TEST MR</PassengerName><TicketNumber Label=\"Ticket Number\">1083735176</TicketNumber><CarrierCode Label=\"Airline Code\">ZL</CarrierCode><ShortItin Label=\"Itinerary Summary\">SYD-MIM</ShortItin><PassengerType Label=\"Passenger Type\">ADULT</PassengerType></Ticket></TicketNumbers><PassengerNameList Label=\"Itinerary For\">BASTOCK/KATHY MRS, KAALZ/TEST MR, KAALZ/TEST MR</PassengerNameList></BookingPassengers></BookingDetails><ItineraryItems><ItineraryItem><ItemNo>3903631</ItemNo><SortingOrderNumber>1</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Car</SegmentTypeCode><PassengerDetails><PassengerNameList>KAALZ/TEST MR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Car Company</SupplierType><SupplierCode Label=\"Supplier Code\">SX</SupplierCode><SupplierName>SIXT AUTO</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Car Type\">Compact Car</ServiceTypeDescription><Start><StartLabelValue>Pick Up</StartLabelValue><ServiceDate>Sat 08 Apr 17</ServiceDate><ServiceTime>09:00</ServiceTime><CityCode/><CityName/><LocationDescription>MEL</LocationDescription><Address><Address1>2 TARMAC DRIVE</Address1><Address2>SHUTTLE SERVICE</Address2><Address3>TULLAMARINE</Address3><PostCode>3043</PostCode><State>VIC</State><Country>Australia</Country></Address><Contacts><PhoneNo>P-0061 393303529</PhoneNo><FaxNo/></Contacts></Start><End><EndLabelValue>Drop Off</EndLabelValue><ServiceDate>Sun 09 Apr 17</ServiceDate><ServiceTime>18:00</ServiceTime><CityCode/><CityName/><LocationDescription>MEL</LocationDescription><Address><Address1>2 TARMAC DRIVE</Address1><Address2>SHUTTLE SERVICE</Address2><Address3>TULLAMARINE</Address3><PostCode>3043</PostCode><State>VIC</State><Country>Australia</Country></Address><Contacts><PhoneNo>P-0061 393303529</PhoneNo><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><ConfirmationNo Label=\"Confirmation No\">9841356099</ConfirmationNo><AccountDetails><PaymentNarrative>All Charges</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions>AUD42.33 UNL WD</Inclusions><Exclusions/><RateGuaranteeDescription>Rate Guarantee</RateGuaranteeDescription><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. of Cars\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">2</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD84.66</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD84.66</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3903630</ItemNo><SortingOrderNumber>2</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Hotel</SegmentTypeCode><PassengerDetails><PassengerNameList>KAALZ/TEST MR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Hotel Company</SupplierType><SupplierCode Label=\"Supplier Code\">SYD-PA001</SupplierCode><SupplierName>PARK HYATT SYDNEY</SupplierName><SupplierAddress><Address><Address1>7 HICKSON RD THE ROCKS</Address1><Address2>SYDNEY AU 2000</Address2><Address3/><PostCode/><State>NSW</State><Country>Australia</Country></Address></SupplierAddress><SupplierPhone Label=\"Phone\">P-61-2-92411234</SupplierPhone><SupplierFax>F-61-2-92561555</SupplierFax></Supplier><ServiceTypeDescription Label=\"Room Type\">1WK</ServiceTypeDescription><Start><StartLabelValue>Check In</StartLabelValue><ServiceDate>Sun 09 Apr 17</ServiceDate><ServiceTime>06:00</ServiceTime><CityCode/><CityName>SYD</CityName><LocationDescription>SYD</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Check Out</EndLabelValue><ServiceDate>Mon 10 Apr 17</ServiceDate><ServiceTime>02:00</ServiceTime><CityCode/><CityName>SYD</CityName><LocationDescription>SYD</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><ConfirmationNo Label=\"Confirmation No\">HY0036918959</ConfirmationNo><AccountDetails><PaymentNarrative>All Charges</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription>Cancel 48 hours prior to arrival</CancellationDescription></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. of Rooms\">1</NoOfItems><NoOfdays UnitOfCount=\"Nights\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD930.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD930.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3903629</ItemNo><SortingOrderNumber>3</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Flight</SegmentTypeCode><PassengerDetails><PassengerNameList>BASTOCK/KATHY MRS\n" +
      "KAALZ/TEST MR\n" +
      "</PassengerNameList></PassengerDetails><Supplier><SupplierType>Flight Company</SupplierType><SupplierName>REGIONAL EXPRESS</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Aircraft Type\">Saab SF340A/B</ServiceTypeDescription><ServiceIdentifier>ZL0133</ServiceIdentifier><ServiceClassDescription Label=\"Class\">Y - Rex Flex</ServiceClassDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Mon 30 Apr 18</ServiceDate><ServiceTime>14:20</ServiceTime><CityCode/><CityName>SYD</CityName><LocationDescription>TERMINAL 2 DOMESTIC</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Mon 30 Apr 18</ServiceDate><ServiceTime>15:30</ServiceTime><CityCode/><CityName>MIM</CityName><LocationDescription></LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><AirlineReloc Label=\"Airline Reloc\">KVFLYS</AirlineReloc><FlightSpecificMealsSeats>BASTOCK/KATHY MRS\n" +
      "Aisle - Side Block\n" +
      "Meal Requested UNKNOWN\n" +
      "\n" +
      "KAALZ/TEST MR\n" +
      "</FlightSpecificMealsSeats><Legs><LegsSummary>SYD (TERMINAL - TERMINAL 2 DOMESTIC) MIM (TERMINAL - ), Dept Time 30-04-2018 14:20, Arrival Time 30-04-2018 15:30 - Travelling time: 1 hr 10 mins\n" +
      "</LegsSummary><Leg><StopOverLeg Label=\"Journey Info\">0</StopOverLeg><DepartureCity Label=\"Departure City\">Kingsford Smith, Sydney</DepartureCity><DepartureTerminal Label=\"Departure Terminal\">TERMINAL 2 DOMESTIC</DepartureTerminal><DepartureDate Label=\"Departure Date\">30-04-2018</DepartureDate><DepartureTime Label=\"Departure Time\">14:20</DepartureTime><ArrivalCity Label=\"Arrival City\">MERIMBULA, AUSTRALIA</ArrivalCity><ArrivalDate Label=\"Arrival Date\">30-04-2018</ArrivalDate><ArrivalTime Label=\"Arrival Time\">15:30</ArrivalTime><FlightTime Label=\"Flight Time\">1:10</FlightTime><Mileage Label=\"Mileage\">218</Mileage></Leg></Legs></FlightSpecificDetails><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><RateInformation/></ItineraryItem></ItineraryItems><CostingsItems><CostingItem><ItemNo>2216640</ItemNo><SortingOrderNumber>1</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode><Creditor><CreditorName>ZL - Y - Rex Flex</CreditorName></Creditor><TKTSpecific><AirlineCode/><TKTPassenger>KAALZ/TEST MR</TKTPassenger><PassengerType>ADULT</PassengerType><ClassCode/></TKTSpecific><ShortItinerary Label=\"Short Itinerary\">SYD-MIM</ShortItinerary><CostingDescriptor/><IssueDate>22 Sep 17</IssueDate><ConfirmationNo Label=\"Ticket No\">1083735176</ConfirmationNo><StartServiceDate Label=\"Start Service Date\">30 Apr 18</StartServiceDate><StartCity/><StartCityName Label=\"Start City\"/><EndCity/><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>390.00</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>24.55</TaxesAndFeesExGST><GST Label=\"GST\">41.45</GST><ClientDue>456.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2216639</ItemNo><SortingOrderNumber>2</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Car</SegmentTypeCode><Creditor><CreditorName>SIXT AUTO</CreditorName></Creditor><CostingDescriptor/><IssueDate></IssueDate><ConfirmationNo Label=\"Confirmation No\">9841356099</ConfirmationNo><StartServiceDate Label=\"Start Service Date\">08 Apr 17</StartServiceDate><StartCity/><StartCityName Label=\"Pick-Up City\">MEL</StartCityName><EndServiceDate Label=\"End Service Date\">09 Apr 17</EndServiceDate><EndCity/><EndCityName Label=\"Drop-off City\">MEL</EndCityName><CostingNotes/><CostingFinancials><BaseFare>76.96</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>7.70</TaxesAndFeesExGST><GST Label=\"GST\">8.47</GST><ClientDue>93.13</ClientDue><PaymentType>Pay Direct</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2216638</ItemNo><SortingOrderNumber>3</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Hotel</SegmentTypeCode><Creditor><CreditorName>PARK HYATT SYDNEY</CreditorName></Creditor><CostingDescriptor/><IssueDate></IssueDate><ConfirmationNo Label=\"Confirmation No\">HY0036918959</ConfirmationNo><StartServiceDate Label=\"Start Service Date\">09 Apr 17</StartServiceDate><StartCity>SYD</StartCity><StartCityName Label=\"Address\">SYD</StartCityName><EndServiceDate Label=\"End Service Date\">10 Apr 17</EndServiceDate><EndCity>SYD</EndCity><CostingNotes/><CostingFinancials><BaseFare>845.45</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">84.55</GST><ClientDue>930.00</ClientDue><PaymentType>Pay Direct</PaymentType></CostingFinancials><CancellationDescription Label=\"Cancellation\">Cancel 48 hours prior to arrival</CancellationDescription></CostingItem><DocumentTotals><TotalPayDirect><Total><BaseFare>922.41</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>7.70</TaxesAndFeesExcGST><GST>93.02</GST><ClientDue>1023.13</ClientDue></Total></TotalPayDirect><TotalExcPayDirect><Total><BaseFare>390.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>24.55</TaxesAndFeesExcGST><GST>41.45</GST><ClientDue>456.00</ClientDue></Total></TotalExcPayDirect><TotalIncPayDirect><Total><BaseFare>1312.41</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>32.25</TaxesAndFeesExcGST><GST>134.47</GST><ClientDue>1479.13</ClientDue></Total></TotalIncPayDirect><TotalPaid>456.00</TotalPaid><TotalOutstandingExcPayDirect>0.00</TotalOutstandingExcPayDirect><Currency>AUD</Currency></DocumentTotals></CostingsItems></Booking></DocumentInformation>";


  String xml5 = "<DocumentInformation>\n" +
      "  <Agency>\n" +
      "    <AgencyName Label=\"TMC\">Tramada Travel</AgencyName>\n" +
      "    <AgencyCode Label=\"TMC Code\">trunk_tramada</AgencyCode>\n" +
      "    <AgencyAddress>\n" +
      "      <Address>\n" +
      "        <Address1 Label=\"Address Line 1\">61 Salamanca Place</Address1>\n" +
      "        <Address2 Label=\" Address Line 2\">Hobart</Address2>\n" +
      "        <PostCode Label=\" Postcode\">7000</PostCode>\n" +
      "        <State Label=\"State\">TAS</State>\n" +
      "        <Country Label=\"Country\">Australia</Country>\n" +
      "      </Address>\n" +
      "    </AgencyAddress>\n" +
      "    <AgencyContacts>\n" +
      "      <AgencyEmail Label=\"Email: \">test@tramada.com</AgencyEmail>\n" +
      "      <AgencyPhone Label=\"Phone: \">03 6221 3499</AgencyPhone>\n" +
      "      <AgencyFax Label=\"Fax No.: \"/>\n" +
      "    </AgencyContacts>\n" +
      "  </Agency>\n" +
      "  <DocumentDetails>\n" +
      "    <DocumentPrintDate>Tuesday 28 November 2017 11:51</DocumentPrintDate>\n" +
      "    <DocumentPrintTime>Sydney, NSW</DocumentPrintTime>\n" +
      "    <DocumentTitle Label=\"Type\">Itinerary &amp; Costing - Full</DocumentTitle>\n" +
      "    <DocumentAddress>\n" +
      "      <Address/>\n" +
      "    </DocumentAddress>\n" +
      "    <DocumentContactDetails>\n" +
      "      <EmergencyContact Label=\"Emergency Contact: \">0409 245 508</EmergencyContact>\n" +
      "      <AfterHours Label=\"After Hours Contact: \">0409 245 508</AfterHours>\n" +
      "      <Phone Label=\"Phone: \">03 6221 3499</Phone>\n" +
      "    </DocumentContactDetails>\n" +
      "    <DocumentCommercialDetails>\n" +
      "      <ABN Label=\"ABN: \">62 065 009 887</ABN>\n" +
      "      <ATAS Label=\"ATAS Accreditation Number: \">A10616</ATAS>\n" +
      "    </DocumentCommercialDetails>\n" +
      "    <DocumentHeaders>\n" +
      "      <DocumentHeader HeaderTitle=\"CORRECT NAMES\">It is important that you check the spelling of all passenger names on this booking. It is necessary that all passenger names are as per your passport or a valid form of photo identification. Failure to advise your Travel Specialist immediately of any incorrect name spelling can result in further charges to make the required amendments, or ultimately in you being denied boarding by the relevant travel provider.</DocumentHeader>\n" +
      "    </DocumentHeaders>\n" +
      "    <DocumentFooters/>\n" +
      "    <CarbonEmissionInformation>\n" +
      "      <EmissionTonnage Label=\"Tonnes CO2e\">Flight Carbon Emissions: 0.26 Tonnes CO2e.</EmissionTonnage>\n" +
      "    </CarbonEmissionInformation>\n" +
      "  </DocumentDetails>\n" +
      "  <Booking>\n" +
      "    <BookingDetails>\n" +
      "      <BookingNumber Label=\"Booking ID: \">B90835</BookingNumber>\n" +
      "      <PNRDetails>\n" +
      "        <PNRList/>\n" +
      "      </PNRDetails>\n" +
      "      <BookingConsultant1>Anne Bourke</BookingConsultant1>\n" +
      "\t  <BookingConsultant1EmailAddress>anne@tramadatravel.com.au</BookingConsultant1EmailAddress>\n" +
      "      <BookingConsultant2/>\n" +
      "      <TravelApprover/>\n" +
      "      <BookingDepartureDate Label=\"Departure Date: \">11 Aug 17</BookingDepartureDate>\n" +
      "      <BookingVisaStatus Label=\"Visa\">Already acquired</BookingVisaStatus>\n" +
      "      <BookingFinalTKTDate Label=\"Final TKT Date: \"/>\n" +
      "      <BookingCancellationCondition/>\n" +
      "      <BookingDeposits/>\n" +
      "      <BookingDebtors>\n" +
      "        <BookingDebtor>\n" +
      "          <DebtorName Label=\"Debtor: \">OMEGAGG</DebtorName>\n" +
      "        </BookingDebtor>\n" +
      "      </BookingDebtors>\n" +
      "      <BookingPassengers>\n" +
      "        <BookingPassenger>\n" +
      "          <PassengerName>ABBOTT/JOHN MR</PassengerName>\n" +
      "        </BookingPassenger>\n" +
      "        <BookingPassenger>\n" +
      "          <PassengerName>B</PassengerName>\n" +
      "        </BookingPassenger>\n" +
      "        <TicketNumbers>\n" +
      "          <Ticket>\n" +
      "            <PassengerName Label=\"Passenger\">ABBOTT/JOHN MR</PassengerName>\n" +
      "            <TicketNumber Label=\"Ticket Number\">258963</TicketNumber>\n" +
      "            <CarrierCode Label=\"Airline Code\">YY</CarrierCode>\n" +
      "            <PassengerType Label=\"Passenger Type\">ADULT</PassengerType>\n" +
      "          </Ticket>\n" +
      "        </TicketNumbers>\n" +
      "        <PassengerNameList Label=\"Itinerary For\">ABBOTT/JOHN MR, ABBOTT/JOHN MR, B</PassengerNameList>\n" +
      "      </BookingPassengers>\n" +
      "    </BookingDetails>\n" +
      "    <ItineraryItems>\n" +
      "      <ItineraryItem>\n" +
      "        <ItemNo>760669</ItemNo>\n" +
      "        <SortingOrderNumber>1</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"Segment Type\">Flight</SegmentTypeCode>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerNameList>ABBOTT/JOHN MR ABBOTT/JOHN MR</PassengerNameList>\n" +
      "        </PassengerDetails>\n" +
      "        <Supplier>\n" +
      "          <SupplierType>Flight Company</SupplierType>\n" +
      "          <SupplierName>QANTAS AIRWAYS</SupplierName>\n" +
      "          <SupplierAddress>\n" +
      "            <Address>\n" +
      "              <Address1/>\n" +
      "              <Address2/>\n" +
      "              <Address3/>\n" +
      "              <PostCode/>\n" +
      "              <State/>\n" +
      "              <Country/>\n" +
      "            </Address>\n" +
      "          </SupplierAddress>\n" +
      "          <SupplierFax/>\n" +
      "        </Supplier>\n" +
      "        <ServiceTypeDescription Label=\"Aircraft Type\">76W</ServiceTypeDescription>\n" +
      "        <ServiceIdentifier>QF1850</ServiceIdentifier>\n" +
      "        <ServiceClassDescription Label=\"Class\">Economy</ServiceClassDescription>\n" +
      "        <Start>\n" +
      "          <StartLabelValue>Departure</StartLabelValue>\n" +
      "          <ServiceDate>Sat 25 Nov 17</ServiceDate>\n" +
      "          <ServiceTime/>\n" +
      "          <CityCode/>\n" +
      "          <CityName>SYDNEY, AUSTRALIA</CityName>\n" +
      "          <LocationDescription/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </Start>\n" +
      "        <End>\n" +
      "          <EndLabelValue>Arrival</EndLabelValue>\n" +
      "          <ServiceDate>Sat 25 Nov 17</ServiceDate>\n" +
      "          <ServiceTime/>\n" +
      "          <CityCode/>\n" +
      "          <CityName>MELBOURNE, AUSTRALIA</CityName>\n" +
      "          <LocationDescription/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </End>\n" +
      "        <FlightSpecificDetails>\n" +
      "          <BaggageAllowance Label=\"Baggage Allowance\">1 Piece at 23KG</BaggageAllowance>\n" +
      "          <FlightSpecificMealsSeats>ABBOTT/JOHN MR ABBOTT/JOHN MR</FlightSpecificMealsSeats>\n" +
      "          <Legs>\n" +
      "            <LegsSummary>SYDNEY, AUSTRALIA (TERMINAL - ) MELBOURNE, AUSTRALIA (TERMINAL - ), Dept Time 25-11-2017 - Meal Service: Lunch MELBOURNE, AUSTRALIA (TERMINAL - ) MELBOURNE, AUSTRALIA (TERMINAL - ), Dept Time 25-11-2017 - Meal Service: Breakfast</LegsSummary>\n" +
      "            <Leg>\n" +
      "              <StopOverLeg Label=\"Journey Info\">0</StopOverLeg>\n" +
      "              <DepartureCity Label=\"Departure City\">SYDNEY, AUSTRALIA</DepartureCity>\n" +
      "              <DepartureDate Label=\"Departure Date\">25-11-2017</DepartureDate>\n" +
      "              <ArrivalCity Label=\"Arrival City\">MELBOURNE, AUSTRALIA</ArrivalCity>\n" +
      "              <MealServicesCode Label=\"Meal Services Code\">L</MealServicesCode>\n" +
      "              <MealServicesDescription Label=\"Meal Services Description\">Lunch</MealServicesDescription>\n" +
      "            </Leg>\n" +
      "            <Leg>\n" +
      "              <StopOverLeg Label=\"Stop Over\">1</StopOverLeg>\n" +
      "              <DepartureCity Label=\"Departure City\">MELBOURNE, AUSTRALIA</DepartureCity>\n" +
      "              <DepartureDate Label=\"Departure Date\">25-11-2017</DepartureDate>\n" +
      "              <ArrivalCity Label=\"Arrival City\">MELBOURNE, AUSTRALIA</ArrivalCity>\n" +
      "              <MealServicesCode Label=\"Meal Services Code\">B</MealServicesCode>\n" +
      "              <MealServicesDescription Label=\"Meal Services Description\">Breakfast</MealServicesDescription>\n" +
      "            </Leg>\n" +
      "          </Legs>\n" +
      "        </FlightSpecificDetails>\n" +
      "        <Status>Confirmed</Status>\n" +
      "        <IssueDate/>\n" +
      "        <OrderDate/>\n" +
      "        <AccountDetails>\n" +
      "          <PaymentNarrative/>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription/>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <RateInformation/>\n" +
      "      </ItineraryItem>\n" +
      "    </ItineraryItems>\n" +
      "    <CostingsItems>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>382773</ItemNo>\n" +
      "        <SortingOrderNumber>1</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>YY - Economy</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode/>\n" +
      "          <TKTPassenger>ABBOTT/JOHN MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode/>\n" +
      "        </TKTSpecific>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>28 Sep 17</IssueDate>\n" +
      "        <ConfirmationNo Label=\"Ticket No\">258963</ConfirmationNo>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>-150.00</BaseFare>\n" +
      "          <Discount Label=\"Comm\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>-150.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>382778</ItemNo>\n" +
      "        <SortingOrderNumber>3</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>YY - Economy</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode/>\n" +
      "          <TKTPassenger>ABBOTT/JOHN MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode/>\n" +
      "        </TKTSpecific>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>28 Sep 17</IssueDate>\n" +
      "        <ConfirmationNo Label=\"Ticket No\">258963</ConfirmationNo>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>150.00</BaseFare>\n" +
      "          <Discount Label=\"Comm\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>150.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>382779</ItemNo>\n" +
      "        <SortingOrderNumber>4</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>YY - Economy</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode/>\n" +
      "          <TKTPassenger>ABBOTT/JOHN MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode/>\n" +
      "        </TKTSpecific>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>28 Sep 17</IssueDate>\n" +
      "        <ConfirmationNo Label=\"Ticket No\">258963</ConfirmationNo>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>150.00</BaseFare>\n" +
      "          <Discount Label=\"Comm\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>150.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>382780</ItemNo>\n" +
      "        <SortingOrderNumber>5</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>YY - Economy</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode/>\n" +
      "          <TKTPassenger>ABBOTT/JOHN MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode/>\n" +
      "        </TKTSpecific>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>28 Sep 17</IssueDate>\n" +
      "        <ConfirmationNo Label=\"Ticket No\">258963</ConfirmationNo>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>150.00</BaseFare>\n" +
      "          <Discount Label=\"Comm\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>150.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>382781</ItemNo>\n" +
      "        <SortingOrderNumber>6</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>YY - Economy</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode/>\n" +
      "          <TKTPassenger>ABBOTT/JOHN MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode/>\n" +
      "        </TKTSpecific>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>28 Sep 17</IssueDate>\n" +
      "        <ConfirmationNo Label=\"Ticket No\">258963</ConfirmationNo>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>150.00</BaseFare>\n" +
      "          <Discount Label=\"Comm\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>150.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>382782</ItemNo>\n" +
      "        <SortingOrderNumber>7</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>YY - Economy</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode/>\n" +
      "          <TKTPassenger>ABBOTT/JOHN MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode/>\n" +
      "        </TKTSpecific>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>28 Sep 17</IssueDate>\n" +
      "        <ConfirmationNo Label=\"Ticket No\">258963</ConfirmationNo>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>150.00</BaseFare>\n" +
      "          <Discount Label=\"Comm\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>150.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>382783</ItemNo>\n" +
      "        <SortingOrderNumber>8</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>YY - Economy</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode/>\n" +
      "          <TKTPassenger>ABBOTT/JOHN MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode/>\n" +
      "        </TKTSpecific>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>28 Sep 17</IssueDate>\n" +
      "        <ConfirmationNo Label=\"Ticket No\">258963</ConfirmationNo>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>150.00</BaseFare>\n" +
      "          <Discount Label=\"Comm\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>150.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>382784</ItemNo>\n" +
      "        <SortingOrderNumber>9</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>YY - Economy</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode/>\n" +
      "          <TKTPassenger>ABBOTT/JOHN MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode/>\n" +
      "        </TKTSpecific>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>28 Sep 17</IssueDate>\n" +
      "        <ConfirmationNo Label=\"Ticket No\">258963</ConfirmationNo>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>150.00</BaseFare>\n" +
      "          <Discount Label=\"Comm\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>150.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>382785</ItemNo>\n" +
      "        <SortingOrderNumber>10</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>YY - Economy</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode/>\n" +
      "          <TKTPassenger>ABBOTT/JOHN MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode/>\n" +
      "        </TKTSpecific>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>28 Sep 17</IssueDate>\n" +
      "        <ConfirmationNo Label=\"Ticket No\">258963</ConfirmationNo>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>150.00</BaseFare>\n" +
      "          <Discount Label=\"Comm\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>150.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>382786</ItemNo>\n" +
      "        <SortingOrderNumber>11</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>YY - Economy</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode/>\n" +
      "          <TKTPassenger>ABBOTT/JOHN MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode/>\n" +
      "        </TKTSpecific>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>28 Sep 17</IssueDate>\n" +
      "        <ConfirmationNo Label=\"Ticket No\">258963</ConfirmationNo>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>150.00</BaseFare>\n" +
      "          <Discount Label=\"Comm\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>150.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>382787</ItemNo>\n" +
      "        <SortingOrderNumber>12</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>YY - Economy</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode/>\n" +
      "          <TKTPassenger>ABBOTT/JOHN MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode/>\n" +
      "        </TKTSpecific>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>28 Sep 17</IssueDate>\n" +
      "        <ConfirmationNo Label=\"Ticket No\">258963</ConfirmationNo>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>150.00</BaseFare>\n" +
      "          <Discount Label=\"Comm\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>150.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>382788</ItemNo>\n" +
      "        <SortingOrderNumber>13</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>YY - Economy</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode/>\n" +
      "          <TKTPassenger>ABBOTT/JOHN MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode/>\n" +
      "        </TKTSpecific>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>28 Sep 17</IssueDate>\n" +
      "        <ConfirmationNo Label=\"Ticket No\">258963</ConfirmationNo>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>150.00</BaseFare>\n" +
      "          <Discount Label=\"Comm\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>150.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>382789</ItemNo>\n" +
      "        <SortingOrderNumber>14</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>YY - Economy</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode/>\n" +
      "          <TKTPassenger>ABBOTT/JOHN MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode/>\n" +
      "        </TKTSpecific>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>28 Sep 17</IssueDate>\n" +
      "        <ConfirmationNo Label=\"Ticket No\">258963</ConfirmationNo>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>150.00</BaseFare>\n" +
      "          <Discount Label=\"Comm\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>150.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>382790</ItemNo>\n" +
      "        <SortingOrderNumber>15</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>YY - Economy</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode/>\n" +
      "          <TKTPassenger>ABBOTT/JOHN MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode/>\n" +
      "        </TKTSpecific>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>28 Sep 17</IssueDate>\n" +
      "        <ConfirmationNo Label=\"Ticket No\">258963</ConfirmationNo>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>150.00</BaseFare>\n" +
      "          <Discount Label=\"Comm\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>150.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>382791</ItemNo>\n" +
      "        <SortingOrderNumber>16</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>YY - Economy</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode/>\n" +
      "          <TKTPassenger>ABBOTT/JOHN MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode/>\n" +
      "        </TKTSpecific>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>28 Sep 17</IssueDate>\n" +
      "        <ConfirmationNo Label=\"Ticket No\">258963</ConfirmationNo>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>150.00</BaseFare>\n" +
      "          <Discount Label=\"Comm\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>150.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>382792</ItemNo>\n" +
      "        <SortingOrderNumber>17</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"Segment Type\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>YY - Economy</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode/>\n" +
      "          <TKTPassenger>ABBOTT/JOHN MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode/>\n" +
      "        </TKTSpecific>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>28 Sep 17</IssueDate>\n" +
      "        <ConfirmationNo Label=\"Ticket No\">258963</ConfirmationNo>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>150.00</BaseFare>\n" +
      "          <Discount Label=\"Comm\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>150.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <DocumentTotals>\n" +
      "        <TotalPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>0.00</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST>\n" +
      "            <GST>0.00</GST>\n" +
      "            <ClientDue>0.00</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalPayDirect>\n" +
      "        <TotalExcPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>2100.00</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST>\n" +
      "            <GST>0.00</GST>\n" +
      "            <ClientDue>2100.00</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalExcPayDirect>\n" +
      "        <TotalIncPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>2100.00</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST>\n" +
      "            <GST>0.00</GST>\n" +
      "            <ClientDue>2100.00</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalIncPayDirect>\n" +
      "        <TotalPaid>1800.00</TotalPaid>\n" +
      "        <TotalOutstandingExcPayDirect>300.00</TotalOutstandingExcPayDirect>\n" +
      "        <Currency>AUD</Currency>\n" +
      "      </DocumentTotals>\n" +
      "    </CostingsItems>\n" +
      "  </Booking>\n" +
      "</DocumentInformation>\n";

  String xml6 = "<DocumentInformation><Agency><AgencyName Label=\"TMC\">Tests Travel Management</AgencyName><AgencyCode Label=\"TMC Code\">trunk_tramada</AgencyCode><AgencyAddress><Address><Address1 Label=\"Address Line 1\">trunkctm</Address1><Address2 Label=\" Address Line 2\">Address 2</Address2><Address3 Label=\" Address Line 3\">Address 3</Address3><PostCode Label=\" Postcode\">2000</PostCode><State Label=\"State\">NSW</State><Country Label=\"Country\">Australia</Country></Address></AgencyAddress><AgencyContacts><AgencyEmail Label=\"Email: \">ashok@tramada.com</AgencyEmail><AgencyPhone Label=\"Phone: \">0470632287</AgencyPhone><AgencyFax Label=\"Fax No.: \">0470632287</AgencyFax></AgencyContacts></Agency><DocumentDetails><DocumentPrintDate>Tuesday 05 December 2019 14:55</DocumentPrintDate><DocumentPrintTime>Sydney, NSW</DocumentPrintTime><DocumentTitle Label=\"Type\">Itinerary - Short</DocumentTitle><DocumentAddress><Address><Address1 Label=\"Address Line 1\">ADDRESSADDRESSADDRESSADDRESS</Address1><Address2 Label=\" Address Line 2\">ADDRESSADDRESSADDRES1</Address2><Address3 Label=\" Address Line 3\">city testtttt</Address3><PostCode Label=\" Postcode\">2077</PostCode><State Label=\"State\">NSW</State><Country Label=\"Country\">AU</Country></Address></DocumentAddress><DocumentContactDetails><EmergencyContact Label=\"Emergency Contact: \">020000000</EmergencyContact><AfterHours Label=\"After Hours Contact: \">020000000</AfterHours><TollFree Label=\"Toll Free Number: \">1300 000 000</TollFree><Phone Label=\"Phone: \">0470632287</Phone><Fax Label=\"Fax: \">0470632287</Fax></DocumentContactDetails><DocumentCommercialDetails><LicNo Label=\"Licence No.: \">lic1</LicNo><ABN Label=\"ABN: \">52 005 000 895</ABN><ACN Label=\"ACN: \">005 000 895</ACN><MerchantNo Label=\"Merchant No: \">3333863706</MerchantNo><ATAS Label=\"ATAS Accreditation Number: \">atas1</ATAS></DocumentCommercialDetails><DocumentHeaders><DocumentHeader HeaderTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually There®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.\n" +
      "You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentHeader><DocumentHeader HeaderTitle=\"label\">Testing Helloworld Header</DocumentHeader></DocumentHeaders><DocumentFooters/><CarbonEmissionInformation><EmissionTonnage Label=\"Tonnes CO2e\">Flight Carbon Emissions: 0.26 Tonnes CO2e. </EmissionTonnage></CarbonEmissionInformation></DocumentDetails><Booking><BookingDetails><BookingNumber Label=\"Booking ID: \">B579182</BookingNumber><PNRDetails><PNRList/></PNRDetails><BookingConsultant1>agent</BookingConsultant1><BookingConsultant1EmailAddress>agency@test.com</BookingConsultant1EmailAddress><BookingConsultant2/><TravelApprover/><BookingDepartureDate Label=\"Departure Date: \">14 Nov 19</BookingDepartureDate><BookingFinalTKTDate Label=\"Final TKT Date: \"></BookingFinalTKTDate><BookingCancellationCondition/><BookingDeposits/><BookingDebtors><BookingDebtor><DebtorName Label=\"Debtor: \">SCB DEBTOR</DebtorName><DepartmentName Label=\"Department: \">GTM</DepartmentName><CostCentreName Label=\"Cost Centre: \">32323232323232</CostCentreName></BookingDebtor></BookingDebtors><BookingPassengers><BookingPassenger><PassengerName>BASTOCK</PassengerName></BookingPassenger><BookingPassenger><PassengerName>BASTOCK/KENNETH MR</PassengerName></BookingPassenger><BookingPassenger><PassengerName>BASTOCK/STUART MR</PassengerName></BookingPassenger><BookingPassenger><PassengerName>LH/FH MR</PassengerName></BookingPassenger><TicketNumbers/><PassengerNameList Label=\"Itinerary For\">BASTOCK, BASTOCK/KENNETH MR, BASTOCK/STUART MR, LH/FH MR</PassengerNameList></BookingPassengers></BookingDetails><ItineraryItems><ItineraryItem><ItemNo>3906394</ItemNo><SortingOrderNumber>1</SortingOrderNumber><SegmentTypeCode Label=\"COMMENT\">Comment</SegmentTypeCode><PassengerDetails><PassengerNameList/></PassengerDetails><Supplier><SupplierType>Comment Company</SupplierType><SupplierName/><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Type\">TEST COMMENT</ServiceTypeDescription><Start><StartLabelValue>Start</StartLabelValue><ServiceDate>Tue 21 Nov 19</ServiceDate><ServiceTime>11:00</ServiceTime><CityCode/><CityName/><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>End</EndLabelValue><ServiceDate>Tue 21 Nov 19</ServiceDate><ServiceTime>19:00</ServiceTime><CityCode/><CityName/><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status/><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3906397</ItemNo><SortingOrderNumber>2</SortingOrderNumber><SegmentTypeCode Label=\"TOUR\">Tour</SegmentTypeCode><PassengerDetails><PassengerNameList>BASTOCK, BASTOCK/KENNETH MR, BASTOCK/STUART MR, LH/FH MR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Tour Company</SupplierType><SupplierName>TESTCREDITOR**+-</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Wed 22 Nov 19</ServiceDate><ServiceTime/><CityCode>SYD</CityCode><CityName>Kingsford Smith, Sydney</CityName><LocationDescription>KINGSFORD SMITH, SYDNEY</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Wed 22 Nov 19</ServiceDate><ServiceTime/><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><LocationDescription>MELBOURNE, AUSTRALIA</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>Cash</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription>24 hours cancellation notice required</CancellationDescription></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. Of Passengers\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD100.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD100.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3906400</ItemNo><SortingOrderNumber>3</SortingOrderNumber><SegmentTypeCode Label=\"MISC\">Leisure</SegmentTypeCode><PassengerDetails><PassengerNameList/></PassengerDetails><Supplier><SupplierType>Leisure Company</SupplierType><SupplierName>BEACH CLUB RESORT MOOLOOLABA</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Tue 28 Nov 19</ServiceDate><ServiceTime/><CityCode>SYD</CityCode><CityName>Kingsford Smith, Sydney</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Tue 28 Nov 19</ServiceDate><ServiceTime/><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>Pre-Paid</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription>24 hours cancellation notice required</CancellationDescription></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. of Units\">1</NoOfItems><NoOfdays UnitOfCount=\"Nights\" Label=\"Duration\">1</NoOfdays><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3906393</ItemNo><SortingOrderNumber>4</SortingOrderNumber><SegmentTypeCode Label=\"CAR\">Car</SegmentTypeCode><PassengerDetails><PassengerNameList/></PassengerDetails><Supplier><SupplierType>Car Company</SupplierType><SupplierName>Test Agency</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Pick Up</StartLabelValue><ServiceDate>Tue 28 Nov 19</ServiceDate><ServiceTime/><CityCode>SYD</CityCode><CityName>Kingsford Smith, Sydney</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Drop Off</EndLabelValue><ServiceDate>Tue 28 Nov 19</ServiceDate><ServiceTime/><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>Cash</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. of Cars\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3906401</ItemNo><SortingOrderNumber>5</SortingOrderNumber><SegmentTypeCode Label=\"TRANSFER\">Transfer</SegmentTypeCode><PassengerDetails><PassengerNameList>LH/FH MR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Transfer Company</SupplierType><SupplierName>TABCORP PARK</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Pick Up</StartLabelValue><ServiceDate>Wed 29 Nov 19</ServiceDate><ServiceTime/><CityCode>SYD</CityCode><CityName>Kingsford Smith, Sydney</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Drop Off</EndLabelValue><ServiceDate>Wed 29 Nov 19</ServiceDate><ServiceTime/><CityCode>MEL DOM APT</CityCode><CityName>MEL DOM APT</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>Pre-Paid</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription>24 hours cancellation notice required</CancellationDescription></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. Of Passengers\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD200.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD200.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3906391</ItemNo><SortingOrderNumber>6</SortingOrderNumber><SegmentTypeCode Label=\"FLIGHT\">Flight</SegmentTypeCode><PassengerDetails><PassengerNameList>BASTOCK, BASTOCK/KENNETH MR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Flight Company</SupplierType><SupplierName>CATHAY PACIFIC AIRWAYS</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceIdentifier/><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Thu 07 Dec 19</ServiceDate><ServiceTime/><CityCode>SYD</CityCode><CityName>Kingsford Smith, Sydney</CityName><LocationDescription></LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Thu 07 Dec 19</ServiceDate><ServiceTime/><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><LocationDescription></LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><FlightSpecificMealsSeats>BASTOCK\n" +
      "Aisle - Forward\n" +
      "Meal Requested BLAND MEAL\n" +
      "|BASTOCK/KENNETH MR\n" +
      "Aisle - Upper Deck\n" +
      "Meal Requested KOSHER MEAL\n" +
      "</FlightSpecificMealsSeats><Legs><LegsSummary>Kingsford Smith, Sydney (TERMINAL - ) MELBOURNE, AUSTRALIA (TERMINAL - ), Dept Time 07-12-2017\n" +
      "</LegsSummary><Leg><StopOverLeg Label=\"Journey Info\">0</StopOverLeg><DepartureCity Label=\"Departure City\">Kingsford Smith, Sydney</DepartureCity><DepartureDate Label=\"Departure Date\">07-12-2017</DepartureDate><ArrivalCity Label=\"Arrival City\">MELBOURNE, AUSTRALIA</ArrivalCity></Leg></Legs></FlightSpecificDetails><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3906396</ItemNo><SortingOrderNumber>7</SortingOrderNumber><SegmentTypeCode Label=\"TRAIN\">Train</SegmentTypeCode><PassengerDetails><PassengerNameList>LH/FH MR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Train Company</SupplierType><SupplierName>KIMBA COMMUNITY HOTEL MOTEL</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Embark</StartLabelValue><ServiceDate>Fri 08 Dec 19</ServiceDate><ServiceTime/><CityCode>LEARMONTH</CityCode><CityName>LEARMONTH</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Disembark</EndLabelValue><ServiceDate>Fri 08 Dec 19</ServiceDate><ServiceTime/><CityCode>NA</CityCode><CityName>NA</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails/><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>Pre-Paid</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription>24 hours cancellation notice required</CancellationDescription></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. of Units\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD200.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD200.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3906395</ItemNo><SortingOrderNumber>8</SortingOrderNumber><SegmentTypeCode Label=\"CRUISE\">Cruise</SegmentTypeCode><PassengerDetails><PassengerNameList>LH/FH MR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Cruise Company</SupplierType><SupplierName>NEW LODGE MOTEL</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Fri 15 Dec 19</ServiceDate><ServiceTime/><CityCode>ADELAIDE AIPORT</CityCode><CityName>ADELAIDE AIPORT</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Fri 15 Dec 19</ServiceDate><ServiceTime/><CityCode>PERTH CITY</CityCode><CityName>PERTH CITY</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails/><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>Pre-Paid</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. Of Passengers\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD100.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD100.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3906443</ItemNo><SortingOrderNumber>9</SortingOrderNumber><SegmentTypeCode Label=\"HOTEL\">Hotel</SegmentTypeCode><PassengerDetails><PassengerNameList>BASTOCK, BASTOCK/KENNETH MR, BASTOCK/STUART MR, LH/FH MR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Hotel Company</SupplierType><SupplierName>Test Agency</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Check In</StartLabelValue><ServiceDate>Fri 15 Dec 19</ServiceDate><ServiceTime>06:00</ServiceTime><CityCode>SYD</CityCode><CityName>Kingsford Smith, Sydney</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Check Out</EndLabelValue><ServiceDate>Sat 16 Dec 19</ServiceDate><ServiceTime>02:00</ServiceTime><CityCode/><CityName/><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>Pre-Paid</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. of Rooms\">1</NoOfItems><NoOfdays UnitOfCount=\"Nights\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD120.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD120.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3906392</ItemNo><SortingOrderNumber>10</SortingOrderNumber><SegmentTypeCode Label=\"FERRY\">Ferry</SegmentTypeCode><PassengerDetails><PassengerNameList>LH/FH MR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Ferry Company</SupplierType><SupplierName>Test Creditor</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Embark</StartLabelValue><ServiceDate>Fri 29 Dec 19</ServiceDate><ServiceTime/><CityCode>NADI AIRPORT</CityCode><CityName>NADI AIRPORT</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Disembark</EndLabelValue><ServiceDate>Fri 29 Dec 19</ServiceDate><ServiceTime/><CityCode>SIN</CityCode><CityName>SINGAPORE, SINGAPORE</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>nan</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription>Cancel 4 Hours Prior to Arrival</CancellationDescription></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. Of Passengers\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD250.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD250.00</LocalRate></RateInformation></ItineraryItem></ItineraryItems><CostingsItems><DocumentTotals><TotalPayDirect><Total><BaseFare>0.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>0.00</ClientDue></Total></TotalPayDirect><TotalExcPayDirect><Total><BaseFare>0.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>0.00</ClientDue></Total></TotalExcPayDirect><TotalIncPayDirect><Total><BaseFare>0.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>0.00</ClientDue></Total></TotalIncPayDirect><TotalPaid>0.00</TotalPaid><TotalOutstandingExcPayDirect>0.00</TotalOutstandingExcPayDirect><Currency>AUD</Currency></DocumentTotals></CostingsItems></Booking></DocumentInformation>";

  String xml7 = "<DocumentInformation><Agency><AgencyName Label=\"TMC\">Tests Travel Management</AgencyName><AgencyCode Label=\"TMC Code\">trunk_tramada</AgencyCode><AgencyAddress><Address><Address1 Label=\"Address Line 1\">trunkctm</Address1><Address2 Label=\" Address Line 2\">Address 2</Address2><Address3 Label=\" Address Line 3\">Address 3</Address3><PostCode Label=\" Postcode\">2000</PostCode><State Label=\"State\">NSW</State><Country Label=\"Country\">Australia</Country></Address></AgencyAddress><AgencyContacts><AgencyEmail Label=\"Email: \">ashok@tramada.com</AgencyEmail><AgencyPhone Label=\"Phone: \">0470632287</AgencyPhone><AgencyFax Label=\"Fax No.: \">0470632287</AgencyFax></AgencyContacts></Agency><DocumentDetails><DocumentPrintDate>Tuesday 05 December 2017 12:16</DocumentPrintDate><DocumentPrintTime>Sydney, NSW</DocumentPrintTime><DocumentTitle Label=\"Type\">Itinerary - Full</DocumentTitle><DocumentAddress><Address/></DocumentAddress><DocumentContactDetails><EmergencyContact Label=\"Emergency Contact: \">1800 663 622</EmergencyContact><AfterHours Label=\"After Hours Contact: \">1800 663 622</AfterHours><TollFree Label=\"Toll Free Number: \">1800 663 622</TollFree><Phone Label=\"Phone: \">0470632287</Phone><Fax Label=\"Fax: \">0470632287</Fax></DocumentContactDetails><DocumentCommercialDetails><LicNo Label=\"Licence No.: \">854</LicNo><ABN Label=\"ABN: \">52 005 000 895</ABN><ACN Label=\"ACN: \">005 000 895</ACN></DocumentCommercialDetails><DocumentHeaders><DocumentHeader HeaderTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually There®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.\n" +
      "You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentHeader><DocumentHeader HeaderTitle=\"Third party hotel booking\">Please note: your hotel has been booked via a third party provider. They have provided a confirmation number and this may be required at check-in. In most instances you will also have been provided with a confirmation and payment voucher separately to the itinerary. Please ensure you have printed a copy of this voucher and are able to produce it upon request. There can be occasional communication problems between third party bookers and hotels. If you experience any difficulties at check-in please call CTM on the numbers below.</DocumentHeader><DocumentHeader HeaderTitle=\"label\">Testing Helloworld Header</DocumentHeader><DocumentHeader HeaderTitle=\"CTM EMERGENCY AFTER HOURS\">Should you require assistance with booking changes or amendments relating to imminent departures, please contact our emergency afterhours service on the following numbers:\n" +
      "\n" +
      "Within Australia - 1800 663 622\n" +
      "\n" +
      "Outside Australia - +61 7 3211 2400\n" +
      "\n" +
      "DUE TO PRIVACY LAWS WE ARE UNABLE TO RELEASE TRAVEL DETAILS TO ANYONE OTHER THAN THE PASSENGER.</DocumentHeader></DocumentHeaders><DocumentFooters><DocumentFooter FooterTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually There®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.\n" +
      "You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentFooter><DocumentFooter FooterTitle=\"Assistance while overseas\">Subsea 7 provide assistance in emergencies arising when personnel are travelling overseas. Comprehensive emergency assistance is available to Employees through International SOS, these cover Medical Services and Security.\n" +
      "\n" +
      "The following information is required for International SOS:\n" +
      "\n" +
      "SOS Programme Number: 14ACPA000020\n" +
      "\n" +
      "Employer: Subsea 7\n" +
      "\n" +
      "24h phone number: +61 2 9372 2468\n" +
      "\n" +
      "www.internationalsos.com</DocumentFooter><DocumentFooter FooterTitle=\"DOMESTIC BAGGAGE (JQ/QF/DJ)\">JETSTAR\n" +
      "\n" +
      "Economy Starter fare includes 10kg of carry- on luggage, customise your requirements at time of booking.\n" +
      "\n" +
      "\n" +
      "\n" +
      "http://www.jetstar.com/au/en/planning-and-booking/baggage/checked-baggage\n" +
      "\n" +
      "QANTAS\n" +
      "\n" +
      "Economy Class: 1 piece of cabin baggage weighing 7kg + 1 piece of checked baggage weighing maximum 23kg.\n" +
      "\n" +
      "Business Class: 1 piece of cabin baggage weighing 7kg + 1 piece of checked baggage weighing 32kg per piece.\n" +
      "\n" +
      "Silver/Gold/Platinum frequent flyers receive extra baggage concessions.\n" +
      "\n" +
      "http://www.qantas.com.au/travel/airlines/checked-baggage/global/en#jump0\n" +
      "\n" +
      "VIRGIN AUSTRALIA\n" +
      "\n" +
      "Economy Class Saver Fare no allowance: prepaid baggage up to 23kg will cost a flat rate of $12 per flight.\n" +
      "\n" +
      "Cabin baggage - Up to 7kgs complimentary.\n" +
      "\n" +
      "NOTE - If paid at check-in up to 23kg will cost a flat rate of $20 per flight.\n" +
      "\n" +
      "Velocity Gold Members: free extended baggage limit of 32kg checked baggage.\n" +
      "\n" +
      "Business Class/Premium Economy: permitted 69kg of checked baggage.\n" +
      "\n" +
      "http://www.virginaustralia.com/Personal/Flightinfo/BeforeYouFly/Baggagedangerousgoods/index.htm</DocumentFooter><DocumentFooter FooterTitle=\"DOMESTIC CHECK IN (JQ/QF/DJ)\">JETSTAR\n" +
      "\n" +
      "Check-in for Jetstar flights leaving from any domestic Australian destination is a minimum (recommended 60 minutes) before your scheduled flight. Check in opens 2 hours prior to the scheduled flights and closes 30 minutes prior to scheduled departure time.\n" +
      "\n" +
      "http://www.jetstar.com/au/en/planning-and-booking/checking-in/web-check-in\n" +
      "\n" +
      "QANTAS\n" +
      "\n" +
      "QANTAS domestic flights check-in closes 30 minutes prior to scheduled departure time - except for flights numbered QF2000-QF2299 and QF7000-QF7299 departing from Sydney, check-in time is 1 hour. Online check-in is now available for QANTAS and QANTAS Link Australian domestic bookings between 24 hours and 1 hour before your flight departure.\n" +
      "\n" +
      "http://www.qantas.com.au/info/flying/atTheAirport/checkinTimes\n" +
      "\n" +
      "VIRGIN AUSTRALIA\n" +
      "\n" +
      "Virgin Australia domestic flights check-in closes 30 minutes prior to scheduled departure time. Virgin Blue online check-in is available and opens 24 hours before departure of your flights. Passengers wanting to utilise this facility must present a print out of your boarding pass at check in.\n" +
      "\n" +
      "http://www.virginaustralia.com/Personal/Services/Check-inoptions/index.htm</DocumentFooter><DocumentFooter FooterTitle=\"DOMESTIC FARE CONDITIONS (JQ/QF/DJ)\">JETSTAR - http://travelctm.com/uploads/JQFareGrid.pdf\n" +
      "\n" +
      "QANTAS - http://travelctm.com/uploads/QFDomesticandNZFareGrid.pdf\n" +
      "\n" +
      "VIRGIN AUSTRALIA - http://travelctm.com/uploads/DJFaregrid.pdf\n" +
      "\n" +
      "NOTE: Business Class Virgin Australia only offered on A330-200 aircraft (subject to change).</DocumentFooter><DocumentFooter FooterTitle=\"E-TICKET\">E ticket identification required by airlines:\n" +
      "\n" +
      "Photo identification that has been issued by the Commonwealth of Australia or an Australian State or Territory. Acceptable photo identification includes: drivers license, passport, any state, territory or federal government issued card, company issued identification, or a student card. Qantas acceptable non photo identification includes: credit card used to pay for the ticket, other credit or debit cards, social security card, QF club or frequent flyer card, certified copy/original of a birth of citizenship document. All ID?s must be current and valid.</DocumentFooter><DocumentFooter FooterTitle=\"FREQUENT FLYER\">* Please advise your airline loyalty number if applicable. Once received we will enter this into your booking and your profile for future.\n" +
      "\n" +
      "* To ensure you receive your frequent flyer points, please retain tickets and boarding passes where possible until your points have appeared on your frequent flyer statement.\n" +
      "\n" +
      "* Should your points not appear on your statement, please send copies of your boarding passes, tickets and supporting information to the relevant airline.</DocumentFooter><DocumentFooter FooterTitle=\"IATA\">Transportation and other services provided by the carrier are subject to conditions of contract and other important notices which are delivered with this\n" +
      "itinerary/receipt and form part of the nearest office of the issuing airline or travel agent to obtain a copy prior to the commencement of your trip. Notice if\n" +
      "the passenger's journey involves an ultimate destination or stop in a country other than the country of departure the Warsaw convention may be applicable\n" +
      "and the convention governs and in most cases limits the liability of carriers for death or personal injury and in respect of loss or damage to baggage.</DocumentFooter><DocumentFooter FooterTitle=\"ITINERARY ACCEPTANCE\">Whilst all care has been taken to ensure your travel requirements are booked as per your request, CTM requests that you accept responsibility for double checking these details, and advise of any changes that need to be made as soon as possible. Upon receiving your itinerary please check and make sure the relevant booking is correct and as per your travel needs.</DocumentFooter></DocumentFooters><CarbonEmissionInformation/></DocumentDetails><Booking><BookingDetails><BookingNumber Label=\"Booking ID: \">B579185</BookingNumber><PNRDetails><PNRList/></PNRDetails><BookingConsultant1>agent</BookingConsultant1><BookingConsultant1EmailAddress>shusma1@tramada.com</BookingConsultant1EmailAddress><BookingConsultant2/><TravelApprover/><BookedBy Label=\"Booked By: \">Sharyne Andrews</BookedBy><BookingDepartureDate Label=\"Departure Date: \">31 Jan 18</BookingDepartureDate><BookingReturnDate Label=\"Return Date: \">05 Feb 18</BookingReturnDate><BookingFinalTKTDate Label=\"Final TKT Date: \">01 Dec 17</BookingFinalTKTDate><BookingCancellationCondition/><BookingDeposits/><BookingDebtors><BookingDebtor><DebtorName Label=\"Debtor: \">KAL</DebtorName></BookingDebtor></BookingDebtors><BookingPassengers><BookingPassenger><PassengerName>KAALZ/TEST MR</PassengerName></BookingPassenger><BookingPassenger><PassengerName>LEONARD/GERARDMR</PassengerName><Memberships><Membership><MembershipNumber>2045223</MembershipNumber><MembershipCompanyName>QANTAS AIRWAYS</MembershipCompanyName><MembershipCompanyCode>QF</MembershipCompanyCode><MembershipProgramName>QF</MembershipProgramName><MembershipProgramCode>QF</MembershipProgramCode></Membership></Memberships></BookingPassenger><BookingPassenger><PassengerName>MR MARCO POLO</PassengerName></BookingPassenger><BookingPassenger><PassengerName>MRS MARCH POLO</PassengerName></BookingPassenger><TicketNumbers/><PassengerNameList Label=\"Itinerary For\">KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList></BookingPassengers></BookingDetails><ItineraryItems><ItineraryItem><ItemNo>3906415</ItemNo><SortingOrderNumber>1</SortingOrderNumber><SegmentTypeCode Label=\"TRAIN\">Train</SegmentTypeCode><PassengerDetails><PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Train Company</SupplierType><SupplierName>Test Agency</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Embark</StartLabelValue><ServiceDate>Fri 01 Dec 17</ServiceDate><ServiceTime/><CityCode>SYD</CityCode><CityName>Kingsford Smith, Sydney</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Disembark</EndLabelValue><ServiceDate>Fri 01 Dec 17</ServiceDate><ServiceTime/><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails/><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>Free of Charge</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription>24 hours cancellation notice required</CancellationDescription></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. of Units\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD120.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD120.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3906413</ItemNo><SortingOrderNumber>2</SortingOrderNumber><SegmentTypeCode Label=\"CAR\">Car</SegmentTypeCode><PassengerDetails><PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList></PassengerDetails><Supplier><SupplierType>Car Company</SupplierType><SupplierName>V Australia</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Pick Up</StartLabelValue><ServiceDate>Sat 09 Dec 17</ServiceDate><ServiceTime/><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Drop Off</EndLabelValue><ServiceDate>Mon 11 Dec 17</ServiceDate><ServiceTime/><CityCode>MEL APRT</CityCode><CityName>MEL APRT</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>All Charges</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription>Cancel 10 Hours Prior to Arrival</CancellationDescription></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. of Cars\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">2</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD200.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD200.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3906414</ItemNo><SortingOrderNumber>3</SortingOrderNumber><SegmentTypeCode Label=\"BUS\">Bus</SegmentTypeCode><PassengerDetails><PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList></PassengerDetails><Supplier><SupplierType>Bus Company</SupplierType><SupplierName/><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Sat 09 Dec 17</ServiceDate><ServiceTime/><CityCode>SYD</CityCode><CityName>Kingsford Smith, Sydney</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Sat 09 Dec 17</ServiceDate><ServiceTime/><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>nan</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. Of Passengers\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD100.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD100.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3906412</ItemNo><SortingOrderNumber>4</SortingOrderNumber><SegmentTypeCode Label=\"FLIGHT\">Flight</SegmentTypeCode><PassengerDetails><PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Flight Company</SupplierType><SupplierName>REGIONAL EXPRESS</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Aircraft Type\">Saab SF340A/B</ServiceTypeDescription><ServiceIdentifier>ZL0773</ServiceIdentifier><ServiceClassDescription Label=\"Class\">Y - Rex Flex</ServiceClassDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Wed 31 Jan 18</ServiceDate><ServiceTime>11:45 AM</ServiceTime><CityCode>SYD</CityCode><CityName>Kingsford Smith, Sydney</CityName><LocationDescription>TERMINAL 2 DOMESTIC</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Wed 31 Jan 18</ServiceDate><ServiceTime>1:10 PM</ServiceTime><CityCode>ABX</CityCode><CityName>ALBURY, AUSTRALIA</CityName><LocationDescription></LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><AirlineReloc Label=\"Airline Reloc\">RFWAFO</AirlineReloc><BaggageAllowance Label=\"Baggage Allowance\">23 kilos</BaggageAllowance><FlightSpecificMealsSeats>KAALZ/TEST MR\n" +
      "Seat No 28A (Aisle)\n" +
      "Meal Requested LOW CALORIE\n" +
      "|LEONARD/GERARDMR\n" +
      "Seat No 23C (Sleeper Seat)\n" +
      "Meal Requested NO SALT MEAL\n" +
      "</FlightSpecificMealsSeats><Legs><LegsSummary>Kingsford Smith, Sydney (TERMINAL - TERMINAL 2 DOMESTIC) ALBURY, AUSTRALIA (TERMINAL - ), Dept Time 31-01-2018 11:45, Arrival Time 31-01-2018 13:10 - Travelling time: 1 hr 25 mins\n" +
      "</LegsSummary><Leg><StopOverLeg Label=\"Journey Info\">0</StopOverLeg><DepartureCity Label=\"Departure City\">Kingsford Smith, Sydney</DepartureCity><DepartureTerminal Label=\"Departure Terminal\">TERMINAL 2 DOMESTIC</DepartureTerminal><DepartureDate Label=\"Departure Date\">31-01-2018</DepartureDate><DepartureTime Label=\"Departure Time\">11:45</DepartureTime><ArrivalCity Label=\"Arrival City\">ALBURY, AUSTRALIA</ArrivalCity><ArrivalDate Label=\"Arrival Date\">31-01-2018</ArrivalDate><ArrivalTime Label=\"Arrival Time\">13:10</ArrivalTime><FlightTime Label=\"Flight Time\">1:25</FlightTime><Mileage Label=\"Mileage\">281</Mileage></Leg></Legs></FlightSpecificDetails><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3906411</ItemNo><SortingOrderNumber>5</SortingOrderNumber><SegmentTypeCode Label=\"FLIGHT\">Flight</SegmentTypeCode><PassengerDetails><PassengerNameList>KAALZ/TEST MR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Flight Company</SupplierType><SupplierName>REGIONAL EXPRESS</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Aircraft Type\">Saab SF340A/B</ServiceTypeDescription><ServiceIdentifier>ZL3152</ServiceIdentifier><ServiceClassDescription Label=\"Class\">Y - Rex Flex</ServiceClassDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Mon 05 Feb 18</ServiceDate><ServiceTime>8:25 AM</ServiceTime><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><LocationDescription>TERMINAL 4</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Mon 05 Feb 18</ServiceDate><ServiceTime>9:25 AM</ServiceTime><CityCode>ABX</CityCode><CityName>ALBURY, AUSTRALIA</CityName><LocationDescription></LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><AirlineReloc Label=\"Airline Reloc\">RFWAFO</AirlineReloc><BaggageAllowance Label=\"Baggage Allowance\">23 kilos</BaggageAllowance><FlightSpecificMealsSeats>KAALZ/TEST MR\n" +
      "</FlightSpecificMealsSeats><Legs><LegsSummary>MELBOURNE, AUSTRALIA (TERMINAL - TERMINAL 4) ALBURY, AUSTRALIA (TERMINAL - ), Dept Time 05-02-2018 08:25, Arrival Time 05-02-2018 09:25 - Travelling time: 1 hr\n" +
      "</LegsSummary><Leg><StopOverLeg Label=\"Journey Info\">0</StopOverLeg><DepartureCity Label=\"Departure City\">MELBOURNE, AUSTRALIA</DepartureCity><DepartureTerminal Label=\"Departure Terminal\">TERMINAL 4</DepartureTerminal><DepartureDate Label=\"Departure Date\">05-02-2018</DepartureDate><DepartureTime Label=\"Departure Time\">08:25</DepartureTime><ArrivalCity Label=\"Arrival City\">ALBURY, AUSTRALIA</ArrivalCity><ArrivalDate Label=\"Arrival Date\">05-02-2018</ArrivalDate><ArrivalTime Label=\"Arrival Time\">09:25</ArrivalTime><FlightTime Label=\"Flight Time\">01.00</FlightTime><Mileage Label=\"Mileage\">179</Mileage></Leg></Legs></FlightSpecificDetails><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><RateInformation/></ItineraryItem></ItineraryItems><CostingsItems><DocumentTotals><TotalPayDirect><Total><BaseFare>0.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>0.00</ClientDue></Total></TotalPayDirect><TotalExcPayDirect><Total><BaseFare>0.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>0.00</ClientDue></Total></TotalExcPayDirect><TotalIncPayDirect><Total><BaseFare>0.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>0.00</ClientDue></Total></TotalIncPayDirect><TotalPaid>0.00</TotalPaid><TotalOutstandingExcPayDirect>0.00</TotalOutstandingExcPayDirect><Currency>AUD</Currency></DocumentTotals></CostingsItems></Booking></DocumentInformation>";

  String xml8 = "<DocumentInformation><Agency><AgencyName Label=\"TMC\">Tests Travel Management</AgencyName><AgencyCode Label=\"TMC Code\">trunk_tramada</AgencyCode><AgencyAddress><Address><Address1 Label=\"Address Line 1\">trunkctm</Address1><Address2 Label=\" Address Line 2\">Address 2</Address2><Address3 Label=\" Address Line 3\">Address 3</Address3><PostCode Label=\" Postcode\">2000</PostCode><State Label=\"State\">NSW</State><Country Label=\"Country\">Australia</Country></Address></AgencyAddress><AgencyContacts><AgencyEmail Label=\"Email: \">ashok@tramada.com</AgencyEmail><AgencyPhone Label=\"Phone: \">0470632287</AgencyPhone><AgencyFax Label=\"Fax No.: \">0470632287</AgencyFax></AgencyContacts></Agency><DocumentDetails><DocumentPrintDate>Tuesday 05 December 2017 13:57</DocumentPrintDate><DocumentPrintTime>Sydney, NSW</DocumentPrintTime><DocumentTitle Label=\"Type\">Itinerary &amp; Costing - Full</DocumentTitle><DocumentAddress><Address/></DocumentAddress><DocumentContactDetails><EmergencyContact Label=\"Emergency Contact: \">020000000</EmergencyContact><AfterHours Label=\"After Hours Contact: \">020000000</AfterHours><TollFree Label=\"Toll Free Number: \">1300 000 000</TollFree><Phone Label=\"Phone: \">0470632287</Phone><Fax Label=\"Fax: \">0470632287</Fax></DocumentContactDetails><DocumentCommercialDetails><LicNo Label=\"Licence No.: \">lic1</LicNo><ABN Label=\"ABN: \">52 005 000 895</ABN><ACN Label=\"ACN: \">005 000 895</ACN><MerchantNo Label=\"Merchant No: \">3333863706</MerchantNo><ATAS Label=\"ATAS Accreditation Number: \">atas1</ATAS></DocumentCommercialDetails><DocumentHeaders><DocumentHeader HeaderTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually There®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.\n" +
      "You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentHeader><DocumentHeader HeaderTitle=\"Third party hotel booking\">Please note: your hotel has been booked via a third party provider. They have provided a confirmation number and this may be required at check-in. In most instances you will also have been provided with a confirmation and payment voucher separately to the itinerary. Please ensure you have printed a copy of this voucher and are able to produce it upon request. There can be occasional communication problems between third party bookers and hotels. If you experience any difficulties at check-in please call CTM on the numbers below.</DocumentHeader><DocumentHeader HeaderTitle=\"label\">Testing Helloworld Header</DocumentHeader><DocumentHeader HeaderTitle=\"CTM EMERGENCY AFTER HOURS\">Should you require assistance with booking changes or amendments relating to imminent departures, please contact our emergency afterhours service on the following numbers:\n" +
      "\n" +
      "Within Australia - 1800 105 334\n" +
      "\n" +
      "Outside Australia -  +61 2 8268 4100.\n" +
      "\n" +
      "Please note: Service Fees will apply as per your service agreement.\n" +
      "\n" +
      "Due to privacy laws we are unable to release travel details to anyone other than the passenger.</DocumentHeader></DocumentHeaders><DocumentFooters><DocumentFooter FooterTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually There®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.\n" +
      "You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentFooter><DocumentFooter FooterTitle=\"Assistance while overseas\">Subsea 7 provide assistance in emergencies arising when personnel are travelling overseas. Comprehensive emergency assistance is available to Employees through International SOS, these cover Medical Services and Security.\n" +
      "\n" +
      "The following information is required for International SOS:\n" +
      "\n" +
      "SOS Programme Number: 14ACPA000020\n" +
      "\n" +
      "Employer: Subsea 7\n" +
      "\n" +
      "24h phone number: +61 2 9372 2468\n" +
      "\n" +
      "www.internationalsos.com</DocumentFooter><DocumentFooter FooterTitle=\"DOMESTIC BAGGAGE (JQ/QF/DJ)\">JETSTAR\n" +
      "\n" +
      "Economy Starter fare includes 10kg of carry- on luggage, customise your requirements at time of booking.\n" +
      "\n" +
      "\n" +
      "\n" +
      "http://www.jetstar.com/au/en/planning-and-booking/baggage/checked-baggage\n" +
      "\n" +
      "QANTAS\n" +
      "\n" +
      "Economy Class: 1 piece of cabin baggage weighing 7kg + 1 piece of checked baggage weighing maximum 23kg.\n" +
      "\n" +
      "Business Class: 1 piece of cabin baggage weighing 7kg + 1 piece of checked baggage weighing 32kg per piece.\n" +
      "\n" +
      "Silver/Gold/Platinum frequent flyers receive extra baggage concessions.\n" +
      "\n" +
      "http://www.qantas.com.au/travel/airlines/checked-baggage/global/en#jump0\n" +
      "\n" +
      "VIRGIN AUSTRALIA\n" +
      "\n" +
      "Economy Class Saver Fare no allowance: prepaid baggage up to 23kg will cost a flat rate of $12 per flight.\n" +
      "\n" +
      "Cabin baggage - Up to 7kgs complimentary.\n" +
      "\n" +
      "NOTE - If paid at check-in up to 23kg will cost a flat rate of $20 per flight.\n" +
      "\n" +
      "Velocity Gold Members: free extended baggage limit of 32kg checked baggage.\n" +
      "\n" +
      "Business Class/Premium Economy: permitted 69kg of checked baggage.\n" +
      "\n" +
      "http://www.virginaustralia.com/Personal/Flightinfo/BeforeYouFly/Baggagedangerousgoods/index.htm</DocumentFooter><DocumentFooter FooterTitle=\"DOMESTIC CHECK IN (JQ/QF/DJ)\">JETSTAR\n" +
      "\n" +
      "Check-in for Jetstar flights leaving from any domestic Australian destination is a minimum (recommended 60 minutes) before your scheduled flight. Check in opens 2 hours prior to the scheduled flights and closes 30 minutes prior to scheduled departure time.\n" +
      "\n" +
      "http://www.jetstar.com/au/en/planning-and-booking/checking-in/web-check-in\n" +
      "\n" +
      "QANTAS\n" +
      "\n" +
      "QANTAS domestic flights check-in closes 30 minutes prior to scheduled departure time - except for flights numbered QF2000-QF2299 and QF7000-QF7299 departing from Sydney, check-in time is 1 hour. Online check-in is now available for QANTAS and QANTAS Link Australian domestic bookings between 24 hours and 1 hour before your flight departure.\n" +
      "\n" +
      "http://www.qantas.com.au/info/flying/atTheAirport/checkinTimes\n" +
      "\n" +
      "VIRGIN AUSTRALIA\n" +
      "\n" +
      "Virgin Australia domestic flights check-in closes 30 minutes prior to scheduled departure time. Virgin Blue online check-in is available and opens 24 hours before departure of your flights. Passengers wanting to utilise this facility must present a print out of your boarding pass at check in.\n" +
      "\n" +
      "http://www.virginaustralia.com/Personal/Services/Check-inoptions/index.htm</DocumentFooter><DocumentFooter FooterTitle=\"DOMESTIC FARE CONDITIONS (JQ/QF/DJ)\">JETSTAR - http://travelctm.com/uploads/JQFareGrid.pdf\n" +
      "\n" +
      "QANTAS - http://travelctm.com/uploads/QFDomesticandNZFareGrid.pdf\n" +
      "\n" +
      "VIRGIN AUSTRALIA - http://travelctm.com/uploads/DJFaregrid.pdf\n" +
      "\n" +
      "NOTE: Business Class Virgin Australia only offered on A330-200 aircraft (subject to change).</DocumentFooter><DocumentFooter FooterTitle=\"E-TICKET\">E ticket identification required by airlines:\n" +
      "\n" +
      "Photo identification that has been issued by the Commonwealth of Australia or an Australian State or Territory. Acceptable photo identification includes: drivers license, passport, any state, territory or federal government issued card, company issued identification, or a student card. Qantas acceptable non photo identification includes: credit card used to pay for the ticket, other credit or debit cards, social security card, QF club or frequent flyer card, certified copy/original of a birth of citizenship document. All ID?s must be current and valid.</DocumentFooter><DocumentFooter FooterTitle=\"FREQUENT FLYER\">* Please advise your airline loyalty number if applicable. Once received we will enter this into your booking and your profile for future.\n" +
      "\n" +
      "* To ensure you receive your frequent flyer points, please retain tickets and boarding passes where possible until your points have appeared on your frequent flyer statement.\n" +
      "\n" +
      "* Should your points not appear on your statement, please send copies of your boarding passes, tickets and supporting information to the relevant airline.</DocumentFooter><DocumentFooter FooterTitle=\"IATA\">Transportation and other services provided by the carrier are subject to conditions of contract and other important notices which are delivered with this\n" +
      "itinerary/receipt and form part of the nearest office of the issuing airline or travel agent to obtain a copy prior to the commencement of your trip. Notice if\n" +
      "the passenger's journey involves an ultimate destination or stop in a country other than the country of departure the Warsaw convention may be applicable\n" +
      "and the convention governs and in most cases limits the liability of carriers for death or personal injury and in respect of loss or damage to baggage.</DocumentFooter><DocumentFooter FooterTitle=\"ITINERARY ACCEPTANCE\">Whilst all care has been taken to ensure your travel requirements are booked as per your request, CTM requests that you accept responsibility for double checking these details, and advise of any changes that need to be made as soon as possible. Upon receiving your itinerary please check and make sure the relevant booking is correct and as per your travel needs.</DocumentFooter></DocumentFooters><CarbonEmissionInformation/></DocumentDetails><Booking><BookingDetails><BookingNumber Label=\"Booking ID: \">B579186</BookingNumber><PNRDetails><PNRList/></PNRDetails><BookingConsultant1>agent</BookingConsultant1><BookingConsultant1EmailAddress>tester18@tramada.com</BookingConsultant1EmailAddress><BookingConsultant2/><TravelApprover/><BookedBy Label=\"Booked By: \">KALAI SELVAN</BookedBy><BookingDepartureDate Label=\"Departure Date: \">05 Dec 17</BookingDepartureDate><BookingFinalTKTDate Label=\"Final TKT Date: \"></BookingFinalTKTDate><BookingCancellationCondition/><BookingDeposits/><BookingDebtors><BookingDebtor><DebtorName Label=\"Debtor: \">BETA TECHNOLOGY</DebtorName></BookingDebtor></BookingDebtors><BookingPassengers><BookingPassenger><PassengerName>KIM MARK</PassengerName></BookingPassenger><BookingPassenger><PassengerName>SAURABH</PassengerName></BookingPassenger><TicketNumbers/><PassengerNameList Label=\"Itinerary For\">KIM MARK, SAURABH</PassengerNameList></BookingPassengers></BookingDetails><ItineraryItems><ItineraryItem><ItemNo>3906419</ItemNo><SortingOrderNumber>1</SortingOrderNumber><SegmentTypeCode Label=\"MISC\">OTHER</SegmentTypeCode><PassengerDetails><PassengerNameList>KIM MARK, SAURABH</PassengerNameList></PassengerDetails><Supplier><SupplierType>OTHER Company</SupplierType><SupplierName>KADINA GATEWAY MOTOR INN</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Wed 06 Dec 17</ServiceDate><ServiceTime/><CityCode>ADELAIDE AIPORT</CityCode><CityName>ADELAIDE AIPORT</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Wed 06 Dec 17</ServiceDate><ServiceTime/><CityCode>ADELAIDE AIPORT</CityCode><CityName>ADELAIDE AIPORT</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>Pre-paid remark test cccf</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription>24 hours cancellation notice required</CancellationDescription></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. of Units\">1</NoOfItems><NoOfdays UnitOfCount=\"Nights\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD10.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD10.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3906417</ItemNo><SortingOrderNumber>2</SortingOrderNumber><SegmentTypeCode Label=\"COMMENT\">Comment</SegmentTypeCode><PassengerDetails><PassengerNameList/></PassengerDetails><Supplier><SupplierType>Comment Company</SupplierType><SupplierName/><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Type\">TEST COMMENT</ServiceTypeDescription><Start><StartLabelValue>Start</StartLabelValue><ServiceDate>Tue 12 Dec 17</ServiceDate><ServiceTime>11:00 AM</ServiceTime><CityCode/><CityName/><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>End</EndLabelValue><ServiceDate>Thu 14 Dec 17</ServiceDate><ServiceTime>12:00 PM</ServiceTime><CityCode/><CityName/><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status/><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3906416</ItemNo><SortingOrderNumber>3</SortingOrderNumber><SegmentTypeCode Label=\"BUS\">Bus</SegmentTypeCode><PassengerDetails><PassengerNameList>KIM MARK, SAURABH</PassengerNameList></PassengerDetails><Supplier><SupplierType>Bus Company</SupplierType><SupplierName/><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Thu 14 Dec 17</ServiceDate><ServiceTime/><CityCode>SYD</CityCode><CityName>Kingsford Smith, Sydney</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Thu 14 Dec 17</ServiceDate><ServiceTime/><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>Pre-paid remark test cccf</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. Of Passengers\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD120.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD120.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3906418</ItemNo><SortingOrderNumber>4</SortingOrderNumber><SegmentTypeCode Label=\"HOTEL\">Hotel</SegmentTypeCode><PassengerDetails><PassengerNameList>KIM MARK, SAURABH</PassengerNameList></PassengerDetails><Supplier><SupplierType>Hotel Company</SupplierType><SupplierName>Test&amp;creditor</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Check In</StartLabelValue><ServiceDate>Thu 14 Dec 17</ServiceDate><ServiceTime>6:00 AM</ServiceTime><CityCode>PER</CityCode><CityName>PERTH, AUSTRALIA</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Check Out</EndLabelValue><ServiceDate>Fri 15 Dec 17</ServiceDate><ServiceTime>2:00 AM</ServiceTime><CityCode/><CityName/><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>Free of Charge</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. of Rooms\">1</NoOfItems><NoOfdays UnitOfCount=\"Nights\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD80.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD80.00</LocalRate></RateInformation></ItineraryItem></ItineraryItems><CostingsItems><CostingItem><ItemNo>2218625</ItemNo><SortingOrderNumber>1</SortingOrderNumber><SegmentTypeCode Label=\"BUS\">Bus</SegmentTypeCode><Creditor><CreditorName>TABCORP PARK</CreditorName></Creditor><CostingDescriptor/><IssueDate></IssueDate><StartServiceDate Label=\"Start Service Date\">14 Dec 17</StartServiceDate><StartCity>Kingsford Smith, Sydney</StartCity><StartCityName Label=\"Start City\"/><EndServiceDate Label=\"End Service Date\">14 Dec 17</EndServiceDate><EndCity>MELBOURNE, AUSTRALIA</EndCity><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>109.09</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">10.91</GST><ClientDue>120.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2218626</ItemNo><SortingOrderNumber>2</SortingOrderNumber><SegmentTypeCode Label=\"HOTEL\">Hotel</SegmentTypeCode><Creditor><CreditorName>Test&amp;creditor</CreditorName></Creditor><CostingDescriptor/><IssueDate></IssueDate><StartServiceDate Label=\"Start Service Date\">14 Dec 17</StartServiceDate><StartCity>PERTH, AUSTRALIA</StartCity><StartCityName Label=\"Address\"/><EndServiceDate Label=\"End Service Date\">15 Dec 17</EndServiceDate><EndCity/><CostingNotes/><CostingFinancials><BaseFare>72.73</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">7.27</GST><ClientDue>80.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials></CostingItem><CostingItem><ItemNo>2218627</ItemNo><SortingOrderNumber>3</SortingOrderNumber><SegmentTypeCode Label=\"MISC\">OTHER</SegmentTypeCode><Creditor><CreditorName>KADINA GATEWAY MOTOR INN</CreditorName></Creditor><CostingDescriptor/><IssueDate></IssueDate><StartServiceDate Label=\"Start Service Date\">06 Dec 17</StartServiceDate><StartCity>ADELAIDE AIPORT</StartCity><StartCityName Label=\"Start City\"/><EndServiceDate Label=\"End Service Date\">06 Dec 17</EndServiceDate><EndCity>ADELAIDE AIPORT</EndCity><EndCityName Label=\"Finish City\"/><CostingNotes/><CostingFinancials><BaseFare>9.09</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">0.91</GST><ClientDue>10.00</ClientDue><PaymentType>Prepaid</PaymentType></CostingFinancials><CancellationDescription Label=\"Cancellation\">24 hours cancellation notice required</CancellationDescription></CostingItem><DocumentTotals><TotalPayDirect><Total><BaseFare>0.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>0.00</ClientDue></Total></TotalPayDirect><TotalExcPayDirect><Total><BaseFare>190.91</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>19.09</GST><ClientDue>210.00</ClientDue></Total></TotalExcPayDirect><TotalIncPayDirect><Total><BaseFare>190.91</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>19.09</GST><ClientDue>210.00</ClientDue></Total></TotalIncPayDirect><TotalPaid>0.00</TotalPaid><TotalOutstandingExcPayDirect>210.00</TotalOutstandingExcPayDirect><Currency>AUD</Currency></DocumentTotals></CostingsItems></Booking></DocumentInformation>";

  String miscXml7 = "<DocumentInformation><Agency><AgencyName Label=\"TMC\">Tests Travel Management</AgencyName><AgencyCode Label=\"TMC Code\">trunk_tramada</AgencyCode><AgencyAddress><Address><Address1 Label=\"Address Line 1\">trunkctm</Address1><Address2 Label=\" Address Line 2\">Address 2</Address2><Address3 Label=\" Address Line 3\">Address 3</Address3><PostCode Label=\" Postcode\">2000</PostCode><State Label=\"State\">NSW</State><Country Label=\"Country\">Australia</Country></Address></AgencyAddress><AgencyContacts><AgencyEmail Label=\"Email: \">ashok@tramada.com</AgencyEmail><AgencyPhone Label=\"Phone: \">0470632287</AgencyPhone><AgencyFax Label=\"Fax No.: \">0470632287</AgencyFax></AgencyContacts></Agency><DocumentDetails><DocumentPrintDate>Tuesday 05 December 2017 12:16</DocumentPrintDate><DocumentPrintTime>Sydney, NSW</DocumentPrintTime><DocumentTitle Label=\"Type\">Itinerary - Full</DocumentTitle><DocumentAddress><Address/></DocumentAddress><DocumentContactDetails><EmergencyContact Label=\"Emergency Contact: \">1800 663 622</EmergencyContact><AfterHours Label=\"After Hours Contact: \">1800 663 622</AfterHours><TollFree Label=\"Toll Free Number: \">1800 663 622</TollFree><Phone Label=\"Phone: \">0470632287</Phone><Fax Label=\"Fax: \">0470632287</Fax></DocumentContactDetails><DocumentCommercialDetails><LicNo Label=\"Licence No.: \">854</LicNo><ABN Label=\"ABN: \">52 005 000 895</ABN><ACN Label=\"ACN: \">005 000 895</ACN></DocumentCommercialDetails><DocumentHeaders><DocumentHeader HeaderTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually There®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.\n" +
      "You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentHeader><DocumentHeader HeaderTitle=\"Third party hotel booking\">Please note: your hotel has been booked via a third party provider. They have provided a confirmation number and this may be required at check-in. In most instances you will also have been provided with a confirmation and payment voucher separately to the itinerary. Please ensure you have printed a copy of this voucher and are able to produce it upon request. There can be occasional communication problems between third party bookers and hotels. If you experience any difficulties at check-in please call CTM on the numbers below.</DocumentHeader><DocumentHeader HeaderTitle=\"label\">Testing Helloworld Header</DocumentHeader><DocumentHeader HeaderTitle=\"CTM EMERGENCY AFTER HOURS\">Should you require assistance with booking changes or amendments relating to imminent departures, please contact our emergency afterhours service on the following numbers:\n" +
      "\n" +
      "Within Australia - 1800 663 622\n" +
      "\n" +
      "Outside Australia - +61 7 3211 2400\n" +
      "\n" +
      "DUE TO PRIVACY LAWS WE ARE UNABLE TO RELEASE TRAVEL DETAILS TO ANYONE OTHER THAN THE PASSENGER.</DocumentHeader></DocumentHeaders><DocumentFooters><DocumentFooter FooterTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually There®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.\n" +
      "You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentFooter><DocumentFooter FooterTitle=\"Assistance while overseas\">Subsea 7 provide assistance in emergencies arising when personnel are travelling overseas. Comprehensive emergency assistance is available to Employees through International SOS, these cover Medical Services and Security.\n" +
      "\n" +
      "The following information is required for International SOS:\n" +
      "\n" +
      "SOS Programme Number: 14ACPA000020\n" +
      "\n" +
      "Employer: Subsea 7\n" +
      "\n" +
      "24h phone number: +61 2 9372 2468\n" +
      "\n" +
      "www.internationalsos.com</DocumentFooter><DocumentFooter FooterTitle=\"DOMESTIC BAGGAGE (JQ/QF/DJ)\">JETSTAR\n" +
      "\n" +
      "Economy Starter fare includes 10kg of carry- on luggage, customise your requirements at time of booking.\n" +
      "\n" +
      "\n" +
      "\n" +
      "http://www.jetstar.com/au/en/planning-and-booking/baggage/checked-baggage\n" +
      "\n" +
      "QANTAS\n" +
      "\n" +
      "Economy Class: 1 piece of cabin baggage weighing 7kg + 1 piece of checked baggage weighing maximum 23kg.\n" +
      "\n" +
      "Business Class: 1 piece of cabin baggage weighing 7kg + 1 piece of checked baggage weighing 32kg per piece.\n" +
      "\n" +
      "Silver/Gold/Platinum frequent flyers receive extra baggage concessions.\n" +
      "\n" +
      "http://www.qantas.com.au/travel/airlines/checked-baggage/global/en#jump0\n" +
      "\n" +
      "VIRGIN AUSTRALIA\n" +
      "\n" +
      "Economy Class Saver Fare no allowance: prepaid baggage up to 23kg will cost a flat rate of $12 per flight.\n" +
      "\n" +
      "Cabin baggage - Up to 7kgs complimentary.\n" +
      "\n" +
      "NOTE - If paid at check-in up to 23kg will cost a flat rate of $20 per flight.\n" +
      "\n" +
      "Velocity Gold Members: free extended baggage limit of 32kg checked baggage.\n" +
      "\n" +
      "Business Class/Premium Economy: permitted 69kg of checked baggage.\n" +
      "\n" +
      "http://www.virginaustralia.com/Personal/Flightinfo/BeforeYouFly/Baggagedangerousgoods/index.htm</DocumentFooter><DocumentFooter FooterTitle=\"DOMESTIC CHECK IN (JQ/QF/DJ)\">JETSTAR\n" +
      "\n" +
      "Check-in for Jetstar flights leaving from any domestic Australian destination is a minimum (recommended 60 minutes) before your scheduled flight. Check in opens 2 hours prior to the scheduled flights and closes 30 minutes prior to scheduled departure time.\n" +
      "\n" +
      "http://www.jetstar.com/au/en/planning-and-booking/checking-in/web-check-in\n" +
      "\n" +
      "QANTAS\n" +
      "\n" +
      "QANTAS domestic flights check-in closes 30 minutes prior to scheduled departure time - except for flights numbered QF2000-QF2299 and QF7000-QF7299 departing from Sydney, check-in time is 1 hour. Online check-in is now available for QANTAS and QANTAS Link Australian domestic bookings between 24 hours and 1 hour before your flight departure.\n" +
      "\n" +
      "http://www.qantas.com.au/info/flying/atTheAirport/checkinTimes\n" +
      "\n" +
      "VIRGIN AUSTRALIA\n" +
      "\n" +
      "Virgin Australia domestic flights check-in closes 30 minutes prior to scheduled departure time. Virgin Blue online check-in is available and opens 24 hours before departure of your flights. Passengers wanting to utilise this facility must present a print out of your boarding pass at check in.\n" +
      "\n" +
      "http://www.virginaustralia.com/Personal/Services/Check-inoptions/index.htm</DocumentFooter><DocumentFooter FooterTitle=\"DOMESTIC FARE CONDITIONS (JQ/QF/DJ)\">JETSTAR - http://travelctm.com/uploads/JQFareGrid.pdf\n" +
      "\n" +
      "QANTAS - http://travelctm.com/uploads/QFDomesticandNZFareGrid.pdf\n" +
      "\n" +
      "VIRGIN AUSTRALIA - http://travelctm.com/uploads/DJFaregrid.pdf\n" +
      "\n" +
      "NOTE: Business Class Virgin Australia only offered on A330-200 aircraft (subject to change).</DocumentFooter><DocumentFooter FooterTitle=\"E-TICKET\">E ticket identification required by airlines:\n" +
      "\n" +
      "Photo identification that has been issued by the Commonwealth of Australia or an Australian State or Territory. Acceptable photo identification includes: drivers license, passport, any state, territory or federal government issued card, company issued identification, or a student card. Qantas acceptable non photo identification includes: credit card used to pay for the ticket, other credit or debit cards, social security card, QF club or frequent flyer card, certified copy/original of a birth of citizenship document. All ID?s must be current and valid.</DocumentFooter><DocumentFooter FooterTitle=\"FREQUENT FLYER\">* Please advise your airline loyalty number if applicable. Once received we will enter this into your booking and your profile for future.\n" +
      "\n" +
      "* To ensure you receive your frequent flyer points, please retain tickets and boarding passes where possible until your points have appeared on your frequent flyer statement.\n" +
      "\n" +
      "* Should your points not appear on your statement, please send copies of your boarding passes, tickets and supporting information to the relevant airline.</DocumentFooter><DocumentFooter FooterTitle=\"IATA\">Transportation and other services provided by the carrier are subject to conditions of contract and other important notices which are delivered with this\n" +
      "itinerary/receipt and form part of the nearest office of the issuing airline or travel agent to obtain a copy prior to the commencement of your trip. Notice if\n" +
      "the passenger's journey involves an ultimate destination or stop in a country other than the country of departure the Warsaw convention may be applicable\n" +
      "and the convention governs and in most cases limits the liability of carriers for death or personal injury and in respect of loss or damage to baggage.</DocumentFooter><DocumentFooter FooterTitle=\"ITINERARY ACCEPTANCE\">Whilst all care has been taken to ensure your travel requirements are booked as per your request, CTM requests that you accept responsibility for double checking these details, and advise of any changes that need to be made as soon as possible. Upon receiving your itinerary please check and make sure the relevant booking is correct and as per your travel needs.</DocumentFooter></DocumentFooters><CarbonEmissionInformation/></DocumentDetails><Booking><BookingDetails><BookingNumber Label=\"Booking ID: \">B579185</BookingNumber><PNRDetails><PNRList/></PNRDetails><BookingConsultant1>agent</BookingConsultant1><BookingConsultant1EmailAddress>shusma1@tramada.com</BookingConsultant1EmailAddress><BookingConsultant2/><TravelApprover/><BookedBy Label=\"Booked By: \">Sharyne Andrews</BookedBy><BookingDepartureDate Label=\"Departure Date: \">31 Jan 18</BookingDepartureDate><BookingReturnDate Label=\"Return Date: \">05 Feb 18</BookingReturnDate><BookingFinalTKTDate Label=\"Final TKT Date: \">01 Dec 17</BookingFinalTKTDate><BookingCancellationCondition/><BookingDeposits/><BookingDebtors><BookingDebtor><DebtorName Label=\"Debtor: \">KAL</DebtorName></BookingDebtor></BookingDebtors><BookingPassengers><BookingPassenger><PassengerName>KAALZ/TEST MR</PassengerName></BookingPassenger><BookingPassenger><PassengerName>LEONARD/GERARDMR</PassengerName><Memberships><Membership><MembershipNumber>2045223</MembershipNumber><MembershipCompanyName>QANTAS AIRWAYS</MembershipCompanyName><MembershipCompanyCode>QF</MembershipCompanyCode><MembershipProgramName>QF</MembershipProgramName><MembershipProgramCode>QF</MembershipProgramCode></Membership></Memberships></BookingPassenger><BookingPassenger><PassengerName>MR MARCO POLO</PassengerName></BookingPassenger><BookingPassenger><PassengerName>MRS MARCH POLO</PassengerName></BookingPassenger><TicketNumbers/><PassengerNameList Label=\"Itinerary For\">KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList></BookingPassengers></BookingDetails><ItineraryItems><ItineraryItem><ItemNo>3906415</ItemNo><SortingOrderNumber>1</SortingOrderNumber><SegmentTypeCode Label=\"MISC\">Package</SegmentTypeCode><PassengerDetails><PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Train Company</SupplierType><SupplierName>Test Agency</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Embark</StartLabelValue><ServiceDate>Fri 01 Dec 17</ServiceDate><ServiceTime/><CityCode>SYD</CityCode><CityName>Kingsford Smith, Sydney</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Disembark</EndLabelValue><ServiceDate>Fri 01 Dec 17</ServiceDate><ServiceTime/><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails/><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>Free of Charge</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription>24 hours cancellation notice required</CancellationDescription></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. of Units\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD120.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD120.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3906413</ItemNo><SortingOrderNumber>2</SortingOrderNumber><SegmentTypeCode Label=\"MISC\">Car</SegmentTypeCode><PassengerDetails><PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList></PassengerDetails><Supplier><SupplierType>Car Company</SupplierType><SupplierName>V Australia</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Pick Up</StartLabelValue><ServiceDate>Sat 09 Dec 17</ServiceDate><ServiceTime/><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Drop Off</EndLabelValue><ServiceDate>Mon 11 Dec 17</ServiceDate><ServiceTime/><CityCode>MEL APRT</CityCode><CityName>MEL APRT</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>All Charges</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription>Cancel 10 Hours Prior to Arrival</CancellationDescription></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. of Cars\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">2</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD200.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD200.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3906414</ItemNo><SortingOrderNumber>3</SortingOrderNumber><SegmentTypeCode Label=\"MISC\">Bus</SegmentTypeCode><PassengerDetails><PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList></PassengerDetails><Supplier><SupplierType>Bus Company</SupplierType><SupplierName/><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Sat 09 Dec 17</ServiceDate><ServiceTime/><CityCode>SYD</CityCode><CityName>Kingsford Smith, Sydney</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Sat 09 Dec 17</ServiceDate><ServiceTime/><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><LocationDescription/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative>nan</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. Of Passengers\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD100.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD100.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3906412</ItemNo><SortingOrderNumber>4</SortingOrderNumber><SegmentTypeCode Label=\"MISC\">Misc</SegmentTypeCode><PassengerDetails><PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Flight Company</SupplierType><SupplierName>REGIONAL EXPRESS</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Aircraft Type\">Saab SF340A/B</ServiceTypeDescription><ServiceIdentifier>ZL0773</ServiceIdentifier><ServiceClassDescription Label=\"Class\">Y - Rex Flex</ServiceClassDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Wed 31 Jan 18</ServiceDate><ServiceTime>11:45 AM</ServiceTime><CityCode>SYD</CityCode><CityName>Kingsford Smith, Sydney</CityName><LocationDescription>TERMINAL 2 DOMESTIC</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Wed 31 Jan 18</ServiceDate><ServiceTime>1:10 PM</ServiceTime><CityCode>ABX</CityCode><CityName>ALBURY, AUSTRALIA</CityName><LocationDescription></LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><AirlineReloc Label=\"Airline Reloc\">RFWAFO</AirlineReloc><BaggageAllowance Label=\"Baggage Allowance\">23 kilos</BaggageAllowance><FlightSpecificMealsSeats>KAALZ/TEST MR\n" +
      "Seat No 28A (Aisle)\n" +
      "Meal Requested LOW CALORIE\n" +
      "|LEONARD/GERARDMR\n" +
      "Seat No 23C (Sleeper Seat)\n" +
      "Meal Requested NO SALT MEAL\n" +
      "</FlightSpecificMealsSeats><Legs><LegsSummary>Kingsford Smith, Sydney (TERMINAL - TERMINAL 2 DOMESTIC) ALBURY, AUSTRALIA (TERMINAL - ), Dept Time 31-01-2018 11:45, Arrival Time 31-01-2018 13:10 - Travelling time: 1 hr 25 mins\n" +
      "</LegsSummary><Leg><StopOverLeg Label=\"Journey Info\">0</StopOverLeg><DepartureCity Label=\"Departure City\">Kingsford Smith, Sydney</DepartureCity><DepartureTerminal Label=\"Departure Terminal\">TERMINAL 2 DOMESTIC</DepartureTerminal><DepartureDate Label=\"Departure Date\">31-01-2018</DepartureDate><DepartureTime Label=\"Departure Time\">11:45</DepartureTime><ArrivalCity Label=\"Arrival City\">ALBURY, AUSTRALIA</ArrivalCity><ArrivalDate Label=\"Arrival Date\">31-01-2018</ArrivalDate><ArrivalTime Label=\"Arrival Time\">13:10</ArrivalTime><FlightTime Label=\"Flight Time\">1:25</FlightTime><Mileage Label=\"Mileage\">281</Mileage></Leg></Legs></FlightSpecificDetails><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><RateInformation/></ItineraryItem><ItineraryItem><ItemNo>3906411</ItemNo><SortingOrderNumber>5</SortingOrderNumber><SegmentTypeCode Label=\"MISC\">Other</SegmentTypeCode><PassengerDetails><PassengerNameList>KAALZ/TEST MR</PassengerNameList></PassengerDetails><Supplier><SupplierType>Flight Company</SupplierType><SupplierName>REGIONAL EXPRESS</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Aircraft Type\">Saab SF340A/B</ServiceTypeDescription><ServiceIdentifier>ZL3152</ServiceIdentifier><ServiceClassDescription Label=\"Class\">Y - Rex Flex</ServiceClassDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Mon 05 Feb 18</ServiceDate><ServiceTime>8:25 AM</ServiceTime><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><LocationDescription>TERMINAL 4</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Mon 05 Feb 18</ServiceDate><ServiceTime>9:25 AM</ServiceTime><CityCode>ABX</CityCode><CityName>ALBURY, AUSTRALIA</CityName><LocationDescription></LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><AirlineReloc Label=\"Airline Reloc\">RFWAFO</AirlineReloc><BaggageAllowance Label=\"Baggage Allowance\">23 kilos</BaggageAllowance><FlightSpecificMealsSeats>KAALZ/TEST MR\n" +
      "</FlightSpecificMealsSeats><Legs><LegsSummary>MELBOURNE, AUSTRALIA (TERMINAL - TERMINAL 4) ALBURY, AUSTRALIA (TERMINAL - ), Dept Time 05-02-2018 08:25, Arrival Time 05-02-2018 09:25 - Travelling time: 1 hr\n" +
      "</LegsSummary><Leg><StopOverLeg Label=\"Journey Info\">0</StopOverLeg><DepartureCity Label=\"Departure City\">MELBOURNE, AUSTRALIA</DepartureCity><DepartureTerminal Label=\"Departure Terminal\">TERMINAL 4</DepartureTerminal><DepartureDate Label=\"Departure Date\">05-02-2018</DepartureDate><DepartureTime Label=\"Departure Time\">08:25</DepartureTime><ArrivalCity Label=\"Arrival City\">ALBURY, AUSTRALIA</ArrivalCity><ArrivalDate Label=\"Arrival Date\">05-02-2018</ArrivalDate><ArrivalTime Label=\"Arrival Time\">09:25</ArrivalTime><FlightTime Label=\"Flight Time\">01.00</FlightTime><Mileage Label=\"Mileage\">179</Mileage></Leg></Legs></FlightSpecificDetails><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><RateInformation/></ItineraryItem></ItineraryItems><CostingsItems><DocumentTotals><TotalPayDirect><Total><BaseFare>0.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>0.00</ClientDue></Total></TotalPayDirect><TotalExcPayDirect><Total><BaseFare>0.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>0.00</ClientDue></Total></TotalExcPayDirect><TotalIncPayDirect><Total><BaseFare>0.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>0.00</ClientDue></Total></TotalIncPayDirect><TotalPaid>0.00</TotalPaid><TotalOutstandingExcPayDirect>0.00</TotalOutstandingExcPayDirect><Currency>AUD</Currency></DocumentTotals></CostingsItems></Booking></DocumentInformation>";


  String xml9 = "<DocumentInformation>\n" +
      "  <Agency>\n" +
      "    <AgencyName Label=\"TMC\">Tests Travel Management</AgencyName>\n" +
      "    <AgencyCode Label=\"TMC Code\">trunk_tramada</AgencyCode>\n" +
      "    <AgencyAddress>\n" +
      "      <Address>\n" +
      "        <Address1 Label=\"Address Line 1\">trunkctm</Address1>\n" +
      "        <Address2 Label=\" Address Line 2\">Address 2</Address2>\n" +
      "        <Address3 Label=\" Address Line 3\">Address 3</Address3>\n" +
      "        <PostCode Label=\" Postcode\">2000</PostCode>\n" +
      "        <State Label=\"State\">NSW</State>\n" +
      "        <Country Label=\"Country\">Australia</Country>\n" +
      "      </Address>\n" +
      "    </AgencyAddress>\n" +
      "    <AgencyContacts>\n" +
      "      <AgencyEmail Label=\"Email: \">ashok@tramada.com</AgencyEmail>\n" +
      "      <AgencyPhone Label=\"Phone: \">0470632287</AgencyPhone>\n" +
      "      <AgencyFax Label=\"Fax No.: \">0470632287</AgencyFax>\n" +
      "    </AgencyContacts>\n" +
      "  </Agency>\n" +
      "  <DocumentDetails>\n" +
      "    <DocumentPrintDate>Wednesday 03 January 2018 09:22</DocumentPrintDate>\n" +
      "    <DocumentPrintTime>Brisbane, QLD</DocumentPrintTime>\n" +
      "    <DocumentTitle Label=\"Type\">Itinerary &amp; Costing - Full</DocumentTitle>\n" +
      "    <DocumentAddress>\n" +
      "      <Address/>\n" +
      "    </DocumentAddress>\n" +
      "    <DocumentContactDetails>\n" +
      "      <EmergencyContact Label=\"Emergency Contact: \">1800 663 622</EmergencyContact>\n" +
      "      <AfterHours Label=\"After Hours Contact: \">1800 663 622</AfterHours>\n" +
      "      <TollFree Label=\"Toll Free Number: \">1800 663 622</TollFree>\n" +
      "      <Phone Label=\"Phone: \">0470632287</Phone>\n" +
      "      <Fax Label=\"Fax: \">0470632287</Fax>\n" +
      "    </DocumentContactDetails>\n" +
      "    <DocumentCommercialDetails>\n" +
      "      <LicNo Label=\"Licence No.: \">854</LicNo>\n" +
      "      <ABN Label=\"ABN: \">52 005 000 895</ABN>\n" +
      "      <ACN Label=\"ACN: \">005 000 895</ACN>\n" +
      "    </DocumentCommercialDetails>\n" +
      "    <DocumentHeaders>\n" +
      "      <DocumentHeader HeaderTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually There®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentHeader>\n" +
      "      <DocumentHeader HeaderTitle=\"Third party hotel booking\">Please note: your hotel has been booked via a third party provider. They have provided a confirmation number and this may be required at check-in. In most instances you will also have been provided with a confirmation and payment voucher separately to the itinerary. Please ensure you have printed a copy of this voucher and are able to produce it upon request. There can be occasional communication problems between third party bookers and hotels. If you experience any difficulties at check-in please call CTM on the numbers below.</DocumentHeader>\n" +
      "      <DocumentHeader HeaderTitle=\"label\">Testing Helloworld Header</DocumentHeader>\n" +
      "    </DocumentHeaders>\n" +
      "    <DocumentFooters>\n" +
      "      <DocumentFooter FooterTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually There®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"Assistance while overseas\">Subsea 7 provide assistance in emergencies arising when personnel are travelling overseas. Comprehensive emergency assistance is available to Employees through International SOS, these cover Medical Services and Security.The following information is required for International SOS:SOS Programme Number: 14ACPA000020Employer: Subsea 724h phone number: +61 2 9372 2468www.internationalsos.com</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"DOMESTIC BAGGAGE (JQ/QF/DJ)\">JETSTAREconomy Starter fare includes 10kg of carry- on luggage, customise your requirements at time of booking. http://www.jetstar.com/au/en/planning-and-booking/baggage/checked-baggageQANTASEconomy Class: 1 piece of cabin baggage weighing 7kg + 1 piece of checked baggage weighing maximum 23kg.Business Class: 1 piece of cabin baggage weighing 7kg + 1 piece of checked baggage weighing 32kg per piece.Silver/Gold/Platinum frequent flyers receive extra baggage concessions.http://www.qantas.com.au/travel/airlines/checked-baggage/global/en#jump0VIRGIN AUSTRALIAEconomy Class Saver Fare no allowance: prepaid baggage up to 23kg will cost a flat rate of $12 per flight.Cabin baggage - Up to 7kgs complimentary.NOTE - If paid at check-in up to 23kg will cost a flat rate of $20 per flight.Velocity Gold Members: free extended baggage limit of 32kg checked baggage.Business Class/Premium Economy: permitted 69kg of checked baggage.http://www.virginaustralia.com/Personal/Flightinfo/BeforeYouFly/Baggagedangerousgoods/index.htm</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"DOMESTIC CHECK IN (JQ/QF/DJ)\">JETSTARCheck-in for Jetstar flights leaving from any domestic Australian destination is a minimum (recommended 60 minutes) before your scheduled flight. Check in opens 2 hours prior to the scheduled flights and closes 30 minutes prior to scheduled departure time.http://www.jetstar.com/au/en/planning-and-booking/checking-in/web-check-inQANTASQANTAS domestic flights check-in closes 30 minutes prior to scheduled departure time - except for flights numbered QF2000-QF2299 and QF7000-QF7299 departing from Sydney, check-in time is 1 hour. Online check-in is now available for QANTAS and QANTAS Link Australian domestic bookings between 24 hours and 1 hour before your flight departure.http://www.qantas.com.au/info/flying/atTheAirport/checkinTimesVIRGIN AUSTRALIAVirgin Australia domestic flights check-in closes 30 minutes prior to scheduled departure time. Virgin Blue online check-in is available and opens 24 hours before departure of your flights. Passengers wanting to utilise this facility must present a print out of your boarding pass at check in.http://www.virginaustralia.com/Personal/Services/Check-inoptions/index.htm</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"DOMESTIC FARE CONDITIONS (JQ/QF/DJ)\">JETSTAR - http://travelctm.com/uploads/JQFareGrid.pdfQANTAS - http://travelctm.com/uploads/QFDomesticandNZFareGrid.pdfVIRGIN AUSTRALIA - http://travelctm.com/uploads/DJFaregrid.pdfNOTE: Business Class Virgin Australia only offered on A330-200 aircraft (subject to change).</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"E-TICKET\">E ticket identification required by airlines:Photo identification that has been issued by the Commonwealth of Australia or an Australian State or Territory. Acceptable photo identification includes: drivers license, passport, any state, territory or federal government issued card, company issued identification, or a student card. Qantas acceptable non photo identification includes: credit card used to pay for the ticket, other credit or debit cards, social security card, QF club or frequent flyer card, certified copy/original of a birth of citizenship document. All ID?s must be current and valid.</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"FREQUENT FLYER\">* Please advise your airline loyalty number if applicable. Once received we will enter this into your booking and your profile for future.* To ensure you receive your frequent flyer points, please retain tickets and boarding passes where possible until your points have appeared on your frequent flyer statement.* Should your points not appear on your statement, please send copies of your boarding passes, tickets and supporting information to the relevant airline.</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"IATA\">Transportation and other services provided by the carrier are subject to conditions of contract and other important notices which are delivered with thisitinerary/receipt and form part of the nearest office of the issuing airline or travel agent to obtain a copy prior to the commencement of your trip. Notice ifthe passenger's journey involves an ultimate destination or stop in a country other than the country of departure the Warsaw convention may be applicableand the convention governs and in most cases limits the liability of carriers for death or personal injury and in respect of loss or damage to baggage.</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"ITINERARY ACCEPTANCE\">Whilst all care has been taken to ensure your travel requirements are booked as per your request, CTM requests that you accept responsibility for double checking these details, and advise of any changes that need to be made as soon as possible. Upon receiving your itinerary please check and make sure the relevant booking is correct and as per your travel needs.</DocumentFooter>\n" +
      "    </DocumentFooters>\n" +
      "    <CarbonEmissionInformation/>\n" +
      "  </DocumentDetails>\n" +
      "  <Booking>\n" +
      "    <BookingDetails>\n" +
      "      <BookingNumber Label=\"Booking ID: \">579236</BookingNumber>\n" +
      "      <PNRDetails>\n" +
      "        <PNRList/>\n" +
      "      </PNRDetails>\n" +
      "      <BookingConsultant1>agent</BookingConsultant1>\n" +
      "      <BookingConsultant1EmailAddress>shusma1@tramada.com</BookingConsultant1EmailAddress>\n" +
      "      <BookingConsultant2/>\n" +
      "      <TravelApprover/>\n" +
      "      <BookedBy Label=\"Booked By: \">Sharyne Andrews</BookedBy>\n" +
      "      <BookingDestinationCity/>\n" +
      "      <BookingDepartureDate Label=\"Departure Date: \">31 Jan 18</BookingDepartureDate>\n" +
      "      <BookingFinalTKTDate Label=\"Final TKT Date: \">24 Jan 18</BookingFinalTKTDate>\n" +
      "      <BookingCancellationCondition/>\n" +
      "      <BookingDeposits/>\n" +
      "      <BookingDebtors>\n" +
      "        <BookingDebtor>\n" +
      "          <DebtorName Label=\"Debtor: \">KAL</DebtorName>\n" +
      "        </BookingDebtor>\n" +
      "      </BookingDebtors>\n" +
      "      <BookingPassengers>\n" +
      "        <BookingPassenger>\n" +
      "          <PassengerName>KAALZ/TEST MR</PassengerName>\n" +
      "        </BookingPassenger>\n" +
      "        <BookingPassenger>\n" +
      "          <PassengerName>LEONARD/GERARDMR</PassengerName>\n" +
      "          <Memberships>\n" +
      "            <Membership>\n" +
      "              <MembershipNumber>2045223</MembershipNumber>\n" +
      "              <MembershipCompanyName>QANTAS AIRWAYS</MembershipCompanyName>\n" +
      "              <MembershipCompanyCode>QF</MembershipCompanyCode>\n" +
      "              <MembershipProgramName>Qantas</MembershipProgramName>\n" +
      "              <MembershipProgramCode>QF</MembershipProgramCode>\n" +
      "            </Membership>\n" +
      "          </Memberships>\n" +
      "        </BookingPassenger>\n" +
      "        <BookingPassenger>\n" +
      "          <PassengerName>MR MARCO POLO</PassengerName>\n" +
      "        </BookingPassenger>\n" +
      "        <BookingPassenger>\n" +
      "          <PassengerName>MRS MARCH POLO</PassengerName>\n" +
      "        </BookingPassenger>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerName>MR MARCO POLO</PassengerName>\n" +
      "          <PassengerAge>36</PassengerAge>\n" +
      "        </PassengerDetails>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerName>MRS MARCH POLO</PassengerName>\n" +
      "          <PassengerAge>34</PassengerAge>\n" +
      "        </PassengerDetails>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerName>LEONARD/GERARDMR</PassengerName>\n" +
      "          <PassengerAge>ADULT</PassengerAge>\n" +
      "        </PassengerDetails>\n" +
      "        <TicketNumbers/>\n" +
      "        <PassengerNameList Label=\"Itinerary For\">KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList>\n" +
      "      </BookingPassengers>\n" +
      "      <BookingStatus Label=\"Booking Status: \">QUOTE</BookingStatus>\n" +
      "    </BookingDetails>\n" +
      "    <ItineraryItems>\n" +
      "      <ItineraryItem>\n" +
      "        <ItemNo>3906593</ItemNo>\n" +
      "        <SortingOrderNumber>1</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"FLIGHT\">Flight</SegmentTypeCode>\n" +
      "        <PassengerDetails>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>KAALZ/TEST MR</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>LEONARD/GERARDMR</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>MR MARCO POLO</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>MRS MARCH POLO</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList>\n" +
      "        </PassengerDetails>\n" +
      "        <Supplier>\n" +
      "          <SupplierType>Flight Company</SupplierType>\n" +
      "          <SupplierName>AMERICAN AIRLINES</SupplierName>\n" +
      "          <SupplierAddress>\n" +
      "            <Address>\n" +
      "              <Address1/>\n" +
      "              <Address2/>\n" +
      "              <Address3/>\n" +
      "              <PostCode/>\n" +
      "              <State/>\n" +
      "              <Country/>\n" +
      "            </Address>\n" +
      "          </SupplierAddress>\n" +
      "          <SupplierFax/>\n" +
      "        </Supplier>\n" +
      "        <ServiceIdentifier/>\n" +
      "        <Start>\n" +
      "          <StartLabelValue>Departure</StartLabelValue>\n" +
      "          <ServiceDate>Fri 19 Jan 18</ServiceDate>\n" +
      "          <ServiceTime>14:20</ServiceTime>\n" +
      "          <CityCode>DFW</CityCode>\n" +
      "          <CityName>DALLAS FT WORTH, TX</CityName>\n" +
      "          <LocationDescription Label=\"Departure Terminal\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </Start>\n" +
      "        <End>\n" +
      "          <EndLabelValue>Arrival</EndLabelValue>\n" +
      "          <ServiceDate>Sat 20 Jan 18</ServiceDate>\n" +
      "          <ServiceTime>09:25</ServiceTime>\n" +
      "          <CityCode>SYD</CityCode>\n" +
      "          <CityName>Kingsford Smith, Sydney</CityName>\n" +
      "          <LocationDescription Label=\"Arrival Terminal\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </End>\n" +
      "        <FlightSpecificDetails>\n" +
      "          <FlightSpecificMealsSeats>KAALZ/TEST MR|LEONARD/GERARDMR|MR MARCO POLO|MRS MARCH POLO</FlightSpecificMealsSeats>\n" +
      "          <Legs>\n" +
      "            <LegsSummary>DALLAS FT WORTH, TX (TERMINAL - ) Kingsford Smith, Sydney (TERMINAL - ), Dept Time 19-01-2018 14:20, Arrival Time 20-01-2018 09:25</LegsSummary>\n" +
      "            <Leg>\n" +
      "              <StopOverLeg Label=\"Journey Info\">0</StopOverLeg>\n" +
      "              <DepartureCity Label=\"Departure City\">DALLAS FT WORTH, TX</DepartureCity>\n" +
      "              <DepartureDate Label=\"Departure Date\">19-01-2018</DepartureDate>\n" +
      "              <DepartureTime Label=\"Departure Time\">14:20</DepartureTime>\n" +
      "              <ArrivalCity Label=\"Arrival City\">Kingsford Smith, Sydney</ArrivalCity>\n" +
      "              <ArrivalDate Label=\"Arrival Date\">20-01-2018</ArrivalDate>\n" +
      "              <ArrivalTime Label=\"Arrival Time\">09:25</ArrivalTime>\n" +
      "            </Leg>\n" +
      "          </Legs>\n" +
      "        </FlightSpecificDetails>\n" +
      "        <Status>Confirmed</Status>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <OrderDate/>\n" +
      "        <AccountDetails>\n" +
      "          <PaymentTypeDescription/>\n" +
      "          <PaymentNarrative/>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription/>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "          <FareRuleNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <RateInformation/>\n" +
      "      </ItineraryItem>\n" +
      "      <ItineraryItem>\n" +
      "        <ItemNo>3906596</ItemNo>\n" +
      "        <SortingOrderNumber>2</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"HOTEL\">Hotel</SegmentTypeCode>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList>\n" +
      "        </PassengerDetails>\n" +
      "        <Supplier>\n" +
      "          <SupplierType>Hotel Company</SupplierType>\n" +
      "          <SupplierCode Label=\"Supplier Code\">MCSYD7468-1</SupplierCode>\n" +
      "          <SupplierName>MARRIOTT SYDNEY HARBOUR</SupplierName>\n" +
      "          <SupplierAddress>\n" +
      "            <Address>\n" +
      "              <Address1>30 PITT STREET</Address1>\n" +
      "              <Address2>SYDNEY AU 2000</Address2>\n" +
      "              <Address3/>\n" +
      "              <PostCode/>\n" +
      "              <State/>\n" +
      "              <Country>Australia</Country>\n" +
      "            </Address>\n" +
      "          </SupplierAddress>\n" +
      "          <SupplierPhone Label=\"Phone\">P-612-9259-7000</SupplierPhone>\n" +
      "          <SupplierFax>F-612-9251-1122</SupplierFax>\n" +
      "        </Supplier>\n" +
      "        <Start>\n" +
      "          <StartLabelValue>Check In</StartLabelValue>\n" +
      "          <ServiceDate>Sat 20 Jan 18</ServiceDate>\n" +
      "          <ServiceTime>06:00</ServiceTime>\n" +
      "          <CityCode>SYD</CityCode>\n" +
      "          <CityName>Kingsford Smith, Sydney</CityName>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </Start>\n" +
      "        <End>\n" +
      "          <EndLabelValue>Check Out</EndLabelValue>\n" +
      "          <ServiceDate>Tue 23 Jan 18</ServiceDate>\n" +
      "          <ServiceTime>02:00</ServiceTime>\n" +
      "          <CityCode/>\n" +
      "          <CityName/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </End>\n" +
      "        <Status>Confirmed</Status>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <OrderDate/>\n" +
      "        <AccountDetails>\n" +
      "          <PaymentTypeDescription>Prepaid</PaymentTypeDescription>\n" +
      "          <PaymentNarrative>Pre-paid remark test cccf</PaymentNarrative>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription/>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "          <FareRuleNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <NoOfItems Label=\"No. of Rooms\">1</NoOfItems>\n" +
      "        <NoOfdays UnitOfCount=\"Nights\" Label=\"Duration\">3</NoOfdays>\n" +
      "        <RateInformation>\n" +
      "          <AUDRate Label=\"Rate Incl GST\">AUD260.00</AUDRate>\n" +
      "          <LocalRate Label=\"Local Rate Incl GST\">AUD260.00</LocalRate>\n" +
      "        </RateInformation>\n" +
      "      </ItineraryItem>\n" +
      "      <ItineraryItem>\n" +
      "        <ItemNo>3906592</ItemNo>\n" +
      "        <SortingOrderNumber>3</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"FLIGHT\">Flight</SegmentTypeCode>\n" +
      "        <PassengerDetails>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>KAALZ/TEST MR</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>LEONARD/GERARDMR</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>MR MARCO POLO</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>MRS MARCH POLO</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList>\n" +
      "        </PassengerDetails>\n" +
      "        <Supplier>\n" +
      "          <SupplierType>Flight Company</SupplierType>\n" +
      "          <SupplierName>AMERICAN AIRLINES</SupplierName>\n" +
      "          <SupplierAddress>\n" +
      "            <Address>\n" +
      "              <Address1/>\n" +
      "              <Address2/>\n" +
      "              <Address3/>\n" +
      "              <PostCode/>\n" +
      "              <State/>\n" +
      "              <Country/>\n" +
      "            </Address>\n" +
      "          </SupplierAddress>\n" +
      "          <SupplierFax/>\n" +
      "        </Supplier>\n" +
      "        <ServiceIdentifier/>\n" +
      "        <Start>\n" +
      "          <StartLabelValue>Departure</StartLabelValue>\n" +
      "          <ServiceDate>Tue 23 Jan 18</ServiceDate>\n" +
      "          <ServiceTime>14:20</ServiceTime>\n" +
      "          <CityCode>SYD</CityCode>\n" +
      "          <CityName>Kingsford Smith, Sydney</CityName>\n" +
      "          <LocationDescription Label=\"Departure Terminal\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </Start>\n" +
      "        <End>\n" +
      "          <EndLabelValue>Arrival</EndLabelValue>\n" +
      "          <ServiceDate>Tue 23 Jan 18</ServiceDate>\n" +
      "          <ServiceTime>09:25</ServiceTime>\n" +
      "          <CityCode>DFW</CityCode>\n" +
      "          <CityName>DALLAS FT WORTH, TX</CityName>\n" +
      "          <LocationDescription Label=\"Arrival Terminal\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </End>\n" +
      "        <FlightSpecificDetails>\n" +
      "          <FlightSpecificMealsSeats>KAALZ/TEST MR|LEONARD/GERARDMR|MR MARCO POLO|MRS MARCH POLO</FlightSpecificMealsSeats>\n" +
      "          <Legs>\n" +
      "            <LegsSummary>Kingsford Smith, Sydney (TERMINAL - ) DALLAS FT WORTH, TX (TERMINAL - ), Dept Time 23-01-2018 14:20, Arrival Time 20-01-2018 09:25</LegsSummary>\n" +
      "            <Leg>\n" +
      "              <StopOverLeg Label=\"Journey Info\">0</StopOverLeg>\n" +
      "              <DepartureCity Label=\"Departure City\">Kingsford Smith, Sydney</DepartureCity>\n" +
      "              <DepartureDate Label=\"Departure Date\">23-01-2018</DepartureDate>\n" +
      "              <DepartureTime Label=\"Departure Time\">14:20</DepartureTime>\n" +
      "              <ArrivalCity Label=\"Arrival City\">DALLAS FT WORTH, TX</ArrivalCity>\n" +
      "              <ArrivalDate Label=\"Arrival Date\">20-01-2018</ArrivalDate>\n" +
      "              <ArrivalTime Label=\"Arrival Time\">09:25</ArrivalTime>\n" +
      "            </Leg>\n" +
      "          </Legs>\n" +
      "        </FlightSpecificDetails>\n" +
      "        <Status>Confirmed</Status>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <OrderDate/>\n" +
      "        <AccountDetails>\n" +
      "          <PaymentTypeDescription/>\n" +
      "          <PaymentNarrative/>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription/>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "          <FareRuleNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <RateInformation/>\n" +
      "      </ItineraryItem>\n" +
      "      <ItineraryItem>\n" +
      "        <ItemNo>3906595</ItemNo>\n" +
      "        <SortingOrderNumber>4</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"CAR\">Car</SegmentTypeCode>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList>\n" +
      "        </PassengerDetails>\n" +
      "        <Supplier>\n" +
      "          <SupplierType>Car Company</SupplierType>\n" +
      "          <SupplierName>V Australia</SupplierName>\n" +
      "          <SupplierAddress>\n" +
      "            <Address>\n" +
      "              <Address1/>\n" +
      "              <Address2/>\n" +
      "              <Address3/>\n" +
      "              <PostCode/>\n" +
      "              <State/>\n" +
      "              <Country/>\n" +
      "            </Address>\n" +
      "          </SupplierAddress>\n" +
      "          <SupplierFax/>\n" +
      "        </Supplier>\n" +
      "        <Start>\n" +
      "          <StartLabelValue>Pick Up</StartLabelValue>\n" +
      "          <ServiceDate>Thu 25 Jan 18</ServiceDate>\n" +
      "          <ServiceTime/>\n" +
      "          <CityCode>MEL</CityCode>\n" +
      "          <CityName>MELBOURNE, AUSTRALIA</CityName>\n" +
      "          <LocationTypeDescription Label=\"Pick Up Location Type\"/>\n" +
      "          <LocationDescription Label=\"Pick Up location\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </Start>\n" +
      "        <End>\n" +
      "          <EndLabelValue>Drop Off</EndLabelValue>\n" +
      "          <ServiceDate>Thu 25 Jan 18</ServiceDate>\n" +
      "          <ServiceTime/>\n" +
      "          <CityCode>MEL APRT</CityCode>\n" +
      "          <CityName>MEL APRT</CityName>\n" +
      "          <LocationTypeDescription Label=\"Drop Off Location Type\"/>\n" +
      "          <LocationDescription Label=\"Drop Off Location\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </End>\n" +
      "        <Status>Confirmed</Status>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <OrderDate/>\n" +
      "        <AccountDetails>\n" +
      "          <PaymentTypeDescription>Prepaid</PaymentTypeDescription>\n" +
      "          <PaymentNarrative>Pre-paid remark test cccf</PaymentNarrative>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription>Cancel 10 Hours Prior to Arrival</CancellationDescription>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "          <FareRuleNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <NoOfItems Label=\"No. of Cars\">1</NoOfItems>\n" +
      "        <NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays>\n" +
      "        <RateInformation>\n" +
      "          <AUDRate Label=\"Rate Incl GST\">AUD200.00</AUDRate>\n" +
      "          <LocalRate Label=\"Local Rate Incl GST\">AUD200.00</LocalRate>\n" +
      "        </RateInformation>\n" +
      "      </ItineraryItem>\n" +
      "    </ItineraryItems>\n" +
      "    <CostingsItems>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>2218755</ItemNo>\n" +
      "        <SortingOrderNumber>1</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"TICKET\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>BSP-IATA</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode>AA</AirlineCode>\n" +
      "          <TKTPassenger>KAALZ/TEST MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode>Y</ClassCode>\n" +
      "          <ClassDescription>Economy</ClassDescription>\n" +
      "        </TKTSpecific>\n" +
      "        <ShortItinerary Label=\"Short Itinerary\">DALLAS FT WORTH-Kingsford Smith-DALLAS FT WORTH</ShortItinerary>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <CurrencyCode/>\n" +
      "          <BaseFare>1400.00</BaseFare>\n" +
      "          <Discount Label=\"Discount ex GST\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>1400.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "        <CancellationDescription Label=\"Cancellation\">24 hours cancellation notice required</CancellationDescription>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>2218756</ItemNo>\n" +
      "        <SortingOrderNumber>2</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"CAR\">Car</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>V Australia</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <StartServiceDate Label=\"Start Service Date\">25 Jan 18</StartServiceDate>\n" +
      "        <StartCity>MELBOURNE, AUSTRALIA</StartCity>\n" +
      "        <StartCityName Label=\"Pick-Up City\"/>\n" +
      "        <EndServiceDate Label=\"End Service Date\">25 Jan 18</EndServiceDate>\n" +
      "        <EndCity>MEL APRT</EndCity>\n" +
      "        <EndCityName Label=\"Drop-off City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <CurrencyCode>AUD</CurrencyCode>\n" +
      "          <BaseFare>181.82</BaseFare>\n" +
      "          <Discount Label=\"Discount ex GST\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">18.18</GST>\n" +
      "          <ClientDue>200.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "        <CancellationDescription Label=\"Cancellation\">Cancel 10 Hours Prior to Arrival</CancellationDescription>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>2218757</ItemNo>\n" +
      "        <SortingOrderNumber>3</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"HOTEL\">Hotel</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>MARRIOTT SYDNEY HARBOUR</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <StartServiceDate Label=\"Start Service Date\">20 Jan 18</StartServiceDate>\n" +
      "        <StartCity>Kingsford Smith, Sydney</StartCity>\n" +
      "        <StartCityName Label=\"Address\"/>\n" +
      "        <EndServiceDate Label=\"End Service Date\">23 Jan 18</EndServiceDate>\n" +
      "        <EndCity/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <CurrencyCode>AUD</CurrencyCode>\n" +
      "          <BaseFare>709.09</BaseFare>\n" +
      "          <Discount Label=\"Discount ex GST\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">70.91</GST>\n" +
      "          <ClientDue>780.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <DocumentTotals>\n" +
      "        <TotalPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>0.00</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST>\n" +
      "            <GST>0.00</GST>\n" +
      "            <ClientDue>0.00</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalPayDirect>\n" +
      "        <TotalExcPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>2290.91</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST>\n" +
      "            <GST>89.09</GST>\n" +
      "            <ClientDue>2380.00</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalExcPayDirect>\n" +
      "        <TotalIncPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>2290.91</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST>\n" +
      "            <GST>89.09</GST>\n" +
      "            <ClientDue>2380.00</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalIncPayDirect>\n" +
      "        <TotalPaid>0.00</TotalPaid>\n" +
      "        <TotalOutstandingExcPayDirect>2380.00</TotalOutstandingExcPayDirect>\n" +
      "        <Currency>AUD</Currency>\n" +
      "      </DocumentTotals>\n" +
      "    </CostingsItems>\n" +
      "  </Booking>\n" +
      "</DocumentInformation>\n";

  String xml10 = "<DocumentInformation>\n" +
      "  <Agency>\n" +
      "    <AgencyName Label=\"TMC\">Tests Travel Management</AgencyName>\n" +
      "    <AgencyCode Label=\"TMC Code\">trunk_tramada</AgencyCode>\n" +
      "    <AgencyAddress>\n" +
      "      <Address>\n" +
      "        <Address1 Label=\"Address Line 1\">trunkctm</Address1>\n" +
      "        <Address2 Label=\" Address Line 2\">Address 2</Address2>\n" +
      "        <Address3 Label=\" Address Line 3\">Address 3</Address3>\n" +
      "        <PostCode Label=\" Postcode\">2000</PostCode>\n" +
      "        <State Label=\"State\">NSW</State>\n" +
      "        <Country Label=\"Country\">Australia</Country>\n" +
      "      </Address>\n" +
      "    </AgencyAddress>\n" +
      "    <AgencyContacts>\n" +
      "      <AgencyEmail Label=\"Email: \">ashok@tramada.com</AgencyEmail>\n" +
      "      <AgencyPhone Label=\"Phone: \">0470632287</AgencyPhone>\n" +
      "      <AgencyFax Label=\"Fax No.: \">0470632287</AgencyFax>\n" +
      "    </AgencyContacts>\n" +
      "  </Agency>\n" +
      "  <DocumentDetails>\n" +
      "    <DocumentPrintDate>Wednesday 03 January 2018 08:36</DocumentPrintDate>\n" +
      "    <DocumentPrintTime>Brisbane, QLD</DocumentPrintTime>\n" +
      "    <DocumentTitle Label=\"Type\">Itinerary &amp; Costing - Full</DocumentTitle>\n" +
      "    <DocumentAddress>\n" +
      "      <Address/>\n" +
      "    </DocumentAddress>\n" +
      "    <DocumentContactDetails>\n" +
      "      <EmergencyContact Label=\"Emergency Contact: \">020000000</EmergencyContact>\n" +
      "      <AfterHours Label=\"After Hours Contact: \">020000000</AfterHours>\n" +
      "      <TollFree Label=\"Toll Free Number: \">1300 000 000</TollFree>\n" +
      "      <Phone Label=\"Phone: \">0470632287</Phone>\n" +
      "      <Fax Label=\"Fax: \">0470632287</Fax>\n" +
      "    </DocumentContactDetails>\n" +
      "    <DocumentCommercialDetails>\n" +
      "      <LicNo Label=\"Licence No.: \">lic1</LicNo>\n" +
      "      <ABN Label=\"ABN: \">52 005 000 895</ABN>\n" +
      "      <ACN Label=\"ACN: \">005 000 895</ACN>\n" +
      "      <MerchantNo Label=\"Merchant No: \">3333863706</MerchantNo>\n" +
      "      <ATAS Label=\"ATAS Accreditation Number: \">atas1</ATAS>\n" +
      "    </DocumentCommercialDetails>\n" +
      "    <DocumentHeaders>\n" +
      "      <DocumentHeader HeaderTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually There®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentHeader>\n" +
      "      <DocumentHeader HeaderTitle=\"Third party hotel booking\">Please note: your hotel has been booked via a third party provider. They have provided a confirmation number and this may be required at check-in. In most instances you will also have been provided with a confirmation and payment voucher separately to the itinerary. Please ensure you have printed a copy of this voucher and are able to produce it upon request. There can be occasional communication problems between third party bookers and hotels. If you experience any difficulties at check-in please call CTM on the numbers below.</DocumentHeader>\n" +
      "      <DocumentHeader HeaderTitle=\"label\">Testing Helloworld Header</DocumentHeader>\n" +
      "      <DocumentHeader HeaderTitle=\"CTM EMERGENCY AFTER HOURS\">Should you require assistance with booking changes or amendments relating to imminent departures, please contact our emergency afterhours service on the following numbers:Within Australia - 1800 105 334Outside Australia -  +61 2 8268 4100.Please note: Service Fees will apply as per your service agreement.Due to privacy laws we are unable to release travel details to anyone other than the passenger.</DocumentHeader>\n" +
      "    </DocumentHeaders>\n" +
      "    <DocumentFooters>\n" +
      "      <DocumentFooter FooterTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually There®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"Assistance while overseas\">Subsea 7 provide assistance in emergencies arising when personnel are travelling overseas. Comprehensive emergency assistance is available to Employees through International SOS, these cover Medical Services and Security.The following information is required for International SOS:SOS Programme Number: 14ACPA000020Employer: Subsea 724h phone number: +61 2 9372 2468www.internationalsos.com</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"DOMESTIC BAGGAGE (JQ/QF/DJ)\">JETSTAREconomy Starter fare includes 10kg of carry- on luggage, customise your requirements at time of booking. http://www.jetstar.com/au/en/planning-and-booking/baggage/checked-baggageQANTASEconomy Class: 1 piece of cabin baggage weighing 7kg + 1 piece of checked baggage weighing maximum 23kg.Business Class: 1 piece of cabin baggage weighing 7kg + 1 piece of checked baggage weighing 32kg per piece.Silver/Gold/Platinum frequent flyers receive extra baggage concessions.http://www.qantas.com.au/travel/airlines/checked-baggage/global/en#jump0VIRGIN AUSTRALIAEconomy Class Saver Fare no allowance: prepaid baggage up to 23kg will cost a flat rate of $12 per flight.Cabin baggage - Up to 7kgs complimentary.NOTE - If paid at check-in up to 23kg will cost a flat rate of $20 per flight.Velocity Gold Members: free extended baggage limit of 32kg checked baggage.Business Class/Premium Economy: permitted 69kg of checked baggage.http://www.virginaustralia.com/Personal/Flightinfo/BeforeYouFly/Baggagedangerousgoods/index.htm</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"DOMESTIC CHECK IN (JQ/QF/DJ)\">JETSTARCheck-in for Jetstar flights leaving from any domestic Australian destination is a minimum (recommended 60 minutes) before your scheduled flight. Check in opens 2 hours prior to the scheduled flights and closes 30 minutes prior to scheduled departure time.http://www.jetstar.com/au/en/planning-and-booking/checking-in/web-check-inQANTASQANTAS domestic flights check-in closes 30 minutes prior to scheduled departure time - except for flights numbered QF2000-QF2299 and QF7000-QF7299 departing from Sydney, check-in time is 1 hour. Online check-in is now available for QANTAS and QANTAS Link Australian domestic bookings between 24 hours and 1 hour before your flight departure.http://www.qantas.com.au/info/flying/atTheAirport/checkinTimesVIRGIN AUSTRALIAVirgin Australia domestic flights check-in closes 30 minutes prior to scheduled departure time. Virgin Blue online check-in is available and opens 24 hours before departure of your flights. Passengers wanting to utilise this facility must present a print out of your boarding pass at check in.http://www.virginaustralia.com/Personal/Services/Check-inoptions/index.htm</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"DOMESTIC FARE CONDITIONS (JQ/QF/DJ)\">JETSTAR - http://travelctm.com/uploads/JQFareGrid.pdfQANTAS - http://travelctm.com/uploads/QFDomesticandNZFareGrid.pdfVIRGIN AUSTRALIA - http://travelctm.com/uploads/DJFaregrid.pdfNOTE: Business Class Virgin Australia only offered on A330-200 aircraft (subject to change).</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"E-TICKET\">E ticket identification required by airlines:Photo identification that has been issued by the Commonwealth of Australia or an Australian State or Territory. Acceptable photo identification includes: drivers license, passport, any state, territory or federal government issued card, company issued identification, or a student card. Qantas acceptable non photo identification includes: credit card used to pay for the ticket, other credit or debit cards, social security card, QF club or frequent flyer card, certified copy/original of a birth of citizenship document. All ID?s must be current and valid.</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"FREQUENT FLYER\">* Please advise your airline loyalty number if applicable. Once received we will enter this into your booking and your profile for future.* To ensure you receive your frequent flyer points, please retain tickets and boarding passes where possible until your points have appeared on your frequent flyer statement.* Should your points not appear on your statement, please send copies of your boarding passes, tickets and supporting information to the relevant airline.</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"IATA\">Transportation and other services provided by the carrier are subject to conditions of contract and other important notices which are delivered with thisitinerary/receipt and form part of the nearest office of the issuing airline or travel agent to obtain a copy prior to the commencement of your trip. Notice ifthe passenger's journey involves an ultimate destination or stop in a country other than the country of departure the Warsaw convention may be applicableand the convention governs and in most cases limits the liability of carriers for death or personal injury and in respect of loss or damage to baggage.</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"ITINERARY ACCEPTANCE\">Whilst all care has been taken to ensure your travel requirements are booked as per your request, CTM requests that you accept responsibility for double checking these details, and advise of any changes that need to be made as soon as possible. Upon receiving your itinerary please check and make sure the relevant booking is correct and as per your travel needs.</DocumentFooter>\n" +
      "    </DocumentFooters>\n" +
      "    <CarbonEmissionInformation/>\n" +
      "  </DocumentDetails>\n" +
      "  <Booking>\n" +
      "    <BookingDetails>\n" +
      "      <BookingNumber Label=\"Booking ID: \">579186</BookingNumber>\n" +
      "      <PNRDetails>\n" +
      "        <PNRList/>\n" +
      "      </PNRDetails>\n" +
      "      <BookingConsultant1>agent</BookingConsultant1>\n" +
      "      <BookingConsultant1EmailAddress>tester18@tramada.com</BookingConsultant1EmailAddress>\n" +
      "      <BookingConsultant2/>\n" +
      "      <TravelApprover/>\n" +
      "      <BookedBy Label=\"Booked By: \">KALAI SELVAN</BookedBy>\n" +
      "      <BookingDestinationCity/>\n" +
      "      <BookingDepartureDate Label=\"Departure Date: \">05 Dec 17</BookingDepartureDate>\n" +
      "      <BookingFinalTKTDate Label=\"Final TKT Date: \">\n" +
      "      </BookingFinalTKTDate>\n" +
      "      <BookingCancellationCondition/>\n" +
      "      <BookingDeposits/>\n" +
      "      <BookingDebtors>\n" +
      "        <BookingDebtor>\n" +
      "          <DebtorName Label=\"Debtor: \">BETA TECHNOLOGY</DebtorName>\n" +
      "        </BookingDebtor>\n" +
      "      </BookingDebtors>\n" +
      "      <BookingPassengers>\n" +
      "        <BookingPassenger>\n" +
      "          <PassengerName>KIM MARK</PassengerName>\n" +
      "        </BookingPassenger>\n" +
      "        <BookingPassenger>\n" +
      "          <PassengerName>SAURABH</PassengerName>\n" +
      "        </BookingPassenger>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerName>KIM MARK</PassengerName>\n" +
      "          <PassengerAge>29</PassengerAge>\n" +
      "        </PassengerDetails>\n" +
      "        <TicketNumbers/>\n" +
      "        <PassengerNameList Label=\"Itinerary For\">KIM MARK, SAURABH</PassengerNameList>\n" +
      "      </BookingPassengers>\n" +
      "      <BookingStatus Label=\"Booking Status: \">QUOTE</BookingStatus>\n" +
      "    </BookingDetails>\n" +
      "    <ItineraryItems>\n" +
      "      <ItineraryItem>\n" +
      "        <ItemNo>3906419</ItemNo>\n" +
      "        <SortingOrderNumber>1</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"MISC\">OTHER</SegmentTypeCode>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerNameList>KIM MARK, SAURABH</PassengerNameList>\n" +
      "        </PassengerDetails>\n" +
      "        <Supplier>\n" +
      "          <SupplierType>OTHER Company</SupplierType>\n" +
      "          <SupplierName>KADINA GATEWAY MOTOR INN</SupplierName>\n" +
      "          <SupplierAddress>\n" +
      "            <Address>\n" +
      "              <Address1/>\n" +
      "              <Address2/>\n" +
      "              <Address3/>\n" +
      "              <PostCode/>\n" +
      "              <State/>\n" +
      "              <Country/>\n" +
      "            </Address>\n" +
      "          </SupplierAddress>\n" +
      "          <SupplierFax/>\n" +
      "        </Supplier>\n" +
      "        <Start>\n" +
      "          <StartLabelValue>Departure</StartLabelValue>\n" +
      "          <ServiceDate>Wed 06 Dec 17</ServiceDate>\n" +
      "          <ServiceTime/>\n" +
      "          <CityCode>ADELAIDE AIPORT</CityCode>\n" +
      "          <CityName>ADELAIDE AIPORT</CityName>\n" +
      "          <LocationDescription Label=\"Start Location\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </Start>\n" +
      "        <End>\n" +
      "          <EndLabelValue>Arrival</EndLabelValue>\n" +
      "          <ServiceDate>Wed 06 Dec 17</ServiceDate>\n" +
      "          <ServiceTime/>\n" +
      "          <CityCode>ADELAIDE AIPORT</CityCode>\n" +
      "          <CityName>ADELAIDE AIPORT</CityName>\n" +
      "          <LocationDescription Label=\"Drop Off Location\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </End>\n" +
      "        <Status>Confirmed</Status>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <OrderDate/>\n" +
      "        <AccountDetails>\n" +
      "          <PaymentTypeDescription>Prepaid</PaymentTypeDescription>\n" +
      "          <PaymentNarrative>Pre-paid remark test cccf</PaymentNarrative>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription>24 hours cancellation notice required</CancellationDescription>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "          <FareRuleNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <NoOfItems Label=\"No. of Units\">1</NoOfItems>\n" +
      "        <NoOfdays UnitOfCount=\"Nights\" Label=\"Duration\">1</NoOfdays>\n" +
      "        <RateInformation>\n" +
      "          <AUDRate Label=\"Rate Incl GST\">AUD10.00</AUDRate>\n" +
      "          <LocalRate Label=\"Local Rate Incl GST\">AUD10.00</LocalRate>\n" +
      "        </RateInformation>\n" +
      "      </ItineraryItem>\n" +
      "      <ItineraryItem>\n" +
      "        <ItemNo>3906417</ItemNo>\n" +
      "        <SortingOrderNumber>2</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"COMMENT\">Comment</SegmentTypeCode>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerNameList/>\n" +
      "        </PassengerDetails>\n" +
      "        <Supplier>\n" +
      "          <SupplierType>Comment Company</SupplierType>\n" +
      "          <SupplierName/>\n" +
      "          <SupplierAddress>\n" +
      "            <Address>\n" +
      "              <Address1/>\n" +
      "              <Address2/>\n" +
      "              <Address3/>\n" +
      "              <PostCode/>\n" +
      "              <State/>\n" +
      "              <Country/>\n" +
      "            </Address>\n" +
      "          </SupplierAddress>\n" +
      "          <SupplierFax/>\n" +
      "        </Supplier>\n" +
      "        <ServiceTypeDescription Label=\"Type\">TEST COMMENT</ServiceTypeDescription>\n" +
      "        <Start>\n" +
      "          <StartLabelValue>Start</StartLabelValue>\n" +
      "          <ServiceDate>Tue 12 Dec 17</ServiceDate>\n" +
      "          <ServiceTime>11:00</ServiceTime>\n" +
      "          <CityCode/>\n" +
      "          <CityName/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </Start>\n" +
      "        <End>\n" +
      "          <EndLabelValue>End</EndLabelValue>\n" +
      "          <ServiceDate>Thu 14 Dec 17</ServiceDate>\n" +
      "          <ServiceTime>12:00</ServiceTime>\n" +
      "          <CityCode/>\n" +
      "          <CityName/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </End>\n" +
      "        <Status/>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <OrderDate/>\n" +
      "        <AccountDetails>\n" +
      "          <PaymentTypeDescription/>\n" +
      "          <PaymentNarrative/>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription/>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "          <FareRuleNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <RateInformation/>\n" +
      "      </ItineraryItem>\n" +
      "      <ItineraryItem>\n" +
      "        <ItemNo>3906416</ItemNo>\n" +
      "        <SortingOrderNumber>3</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"BUS\">Bus</SegmentTypeCode>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerNameList>KIM MARK, SAURABH</PassengerNameList>\n" +
      "        </PassengerDetails>\n" +
      "        <Supplier>\n" +
      "          <SupplierType>Bus Company</SupplierType>\n" +
      "          <SupplierName/>\n" +
      "          <SupplierAddress>\n" +
      "            <Address>\n" +
      "              <Address1/>\n" +
      "              <Address2/>\n" +
      "              <Address3/>\n" +
      "              <PostCode/>\n" +
      "              <State/>\n" +
      "              <Country/>\n" +
      "            </Address>\n" +
      "          </SupplierAddress>\n" +
      "          <SupplierFax/>\n" +
      "        </Supplier>\n" +
      "        <Start>\n" +
      "          <StartLabelValue>Departure</StartLabelValue>\n" +
      "          <ServiceDate>Thu 14 Dec 17</ServiceDate>\n" +
      "          <ServiceTime/>\n" +
      "          <CityCode>SYD</CityCode>\n" +
      "          <CityName>Kingsford Smith, Sydney</CityName>\n" +
      "          <LocationDescription Label=\"Start Location\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </Start>\n" +
      "        <End>\n" +
      "          <EndLabelValue>Arrival</EndLabelValue>\n" +
      "          <ServiceDate>Thu 14 Dec 17</ServiceDate>\n" +
      "          <ServiceTime/>\n" +
      "          <CityCode>MEL</CityCode>\n" +
      "          <CityName>MELBOURNE, AUSTRALIA</CityName>\n" +
      "          <LocationDescription Label=\"Drop Off Location\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </End>\n" +
      "        <Status>Confirmed</Status>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <OrderDate/>\n" +
      "        <AccountDetails>\n" +
      "          <PaymentTypeDescription>Prepaid</PaymentTypeDescription>\n" +
      "          <PaymentNarrative>Pre-paid remark test cccf</PaymentNarrative>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription/>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "          <FareRuleNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <NoOfItems Label=\"No. Of Passengers\">1</NoOfItems>\n" +
      "        <NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays>\n" +
      "        <RateInformation>\n" +
      "          <AUDRate Label=\"Rate Incl GST\">AUD120.00</AUDRate>\n" +
      "          <LocalRate Label=\"Local Rate Incl GST\">AUD120.00</LocalRate>\n" +
      "        </RateInformation>\n" +
      "      </ItineraryItem>\n" +
      "      <ItineraryItem>\n" +
      "        <ItemNo>3906418</ItemNo>\n" +
      "        <SortingOrderNumber>4</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"HOTEL\">Hotel</SegmentTypeCode>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerNameList>KIM MARK, SAURABH</PassengerNameList>\n" +
      "        </PassengerDetails>\n" +
      "        <Supplier>\n" +
      "          <SupplierType>Hotel Company</SupplierType>\n" +
      "          <SupplierName>Test&amp;creditor</SupplierName>\n" +
      "          <SupplierAddress>\n" +
      "            <Address>\n" +
      "              <Address1/>\n" +
      "              <Address2/>\n" +
      "              <Address3/>\n" +
      "              <PostCode/>\n" +
      "              <State/>\n" +
      "              <Country/>\n" +
      "            </Address>\n" +
      "          </SupplierAddress>\n" +
      "          <SupplierFax/>\n" +
      "        </Supplier>\n" +
      "        <Start>\n" +
      "          <StartLabelValue>Check In</StartLabelValue>\n" +
      "          <ServiceDate>Thu 14 Dec 17</ServiceDate>\n" +
      "          <ServiceTime>06:00</ServiceTime>\n" +
      "          <CityCode>PER</CityCode>\n" +
      "          <CityName>PERTH, AUSTRALIA</CityName>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </Start>\n" +
      "        <End>\n" +
      "          <EndLabelValue>Check Out</EndLabelValue>\n" +
      "          <ServiceDate>Fri 15 Dec 17</ServiceDate>\n" +
      "          <ServiceTime>02:00</ServiceTime>\n" +
      "          <CityCode/>\n" +
      "          <CityName/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </End>\n" +
      "        <Status>Confirmed</Status>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <OrderDate/>\n" +
      "        <AccountDetails>\n" +
      "          <PaymentTypeDescription>Prepaid</PaymentTypeDescription>\n" +
      "          <PaymentNarrative>Free of Charge</PaymentNarrative>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription/>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "          <FareRuleNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <NoOfItems Label=\"No. of Rooms\">1</NoOfItems>\n" +
      "        <NoOfdays UnitOfCount=\"Nights\" Label=\"Duration\">1</NoOfdays>\n" +
      "        <RateInformation>\n" +
      "          <AUDRate Label=\"Rate Incl GST\">AUD80.00</AUDRate>\n" +
      "          <LocalRate Label=\"Local Rate Incl GST\">AUD80.00</LocalRate>\n" +
      "        </RateInformation>\n" +
      "      </ItineraryItem>\n" +
      "    </ItineraryItems>\n" +
      "    <CostingsItems>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>2218625</ItemNo>\n" +
      "        <SortingOrderNumber>1</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"BUS\">Bus</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>TABCORP PARK</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <StartServiceDate Label=\"Start Service Date\">14 Dec 17</StartServiceDate>\n" +
      "        <StartCity>Kingsford Smith, Sydney</StartCity>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndServiceDate Label=\"End Service Date\">14 Dec 17</EndServiceDate>\n" +
      "        <EndCity>MELBOURNE, AUSTRALIA</EndCity>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <CurrencyCode>AUD</CurrencyCode>\n" +
      "          <BaseFare>109.09</BaseFare>\n" +
      "          <Discount Label=\"Discount ex GST\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">10.91</GST>\n" +
      "          <ClientDue>120.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>2218626</ItemNo>\n" +
      "        <SortingOrderNumber>2</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"HOTEL\">Hotel</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>Test&amp;creditor</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <StartServiceDate Label=\"Start Service Date\">14 Dec 17</StartServiceDate>\n" +
      "        <StartCity>PERTH, AUSTRALIA</StartCity>\n" +
      "        <StartCityName Label=\"Address\"/>\n" +
      "        <EndServiceDate Label=\"End Service Date\">15 Dec 17</EndServiceDate>\n" +
      "        <EndCity/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <CurrencyCode>AUD</CurrencyCode>\n" +
      "          <BaseFare>72.73</BaseFare>\n" +
      "          <Discount Label=\"Discount ex GST\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">7.27</GST>\n" +
      "          <ClientDue>80.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>2218627</ItemNo>\n" +
      "        <SortingOrderNumber>3</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"MISC\">OTHER</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>KADINA GATEWAY MOTOR INN</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <StartServiceDate Label=\"Start Service Date\">06 Dec 17</StartServiceDate>\n" +
      "        <StartCity>ADELAIDE AIPORT</StartCity>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndServiceDate Label=\"End Service Date\">06 Dec 17</EndServiceDate>\n" +
      "        <EndCity>ADELAIDE AIPORT</EndCity>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <CurrencyCode>AUD</CurrencyCode>\n" +
      "          <BaseFare>9.09</BaseFare>\n" +
      "          <Discount Label=\"Discount ex GST\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.91</GST>\n" +
      "          <ClientDue>10.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "        <CancellationDescription Label=\"Cancellation\">24 hours cancellation notice required</CancellationDescription>\n" +
      "      </CostingItem>\n" +
      "      <DocumentTotals>\n" +
      "        <TotalPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>0.00</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST>\n" +
      "            <GST>0.00</GST>\n" +
      "            <ClientDue>0.00</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalPayDirect>\n" +
      "        <TotalExcPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>190.91</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST>\n" +
      "            <GST>19.09</GST>\n" +
      "            <ClientDue>210.00</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalExcPayDirect>\n" +
      "        <TotalIncPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>190.91</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST>\n" +
      "            <GST>19.09</GST>\n" +
      "            <ClientDue>210.00</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalIncPayDirect>\n" +
      "        <TotalPaid>0.00</TotalPaid>\n" +
      "        <TotalOutstandingExcPayDirect>210.00</TotalOutstandingExcPayDirect>\n" +
      "        <Currency>AUD</Currency>\n" +
      "      </DocumentTotals>\n" +
      "    </CostingsItems>\n" +
      "  </Booking>\n" +
      "</DocumentInformation>\n";

  String xml11 = "<DocumentInformation>\n" +
      "<Agency>\n" +
      "  <AgencyName Label=\"TMC\">Tests Travel Management</AgencyName>\n" +
      "  <AgencyCode Label=\"TMC Code\">trunk_tramada</AgencyCode>\n" +
      "  <AgencyAddress>\n" +
      "    <Address>\n" +
      "      <Address1 Label=\"Address Line 1\">trunkctm</Address1>\n" +
      "      <Address2 Label=\" Address Line 2\">Address 2</Address2>\n" +
      "      <Address3 Label=\" Address Line 3\">Address 3</Address3>\n" +
      "      <PostCode Label=\" Postcode\">2000</PostCode>\n" +
      "      <State Label=\"State\">NSW</State>\n" +
      "      <Country Label=\"Country\">Australia</Country>\n" +
      "    </Address>\n" +
      "  </AgencyAddress>\n" +
      "  <AgencyContacts>\n" +
      "    <AgencyEmail Label=\"Email: \">ashok@tramada.com</AgencyEmail>\n" +
      "    <AgencyPhone Label=\"Phone: \">0470632287</AgencyPhone>\n" +
      "    <AgencyFax Label=\"Fax No.: \">0470632287</AgencyFax>\n" +
      "  </AgencyContacts>\n" +
      "</Agency>\n" +
      "<DocumentDetails>\n" +
      "  <DocumentPrintDate>Wednesday 03 January 2018 03:25</DocumentPrintDate>\n" +
      "  <DocumentPrintTime>Sydney, NSW</DocumentPrintTime>\n" +
      "  <DocumentTitle Label=\"Type\">Itinerary &amp; Costing - Full</DocumentTitle>\n" +
      "  <DocumentAddress>\n" +
      "    <Address/>\n" +
      "  </DocumentAddress>\n" +
      "  <DocumentContactDetails>\n" +
      "    <EmergencyContact Label=\"Emergency Contact: \">1800 663 622</EmergencyContact>\n" +
      "    <AfterHours Label=\"After Hours Contact: \">1800 663 622</AfterHours>\n" +
      "    <TollFree Label=\"Toll Free Number: \">1800 663 622</TollFree>\n" +
      "    <Phone Label=\"Phone: \">0470632287</Phone>\n" +
      "    <Fax Label=\"Fax: \">0470632287</Fax>\n" +
      "  </DocumentContactDetails>\n" +
      "  <DocumentCommercialDetails>\n" +
      "    <LicNo Label=\"Licence No.: \">854</LicNo>\n" +
      "    <ABN Label=\"ABN: \">52 005 000 895</ABN>\n" +
      "    <ACN Label=\"ACN: \">005 000 895</ACN>\n" +
      "  </DocumentCommercialDetails>\n" +
      "  <DocumentHeaders>\n" +
      "    <DocumentHeader HeaderTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually ThereÂ®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentHeader>\n" +
      "    <DocumentHeader HeaderTitle=\"Third party hotel booking\">Please note: your hotel has been booked via a third party provider. They have provided a confirmation number and this may be required at check-in. In most instances you will also have been provided with a confirmation and payment voucher separately to the itinerary. Please ensure you have printed a copy of this voucher and are able to produce it upon request. There can be occasional communication problems between third party bookers and hotels. If you experience any difficulties at check-in please call CTM on the numbers below.</DocumentHeader>\n" +
      "    <DocumentHeader HeaderTitle=\"label\">Testing Helloworld Header</DocumentHeader>\n" +
      "  </DocumentHeaders>\n" +
      "  <DocumentFooters>\n" +
      "    <DocumentFooter FooterTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually ThereÂ®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentFooter>\n" +
      "    <DocumentFooter FooterTitle=\"Assistance while overseas\">Subsea 7 provide assistance in emergencies arising when personnel are travelling overseas. Comprehensive emergency assistance is available to Employees through International SOS, these cover Medical Services and Security.The following information is required for International SOS:SOS Programme Number: 14ACPA000020Employer: Subsea 724h phone number: +61 2 9372 2468www.internationalsos.com</DocumentFooter>\n" +
      "    <DocumentFooter FooterTitle=\"DOMESTIC BAGGAGE (JQ/QF/DJ)\">JETSTAREconomy Starter fare includes 10kg of carry- on luggage, customise your requirements at time of booking. http://www.jetstar.com/au/en/planning-and-booking/baggage/checked-baggageQANTASEconomy Class: 1 piece of cabin baggage weighing 7kg + 1 piece of checked baggage weighing maximum 23kg.Business Class: 1 piece of cabin baggage weighing 7kg + 1 piece of checked baggage weighing 32kg per piece.Silver/Gold/Platinum frequent flyers receive extra baggage concessions.http://www.qantas.com.au/travel/airlines/checked-baggage/global/en#jump0VIRGIN AUSTRALIAEconomy Class Saver Fare no allowance: prepaid baggage up to 23kg will cost a flat rate of $12 per flight.Cabin baggage - Up to 7kgs complimentary.NOTE - If paid at check-in up to 23kg will cost a flat rate of $20 per flight.Velocity Gold Members: free extended baggage limit of 32kg checked baggage.Business Class/Premium Economy: permitted 69kg of checked baggage.http://www.virginaustralia.com/Personal/Flightinfo/BeforeYouFly/Baggagedangerousgoods/index.htm</DocumentFooter>\n" +
      "    <DocumentFooter FooterTitle=\"DOMESTIC CHECK IN (JQ/QF/DJ)\">JETSTARCheck-in for Jetstar flights leaving from any domestic Australian destination is a minimum (recommended 60 minutes) before your scheduled flight. Check in opens 2 hours prior to the scheduled flights and closes 30 minutes prior to scheduled departure time.http://www.jetstar.com/au/en/planning-and-booking/checking-in/web-check-inQANTASQANTAS domestic flights check-in closes 30 minutes prior to scheduled departure time - except for flights numbered QF2000-QF2299 and QF7000-QF7299 departing from Sydney, check-in time is 1 hour. Online check-in is now available for QANTAS and QANTAS Link Australian domestic bookings between 24 hours and 1 hour before your flight departure.http://www.qantas.com.au/info/flying/atTheAirport/checkinTimesVIRGIN AUSTRALIAVirgin Australia domestic flights check-in closes 30 minutes prior to scheduled departure time. Virgin Blue online check-in is available and opens 24 hours before departure of your flights. Passengers wanting to utilise this facility must present a print out of your boarding pass at check in.http://www.virginaustralia.com/Personal/Services/Check-inoptions/index.htm</DocumentFooter>\n" +
      "    <DocumentFooter FooterTitle=\"DOMESTIC FARE CONDITIONS (JQ/QF/DJ)\">JETSTAR - http://travelctm.com/uploads/JQFareGrid.pdfQANTAS - http://travelctm.com/uploads/QFDomesticandNZFareGrid.pdfVIRGIN AUSTRALIA - http://travelctm.com/uploads/DJFaregrid.pdfNOTE: Business Class Virgin Australia only offered on A330-200 aircraft (subject to change).</DocumentFooter>\n" +
      "    <DocumentFooter FooterTitle=\"E-TICKET\">E ticket identification required by airlines:Photo identification that has been issued by the Commonwealth of Australia or an Australian State or Territory. Acceptable photo identification includes: drivers license, passport, any state, territory or federal government issued card, company issued identification, or a student card. Qantas acceptable non photo identification includes: credit card used to pay for the ticket, other credit or debit cards, social security card, QF club or frequent flyer card, certified copy/original of a birth of citizenship document. All ID?s must be current and valid.</DocumentFooter>\n" +
      "    <DocumentFooter FooterTitle=\"FREQUENT FLYER\">* Please advise your airline loyalty number if applicable. Once received we will enter this into your booking and your profile for future.* To ensure you receive your frequent flyer points, please retain tickets and boarding passes where possible until your points have appeared on your frequent flyer statement.* Should your points not appear on your statement, please send copies of your boarding passes, tickets and supporting information to the relevant airline.</DocumentFooter>\n" +
      "    <DocumentFooter FooterTitle=\"IATA\">Transportation and other services provided by the carrier are subject to conditions of contract and other important notices which are delivered with thisitinerary/receipt and form part of the nearest office of the issuing airline or travel agent to obtain a copy prior to the commencement of your trip. Notice ifthe passenger's journey involves an ultimate destination or stop in a country other than the country of departure the Warsaw convention may be applicableand the convention governs and in most cases limits the liability of carriers for death or personal injury and in respect of loss or damage to baggage.</DocumentFooter>\n" +
      "    <DocumentFooter FooterTitle=\"ITINERARY ACCEPTANCE\">Whilst all care has been taken to ensure your travel requirements are booked as per your request, CTM requests that you accept responsibility for double checking these details, and advise of any changes that need to be made as soon as possible. Upon receiving your itinerary please check and make sure the relevant booking is correct and as per your travel needs.</DocumentFooter>\n" +
      "  </DocumentFooters>\n" +
      "  <CarbonEmissionInformation/>\n" +
      "</DocumentDetails>\n" +
      "<Booking>\n" +
      "  <BookingDetails>\n" +
      "    <BookingNumber Label=\"Booking ID: \">579235</BookingNumber>\n" +
      "    <PNRDetails>\n" +
      "      <PNRList/>\n" +
      "    </PNRDetails>\n" +
      "    <BookingConsultant1>agent</BookingConsultant1>\n" +
      "    <BookingConsultant1EmailAddress>shusma1@tramada.com</BookingConsultant1EmailAddress>\n" +
      "    <BookingConsultant2/>\n" +
      "    <TravelApprover/>\n" +
      "    <BookedBy Label=\"Booked By: \">Sharyne Andrews</BookedBy>\n" +
      "    <BookingDestinationCity/>\n" +
      "    <BookingDepartureDate Label=\"Departure Date: \">31 Jan 18</BookingDepartureDate>\n" +
      "    <BookingReturnDate Label=\"Return Date: \">05 Feb 18</BookingReturnDate>\n" +
      "    <BookingFinalTKTDate Label=\"Final TKT Date: \">01 Dec 17</BookingFinalTKTDate>\n" +
      "    <BookingCancellationCondition/>\n" +
      "    <BookingDeposits/>\n" +
      "    <BookingDebtors>\n" +
      "      <BookingDebtor>\n" +
      "        <DebtorName Label=\"Debtor: \">KAL</DebtorName>\n" +
      "      </BookingDebtor>\n" +
      "    </BookingDebtors>\n" +
      "    <BookingPassengers>\n" +
      "      <BookingPassenger>\n" +
      "        <PassengerName>KAALZ/TEST MR</PassengerName>\n" +
      "      </BookingPassenger>\n" +
      "      <BookingPassenger>\n" +
      "        <PassengerName>LEONARD/GERARDMR</PassengerName>\n" +
      "        <Memberships>\n" +
      "          <Membership>\n" +
      "            <MembershipNumber>2045223</MembershipNumber>\n" +
      "            <MembershipCompanyName>QANTAS AIRWAYS</MembershipCompanyName>\n" +
      "            <MembershipCompanyCode>QF</MembershipCompanyCode>\n" +
      "            <MembershipProgramName>Qantas</MembershipProgramName>\n" +
      "            <MembershipProgramCode>QF</MembershipProgramCode>\n" +
      "          </Membership>\n" +
      "        </Memberships>\n" +
      "      </BookingPassenger>\n" +
      "      <BookingPassenger>\n" +
      "        <PassengerName>MR MARCO POLO</PassengerName>\n" +
      "      </BookingPassenger>\n" +
      "      <BookingPassenger>\n" +
      "        <PassengerName>MRS MARCH POLO</PassengerName>\n" +
      "      </BookingPassenger>\n" +
      "      <PassengerDetails>\n" +
      "        <PassengerName>MR MARCO POLO</PassengerName>\n" +
      "        <PassengerAge>36</PassengerAge>\n" +
      "      </PassengerDetails>\n" +
      "      <PassengerDetails>\n" +
      "        <PassengerName>MRS MARCH POLO</PassengerName>\n" +
      "        <PassengerAge>34</PassengerAge>\n" +
      "      </PassengerDetails>\n" +
      "      <PassengerDetails>\n" +
      "        <PassengerName>LEONARD/GERARDMR</PassengerName>\n" +
      "        <PassengerAge>ADULT</PassengerAge>\n" +
      "      </PassengerDetails>\n" +
      "      <TicketNumbers>\n" +
      "        <Ticket>\n" +
      "          <PassengerName Label=\"Passenger\">KAALZ/TEST MR</PassengerName>\n" +
      "          <TicketNumber Label=\"Ticket Number\">4556574672</TicketNumber>\n" +
      "          <CarrierCode Label=\"Airline Code\">AA</CarrierCode>\n" +
      "          <ShortItin Label=\"Itinerary Summary\">DFW-SYD-DFW</ShortItin>\n" +
      "          <PassengerType Label=\"Passenger Type\">ADULT</PassengerType>\n" +
      "        </Ticket>\n" +
      "      </TicketNumbers>\n" +
      "      <PassengerNameList Label=\"Itinerary For\">KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList>\n" +
      "    </BookingPassengers>\n" +
      "    <BookingStatus Label=\"Booking Status: \">QUOTE</BookingStatus>\n" +
      "  </BookingDetails>\n" +
      "  <ItineraryItems>\n" +
      "    <ItineraryItem>\n" +
      "      <ItemNo>3906589</ItemNo>\n" +
      "      <SortingOrderNumber>1</SortingOrderNumber>\n" +
      "      <SegmentTypeCode Label=\"CAR\">Car</SegmentTypeCode>\n" +
      "      <PassengerDetails>\n" +
      "        <PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList>\n" +
      "      </PassengerDetails>\n" +
      "      <Supplier>\n" +
      "        <SupplierType>Car Company</SupplierType>\n" +
      "        <SupplierName>V Australia</SupplierName>\n" +
      "        <SupplierAddress>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "        </SupplierAddress>\n" +
      "        <SupplierFax/>\n" +
      "      </Supplier>\n" +
      "      <Start>\n" +
      "        <StartLabelValue>Pick Up</StartLabelValue>\n" +
      "        <ServiceDate>Sat 09 Dec 17</ServiceDate>\n" +
      "        <ServiceTime/>\n" +
      "        <CityCode>MEL</CityCode>\n" +
      "        <CityName>MELBOURNE, AUSTRALIA</CityName>\n" +
      "        <LocationTypeDescription Label=\"Pick Up Location Type\"/>\n" +
      "        <LocationDescription Label=\"Pick Up location\"/>\n" +
      "        <Address>\n" +
      "          <Address1/>\n" +
      "          <Address2/>\n" +
      "          <Address3/>\n" +
      "          <PostCode/>\n" +
      "          <State/>\n" +
      "          <Country/>\n" +
      "        </Address>\n" +
      "        <Contacts>\n" +
      "          <PhoneNo/>\n" +
      "          <FaxNo/>\n" +
      "        </Contacts>\n" +
      "      </Start>\n" +
      "      <End>\n" +
      "        <EndLabelValue>Drop Off</EndLabelValue>\n" +
      "        <ServiceDate>Mon 11 Dec 17</ServiceDate>\n" +
      "        <ServiceTime/>\n" +
      "        <CityCode>MEL APRT</CityCode>\n" +
      "        <CityName>MEL APRT</CityName>\n" +
      "        <LocationTypeDescription Label=\"Drop Off Location Type\"/>\n" +
      "        <LocationDescription Label=\"Drop Off Location\"/>\n" +
      "        <Address>\n" +
      "          <Address1/>\n" +
      "          <Address2/>\n" +
      "          <Address3/>\n" +
      "          <PostCode/>\n" +
      "          <State/>\n" +
      "          <Country/>\n" +
      "        </Address>\n" +
      "        <Contacts>\n" +
      "          <PhoneNo/>\n" +
      "          <FaxNo/>\n" +
      "        </Contacts>\n" +
      "      </End>\n" +
      "      <Status>Confirmed</Status>\n" +
      "      <IssueDate>\n" +
      "      </IssueDate>\n" +
      "      <OrderDate/>\n" +
      "      <AccountDetails>\n" +
      "        <PaymentTypeDescription>Prepaid</PaymentTypeDescription>\n" +
      "        <PaymentNarrative>Pre-paid remark test cccf</PaymentNarrative>\n" +
      "        <SupplierCharges/>\n" +
      "        <SupplierFees/>\n" +
      "        <Inclusions/>\n" +
      "        <Exclusions/>\n" +
      "        <RateGuaranteeDescription/>\n" +
      "        <CancelConditions/>\n" +
      "        <CancellationDescription>Cancel 10 Hours Prior to Arrival</CancellationDescription>\n" +
      "      </AccountDetails>\n" +
      "      <ItineraryNotes>\n" +
      "        <SegmentNotes/>\n" +
      "        <FareRuleNotes/>\n" +
      "      </ItineraryNotes>\n" +
      "      <NoOfItems Label=\"No. of Cars\">1</NoOfItems>\n" +
      "      <NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">2</NoOfdays>\n" +
      "      <RateInformation>\n" +
      "        <AUDRate Label=\"Rate Incl GST\">AUD200.00</AUDRate>\n" +
      "        <LocalRate Label=\"Local Rate Incl GST\">AUD200.00</LocalRate>\n" +
      "      </RateInformation>\n" +
      "    </ItineraryItem>\n" +
      "    <ItineraryItem>\n" +
      "      <ItemNo>3906588</ItemNo>\n" +
      "      <SortingOrderNumber>2</SortingOrderNumber>\n" +
      "      <SegmentTypeCode Label=\"FLIGHT\">Flight</SegmentTypeCode>\n" +
      "      <PassengerDetails>\n" +
      "        <Passenger>\n" +
      "          <PassengerName>KAALZ/TEST MR</PassengerName>\n" +
      "          <FlightSpecific/>\n" +
      "        </Passenger>\n" +
      "        <Passenger>\n" +
      "          <PassengerName>LEONARD/GERARDMR</PassengerName>\n" +
      "          <FlightSpecific/>\n" +
      "        </Passenger>\n" +
      "        <Passenger>\n" +
      "          <PassengerName>MR MARCO POLO</PassengerName>\n" +
      "          <FlightSpecific/>\n" +
      "        </Passenger>\n" +
      "        <Passenger>\n" +
      "          <PassengerName>MRS MARCH POLO</PassengerName>\n" +
      "          <FlightSpecific/>\n" +
      "        </Passenger>\n" +
      "        <PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList>\n" +
      "      </PassengerDetails>\n" +
      "      <Supplier>\n" +
      "        <SupplierType>Flight Company</SupplierType>\n" +
      "        <SupplierName>AMERICAN AIRLINES</SupplierName>\n" +
      "        <SupplierAddress>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "        </SupplierAddress>\n" +
      "        <SupplierFax/>\n" +
      "      </Supplier>\n" +
      "      <ServiceIdentifier/>\n" +
      "      <Start>\n" +
      "        <StartLabelValue>Departure</StartLabelValue>\n" +
      "        <ServiceDate>Fri 19 Jan 18</ServiceDate>\n" +
      "        <ServiceTime>14:20</ServiceTime>\n" +
      "        <CityCode>DFW</CityCode>\n" +
      "        <CityName>DALLAS FT WORTH, TX</CityName>\n" +
      "        <LocationDescription Label=\"Departure Terminal\"/>\n" +
      "        <Address>\n" +
      "          <Address1/>\n" +
      "          <Address2/>\n" +
      "          <Address3/>\n" +
      "          <PostCode/>\n" +
      "          <State/>\n" +
      "          <Country/>\n" +
      "        </Address>\n" +
      "        <Contacts>\n" +
      "          <PhoneNo/>\n" +
      "          <FaxNo/>\n" +
      "        </Contacts>\n" +
      "      </Start>\n" +
      "      <End>\n" +
      "        <EndLabelValue>Arrival</EndLabelValue>\n" +
      "        <ServiceDate>Sat 20 Jan 18</ServiceDate>\n" +
      "        <ServiceTime>09:25</ServiceTime>\n" +
      "        <CityCode>SYD</CityCode>\n" +
      "        <CityName>Kingsford Smith, Sydney</CityName>\n" +
      "        <LocationDescription Label=\"Arrival Terminal\"/>\n" +
      "        <Address>\n" +
      "          <Address1/>\n" +
      "          <Address2/>\n" +
      "          <Address3/>\n" +
      "          <PostCode/>\n" +
      "          <State/>\n" +
      "          <Country/>\n" +
      "        </Address>\n" +
      "        <Contacts>\n" +
      "          <PhoneNo/>\n" +
      "          <FaxNo/>\n" +
      "        </Contacts>\n" +
      "      </End>\n" +
      "      <FlightSpecificDetails>\n" +
      "        <FlightSpecificMealsSeats>KAALZ/TEST MR|LEONARD/GERARDMR|MR MARCO POLO|MRS MARCH POLO</FlightSpecificMealsSeats>\n" +
      "        <Legs>\n" +
      "          <LegsSummary>DALLAS FT WORTH, TX (TERMINAL - ) Kingsford Smith, Sydney (TERMINAL - ), Dept Time 19-01-2018 14:20, Arrival Time 20-01-2018 09:25</LegsSummary>\n" +
      "          <Leg>\n" +
      "            <StopOverLeg Label=\"Journey Info\">0</StopOverLeg>\n" +
      "            <DepartureCity Label=\"Departure City\">DALLAS FT WORTH, TX</DepartureCity>\n" +
      "            <DepartureDate Label=\"Departure Date\">19-01-2018</DepartureDate>\n" +
      "            <DepartureTime Label=\"Departure Time\">14:20</DepartureTime>\n" +
      "            <ArrivalCity Label=\"Arrival City\">Kingsford Smith, Sydney</ArrivalCity>\n" +
      "            <ArrivalDate Label=\"Arrival Date\">20-01-2018</ArrivalDate>\n" +
      "            <ArrivalTime Label=\"Arrival Time\">09:25</ArrivalTime>\n" +
      "          </Leg>\n" +
      "        </Legs>\n" +
      "      </FlightSpecificDetails>\n" +
      "      <Status>Confirmed</Status>\n" +
      "      <IssueDate>\n" +
      "      </IssueDate>\n" +
      "      <OrderDate/>\n" +
      "      <AccountDetails>\n" +
      "        <PaymentTypeDescription/>\n" +
      "        <PaymentNarrative/>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription/>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "          <FareRuleNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <RateInformation/>\n" +
      "      </ItineraryItem>\n" +
      "      <ItineraryItem>\n" +
      "        <ItemNo>3906590</ItemNo>\n" +
      "        <SortingOrderNumber>3</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"HOTEL\">Hotel</SegmentTypeCode>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList>\n" +
      "        </PassengerDetails>\n" +
      "        <Supplier>\n" +
      "          <SupplierType>Hotel Company</SupplierType>\n" +
      "          <SupplierCode Label=\"Supplier Code\">MCSYD7468-1</SupplierCode>\n" +
      "          <SupplierName>MARRIOTT SYDNEY HARBOUR</SupplierName>\n" +
      "          <SupplierAddress>\n" +
      "            <Address>\n" +
      "              <Address1>30 PITT STREET</Address1>\n" +
      "              <Address2>SYDNEY AU 2000</Address2>\n" +
      "              <Address3/>\n" +
      "              <PostCode/>\n" +
      "              <State/>\n" +
      "              <Country>Australia</Country>\n" +
      "            </Address>\n" +
      "          </SupplierAddress>\n" +
      "          <SupplierPhone Label=\"Phone\">P-612-9259-7000</SupplierPhone>\n" +
      "          <SupplierFax>F-612-9251-1122</SupplierFax>\n" +
      "        </Supplier>\n" +
      "        <Start>\n" +
      "          <StartLabelValue>Check In</StartLabelValue>\n" +
      "          <ServiceDate>Sat 20 Jan 18</ServiceDate>\n" +
      "          <ServiceTime>06:00</ServiceTime>\n" +
      "          <CityCode>SYD</CityCode>\n" +
      "          <CityName>Kingsford Smith, Sydney</CityName>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </Start>\n" +
      "        <End>\n" +
      "          <EndLabelValue>Check Out</EndLabelValue>\n" +
      "          <ServiceDate>Tue 23 Jan 18</ServiceDate>\n" +
      "          <ServiceTime>02:00</ServiceTime>\n" +
      "          <CityCode/>\n" +
      "          <CityName/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </End>\n" +
      "        <Status>Confirmed</Status>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <OrderDate/>\n" +
      "        <ConfirmationNo Label=\"Confirmation No\">34353424</ConfirmationNo>\n" +
      "        <AccountDetails>\n" +
      "          <PaymentTypeDescription>Prepaid</PaymentTypeDescription>\n" +
      "          <PaymentNarrative>Pre-paid remark test cccf</PaymentNarrative>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription/>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "          <FareRuleNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <NoOfItems Label=\"No. of Rooms\">1</NoOfItems>\n" +
      "        <NoOfdays UnitOfCount=\"Nights\" Label=\"Duration\">3</NoOfdays>\n" +
      "        <RateInformation>\n" +
      "          <AUDRate Label=\"Rate Incl GST\">AUD260.00</AUDRate>\n" +
      "          <LocalRate Label=\"Local Rate Incl GST\">AUD260.00</LocalRate>\n" +
      "        </RateInformation>\n" +
      "      </ItineraryItem>\n" +
      "      <ItineraryItem>\n" +
      "        <ItemNo>3906587</ItemNo>\n" +
      "        <SortingOrderNumber>4</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"FLIGHT\">Flight</SegmentTypeCode>\n" +
      "        <PassengerDetails>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>KAALZ/TEST MR</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>LEONARD/GERARDMR</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>MR MARCO POLO</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>MRS MARCH POLO</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList>\n" +
      "        </PassengerDetails>\n" +
      "        <Supplier>\n" +
      "          <SupplierType>Flight Company</SupplierType>\n" +
      "          <SupplierName>AMERICAN AIRLINES</SupplierName>\n" +
      "          <SupplierAddress>\n" +
      "            <Address>\n" +
      "              <Address1/>\n" +
      "              <Address2/>\n" +
      "              <Address3/>\n" +
      "              <PostCode/>\n" +
      "              <State/>\n" +
      "              <Country/>\n" +
      "            </Address>\n" +
      "          </SupplierAddress>\n" +
      "          <SupplierFax/>\n" +
      "        </Supplier>\n" +
      "        <ServiceIdentifier/>\n" +
      "        <Start>\n" +
      "          <StartLabelValue>Departure</StartLabelValue>\n" +
      "          <ServiceDate>Tue 23 Jan 18</ServiceDate>\n" +
      "          <ServiceTime>14:20</ServiceTime>\n" +
      "          <CityCode>SYD</CityCode>\n" +
      "          <CityName>Kingsford Smith, Sydney</CityName>\n" +
      "          <LocationDescription Label=\"Departure Terminal\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </Start>\n" +
      "        <End>\n" +
      "          <EndLabelValue>Arrival</EndLabelValue>\n" +
      "          <ServiceDate>Tue 23 Jan 18</ServiceDate>\n" +
      "          <ServiceTime>09:25</ServiceTime>\n" +
      "          <CityCode>DFW</CityCode>\n" +
      "          <CityName>DALLAS FT WORTH, TX</CityName>\n" +
      "          <LocationDescription Label=\"Arrival Terminal\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </End>\n" +
      "        <FlightSpecificDetails>\n" +
      "          <FlightSpecificMealsSeats>KAALZ/TEST MR|LEONARD/GERARDMR|MR MARCO POLO|MRS MARCH POLO</FlightSpecificMealsSeats>\n" +
      "          <Legs>\n" +
      "            <LegsSummary>Kingsford Smith, Sydney (TERMINAL - ) DALLAS FT WORTH, TX (TERMINAL - ), Dept Time 23-01-2018 14:20, Arrival Time 20-01-2018 09:25</LegsSummary>\n" +
      "            <Leg>\n" +
      "              <StopOverLeg Label=\"Journey Info\">0</StopOverLeg>\n" +
      "              <DepartureCity Label=\"Departure City\">Kingsford Smith, Sydney</DepartureCity>\n" +
      "              <DepartureDate Label=\"Departure Date\">23-01-2018</DepartureDate>\n" +
      "              <DepartureTime Label=\"Departure Time\">14:20</DepartureTime>\n" +
      "              <ArrivalCity Label=\"Arrival City\">DALLAS FT WORTH, TX</ArrivalCity>\n" +
      "              <ArrivalDate Label=\"Arrival Date\">20-01-2018</ArrivalDate>\n" +
      "              <ArrivalTime Label=\"Arrival Time\">09:25</ArrivalTime>\n" +
      "            </Leg>\n" +
      "          </Legs>\n" +
      "        </FlightSpecificDetails>\n" +
      "        <Status>Confirmed</Status>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <OrderDate/>\n" +
      "        <AccountDetails>\n" +
      "          <PaymentTypeDescription/>\n" +
      "          <PaymentNarrative/>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription/>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "          <FareRuleNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <RateInformation/>\n" +
      "      </ItineraryItem>\n" +
      "    </ItineraryItems>\n" +
      "    <CostingsItems>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>2218753</ItemNo>\n" +
      "        <SortingOrderNumber>1</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"HOTEL\">Hotel</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>MARRIOTT SYDNEY HARBOUR</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <ConfirmationNo Label=\"Confirmation No\">34353424</ConfirmationNo>\n" +
      "        <StartServiceDate Label=\"Start Service Date\">20 Jan 18</StartServiceDate>\n" +
      "        <StartCity>Kingsford Smith, Sydney</StartCity>\n" +
      "        <StartCityName Label=\"Address\"/>\n" +
      "        <EndServiceDate Label=\"End Service Date\">23 Jan 18</EndServiceDate>\n" +
      "        <EndCity/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <CurrencyCode>AUD</CurrencyCode>\n" +
      "          <BaseFare>709.09</BaseFare>\n" +
      "          <Discount Label=\"Discount ex GST\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">70.91</GST>\n" +
      "          <ClientDue>780.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>2218754</ItemNo>\n" +
      "        <SortingOrderNumber>2</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"TICKET\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>BSP-IATA</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode>AA</AirlineCode>\n" +
      "          <TKTPassenger>KAALZ/TEST MR</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode>Y</ClassCode>\n" +
      "          <ClassDescription>Economy</ClassDescription>\n" +
      "        </TKTSpecific>\n" +
      "        <ShortItinerary Label=\"Short Itinerary\">DALLAS FT WORTH-Kingsford Smith-DALLAS FT WORTH</ShortItinerary>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>02 Jan 18</IssueDate>\n" +
      "        <ConfirmationNo Label=\"Ticket No\">4556574672</ConfirmationNo>\n" +
      "        <StartServiceDate Label=\"Start Service Date\">19 Jan 18</StartServiceDate>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <CurrencyCode/>\n" +
      "          <BaseFare>1400.00</BaseFare>\n" +
      "          <Discount Label=\"Discount ex GST\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">0.00</GST>\n" +
      "          <ClientDue>1400.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "        <CancellationDescription Label=\"Cancellation\">24 hours cancellation notice required</CancellationDescription>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>2218752</ItemNo>\n" +
      "        <SortingOrderNumber>3</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"CAR\">Car</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>V Australia</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <StartServiceDate Label=\"Start Service Date\">09 Dec 17</StartServiceDate>\n" +
      "        <StartCity>MELBOURNE, AUSTRALIA</StartCity>\n" +
      "        <StartCityName Label=\"Pick-Up City\"/>\n" +
      "        <EndServiceDate Label=\"End Service Date\">11 Dec 17</EndServiceDate>\n" +
      "        <EndCity>MEL APRT</EndCity>\n" +
      "        <EndCityName Label=\"Drop-off City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <CurrencyCode>AUD</CurrencyCode>\n" +
      "          <BaseFare>363.64</BaseFare>\n" +
      "          <Discount Label=\"Discount ex GST\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"GST\">36.36</GST>\n" +
      "          <ClientDue>400.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "        <CancellationDescription Label=\"Cancellation\">Cancel 10 Hours Prior to Arrival</CancellationDescription>\n" +
      "      </CostingItem>\n" +
      "      <DocumentTotals>\n" +
      "        <TotalPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>0.00</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST>\n" +
      "            <GST>0.00</GST>\n" +
      "            <ClientDue>0.00</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalPayDirect>\n" +
      "        <TotalExcPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>2472.73</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST>\n" +
      "            <GST>107.27</GST>\n" +
      "            <ClientDue>2580.00</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalExcPayDirect>\n" +
      "        <TotalIncPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>2472.73</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST>\n" +
      "            <GST>107.27</GST>\n" +
      "            <ClientDue>2580.00</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalIncPayDirect>\n" +
      "        <TotalPaid>0.00</TotalPaid>\n" +
      "        <TotalOutstandingExcPayDirect>2580.00</TotalOutstandingExcPayDirect>\n" +
      "        <Currency>AUD</Currency>\n" +
      "      </DocumentTotals>\n" +
      "    </CostingsItems>\n" +
      "  </Booking>\n" +
      "</DocumentInformation>";

  String xml12 = "<DocumentInformation>\n" +
      "  <Agency>\n" +
      "    <AgencyName Label=\"TMC\">Demo Travel Services</AgencyName>\n" +
      "    <AgencyCode Label=\"TMC Code\">trunk_tramada</AgencyCode>\n" +
      "    <AgencyAddress>\n" +
      "      <Address>\n" +
      "        <Address1 Label=\"Address Line 1\">7460 Warren Parkway Suite 100</Address1>\n" +
      "        <Address3 Label=\" Address Line 3\">Frisco</Address3>\n" +
      "        <PostCode Label=\" Postcode\">75034</PostCode>\n" +
      "        <State Label=\"State\">Tx</State>\n" +
      "        <Country Label=\"Country\">United States</Country>\n" +
      "      </Address>\n" +
      "    </AgencyAddress>\n" +
      "    <AgencyContacts>\n" +
      "      <AgencyEmail Label=\"Email: \">accountmanagement@tramada.com</AgencyEmail>\n" +
      "      <AgencyPhone Label=\"Phone: \">214.783.8161</AgencyPhone>\n" +
      "      <AgencyFax Label=\"Fax No.: \">\n" +
      "      </AgencyFax>\n" +
      "    </AgencyContacts>\n" +
      "  </Agency>\n" +
      "  <DocumentDetails>\n" +
      "    <DocumentPrintDate>Tuesday January 02 2018 09:54</DocumentPrintDate>\n" +
      "    <DocumentPrintTime>Central Time</DocumentPrintTime>\n" +
      "    <DocumentTitle Label=\"Type\">Itinerary &amp; Costing - Full</DocumentTitle>\n" +
      "    <DocumentAddress>\n" +
      "      <Address/>\n" +
      "    </DocumentAddress>\n" +
      "    <DocumentContactDetails>\n" +
      "      <Phone Label=\"Phone: \">214.783.8161</Phone>\n" +
      "    </DocumentContactDetails>\n" +
      "    <DocumentCommercialDetails>\n" +
      "      <LicNo Label=\"Licence No.: \">3XX474556</LicNo>\n" +
      "      <ABN Label=\"ABN: \">11 222 333 444</ABN>\n" +
      "      <ACN Label=\"ACN: \">999 888 777</ACN>\n" +
      "    </DocumentCommercialDetails>\n" +
      "    <DocumentHeaders>\n" +
      "      <DocumentHeader HeaderTitle=\"After Hours Assistance\">Should you require assistance with booking changes or amendments relating to imminent departures, please contact our emergency afterhours service on +1.281.xxx.xxxx.</DocumentHeader>\n" +
      "    </DocumentHeaders>\n" +
      "    <DocumentFooters>\n" +
      "      <DocumentFooter FooterTitle=\"Baggage Allowance\">Cabin baggage: Up to 15.4lb complimentaryChecked-in baggage: Please refer to the airline website for checked-in baggage allowance.</DocumentFooter>\n" +
      "    </DocumentFooters>\n" +
      "    <CarbonEmissionInformation/>\n" +
      "  </DocumentDetails>\n" +
      "  <Booking>\n" +
      "    <BookingDetails>\n" +
      "      <BookingNumber Label=\"Booking ID: \">B34911</BookingNumber>\n" +
      "      <PNRDetails>\n" +
      "        <PNRList/>\n" +
      "      </PNRDetails>\n" +
      "      <BookingConsultant1>agent</BookingConsultant1>\n" +
      "      <BookingConsultant1EmailAddress>tejas@tramada.com</BookingConsultant1EmailAddress>\n" +
      "      <BookingConsultant2/>\n" +
      "      <TravelApprover/>\n" +
      "      <BookingDepartureDate Label=\"Departure Date: \">May 15 18</BookingDepartureDate>\n" +
      "      <BookingReturnDate Label=\"Return Date: \">May 20 18</BookingReturnDate>\n" +
      "      <BookingFinalTKTDate Label=\"Final TKT Date: \">Dec 18 17</BookingFinalTKTDate>\n" +
      "      <BookingCancellationCondition/>\n" +
      "      <BookingDeposits/>\n" +
      "      <BookingDebtors>\n" +
      "        <BookingDebtor>\n" +
      "          <DebtorName Label=\"Debtor: \">Howdy Holidays &amp; Cruise</DebtorName>\n" +
      "        </BookingDebtor>\n" +
      "      </BookingDebtors>\n" +
      "      <BookingPassengers>\n" +
      "        <BookingPassenger>\n" +
      "          <PassengerName>BRANDIS/TOM</PassengerName>\n" +
      "        </BookingPassenger>\n" +
      "        <TicketNumbers/>\n" +
      "        <PassengerNameList Label=\"Itinerary For\">BRANDIS/TOM</PassengerNameList>\n" +
      "      </BookingPassengers>\n" +
      "    </BookingDetails>\n" +
      "    <ItineraryItems>\n" +
      "      <ItineraryItem>\n" +
      "        <ItemNo>310004</ItemNo>\n" +
      "        <SortingOrderNumber>1</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"FLIGHT\">Flight</SegmentTypeCode>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerNameList>BRANDIS/TOM</PassengerNameList>\n" +
      "        </PassengerDetails>\n" +
      "        <Supplier>\n" +
      "          <SupplierType>Flight Company</SupplierType>\n" +
      "          <SupplierName>AMERICAN AIRLINES</SupplierName>\n" +
      "          <SupplierAddress>\n" +
      "            <Address>\n" +
      "              <Address1/>\n" +
      "              <Address2/>\n" +
      "              <Address3/>\n" +
      "              <PostCode/>\n" +
      "              <State/>\n" +
      "              <Country/>\n" +
      "            </Address>\n" +
      "          </SupplierAddress>\n" +
      "          <SupplierFax/>\n" +
      "        </Supplier>\n" +
      "        <ServiceTypeDescription Label=\"Aircraft Type\">McDonnell Douglas MD-80, 132-172 SEATS</ServiceTypeDescription>\n" +
      "        <ServiceIdentifier>AA2507</ServiceIdentifier>\n" +
      "        <ServiceClassDescription Label=\"Class\">Y - Economy Class</ServiceClassDescription>\n" +
      "        <Start>\n" +
      "          <StartLabelValue>Departure</StartLabelValue>\n" +
      "          <ServiceDate>Tue May 15 18</ServiceDate>\n" +
      "          <ServiceTime>8:15 AM</ServiceTime>\n" +
      "          <CityCode>IAH</CityCode>\n" +
      "          <CityName>HOUSTON INTL IAH, TX</CityName>\n" +
      "          <LocationDescription>TERMINAL A</LocationDescription>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </Start>\n" +
      "        <End>\n" +
      "          <EndLabelValue>Arrival</EndLabelValue>\n" +
      "          <ServiceDate>Tue May 15 18</ServiceDate>\n" +
      "          <ServiceTime>9:25 AM</ServiceTime>\n" +
      "          <CityCode>DFW</CityCode>\n" +
      "          <CityName>DALLAS FT WORTH, TX</CityName>\n" +
      "          <LocationDescription>\n" +
      "          </LocationDescription>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </End>\n" +
      "        <FlightSpecificDetails>\n" +
      "          <FareBasis Label=\"Fare Basis\">YA2AAN</FareBasis>\n" +
      "          <AirlineReloc Label=\"Airline Reloc\">YXJNUH</AirlineReloc>\n" +
      "          <FlightSpecificMealsSeats>BRANDIS/TOM</FlightSpecificMealsSeats>\n" +
      "          <Legs>\n" +
      "            <LegsSummary>HOUSTON INTL IAH, TX (TERMINAL - TERMINAL A) DALLAS FT WORTH, TX (TERMINAL - ), Dept Time 05-15-2018 08:15, Arrival Time 05-15-2018 09:25 - Travelling time: 1 hr 10 mins</LegsSummary>\n" +
      "            <Leg>\n" +
      "              <StopOverLeg Label=\"Journey Info\">0</StopOverLeg>\n" +
      "              <DepartureCity Label=\"Departure City\">HOUSTON INTL IAH, TX</DepartureCity>\n" +
      "              <DepartureTerminal Label=\"Departure Terminal\">TERMINAL A</DepartureTerminal>\n" +
      "              <DepartureDate Label=\"Departure Date\">05-15-2018</DepartureDate>\n" +
      "              <DepartureTime Label=\"Departure Time\">08:15</DepartureTime>\n" +
      "              <ArrivalCity Label=\"Arrival City\">DALLAS FT WORTH, TX</ArrivalCity>\n" +
      "              <ArrivalDate Label=\"Arrival Date\">05-15-2018</ArrivalDate>\n" +
      "              <ArrivalTime Label=\"Arrival Time\">09:25</ArrivalTime>\n" +
      "              <FlightTime Label=\"Flight Time\">01.10</FlightTime>\n" +
      "              <Mileage Label=\"Mileage\">233</Mileage>\n" +
      "            </Leg>\n" +
      "          </Legs>\n" +
      "        </FlightSpecificDetails>\n" +
      "        <Status>Confirmed</Status>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <OrderDate/>\n" +
      "        <AccountDetails>\n" +
      "          <PaymentNarrative/>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription/>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <RateInformation/>\n" +
      "      </ItineraryItem>\n" +
      "      <ItineraryItem>\n" +
      "        <ItemNo>310003</ItemNo>\n" +
      "        <SortingOrderNumber>2</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"FLIGHT\">Flight</SegmentTypeCode>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerNameList>BRANDIS/TOM</PassengerNameList>\n" +
      "        </PassengerDetails>\n" +
      "        <Supplier>\n" +
      "          <SupplierType>Flight Company</SupplierType>\n" +
      "          <SupplierName>AMERICAN AIRLINES</SupplierName>\n" +
      "          <SupplierAddress>\n" +
      "            <Address>\n" +
      "              <Address1/>\n" +
      "              <Address2/>\n" +
      "              <Address3/>\n" +
      "              <PostCode/>\n" +
      "              <State/>\n" +
      "              <Country/>\n" +
      "            </Address>\n" +
      "          </SupplierAddress>\n" +
      "          <SupplierFax/>\n" +
      "        </Supplier>\n" +
      "        <ServiceTypeDescription Label=\"Aircraft Type\">McDonnell Douglas MD-80, 132-172 SEATS</ServiceTypeDescription>\n" +
      "        <ServiceIdentifier>AA0985</ServiceIdentifier>\n" +
      "        <ServiceClassDescription Label=\"Class\">Y - Economy Class</ServiceClassDescription>\n" +
      "        <Start>\n" +
      "          <StartLabelValue>Departure</StartLabelValue>\n" +
      "          <ServiceDate>Sun May 20 18</ServiceDate>\n" +
      "          <ServiceTime>9:05 AM</ServiceTime>\n" +
      "          <CityCode>DFW</CityCode>\n" +
      "          <CityName>DALLAS FT WORTH, TX</CityName>\n" +
      "          <LocationDescription>\n" +
      "          </LocationDescription>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </Start>\n" +
      "        <End>\n" +
      "          <EndLabelValue>Arrival</EndLabelValue>\n" +
      "          <ServiceDate>Sun May 20 18</ServiceDate>\n" +
      "          <ServiceTime>10:20 AM</ServiceTime>\n" +
      "          <CityCode>IAH</CityCode>\n" +
      "          <CityName>HOUSTON INTL IAH, TX</CityName>\n" +
      "          <LocationDescription>TERMINAL A</LocationDescription>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </End>\n" +
      "        <FlightSpecificDetails>\n" +
      "          <FareBasis Label=\"Fare Basis\">YA2AAN</FareBasis>\n" +
      "          <AirlineReloc Label=\"Airline Reloc\">YXJNUH</AirlineReloc>\n" +
      "          <FlightSpecificMealsSeats>BRANDIS/TOM</FlightSpecificMealsSeats>\n" +
      "          <Legs>\n" +
      "            <LegsSummary>DALLAS FT WORTH, TX (TERMINAL - ) HOUSTON INTL IAH, TX (TERMINAL - TERMINAL A), Dept Time 05-20-2018 09:05, Arrival Time 05-20-2018 10:20 - Travelling time: 1 hr 15 mins</LegsSummary>\n" +
      "            <Leg>\n" +
      "              <StopOverLeg Label=\"Journey Info\">0</StopOverLeg>\n" +
      "              <DepartureCity Label=\"Departure City\">DALLAS FT WORTH, TX</DepartureCity>\n" +
      "              <DepartureDate Label=\"Departure Date\">05-20-2018</DepartureDate>\n" +
      "              <DepartureTime Label=\"Departure Time\">09:05</DepartureTime>\n" +
      "              <ArrivalCity Label=\"Arrival City\">HOUSTON INTL IAH, TX</ArrivalCity>\n" +
      "              <ArrivalTerminal Label=\"Arrival Terminal\">TERMINAL A</ArrivalTerminal>\n" +
      "              <ArrivalDate Label=\"Arrival Date\">05-20-2018</ArrivalDate>\n" +
      "              <ArrivalTime Label=\"Arrival Time\">10:20</ArrivalTime>\n" +
      "              <FlightTime Label=\"Flight Time\">01.15</FlightTime>\n" +
      "              <Mileage Label=\"Mileage\">233</Mileage>\n" +
      "            </Leg>\n" +
      "          </Legs>\n" +
      "        </FlightSpecificDetails>\n" +
      "        <Status>Confirmed</Status>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <OrderDate/>\n" +
      "        <AccountDetails>\n" +
      "          <PaymentNarrative/>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription/>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <RateInformation/>\n" +
      "      </ItineraryItem>\n" +
      "    </ItineraryItems>\n" +
      "    <CostingsItems>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>131761</ItemNo>\n" +
      "        <SortingOrderNumber>1</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"SERVICE_FEE\">Service Fee</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>DEMO SYSTEM TRAVEL</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <CostingDescriptor>Domestic Booking Fee</CostingDescriptor>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>40.00</BaseFare>\n" +
      "          <Discount Label=\"Discount ex GST\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>0.00</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"Sales Tax\">0.00</GST>\n" +
      "          <ClientDue>40.00</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <CostingItem>\n" +
      "        <ItemNo>131760</ItemNo>\n" +
      "        <SortingOrderNumber>2</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"TICKET\">Ticket</SegmentTypeCode>\n" +
      "        <Creditor>\n" +
      "          <CreditorName>Arc</CreditorName>\n" +
      "        </Creditor>\n" +
      "        <TKTSpecific>\n" +
      "          <AirlineCode>AA</AirlineCode>\n" +
      "          <TKTPassenger>BRANDIS/TOM</TKTPassenger>\n" +
      "          <PassengerType>ADULT</PassengerType>\n" +
      "          <ClassCode>Y</ClassCode>\n" +
      "          <ClassDescription>Economy Class</ClassDescription>\n" +
      "        </TKTSpecific>\n" +
      "        <ShortItinerary Label=\"Short Itinerary\">HOUSTON INTL IAH-DALLAS FT WORTH-HOUSTON INTL IAH</ShortItinerary>\n" +
      "        <CostingDescriptor/>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <StartCity/>\n" +
      "        <StartCityName Label=\"Start City\"/>\n" +
      "        <EndCity/>\n" +
      "        <EndCityName Label=\"Finish City\"/>\n" +
      "        <CostingNotes/>\n" +
      "        <CostingFinancials>\n" +
      "          <BaseFare>1425.12</BaseFare>\n" +
      "          <Discount Label=\"Discount ex GST\">0.00</Discount>\n" +
      "          <TaxesAndFeesExGST>135.28</TaxesAndFeesExGST>\n" +
      "          <GST Label=\"Sales Tax\">0.00</GST>\n" +
      "          <ClientDue>1560.40</ClientDue>\n" +
      "          <PaymentType>Prepaid</PaymentType>\n" +
      "        </CostingFinancials>\n" +
      "      </CostingItem>\n" +
      "      <DocumentTotals>\n" +
      "        <TotalPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>0.00</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST>\n" +
      "            <GST>0.00</GST>\n" +
      "            <ClientDue>0.00</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalPayDirect>\n" +
      "        <TotalExcPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>1465.12</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>135.28</TaxesAndFeesExcGST>\n" +
      "            <GST>0.00</GST>\n" +
      "            <ClientDue>1600.40</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalExcPayDirect>\n" +
      "        <TotalIncPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>1465.12</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>135.28</TaxesAndFeesExcGST>\n" +
      "            <GST>0.00</GST>\n" +
      "            <ClientDue>1600.40</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalIncPayDirect>\n" +
      "        <TotalPaid>0.00</TotalPaid>\n" +
      "        <TotalOutstandingExcPayDirect>1600.40</TotalOutstandingExcPayDirect>\n" +
      "        <Currency>USD</Currency>\n" +
      "      </DocumentTotals>\n" +
      "    </CostingsItems>\n" +
      "  </Booking>\n" +
      "</DocumentInformation>\n";

  String xml13 = "<DocumentInformation>\n" +
      "  <Agency>\n" +
      "    <AgencyName Label=\"TMC\">Tests Travel Management</AgencyName>\n" +
      "    <AgencyCode Label=\"TMC Code\">trunk_tramada</AgencyCode>\n" +
      "    <AgencyAddress>\n" +
      "      <Address>\n" +
      "        <Address1 Label=\"Address Line 1\">trunkctm</Address1>\n" +
      "        <Address2 Label=\" Address Line 2\">Address 2</Address2>\n" +
      "        <Address3 Label=\" Address Line 3\">Address 3</Address3>\n" +
      "        <PostCode Label=\" Postcode\">2000</PostCode>\n" +
      "        <State Label=\"State\">NSW</State>\n" +
      "        <Country Label=\"Country\">Australia</Country>\n" +
      "      </Address>\n" +
      "    </AgencyAddress>\n" +
      "    <AgencyContacts>\n" +
      "      <AgencyEmail Label=\"Email: \">ashok@tramada.com</AgencyEmail>\n" +
      "      <AgencyPhone Label=\"Phone: \">0470632287</AgencyPhone>\n" +
      "      <AgencyFax Label=\"Fax No.: \">0470632287</AgencyFax>\n" +
      "    </AgencyContacts>\n" +
      "  </Agency>\n" +
      "  <DocumentDetails>\n" +
      "    <DocumentPrintDate>Wednesday 03 January 2018 03:05</DocumentPrintDate>\n" +
      "    <DocumentPrintTime>Sydney, NSW</DocumentPrintTime>\n" +
      "    <DocumentTitle Label=\"Type\">Itinerary - Full</DocumentTitle>\n" +
      "    <DocumentAddress>\n" +
      "      <Address/>\n" +
      "    </DocumentAddress>\n" +
      "    <DocumentContactDetails>\n" +
      "      <EmergencyContact Label=\"Emergency Contact: \">1800 663 622</EmergencyContact>\n" +
      "      <AfterHours Label=\"After Hours Contact: \">1800 663 622</AfterHours>\n" +
      "      <TollFree Label=\"Toll Free Number: \">1800 663 622</TollFree>\n" +
      "      <Phone Label=\"Phone: \">0470632287</Phone>\n" +
      "      <Fax Label=\"Fax: \">0470632287</Fax>\n" +
      "    </DocumentContactDetails>\n" +
      "    <DocumentCommercialDetails>\n" +
      "      <LicNo Label=\"Licence No.: \">854</LicNo>\n" +
      "      <ABN Label=\"ABN: \">52 005 000 895</ABN>\n" +
      "      <ACN Label=\"ACN: \">005 000 895</ACN>\n" +
      "    </DocumentCommercialDetails>\n" +
      "    <DocumentHeaders>\n" +
      "      <DocumentHeader HeaderTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually There®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentHeader>\n" +
      "      <DocumentHeader HeaderTitle=\"Third party hotel booking\">Please note: your hotel has been booked via a third party provider. They have provided a confirmation number and this may be required at check-in. In most instances you will also have been provided with a confirmation and payment voucher separately to the itinerary. Please ensure you have printed a copy of this voucher and are able to produce it upon request. There can be occasional communication problems between third party bookers and hotels. If you experience any difficulties at check-in please call CTM on the numbers below.</DocumentHeader>\n" +
      "      <DocumentHeader HeaderTitle=\"label\">Testing Helloworld Header</DocumentHeader>\n" +
      "    </DocumentHeaders>\n" +
      "    <DocumentFooters>\n" +
      "      <DocumentFooter FooterTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually There®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"Assistance while overseas\">Subsea 7 provide assistance in emergencies arising when personnel are travelling overseas. Comprehensive emergency assistance is available to Employees through International SOS, these cover Medical Services and Security.The following information is required for International SOS:SOS Programme Number: 14ACPA000020Employer: Subsea 724h phone number: +61 2 9372 2468www.internationalsos.com</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"DOMESTIC BAGGAGE (JQ/QF/DJ)\">JETSTAREconomy Starter fare includes 10kg of carry- on luggage, customise your requirements at time of booking. http://www.jetstar.com/au/en/planning-and-booking/baggage/checked-baggageQANTASEconomy Class: 1 piece of cabin baggage weighing 7kg + 1 piece of checked baggage weighing maximum 23kg.Business Class: 1 piece of cabin baggage weighing 7kg + 1 piece of checked baggage weighing 32kg per piece.Silver/Gold/Platinum frequent flyers receive extra baggage concessions.http://www.qantas.com.au/travel/airlines/checked-baggage/global/en#jump0VIRGIN AUSTRALIAEconomy Class Saver Fare no allowance: prepaid baggage up to 23kg will cost a flat rate of $12 per flight.Cabin baggage - Up to 7kgs complimentary.NOTE - If paid at check-in up to 23kg will cost a flat rate of $20 per flight.Velocity Gold Members: free extended baggage limit of 32kg checked baggage.Business Class/Premium Economy: permitted 69kg of checked baggage.http://www.virginaustralia.com/Personal/Flightinfo/BeforeYouFly/Baggagedangerousgoods/index.htm</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"DOMESTIC CHECK IN (JQ/QF/DJ)\">JETSTARCheck-in for Jetstar flights leaving from any domestic Australian destination is a minimum (recommended 60 minutes) before your scheduled flight. Check in opens 2 hours prior to the scheduled flights and closes 30 minutes prior to scheduled departure time.http://www.jetstar.com/au/en/planning-and-booking/checking-in/web-check-inQANTASQANTAS domestic flights check-in closes 30 minutes prior to scheduled departure time - except for flights numbered QF2000-QF2299 and QF7000-QF7299 departing from Sydney, check-in time is 1 hour. Online check-in is now available for QANTAS and QANTAS Link Australian domestic bookings between 24 hours and 1 hour before your flight departure.http://www.qantas.com.au/info/flying/atTheAirport/checkinTimesVIRGIN AUSTRALIAVirgin Australia domestic flights check-in closes 30 minutes prior to scheduled departure time. Virgin Blue online check-in is available and opens 24 hours before departure of your flights. Passengers wanting to utilise this facility must present a print out of your boarding pass at check in.http://www.virginaustralia.com/Personal/Services/Check-inoptions/index.htm</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"DOMESTIC FARE CONDITIONS (JQ/QF/DJ)\">JETSTAR - http://travelctm.com/uploads/JQFareGrid.pdfQANTAS - http://travelctm.com/uploads/QFDomesticandNZFareGrid.pdfVIRGIN AUSTRALIA - http://travelctm.com/uploads/DJFaregrid.pdfNOTE: Business Class Virgin Australia only offered on A330-200 aircraft (subject to change).</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"E-TICKET\">E ticket identification required by airlines:Photo identification that has been issued by the Commonwealth of Australia or an Australian State or Territory. Acceptable photo identification includes: drivers license, passport, any state, territory or federal government issued card, company issued identification, or a student card. Qantas acceptable non photo identification includes: credit card used to pay for the ticket, other credit or debit cards, social security card, QF club or frequent flyer card, certified copy/original of a birth of citizenship document. All ID?s must be current and valid.</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"FREQUENT FLYER\">* Please advise your airline loyalty number if applicable. Once received we will enter this into your booking and your profile for future.* To ensure you receive your frequent flyer points, please retain tickets and boarding passes where possible until your points have appeared on your frequent flyer statement.* Should your points not appear on your statement, please send copies of your boarding passes, tickets and supporting information to the relevant airline.</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"IATA\">Transportation and other services provided by the carrier are subject to conditions of contract and other important notices which are delivered with thisitinerary/receipt and form part of the nearest office of the issuing airline or travel agent to obtain a copy prior to the commencement of your trip. Notice ifthe passenger's journey involves an ultimate destination or stop in a country other than the country of departure the Warsaw convention may be applicableand the convention governs and in most cases limits the liability of carriers for death or personal injury and in respect of loss or damage to baggage.</DocumentFooter>\n" +
      "      <DocumentFooter FooterTitle=\"ITINERARY ACCEPTANCE\">Whilst all care has been taken to ensure your travel requirements are booked as per your request, CTM requests that you accept responsibility for double checking these details, and advise of any changes that need to be made as soon as possible. Upon receiving your itinerary please check and make sure the relevant booking is correct and as per your travel needs.</DocumentFooter>\n" +
      "    </DocumentFooters>\n" +
      "    <CarbonEmissionInformation/>\n" +
      "  </DocumentDetails>\n" +
      "  <Booking>\n" +
      "    <BookingDetails>\n" +
      "      <BookingNumber Label=\"Booking ID: \">579189</BookingNumber>\n" +
      "      <PNRDetails>\n" +
      "        <PNRList/>\n" +
      "      </PNRDetails>\n" +
      "      <BookingConsultant1>agent</BookingConsultant1>\n" +
      "      <BookingConsultant1EmailAddress>shusma1@tramada.com</BookingConsultant1EmailAddress>\n" +
      "      <BookingConsultant2/>\n" +
      "      <TravelApprover/>\n" +
      "      <BookedBy Label=\"Booked By: \">Sharyne Andrews</BookedBy>\n" +
      "      <BookingDestinationCity/>\n" +
      "      <BookingDepartureDate Label=\"Departure Date: \">31 Jan 18</BookingDepartureDate>\n" +
      "      <BookingReturnDate Label=\"Return Date: \">05 Feb 18</BookingReturnDate>\n" +
      "      <BookingFinalTKTDate Label=\"Final TKT Date: \">01 Dec 17</BookingFinalTKTDate>\n" +
      "      <BookingCancellationCondition/>\n" +
      "      <BookingDeposits/>\n" +
      "      <BookingDebtors>\n" +
      "        <BookingDebtor>\n" +
      "          <DebtorName Label=\"Debtor: \">KAL</DebtorName>\n" +
      "        </BookingDebtor>\n" +
      "      </BookingDebtors>\n" +
      "      <BookingPassengers>\n" +
      "        <BookingPassenger>\n" +
      "          <PassengerName>KAALZ/TEST MR</PassengerName>\n" +
      "        </BookingPassenger>\n" +
      "        <BookingPassenger>\n" +
      "          <PassengerName>LEONARD/GERARDMR</PassengerName>\n" +
      "          <Memberships>\n" +
      "            <Membership>\n" +
      "              <MembershipNumber>2045223</MembershipNumber>\n" +
      "              <MembershipCompanyName>QANTAS AIRWAYS</MembershipCompanyName>\n" +
      "              <MembershipCompanyCode>QF</MembershipCompanyCode>\n" +
      "              <MembershipProgramName>Qantas</MembershipProgramName>\n" +
      "              <MembershipProgramCode>QF</MembershipProgramCode>\n" +
      "            </Membership>\n" +
      "          </Memberships>\n" +
      "        </BookingPassenger>\n" +
      "        <BookingPassenger>\n" +
      "          <PassengerName>MR MARCO POLO</PassengerName>\n" +
      "        </BookingPassenger>\n" +
      "        <BookingPassenger>\n" +
      "          <PassengerName>MRS MARCH POLO</PassengerName>\n" +
      "        </BookingPassenger>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerName>MRS MARCH POLO</PassengerName>\n" +
      "          <PassengerAge>34</PassengerAge>\n" +
      "        </PassengerDetails>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerName>MR MARCO POLO</PassengerName>\n" +
      "          <PassengerAge>36</PassengerAge>\n" +
      "        </PassengerDetails>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerName>LEONARD/GERARDMR</PassengerName>\n" +
      "          <PassengerAge>ADULT</PassengerAge>\n" +
      "        </PassengerDetails>\n" +
      "        <TicketNumbers/>\n" +
      "        <PassengerNameList Label=\"Itinerary For\">KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList>\n" +
      "      </BookingPassengers>\n" +
      "      <BookingStatus Label=\"Booking Status: \">BOOKED</BookingStatus>\n" +
      "    </BookingDetails>\n" +
      "    <ItineraryItems>\n" +
      "      <ItineraryItem>\n" +
      "        <ItemNo>3906444</ItemNo>\n" +
      "        <SortingOrderNumber>1</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"CAR\">Car</SegmentTypeCode>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList>\n" +
      "        </PassengerDetails>\n" +
      "        <Supplier>\n" +
      "          <SupplierType>Car Company</SupplierType>\n" +
      "          <SupplierName>V Australia</SupplierName>\n" +
      "          <SupplierAddress>\n" +
      "            <Address>\n" +
      "              <Address1/>\n" +
      "              <Address2/>\n" +
      "              <Address3/>\n" +
      "              <PostCode/>\n" +
      "              <State/>\n" +
      "              <Country/>\n" +
      "            </Address>\n" +
      "          </SupplierAddress>\n" +
      "          <SupplierFax/>\n" +
      "        </Supplier>\n" +
      "        <Start>\n" +
      "          <StartLabelValue>Pick Up</StartLabelValue>\n" +
      "          <ServiceDate>Sat 09 Dec 17</ServiceDate>\n" +
      "          <ServiceTime/>\n" +
      "          <CityCode>MEL</CityCode>\n" +
      "          <CityName>MELBOURNE, AUSTRALIA</CityName>\n" +
      "          <LocationTypeDescription Label=\"Pick Up Location Type\"/>\n" +
      "          <LocationDescription Label=\"Pick Up location\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </Start>\n" +
      "        <End>\n" +
      "          <EndLabelValue>Drop Off</EndLabelValue>\n" +
      "          <ServiceDate>Mon 11 Dec 17</ServiceDate>\n" +
      "          <ServiceTime/>\n" +
      "          <CityCode>MEL APRT</CityCode>\n" +
      "          <CityName>MEL APRT</CityName>\n" +
      "          <LocationTypeDescription Label=\"Drop Off Location Type\"/>\n" +
      "          <LocationDescription Label=\"Drop Off Location\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </End>\n" +
      "        <Status>Confirmed</Status>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <OrderDate/>\n" +
      "        <AccountDetails>\n" +
      "          <PaymentTypeDescription>Prepaid</PaymentTypeDescription>\n" +
      "          <PaymentNarrative>Pre-paid remark test cccf</PaymentNarrative>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription>Cancel 10 Hours Prior to Arrival</CancellationDescription>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "          <FareRuleNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <NoOfItems Label=\"No. of Cars\">1</NoOfItems>\n" +
      "        <NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">2</NoOfdays>\n" +
      "        <RateInformation>\n" +
      "          <AUDRate Label=\"Rate Incl GST\">AUD200.00</AUDRate>\n" +
      "          <LocalRate Label=\"Local Rate Incl GST\">AUD200.00</LocalRate>\n" +
      "        </RateInformation>\n" +
      "      </ItineraryItem>\n" +
      "      <ItineraryItem>\n" +
      "        <ItemNo>3906562</ItemNo>\n" +
      "        <SortingOrderNumber>2</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"FLIGHT\">Flight</SegmentTypeCode>\n" +
      "        <PassengerDetails>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>KAALZ/TEST MR</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>LEONARD/GERARDMR</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>MR MARCO POLO</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>MRS MARCH POLO</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList>\n" +
      "        </PassengerDetails>\n" +
      "        <Supplier>\n" +
      "          <SupplierType>Flight Company</SupplierType>\n" +
      "          <SupplierName>AMERICAN AIRLINES</SupplierName>\n" +
      "          <SupplierAddress>\n" +
      "            <Address>\n" +
      "              <Address1/>\n" +
      "              <Address2/>\n" +
      "              <Address3/>\n" +
      "              <PostCode/>\n" +
      "              <State/>\n" +
      "              <Country/>\n" +
      "            </Address>\n" +
      "          </SupplierAddress>\n" +
      "          <SupplierFax/>\n" +
      "        </Supplier>\n" +
      "        <ServiceIdentifier/>\n" +
      "        <Start>\n" +
      "          <StartLabelValue>Departure</StartLabelValue>\n" +
      "          <ServiceDate>Fri 19 Jan 18</ServiceDate>\n" +
      "          <ServiceTime>14:20</ServiceTime>\n" +
      "          <CityCode>DFW</CityCode>\n" +
      "          <CityName>DALLAS FT WORTH, TX</CityName>\n" +
      "          <LocationDescription Label=\"Departure Terminal\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </Start>\n" +
      "        <End>\n" +
      "          <EndLabelValue>Arrival</EndLabelValue>\n" +
      "          <ServiceDate>Sat 20 Jan 18</ServiceDate>\n" +
      "          <ServiceTime>09:25</ServiceTime>\n" +
      "          <CityCode>SYD</CityCode>\n" +
      "          <CityName>Kingsford Smith, Sydney</CityName>\n" +
      "          <LocationDescription Label=\"Arrival Terminal\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </End>\n" +
      "        <FlightSpecificDetails>\n" +
      "          <FlightSpecificMealsSeats>KAALZ/TEST MR|LEONARD/GERARDMR|MR MARCO POLO|MRS MARCH POLO</FlightSpecificMealsSeats>\n" +
      "          <Legs>\n" +
      "            <LegsSummary>DALLAS FT WORTH, TX (TERMINAL - ) Kingsford Smith, Sydney (TERMINAL - ), Dept Time 19-01-2018 14:20, Arrival Time 20-01-2018 09:25</LegsSummary>\n" +
      "            <Leg>\n" +
      "              <StopOverLeg Label=\"Journey Info\">0</StopOverLeg>\n" +
      "              <DepartureCity Label=\"Departure City\">DALLAS FT WORTH, TX</DepartureCity>\n" +
      "              <DepartureDate Label=\"Departure Date\">19-01-2018</DepartureDate>\n" +
      "              <DepartureTime Label=\"Departure Time\">14:20</DepartureTime>\n" +
      "              <ArrivalCity Label=\"Arrival City\">Kingsford Smith, Sydney</ArrivalCity>\n" +
      "              <ArrivalDate Label=\"Arrival Date\">20-01-2018</ArrivalDate>\n" +
      "              <ArrivalTime Label=\"Arrival Time\">09:25</ArrivalTime>\n" +
      "            </Leg>\n" +
      "          </Legs>\n" +
      "        </FlightSpecificDetails>\n" +
      "        <Status>Confirmed</Status>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <OrderDate/>\n" +
      "        <AccountDetails>\n" +
      "          <PaymentTypeDescription/>\n" +
      "          <PaymentNarrative/>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription/>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "          <FareRuleNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <RateInformation/>\n" +
      "      </ItineraryItem>\n" +
      "      <ItineraryItem>\n" +
      "        <ItemNo>3906561</ItemNo>\n" +
      "        <SortingOrderNumber>3</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"HOTEL\">Hotel</SegmentTypeCode>\n" +
      "        <PassengerDetails>\n" +
      "          <PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList>\n" +
      "        </PassengerDetails>\n" +
      "        <Supplier>\n" +
      "          <SupplierType>Hotel Company</SupplierType>\n" +
      "          <SupplierCode Label=\"Supplier Code\">MCSYD7468-1</SupplierCode>\n" +
      "          <SupplierName>MARRIOTT SYDNEY HARBOUR</SupplierName>\n" +
      "          <SupplierAddress>\n" +
      "            <Address>\n" +
      "              <Address1>30 PITT STREET</Address1>\n" +
      "              <Address2>SYDNEY AU 2000</Address2>\n" +
      "              <Address3/>\n" +
      "              <PostCode/>\n" +
      "              <State/>\n" +
      "              <Country>Australia</Country>\n" +
      "            </Address>\n" +
      "          </SupplierAddress>\n" +
      "          <SupplierPhone Label=\"Phone\">P-612-9259-7000</SupplierPhone>\n" +
      "          <SupplierFax>F-612-9251-1122</SupplierFax>\n" +
      "        </Supplier>\n" +
      "        <Start>\n" +
      "          <StartLabelValue>Check In</StartLabelValue>\n" +
      "          <ServiceDate>Sat 20 Jan 18</ServiceDate>\n" +
      "          <ServiceTime>06:00</ServiceTime>\n" +
      "          <CityCode>SYD</CityCode>\n" +
      "          <CityName>Kingsford Smith, Sydney</CityName>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </Start>\n" +
      "        <End>\n" +
      "          <EndLabelValue>Check Out</EndLabelValue>\n" +
      "          <ServiceDate>Tue 23 Jan 18</ServiceDate>\n" +
      "          <ServiceTime>02:00</ServiceTime>\n" +
      "          <CityCode/>\n" +
      "          <CityName/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </End>\n" +
      "        <Status>Confirmed</Status>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <OrderDate/>\n" +
      "        <ConfirmationNo Label=\"Confirmation No\">34353424</ConfirmationNo>\n" +
      "        <AccountDetails>\n" +
      "          <PaymentTypeDescription>Prepaid</PaymentTypeDescription>\n" +
      "          <PaymentNarrative>Pre-paid remark test cccf</PaymentNarrative>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription/>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "          <FareRuleNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <NoOfItems Label=\"No. of Rooms\">1</NoOfItems>\n" +
      "        <NoOfdays UnitOfCount=\"Nights\" Label=\"Duration\">3</NoOfdays>\n" +
      "        <RateInformation>\n" +
      "          <AUDRate Label=\"Rate Incl GST\">AUD260.00</AUDRate>\n" +
      "          <LocalRate Label=\"Local Rate Incl GST\">AUD260.00</LocalRate>\n" +
      "        </RateInformation>\n" +
      "      </ItineraryItem>\n" +
      "      <ItineraryItem>\n" +
      "        <ItemNo>3906563</ItemNo>\n" +
      "        <SortingOrderNumber>4</SortingOrderNumber>\n" +
      "        <SegmentTypeCode Label=\"FLIGHT\">Flight</SegmentTypeCode>\n" +
      "        <PassengerDetails>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>KAALZ/TEST MR</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>LEONARD/GERARDMR</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>MR MARCO POLO</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <Passenger>\n" +
      "            <PassengerName>MRS MARCH POLO</PassengerName>\n" +
      "            <FlightSpecific/>\n" +
      "          </Passenger>\n" +
      "          <PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList>\n" +
      "        </PassengerDetails>\n" +
      "        <Supplier>\n" +
      "          <SupplierType>Flight Company</SupplierType>\n" +
      "          <SupplierName>AMERICAN AIRLINES</SupplierName>\n" +
      "          <SupplierAddress>\n" +
      "            <Address>\n" +
      "              <Address1/>\n" +
      "              <Address2/>\n" +
      "              <Address3/>\n" +
      "              <PostCode/>\n" +
      "              <State/>\n" +
      "              <Country/>\n" +
      "            </Address>\n" +
      "          </SupplierAddress>\n" +
      "          <SupplierFax/>\n" +
      "        </Supplier>\n" +
      "        <ServiceIdentifier/>\n" +
      "        <Start>\n" +
      "          <StartLabelValue>Departure</StartLabelValue>\n" +
      "          <ServiceDate>Tue 23 Jan 18</ServiceDate>\n" +
      "          <ServiceTime>14:20</ServiceTime>\n" +
      "          <CityCode>SYD</CityCode>\n" +
      "          <CityName>Kingsford Smith, Sydney</CityName>\n" +
      "          <LocationDescription Label=\"Departure Terminal\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </Start>\n" +
      "        <End>\n" +
      "          <EndLabelValue>Arrival</EndLabelValue>\n" +
      "          <ServiceDate>Tue 23 Jan 18</ServiceDate>\n" +
      "          <ServiceTime>09:25</ServiceTime>\n" +
      "          <CityCode>DFW</CityCode>\n" +
      "          <CityName>DALLAS FT WORTH, TX</CityName>\n" +
      "          <LocationDescription Label=\"Arrival Terminal\"/>\n" +
      "          <Address>\n" +
      "            <Address1/>\n" +
      "            <Address2/>\n" +
      "            <Address3/>\n" +
      "            <PostCode/>\n" +
      "            <State/>\n" +
      "            <Country/>\n" +
      "          </Address>\n" +
      "          <Contacts>\n" +
      "            <PhoneNo/>\n" +
      "            <FaxNo/>\n" +
      "          </Contacts>\n" +
      "        </End>\n" +
      "        <FlightSpecificDetails>\n" +
      "          <FlightSpecificMealsSeats>KAALZ/TEST MR|LEONARD/GERARDMR|MR MARCO POLO|MRS MARCH POLO</FlightSpecificMealsSeats>\n" +
      "          <Legs>\n" +
      "            <LegsSummary>Kingsford Smith, Sydney (TERMINAL - ) DALLAS FT WORTH, TX (TERMINAL - ), Dept Time 23-01-2018 14:20, Arrival Time 20-01-2018 09:25</LegsSummary>\n" +
      "            <Leg>\n" +
      "              <StopOverLeg Label=\"Journey Info\">0</StopOverLeg>\n" +
      "              <DepartureCity Label=\"Departure City\">Kingsford Smith, Sydney</DepartureCity>\n" +
      "              <DepartureDate Label=\"Departure Date\">23-01-2018</DepartureDate>\n" +
      "              <DepartureTime Label=\"Departure Time\">14:20</DepartureTime>\n" +
      "              <ArrivalCity Label=\"Arrival City\">DALLAS FT WORTH, TX</ArrivalCity>\n" +
      "              <ArrivalDate Label=\"Arrival Date\">20-01-2018</ArrivalDate>\n" +
      "              <ArrivalTime Label=\"Arrival Time\">09:25</ArrivalTime>\n" +
      "            </Leg>\n" +
      "          </Legs>\n" +
      "        </FlightSpecificDetails>\n" +
      "        <Status>Confirmed</Status>\n" +
      "        <IssueDate>\n" +
      "        </IssueDate>\n" +
      "        <OrderDate/>\n" +
      "        <AccountDetails>\n" +
      "          <PaymentTypeDescription/>\n" +
      "          <PaymentNarrative/>\n" +
      "          <SupplierCharges/>\n" +
      "          <SupplierFees/>\n" +
      "          <Inclusions/>\n" +
      "          <Exclusions/>\n" +
      "          <RateGuaranteeDescription/>\n" +
      "          <CancelConditions/>\n" +
      "          <CancellationDescription/>\n" +
      "        </AccountDetails>\n" +
      "        <ItineraryNotes>\n" +
      "          <SegmentNotes/>\n" +
      "          <FareRuleNotes/>\n" +
      "        </ItineraryNotes>\n" +
      "        <RateInformation/>\n" +
      "      </ItineraryItem>\n" +
      "    </ItineraryItems>\n" +
      "    <CostingsItems>\n" +
      "      <DocumentTotals>\n" +
      "        <TotalPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>0.00</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST>\n" +
      "            <GST>0.00</GST>\n" +
      "            <ClientDue>0.00</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalPayDirect>\n" +
      "        <TotalExcPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>0.00</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST>\n" +
      "            <GST>0.00</GST>\n" +
      "            <ClientDue>0.00</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalExcPayDirect>\n" +
      "        <TotalIncPayDirect>\n" +
      "          <Total>\n" +
      "            <BaseFare>0.00</BaseFare>\n" +
      "            <Discount>0.00</Discount>\n" +
      "            <TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST>\n" +
      "            <GST>0.00</GST>\n" +
      "            <ClientDue>0.00</ClientDue>\n" +
      "          </Total>\n" +
      "        </TotalIncPayDirect>\n" +
      "        <TotalPaid>0.00</TotalPaid>\n" +
      "        <TotalOutstandingExcPayDirect>0.00</TotalOutstandingExcPayDirect>\n" +
      "        <Currency>AUD</Currency>\n" +
      "      </DocumentTotals>\n" +
      "    </CostingsItems>\n" +
      "  </Booking>\n" +
      "</DocumentInformation>\n";


  @Test
  public void testTramadaHelper1() throws Exception {
    System.out.println("Testing Tramada 1");
    String token = "1Lzy8YPNgNY";

    HttpClient c = HttpClientBuilder.create().build();
    HttpPost p = new HttpPost("http://localhost:9000/tramada/importBooking");
    p.setHeader("Content-Type", "application/xml");
    p.setHeader("Authorization", "Bearer " + token);
    StringEntity se = new StringEntity(xml);
    p.setEntity(se);

    HttpResponse r = c.execute(p);
    int code = r.getStatusLine().getStatusCode();
    System.out.println(code);
    String result = EntityUtils.toString(r.getEntity());
    assertTrue("Success Code - 200!!", code == 200);
  }


  @Test
  public void testTramadaHelper2() throws Exception {

    System.out.println("Testing Tramada 2");
    String token = "1Lzy8YPNgNY";

    HttpClient c = HttpClientBuilder.create().build();
    HttpPost p = new HttpPost("http://localhost:9000/tramada/importBooking");
    p.setHeader("Content-Type", "application/xml");
    p.setHeader("Authorization", "Bearer " + token);
    StringEntity se = new StringEntity(xml2);
    p.setEntity(se);

    HttpResponse r = c.execute(p);
    int code = r.getStatusLine().getStatusCode();
    System.out.println(code);
    String result = EntityUtils.toString(r.getEntity());
    assertTrue("Success Code - 200!!", code == 200);
  }

  @Test
  public void testTramadaHelper3() throws Exception {

    System.out.println("Testing Tramada 3");
    String token = "1Lzy8YPNgNY";

    HttpClient c = HttpClientBuilder.create().build();
    HttpPost p = new HttpPost("http://localhost:9000/tramada/importBooking");
    p.setHeader("Content-Type", "application/xml");
    p.setHeader("Authorization", "Bearer " + token);
    StringEntity se = new StringEntity(xml3);
    p.setEntity(se);

    HttpResponse r = c.execute(p);
    int code = r.getStatusLine().getStatusCode();
    System.out.println(code);
    String result = EntityUtils.toString(r.getEntity());
    assertTrue("Success Code - 200!!", code == 200);
  }



  @Test
  public void testTramadaHelper4() throws Exception {
    System.out.println("Testing Tramada 4");
    String token = "1Lzy8YPNgNY";

    HttpClient c = HttpClientBuilder.create().build();
    HttpPost p = new HttpPost("http://localhost:9000/tramada/importBooking");
    p.setHeader("Content-Type", "application/xml");
    p.setHeader("Authorization", "Bearer " + token);
    StringEntity se = new StringEntity(xml4);
    p.setEntity(se);

    HttpResponse r = c.execute(p);
    int code = r.getStatusLine().getStatusCode();
    System.out.println(code);
    String result = EntityUtils.toString(r.getEntity());
    assertTrue("Success Code - 200!!", code == 200);
  }


  @Test
  public void testTramadaHelper5() throws Exception {
    System.out.println("Testing Tramada 5");
    String token = "1Lzy8YPNgNY";

    HttpClient c = HttpClientBuilder.create().build();
    HttpPost p = new HttpPost("http://localhost:9000/tramada/importBooking");
    p.setHeader("Content-Type", "application/xml");
    p.setHeader("Authorization", "Bearer " + token);
    StringEntity se = new StringEntity(xml5);
    p.setEntity(se);

    HttpResponse r = c.execute(p);
    int code = r.getStatusLine().getStatusCode();
    System.out.println(code);
    String result = EntityUtils.toString(r.getEntity());
    assertTrue("Success Code - 200!!", code == 200);
  }

  @Test
  public void testTramadaHelper6() throws Exception {

    System.out.println("Testing Tramada 6");
    String token = "1Lzy8YPNgNY";

    HttpClient c = HttpClientBuilder.create().build();
    HttpPost p = new HttpPost("http://localhost:9000/tramada/importBooking");
    p.setHeader("Content-Type", "application/xml");
    p.setHeader("Authorization", "Bearer " + token);
    StringEntity se = new StringEntity(xml6);
    p.setEntity(se);

    HttpResponse r = c.execute(p);
    int code = r.getStatusLine().getStatusCode();
    System.out.println(code);
    String result = EntityUtils.toString(r.getEntity());
    assertTrue("Success Code - 200!!", code == 200);
  }

  @Test
  public void testTramadaHelper7() throws Exception {

    System.out.println("Testing Tramada 7");
    String token = "1Lzy8YPNgNY";

    HttpClient c = HttpClientBuilder.create().build();
    HttpPost p = new HttpPost("http://localhost:9000/tramada/importBooking");
    p.setHeader("Content-Type", "application/xml");
    p.setHeader("Authorization", "Bearer " + token);

    StringEntity se = new StringEntity(miscXml7);
    p.setEntity(se);

    HttpResponse r = c.execute(p);
    int code = r.getStatusLine().getStatusCode();
    System.out.println(code);
    String result = EntityUtils.toString(r.getEntity());
    assertTrue("Success Code - 200!!", code == 200);
  }

  @Test
  public void testTramadaHelper8() throws Exception {

    System.out.println("Testing Tramada 8");
    String token = "1Lzy8YPNgNY";

    HttpClient c = HttpClientBuilder.create().build();
    HttpPost p = new HttpPost("http://localhost:9000/tramada/importBooking");
    p.setHeader("Content-Type", "application/xml");
    p.setHeader("Authorization", "Bearer " + token);
    StringEntity se = new StringEntity(xml8);
    p.setEntity(se);

    HttpResponse r = c.execute(p);
    int code = r.getStatusLine().getStatusCode();
    System.out.println(code);
    String result = EntityUtils.toString(r.getEntity());
    assertTrue("Success Code - 200!!", code == 200);
  }

  @Test
  public void testTramadaHelper9() throws Exception {
    System.out.println("Testing Tramada 9");
    String token = "1Lzy8YPNgNY";

    HttpClient c = HttpClientBuilder.create().build();
    HttpPost p = new HttpPost("http://localhost:9000/tramada/importBooking");
    p.setHeader("Content-Type", "application/xml");
    p.setHeader("Authorization", "Bearer " + token);
    StringEntity se = new StringEntity(xml9);
    p.setEntity(se);

    HttpResponse r = c.execute(p);
    int code = r.getStatusLine().getStatusCode();
    System.out.println(code);
    String result = EntityUtils.toString(r.getEntity());
    assertTrue("Success Code - 200!!", code == 200);
  }

  @Test
  public void testTramadaHelper10() throws Exception {
    System.out.println("Testing Tramada 10");
    String token = "1Lzy8YPNgNY";

    HttpClient c = HttpClientBuilder.create().build();
    HttpPost p = new HttpPost("http://localhost:9000/tramada/importBooking");
    p.setHeader("Content-Type", "application/xml");
    p.setHeader("Authorization", "Bearer " + token);
    StringEntity se = new StringEntity(xml10);
    p.setEntity(se);

    HttpResponse r = c.execute(p);
    int code = r.getStatusLine().getStatusCode();
    System.out.println(code);
    String result = EntityUtils.toString(r.getEntity());
    assertTrue("Success Code - 200!!", code == 200);
  }

  @Test
  public void testTramadaHelper11() throws Exception {
    System.out.println("Testing Tramada 11");
    String token = "1Lzy8YPNgNY";

    HttpClient c = HttpClientBuilder.create().build();
    HttpPost p = new HttpPost("http://localhost:9000/tramada/importBooking");
    p.setHeader("Content-Type", "application/xml");
    p.setHeader("Authorization", "Bearer " + token);
    StringEntity se = new StringEntity(xml11);
    p.setEntity(se);

    HttpResponse r = c.execute(p);
    int code = r.getStatusLine().getStatusCode();
    System.out.println(code);
    String result = EntityUtils.toString(r.getEntity());
    assertTrue("Success Code - 200!!", code == 200);
  }

  @Test
  public void testTramadaHelper12() throws Exception {
    System.out.println("Testing Tramada 12");
    String token = "1Lzy8YPNgNY";

    HttpClient c = HttpClientBuilder.create().build();
    HttpPost p = new HttpPost("http://localhost:9000/tramada/importBooking");
    p.setHeader("Content-Type", "application/xml");
    p.setHeader("Authorization", "Bearer " + token);
    StringEntity se = new StringEntity(xml12);
    p.setEntity(se);

    HttpResponse r = c.execute(p);
    int code = r.getStatusLine().getStatusCode();
    System.out.println(code);
    String result = EntityUtils.toString(r.getEntity());
    assertTrue("Success Code - 200!!", code == 200);
  }

  @Test
  public void testTramadaHelper13() throws Exception {
    System.out.println("Testing Tramada 13");
    String token = "1Lzy8YPNgNY";

    HttpClient c = HttpClientBuilder.create().build();
    HttpPost p = new HttpPost("http://localhost:9000/tramada/importBooking");
    p.setHeader("Content-Type", "application/xml");
    p.setHeader("Authorization", "Bearer " + token);
    StringEntity se = new StringEntity(xml13);
    p.setEntity(se);

    HttpResponse r = c.execute(p);
    int code = r.getStatusLine().getStatusCode();
    System.out.println(code);
    String result = EntityUtils.toString(r.getEntity());
    assertTrue("Success Code - 200!!", code == 200);
  }*/

  /*String xml14 = "<DocumentInformation><Agency><AgencyName Label=\"TMC\">Tests Travel Management</AgencyName><AgencyCode Label=\"TMC Code\">trunk_tramada</AgencyCode><AgencyAddress><Address><Address1 Label=\"Address Line 1\">trunkTRAMADA</Address1><Address2 Label=\" Address Line 2\">Address 2</Address2><Address3 Label=\" Address Line 3\">Address 3</Address3><PostCode Label=\" Postcode\">2000</PostCode><State Label=\"State\">NSW</State><Country Label=\"Country\">Australia</Country></Address></AgencyAddress><AgencyContacts><AgencyEmail Label=\"Email: \">ashok@tramada.com</AgencyEmail><AgencyPhone Label=\"Phone: \">0470632287</AgencyPhone><AgencyFax Label=\"Fax No.: \">0470632287</AgencyFax></AgencyContacts><PccCode Label=\"PCC Code: \">3G1A</PccCode></Agency><DocumentDetails><DocumentPrintDate>Friday 16 March 2018 17:26</DocumentPrintDate><DocumentPrintTime>Sydney, NSW</DocumentPrintTime><DocumentTitle Label=\"Type\">Itinerary &amp; Costing - Full</DocumentTitle><DocumentAddress><Address/></DocumentAddress><DocumentContactDetails><Website Label=\"Website: \">www.travelctm.com</Website><Email Label=\"Email: \">shusma1@tramada.com</Email><EmergencyContact Label=\"Emergency Contact: \">1800 663 622</EmergencyContact><AfterHours Label=\"After Hours Contact: \">1800 663 622</AfterHours><TollFree Label=\"Toll Free Number: \">1800 663 622</TollFree></DocumentContactDetails><DocumentCommercialDetails><LicNo Label=\"Licence No.: \">854</LicNo><ABN Label=\"ABN: \">52 005 000 895</ABN><ACN Label=\"ACN: \">005 000 895</ACN></DocumentCommercialDetails><DocumentHeaders><DocumentHeader HeaderTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually There®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.\n" +
      "You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentHeader><DocumentHeader HeaderTitle=\"Third party hotel booking\">Please note: your hotel has been booked via a third party provider. They have provided a confirmation number and this may be required at check-in. In most instances you will also have been provided with a confirmation and payment voucher separately to the itinerary. Please ensure you have printed a copy of this voucher and are able to produce it upon request. There can be occasional communication problems between third party bookers and hotels. If you experience any difficulties at check-in please call CTM on the numbers below.</DocumentHeader><DocumentHeader HeaderTitle=\"label\">Testing Helloworld Header</DocumentHeader></DocumentHeaders><DocumentFooters><DocumentFooter FooterTitle=\"Access your itinerary online\">You will also receive from your travel booker an email with a link to your live itinerary via a website called Virtually There®.  This email will have a hyperlink to click to access your reservation online in real time, including any time changes.\n" +
      "You may also access your reservation on the web or from your mobile device at www.virtuallythere.com. Simply enter your last name and the six-character reservation code (found in the top right hand corner of this itinerary called the PNR Locator). As a security measure, you will be prompted to enter your e-mail address</DocumentFooter><DocumentFooter FooterTitle=\"Assistance while overseas\">Subsea 7 provide assistance in emergencies arising when personnel are travelling overseas. Comprehensive emergency assistance is available to Employees through International SOS, these cover Medical Services and Security.\n" +
      "\n" +
      "The following information is required for International SOS:\n" +
      "\n" +
      "SOS Programme Number: 14ACPA000020\n" +
      "\n" +
      "Employer: Subsea 7\n" +
      "\n" +
      "24h phone number: +61 2 9372 2468\n" +
      "\n" +
      "www.internationalsos.com</DocumentFooter><DocumentFooter FooterTitle=\"DOMESTIC BAGGAGE (JQ/QF/DJ)\">JETSTAR\n" +
      "\n" +
      "Economy Starter fare includes 10kg of carry- on luggage, customise your requirements at time of booking.\n" +
      "\n" +
      " \n" +
      "\n" +
      "http://www.jetstar.com/au/en/planning-and-booking/baggage/checked-baggage\n" +
      "\n" +
      "QANTAS\n" +
      "\n" +
      "Economy Class: 1 piece of cabin baggage weighing 7kg + 1 piece of checked baggage weighing maximum 23kg.\n" +
      "\n" +
      "Business Class: 1 piece of cabin baggage weighing 7kg + 1 piece of checked baggage weighing 32kg per piece.\n" +
      "\n" +
      "Silver/Gold/Platinum frequent flyers receive extra baggage concessions.\n" +
      "\n" +
      "http://www.qantas.com.au/travel/airlines/checked-baggage/global/en#jump0\n" +
      "\n" +
      "VIRGIN AUSTRALIA\n" +
      "\n" +
      "Economy Class Saver Fare no allowance: prepaid baggage up to 23kg will cost a flat rate of $12 per flight.\n" +
      "\n" +
      "Cabin baggage - Up to 7kgs complimentary.\n" +
      "\n" +
      "NOTE - If paid at check-in up to 23kg will cost a flat rate of $20 per flight.\n" +
      "\n" +
      "Velocity Gold Members: free extended baggage limit of 32kg checked baggage.\n" +
      "\n" +
      "Business Class/Premium Economy: permitted 69kg of checked baggage.\n" +
      "\n" +
      "http://www.virginaustralia.com/Personal/Flightinfo/BeforeYouFly/Baggagedangerousgoods/index.htm</DocumentFooter><DocumentFooter FooterTitle=\"DOMESTIC CHECK IN (JQ/QF/DJ)\">JETSTAR\n" +
      "\n" +
      "Check-in for Jetstar flights leaving from any domestic Australian destination is a minimum (recommended 60 minutes) before your scheduled flight. Check in opens 2 hours prior to the scheduled flights and closes 30 minutes prior to scheduled departure time.\n" +
      "\n" +
      "http://www.jetstar.com/au/en/planning-and-booking/checking-in/web-check-in\n" +
      "\n" +
      "QANTAS\n" +
      "\n" +
      "QANTAS domestic flights check-in closes 30 minutes prior to scheduled departure time - except for flights numbered QF2000-QF2299 and QF7000-QF7299 departing from Sydney, check-in time is 1 hour. Online check-in is now available for QANTAS and QANTAS Link Australian domestic bookings between 24 hours and 1 hour before your flight departure.\n" +
      "\n" +
      "http://www.qantas.com.au/info/flying/atTheAirport/checkinTimes\n" +
      "\n" +
      "VIRGIN AUSTRALIA\n" +
      "\n" +
      "Virgin Australia domestic flights check-in closes 30 minutes prior to scheduled departure time. Virgin Blue online check-in is available and opens 24 hours before departure of your flights. Passengers wanting to utilise this facility must present a print out of your boarding pass at check in.\n" +
      "\n" +
      "http://www.virginaustralia.com/Personal/Services/Check-inoptions/index.htm</DocumentFooter><DocumentFooter FooterTitle=\"DOMESTIC FARE CONDITIONS (JQ/QF/DJ)\">JETSTAR - http://travelctm.com/uploads/JQFareGrid.pdf\n" +
      "\n" +
      "QANTAS - http://travelctm.com/uploads/QFDomesticandNZFareGrid.pdf\n" +
      "\n" +
      "VIRGIN AUSTRALIA - http://travelctm.com/uploads/DJFaregrid.pdf\n" +
      "\n" +
      "NOTE: Business Class Virgin Australia only offered on A330-200 aircraft (subject to change).</DocumentFooter><DocumentFooter FooterTitle=\"E-TICKET\">E ticket identification required by airlines:\n" +
      "\n" +
      "Photo identification that has been issued by the Commonwealth of Australia or an Australian State or Territory. Acceptable photo identification includes: drivers license, passport, any state, territory or federal government issued card, company issued identification, or a student card. Qantas acceptable non photo identification includes: credit card used to pay for the ticket, other credit or debit cards, social security card, QF club or frequent flyer card, certified copy/original of a birth of citizenship document. All ID?s must be current and valid.</DocumentFooter><DocumentFooter FooterTitle=\"FREQUENT FLYER\">* Please advise your airline loyalty number if applicable. Once received we will enter this into your booking and your profile for future.\n" +
      "\n" +
      "* To ensure you receive your frequent flyer points, please retain tickets and boarding passes where possible until your points have appeared on your frequent flyer statement.\n" +
      "\n" +
      "* Should your points not appear on your statement, please send copies of your boarding passes, tickets and supporting information to the relevant airline.</DocumentFooter><DocumentFooter FooterTitle=\"IATA\">Transportation and other services provided by the carrier are subject to conditions of contract and other important notices which are delivered with this\n" +
      "itinerary/receipt and form part of the nearest office of the issuing airline or travel agent to obtain a copy prior to the commencement of your trip. Notice if\n" +
      "the passenger's journey involves an ultimate destination or stop in a country other than the country of departure the Warsaw convention may be applicable\n" +
      "and the convention governs and in most cases limits the liability of carriers for death or personal injury and in respect of loss or damage to baggage.</DocumentFooter><DocumentFooter FooterTitle=\"ITINERARY ACCEPTANCE\">Whilst all care has been taken to ensure your travel requirements are booked as per your request, CTM requests that you accept responsibility for double checking these details, and advise of any changes that need to be made as soon as possible. Upon receiving your itinerary please check and make sure the relevant booking is correct and as per your travel needs.</DocumentFooter></DocumentFooters><CarbonEmissionInformation/></DocumentDetails><Booking><BookingDetails><BookingNumber Label=\"Booking ID: \">579761</BookingNumber><BookingLevel1Branch Label=\"Branch: \">Corporate Travel Management</BookingLevel1Branch><PNRDetails><PNRList/></PNRDetails><BookingConsultant1>agent</BookingConsultant1><BookingConsultant1EmailAddress>shusma1@tramada.com</BookingConsultant1EmailAddress><BookingConsultant2></BookingConsultant2><TravelArranger></TravelArranger><TravelApprover></TravelApprover><BookedBy Label=\"Booked By: \">Sharyne Andrews</BookedBy><BookingDestinationCity><CityCode Label=\"Booking Destination City Code\">UNA</CityCode><CityName Label=\" Booking Destination City\">ILHA COMANDATUBA, BRAZIL</CityName></BookingDestinationCity><BookingDepartureDate Label=\"Departure Date: \">01 Mar 18</BookingDepartureDate><BookingReturnDate Label=\"Return Date: \">31 Mar 18</BookingReturnDate><BookingFinalTKTDate Label=\"Final TKT Date: \">01 Dec 18</BookingFinalTKTDate><BookingCancellationCondition></BookingCancellationCondition><BookingReasonForTravel Label=\"Reason for Travel: \">test</BookingReasonForTravel><BookingDeposits/><BookingDebtors><BookingDebtor><DebtorName Label=\"Debtor: \">KAL</DebtorName></BookingDebtor></BookingDebtors><BookingPassengers><BookingPassenger><PassengerName>KAALZ/TEST MR</PassengerName><Memberships><Membership><MembershipType>AIR</MembershipType><MembershipNumber>12356</MembershipNumber><MembershipCompanyName>VIRGIN AUSTRALIA</MembershipCompanyName><MembershipCompanyCode>DJ</MembershipCompanyCode><MembershipProgramName>VELOCITY GOLD/SILVER</MembershipProgramName><MembershipProgramCode>AIR_DJ2</MembershipProgramCode></Membership></Memberships></BookingPassenger><BookingPassenger><PassengerName>LEONARD/GERARDMR</PassengerName><Memberships><Membership><MembershipType>AIR</MembershipType><MembershipNumber>2045223</MembershipNumber><MembershipCompanyName>QANTAS AIRWAYS</MembershipCompanyName><MembershipCompanyCode>QF</MembershipCompanyCode><MembershipProgramName>Qf_test</MembershipProgramName><MembershipProgramCode>QF_AIR</MembershipProgramCode></Membership></Memberships></BookingPassenger><BookingPassenger><PassengerName>MR MARCO POLO</PassengerName><Memberships><Membership><MembershipType>CAR</MembershipType><MembershipNumber>56239</MembershipNumber><MembershipCompanyName>AO</MembershipCompanyName><MembershipCompanyCode>AO</MembershipCompanyCode><MembershipProgramName>AO</MembershipProgramName><MembershipProgramCode>AO_CAR</MembershipProgramCode></Membership></Memberships></BookingPassenger><BookingPassenger><PassengerName>MRS MARCH POLO</PassengerName></BookingPassenger><PassengerDetails><PassengerName>MR MARCO POLO</PassengerName><PassengerAge>36</PassengerAge></PassengerDetails><PassengerDetails><PassengerName>LEONARD/GERARDMR</PassengerName><PassengerAge>ADULT</PassengerAge></PassengerDetails><PassengerDetails><PassengerName>MRS MARCH POLO</PassengerName><PassengerAge>34</PassengerAge></PassengerDetails><TicketNumbers/><PassengerNameList Label=\"Itinerary For\">KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList></BookingPassengers><BookingStatus Label=\"Booking Status: \">BOOKED</BookingStatus></BookingDetails><ItineraryItems><ItineraryItem><ItemNo>3908728</ItemNo><SortingOrderNumber>1</SortingOrderNumber><SegmentTypeCode Label=\"CAR\">Car</SegmentTypeCode><PassengerDetails><PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList></PassengerDetails><Supplier><SupplierType>Car Company</SupplierType><SupplierName>V Australia</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Pick Up</StartLabelValue><ServiceDate>Sat 09 Dec 18</ServiceDate><ServiceTime/><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><LocationTypeDescription Label=\"Pick Up Location Type\"/><LocationDescription Label=\"Pick Up location\"/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Drop Off</EndLabelValue><ServiceDate>Mon 11 Dec 18</ServiceDate><ServiceTime/><CityCode>MEL APRT</CityCode><CityName>MEL APRT</CityName><LocationTypeDescription Label=\"Drop Off Location Type\"/><LocationDescription Label=\"Drop Off Location\"/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate>Sat 09 Dec 18</IssueDate><OrderDate/><ConfirmationNo Label=\"Confirmation No\">123456NEW</ConfirmationNo><AccountDetails><PaymentTypeDescription>Pay Direct</PaymentTypeDescription><PaymentNarrative>All Charges</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription>Cancel 10 Hours Prior to Arrival</CancellationDescription></AccountDetails><ItineraryNotes><SegmentNotes/><FareRuleNotes/><HistoricalNotes/></ItineraryNotes><NoOfItems Label=\"No. of Cars\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">2</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD200.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD200.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3908876</ItemNo><SortingOrderNumber>2</SortingOrderNumber><SegmentTypeCode Label=\"TRAIN\">Train</SegmentTypeCode><PassengerDetails><PassengerNameList>KAALZ/TEST MR, LEONARD/GERARDMR, MR MARCO POLO, MRS MARCH POLO</PassengerNameList></PassengerDetails><Supplier><SupplierType>Train Company</SupplierType><SupplierCode Label=\"Supplier Code\">TRAFALGAR</SupplierCode><SupplierName>TRAFALGAR TOURS</SupplierName><SupplierAddress><Address><Address1>TRAVEL HOUSE</Address1><Address2>35 GRAFTON ST</Address2><Address3>WOOLLAHRA</Address3><PostCode>2025</PostCode><State>NSW</State><Country>Australia</Country></Address></SupplierAddress><SupplierPhone Label=\"Phone\">02 9657 3333</SupplierPhone><SupplierFax>02 9657 3456</SupplierFax></Supplier><Start><StartLabelValue>Embark</StartLabelValue><ServiceDate>Fri 16 Mar 18</ServiceDate><ServiceTime/><CityCode>SYD</CityCode><CityName>Kingsford Smith, Sydney</CityName><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Disembark</EndLabelValue><ServiceDate>Fri 16 Mar 18</ServiceDate><ServiceTime/><CityCode>MEL</CityCode><CityName>MELBOURNE, AUSTRALIA</CityName><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails/><Status>Confirmed</Status><IssueDate>Wed 31 Mar 18</IssueDate><OrderDate/><ConfirmationNo Label=\"Confirmation No\">TRAIN11</ConfirmationNo><AccountDetails><PaymentTypeDescription>Prepaid</PaymentTypeDescription><PaymentNarrative>Free of Charge</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription>24 hours cancellation notice required</CancellationDescription></AccountDetails><ItineraryNotes><SegmentNotes/><FareRuleNotes/><HistoricalNotes/></ItineraryNotes><NoOfItems Label=\"No. of Units\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD250.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD250.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3908877</ItemNo><SortingOrderNumber>17</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Ferry</SegmentTypeCode><PassengerDetails><PassengerNameList>MR TWO TEST\n" +
      "MRS THREE TEST\n" +
      "SIMPSON/OLIVIA MISS\n" +
      "VT/INFANT</PassengerNameList></PassengerDetails><Supplier><SupplierType>Ferry Company</SupplierType><SupplierCode Label=\"Supplier Code\">WILLF</SupplierCode><SupplierName>Williamstown Ferries</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><Start><StartLabelValue>Embark</StartLabelValue><ServiceDate>Thu 27 Jul 17</ServiceDate><ServiceTime/><CityCode/><CityName>MEL</CityName><LocationDescription>MEL</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Disembark</EndLabelValue><ServiceDate>Thu 27 Jul 17</ServiceDate><ServiceTime/><CityCode/><CityName>MEL</CityName><LocationDescription>MEL</LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><ConfirmationNo Label=\"Confirmation No\">FERRY11</ConfirmationNo><AccountDetails><PaymentNarrative>Free of Charge</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><NoOfItems Label=\"No. Of Passengers\">1</NoOfItems><NoOfdays UnitOfCount=\"Days\" Label=\"Duration\">1</NoOfdays><RateInformation><AUDRate Label=\"Rate Incl GST\">AUD300.00</AUDRate><LocalRate Label=\"Local Rate Incl GST\">AUD300.00</LocalRate></RateInformation></ItineraryItem><ItineraryItem><ItemNo>3908878</ItemNo><SourceReloc Label=\"PNR Reference\">TUYVPFNEW</SourceReloc><SortingOrderNumber>18</SortingOrderNumber><SegmentTypeCode Label=\"Segment Type\">Flight</SegmentTypeCode><PassengerDetails><PassengerNameList>MR TWO TEST\n" +
      "MRS THREE TEST\n" +
      "SIMPSON/OLIVIA MISS\n" +
      "VT/INFANT\n" +
      "</PassengerNameList></PassengerDetails><Supplier><SupplierType>Flight Company</SupplierType><SupplierName>AIR NAMIBIA</SupplierName><SupplierAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></SupplierAddress><SupplierFax/></Supplier><ServiceIdentifier>SW0123</ServiceIdentifier><ServiceClassDescription Label=\"Class\">Y - Economy</ServiceClassDescription><Start><StartLabelValue>Departure</StartLabelValue><ServiceDate>Wed 27 Dec 17</ServiceDate><ServiceTime>09:00</ServiceTime><CityCode/><CityName>DFW</CityName><LocationDescription></LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Arrival</EndLabelValue><ServiceDate>Wed 27 Dec 17</ServiceDate><ServiceTime>10:20</ServiceTime><CityCode/><CityName>HOU</CityName><LocationDescription></LocationDescription><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><FlightSpecificDetails><FlightSpecificMealsSeats>MR TWO TEST\n" +
      "Seat No 12A (Aisle - Forward)\n" +
      "Meal Requested ASIAN VEG.MEAL\n" +
      "\n" +
      "MRS THREE TEST\n" +
      "Seat No 22A (Window - Upper Deck)\n" +
      "Meal Requested ORIENTAL\n" +
      "\n" +
      "SIMPSON/OLIVIA MISS\n" +
      "BSCT\n" +
      "Meal Requested INF/BABY FOOD\n" +
      "\n" +
      "VT/INFANT\n" +
      "BSCT\n" +
      "Meal Requested INF/BABY FOOD\n" +
      "</FlightSpecificMealsSeats><Legs><LegsSummary>DFW (TERMINAL - ) HOU (TERMINAL - ), Dept Time 27-12-2017 09:00, Arrival Time 27-12-2017 10:20 - Meal Service: Snack\n" +
      "</LegsSummary></Legs></FlightSpecificDetails><Status>Confirmed</Status><IssueDate></IssueDate><OrderDate/><AccountDetails><PaymentNarrative/><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/></ItineraryNotes><RateInformation/></ItineraryItem></ItineraryItems><CostingsItems><CostingItem><ItemNo>2220299</ItemNo><SortingOrderNumber>1</SortingOrderNumber><SegmentTypeCode Label=\"CAR\">Car</SegmentTypeCode><Creditor><CreditorType>Car Company</CreditorType><CreditorCode Label=\"Creditor Code\">VA</CreditorCode><CreditorName>V Australia</CreditorName><CreditorAddress><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address></CreditorAddress><CreditorFax/></Creditor><CostingDescriptor/><IssueDate>09 Dec 18</IssueDate><ConfirmationNo Label=\"Confirmation No\">123456NEW</ConfirmationNo><StartServiceDate Label=\"Start Service Date\">09 Dec 18</StartServiceDate><StartCity>MELBOURNE, AUSTRALIA</StartCity><StartCityName Label=\"Pick-Up City\"/><EndServiceDate Label=\"End Service Date\">11 Dec 18</EndServiceDate><EndCity>MEL APRT</EndCity><EndCityName Label=\"Drop-off City\"/><CostingNotes/><CostingFinancials><AppliedRate>1.00</AppliedRate><CurrencyCode>AUD</CurrencyCode><BaseFare>363.64</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">36.36</GST><ClientDue>400.00</ClientDue><PaymentType>Pay Direct</PaymentType><ChargeBackFlag ChargebackDateValue=\"\">N</ChargeBackFlag></CostingFinancials><CancellationDescription Label=\"Cancellation\">Cancel 10 Hours Prior to Arrival</CancellationDescription></CostingItem><CostingItem><ItemNo>2220420</ItemNo><SortingOrderNumber>2</SortingOrderNumber><SegmentTypeCode Label=\"TRAIN\">Train</SegmentTypeCode><Creditor><CreditorType>Train Company</CreditorType><CreditorCode Label=\"Creditor Code\">TRAFALGAR</CreditorCode><CreditorName>TRAFALGAR TOURS</CreditorName><CreditorAddress><Address><Address1>TRAVEL HOUSE</Address1><Address2>35 GRAFTON ST</Address2><Address3>WOOLLAHRA</Address3><PostCode>2025</PostCode><State>NSW</State><Country>Australia</Country></Address></CreditorAddress><CreditorPhone Label=\"Phone No.\">02 9657 3333</CreditorPhone><CreditorFax>02 9657 3456</CreditorFax></Creditor><CostingDescriptor/><IssueDate>31 Mar 18</IssueDate><StartServiceDate Label=\"Start Service Date\">16 Feb 18</StartServiceDate><StartCity>Kingsford Smith, Sydney</StartCity><StartCityName Label=\"Embark City\"/><EndServiceDate Label=\"End Service Date\">16 Mar 18</EndServiceDate><EndCity>MELBOURNE, AUSTRALIA</EndCity><EndCityName Label=\"Disembark City\"/><CostingNotes/><CostingFinancials><AppliedRate>1.00</AppliedRate><CurrencyCode>AUD</CurrencyCode><BaseFare>227.27</BaseFare><Discount Label=\"Discount ex GST\">0.00</Discount><TaxesAndFeesExGST>0.00</TaxesAndFeesExGST><GST Label=\"GST\">22.73</GST><ClientDue>250.00</ClientDue><PaymentType>Prepaid</PaymentType><ChargeBackFlag ChargebackDateValue=\"\">N</ChargeBackFlag></CostingFinancials><CancellationDescription Label=\"Cancellation\">24 hours cancellation notice required</CancellationDescription></CostingItem><DocumentTotals><TotalPayDirect><Total><BaseFare>363.64</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>36.36</GST><ClientDue>400.00</ClientDue></Total></TotalPayDirect><TotalPrePaidExcChargeback><Total><BaseFare>227.27</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>22.73</GST><ClientDue>250.00</ClientDue></Total></TotalPrePaidExcChargeback><TotalChargebacks><Total><BaseFare>0.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>0.00</ClientDue></Total></TotalChargebacks><TotalExcPayDirect><Total><BaseFare>227.27</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>22.73</GST><ClientDue>250.00</ClientDue></Total></TotalExcPayDirect><TotalIncPayDirect><Total><BaseFare>590.91</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>59.09</GST><ClientDue>650.00</ClientDue></Total></TotalIncPayDirect><TotalPaid>0.00</TotalPaid><TotalOutstandingExcPayDirect>250.00</TotalOutstandingExcPayDirect><TotalOutstandingIncPayDirect>650.00</TotalOutstandingIncPayDirect><Currency>AUD</Currency></DocumentTotals></CostingsItems></Booking></DocumentInformation>";

  @Test
  public void testTramadaHelper14() throws Exception {
    System.out.println("Testing Tramada 14");
    String token = "1Lzy8YPNgNY";

    HttpClient c = HttpClientBuilder.create().build();
    HttpPost p = new HttpPost("http://localhost:9000/tramada/importBooking");
    p.setHeader("Content-Type", "application/xml");
    p.setHeader("Authorization", "Bearer " + token);
    StringEntity se = new StringEntity(xml14);
    p.setEntity(se);

    HttpResponse r = c.execute(p);
    int code = r.getStatusLine().getStatusCode();
    System.out.println(code);
    String result = EntityUtils.toString(r.getEntity());
    assertTrue("Success Code - 200!!", code == 200);
  }*/

  String xml15 = "<DocumentInformation><Agency><AgencyName Label=\"TMC\">Demo Travel Services</AgencyName><AgencyCode Label=\"TMC Code\">trunk_tramada</AgencyCode><AgencyAddress><Address><Address1 Label=\"Address Line 1\">Level 10</Address1><Address2 Label=\" Address Line 2\">115 Pitt Street</Address2><Address3 Label=\" Address Line 3\">Sydney</Address3><PostCode Label=\" Postcode\">2000</PostCode><State Label=\"State\">NSW</State><Country Label=\"Country\">Australia</Country></Address></AgencyAddress><AgencyContacts><AgencyEmail Label=\"Email: \">training@tramada.com</AgencyEmail><AgencyPhone Label=\"Phone: \">02 8227 7333</AgencyPhone><AgencyFax Label=\"Fax No.: \"/></AgencyContacts><PccCode Label=\"PCC Code: \">SYDA93100</PccCode></Agency><DocumentDetails><DocumentPrintDate>Tuesday 26 June 2018 13:51</DocumentPrintDate><DocumentPrintTime>Sydney, NSW</DocumentPrintTime><DocumentTitle Label=\"Type\">Itinerary - Full</DocumentTitle><DocumentAddress><Address/></DocumentAddress><DocumentContactDetails><Website Label=\"Website: \">www.demotravel.com.au</Website><Email Label=\"Email: \">mark@tramada.com</Email></DocumentContactDetails><DocumentCommercialDetails><LicNo Label=\"Licence No.: \">3XX474556</LicNo><ABN Label=\"ABN: \">11 222 333 444</ABN><ACN Label=\"ACN: \">999 888 777</ACN></DocumentCommercialDetails><DocumentHeaders/><DocumentFooters/><CarbonEmissionInformation/></DocumentDetails><Booking><BookingDetails><BookingNumber Label=\"Booking ID: \">34857</BookingNumber><BookingLevel1Branch Label=\"Branch: \">Demo Travel Services</BookingLevel1Branch><PNRDetails><PNRList/></PNRDetails><ClientCode Label=\"Client Code: \">KADRY/MARK MR</ClientCode><BookingConsultant1>Tanya Haylock</BookingConsultant1><BookingConsultant1EmailAddress>mark@tramada.com</BookingConsultant1EmailAddress><BookingConsultant2/><TravelArranger/><TravelApprover/><BookingDestinationCity/><BookingDepartureDate Label=\"Departure Date: \">08 Sep 18</BookingDepartureDate><BookingReturnDate Label=\"Return Date: \">30 Sep 18</BookingReturnDate><BookingFinalTKTDate Label=\"Final TKT Date: \">12 Jul 18</BookingFinalTKTDate><BookingFinalDueDate Label=\"Final Due Date: \">01 Sep 18</BookingFinalDueDate><BookingCancellationCondition/><BookingDeposits/><BookingDebtors><BookingDebtor><DebtorName Label=\"Debtor: \">Retail</DebtorName></BookingDebtor></BookingDebtors><BookingPassengers><BookingPassenger><PassengerName>KADRY/MARK MR</PassengerName><Memberships><Membership><MembershipType>AIR</MembershipType><MembershipNumber>4790001426</MembershipNumber><MembershipCompanyName>VIRGIN AUSTRALIA</MembershipCompanyName><MembershipCompanyCode>VA</MembershipCompanyCode><MembershipProgramName>VA</MembershipProgramName><MembershipProgramCode>VA_AIR</MembershipProgramCode></Membership><Membership><MembershipType>AIR</MembershipType><MembershipNumber>4790001426</MembershipNumber><MembershipCompanyName>VIRGIN AUSTRALIA</MembershipCompanyName><MembershipCompanyCode>VA</MembershipCompanyCode><MembershipProgramName>VA</MembershipProgramName><MembershipProgramCode>VA_AIR</MembershipProgramCode></Membership></Memberships></BookingPassenger><BookingPassenger><PassengerName>TEUTENBERG/AMY MS</PassengerName><Memberships><Membership><MembershipType>AIR</MembershipType><MembershipNumber>1015774752</MembershipNumber><MembershipCompanyName>VIRGIN AUSTRALIA</MembershipCompanyName><MembershipCompanyCode>VA</MembershipCompanyCode><MembershipProgramName>VA</MembershipProgramName><MembershipProgramCode>VA_AIR</MembershipProgramCode></Membership><Membership><MembershipType>AIR</MembershipType><MembershipNumber>1015774752</MembershipNumber><MembershipCompanyName>VIRGIN AUSTRALIA</MembershipCompanyName><MembershipCompanyCode>VA</MembershipCompanyCode><MembershipProgramName>VA</MembershipProgramName><MembershipProgramCode>VA_AIR</MembershipProgramCode></Membership></Memberships></BookingPassenger><PassengerDetails><PassengerName>TEUTENBERG/AMY MS</PassengerName><PassengerAge/><PassengerEmail/></PassengerDetails><PassengerDetails><PassengerName>KADRY/MARK MR</PassengerName><PassengerAge/><PassengerEmail/></PassengerDetails><TicketNumbers/><PassengerNameList Label=\"Itinerary For\">KADRY/MARK MR, TEUTENBERG/AMY MS</PassengerNameList></BookingPassengers><BookingStatus Label=\"Booking Status: \">QUOTE</BookingStatus></BookingDetails><ItineraryItems><ItineraryItem><ItemNo>310451</ItemNo><SortingOrderNumber>5</SortingOrderNumber><SegmentTypeCode Label=\"HOTEL\">Hotel</SegmentTypeCode><PassengerDetails><PassengerNameList>KADRY/MARK MR, TEUTENBERG/AMY MS</PassengerNameList></PassengerDetails><Supplier><SupplierType>Hotel Company</SupplierType><SupplierCode Label=\"Supplier Code\">NYC-CM</SupplierCode><SupplierName>citizenM New York Times Square</SupplierName><SupplierAddress><Address><Address1>218 West 50th Street</Address1><Address2/><Address3>New York</Address3><PostCode>10019</PostCode><State>NY</State><Country>United States</Country></Address></SupplierAddress><SupplierPhone Label=\"Phone\">P-+1 212-461-3638</SupplierPhone><SupplierFax/></Supplier><ServiceTypeDescription Label=\"Room Type\">King Room - Free WiFi</ServiceTypeDescription><Start><StartLabelValue>Check In</StartLabelValue><ServiceDate>Sat 08 Sep 18</ServiceDate><ServiceTime>2:00 PM</ServiceTime><CityCode>NYC</CityCode><CityName>NEW YORK CITY, NY</CityName><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></Start><End><EndLabelValue>Check Out</EndLabelValue><ServiceDate>Mon 10 Sep 18</ServiceDate><ServiceTime>11:00 AM</ServiceTime><CityCode/><CityName/><Address><Address1/><Address2/><Address3/><PostCode/><State/><Country/></Address><Contacts><PhoneNo/><FaxNo/></Contacts></End><Status>Confirmed</Status><IssueDate>Thu 12 Apr 18</IssueDate><OrderDate/><ConfirmationNo Label=\"Confirmation No\">1566057483</ConfirmationNo><AccountDetails><PaymentTypeDescription>Prepaid</PaymentTypeDescription><PaymentNarrative>Payment by credit card</PaymentNarrative><SupplierCharges/><SupplierFees/><Inclusions/><Exclusions/><RateGuaranteeDescription/><CancelConditions/><CancellationDescription/></AccountDetails><ItineraryNotes><SegmentNotes/><FareRuleNotes/><HistoricalNotes/></ItineraryNotes><NoOfItems Label=\"No. of Rooms\">1</NoOfItems><NoOfdays UnitOfCount=\"Nights\" Label=\"Duration\">2</NoOfdays><RateInformation><LocalRate Label=\"Local Rate Incl GST\">USD681.73</LocalRate></RateInformation></ItineraryItem></ItineraryItems><CostingsItems><DocumentTotals><TotalPayDirect><Total><BaseFare>0.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>0.00</ClientDue></Total></TotalPayDirect><TotalPrePaidExcChargeback><Total><BaseFare>0.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>0.00</ClientDue></Total></TotalPrePaidExcChargeback><TotalChargebacks><Total><BaseFare>0.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>0.00</ClientDue></Total></TotalChargebacks><TotalExcPayDirect><Total><BaseFare>0.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>0.00</ClientDue></Total></TotalExcPayDirect><TotalIncPayDirect><Total><BaseFare>0.00</BaseFare><Discount>0.00</Discount><TaxesAndFeesExcGST>0.00</TaxesAndFeesExcGST><GST>0.00</GST><ClientDue>0.00</ClientDue></Total></TotalIncPayDirect><TotalPaid>0.00</TotalPaid><TotalOutstandingExcPayDirect>0.00</TotalOutstandingExcPayDirect><TotalOutstandingIncPayDirect>0.00</TotalOutstandingIncPayDirect><Currency>AUD</Currency></DocumentTotals></CostingsItems></Booking></DocumentInformation>";

  @Test
  public void testTramadaHelper15() throws Exception {
    System.out.println("Testing Tramada 15");
    String token = "1Lzy8YPNgNY";

    HttpClient c = HttpClientBuilder.create().build();
    HttpPost p = new HttpPost("http://localhost:9000/tramada/importBooking");
    p.setHeader("Content-Type", "application/xml");
    p.setHeader("Authorization", "Bearer " + token);
    StringEntity se = new StringEntity(xml15);
    p.setEntity(se);

    HttpResponse r = c.execute(p);
    int code = r.getStatusLine().getStatusCode();
    System.out.println(code);
    String result = EntityUtils.toString(r.getEntity());
    assertTrue("Success Code - 200!!", code == 200);
  }

}