package parsers;

import com.mapped.publisher.parse.extractor.booking.valueObject.TransportVO;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFTextStripperByArea;
import org.junit.Assert;
import org.junit.Test;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;

import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.running;

/**
 * Created by ryan on 31/08/16.
 */
public class AutoEuropeParserTest extends Assert{

    private static String[] dateTimeFormat = {"dd-MMM-yy hh:mm a", "MMM d yyyy HH:mm", "MMM dd yyyy HH:mm","MMM d yyyy h:mma", "MMM dd yyyy h:mma","MMM d yyyy hh:mma", "MMM dd yyyy hh:mma", "d MMM yyyy h:mma", "dd MMM yyyy h:mma","d MMM yyyy hh:mma", "dd MMM yyyy hh:mma", "EEEE dd MMM yyyy h:mma", "EEEE MMM dd yyyy h:mma", "M/dd/yyyy", "M/dd/yyyy h:mm:ss a", "dd MMM yyyy", "dd MMM yyyy ", "E dd MMM yyyy h:mma",  "E dd MMM yyyy", "E dd MMM yyyy ", "E dd MMMMM yyyy HH:mm" , "dd MMM yyyy HH:mm a", "E dd MMM yyyy HH:mm" , "dd MMMMM yyyy HH:mm", "E, MMMMM dd, yyyy HH:mm", "E, MMMMM dd, yyyy", "E, MMMMM dd, yyyy hh:mm a", "EEE dd MMM yyyy h:mm a", "EEE dd MMM yyyy 0h:mm a", "MMM dd yyyy h:mma"} ;

    private static DateUtils du = new DateUtils();

    private static String l;
    private static String r;

    @Test
    public void sample_parse() {
        running(fakeApplication(), new Runnable() {
            public void run() {
                try {
                    String dir = "test//parsers//Voucher_US4819686.pdf";
                    TransportVO tr = new TransportVO();
                    File file = new File(dir);
                    InputStream ins = new FileInputStream(file);
                    getPDFContentAsString(ins);
                    //System.out.println(l);
                    //tr = AutoEuropeExtractor.parseTransport();
                    //System.out.println(tr.toString());
                    //System.out.println(r);

                    //String pdf = getPDFContentAsString(ins);
                    //System.out.println(pdf);
                    //System.out.println(pdf);
                    assertTrue("Line below is commented out as it does not allow other tests to run - fix!!!", false);
                    //tr = AutoEuropeExtractor.parseTransport(pdf); //TODO: Ryan, please fix compile
                    //System.out.println(tr.toString());

                    /*BufferedReader bufReader = new BufferedReader(new StringReader(pdf));
                    String line = null;

                    Pattern date = Pattern.compile("(?<!As\\sOf\\s)[0-2][0-9]-[A-Za-z]{3}-[0-9]{2}");
                    Pattern time = Pattern.compile("\\s[0-9][0-9]*:[0-9]{2}\\s(AM|PM)[\\s]?(?!-)");
                    Pattern companyName = Pattern.compile("\\s\\s[A-Za-z]+");
                    Pattern location = Pattern.compile("[A-Z\\s\\-]+");

                    Matcher dateMatcher;
                    Matcher timeMatcher;
                    Matcher companyMatcher;

                    String pickupDate = "";
                    String dropoffDate = "";
                    String pickupTime = "";
                    String dropoffTime = "";

                    Boolean company = false;

                    while((line=bufReader.readLine())!= null)
                    {
                        dateMatcher = date.matcher(line);
                        timeMatcher = time.matcher(line);

                        while(dateMatcher.find()) {
                            if (pickupDate.equals("")) {
                                pickupDate = dateMatcher.group().trim();
                            } else if (dropoffDate.equals("")) {
                                dropoffDate = dateMatcher.group().trim();
                            }
                        }

                        while(timeMatcher.find()) {
                            if(pickupTime.equals("")) {
                                pickupTime = timeMatcher.group().trim();
                                tr.setPickupLocation(line.substring(0, line.indexOf(pickupTime)).trim());
                            }
                            else if (dropoffTime.equals("")) {
                                dropoffTime = timeMatcher.group().trim();
                                System.out.println(line.indexOf(pickupTime) + " " + line.lastIndexOf(dropoffTime));
                                tr.setDropoffLocation(line.substring(line.indexOf(pickupTime) + 9, line.lastIndexOf(dropoffTime)).trim());
                            }
                        }

                        if(!pickupDate.equals("") && !pickupTime.equals("")) {
                            tr.setPickupDate(getTimestamp(pickupDate, pickupTime));
                        }

                        if(!dropoffDate.equals("") && !dropoffTime.equals("")) {
                            tr.setDropoffDate(getTimestamp(dropoffDate, dropoffTime));
                        }

                        if(company) {
                            companyMatcher = companyName.matcher(line);
                            if(companyMatcher.find()) {
                                tr.setCmpyName(companyMatcher.group().trim());
                            }
                            company = false;
                        }

                        if(line.contains("Rental Company:")) {
                            company = true;
                        }

                        //tr.setConfirmationNumber("");
                        //tr.setBookingType("");*/
                    //}
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
    }

    private static final void getPDFContentAsString(InputStream fis) {
        PDDocument pdfDocument = null;
        StringBuilder contents = new StringBuilder();
        try {
            PDFParser parser = new PDFParser(fis);
            parser.parse();

            pdfDocument = parser.getPDDocument();

            //left
            PDFTextStripperByArea stripper = new PDFTextStripperByArea();

            stripper.setSortByPosition(true);
            Rectangle rect = new Rectangle(0, 50, 300, 700);
            stripper.addRegion("body", rect);

            java.util.List<?> allPages = pdfDocument.getDocumentCatalog().getAllPages();

            for (Object pdPage : allPages) {
                stripper.extractRegions((PDPage) pdPage);
                contents.append(stripper.getTextForRegion("body"));
            }
            l = contents.toString();

            //right
            contents = new StringBuilder();
            stripper = new PDFTextStripperByArea();

            stripper.setSortByPosition(true);
            rect = new Rectangle(300, 50, 300, 700);
            stripper.addRegion("body", rect);

            allPages = pdfDocument.getDocumentCatalog().getAllPages();

            for (Object pdPage : allPages) {
                stripper.extractRegions((PDPage) pdPage);
                contents.append(stripper.getTextForRegion("body"));
            }
            r = contents.toString();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Timestamp getTimestamp (String date, String time) {
        try {
            Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime() );
            return t;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
