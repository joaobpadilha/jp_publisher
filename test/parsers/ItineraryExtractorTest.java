package parsers;

import com.umapped.external.wetu.itinerary.ItineraryExtractor;
import com.umapped.external.wetu.itinerary.list.ItineraryList;
import com.umapped.external.wetu.itinerary.details.ItineraryDetail;

import java.util.Arrays;
import org.junit.Assert;
import org.junit.Test;

public class ItineraryExtractorTest extends Assert {

  @Test
  public void testExtractItineraryList() throws Exception {
    ItineraryExtractor data = new ItineraryExtractor();
    ItineraryList list = data.extractItineraryList("DIAKOANOUFR6UB6I","results=10&start=0&filter=All&own=false");
    //System.out.println("This is the JSON details for Accommodation:: \n======================\n"+ Arrays.asList(list).toString()+"\n\n------------END");

    assertTrue("Itinerary List Ids are not the same", list.getTotalResults() == 274);

  }

  @Test
  public void testExtractItineraryDetail() throws Exception {
    ItineraryExtractor data = new ItineraryExtractor();
    ItineraryDetail list = data.extractItineraryDetail("id=DA428A36-34E8-9D48-E910-1054682DB00A");

    //System.out.println("This is the JSON details for Accommodation:: \n======================\n"+ Arrays.asList(list).toString()+"\n\n------------END");

    assertTrue("Accomodation Ids both 64", list.getOperatorId() == 4181);
    //assertTrue("Acommodation Ids both 65", list[1].getMapObjectId() == 65);
    //assertTrue("There are two items in list!", list.length == 2);

  }
}