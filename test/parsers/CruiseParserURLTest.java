package parsers;

import com.mapped.publisher.parse.virtuoso.cruise.CruiseParser;
import com.mapped.publisher.parse.virtuoso.cruise.CruiseVO;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.running;

/**
 * Created by ryan on 04/08/16.
 */
public class CruiseParserURLTest extends Assert {

    public static void main (String[] args) {
      String url = "https://www.virtuoso.com/cruises/sailings/14299465/mediterranean-04oct2016-14oct2016";
      System.out.println("CRUISES");
      CruiseVO cruise = CruiseParser.getCruise(url);
      System.out.println("CRUISES2");
      System.out.println("CRUISES: " + cruise.toJson());

    }

    @Test
    public void testURL() {
      /*
        running(fakeApplication(), new Runnable() {
            public void run() {

               String url = "https://www.virtuoso.com/cruises/sailings/15193329/israel-and-mediterranean-29sep2017-11oct2017";
                String url = "https://www.virtuoso.com/cruises/sailings/14299465/mediterranean-04oct2016-14oct2016";

                System.out.println("CRUISES");
                CruiseVO cruise = CruiseParser.getCruise(url);
                System.out.println("CRUISES2");
                System.out.println("CRUISES: " + cruise.toJson());

                try {
                    HttpClient client = new HttpClient();
                    client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
                    HttpMethod method = new GetMethod(url);
                    method.setFollowRedirects(true);

                    client.executeMethod(method);

                    System.out.println("CRUISE START");
                    InputStream in = method.getResponseBodyAsStream();

                    BufferedReader rd = new BufferedReader(new InputStreamReader(in));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = rd.readLine()) != null) {
                        result.append(line);
                    }

                    Document cruiseDoc = Jsoup.parse(result.toString());
                    System.out.println("CRUISES");
                    //System.out.println(cruiseDoc.toString());

                    System.out.println(cruiseDoc.getElementById("titleName").text());

                }
                catch(Exception e) {
                    System.out.println(e.getMessage());
                }

            }
            });
     */
        //CruiseParser.getCruise();

      assertTrue("OK", true);
    }
}
