package parsers;

import com.mapped.publisher.parse.extractor.booking.Travel2Extractor;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import models.publisher.FileInfo;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.running;


/**
 * Created by twong on 2016-07-11.
 */
public class Travel2ExtractorTest extends Assert {

  //@Test
  public void sample_invoice1()  {
    try {
      String dir = "/volumes/data2/Downloads/t2-prop1.pdf";//"/home/twong/temp/travel2/space1.pdf";//"/users/twong/Dropbox/UMapped_Share/Integration Samples/Travel2/US518303_itn.pdf"; //"/users/twong/Dropbox/UMapped_Share/Companies/Travel2/reumappedtravel2nextsteps/196402_itn.pdf";
      // String dir = "/users/twong/downloads/travel2.pdf";
      File file = new File(dir);
      InputStream ins = new FileInputStream(file);

      //System.out.println(getPDFContentAsString(ins));

      Travel2Extractor p = new Travel2Extractor();
      TripVO t = p.extractData(ins);
      //p.postProcessingImpl("1", "2");
      System.out.println(t);

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

 // @Test
  public void sample_invoice2() {
    running(fakeApplication(), new Runnable() {
      public void run() {
        try {
          FileInfo f = FileInfo.find.byId(Long.valueOf(1003916792430000002L));
          System.out.println("H E L L O");
          assertTrue("ok", true);
        }
        catch (Exception e) {
          assertTrue(e.getMessage(), false);
        }
      }
    });
  }


}
