package parsers;

import org.junit.Assert;
import org.junit.Test;

import com.mapped.publisher.parse.traxo.TraxoHelper;
//import com.mapped.publisher.parse.traxo.*;
import com.mapped.publisher.parse.schemaorg.*;

import java.util.ArrayList;
import java.util.List;

public class TraxoTest extends Assert {

  @Test
  public void testTraxoParser() throws Exception {
    String s = "{\"created\": \"2015-04-10T04:49:34+00:00\"," +
        "    \"data\": {" +
        "        \"object\": {" +
        "            \"created\": \"2015-04-10T04:49:21+0000\"," +
        "            \"from_address\": \"testuser@example.com\"," +
        "            \"id\": \"405612237952923800\"," +
        "            \"mailbox_id\": \"405621341795997700\"," +
        "            \"modified\": \"2015-04-10T04:49:34+0000\"," +
        "            \"segments\": [" +
        "                {" +
        "    \"activity_type\": \"General\"," +
        "    \"activity_name\": \"Van Halen\"," +
        "    \"first_name\": \"Don\"," +
        "    \"last_name\": \"Draper\"," +
        "    \"start_name\": \"Park City Ampitheater\"," +
        "    \"start_admin_code\": \"UT\"," +
        "    \"start_address1\": \"1895 Sidewinder Drive\"," +
        "    \"start_address2\": null," +
        "    \"start_city_name\": \"Park City\"," +
        "    \"start_country\": \"US\"," +
        "    \"start_lat\": \"40.6460609436035\"," +
        "    \"start_lon\": \"-111.497970581055\"," +
        "    \"start_postal_code\": \"84060\"," +
        "    \"end_name\": \"Park City Ampitheater\"," +
        "    \"end_admin_code\": \"UT\"," +
        "    \"end_address1\": \"1895 Sidewinder Drive\"," +
        "    \"end_address2\": null," +
        "    \"end_city_name\": \"Park City\"," +
        "    \"end_country\": \"US\"," +
        "    \"end_lat\": \"40.6460609436035\"," +
        "    \"end_lon\": \"-111.497970581055\"," +
        "    \"end_postal_code\": \"84060\"," +
        "    \"start_datetime\": \"2015-03-06 18:00:00\"," +
        "    \"end_datetime\": \"2015-03-10 22:00:00\"," +
        "    \"confirmation_no\": \"12345678\"," +
        "    \"created\": \"2015-04-10T04:49:35+0000\"," +
        "    \"currency\": \"USD\"," +
        "    \"price\": \"1282.58\"," +
        "    \"source\": \"Email\"," +
        "    \"status\": \"Active\"," +
        "    \"time_zone_id\": \"America/Denver\"," +
        "    \"type\": \"Activity\"," +
        "    \"travelers\": [" +
        "      {" +
        "        \"name\": \"Mr. Don Draper\"," +
        "        \"first_name\": \"Don\"," +
        "        \"last_name\": \"Draper\"" +
        "      }" +
        "    ]," +
        "    \"price_details\": [" +
        "      {" +
        "        \"type\": \"total\"," +
        "        \"name\": \"Total\"," +
        "        \"value\": \"50.00\"," +
        "        \"units\": \"USD\"" +
        "      }," +
        "      {" +
        "        \"type\": \"tax\"," +
        "        \"name\": \"Tax\"," +
        "        \"value\": \"8.25\"," +
        "        \"units\": \"USD\"" +
        "      }" +
        "    ]" +
        "}" +
        "            ]," +
        "            \"source\": \"Email\"," +
        "            \"status\": \"Processed\"," +
        "            \"subject\": \"Fwd: Reservation Confirmation #12345678 for Park City Marriott\"," +
        "            \"to_address\": \"plans@example-travelapp.com\"," +
        "            \"user_address\": \"testuser@example.com\"" +
        "        }" +
        "    }," +
        "    \"type\": \"email.updated\"" +
        "}";
    TraxoHelper t = new TraxoHelper();
    t.parseJson(s);
    for (com.mapped.publisher.parse.traxo.Activity a : t.activities) {
      System.out.println(t.getDateTime(a.created));
      System.out.println(t.getDateTime(a.start_datetime));
      System.out.printf(a.price_details.get(0).value);
    }

    assertTrue("Ids are same", t.dataObject.id.equals("405612237952923800"));

  }

  @Test
  public void testToFlightReservation() throws Exception {
    String s = "{\"created\": \"2015-04-10T04:49:34+00:00\"," +
        "    \"data\": {" +
        "        \"object\": {" +
        "            \"created\": \"2015-04-10T04:49:21+0000\"," +
        "            \"from_address\": \"testuser@example.com\"," +
        "            \"id\": \"405612237952923800\"," +
        "            \"mailbox_id\": \"405621341795997700\"," +
        "            \"modified\": \"2015-04-10T04:49:34+0000\"," +
        "            \"segments\": [" +
                          "{\n" +
                            "    \"airline\": \"Alaska Airlines, Inc.\",\n" +
                            "    \"arrival_datetime\": \"2015-06-14T19:39:00\",\n" +
                            "    \"arrival_time_zone_id\": \"America/Los_Angeles\",\n" +
                            "    \"class_of_service\": \"Coach\",\n" +
                            "    \"confirmation_no\": \"ABC123\",\n" +
                            "    \"created\": \"2015-04-10T06:03:07+0000\",\n" +
                            "    \"currency\": \"USD\",\n" +
                            "    \"departure_datetime\": \"2015-06-14T17:40:00\",\n" +
                            "    \"departure_time_zone_id\": \"America/Chicago\",\n" +
                            "    \"destination\": \"PDX\",\n" +
                            "    \"destination_name\": \"Portland International\",\n" +
                            "    \"destination_admin_code\": \"OR\",\n" +
                            "    \"destination_city_name\": \"Portland\",\n" +
                            "    \"destination_country\": \"US\",\n" +
                            "    \"destination_lat\": \"45.5833320617676\",\n" +
                            "    \"destination_lon\": \"-122.599998474121\",\n" +
                            "    \"fare_basis_code\": \"G\",\n" +
                            "    \"first_name\": \"Roger\",\n" +
                            "    \"flight_number\": \"655\",\n" +
                            "    \"iata_code\": \"AS\",\n" +
                            "    \"last_name\": \"Sterling\",\n" +
                            "    \"normalized_airline\": \"ASA\",\n" +
                            "    \"number_of_pax\": \"1\",\n" +
                            "    \"origin\": \"DFW\",\n" +
                            "    \"origin_name\": \"Dallas/Fort Worth International\",\n" +
                            "    \"origin_admin_code\": \"TX\",\n" +
                            "    \"origin_city_name\": \"Dallas\",\n" +
                            "    \"origin_country\": \"US\",\n" +
                            "    \"origin_lat\": \"32.9000015258789\",\n" +
                            "    \"origin_lon\": \"-97.0500030517578\",\n" +
                            "    \"price\": \"380.20\",\n" +
                            "    \"seat_assignment\": \"22D\",\n" +
                            "    \"source\": \"Alaskaairlines\",\n" +
                            "    \"status\": \"Active\",\n" +
                            "    \"ticket_number\": \"1234567890\",\n" +
                            "    \"type\": \"Air\",\n" +
                            "    \"travelers\": [\n" +
                            "      {\n" +
                            "        \"name\": \"Roger Sterling\",\n" +
                            "        \"first_name\": \"Roger\",\n" +
                            "        \"last_name\": \"Sterling\"\n" +
                            "      }\n" +
                            "    ],\n" +
                            "    \"tickets\": [\n" +
                            "      \"1234567890\"\n" +
                            "    ],\n" +
                            "    \"seats\": [\n" +
                            "      \"22D\"\n" +
                            "    ],\n" +
                            "    \"price_details\": [\n" +
                            "      {\n" +
                            "        \"type\": \"total\",\n" +
                            "        \"name\": \"Total\",\n" +
                            "        \"value\": \"380.20\",\n" +
                            "        \"units\": \"USD\"\n" +
                            "      }\n" +
                            "   ]\n" +
                          "},\n" +
                          "{\n" +
                          "    \"car_company\": \"Hertz\",\n" +
                          "    \"car_description\": null,\n" +
                          "    \"car_type\": \"Intermediate 2 or 4 dr., ICAR (C\",\n" +
                          "    \"confirmation_no\": \"G0000000000\",\n" +
                          "    \"created\": \"2015-04-10T06:06:35+0000\",\n" +
                          "    \"currency\": \"USD\",\n" +
                          "    \"dropoff_address1\": \"3500 South Las Vegas Boulevard\",\n" +
                          "    \"dropoff_address2\": null,\n" +
                          "    \"dropoff_admin_code\": \"NV\",\n" +
                          "    \"dropoff_city_name\": \"Las Vegas\",\n" +
                          "    \"dropoff_country\": \"US\",\n" +
                          "    \"dropoff_datetime\": \"2015-07-10T12:00:00\",\n" +
                          "    \"dropoff_lat\": \"36.1749687194824\",\n" +
                          "    \"dropoff_lon\": \"-115.137222290039\",\n" +
                          "    \"dropoff_postal_code\": \"89109\",\n" +
                          "    \"dropoff_time_zone_id\": \"America/Los_Angeles\",\n" +
                          "    \"first_name\": \"Pete\",\n" +
                          "    \"hours_of_operation\": \"Mon-Sun 7:00am-1:00pm 2:00pm-5:00pm\",\n" +
                          "    \"last_name\": \"Campbell\",\n" +
                          "    \"normalized_car_company\": \"ZE\",\n" +
                          "    \"pickup_address1\": \"3500 South Las Vegas Boulevard\",\n" +
                          "    \"pickup_address2\": null,\n" +
                          "    \"pickup_admin_code\": \"NV\",\n" +
                          "    \"pickup_city_name\": \"Las Vegas\",\n" +
                          "    \"pickup_country\": \"US\",\n" +
                          "    \"pickup_datetime\": \"2015-07-09T12:00:00\",\n" +
                          "    \"pickup_lat\": \"36.1749687194824\",\n" +
                          "    \"pickup_lon\": \"-115.137222290039\",\n" +
                          "    \"pickup_postal_code\": \"89109\",\n" +
                          "    \"pickup_time_zone_id\": \"America/Los_Angeles\",\n" +
                          "    \"price\": \"60.59\",\n" +
                          "    \"source\": \"Hertz\",\n" +
                          "    \"status\": \"Active\",\n" +
                          "    \"type\": \"Car\",\n" +
                          "\t\"travelers\": [\n" +
                          "      {\n" +
                          "          \"name\": \"Pete Campbell\",\n" +
                          "          \"first_name\": \"Pete\",\n" +
                          "          \"last_name\": \"Campbell\"\n" +
                          "      }\n" +
                          "    ],\n" +
                          "    \"price_details\": [\n" +
                          "      {\n" +
                          "          \"type\": \"total\",\n" +
                          "          \"name\": \"Total\",\n" +
                          "          \"value\": \"60.59\",\n" +
                          "          \"units\": \"USD\"\n" +
                          "      },\n" +
                          "      {\n" +
                          "          \"type\": \"tax\",\n" +
                          "          \"name\": \"Tax\",\n" +
                          "          \"value\": \"9.63\",\n" +
                          "          \"units\": \"USD\"\n" +
                          "      }\n" +
                          "    ]\n" +
                          "}," +
                          "{\n" +
                          "    \"address1\": \"1895 Sidewinder Drive\",\n" +
                          "    \"address2\": null,\n" +
                          "    \"admin_code\": \"UT\",\n" +
                          "    \"cancellation_policy\": \"\\u2022 Please note that a change in the length or dates of your reservation may result in a rate change. \\u2022 To ensure that you receive this special rate, we will charge your credit card a prepayment of 1,282.58 USD. \\u2022 Please send a check or money order prepayment to PARK CITY MARRIOTT PO BOX 684447 ATTN: RESERVATIONS PARK CITY UT 84060 US \\u2022 You may cancel your reservation for no charge until Tuesday, January 20, 2015 (45 day[s] before arrival). \\u2022 Please note that we will assess a fee of 641.29 USD if you must cancel after this deadline. If you have made a prepayment, we will retain all or part of your prepayment. If not, we will charge your credit card. \\u2022 Travel agents: please note that this rate is commissionable. RATE GUARANTEE LIMITATION(S) \\u2022 Changes in taxes or fees implemented after booking will affect the total room price. \\u2022 Please note that a change in the length or dates of your reservation may result in a rate change. ADDITIONAL INFORMATION \\u2022 The Responsible Tourist and Traveler A practical guide to help you make your trip an enriching experience\",\n" +
                          "    \"checkin_date\": \"2015-03-06\",\n" +
                          "    \"checkout_date\": \"2015-03-10\",\n" +
                          "    \"city_name\": \"Park City\",\n" +
                          "    \"confirmation_no\": \"12345678\",\n" +
                          "    \"country\": \"US\",\n" +
                          "    \"created\": \"2015-04-10T04:49:35+0000\",\n" +
                          "    \"currency\": \"USD\",\n" +
                          "    \"first_name\": \"Don\",\n" +
                          "    \"hotel_name\": \"Park City Marriott\",\n" +
                          "    \"last_name\": \"Draper\",\n" +
                          "    \"lat\": \"40.6460609436035\",\n" +
                          "    \"lon\": \"-111.497970581055\",\n" +
                          "    \"number_of_rooms\": \"1\",\n" +
                          "    \"postal_code\": \"84060\",\n" +
                          "    \"price\": \"1282.58\",\n" +
                          "    \"rate_description\": null,\n" +
                          "    \"room_description\": null,\n" +
                          "    \"room_type\": null,\n" +
                          "    \"source\": \"Marriott\",\n" +
                          "    \"status\": \"Active\",\n" +
                          "    \"time_zone_id\": \"America/Denver\",\n" +
                          "    \"type\": \"Hotel\",\n" +
                          "    \"travelers\": [\n" +
                          "      {\n" +
                          "        \"name\": \"Mr. Don Draper\",\n" +
                          "        \"first_name\": \"Don\",\n" +
                          "        \"last_name\": \"Draper\"\n" +
                          "      }\n" +
                          "    ],\n" +
                          "    \"price_details\": [\n" +
                          "      {\n" +
                          "        \"type\": \"total\",\n" +
                          "        \"name\": \"Total\",\n" +
                          "        \"value\": \"1282.58\",\n" +
                          "        \"units\": \"USD\"\n" +
                          "      },\n" +
                          "      {\n" +
                          "        \"type\": \"tax\",\n" +
                          "        \"name\": \"Tax\",\n" +
                          "        \"value\": \"31.64\",\n" +
                          "        \"units\": \"USD\"\n" +
                          "      }\n" +
                          "    ]\n" +
                          "}," +
                          "{\n" +
                          "    \"rail_line\": \"Amtrak\",\n" +
                          "    \"arrival_datetime\": \"2015-06-14T19:39:00\",\n" +
                          "    \"arrival_time_zone_id\": \"America/Los_Angeles\",\n" +
                          "    \"confirmation_no\": \"ABC123\",\n" +
                          "    \"created\": \"2015-04-10T06:03:07+0000\",\n" +
                          "    \"currency\": \"USD\",\n" +
                          "    \"departure_datetime\": \"2015-06-14T17:40:00\",\n" +
                          "    \"departure_time_zone_id\": \"America/Chicago\",\n" +
                          "    \"destination\": \"PDX\",\n" +
                          "    \"destination_name\": null,\n" +
                          "    \"destination_admin_code\": \"OR\",\n" +
                          "    \"destination_city_name\": \"Portland\",\n" +
                          "    \"destination_country\": \"US\",\n" +
                          "    \"destination_lat\": \"45.5833320617676\",\n" +
                          "    \"destination_lon\": \"-122.599998474121\",\n" +
                          "    \"first_name\": \"Roger\",\n" +
                          "    \"last_name\": \"Sterling\",\n" +
                          "    \"number_of_pax\": \"1\",\n" +
                          "    \"origin\": \"DAL\",\n" +
                          "    \"origin_name\": null,\n" +
                          "    \"origin_admin_code\": \"TX\",\n" +
                          "    \"origin_city_name\": \"Dallas\",\n" +
                          "    \"origin_country\": \"US\",\n" +
                          "    \"origin_lat\": \"32.9000015258789\",\n" +
                          "    \"origin_lon\": \"-97.0500030517578\",\n" +
                          "    \"price\": \"380.20\",\n" +
                          "    \"seat_assignment\": \"22D\",\n" +
                          "    \"source\": \"Amtrak\",\n" +
                          "    \"status\": \"Active\",\n" +
                          "    \"ticket_number\": \"1234567890\",\n" +
                          "    \"type\": \"Rail\",\n" +
                          "    \"train_number\": \"1234\",\n" +
                          "    \"travelers\": [\n" +
                          "      {\n" +
                          "        \"name\": \"Roger Sterling\",\n" +
                          "        \"first_name\": \"Roger\",\n" +
                          "        \"last_name\": \"Sterling\"\n" +
                          "      }\n" +
                          "    ],\n" +
                          "    \"tickets\": [\n" +
                          "      \"1234567890\"\n" +
                          "    ],\n" +
                          "    \"seats\": [\n" +
                          "      \"22D\"\n" +
                          "    ],\n" +
                          "    \"price_details\": [\n" +
                          "      {\n" +
                          "        \"type\": \"total\",\n" +
                          "        \"name\": \"Total\",\n" +
                          "        \"value\": \"380.20\",\n" +
                          "        \"units\": \"USD\"\n" +
                          "      }\n" +
                          "   ]\n" +
                          "}," +
                          "{" +

                            "    \"activity_type\": \"General\"," +
                            "    \"activity_name\": \"Van Halen\"," +
                            "    \"first_name\": \"Don\"," +
                            "    \"last_name\": \"Draper\"," +
                            "    \"start_name\": \"Park City Ampitheater\"," +
                            "    \"start_admin_code\": \"UT\"," +
                            "    \"start_address1\": \"1895 Sidewinder Drive\"," +
                            "    \"start_address2\": null," +
                            "    \"start_city_name\": \"Park City\"," +
                            "    \"start_country\": \"US\"," +
                            "    \"start_lat\": \"40.6460609436035\"," +
                            "    \"start_lon\": \"-111.497970581055\"," +
                            "    \"start_postal_code\": \"84060\"," +
                            "    \"end_name\": \"Park City Ampitheater\"," +
                            "    \"end_admin_code\": \"UT\"," +
                            "    \"end_address1\": \"1895 Sidewinder Drive\"," +
                            "    \"end_address2\": null," +
                            "    \"end_city_name\": \"Park City\"," +
                            "    \"end_country\": \"US\"," +
                            "    \"end_lat\": \"40.6460609436035\"," +
                            "    \"end_lon\": \"-111.497970581055\"," +
                            "    \"end_postal_code\": \"84060\"," +
                            "    \"start_datetime\": \"2015-03-06 18:00:00\"," +
                            "    \"end_datetime\": \"2015-03-10 22:00:00\"," +
                            "    \"confirmation_no\": \"12345678\"," +
                            "    \"created\": \"2015-04-10T04:49:35+0000\"," +
                            "    \"currency\": \"USD\"," +
                            "    \"price\": \"1282.58\"," +
                            "    \"source\": \"Email\"," +
                            "    \"status\": \"Active\"," +
                            "    \"time_zone_id\": \"America/Denver\"," +
                            "    \"type\": \"Activity\"," +
                            "    \"travelers\": [" +
                            "      {" +
                            "        \"name\": \"Mr. Don Draper\"," +
                            "        \"first_name\": \"Don\"," +
                            "        \"last_name\": \"Draper\"" +
                            "      }" +
                            "    ]," +
                            "    \"price_details\": [" +
                            "      {" +
                            "        \"type\": \"total\"," +
                            "        \"name\": \"Total\"," +
                            "        \"value\": \"50.00\"," +
                            "        \"units\": \"USD\"" +
                            "      }," +
                            "      {" +
                            "        \"type\": \"tax\"," +
                            "        \"name\": \"Tax\"," +
                            "        \"value\": \"8.25\"," +
                            "        \"units\": \"USD\"" +
                            "      }" +
                            "    ]" +
                        "}" +
        "            ]," +
        "            \"source\": \"Email\"," +
        "            \"status\": \"Processed\"," +
        "            \"subject\": \"Fwd: Reservation Confirmation #12345678 for Park City Marriott\"," +
        "            \"to_address\": \"plans@example-travelapp.com\"," +
        "            \"user_address\": \"testuser@example.com\"" +
        "        }" +
        "    }," +
        "    \"type\": \"email.updated\"" +
        "}";
    TraxoHelper t = new TraxoHelper();
    t.parseJson(s);
    List<FlightReservation> flights = t.toFlightReservations();
    for (FlightReservation f : flights) {
      System.out.println("Flight: " + f.reservationFor.airline.name);
      System.out.println(f.reservationNumber);
      System.out.println(f);
    }
    List<LodgingReservation> hotels = t.toHotelReservations();
    for (LodgingReservation l : hotels) {
      System.out.println("Hotel: " + l.reservationFor.name);
      System.out.println(l.reservationNumber);
      System.out.println(l);
    }
    List<RentalCarReservation> cars = t.toCarReservations();
    for (RentalCarReservation c : cars) {
      System.out.println("Rental Car: " + c.reservationFor.rentalCompany.name);
      System.out.println(c.reservationNumber);
      System.out.println(c);
    }
    List<TrainReservation> trains = t.toRailReservations();
    for (TrainReservation tr : trains) {
      System.out.println("Train: " + tr.reservationFor.trainName);
      System.out.println(tr.reservationNumber);
      System.out.println(tr);
    }
    List<ActivityReservation> activities = t.toActivityReservations();
    for (ActivityReservation a : activities) {
      System.out.println("Activity: " + a.name);
      System.out.println(a.reservationNumber);
      System.out.println(a);
    }

    assertTrue("Ids are same", flights.get(0).reservationFor.airline.name.equals("Alaska Airlines, Inc."));
    assertTrue("Ids are same", hotels.get(0).reservationFor.name.equals("Park City Marriott"));
    assertTrue("Ids are same", cars.get(0).reservationFor.rentalCompany.name.equals("Hertz"));
    assertTrue("Ids are same", trains.get(0).reservationFor.trainName.equals("Amtrak"));
    assertTrue("Ids are same", activities.get(0).name.equals("Van Halen"));

  }

}