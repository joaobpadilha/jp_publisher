package parsers;

import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.parse.extractor.booking.WorldMateResult;
import com.mapped.publisher.parse.worldmate.Header;
import com.mapped.publisher.parse.worldmate.WorldmateParsingResult;
import com.mapped.publisher.persistence.RecordStatus;
import com.mapped.publisher.persistence.bookingapi.BookingAPIMgr;
import com.mapped.publisher.persistence.bookingapi.BookingRS;
import controllers.EmailController;
import models.AbstractEBeanModelTest;
import models.publisher.*;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.running;

/**
 * Created by twong on 23/05/17.
 */
public class WorldmateParserTest extends Assert {
    @Test
    public void parser() {
        running(fakeApplication(), new Runnable() {
            public void run() {
                WorldMateResult wmr = null;
                final String EMAIL_HEADER_EPLID = "X-Umapped-EPL-ID";
                final String EMAIL_HEADER_WM_SUB = "Subject";
                final String EMAIL_FROM = "From";
                try {
                    String dir = "/home/twong/Dropbox/sync/wip/pnrs/worldmate/flight4.xml";
                    File file = new File(dir);
                    InputStream ins = new FileInputStream(file);
                    StringWriter writer = new StringWriter();
                    IOUtils.copy(ins, writer);

                    StringReader strReader = new StringReader(writer.toString());
                    IOUtils.closeQuietly(ins);


                    JAXBContext jc = JAXBContext.newInstance("com.mapped.publisher.parse.worldmate");
                    Unmarshaller u = jc.createUnmarshaller();
                    wmr = new WorldMateResult((WorldmateParsingResult) u.unmarshal(strReader));
                } catch (Exception e) {
                    System.out.println("Error parsing xml: " + e.getMessage());
                    e.printStackTrace();
                    System.exit(0);
                }


                String eplIdStr = null;
                String subject = null;
                String from = null;

                for (Header header : wmr.getHeaders().getHeader()) {
                    switch (header.getName()) {
                        case EMAIL_HEADER_EPLID:
                            eplIdStr = header.getValue();
                            break;
                        case EMAIL_HEADER_WM_SUB:
                            subject = header.getValue();
                            break;
                        case EMAIL_FROM:
                            from = EmailController.extractEmailAddress(header.getValue());
                            System.out.println("------------------------\n" + from +"\n---------------------");
                            break;
                    }
                }

                System.out.println("MAIL_Stage_D:WM_CB: Received " + wmr.getStatusCode().name() + " EPLID: " + eplIdStr + " Subject: " + subject);

                if (eplIdStr == null) {
                    if (wmr.getStatusCode() == WorldMateResult.ReturnCode.SUCCESS ||
                            wmr.getStatusCode() == WorldMateResult.ReturnCode.PARTIAL_SUCCESS) {
                        //this is something that was sent directly to the worldmate API
                        //so we send it to the booking inbox
                        String tripName = "Email Confirmation";
                        String email = "";
                        for (Header h : wmr.getHeaders().getHeader()) {
                            if (h.getName().equals("Subject")) {
                                System.out.println("Subject: " + EmailController.cleanupSubject(h.getValue()));

                                tripName = EmailController.cleanupSubject(h.getValue());
                            }
                        }

                        if (wmr.getEndUserEmails() != null && wmr.getEndUserEmails().getUser() != null && wmr.getEndUserEmails().getUser().size() > 0) {
                            email = wmr.getEndUserEmails().getUser().get(0).getEmail().trim();
                            System.out.println("Email: " + email);
                        }
                        email = "thierry+thierry@umapped.com";

                        Account a = Account.findByEmail(email);
                        if (a != null && !a.isTraveler() && a.getState() == RecordStatus.ACTIVE) {
                            AccountCmpyLink cmpyLink = AccountCmpyLink.findActiveAccount(a.getUid());
                            if (cmpyLink != null) {
                                Company c = Company.find.byId(cmpyLink.getCmpyid());
                                if (c != null && c.getStatus() == APPConstants.STATUS_ACTIVE) {
                                    List<BookingRS> newRecs = wmr.getBookingRS(a.getLegacyId(), c.getCmpyid(), tripName);
                                    int insert = 0;
                                    int failed = 0;
                                    for (BookingRS rs : newRecs) {
                                        if (!rs.getState().equals("DELETED")) {
                                            try {
                                                if (rs.createdBy == null) {
                                                    rs.createdBy = "system";
                                                }
                                                if (rs.modifiedBy == null) {
                                                    rs.modifiedBy = "system";
                                                }
                                                BookingAPIMgr.insert(rs);
                                                insert++;
                                            } catch (Exception e) {
                                                failed++;
                                                System.out.println("Worldmate Email Parser: Cannot insert reservation " + email + " - " + tripName + " - " + rs.toString());
                                            }
                                        }
                                    }
                                    //create an EPL log
                                    List<EmailToken> tokens = EmailToken.getCompanyActiveTokens(c.getCmpyid());
                                    EmailToken emailToken = EmailToken.buildCmpyToken(c.getCmpyid());
                                    if (tokens != null && tokens.size() > 0) {
                                        emailToken = tokens.get(0);
                                    }

                                    EmailParseLog epl = EmailParseLog.buildRecord();
                                    epl.setFromAddr(a.getEmail());
                                    epl.setFromName(a.getFullName());
                                    epl.setSubject(tripName);
                                    epl.setAccount(a);
                                    epl.setToken(emailToken);
                                    epl.setState(EmailParseLog.EmailState.PARSED_WM);
                                    epl.setBkCount(insert);
                                    epl.setCreatedTs(new Timestamp(Instant.now().toEpochMilli()));
                                    epl.setSrc(EmailParseLog.EmailSource.DIRECT);
                                    epl.save();
                                    System.out.println("EPL inserted: " + epl.getPk());
                                } else {
                                    System.out.println("Worldmate Email Parser: unkown cmpy " + email + " - " + tripName);
                                }
                            } else {
                                System.out.println("Worldmate Email Parser: unkown cmpy linking " + email + " - " + tripName);
                            }

                        } else {
                            System.out.println("Worldmate Email Parser: unkown email " + email + " - " + tripName);
                        }

                    } else {


                        System.out.println("EMAIL_Stage_D:WM_CB: Worldmate didn't not return all headers");
                    }
                    System.out.println("OK");
                }
            }
        });


    }
}
