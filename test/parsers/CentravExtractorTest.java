package parsers;

import com.mapped.publisher.parse.extractor.booking.email.CentravExtractor;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by surge on 2016-04-20.
 */
public class CentravExtractorTest
    extends Assert {

  @Test
  public void sample_20150209_4MH765()  {
    String input =
        "\"---------- Forwarded message ----------\n" +
        "From: Jessica MacKinnon <jessica@umapped.com>\n" +
        "Date: Wed, Sep 2, 2015 at 10:01 PM\n" +
        "Subject: Fwd: Centrav E-Ticket Confirmation for Record 4MH765\n" +
        "To: Thierry Wong <thierry@umapped.com>\n" +
        "\n" +
        "\n" +
        "\n" +
        "Jessica MacKinnon\n" +
        "Manager, Customer Experience\n" +
        "Umapped <http://www.umapped.com>\n" +
        "\n" +
        "Sign up for a free introductory webinar <http://umapped.com/webinars>\n" +
        "Visit our Support Portal <http://support.umapped.com> for helpful guides\n" +
        "and videos\n" +
        "\n" +
        "---------- Forwarded message ----------\n" +
        "From: <jane@knowandgotravel.com>\n" +
        "Date: Wed, Sep 2, 2015 at 5:36 PM\n" +
        "Subject: FWD: Centrav E-Ticket Confirmation for Record 4MH765\n" +
        "To: jessica@umapped.com\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "Kind Regards,\n" +
        "Jane\n" +
        "\n" +
        "Jane Rutledge\n" +
        "Phone:256-783-4694\n" +
        "FAX:256-534-4652\n" +
        "jane@knowandgotravel.com\n" +
        "www.KnowAndGoTravel.com\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "--------- Original Message ---------\n" +
        "Subject: Centrav E-Ticket Confirmation for Record 4MH765\n" +
        "From: \"Centrav Travel Documents\" <messages@techtrav.net>\n" +
        "Date: 9/2/15 10:45 am\n" +
        "To: jane@knowandgotravel.com\n" +
        "\n" +
        "Centrav Inc.\n" +
        "[image: Centrav Inc. - www.centrav.com] <http://www.centrav.com>\n" +
        "E-Tickets have been Issued for 4MH765\n" +
        "This is your passenger receipt. Please *carefully inspect* the details.\n" +
        "Please provide a copy of this receipt, inclusive of ticket numbers, to your\n" +
        "customer.\n" +
        "\n" +
        "   - The issued tickets are subject to considerable penalties for changes\n" +
        "   and/or cancellations.\n" +
        "   - *Trip Cancellation & Interruption Protection* is available for your\n" +
        "   client. For more information read Schedule of Coverages\n" +
        "   <http://www.centrav.com/documents/EN/scheduleofCoverages.pdf>.\n" +
        "\n" +
        "If your client has purchased travel protection, please provide them with a\n" +
        "copy of the Insurance Certificate\n" +
        "<http://www.centrav.com/documents/EN/descriptionofCoverage.pdf>\n" +
        "(Description of Coverage).\n" +
        "\n" +
        "**IMPORTANT** Please advise your customers to *reconfirm* their flights\n" +
        "with the airline 72 hours prior to departure and return.\n" +
        "\n" +
        "Record Information\n" +
        "*Record Locator*: 4MH765\n" +
        "*Ticket Type*: e-ticket\n" +
        "*Departure Date*: 04/02/2016\n" +
        "*Date of Issue*:09/02/2015\n" +
        "*Ticket Numbers*:\n" +
        "0067638598129 - WORTHINGTON/JOHNTODD\n" +
        "0067638598130 - WORTHINGTON/JOHNTODD\n" +
        "0067638598131 - WORTHINGTON/PAMELATHOMAS\n" +
        "0067638598132 - WORTHINGTON/PAMELATHOMAS\n" +
        "*Status*: ticketed\n" +
        "\n" +
        "*Passengers*\n" +
        "Worthington/John.Todd*ADT\n" +
        "Worthington/Pamela.Thomas*ADT\n" +
        "\n" +
        "*Itinerary*\n" +
        "[image: DL - Delta] Delta 1380\n" +
        "Class: T\n" +
        "Aircraft: 717\n" +
        "Airline RL: HKRKER\n" +
        "*Depart* 02Apr 11:05am - Huntsville (HSV) *Arrive* 02Apr 1:06pm - Atlanta\n" +
        "(ATL)\n" +
        "Seats: not assigned\n" +
        "Miles: 143\n" +
        "Elapse: 1.01 Hours\n" +
        "... Connection Time: 3 hours 39 minutes ... [image: DL - Delta] Delta 130\n" +
        "Class: T\n" +
        "Aircraft: 333\n" +
        "Airline RL: HKRKER\n" +
        "*Depart* 02Apr 4:45pm - Atlanta (ATL) *Arrive* 03Apr 8:25am - Munich (MUC)\n" +
        "Seats: not assigned\n" +
        "Miles: 4787\n" +
        "Elapse: 9.40 Hours\n" +
        "[image: DL - Delta] Delta 9453\n" +
        "Class: V\n" +
        "Aircraft: E90\n" +
        "Airline RL: HKRKER\n" +
        "*Depart* 18Apr 9:30am - Gothenburg (GOT) *Arrive* 18Apr 11:05am - Amsterdam\n" +
        "(AMS)\n" +
        "operated by: klm cityhopper -- kl1154\n" +
        "Seats: not assigned\n" +
        "Miles: 474\n" +
        "Elapse: 1.35 Hours\n" +
        "... Connection Time: 2 hours 15 minutes ... [image: DL - Delta] Delta 75\n" +
        "Class: V\n" +
        "Aircraft: 333\n" +
        "Airline RL: HKRKER\n" +
        "*Depart* 18Apr 1:20pm - Amsterdam (AMS) *Arrive* 18Apr 5:16pm - Atlanta\n" +
        "(ATL)\n" +
        "Seats: not assigned\n" +
        "Miles: 4393\n" +
        "Elapse: 9.56 Hours\n" +
        "... Connection Time: 1 hour 49 minutes ... [image: DL - Delta] Delta 2504\n" +
        "Class: V\n" +
        "Aircraft: 717\n" +
        "Airline RL: HKRKER\n" +
        "*Depart* 18Apr 7:05pm - Atlanta (ATL) *Arrive* 18Apr 7:06pm - Huntsville\n" +
        "(HSV)\n" +
        "Seats: not assigned\n" +
        "Miles: 143\n" +
        "Elapse: 1.01 Hours\n" +
        "\"";

    CentravExtractor ce = new CentravExtractor();
    TripVO tvo = null;
    try {
      tvo = ce.extractData(input);
      assertNotNull(tvo);
      System.out.println(tvo.toString());
      assertTrue("Not all flights found", tvo.getFlights().size() == 5);
      //assertTrue("Not all passengers found", tvo.getPassengers().size() == 2);

    } catch (Exception e) {
      System.err.println(e.getMessage());
      e.printStackTrace();
      assertTrue("Exception....", false);
    }
  }

  @Test
  public void sample_20160315_28LCP7_PUB741() {
    String input =
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "This is a forwarded message\n" +
        "From: \"Centrav Travel Documents\" <messages@techtrav.net>\n" +
        "To: TAI@TAITRAVEL.COM; \n" +
        "Dated: 3/15/2016 5:06:31 PM\n" +
        "Subject: Centrav E-Ticket Confirmation for Record 28LCP7\n" +
        "\n" +
        "\n" +
        "\n" +
        "E-Tickets have been Issued for 28LCP7\n" +
        "This is your passenger receipt. Please carefully inspect the details. Please provide a copy of this receipt," +
        " inclusive of ticket numbers, to your customer. \n" +
        "The issued tickets are subject to considerable penalties for changes and/or cancellations. \n" +
        "Trip Cancellation & Interruption Protection is available for your client. For more information read Schedule" +
        " of Coverages. \n" +
        "If your client has purchased travel protection, please provide them with a copy of the Insurance Certificate" +
        " (Description of Coverage). \n" +
        "\n" +
        "*IMPORTANT* Please advise your customers to reconfirm their flights with the airline 72 hours prior to " +
        "departure and return. \n" +
        "\n" +
        "\n" +
        "Record Information\n" +
        "Record Locator: 28LCP7\n" +
        "Ticket Type: e-ticket\n" +
        "Departure Date: 07/30/2016\n" +
        "Date of Issue:03/15/2016\n" +
        "Ticket Numbers:\n" +
        "PAX 047-7805303087 ET - Brettnacher/Rebecca.Ann\n" +
        "PAX 047-7805303088 ET - Brettnacher/Richard.Wayne\n" +
        "PAX 047-7805303089 ET - Brettnacher/Alison.June\n" +
        "PAX 047-7805303090 ET - Brettnacher/Elizabeth.Ann\n" +
        "Status: ticketed\n" +
        "\n" +
        "\n" +
        "Passengers\n" +
        "Brettnacher/Rebecca.Ann*ADT\n" +
        "Brettnacher/Richard.Wayne*ADT\n" +
        "Brettnacher/Alison.June*ADT\n" +
        "Brettnacher/Elizabeth.Ann*ADT\n" +
        "\n" +
        "\n" +
        "\n" +
        "Itinerary\n" +
        " Air Portugal 1003 \n" +
        "\n" +
        "Class: O\n" +
        "Aircraft: 319\n" +
        "Airline RL: 28LCP7Depart30Jul12:20pm-Madrid(MAD) \n" +
        "Arrive30Jul12:30pm-Porto(OPO)\n" +
        "\n" +
        "Seats: not assigned\n" +
        "Miles: 273\n" +
        " Air Portugal 1937 \n" +
        "\n" +
        "Class: O\n" +
        "Aircraft: AT7\n" +
        "Airline RL: 28LCP7Depart01Aug12:30pm-Porto(OPO) \n" +
        "Arrive01Aug1:30pm-Lisbon(LIS)\n" +
        "\n" +
        "operated by: WHITE AIRWAYS S.A.\n" +
        "Seats: not assigned\n" +
        "Miles: 172\n" +
        "\n" +
        "\n" +
        " ";

    CentravExtractor ce = new CentravExtractor();
    TripVO tvo = null;
    try {
      tvo = ce.extractData(input);
      System.out.println(tvo.toString());

      assertNotNull(tvo);
      assertTrue("Not all flights found", tvo.getFlights().size() == 2);
      //assertTrue("Not all passengers found", tvo.getPassengers().size() == 4);

    } catch (Exception e) {
      System.err.println(e.getMessage());
      e.printStackTrace();
      assertTrue("Exception....", false);
    }
  }

  @Test
  public void sample_20160420_43CGM5_PUB_471() {
    String input =
        "From: \"Centrav Travel Documents\" \n" +
        "To: TAI@TAITRAVEL.COM; \n" +
        "Dated: 4/20/2016 2:06:00 PM\n" +
        "Subject: Centrav E-Ticket Confirmation for Record 43CGM5\n" +
        "Centrav Inc. - www.centrav.com\n" +
        "E-Tickets have been Issued for 43CGM5\n" +
        "This is your passenger receipt. Please carefully inspect the details. Please provide a copy of this receipt," +
        " inclusive of ticket numbers, to your customer.\n" +
        "The issued tickets are subject to considerable penalties for changes and/or cancellations.\n" +
        "Trip Cancellation & Interruption Protection is available for your client. For more information read Schedule" +
        " of Coverages.\n" +
        "If your client has purchased travel protection, please provide them with a copy of the Insurance Certificate" +
        " (Description of Coverage).\n" +
        "IMPORTANT Please advise your customers to reconfirm their flights with the airline 72 hours prior to " +
        "departure and return.\n" +
        "Record Information\n" +
        "Record Locator: 43CGM5\n" +
        "Ticket Type: e-ticket\n" +
        "Departure Date: 06/16/2016\n" +
        "Date of Issue:04/20/2016\n" +
        "Ticket Numbers:\n" +
        "PAX 001-7809500966 ET - Adair/Deborah.J\n" +
        "Status: ticketed\n" +
        "Passengers\n" +
        "Adair/Deborah.J*ADT\n" +
        "Itinerary\n" +
        "AA - American Airlines\tAmerican Airlines 3285\n" +
        "Class: Q\n" +
        "Aircraft: ER4\n" +
        "Airline RL: NANWRD\n" +
        "Depart\t16Jun\t3:19pm\t-\tBloomington-Normal, IL\t(BMI)\n" +
        "Arrive\t16Jun\t5:25pm\t-\tDallas Ft Worth\t(DFW)\n" +
        "operated by: ENVOY AIR AS AMERICAN EAG\n" +
        "Seats: not assigned\n" +
        "Miles: 689\n" +
        "... Connection Time: 1 hour 20 minutes ...\n" +
        "AA - American Airlines\tAmerican Airlines 1075\n" +
        "Class: Q\n" +
        "Aircraft: M80\n" +
        "Airline RL: NANWRD\n" +
        "Depart\t16Jun\t6:45pm\t-\tDallas Ft Worth\t(DFW)\n" +
        "Arrive\t16Jun\t7:25pm\t-\tEl Paso\t(ELP)\n" +
        "Seats: not assigned\n" +
        "Miles: 552\n" +
        "AA - American Airlines\tAmerican Airlines 1330\n" +
        "Class: Q\n" +
        "Aircraft: 319\n" +
        "Airline RL: NANWRD\n" +
        "Depart\t20Jun\t12:08pm\t-\tEl Paso\t(ELP)\n" +
        "Arrive\t20Jun\t2:55pm\t-\tDallas Ft Worth\t(DFW)\n" +
        "Seats: not assigned\n" +
        "Miles: 552\n" +
        "... Connection Time: 1 hour 35 minutes ...\n" +
        "AA - American Airlines\tAmerican Airlines 3506\n" +
        "Class: Q\n" +
        "Aircraft: ER4\n" +
        "Airline RL: NANWRD\n" +
        "Depart\t20Jun\t4:30pm\t-\tDallas Ft Worth\t(DFW)\n" +
        "Arrive\t20Jun\t6:32pm\t-\tBloomington-Normal, IL\t(BMI)\n" +
        "operated by: ENVOY AIR AS AMERICAN EAG\n" +
        "Seats: not assigned\n" +
        "Miles: 689";

    CentravExtractor ce = new CentravExtractor();
    TripVO tvo = null;
    try {
      tvo = ce.extractData(input);
      assertNotNull(tvo);
      System.out.println(tvo.toString());

      assertTrue("Not all flights found", tvo.getFlights().size() == 4);
      //assertTrue("Not all passengers found", tvo.getPassengers().size() == 1);

    } catch (Exception e) {
      System.err.println(e.getMessage());
      e.printStackTrace();
      assertTrue("Exception....", false);
    }
  }

  @Test
  public void sample_20160420_448TJW_PUB_471() {
    String input =
        "This is a forwarded message\n" +
        "From: \"Centrav Travel Documents\" \n" +
        "To: TAI@TAITRAVEL.COM; \n" +
        "Dated: 4/20/2016 1:48:33 PM\n" +
        "Subject: Centrav E-Ticket Confirmation for Record 448TJW\n" +
        "Centrav Inc. - www.centrav.com\n" +
        "E-Tickets have been Issued for 448TJW\n" +
        "This is your passenger receipt. Please carefully inspect the details. Please provide a copy of this receipt," +
        " inclusive of ticket numbers, to your customer.\n" +
        "The issued tickets are subject to considerable penalties for changes and/or cancellations.\n" +
        "Trip Cancellation & Interruption Protection is available for your client. For more information read Schedule" +
        " of Coverages.\n" +
        "If your client has purchased travel protection, please provide them with a copy of the Insurance Certificate" +
        " (Description of Coverage).\n" +
        "IMPORTANT Please advise your customers to reconfirm their flights with the airline 72 hours prior to " +
        "departure and return.\n" +
        "Record Information\n" +
        "Record Locator: 448TJW\n" +
        "Ticket Type: e-ticket\n" +
        "Departure Date: 01/21/2017\n" +
        "Date of Issue:04/20/2016\n" +
        "Ticket Numbers:\n" +
        "PAX 006-7809500953 ET - Dewberry/Mary.Ellen\n" +
        "PAX 006-7809500954 ET - Dewberry/Richard.Elliott\n" +
        "Status: ticketed\n" +
        "Passengers\n" +
        "Dewberry/Mary.Ellen*ADT\n" +
        "Dewberry/Richard.Elliott*ADT\n" +
        "Itinerary\n" +
        "DL - Delta\tDelta 2602\n" +
        "Class: X\n" +
        "Aircraft: M88\n" +
        "Airline RL: GXBE6Z\n" +
        "Depart\t21Jan\t9:45am\t-\tBirmingham, AL\t(BHM)\n" +
        "Arrive\t21Jan\t11:43am\t-\tAtlanta\t(ATL)\n" +
        "Seats: not assigned\n" +
        "Miles: 134\n" +
        "... Connection Time: 57 minutes ...\n" +
        "DL - Delta\tDelta 690\n" +
        "Class: X\n" +
        "Aircraft: 320\n" +
        "Airline RL: GXBE6Z\n" +
        "Depart\t21Jan\t12:40pm\t-\tAtlanta\t(ATL)\n" +
        "Arrive\t21Jan\t3:14pm\t-\tGrand Cayman\t(GCM)\n" +
        "Seats: not assigned\n" +
        "Miles: 1007\n" +
        "DL - Delta\tDelta 508\n" +
        "Class: V\n" +
        "Aircraft: 320\n" +
        "Airline RL: GXBE6Z\n" +
        "Depart\t28Jan\t4:14pm\t-\tGrand Cayman\t(GCM)\n" +
        "Arrive\t28Jan\t7:00pm\t-\tAtlanta\t(ATL)\n" +
        "Seats: not assigned\n" +
        "Miles: 1007\n" +
        "... Connection Time: 2 hours ...\n" +
        "DL - Delta\tDelta 1175\n" +
        "Class: V\n" +
        "Aircraft: 73W\n" +
        "Airline RL: GXBE6Z\n" +
        "Depart\t28Jan\t9:00pm\t-\tAtlanta\t(ATL)\n" +
        "Arrive\t28Jan\t8:56pm\t-\tBirmingham, AL\t(BHM)\n" +
        "Seats: not assigned\n" +
        "Miles: 134";

    CentravExtractor ce = new CentravExtractor();
    TripVO tvo = null;
    try {
      tvo = ce.extractData(input);
      assertNotNull(tvo);
      System.out.println(tvo.toString());
      assertTrue("Not all flights found", tvo.getFlights().size() == 4);
      //assertTrue("Not all passengers found", tvo.getPassengers().size() == 2);

    } catch (Exception e) {
      System.err.println(e.getMessage());
      e.printStackTrace();
      assertTrue("Exception....", false);
    }
  }


  @Test
  public void sample_20160609_ZRPTXU_PUB_831() {
    String input = "\n" +
                   "\n" +
                   "Barbara Oliver CTA, ECC\n" +
                   "\n" +
                   "   Vacation & Honeymoon Specialist\n" +
                   "\n" +
                   "All Together Now Travel\n" +
                   "\n" +
                   "   661-255-9656\n" +
                   "\n" +
                   "\n" +
                   "A Journey Shared...a Memory Forever(r)\n" +
                   "\n" +
                   "From: messages@techtrav.net [mailto:messages@techtrav.net]\n" +
                   "Sent: Wednesday, February 17, 2016 3:04 PM\n" +
                   "To: Barbara Oliver CTA, ECC <boliver@alltogethernowtravel.com>\n" +
                   "Subject: Centrav E-Ticket Confirmation for Record ZRPTXU\n" +
                   "\n" +
                   "[Centrav Inc. - www.centrav.com]<http://www.centrav.com>\n" +
                   "E-Tickets have been Issued for ZRPTXU\n" +
                   "This is your passenger receipt. Please carefully inspect the details. Please provide a copy of " +
                   "this receipt, inclusive of ticket numbers, to your customer.\n" +
                   "\n" +
                   "  *   The issued tickets are subject to considerable penalties for changes and/or cancellations" +
                   ".\n" +
                   "  *   Trip Cancellation & Interruption Protection is available for your client. For more " +
                   "information read Schedule of Coverages<http://www.centrav.com/documents/EN/scheduleofCoverages" +
                   ".pdf>.\n" +
                   "If your client has purchased travel protection, please provide them with a copy of the Insurance " +
                   "Certificate<http://www.centrav.com/documents/EN/descriptionofCoverage.pdf> (Description of " +
                   "Coverage).\n" +
                   "\n" +
                   "*IMPORTANT* Please advise your customers to reconfirm their flights with the airline 72 hours " +
                   "prior to departure and return.\n" +
                   "Record Information\n" +
                   "Record Locator: ZRPTXU\n" +
                   "Ticket Type: e-ticket\n" +
                   "Departure Date: 07/22/2016\n" +
                   "Date of Issue:02/17/2016\n" +
                   "Ticket Numbers:\n" +
                   "PAX 108-7802069243-44 ET - Kentoffio/Frank.J\n" +
                   "PAX 108-7802069245-46 ET - Kentoffio/Joanne\n" +
                   "Status: ticketed\n" +
                   "\n" +
                   "Passengers\n" +
                   "Kentoffio/Frank.J*ADT\n" +
                   "Kentoffio/Joanne*ADT\n" +
                   "\n" +
                   "Itinerary\n" +
                   "[FI - Icelandair]\n" +
                   "\n" +
                   "Icelandair 612\n" +
                   "\n" +
                   "Class: V\n" +
                   "Aircraft: 75W\n" +
                   "Airline RL: ZRPTXU\n" +
                   "\n" +
                   "Depart\n" +
                   "\n" +
                   "22Jul\n" +
                   "\n" +
                   "2:10pm\n" +
                   "\n" +
                   "-\n" +
                   "\n" +
                   "New York\n" +
                   "\n" +
                   "(JFK)\n" +
                   "\n" +
                   "Arrive\n" +
                   "\n" +
                   "22Jul\n" +
                   "\n" +
                   "11:40pm\n" +
                   "\n" +
                   "-\n" +
                   "\n" +
                   "Reykjavik\n" +
                   "\n" +
                   "(KEF)\n" +
                   "\n" +
                   "Seats: not assigned\n" +
                   "Miles: 2592\n" +
                   "\n" +
                   "\n" +
                   "[FI - Icelandair]\n" +
                   "\n" +
                   "Icelandair 304\n" +
                   "\n" +
                   "Class: V\n" +
                   "Aircraft: 75W\n" +
                   "Airline RL: ZRPTXU\n" +
                   "\n" +
                   "Depart\n" +
                   "\n" +
                   "23Jul\n" +
                   "\n" +
                   "1:15am\n" +
                   "\n" +
                   "-\n" +
                   "\n" +
                   "Reykjavik\n" +
                   "\n" +
                   "(KEF)\n" +
                   "\n" +
                   "Arrive\n" +
                   "\n" +
                   "23Jul\n" +
                   "\n" +
                   "6:10am\n" +
                   "\n" +
                   "-\n" +
                   "\n" +
                   "Stockholm\n" +
                   "\n" +
                   "(ARN)\n" +
                   "\n" +
                   "Seats: not assigned\n" +
                   "Miles: 1336\n" +
                   "\n" +
                   "\n" +
                   "[FI - Icelandair]\n" +
                   "\n" +
                   "Icelandair 205\n" +
                   "\n" +
                   "Class: T\n" +
                   "Aircraft: 75W\n" +
                   "Airline RL: ZRPTXU\n" +
                   "\n" +
                   "Depart\n" +
                   "\n" +
                   "06Aug\n" +
                   "\n" +
                   "2:00pm\n" +
                   "\n" +
                   "-\n" +
                   "\n" +
                   "Copenhagen\n" +
                   "\n" +
                   "(CPH)\n" +
                   "\n" +
                   "Arrive\n" +
                   "\n" +
                   "06Aug\n" +
                   "\n" +
                   "3:10pm\n" +
                   "\n" +
                   "-\n" +
                   "\n" +
                   "Reykjavik\n" +
                   "\n" +
                   "(KEF)\n" +
                   "\n" +
                   "Seats: not assigned\n" +
                   "Miles: 1337\n" +
                   "\n" +
                   "... Connection Time: 1 hour 50 minutes ...\n" +
                   "\n" +
                   "[FI - Icelandair]\n" +
                   "\n" +
                   "Icelandair 615\n" +
                   "\n" +
                   "Class: T\n" +
                   "Aircraft: 76W\n" +
                   "Airline RL: ZRPTXU\n" +
                   "\n" +
                   "Depart\n" +
                   "\n" +
                   "06Aug\n" +
                   "\n" +
                   "5:00pm\n" +
                   "\n" +
                   "-\n" +
                   "\n" +
                   "Reykjavik\n" +
                   "\n" +
                   "(KEF)\n" +
                   "\n" +
                   "Arrive\n" +
                   "\n" +
                   "06Aug\n" +
                   "\n" +
                   "7:00pm\n" +
                   "\n" +
                   "-\n" +
                   "\n" +
                   "New York\n" +
                   "\n" +
                   "(JFK)\n" +
                   "\n" +
                   "Seats: not assigned\n" +
                   "Miles: 2592\n" +
                   "\n" +
                   "\n" +
                   "[http://email.techtrav" +
                   "" +
                   ".net/o/eJwNy0ESgyAMAMDXlKMTJYAceExIIjqD0qFov9_ufSVBjGyOxBKcBKLZ28UDoATUHJxdGMRS3F4In3O8p5OOWu5rar2YPWHwq80u88aWVGVmEQwrgI8yY3Smp9zq8Wj_f6p1tKJj13617-j0aJ24nT8zPyoG]\n";

    CentravExtractor ce = new CentravExtractor();
    TripVO tvo = null;
    try {
      tvo = ce.extractData(input);
      assertNotNull(tvo);
      System.out.println(tvo.toString());
      assertTrue("Not all flights found", tvo.getFlights().size() == 4);
      //assertTrue("Not all passengers found", tvo.getPassengers().size() == 2);

    } catch (Exception e) {
      System.err.println(e.getMessage());
      e.printStackTrace();
      assertTrue("Exception....", false);
    }
  }
}
