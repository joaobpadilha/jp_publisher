package com.mapped.publisher.utils;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Test;

public class TestUtils {

  @Test
  public void testParseFlightNumber() {

    Pair<String, String> result = Utils.parseFlightNumber("AC100");
    Assert.assertNotNull(result);
    Assert.assertEquals("AC", result.getLeft());
    Assert.assertEquals("100", result.getRight());

    result = Utils.parseFlightNumber(" AC  100");
    Assert.assertNotNull(result);
    Assert.assertEquals("AC", result.getLeft());
    Assert.assertEquals("100", result.getRight());

    result = Utils.parseFlightNumber("Air Canada  100");
    Assert.assertNull(result);

    result = Utils.parseFlightNumber("9N100");
    Assert.assertNotNull(result);
    Assert.assertEquals("9N", result.getLeft());
    Assert.assertEquals("100", result.getRight());

    result = Utils.parseFlightNumber("9NA100");
    Assert.assertNull(result);

    result = Utils.parseFlightNumber("9N100a");
    Assert.assertNull(result);
  }
}
