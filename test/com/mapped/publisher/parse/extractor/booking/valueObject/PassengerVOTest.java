package com.mapped.publisher.parse.extractor.booking.valueObject;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.umapped.persistence.reservation.UmReservation;
import com.umapped.persistence.reservation.UmTraveler;
import com.umapped.persistence.reservation.flight.UmFlightReservation;
import com.umapped.persistence.reservation.flight.UmFlightTraveler;
import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

public class PassengerVOTest {

  private List<PassengerVO> passengers;
  private List<UmTraveler> travelers;
  
  @Before
  public void setup() {
    passengers = new ArrayList<>();
    
    PassengerVO p = new PassengerVO();
    p.setFirstName("John");
    p.setLastName("Smith");
    p.setFrequentFlyer("F123");
    p.seteTicketNumber("E123456");
    p.setSeat("50E");
    p.setSeatClass("Economic");
    p.setMeal("Vegetarian");

    passengers.add(p);
    p = new PassengerVO();
    p.setFirstName("Jane");
    p.setLastName("Smith");
    p.setFrequentFlyer("F126");
    p.seteTicketNumber("E123457");
    p.setSeat("50G");
    p.setSeatClass("Economic");
    p.setMeal("Regular");
    passengers.add(p);
    
    travelers = PassengerVO.getTravelers(passengers);
  }

  @Test
  public void testToReservation() {
    Assert.assertEquals(passengers.size(), travelers.size());
    UmFlightTraveler t = cast(travelers.get(0), UmFlightTraveler.class);
    PassengerVO p = passengers.get(0);
    Assert.assertEquals(p.getFirstName(), t.getGivenName());
    Assert.assertEquals(p.getLastName(), t.getFamilyName());
    Assert.assertEquals(p.geteTicketNumber(), t.getTicketNumber());
    Assert.assertEquals(p.getFrequentFlyer(), t.getProgram().membershipNumber);
    Assert.assertEquals(p.getSeat(), t.getSeat());
    Assert.assertEquals(p.getSeatClass(), t.getSeatClass().seatCategory);
    Assert.assertEquals(p.getMeal(), t.getMeal());
  }
  
  @Test
  public void testToMPassenger() {
    UmReservation reservation = new UmFlightReservation();
    reservation.setTravelers(travelers);
    List<PassengerVO> ps = PassengerVO.getPassengerVO(reservation);
    Assert.assertEquals(ps.size(), passengers.size());
    UmFlightTraveler t = cast(travelers.get(0), UmFlightTraveler.class);
    PassengerVO p = ps.get(0);
    
    Assert.assertEquals(p.getFirstName(), t.getGivenName());
    Assert.assertEquals(p.getLastName(), t.getFamilyName());
    Assert.assertEquals(p.geteTicketNumber(), t.getTicketNumber());
    Assert.assertEquals(p.getFrequentFlyer(), t.getProgram().membershipNumber);
    Assert.assertEquals(p.getSeat(), t.getSeat());
    Assert.assertEquals(p.getSeatClass(), t.getSeatClass().seatCategory);
    Assert.assertEquals(p.getMeal(), t.getMeal());
  }
}
