package com.umapped.persistence.reservation.utils;

import org.junit.Assert;
import org.junit.Test;

import com.umapped.persistence.reservation.UmReservation;
import com.umapped.persistence.reservation.accommodation.UmAccommodationReservation;
import com.umapped.persistence.reservation.flight.UmFlightReservation;

public class UmReservationUtilsTest {
  @Test
  public void testCast() {
    UmReservation flight = new UmFlightReservation();
    UmReservation accomondation = new UmAccommodationReservation();

    Assert.assertNull(UmReservationUtils.cast(flight, UmAccommodationReservation.class));
    Assert.assertNotNull(UmReservationUtils.cast(flight, UmFlightReservation.class));
  
    Assert.assertNotNull(UmReservationUtils.cast(accomondation, UmAccommodationReservation.class));
    Assert.assertNull(UmReservationUtils.cast(accomondation, UmFlightReservation.class));
 
    Assert.assertNull(UmReservationUtils.cast(null, UmFlightReservation.class));
  }
}
