package com.umapped.mobile.api;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Module;
import models.publisher.Account;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.Application;
import play.ApplicationLoader;
import play.Environment;
import play.inject.guice.GuiceApplicationBuilder;
import play.inject.guice.GuiceApplicationLoader;
import play.test.Helpers;

import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * Created by wei on 2017-07-17.
 */
public class TripQueriesTest {
  @Inject Application application;

  @Inject TripQueries queries;
  @Before
  public void setup() {
    Module testModule = new AbstractModule() {
      @Override
      public void configure() {
        // Install custom test binding here
      }
    };

    GuiceApplicationBuilder builder = new GuiceApplicationLoader()
        .builder(new ApplicationLoader.Context(Environment.simple()))
        .overrides(testModule);
    Guice.createInjector(builder.applicationModule()).injectMembers(this);

    Helpers.start(application);

  }

  @After
  public void teardown() {
    Helpers.stop(application);
  }

  //@Test
  public void testGetItinerary() throws IOException {
    Account account = new Account();
    account.setUid(1416765721790000041L);
    Set<Pair<String, Integer>> trips = queries.getTravelerChangedActiveTrips(account, System.currentTimeMillis());

    System.out.println(trips);
  }

  @Test
  public void testGetPublisherItinerary() throws IOException {
    Account account = new Account();
    account.setLegacyId("thierry");
    Set<Pair<String, Integer>> trips = queries.getPublisherChangedActiveTrips(account, System.currentTimeMillis());

    System.out.println(trips);
  }
}
