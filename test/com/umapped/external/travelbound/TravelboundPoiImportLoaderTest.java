package com.umapped.external.travelbound;

import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.mapped.publisher.persistence.CountriesInfo;
import com.mapped.publisher.utils.S3Util;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import play.Application;
import play.ApplicationLoader;
import play.Environment;
import play.inject.guice.GuiceApplicationBuilder;
import play.inject.guice.GuiceApplicationLoader;
import play.test.Helpers;

import java.util.List;

/**
 * Created by wei on 2017-05-18.
 */
public class TravelboundPoiImportLoaderTest {
  @Inject Application application;

  private TravelboundAPICredential credential;
  private TravelboundPoiImportLoader loader;

  @Before
  public void setup() {
    loader = new TravelboundPoiImportLoader();
    Module testModule = new AbstractModule() {
      @Override
      public void configure() {
        // Install custom test binding here
      }
    };

    GuiceApplicationBuilder builder = new GuiceApplicationLoader()
        .builder(new ApplicationLoader.Context(Environment.simple()))
        .overrides(testModule);
    Guice.createInjector(builder.applicationModule()).injectMembers(this);

    Helpers.start(application);

    credential = new TravelboundAPICredential(46607, "044","API@UMAPPED.COM", "PASS");
  }

  //@Test
  public void testLoadYYZ() {
    CountriesInfo.Country country = CountriesInfo.Instance().byAlpha2("CA");
    loader.load(country, "YYZ", "/Users/wei/DevDocuments/APIs/GTS_API/Toronto");
  }

  @Test
  public void testS3() {
    String prefix = "feeds/Travelbound/";
    List<String> folders = S3Util.getCommonPrefixes(S3Util.getPDFBucketName(), prefix);
    for (String o : folders) {
      System.out.println(o);
    }
  }
}
