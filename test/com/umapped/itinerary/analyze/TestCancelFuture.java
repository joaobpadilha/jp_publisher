package com.umapped.itinerary.analyze;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;

/**
 * Created by wei on 2017-03-15.
 */
public class TestCancelFuture {
  public static void main(String[] args) throws  Exception {
    CompletableFuture<String> worker = CompletableFuture.supplyAsync(
        () -> {
          List<Integer> list = new ArrayList<>();
          for (int j = 0; j < 100000; j++) {
            //System.out.println(j);
            for (int i = 0; i < 1000000; i++) {
              list.add(i);
            }

            for (int i = 0; i < 10000; i++) {
              list.remove(i);
            }
            if (j %100 == 0) {
            //  System.out.println(j);
            }
          }
          System.out.println("Done");
          return "done";
        }
    );
    System.out.println("Sleep for 1 sec");
    Thread.sleep(3000);
    System.out.println("Issuing cancel");
    worker.cancel(true);
    System.out.println("cancel issued");

    if (worker.isCancelled()) {
      System.out.println("Cancelled successfully");
    } else {
      System.out.println(worker.get());
    }
  }
}
