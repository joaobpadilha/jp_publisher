package com.umapped.itinerary.analyze;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.EbeanServerFactory;
import com.avaje.ebean.config.ServerConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Guice;
import com.google.inject.Injector;
import modules.ItineraryAnalyzeModule;
import com.umapped.itinerary.analyze.db.model.ItineraryAnalyzeResultModel;
import com.umapped.itinerary.analyze.recommendation.ItineraryRecommendation;
import com.umapped.itinerary.analyze.recommendation.RecommendationService;

import java.io.StringWriter;
import java.util.List;

/**
 * Created by wei on 2017-03-31.
 */
public class RecommendationServiceTest {
  public static void main(String[] args)
      throws Exception {
    Injector container = Guice.createInjector(new ItineraryAnalyzeModule());

    RecommendationService recommendationService = container.getInstance(RecommendationService.class);

    initDB();

    List<ItineraryAnalyzeResultModel> ars =  Ebean.find(ItineraryAnalyzeResultModel.class).findList();

    if (ars.size() == 0) {
      System.out.println("Analyze Result not found");
      return;
    }

    ObjectMapper om = container.getInstance(ObjectMapper.class);
    for (ItineraryAnalyzeResultModel r : ars) {
      ItineraryAnalyzeResult analyzeResult = om.convertValue(r.getItineraryAnalyzeResult(),
                                                             ItineraryAnalyzeResult.class);
      ItineraryRecommendation recs = recommendationService.recommend(analyzeResult);

      StringWriter w = new StringWriter();

      om.writeValue(w, recs);
      System.out.println(w);
    }
  }

  private static void initDB() {
    ServerConfig config = new ServerConfig();
    config.setName("db");

    config.loadFromProperties();

    // set as default and register so that Model can be
    // used if desired for save() and update() etc
    config.setDefaultServer(true);
    config.setRegister(true);

    EbeanServerFactory.create(config);
  }
}
