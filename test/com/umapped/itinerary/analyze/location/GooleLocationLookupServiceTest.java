package com.umapped.itinerary.analyze.location;

import com.google.inject.Guice;
import com.google.inject.Injector;
import modules.ItineraryAnalyzeModule;
import com.umapped.itinerary.analyze.model.Location;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by wei on 2017-03-22.
 */
public class GooleLocationLookupServiceTest {
  @Test
  public void testGoolgeLookup() throws Exception {
    Injector container = Guice.createInjector(new ItineraryAnalyzeModule());

    LocationLookupService s = container.getInstance(LocationLookupService.class);

    String[] places = new String[]{"Hotel Villa San Pio", "Valamar Argosy Hotel", "Le Meridien Lav, Split", "Rome " +
                                                                                                            "Fiumicino International Airport", "Civitavecchia"};

    List<Location> locationList = new ArrayList<>();

    CompletableFuture all = null;
    for (String p : places) {
      Location loc = new Location();
      loc.setDescription(p);
      CompletionStage cs = CompletableFuture.supplyAsync(() -> s.lookup(loc)).whenComplete((l, e) -> locationList.add(l));
      if (all == null) {
        all = cs.toCompletableFuture();
      }
      else {
        all = CompletableFuture.allOf(all, cs.toCompletableFuture());
      }
    }
    all.join();
    for (Location location : locationList) {
      if (location != null) {
        System.out.println(location);
      }
    }

  }
}
