package com.umapped.itinerary.analyze;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.EbeanServerFactory;
import com.avaje.ebean.SqlRow;
import com.avaje.ebean.config.ServerConfig;

import java.util.List;

/**
 * Created by wei on 2017-03-31.
 */
public class PostGISTest {
  private static void initDB() {
    ServerConfig config = new ServerConfig();
    config.setName("db");

    config.loadFromProperties();

    // set as default and register so that Model can be
    // used if desired for save() and update() etc
    config.setDefaultServer(true);
    config.setRegister(true);

    EbeanServerFactory.create(config);
  }

  public static void main(String[] args) {
    initDB();
    String sql = "SELECT city_id as city_id, name as name, ST_Distance(location, ST_MakePoint(:lng, :lat)) distance " +
                 " FROM wcities_city WHERE ST_Distance(location, ST_MakePoint(:lng, :lat)) <= :radius * 1000 order by distance asc";

    List<SqlRow> rows = Ebean.createSqlQuery(sql).setParameter("lng", -0.461389)
                             .setParameter("lat", 51.4775)
                             .setParameter("radius", 300).findList();
    for (SqlRow r : rows) {
      System.out.println(r.getString("city_id") + " : " + r.getString("name") + " :  " + r.getDouble("distance"));
    }
  }
}
