package com.umapped.itinerary.analyze.geo;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.GeocodingApiRequest;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Test;

/**
 * Created by wei on 2017-02-23.
 */
public class GeoCodingTest  {

    @Test
    public void testGeoCoding() throws Exception {
        GeoApiContext context = GoogleMapApiSupport.getGeoCodingApiContext();
        LatLng location = new LatLng(43.677223, -79.630554);
        GeocodingApiRequest request = GeocodingApi.reverseGeocode(context, location);
        GeocodingResult[] results = request.await();
        for (GeocodingResult r : results) {
            System.out.println(ToStringBuilder.reflectionToString(r, ToStringStyle.MULTI_LINE_STYLE ));
        }
    }
}
